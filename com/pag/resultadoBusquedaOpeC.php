<?php require_once("../../includes/init.php") ?>
<?php if ($_POST): ?> 
<?php 
   $datos=limpiar($_POST['datos']); 
   if (isset($_POST['convenio'])) {
    $idusuario=$_POST['convenio'];
   }else{
    $idusuario=$session->idusuario;
   }
   $datos="".$datos."%";
   $fecha='';
  if (isset($_POST["fecha"])) {
    $fecha=$_POST['fecha'];
    $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
    $desde=date("YmdHis",strtotime($desde));  
    $hasta=date("Ymd",strtotime($hasta)); 
    $hasta=$hasta."235959";
  }
 ?>  
<?php 
  $opOBJ=new Operacion;
  $opOBJ->idConfirmacionOPE=limpiar($idusuario);
  if (empty($fecha)) {
    $datosA=$opOBJ->buscarParaFiltroDatosOPEC($datos);
  } else {
    $datosA=$opOBJ->buscarParaFiltroOPECFecha($datos,$desde,$hasta);  
  }
  $n=1;
 ?>
  <?php if ($datosA): ?>                
  <?php foreach ($datosA as $var): ?>    
    <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>
    <?php $datosF=DatosFacturacion::obtenerDatosFacturacionPorId($var->datosfacturacionid) ?>
    <?php $usuarioV=Usuario::obtenerUsuario($datosF->idUsuarioCreado)  ?>
    <?php $vhOC=Vehiculo::verificarSiEstaDuplicado($datosVehiculo->chasis) ?>
    <?php $datosOpera=Operacion::obtenerOperacionesPorVehiculoIDClienteId($var->idvehiculo,$var->idcliente); ?>
    <?php $datosTrama = TramaIntegracionTY::obtenerTramaIntegracionPorIdOperacion($var->idoperacion);  ?>

<tr class='<?= obtenerClassSinClass($var->lugarInstalacion) ?> <?= ($vhOC->cantidad>1)?"danger":"" ?>  '  style="<?= $datosVehiculo->campana=="Toyota"?"border: 2px solid red !important":"" ?>">
    <td><?= $n++ ?></td>
    <td ><span class="label label-primary"><?= $datosOpera->trabajo ?></span></td>
   <td><?php if($clienteObj->tipoCliente=="Natural"){
        echo strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre);
    }else{
        echo (!empty($clienteObj->nombre))?strtoupper($clienteObj->nombre):strtoupper($clienteObj->razonSocial);
    }
   ?></td>
    <td class="center"> Placa: <?= $datosVehiculo->placa ?> Chasis : <?= $datosVehiculo->chasis ?></td>
    <td class="center"><?= haceMinimo($var->fechaConfirmacionOPE) ?></td>
    <td class="center"><?= !empty($var->fechaInstalacion)?formatoHaceEn($var->fechaInstalacion):"Pendiente" ?></td>
    <td class="center"><?= obtenerEstado($datosVehiculo->estado) ?> <?= obtenerEstado($var->estado) ?></td>    
    <td class="center"><?= obtenerEstado($datosF->estadoFacturacion) ?> <?= empty($datosF->numeroBoleta)?$datosF->numeroFactura:$datosF->numeroBoleta  ?></td>    
    <td class="center"><?= empty($datosF->orden)?'Pendiente':$datosF->orden ?></td>
    <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosVehiculo->campana</span>":$datosVehiculo->campana) ?></td>
    <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosTrama->tipo_transaccion</span>":'') ?></td>
    <td class="center"><?= $usuarioV->usuarioNombre;?>&nbsp;<?= $usuarioV->usuarioApellidoPaterno;  ?></td>
    <td class="center ">
      <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#bateriasOModal" onclick="limpiarChasis()" data-id="<?= $var->idoperacion ?>">Elegir <i class="fa fa-cog"> </i></button>
    </td> 
</tr>
<?php endforeach ?>
   <?php else: ?>
   <tr>   
        <td colspan="8" class="text-center">No se encontro ningun registro con <h2><?= $_POST['datos'] ?>.</h2> </td>
   </tr>
   <?php endif ?>   
 <?php endif ?> 

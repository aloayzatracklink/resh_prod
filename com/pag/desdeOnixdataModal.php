<?php  
if (isset($_POST['chasis'])) {
	include_once("../../includes/init.php");
	$chasis=limpiar(trim($_POST['chasis']));		
	$clienteObj=Onix::ObtenerDatosDesdeChasis($chasis);
	if ($clienteObj) {
		$cantidadObjeto=Onix::obtenerCantididadVehiculos($clienteObj->idcustomer);
		$planobj=Onix::obtenerPlanDeServicio($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation);
	}
?>
<link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
 <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>
	<div class="well" style="padding-top: 11px;
	padding-bottom: 10px;">
	<div class="row">
		<div class="row">
		<h2 class="text-center"><?php 
                        if($clienteObj){  
                        	// echo vence($clienteObj->hasta);
                        }else{
                            echo "CHASIS $chasis No esta en la base de datos consulta este mensaje con pvasquez@tracklink.pe";
                        }
                        ?></h2>
                        <?php if ($clienteObj): ?>                        	
                        <?php if ($cantidadObjeto): ?>
                        	<?php if ($cantidadObjeto->cantidad>1): ?>
                        	<h4 class="text-center text-warning">ATENCION ! Cliente tiene <?php echo $cantidadObjeto->cantidad ?> Vehiculos en Tracklink</h4>	
                        	<?php endif ?>
                        <?php endif ?>
	<input type="hidden" name="idcustomer" value="<?= $clienteObj->idcustomer ?>">
	<input type="hidden" name="idvehicle" value="<?= $clienteObj->idvehicle ?>">
	<input type="hidden" name="idlocation" value="<?= $clienteObj->idlocation ?>">
			<div class="form-group col-md-3">
				<label for="Observaciones" >Dni O Ruc </label>
				<input name="dniORuc" style="border: none;" type="text" placeholder="Web" id="Observaciones" <?php echo $retVal = (!empty($clienteObj->dni)) ? "readonly" : "" ; ?> class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>">
			</div>
			<div class="form-group col-md-5">
			<?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
	<input type="hidden" name="nombres" value="<?= $clienteObj->nombre ?>">
	<input type="hidden" name="apellidos" value="<?= $apellidos ?>">
	<input type="hidden" name="anioF" value="<?= $clienteObj->anio ?>">
				<label for="numeroTecnico" >Nombre de cliente*</label>
				<input  style="border: none;" type="text" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->nombre)) ? "$apellidos"." "."$clienteObj->nombre" : "$apellidos" ; ?> ">
			</div>
			<div class="col-md-2"><label for="estado" >Celular</label>
				<input style="border: none;" type="text" readonly name="celular" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>" >
			</div>
			<div class="col-md-2"><label for="estado" >Celular</label>
				<input style="border: none;" type="text" readonly name="telefono5" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular2)) ? "$clienteObj->celular2" : "" ; ?>">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-6">
				<label for="telefono" >Telefono </label>
				<input style="border: none;" type="text" readonly id="telefono"  class="border-bottom form-control" name="telefono" value="<?php echo $retVal = (!empty($clienteObj->telefonoTrabajo)) ? "$clienteObj->telefonoTrabajo" : "" ; ?>">
			</div>
			<div class="form-group col-md-6">
				<label for="telefono" >Telefono </label>
				<input style="border: none;" type="text" readonly id="telefono"  class="border-bottom form-control" name="telefono2" value="<?php echo $retVal = (!empty($clienteObj->telefono)) ? "$clienteObj->telefono" : "" ; ?>">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label for="Observaciones"  >Correo cliente.</label>
				<input style="border: none;" type="text" placeholder="Correo" id="Observaciones"  class="border-bottom form-control" name="correoCliente" value="<?php echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo," : "$clienteObj->correo" ;echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo2" : "" ;  ?>">
			</div>
		</div>		
	</div>

	</div>
</div>
<hr>
<div class="row">
	<div class="form-group col-md-3">
		<label for="numeroTecnico" >Placa Vehiculo*</label>
		<input  style="border: none;" readonly type="text" placeholder="Placa" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->placa)) ? "$clienteObj->placa" : "" ; ?>"  >
	</div>
	<div class="form-group col-md-3">
		<label for="Observaciones">Marca *</label>
		<input required <?php echo $retVal = (!empty($clienteObj->marca)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->marca)) ? "$clienteObj->marca" : "" ; ?>" >
	</div>
	<div class="form-group col-md-3">
		<label for="Observaciones">Modelo*</label>
		<input required <?php echo $retVal = (!empty($clienteObj->modelo)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($clienteObj->modelo)) ? "$clienteObj->modelo" : "" ; ?>">
	</div>
	<div class="form-group col-md-3">
		<label for="Observaciones" >Color</label>
		<input <?php echo $retVal = (!empty($clienteObj->color)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Color" id="Observaciones" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->color)) ? "$clienteObj->color" : "" ; ?>">
	</div>
	
</div>
<div class="row">
	<div class="form-group col-md-6">
		<label for="Observaciones" >Chasis*</label>
		<input required <?php echo $retVal = (!empty($clienteObj->chasis)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->chasis)) ? "$clienteObj->chasis" : "$chasis" ; ?>">
	</div>
	<div class="form-group col-md-6">
		<label for="Observaciones" >Motor</label>
		<input <?php echo $retVal = (!empty($clienteObj->motor)) ? "readonly" : "readonly" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="motor" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->motor)) ? "$clienteObj->motor" : "Actualizar" ; ?>">
	</div>
</div>
<div class="row">
	<div class="form-group col-md-12">
		  <label for="seguro" class="">Seleccione Compañía de Seguros</label>
            <select  style="border: none;" class="border-bottom form-control text-capitalize" required name="seguro">
                <option class='text-capitalize' value='' disabled selected >Seleccione Compañía de Seguros</option>
                <option class='text-capitalize' value='No lo se, No recuerdo' >No lo se, No recuerdo</option>
                <option class='text-capitalize' value='No tengo' >No tengo</option>
                <option class='text-capitalize' value='La Positiva' >La Positiva</option>
                <option class='text-capitalize' value='Pacifico Seguros' >Pacifico Seguros</option>
                 <option class='text-capitalize' value='Rimac seguros' >Rimac seguros</option> 
                <option class='text-capitalize' value='Mapfre Peru' >Mapfre Peru</option> 
                <option class='text-capitalize' value='MHDI Seguros' >HDI Seguros</option>
             </select>
	</div>
</div>
<hr>
<div class="row">
	<div class="form-group col-md-12">
		  <label for="idEjecutvio" class="">Seleccione Ejecutivo</label>
            <select  style="border: none;" class="border-bottom form-control text-capitalize" required name="idEjecutvio">
                <option class='text-capitalize' value='' disabled selected >Seleccione Ejecutivo</option>                
                <?php if ($datosUsu=Usuario::obtenerEjecutivosAsignados($session->idusuario)): ?>
                	<?php foreach ($datosUsu as $var): ?>
                	<option class='text-capitalize' value='<?= $var->idusuario ?>' > <?= $var->usuarioNombre." ".$var->usuarioApellidoPaterno;  ?></option>
                	<?php endforeach ?>
                <?php endif ?>                
             </select>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-6">
		 <label for="plan" class="">Plan *</label>
         <select  style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
          <?php if (isset($cotizarObj->plan)): ?>
            <option class="" value="<?php echo $cotizarObj->plan ?>"  selected ><?php echo $cotizarObj->plan ?></option>
          <?php endif ?>
          <option class="" value="" disabled selected >Seleccione Plan *</option>
          <?php Cliente::obtenerPlan() ?>      
        </select> 
	</div>
	<div class="col-md-3">
		<label for="ejecutivoCanal" >Precio lista</label>
		<input  style="border: none;" type="text" placeholder="Precio lista" id="ejecutivoCanal" name="precioLista" class="border-bottom form-control" required >
	</div>
	<div class="col-md-3">
		<label for="time" >Tiempo *</label>
		<input  style="border: none;" type="text" placeholder="Tiempo *" id="time" name="tiempo" class=" tiempo border-bottom form-control" required autocomplete="off" >
	</div>
</div><br>
<?php include("../../includes/campanas.php") ?>
<hr>
<div class="row">
  <div class="form-group col-md-4">
    <label for="tipoCanal" class="">Seleccione canal *</label>
    <select style="border: none;"  name="tipoCanal" class="cd-selec form-control border-bottom" required id="tipoCanal">
      <option value="" disabled selected>Seleccione canal *</option>
      <option value="BROKER">Broker</option>
      <option value="CONCESIONARIO">Concesionario</option>
      <option value="CORPORATIVO">Corporativo</option>      
      <option value="ASEGURADORA">Aseguradora</option>
      <option value="CONVENIO">Convenio</option>
      <option value="BANCO">Banco</option>
      <option value="OTROS">Otros</option>
    </select>   
  </div>
	<div class="col-md-4">
		<label for="Canal" >Canal</label>
		<input  style="border: none;" type="text" placeholder="Canal *" id="Canal" name="canal" class="border-bottom form-control nombreCanal" required autocomplete="off">
	</div>
	<div class="col-md-4">
		<label for="ejecutivoCanal" >Ejecutivo de canal</label>
		<input  style="border: none;" type="text" placeholder="Ejecutivo Canal" id="ejecutivoCanal" name="ejecutivoCanal" class="border-bottom form-control ejecutivoCanalN" required  autocomplete="off">
	</div> 
</div>
<br>

<div class="row">
<h3 class="col-md-6">Datos de facturación </h3>
</div>
<div class="row">
	<div class="col-md-6">
		<label for="Forma*" >Forma</label>
		<select style="border: none;" id="formaPago" name="formaPago" class="cd-selec form-control border-bottom" required >
		<option value="" disabled selected>Datos de pago*</option>
		<option value="Deposito">Deposito</option>
		<option value="OrdenDeCompra">Órden de compra</option>
		<option value="pacificoPandero">Pacífico Pandero</option>
		<option value="pacificoBanbif">Pacífico BanBif</option>
		<option value="Banbif">BanBif</option>
		<option value="PagoEntaler">Págo en taller</option>
	</select>
	</div>
	<div class="col-lg-6">
		<label for="Forma*" >Voucher</label>
			<div class="fileinput fileinput-new input-group" data-provides="fileinput">
				<div class="form-control" data-trigger="fileinput">
					<i class="glyphicon glyphicon-file fileinput-exists"></i>
					<span class="fileinput-filename"></span>
				</div>
				<span class="input-group-addon btn btn-default btn-file">
					<span class="fileinput-new">Seleccionar Voucher de pago*</span>
					<span class="fileinput-exists">Cambiar</span>
					<input type="file" name="voucher"  id="voucherImg" required="" />
				</span>
				<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
			</div> 
		</div>
</div>
<br>
<div class="row">
<div class="col-md-4">
	<select style="border: none;" id="selectNa" name="tipoDocumentoFacturacion" class="cd-selec form-control border-bottom" required >
		<option value="" disabled selected>Datos de facturación*</option>
		<option value="Boleta">Boleta</option>
		<option value="Factura">Factura</option>
	</select>   
</div>
<div class="col-md-8">
	<div class="content-pane e1">
		<h3>Seleccione pago</h3>
	</div>
	<div class="content-pane eBoleta">
		<h3>Continuar</h3>
	</div>
	<div class="content-pane eFactura">
		<div class="row"><br>
			<div class="form-group col-md-4">
				<label for="rucFacturacion" class="">Ruc *</label>
				<input style="border: none;" type="number" placeholder="Ruc *" id="rucFacturacion" name="rucFacturacion" class="form-control x2 border-bottom" maxlength="12" minlength="10" value="<?= $retVal = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>"  >
			</div>
			<div class="form-group col-md-8">
				<label for="razonSocialFacturacion" class="">Razón Social *</label>
				<input style="border: none;" type="text" placeholder="Razón Social *" id="razonSocialFacturacion" name="razonSocialFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ; ?>" >
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-8">
				<label for="direccionFiscalFacturacion" class="sr-only">Dirección Fiscal *</label>
				<input style="border: none;" type="text" placeholder="Dirección Fiscal *" id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="form-control border-bottom text-capitalize" value="<?= $clienteObj->direccion ?>">

			</div>
			<div class="form-group col-md-4">
				<label for="telefonoFacturacion" class="sr-only">Teléfono</label>
				<input style="border: none;" type="text" placeholder="Teléfono" id="telefonoFacturacion" name="telefonoFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">

			</div>
		</div>      
	</div> 
</div>  
</div>
<br>	
<div class="row">
    <div class="col-md-8">
      <label for="entregaEnInstalacion">¿ Cliente desea que se entregue la factura al momento de la instalación ?</label>        
          </div>  
    <div class="col-md-2">
      <div class="switch">
        <div class="onoffswitch">
          <input type="checkbox" class="onoffswitch-checkbox" id="entregaEnInstalacion" value="si" name="entregaEnInstalacion"  onclick="direccionInstacion()" checked="" >
          <label class="onoffswitch-label" for="entregaEnInstalacion">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
          </label>
        </div>
      </div>
    </div>
    <div class="col-md-2"></div>
    </div>    
      <div class="row">
        <div class="form-group col-md-6">
          <label for="direccionEntrega" class="sr-only">Dirección de engrega</label>
          <input style="border: none;" type="text" placeholder="Dirección de engrega" id="direccionEntrega" name="direccionEntrega" class="form-control border-bottom text-capitalize">
        </div>
        <div class="form-group col-md-6">
          <label for="referenciaEntrega" class="sr-only">Referencia de engrega</label>
          <input style="border: none;" type="text" placeholder="Referencia de engrega" id="referenciaEntrega" name="referenciaEntrega" class="form-control border-bottom text-capitalize" required="" >
        </div>
      </div>
      <div class="row">
        <div class="form-group col-md-4">
          <label for="distritoEntrega" class="sr-only">Distrito de entrega</label>
          <input style="border: none;" type="text" placeholder="Distrito de entrega" id="distritoEntrega" name="distritoEntrega" class="form-control border-bottom text-capitalize" required autocomplete="off" >
        </div>
        <div class="form-group col-md-4">
          <label for="horarioEntrega" class="sr-only">Horario de entrega</label>
          <input style="border: none;" type="text" placeholder="Horario de entrega" id="horarioEntrega" name="horarioEntrega" class="form-control border-bottom text-capitalize" autocomplete="off" required >
        </div>
        <div class="form-group col-md-4">
          <label for="personaEntrega" class="sr-only">Persona de recepción de factura </label>
          <input style="border: none;" type="text" placeholder="Persona de recepción de factura" id="personaEntrega" name="personaEntrega" class="form-control border-bottom text-capitalize" required >
        </div>
      </div>
<br>

 <br>
<div class="row">
	<h3 class="col-md-6">Datos de instalación</h3>
	<div class="col-md-6">
      <label for="pendienteInstacion">Pendiente datos de instalación</label>
      <div class="switch">
        <div class="onoffswitch">
          <input type="checkbox"  class="onoffswitch-checkbox" id="pendienteInstacion" value="si" name="pendienteInstacion">
          <label class="onoffswitch-label" for="pendienteInstacion">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
          </label>
        </div>
      </div>
    </div>	
</div>

<div role="tablist">

  <input type="radio" title="taller" name="lugarInstacion" id="taller" role="tab" aria-controls="taller-panel" checked value="Taller" class="lugarDeInstalacion tall" />
  <input type="radio" title="domicilio" name="lugarInstacion" id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente" class="lugarDeInstalacion domm" />
  <input type="radio" title="concesionario" name="lugarInstacion" id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario" class="lugarDeInstalacion conn" >

  <div class="tabs" aria-hidden="true">
    <label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
    <label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
    <label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
  </div>
  <div class="panels">
    <ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">
      <h2>TALLER</h2>
      <p>Av. Paseo de la República 4925 con Angamos Este </p>
      <p>Surquillo, Lima, Lima</p>
      <input type="hidden" name="direccionTaller" value="Av. Paseo de la República 4925 con Angamos Este - Surquillo, Lima, Lima">
      <div class="row">
      <div class="row">
    <div class="form-group col-md-5" id="data_1">
      <!-- <label  class="sr-only">Fecha</label> -->
      <!-- <input type="text" id="datetimepicker3" name="fechaT" required/>     -->
      <div class="row">
        <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control tal tala"  id="fechaIT" name="fechaIT" autocomplete="off" required>
          </div>
        </div>
        <div class="form-group col-md-6">
                      <label  class="">Hora</label>
          <div class="input-group" data-autoclose="true">
              <input type="text" class="form-control tal tala" id="horaIT" name="horaIT" autocomplete="off" required>
              <span class="input-group-addon">
                  <span class="fa fa-clock-o"></span>
              </span>
          </div>
        </div>
      </div>
    </div>  
            <div class="form-group col-md-4">
              <label for="nombreContactoIT" class="">Nombre Contacto *</label>
              <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIT" name="nombreContactoIT" class="form-control x2 text-capitalize border-bottom tal" required="" value="<?= $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ; ?>">
            </div>
            <div class="form-group col-md-3">
            <label for="numeroContactoIT" class="">Número Contacto *</label>
            <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContactoIT" name="numeroContactoIT" class="form-control x2 text-capitalize border-bottom tal" required=""  value="<?= (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">
          </div>
        </div>      
      </div>
    </ul>
    <ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab"> 
      <h2>Domicilio Cliente</h2>
      <div class="row">
        <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionD" class="">Departamento*</label>          
            <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize dom domo" required name="departamentoInstalacionD" id="departamentoInstalacionD">
                <option class="text-capitalize" value="" disabled selected >Seleccione Departamento* </option>
                <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                <?php if ($objDepartamento): ?>
                  <?php foreach ($objDepartamento as $var): ?>
                    <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                  <?php endforeach ?>
                <?php else: ?>
                <?php endif ?>
              </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionD" class="">Provincia*</label>          
            <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize dom domo" required name="provinciaInstalacionD" id="provinciaInstalacionD" >			              
		     </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionD" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize dom domo" required name="distritoInstalacionD"  id="distritoInstalacionD" >
          </select>	
          </div>

        </div>
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="direccionInstalacionD" class="sr-only">Direccion*</label>
            <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionD" name="direccionInstalacionD"  class="form-control border-bottom dom domo" >
          </div>
          <div class="form-group  col-md-6">
            <label for="referenciaInstalacionD" class="sr-only">Referencia *</label>
            <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" required="" name="referenciaInstalacionD" class="form-control border-bottom dom domo"  >
          </div>
        </div>  
        <div class="row">
          <div class="form-group col-md-6">
            <label for="cod" class="sr-only">Cordenadas Domicilio *</label>
            <input style="border: none;" type="text" placeholder="Cordenadas Domicilio *" id="cod" name="cordenadasD" class="form-control x2 text-capitalize border-bottom dom domo" required>
          </div>
          <div class="form-group col-md-6">
            <label for="observacion" class="sr-only">Observaciones *</label>
            <input style="border: none;" type="text" placeholder="Observaciones *" id="observacion" name="observacion" class="form-control x2 text-capitalize border-bottom dom " >
          </div>
        </div>  
         <div class="form-group col-md-5" id="data_1">
                <!-- <label  class="sr-only">Fecha</label> -->
            <!-- <input type="text" id="datetimepicker4" name="fechaD" required/>  -->
      <div class="row">
        <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control dom domo"  id="fechaID" name="fechaID" autocomplete="off" required>
          </div>
        </div>
        <div class="form-group col-md-6">
                      <label  class="">Hora</label>
          <div class="input-group" data-autoclose="true">
              <input type="text" class="form-control dom domo" id="horaID" name="horaID" autocomplete="off" required>
              <span class="input-group-addon">
                  <span class="fa fa-clock-o"></span>
              </span>
          </div>
        </div>
      </div>

         </div> 
        <div class="row">
          <div class="form-group col-md-6">
            <label for="nombreContactoID" class="">Nombre Contacto *</label>
            <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoID" name="nombreContactoID" class="form-control x2 text-capitalize border-bottom dom" required="" value="<?= $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ; ?>">
          </div><div class="form-group col-md-6">
          <label for="numeroContactoID" class="">Número Contacto *</label>
          <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContactoID" name="numeroContactoID" class="form-control x2 text-capitalize border-bottom dom" required="" value="<?= (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>" >
        </div>
      </div>
    </div> 
  </ul>
  <ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">
    
    <div class="row">
      <div class="col-md-6"><h2>Concesionario</h2></div>
      <div class="col-md-6">
        <label for="concesionarioInstalacion" class="sr-only">Nombre de concesionario*</label>
        <input style="border: none;" type="text" placeholder="Nombre de concesionario*" id="concesionarioInstalacion" name="concesionarioInstalacion" class="form-control border-bottom con" required="" >
      </div>
    </div>    

    <div class="row">
      <div class="row">
        <div class="form-group  col-md-4">
          <label for="departamentoInstalacionC" class="">Departamento*</label>         
          <select  style="border: none;" class="departamentoInstalacionC border-bottom form-control text-capitalize con cono" required name="departamentoInstalacionC" id="departamentoInstalacionC">    
	        <option class="text-capitalize" disabled selected >Seleccione Departamento</option>           
	                <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
	                <?php if ($objDepartamento): ?>
	                  <?php foreach ($objDepartamento as $var): ?>
	                    <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
	                  <?php endforeach ?>
	                <?php else: ?>
	                <?php endif ?>				        
	     </select>
        </div>
        <div class="form-group  col-md-4">
          <label for="provinciaInstalacionC" class="">Provincia*</label>
          <select  style="border: none;" class="provinciaInstalacionC border-bottom form-control text-capitalize con cono" required name="provinciaInstalacionC" id="provinciaInstalacionC" >
            </select>
        </div>
        <div class="form-group  col-md-4">
          <label for="distritoInstalacionC" class="">Distrito*</label>
          <select  style="border: none;" class="distritoInstalacionC border-bottom form-control text-capitalize con cono" required name="distritoInstalacionC"  id="distritoInstalacionC" > 		</select>			
        </div>

      </div>
      <div class="row">
        <div class="form-group  col-md-6">
          <label for="direccionInstalacionC" class="">Direccion*</label>
          <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom con cono" >
        </div>
        <div class="form-group  col-md-6">
          <label for="referenciaInstalacionC" class="">Referencia *</label>
          <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom con cono" >
        </div>
      </div>    
      <div class="row">
          <div class="form-group col-md-5" id="data_1">
                <div class="row">
                  <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
                      <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control con cono"  id="fechaIC" name="fechaIC" autocomplete="off" required>
                      </div>
                  </div>
                  <div class="form-group col-md-6">
                                <label  class="">Hora</label>
                    <div class="input-group" data-autoclose="true">
                        <input type="text" class="form-control con cono" id="horaIC" name="horaIC" autocomplete="off" required>
                        <span class="input-group-addon">
                            <span class="fa fa-clock-o"></span>
                        </span>
                    </div>
                  </div>
                </div>
         </div> 
      </div>
      <div class="row">
        <div class="form-group col-md-6">
          <label for="nombreContactoIC" class="">Nombre Contacto *</label>
          <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIC" name="nombreContactoIC" class="form-control x2 text-capitalize border-bottom con" required="" value="<?= $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ; ?>" >
        </div>
        <div class="form-group col-md-6">
        <label for="numeroContactoIC" class="">Número Contacto *</label>
        <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContactoIC" name="numeroContactoIC" class="con form-control x2 text-capitalize border-bottom" required="" value="<?= (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-12">
        <label for="observacion" class="">Observaciones *</label>
        <input style="border: none;" type="text" placeholder="Observaciones *" id="observacion" name="observacion" class="con form-control x2 text-capitalize border-bottom" >
      </div>
    </div>
  </div>
</ul>
</div>
</div>
<br>	
  <div id="programacionHtml"></div>
<div class="row">
  <div class="form-group col-md-12">
    <label for="observaciong" class="">Observaciones </label>
    <input style="border: none;" type="text" placeholder="Observaciones " id="observaciong" name="observaciong" class="form-control x2 text-capitalize border-bottom" >
  </div>
</div>


<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../../assets/js/plugins/select2/select2.full.min.js"></script>
<script src="../../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../../assets/build/jquery.datetimepicker.full.js"></script>
<script src="../../assets/js/plugins/iCheck/icheck.min.js"></script>
<script src="../../assets/js/registrarme.js"></script>
<script src="../../assets/js/validacionFactura.pabel.js"></script>
<?php include_once("../../includes/campanasScript.php") ?>
<script>
function validar(){
	var lugarI=$( "input[name='lugarInstacion']:checked" ).val()
	if (lugarI=='Taller') {
    	$('.con').prop('disabled', true);
    	$('.dom').prop('disabled', true);
    	$('.tal').prop('disabled', false);
	}else if (lugarI=='Domicilio Cliente') {
		$('.con').prop('disabled', true);
    	$('.dom').prop('disabled', false);
    	$('.tal').prop('disabled', true);    	
	}else if (lugarI=='Concesionario') {
		$('.con').prop('disabled', false);
    	$('.dom').prop('disabled', true);
    	$('.tal').prop('disabled', true);
	}
}
 $(document).ready(function(){	
	$( "input[name='lugarInstacion']" ).on( "click",validar);		
	validar();
});

$('#rucFacturacion').on("change",function(){
	ruc=$('#rucFacturacion').val();
	if (ruc.length==11) {

	}else{
		alert("El ruc debe contener 11 caracteres actualmente tiene: "+ruc.length );
	}

	});

</script>
<script>  
$(".departamentoInstalacionD").change(function(){provincias4();});
 $(".departamentoInstalacionC").change(function(){provincias5();});
  $(".provinciaInstalacionD").change(function(){distritos4();});
  $(".provinciaInstalacionC").change(function(){distritos5();});
function provincias4(){
  var iddepartamento = $(".departamentoInstalacionD").val();
  $.ajax({        
    type : 'POST',
    url  : 'ajax/provincia',
    data :  'iddepartamento='+ iddepartamento,
    success : function(data)
    {
      $(".provinciaInstalacionD").html(data);
    }
  });
  return false;
}
function provincias5(){
  var iddepartamento = $(".departamentoInstalacionC").val();
  $.ajax({        
    type : 'POST',
    url  : 'ajax/provincia',
    data :  'iddepartamento='+ iddepartamento,
    success : function(data)
    {
      $(".provinciaInstalacionC").html(data);
    }
  });
  return false;
}

  function distritos4(){
      var idprovincia = $(".provinciaInstalacionD").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionD").html(data)
        }
      });
      return false;
    }
    function distritos5(){
      var idprovincia = $(".provinciaInstalacionC").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionC").html(data)
        }
      });
      return false;
}

$('#formaPago').on("change",function(){
	var formaPagoS=document.getElementById('formaPago');
	value = formaPagoS.options[formaPagoS.selectedIndex].value;
	if (value=='Deposito'||value=='OrdenDeCompra') {		  
  			$('#voucherImg').prop('disabled', false);
  			$('#selectNa').prop('disabled', false);
	}
	if (value=='PagoEntaler'|| value=='pacificoPandero'|| value=='pacificoBanbif'|| value=='Banbif') {
  		$('#voucherImg').prop('disabled', true);
  		if (value=='pacificoPandero'|| value=='pacificoBanbif'|| value=='Banbif') {
  			$('#selectNa').prop('disabled', true);
  		}else{
  			$('#selectNa').prop('disabled', false);
  		}
	}
});

$('#pendienteInstacion').on("change",function(){	
	var pendienteInstacion=document.getElementById("pendienteInstacion");
	if (pendienteInstacion.checked) {
  		$('.cono').prop('disabled', true);
  		// $('.conn').prop('disabled', true);
    	$('.domo').prop('disabled', true);
    	// $('.domm').prop('disabled', true);
    	$('.tala').prop('disabled', true);
    	limpiarCapacidad();
    	// $('.tall').prop('disabled', true);
	}else{
  		$('.cono').prop('disabled', false);
  		// $('.conn').prop('disabled', false);
    	$('.domo').prop('disabled', false);
    	// $('.domm').prop('disabled', false);
    	$('.tala').prop('disabled', false);
    	// $('.tall').prop('disabled', false);
	}
	// value = pendienteInstacion.options[pendienteInstacion.selectedIndex].value;
	// if (value=='Deposito'||value=='OrdenDeCompra') {
	// 	  document.getElementById("voucherImg").removeAttribute("disabled");
	// }
	// if (value=='PagoEntaler') {
	// 	var voucher = document.getElementById("voucherImg"); 
 //  		var att = document.createAttribute("disabled");     
 //  			att.value = "true";
	// 	 voucher.setAttributeNode(att);
	// }
});


$('#selectNa').on("change",function () {
  
var tipoDePagoSeleccionado=document.getElementById('selectNa');
value = tipoDePagoSeleccionado.options[tipoDePagoSeleccionado.selectedIndex].value;
if (value=="Boleta") {

  document.getElementById('rucFacturacion').removeAttribute("required");
  document.getElementById('razonSocialFacturacion').removeAttribute("required");
  document.getElementById('direccionFiscalFacturacion').removeAttribute("required");
  document.getElementById('direccionEntrega').removeAttribute("required");
}
if(value=="Factura"){
ruc=$('#rucFacturacion').val();
	if (ruc.length==11) {

	}else{
		alert("El ruc debe contener 11 caracteres actualmente tiene: "+ruc.length );
	}
	
  var rucFacturacion=document.getElementById('rucFacturacion');
  var razonSocialFacturacion=document.getElementById('razonSocialFacturacion');
  var direccionFiscalFacturacion=document.getElementById('direccionFiscalFacturacion');
  var direccionEntrega=document.getElementById('direccionEntrega');
  var att = document.createAttribute("required");
  var att1 = document.createAttribute("required");
  var att2 = document.createAttribute("required");
  var att3 = document.createAttribute("required");
  att.value = "true";  
  rucFacturacion.setAttributeNode(att);
  razonSocialFacturacion.setAttributeNode(att1);
  direccionFiscalFacturacion.setAttributeNode(att2);
  direccionEntrega.setAttributeNode(att3);
}

  });
  </script>
<script>

<?php $fechaA=date('Y-m-d') ?>
	 jQuery.datetimepicker.setLocale('es');

  $('#fechaIT').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d', 
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
    // allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });
  $('#horaIT').datetimepicker({
    minDate:'<?= $fechaA ?>',
    // inline:true,
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });

  $('#fechaIC').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d', 
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
  });   
  $('#horaIC').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });


   $('#fechaID').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d', 
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
  });
  $('#horaID').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });

   $('#fechaEngrega').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d', 
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
  });
  $('#horaEntrega').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });

<?php if($canales=Canales::verNombreCanal()): ?>
	$('.nombreCanal').typeahead({
	source: [
	<?php foreach ($canales as $var):?>
	{"name": "<?php echo $var->nombreCanal ?>"},        
	<?php endforeach ?>
	]
	}); 
<?php endif ?>

<?php if($canales1=Canales::verEjecutiVodeCanal()): ?>
	$('.ejecutivoCanalN').typeahead({
	source: [
	<?php foreach ($canales1 as $var):?>
	{"name": "<?php echo $var->ejecutivoCanal ?>"},        
	<?php endforeach ?>
	]
	}); 
<?php endif ?>
$('.tiempo').typeahead({
    source: [
    {"name": "1 año"},
    {"name": "2 años"},
    {"name": "3 años"},
    {"name": "4 años"},
    {"name": "5 años"},
    {"name": "6 años"},
    {"name": "8 años"},
    {"name": "7 años"}
    ]
  });

// entregaEnInstalacion
var entregaEnInstalacion=document.getElementById("entregaEnInstalacion");
  if (entregaEnInstalacion.checked) {
     direccionInstacion();
  }
<?php if ($datos=DatosFacturacion::horariosSugeridosParaEntregaDeFactura()): ?>
$('#horarioEntrega').typeahead({
  source: [
    <?php foreach ($datos as $var): ?>
      {"name": "<?php echo $var->fecha ?>"},  
    <?php endforeach ?>
    ]
  }); 
<?php endif ?>

<?php if ($datosD=Cliente::otenerTodoLosDistritos()): ?>
$('#distritoEntrega').typeahead({
  source: [
    <?php foreach ($datosD as $var): ?>
      {"name": "<?php echo $var->distrito ?>"},  
    <?php endforeach ?>
    ]
  }); 
<?php endif ?>

<?php if ($datosDopeCon=Operacion::obtenerConcesionarioInstalacion()): ?>
$('#concesionarioInstalacion').typeahead({
  source: [
    <?php foreach ($datosDopeCon as $var): ?>
      {"name": "<?php echo $var->nombreConcecionarioInstalicion ?>"},  
    <?php endforeach ?>
    ]
  }); 
<?php endif ?>
  
 $("#horaIT").bind("change ",function()
    { 
      var hora = $(this).val(); 
      var fecha= $("#fechaIT").val();     
      fecha=fecha+' '+hora;      
      $.ajax({
        type : 'POST',
        url  : '../includes/programacionAjax',
        data : {fecha:fecha},
        success : function(data)
        {
         $("#programacionHtml").html(data);
        }
      });      
      return false;
    });
 $("#fechaIT").bind("change",function()
    { 
      var fecha = $(this).val(); 
      var hora= $("#horaIT").val();     
      fecha=fecha+' '+hora;
      if (hora!='') {
        $.ajax({
          type : 'POST',
          url  : '../includes/programacionAjax',
          data : {fecha:fecha},
          success : function(data)
          {
           $("#programacionHtml").html(data);
          }
        });
      }      
      return false;
    });
  
 $("#horaIC").bind("change ",function()
    { 
      var horaIC = $(this).val(); 
      var fechaIC= $("#horaIC").val();     
      fechaIC=fechaIC+' '+horaIC;      
      $.ajax({
        type : 'POST',
        url  : '../includes/programacionAjax',
        data : {fecha:fechaIC},
        success : function(data)
        {
         $("#programacionHtml").html(data);
      	limpiarCapacidad();
        }
      });
      return false;

    });
 $("#horaIC").bind("change",function()
    { 
      var fechaIC = $(this).val(); 
      var horaIC= $("#horaIC").val();     
      fechaIC=fechaIC+' '+horaIC;
      if (horaIC!='') {
        $.ajax({
          type : 'POST',
          url  : '../includes/programacionAjax',
          data : {fecha:fechaIC},
          success : function(data)
          {
           $("#programacionHtml").html(data);
      		limpiarCapacidad();   
          }
        });
      }   
      return false;
    });
  
 $("#horaID").bind("change ",function()
    { 
      var horaID = $(this).val(); 
      var fechaID= $("#fechaID").val();     
      fechaID=fechaID+' '+horaID;      
      $.ajax({
        type : 'POST',
        url  : '../includes/programacionAjax',
        data : {fecha:fechaID},
        success : function(data)
        {
         $("#programacionHtml").html(data);
      	limpiarCapacidad();
        }
      });
      return false;

    });
 $("#fechaID").bind("change",function()
    { 
      var fechaID = $(this).val(); 
      var horaID= $("#horaID").val();     
      fechaID=fechaID+' '+horaID;
      if (horaID!='') {
        $.ajax({
          type : 'POST',
          url  : '../includes/programacionAjax',
          data : {fecha:fechaID},
          success : function(data)
          {
           $("#programacionHtml").html(data);
      	limpiarCapacidad();   
          }
        });
      }   
      return false;
    });

 function esconder(){  
  $("#programacionHtml").html("");
 }


</script>

<?php endif ?>

<?php
} 
?>
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<!-- ////////////////////////////// -->
<?php if (isset($_POST['idperacion'])){

?>
<?php 
if ($_POST['idperacion']) {
  include_once("../../includes/init.php");
  $idOperacion=htmlspecialchars(htmlentities($_POST['idperacion']));
  $clienteObjModal=new Cliente;
  $ObjetoOperacion=Operacion::obtenerOperacionPorId($idOperacion);
  if ($ObjetoOperacion->idlocation==0) {
    $vehiculoObj=Vehiculo::obtenerVehiculo($ObjetoOperacion->idvehiculo);
    $clienteObj=Cliente::obtenerClientePorVehiculo($ObjetoOperacion->idvehiculo,$ObjetoOperacion->idcliente);  
  }else{
    $clienteObj=Onix::obtenerDatosDesdeIds($ObjetoOperacion->idcliente,$ObjetoOperacion->idvehiculo,$ObjetoOperacion->idlocation) ;
    $chequeoObj=Chequeos::obtenerChequeoTrackPorId($ObjetoOperacion->idChequeo);
    $vehiculoObj=false;
  }
  ?>
  <?php if ($ObjetoOperacion->estado=='36'||$ObjetoOperacion->estado=='14'){ ?>
  <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
  <h2 class="text-center"><?php echo $ObjetoOperacion->trabajo ?></h2>
  <input type="hidden" name="trabajoO" value="<?= $ObjetoOperacion->trabajo  ?>" >
  <?php if ($ObjetoOperacion->idChequeo!=0): ?>
    <h3 id="mot" class="text-center text-danger">MOTIVO: <?= $chequeoObj->motivo ?></h3>
    <h3 id="seguimiento" class="text-center text-danger">Seguimiento realizado: <?= $chequeoObj->seguimientoRealizado ?></h3>
  <?php endif ?>

<?php if ($vehiculoObj): ?>

<?php if (!empty($vehiculoObj->serviciosAdicionales)): ?>
 <div class="row">
   <div class="form-group col-md-12">
     Servicios adicionales: <?php echo $vehiculoObj->serviciosAdicionales ?>
   </div>
 </div>
<?php endif ?>


</div> 
<!-- ORDENES INSTALACIONES FIN END END END END  --> 
  <div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>

        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->dni ?>">       
      <?php else: ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->ruc ?>">        
      <?php endif ?>  
    </div>

    <div class="col-md-9">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->razonSocial ?>">       
      <?php endif ?>
    </div> 
    </div> 
<hr>
<h3>Datos del vehiculo</h3>
  <div class="row"><hr>
    <div class="col-lg-12 field_wrapper"> 
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="field_name[0][]" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $vehiculoObj->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="">Chasis</label>
          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="field_name[1][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->chasis ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="motor" class="">Motor</label>
          <input style="border: none;" type="text" placeholder="Motor" id="motor" name="field_name[2][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->motor ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="marca" class="">Marca</label>
          <input style="border: none;" type="text" placeholder="Marca" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $vehiculoObj->marca ?>" >
        </div>
      </div> 
      <div class="row">
        <div class="form-group col-md-3">
          <label for="modelo" class="">Modelo</label>
          <input style="border: none;" type="text" placeholder="Modelo" id="modelo" name="field_name[4][]" class="border-bottom form-control text-capitalize"  required  value="<?php echo $vehiculoObj->modelo ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="color" class="">Color</label>
          <input style="border: none;" type="text" placeholder="Color" id="color" name="field_name[5][]" class="border-bottom form-control x2 text-capitalize" required value="<?php echo $vehiculoObj->color ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="anio" class="">Año</label>
          <input style="border: none;" type="text" placeholder="Año" id="anio" name="field_name[6][]" class="border-bottom form-control" required value="<?php echo $vehiculoObj->anio ?>" >
        </div>
        <div class="form-group col-md-3">
           <label for="plan" class="">Plan *</label>
           <select  style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
              <option class="" value="<?php echo $vehiculoObj->plan ?>"  selected ><?php echo $vehiculoObj->plan ?></option>      
          </select> 
        </div>
      </div>     
    </div>
  </div> 
<hr>

<h2 class="text-center">Datos de instalacíon</h2>
<div class="row">
  <div class="form-group col-md-6">
    <label for="tecnico" class="">Tecnicos</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="tecnico">
      <?php if ($ObjetoOperacion->idTecnicoAsignado!=0): ?>
        <?php 
          $usuarioDato=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoAsignado);
         ?>
      <option class="text-capitalize" value="<?= $ObjetoOperacion->idTecnicoAsignado ?>" selected ><?= $usuarioDato->usuarioNombre ?> <?= $usuarioDato->usuarioApellidoPaterno ?></option>
      <?php else: ?>
      <option class="text-capitalize" value="" disabled selected >Seleccione técnico</option>
      <?php endif ?>
      <?php Usuario::obtenerTecnicos() ?>            
    </select>
  </div>
  <div class="form-group col-md-6">
    <label for="orte" class="">Numero de Orte</label>
    <input style="border: none;" type="text" placeholder="Numero de Orte" id="orte" name="orte" class="border-bottom form-control x2 " required  value="<?= $ObjetoOperacion->numeroOrte ?>">
  </div>
</div> 
<?php if ($ObjetoOperacion->lugarInstalacion=="Domicilio"): ?>
<div class="row">
   <div class="col-md-3"><h3>Coordenadas de instalación --></h3></div>
   <div class="col-md-6">
    <input style="border: none;" type="text" placeholder="Coordenadas insalaicon" id="orden" name="orden" class="border-bottom form-control x2 " value="<?php echo $ObjetoOperacion->coordenadasInstalacion ?>" >
  </div>
</div>
<?php endif ?>
<hr>
<h4>Lugar de instalación <?php echo $ObjetoOperacion->lugarInstalacion ?> <?php echo $retVal = (!empty($ObjetoOperacion->nombreConcecionarioInstalicion)) ? $ObjetoOperacion->nombreConcecionarioInstalicion : "" ; ?>  del dia <?= formatoHaceEn($ObjetoOperacion->fechaInstalacion) ?></h4>
<hr>

<input type="hidden" name="idoperacion" value="<?php echo $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idvh" value="<?php echo $vehiculoObj->idvehiculo ?>">
<input type="hidden" name="datosfacturacionid" value="<?php echo $ObjetoOperacion->datosfacturacionid ?>">
<div class="row">
  <div class="form-group  col-md-4">
    <label for="departamentoInstalacionD" class="">Departamento*</label>
    <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize" required name="departamentoInstalacionD" id="departamentoInstalacionD">
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>" disabled selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
      <?php if ($objDepartamento): ?>
        <?php foreach ($objDepartamento as $var): ?>
          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
        <?php endforeach ?>
      <?php else: ?>
      <?php endif ?>
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="provinciaInstalacionD" class="">Provincia*</label>
    <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>" disabled selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="distritoInstalacionD" class="">Distrito*</label>
    <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize" required name="distritoInstalacionD"  id="distritoInstalacionD" >
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>" disabled selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
    </select>           
  </div>
</div>
<div class="row">          
  <div class="form-group col-md-3">
    <label for="lugar" class="">Lugar</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="lugar">
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->lugarInstalacion ?>"  selected ><?php echo $ObjetoOperacion->lugarInstalacion ?></option>
      <option class="text-capitalize" value="Taller" >Taller</option>
      <option class="text-capitalize" value="Domicilio Cliente" >Domicilio Cliente</option>
      <option class="text-capitalize" value="Concesionario" >Concesionario</option>
    </select>
  </div>
  <div class="form-group col-md-9">
    <label for="direccionInstalacion" class="">Dirección Instalación </label>
    <input style="border: none;" type="text" placeholder="Direccion Instalcion" id="direccionInstalacion" name="direccionInstalacion" class="border-bottom form-control" required value="<?php echo $ObjetoOperacion->direccionInstacion ?>">
  </div>
</div>
<?php if ($ObjetoOperacion->coordenadasInstalacion != ''): ?>
  <div class="row"><div class="col-lg-12"><label for="cordenadas">Coordenadas de instalacion: </label><label > <?php echo $ObjetoOperacion->coordenadasInstalacion ?></label><br><h1 class="text-success"><a  target="_blank" href="mapa.php?coordenadas=<?php echo $ObjetoOperacion->coordenadasInstalacion ?>&lugar=<?php echo $ObjetoOperacion->direccionInstacion ?>">ver mapa</a></h1></div></div>    
<?php endif ?>

<div class="row">
  <div class="form-group col-md-5" id="data_1">
    <!-- <label  class="sr-only">Fecha</label>
    <input type="text" id="datetimepicker3" name="fecha" required/> -->      
    <div class="row">
      <div class="form-group col-md-6">   
        <label>Fecha</label>
        <div class="input-group date">
          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control"  id="fechaCh" name="fecha" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10) ?>" >
        </div>
      </div>
      <div class="form-group col-md-6">
        <label>Hora</label>
        <div class="input-group" data-autoclose="true">
          <input type="text" class="form-control" id="horaCh" name="hora" autocomplete="off" required value="<?=  substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>"> 
          <span class="input-group-addon">
            <span class="fa fa-clock-o"></span>
          </span>
        </div>
      </div>
    </div> 
  </div> 
  <div class="form-group col-md-4">
     <label for="personaContacto" class="">Persona contacto </label>
    <input style="border: none;" type="text" placeholder="Direccion Instalcion" id="personaContacto" name="personaContacto" class="border-bottom form-control" required value="<?php echo $ObjetoOperacion->nombreContacto ?>">
  </div>
  <div class="form-group col-md-3">
     <label for="numeroContacto" class="">Número contacto </label>
    <input style="border: none;" type="text" placeholder="Número contacto" id="numeroContacto" name="numeroContacto" class="border-bottom form-control" required value="<?php echo $ObjetoOperacion->numeroContacto ?>">
  </div>
</div>
<?php else: ?>
  <div class="row">
        <div class="form-group col-md-3">
        <label for="Observaciones" > Usuario Web</label>
        <input name="usuarioWeb" style="border: none;" type="text" placeholder="Web" id="Observaciones" <?php echo $retVal = (!empty($clienteObj->dni)) ? "readonly" : "" ; ?> class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>">
      </div>
      <div class="form-group col-md-5">
      <?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
        <label for="numeroTecnico" >Nombre de cliente*</label>
        <input  style="border: none;" type="text" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ; ?> ">
      </div>
      <div class="col-md-2"><label for="estado" >Celular</label>
        <input style="border: none;" type="text" <?php echo $retVal = (!empty($clienteObj->celular)) ? "readonly" : "" ; ?> id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">
      </div>
      <div class="col-md-2"><label for="estado" >Celular</label>
        <input style="border: none;" type="text" readonly id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular2)) ? "$clienteObj->celular2" : "" ; ?>">
      </div>
    </div>
     </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="form-group col-md-3">
    <label for="numeroTecnico" >Placa Vehiculo*</label>
    <input  style="border: none;" readonly type="text" placeholder="Placa" id="numeroTecnico" name="placa" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->placa)) ? "$clienteObj->placa" : "" ; ?>"   >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Marca *</label>
    <input required <?php echo $retVal = (!empty($clienteObj->marca)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->marca)) ? "$clienteObj->marca" : "" ; ?>" >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Modelo*</label>
    <input  required <?php echo $retVal = (!empty($clienteObj->modelo)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Modelo" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($clienteObj->modelo)) ? "$clienteObj->modelo" : "" ; ?>">
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Color</label>
    <input <?php echo $retVal = (!empty($clienteObj->color)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Color" id="Observaciones" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->color)) ? "$clienteObj->color" : "" ; ?>">
  </div>  
</div>

<span class="ocultarThis">
<div class="row">
  <div class="form-group col-md-4">
    <label for="Observaciones"  >Chasis*</label>
    <input required <?php echo $retVal = (!empty($clienteObj->chasis)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->chasis)) ? "$clienteObj->chasis" : "" ; ?>">
  </div>
  <div class="form-group col-md-4">
    <label for="Observaciones" >Motor</label>
    <input <?php echo $retVal = (!empty($clienteObj->motor)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="motor" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->motor)) ? "$clienteObj->motor" : "Actualizar" ; ?>"  >
  </div>
  <div class="form-group col-md-4">
    <label for="Observaciones" >Plan</label>
    <input  style="border: none;" type="text" placeholder="Observaciones" readonly id="Observaciones" name="plan" class="border-bottom form-control" value="<?= $chequeoObj->plan ?>"  >
  </div>
</div>
<input type="hidden" name="idoperacion" value="<?= $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idChequeo" value="<?= $ObjetoOperacion->idChequeo ?>">
  <?php if ($ObjetoOperacion->trabajo=='Chequeo en linea'): ?>
    <div class="row"> 
  <div class="form-group col-md-12">
    <label for="Observaciones" >Número de orden</label>
    <input  style="border: none;" type="text" placeholder="Número de orden" id="numeroOrden" name="numeroOrden" class="border-bottom form-control"  >
    <input type="hidden" name="chequeo" value="ok">
    <input type="hidden" name="chequeoLinea" value="ok">

  </div>
</div>
  <?php endif ?>
<div class="row">
  <div class="form-group col-md-6">
    <label for="numeroTecnico" >Persona de contacto</label>
    <input  style="border: none;" readonly type="text" placeholder="Persona de contacto" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?= $chequeoObj->personaContacto ?>"   >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Numero contacto *</label>
    <input required readonly="" style="border: none;" type="text" placeholder="Numero contacto *" id="Observaciones" name="numcontacto" class="border-bottom form-control"  value="<?= $chequeoObj->numeroContacto ?>" >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Ubicacion de sistema*</label>
    <input  required readonly style="border: none;" type="text" placeholder="Ubicacion de sistema" id="Observaciones" name="ubsistema" class="border-bottom form-control"   value="<?= $chequeoObj->ubicacionSistema ?>">
  </div>
  </div>
  <div class="row">
  <div class="form-group col-md-2">
    <label for="numeroTecnico" >Lugar</label>
    <input  style="border: none;" readonly type="text" placeholder="Persona de contacto" id="numeroTecnico" name="lugar" class="border-bottom form-control" required value="<?= $chequeoObj->lugar ?>"   >
  </div>
  <div class="form-group col-md-5">
    <label for="Observaciones" >Direccion *</label> <a target="_blank" href="mapa.php?coordenadas=<?= $chequeoObj->coordenadas ?>&lugar=<?= $chequeoObj->direccion ?>" class="text-center">Ver mapa</a>
    <input required readonly="" style="border: none;" type="text" placeholder="Numero contacto *" id="Observaciones" name="dir" class="border-bottom form-control"  value="<?= $chequeoObj->direccion ?>" >
  </div>
  <div class="form-group col-md-5">
    <label for="Observaciones" >Referencia *</label>
    <input  required readonly style="border: none;" type="text" placeholder="Ubicacion de sistema" id="Observaciones" name="ref" class="border-bottom form-control"   value="<?= $chequeoObj->referencia ?>">
  </div>  
</div>  
  <div class="row">
  <div class="form-group col-md-5">
    <label for="numeroTecnico" >Fecha instalacion</label>
    <input  style="border: none;" readonly type="text" placeholder="Persona de contacto" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?= $chequeoObj->fechaInstalacion ?>" >
  </div>
  <div class="form-group col-md-7">
    <label for="Observaciones" >Tecnico instalador *</label>
    <input required readonly="" style="border: none;" type="text" placeholder="Numero contacto *" id="Observaciones" name="tei" class="border-bottom form-control"  value="<?= $chequeoObj->instalador ?>">
  </div>
  <?php 
      $chequeoObj->fechaChequeoEnSac;
      $fecha=date("Y-m-d",strtotime($chequeoObj->fechaChequeoEnSac));
      $hora=date("H:i",strtotime($chequeoObj->fechaChequeoEnSac));

   ?>
    <div class="row">
    <h4 class="text-center">hora y fecha estimada de instalación: <?= formatoHaceEn($chequeoObj->fechaChequeoEnSac) ?></h4>
  </div>
  
</div>
  <div class="row"> 
  <div class="form-group col-md-6">
     <label for="tecnico" class="">Tecnicos</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="tecnico">
    <?php if ($ObjetoOperacion->idTecnicoAsignado!=0): ?>
        <?php 
          $usuarioDato=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoAsignado);
         ?>
      <option class="text-capitalize" value="<?= $ObjetoOperacion->idTecnicoAsignado ?>" selected ><?= $usuarioDato->usuarioNombre ?> <?= $usuarioDato->usuarioApellidoPaterno ?></option>
      <?php else: ?>
      <option class="text-capitalize" value="" disabled selected >Seleccione técnico</option>
      <?php endif ?>    
      <?php Usuario::obtenerTecnicos() ?>            
    </select>
  </div>
  <div class="form-group col-md-6">
    <label for="numeroOrte" >Número de orte</label>
    <input  style="border: none;" type="text" placeholder="Número de orte" id="numeroOrte" name="numeroOrte" class="border-bottom form-control"  required="" value="<?= $ObjetoOperacion->numeroOrte  ?>">
  </div>
</div>
<div class="row">
  <div class="col-md-6 form-group"><label for="Observaciones" >Fecha *</label>
    <input required  style="border: none;" type="text" placeholder="" id="fechaCh" name="fecha" class="border-bottom form-control"  value="<?= $fecha ?>" ></div>
  <div class="col-md-6 form-group"><label for="Observaciones" >Hora *</label>
    <input required  style="border: none;" type="text" placeholder=" *" id="horaCh" name="hora" class="border-bottom form-control"  value="<?= $hora ?>" ></div>
</div>
  
  
<?php //en los dos tipos de chequeo que queda en SAC confirman orden orte fecha y lugar  ?>
</span>
<?php endif ?>
<?php }else{ ?>
<div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>

        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->dni ?>">       
      <?php else: ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->ruc ?>">        
      <?php endif ?>  
    </div>

    <div class="col-md-9">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->razonSocial ?>">       
      <?php endif ?>
    </div> 
    </div> 
<hr>
<h3>Datos del vehiculo</h3>
  <div class="row"><hr>
    <div class="col-lg-12 field_wrapper"> 
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="field_name[0][]" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $vehiculoObj->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="">Chasis</label>
          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="field_name[1][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->chasis ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="motor" class="">Motor</label>
          <input style="border: none;" type="text" placeholder="Motor" id="motor" name="field_name[2][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->motor ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="marca" class="">Marca</label>
          <input style="border: none;" type="text" placeholder="Marca" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $vehiculoObj->marca ?>" >
        </div>
      </div>
      </div>
  </div>
<h2 class="text-center">Datos de instalacíon</h2>
<div class="row">
  <div class="form-group col-md-6">
    <label for="tecnico" class="">Tecnicos</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="tecnico" disabled>
      <?php if ($ObjetoOperacion->idTecnicoAsignado!=0): ?>
        <?php 
          $usuarioDato=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoAsignado);
         ?>
      <option class="text-capitalize" value="<?= $ObjetoOperacion->idTecnicoAsignado ?>" selected ><?= $usuarioDato->usuarioNombre ?> <?= $usuarioDato->usuarioApellidoPaterno ?></option>
      <?php else: ?>
      <option class="text-capitalize" value="" disabled selected >Seleccione técnico</option>
      <?php endif ?>
      <?php Usuario::obtenerTecnicos() ?>            
    </select>
  </div>
  <div class="form-group col-md-6">
    <label for="orte" class="">Numero de Orte</label>
    <input style="border: none;" type="text" placeholder="Numero de Orte" id="orte" name="orte" class="border-bottom form-control x2 " required  value="<?= $ObjetoOperacion->numeroOrte ?>">
  </div>
</div> 
<?php if ($ObjetoOperacion->lugarInstalacion=="Domicilio"): ?>
<div class="row">
   <div class="col-md-3"><h3>Coordenadas de instalación --></h3></div>
   <div class="col-md-6">
    <input style="border: none;" type="text" placeholder="Coordenadas insalaicon" id="orden" name="orden" class="border-bottom form-control x2 " value="<?php echo $ObjetoOperacion->coordenadasInstalacion ?>" >
  </div>
</div>
<?php endif ?>
<hr>
<h4>Lugar de instalación <?php echo $ObjetoOperacion->lugarInstalacion ?> <?php echo $retVal = (!empty($ObjetoOperacion->nombreConcecionarioInstalicion)) ? $ObjetoOperacion->nombreConcecionarioInstalicion : "" ; ?>  del dia <?= formatoHaceEn($ObjetoOperacion->fechaInstalacion) ?></h4>
<hr>

<input type="hidden" name="idoperacion" value="<?php echo $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idvh" value="<?php echo $vehiculoObj->idvehiculo ?>">
<input type="hidden" name="datosfacturacionid" value="<?php echo $ObjetoOperacion->datosfacturacionid ?>">
<div class="row">
  <div class="form-group  col-md-4">
    <label for="departamentoInstalacionD" class="">Departamento*</label>
    <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize" required name="departamentoInstalacionD" id="departamentoInstalacionD">
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>" disabled selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
      <?php if ($objDepartamento): ?>
        <?php foreach ($objDepartamento as $var): ?>
          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
        <?php endforeach ?>
      <?php else: ?>
      <?php endif ?>
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="provinciaInstalacionD" class="">Provincia*</label>
    <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>" disabled selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="distritoInstalacionD" class="">Distrito*</label>
    <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize" required name="distritoInstalacionD"  id="distritoInstalacionD" >
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>" disabled selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
    </select>           
  </div>
</div>
<div class="row">          
  <div class="form-group col-md-3">
    <label for="lugar" class="">Lugar</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="lugar">
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->lugarInstalacion ?>"  selected ><?php echo $ObjetoOperacion->lugarInstalacion ?></option>
      <option class="text-capitalize" value="Taller" >Taller</option>
      <option class="text-capitalize" value="Domicilio Cliente" >Domicilio Cliente</option>
      <option class="text-capitalize" value="Concesionario" >Concesionario</option>
    </select>
  </div>
  <div class="form-group col-md-9">
    <label for="direccionInstalacion" class="">Dirección Instalación </label>
    <input style="border: none;" type="text" placeholder="Direccion Instalcion" id="direccionInstalacion" name="direccionInstalacion" class="border-bottom form-control" required value="<?php echo $ObjetoOperacion->direccionInstacion ?>">
  </div>
</div>
<?php if ($ObjetoOperacion->coordenadasInstalacion != ''): ?>
  <div class="row"><div class="col-lg-12"><label for="cordenadas">Coordenadas de instalacion: </label><label > <?php echo $ObjetoOperacion->coordenadasInstalacion ?></label><br><h1 class="text-success"><a  target="_blank" href="mapa.php?coordenadas=<?php echo $ObjetoOperacion->coordenadasInstalacion ?>&lugar=<?php echo $ObjetoOperacion->direccionInstacion ?>">ver mapa</a></h1></div></div>    
<?php endif ?>

<div class="row">
  <div class="form-group col-md-5" id="data_1">
    <!-- <label  class="sr-only">Fecha</label>
    <input type="text" id="datetimepicker3" name="fecha" required/> -->      
    <div class="row">
      <div class="form-group col-md-6">   
        <label>Fecha</label>
        <div class="input-group date">
          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control"  id="fechaCh" name="fecha" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10) ?>" >
        </div>
      </div>
      <div class="form-group col-md-6">
        <label>Hora</label>
        <div class="input-group" data-autoclose="true">
          <input type="text" class="form-control" id="horaCh" name="hora" autocomplete="off" required value="<?=  substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>"> 
          <span class="input-group-addon">
            <span class="fa fa-clock-o"></span>
          </span>
        </div>
      </div>
    </div> 
  </div> 
  <div class="form-group col-md-4">
     <label for="personaContacto" class="">Persona contacto </label>
    <input style="border: none;" type="text" placeholder="Direccion Instalcion" id="personaContacto" name="personaContacto" class="border-bottom form-control" required value="<?php echo $ObjetoOperacion->nombreContacto ?>">
  </div>
  <div class="form-group col-md-3">
     <label for="numeroContacto" class="">Número contacto </label>
    <input style="border: none;" type="text" placeholder="Número contacto" id="numeroContacto" name="numeroContacto" class="border-bottom form-control" required value="<?php echo $ObjetoOperacion->numeroContacto ?>">
  </div>
</div>
  <div class="row">
       <div class="form-group col-md-4">
          <label for="placa" class="">Tecnico Asignado</label>
          <?php $usuarioAsgi=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoAsignado) ?>
          <input style="border: none;" type="text" placeholder="Tecnico Asignado" id="placa" name="" class="border-bottom form-control x2 text-uppercase" required value="<?= $usuarioAsgi->usuarioNombre.' '.$usuarioAsgi->usuarioApellidoPaterno ?>">
        </div>
       <div class="form-group col-md-4">
       <?php $usuarioInst=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoInstalador) ?>
          <label for="placa" class="">Tecnico instalador</label>
          <input style="border: none;" type="text" placeholder="Tecnico instalador" id="placa" name="" class="border-bottom form-control x2 text-uppercase" required value="<?= $usuarioInst->usuarioNombre.' '.$usuarioInst->usuarioApellidoPaterno ?>">
        </div>
       <div class="form-group col-md-4">
       <?php $usuarioCentr=Usuario::obtenerUsuario($ObjetoOperacion->idusuarioActivador) ?>
          <label for="placa" class="">Usuario cetral Hora</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="" class="border-bottom form-control x2 text-uppercase" required value="<?= $usuarioCentr->usuarioNombre.' '.$usuarioCentr->usuarioApellidoPaterno ?>">
          <br><small class="text-danger">hora <?= haceMinimo($ObjetoOperacion->fechaActivacion) ?></small>
        </div>
  </div>
<?php } ?>
<?php
} 
?> 
<script src="../../assets/build/jquery.datetimepicker.full.js"></script>
<script>  
<?php $fechaA=date('Y-m-d') ?>
  jQuery.datetimepicker.setLocale('es');
  $('#datetimepicker3').datetimepicker({
    minDate:'<?= $fechaA ?>',
    inline:true,
    scrollInput: false,
    scrollMonth : false,
    allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']

  }); 
  $('#fechaCh').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d',
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
  });
  $('#horaCh').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });

</script>

<?php


	} ?>




<?php
require_once("../../includes/init.php");

if (isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    if (isset($_POST["page"])) {
        $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
        if (!is_numeric($numeroDePagina)) {
            die('Numero de pagina incorrecta !');
        } //incase of invalid page number
    } else {
        $numeroDePagina = 1; //if there's no page number, set it to 1
    }
    $idusuario = $session->idusuario;

    $objTrama = new WebTrack();

    $cantidadO = $objTrama->obtenerCantidadTramaIntegracionPag();
    $n = 1;
    $registrosPorPagina = 370;
    $paginasEnTotal = ceil($cantidadO->cantidad / $registrosPorPagina);
    $posisionDePagina = (($numeroDePagina - 1) * $registrosPorPagina);
    $datos = $objTrama->obtenerTramaIntegracionPag($posisionDePagina, $registrosPorPagina);
    print_r($datos);
    if ($datos):
        foreach ($datos as $var): ?>
        <?php print_r($var->numero_documento) ?>
            <tr class=''>
                <td><?= $n++ ?></td>
                <td class="center"><?= $var->fecha ?></td>
                <td class="center"><?= $var->numero_documento ?></td>
                <td class="center"><?= ($var->nombre . " " .$var->apellido) ?></td>
                <td class="center"><?= $var->direccion ?></td>
                <?php if(!is_null($var->idsuariocargo)){ $usuarioObj=Usuario::obtenerUsuario($var->idsuariocargo); }   ?>
                <td class="center"  style="<?php if(!is_null($var->idsuariocargo)){ echo 'background-color: #86d7fd'; }else{ echo 'background-color: #8befda';} ?>"><?php if(!is_null($var->idsuariocargo)){echo $usuarioObj->usuarioNombre.' '.$usuarioObj->usuarioApellidoPaterno;}else{ echo 'Libre';}?></td>
                <td class="center ">
                    <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0" data-toggle="modal"
                            data-target="#bateriasOModal" onclick="limpiarTrama()" data-idSolRadar="<?= $var->codigo ?>">
                        Elegir <i class="fa fa-cog"> </i></button>
                </td>
            </tr>
        <?php
        endforeach;
        ?>
        <tr>
            <td colspan="2">
                Mostrando <?= $retVal = ($cantidadO->cantidad < $registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina; ?>
                de <?= $cantidadO->cantidad ?></td>
            <td colspan="9">
                <?= paginate_function($registrosPorPagina, $numeroDePagina, $cantidadO->cantidad, $paginasEnTotal) ?>
            </td>
        </tr>
    <?php
    else:
        ?>
        <tr>
            <td colspan="8" class="text-center">No se encontro registro alguno</td>
        </tr>
    <?php
    endif;
}

<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} 
  }else{
    $numeroDePagina = 1;
  }
  $fecha='';
  // $idusuario=htmlspecialchars($_POST['convenio']);
  if (isset($_POST["fecha"])) {
    $fecha=strip_tags($_POST['fecha']);
    $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
    $desde=date("YmdHis",strtotime($desde));  
    $hasta=date("Ymd",strtotime($hasta)); 
    $hasta=$hasta."235959";
  }
  $datosFacObj=new DatosFacturacion;
  // $datosFacObj->idConfirmacionOPE=limpiar($idusuario);
  if (empty($fecha)){    
    $cantidadO=$datosFacObj->obtenerDatosParaCOMHCantidad();    
  }else{
    $cantidadO=$datosFacObj->obtenerDatosParaCOMHCantidadRANGO($desde,$hasta);
  }  
  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  $n=1;
  if (empty($fecha)) {
    $datos=$datosFacObj->obtenerDatosParaCOMH($posisionDePagina,$registrosPorPagina);    
  } else {
    $datos=$datosFacObj->obtenerDatosParaCOMHRango($posisionDePagina,$registrosPorPagina,$desde,$hasta);
  }  
?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>  
    <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculos) ?>
    <?php $opeObj=Operacion::obtenereOperacionesPorIdFacturacion($var->datosfacturacionid) ?> 
  <tr <?= obtenerClass($var->lugarInstalacion) ?>>
    <td><?= $n++ ?></td>
<td><?= ($clienteObj->tipoCliente=="Natural") ? $clienteObj->apellidosC." ".$clienteObj->nombre : $clienteObj->razonSocial ; ?></td>
    <td class="center"> Placa: <?= $datosVehiculo->placa ?> Chasis : <?= $datosVehiculo->chasis ?></td>
    <td class="center"><?= haceMinimo($var->fechaCreada) ?></td>
    <td class="center"><?= obtenerEstado($datosVehiculo->estado) ?> <?= obtenerEstado($opeObj->estado) ?></td>    
    <td class="center"><?= obtenerEstado($var->estadoFacturacion) ?> <?= empty($var->numeroBoleta)?$var->numeroFactura:$var->numeroBoleta  ?></td>    
    <td class="center"><?= strtoupper($var->nombreCanal)." -  ".strtoupper($var->ejecutivoCanal) ?></td>        
    <?php 
      if(isset($usuarioV)){
        echo "<td class='center'>$usuarioV->usuarioNombre.' '.$usuarioV->usuarioApellidoPaterno</td>";
      }else{
        echo "<td class='center'>No se encontraron sus nombres y apellidos</td>";
      }
    ?>
    <td class="center ">
      <a class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  href="registrarVehiculo?id=<?= $var->idcliente ?>"  >Agregar <i class="fa fa-plus-circle"> </i></a>
    </td>  
</tr>
<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="9">                        
  <?= paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="9" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>

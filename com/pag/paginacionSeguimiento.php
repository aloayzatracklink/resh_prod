
<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $fecha='';
  $tipo=limpiar($_POST['tipoV']);
  if (isset($_POST["fecha"])) {
    $fecha=strip_tags($_POST['fecha']);
    $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
    $desde=date("YmdHis",strtotime($desde));  
    $hasta=date("Ymd",strtotime($hasta)); 
    $hasta=$hasta."235959";
  }
  $opOBJ=new Operacion;
  if($tipo=='ventas'){
    if (empty($fecha)){    
            $cantidadO=$opOBJ->obtenerDatosParaOPECCantidadVentas();    
    }else{
        $cantidadO=$opOBJ->obtenerDatosParaOPECCantidadRANGOVentas($desde,$hasta);
    }  
    $registrosPorPagina=20;
    $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
    $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
    $n=1;
    if (empty($fecha)) {
        $datos=$opOBJ->obtenerDatosParaOpeCVentas($posisionDePagina,$registrosPorPagina);    
    } else {
        $datos=$opOBJ->obtenerDatosParaOpeCRangoVentas($posisionDePagina,$registrosPorPagina,$desde,$hasta);
    }  
?>
    <?php if ($datos): ?>              
    <?php foreach ($datos as $var): ?>  
        <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
        <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>
        <?php $datosF=DatosFacturacion::obtenerDatosFacturacionPorId($var->datosfacturacionid) ?>
        <?php $usuarioV=Usuario::obtenerUsuario($datosF->idUsuarioCreado)  ?>
        <?php $vhOC=Vehiculo::verificarSiEstaDuplicado($datosVehiculo->chasis) ?>
        <?php $datosOpera=Operacion::obtenerOperacionesPorVehiculoIDClienteId($var->idvehiculo,$var->idcliente); ?>
        <tr class='<?= obtenerClassSinClass($var->lugarInstalacion) ?> <?= ($vhOC->cantidad>1)?"danger":"" ?>' >
        <td ><span class="label label-primary"><?= $datosOpera->trabajo ?></span></td>
        <td><?= ($clienteObj->tipoCliente=="Natural") ? $clienteObj->apellidosC." ".$clienteObj->nombre : $clienteObj->razonSocial ; ?></td>
        <td class="center"> Placa: <?= $datosVehiculo->placa ?> <br><?= $datosVehiculo->chasis ?></td>
        <td class="center"><?= haceMinimo($datosVehiculo->fechaCreacion) ?></td>
        <td class="center"><?= !empty($var->fechaInstalacion)?formatoHaceEn($var->fechaInstalacion):"Pendiente" ?></td>    
        <td class="center"><?= obtenerEstado($datosVehiculo->estado) ?> <?= obtenerEstado($datosF->estadoFacturacion) ?></td>    
        <td class="center"><?= $usuarioV->usuarioNombre;?>&nbsp;<?= $usuarioV->usuarioApellidoPaterno;  ?></td>   
        <td class="center"><?= strtoupper($datosF->nombreCanal);  ?></td>
       <td class="center ">
        <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#bateriasOModal" data-id="<?= ",".$var->idoperacion ?>">Elegir <i class="fa fa-cog"> </i></button>
        </td>
    </tr>

    <?php endforeach ?>
    <tr>
    <td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
    <td colspan="9">                        
    <?= paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
    </td>
    </tr>         
    <?php else: ?>
    <tr>
    <td colspan="9" class="text-center">No se encontro registro alguno    
    </td>
    </tr>   
    <?php endif ?>
<?php
  }else if($tipo=='citas'){
      $citaObj=new Cita;
    if (empty($fecha)){    
        $cantidadO=$citaObj->pendienteComCitaTotalSeguimientoCom();    
    }else{
        $cantidadO=$citaObj->pendienteComCitaTotalSeguimientoComRango($desde,$hasta);
    }  
    $registrosPorPagina=10;
    $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
    $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
    $n=1;
    if (empty($fecha)) {
        $datos=$citaObj->pendienteComCitaSinFecha($posisionDePagina,$registrosPorPagina);    
    } else {
        $datos=$citaObj->pendienteComCitaConFecha($posisionDePagina,$registrosPorPagina,$desde,$hasta);
    }  
  
?>
    <?php if ($datos): ?>              
    <?php foreach ($datos as $var): ?>  
        <?php $usuarioV=Usuario::obtenerUsuario($var->idcreador)  ?>
        <?php $datosCanal=Canales::obtenerDatosCanalPorId($var->idcanal) ?>
    <tr >
        <td>Citas</td>
    <td><?= $var->nombreCliente ?></td>
        <td class="center"> Placa: <?= $var->placa ?></td>
        <td class="center"><?= haceMinimo($var->fechaRegistro) ?></td>
        <td class="center"><?= !empty($var->fecha)?formatoHaceEn($var->fecha):"Pendiente" ?></td>    
        <td class="center"><?= obtenerEstado($var->idestado) ?></td>    
        <td class="center"><?= $usuarioV->usuarioNombre;?>&nbsp;<?= $usuarioV->usuarioApellidoPaterno;  ?></td>   
        <td class="center"><?= ($datosCanal)?$datosCanal->nombreCanal." -  ".$datosCanal->ejecutivoCanal:'Pendiente';  ?></td>
       <td class="center ">
        <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#bateriasOModal" data-id="<?= ".".$var->idcita ?>">Elegir <i class="fa fa-cog"> </i></button>
        </td>
    </tr>

    <?php endforeach ?>
    <tr>
    <td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
    <td colspan="9">                        
    <?= paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
    </td>
    </tr>         
    <?php else: ?>
    <tr>
    <td colspan="9" class="text-center">No se encontro registro alguno    
    </td>
    </tr>   
    <?php endif ?>
<?php
  }     ?>      
<?php } ?>

<?php require_once("../../includes/init.php") ?>  
<?php 
  if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $idEjecutivo='';
  $estado='';
  $fecha='';
  // var_dump($_POST['estado']);
  if (isset($_POST["ejecutivoComercial"]) && $_POST['ejecutivoComercial'] <> 'null') {
      $idEjecutivo=htmlspecialchars($_POST['ejecutivoComercial']);
  }
  if (isset($_POST['estado'])) {
    $estado=$_POST['estado'];
  }
  if (isset($_POST['fecha'])) {
    $fecha=$_POST['fecha'];  
    $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
    $desde=date("Ymd000000",strtotime($desde));    
    $hasta=date("Ymd",strtotime($hasta)); 
    $hasta=$hasta."235959";  
  }

  $comObj=new Comisiones();
  if ($estado=='' &&empty($fecha)) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaEJE();    
  } 
  
  if (!empty($_POST['fecha'])) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaEFechaEJE($desde,$hasta);        
  }
  
  if ($estado!='') {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaEestadoPagoEJE($estado);        
  }
  
  if ($estado!=''&&!empty($fecha)) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaEestadoFechaEJE($estado,$desde,$hasta);        
  }
  // if (!empty($idEjecutivo)||!empty($estado)) {
  //   $cantidadO=$comObj->obtenerRegistrosSubidosCantidaE($idEjecutivo);        
  // }  

  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  // echo $cantidadO->cantidad; 
  $n=1;
  if ($estado==''&&empty($fecha)) {
    $datos=$comObj->obtenerRegistrosSubidosEJE($posisionDePagina,$registrosPorPagina);    
  } 
  
  if (!empty($fecha)) {
    $datos=$comObj->obtenerRegistrosSubidosEFechaEJE($posisionDePagina,$registrosPorPagina,$desde,$hasta);        
  }
  if ($estado!='') {
    $datos=$comObj->obtenerRegistrosSubidosEEstadoEJE($posisionDePagina,$registrosPorPagina,$estado);        
  }
  if ($estado!='' && !empty($fecha)  ) {
    $datos=$comObj->obtenerRegistrosSubidosEFechaEstadoEJE($posisionDePagina,$registrosPorPagina,$estado,$desde,$hasta);        
  }
?>   

<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?> 
<?php $canalD=Canales::obtenerDatosCanalPorId($var->idcanal); ?>   
    <tr>

        <td><?= ($var->trabajo) ?></td>
        <td>
            <?php
            if ($var->idrenovacion ==0) {
                $clienDatos = Cliente::obtenerDatosDeclientePorId($var->idcliente);
                echo $retVal = ($clienDatos->tipoCliente == "Natural") ? $clienDatos->apellidosC . " " . $clienDatos->nombre : $clienDatos->razonSocial;
            }else{
                $datosCL=Renovaciones::obtenerDatosDeRenovacionPorId($var->idrenovacion);
                echo $retVal =$datosCL->nombreCliente;
            }
            ?>

        </td>

        <td>
            <?php
            if ($var->idrenovacion ==0) {
                $vhDatos=Vehiculo::obtenerVehiculo($var->idvehiculo);echo $vhDatos->placa;
            }else{
                $vhDatos=Renovaciones::obtenerDatosDeRenovacionPorId($var->idrenovacion);
                echo $retVal =$vhDatos->chasis." | ".$vhDatos->placa;
            }
            ?>
        </td>
          <td><?= ($canalD)?$canalD->nombreCanal." - ".$canalD->ejecutivoCanal:'PENDIENTE' ?> <?= (Canales::verificarEstadoDeCanal($var->idcanal))? '&nbsp;<span class="label label-warning">Canal</span>':'' ?></td>
          <td><?= obtenerEstado($var->estadoFacturacion) ?></td>         
          <td><?= obtenerEstado($var->estadoPago) ?></td>         
        <td><?= obtenerEstado($var->estado) ?></td>
        <td><?= obtenerEstadoComision($var->type_commission) ?></td>
        <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" data-id="<?php echo $var->idcomision ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;</td>
    </tr>
<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="6">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="6" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>
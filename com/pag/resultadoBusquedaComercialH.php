<?php require_once("../../includes/init.php") ?>
<?php if ($_POST): ?> 
<?php 

   $datos=limpiar($_POST['datos']); 
   // $idusuario=$_POST['convenio'];
   $datos="".$datos."%";
   $fecha='';
  if (isset($_POST["fecha"])) {
    $fecha=$_POST['fecha'];
    $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
    $desde=date("YmdHis",strtotime($desde));  
    $hasta=date("Ymd",strtotime($hasta)); 
    $hasta=$hasta."235959";
  }
 ?>  
<?php 
  $factObj=new DatosFacturacion;
  // $factObj->idConfirmacionOPE=limpiar($idusuario);
if (empty($fecha)) {
  $datosA=$factObj->buscarParaFiltroDatosOPEC($datos);
} else {
  $datosA=$factObj->buscarParaFiltroOPECFecha($datos,$desde,$hasta);  
}
$n=1;
 ?>
  <?php if ($datosA): ?>                
  <?php foreach ($datosA as $var): ?>    
   <?php // $usuarioV=Usuario::obtenerUsuario($var->idCreador)  ?>
    <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculos) ?>
    <?php $opeObj=Operacion::obtenereOperacionesPorIdFacturacion($var->datosfacturacionid) ?> 
  <tr <?= obtenerClass($var->lugarInstalacion) ?>>
    <td><?= $n++ ?></td>

<td><?= ($clienteObj->tipoCliente=="Natural") ? $clienteObj->apellidosC." ".$clienteObj->nombre : $clienteObj->razonSocial ; ?></td>
    <td class="center"> Placa: <?= $datosVehiculo->placa ?> Chasis : <?= $datosVehiculo->chasis ?></td>
    <td class="center"><?= haceMinimo($var->fechaCreada) ?></td>
  
    <td class="center"><?= obtenerEstado($datosVehiculo->estado) ?> <?= obtenerEstado($opeObj->estado) ?></td>    
    <td class="center"><?= obtenerEstado($var->estadoFacturacion) ?> <?= empty($var->numeroBoleta)?$var->numeroFactura:$var->numeroBoleta  ?></td>    
    <td class="center"><?= strtoupper($var->nombreCanal)." - ".strtoupper($var->ejecutivoCanal) ?></td>        
    <!-- <td class="center"><?= $usuarioV->usuarioNombre;?>&nbsp;<?= $usuarioV->usuarioApellidoPaterno;  ?></td> -->
    <td class="center ">
      <a class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  href="registrarVehiculo?id=<?= $var->idcliente ?>"  >Agregar <i class="fa fa-plus-circle"> </i></a>
    </td>  
</tr>
<?php endforeach ?>
   <?php else: ?>
   <tr>   
        <td colspan="8" class="text-center">No se encontro ningun registro con <h2><?= $_POST['datos'] ?>.</h2> </td>
   </tr>
   <?php endif ?>   
 <?php endif ?> 

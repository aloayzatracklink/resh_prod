<?php require_once("../../includes/init.php");

if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    if (isset($_POST["page"])) {
        $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
        if (!is_numeric($numeroDePagina)) {
            die('Numero de pagina incorrecta !');
        } //incase of invalid page number
    } else {
        $numeroDePagina = 1; //if there's no page number, set it to 1
    }
    $datos = limpiar($_POST['datos']);

    $estdAc= limpiar($_POST['estdAc']);
    $estdVn = limpiar($_POST['estdVn']);
    $tr = ($_POST['tr']);

    $fecha='';$desde="";$hasta="";
    if (isset($_POST["fecha"])) {
        if($_POST["fecha"]!="") {
            $fecha = strip_tags($_POST['fecha']);
            $desde = trim(current(explode('/', $fecha)));
            $tmp = explode('/', $fecha);
            $hasta = end($tmp);
            $desde = date("Y-m-d", strtotime($desde));
            $hasta = date("Y-m-d", strtotime($hasta));
        }

    }

    $objTY = new TramaIntegracionTY();

    $cantidadO = $objTY->getCantidadInfoSegTyAdvTyConmigo($datos,$tr,$estdAc,$estdVn,$desde,$hasta);
    $registrosPorPagina = 20;
    $paginasEnTotal = ceil($cantidadO->cantidad / $registrosPorPagina);
    $posisionDePagina = (($numeroDePagina - 1) * $registrosPorPagina);

    $obj=TramaIntegracionTY::getInfoSegTyAdvTyConmigo($datos,$tr,$estdAc,$estdVn,$desde,$hasta,$posisionDePagina,$registrosPorPagina);$n=0; ?>
    <?php if($obj){
        foreach ($obj as $var):
            $n++; ?>
            <tr >
                <td><?=  $n ?></td>
                <td><?=  $var->tipo_transaccion ?></td>
                <td><?=  $var->fechaRecibido ?></td>
                <td><?=  $var->cliente ?></td>
                <td><?=  $var->num_documento?></td>
                <td><?=  $var->placa ?></td>
                <td><?=  $var->chasis ?></td>
                <td><?=  $var->cbu ?></td>
                <td><?=  $var->plan ?></td>
                <td><?=  $var->tiempo ?></td>
                <td><?=  $var->precio_lista ?></td>
                <td><?=  $var->precio_tracklink ?></td>
                <td><?=  $var->dlr_madre ?></td>
                <td><?=  $var->fechaInstalacion ?></td>
                <td><?=  $var->fecha_preactivacion ?></td>
                <td><?=  $var->estado_preactivacion ?></td>
                <td><?=  $var->fecha_activacion ?></td>
                <td><?=  $var->estadoActivacion ?></td>
                <td><?=  $var->fecha_vinculacion ?></td>
                <td><?=  $var->estadoVinculacion ?></td>
                <td><?=  $var->estadoTrama ?></td>
                <td><?=  $var->estadoOpera ?></td>
                <td><?=  $var->motivoAnulacion ?></td>
                <td><?=  $var->fechaAnulacion ?></td>
                <?php $usuarioObjR=Usuario::obtenerUsuario($var->idsuariocargo);  ?>
                <td style="background-color: <?php echo $retVal = ($usuarioObjR->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>"><?php  echo $usuarioObjR->usuarioNombre ?> <?php echo $usuarioObjR->usuarioApellidoPaterno?></td>
                <?php $usuarioObjV=Usuario::obtenerUsuario($var->usuario_vinculacion);  ?>
                <td style="background-color: <?php echo $retVal = ($usuarioObjV->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>"><?php  echo $usuarioObjV->usuarioNombre ?> <?php echo $usuarioObjV->usuarioApellidoPaterno?></td>
                <?php $usuarioObjPA=Usuario::obtenerUsuario($var->usuario_preactivacion);  ?>
                <td style="background-color: <?php echo $retVal = ($usuarioObjA->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>"><?php  echo $usuarioObjPA->usuarioNombre ?> <?php echo $usuarioObjPA->usuarioApellidoPaterno?></td>
                <?php $usuarioObjA=Usuario::obtenerUsuario($var->usuario_activacion);  ?>
                <td style="background-color: <?php echo $retVal = ($usuarioObjA->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>"><?php  echo $usuarioObjA->usuarioNombre ?> <?php echo $usuarioObjA->usuarioApellidoPaterno?></td>
                <?php $usuarioObjAN=Usuario::obtenerUsuario($var->usuarioAnulacion);  ?>
                <td style="background-color: <?php echo $retVal = ($usuarioObjAN->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>"><?php  echo $usuarioObjAN->usuarioNombre ?> <?php echo $usuarioObjAN->usuarioApellidoPaterno?></td>
                <td><?=  $var->seguimiento ?> </td>
                <td>
                    <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0;display: inline-block" data-toggle="modal"
                              data-target="#bateriasOModal" onclick="limpiarTrama()"  data-idInt="<?= $var->idtrama ?>"><i class="fa fa-eye"></i>
                    </button>
                    <?php if(($var->estado!='99' or $var->estado!=99) and ($var->tipo_transaccion!="Pre-Registro")){?>
                    <button class="btn btn-danger " style="padding-top: 0;padding-bottom: 0;display: inline-block" data-toggle="modal"
                            data-target="#anularOModal"   data-idInt="<?= $var->idtrama ?>"><i class="fa fa-ban"></i>
                    </button>
                    <?php }?>
                </td>
            </tr>
        <?php endforeach;?>
        <tr>
            <td colspan="2" class="text-right">Mostrando <?= $retVal = ($n<$registrosPorPagina) ? $n : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>

        </tr>
    <?php } else{?>
        <tr>
            <td colspan="16" class="text-center">No hay información Registrada</td>
        </tr>
    <?php }?>
<?php } ?>

<?php require_once("../../includes/init.php") ?>  
<?php 
  if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $idEjecutivo='';
  $estado='';
  $fecha='';
  
  if (isset($_POST["ejecutivoComercial"]) && $_POST['ejecutivoComercial'] <> 'null') {
      $idEjecutivo=htmlspecialchars($_POST['ejecutivoComercial']);
  }
  if (isset($_POST['estado'])) {
    $estado=$_POST['estado'];
  }
  if (isset($_POST['fecha'])) {
    $fecha=$_POST['fecha'];  
    $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
     $desde=date("Ymd000000",strtotime($desde));    
    $hasta=date("Ymd",strtotime($hasta)); 
    $hasta=$hasta."23:59:59";  
  }

  $comObj=new Comisiones();

  $renObj=new Renovaciones();
  if (empty($idEjecutivo)&&empty($estado) &&empty($fecha)) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCONRHORUCCANTIDAD();    
  } 

  if (!empty($idEjecutivo)) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaE($idEjecutivo);        
  }
  
  if (!empty($_POST['fecha'])) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaEFecha($desde,$hasta);        
  }
  
  if (!empty($_POST['estado'])) {
    $cantidadO=$comObj->obtenerRegistrosPORESTADOCOmision($estado);        
  }

  if (!empty($idEjecutivo)&&!empty($fecha)) {
    $cantidadO=$comObj->obtenerCantidadListosEjeYfecha($idEjecutivo,$desde,$hasta);        
  }
  if (!empty($estado)&&!empty($idEjecutivo)) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaEestaodIdejutivo($idEjecutivo,$estado);        
  }  
  if (!empty($estado)&&!empty($fecha)) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaEestadoFecha($estado,$desde,$hasta);        
  }
  // if (!empty($idEjecutivo)||!empty($estado)) {
  //   $cantidadO=$comObj->obtenerRegistrosSubidosCantidaE($idEjecutivo);        
  // }
  if (!empty($idEjecutivo)&&!empty($estado)&&!empty($fecha)) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaETodos($estado,$desde,$hasta,$idEjecutivo);        
  }
  $registrosPorPagina=30;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  // echo $cantidadO->cantidad; 
  $n=1;
  if (empty($idEjecutivo)&&empty($estado) &&empty($fecha)) {
    $datos=$comObj->obtenerRegistrosSubidosCONRHORUC($posisionDePagina,$registrosPorPagina);    
  } 

  if (!empty($idEjecutivo)) {
    $datos=$comObj->obtenerRegistrosSubidosE($posisionDePagina,$registrosPorPagina,$idEjecutivo);        
  }
  if (!empty($fecha)) {
    $datos=$comObj->obtenerRegistrosSubidosEFecha($posisionDePagina,$registrosPorPagina,$desde,$hasta);        
  }
  if (!empty($estado)) {
    $datos=$comObj->obtenerResEstadoComsionnn($posisionDePagina,$registrosPorPagina,$estado);        
  }
  if (!empty($idEjecutivo) && !empty($estado)  ) {
    $datos=$comObj->obtenerRegistrosSubidosEestadoIdejecu($posisionDePagina,$registrosPorPagina,$estado,$idEjecutivo);        
  }
  if (!empty($idEjecutivo) && !empty($fecha)  ) {
    $datos=$comObj->obtenerRegistrosSubidosEfechaIdejecutioListo($posisionDePagina,$registrosPorPagina,$idEjecutivo,$desde,$hasta);        
  }
  if (!empty($estado) && !empty($fecha)  ) {
    $datos=$comObj->obtenerRegistrosSubidosEFechaEstado($posisionDePagina,$registrosPorPagina,$estado,$desde,$hasta);        
  }
  if (!empty($estado) && !empty($fecha) && !empty($idEjecutivo)  ) {
    $datos=$comObj->obtenerRegistrosSubidosETodos($posisionDePagina,$registrosPorPagina,$estado,$idEjecutivo,$desde,$hasta);        
  }

?>   
<?php if ($datos): ?>  
<?php foreach ($datos as $var): ?>    
    <?php
    if ($var->idrenovacion ==0) {
        $u = Usuario::obtenerUsuario($var->idCreador);
        }else{
        $da = $renObj->obtenerDatosDeRenovacionPorId($var->idrenovacion);
        $u = Usuario::obtenerUsuario($da->idejecutivo);
    }

              ?>
<?php
    $dd=DatosFacturacion::obtenerDatosFacturacionPorId($var->idfacturacion);
?>
    <tr>
        <td><?= ($var->trabajo) ?></td>
        <td>
            <?php
            if ($var->idrenovacion ==0) {
                $clienDatos = Cliente::obtenerDatosDeclientePorId($var->idcliente);
                echo $retVal = ($clienDatos->tipoCliente == "Natural") ? $clienDatos->apellidosC . " " . $clienDatos->nombre : $clienDatos->razonSocial;
            }else{
                $datosCL=Renovaciones::obtenerDatosDeRenovacionPorId($var->idrenovacion);
                echo $retVal =$datosCL->nombreCliente;
            }
            ?>

        </td>

        <td>
            <?php
            if ($var->idrenovacion ==0) {
                $vhDatos=Vehiculo::obtenerVehiculo($var->idvehiculo);echo $vhDatos->placa;
            }else{
                $vhDatos=Renovaciones::obtenerDatosDeRenovacionPorId($var->idrenovacion);
                echo $retVal =$vhDatos->chasis." | ".$vhDatos->placa;
            }
            ?>
        </td>
        
        <td> 
          <?php if (!empty($var->idvehiculo)){
            $vhDatos=Vehiculo::obtenerVehiculo($var->idvehiculo);echo $vhDatos->chasis;

          }
          ?>
        </td>

          <td><?= strtoupper($dd->nombreCanal) ?></td>
<?php
    $doc=(empty($dd->numeroBoleta))?$dd->numeroFactura:$dd->numeroBoleta;
?>

          <td><?= $dd->tipoDocumentoFacturacion[0]." ".$doc." ".obtenerEstado($var->estadoFacturacion) ?></td>
          <td><?= obtenerEstado($var->estadoPago) ?></td>         
          <td><?= obtenerEstado($var->estado) ?></td> <!--ESTADO COMISIÓN : corregir-->
        <td><?= obtenerEstadoComision($var->type_commission) ?></td>
          <td><?= haceMinimoDias($var->fechaRegistro) ?></td>
          <td><?= $u->usuarioNombre;?>&nbsp;<?= $u->usuarioApellidoPaterno;  ?> <span class="<?= ($var->idtitular=='0')?'':'fa fa-check' ?>"></span></td>
          <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" data-id="<?php echo $var->idcomision ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;</td>
    </tr>
<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="7">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="7" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>

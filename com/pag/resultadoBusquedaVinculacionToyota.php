<?php require_once("../../includes/init.php");

if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    if (isset($_POST["page"])) {
        $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
        if (!is_numeric($numeroDePagina)) {
            die('Numero de pagina incorrecta !');
        } //incase of invalid page number
    } else {
        $numeroDePagina = 1; //if there's no page number, set it to 1
    }
    $datos = limpiar($_POST['datos']);
    $est = limpiar($_POST['estd']);

    $objTY = new TramaIntegracionTY();

    $cantidadO = $objTY->getCantidadInfoVinculacion($datos,$est);
    $registrosPorPagina = 20;
    $paginasEnTotal = ceil($cantidadO->cantidad / $registrosPorPagina);
    $posisionDePagina = (($numeroDePagina - 1) * $registrosPorPagina);

    $obj=TramaIntegracionTY::getInfoVinculacion($datos,$est,$posisionDePagina,$registrosPorPagina);$n=0; ?>
    <?php if($obj){
        foreach ($obj as $var):
            $n++; ?>
            <tr >
                <td><?=  $n ?></td>
                <td><?=  $var->asegurado2 ?></td>
                <td><?=  $var->num_documento2 ?></td>
                <td><?=  $var->chasis2 ?></td>
                <td><?=  $var->placa2?></td>
                <td><?=  $var->fecha_registro2 ?></td>
                <td><?=  $var->asegurado ?></td>
                <td><?=  $var->num_documento ?></td>
                <td><?=  $var->chasis ?></td>
                <td><?=  $var->placa ?></td>
                <td><?=  $var->fecha_registro ?></td>
                <td><?=  $var->estadoVinculacion ?></td>
                <td><?=  $var->fecha_vinculacion ?></td>
                <?php $usuarioObj=Usuario::obtenerUsuario($var->usuario_vinculacion);  ?>
                <td style="background-color: <?php echo $retVal = ($usuarioObj->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>"><?php  echo $usuarioObj->usuarioNombre ?> <?php echo $usuarioObj->usuarioApellidoPaterno?></td>
                <td>
                    <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0" data-toggle="modal"
                            data-target="#vinculacionOModal" onclick="limpiarTrama()"  data-idInt="<?= $var->idtrama ?>"><i class="fa fa-eye"></i></button>
                    <?php if($var->estado_vinculacion==0 && $var->estado!=99){?>
                        <form  method="post" name="FRMLINK" id="FRMLINK<?= $var->idtrama ?>">
                            <button type="submit" class="btn btn-primary btnlink<?= $var->idtrama ?>" data-style="zoom-in" style="padding-top: 0;padding-bottom: 0" onclick="return btnLink(this)" data-tipo="blink"  data-Hlink="<?= $var->chasis ?>" data-id="<?= $var->idtrama ?>"  id="btnlink<?= $var->idtrama ?>" name="btnlink">Vincular</button>
                            <input type="text" name="idlinkHomo" id="idlinkHomo<?= $var->idtrama ?>" disabled value="<?= $var->idtrama; ?>" style="display:none;">
                            <input type="text" name="btnblink" id="btnblink<?=$var->idtrama ?>" disabled value="<?= $var->idtrama ?>" style="display:none;">
                        </form>
                    <?php }?>

                    <?php if($var->estado_vinculacion==1 && $var->estado!=99 && $var->estado_preactivacion==0){?>
                        <form  method="post" name="FRMLINK2" id="FRMLINK2<?= $var->idtrama ?>">
                            <button type="submit" class="btn btn-primary btnlink2<?= $var->idtrama ?>" data-style="zoom-in" style="padding-top: 0;padding-bottom: 0" onclick="return btnLink2(this)" data-tipo="blink2"  data-Hlink="<?= $var->chasis ?>" data-id="<?= $var->idtrama ?>"  id="btnlink2<?= $var->idtrama ?>" name="btnlink2">Pre Activar</button>
                            <input type="text" name="idlinkHomo2" id="idlinkHomo2<?= $var->idtrama ?>" disabled value="<?= $var->idtrama; ?>" style="display:none;">
                            <input type="text" name="btnblink2" id="btnblink2<?=$var->idtrama ?>" disabled value="<?= $var->idtrama ?>" style="display:none;">
                        </form>
                    <?php }?>

                </td>
            </tr>
        <?php endforeach;?>
        <tr>
            <td colspan="13"></td>
            <td colspan="2" class="text-right">Mostrando <?= $retVal = ($n<$registrosPorPagina) ? $n : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>

        </tr>
    <?php } else{?>
        <tr>
            <td colspan="16" class="text-center">No hay información Registrada</td>
        </tr>
    <?php }?>
<?php } ?>

<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $idEjecutivo='';
  if (isset($_POST["ejecutivoComercial"]) && $_POST['ejecutivoComercial'] <> 'null') {
      $idEjecutivo=htmlspecialchars($_POST['ejecutivoComercial']);
  }

  $comObj=new Comisiones();
  if (empty($idEjecutivo)) {
    $cantidadO=$comObj->obtenerRecibosSubidosCantida();    
  } else {
    // $cantidadO=$comObj->obtenerRegistrosSubidosCantidaE($idEjecutivo);        
  }
  
  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  // echo $cantidadO->cantidad; 
  $n=1;
  if (empty($idEjecutivo)) {
    $datos=$comObj->obtenerRecibosSubidos($posisionDePagina,$registrosPorPagina);    
  } else {
    // $datos=$comObj->obtenerRegistrosSubidosE($posisionDePagina,$registrosPorPagina,$idEjecutivo);        
  } 
?>    
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>    
    <tr class="animated bounceIn">
         <td><?php echo $n++ ?></td>
          <td><?php $clienDatos=Cliente::obtenerDatosDeclientePorId($var->idcliente); echo $retVal = ($clienDatos->tipoCliente=="Natural") ? $clienDatos->apellidosC." ".$clienDatos->nombre : $clienDatos->razonSocial ; ?></td>
          <td><?php $canalD=Canales::obtenerDatosCanalPorId($var->idcanal);
           echo $canalD->nombreCanal; echo "&nbsp;&nbsp;";echo $canalD->ejecutivoCanal ?></td>
          <td><?=  $retVal = (empty($var->numeroReciboHonorario)) ? "pendiente" : $var->numeroReciboHonorario ;   ?> - <?= $retVal = (empty($var->numeroFacturacion)) ? "pendiente" : $var->numeroFacturacion ; ?></td>
          <td><?= $var->comision ?></td>          
          <td><?= haceMinimo($var->fechaRegistro) ?></td>          
          <?php $o=Usuario::obtenerUsuario($var->idCreador); ?>
          <td><?= $o->usuarioNombre." ".$o->usuarioApellidoPaterno;  ?></td>          
          <td><a class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" target="_blank"  href="../../public/vouchers/<?php echo $var->reciboHonorarios ?>" >Ver <i class="fa fa-eye"></i></a>&nbsp;<a class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" target="_blank" download="../../public/vouchers/<?php echo $var->reciboHonorarios ?>" href="../../public/vouchers/<?php echo $var->reciboHonorarios ?>" >Descargar <i class="fa fa-cloud-download"></i></a></td>
    </tr>
<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="6">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="6" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>


<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $fecha='';
  if (isset($_POST['convenio'])) {
    $idusuario=htmlspecialchars($_POST['convenio']);
  }else{
    $idusuario=$session->idusuario;
  }
  if (isset($_POST["fecha"])) {
    $fecha=strip_tags($_POST['fecha']);
    $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
    $desde=date("YmdHis",strtotime($desde));  
    $hasta=date("Ymd",strtotime($hasta)); 
    $hasta=$hasta."235959";
  }
  $opOBJ=new Operacion;
  $opOBJ->idConfirmacionOPE=limpiar($idusuario);
  if ($_POST['estado']=='333') {    
    if (empty($fecha)){    
      $cantidadO=$opOBJ->obtenerDatosParaOPECCantidad();    
    }else{
      $cantidadO=$opOBJ->obtenerDatosParaOPECCantidadRANGO($desde,$hasta);
    }  
    $registrosPorPagina=10;
    $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
    $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
    $n=1;
    if (empty($fecha)) {
      $datos=$opOBJ->obtenerDatosParaOpeC($posisionDePagina,$registrosPorPagina);    
    } else {
      $datos=$opOBJ->obtenerDatosParaOpeCRango($posisionDePagina,$registrosPorPagina,$desde,$hasta);
    }  
  }else{
    /////////////////////////CON ESTADO ///////////////////////////
    if (empty($fecha)){    
      $cantidadO=$opOBJ->obtenerDatosParaOPECCantidadEstado($_POST['estado']);    
    }else{
      $cantidadO=$opOBJ->obtenerDatosParaOPECCantidadRANGOEstado($desde,$hasta,$_POST['estado']);
    }  
    $registrosPorPagina=10;
    $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
    $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);   
    $n=1;
    if (empty($fecha)) {
      $datos=$opOBJ->obtenerDatosParaOpeCEstado($posisionDePagina,$registrosPorPagina,$_POST['estado']);    
    } else {
      $datos=$opOBJ->obtenerDatosParaOpeCRangoEstado($posisionDePagina,$registrosPorPagina,$desde,$hasta, $_POST['estado']);
    }
    /////////////////////////CON ESTADO ///////////////////////////
  }
?>    
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>  
    <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>
    <?php $datosF=DatosFacturacion::obtenerDatosFacturacionPorId($var->datosfacturacionid) ?>
    <?php $usuarioV=Usuario::obtenerUsuario($datosF->idUsuarioCreado)  ?>
    <?php $vhOC=Vehiculo::verificarSiEstaDuplicado($datosVehiculo->chasis) ?>
    <?php $datosOpera=Operacion::obtenerOperacionesPorVehiculoIDClienteId($var->idvehiculo,$var->idcliente); ?>
    <?php $datosTrama = TramaIntegracionTY::obtenerTramaIntegracionPorIdOperacion($var->idoperacion);  ?>
<tr class='<?= obtenerClassSinClass($var->lugarInstalacion) ?> <?= ($vhOC->cantidad>1)?"danger":"" ?> ' style="<?= $datosVehiculo->campana=="Toyota"?"border: 2px solid red !important":"" ?>" >
    <td style="width:50px"><?= $n++ ?></td>
    <td ><span class="label label-primary"><?= $datosOpera->trabajo ?></span></td>
    <td><?php if($clienteObj->tipoCliente=="Natural"){
        echo strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre);
    }else{
        echo (!empty($clienteObj->nombre))?strtoupper($clienteObj->nombre):strtoupper($clienteObj->razonSocial);
    }
   ?></td>
    <td class="center" style=" "> Placa: <?= $datosVehiculo->placa ?> Chasis : <?= $datosVehiculo->chasis ?></td>
    <td class="center"><?= haceMinimo($var->fechaConfirmacionOPE) ?></td>
    <td class="center"><?= !empty($var->fechaInstalacion)?formatoHaceEn($var->fechaInstalacion):"Pendiente" ?></td>
    <td class="center"><?= obtenerEstado($datosVehiculo->estado) ?> <?= obtenerEstado($var->estado) ?></td>    
    <td class="center"><?= obtenerEstado($datosF->estadoFacturacion) ?> <?= empty($datosF->numeroBoleta)?$datosF->numeroFactura:$datosF->numeroBoleta  ?></td>    
    <td class="center"><?= empty($datosF->orden)?'Pendiente':$datosF->orden ?></td>
    <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosVehiculo->campana</span>":$datosVehiculo->campana) ?></td>
    <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosTrama->tipo_transaccion</span>":'') ?></td>
    <td class="center"><?= $usuarioV->usuarioNombre;?>&nbsp;<?= $usuarioV->usuarioApellidoPaterno;  ?></td>
    <td class="center ">
      <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#bateriasOModal" onclick="limpiarChasis()" data-id="<?= $var->idoperacion ?>">Elegir <i class="fa fa-cog"> </i></button>
    </td> 
</tr>
<?php endforeach ?>
<tr>
<td colspan="2">Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="9">
  <?= paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>
<?php else: ?>
<tr>
  <td colspan="9" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>

<?php require_once("../../includes/init.php");
 if ($_POST):
    $datos=limpiar($_POST['datos']);
    $idusuario=$session->idusuario;
    $datos="".$datos."%";

    $objTrama=new TramaIntegracion();
    $datosA=$objTrama->obtenerTramaIntegracionPagn($datos);

    $n=1;
    if ($datosA):
        foreach ($datosA as $var): ?>

            <tr class=''>
                <td><?= $n++ ?></td>
                <td class="center"><?= $var->fecha_registro ?></td>
                 <td class="center"><?= $var->apellido_asegurado ?> <?= $var->nombre_asegurado?></td>
                <td class="center"><?= $var->placa ?>|<?= $var->marca ?></td>

                <td class="center"><?= $var->fecha_inicio ?></td>
                <td class="center">
              <?php foreach (TramaIntegracion::getSeg($var->idtrama) as $tr ): ?>
                    <?= $tr->body ?> <br> 
              <?php endforeach ?>
                </td>

                <?php if(!is_null($var->idsuariocargo)){ $usuarioObj=Usuario::obtenerUsuario($var->idsuariocargo); }   ?>
                <td class="center"  style="<?php if(!is_null($var->idsuariocargo)){ echo 'background-color: #86d7fd'; }else{ echo 'background-color: #8befda';} ?>"><?php if(!is_null($var->idsuariocargo)){echo $usuarioObj->usuarioNombre.' '.$usuarioObj->usuarioApellidoPaterno;}else{ echo 'Libre';}?></td>
                <td class="center ">
                    <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0" data-toggle="modal"
                            data-target="#bateriasOModal" onclick="limpiarTrama()" data-idInt="<?= $var->idtrama ?>">Elegir <i class="fa fa-cog"> </i></button>
                </td>
            </tr>

<?php
        endforeach ;
    else:
?>
        <tr>
            <td colspan="8" class="text-center">No se encontro ningun registro con <h2><?= $_POST['datos'] ?>.</h2> </td>
        </tr>
 <?php
    endif;
 endif;

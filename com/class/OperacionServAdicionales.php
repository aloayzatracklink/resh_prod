<?php


class OperacionServAdicionales extends Recursos
{
    public $idosv;
    public $idoperacion;
    public $idser;
    public $idusuario;
    public $fechaCreada;

    public function registrarOperacionServAdicional(){
        $this->fechaCreada=date("Y-m-d H:i:s");
        global $db;
        global $session;
        $sql="INSERT INTO opera_servad(idoperacion,idser,idusuario,fechaCreada,tipo) ";
        $sql.=" VALUES (";
        $sql.= $db->escapeString($this->idoperacion)." , ";
        $sql.= $db->escapeString($this->idser)." , '";
        $sql.= $db->escapeString($session->idusuario)."' , '";
        $sql.= $db->escapeString($this->fechaCreada)."',1)";
        if ($db->query($sql)) {
            return true;
        }else{
            return false;
        }
    }

    public function borrarServAdicionales(){
        global $db;
        $sql = "DELETE FROM opera_servad WHERE idoperacion ='".$db->escapeString($this->idoperacion)."' ";
        if ($db->query($sql)) {
            return true;
        }else{
            return false;
        }
    }


    public function registrarOperacionServAdicionalCita(){
        $this->fechaCreada=date("Y-m-d H:i:s");
        global $db;
        global $session;
        $sql="INSERT INTO opera_servad(idoperacion,idser,idusuario,fechaCreada,tipo) ";
        $sql.=" VALUES (";
        $sql.= $db->escapeString($this->idoperacion)." , ";
        $sql.= $db->escapeString($this->idser)." , '";
        $sql.= $db->escapeString($session->idusuario)."' , '";
        $sql.= $db->escapeString($this->fechaCreada)."',2)";
        if ($db->query($sql)) {
            return true;
        }else{
            return false;
        }
    }
}
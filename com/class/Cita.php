<?php 
 
class Cita extends Recursos
{	
	public $idcita;
	public $nombreCliente;
	public $plan;
	public $nombre;
	public $celular;
	public $correo;
	public $costo;
	public $seguro;
	public $placa;
	public $fecha;
	public $observacion;
	public $lugarTaller;
	public $idcreador;
	public $fechaRegistro;
	public $idSac;
	public $fechaSac;
	public $idOpe;
	public $fechaOpe;
	public $idestado;
	public $obsSac;
	public $idcanal;
	public $idconvenio;
	public $comision;
	public $comsiona;
	public $campana;
	public $idReprogramado;
	public $fechaRegistroReprogramacion;
	public $tiempo;
	public $cantidad;
	public $estadoPago;
	public $voucher;
	public $idoperacion;
	public $oficina;
	public $tipoCanal;
	public $nombreCanal;
	public $ejecutivoCanal;
	function __construct()
	{	
	}
	public function crearCita(){
		global $db;
		global $session;
		$this->fechaRegistro=date("YmdHis");
		$sql="INSERT INTO citasTaller (nombreCliente,plan,celular,correo,tiempo,costo,seguro,placa,fecha,observacion,idcreador,idReprogramado,fechaRegistroReprogramacion,obsSac,idconvenio,idcanal,comision,comsiona,campana,idSac,idestado,estadoPago,voucher,oficina,tipoCanal,nombreCanal,ejecutivoCanal,fechaRegistro) ";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString(ucfirst($this->nombreCliente))."' , '";
		$sql.= $db->escapeString($this->plan)."' , '";
		$sql.= $db->escapeString($this->celular)."' , '";
		$sql.= $db->escapeString($this->correo)."' , '";
		$sql.= $db->escapeString($this->tiempo)."' , '";
		$sql.= $db->escapeString($this->costo)."' , '";
		$sql.= $db->escapeString($this->seguro)."' , '";
		$sql.= $db->escapeString($this->placa)."' , '";
		$sql.= $db->escapeString($this->fecha)."' , '";
		$sql.= $db->escapeString($this->observacion)."' , '";
		$sql.= $db->escapeString($this->idcreador)."' , '";
		$sql.= $db->escapeString($this->idReprogramado)."' , '";
		$sql.= $db->escapeString($this->fechaRegistroReprogramacion)."' , '";
		$sql.= $db->escapeString($this->obsSac)."' , '";
		$sql.= $db->escapeString($this->idconvenio)."' , '";
		$sql.= $db->escapeString($this->idcanal)."' , '";
		$sql.= $db->escapeString($this->comision)."' , '";
		$sql.= $db->escapeString($this->comsiona)."' , '";
		$sql.= $db->escapeString($this->campana)."' , '";
		$sql.= $db->escapeString($this->idSac)."' , '";
		$sql.= $db->escapeString($this->idestado)."' , '";
		$sql.= $db->escapeString($this->estadoPago)."' , '";
		$sql.= $db->escapeString($this->voucher)."' , '";
		$sql.= $db->escapeString($this->oficina)."' , '";
		$sql.= $db->escapeString($this->tipoCanal)."' , '";
		$sql.= $db->escapeString($this->nombreCanal)."' , '";
		$sql.= $db->escapeString($this->ejecutivoCanal)."' , '";
		$sql.= $db->escapeString($this->fechaRegistro)."')";
		if ($db->query($sql)) {
			$this->idcita=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}
	}
	public function crearCitaOPE(){
		global $db;
		global $session;
		$this->fechaRegistro=date("YmdHis");
		$this->fechaOpe=date("YmdHis");
		$sql="INSERT INTO citasTaller (nombreCliente,plan,celular,correo,tiempo,costo,seguro,placa,fecha,observacion,idcreador,idReprogramado,fechaRegistroReprogramacion,obsSac,idconvenio,idcanal,comision,comsiona,campana,estadoPago,voucher,idSac,idestado,fechaOpe,idOpe,oficina,tipoCanal,nombreCanal,ejecutivoCanal,fechaRegistro) ";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString(ucfirst($this->nombreCliente))."' , '";
		$sql.= $db->escapeString($this->plan)."' , '";
		$sql.= $db->escapeString($this->celular)."' , '";
		$sql.= $db->escapeString($this->correo)."' , '";
		$sql.= $db->escapeString($this->tiempo)."' , '";
		$sql.= $db->escapeString($this->costo)."' , '";
		$sql.= $db->escapeString($this->seguro)."' , '";
		$sql.= $db->escapeString($this->placa)."' , '";
		$sql.= $db->escapeString($this->fecha)."' , '";
		$sql.= $db->escapeString($this->observacion)."' , '";
		$sql.= $db->escapeString($this->idcreador)."' , '";
		$sql.= $db->escapeString($this->idReprogramado)."' , '";
		$sql.= $db->escapeString($this->fechaRegistroReprogramacion)."' , '";
		$sql.= $db->escapeString($this->obsSac)."' , '";
		$sql.= $db->escapeString($this->idconvenio)."' , '";
		$sql.= $db->escapeString($this->idcanal)."' , '";
		$sql.= $db->escapeString($this->comision)."' , '";
		$sql.= $db->escapeString($this->comsiona)."' , '";
		$sql.= $db->escapeString($this->campana)."' , '";
		$sql.= $db->escapeString($this->estadoPago)."' , '";
		$sql.= $db->escapeString($this->voucher)."' , '";
		$sql.= $db->escapeString($this->idSac)."' , '14', '";
		$sql.= $db->escapeString($this->fechaOpe)."' , '";
		$sql.= $db->escapeString($session->idusuario)."' , '";		
		$sql.= $db->escapeString($this->oficina)."' , '";
		$sql.= $db->escapeString($this->tipoCanal)."' , '";
		$sql.= $db->escapeString($this->nombreCanal)."' , '";
		$sql.= $db->escapeString($this->ejecutivoCanal)."' , '";
		$sql.= $db->escapeString($this->fechaRegistro)."')";
		if ($db->query($sql)) {
			$this->idcita=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}
	}

	public function reprogramarCita(){
		global $db;
		global $session;
		$datosA=self::obtenerCitaPorId($this->idcita);
		$this->fecha=$datosA->fecha;
        $this->guardarComoIncidencia();
        $rCE="UPDATE citasTaller SET ";
        $rCE.="idestado='321',";
        $rCE.="obsSac='";
        $rCE.=$this->obsSac."', ";
        $rCE.="fechaRegistroReprogramacion='";
        $rCE.=date('YmdHis')."', ";
        $rCE.="idSac='";
        $rCE.=$session->idusuario."' ";
        $rCE.=" WHERE idcita='";
        $rCE.=$this->idcita."'";
        return ($db->query($rCE))?true:false;
	}
    public function reprogramarCitaEstadoAUTO(){
        global $db;
        $rCE="UPDATE citasTaller SET ";
        $rCE.="idestado='321',";
        $rCE.="obsSac='";
        $rCE.=$this->obsSac."', ";
        $rCE.="fechaRegistroReprogramacion='";
        $rCE.=date('YmdHis')."', ";
        $rCE.="idSac='0' ";
        $rCE.=" WHERE idcita='";
        $rCE.=$this->idcita."'";
        $this->borrardeProgramacioAUTO();
        return ($db->query($rCE))?true:false;
    }
    public function borrardeProgramacioAUTO(){
        global $db;
        $bdP="UPDATE tablaProgramacion SET estado='321' WHERE idcita='$this->idcita'";
        return ($db->query($bdP))?true:false;
    }

	public function completaridCanal(){
		global $db;
		$sql="UPDATE citasTaller SET ";
		$sql.="comision ='";
		$sql.=$db->escapeString($this->comision)."', ";
		$sql.="idcanal ='";
		$sql.=$db->escapeString($this->idcanal)."' ";
		$sql.=" WHERE ";
		$sql.=" idcita='";
		$sql.=$db->escapeString($this->idcita)."'";
		return ($db->query($sql))?true:false;
	}

	public static function obtenerCitasEnCreador()
	{
		global $session;
		$obtenerCitas=self::sentencia("SELECT * FROM citasTaller WHERE (idcreador='$session->idusuario') AND (idestado='0' OR idestado='14'  OR idestado='321') ORDER BY idcita DESC LIMIT 80 ");
		return !empty($obtenerCitas)?$obtenerCitas:false;
	}
	
	public static function obtenerCitaADV()
	{
		global $session;
        $uD=Usuario::obtenerEjecutivosAsignados($session->idusuario);
        $cantidad=count($uD);
        if ($uD) {
            $idEjeA="0,";
            foreach ($uD as $var){
                if ($var===end($uD)) {
                    $idEjeA.=$var->idusuario;
                }else{
                    $idEjeA.=$var->idusuario.",";
                }
            }
            $obtenerCitas=self::sentencia("SELECT * FROM citasTaller WHERE idcreador IN ($idEjeA) AND (idestado='0' OR idestado='14'  OR idestado='321') ORDER BY idcita DESC LIMIT 80 ");
            return !empty($obtenerCitas)?$obtenerCitas:false;
        }else{
            return false;
        }
	}
	public static function reprogramarMenuIzquierdoCita(){
		global $session;
		$menuCitaRep=self::sentencia("SELECT COUNT(*) AS cantidad FROM citasTaller WHERE (idcreador='$session->idusuario' OR idOpe='$session->idusuario') AND idestado='321'");
		return !empty($menuCitaRep)?array_shift($menuCitaRep):false;
	}
	
	public static function obtenerUltimaCitaRegistradaa(){
		global $session;
		$sqlULtim=self::sentencia("SELECT * FROM citasTaller CT WHERE '$session->idusuario' = (SELECT comercialOperaciones FROM usuarios U  WHERE U.idusuario = CT.idCreador) AND idestado='0' ORDER BY idcita DESC LIMIT 1 ");
		return !empty($sqlULtim)? array_shift($sqlULtim):false;
	}

	public static function obtenerCitasParaConfirmarRegistro(){
		global $session;
		$sqlULtim=self::sentencia("SELECT * FROM citasTaller CT WHERE $session->idusuario = (SELECT comercialOperaciones FROM usuarios U  WHERE U.idusuario = CT.idCreador) AND idestado='0'");
		return !empty($sqlULtim)? $sqlULtim:false;
	}

	public static function obtenerCitasParaConfirmarRegistroMenuLateral(){
		global $session; 
		$sqlULtim=self::sentencia("SELECT COUNT(*) as cantidad FROM citasTaller CT WHERE $session->idusuario = (SELECT comercialOperaciones FROM usuarios U  WHERE U.idusuario = CT.idCreador) AND idestado='0' LIMIT 1");
		return !empty($sqlULtim)? array_shift($sqlULtim):false;
	}

	public static function obtenerCitaPorId($idCita){
		global $session;
		$idcita=limpiar($idCita);
		$obtenerCitaPorIdQ=self::sentencia("SELECT * FROM citasTaller WHERE idcita='$idcita'");
		return !empty($obtenerCitaPorIdQ)? array_shift($obtenerCitaPorIdQ):false;
	}

	public static function asignarCitaSac($idcita){
		global $db;
		global $session;
		$idcita=$db->escapeString(limpiar($idcita));
		$sql="UPDATE citasTaller SET idSac='$session->idusuario' WHERE idcita='$idcita'";
		return ($db->query($sql))? true :false;
	}

	public function confirmarCitaOpe(){
		global $session;
		global $db;
		$this->fechaOpe=date("YmdHis");
		$sql="UPDATE citasTaller SET ";
		$sql.=" fecha='";
		$sql.=$db->escapeString($this->fecha)."', ";
		$sql.=" nombreCliente='";
		$sql.=$db->escapeString($this->nombreCliente)."', ";
		$sql.=" tiempo='";
		$sql.=$db->escapeString($this->tiempo)."', ";
		$sql.=" placa='";
		$sql.=$db->escapeString($this->placa)."', ";
		$sql.=" observacion='";
		$sql.=$db->escapeString($this->observacion)."', ";
		$sql.=" seguro='";
		$sql.=$db->escapeString($this->seguro)."', ";
		$sql.=" idestado='14', ";
		$sql.=" idOpe ='";
		$sql.=$db->escapeString($session->idusuario)."' ,";
		$sql.=" fechaOpe ='";
		$sql.=$db->escapeString($this->fechaOpe)."'";
		$sql.=" WHERE ";
		$sql.=" idcita ='";
		$sql.=$db->escapeString(limpiar($this->idcita))."'";
		return ($db->query($sql))?true:false;
	}

	public static function obtenerCitasSac(){
		$obcSac=self::sentencia("SELECT * FROM citasTaller WHERE idestado='14' ORDER BY fecha ASC");
		return !empty($obcSac)?$obcSac:false;
	}

	public function instalacionProgramada(){
		global $db;
		global $session;
		$this->fechaSac=date("YmdHis");
		$sql="UPDATE citasTaller SET ";
		$sql.="idSac='";
		$sql.=$db->escapeString($session->idusuario)."', ";
		$sql.="idestado='777', ";
		$sql.=" fechaSac='";
		$sql.=$db->escapeString($this->fechaSac)."' ";
		$sql.=" WHERE ";
		$sql.=" idcita='";
		$sql.=$db->escapeString($this->idcita)."'";
		return ($db->query($sql))?true:false;
	}
	public function guardarCitaComoEliminado(){
		global $db;
		global $session;
		$this->fechaOpe=date("YmdHis");
		$sql="UPDATE citasTaller SET ";
		$sql.=" observacion='";
		$sql.=$db->escapeString($this->observacion)."',";
		$sql.=" fechaOpe='";
		$sql.=$db->escapeString($this->fechaOpe)."',";
		$sql.=" idestado='75' ";
		$sql.=" WHERE idcita='";
		$sql.=$db->escapeString($this->idcita)."'";
		if($db->query($sql)){
		   $this->borrardeProgramacioAUTO()	;
			return true;
		}else{
			return false;
		}
//		return ($db->query($sql))?true:false;
	}
	public function actualizarDatosDeCita(){
		global $db;
		global $session;
		$sql="UPDATE citasTaller SET ";
		$sql.=" nombreCliente='";
		$sql.=$db->escapeString($this->nombreCliente)."', ";
/*		$sql.=" plan='";
		$sql.=$db->escapeString($this->plan)."', ";*/
		$sql.=" celular='";
		$sql.=$db->escapeString($this->celular)."', ";
		$sql.=" correo='";
		$sql.=$db->escapeString($this->correo)."', ";
		$sql.=" tiempo='";
		$sql.=$db->escapeString($this->tiempo)."', ";
		$sql.=" costo='";
		$sql.=$db->escapeString($this->costo)."', ";
		$sql.=" seguro='";
		$sql.=$db->escapeString($this->seguro)."', ";
		$sql.=" idestado='";
		$sql.=$db->escapeString($this->idestado)."', ";
		$sql.=" placa='";		
		$sql.=$db->escapeString($this->placa)."', ";
		$sql.=" idSac='0', ";		
		$sql.=" voucher='";
		$sql.=$db->escapeString($this->voucher)."', ";
		$sql.=" estadoPago='";
		$sql.=$db->escapeString($this->estadoPago)."', ";
		$sql.=" fecha='";
		$sql.=$db->escapeString($this->fecha)."', ";
		$sql.=" observacion='";
		$sql.=$db->escapeString($this->observacion)."' ";
		$sql.=" WHERE idcita='";
		$sql.=$db->escapeString($this->idcita)."'";
		return ($db->query($sql))?true:false;
	}

	public function agregarIdope(){
		global $db;
		$sqll="UPDATE citasTaller SET idopseracion='$this->idoperacion' WHERE idcita='$this->idcita'";	
		return ($db->query($sql))? true :false;
	}

	public function pendienteComCitaTotal(){
		$pendienteComTotal=self::sentencia("SELECT COUNT(*) as cantidad FROM citasTaller WHERE idestado='0' LIMIT 1");
		return !empty($pendienteComTotal)? array_shift($pendienteComTotal) :false;
	}

	public function pendienteComCitaTotalSeguimientoComRango($desde, $hasta){
		$pendienteComTotal=self::sentencia("SELECT COUNT(*) as cantidad FROM citasTaller WHERE fechaRegistro BETWEEN '$desde' AND '$hasta' LIMIT 1");
		return !empty($pendienteComTotal)? array_shift($pendienteComTotal) :false;
	}
	public function pendienteComCitaTotalSeguimientoCom(){
		$pendienteComTotal=self::sentencia("SELECT COUNT(*) as cantidad FROM citasTaller LIMIT 1");
		return !empty($pendienteComTotal)? array_shift($pendienteComTotal) :false;
	}


	public function pendienteComCitaConFecha($posisionDePagina,$registrosPorPagina,$desde,$hasta){
		$pendienteComCita=self::sentencia("SELECT * FROM citasTaller WHERE fechaRegistro BETWEEN '$desde' AND '$hasta' ORDER BY idcita DESC LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($pendienteComCita)? $pendienteComCita:false;		
	}
	public function pendienteComCitaSinFecha($posisionDePagina,$registrosPorPagina){
        $pendienteComCita=self::sentencia("SELECT * FROM citasTaller ORDER BY idcita DESC LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($pendienteComCita)? $pendienteComCita:false;		
	}

	public function pendienteComCita($posisionDePagina,$registrosPorPagina){
		$pendienteComCita=self::sentencia("SELECT * FROM citasTaller  WHERE  idestado='0' LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($pendienteComCita)? $pendienteComCita:false;		
	}
	public function pendienteComCitaReporteEXcel(){
		$pendienteComCita=self::sentencia("SELECT * FROM citasTaller  WHERE  idestado='0' or idestado='14' ");
		return !empty($pendienteComCita)? $pendienteComCita:false;		
	}

	public function pendienteComCitaFiltro($datos){
		$pendienteComCita=self::sentencia("SELECT * FROM citasTaller  WHERE  idestado='0' AND (nombreCliente LIKE '$datos' OR placa LIKE '$datos' OR observacion LIKE '$datos' OR plan LIKE '$datos') LIMIT 10 ");
		return !empty($pendienteComCita)? $pendienteComCita:false;		
	}

	public function seguimientoComCitaFiltroFecha($datos,$desde,$hasta){
		$pendienteComCita=self::sentencia("SELECT * FROM citasTaller  WHERE fechaRegistro BETWEEN '$desde' AND '$hasta' AND (nombreCliente LIKE '$datos' OR placa LIKE '$datos' OR observacion LIKE '$datos' OR plan LIKE '$datos') LIMIT 10 ");
		return !empty($pendienteComCita)? $pendienteComCita:false;		
	}
	public function seguimientoComCitaFiltro($datos){
		$pendienteComCita=self::sentencia("SELECT * FROM citasTaller  WHERE  (nombreCliente LIKE '$datos' OR placa LIKE '$datos' OR observacion LIKE '$datos' OR plan LIKE '$datos') LIMIT 10 ");
		return !empty($pendienteComCita)? $pendienteComCita:false;		
	}
	public static function marcarComoCitaOk($id){
		global $session;
		global $db;
		$sql="UPDATE citasTaller SET idestado = '14' where idcita='$id'";
		return ($db->query($sql))?true:false;
	} 
    public static function verificarExsitenciaPorOpeID($idoperacion){
        $sql=self::sentencia("SELECT * FROM citasTaller WHERE idoperacion='$idoperacion' LIMIT 1");
        return !empty($sql)?true:false;
    }
    public function guardarComoIncidencia(){
        global $db;
        global $session;
        $sqlGCI="INSERT INTO incidencias (idcita,fecha,idusuario,observacion,fechaRegistro)";
        $sqlGCI.=" VALUES ('";
        $sqlGCI.=$this->idcita."','";
        $sqlGCI.=$this->fecha."','";
        $sqlGCI.=$session->idusuario."','";
        $sqlGCI.=$this->obsSac."','";
        $sqlGCI.=date('YmdHis')."')";
        return ($db->query($sqlGCI))?true:false;
    }

    public function guardarComoIncidenciaAUTO(){
        global $db;
        $sqlGCI="INSERT INTO incidencias (idcita,fecha,idusuario,observacion,fechaRegistro)";
        $sqlGCI.=" VALUES ('";
        $sqlGCI.=$this->idcita."','";
        $sqlGCI.=$this->fecha."','";
        $sqlGCI.="0','";
        $sqlGCI.=$this->obsSac."','";
        $sqlGCI.=date('YmdHis')."')";
        return ($db->query($sqlGCI))?true:false;
    }
	
    public function obtenerCitasParaReprogramar(){
        $hoy=new DateTime();
        $hoy->modify('-45 minutes');
        $oCPR=self::sentencia("SELECT * FROM citasTaller WHERE idestado='14' AND DATE_FORMAT(fecha,'%Y-%m-%d %H:%i') < '".$hoy->format('Y-m-d H:i')."'");
        return ($oCPR)?$oCPR:false;
    }
    public function obtenerCitasMarcadasComoReprogramar(){
        $oCPR=self::sentencia("SELECT * FROM citasTaller WHERE idestado='14' AND DATE_FORMAT(fecha,'%Y-%m-%d %H:%i') < '".$hoy->format('Y-m-d H:i')."'");
        return ($oCPR)?$oCPR:false;
    }
    public function obtnerCitasEnEstadoReprogramar(){
        $trd=new DateTime();
        $trd->modify('-20 days');
        $oCPR=self::sentencia("SELECT * FROM citasTaller WHERE idestado='321' AND (DATE_FORMAT(fecha,'%Y%m%d') BETWEEN ".$trd->format('Ymd')." AND '".date('Ymd')."')");
        return ($oCPR)?$oCPR:false;
    }
    public function activarCitaQ(){
        global $db;
        $aCq="UPDATE citasTaller SET idestado='14' WHERE idcita='$this->idcita'";
        return ($db->query($aCq))?true:false;
    }
    public function ponerparaRcita(){
        global $db;
        $aCq="UPDATE citasTaller SET idestado='321' WHERE idcita='$this->idcita'";
        return ($db->query($aCq))?true:false;
    }

}


?>

<?php 

class Vehiculo extends Recursos
{
	public $idcliente;
	public $idvehiculo;
	public $plan;
	public $planTiempo;
	public $placa;
	public $chasis;
	public $motor;
	public $marca;
	public $modelo;
	public $color;
	public $anio;
	public $companiaSeguro;
	public $seguro;
	public $numeroTramiteSeguro;
	public $numeroPoliza;
	public $idAsignador;
	public $fechaCreacion;
	public $observaciones;
	public $idcreador;
	public $estado;
	public $idcorredordeseguro;
	public $corredor;
	public $razonSocial;
	public $cantidad;
	public $idUsuarioBienvenida;
	public $accionBienvenida;
	public $fechaBienvenida;
	public $serviciosAdicionales;
	public $concesionario;
	public $enOnix;
	public $idRegistradorEnOnix;
	public $fechaRegistradoEnOnix;
	public $idCargoB;
	public $idoperacion;
	public $campana;
	public $sendMail;
	public $tiempo;

	function __construct()
	{
		
	}
	public function registrarVehiculo($idcliente){
		global $db;
		global $session;
		$this->fechaCreacion=date("YmdHis");
		$id=htmlspecialchars(htmlspecialchars($db->escapeString($idcliente)));
		$sql="INSERT INTO vehiculos (idcliente,plan,planTiempo,placa,chasis,motor,marca,modelo,color,anio,companiaSeguro,concesionario,numeroTramiteSeguro,numeroPoliza,idcorredordeseguro,serviciosAdicionales,enOnix,fechaCreacion,idCreador,campana,estado) ";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString($idcliente)."' , '";
		$sql.= $db->escapeString($this->plan)."' , '";
		$sql.= $db->escapeString(ucfirst($this->planTiempo))."' , '";
		$sql.= $db->escapeString(strtoupper($this->placa))."' , '";
		$sql.= $db->escapeString($this->chasis)."' , '";
		$sql.= $db->escapeString($this->motor)."' , '";
		$sql.= $db->escapeString($this->marca)."' , '";
		$sql.= $db->escapeString($this->modelo)."' , '";
		$sql.= $db->escapeString($this->color)."' , '";
		$sql.= $db->escapeString($this->anio)."' , '";
		$sql.= $db->escapeString($this->companiaSeguro)."' , '";
		$sql.= $db->escapeString($this->concesionario)."' , '";
		$sql.= $db->escapeString($this->numeroTramiteSeguro)."' , '";
		$sql.= $db->escapeString($this->numeroPoliza)."' , '";
		$sql.= $db->escapeString($this->idcorredordeseguro)."' , '";
		$sql.= $db->escapeString($this->serviciosAdicionales)."' , '";
		$sql.= $db->escapeString($this->enOnix)."' , '";
		$sql.= $db->escapeString($this->fechaCreacion)."' , '";
		$sql.= $db->escapeString($session->idusuario)."' , '";
		$sql.= $db->escapeString($this->campana)."' , '";
		$sql.= $db->escapeString(1)."')";
		if ($db->query($sql)) {
				$this->idvehiculo=$db->ultimoIdInsertado();
				return true;
			}else{
				return false;
			}

		
	}
	public function registrarVehiculosDesdeCliente($datosVehiculo,$idcliente,$idcreador){
		$fechaRegistro=date("YmdHis");
		global $db;
		$placa = $datosVehiculo['placa_i'];
		$chasis = $datosVehiculo['chasis_i'];
		$motor = $datosVehiculo['motor_i'];
		$marca = $datosVehiculo['marca_i'];
		$modelo = $datosVehiculo['modelo_i'];
		$color = $datosVehiculo['color_i'];
		$anio = $datosVehiculo['anio_i'];
		$seguro = $datosVehiculo['seguro_i'];
		$numeroTramiteSeguro = $datosVehiculo['numeroTramiteSeguro_i'];
		$numeroPoliza = $datosVehiculo['numeroPoliza_i'];
		$corredor = $datosVehiculo['corVehiredor_i'];
		$concesionario = $datosVehiculo['concesionario_i'];
		$sql = "INSERT INTO vehiculosTemporal (idcliente, placa, chasis, motor, marca ,modelo, color, anio, seguro, concesionario , numeroTramiteSeguro, numeroPoliza ,corredor,fechaRegistro ,idcreador) values ('$idcliente','$placa','$chasis','$motor','$marca','$modelo','$color','$anio','$seguro','$concesionario','$numeroTramiteSeguro','$numeroPoliza','$corredor','$fechaRegistro','$idcreador' ) ";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
			echo "Fail detected";
		}
	}

	public function registrarVehiculosDedeVerificacion($idcliente,$datosVehiculo){
		$fecha=date("YmdHis");
		global $db;
		global $session;
		$placa = $datosVehiculo['placa_i'];
		$chasis=$datosVehiculo['chasis_i'];
		$motor=$datosVehiculo['motor_i'];
		$marca=$datosVehiculo['marca_i'];
		$modelo=$datosVehiculo['modelo_i'];
		$color=$datosVehiculo['color_i'];
		$anio=$datosVehiculo['anio_i'];
		$seguro=$datosVehiculo['seguro_i'];
		$concesionario=$datosVehiculo['concesionario_i'];
		$numeroTramiteSeguro=$datosVehiculo['numeroTramiteSeguro_i'];
		$numeroPoliza=$datosVehiculo['numeroPoliza_i'];
		$corredor=$datosVehiculo['corredor_i'];
		$campana=$datosVehiculo['campana_i'];

		$plan=$datosVehiculo['plan'];
		$tiempo=$datosVehiculo['tiempo'];


		$sql="INSERT INTO vehiculos (idcliente,plan,planTiempo,placa,chasis,motor,marca,modelo,color,anio,companiaSeguro,concesionario,numeroTramiteSeguro,numeroPoliza,idcorredordeseguro,campana,fechaCreacion,idcreador,estado) ";
		$sql.="VALUES ('";
		$sql.=$db->escapeString($idcliente)."','";
		$sql.=$db->escapeString($plan)."','";
		$sql.=$db->escapeString($tiempo)."','";
		$sql.=$db->escapeString($placa)."','";
		$sql.=$db->escapeString($chasis)."','";
		$sql.=$db->escapeString($motor)."','";
		$sql.=$db->escapeString($marca)."','";
		$sql.=$db->escapeString($modelo)."','";
		$sql.=$db->escapeString($color)."','";
		$sql.=$db->escapeString($anio)."','";
		$sql.=$db->escapeString($seguro)."','";
		$sql.=$db->escapeString($concesionario)."','";
		$sql.=$db->escapeString($numeroTramiteSeguro)."','";
		$sql.=$db->escapeString($numeroPoliza)."','";
		$sql.=$db->escapeString($corredor)."','";
		$sql.=$db->escapeString($campana)."','";
		$sql.=$db->escapeString($fecha)."','";
		$sql.=$db->escapeString($session->idusuario)."','";
		$sql.=$db->escapeString(1)."')";
		if ($db->query($sql)) {
			$this->idvehiculo=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
			echo " the fail 2";
		}
	}

	public function obtenerVehiculosConPlan($plan){
		global $db;
		$counter = 0;
		$vhObj = self::sentencia("SELECT * FROM vehiculos WHERE plan = '$plan' ORDER BY idvehiculo DESC;");
		if($vhObj){
			foreach($vhObj as $var):
				$counter++;
				?>
			<tr class="gradeX">
		        <td><?php echo $counter ?></td>
		        <td><?php echo $var->placa ?>
		        </td>
		        <td><?php echo $var->marca ?></td>
		        <td class="center"><?php echo $var->modelo ?></td>
		        <td class="center"><?php echo obtenerEstado($var->estado)  ?></td>
		        <td class="center"><?php echo $var->plan ?></td>
		        <td class="center"><?php echo $var->planTiempo ?></td>
		        <td class="center"><?php echo $var->chasis ?></td>
		        <td class="center"><?php echo $var->motor ?></td>
		        <td class="center"><?php echo $var->color ?></td>
		        <td class="center"><?php echo $var->anio ?></td>
		        <td class="center"><?php echo $var->companiaSeguro ?></td>
		        <td class="center"><?php echo $var->numeroPoliza ?></td>
		        <td class="center"><?php echo $var->idcorredordeseguro ?></td>
		    </tr>
				<?php
			endforeach;			
			return true;
		} else {
			return false;
		}
	}

	public function obtenerVehiculos($idcliente){
		global $db;
		$n=0;
		$id=htmlspecialchars(htmlspecialchars($db->escapeString($idcliente)));
		$vhObj=self::sentencia("SELECT * FROM vehiculos WHERE idcliente='$id' ORDER BY idvehiculo DESC ");
		if ($vhObj) {
			foreach ($vhObj as $var) :
				$n++;
				?>
			<tr class="gradeX">
		        <td><?php echo $n ?></td>
		        <td><?php echo $var->placa ?>
		        </td>
		        <td><?php echo $var->marca ?></td>
		        <td class="center"><?php echo $var->modelo ?></td>
		        <td class="center"><?php echo obtenerEstado($var->estado)  ?></td>
		        <td class="center"><?php echo $var->plan ?></td>
		        <td class="center"><?php echo $var->planTiempo ?></td>
		        <td class="center"><?php echo $var->chasis ?></td>
		        <td class="center"><?php echo $var->motor ?></td>
		        <td class="center"><?php echo $var->color ?></td>
		        <td class="center"><?php echo $var->anio ?></td>
		        <td class="center"><?php echo $var->companiaSeguro ?></td>
		        <td class="center"><?php echo $var->numeroPoliza ?></td>
		        <td class="center"><?php echo $var->idcorredordeseguro ?></td>
		    </tr>
				<?php
			endforeach;			
			return true;
		} else {
			return false;
		}
		
}

public static function obtenerVehiculo($idvehiculo){
		$obtenerVehiculoQ=self::sentencia("SELECT * FROM vehiculos WHERE idvehiculo = '$idvehiculo' LIMIT 1");
		return  !empty($obtenerVehiculoQ) ? array_shift($obtenerVehiculoQ) : false ;
}

public static function obtenerClienteTemporal($idclieteTemporal){
	return $obtenerClienteTemporalQ=self::sentenciaPublic("SELECT * FROM vehiculosTemporal WHERE 	idcliente = '$idclieteTemporal' ");
	
}
public static function obtenerClientePorIdCliente($idcliete,$idvehiculo){
	return $obtenerClientePorIdCliente=self::sentencia("SELECT * FROM vehiculos WHERE 	idcliente = '$idcliete' AND idvehiculo='$idvehiculo'");
}



public function verificarExistenciaVehiculos($idcliente){
		global $db;
		$id=htmlspecialchars(htmlspecialchars($db->escapeString($idcliente)));
		$vhObj=self::sentencia("SELECT idcliente FROM vehiculos WHERE idcliente='$id'");
		if ($vhObj) {
			return true;
		} else {
			return false;
		}
		
}	
public function verificarCantidadVehiculos($idcliente){
		global $db;
		$id=htmlspecialchars(htmlspecialchars($db->escapeString($idcliente)));
		$vhObj=self::sentencia("SELECT idcliente FROM vehiculos WHERE idcliente='$id'");
		if ($vhObj) {
			$n=0;
			foreach ($vhObj as $var) {
				$n++;
				$this->cantidad=$n;
			}
				echo $n;
			return true;
		} else {
			return false;
		}
		
}	

	public function bienvenida($idvehiculo){
		global $db;
		global $session;
		$this->fechaBienvenida=date("YmdHis");
		$this->idUsuarioBienvenida=$session->idusuario;
		$sql="UPDATE vehiculos SET ";
		$sql.="idUsuarioBienvenida = '".$db->escapeString($this->idUsuarioBienvenida)."',";
		$sql.="accionBienvenida = '".$db->escapeString($this->accionBienvenida)."',";
		$sql.="fechaBienvenida = '".$db->escapeString($this->fechaBienvenida)."',";
		$sql.="estado =4 ";
		$sql.=" WHERE idvehiculo = '$idvehiculo' ";
		if ($db->query($sql)) {
			# code...
		}else{
			echo "the fail";
		}
	}


    public function updateEstadoVehiculo($idvehiculo,$estado){
        global $db;
        global $session;
        $this->fechaBienvenida=date("YmdHis");
        $this->idUsuarioBienvenida=$session->idusuario;
        $sql="UPDATE vehiculos SET ";
        $sql.="estado ='$estado'";
        $sql.=" WHERE idvehiculo = '$idvehiculo' ";
        if ($db->query($sql)) {
            # code...
        }else{
            echo "the fail";
        }
    }

	public function bienvenidaMail($idvehiculo , $estado){
		global $db;
		global $session;
		$this->fechaBienvenida=date("YmdHis");
		$this->idUsuarioBienvenida=$session->idusuario;
		$sql="UPDATE vehiculos SET ";
		$sql.="sendMail = '".$estado."'";
		$sql.=" WHERE idvehiculo = '$idvehiculo' ";
		if ($db->query($sql)) {
			# code...
		}else{
			echo "the fail";
		}
	}

	public function pendienteBienvenida($idvehiculo){
		global $db;
		global $session;
		$this->fechaBienvenida=date("YmdHis");
		$this->idUsuarioBienvenida=$session->idusuario;
		$sql="UPDATE vehiculos SET ";
		$sql.="idUsuarioBienvenida = '".$db->escapeString($this->idUsuarioBienvenida)."',";
		$sql.="accionBienvenida = '".$db->escapeString($this->accionBienvenida)."',";
		$sql.="fechaBienvenida = '".$db->escapeString($this->fechaBienvenida)."' ";
		$sql.=" WHERE idvehiculo = '$idvehiculo' ";
		if ($db->query($sql)) {
			# code...
		}else{
			echo "the fail";
		}
	}

	public static function obtenerCorredores(){
		global $db;
		$vehiculoObj=self::sentencia("SELECT razonSocial FROM corredorSeguros");
		return !empty($vehiculoObj) ? $vehiculoObj:false;
	}
	public static function obtenerCorredorPorId($idCorredor){
		$obtenerCorredorPorIdQ=self::sentencia("SELECT razonSocial FROM corredorSeguros WHERE idcorredordeseguro='$idCorredor' LIMIT 1");
		return  !empty($obtenerCorredorPorIdQ) ? array_shift($obtenerCorredorPorIdQ) : false ;
	}

	public function obtenerVehiculosParaRegistrarEnOnix(){
		global $session;
		$obtenerVehiculosParaRegistrarEnOnix=self::sentencia("SELECT O.idoperacion,V.plan,V.idcliente, V.estado, V.chasis, V.idcreador, O.fechaCreacion, O.idvehiculo, O.datosfacturacionid FROM operaciones O INNER JOIN vehiculos V ON(O.idvehiculo=V.idvehiculo AND V.idcliente=O.idcliente AND V.enOnix='' AND $session->idusuario=(SELECT comercialOperaciones FROM usuarios U  WHERE U.idusuario = V.idCreador)) AND O.idChequeo=0 AND O.estado<>'18' ");
		return !empty($obtenerVehiculosParaRegistrarEnOnix)?$obtenerVehiculosParaRegistrarEnOnix:false;
		// SELECT * FROM vehiculos V WHERE enOnix='' AND $session->idusuario=(SELECT comercialOperaciones FROM usuarios U  WHERE U.idusuario = V.idCreador )
	}
	public function obtenerVehiculosParaRegistrarEnOnixAdmin(){
		global $session;
		$obtenerVehiculosParaRegistrarEnOnix=self::sentencia("SELECT O.idoperacion,V.plan,V.idcliente, V.estado, V.chasis, V.idcreador, O.fechaCreacion, O.idvehiculo, O.datosfacturacionid FROM operaciones O INNER JOIN vehiculos V ON(O.idvehiculo=V.idvehiculo AND V.idcliente=O.idcliente AND V.enOnix='') AND O.idChequeo=0 AND O.estado<>'18' ");
		return !empty($obtenerVehiculosParaRegistrarEnOnix)?$obtenerVehiculosParaRegistrarEnOnix:false;
		// SELECT * FROM vehiculos V WHERE enOnix='' AND $session->idusuario=(SELECT comercialOperaciones FROM usuarios U  WHERE U.idusuario = V.idCreador )
	}
	public static function obtenerVehiculosParaRegistrarEnOnixUltimoRegistroParaNotifi(){
		global $session;
		$obtenerVehiculosParaRegistrarEnOnix=self::sentencia("SELECT O.idoperacion,V.plan,V.idcliente, V.estado, V.chasis, V.idcreador, O.fechaCreacion, O.idvehiculo, O.datosfacturacionid FROM operaciones O INNER JOIN vehiculos V ON(O.idvehiculo=V.idvehiculo AND V.idcliente=O.idcliente AND V.enOnix='' AND $session->idusuario=(SELECT comercialOperaciones FROM usuarios U  WHERE U.idusuario = V.idCreador)) AND O.idChequeo=0  ORDER BY idoperacion DESC LIMIT 1");
		return !empty($obtenerVehiculosParaRegistrarEnOnix)?array_shift($obtenerVehiculosParaRegistrarEnOnix):false;
		// SELECT * FROM vehiculos V WHERE enOnix='' AND $session->idusuario=(SELECT comercialOperaciones FROM usuarios U  WHERE U.idusuario = V.idCreador )
	}

	public static function obtenerVehiculosParaRegistrarEnOnixCantidad(){
		global $session;
		$obtenerVehiculosParaRegistrarEnOnixCantidad=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones O INNER JOIN vehiculos V ON(O.idvehiculo=V.idvehiculo AND V.idcliente=O.idcliente AND V.enOnix='' AND $session->idusuario=(SELECT comercialOperaciones FROM usuarios U  WHERE U.idusuario = V.idCreador)) AND O.idChequeo=0  AND O.estado<>'18' LIMIT 1");
		return !empty($obtenerVehiculosParaRegistrarEnOnixCantidad)? array_shift($obtenerVehiculosParaRegistrarEnOnixCantidad):false;
	}

	public function confirmarRegistroEnOnix(){
		global $db;
		global $session;
		$this->fechaRegistradoEnOnix=date("YmdHis");
		$sql="UPDATE vehiculos SET";
		$sql.=" enOnix='";
		$sql.=$db->escapeString('si')."', ";
		$sql.=" fechaRegistradoEnOnix='";
		$sql.=$db->escapeString($this->fechaRegistradoEnOnix)."', ";
		$sql.=" campana='";
		$sql.=$db->escapeString($this->campana)."', ";
		$sql.=" idRegistradorEnOnix='";
		$sql.=$db->escapeString($session->idusuario)."' WHERE ";
		$sql.="idvehiculo='".$db->escapeString($this->idvehiculo)."'";
		return ($db->query($sql))?true:false;
	}

	public static function obtenerVehiculoParaBienvenida(){
		$sqlObtener=self::sentencia("SELECT * FROM vehiculos WHERE estado ='3'");
		return !empty($sqlObtener)?$sqlObtener:false;
	}
	public function asignarCargoDeBienvenida(){
		global $db;
		global $session;
		$sql="UPDATE vehiculos SET ";
		$sql.="idCargoB='";
		$sql.=$db->escapeString($session->idusuario)."' ";
		$sql.=" WHERE idvehiculo=";
		$sql.=$db->escapeString($this->idvehiculo);
		return ($db->query($sql)) ? true:false;

	}
	public static function cambiarEstado($estado){
		global $db;
		$sql="UPDATE vehiculos SET ";
		$sql.=" estado='2' ";
		$sql.=" WHERE idvehiculo='";
		$sql.=$estado."'";
		return ($db->query($sql))?true:false;
	}
	public static function obtenerInstalados(){
		global $session;
		$sqlSen=self::sentencia("SELECT * FROM vehiculos WHERE estado ='4' AND idUsuarioBienvenida='$session->idusuario'  ORDER BY fechaBienvenida DESC LIMIT 90 ");
		return !empty($sqlSen)?$sqlSen:false;
	}
	public static function verificarSiEstaDuplicado($chasis){
        $chasis=limpiar($chasis);
        $chasis="%".$chasis."%";
        $sqlSen=self::sentencia("SELECT COUNT(*) as cantidad FROM vehiculos WHERE chasis LIKE '$chasis' LIMIT 1"); 
        return !empty($sqlSen)?array_shift($sqlSen):false;
	}


	public static function obtenerVehiculosbyid($idvehiculo){
        $idvehiculo=limpiar($idvehiculo);
        $sqlSen=self::sentencia("SELECT * FROM vehiculos WHERE idvehiculo ='$idvehiculo' "); 
        return !empty($sqlSen)?array_shift($sqlSen):false;
	}

	public static function obtenerVehiculosConElMismoChasis($chasis){
        $chasis=limpiar($chasis);
        $chasis="%".$chasis."%";
        $sqlSen=self::sentencia("SELECT * FROM vehiculos WHERE chasis LIKE '$chasis'order by fechaRegistradoEnOnix desc ");
        return !empty($sqlSen)?$sqlSen:false;
	}

    public static function obtenerVehiculosConElMismoChasisOP($chasis){
        $chasis=limpiar($chasis);
        $chasis="".$chasis."";
        $sqlSen=self::sentencia("SELECT * FROM vehiculos WHERE chasis='$chasis' order by fechaRegistradoEnOnix desc ");
        return !empty($sqlSen)?$sqlSen:false;
    }

    public function guardarComoEliminado($ultimoid,$idopera){
        global $db;
        $datosOld=new SeguimientoConsulta();
        $dOld=$datosOld->consultaSeg("vehiculos",$this->idvehiculo,"idvehiculo");
        $dOldd=$dOld;
        $sql="UPDATE vehiculos SET estado='75' , observaciones='$this->observaciones' WHERE idvehiculo='$this->idvehiculo'";
        if($db->query($sql)){
            new SeguimientoTablas("vehiculos"," Modifico estado como Eliminado",$this->idvehiculo,$this,"idvehiculo",$dOldd,$ultimoid,$idopera);
           return true ;
        }else{
            return false;
        }
//        return ($db->query($sql))?true:false;
    }

    public static function guardarComoinstalado($idvehiculo){
        global $db; 
        $sql="UPDATE vehiculos SET estado='3' , observaciones='Agreado por resolución de seguimiento' WHERE idvehiculo='$idvehiculo'";
        return ($db->query($sql))?true:false;
    }


    public function actulizarDatosDeVehiculoDeOnyx(){
        global $db;
        $sqlAcA="UPDATE vehiculos SET placa='$this->placa', chasis='$this->chasis', motor='$this->motor', marca='$this->marca', modelo ='$this->modelo' , color ='$this->color' , anio ='$this->anio'  WHERE idvehiculo = '$this->idvehiculo'";
        return ($db->query($sqlAcA))?true:false;
    }

    public function actulizarDatosDeVehiculoDeAdmin($ultimoid,$idopera){
        global $db;
        $datosOld=new SeguimientoConsulta();
        $dold=$datosOld->consultaSeg("vehiculos",$this->idvehiculo,"idvehiculo");
        $doldd=$dold;
        $sqlAcA="UPDATE vehiculos SET placa='$this->placa', chasis='$this->chasis', motor='$this->motor', marca='$this->marca', modelo ='$this->modelo' , color ='$this->color' , anio ='$this->anio' , companiaSeguro='$this->companiaSeguro', concesionario='$this->concesionario' , campana ='$this->campana', planTiempo='$this->planTiempo', plan='$this->plan'  WHERE idvehiculo = '$this->idvehiculo'";
        if($db->query($sqlAcA)){
            new SeguimientoTablas("vehiculos"," Modifico Datos de Vehiculos desde Admin",$this->idvehiculo,$this,"idvehiculo",$doldd,$ultimoid,$idopera);
            return true ;
        }else{
            return false;
        }
    //    return ($db->query($sqlAcA))?true:false;
    }
    public function cambiarEstadoInstaladorPeroNoreporta(){
        global $db;
        $sqlA="UPDATE vehiculos SET estado='99' WHERE idvehiculo='$this->idvehiculo' ";
        return ($db->query($sqlA))?true:false;
    }
    public function cambiarEstadoInstaladosinbienvenida(){
        global $db;
        $sqlA="UPDATE vehiculos SET estado='3', observaciones ='$this->observaciones' WHERE idvehiculo='$this->idvehiculo' ";
        return ($db->query($sqlA))?true:false;
    }
} ?>

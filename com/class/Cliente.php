<?php 
class Cliente extends Recursos
{	
	public $idcliente;
	public $cotizacionid;
	public $idcontactoautorizado;
	public $departamento;
	public $provincia;
	public $distrito;
	public $plan;
	public $idtipo;
	public $tipo;
	public $tipoCliente;
	public $dni;
	public $ruc;
	public $nombre;
	public $razonSocial;
	public $apellidosC;
	public $observaciones;
	public $direccion;
	public $referencia;
	public $departamentoL;
	public $provinciaL;
	public $distritoL;
	public $direccionL;
	public $telefono;
	public $telefono1;
	public $telefono2;
	public $telefono3;
	public $correo;
	public $correo1;
	public $correo2;
	public $correo3;
	public $sectorDesempenia;
	public $idCreador;
	public $fechaCreada;
	public $parentesco;
	public $apellidos;
	public $estado;
	public $idclienteTemporal;
	public $idclientetemporal;
	public $tipoDocumentoFacturacion;
	public $rucFacturacion;
	public $razonSocialFacturacion;
	public $direccionFiscalFacturacion;
	public $telefonoFacturacion;
	public $fechaNacimiento;
	public $cantidad;
	public $fechaActualizacion;
	public $idactualizador;
	public $direccionEntrega;
	public $enOnix;
	public $idRegistradorEnOnix;
	public $enOnixA;
	public $fechaRegistradoEnOnix;
	public $referenciaEntrega;
	public $fechaHoraEntrega;
	public $distritoEntrega;
	public $recibidoPor;
	public $entregaEnInstalacion;
	public $descripcion;
	public $iddepartamento;
	public $idprovincia;
	public $iddistrito;
	public $idtec;
	public $tipocliente;
	
	function __construct()
	{
		# code...
	}

	public static function obtenerDatosDeclientePorId($id){
		global $db;
		$idd=htmlspecialchars(htmlentities($id));
		$obtenerDatosDeclientePorIdQ=self::sentencia("SELECT * FROM clientes WHERE idcliente='".$db->escapeString($idd)."' LIMIT 1");
		return !empty($obtenerDatosDeclientePorIdQ) ? array_shift($obtenerDatosDeclientePorIdQ):false; 
	}	

	public static function obtenerDatosDeClienteDesdeCotizacion($idcotizacion){
		// global $dbp;
		$idcotizacion=limpiar($idcotizacion);
		$obtenerDatosDeClienteDesdeCotizacionQ=self::sentenciaPublic("SELECT * FROM clientesTemporal WHERE cotizacionid='$idcotizacion' LIMIT 1");
		return !empty($obtenerDatosDeClienteDesdeCotizacionQ) ? array_shift($obtenerDatosDeClienteDesdeCotizacionQ):false; 
	}




	public static function obtenerDatosDeclienteTemporalPorDni($dni){
		global $db;
		$idd=htmlspecialchars(htmlentities($dni));
		$obtenerDatosDeclientePorIdQ=self::sentencia("SELECT * FROM clientesTemporal WHERE dni='".$db->escapeString($idd)."' LIMIT 1");
		return !empty($obtenerDatosDeclientePorIdQ) ? array_shift($obtenerDatosDeclientePorIdQ):false; 
	}
	public static function obtenerDatosDeclienteTemporalPorRuc($ruc){
		global $db;
		$rucc=htmlspecialchars(htmlentities($ruc));
		$obtenerDatosDeclienteTemporalPorRucQ=self::sentencia("SELECT * FROM clientesTemporal WHERE ruc='".$db->escapeString($rucc)."' LIMIT 1");
		return !empty($obtenerDatosDeclienteTemporalPorRucQ) ? array_shift($obtenerDatosDeclienteTemporalPorRucQ):false; 
	}

	public static function verificarExistenciaDNIoRUC($dniOruc){
		global $db;
		$vehiculoObj= new Vehiculo();
		$n=0;
		$existe=self::sentencia("SELECT * FROM clientes WHERE dni='".$dniOruc."' or ruc='".$dniOruc."'");
		if ($existe) {
			# code...
		foreach ($existe as $var) :
		$n++;
	?>
	<tr>
       <td><?php echo $n ?></td>
        <td><?php echo $retVal = ($var->tipoCliente=="Natural") ? $var->dni : $var->ruc ; ?></td>
        <td><?php echo $retVal1 = ($var->tipoCliente=="Natural") ? $var->nombre." ".$var->apellidos : $var->razonSocial ; ?></td>
        <td><?php echo $var->tipoCliente ?></td>
        <td><?php echo $retVal = ($vehiculoObj->verificarCantidadVehiculos($var->idcliente)) ? "" : "Sin vehiculo aun" ;
         ?></td><!-- falta -->
        <td><a href="registrarVehiculo.php?id=<?php echo $var->idcliente ?>">Elegir</a></td>
    </tr>
   
	<?php
			return true;
		endforeach;
		}else{
			return false;
		}
		
	}

// ---------------------------------------------




public static function obtenerClientePorVehiculo($idvehiculo,$idcliente){
		$obtenerClientePorVehiculoQ=self::sentencia("SELECT * FROM vehiculos INNER JOIN clientes ON vehiculos.idvehiculo='$idvehiculo' AND clientes.idcliente='$idcliente' LIMIT 1");
		return  !empty($obtenerClientePorVehiculoQ) ? array_shift($obtenerClientePorVehiculoQ) : false ;
}



	// ------------------------------------------


	// ----------------------------------------------
	public function registrarClienteNatural(){
		$this->fechaCreada=date("YmdHis");
		global $db;
		global $session;
		$sql="INSERT INTO clientes (tipocliente,dni,nombre,apellidosC,departamento,provincia,distrito,direccion,referencia,telefono,telefono1,correo ,correo1 ,fechaNacimiento,enOnix,observaciones,idCreador,fechaCreada) ";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString(ucfirst($this->tipoCliente))."' , '";
		$sql.= $db->escapeString(strtoupper($this->dni))."' , '";
		$sql.= $db->escapeString(htmlspecialchars($this->nombre))."' , '";
		$sql.= $db->escapeString(htmlspecialchars($this->apellidosC))."' , '";
		$sql.= $db->escapeString($this->departamento)."' , '";
		$sql.= $db->escapeString($this->provincia)."' , '";
		$sql.= $db->escapeString($this->distrito)."' , '";
		$sql.= $db->escapeString(htmlspecialchars($this->direccion))."' , '";
		$sql.= $db->escapeString($this->referencia)."' , '";
		$sql.= $db->escapeString($this->telefono)."' , '";
		$sql.= $db->escapeString($this->telefono1)."' , '";
		$sql.= $db->escapeString($this->correo)."' , '";
		$sql.= $db->escapeString($this->correo1)."' , '";
		$sql.= $db->escapeString($this->fechaNacimiento)."' , '";
		$sql.= $db->escapeString($this->enOnix)."' , '";
		$sql.= $db->escapeString(htmlspecialchars($this->observaciones))."' , '";
		$sql.= $db->escapeString($session->idusuario)."' , '";
		$sql.= $db->escapeString($this->fechaCreada)."')";
		if ($db->query($sql)) {
			 
			$this->idcliente=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}


		
	}
	public function registrarClienteNaturalTemporal(){
		$this->fechaCreada=date("YmdHis");
		global $db;
		$sql="INSERT INTO clientesTemporal (cotizacionid,tipocliente,dni,nombre,apellidosC,departamento,provincia,distrito,direccion,referencia,telefono,telefono1,correo,correo1,fechaNacimiento,tipoDocumentoFacturacion,rucFacturacion,razonSocialFacturacion,direccionFiscalFacturacion,telefonoFacturacion,direccionEntrega,entregaEnInstalacion,referenciaEntrega,distritoEntrega,fechaHoraEntrega,recibidoPor,idCreador,fechaCreada) ";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString($this->cotizacionid)."' , '";
		$sql.= $db->escapeString(ucfirst($this->tipoCliente))."' , '";
		$sql.= $db->escapeString(strtoupper($this->dni))."' , '";
		$sql.= $db->escapeString($this->nombre)."' , '";
		$sql.= $db->escapeString($this->apellidosC)."' , '";
		$sql.= $db->escapeString($this->departamento)."' , '";
		$sql.= $db->escapeString($this->provincia)."' , '";
		$sql.= $db->escapeString($this->distrito)."' , '";
		$sql.= $db->escapeString($this->direccion)."' , '";
		$sql.= $db->escapeString($this->referencia)."' , '";
		$sql.= $db->escapeString($this->telefono)."' , '";
		$sql.= $db->escapeString($this->telefono1)."' , '";
		$sql.= $db->escapeString($this->correo)."' , '";
		$sql.= $db->escapeString($this->correo1)."' , '";
		$sql.= $db->escapeString($this->fechaNacimiento)."' , '";
		$sql.= $db->escapeString($this->tipoDocumentoFacturacion)."' , '";
		$sql.= $db->escapeString($this->rucFacturacion)."' , '";
		$sql.= $db->escapeString($this->razonSocialFacturacion)."' , '";
		$sql.= $db->escapeString($this->direccionFiscalFacturacion)."' , '";
		$sql.= $db->escapeString($this->telefonoFacturacion)."' , '";
		$sql.= $db->escapeString($this->direccionEntrega)."' , '";
		$sql.= $db->escapeString($this->entregaEnInstalacion)."' , '";
		$sql.= $db->escapeString($this->referenciaEntrega)."' , '";
		$sql.= $db->escapeString($this->distritoEntrega)."' , '";
		$sql.= $db->escapeString($this->fechaHoraEntrega)."' , '";
		$sql.= $db->escapeString($this->recibidoPor)."' , '";
		$sql.= $db->escapeString($this->idCreador)."' , '";
		$sql.= $db->escapeString($this->fechaCreada)."')";
		if ($db->query($sql)) {
			$this->idclienteTemporal=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}


		
	}
	
	public function registrarClienteEmpresa(){
		$this->fechaCreada=date("YmdHis");
		global $db;
		global $session;
		$sql="INSERT INTO clientes (tipocliente,dni,ruc,razonSocial,nombre,apellidosC,departamento,provincia,distrito,direccion,referencia,departamentoL,provinciaL,distritoL,direccionL,telefono,telefono1,telefono2,telefono3,correo, correo1 ,correo2, correo3, fechaNacimiento,enOnix,sectorDesempenia,idCreador,fechaCreada)";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString(ucfirst($this->tipoCliente))."' , '";
		$sql.= $db->escapeString(ucfirst($this->dni))."' , '";
		$sql.= $db->escapeString(strtoupper($this->ruc))."' , '";
		$sql.= $db->escapeString(htmlspecialchars($this->razonSocial))."' , '";
		$sql.= $db->escapeString(htmlspecialchars($this->nombre))."' , '";
		$sql.= $db->escapeString($this->apellidosC)."' , '";
		$sql.= $db->escapeString($this->departamento)."' , '";
		$sql.= $db->escapeString($this->provincia)."' , '";
		$sql.= $db->escapeString($this->distrito)."' , '";
		$sql.= $db->escapeString(htmlspecialchars($this->direccion))."' , '";
		$sql.= $db->escapeString($this->referencia)."' , '";
		$sql.= $db->escapeString($this->departamentoL)."' , '";
		$sql.= $db->escapeString($this->provinciaL)."' , '";
		$sql.= $db->escapeString($this->distritoL)."' , '";
		$sql.= $db->escapeString($this->direccionL)."' , '";
		$sql.= $db->escapeString($this->telefono)."' , '";
		$sql.= $db->escapeString($this->telefono1)."' , '";
		$sql.= $db->escapeString($this->telefono2)."' , '";
		$sql.= $db->escapeString($this->telefono3)."' , '";
		$sql.= $db->escapeString($this->correo)."' , '";
		$sql.= $db->escapeString($this->correo1)."' , '";
		$sql.= $db->escapeString($this->correo2)."' , '";
		$sql.= $db->escapeString($this->correo3)."' , '";
		$sql.= $db->escapeString($this->fechaNacimiento)."' , '";
		$sql.= $db->escapeString($this->enOnix)."' , '";
		$sql.= $db->escapeString($this->sectorDesempenia)."' , '";
		$sql.= $db->escapeString($session->idusuario)."' , '";
		$sql.= $db->escapeString($this->fechaCreada)."')";
		if ($db->query($sql)) {
			$this->idcliente=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}
	}

	public function registrarClienteEmpresaTemporal(){
		$this->fechaCreada=date("YmdHis");
		global $db;
		$sql="INSERT INTO clientesTemporal (cotizacionid, tipocliente,dni,ruc,razonSocial,nombre,apellidosC,departamento,provincia,distrito,direccion,departamentoL,provinciaL,distritoL,direccionL,telefono,telefono1,telefono2,telefono3,correo,correo1,correo2, correo3, fechaNacimiento,tipoDocumentoFacturacion,rucFacturacion,razonSocialFacturacion,direccionFiscalFacturacion,telefonoFacturacion,direccionEntrega,entregaEnInstalacion,referenciaEntrega,distritoEntrega,fechaHoraEntrega,recibidoPor, sectorDesempenia,idCreador,fechaCreada)";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString($this->cotizacionid)."' , '";
		$sql.= $db->escapeString(ucfirst($this->tipoCliente))."' , '";
		$sql.= $db->escapeString(ucfirst($this->dni))."' , '";
		$sql.= $db->escapeString(strtoupper($this->ruc))."' , '";
		$sql.= $db->escapeString($this->razonSocial)."' , '";
		$sql.= $db->escapeString($this->nombre)."' , '";
		$sql.= $db->escapeString($this->apellidosC)."' , '";
		$sql.= $db->escapeString($this->departamento)."' , '";
		$sql.= $db->escapeString($this->provincia)."' , '";
		$sql.= $db->escapeString($this->distrito)."' , '";
		$sql.= $db->escapeString($this->direccion)."' , '";
		$sql.= $db->escapeString($this->departamentoL)."' , '";
		$sql.= $db->escapeString($this->provinciaL)."' , '";
		$sql.= $db->escapeString($this->distritoL)."' , '";
		$sql.= $db->escapeString($this->direccionL)."' , '";
		$sql.= $db->escapeString($this->telefono)."' , '";
		$sql.= $db->escapeString($this->telefono1)."' , '";
		$sql.= $db->escapeString($this->telefono2)."' , '";
		$sql.= $db->escapeString($this->telefono3)."' , '";
		$sql.= $db->escapeString($this->correo)."' , '";
		$sql.= $db->escapeString($this->correo1)."' , '";
		$sql.= $db->escapeString($this->correo2)."' , '";
		$sql.= $db->escapeString($this->correo3)."' , '";		
		$sql.= $db->escapeString($this->fechaNacimiento)."' , '";
		$sql.= $db->escapeString($this->tipoDocumentoFacturacion)."' , '";
		$sql.= $db->escapeString($this->rucFacturacion)."' , '";
		$sql.= $db->escapeString($this->razonSocialFacturacion)."' , '";
		$sql.= $db->escapeString($this->direccionFiscalFacturacion)."' , '";
		$sql.= $db->escapeString($this->telefonoFacturacion)."' , '";
		$sql.= $db->escapeString($this->direccionEntrega)."' , '";
		$sql.= $db->escapeString($this->sectorDesempenia)."' , '";
		$sql.= $db->escapeString($this->entregaEnInstalacion)."' , '";
		$sql.= $db->escapeString($this->referenciaEntrega)."' , '";
		$sql.= $db->escapeString($this->distritoEntrega)."' , '";
		$sql.= $db->escapeString($this->fechaHoraEntrega)."' , '";
		$sql.= $db->escapeString($this->recibidoPor)."' , '";
		$sql.= $db->escapeString($this->idCreador)."' , '";
		$sql.= $db->escapeString($this->fechaCreada)."')";
	if ($db->query($sql)) {
				$this->idclienteTemporal=$db->ultimoIdInsertado();
				return true;
		}else{
			return false;
		}
	}

// -----------------------LOS CONTACTOS AUTORIZXADOS HERE--------------------------------------------

	public function registrarContactoAutorizado($datosDeContactoAutorizado,$id){
		$this->horaRegistro=date("YmdHis");
		global $db;
		$dni = $datosDeContactoAutorizado['dni_i'];
		$nombre = $datosDeContactoAutorizado['nombre_i'];
		$apellidos = $datosDeContactoAutorizado['apellidos_i'];
		$celular = $datosDeContactoAutorizado['celular_i'];
		$telefono = $datosDeContactoAutorizado['telefono_i'];
		$correo = $datosDeContactoAutorizado['correo_i'];
		$parentesco = $datosDeContactoAutorizado['parentesco_i'];
		$sql = "INSERT INTO contactosAutorizados (idcliente, dni, nombre,apellidos,telefono,telefono1,correo,parentesco) values ('$id','$dni','$nombre','$apellidos','$celular','$telefono','$correo','$parentesco') ";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
			echo "Fail detected";
		}
	}
	public function registrarContactoAutorizadoTemporal($datosDeContactoAutorizado,$id){
		$fecha=date("YmdHis");
		global $db;
		$dni = $datosDeContactoAutorizado['dni_i'];
		$nombre = $datosDeContactoAutorizado['nombre_i'];
		$apellidos = $datosDeContactoAutorizado['apellidos_i'];
		$celular = $datosDeContactoAutorizado['celular_i'];
		$telefono = $datosDeContactoAutorizado['telefono_i'];
		$correo = $datosDeContactoAutorizado['correo_i'];
		$parentesco = $datosDeContactoAutorizado['parentesco_i'];
		$sql = "INSERT INTO contactosAutorizadosTemporal (idclientetemporal, dni, nombre,apellidos,telefono,telefono1,correo,parentesco,fecha) values ('$id','$dni','$nombre','$apellidos','$celular','$telefono','$correo','$parentesco','$fecha') ";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
			echo "Fail detected";
		}
	}

	public function registrarContactoAutorizadoDesdeVerificacion($datosContactosAutorizados,$id){
		global $db;
		$fecha=date("YmdHis");
		$dni=$datosContactosAutorizados['dni_i'];
		$nombre=$datosContactosAutorizados['nombre_i'];
		$apellidos=$datosContactosAutorizados['apellidos_i'];
		$telefono=$datosContactosAutorizados['telefono_i'];
		$telefono1=$datosContactosAutorizados['telefono1_i'];
		$correo=$datosContactosAutorizados['correo_i'];
		$parentesco=$datosContactosAutorizados['parentesco_i'];
		$sql="INSERT INTO contactosAutorizados (idcliente,dni,nombre,apellidos,telefono,telefono1,correo ,parentesco,fecha) VALUES ('$id','$dni','$nombre','$apellidos','$telefono','$telefono1','$correo','$parentesco','$fecha')";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
			echo "the other fail is here 3";
		}
	}

	public function mostrarContactosAutorizados($id){
		global $db;
		$contactoClienteObj=self::sentencia("SELECT * FROM contactosAutorizados WHERE idcliente='$id'");
		if ($contactoClienteObj){
			$n=0;
			foreach ($contactoClienteObj as $var) {
				$n++;
				?>
				<div class="">
                    <div class="row">
                    <h2><?php echo $n ?></h2>
                        <div class="form-group col-md-3">
                            <label for="dniC" class="sr-only">DNI no Obligatorio</label>
                            <input style="border: none;" type="text" placeholder="DNI no Obligatori" id="dniC" name="field_name[0][]" class="form-control border-bottom "   data-mask="99999999" id="dniI" value="<?php echo $var->dni ?>">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nombreC" class="sr-only">Nombres *</label>
                            <input style="border: none;" type="text" placeholder="Nombres *" id="nombreC" name="field_name[1][]" class="form-control border-bottom text-capitalize"  required id="name" value="<?php echo $var->nombre ?>">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="apellidos" class="sr-only">Apellidos *</label>
                            <input style="border: none;" type="text" placeholder="Apellidos *" id="apellidos" name="field_name[2][]" class="form-control x2 text-capitalize border-bottom text-capitalize" required  value="<?php echo $var->apellidos ?>" >
                        </div>
                        <div class="form-group col-md-3">
                           <label for="dniC" class="sr-only">Celular *</label>
                            <input style="border: none;" type="text" placeholder="Celular *" id="celular" name="field_name[3][]" class="form-control border-bottom " required  data-mask="999 999 999" id="celular" value="<?php echo $var->telefono ?>">
                        </div>
                        
                    </div> 
                    <div class="row">
                        
                        <div class="form-group col-md-4">
                            <label for="telefono" class="sr-only">Telefono </label>
                            <input style="border: none;" type="text" placeholder="telefono" id="telefono" name="field_name[4][]" class="form-control border-bottom text-capitalize"  id="telefono" value="<?php echo $var->telefono1 ?>">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="correo" class="sr-only">Correo</label>
                            <input style="border: none;" type="text" placeholder="Correo" id="correo" name="field_name[5][]" class="form-control x2 text-capitalize border-bottom text-capitalize" value="<?php echo $var->correo ?>">
                        </div>
                       <div class="form-group col-md-4">
           <label for="Parentesco" class="sr-only">Parentesco *</label>
           <select  style="border: none;" class="tecnico border-bottom form-control" required name="field_name[6][]">
            <option class="" value="<?php echo $var->parentesco ?>" selected ><?php echo $var->parentesco ?></option>
            <option value="Usuario">Usuario(a)</option>           
            <option value="padre">Padre</option>           
            <option value="Madre">Madre</option>           
            <option value="Hermano">Hermano(a)</option>           
            <option value="Conyuge">Conyuge</option>           
            <option value="Hijo">Hijo(a)</option>           
            <option value="Nieto">Nieto(a)</option>           
            <option value="Cuñado">Cuñado(a)</option>           
            <option value="Suegro">Suegro(a)</option>           
          </select>
        </div>
                        
<div class="hr-line-dashed"></div>
<hr>
                    </div>
             <a href='javascript:void(0);' class='remove_button  col-md-offset-10 btn btn-warning' title='quitar contacto'><strong>Quitar </strong><i class='fa fa-minus-circle'></i></a>           
                    </div>
				<?php
			}
			return true;
		}else{
			return false;
		}
	}

public function borrarContactosAutorizados($idcliente){
	global $db;
	$sql = "DELETE FROM contactosAutorizados WHERE idcliente = '$idcliente' ";
	if ($db->query($sql)) {
		return true;
	}else{
		return false;
	}
}
public static function obtenerCantidadContactosAutorizados($idcliente){
	$obtenerCantidadContactosAutorizados=self::sentencia("SELECT COUNT(*) AS cantidad FROM contactosAutorizados WHERE idcliente='$idcliente' ");
	return array_shift($obtenerCantidadContactosAutorizados);
}


public static function obtenerContactosAutorizadosTemporal($idClienteTemporal){
	return $obtenerClienteTemporalQ=self::SentenciaPublic("SELECT * FROM contactosAutorizadosTemporal WHERE 	idclientetemporal = $idClienteTemporal ");
}

public static function obtenerContactosAutorizados($idCliente){
	$obtenerContactosAutorizados=self::sentencia("SELECT * FROM contactosAutorizados WHERE 	idcliente = $idCliente ");
	return !empty($obtenerContactosAutorizados) ? $obtenerContactosAutorizados : false ;
}
//////////////////////////////////THE UPDATES HERE :v ////////////////////////////////////////////////////////////////

public function actualizarDatosNatural($idcliente){
	global $db;
	global $session;
	$this->fechaActualizacion=date("YmdHis");
	$this->idactualizador=$session->idusuario;
	$sql="UPDATE clientes SET nombre='".$db->escapeString($this->nombre)."', ";
	$sql.=" apellidosC= '".$db->escapeString($this->apellidosC)."' ,";
	$sql.=" departamento= '".$db->escapeString($this->departamento)."' ,";
	$sql.=" provincia= '".$db->escapeString($this->provincia)."' ,";
	$sql.=" distrito= '".$db->escapeString($this->distrito)."' ,";
	$sql.=" direccion= '".$db->escapeString($this->direccion)."' ,";
	$sql.=" referencia= '".$db->escapeString($this->referencia)."' ,";
	$sql.=" telefono= '".$db->escapeString($this->telefono)."' ,";
	$sql.=" telefono1= '".$db->escapeString($this->telefono1)."' ,";
	$sql.=" correo= '".$db->escapeString($this->correo)."' ,";
	$sql.=" fechaActualizacion= '".$this->fechaActualizacion."' ,";
	$sql.=" idactualizador= '".$db->escapeString($this->idactualizador)."' ,";
	$sql.=" correo1= '".$db->escapeString($this->correo1)."' ";
	$sql.="WHERE idcliente=".$db->escapeString($idcliente);
	if($db->query($sql)){
		return true;
	}else{
		return false;
	}
}

public function actualizarDatosEmpresa($idcliente){
	global $db;
	global $session;
	$this->fechaActualizacion=date("YmdHis");
	$this->idactualizador=$session->idusuario;
	$sql="UPDATE clientes SET razonSocial='".$db->escapeString($this->razonSocial)."', ";
	$sql.=" sectorDesempenia= '".$db->escapeString($this->sectorDesempenia)."' ,";
	$sql.=" departamentoL= '".$db->escapeString($this->departamentoL)."' ,";
	$sql.=" provinciaL= '".$db->escapeString($this->provinciaL)."' ,";
	$sql.=" distritoL= '".$db->escapeString($this->distritoL)."' ,";
	$sql.=" direccionL= '".$db->escapeString($this->direccionL)."' ,";
	$sql.=" telefono= '".$db->escapeString($this->telefono)."' ,";
	$sql.=" telefono1= '".$db->escapeString($this->telefono1)."' ,";
	$sql.=" correo= '".$db->escapeString($this->correo)."' ,";
	$sql.=" correo1= '".$db->escapeString($this->correo1)."' ,";
	/////THIS IS THE REPRESENTANTE LEGAL -->>>>><<<<<-----
	$sql.=" nombre= '".$db->escapeString($this->nombre)."' ,";
	$sql.=" apellidosC= '".$db->escapeString($this->apellidosC)."' ,";
	$sql.=" departamento= '".$db->escapeString($this->departamento)."' ,";
	$sql.=" provincia= '".$db->escapeString($this->provincia)."' ,";
	$sql.=" distrito= '".$db->escapeString($this->distrito)."' ,";
	$sql.=" direccion= '".$db->escapeString($this->direccion)."' ,";
	$sql.=" referencia= '".$db->escapeString($this->referencia)."' ,";
	$sql.=" telefono2= '".$db->escapeString($this->telefono2)."' ,";
	$sql.=" telefono3= '".$db->escapeString($this->telefono3)."' ,";
	$sql.=" correo2= '".$db->escapeString($this->correo2)."' ,";
	$sql.=" fechaActualizacion= '".$this->fechaActualizacion."' ,";
	$sql.=" idactualizador= '".$db->escapeString($this->idactualizador)."' ,";
	$sql.=" correo3= '".$db->escapeString($this->correo3)."' ";
	$sql.="WHERE idcliente=".$db->escapeString($idcliente);
	if($db->query($sql)){
		return true;
	}else{
		return false; 
	}

}


	///////////Consulta Basica... :v...
	public static function obtenerDatosPorDni($dni){
		global $db;
		$dni=$db->escapeString(htmlspecialchars($dni));
		$obtenerDatosPorDniQ=self::sentencia("SELECT nombre,apellidosC,dni,idcliente FROM clientes WHERE dni=$dni AND tipoCliente='Natural' LIMIT 1");
		return !empty($obtenerDatosPorDniQ)?array_shift($obtenerDatosPorDniQ):false;
	}

	public static function obtenerDatosPorRuc($ruc){
		global $db;
		$ruc=$db->escapeString(htmlspecialchars($ruc));
		$obtenerDatosPorRucQ=self::sentencia("SELECT razonSocial,ruc,idcliente FROM clientes WHERE ruc=$ruc AND tipoCliente='Empresa' LIMIT 1");
		return !empty($obtenerDatosPorRucQ) ? array_shift($obtenerDatosPorRucQ):false;
	}



///////////////////////////////////FIN THE UPDATES HERE :V ////////////////////7777

// -----------------------------------------------------------------

	public static function obtenerDepartamentos(){
		global $db;
		$clienteObj=self::sentencia("SELECT * FROM departamentos");
		return !empty($clienteObj)?$clienteObj:false;
	}
	public static function obtenerProvincias($iddepartamento){
		global $db;
		$clienteObj=self::sentencia("SELECT * FROM provincias WHERE iddepartamento= $iddepartamento");
		return !empty($clienteObj)?$clienteObj:false;

	}
	public static function obtenerDistritos($idprovincia){
		global $db;
		$clienteObj=self::sentencia("SELECT * FROM distritos WHERE idprovincia=$idprovincia");
		return !empty($clienteObj)?$clienteObj:false;

	}

	public static function otenerTodoLosDistritos(){
		$distrObj=self::sentencia("SELECT DISTINCT(distrito) FROM distritos ORDER BY idprovincia DESC ");
		return !empty($distrObj)?$distrObj:false;
	}

	public function obtenerDepartamentoPorId($iddepartamento){
		$obtenerDepartamentoPorId=self::sentencia("SELECT * FROM departamentos WHERE iddepartamento= $iddepartamento");
		return !empty($obtenerDepartamentoPorId)?array_shift($obtenerDepartamentoPorId):false;
	}
	public function otenerProvinciaPorId($idprovincia){
		$otenerProvinciaPorId=self::sentencia("SELECT * FROM provincias WHERE idprovincia=$idprovincia");
		return !empty($otenerProvinciaPorId) ? array_shift($otenerProvinciaPorId):false;
	}
	public function obtenerDistritoPorId($iddistrito){
		$obtenerDistritoPorId=self::sentencia("SELECT * FROM distritos WHERE iddistrito=$iddistrito");
		return !empty($obtenerDistritoPorId)?array_shift($obtenerDistritoPorId):false;
	}
	// --------------------------------esto es del obejeto vehículo-----------------------

	public static function obtenerPlan(){
		global $db;
		$clienteObj=self::sentencia("SELECT plan,COALESCE(idtec,0) as idtec FROM planes WHERE estado = 1 ORDER BY orden ASC");
		foreach ($clienteObj as $var) :
			?>
		<option value="<?php echo $var->plan ?>" data-type="<?php echo $var->idtec ?>"><?php echo $var->plan ?></option>
			<?php
		endforeach;	

	}

    public static function obtenerPlanSelected($desc){
        global $db;
        $clienteObj=self::sentencia("SELECT plan,COALESCE(idtec,0) as idtec FROM planes WHERE estado = 1 ORDER BY orden ASC");
        foreach ($clienteObj as $var) :
            ?>
            <option value="<?php echo $var->plan ?>" data-type="<?php echo $var->idtec ?>" <?php echo $var->plan==$desc?'selected':''; ?>><?php echo $var->plan ?></option>
        <?php
        endforeach;

    }

    public static function obtenerAseguradoraSelected($desc,$aseguradora,$descPlan){
          if(strpos(trim($desc),'Plan Kil')!== false) {
             // }
        ?>
        <option class="" value="<?= $desc ?>"> Plan Kilómetros</option>
        <?php } else if(strpos(trim($desc),'Plan Full')!== false) {
             // }
        ?>
        <option class="" value="<?= $desc ?>"> Plan Full</option>
          <?php } else if(strpos(trim($desc),'Toyota')!== false) {
              // }
              ?>
              <option class="" value="<?= $desc ?>"> Toyota</option>
        <?php            } else{
        ?>
        <option class="" <?php if (($desc === "GpsSmart" || $aseguradora === 'Pacifico Seguros') && ($descPlan === "E-call" || $descPlan === 'E-call Plus')) {
            echo "";
        } else {
            echo "style='display:none;'";
        } ?>
                value="GpsSmart" <?php if (($desc === "GpsSmart" || $aseguradora === 'Pacifico Seguros') && ($descPlan === "E-call" || $descPlan === 'E-call Plus')) {
            echo 'selected';
        } ?>>GpsSmart
        </option>
        <option class="" value="no" <?php echo $desc === "no" ? 'selected' : ''; ?>>No</option>
        <?php
           }
    }


    public static function obtenerAseguradora($desc){
        global $db;
        $Obj=self::sentencia("SELECT descripcion FROM aseguradoras WHERE estado = 1  order by idaseg asc");
        foreach ($Obj as $var) :
            ?>
            <option class='text-capitalize' value="<?php echo $var->descripcion ?>"  <?php echo $var->descripcion===$desc?'selected':''; ?>><?php echo $var->descripcion ?></option>
        <?php
        endforeach;
    }

    public static function obtenerTipoServicio($filTOP,$filPerm){
        global $db;

        if($filTOP!=''){
            $filTOP=" and tipo not in (".$filTOP.") ";
        }
        if($filPerm!=''){
            $filPerm="  OR tipo in (".$filPerm.") ";
        }
?>       <option class="" value="" disabled selected >Seleccione Tipo Servicio *</option><?php

        $tipoServ=self::sentencia("SELECT idtipo,tipo FROM tipo_servicio WHERE estado = 1 ".$filTOP.$filPerm." ORDER BY orden ASC");
        foreach ($tipoServ as $var) :
            ?>
            <option value="<?php echo $var->tipo ?>"><?php echo $var->tipo ?></option>
        <?php
        endforeach;

    }
    public static function obtenerTpServicio($filTOP,$filPerm){
        global $db;
        $result=null;
        $concat='';

        if($filTOP!=''){
            $filTOP="  and tipo not in (".$filTOP.")";
        }

        if($filPerm!=''){
            $filPerm="  OR tipo  in (".$filPerm.")";
        }
        $tipoServ=self::sentencia("SELECT idtipo,tipo FROM tipo_servicio WHERE estado = 1".$filTOP.$filPerm." ORDER BY orden ASC");
        foreach ($tipoServ as $var) :
          $result=$result.$concat.$var->tipo;
          $concat=' o ';
        endforeach;
        return $result;
    }
// ------------------------------------

	public function obtenerClientesPendientes(){
		global $db;
 
	}
	public function registradoEnOnix(){
		global $db;
		global $session;
		$this->fechaRegistradoEnOnix=date("YmdHis");
		$sql="UPDATE clientes SET ";
		$sql.=" enOnix='";
		$sql.=$db->escapeString('si')."', ";
		$sql.=" fechaRegistradoEnOnix='";
		$sql.=$db->escapeString($this->fechaRegistradoEnOnix)."', ";
		$sql.=" idRegistradorEnOnix='";
		$sql.=$db->escapeString($session->idusuario)."'";
		$sql.=" WHERE idcliente='";
		$sql.=$db->escapeString($this->idcliente)."'";
		return ($db->query($sql))?true:false;
	}

    public function actuDBasicosCliente(){
        global $db; 
        $sqlAc="UPDATE clientes SET tipoCliente='$this->idcliente', dni='$this->dni', nombre ='$this->nombre', apellidosC='$this->apellidosC', ruc='$this->ruc' ,razonSocial = '$this->razonSocial' WHERE idcliente='$this->idcliente' ";
        return ($db->query($sqlAc))?true:false;
    }

    public function actualizarDatosDesdeOnyx($idcliente){
	global $db;
	global $session;
	$this->fechaActualizacion=date("YmdHis");
	$this->idactualizador=$session->idusuario;
	$sql="UPDATE clientes SET nombre='".$db->escapeString($this->nombre)."', ";
    $sql.=" tipoCliente= '".$db->escapeString($this->tipoCliente)."' , ";
	$sql.=" apellidosC= '".$db->escapeString($this->apellidosC)."' ,";
	$sql.=" razonSocial= '".$db->escapeString($this->razonSocial)."' ,";
	$sql.=" ruc= '".$db->escapeString($this->ruc)."' ,";
	$sql.=" dni= '".$db->escapeString($this->dni)."' ,";
	$sql.=" fechaActualizacion= '".$this->fechaActualizacion."' ,";
	$sql.=" idactualizador= '".$db->escapeString($this->idactualizador)."' ";
	$sql.="WHERE idcliente=".$db->escapeString($idcliente);

	if($db->query($sql)){
		return true;
	}else{
		return false;
	}
}


    public function actualizarDatosViculacion($idcliente){
        global $db;
        global $session;
        $this->fechaActualizacion=date("YmdHis");
        $this->idactualizador=$session->idusuario;
        $sql="UPDATE clientes SET nombre='".$db->escapeString($this->nombre)."', ";
        $sql.=" tipoCliente= '".$db->escapeString($this->tipoCliente)."' , ";
        $sql.=" apellidosC= '".$db->escapeString($this->apellidosC)."' ,";
        $sql.=" razonSocial= '".$db->escapeString($this->razonSocial)."' ,";
        $sql.=" ruc= '".$db->escapeString($this->ruc)."' ,";
        $sql.=" dni= '".$db->escapeString($this->dni)."' ,";
        $sql.=" correo= '".$db->escapeString($this->correo)."' ,";
        $sql.=" telefono= '".$db->escapeString($this->telefono)."' ,";
        $sql.=" telefono2= '".$db->escapeString($this->telefono2)."' ,";
        $sql.=" direccion= '".$db->escapeString($this->direccion)."' ,";
        $sql.=" fechaActualizacion= '".$this->fechaActualizacion."' ,";
        $sql.=" idactualizador= '".$db->escapeString($this->idactualizador)."' ";
        $sql.="WHERE idcliente=".$db->escapeString($idcliente);

        if($db->query($sql)){
            return true;
        }else{
            return false;
        }
    }

	public function obtenerBuscarClientesCantidad($criterio){
		global $db;
		$result=self::sentenciaV2("SELECT count(*) as cantidad 
									FROM  
									clientes 
									WHERE 
									dni like '%".$db->escapeString($criterio)."%'
									or nombre like '%".$db->escapeString($criterio)."%' 
									or apellidosC like '%".$db->escapeString($criterio)."%'
									or razonSocial like '%".$db->escapeString($criterio)."%'");
		return !empty($result)? array_shift($result):false;		
	}

	public function obtenerBuscarClientesPaginacion($criterio, $posisionDePagina,$registrosPorPagina){
		global $db;
		$result=self::sentenciaV2("SELECT * 
									FROM  
									clientes  
									WHERE  
									dni like '%".$db->escapeString($criterio)."%'
									or nombre like '%".$db->escapeString($criterio)."%' 
									or apellidosC like '%".$db->escapeString($criterio)."%'
									or razonSocial like '%".$db->escapeString($criterio)."%'
									ORDER BY idcliente DESC 
									LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($result)? $result:false;
	}
}
 ?>

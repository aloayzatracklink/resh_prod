<?php

class Comisiones extends Recursos
{
	public $idcomision;
	public $idcliente;
	public $idvehiculo;
	public $idcanal;
	public $idconvenio;
	public $idfacturacion;
	public $plan;
	public $montoTotal;
	public $comision;
	public $numeroReciboHonorario;
	public $titularCuenta;
	public $reciboHonorarios;
	public $fechapago;
	public $documentoFacturacion;
	public $numeroFacturacion;
	public $fechaRegistro;
	public $idCreador;
	public $modificadoPor;
	public $fechaModificacion;
	public $bancoC;
	public $monedaC;
	public $numeroCuentaPagado;
	public $fechaPagado;
	public $idPagador;
	public $estado;
	public $observacion;
	public $estadoPago;
	public $estadoFacturacion;
	public $idVerficadorPago;
	public $fechaVerificadorPago;
	public $idFacturador;
	public $fechaFacturador;
	public $cantidad;
	public $total;
	public $totalComision;
	public $tiempo;
	public $idcomisionConvenio;
	public $precioConvenio;
	public $precioCliente;
	public $comison;
	public $moneda;
	public $rucC;
	public $cciC;
	public $idtitular;
	public $type_commission;
	public $trabajo;
	public $register_at;

	public $ejecutivo;
	public $fechapagocliente;
	public $cliente;
	public $tipocanal;
	public $nombrecanal;
	public $ejecutivocanal;
	public $sistema;
	//public $cantidad;
	public $preciosinigv;
	public $precioconigv;
	// public $comision;
	public $boletafatura;
	public $emisiorrecibo;
	public $dni;
	public $rucrecibo;
	public $numeroreciboporhonorarios;
	public $nbanco;
	public $numero;
	//public $moneda;
	public $fechapagocomision;
	public $idcomsion;
	public $formapagocomision;

	public $idrenovacion;

	public $chasis;


	function __construct()
	{
	}
	public function registrarComision()
	{
		global $db;
		$fechaHoy = date("YmdHis");
		$sql = "INSERT INTO comisiones (idcliente,idcanal,idconvenio,idvehiculo,idfacturacion,tiempo,montoTotal,comision,fechapago,documentoFacturacion,fechaRegistro,idCreador) VALUES ('";
		$sql .= $db->escapeString($this->idcliente) . "','";
		$sql .= $db->escapeString($this->idcanal) . "','";
		$sql .= $db->escapeString($this->idconvenio) . "','";
		$sql .= $db->escapeString($this->idvehiculo) . "','";
		$sql .= $db->escapeString($this->idfacturacion) . "','";
		$sql .= $db->escapeString($this->tiempo) . "','";
		$sql .= $db->escapeString($this->montoTotal) . "','";
		$sql .= $db->escapeString($this->comision) . "','";
		$sql .= $db->escapeString($this->fechapago) . "','";
		$sql .= $db->escapeString($this->documentoFacturacion) . "','";
		$sql .= $db->escapeString($fechaHoy) . "','";
		$sql .= $db->escapeString($this->idCreador) . "')";
		if ($db->query($sql)) {
			$this->idcomision = $db->ultimoIdInsertado();
			return true;
		} else {
			return false;
		}
	}


	public function obtenerComisiones()
	{
		global $session;
		$obtenerComisionesQ = self::sentencia("SELECT * FROM comisiones C WHERE idCreador='$session->idusuario' AND idconvenio='0' AND (('PENDIENTE'=(SELECT banco FROM canales CA WHERE C.idcanal=CA.idcanal ) OR 'PENDIENTE'=(SELECT numeroCuenta FROM canales CA WHERE C.idcanal=CA.idcanal ) OR 'PENDIENTE'=(SELECT moneda FROM canales CA WHERE C.idcanal=CA.idcanal ) OR 'PENDIENTE'=(SELECT canalId FROM canales CA WHERE C.idcanal=CA.idcanal ) OR 'PENDIENTE'=(SELECT titular FROM canales CA WHERE C.idcanal=CA.idcanal )) OR  (estadoFacturacion<>'34' OR C.estadoPago<>'67') OR C.idcanal='0') ORDER BY idcomision DESC");
		return !empty($obtenerComisionesQ) ? $obtenerComisionesQ : false;
	}

	public function obtenerComisionesRealizadas()
	{
		global $session;
		$obtenerComisionesRealizadasQ = self::sentencia("SELECT * FROM comisiones C WHERE idCreador='$session->idusuario' AND idconvenio='0' AND (('PENDIENTE'<>(SELECT banco FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'<>(SELECT numeroCuenta FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'<>(SELECT moneda FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'<>(SELECT canalId FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'<>(SELECT titular FROM canales CA WHERE C.idcanal=CA.idcanal ))) AND (estadoFacturacion='34' AND C.estadoPago='67') ORDER BY idcomision DESC");
		return !empty($obtenerComisionesRealizadasQ) ? $obtenerComisionesRealizadasQ : false;
	}


	public static function obtenerDatosDeComision($idcomision)
	{
		global $db;
		$obtenerDatosDeComisionQ = self::sentencia("SELECT * FROM comisiones WHERE idcomision=" . $db->escapeString($idcomision) . " LIMIT 1");
		return !empty($obtenerDatosDeComisionQ) ? array_shift($obtenerDatosDeComisionQ) : false;
	}
	public static function obtenerDatosDeComisionPorIdFacturacion($idfacturacion)
	{
		global $db;
		$facturacionDatosDeComision = self::sentencia("SELECT * FROM comisiones WHERE idfacturacion=" . $db->escapeString($idfacturacion) . " LIMIT 1");
		return !empty($facturacionDatosDeComision) ? array_shift($facturacionDatosDeComision) : false;
	}

	public function cambiarEstadoComision($idEstado, $idcomision)
	{
		global $db;
		global $session;
		$sql = "UPDATE comisiones SET estado='";
		$sql .= $db->escapeString($idEstado) . "' WHERE idcomision=";
		$sql .= $db->escapeString($idcomision) . "";
		if ($db->query($sql)) {
			return true;
		} else {
			return false;
		}
	}

	public function actualizarComision()
	{
		global $db;
		global $session;
		$sql = "UPDATE comisiones SET comision='";
		$sql .= $db->escapeString($this->comision) . "', ";
		$sql .= " estado='";
		$sql .= $db->escapeString($this->estado) . "', ";
		$sql .= " reciboHonorarios='";
		$sql .= $db->escapeString($this->reciboHonorarios) . "' ";
		$sql .= "WHERE idcomision='" . $db->escapeString($this->idcomision) . "'";
		if ($db->query($sql)) {
			return true;
		} else {
			return false;
		}
	}

	public function registrarDatosDeFacturacion()
	{
		global $db;
		global $session;
		$fechaHoy = date("YmdHis");
		$sql = "UPDATE comisiones SET numeroFacturacion ='";
		$sql .= $db->escapeString($this->numeroFacturacion) . "',";
		$sql .= " modificadoPor ='";
		$sql .= $db->escapeString($session->idusuario) . "' ,";
		$sql .= " fechaModificacion ='";
		$sql .= $fechaHoy . "' ";
		$sql .= "WHERE idfacturacion=" . $db->escapeString($this->idfacturacion) . " ";
		if ($db->query($sql)) {
			$this->estadoFacturacion = '34';
			$this->registrarEstadoFacturacion();
			return true;
		} else {
			return false;
		}
	}

	///Exel -. -. -. -.a
	public static function obtenerComisionesRealizadasExel($idCreador, $desde, $hasta)
	{
		$obtenerComisionesRealizadasQ = self::sentencia("SELECT * FROM comisiones C WHERE fechaRegistro BETWEEN '$desde' AND '$hasta' AND idCreador='$idCreador'   AND estadoFacturacion='34' AND C.estadoPago='67' AND numeroReciboHonorario<>'' AND estado = '59' AND idtitular<>'0'  GROUP BY idtitular,idCreador");
		return !empty($obtenerComisionesRealizadasQ) ? $obtenerComisionesRealizadasQ : false;
	}

	public static function obtenerComisionesRealizadasPorCanal($idtitular, $idCreador, $desde, $hasta)
	{
		$obtenerComisionesRealizadasPorCanalQ = self::sentencia("SELECT * FROM comisiones C WHERE fechaRegistro BETWEEN '$desde' AND '$hasta' AND idtitular='$idtitular' AND idCreador='$idCreador' AND  estadoFacturacion='34' AND estadoPago='67' AND numeroReciboHonorario<>'' AND estado='59'");
		return !empty($obtenerComisionesRealizadasPorCanalQ) ? $obtenerComisionesRealizadasPorCanalQ : false;
	}

	public static function obtenerComisionesRealizadasPorCanalTotal($idtitular, $idCreador, $desde, $hasta)
	{
		$obtenerComisionesRealizadasPorCanalQ = self::sentencia("SELECT COUNT(idcanal) AS cantidad, SUM(montoTotal) AS total, SUM(comision) AS totalComision FROM comisiones C WHERE fechaRegistro BETWEEN '$desde' AND '$hasta' AND estadoFacturacion='34' AND C.estadoPago='67' AND  numeroReciboHonorario<>'' AND estado = '59' AND idtitular='$idtitular' AND idCreador='$idCreador' LIMIT 1");
		return !empty($obtenerComisionesRealizadasPorCanalQ) ? $obtenerComisionesRealizadasPorCanalQ : false;
	}
	public static function obtenerComisionesPendientes()
	{
		global $session;
		$obtenerComisionesPendientes = self::sentencia("SELECT COUNT(idcanal) AS cantidad FROM comisiones C WHERE idCreador='$session->idusuario' AND idconvenio='0' AND (('PENDIENTE'=(SELECT banco FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'=(SELECT numeroCuenta FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'=(SELECT moneda FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'=(SELECT canalId FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'=(SELECT titular FROM canales CA WHERE C.idcanal=CA.idcanal )) OR  (estadoFacturacion<>'34' OR C.estadoPago<>'67')) ORDER BY idcomision DESC");
		return !empty($obtenerComisionesPendientes) ? array_shift($obtenerComisionesPendientes) : false;
	}

	public function obtenerComisionesPendientesCantidadTodos()
	{
		global $session;
		$obtenerComisionesPendientes = self::sentencia("SELECT COUNT(idcanal) AS cantidad FROM comisiones C WHERE idconvenio='0' AND ((('PENDIENTE'=(SELECT banco FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'=(SELECT numeroCuenta FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'=(SELECT moneda FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'=(SELECT canalId FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'=(SELECT titular FROM canales CA WHERE C.idcanal=CA.idcanal )) OR (estadoFacturacion<>'34' OR C.estadoPago<>'67')) OR ''=(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) OR  numeroReciboHonorario = '') LIMIT 1");
		return !empty($obtenerComisionesPendientes) ? array_shift($obtenerComisionesPendientes) : false;

	}
	public function obtenerComisionessCantidadTodos()
	{
		global $session;
		$obtenerComisionesPendientes = self::sentencia("SELECT COUNT(idcanal) AS cantidad FROM comisiones LIMIT 1");
		return !empty($obtenerComisionesPendientes) ? array_shift($obtenerComisionesPendientes) : false;

	}

	public function obtenerComisionesPendientesTodos()
	{
		$obtenerComisionesPendientesTodos = self::sentencia("SELECT * FROM comisiones C WHERE  idconvenio='0' AND ((('PENDIENTE'=(SELECT banco FROM canales CA WHERE C.idcanal=CA.idcanal ) OR 'PENDIENTE'=(SELECT numeroCuenta FROM canales CA WHERE C.idcanal=CA.idcanal ) OR 'PENDIENTE'=(SELECT moneda FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'=(SELECT canalId FROM canales CA WHERE C.idcanal=CA.idcanal ) OR 'PENDIENTE'=(SELECT titular FROM canales CA WHERE C.idcanal=CA.idcanal )) OR (C.estadoFacturacion<>'34' OR C.estadoPago<>'67')) OR ''=(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) OR  numeroReciboHonorario = '') ORDER BY idcomision ASC");
		return !empty($obtenerComisionesPendientesTodos) ? $obtenerComisionesPendientesTodos : false;
	}
	public function obtenerPrimerRegistroComision($idCreador)
	{
		$obtenerPrimerRegistroComision = self::sentencia("SELECT fechaRegistro FROM comisiones WHERE (('PENDIENTE'<>(SELECT banco FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'<>(SELECT numeroCuenta FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'<>(SELECT moneda FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'<>(SELECT canalId FROM canales CA WHERE C.idcanal=CA.idcanal ) AND 'PENDIENTE'<>(SELECT titular FROM canales CA WHERE C.idcanal=CA.idcanal )) AND ('34'=(SELECT estadoFacturacion FROM datosFacturacion F WHERE C.idfacturacion=F.datosfacturacionid ))) AND idCreador=$idCreador");
		return !empty($obtenerPrimerRegistroComision) ? array_shift($obtenerPrimerRegistroComision) : false;
	}

	public function registrarNumeroFacturacion()
	{
		global $db;
		$registrarNumeroFacturacionQ = "UPDATE comisiones SET numeroReciboHonorario ='" . $db->escapeString($this->numeroReciboHonorario) . "' WHERE idcomision ='" . $db->escapeString($this->idcomision) . "'";
		if ($db->query($registrarNumeroFacturacionQ)) {
			return true;
		} else {
			return false;
		}
	}


	public function registrarNumeroFacturacionRUCC()
	{
		global $db;
		$registrarNumeroFacturacionQ = "UPDATE comisiones SET rucC ='" . $db->escapeString($this->rucC) . "' WHERE idcomision ='" . $db->escapeString($this->idcomision) . "'";
		if ($db->query($registrarNumeroFacturacionQ)) {
			return true;
		} else {
			return false;
		}
	}

	public function registrarNumeroCCIC()
	{
		global $db;
		$registrarNumeroFacturacionQ = "UPDATE comisiones SET cciC ='" . $db->escapeString($this->cciC) . "' WHERE idcomision ='" . $db->escapeString($this->idcomision) . "'";
		if ($db->query($registrarNumeroFacturacionQ)) {
			return true;
		} else {
			return false;
		}
	}

	public function buscarParaFiltroE($datos, $idEjecutivo)
	{
		global $session;
		global $db;
		$datos = $db->escapeString($datos);
		$idEjecutivo = $db->escapeString($idEjecutivo);
		$obtenerRegistroSubidoQ = self::sentencia("SELECT C.idcomision, C.idcliente , C.idcanal, C.idvehiculo, C.idfacturacion, C.numeroReciboHonorario, C.fechaRegistro, C.estado , C.numeroFacturacion , C.estadoPago, C.estadoFacturacion,C.trabajo  FROM comisiones C LEFT JOIN  clientes Cl ON Cl.idcliente=C.idcliente  WHERE  ((C.numeroFacturacion LIKE '$datos' OR C.numeroReciboHonorario LIKE '$datos') OR (Cl.nombre LIKE '$datos' OR Cl.razonSocial LIKE '$datos' OR Cl.apellidosC LIKE '$datos' OR CONCAT_WS('',Cl.nombre,Cl.apellidosC) LIKE '$datos')) AND C.idCreador='$idEjecutivo' AND  (estadoFacturacion='34' AND C.estadoPago='67' AND ''<>(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) AND numeroReciboHonorario<>'')");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}

	public function obtenerRegistrosSubidosE($posisionDePagina, $registrosPorPagina, $idEjecutivo)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones C  
		WHERE C.estadoFacturacion='34' AND C.estadoPago='67' 
		AND ''<>(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) 
		AND C.numeroReciboHonorario<>'' AND C.idCreador='$idEjecutivo'  
		ORDER BY C.idcomision ASC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}


	public function obtenerRegistrosSubidosEFecha($posisionDePagina, $registrosPorPagina, $desde, $hasta)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones C  
		WHERE estadoFacturacion='34' AND C.estadoPago='67' 
		AND ''<>(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) 
		AND C.numeroReciboHonorario<>'' AND C.fechaFacturador 
		BETWEEN '$desde' AND '$hasta'  ORDER BY idcomision ASC 
		LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}

	public function obtenerRegistrosSubidosEFechaEJE($posisionDePagina, $registrosPorPagina, $desde, $hasta)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones C WHERE (SUBSTRING(reciboHonorarios, 1, 14) BETWEEN '$desde' AND '$hasta')  AND idCreador='$session->idusuario' ORDER BY idcomision DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}

	public function obtenerRegistrosSubidosEEstado($posisionDePagina, $registrosPorPagina, $estado)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones C WHERE estadoFacturacion='34' AND C.estadoPago='67' AND   C.estado='$estado'  ORDER BY idcomision ASC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}

	public function obtenerResEstadoComsionnn($posisionDePagina, $registrosPorPagina, $estado)
	{
		global $session;
		// $obtenerRegistroSubidoQ = self::sentencia("SELECT *,V.chasis FROM comisiones C
		// LEFT JOIN vehiculos V ON C.idvehiculo = V.idvehiculo  
		// WHERE C.estadoFacturacion='34'  AND  C.estado='$estado'  
		// ORDER BY C.idcomision ASC LIMIT $posisionDePagina, $registrosPorPagina");

		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones C 
		WHERE estadoFacturacion='34'  AND  C.estado='$estado'  
		ORDER BY idcomision ASC LIMIT $posisionDePagina, $registrosPorPagina");

		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}
	public function obtenerRegistrosSubidosEEstadoEJE($posisionDePagina, $registrosPorPagina, $estado)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones C WHERE estado='$estado' AND idCreador='$session->idusuario'  ORDER BY idcomision DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}

	public function obtenerRegistrosSubidosEestadoIdejecu($posisionDePagina, $registrosPorPagina, $estado, $idEjecutivo)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones C 
		WHERE C.estadoFacturacion='34' AND C.estadoPago='67'  
		AND C.estado='$estado' AND C.idCreador='$idEjecutivo'  
		ORDER BY C.idcomision ASC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}

	public function obtenerRegistrosSubidosEfechaIdejecutiov($posisionDePagina, $registrosPorPagina, $idEjecutivo, $desde, $hasta)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones C WHERE estadoFacturacion='34' AND C.estadoPago='67' AND ''<>(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) AND numeroReciboHonorario<>'' AND C.fechaFacturador BETWEEN '$desde' AND '$hasta' AND C.idCreador='$idEjecutivo'  ORDER BY idcomision ASC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}


	public function obtenerRegistrosSubidosEFechaEstado($posisionDePagina, $registrosPorPagina, $estado, $desde, $hasta)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones C  
		WHERE C.estadoFacturacion='34' 
		AND C.estadoPago='67' 
		AND ''<>(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) 
		AND C.numeroReciboHonorario<>'' 
		AND C.fechaFacturador BETWEEN '$desde' AND '$hasta' 
		AND C.estado='$estado'  ORDER BY C.idcomision ASC 
		LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}



	public function obtenerRegistrosSubidosEFechaEstadoEJE($posisionDePagina, $registrosPorPagina, $estado, $desde, $hasta)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones WHERE (SUBSTRING(reciboHonorarios, 1, 14) BETWEEN '$desde' AND '$hasta')  AND estado='$estado' AND idCreador='$session->idusuario'  ORDER BY idcomision DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}


	public function obtenerRegistrosSubidosETodos($posisionDePagina, $registrosPorPagina, $estado, $idEjecutivo, $desde, $hasta)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones C 
		WHERE C.estadoFacturacion='34' AND C.estadoPago='67' 
		AND ''<>(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) 
		AND C.numeroReciboHonorario<>'' AND C.fechaFacturador BETWEEN '$desde' AND '$hasta' 
		AND C.estado='$estado' AND C.idCreador='$idEjecutivo' 
		ORDER BY C.idcomision ASC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}

	public function obtenerRegistrosSubidosCantidaE($idEjecutivo)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE estadoFacturacion='34' AND C.estadoPago='67' AND ''<>(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) AND numeroReciboHonorario<>'' AND C.idCreador='$idEjecutivo' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}

	public function obtenerRegistrosSubidosCantidaEFecha($desde, $hasta)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE estadoFacturacion='34' AND C.estadoPago='67' AND ''<>(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) AND numeroReciboHonorario<>'' AND C.fechaFacturador BETWEEN '$desde' AND '$hasta' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}
	// SUBSTRING('$desde', 1, 14) AND SUBSTRING('$hasta', 1, 14)
	public function obtenerRegistrosSubidosCantidaEFechaEJE($desde, $hasta)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE (SUBSTRING(reciboHonorarios, 1, 14) BETWEEN '$desde' AND '$hasta')  AND idCreador='$session->idusuario' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}

	public function obtenerRegistrosSubidosCantidaEestadoPago($estado)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE estadoFacturacion='34' AND C.estadoPago='67' AND C.estado='$estado' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}

	public function obtenerRegistrosPORESTADOCOmision($estado)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE estadoFacturacion='34'  AND C.estado='$estado' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}

	public function obtenerRegistrosSubidosCantidaEestadoPagoEJE($estado)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE  estado='$estado' AND idCreador='$session->idusuario' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}

	public function obtenerRegistrosSubidosCantidaEestaodIdejutivo($idEjecutivo, $estado)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE estadoFacturacion='34' AND C.estadoPago='67'  AND C.estado='$estado' AND C.idCreador='$idEjecutivo' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}
	public function obtenerRegistrosSubidosCantidaEejecutviFecha($idEjecutivo, $desde, $hasta)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE estadoFacturacion='34' AND C.estadoPago='67' AND ''<>(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) AND numeroReciboHonorario<>'' AND C.fechaFacturador BETWEEN '$desde' AND '$hasta' AND C.idCreador='$idEjecutivo' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}
	public function obtenerRegistrosSubidosCantidaEestadoFecha($estado, $desde, $hasta)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE estadoFacturacion='34' AND C.estadoPago='67' AND ''<>(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) AND numeroReciboHonorario<>'' AND C.fechaFacturador BETWEEN '$desde' AND '$hasta' AND C.estado='$estado' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}
	public function obtenerRegistrosSubidosCantidaEestadoFechaEJE($estado, $desde, $hasta)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE  (SUBSTRING(reciboHonorarios, 1, 14) BETWEEN '$desde' AND '$hasta') AND estado='$estado' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}
	public function obtenerRegistrosSubidosCantidaETodos($estado, $desde, $hasta, $idEjecutivo)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE estadoFacturacion='34' AND C.estadoPago='67' AND ''<>(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) AND numeroReciboHonorario<>'' AND C.fechaFacturador BETWEEN '$desde' AND '$hasta' AND C.estado='$estado' AND C.idCreador='$idEjecutivo' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}
	////////////////////////////////////////sin id///////////////////////////////
	public function buscarParaFiltro($datos)
	{
		global $session;
		global $db;
		$datos = $db->escapeString($datos);
		$obtenerRegistroSubfiltro = self::sentencia("SELECT C.idcomision, C.idcliente , C.idcanal, C.idvehiculo, C.idfacturacion, C.numeroReciboHonorario, C.fechaRegistro, C.estado , C.numeroFacturacion, C.estadoPago, C.estadoFacturacion,C.trabajo FROM comisiones C LEFT JOIN  clientes Cl ON Cl.idcliente=C.idcliente  WHERE  (C.numeroFacturacion LIKE '$datos' OR C.numeroReciboHonorario LIKE '$datos') OR (Cl.nombre LIKE '$datos' OR Cl.razonSocial LIKE '$datos' OR Cl.apellidosC LIKE '$datos' OR CONCAT_WS('',Cl.nombre,Cl.apellidosC) LIKE '$datos') AND  (estadoFacturacion='34' AND C.estadoPago='67' AND ''<>(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) AND numeroReciboHonorario<>'') LIMIT 15");
		return !empty($obtenerRegistroSubfiltro) ? $obtenerRegistroSubfiltro : false;

	}

	public function buscarParaFiltroCOMMM($datos)
	{
		global $session;
		global $db;
		$datos = $db->escapeString($datos);
		$obtenerRegistroSubfiltro = self::sentencia("SELECT C.idcomision, C.idcliente , 
		C.idcanal, C.idvehiculo, C.idfacturacion, C.numeroReciboHonorario, C.fechaRegistro, 
		C.estado , C.numeroFacturacion, C.estadoPago, C.estadoFacturacion, C.idCreador,
		C.type_commission,C.trabajo,C.idrenovacion, V.chasis  
		FROM comisiones C 
		LEFT JOIN  clientes Cl ON (Cl.idcliente=C.idcliente) 
		LEFT JOIN renovaciones r ON r.idrenovacion =C.idrenovacion
		LEFT JOIN vehiculos V ON C.idvehiculo = V.idvehiculo  
		WHERE  (C.numeroFacturacion LIKE '$datos' 
		OR C.numeroReciboHonorario LIKE '$datos') 
		OR (UPPER(Cl.nombre) LIKE '%$datos%' 
		OR UPPER(Cl.razonSocial) LIKE '%$datos%'
		OR V.chasis LIKE '%$datos%' 
		OR UPPER(Cl.apellidosC) LIKE '%$datos%' 
		OR UPPER(CONCAT_WS('',Cl.nombre,Cl.apellidosC)) LIKE '%$datos%') 
		OR UPPER(r.nombreCliente) like '%$datos%'  LIMIT 180");
		return !empty($obtenerRegistroSubfiltro) ? $obtenerRegistroSubfiltro : false;
	}

	public function buscarParaFiltroEJE($datos)
	{
		global $session;
		$obtenerRegistroSubfiltro = self::sentencia("SELECT C.idcomision, C.idcliente , 
		C.idcanal, C.idvehiculo, C.idfacturacion, C.numeroReciboHonorario, 
		C.fechaRegistro, C.estado , C.numeroFacturacion, C.estadoPago, 
		C.estadoFacturacion,C.type_commission,C.trabajo,C.idrenovacion, V.chasis 
		FROM comisiones C 
		LEFT JOIN  clientes Cl ON Cl.idcliente=C.idcliente 
		LEFT JOIN canales CANL ON CANL.idcanal=C.idcanal  
		LEFT JOIN renovaciones r ON r.idrenovacion =C.idrenovacion
		LEFT JOIN vehiculos V ON C.idvehiculo = V.idvehiculo  
		WHERE  (C.numeroFacturacion LIKE '$datos' 
		OR C.numeroReciboHonorario LIKE '$datos') 
		OR (UPPER(Cl.nombre) LIKE '%$datos%' 
		OR UPPER(Cl.razonSocial) LIKE '%$datos%' 
		OR UPPER(Cl.apellidosC) LIKE '%$datos%'
		OR V.chasis LIKE '%$datos%' 
		OR UPPER(CONCAT_WS('',Cl.nombre,Cl.apellidosC)) LIKE '%$datos%' 
		OR CANL.ejecutivoCanal LIKE '%$datos%' 
		OR CANL.nombreCanal LIKE '%$datos%' 
		OR upper(r.nombreCliente) like '%$datos%')  
		AND C.idCreador='$session->idusuario' LIMIT 180");
		return !empty($obtenerRegistroSubfiltro) ? $obtenerRegistroSubfiltro : false;
	}

	public function buscarParaFiltroRecibosH($datos)
	{
		global $session;
		global $db;
		$datos = $db->escapeString($datos);
		$obtenerRegistroSubidoQ = self::sentencia("SELECT C.idcomision, C.idcliente , C.idcanal, C.idvehiculo, C.idfacturacion, C.numeroReciboHonorario, C.fechaRegistro, C.estado , C.numeroFacturacion, C.idCreador, C.reciboHonorarios, C.comision,C.trabajo   FROM comisiones C INNER JOIN  clientes Cl ON (Cl.idcliente=C.idcliente) INNER JOIN usuarios U ON (C.idCreador=U.idusuario) INNER JOIN canales CNL ON (CNL.idcanal=C.idcanal)  WHERE reciboHonorarios<>'' AND (C.numeroFacturacion LIKE '$datos' OR C.numeroReciboHonorario LIKE '$datos' OR Cl.nombre LIKE '$datos' OR Cl.razonSocial LIKE '$datos' OR Cl.apellidosC LIKE '$datos' OR CONCAT_WS('',Cl.nombre,Cl.apellidosC) LIKE '$datos' OR U.usuarioNombre LIKE '$datos' OR U.usuarioApellidoPaterno LIKE '$datos' OR CNL.ejecutivoCanal LIKE '$datos' OR CNL.nombreCanal LIKE '$datos') ");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;

	}
	public function obtenerRegistrosSubidos($posisionDePagina, $registrosPorPagina)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones C WHERE estadoFacturacion='34' AND C.estadoPago='67' AND ''<>(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) AND numeroReciboHonorario<>''  ORDER BY idcomision ASC LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}
	public function obtenerRegistrosSubidosCONRHORUC($posisionDePagina, $registrosPorPagina)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones C 
		WHERE  C.reciboHonorarios<>'' AND C.estado <>'67'  ORDER BY C.idcomision ASC LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}
	public function obtenerRegistrosSubidosEJE($posisionDePagina, $registrosPorPagina)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones C WHERE  idCreador='$session->idusuario'  ORDER BY idcomision DESC LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}

	public function obtenerRecibosSubidos($posisionDePagina, $registrosPorPagina)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones C WHERE reciboHonorarios<>''  ORDER BY idcomision ASC LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}

	public function obtenerRegistrosSubidosCantida()
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE estadoFacturacion='34' AND C.estadoPago='67' AND ''<>(SELECT ruc FROM canales CA WHERE C.idcanal=CA.idcanal) AND numeroReciboHonorario<>'' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}

	public function obtenerRegistrosSubidosCONRHORUCCANTIDAD()
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE  reciboHonorarios<>'' AND estado <>'67'  LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}

	public function obtenerRegistrosSubidosCantidaEJE()
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE idCreador='$session->idusuario' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}

	public function obtenerRecibosSubidosCantida()
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE  reciboHonorarios<>'' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}

	public function marcarComoPagado()
	{
		global $db;
		global $session;
		$this->fechaPagado = date("YmdHis");
		$sql = "UPDATE comisiones SET ";
		$sql .= " numeroCuentaPagado='";
		$sql .= $db->escapeString($this->numeroCuentaPagado) . "',";
		$sql .= " fechaPagado='";
		$sql .= $db->escapeString($this->fechaPagado) . "',";
		$sql .= " idPagador='";
		$sql .= $db->escapeString($session->idusuario) . "', ";
		$sql .= " estado='";
		$sql .= $db->escapeString($this->estado) . "', ";
		$sql .= " observacion='";
		$sql .= $db->escapeString($this->observacion) . "'  ";
		$sql .= " WHERE  idcomision='";
		$sql .= $db->escapeString($this->idcomision) . "'";
		return ($db->query($sql)) ? true : false;
	}

	public static function verificarExistencia($idfacturacion, $idvehiculo)
	{
		$idfacturacion = limpiar($idfacturacion);
		$idvehiculo = limpiar($idvehiculo);
		$verificarExistencia = self::sentencia("SELECT idcomision FROM comisiones WHERE idfacturacion='$idfacturacion' AND idvehiculo='$idvehiculo'  LIMIT 1");
		return !empty($verificarExistencia) ? array_shift($verificarExistencia) : false;
	}

	public function registrarEstadoPago()
	{
		global $db;
		global $session;
		$this->fechaVerificadorPago = date("YmdHis");
		$this->idVerficadorPago = $session->idusuario;
		$sql = "UPDATE comisiones SET ";
		$sql .= " estadoPago='";
		$sql .= $db->escapeString($this->estadoPago) . "',";
		$sql .= " idVerficadorPago='";
		$sql .= $db->escapeString($this->idVerficadorPago) . "', ";
		$sql .= " fechaVerificadorPago='";
		$sql .= $db->escapeString($this->fechaVerificadorPago) . "'";
		$sql .= " WHERE idcomision='";
		$sql .= $db->escapeString($this->idcomision) . "'";
		return ($db->query($sql)) ? true : false;
	}

	public function registrarEstadoFacturacion()
	{
		global $db;
		global $session;
		$this->fechaFacturador = date("YmdHis");
		$this->idFacturador = $session->idusuario;
		$sql = " UPDATE comisiones SET ";
		$sql .= " estadoFacturacion='";
		$sql .= $db->escapeString($this->estadoFacturacion) . "', ";
		$sql .= " idFacturador='";
		$sql .= $db->escapeString($this->idFacturador) . "' ,";
		$sql .= " fechaFacturador='";
		$sql .= $db->escapeString($this->fechaFacturador) . "'";
		$sql .= " WHERE idcomision='";
		$sql .= $db->escapeString($this->idcomision) . "'";
		return ($db->query($sql)) ? true : false;
	}
	// 291534
	public function registrarConvenio()
	{
		global $db;
		global $session;
		$this->fechaRegistro = date("YmdHis");
		$sql = "INSERT INTO comisiones (idcliente, idcanal, idconvenio, idvehiculo, idfacturacion, tiempo , montoTotal, comision,idCreador,fechapago, fechaRegistro ) VALUES ('";
		$sql .= $db->escapeString($this->idcliente) . "', '";
		$sql .= $db->escapeString($this->idcanal) . "', '";
		$sql .= $db->escapeString($this->idconvenio) . "', '";
		$sql .= $db->escapeString($this->idvehiculo) . "', '";
		$sql .= $db->escapeString($this->idfacturacion) . "', '";
		$sql .= $db->escapeString($this->tiempo) . "', '";
		$sql .= $db->escapeString($this->montoTotal) . "', '";
		$sql .= $db->escapeString($this->comision) . "', '";
		$sql .= $db->escapeString($session->idusuario) . "', '";
		$sql .= $db->escapeString($this->fechapago) . "', '";
		$sql .= $db->escapeString($this->fechaRegistro) . "') ";
		return ($db->query($sql)) ? true : false;
	}
	public static function verificarSiExisteConvenioMenu()
	{
		global $session;
		$verEMenu = self::sentencia("SELECT * FROM convenios WHERE idEjecutivoAsignado='$session->idusuario' LIMIT 1");
		return !empty($verEMenu) ? true : false;
	}
	public static function obtenerConvenios($idconvenio, $desde, $hasta)
	{
		$obteConvenios = self::sentencia("SELECT * FROM comisiones WHERE idconvenio='$idconvenio' AND fechaRegistro BETWEEN '$desde' AND '$hasta' ");
		return !empty($obteConvenios) ? $obteConvenios : false;

	}

	public function obtenerComisionesConveniosCantidad()
	{
		$obconCan = self::sentencia("SELECT COUNT(*) AS cantidad FROM comisiones WHERE idconvenio='$this->idconvenio' ");
		return !empty($obconCan) ? array_shift($obconCan) : false;
	}

	public function obtenerComisionesConveniosRANGOCantidad($desde, $hasta)
	{
		$obconCan = self::sentencia("SELECT COUNT(*) AS cantidad FROM comisiones WHERE idconvenio='$this->idconvenio' AND fechaRegistro BETWEEN '$desde' AND '$hasta'");
		return !empty($obconCan) ? array_shift($obconCan) : false;
	}


	public function obtenerComisionesConvenios($posisionDePagina, $registrosPorPagina)
	{
		$conve = self::sentencia("SELECT * FROM  comisiones  WHERE  idconvenio='$this->idconvenio' ORDER BY idcomision DESC LIMIT $posisionDePagina, $registrosPorPagina  ");
		return !empty($conve) ? $conve : false;
	}


	public function obtenerComisionesConveniosRANGO($posisionDePagina, $registrosPorPagina, $desde, $hasta)
	{
		$conve = self::sentencia("SELECT * FROM  comisiones  WHERE  idconvenio='$this->idconvenio' AND (fechaRegistro BETWEEN '$desde' AND '$hasta') ORDER BY idcomision DESC LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($conve) ? $conve : false;
	}

	public function buscarParaFiltroConvenios($datos)
	{
		global $session;
		global $db;
		$datos = $db->escapeString($datos);
		$buscarParaFiltroEntregaF = self::sentencia("SELECT C.idcliente, C.idcomision, C.idcanal, C.idconvenio, C.idvehiculo, C.idfacturacion, C.idCreador, C.fechaRegistro,C.trabajo FROM comisiones C LEFT JOIN usuarios U ON (C.idCreador=U.idusuario) LEFT JOIN clientes CL ON (CL.idcliente=C.idcliente) WHERE  idconvenio = '$this->idconvenio'  AND (numeroFacturacion LIKE '$datos' OR U.usuarioNombre LIKE '$datos' OR U.usuarioApellidoPaterno LIKE '$datos' OR CL.nombre LIKE '$datos' OR CL.razonSocial LIKE '$datos' OR CL.apellidosC LIKE '$datos' ) GROUP BY C.idcliente");
		return !empty($buscarParaFiltroEntregaF) ? $buscarParaFiltroEntregaF : false;

	}
	public function buscarParaFiltroConveniosFecha($datos, $desde, $hasta)
	{
		global $session;
		global $db;
		$datos = $db->escapeString($datos);
		$buscarParaFiltroEntregaF = self::sentencia("SELECT C.idcliente, C.idcomision, C.idcanal, C.idconvenio, C.idvehiculo, C.idfacturacion, C.idCreador, C.fechaRegistro,C.trabajo FROM comisiones C LEFT JOIN usuarios U ON (C.idCreador=U.idusuario) LEFT JOIN clientes CL ON (CL.idcliente=C.idcliente) WHERE  idconvenio = '$this->idconvenio' AND (fechaRegistro BETWEEN '$desde' AND '$hasta') AND (numeroFacturacion LIKE '$datos' OR U.usuarioNombre LIKE '$datos' OR U.usuarioApellidoPaterno LIKE '$datos' OR CL.nombre LIKE '$datos' OR CL.razonSocial LIKE '$datos' OR CL.apellidosC LIKE '$datos' ) GROUP BY C.idcliente ");
		return !empty($buscarParaFiltroEntregaF) ? $buscarParaFiltroEntregaF : false;

	}

	public static function calcularComisionConvenio($plan, $tiempo, $idconvenio)
	{
		if ($idconvenio == '1') {
			$resultado = self::sentencia("SELECT precioConvenio as comision FROM comisionConvenio WHERE plan='$plan' AND tiempo = '$tiempo' AND idconvenio='1' LIMIT 1");
			return !empty($resultado) ? array_shift($resultado) : false;
		}
		if ($idconvenio == '2') {
			$resultado = self::sentencia("SELECT precioConvenio, comision FROM comisionConvenio WHERE plan='$plan' AND tiempo = '$tiempo' AND idconvenio='2' LIMIT 1");
			return !empty($resultado) ? array_shift($resultado) : false;
		}
	}
	public function agregarIdCanal()
	{
		global $db;
		$sql = "UPDATE comisiones SET idcanal='$this->idcanal' WHERE idcomision='$this->idcomision'";
		$sql2 = "UPDATE datosFacturacion SET idcanal='$this->idcanal' WHERE datosfacturacionid='$this->idfacturacion'";
		return ($db->query($sql) && $db->query($sql2)) ? true : false;
	}

	public function agregarIdTitular()
	{
		global $db;
		$sql = "UPDATE comisiones SET idtitular='$this->idtitular' WHERE idcomision='$this->idcomision'";
		return ($db->query($sql)) ? true : false;
	}

	/////Actuallizar otra tabla xdd
	// public function actualizarIdCanalFacu(){
	// 	global $db;

	// }



	public static function obtenerCantidadListosEjeYfecha($idCreador, $desde, $hasta)
	{
		$obtenerComisionesRealizadasQ = self::sentencia("SELECT COUNT(*) as cantidad FROM comisiones C WHERE fechaRegistro BETWEEN '$desde' AND '$hasta' AND idCreador=$idCreador AND  estadoFacturacion='34' AND C.estadoPago='67'  AND numeroReciboHonorario<>'' AND estado = '59' ");
		return !empty($obtenerComisionesRealizadasQ) ? array_shift($obtenerComisionesRealizadasQ) : false;
	}

	public function obtenerRegistrosSubidosEfechaIdejecutioListo($posisionDePagina, $registrosPorPagina, $idEjecutivo, $desde, $hasta)
	{
		global $session;
		$obtenerRegistroSubidoQ = self::sentencia("SELECT * FROM comisiones C 
		WHERE C.fechaRegistro BETWEEN '$desde' AND '$hasta' 
		AND C.idCreador='$idEjecutivo' AND C.estadoFacturacion='34' 
		AND C.estadoPago='67' AND C.numeroReciboHonorario<>'' 
		AND C.estado = '59'  ORDER BY C.idcomision ASC 
		LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}


	public function buscarParaFiltroECOMM($datos, $idEjecutivo)
	{
		global $session;
		global $db;
		$datos = $db->escapeString($datos);
		$idEjecutivo = $db->escapeString($idEjecutivo);
		$obtenerRegistroSubidoQ = self::sentencia("SELECT C.idcomision, C.idcliente ,
		C.idrenovacion, C.idcanal, C.idvehiculo, C.idfacturacion, C.numeroReciboHonorario, 
		C.fechaRegistro, C.estado , C.numeroFacturacion , C.estadoPago, 
		C.estadoFacturacion,  C.idCreador,C.type_commission,C.trabajo, V.chasis 
		FROM comisiones C LEFT JOIN  clientes Cl ON Cl.idcliente=C.idcliente  
		LEFT JOIN renovaciones r ON r.idrenovacion =C.idrenovacion
		LEFT JOIN vehiculos V ON C.idvehiculo = V.idvehiculo  
		WHERE  ((C.numeroFacturacion LIKE '$datos' 
		OR C.numeroReciboHonorario LIKE '$datos') 
		OR (UPPER(Cl.nombre) LIKE '%$datos%' 
		OR UPPER(Cl.razonSocial) LIKE '%$datos%'
		 OR V.chasis LIKE '%$datos%' 
		OR UPPER(Cl.apellidosC) LIKE '%$datos%' 
		OR UPPER(CONCAT_WS('',Cl.nombre,Cl.apellidosC)) LIKE '%$datos%') 
		OR UPPER(r.nombreCliente) like '%$datos%' ) AND C.idCreador='$idEjecutivo' ");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}

	public function verificarSiexiste()
	{
		$sresVeEx = self::sentencia("SELECT * FROM comisiones WHERE idfacturacion='$this->idfacturacion' ");
		return !empty($sresVeEx) ? true : false;
	}


	public function verTrabajoComision()
	{
		$sresVeEx = self::sentencia("SELECT trabajo FROM comisiones WHERE idfacturacion='$this->idfacturacion' ");
		return !empty($sresVeEx) ? array_shift($sresVeEx) : false;
	}
	// public function verTrabajoComision(){
	// 	$sresVeEx = self::sentencia("SELECT * FROM comisiones WHERE idfacturacion='$this->idfacturacion' ");
	// 	return !empty($sresVeEx) ? array_shift($sresVeEx) : null;
	// }


	public function marcarComoAnulado($ultimoid, $idopera)
	{
		global $db;
		$datosOld = new SeguimientoConsulta();
		$dOld = $datosOld->consultaSeg("comisiones", $this->idfacturacion, "idfacturacion");
		$dOldd = $dOld;
		$sqlMa = "UPDATE comisiones SET estado='18' , estadoPago='18' , estadoFacturacion='18' , observacion='$this->observacion' WHERE idfacturacion ='$this->idfacturacion' ";
		if ($db->query($sqlMa)) {
			new SeguimientoTablas("comisiones", " Marco como Anulado", $this->idfacturacion, $this, "idfacturacion", $dOldd, $ultimoid, $idopera);
			return true;
		} else {
			return false;
		}
		//        return ($db->query($sqlMa))?true:false;
	}
	public function actualizarDatosCom($ultimoid, $idopera)
	{
		global $db;
		$datosOld = new SeguimientoConsulta();
		$dold = $datosOld->consultaSeg("comisiones", $this->idfacturacion, "idfacturacion");
		$doldd = $dold;
		$actCom = "UPDATE comisiones SET montoTotal='$this->montoTotal',tiempo='$this->tiempo', idCreador='$this->idCreador' WHERE idfacturacion='$this->idfacturacion'";
		if ($db->query($actCom)) {
			new SeguimientoTablas("comisiones", " Modifico datos", $this->idfacturacion, $this, "idfacturacion", $doldd, $ultimoid, $idopera);
			return true;
		} else {
			return false;
		}
		// return ($db->query($actCom))?true:false;
	}

	public function cambiarRH()
	{
		global $db;
		$actCom = "UPDATE comisiones SET reciboHonorarios='', estado = '0', numeroReciboHonorario='' WHERE idcomision='$this->idcomision'";
		return ($db->query($actCom)) ? true : false;
	}

	public function eliminarComision()
	{
		global $db;
		$actCom = "DELETE FROM comisiones WHERE idcomision='$this->idcomision'";
		return ($db->query($actCom)) ? true : false;
	}
	public function cambiarTituar()
	{
		global $db;
		$actCom = "UPDATE comisiones SET idcanal='',idtitular='', estado = '0', numeroReciboHonorario='' WHERE idcomision='$this->idcomision'";
		return ($db->query($actCom)) ? true : false;
	}

	public function cambiarTituarTC()
	{
		global $db;
		$actCom = "UPDATE comisiones SET idcanal='',idtitular='', estado = '0', numeroReciboHonorario='',type_commission='$this->type_commission' WHERE idcomision='$this->idcomision'";
		return ($db->query($actCom)) ? true : false;
	}

	public function cambiarTitularNew(){
		global $db;
		$actCom = "UPDATE comisiones SET idcanal='',idtitular='', numeroReciboHonorario='' WHERE idcomision='$this->idcomision'";

		return ($db->query($actCom)) ? true : false;
	}

	public function borrarIDCANAL()
	{
		global $db;
		$actCom = "UPDATE comisiones SET idcanal='0'  WHERE idcomision='$this->idcomision'";
		return ($db->query($actCom)) ? true : false;
	}

	public static function obtenerComisioensTOMAIL()
	{
		$obca = self::sentencia("SELECT CO.* FROM comisiones CO INNER JOIN titular CA ON(CO.idtitular=CA.idtitular)  WHERE estadoFacturacion='34' AND (estadoPago='67' OR estadoPago='71') AND numeroReciboHonorario<>'' AND CO.estado = '59' GROUP BY CO.idtitular ORDER BY CA.banco DESC");
		return !empty($obca) ? $obca : false;
	}
	public static function obtenerComisionesDeIdcanal($idcanal)
	{
		$obca = self::sentencia("SELECT * FROM comisiones CO WHERE idcanal='$idcanal' AND estadoFacturacion='34' AND (estadoPago='67' OR estadoPago='71') AND numeroReciboHonorario<>'' AND CO.estado = '59' ");
		return !empty($obca) ? $obca : false;
	}

	public static function obtenerComisionesDeidTitular($idcanal)
	{
		$obca = self::sentencia("SELECT * FROM comisiones CO WHERE idtitular='$idcanal' AND estadoFacturacion='34' AND (estadoPago='67' OR estadoPago='71') AND numeroReciboHonorario<>'' AND CO.estado = '59' ");
		return !empty($obca) ? $obca : false;
	}

	public function obtenerComisionesExcel($fechaInicio, $fechaFin)
	{
		$sql = "SELECT CONCAT(usuarios.usuarioApellidoPaterno, ' ', usuarios.usuarioApellidoMaterno,' ',usuarios.usuarioNombre) as ejecutivo,
DATE_FORMAT( datosFacturacion.fechaCreada,'%Y-%m-%d' ) as  fechapagocliente,
CONCAT(clientes.apellidosC,' ', clientes.nombre) as cliente,
if(datosFacturacion.tipoCanal='',canales.tipoCanal,datosFacturacion.tipoCanal) as tipocanal,
if(datosFacturacion.nombreCanal='',canales.nombreCanal,datosFacturacion.nombreCanal) as nombrecanal,
if(datosFacturacion.ejecutivoCanal='',canales.ejecutivoCanal,datosFacturacion.ejecutivoCanal) as ejecutivocanal,
datosFacturacion.plan as sistema,
1 as cantidad,
cast(datosFacturacion.costo as decimal(12,2)) as preciosinigv, 
SinIGV(datosFacturacion.costo) as precioconigv, 
cast(comisiones.comision as decimal(12,2)) comision,
Concat(datosFacturacion.numeroBoleta,'',datosFacturacion.numeroFactura) boletafatura,
if(titular.titular is null,canales.titular,titular.titular) emisiorrecibo,
CONCAT('',if(if(CHAR_LENGTH(trim(titular.ruc))>8,trim(titular.ruc),trim(titular.ruc)) is null,
 if(CHAR_LENGTH(trim(canales.ruc))>8,(trim(canales.ruc)),trim(canales.ruc)),
 if(CHAR_LENGTH(trim(titular.ruc))>8,(trim(titular.ruc)),trim(titular.ruc)))) dni,
#titular.ruc,
if(concat(titular.ruc,'-',comisiones.numeroReciboHonorario) is null ,concat(canales.ruc,comisiones.numeroReciboHonorario),concat(titular.ruc,'-',comisiones.numeroReciboHonorario) ) rucrecibo,
comisiones.numeroReciboHonorario numeroreciboporhonorarios,
if(CONCAT(titular.banco,'-',if(titular.numeroCuenta = '',titular.cci,titular.numeroCuenta)) is null ,concat(canales.banco,' - ',if(canales.numeroCuenta='',canales.cci,canales.numeroCuenta)),CONCAT(titular.banco,'-',if(titular.numeroCuenta = '',titular.cci,titular.numeroCuenta))) nbanco,
if(
	if(titular.numeroCuenta = '',titular.cci,titular.numeroCuenta) is null ,
	if(canales.numeroCuenta='',canales.cci,canales.numeroCuenta),if(titular.numeroCuenta = '',titular.cci,titular.numeroCuenta)
) numero,
if(titular.moneda is null,canales.moneda,titular.moneda) moneda,
DATE_FORMAT( comisiones.fechaPagado,'%Y-%m-%d' ) fechapagocomision,comisiones.idcomision,
CASE WHEN comisiones.type_commission = 1 THEN 'Tarjeta' WHEN comisiones.type_commission = 2 then 'Factura' else '' END as formapagocomision,trabajo
from comisiones
LEFT join usuarios on (comisiones.idCreador = usuarios.idusuario)
LEFT join datosFacturacion on (comisiones.idfacturacion=datosFacturacion.datosfacturacionid)
LEFT join clientes on (comisiones.idcliente=clientes.idcliente)
left join titular on (comisiones.idtitular=titular.idtitular)
left join canales on (comisiones.idcanal=canales.idcanal)
where 1=1 -- and comisiones.type_commission is not null 
  and comisiones.estado=59
-- and comisiones.fechaPagado BETWEEN '$fechaInicio' AND '$fechaFin' 
order by comisiones.fechaPagado
";

		$dtp = self::sentencia($sql);
		return !empty($dtp) ? $dtp : false;
	}

}


?>
<?php
class DatosTablasClass extends Recursos{
    function __construct()
	{		
	}

    /**
     * clientes
     */
    function datosClientes($id=null){
        $sqlWhere = is_null($id)? "" : " WHERE idcliente={$id}";
        $result=self::sentenciaV2("SELECT * FROM  clientes {$sqlWhere} ORDER BY idcliente ASC");
		return !empty($result)? $result:[];	
    }

    function datosClientesPorNombre($cliente_nombre=null){
        global $db;
		global $session;
        $result=self::sentenciaV2("SELECT 
                                    *,
                                    IF(clientes.tipoCliente='Natural', CONCAT_WS(' ', clientes.apellidosC, clientes.nombre), clientes.razonSocial)  AS cliente_nombre
                                    FROM  
                                    clientes 
                                    having
                                    LOWER(cliente_nombre) = LOWER('".$db->escapeString($cliente_nombre)."')");
		return !empty($result)? $result:[];	
    }

    /**
     * vehiculos
     */
    function datosVehiculos($idcliente=null, $idvehiculo=null){
        if(is_null($idcliente)){
            $sqlWhere = "";
        }else{
            $idClt = intval($idcliente);
            $sqlWhere = " AND idcliente = {$idClt}";
        }

        if(is_null($idvehiculo)){
            $sqlWhere .= "";
        }else{
            $idVeh = intval($idvehiculo);
            $sqlWhere .= " AND idvehiculo = {$idVeh}";
        }
        // $sqlWhere = ? "" : " AND idcliente={$idcliente} ";
        // $sqlWhere .= is_null($idvehiculo)? "" : " AND idvehiculo={$idvehiculo} ";
        $query = "SELECT * FROM vehiculos WHERE 1=1 {$sqlWhere} ORDER BY idcliente ASC";
        $result=self::sentenciaV2($query);
		return !empty($result)? $result:[];	
    }

    function datosVehiculosPorPlaca($vehiculo_placa=null){
        global $db;
		global $session;
        if($vehiculo_placa != ""){
            $result=self::sentenciaV2("SELECT 
                                    *
                                    FROM  
                                    vehiculos 
                                    WHERE
                                    LOWER(placa) = LOWER('".$db->escapeString($vehiculo_placa)."')");

            $response = !empty($result) ? $result : self::datosVehiculosPorChasis($vehiculo_placa);

        }else{
            $response = [];
        }

		return $response;	

        
    }

    function datosVehiculosPorChasis($vehiculo_chasis=null){
        global $db;
		global $session;
        $result=self::sentenciaV2("SELECT 
                                    *
                                    FROM  
                                    vehiculos 
                                    WHERE
                                    LOWER(chasis) = LOWER('".$db->escapeString($vehiculo_chasis)."')");
		return !empty($result)? $result:[];	
    }

    /**
     * Reclamo solictud
     */
    function datosReclamoSolicitud($id=null){
        $sqlWhere = is_null($id)? "" : " WHERE idreclamo_solicitud={$id}";
        $result=self::sentenciaV2("SELECT * FROM  reclamo_solicitud {$sqlWhere} ORDER BY vdescripcion ASC");
		return !empty($result)? $result:[];	
    }

    /**
     * Reclamo estado
     */
    function datosReclamoEstado($id=null){
        $sqlWhere = is_null($id)? "" : " WHERE idreclamo_estado={$id}";
        $result=self::sentenciaV2("SELECT * FROM  reclamo_estado {$sqlWhere} ORDER BY vdescripcion ASC");
		return !empty($result)? $result:[];	
    }

    function datosReclamoEstadoPorNombre($nombre=null){
        global $db;
		global $session;
        $result=self::sentenciaV2("SELECT * FROM  reclamo_estado WHERE LOWER(vdescripcion)=LOWER('".$db->escapeString($nombre)."') ORDER BY vdescripcion ASC");
		return !empty($result)? $result:[];	
    }

    /**
     * Reclamo Canal
     */
    function datosReclamoCanal($id=null){
        $sqlWhere = is_null($id)? "" : " WHERE idreclamo_canal={$id}";
        $result=self::sentenciaV2("SELECT * FROM  reclamo_canal {$sqlWhere} ORDER BY vdescripcion ASC");
		return !empty($result)? $result:[];	
    }

    function datosReclamoCanalPorNombre($nombre=null){
        global $db;
		global $session;
        $result=self::sentenciaV2("SELECT * FROM  reclamo_canal WHERE LOWER(vdescripcion)=LOWER('".$db->escapeString($nombre)."') ORDER BY vdescripcion ASC");
		return !empty($result)? $result:[];	
    }

    /**
     * Reclamo Nivel Atencion
     */
    function datosReclamoNivelAtencion($id=null){
        $sqlWhere = is_null($id)? "" : " WHERE idreclamo_nivel_atencion={$id}";
        $result=self::sentenciaV2("SELECT * FROM  reclamo_nivel_atencion {$sqlWhere} ORDER BY vdescripcion ASC");
		return !empty($result)? $result:[];	
    }

    function datosReclamoNivelAtencionPorNombre($nombre=null){
        global $db;
		global $session;
        $result=self::sentenciaV2("SELECT * FROM  reclamo_nivel_atencion WHERE LOWER(vdescripcion)=LOWER('".$db->escapeString($nombre)."') ORDER BY vdescripcion ASC");
		return !empty($result)? $result:[];	
    }

    /**
     * Reclamo plazo
     */
    function datosReclamoPlazo($id=null){
        $sqlWhere = is_null($id)? "" : " WHERE idreclamo_plazo={$id}";
        $result=self::sentenciaV2("SELECT * FROM  reclamo_plazo {$sqlWhere} ORDER BY vdescripcion ASC");
		return !empty($result)? $result:[];	
    }

    function datosReclamoPlazoPorNombre($nombre=null){
        global $db;
		global $session;
        $result=self::sentenciaV2("SELECT * FROM  reclamo_plazo WHERE LOWER(vdescripcion)=LOWER('".$db->escapeString($nombre)."') ORDER BY vdescripcion ASC");
		return !empty($result)? $result:[];	
    }

    /**
     * Areas
     */
    function datosAreas($id=null){
        $sqlWhere = (is_null($id) || trim($id)=="")? "" : " AND r.idrol={$id}";
        $result=self::sentenciaV2("SELECT r.*, CONCAT_WS(' - ', r.area, r.cargo) AS area FROM  roles r WHERE 1=1 {$sqlWhere} ORDER BY area ASC");
		return !empty($result)? $result:[];
    }

    /**
     * Usuarios - Areas
     */
    function datosUsuariosArea($idrol=null, $idusuario=null){
        $sqlWhere = (is_null($idrol) || trim($idrol)=="")? "" : " AND u.idrol={$idrol}";
        $sqlWhere .= (is_null($idusuario) || trim($idusuario)=="")? "" : " AND u.idusuario={$idusuario}";
        $result=self::sentenciaV2("SELECT 
                                    u.idusuario,
                                    u.idrol,
                                    u.usuarioNombre,
                                    u.usuarioApellidoPaterno,
                                    u.usuarioApellidoMaterno,
                                    CONCAT_WS(' ', u.usuarioNombre, u.usuarioApellidoPaterno, u.usuarioApellidoMaterno) AS usuario_nombre
                                    FROM  
                                    usuarios u 
                                    WHERE 1=1 
                                    {$sqlWhere} 
                                    ORDER BY usuario_nombre ASC");                           
		return !empty($result)? $result:[];
    }

    function correoDeAreaPorPermisoAlModulo($id=null){
		$result=self::sentenciaV2("SELECT o.idusuario, u.usuario AS correotrabajador, r.area AS arearesponsable, r.cargo AS nombredelcargo, r.email AS correoarea  FROM opciones o
		INNER JOIN usuarios u ON o.idusuario = u.idusuario
		INNER JOIN roles r ON u.idrol = r.idrol
		WHERE u.idusuario='$id'");
		return !empty($result)? $result:[];

            // $sqlWhere = (is_null($id) || trim($id)=="")? "" : " AND r.idrol={$id}";
            // $result=self::sentenciaV2("SELECT o.idusuario, u.usuario AS correotrabajador, r.area AS arearesponsable, r.email AS correoarea  FROM opciones o
            // INNER JOIN usuarios u ON o.idusuario = u.idusuario
            // INNER JOIN roles r ON u.idrol = r.idrol
            // WHERE o.enviarcredenciales='si' and 1=1 {$sqlWhere}");
            // return !empty($result)? $result:[];
    
	}

    /**
     * Buscar Usuario por nombre y Area nombre
     */
    function datosUsuarioPorNonbreYAreaPorNombre($usuario_nombre, $area_nombre){
        global $db;
		global $session;
        $result=self::sentenciaV2("SELECT
                                    u.idrol,
                                    u.idusuario,
                                    CONCAT_WS(' ', u.usuarioNombre, u.usuarioApellidoPaterno, u.usuarioApellidoMaterno) AS usuario_nombre,
                                    CONCAT_WS(' - ', r.area, r.cargo) AS area_nombre
                                    FROM
                                    usuarios u
                                    LEFT JOIN roles r on u.idrol = r.idrol
                                    HAVING
                                        LOWER(usuario_nombre) = LOWER('".$db->escapeString($usuario_nombre)."')
                                        AND LOWER(area_nombre) = LOWER('".$db->escapeString($area_nombre)."');");
        return !empty($result)? $result:[];
    }

    // function getLastUidReclamo(){
    //     $sql = "SELECT vreclamo_cliente FROM reclamo_cliente ORDER BY idreclamo_cliente DESC LIMIT 1";
    //     $full_uid = self::sentenciaV2($sql);
    //     // $full_uid = mysqli_fetch_array($result, MYSQLI_BOTH);
    //     $r = intval(trim(substr($full_uid['vreclamo_cliente'], 1)));
    //     $uid = "R".strval(++$r);
    //     return $uid;
    // }

    function getLastUidReclamo(){
        $sql = "SELECT vreclamo_cliente FROM reclamo_cliente ORDER BY idreclamo_cliente DESC LIMIT 1";
        $full_uid = self::sentenciaV2($sql);
        
        $lastValue = isset($full_uid[0]->vreclamo_cliente) ? $full_uid[0]->vreclamo_cliente : 'R0'; // Por defecto, si no hay resultado anterior.
        
        $r = intval(trim(substr($lastValue, 1)));
        
        $uid = "R".strval(++$r);
        
        return $uid;
    }
    
    
}   
?>
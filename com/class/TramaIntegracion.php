<?php 
class TramaIntegracion extends Recursos
{
    public $idtrama ;

    public $otros ;
    public $fecha_registro ;
    public $tipo_documento ;
    public $body;
    public $fecha;
    public $num_documento;
    public $nombre_asegurado;
    public $apellido_asegurado;
    public $ubicacion_telefono;
    public $num_telefono;
    public $celular;
    public $direccion;
    public $correo;
    public $departamento;
    public $placa;
    public $chasis;
    public $chasisC;
    public $motor;
    public $marca;
    public $modelo;
    public $color;
    public $anio;
    public $tipo_transaccion;
    public $numero_plaza;
    public $oferta;
    public $fecha_inicio;
    public $fecha_fin;
    public $uso;
    public $plan_pagos;
    public $prima_fija_anual;
    public $prima_fija_mensual;
    public $prima_variable;
    public $fecha_corte;
    public $fecha_fact;
    public $fecha_vencimiento;
    public $idvehicle;
    public $idvoucher;
    public $idoperacion;
    public $precio_lista;
    public $tiempo;
    public $compania;
    public $plan;
    public $servicio_adicional;
    public $campana;
    public $ejecutivo;
    public $tipoServicio;
    public $canal;
    public $canal_nombre;
    public $ejecutivo_canal;
    public $cobra_comision;
    public $forma_pago;
    public $doc_factura;
    public $ruc_factura;
    public $razonSocial_Factura;
    public $direccion_factura;
    public $numero_fatura;
    public $numero_i;
    public $contacto_i;
    public $estado;
    public $idusuario;
    public $idsuariocargo;
    public $cantidad;
    public $policy;
    public $completadov;
    public $catidadregv;
    public $statusv;
    public $asegurado;
    public $estadoOpera;
    public $fechaInstalacion;
    public $seguimiento;
    public $statusseg;

    public $groupseg;

    public $descseg0_5;
    public $seg0_5;

    public $descseg6_10;
    public $seg6_10;

    public $descseg11_30;
    public $seg11_30;

    public $descseg30;
    public $segmas30;

    public $atraso;
    public $creation_at;
    public $reasignacion;
    public $cant_reasignacion;
    function __construct()
    {

    }

    public function verifyChasisCorrected(){
        global $db;
        $trama = self::obtenerTramaIntegracion($this->idtrama,'');
        if($trama->chasis != $this->chasis){
            $sqlicid="UPDATE tramaIntegrancion SET chasisC='$this->chasis' WHERE idtrama='$this->idtrama'";
            return ($db->query($sqlicid))?true:false;
        }
    }

    public function updateTrama(){
        global $db;
        $sqlicid="UPDATE tramaIntegrancion SET idoperacion='$this->idoperacion' ,reasignacion = '$this->reasignacion' WHERE idtrama='$this->idtrama'";
        return ($db->query($sqlicid))?true:false;
    }

    public function updateTrama2(){
        global $db;
        $sqlicid="UPDATE tramaIntegrancion SET chasis=TRIM('$this->chasis') , placa=TRIM('$this->placa') WHERE idtrama='$this->idtrama'";
        return ($db->query($sqlicid))?true:false;
    }

    public static function obtenerTramaIntegracion($idtrama,$datos){
        global $db;
        if($idtrama<>''){
            $idtrama=' and idtrama='.$idtrama.'';
        }

        if($datos<>''){
           $datos="and (
                                 (CONCAT(apellido_asegurado,' ',nombre_asegurado) like '%".$datos."%') or (placa like '%".$datos."%') or (marca  like '%".$datos."%') or (modelo  like '%".$datos."%') or (departamento  like '%".$datos."%') or (tipo_transaccion  like '%".$datos."%') or (fecha_registro  like '%".$datos."%') 
                                  )";
        }

        $obj=self::sentencia("SELECT otros, idtrama, fecha_registro, tipo_documento, num_documento,reasignacion, nombre_asegurado, apellido_asegurado, ubicacion_telefono, coalesce(num_telefono,'')num_telefono,coalesce(celular,'')celular, direccion,LOWER(correo) as correo, departamento, placa, chasis, motor, marca, modelo, color, anio, tipo_transaccion, numero_plaza, oferta, fecha_inicio, fecha_fin, uso, plan_pagos, prima_fija_anual, prima_fija_mensual, prima_variable, fecha_corte, fecha_fact, fecha_vencimiento, idvehicle, idvoucher, idoperacion, precio_lista, tiempo, compania, plan, servicio_adicional, campana, ejecutivo, tipoServicio, canal, canal_nombre, ejecutivo_canal, cobra_comision, forma_pago, ruc_factura, razonSocial_Factura, direccion_factura, numero_fatura,doc_factura, estado, idusuario,contacto_i,numero_i,idsuariocargo,policy,statusseg
                                FROM  tramaIntegrancion 
                                 where 1=1   ".$datos."  ".$idtrama."  
                                 ");
        return !empty($obj)?array_shift($obj):false;
    }


    public static function obtenerTramaIntegracionbyOperacion($idoperacion){
        global $db;
        if($idoperacion<>''){
            $idoperacion=' and idoperacion='.$idoperacion.'';
        }

        $obj=self::sentencia("SELECT idtrama, fecha_registro, tipo_documento, num_documento, nombre_asegurado, apellido_asegurado, ubicacion_telefono, coalesce(num_telefono,'')num_telefono,coalesce(celular,'')celular, direccion, correo, departamento, placa, chasis, motor, marca, modelo, color, anio, tipo_transaccion, numero_plaza, oferta, fecha_inicio, fecha_fin, uso, plan_pagos, prima_fija_anual, prima_fija_mensual, prima_variable, fecha_corte, fecha_fact, fecha_vencimiento, idvehicle, idvoucher, idoperacion, precio_lista, tiempo, compania, plan, servicio_adicional, campana, ejecutivo, tipoServicio, canal, canal_nombre, ejecutivo_canal, cobra_comision, forma_pago, ruc_factura, razonSocial_Factura, direccion_factura, numero_fatura,doc_factura, estado, idusuario,contacto_i,numero_i,idsuariocargo,policy
                                FROM  tramaIntegrancion 
                                 where 1=1   ".$idoperacion."  
                                 ");
        return !empty($obj)?array_shift($obj):false;
    }



    public static function verEstadoOperacion($idoperacion){
        global $db;

        $obj=self::sentencia("select estado from operaciones where idoperacion=$idoperacion ");
        return !empty($obj)?array_shift($obj):false;
    }

    public static function obtenerTramaIntegracionPagn($datos){
        global $db;

        if($datos<>''){
            $datos="and (
                                 (CONCAT(apellido_asegurado,' ',nombre_asegurado) like '%".$datos."%') or (placa like '%".$datos."%') or (marca  like '%".$datos."%') or (modelo  like '%".$datos."%') or (departamento  like '%".$datos."%') or (tipo_transaccion  like '%".$datos."%') or (fecha_registro  like '%".$datos."%') 
                                  )";
        }

        $obj=self::sentencia("SELECT otros, idtrama, fecha_registro, tipo_documento, num_documento, nombre_asegurado, apellido_asegurado, ubicacion_telefono, coalesce(num_telefono,'')num_telefono,coalesce(celular,'')celular, direccion, correo, departamento, placa, chasis, motor, marca, modelo, color, anio, tipo_transaccion, numero_plaza, oferta, fecha_inicio, fecha_fin, uso, plan_pagos, prima_fija_anual, prima_fija_mensual, prima_variable, fecha_corte, fecha_fact, fecha_vencimiento, idvehicle, idvoucher, idoperacion, precio_lista, tiempo, compania, plan, servicio_adicional, campana, ejecutivo, tipoServicio, canal, canal_nombre, ejecutivo_canal, cobra_comision, forma_pago, ruc_factura, razonSocial_Factura, direccion_factura, numero_fatura,doc_factura, estado, idusuario,contacto_i,numero_i,idsuariocargo,policy
                                FROM  tramaIntegrancion 
                                 where 1=1 and idoperacion is null and estado <> 99  ".$datos."    
                                 ");
        return !empty($obj)?$obj:false;
    }

    public static function obtenerTramaIntegracionPag($posisionDePagina,$registrosPorPagina){
        $obj=self::sentencia("SELECT idtrama, fecha_registro, tipo_documento, num_documento, nombre_asegurado, apellido_asegurado, ubicacion_telefono, num_telefono, celular, direccion, correo, departamento, placa, chasis, motor, marca, modelo, color, anio, tipo_transaccion, numero_plaza, oferta, fecha_inicio, fecha_fin, uso, plan_pagos, prima_fija_anual, prima_fija_mensual, prima_variable, fecha_corte, fecha_fact, fecha_vencimiento, idvehicle, idvoucher, idoperacion, precio_lista, tiempo, compania, plan, servicio_adicional, campana, ejecutivo, tipoServicio, canal, canal_nombre, ejecutivo_canal, cobra_comision, forma_pago, ruc_factura, razonSocial_Factura, direccion_factura, numero_fatura, doc_factura,estado, idusuario,contacto_i,numero_i,idsuariocargo,policy
                                FROM tramaIntegrancion where 1=1 and idoperacion is null and estado <> 99   order by fecha_registro desc 
                                 LIMIT $posisionDePagina, $registrosPorPagina 
                               ");
        return !empty($obj)?$obj:false;
    }
    public static function obtenerCantidadTramaIntegracionPag(){
        $obj=self::sentencia("SELECT COUNT(*) as cantidad  FROM tramaIntegrancion where 1=1 and idoperacion is null and estado <> 99  LIMIT 1 ");
        return !empty($obj)? array_shift($obj):false;
    }

    public static function obtenerCantidadTramaIntegracionPagGENERAL(){
        $obj=self::sentencia("SELECT COUNT(*) as cantidad  FROM tramaIntegrancion where 1=1 LIMIT 1 ");
        return !empty($obj)? array_shift($obj):false;
    }

    public static function obtenerTramaIntegracionPagGENERAL($posisionDePagina,$registrosPorPagina){
        $obj=self::sentencia("SELECT DISTINCT tyi.*,
        COUNT(vehicle_validation.status) catidadregv,
        vehicle_validation.completed completadov,
        CASE WHEN TIMESTAMPDIFF(DAY, tyi.fecha_registro,tyi.fecha_inicio) < 0 THEN CONCAT(TIMESTAMPDIFF(DAY, tyi.fecha_inicio, tyi.fecha_registro),' dia(s)') ELSE '' END atraso,
        CASE WHEN tyi.reasignacion = 1 THEN 'SI' ELSE 'NO' END reasignacion,
        (SELECT COUNT(*) FROM tramaIntegrancion typ WHERE typ.chasis = tyi.chasis) cant_reasignacion
        FROM tramaIntegrancion tyi 
        LEFT JOIN vehicle_validation ON (vehicle_validation.idoperation=tyi.idoperacion)
        WHERE 1=1
        GROUP BY tyi.idtrama, vehicle_validation.completed
        LIMIT $posisionDePagina, $registrosPorPagina");
        return !empty($obj)?$obj:false;
    }

    public static function obtenerTramasPorReasignacion($posisionDePagina,$registrosPorPagina){
        $obj=self::sentencia("SELECT DISTINCT tyi.*,
        COUNT(vehicle_validation.status) catidadregv,
        vehicle_validation.completed completadov,
        CASE WHEN TIMESTAMPDIFF(DAY, tyi.fecha_registro,tyi.fecha_inicio) < 0 THEN CONCAT(TIMESTAMPDIFF(DAY, tyi.fecha_inicio, tyi.fecha_registro),' dia(s)') ELSE '' END atraso,
        CASE WHEN tyi.reasignacion = 1 THEN 'SI' ELSE 'NO' END reasignacion,
        (SELECT COUNT(*) FROM tramaIntegrancion typ WHERE typ.chasis = tyi.chasis) as cant_reasignacion
        FROM tramaIntegrancion tyi 
        LEFT JOIN vehicle_validation ON (vehicle_validation.idoperation=tyi.idoperacion)
        WHERE 1=1
        AND (SELECT COUNT(*) FROM tramaIntegrancion typ WHERE typ.chasis = tyi.chasis) > 1
        GROUP BY tyi.idtrama
        LIMIT $posisionDePagina, $registrosPorPagina");
        return !empty($obj)?$obj:false;
    }

    public static function obtenerBUSQUEDAPagGENERAL($data){
        $obj=self::sentencia("SELECT DISTINCT tyi.*,
        COUNT(vehicle_validation.status) catidadregv,
        vehicle_validation.completed completadov,
        CASE WHEN TIMESTAMPDIFF(DAY, tyi.fecha_registro,tyi.fecha_inicio) < 0 THEN CONCAT(TIMESTAMPDIFF(DAY, tyi.fecha_inicio, tyi.fecha_registro),' dia(s)') ELSE '' END atraso,
        CASE WHEN tyi.reasignacion = 1 THEN 'SI' ELSE 'NO' END reasignacion,
        (SELECT COUNT(*) FROM tramaIntegrancion typ WHERE typ.chasis = tyi.chasis) cant_reasignacion
        FROM tramaIntegrancion tyi 
        LEFT JOIN vehicle_validation ON (vehicle_validation.idoperation=tyi.idoperacion)
        WHERE 1=1 and placa like '$data' or apellido_asegurado  like '$data' or nombre_asegurado like '$data' or numero_plaza like '$data' or tyi.chasis like '$data'    LIMIT 25 
                                 ");
        return !empty($obj)?$obj:false;
    }


    public static function obtenerBUSQUEDAPagGENERAL2($data){
        $obj=self::sentencia("SELECT DISTINCT tyi.*,
        COUNT(vehicle_validation.status) catidadregv,
        vehicle_validation.completed completadov,
        CASE WHEN TIMESTAMPDIFF(DAY, tyi.fecha_registro,tyi.fecha_inicio) < 0 THEN CONCAT(TIMESTAMPDIFF(DAY, tyi.fecha_inicio, tyi.fecha_registro),' dia(s)') ELSE '' END atraso,
        CASE WHEN tyi.reasignacion = 1 THEN 'SI' ELSE 'NO' END reasignacion,
        (SELECT COUNT(*) FROM tramaIntegrancion typ WHERE typ.chasis = tyi.chasis) cant_reasignacion
        FROM tramaIntegrancion tyi 
        LEFT JOIN vehicle_validation ON (vehicle_validation.idoperation=tyi.idoperacion)
        WHERE 1=1
        and placa like '$data' or apellido_asegurado  like '$data' or nombre_asegurado like '$data' or numero_plaza like '$data' or tyi.chasis like '$data'  
        GROUP BY tyi.idtrama
        LIMIT 25 
                                 ");
        return !empty($obj)?$obj:false;
    }

    public static function getImages($data){
        $obj=self::sentencia("SELECT creation_at fecha_registro ,detail statusv , bucket tipo_documento, name nombre_asegurado ,`type` apellido_asegurado from vehicle_images vi where vehicle_validation_id=$data and name <>'0'  ");
        return !empty($obj)?$obj:false;
    }

    public static function getIds($data){
        $obj=self::sentencia("SELECT id as idtrama, status tipo_documento,creation_at recha_registro,tec_doc nombre_asegurado from vehicle_validation vv where idoperation=$data");
        return !empty($obj)?$obj:false;
    }



    public static function asignarTrama($idTrama){
        global $session;
        global $db;
        $sql="UPDATE tramaIntegrancion SET idsuariocargo='".$session->idusuario."' WHERE idtrama ='".$idTrama."' ";
        if ($db->query($sql)) {

        }
    }

    public static function getSEg($idtrama){
        $obj=self::sentencia("select * from seguimientos_detalle where class_id='$idtrama' and class = 'TramaIntegracion'");
        return !empty($obj)?$obj:[];
    }


    public static function getSEgRes($idtrama,$groupseg){
        $obj=self::sentencia("select * from seguimientos_detalle where class_id='$idtrama' and class = 'TramaIntegracion' and groupseg ='$groupseg' ");
        return !empty($obj)?$obj:[];
    }

    public static function saveSeg($seg){
        global $session;
        global $db;
		$sql="INSERT INTO seguimientos_detalle (body,class,class_id,idusuario) ";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString($seg['body'])."' , '";
		$sql.= $db->escapeString('TramaIntegracion')."' , '";
		$sql.= $db->escapeString($seg['idtrama'])."' , '";
		$sql.= $db->escapeString($session->idusuario)."' )";
        if ($db->query($sql)) {
            return True;
        }
        else{
            return False;
        }
    }


    public static function saveSegResum($seg){
        global $session;
        global $db;
        $sql="INSERT INTO seguimientos_detalle (body,class,class_id,groupseg,idusuario) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString($seg['body'])."' , '";
        $sql.= $db->escapeString('TramaIntegracion')."' , '";
        $sql.= $db->escapeString($seg['idtrama'])."' , '";
        $sql.= $db->escapeString($seg['statusseg'])."' , '";
        $sql.= $db->escapeString($session->idusuario)."' )";
        if ($db->query($sql)) {
            $trm = new TramaIntegracion();
            $trm->updateStatusSeg($seg['statusseg'],($seg['idtrama']));
            return True;
        }
        else{
            return False;
        }
    }

    public function updateStatusSeg($stat,$idtrama){
        global $db;
        $sqlicid="UPDATE tramaIntegrancion SET statusseg='$stat' WHERE idtrama='$idtrama'";
        return ($db->query($sqlicid))?true:false;
    }


    // public function obtenerTramaExcel($fechaInicio,$fechaFin){
    //     $sql="select fecha_registro,policy,fecha_inicio,CONCAT(apellido_asegurado,' ',nombre_asegurado) asegurado,LPAD(num_documento, 8, '0') num_documento,
    //     campana,num_telefono,celular,correo,chasis,placa,marca,modelo,motor,uso,anio,
    //     compania,contacto_i,oferta,departamento,direccion,doc_factura,forma_pago,
    //     case when estado=1 then 'Pendiente' when estado=15 then 'Instalado' when estado =16 then 'Activo'
    //     when estado=99 then 'Anulado' END estado,
    //     (SELECT est.estado FROM estados est WHERE est.codigo=(SELECT estado FROM operaciones op where op.idoperacion=tramaIntegrancion.idoperacion))estadoOpera,
    //     (SELECT GROUP_concat(body) FROM seguimientos_detalle sd where sd.class_id =tramaIntegrancion.idtrama and sd.class='TramaIntegracion') seguimiento,
    //     DATE_FORMAT((SELECT CONCAT(tp.fecha,' ',tp.hora) FROM tablaProgramacion tp WHERE tp.idoperacion = tramaIntegrancion.idoperacion limit 1),'%Y-%m-%d') fechaInstalacion,
    //     (SELECT case when groupseg=0 then 'No Contactado' when groupseg=1 then 'Contactado' when groupseg=2 then 'Agendado' when groupseg=3 then 'Programado' when groupseg=4 then 'Instalado'  END FROM seguimientos_detalle sd where sd.class_id =tramaIntegrancion.idtrama and TIMESTAMPDIFF(DAY, tramaIntegrancion.fecha_inicio, fecha) BETWEEN 1 and 5 and sd.class='TramaIntegracion' and sd.groupseg is not null order by fecha desc limit 1) descseg0_5,
    //     (SELECT GROUP_concat(body,'(',DATE_FORMAT(fecha,'%d/%m/%Y'),')') FROM seguimientos_detalle sd where sd.class_id =tramaIntegrancion.idtrama and TIMESTAMPDIFF(DAY, tramaIntegrancion.fecha_inicio, fecha) BETWEEN 1 and 5 and sd.class='TramaIntegracion' and sd.groupseg is not null) seg0_5,
    //     (SELECT case when groupseg=0 then 'No Contactado' when groupseg=1 then 'Contactado' when groupseg=2 then 'Agendado' when groupseg=3 then 'Programado' when groupseg=4 then 'Instalado'  END FROM seguimientos_detalle sd where sd.class_id =tramaIntegrancion.idtrama and TIMESTAMPDIFF(DAY, tramaIntegrancion.fecha_inicio, fecha) BETWEEN 6 and 10 and sd.class='TramaIntegracion' and sd.groupseg is not null order by fecha desc limit 1) descseg6_10,   
    //     (SELECT GROUP_concat(body,'(',DATE_FORMAT(fecha,'%d/%m/%Y'),')') FROM seguimientos_detalle sd where sd.class_id =tramaIntegrancion.idtrama and TIMESTAMPDIFF(DAY, tramaIntegrancion.fecha_inicio, fecha) BETWEEN 6 and 10 and sd.class='TramaIntegracion' and sd.groupseg is not null) seg6_10,
    //     (SELECT case when groupseg=0 then 'No Contactado' when groupseg=1 then 'Contactado' when groupseg=2 then 'Agendado' when groupseg=3 then 'Programado' when groupseg=4 then 'Instalado'  END FROM seguimientos_detalle sd where sd.class_id =tramaIntegrancion.idtrama and TIMESTAMPDIFF(DAY, tramaIntegrancion.fecha_inicio, fecha) BETWEEN 11 and 30 and sd.class='TramaIntegracion' and sd.groupseg is not null  order by fecha desc limit 1) descseg11_30,
    //     (SELECT GROUP_concat(body,'(',DATE_FORMAT(fecha,'%d/%m/%Y'),')') FROM seguimientos_detalle sd where sd.class_id =tramaIntegrancion.idtrama and TIMESTAMPDIFF(DAY, tramaIntegrancion.fecha_inicio, fecha) BETWEEN 11 and 30 and sd.class='TramaIntegracion' and sd.groupseg is not null) seg11_30,
    //     (SELECT case when groupseg=0 then 'No Contactado' when groupseg=1 then 'Contactado' when groupseg=2 then 'Agendado' when groupseg=3 then 'Programado' when groupseg=4 then 'Instalado'  END FROM seguimientos_detalle sd where sd.class_id =tramaIntegrancion.idtrama and TIMESTAMPDIFF(DAY, tramaIntegrancion.fecha_inicio, fecha) >30 and sd.class='TramaIntegracion' and sd.groupseg is not null order by fecha desc limit 1) descseg30,
    //     (SELECT GROUP_concat(body,'(',DATE_FORMAT(fecha,'%d/%m/%Y'),')') FROM seguimientos_detalle sd where sd.class_id =tramaIntegrancion.idtrama and TIMESTAMPDIFF(DAY, tramaIntegrancion.fecha_inicio, fecha) > 30 and sd.class='TramaIntegracion' and sd.groupseg is not null) segmas30,
    //     case when TIMESTAMPDIFF(DAY, fecha_registro,fecha_inicio) < 0 then concat(TIMESTAMPDIFF(DAY, tramaIntegrancion.fecha_inicio, fecha_registro),' dia(s)') else '' end atraso, creation_at , case when reasignacion = 1 then 'SI' ELSE 'NO' end reasignacion
    //     from  tramaIntegrancion where fecha_registro>='$fechaInicio' and fecha_registro<='$fechaFin'
    //     order by idtrama asc ";
    //     $dtp=self::sentencia($sql);
    //     return !empty($dtp)? $dtp:false;
    // }

    public function obtenerTramaExcel($fechaInicio,$fechaFin){
        $sql="SELECT tramaIntegrancion.fecha_inicio,fecha_registro,policy,fecha_inicio,CONCAT(apellido_asegurado,' ',nombre_asegurado) asegurado,LPAD(num_documento, 8, '0') num_documento,
        campana,num_telefono,celular,correo,chasis,placa,marca,modelo,motor,uso,anio,
        compania,contacto_i,oferta,departamento,direccion,doc_factura,forma_pago,
        CASE WHEN estado=1 THEN 'Pendiente' WHEN estado=15 THEN 'Instalado' WHEN estado =16 THEN 'Activo'
        WHEN estado=99 THEN 'Anulado' END estado,
        (SELECT est.estado FROM estados est WHERE est.codigo=(SELECT estado FROM operaciones op WHERE op.idoperacion=tramaIntegrancion.idoperacion))estadoOpera,
        (SELECT GROUP_CONCAT(BODY) FROM seguimientos_detalle sd WHERE sd.class_id =tramaIntegrancion.idtrama AND sd.class='TramaIntegracion') seguimiento,
        DATE_FORMAT((SELECT CONCAT(tp.fecha,' ',tp.hora) FROM tablaProgramacion tp WHERE tp.idoperacion = tramaIntegrancion.idoperacion LIMIT 1),'%Y-%m-%d') fechaInstalacion,
        NULL AS descseg0_5,
        NULL AS seg0_5,
        NULL AS descseg6_10,   
        NULL AS seg6_10,
        NULL AS descseg11_30,
        NULL AS seg11_30,
        NULL AS descseg30,
        NULL AS segmas30,
        CASE WHEN TIMESTAMPDIFF(DAY, fecha_registro,fecha_inicio) < 0 THEN CONCAT(TIMESTAMPDIFF(DAY, tramaIntegrancion.fecha_inicio, fecha_registro),' dia(s)') ELSE '' END atraso, creation_at , CASE WHEN reasignacion = 1 THEN 'SI' ELSE 'NO' END reasignacion
        FROM tramaIntegrancion 
        WHERE fecha_registro BETWEEN '$fechaInicio' AND '$fechaFin'
        ORDER BY idtrama ASC";
        $dtp=self::sentencia($sql);
        return !empty($dtp)? $dtp:false;
    }

}


<?php 
class Canales extends Recursos
{
	public $idcanal;
	public $idtitular;
	public $tipoCanal;
	public $tiendaCanal;
	public $nombreCanal;
	public $ejecutivoCanal;
	public $cobraComision;
	public $banco;
	public $numeroCuenta;
	public $moneda;
	public $cci;
	public $titular;
	public $canalId;
	public $ruc;
	public $telefono;
	public $idCreador;
	public $estado;
	public $fechaCreada;
	public $nombreConvenio;
	public $idconvenio;
	public $idEjecutivoAsignado;
	public $idIntermediario;
	function __construct()
	{
		
	}
	public function registrarCanal(){
		global $db;
		global $session;
		if ($this->tipoCanal=='') {
			return false;
		}
		if ($this->nombreCanal=='') {
			return false;
		}
		if ($this->ejecutivoCanal=='') {
			return false;
		}
		$fechaCreada=date("YmdHis");
		$sql="INSERT INTO canales (tipoCanal,tiendaCanal,nombreCanal,ejecutivoCanal,cobraComision,banco,numeroCuenta,moneda,cci,titular,canalId,idCreador,fechaCreada) VALUES ('";
		$sql.=$db->escapeString($this->tipoCanal)."','";
		$sql.=$db->escapeString($this->tiendaCanal)."','";
		$sql.=$db->escapeString($this->nombreCanal)."','";
		$sql.=$db->escapeString($this->ejecutivoCanal)."','";
		$sql.=$db->escapeString($this->cobraComision)."','";
		$sql.=$db->escapeString($this->banco)."','";
		$sql.=$db->escapeString($this->numeroCuenta)."','";
		$sql.=$db->escapeString($this->moneda)."','";
		$sql.=$db->escapeString($this->cci)."','";
		$sql.=$db->escapeString($this->ejecutivoCanal)."','";
		$sql.=$db->escapeString(soloNumero($this->canalId))."','";
		$sql.=$db->escapeString($session->idusuario)."','";
		$sql.=$db->escapeString($fechaCreada)."')";
		if ($db->query($sql)) {
			$this->idcanal=$db->ultimoIdInsertado();			
			return true;	
		}else{
			return false;
		}
	}

	public function registrarTitular(){
		global $db;
		global $session;
		$fechaCreada=date("YmdHis");
		$sql="INSERT INTO titular (moneda,banco,numeroCuenta,cci,titular,ruc,idCreador,idIntermediario,fechaCreada) VALUES ('";
		$sql.=$db->escapeString($this->moneda)."','";
		$sql.=$db->escapeString($this->banco)."','";
		$sql.=$db->escapeString($this->numeroCuenta)."','";
		$sql.=$db->escapeString($this->cci)."','";
		$sql.=$db->escapeString($this->titular)."','";
		$sql.=$db->escapeString($this->ruc)."','";
		$sql.=$db->escapeString($session->idusuario)."','";
		$sql.=$db->escapeString($this->idIntermediario)."','";
		$sql.=$db->escapeString($fechaCreada)."')";
		if ($db->query($sql)) {
			$this->idtitular=$db->ultimoIdInsertado();			
			return true;	
		}else{
			return false;
		}
	}

	public function actualizarDatosCanal(){
		global $db;
		$sql="UPDATE canales SET banco ='";
		$sql.=$db->escapeString($this->banco)."', ";
		$sql.="numeroCuenta ='";
		$sql.=$db->escapeString($this->numeroCuenta)."', ";
		$sql.="cci ='";
		$sql.=$db->escapeString($this->cci)."', ";
		$sql.="nombreCanal ='";
		$sql.=$db->escapeString($this->nombreCanal)."', ";
		// $sql.="titular ='";
		// $sql.=$db->escapeString($this->titular)."', ";
		$sql.="ejecutivoCanal ='";
		$sql.=$db->escapeString($this->ejecutivoCanal)."', ";
		$sql.="canalId ='";
		$sql.=$db->escapeString(soloNumero($this->canalId))."', ";
		$sql.="moneda ='";
		$sql.=$db->escapeString($this->moneda)."' ";
		$sql.=" WHERE idcanal =";
		$sql.=$db->escapeString($this->idcanal)." ";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
	public function actualizarDatoTIUTALARtitular(){
		global $db;
		$sql="UPDATE titular SET titular ='";
		 $sql.=$db->escapeString($this->titular)."' ";
		$sql.=" WHERE idtitular =";
		$sql.=$db->escapeString($this->idtitular)." ";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
    
	public function actualizarDatosCanalcom(){
		global $db;
		$sql="UPDATE canales SET banco ='";
		$sql.=$db->escapeString($this->banco)."', ";
		$sql.="numeroCuenta ='";
		$sql.=$db->escapeString($this->numeroCuenta)."', ";
		$sql.="cci ='";
		$sql.=$db->escapeString($this->cci)."', ";
		$sql.="nombreCanal ='";
		$sql.=$db->escapeString($this->nombreCanal)."', ";
		$sql.="titular ='";
		$sql.=$db->escapeString($this->titular)."', ";
		$sql.="ejecutivoCanal ='";
		$sql.=$db->escapeString($this->ejecutivoCanal)."', ";
		$sql.="canalId ='";
		$sql.=$db->escapeString(soloNumero($this->canalId))."', ";
		$sql.="moneda ='";
		$sql.=$db->escapeString($this->moneda)."' ";
		$sql.=" WHERE idcanal =";
		$sql.=$db->escapeString($this->idcanal)." ";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
	public function otenerDatosCanal($canalId){
		global $db;
		$sql=self::sentencia("SELECT * FROM canales WHERE canalId='".$db->escapeString($canalId)."' AND canalId<>'' ORDER BY idcanal DESC LIMIT 1");
		return !empty($sql) ? array_shift($sql) : false;		
	}

	public function otenerDatosCanalPORCAMPOS($datos){
		global $db;
		global $session;
		$sql=self::sentencia("SELECT * FROM canales WHERE (canalId LIKE '%$datos%' OR nombreCanal like '%$datos%' OR ejecutivoCanal LIKE '%$datos%' OR titular LIKE '%$datos%') AND idCreador='$session->idusuario' AND cobraComision='si'");
		return !empty($sql) ? $sql:false;		
	}

	public function otenerDatosCanalPORCAMPOST($datos){
		global $session;
		$sql=self::sentencia("SELECT * FROM titular WHERE (titular LIKE '%$datos%' OR ruc like '%$datos%') AND  idCreador='$session->idusuario'");
		return !empty($sql) ? $sql:false;		
	}
    public function otenerDatosCanalPORCAMPOSTODOS($datos){
            global $db;
            global $session;
            $sql=self::sentencia("SELECT * FROM canales WHERE (canalId LIKE '%$datos%' OR nombreCanal like '%$datos%' OR ejecutivoCanal LIKE '%$datos%' OR titular LIKE '%$datos%') AND cobraComision='si'");
            return !empty($sql) ? $sql:false;       
    }

    public function otenerDatosCanalPORCAMPOSTODOST($datos){
            $sql=self::sentencia("SELECT * FROM titular WHERE titular LIKE '%$datos%' OR ruc like '%$datos%'");
            return !empty($sql) ? $sql:false;       
    }

	public static function verNombreCanal(){
		$sql=self::sentencia("SELECT DISTINCT(nombreCanal) FROM canales WHERE cobraComision='' AND tipoCanal<>'CONVENIO' ");
		return !empty($sql) ? $sql : false;
	}

	public static function verEjecutiVodeCanal(){
		$sql=self::sentencia("SELECT DISTINCT(ejecutivoCanal) FROM canales WHERE cobraComision=''");
		return !empty($sql) ? $sql : false;
	}
	public static function verTiendadeCanal(){
		$sql=self::sentencia("SELECT DISTINCT(tiendaCanal) FROM canales WHERE cobraComision='' AND tipoCanal='CONVENIO' AND nombreCanal='1'");
		return !empty($sql) ? $sql : false;
	}

	public static function verDniCanal(){		
		$sql=self::sentencia("SELECT canalId,ejecutivoCanal FROM canales WHERE canalId <> ''"); 
		return !empty($sql)?$sql:false;
	}

	public static function obtenerDatosCanalPorId($idcanal){
		global $db;
		$obtenerDatosCanalPorIdQ=self::sentencia("SELECT * FROM canales WHERE idcanal =".$db->escapeString($idcanal)." LIMIT 1");
		return !empty($obtenerDatosCanalPorIdQ) ? array_shift($obtenerDatosCanalPorIdQ) : false;
	}

	public static function obtenerDatosTITULARPorId($idtitular){
		global $db;
		$obtenerDatosCanalPorIdQ=self::sentencia("SELECT * FROM titular WHERE idtitular =".$db->escapeString($idtitular)." LIMIT 1");
		return !empty($obtenerDatosCanalPorIdQ) ? array_shift($obtenerDatosCanalPorIdQ) : false;
	}

	public static function verificarEstadoDeCanal($idcanal){
		global $db;
		$verificarQ=self::sentencia("SELECT * FROM canales WHERE idcanal=".$db->escapeString($idcanal)." AND (banco='PENDIENTE' OR numeroCuenta='PENDIENTE' OR moneda='PENDIENTE' OR canalId='PENDIENTE' OR ejecutivoCanal='PENDIENTE')  LIMIT 1");
		return !empty($verificarQ) ? true:false; 
	}
	public function registrarNumeroRuc(){
		global $db;
		$registrarNumeroRucQ="UPDATE canales SET ruc ='".$db->escapeString($this->ruc)."' WHERE idcanal='".$db->escapeString($this->idcanal)."' ";
		if ($db->query($registrarNumeroRucQ)) {
			return true;
		}else{
			return false;
		}
	}
	
	public static function obtenerCanalDesdeNombresYEjecutivo($nombreCanal,$ejecutivoCanal){
		$nombreCanal=htmlspecialchars($nombreCanal);
		$ejecutivoCanal=htmlspecialchars($ejecutivoCanal);
		$obtenerCanalDesdeNombresYEjecutivo=self::sentencia("SELECT * FROM canales WHERE nombreCanal='$nombreCanal' AND ejecutivoCanal='$ejecutivoCanal' LIMIT 1");
		return !(empty($obtenerCanalDesdeNombresYEjecutivo))?array_shift($obtenerCanalDesdeNombresYEjecutivo):false;
	}

	public static function obtenerConvenios(){
		$obc=self::sentencia("SELECT nombreConvenio,idconvenio FROM convenios WHERE estado='0'");
		return !empty($obc)?$obc:false;
	}
	public static function obtenerConveniosDeEjecutivo(){
		global $session;
		$obc=self::sentencia("SELECT nombreConvenio,idconvenio FROM convenios WHERE estado='0' AND idEjecutivoAsignado='$session->idusuario' ");
		return !empty($obc)?$obc:false;
	}
	public static function obtenerConveniosTodos(){
		global $session;
		$obc=self::sentencia("SELECT nombreConvenio,idconvenio FROM convenios WHERE estado='0' ");
		return !empty($obc)?$obc:false;
	}
	public static function obtenerDatosDeConvenioPorId($id){
		$id=limpiar($id);
		$covD=self::sentencia("SELECT nombreConvenio FROM convenios WHERE idconvenio='$id' LIMIT 1 ");
		return !empty($covD)?array_shift($covD):false;
	}
    public function modificarCanal($ultimoid,$idopera){
        global $db;
        global $session;
		$datosOld=new SeguimientoConsulta();
		$dold=$datosOld->consultaSeg("canales",$this->idcanal,"idcanal");
		$doldd=$dold;
		$updaQ="UPDATE canales SET tipoCanal='$this->tipoCanal', nombreCanal = '$this->nombreCanal', ejecutivoCanal= '$this->ejecutivoCanal' WHERE idcanal ='$this->idcanal'";
		/// echo $db->query($updaQ)." aca yo otra vez \n d".$this->idcanal;
		if($db->query($updaQ)){
			new SeguimientoTablas("canales"," Modifico datos",$this->idcanal,$this,"idcanal",$doldd,$ultimoid,$idopera);
			return true ;
		}else{
			return false;
		}
//        return ($db->query($updaQ))?true:false;
    }
    public static function obtenerNumerosDEcuentaDesdeEdicion($idcanal){
        global $db;
        $sqloNdDE=self::sentencia("SELECT numeroCuenta FROM canalesLog WHERE idcanal='".$db->escapeString($idcanal)."' GROUP BY numeroCuenta");
        return !empty($sqloNdDE)?$sqloNdDE:false;
    }
    
    }
 ?>

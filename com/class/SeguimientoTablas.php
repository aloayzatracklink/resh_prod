<?php


class SeguimientoTablas extends Recursos{

    public $id;
    public $tabla;
    public $tipoMod;
    public $idusuario;
    public $register_at;
    public $object_detail;
    public $idobject;


    function __construct($tabla,$tipoMod,$idobject,$object_detail,$keyPrimary,$datosOld,$ultimoid,$idOpera){
        global $db;
        global $session;
        $object = (object) array_filter((array) $object_detail);
        $result = addslashes(json_encode($object));
        //echo  $tipoMod." tabla ".$tabla ." la data es ".var_dump($datosOld)." \n";
        $sql="insert into seguimientoTablas (tabla,idopera,idUpdate,tipoModificacion,idusuario,idobject,object_detailold,object_detailnew) ";
        $sql.="values ('";
        $sql.=$tabla."', ";
        $sql.=$idOpera.", ";
        $sql.=$ultimoid.",' ";
        $sql.=$tipoMod."', ";
        $sql.=$session->idusuario.", ";
        $sql.=$idobject.",'";
        $sql.=$datosOld."','";
        $sql.=$result."')";
        $db->query($sql);
       return true;
    }

}
<?php 

class ReclamoClienteReporteClass extends Recursos
{
	function __construct()
	{		
	}
	
	public function obtenerReporteReclamosClienteCantidad(){
		$result=self::sentenciaV2("SELECT count(*) as cantidad FROM  vreporte_reclamos_cliente");
		return !empty($result)? array_shift($result):false;		
	}

	public function obtenerReporteReclamosClienteRANGOCantidad($desde,$hasta, $estado){
		$sqlWhere = (trim($estado)!="")? " and fk_idreclamo_estado={$estado} " : "";
		$result=self::sentenciaV2("SELECT count(*) as cantidad FROM  vreporte_reclamos_cliente WHERE 1=1  {$sqlWhere} AND (dtreclamo BETWEEN '$desde' AND '$hasta') ORDER BY idreclamo_cliente");
		return !empty($result)? array_shift($result):false;		
	}

	public function obtenerReporteReclamosClientePaginacionRANGO($posisionDePagina,$registrosPorPagina,$desde,$hasta, $estado){
		$sqlWhere = (trim($estado)!="")? " and fk_idreclamo_estado={$estado} " : "";
		$result=self::sentenciaV2("SELECT * FROM  vreporte_reclamos_cliente  WHERE  1=1  {$sqlWhere} AND (dtreclamo BETWEEN '$desde' AND '$hasta') ORDER BY idreclamo_cliente DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($result)? $result:[];
	}
    //------------------------------------------------------------------------
	public function obtenerReporteReclamosClientePaginacionRANGO_2($posisionDePagina,$registrosPorPagina,$desde,$hasta, $estado){
		$sqlWhere = (trim($estado)!="")? " and fk_idreclamo_estado={$estado} " : "";
		$result=self::sentenciaV2("SELECT * FROM  vreporte_reclamos_cliente  WHERE  1=1  {$sqlWhere} AND (dtreclamo BETWEEN '$desde' AND '$hasta') ORDER BY idreclamo_cliente DESC");
		return !empty($result)? $result:[];
	}

	public function TablaListaReclamos($posisionDePagina, $registrosPorPagina){
		$result=self::sentenciaV2("SELECT * FROM vreporte_reclamos_cliente ORDER BY idreclamo_cliente DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($result)? $result:[];
	}

	public function TablaListaReclamosParaFecha($desde,$hasta){
		$result=self::sentenciaV2("SELECT * FROM  vreporte_reclamos_cliente WHERE  1=1 AND (dtreclamo BETWEEN '$desde' AND '$hasta')");
		return !empty($result)? $result:[];
	}

	public function TablaListaReclamosParaFechayEstado($desde,$hasta,$estado){
		$result=self::sentenciaV2("SELECT * FROM  vreporte_reclamos_cliente WHERE  1=1 AND (dtreclamo BETWEEN '$desde' AND '$hasta') AND fk_idreclamo_estado={$estado}");
		return !empty($result)? $result:[];
	}
	//-------------------------------------------------------------------------
	public function obtenerReporteReclamosClienteRANGO($desde,$hasta, $estado){
		$sqlWhere = (trim($estado)!="")? " and fk_idreclamo_estado={$estado} " : "";
		$result=self::sentenciaV2("SELECT * FROM  vreporte_reclamos_cliente  WHERE  1=1  {$sqlWhere} AND (dtreclamo BETWEEN '$desde' AND '$hasta') ORDER BY idreclamo_cliente DESC");
		return !empty($result)? $result:[];
	}

	public function obtenerReporteReclamosClientePorId($id){
		$result=self::sentenciaV2("SELECT * FROM  vreporte_reclamos_cliente  WHERE  idreclamo_cliente='$id'");
		return !empty($result)? $result:[];
	}

	public function obtenerReporteReclamosClientePorNombre($id){
		$result=self::sentenciaV2("SELECT * FROM  vreporte_reclamos_cliente  WHERE  cliente_nombre='$id'");
		return !empty($result)? $result:[];
	}

	/**
	 * Obtener reclamo por nro o código de reclamo 
	 */
	public function obtenerReclamoClientePorNroReclamo($vreclamo_cliente){
		$result=self::sentenciaV2("SELECT * FROM  vreporte_reclamos_cliente  WHERE  vreclamo_cliente='$vreclamo_cliente'");
		return !empty($result)? $result:[];
	}

	/**
	 * Obtener reclamo a notificar
	 */
	public function obtenerReclamosClientePlazo(){
		$result=self::sentenciaV2("SELECT * FROM trackdb.vreporte_reclamos_cliente WHERE enviar_recordatorio = 'S' ");
		return !empty($result)? $result:[];
	}

	public function obtenerReclamosNombreClientePorId($id){
		$result=self::sentenciaV2("SELECT cliente_nombre FROM vreporte_reclamos_cliente vrc 
		INNER JOIN reclamo_cliente rc ON vrc.vreclamo_cliente = rc.vreclamo_cliente
		WHERE vrc.idreclamo_cliente='$id'");
		return !empty($result)? $result:[];
	}

}


 ?>

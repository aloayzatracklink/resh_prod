<?php


class ServiciosAdicionales extends Recursos
{
    public $idser;
    public $descripcion;
    public $codigo;
    public $estado;
    public $slect;
    public $orden;

    public static function obtenerServAdicional($idTecn){
        global $db;
        if($idTecn===''){
            $idTecn="0";
        }
        $obj=self::sentencia("SELECT idser,descripcion ,codigo,
        COALESCE((select 1 from plan_serv_adicionales where idtec='$idTecn' and plan_serv_adicionales.idser like CONCAT('%',(servicios_adicionales.idser),'%') limit 1),0) slect
        FROM servicios_adicionales
        where estado=1 order by orden asc ");
        foreach ($obj as $var) :
            ?>
            <div class="col-md-4" style="padding:0.5px;">
                <div class="checkbox checkbox-primary <?php echo $var->slect=='1'?" ":" disabled " ?> " style="margin-top:1px; margin-bottom: 1px">
                    <input  id="<?php echo $var->codigo ?>" <?php echo $var->slect=='1'?" ":" disabled " ?>  type="checkbox" name="servad[]" VALUE="<?php echo $var->idser ?>">
                    <label for="<?php echo $var->codigo ?>"><?php echo $var->descripcion ?></label>
                </div>
            </div>
        <?php
        endforeach;

    }


    public static function obtenerServAdicionalSeleccionadoVerIdServ($idServ){
        global $db;

        $obj=self::sentencia("SELECT idser,descripcion ,codigo,
        COALESCE((select 1 from plan_serv_adicionales where idser in ($idServ) and plan_serv_adicionales.idser like CONCAT('%',(servicios_adicionales.idser),'%') limit 1),0) slect
        FROM servicios_adicionales
        where estado=1 order by orden
         ");
        foreach ($obj as $var) :
            ?>
            <div class="col-md-4 " style="padding:0.5px;">
                <div class="checkbox checkbox-primary <?php echo $var->slect=='1'?"checked text-danger ":" disabled " ?> " style="margin-top:1px; margin-bottom: 1px">
                    <input  id="<?php echo $var->codigo ?>" <?php echo $var->slect=='1'?"   checked=\"\"":" disabled " ?>   type="checkbox" name="servad[]" VALUE="<?php echo $var->idser ?>">
                    <label for="<?php echo $var->codigo ?>" style="opacity:100"><?php echo $var->descripcion ?></label>
                </div>
            </div>
        <?php
        endforeach;
    }

    public static function obtenerServAdicionalSeleccionadoVer($idOperacion){
        global $db;
        $obj=self::sentencia("SELECT idser,descripcion ,codigo,
        COALESCE((select 1 from opera_servad where idoperacion='$idOperacion' and tipo=1 and opera_servad.idser=servicios_adicionales.idser),0) slect
        FROM servicios_adicionales
        where estado=1 order by orden;
         ");
        foreach ($obj as $var) :
            ?>
            <div class="col-md-4 " style="padding:0.5px;">
                <div class="checkbox checkbox-danger <?php echo $var->slect=='1'?"checked text-danger disabled":" disabled " ?> " style="margin-top:1px; margin-bottom: 1px">
                    <input  id="<?php echo $var->codigo ?>" <?php echo $var->slect=='1'?" disabled  checked=\"\"":" disabled " ?>   type="checkbox" name="servad[]" VALUE="<?php echo $var->idser ?>">
                    <label for="<?php echo $var->codigo ?>" style="opacity:100"><?php echo $var->descripcion ?></label>
                </div>
            </div>
        <?php
        endforeach;
    }



    public static function obtenerServAdicionalSeleccionadoVerAdmin($idOperacion){
        global $db;
        $obj=self::sentencia("SELECT idser,descripcion ,codigo,
        COALESCE((select 1 from opera_servad where idoperacion='$idOperacion' and tipo=1 and opera_servad.idser=servicios_adicionales.idser),0) slect
        FROM servicios_adicionales
        where estado=1 order by orden;
         ");
        foreach ($obj as $var) :
            ?>
            <div class="col-md-4 " style="padding:0.5px;">
                <div class="checkbox checkbox-danger <?php echo $var->slect=='1'?"checked text-danger ":"  " ?> " style="margin-top:1px; margin-bottom: 1px">
                    <input  id="<?php echo $var->codigo ?>" <?php echo $var->slect=='1'?"   checked=\"\"":"  " ?>   type="checkbox" name="servad[]" VALUE="<?php echo $var->idser ?>">
                    <label for="<?php echo $var->codigo ?>" style="opacity:100"><?php echo $var->descripcion ?></label>
                </div>
            </div>
        <?php
        endforeach;
        ?>
            <?php
    }

    public static function obtenerServAdic($idOperacion){
        $sql="SELECT idser,descripcion ,codigo,
        COALESCE((select 1 from opera_servad where idoperacion='$idOperacion' and tipo=1 and opera_servad.idser=servicios_adicionales.idser),0) slect
        FROM servicios_adicionales
        where estado=1 order by orden;
         ";
        $obj=self::sentencia($sql);
        return !empty($obj)?$obj:false;
    }

    public static function obtenerServAdicionalSeleccionadoVerCita($idOperacion){
        global $db;
        $obj=self::sentencia("SELECT idser,descripcion ,codigo,
        COALESCE((select 1 from opera_servad where idoperacion='$idOperacion' and tipo=2 and opera_servad.idser=servicios_adicionales.idser),0) slect
        FROM servicios_adicionales
        where estado=1 order by orden;
         ");
        foreach ($obj as $var) :
            ?>
            <div class="col-md-4 " style="padding:0.5px;">
                <div class="checkbox checkbox-danger <?php echo $var->slect=='1'?"checked text-danger disabled ":"  disabled" ?> " style="margin-top:1px; margin-bottom: 1px">
                    <input  id="R<?php echo $var->codigo ?>" <?php echo $var->slect=='1'?" disabled  checked=\"\"":" disabled " ?>   type="checkbox" name="servadR[]" VALUE="<?php echo $var->idser ?>">
                    <label for="R<?php echo $var->codigo ?>" style="opacity:100"><?php echo $var->descripcion ?></label>
                </div>
            </div>
        <?php
        endforeach;
    }

    public static function obtenerServicesAdicional(){
        global $db;
        $obj=self::sentencia("SELECT idser,descripcion,codigo FROM servicios_adicionales where estado=1 order by idser ");
        return !empty($obj)?$obj:false;
    }

}
<?php
class Motivos extends Recursos
{
    public $id;
    public $motivo_id;
    public $motivo;
    public $idusuario;
    public $fecha;
    public $class_id;
    public $class;

	public static function registrar_motivo_type($type, $motivo){
		global $db;
		global $session;
		$fechaHoy=date("Y-m-d H:i:s");
		$sql="INSERT INTO motivos (motivo,tipo,idusuario) VALUES ('";
		$sql.=$db->escapeString($motivo)."','";
		$sql.=$db->escapeString($type)."','";
		$sql.=$db->escapeString($session->idusuario)."')";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}

	public static function registrar_motivo($motivo_id, $class, $class_id){
		global $db;
		global $session;
		$fechaHoy=date("Y-m-d H:i:s");
		$sql="INSERT INTO motivos_class (motivo_id,class_id,class,idusuario) VALUES ('";
		$sql.=$db->escapeString($motivo_id)."','";
		$sql.=$db->escapeString($class_id)."','";
		$sql.=$db->escapeString($class)."','";
		$sql.=$db->escapeString($session->idusuario)."')";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}

	public static function borrar_motivo($motivo_id, $class, $class_id){
		global $db;
		global $session;
		$fechaHoy=date("Y-m-d H:i:s");
		$sql="delete from motivos_class where motivo_id ='$motivo_id' and class='$class' and $class_id";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}

    public  static function getMotivosForClass($class, $class_id){
		$getMotivos=self::sentencia("SELECT * FROM motivos_class  WHERE class='$class' AND class_id='$class_id' ORDER BY id DESC");
		return !empty($getMotivos)?$getMotivos:[];
    }

    public  static function getMotivosByClassAndIdMotivo($class, $class_id,$motivo_id){
		$getMotivos=self::sentencia("SELECT * FROM motivos_class  WHERE class='$class' AND class_id='$class_id' and motivo_id='$motivo_id' Limit 1");
		return !empty($getMotivos)?true:false;
    }

    public  static function getMotivosByType($type){
		$getMotivos=self::sentencia("SELECT * FROM motivos WHERE tipo='$type' ORDER BY id DESC");
		return !empty($getMotivos)?$getMotivos:[];
    }

}

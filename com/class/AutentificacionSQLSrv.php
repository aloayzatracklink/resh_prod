<?php
require("../includes/baseDatosMSQLSrv.php");
if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
    //* Tabla Autentificacion del SrvCntr (SQLSRV)
    class AutentificacionSQLSrv extends baseDatosMSQLSrv{
        public $ci_ruc_cliente;
        public $password;
        public $tipo_cliente;
        public $cod_cliente;
        public $cod_geografico;
        public $AppMobile;
        public $AccessWeb;
        public $ingresos_web;

        function __construct(){}

        function obtenerCredenciales($id){
            $result = self::conectarMSSQL("SELECT * FROM SrvCntr.dbo.AUTENTIFICACION WHERE cod_cliente='$id'");
            return !empty($result) ? $result: "Error";  
        }

    }
}
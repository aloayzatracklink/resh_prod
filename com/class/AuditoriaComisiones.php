<?php
class AuditoriaComisiones extends Recursos
{
    public $idcomision;
    public $idTitularAnterior;
    public $idTitularActual;
    public $codigoFacturacion;
    public $titularAnterior;
    public $titularActual;
    public $motivo;
    public $idUsuarioCreador;
    public $fechaCreada;
    public $fechaActualizada;

    function __construct(){}
		
    public function registrarAuditoria(){
        global $db;
		global $session;

        $sql = "INSERT INTO auditoria_comisiones (idcomision, idTitularAnterior, codigoFacturacion, titularAnterior, motivo, idUsuarioCreador, fecha_creada, fecha_actualizada)
        VALUES(".
            "'".$db->escapeString($this->idcomision)."',".
            "'".$db->escapeString($this->idTitularAnterior)."',".
            "'".$db->escapeString($this->codigoFacturacion)."',".
            "'".$db->escapeString($this->titularAnterior)."',".
            "'".$db->escapeString($this->motivo)."',".
            "'".$db->escapeString($this->idUsuarioCreador)."',".
            "'".$db->escapeString($this->fechaCreada)."',".
            "'".$db->escapeString($this->fechaActualizada)."'".
        ")";

        return ($db->query($sql)) ? true : false;
    }

    public function obtenerAuditoriaPorNumFacturacion($numFact){
        global $db;
        $sql = "SELECT * FROM auditoria_comisiones WHERE codigoFacturacion = '$numFact' ORDER BY id DESC LIMIT 1";
        $result = self::sentencia($sql);
		return !empty($result) ? array_shift($result) : false;

    }

    public function actualizarAuditoriaPorNumFacturacion($numFact, $idTitularActual){
        global $db;
        global $session;

        $fechaActualizada = date('Y-m-d');
        $sql = "UPDATE auditoria_comisiones SET idTitularActual='$idTitularActual', fecha_actualizada= '$fechaActualizada' WHERE codigoFacturacion = '$numFact'";
        return ($db->query($sql)) ? true : false;
    }
}
?>
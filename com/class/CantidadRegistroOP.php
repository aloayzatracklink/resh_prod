<?php


class CantidadRegistroOP extends Recursos
{
    public $id;
    public $tipo;
    public $chasis;
    public $cantreg;

    function __construct()
    {

    }

    public static function obtenerCantidadOperacionPorTipoServicio($chasis,$tipoServ){
        if($tipoServ!=''){
            $tipoServ=' and tipo="'.$tipoServ.'"';
        }
        $selq=self::sentencia("select DISTINCT tipo,cantreg from cantreg_Operaciones co where co.chasis='".$chasis."' ".$tipoServ."  GROUP BY tipo ");
        return !empty($selq) ? $selq : false;
    }


    public function modificarCantidad($ultimoid,$idopera){
        global $db;
        $datosOld=new SeguimientoConsulta();
       $dold=$datosOld->consultaSeg("cantreg_Operaciones","'".$this->chasis."'","tipo='".$this->tipo."' and chasis");
       $doldd=$dold;
        if($dold){
            $sqlActop="UPDATE cantreg_Operaciones SET cantreg='$this->cantreg' WHERE chasis='$this->chasis' and tipo='$this->tipo' ";
        }else{
           $sqlActop=" INSERT INTO cantreg_Operaciones(tipo,chasis,cantreg) VALUES ( '" ;
            $sqlActop.=$db->escapeString($this->tipo)."','";
            $sqlActop.=$db->escapeString($this->chasis)."',";
            $sqlActop.=$db->escapeString($this->cantreg).")";
        }
        if($db->query($sqlActop)){
            new SeguimientoTablas("cantreg_Operaciones"," Modifico Cantidad Registro","'".$this->chasis."'",$this,"tipo='".$this->tipo."' and chasis",$doldd,$ultimoid,$idopera);
            return true ;
        }else{
            return false;
        }
        //return ($db->query($sqlActop))?true:false;
    }
}
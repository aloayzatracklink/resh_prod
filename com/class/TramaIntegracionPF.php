<?php 
class TramaIntegracionPF extends Recursos
{

    public $idtramaf;
    public $fecha_registro ;
    public $policy;
    public $fecha_vigencia;
    public $nombre_asegurado;
    public $apellido_asegurado;
    public $num_documento;
    public $num_telefono;
    public $celular;
    public $direccion;
    public $correo;
    public $placa;
    public $chasis;
    public $anio;
    public $motor;
    public $marca;
    public $modelo;
    public $tipo_documento ;
    public $color;
    public $compania;
    public $plan;
    public $servicio_adicional;
    public $precio_lista;
    public $tiempo;
    public $campana;
    public $canal;
    public $canal_nombre;
    public $ejecutivo_canal;
    public $ruc_factura;
    public $razonSocial_Factura;
    public $direccion_factura;
    public $register_at;
    public $estado;
    public $forma_pago;
    public $idoperacion;
    public $idsuariocargo;
    public $doc_factura;
    public $cobra_comision;
    public $asegurado;
    public $estadoOpera;
    public $fechaInstalacion;


    public $cantidad;
    public $seguimiento;
    public $body;
    public $fecha;

    function __construct()
    {

    }


    public static function obtenerCantidadTramaIntegracionPag(){
        $obj=self::sentencia("SELECT COUNT(*) as cantidad  FROM tramaPlanFull tpf where 1=1 and idoperacion is null and estado <> 99 and ntype='No Pagado'  LIMIT 1");
        return !empty($obj)? array_shift($obj):false;
    }

    public static function obtenerTramaIntegracionPag($posisionDePagina,$registrosPorPagina){
        $obj=self::sentencia("SELECT idtramaf, fecha_registro, policy, fecha_vigencia, apellido_asegurado, nombre_asegurado, num_documento, coalesce(num_telefono,'')num_telefono,
                                   coalesce(celular,'') celular, correo, direccion, chasis, placa, anio, marca, modelo, motor, tipo_documento, color, compania, plan, servicio_adicional,
                                  precio_lista, tiempo, campana, canal, canal_nombre, ejecutivo_canal, razonSocial_Factura, ruc_factura, direccion_factura, 
                                  register_at, estado, forma_pago, idoperacion, idsuariocargo,doc_factura,cobra_comision
                                  FROM tramaPlanFull 
                                  where 1=1 and idoperacion is null and estado <> 99 and ntype='No Pagado' order by fecha_registro desc 
                                  LIMIT $posisionDePagina, $registrosPorPagina 
                               ");
        return !empty($obj)?$obj:false;
    }

    public static function getSEg($idtramaf){
        $obj=self::sentencia("select * from seguimientos_detalle where class_id='$idtramaf' and class = 'tramaPlanFull'");
        return !empty($obj)?$obj:[];
    }



    public static function obtenerTramaIntegracionPagn($datos){
        global $db;

        if($datos<>''){
            $datos="and (
                                 (CONCAT(apellido_asegurado,' ',nombre_asegurado) like '%".$datos."%') or (placa like '%".$datos."%') or (chasis  like '%".$datos."%') or (marca  like '%".$datos."%'))";
        }

        $obj=self::sentencia("SELECT idtramaf, fecha_registro, policy, fecha_vigencia, apellido_asegurado, nombre_asegurado, num_documento, coalesce(num_telefono,'')num_telefono,
                                   coalesce(celular,'') celular, correo, direccion, chasis, placa, anio, marca, modelo, motor, tipo_documento, color, compania, plan, servicio_adicional,
                                  precio_lista, tiempo, campana, canal, canal_nombre, ejecutivo_canal, razonSocial_Factura, ruc_factura, direccion_factura, 
                                  register_at, estado, forma_pago, idoperacion, idsuariocargo,doc_factura,cobra_comision
                                  FROM tramaPlanFull  
                                 where 1=1 and idoperacion is null and estado <> 99  and ntype='No Pagado'  ".$datos."    
                                 ");
        return !empty($obj)?$obj:false;
    }

    public static function obtenerTramaIntegracion($idtrama,$datos){
        global $db;
        if($idtrama<>''){
            $idtrama=' and idtramaf='.$idtrama.'';
        }

        if($datos<>''){
            $datos="and (
                                 (CONCAT(apellido_asegurado,' ',nombre_asegurado) like '%".$datos."%') or (placa like '%".$datos."%') or (chasis  like '%".$datos."%') or (modelo  like '%".$datos."%'))";
        }

        $obj=self::sentencia("SELECT idtramaf, fecha_registro, policy, fecha_vigencia, apellido_asegurado, nombre_asegurado, num_documento, coalesce(num_telefono,'')num_telefono,
                                   coalesce(celular,'') celular, correo, direccion, chasis, placa, anio, marca, modelo, motor, tipo_documento, color, compania, plan, servicio_adicional,
                                  precio_lista, tiempo, campana, canal, canal_nombre, ejecutivo_canal, razonSocial_Factura, ruc_factura, direccion_factura, 
                                  register_at, estado, forma_pago, idoperacion, idsuariocargo,doc_factura,cobra_comision
                                  FROM tramaPlanFull  
                                 where 1=1 and ntype='No Pagado'  ".$datos."  ".$idtrama."  
                                 ");
        return !empty($obj)?array_shift($obj):false;
    }


    public static function asignarTrama($idTrama){
        global $session;
        global $db;
        $sql="UPDATE tramaPlanFull SET idsuariocargo='".$session->idusuario."' WHERE idtramaf ='".$idTrama."' ";
        if ($db->query($sql)) {

        }
    }




    public function updateTrama(){
        global $db;
        $sqlicid="UPDATE tramaPlanFull SET idoperacion='$this->idoperacion' WHERE idtramaf='$this->idtramaf'";
        return ($db->query($sqlicid))?true:false;
    }


    public static function saveSeg($seg){
        global $session;
        global $db;
        $sql="INSERT INTO seguimientos_detalle (body,class,class_id,idusuario) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString($seg['body'])."' , '";
        $sql.= $db->escapeString('tramaPlanFull')."' , '";
        $sql.= $db->escapeString($seg['idtrama'])."' , '";
        $sql.= $db->escapeString($session->idusuario)."' )";
        if ($db->query($sql)) {
            return True;
        }
        else{
            return False;
        }
    }


    public function updateTrama2(){
        global $db;
        $sqlicid="UPDATE tramaPlanFull SET chasis=TRIM('$this->chasis') , placa=TRIM('$this->placa') WHERE idtramaf='$this->idtramaf'";
        return ($db->query($sqlicid))?true:false;
    }


    public static function obtenerCantidadTramaIntegracionPagGENERAL(){
        $obj=self::sentencia("SELECT COUNT(*) as cantidad  FROM tramaPlanFull where 1=1 and ntype='No Pagado' LIMIT 1 ");
        return !empty($obj)? array_shift($obj):false;
    }

    public static function obtenerTramaIntegracionPagGENERAL($posisionDePagina,$registrosPorPagina){
        $obj=self::sentencia("SELECT idtramaf, fecha_registro, policy, fecha_vigencia, apellido_asegurado, nombre_asegurado, num_documento, coalesce(num_telefono,'')num_telefono,
                                   coalesce(celular,'') celular, correo, direccion, chasis, placa, anio, marca, modelo, motor, tipo_documento, color, compania, plan, servicio_adicional,
                                  precio_lista, tiempo, campana, canal, canal_nombre, ejecutivo_canal, razonSocial_Factura, ruc_factura, direccion_factura, 
                                  register_at, estado, forma_pago, idoperacion, idsuariocargo,doc_factura,cobra_comision,estado
                                  FROM tramaPlanFull 
                                  where 1=1 and ntype='No Pagado' order by fecha_registro  desc  LIMIT $posisionDePagina, $registrosPorPagina  
                                 ");
        return !empty($obj)?$obj:false;
    }

    public static function obtenerBUSQUEDAPagGENERAL2($datos){
        global $db;

        if($datos<>''){
            $datos=" and (
                                 (CONCAT(apellido_asegurado,' ',nombre_asegurado) like '%".$datos."%') or (policy like '%".$datos."%') or (placa like '%".$datos."%') or (chasis  like '%".$datos."%') or (marca  like '%".$datos."%'))";
        }

        $obj=self::sentencia("SELECT idtramaf, fecha_registro, policy, fecha_vigencia, apellido_asegurado, nombre_asegurado, num_documento, coalesce(num_telefono,'')num_telefono,
                                   coalesce(celular,'') celular, correo, direccion, chasis, placa, anio, marca, modelo, motor, tipo_documento, color, compania, plan, servicio_adicional,
                                  precio_lista, tiempo, campana, canal, canal_nombre, ejecutivo_canal, razonSocial_Factura, ruc_factura, direccion_factura, 
                                  register_at, estado, forma_pago, idoperacion, idsuariocargo,doc_factura,cobra_comision,estado
                                  FROM tramaPlanFull  
                                 where 1=1 and ntype='No Pagado'  ".$datos."    
                                 ");
        return !empty($obj)?$obj:false;

    }

    public static function verEstadoOperacion($idoperacion){
        global $db;

        $obj=self::sentencia("select estado from operaciones where idoperacion=$idoperacion ");
        return !empty($obj)?array_shift($obj):false;

    }


    public function obtenerTramaExcel($fechaInicio,$fechaFin){
        $sql="SELECT idtramaf, fecha_registro, policy, fecha_vigencia, apellido_asegurado, nombre_asegurado,CONCAT(apellido_asegurado,' ',nombre_asegurado) asegurado, num_documento, coalesce(num_telefono,'')num_telefono,
                                   coalesce(celular,'') celular, correo, direccion, chasis, placa, anio, marca, modelo, motor, tipo_documento, color, compania, plan, servicio_adicional,
                                  precio_lista, tiempo, campana, canal, canal_nombre, ejecutivo_canal, razonSocial_Factura, ruc_factura, direccion_factura, 
                                  register_at, estado, forma_pago, idoperacion, idsuariocargo,doc_factura,cobra_comision,
                                  case when tramaPlanFull.estado=1 then 'Pendiente' when tramaPlanFull.estado =16 then 'Activo' when estado=99 then 'Anulado' END estado,
                                   (SELECT est.estado FROM estados est WHERE est.codigo=(SELECT estado FROM operaciones op where op.idoperacion=tramaPlanFull.idoperacion))estadoOpera,
                                     DATE_FORMAT((SELECT CONCAT(tp.fecha,' ',tp.hora) FROM tablaProgramacion tp WHERE tp.idoperacion = tramaPlanFull.idoperacion limit 1),'%Y-%m-%d') fechaInstalacion,
                                   (SELECT GROUP_concat(body,'(',DATE_FORMAT(fecha,'%d/%m/%Y'),')') FROM seguimientos_detalle sd where sd.class_id =tramaPlanFull.idtramaf and sd.class='tramaPlanFull') seguimiento
                                  FROM tramaPlanFull  where 1=1 and ntype='No Pagado' and fecha_registro>='$fechaInicio' and fecha_registro<='$fechaFin'
        order by idtramaf asc ";

        $dtp=self::sentencia($sql);
        return !empty($dtp)? $dtp:false;
    }




}


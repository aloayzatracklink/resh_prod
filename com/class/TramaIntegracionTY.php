<?php


class TramaIntegracionTY extends Recursos
{
   public $idtrama;
    public $anio;
    public $apellido_asegurado;
    public $campana;
    public $canal;
    public $canal_nombre;
    public $celular;
    public $chasis;
    public $cobra_comision;
    public $color;
    public $compania;
    public $contacto_i;
    public $correo;
    public $customer;
    public $departamento;
    public $direccion;
    public $direccion_factura;
    public $doc_factura;
    public $ejecutivo;
    public $ejecutivo_canal;
    public $estado;
    public $fecha_corte;
    public $fecha_fact;
    public $fecha_fin;
    public $fecha_inicio;
    public $fecha_registro;
    public $fecha_registro2;
    public $fecha_vencimiento;
    public $forma_pago;
    public $idoperacion;
    public $idsuariocargo;
    public $idusuario;
    public $idvehicle;
    public $idvoucher;
    public $marca;
    public $modelo;
    public $motor;
    public $nombre_asegurado;
    public $num_documento;
    public $num_telefono;
    public $numero_fatura;
    public $numero_i;
    public $numero_plaza;
    public $oferta;
    public $placa;
    public $plan;
    public $plan_pagos;
    public $policy;
    public $precio_lista;
    public $prima_fija_anual;
    public $prima_fija_mensual;
    public $prima_variable;
    public $razonSocial_Factura;
    public $register_at;
    public $ruc_factura;
    public $servicio_adicional;
    public $tiempo;
    public $tipoServicio;
    public $tipo_documento;
    public $tipo_transaccion;
    public $ubicacion_telefono;
    public $uso;
    public $dateInstall;
    public $chasisC;
    public $km_promotion;
    public $scorecompania;
    public $creation_at;
    public $statusseg;
    public $celular2;
    public $version;
    public $anio_modelo;
    public $ejecutivo_venta;
    public $dlr_madre;
    public $cbu;
    public $idtramavinculacion;


    public $cantidad;
    public $seguimiento;
    public $body;
    public $fecha;
    public $kms;
    public $estadoVinculacion;
    public $estado_vinculacion;
    public $precio;
    public $fecha_vinculacion;

public $asegurado;
 public $fechaSolicitud;
 public $fechaActivacion;
 public $asegurado2;
    public $num_documento2;
    public $chasis2;
    public $placa2;
    public $cbu2;
    public $plan2;
    public $tiempo2;
    public $apellido_asegurado2;
    public $tipo_documento2;
    public $nombre_asegurado2;
    public $direccion2;
    public $celular21;
    public $celular22;
    public $dlr_madre2;
    public $ejecutivo_venta2;
    public $motor2;
    public $correo2;
    public $anio2;
    public $compania2;
    public $anio_modelo2;
    public $precio_lista2;

    public $estadoActivacion;
    public $estado_activacion;
    public $usuario_activacion;
    public $usuario_vinculacion;
    public $id_trama_vinculacion;
    public $idcliente;
    public $tipoCliente;
    public $razonSocial;
    public $ruc;
    public $dni;
    public $telefono;
    public $telefono2;
    public $cliente;
    public $idvehiculo;
    public $fechaInstalacion;
    public $estadoOpera;
    public $fechaRecibido;
    public $precio_tracklink;
    public $fecha_activacion;
    public $estadoTrama;
    public $marca2;
    public $modelo2;
    public $version2;
    public $color2;
    public $usuario_anulacion;
    public $fecha_anulacion;
    public $motivo_anulacion;
    public $tipo_anulacion;
    public $motivoAnulacion;
    public $fechaAnulacion;
    public $usuarioAnulacion;
    public $orte;
    public $imei;
    public $id;
    public $odo;
    public $ubicacionSistema;
    public $lugarInstalacion;
    public $tecnicoInstalador;
    public $operador;

    public $estado_preactivacion;
    public $fecha_preactivacion;
    public $usuario_preactivacion;

    function __construct()
    {

    }


    public static function obtenerCantidadTramaIntegracionTyPag(){
        $obj=self::sentencia("SELECT COUNT(*) as cantidad  FROM toyotaintegracion tql where 1=1 and idoperacion is null and estado <> 99 and tipo_transaccion!='Vinculacion'   LIMIT 1");
        return !empty($obj)? array_shift($obj):false;
    }

    public static function obtenerTramaIntegracionTYPag($posisionDePagina,$registrosPorPagina){
        $obj=self::sentencia("SELECT idtrama, anio, apellido_asegurado, campana, canal, canal_nombre, celular, chasis, cobra_comision, color,tipo_transaccion,
                                  compania, contacto_i, correo, customer, departamento, direccion, direccion_factura, doc_factura, ejecutivo, ejecutivo_canal, 
                                  estado, fecha_corte, fecha_fact, fecha_fin, fecha_inicio, fecha_registro, fecha_vencimiento, forma_pago, idoperacion,
                                  idsuariocargo, idusuario, idvehicle, idvoucher, marca, modelo, motor, nombre_asegurado, num_documento, num_telefono,
                                  numero_fatura, numero_i, numero_plaza, oferta, placa, plan, plan_pagos, policy, precio_lista, prima_fija_anual, 
                                  prima_fija_mensual, prima_variable, razonSocial_Factura, register_at, ruc_factura, servicio_adicional, tiempo, 
                                  tipoServicio, tipo_documento, tipo_transaccion, ubicacion_telefono, uso, dateInstall, chasisC, km_promotion,
                                  scorecompania, creation_at, statusseg, celular2, version, anio_modelo, ejecutivo_venta, dlr_madre,cbu
                                  FROM toyotaintegracion 
                                  where 1=1 and idoperacion is null and estado <> 99 and tipo_transaccion!='Vinculacion' order by creation_at desc 
                                  LIMIT $posisionDePagina, $registrosPorPagina  
                               ");
        return !empty($obj)?$obj:false;
    }


    public static function getSEg($idtramat){
        $obj=self::sentencia("select * from seguimientos_detalle where class_id='$idtramat' and class = 'tramatoyota'");
        return !empty($obj)?$obj:[];
    }

    public static function obtenerTramaIntegracionPagn($datos){
        global $db;

        if($datos<>''){
            $datos="and (
                                 (CONCAT(apellido_asegurado,' ',nombre_asegurado) like '%".$datos."%') or (placa like '%".$datos."%') or (chasis  like '%".$datos."%') )";
        }

        $obj=self::sentencia("SELECT idtrama, anio, apellido_asegurado, campana, canal, canal_nombre, celular, chasis, cobra_comision, color,tipo_transaccion,
                                  compania, contacto_i, correo, customer, departamento, direccion, direccion_factura, doc_factura, ejecutivo, ejecutivo_canal, 
                                  estado, fecha_corte, fecha_fact, fecha_fin, fecha_inicio, fecha_registro, fecha_vencimiento, forma_pago, idoperacion,
                                  idsuariocargo, idusuario, idvehicle, idvoucher, marca, modelo, motor, nombre_asegurado, num_documento, num_telefono,
                                  numero_fatura, numero_i, numero_plaza, oferta, placa, plan, plan_pagos, policy, precio_lista, prima_fija_anual, 
                                  prima_fija_mensual, prima_variable, razonSocial_Factura, register_at, ruc_factura, servicio_adicional, tiempo, 
                                  tipoServicio, tipo_documento, tipo_transaccion, ubicacion_telefono, uso, dateInstall, chasisC, km_promotion,
                                  scorecompania, creation_at, statusseg, celular2, version, anio_modelo, ejecutivo_venta, dlr_madre,cbu
                                  FROM toyotaintegracion  
                                 where 1=1 and idoperacion is null and estado <> 99 and tipo_transaccion!='Vinculacion'  ".$datos."    
                                 ");
        return !empty($obj)?$obj:false;
    }

    public static function obtenerTramaIntegracion($idtrama,$datos){
        global $db;
        if($idtrama<>''){
            $idtrama=' and idtrama='.$idtrama.'';
        }

        if($datos<>''){
            $datos="and (
                                 (CONCAT(apellido_asegurado,' ',nombre_asegurado) like '%".$datos."%') or (placa like '%".$datos."%')  or (chasis  like '%".$datos."%') )";
        }

        $obj=self::sentencia("SELECT idtrama, anio, apellido_asegurado, campana, canal, canal_nombre, celular, chasis, cobra_comision, color,tipo_transaccion,
                                  compania, contacto_i, correo, customer, departamento, direccion, direccion_factura, doc_factura, ejecutivo, ejecutivo_canal, 
                                  estado, fecha_corte, fecha_fact, fecha_fin, fecha_inicio, fecha_registro, fecha_vencimiento, forma_pago, idoperacion,
                                  idsuariocargo, idusuario, idvehicle, idvoucher, marca, modelo, motor, nombre_asegurado, num_documento, num_telefono,
                                  numero_fatura, numero_i, numero_plaza, oferta, placa, plan, plan_pagos, policy, precio_lista, prima_fija_anual, 
                                  prima_fija_mensual, prima_variable, razonSocial_Factura, register_at, ruc_factura, servicio_adicional, tiempo, 
                                  tipoServicio, tipo_documento, tipo_transaccion, ubicacion_telefono, uso, dateInstall, chasisC, km_promotion,
                                  scorecompania, creation_at, statusseg, celular2, version, anio_modelo, ejecutivo_venta, dlr_madre,cbu,id_trama_vinculacion,idoperacion                                  
                                  FROM toyotaintegracion  
                                 where 1=1 ".$datos."  ".$idtrama."  
                                 ");

        return !empty($obj)?array_shift($obj):false;
    }


    public static function asignarTrama($idTrama){
        global $session;
        global $db;
        $sql="UPDATE toyotaintegracion SET idsuariocargo='".$session->idusuario."' WHERE idtrama ='".$idTrama."' ";
        if ($db->query($sql)) {

        }
    }

    public function updateTrama(){
        global $session;
        global $db;
        $sqlicid="UPDATE toyotaintegracion SET idoperacion='$this->idoperacion' WHERE idtrama='$this->idtrama'";
        return ($db->query($sqlicid))?true:false;
    }


    public function anulacionTrama($motivo,$tipo){
        global $db;
        global $session;
        $sqlicid="UPDATE toyotaintegracion SET estado='99',tipo_anulacion='".$tipo."',motivo_anulacion='".$motivo."',fecha_anulacion=NOW(),usuario_anulacion='".$session->idusuario."'  WHERE idtrama='$this->idtrama'";
        return ($db->query($sqlicid))?true:false;
    }

    public static function saveSeg($seg){
        global $session;
        global $db;
        $sql="INSERT INTO seguimientos_detalle (body,class,class_id,idusuario) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString($seg['body'])."' , '";
        $sql.= $db->escapeString('tramatoyota')."' , '";
        $sql.= $db->escapeString($seg['idtrama'])."' , '";
        $sql.= $db->escapeString($session->idusuario)."' )";
        if ($db->query($sql)) {
            return True;
        }
        else{
            return False;
        }
    }


    public static function obtenerTramaIntegracionPorIdOperacion($idOperacion){
        global $db;

        $obj=self::sentencia("SELECT idtrama, anio, apellido_asegurado, campana, canal, canal_nombre, celular, chasis, cobra_comision, color,tipo_transaccion,
                                  compania, contacto_i, correo, customer, departamento, direccion, direccion_factura, doc_factura, ejecutivo, ejecutivo_canal, 
                                  estado, fecha_corte, fecha_fact, fecha_fin, fecha_inicio, fecha_registro, fecha_vencimiento, forma_pago, idoperacion,
                                  idsuariocargo, idusuario, idvehicle, idvoucher, marca, modelo, motor, nombre_asegurado, num_documento, num_telefono,
                                  numero_fatura, numero_i, numero_plaza, oferta, placa, plan, plan_pagos, policy, precio_lista, prima_fija_anual, 
                                  prima_fija_mensual, prima_variable, razonSocial_Factura, register_at, ruc_factura, servicio_adicional, tiempo, 
                                  tipoServicio, tipo_documento, tipo_transaccion, ubicacion_telefono, uso, dateInstall, chasisC, km_promotion,
                                  scorecompania, creation_at, statusseg, celular2, version, anio_modelo, ejecutivo_venta, dlr_madre,cbu
                                  FROM toyotaintegracion  
                                 where 1=1  and idoperacion= '$idOperacion' limit 1
                                 ");
        return !empty($obj)?array_shift($obj):false;
    }

    public static function verificarActivacion($idopera){
        $sql =" SELECT DISTINCT t.idtrama ,t.idoperacion ,t.chasis ,t.placa,IFNULL(t.estado_vinculacion,0) estadoVinculacion ,ra.kms ,t.creation_at ,t.tipo_transaccion 
                FROM toyotaintegracion t 
                INNER JOIN request_activation ra ON ra.vin = t.chasis 
                WHERE t.tipo_transaccion = 'Directa' or (t.tipo_transaccion='Vinculacion' and t.estado_vinculacion=1)
                 and   t.idoperacion ='$idopera' ";
        $obj=self::sentencia($sql);

        return !empty($obj)?array_shift($obj):false;
    }


    public static function obtenerCantidadActivaciones($datos,$est){
        $filt="";
        if($est!=""){
            $filt.=" and ifnull(t.estado_activacion,0)='$est' ";
        }
        $sql ="select count(*) as cantidad from (  SELECT DISTINCT t.idtrama ,concat(t.apellido_asegurado,'',t.nombre_asegurado) as asegurado,
t.chasis ,case when t.placa='NULL'THEN ''ELSE t.placa END placa,
case when IFNULL(t.estado_activacion,0)='0' then 'NO ACTIVADO' ELSE 'ACTIVADO' end estadoActivacion,
ra.kms ,(t.creation_at)as fechaSolicitud ,t.tipo_transaccion ,t.estado_activacion,
(t.fecha_activacion)as fechaActivacion
FROM toyotaintegracion t 
            INNER JOIN request_activation ra ON ra.vin = t.chasis 
            where 1=1 and  t.idoperacion  is not null and (t.tipo_transaccion = 'Directa'
            or (t.tipo_transaccion='Vinculacion' 
            and t.estado_vinculacion=1 and t.estado_preactivacion=1) )
              and (concat(t.apellido_asegurado ,' ',t.nombre_asegurado) like '%$datos%' or t.placa like '%$datos%' or t.chasis like '%$datos%')
             $filt  )dat ";
        $obj=self::sentencia($sql);

        return !empty($obj)?array_shift($obj):false;

    }

    public static function listarActivaciones($datos,$est,$posisionDePagina,$registrosPorPagina){
        $filt="";
        if($est!=""){
            $filt.=" and ifnull(t.estado_activacion,0)='$est' ";
        }
        $sql =" SELECT DISTINCT t.idtrama ,concat(t.apellido_asegurado,'',t.nombre_asegurado) as asegurado,
t.chasis ,case when t.placa='NULL'THEN ''ELSE t.placa END placa,t.usuario_activacion ,t.estado,
case when IFNULL(t.estado_activacion,0)='0' then 'NO ACTIVADO' ELSE 'ACTIVADO' end estadoActivacion,
ra.kms ,(t.creation_at)as fechaSolicitud ,t.tipo_transaccion ,t.estado_activacion,
(t.fecha_activacion)as fechaActivacion
FROM toyotaintegracion t 
            INNER JOIN request_activation ra ON ra.vin = t.chasis 
            where 1=1 and  t.idoperacion  is not null and (t.tipo_transaccion = 'Directa'
            or (t.tipo_transaccion='Vinculacion' 
            and t.estado_vinculacion=1 and t.estado_preactivacion=1) )
             and (concat(t.apellido_asegurado ,' ',t.nombre_asegurado) like '%$datos%' or t.placa like '%$datos%' or t.chasis like '%$datos%')
            $filt order by t.idtrama desc LIMIT $posisionDePagina, $registrosPorPagina    ";

        $obj=self::sentencia($sql);

        return !empty($obj)?($obj):false;

    }

    public static function getActivationIdTrama($idTrama){
        $sql ="   SELECT DISTINCT t.idtrama ,t.idoperacion ,t.chasis ,t.placa,IFNULL(t.estado_vinculacion,0) estadoVinculacion ,ra.kms ,t.creation_at ,t.tipo_transaccion ,o.idcliente ,o.idvehiculo 
                FROM toyotaintegracion t 
                INNER JOIN request_activation ra ON ra.vin = t.chasis 
                INNER JOIN operaciones o ON o.idoperacion =t.idoperacion 
                WHERE (t.tipo_transaccion = 'Directa' or t.tipo_transaccion='Vinculacion')
                 and  t.idtrama ='$idTrama' ";
        $obj=self::sentencia($sql);

        return !empty($obj)?array_shift($obj):false;
    }


    public static function updateActivacion($idtrama){
        global $db;
        global $session;
        $sqlicid="UPDATE toyotaintegracion SET estado_activacion=1,fecha_activacion=NOW(),usuario_activacion='".$session->idusuario."' WHERE idtrama='$idtrama'";
        return ($db->query($sqlicid))?true:false;
    }


    public static function getCantidadInfoVinculacion($datos,$est){
        $filt="";
        if($est!=""){
            $filt.=" and ifnull(ty.estado_vinculacion,0)='$est' ";
        }
        $sql="select count(*) as cantidad from ( SELECT ty.idtrama,concat(ty.apellido_asegurado ,' ',ty.nombre_asegurado)as asegurado,concat(ty.tipo_documento,'-',ty.num_documento) as num_documento,
            ty.chasis,case when ty.placa='NULL'THEN ''ELSE ty.placa END placa,ty.fecha_registro,ty.cbu as cbu,ty.usuario_vinculacion,             
            ty2.idtrama as idtrama2,concat(ty2.apellido_asegurado ,' ',ty2.nombre_asegurado)as asegurado2,concat(ty2.tipo_documento,'-',ty2.num_documento) as num_documento2,
            ty2.chasis as chasis2,case when ty2.placa='NULL'THEN ''ELSE ty2.placa END placa2,ty2.fecha_registro as fecha_registro2,ty.fecha_vinculacion,ty2.cbu as cbu2, 
                   case when ifnull(ty.estado_vinculacion,0)=1 then 'VINCULADO' else 'NO VINCULADO' end estadoVinculacion,ty.estado_vinculacion
            FROM toyotaintegracion ty
            INNER JOIN toyotaintegracion ty2 ON ty2.idtrama=ty.id_trama_vinculacion 
            where 1=1  and  ty2.idoperacion  is not null  and ty.tipo_transaccion='Vinculacion'
                 and (concat(ty.apellido_asegurado ,' ',ty.nombre_asegurado) like '%$datos%' or ty.placa like '%$datos%' or ty.chasis like '%$datos%'
                  or concat(ty2.apellido_asegurado ,' ',ty2.nombre_asegurado) like '%$datos%' or ty2.num_documento like '%$datos%' or ty.num_documento like '%$datos%')
             $filt  )dat  ";
        $obj=self::sentencia($sql);

        return !empty($obj)?array_shift($obj):false;
    }

    public static function getInfoVinculacionById($id){
        $obj=self::sentencia(" SELECT ty.idtrama, ty.anio, ty.apellido_asegurado, ty.campana, ty.canal, ty.canal_nombre, ty.celular, ty.chasis, ty.cobra_comision, ty.color,ty.tipo_transaccion,
                                  ty.compania, ty.contacto_i, ty.correo, ty.customer, ty.departamento, ty.direccion, ty.direccion_factura, ty.doc_factura, ty.ejecutivo, ty.ejecutivo_canal, 
                                  ty.estado, ty.fecha_corte, ty.fecha_fact, ty.fecha_fin, ty.fecha_inicio, ty.fecha_registro, ty.fecha_vencimiento, ty.forma_pago, ty.idoperacion,
                                  ty.idsuariocargo, ty.idusuario, ty.idvehicle, ty.idvoucher, ty.marca, ty.modelo, ty.motor, ty.nombre_asegurado, ty.num_documento, ty.num_telefono,
                                  ty.numero_fatura, ty.numero_i,ty.numero_plaza, ty.oferta, ty.placa,ty.plan, ty.plan_pagos, ty.policy, ty.precio_lista, ty.prima_fija_anual, 
                                  ty.prima_fija_mensual, ty.prima_variable, ty.razonSocial_Factura, ty.register_at, ty.ruc_factura, ty.servicio_adicional, ty.tiempo, 
                                  ty.tipoServicio, ty.tipo_documento, ty.tipo_transaccion, ty.ubicacion_telefono, ty.uso, ty.dateInstall, ty.chasisC, ty.km_promotion,
                                  ty.scorecompania, ty.creation_at, ty.statusseg, ty.celular2, ty.version, ty.anio_modelo, ty.ejecutivo_venta, ty.dlr_madre,ty.cbu,     
                                  ty2.idtrama as idtrama2, ty2.anio as anio2, ty2.apellido_asegurado as apellido_asegurado2, ty2.campana as campana2, ty2.canal as canal2, ty2.canal_nombre as canal_nombre2, ty2.celular as celular21, ty2.chasis as chasis2, ty2.cobra_comision as cobra_comision2, ty2.color as color2,ty2.tipo_transaccion as tipo_transaccion2,
                                  ty2.compania as compania2, ty2.contacto_i as contacto_i2, ty2.correo as correo2, ty2.customer as customer2, ty2.departamento as departamento2, ty2.direccion as direccion2, ty2.direccion_factura as direccion_factura2, ty2.doc_factura as doc_factura2, ty2.ejecutivo as ejecutivo2, ty2.ejecutivo_canal as ejecutivo_canal2, 
                                  ty2.estado as estado2, ty2.fecha_corte as fecha_corte2,                                   
                                  ty2.fecha_fact as fecha_fact2, ty2.fecha_fin as fecha_fin2, ty2.fecha_inicio as fecha_inicio2, ty2.fecha_registro as fecha_registro2, 
                                  ty2.fecha_vencimiento as fecha_vencimiento2, ty2.forma_pago as forma_pago2, ty2.idoperacion as idoperacion2,
                                  ty2.idsuariocargo as idsuariocargo2, ty2.idusuario as idusuario2, ty2.idvehicle as idvehicle2, ty2.idvoucher as idvoucher2, ty2.marca as marca2, ty2.modelo as modelo2,
                                  ty2.motor as motor2, ty2.nombre_asegurado as nombre_asegurado2, ty2.num_documento as num_documento2, ty2.num_telefono as num_telefono2,
                                  ty2.numero_fatura as numero_fatura2, ty2.numero_i as numero_i2,ty2.numero_plaza as numero_plaza2, ty2.oferta as oferta2, ty2.placa as placa2,ty2.plan as plan2, 
                                  ty2.plan_pagos as plan_pagos2, ty2.policy as policy2, ty2.precio_lista as precio_lista2, ty2.prima_fija_anual as prima_fija_anual2, 
                                  ty2.prima_fija_mensual as prima_fija_mensual2, ty2.prima_variable as prima_variable2, ty2.razonSocial_Factura as razonSocial_Factura2, ty2.register_at as register_at2, 
                                  ty2.ruc_factura as ruc_factura2, ty2.servicio_adicional as servicio_adicional2, ty2.tiempo as tiempo2, 
                                  ty2.tipoServicio as tipoServicio2, ty2.tipo_documento as tipo_documento2, ty2.tipo_transaccion as tipo_transaccion2, ty2.ubicacion_telefono as ubicacion_telefono2, ty2.uso as uso2,
                                  ty2.dateInstall as dateInstall2, ty2.chasisC  as chasisC2, ty2.km_promotion as km_promotion2,
                                  ty2.scorecompania as scorecompania2, ty2.creation_at as creation_at2, ty2.statusseg as statusseg2, ty2.celular2 as celular22, ty2.version as version2, ty2.anio_modelo as anio_modelo2,
                                  ty2.ejecutivo_venta as  ejecutivo_venta2,
                                  ty2.dlr_madre as dlr_madre2,ty2.cbu as cbu2
                                  FROM toyotaintegracion ty  
                                  INNER JOIN toyotaintegracion ty2 ON ty2.idtrama=ty.id_trama_vinculacion
                                 where 1=1  and  ty.tipo_transaccion='Vinculacion' and ty.idtrama='$id'
                                 ");
        return !empty($obj)?array_shift($obj):false;
    }

    public static function getInfoVinculacion($datos,$est,$posisionDePagina,$registrosPorPagina){
        $filt="";
        if($est!=""){
            $filt.=" and ifnull(ty.estado_vinculacion,0)='$est' ";
        }
        $sql="SELECT ty.idtrama,concat(ty.apellido_asegurado ,' ',ty.nombre_asegurado)as asegurado,concat(ty.tipo_documento,'-',ty.num_documento) as num_documento,
            ty.chasis,case when ty.placa='NULL'THEN ''ELSE ty.placa END placa,ty.cbu as cbu,ty.usuario_vinculacion,  ty.creation_at as fecha_registro,ty2.creation_at  as fecha_registro2 ,           
            ty2.idtrama as idtrama2,concat(ty2.apellido_asegurado ,' ',ty2.nombre_asegurado)as asegurado2,concat(ty2.tipo_documento,'-',ty2.num_documento) as num_documento2,
            ty2.chasis as chasis2,case when ty2.placa='NULL'THEN ''ELSE ty2.placa END placa2,ty.fecha_vinculacion,ty2.cbu as cbu2,ty.estado,
                   case when ifnull(ty.estado_vinculacion,0)=1 then 'VINCULADO' else 'NO VINCULADO' end estadoVinculacion,ty.estado_vinculacion,ifnull(ty.estado_preactivacion,0) estado_preactivacion,ty.fecha_preactivacion
            FROM toyotaintegracion ty
            INNER JOIN toyotaintegracion ty2 ON ty2.idtrama=ty.id_trama_vinculacion 
            where 1=1  and  ty2.idoperacion  is not null and ty.tipo_transaccion='Vinculacion' 
                 and (concat(ty.apellido_asegurado ,' ',ty.nombre_asegurado) like '%$datos%' or ty.placa like '%$datos%' or ty.chasis like '%$datos%'
                  or concat(ty2.apellido_asegurado ,' ',ty2.nombre_asegurado) like '%$datos%' or ty2.num_documento like '%$datos%' or ty.num_documento like '%$datos%'
                 )
            $filt order by ty.idtrama desc LIMIT $posisionDePagina, $registrosPorPagina    ";

        $obj=self::sentencia($sql);

        return !empty($obj)?($obj):false;
    }

    public static function getInfoVinculacionOperacion($idTrama){
       $sql ="   SELECT ty.idtrama,ty2.idtrama as idtramavinculacion ,o.idoperacion ,o.idcliente,
            ty.nombre_asegurado,CONCAT(ty.apellido_asegurado,' ',ty.nombre_asegurado) cliente,
             CASE WHEN ty.tipo_documento!='RUC' THEN ty.apellido_asegurado ELSE '' END as apellido_asegurado,
            CASE WHEN ty.tipo_documento='RUC' THEN ty.apellido_asegurado ELSE '' END as razonSocial,
            CASE WHEN ty.tipo_documento='RUC' THEN 'Empresa' ELSE 'Natural' END tipoCliente,
            CASE WHEN ty.tipo_documento!='RUC' THEN ty.num_documento ELSE '' END as dni,
             CASE WHEN ty.tipo_documento='RUC' THEN ty.num_documento ELSE '' END as ruc,
            ty.correo ,ty.celular as telefono ,ty.celular2 as telefono2,ty.direccion,ty.chasis
            FROM toyotaintegracion ty
            INNER JOIN toyotaintegracion ty2 ON ty2.idtrama=ty.id_trama_vinculacion 
            INNER JOIN operaciones o ON o.idoperacion =ty2.idoperacion 
            where 1=1 and ty.tipo_transaccion='Vinculacion' 
            AND ty.idtrama ='$idTrama' ";
        $obj=self::sentencia($sql);
        return !empty($obj)?array_shift($obj):false;
    }

    public  function updateVinculacion($idtrama,$estado){
        global $db;
        global $session;
        $sqlicid="UPDATE toyotaintegracion SET estado='$estado',idoperacion='".$this->idoperacion."', estado_vinculacion=1,fecha_vinculacion=NOW(),usuario_vinculacion='".$session->idusuario."' WHERE idtrama='$idtrama'";
        return ($db->query($sqlicid))?true:false;
    }

    public  function updatePreActivacion($idtrama){
        global $db;
        global $session;
        $sqlicid="UPDATE toyotaintegracion SET estado_preactivacion=1,fecha_preactivacion=NOW(),usuario_vinculacion='".$session->idusuario."' WHERE idtrama='$idtrama'";
        return ($db->query($sqlicid))?true:false;
    }

    public static function getPrecioPlan($id,$tiempo){
        $sql =" select p.precio from planes_precio p where 1=1 and p.idPlan='$id' and p.tiempo='$tiempo' ";

        $obj=self::sentencia($sql);

        return !empty($obj)?array_shift($obj):false;
    }

    public static function getCantidadInfoSegTyAdv($datos,$tr,$estdAc,$estdVn,$desde,$hasta){
        $filt="";
        if($tr!=""){
            $filt.=" and t.tipo_transaccion in ($tr) ";
        }
        if($estdAc!=""){
            $filt.=" and ifnull(t.estado_activacion,0)='$estdAc' ";
        }
        if($estdVn!=""){
            $filt.=" and ifnull(t.estado_vinculacion,0)='$estdVn' ";
        }
        if($desde!="" && $hasta!=""){
            $filt.=" and DATE_FORMAT(t.creation_at,'%Y-%m-%d') BETWEEN '".$desde."' AND '".$hasta."' ";
        }
        $sql="select count(*) as cantidad from (  SELECT t.idtrama ,t.tipo_transaccion,t.creation_at as fechaRecibido ,t.num_documento ,CONCAT(t.apellido_asegurado,' ',t.nombre_asegurado) cliente,t.plan ,t.precio_lista,
              (SELECT pp.precio FROM planes_precio pp WHERE pp.tiempo=t.tiempo 
              AND pp.idPlan=(CASE WHEN t.plan like '%PERSONAS%' THEN 20 WHEN t.plan like '%EMPRESAS%' THEN 21 ELSE 0 END)) precio_tracklink,t.tiempo ,
              t.cbu,t.placa ,t.marca ,t.modelo ,t.motor ,t.chasis ,t.anio ,t.anio_modelo ,t.color,
              t.fecha_activacion,case when IFNULL(t.estado_activacion,0)=0 then 'NO ACTIVADO' ELSE 'ACTIVADO' end estadoActivacion,
              t.fecha_vinculacion,case when ifnull(t.estado_vinculacion,0)=1 then 'VINCULADO' else 'NO VINCULADO' end estadoVinculacion,
              t.dlr_madre,
              (SELECT est.estado FROM estados est WHERE est.codigo=(SELECT estado FROM operaciones op where op.idoperacion=t.idoperacion))estadoOpera,
               DATE_FORMAT((SELECT CONCAT(tp.fecha,' ',tp.hora) FROM tablaProgramacion tp WHERE tp.idoperacion = t.idoperacion limit 1),'%Y-%m-%d') fechaInstalacion,
               case when estado=1 then 'Pendiente' when estado =16 then 'Activo'
              when estado=99 then 'Anulado' END estado,idsuariocargo,
               (SELECT GROUP_concat(body) FROM seguimientos_detalle sd where sd.class_id =t.idtrama and sd.class='tramatoyota') seguimiento,
               t.id_trama_vinculacion 
              FROM toyotaintegracion t
              WHERE 1=1 
                 and (concat(t.apellido_asegurado ,' ',t.nombre_asegurado) like '%$datos%' or t.placa like '%$datos%' or t.chasis like '%$datos%'  or t.num_documento like '%$datos%')
                 $filt
              order by t.chasis desc ,t.tipo_transaccion asc
               )dat  ";
        //echo $sql;
        $obj=self::sentencia($sql);

        return !empty($obj)?array_shift($obj):false;
    }

    public static function getInfoSegTyAdv($datos,$tr,$estdAc,$estdVn,$desde,$hasta,$posisionDePagina,$registrosPorPagina){
        $filt="";
        if($tr!=""){
            $filt.=" and t.tipo_transaccion in ($tr) ";
        }
        if($estdAc!=""){
            $filt.=" and ifnull(t.estado_activacion,0)='$estdAc' ";
        }
        if($estdVn!=""){
            $filt.=" and ifnull(t.estado_vinculacion,0)='$estdVn' ";
        }
        if($desde!="" && $hasta!=""){
            $filt.=" and DATE_FORMAT(t.creation_at,'%Y-%m-%d') BETWEEN '".$desde."' AND '".$hasta."' ";
        }
        $sql="  SELECT t.idtrama ,t.tipo_transaccion,t.creation_at as fechaRecibido ,t.num_documento ,CONCAT(t.apellido_asegurado,' ',t.nombre_asegurado) cliente,t.plan ,t.precio_lista,
              (SELECT pp.precio FROM planes_precio pp WHERE pp.tiempo=t.tiempo 
              AND pp.idPlan=(CASE WHEN t.plan like '%PERSONAS%' THEN 20 WHEN t.plan like '%EMPRESAS%' THEN 21 ELSE 0 END)) precio_tracklink,t.tiempo ,
              t.cbu,t.placa ,t.marca ,t.modelo ,t.motor ,t.chasis ,t.anio ,t.anio_modelo ,t.color,
              t.fecha_activacion,case when t.tipo_transaccion='Pre-Registro' then '' when  IFNULL(t.estado_activacion,0)=0 then 'NO ACTIVADO' ELSE 'ACTIVADO' end estadoActivacion,
              case when t.tipo_transaccion='Directa' then t.creation_at else t.fecha_vinculacion end fecha_vinculacion,case when t.tipo_transaccion='Directa' then 'VINCULADO'  when ifnull(t.estado_vinculacion,0)=1 then 'VINCULADO' else 'NO VINCULADO' end estadoVinculacion,
              t.dlr_madre,
              (SELECT est.estado FROM estados est WHERE est.codigo=(SELECT estado FROM operaciones op where op.idoperacion=t.idoperacion))estadoOpera,
               DATE_FORMAT((SELECT CONCAT(tp.fecha,' ',tp.hora) FROM tablaProgramacion tp WHERE tp.idoperacion = t.idoperacion limit 1),'%Y-%m-%d') fechaInstalacion,
               case when estado=1 then 'Pendiente' when estado =16 then 'Realizado'
              when estado=99 then 'Anulado' END estadoTrama,t.estado,idsuariocargo,
               (SELECT GROUP_concat(body) FROM seguimientos_detalle sd where sd.class_id =t.idtrama and sd.class='tramatoyota') seguimiento,
               t.id_trama_vinculacion ,t.usuario_activacion ,t.usuario_vinculacion ,concat(t.tipo_anulacion,' : ',t.motivo_anulacion) motivoAnulacion,t.fecha_anulacion as fechaAnulacion,
               t.usuario_anulacion usuarioAnulacion,t.usuario_preactivacion,t.fecha_preactivacion,
               case when IFNULL(t.estado_preactivacion,0)=1 then 'PREACTIVADO' else 'PENDIENTE' end estado_preactivacion
              FROM toyotaintegracion t
              WHERE 1=1 
                 and (concat(t.apellido_asegurado ,' ',t.nombre_asegurado) like '%$datos%' or t.placa like '%$datos%' or t.chasis like '%$datos%'  or t.num_documento like '%$datos%')
                 $filt
              order by t.chasis desc ,t.tipo_transaccion asc LIMIT $posisionDePagina, $registrosPorPagina ";

        $obj=self::sentencia($sql);

        return !empty($obj)?($obj):false;
    }

    public static function getCantidadInfoSegTyAdvTyConmigo($datos,$tr,$estdAc,$estdVn,$desde,$hasta){
        $filt="";
        if($tr!=""){
            $filt.=" and t.tipo_transaccion in ($tr) ";
        }
        if($estdAc!=""){
            $filt.=" and ifnull(t.estado_activacion,0)='$estdAc' ";
        }
        if($estdVn!=""){
            $filt.=" and ifnull(t.estado_vinculacion,0)='$estdVn' ";
        }
        if($desde!="" && $hasta!=""){
            $filt.=" and DATE_FORMAT(t.creation_at,'%Y-%m-%d') BETWEEN '".$desde."' AND '".$hasta."' ";
        }
        $sql="select count(*) as cantidad from (  SELECT t.idtrama ,t.tipo_transaccion,t.creation_at as fechaRecibido ,t.num_documento ,CONCAT(t.apellido_asegurado,' ',t.nombre_asegurado) cliente,t.plan ,t.precio_lista,
              (SELECT pp.precio FROM planes_precio pp WHERE pp.tiempo=t.tiempo 
              AND pp.idPlan=(CASE WHEN t.plan like '%PERSONAS%' THEN 20 WHEN t.plan like '%EMPRESAS%' THEN 21 ELSE 0 END)) precio_tracklink,t.tiempo ,
              t.cbu,t.placa ,t.marca ,t.modelo ,t.motor ,t.chasis ,t.anio ,t.anio_modelo ,t.color,
              t.fecha_activacion,case when IFNULL(t.estado_activacion,0)=0 then 'NO ACTIVADO' ELSE 'ACTIVADO' end estadoActivacion,
              t.fecha_vinculacion,case when ifnull(t.estado_vinculacion,0)=1 then 'VINCULADO' else 'NO VINCULADO' end estadoVinculacion,
              t.dlr_madre,
              (SELECT est.estado FROM estados est WHERE est.codigo=(SELECT estado FROM operaciones op where op.idoperacion=t.idoperacion))estadoOpera,
               DATE_FORMAT((SELECT CONCAT(tp.fecha,' ',tp.hora) FROM tablaProgramacion tp WHERE tp.idoperacion = t.idoperacion limit 1),'%Y-%m-%d') fechaInstalacion,
               case when estado=1 then 'Pendiente' when estado =16 then 'Activo'
              when estado=99 then 'Anulado' END estado,idsuariocargo,
               (SELECT GROUP_concat(body) FROM seguimientos_detalle sd where sd.class_id =t.idtrama and sd.class='tramatoyota') seguimiento,
               t.id_trama_vinculacion 
              FROM toyotaintegracion t
              WHERE 1=1 
                 and (concat(t.apellido_asegurado ,' ',t.nombre_asegurado) like '%$datos%' or t.placa like '%$datos%' or t.chasis like '%$datos%'  or t.num_documento like '%$datos%')
                 AND t.estado = 16
                 $filt
              order by t.fecha_activacion desc ,t.tipo_transaccion asc
               )dat  ";
        //echo $sql;
        $obj=self::sentencia($sql);

        return !empty($obj)?array_shift($obj):false;
    }

    public static function getInfoSegTyAdvTyConmigo($datos,$tr,$estdAc,$estdVn,$desde,$hasta,$posisionDePagina,$registrosPorPagina){
        $filt="";
        if($tr!=""){
            $filt.=" and t.tipo_transaccion in ($tr) ";
        }
        if($estdAc!=""){
            $filt.=" and ifnull(t.estado_activacion,0)='$estdAc' ";
        }
        if($estdVn!=""){
            $filt.=" and ifnull(t.estado_vinculacion,0)='$estdVn' ";
        }
        if($desde!="" && $hasta!=""){
            $filt.=" and DATE_FORMAT(t.creation_at,'%Y-%m-%d') BETWEEN '".$desde."' AND '".$hasta."' ";
        }
        $sql="  SELECT t.idtrama ,t.tipo_transaccion,t.creation_at as fechaRecibido ,t.num_documento ,CONCAT(t.apellido_asegurado,' ',t.nombre_asegurado) cliente,t.plan ,t.precio_lista,
              (SELECT pp.precio FROM planes_precio pp WHERE pp.tiempo=t.tiempo 
              AND pp.idPlan=(CASE WHEN t.plan like '%PERSONAS%' THEN 20 WHEN t.plan like '%EMPRESAS%' THEN 21 ELSE 0 END)) precio_tracklink,t.tiempo ,
              t.cbu,t.placa ,t.marca ,t.modelo ,t.motor ,t.chasis ,t.anio ,t.anio_modelo ,t.color,
              t.fecha_activacion,case when t.tipo_transaccion='Pre-Registro' then '' when  IFNULL(t.estado_activacion,0)=0 then 'NO ACTIVADO' ELSE 'ACTIVADO' end estadoActivacion,
              case when t.tipo_transaccion='Directa' then t.creation_at else t.fecha_vinculacion end fecha_vinculacion,case when t.tipo_transaccion='Directa' then 'VINCULADO'  when ifnull(t.estado_vinculacion,0)=1 then 'VINCULADO' else 'NO VINCULADO' end estadoVinculacion,
              t.dlr_madre,
              (SELECT est.estado FROM estados est WHERE est.codigo=(SELECT estado FROM operaciones op where op.idoperacion=t.idoperacion))estadoOpera,
               DATE_FORMAT((SELECT CONCAT(tp.fecha,' ',tp.hora) FROM tablaProgramacion tp WHERE tp.idoperacion = t.idoperacion limit 1),'%Y-%m-%d') fechaInstalacion,
               case when estado=1 then 'Pendiente' when estado =16 then 'Realizado'
              when estado=99 then 'Anulado' END estadoTrama,t.estado,idsuariocargo,
               (SELECT GROUP_concat(body) FROM seguimientos_detalle sd where sd.class_id =t.idtrama and sd.class='tramatoyota') seguimiento,
               t.id_trama_vinculacion ,t.usuario_activacion ,t.usuario_vinculacion ,concat(t.tipo_anulacion,' : ',t.motivo_anulacion) motivoAnulacion,t.fecha_anulacion as fechaAnulacion,
               t.usuario_anulacion usuarioAnulacion,t.usuario_preactivacion,t.fecha_preactivacion,
               case when IFNULL(t.estado_preactivacion,0)=1 then 'PREACTIVADO' else 'PENDIENTE' end estado_preactivacion
              FROM toyotaintegracion t
              WHERE 1=1 
                 and (concat(t.apellido_asegurado ,' ',t.nombre_asegurado) like '%$datos%' or t.placa like '%$datos%' or t.chasis like '%$datos%'  or t.num_documento like '%$datos%')
                 and t.estado = 16
                 $filt
              order by t.fecha_activacion desc ,t.tipo_transaccion asc LIMIT $posisionDePagina, $registrosPorPagina ";

        $obj=self::sentencia($sql);

        return !empty($obj)?($obj):false;
    }

    public  function obtenerTramaExcel($tr,$estdAc,$estdVn,$desde,$hasta){
        $filt="";
        if($tr!=""){
            $filt.=" and t.tipo_transaccion in ($tr) ";
        }
        if($estdAc!=""){
            $filt.=" and ifnull(t.estado_activacion,0)='$estdAc' ";
        }
        if($estdVn!=""){
            $filt.=" and ifnull(t.estado_vinculacion,0)='$estdVn' ";
        }
        if($desde!="" && $hasta!=""){
            $filt.=" and DATE_FORMAT(t.creation_at,'%Y-%m-%d') BETWEEN '".$desde."' AND '".$hasta."' ";
        }
        $sql="  SELECT t.idtrama ,t.tipo_transaccion,t.creation_at as fechaRecibido ,t.num_documento ,CONCAT(t.apellido_asegurado,' ',t.nombre_asegurado) cliente,t.plan ,t.precio_lista,
              (SELECT pp.precio FROM planes_precio pp WHERE pp.tiempo=t.tiempo 
              AND pp.idPlan=(CASE WHEN t.plan like '%PERSONAS%' THEN 20 WHEN t.plan like '%EMPRESAS%' THEN 21 ELSE 0 END)) precio_tracklink,t.tiempo ,
              t.cbu,t.placa ,t.marca ,t.modelo ,t.motor ,t.chasis ,t.anio ,t.anio_modelo ,t.color,
              t.fecha_activacion,case when t.tipo_transaccion='Pre-Registro' then '' when  IFNULL(t.estado_activacion,0)=0 then 'NO ACTIVADO' ELSE 'ACTIVADO' end estadoActivacion,
              case when t.tipo_transaccion='Directa' then t.creation_at else t.fecha_vinculacion end fecha_vinculacion,case when t.tipo_transaccion='Directa' then 'VINCULADO'  when ifnull(t.estado_vinculacion,0)=1 then 'VINCULADO' else 'NO VINCULADO' end estadoVinculacion,
              t.dlr_madre,
              (SELECT est.estado FROM estados est WHERE est.codigo=(SELECT estado FROM operaciones op where op.idoperacion=t.idoperacion))estadoOpera,
               DATE_FORMAT((SELECT CONCAT(tp.fecha,' ',tp.hora) FROM tablaProgramacion tp WHERE tp.idoperacion = t.idoperacion limit 1),'%Y-%m-%d') fechaInstalacion,
               case when estado=1 then 'Pendiente' when estado =16 then 'Activo'
              when estado=99 then 'Anulado' END estadoTrama,idsuariocargo,
               (SELECT GROUP_concat(body) FROM seguimientos_detalle sd where sd.class_id =t.idtrama and sd.class='tramatoyota') seguimiento,
               t.id_trama_vinculacion ,concat(t.tipo_anulacion,' : ',t.motivo_anulacion) motivoAnulacion,t.fecha_anulacion as fechaAnulacion,
            t.usuario_anulacion usuarioAnulacion
              FROM toyotaintegracion t
              WHERE 1=1 
                 $filt
              order by t.chasis desc ,t.tipo_transaccion asc  ";

        $obj=self::sentencia($sql);

        return !empty($obj)?($obj):false;
    }

    public  function obtenerTramaCEMExcel($tr,$estdAc,$desde,$hasta){
        $filt="";
        if($tr!=""){
            $filt.=" and t.tipo_transaccion in ($tr) ";
        }
        if($estdAc!=""){
            $filt.=" and ifnull(t.estado_activacion,0)='$estdAc' ";
        }

        if($desde!="" && $hasta!=""){
            $filt.=" and DATE_FORMAT(t.creation_at,'%Y-%m-%d') BETWEEN '".$desde."' AND '".$hasta."' ";
        }
        $sql="   SELECT DISTINCT t.tipo_transaccion,t.creation_at AS fechaRecibido,  DATE_FORMAT(di.fecha,'%Y-%m-%d') AS fecha,di.orte,di.cliente ,di.placa ,di.chasis ,di.imei ,di.id ,
        di.odo ,di.ubicacionSistema,di.lugarInstalacion ,di.tecnicoInstalador,t.plan ,di.operador ,
        (SELECT es.estado FROM estados es WHERE es.codigo=di.estado) AS estadoOpera ,
        DATE_FORMAT((SELECT CONCAT(tp.fecha,' ',tp.hora) FROM tablaProgramacion tp WHERE tp.idoperacion = t.idoperacion LIMIT 1),'%Y-%m-%d') fechaInstalacion,
        CASE WHEN IFNULL(t2.estado_activacion,0)=0 THEN 'NO ACTIVADO' ELSE 'ACTIVADO' END estadoActivacion, t2.fecha_activacion,
        CASE WHEN t.tipo_transaccion='Directa' THEN t.creation_at else t.fecha_vinculacion END fecha_vinculacion,
        CASE WHEN t.tipo_transaccion='Directa' then 'VINCULADO'  when ifnull(t.estado_vinculacion,0)=1 then 'VINCULADO' else 'NO VINCULADO' end estadoVinculacion,
        CASE WHEN t.estado=1 THEN 'Pendiente' WHEN t.estado =16 THEN 'Activo' WHEN t.estado=99 THEN 'Anulado' END estadoTrama,
         t2.usuario_activacion ,t.usuario_vinculacion ,
          concat(t.tipo_anulacion,' : ',t.motivo_anulacion) motivoAnulacion,t.fecha_anulacion,t.usuario_anulacion 
         FROM toyotaintegracion t
         LEFT JOIN toyotaintegracion t2 on t2.id_trama_vinculacion =t.idtrama 
         LEFT JOIN operaciones o ON o.idoperacion = t.idoperacion 
         LEFT JOIN datosInstalacion di ON o.idoperacion =di.idoperacion
         WHERE 1=1 AND t.tipo_transaccion!='Vinculacion'
                 $filt
            order by t.chasis desc ,t.tipo_transaccion asc  ";

        $obj=self::sentencia($sql);

        return !empty($obj)?($obj):false;
    }


    public static function getCantidadInfoSegTyCEM($datos,$tr,$estdAc,$desde,$hasta){
        $filt="";
        if($tr!=""){
            $filt.=" and t.tipo_transaccion in ($tr) ";
        }
        if($estdAc!=""){
            $filt.=" and ifnull(t.estado_activacion,0)='$estdAc' ";
        }

        if($desde!="" && $hasta!=""){
            $filt.=" and DATE_FORMAT(t.creation_at,'%Y-%m-%d') BETWEEN '".$desde."' AND '".$hasta."' ";
        }
        $sql="select count(*) as cantidad from (   SELECT DISTINCT t.tipo_transaccion,t.creation_at AS fechaRecibido,  DATE_FORMAT(di.fecha,'%Y-%m-%d') AS fecha,di.orte,di.cliente ,di.placa ,di.chasis ,di.imei ,di.id ,
        di.odo ,di.ubicacionSistema,di.lugarInstalacion ,di.tecnicoInstalador,t.plan ,di.operador ,
        (SELECT es.estado FROM estados es WHERE es.codigo=di.estado) AS estadoOpera ,
        DATE_FORMAT((SELECT CONCAT(tp.fecha,' ',tp.hora) FROM tablaProgramacion tp WHERE tp.idoperacion = t.idoperacion LIMIT 1),'%Y-%m-%d') fechaInstalacion,
        CASE WHEN IFNULL(t2.estado_activacion,0)=0 THEN 'NO ACTIVADO' ELSE 'ACTIVADO' END estadoActivacion, t2.fecha_activacion,
        CASE WHEN t.tipo_transaccion='Directa' THEN t.creation_at else t.fecha_vinculacion END fecha_vinculacion,
        CASE WHEN t.tipo_transaccion='Directa' then 'VINCULADO'  when ifnull(t.estado_vinculacion,0)=1 then 'VINCULADO' else 'NO VINCULADO' end estadoVinculacion,
        CASE WHEN t.estado=1 THEN 'Pendiente' WHEN t.estado =16 THEN 'Activo' WHEN t.estado=99 THEN 'Anulado' END estadoTrama,
         t2.usuario_activacion ,t.usuario_vinculacion ,
          concat(t.tipo_anulacion,' : ',t.motivo_anulacion) motivoAnulacion,t.fecha_anulacion,t.usuario_anulacion 
         FROM toyotaintegracion t
         LEFT JOIN toyotaintegracion t2 on t2.id_trama_vinculacion =t.idtrama 
         LEFT JOIN operaciones o ON o.idoperacion = t.idoperacion 
         LEFT JOIN datosInstalacion di ON o.idoperacion =di.idoperacion
         WHERE 1=1 AND t.tipo_transaccion!='Vinculacion'
                 and (concat(t.apellido_asegurado ,' ',t.nombre_asegurado) like '%$datos%' or t.placa like '%$datos%' or t.chasis like '%$datos%'  or t.num_documento like '%$datos%')
                 AND t.estado = 16
                 $filt
              order by t.chasis desc ,t.tipo_transaccion asc
               )dat  ";
        //echo $sql;
        $obj=self::sentencia($sql);

        return !empty($obj)?array_shift($obj):false;
    }

    public static function getInfoSegTyCEM($datos,$tr,$estdAc,$desde,$hasta,$posisionDePagina,$registrosPorPagina){
        $filt="";
        if($tr!=""){
            $filt.=" and t.tipo_transaccion in ($tr) ";
        }
        if($estdAc!=""){
            $filt.=" and ifnull(t.estado_activacion,0)='$estdAc' ";
        }

        if($desde!="" && $hasta!=""){
            $filt.=" and DATE_FORMAT(t.creation_at,'%Y-%m-%d') BETWEEN '".$desde."' AND '".$hasta."' ";
        }
        $sql="        SELECT DISTINCT t.tipo_transaccion,t.creation_at AS fechaRecibido,  DATE_FORMAT(di.fecha,'%Y-%m-%d') AS fecha,di.orte,di.cliente ,di.placa ,di.chasis ,di.imei ,di.id ,
        di.odo ,di.ubicacionSistema,di.lugarInstalacion ,di.tecnicoInstalador,t.plan ,di.operador ,
        (SELECT es.estado FROM estados es WHERE es.codigo=di.estado) AS estadoOpera ,
        DATE_FORMAT((SELECT CONCAT(tp.fecha,' ',tp.hora) FROM tablaProgramacion tp WHERE tp.idoperacion = t.idoperacion LIMIT 1),'%Y-%m-%d') fechaInstalacion,
        CASE WHEN IFNULL(t2.estado_activacion,0)=0 THEN 'NO ACTIVADO' ELSE 'ACTIVADO' END estadoActivacion, t2.fecha_activacion,
        CASE WHEN t.tipo_transaccion='Directa' THEN t.creation_at else t.fecha_vinculacion END fecha_vinculacion,
        CASE WHEN t.tipo_transaccion='Directa' then 'VINCULADO'  when ifnull(t.estado_vinculacion,0)=1 then 'VINCULADO' else 'NO VINCULADO' end estadoVinculacion,
        CASE WHEN t.estado=1 THEN 'Pendiente' WHEN t.estado =16 THEN 'Activo' WHEN t.estado=99 THEN 'Anulado' END estadoTrama,
         t2.usuario_activacion ,t.usuario_vinculacion ,
          concat(t.tipo_anulacion,' : ',t.motivo_anulacion) motivoAnulacion,t.fecha_anulacion,t.usuario_anulacion 
         FROM toyotaintegracion t
         LEFT JOIN toyotaintegracion t2 on t2.id_trama_vinculacion =t.idtrama 
         LEFT JOIN operaciones o ON o.idoperacion = t.idoperacion 
         LEFT JOIN datosInstalacion di ON o.idoperacion =di.idoperacion
         WHERE 1=1 AND t.tipo_transaccion!='Vinculacion'
                 and (concat(t.apellido_asegurado ,' ',t.nombre_asegurado) like '%$datos%' or t.placa like '%$datos%' or t.chasis like '%$datos%'  or t.num_documento like '%$datos%')
                AND t.estado = 16
                 $filt
              order by t.chasis desc ,t.tipo_transaccion asc LIMIT $posisionDePagina, $registrosPorPagina ";

        $obj=self::sentencia($sql);

        return !empty($obj)?($obj):false;
    }




}
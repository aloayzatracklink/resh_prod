<?php 
class ReportesOnix extends Recursos
{
	function __construct()
	{		
	}
	
	public function obtenerReporteContratosOctoCantidad(){
		$result=self::sentenciaOnixV2("SELECT count(*) as cantidad FROM  vreporte_contratos_onix");
		return !empty($result)? array_shift($result):false;		
	}

	public function obtenerReporteContratosOctoRANGOCantidad($desde,$hasta){
		// $result=self::sentenciaOnixV2("SELECT count(*) as cantidad FROM  vreporte_contratos_onix WHERE  (datecreate BETWEEN '$desde' AND '$hasta') ORDER BY idcontract");
		$result = self::sentenciaOnixV2("SELECT * FROM  vreporte_contratos_onix WHERE servicio LIKE '%OCTO%' ORDER BY idcontract DESC");
		return !empty($result)? array_shift($result):false;		
	}

	public function obtenerReporteContratosOctoPaginacionRANGO($posisionDePagina,$registrosPorPagina,$desde,$hasta){
		// $result=self::sentenciaOnixV2("SELECT * FROM  vreporte_contratos_onix  WHERE  (datecreate BETWEEN '$desde' AND '$hasta') ORDER BY idcontract DESC LIMIT ");
		$result = self::sentenciaOnixV2("SELECT * FROM  vreporte_contratos_onix WHERE servicio LIKE '%OCTO%' ORDER BY idcontract DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($result)? $result:false;
	}

	public function obtenerReporteContratosOctoRANGO($desde,$hasta){
		//TODO: Change query
		$result = self::sentenciaOnixV2("SELECT * FROM  vreporte_contratos_onix WHERE servicio LIKE '%OCTO%' ORDER BY idcontract DESC LIMIT 0, 1000");
		// $result=self::sentenciaOnixV2("SELECT * FROM  vreporte_contratos_onix  WHERE  (datecreate BETWEEN '$desde' AND '$hasta') ORDER BY idcontract DESC");
		return !empty($result)? $result:false;
	}

}
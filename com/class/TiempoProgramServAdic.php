<?php


class TiempoProgramServAdic extends Recursos
{
    public $idpsa;
    public $idtec;
    public $idser;
    public $total;
    public $estado;
    public $idusuario;
    public static function obtenerTotalHorasProgramacion($idtec,$idser){
        global $db;
        if($idtec=='')
            $idtec=0;
        if($idser=='')
            $idser=0;

        $obj=self::sentencia("SELECT coalesce(total,0) as total FROM plan_serv_adicionales where idtec='$idtec' and idser='$idser' limit 1 ");
        return !empty($obj)?$obj:false;
    }

    public static function obtenerTotalHorasProgramacion2($descPlan,$idser){
        global $db;

        if($idser=='')
            $idser=0;

        $obj=self::sentencia("SELECT coalesce(total,0) as total FROM plan_serv_adicionales where idtec=(SELECT idtec from planes where plan like '%".$descPlan."%' limit 1) and idser in ($idser) limit 1 ");

        return !empty($obj)?array_shift($obj):false;
    }


}
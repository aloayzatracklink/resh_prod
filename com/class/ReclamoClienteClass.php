<?php 

if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
	class ReclamoClienteClass extends Recursos
	{

		public $idreclamo_cliente;
		public $vreclamo_cliente;
		public $dtreclamo;
		public $fk_idarea_detecta;
		public $fk_idejecutivo_responsable;
		public $fk_idarea_responsable;
		public $nombre_cliente;
		public $chasis_cliente;
		public $fk_idreclamo_plazo;
		public $tmotivo_reclamo;
		public $tinformacion_brindada;
		public $fk_idpersona_cargo_reclamo;
		public $tsolicitud_reclamo;
		public $tresolucion_reclamo;
		public $fk_idreclamo_estado;
		public $fk_idreclamo_canal;
        public $fk_idreclamo_nivel_atencion;
		public $dtcrea;
		public $usercrea;
		public $dtelimina;
		public $userelimina;
		public $dtactualiza;

		public $fechaRegistro;
		public $useractualiza;

		public $correo_cliente;

		function __construct()
		{
			global $session;
			$this->usercrea = $session->idusuario;
			$this->useractualiza = $session->idusuario;
			$this->vreclamo_cliente = "";
		}

		public function registrarReclamoCliente(){
			global $db;
			$this->fechaRegistro=date("YmdHis");
			$sql="INSERT INTO reclamo_cliente(
				fk_idarea_detecta, 
				fk_idejecutivo_responsable, 
				fk_idarea_responsable, 
				nombre_cliente, 
				chasis_cliente,
				correo_cliente,
				fk_idreclamo_plazo, 
				tmotivo_reclamo, 
				tinformacion_brindada, 
				fk_idpersona_cargo_reclamo, 
				tsolicitud_reclamo, 
				tresolucion_reclamo, 
				fk_idreclamo_estado, 
				fk_idreclamo_canal,
				fk_idreclamo_nivel_atencion,
				dtcrea,
				usercrea) VALUES ('";
			$sql.=$db->escapeString($this->fk_idarea_detecta)."' , '";
			$sql.=$db->escapeString($this->fk_idejecutivo_responsable)."' , '";
			$sql.=$db->escapeString($this->fk_idarea_responsable)."' , '";
			$sql.=$db->escapeString($this->nombre_cliente)."' , '";
			$sql.=$db->escapeString($this->chasis_cliente)."' , '";
			$sql.=$db->escapeString($this->correo_cliente)."' , '";
			$sql.=$db->escapeString($this->fk_idreclamo_plazo)."' , '";
			$sql.=$db->escapeString($this->tmotivo_reclamo)."' , '";
			$sql.=$db->escapeString($this->tinformacion_brindada)."' , '";
			$sql.=$db->escapeString($this->fk_idpersona_cargo_reclamo)."' , '";
			$sql.=$db->escapeString($this->tsolicitud_reclamo)."' , '";
			$sql.=$db->escapeString($this->tresolucion_reclamo)."' , '";
			$sql.=$db->escapeString($this->fk_idreclamo_estado)."' , '";
			$sql.=$db->escapeString($this->fk_idreclamo_canal)."' , '";
			$sql.=$db->escapeString($this->fk_idreclamo_nivel_atencion)."' , ";
			$sql.=" NOW() , '";
			$sql.=$db->escapeString($this->usercrea)."' )";
			if ($db->query($sql)){
				$this->idreclamo_cliente=$db->ultimoIdInsertado();
				if(trim($this->vreclamo_cliente)==""){
					$db->query("UPDATE 
							reclamo_cliente 
							SET 
							vreclamo_cliente='R".$this->idreclamo_cliente."'
							WHERE idreclamo_cliente='".$this->idreclamo_cliente."'");
				}else{
					$db->query("UPDATE 
							reclamo_cliente 
							SET 
							vreclamo_cliente='".$this->vreclamo_cliente."'
							WHERE idreclamo_cliente='".$this->idreclamo_cliente."'");
				}
				
				return true;
			}else{
				return false;
			}

		}

		public function actualizarReclamoCliente(){
			global $db;
			global $session;
			$sql="UPDATE reclamo_cliente SET ";
			$sql.="fk_idarea_detecta='".$db->escapeString($this->fk_idarea_detecta)."',";
			$sql.="fk_idejecutivo_responsable='".$db->escapeString($this->fk_idejecutivo_responsable)."',";
			$sql.="fk_idarea_responsable='".$db->escapeString($this->fk_idarea_responsable)."',";
			$sql.="nombre_cliente='".$db->escapeString($this->nombre_cliente)."',";
			$sql.="chasis_cliente='".$db->escapeString($this->chasis_cliente)."',";
			$sql.="fk_idreclamo_plazo='".$db->escapeString($this->fk_idreclamo_plazo)."',";
			$sql.="tmotivo_reclamo='".$db->escapeString($this->tmotivo_reclamo)."',";
			$sql.="tinformacion_brindada='".$db->escapeString($this->tinformacion_brindada)."',";
			$sql.="fk_idpersona_cargo_reclamo='".$db->escapeString($this->fk_idpersona_cargo_reclamo)."',";
			$sql.="tsolicitud_reclamo='".$db->escapeString($this->tsolicitud_reclamo)."',";
			$sql.="tresolucion_reclamo='".$db->escapeString($this->tresolucion_reclamo)."',";
			$sql.="fk_idreclamo_estado='".$db->escapeString($this->fk_idreclamo_estado)."',";
			$sql.="fk_idreclamo_canal='".$db->escapeString($this->fk_idreclamo_canal)."',";
			$sql.="fk_idreclamo_nivel_atencion='".$db->escapeString($this->fk_idreclamo_nivel_atencion)."',";
			$sql.="dtactualiza=NOW(),";
			$sql.="useractualiza='".$db->escapeString($this->useractualiza)."' ";
			$sql.="WHERE idreclamo_cliente=".$db->escapeString($this->idreclamo_cliente);
			if ($db->query($sql)){
				return true;
			}else{
				return false;
			}

		}

		


	}

}

 ?>

<?php 
/**
* 
*/
class Cotizar extends Recursos
{	
	public $cotizacionid;
	public $tipoCliente;
	public $dni;
	public $ruc;
	public $nombre;
	public $razonSocial;
	public $apellidos;
	public $contacto;
	public $contacto1;
	public $contacto2;
	public $contacto3;
	public $contacto4;
	public $correo;
	public $correo1;
	public $correo2;
	public $correo3;
	public $estado;
	public $plan;
	public $costo;
	public $cantidad;
	public $tiempoContrato;
	public $observaciones;
	public $estadoPago;
	public $voucher;
	public $descuento;
	public $idCreador;
	public $fecha;
	public $estadoCotizacion;
	public $razonCierre;
	public $fechaCierrre;
	public $idcanal;
	public $idconvenio;
	public $comision;
	public $comisiona;
	public $campana;
	function __construct()
	{	
	}
	public function registrarCotizacionNatural(){
		global $db;
		$sql="INSERT INTO cotizaciones (tipoCliente,dni,nombre,apellidos,contacto,contacto1,correo,cantidad,tiempoContrato,plan,costo,estadoPago,voucher,observaciones,idCreador,fecha) ";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString(ucfirst($this->tipoCliente))."' , '";
		$sql.= $db->escapeString(strtoupper($this->dni))."' , '";
		$sql.= $db->escapeString($this->nombre)."' , '";
		$sql.= $db->escapeString($this->apellidos)."' , '";
		$sql.= $db->escapeString($this->contacto)."' , '";
		$sql.= $db->escapeString($this->contacto1)."' , '";
		$sql.= $db->escapeString($this->correo)."' , '";
		$sql.= $db->escapeString($this->cantidad)."' , '";
		$sql.= $db->escapeString($this->tiempoContrato)."' , '";
		$sql.= $db->escapeString($this->plan)."' , '";
		$sql.= $db->escapeString($this->costo)."' , '";
		$sql.= $db->escapeString($this->estadoPago)."' , '";
		$sql.= $db->escapeString($this->voucher)."' , '";
		$sql.= $db->escapeString($this->observaciones)."' , '";
		$sql.= $db->escapeString($this->idCreador)."' , '";
		$sql.= $db->escapeString($this->fecha)."')";
		if ($db->query($sql)) {
			$this->cotizacionid=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}
	}
	public function registrarCotizacionEmpresa(){
		global $db;
		$sql="INSERT INTO cotizaciones (tipoCliente,ruc,razonSocial,contacto,contacto1,correo,cantidad,tiempoContrato,plan,costo,estadoPago,voucher,observaciones,idCreador,fecha)";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString($this->tipoCliente)."' , '";
		$sql.= $db->escapeString(strtoupper($this->ruc))."' , '";
		$sql.= $db->escapeString($this->razonSocial)."' , '";
		$sql.= $db->escapeString($this->contacto)."' , '";
		$sql.= $db->escapeString($this->contacto1)."' , '";
		$sql.= $db->escapeString($this->correo)."' , '";		
		$sql.= $db->escapeString($this->cantidad)."' , '";
		$sql.= $db->escapeString($this->tiempoContrato)."' , '";
		$sql.= $db->escapeString($this->plan)."' , '";
		$sql.= $db->escapeString($this->costo)."' , '";
		$sql.= $db->escapeString($this->estadoPago)."' , '";
		$sql.= $db->escapeString($this->voucher)."' , '";
		$sql.= $db->escapeString($this->observaciones)."' , '";
		$sql.= $db->escapeString($this->idCreador)."' , '";
		$sql.= $db->escapeString($this->fecha)."')";
		if ($db->query($sql)) {
			$this->cotizacionid=$db->ultimoIdInsertado();
				// header("Location:registrarVehiculo.php?id=".$this->cotizacionid."");
			return true;
		}else{
			return false;
		}
	}

					// estado de cotizaciones
	public static function cambiarEstadoDni($estado,$dni){
		global $db;
		$sql="UPDATE cotizaciones SET estado ='$estado' WHERE dni='$dni'";
		$db->query($sql);
	}

	public static function cambiarEstadoRuc($estado,$ruc){
		global $db;
		$sql="UPDATE cotizaciones SET estado ='$estado' WHERE ruc='$ruc'";
		$db->query($sql);

	}
		// El estado cotiacion tiene 3 estados las cueles son : 1).-activo --33 . 2).-acepta.--31 4).-no acepta--18


	public function cambiarEstadoCotizacion($estado,$id){
		global $db;
		$this->fechaCierrre=date("YmdHis");
		$sql="UPDATE cotizaciones SET estadoCotizacion ='$estado', ";
		$sql.= " razonCierre = '".$db->escapeString($this->razonCierre)."', ";
		$sql.= " fechaCierrre = '".$db->escapeString($this->fechaCierrre)."' WHERE cotizacionid='$id'";
		$db->query($sql);
	}


		////end :b
	


	public static function obtenerEstadoDni($dni){
		global $db;
		$dn=$db->escapeString($dni);
		$obtenerEstadoDniQ=self::sentencia("SELECT estado,cantidad FROM cotizaciones WHERE dni = '$dn' AND estadoCotizacion='33' LIMIT 1");
		return  !empty($obtenerEstadoDniQ) ? array_shift($obtenerEstadoDniQ) : false ;
	}

	public static function obtenerEstadoRuc($ruc){
		global $db;
		$ru=$db->escapeString($ruc);
		$obtenerEstadoRucQ=self::sentencia("SELECT estado,cantidad FROM cotizaciones WHERE ruc = '$ru' AND estadoCotizacion='33'  LIMIT 1");
		return  !empty($obtenerEstadoRucQ) ? array_shift($obtenerEstadoRucQ) : false ;
	}
					// estado de cotizaciones


	public static function verificarPorDni($dni){
		global $db;
		$dn=$db->escapeString($dni);
		$verificarPorDniQ=self::sentencia("SELECT estadoCotizacion FROM cotizaciones WHERE dni = '$dn' AND estadoCotizacion='33' LIMIT 1");
		return  empty($verificarPorDniQ) ? FALSE : TRUE ;
	}
	public static function verificarPorRuc($ruc){
		global $db;
		$ru=$db->escapeString($ruc);
		$verificarPorRucQ=self::sentencia("SELECT estadoCotizacion FROM cotizaciones WHERE ruc = '$ruc' AND  estadoCotizacion='33' LIMIT 1");
		return  empty($verificarPorRucQ) ? FALSE : TRUE ;
	}

			// obtener Datos por dni
	public static function obtenerDatosPorDni($dni){
		global $db;
		$dn=$db->escapeString($dni);
		$obtenerDatosPorDniQ=self::sentencia("SELECT * FROM cotizaciones WHERE dni = '$dn' AND  estadoCotizacion='33' ORDER BY cotizacionid DESC LIMIT 1");
		return  !empty($obtenerDatosPorDniQ) ? array_shift($obtenerDatosPorDniQ) : false ;
	}

	public static function obtenerDatosPorRuc($ruc){
		global $db;
		$dn=$db->escapeString($ruc);
		$obtenerDatosPorRucsQ=self::sentencia("SELECT * FROM cotizaciones WHERE ruc = '$dn' AND  estadoCotizacion='33' ORDER BY cotizacionid DESC LIMIT 1");
		return  !empty($obtenerDatosPorRucsQ) ? array_shift($obtenerDatosPorRucsQ) : false ;
	}

	public static function obtenerCotizacion(){
		global $session;
		$obtenerUsuarioQueryArray=self::sentencia("SELECT * FROM cotizaciones WHERE estadoCotizacion='33' AND idCreador='$session->idusuario' ORDER BY cotizacionid DESC ");
		return  !empty($obtenerUsuarioQueryArray) ? $obtenerUsuarioQueryArray : false ;
	}
	
	public static function obtenerCotizacionCantidad(){
		global $session;
		$obtenerCotizacionCantidad=self::sentencia("SELECT COUNT(*) as cantidad FROM cotizaciones WHERE estadoCotizacion='33' AND idCreador='$session->idusuario' LIMIT 1 ");
		return  !empty($obtenerCotizacionCantidad) ? array_shift($obtenerCotizacionCantidad) : false ;
	}

	public function verificarExistenciaDni($dni){
		$verificarExistenciaDniQ=self::sentencia("SELECT dni FROM cotizaciones WHERE dni='$dni' AND estadoCotizacion='33' ");
		return  !empty($verificarExistenciaDniQ) ? true : false ;
	}

	public function verificarExistenciaRuc($ruc){
		$verificarExistenciaRucQ=self::sentencia("SELECT ruc FROM cotizaciones WHERE ruc='$ruc' AND estadoCotizacion='33'");
		return  !empty($verificarExistenciaRucQ) ? true : false ;
	}

	public static function obtenerCotizacionPorId($idd){
		global $db;
		$i=$db->escapeString($idd);
		$obtenerCotizacionPorIdQ=self::sentencia("SELECT * FROM cotizaciones WHERE cotizacionid ='$i' LIMIT 1 ");
		return  !empty($obtenerCotizacionPorIdQ) ? array_shift($obtenerCotizacionPorIdQ) : false ;
	}
	public function actualizarDni($idCotizacion){
		global $db;
		$sql="UPDATE cotizaciones SET dni = '";
		$sql.=$db->escapeString($this->dni)."' ";
		$sql.=" WHERE cotizacionid= '$idCotizacion'";
		if($db->query($sql)){
			return true;
		}else{
			return false;
		}
	}
	public function actualizarRuc($idCotizacion){
		global $db;
		$sql="UPDATE cotizaciones SET ruc = '";
		$sql.=$db->escapeString($this->ruc)."' ";
		$sql.=" WHERE cotizacionid= '$idCotizacion'";
		if($db->query($sql)){
			return true;
		}else{
			return false;
		}

	}

	public function actualizarEstadoPagoDni($dni){
		global $db;
		$sql="UPDATE cotizaciones SET estadoPago = '28' ,";	
		$sql.=" voucher = '";
		$sql.=$db->escapeString($this->voucher)."' ";
		$sql.=" WHERE dni= '$dni'";
		if($db->query($sql)){

		}

	}
	public function actualizarEstadoPagoRuc($ruc){
		global $db;
		$sql="UPDATE cotizaciones SET estadoPago = '30' ,";	
		$sql.=" voucher = '";
		$sql.=$db->escapeString($this->voucher)."' ";
		$sql.=" WHERE ruc= '$ruc'";
		if($db->query($sql)){

		}

	}

	public static function obtenerEstadoPagoDni($dni){
		$obtenerEstadoPagoQ=self::sentencia("SELECT estadoPago FROM cotizaciones WHERE dni='$dni' AND (estadoPago='28' OR estadoPago = '41')");
		return  !empty($obtenerEstadoPagoQ) ? true : false ;
	}
	public static function obtenerEstadoPagoRuc($ruc){
		$obtenerEstadoPagoRucQ=self::sentencia("SELECT estadoPago FROM cotizaciones WHERE ruc='$ruc' AND (estadoPago='30' OR estadoPago = '42') ");
		return  !empty($obtenerEstadoPagoRucQ) ? true : false ;
	}
	public function actualizarCorreoCliente($idCotizacion){
		global $db;
		$actualizarCorreoClienteQ="UPDATE cotizaciones SET correo = '";
		$actualizarCorreoClienteQ.=$db->escapeString($this->correo)."' WHERE cotizacionid='".$db->escapeString($idCotizacion)."'";
		if ($db->query($actualizarCorreoClienteQ)) 
			return true;
		else
			return false;

	}
	public function completaridCanal(){
		global $db;
		$sql="UPDATE cotizaciones SET idcanal ='";
		$sql.=$this->idcanal."', ";
		$sql.=" idconvenio='";
		$sql.=$this->idconvenio."', ";
		$sql.=" comisiona='";
		$sql.=$this->comisiona."', ";
		$sql.=" comision='";
		$sql.=$this->comision."' WHERE cotizacionid ='";
		$sql.=$this->cotizacionid."'";
		return ($db->query($sql))?true:false;
	}

}


?>
<?php


class Intermediarios extends Recursos
{
    public $id;
    public $dni;
    public $executive;
    public $location;
    public $mail;
    public $name;
    public $phone;
    public $phone2;
    public $phone3;
    public $ruc;
    public $titular;
    public $type;
    public $created;

    function __construct()
    {

    }

    public function obetenerDatos($datos,$nusu)
    {
        global $session;
        $sql = self::sentencia("select id,dni,executive,location,mail,name,phone,phone2,phone3,ruc,titular,`type`,created from channel WHERE (executive LIKE '%$datos%' OR dni like '%$datos%') AND  created='$nusu' limit 20");
        return !empty($sql) ? $sql : false;
    }
    public function obetenerDatosTodos($datos)
    {
        global $session;
        $sql = self::sentencia("select id,dni,executive,location,mail,name,phone,phone2,phone3,ruc,titular,`type`,created from channel WHERE (executive LIKE '%$datos%' OR dni like '%$datos%') limit 20");
        return !empty($sql) ? $sql : false;
    }


    public static function obetenerDatosPorId($idint){
        global $db;
        $obtenerDatosCanalPorIdQ=self::sentencia("SELECT id,dni,executive,location,mail,name,phone,phone2,phone3,ruc,titular,`type` FROM channel WHERE  id=".$db->escapeString($idint)." LIMIT 1");
        return !empty($obtenerDatosCanalPorIdQ) ? array_shift($obtenerDatosCanalPorIdQ) : false;
    }

}
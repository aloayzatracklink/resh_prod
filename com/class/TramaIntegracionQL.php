<?php

class TramaIntegracionQL extends Recursos
{
    public $idtramaql;
    public $fecha_registro;
    public $tipo_documento;
    public $num_documento;
    public $nombre_asegurado;
    public $apellido_paterno;
    public $apellido_materno;
    public $apellido_asegurado;
    public $celular;
    public $direccion;
    public $correo;
    public $departamento;
    public $placa;
    public $chasis;
    public $motor;
    public $marca;
    public $modelo;
    public $color;
    public $anio;
    public $tipo_transacción;
    public $policy;
    public $producto;
    public $fecha_vigencia;
    public $fecha_fin_vigencia;
    public $uso;
    public $prima_neta;
    public $prima_neta_anual_dolares;
    public $vigente;
    public $tiene_gps;
    public $num_gps;
    public $num_telefono;
    public $compania;
    public $plan;
    public $servicio_adicional;
    public $precio_lista;
    public $tiempo;
    public $campana;
    public $canal;
    public $canal_nombre;
    public $ejecutivo_canal;
    public $razonSocial_Factura;
    public $ruc_factura;
    public $direccion_factura;
    public $register_at;
    public $estado;
    public $forma_pago;
    public $idoperacion;
    public $idsuariocargo;
    public $cobra_comision;
    public $doc_factura;

    public $asegurado;
    public $estadoOpera;
    public $fechaInstalacion;

    public $cantidad;
    public $seguimiento;
    public $body;
    public $fecha;


    function __construct()
    {

    }



    public static function obtenerCantidadTramaIntegracionPag(){
        $obj=self::sentencia("SELECT COUNT(*) as cantidad  FROM tramaQualitas tql where 1=1 and idoperacion is null and estado <> 99 LIMIT 1");
        return !empty($obj)? array_shift($obj):false;
    }

    public static function obtenerTramaIntegracionPag($posisionDePagina,$registrosPorPagina){
        $obj=self::sentencia("SELECT idtramaql, fecha_registro, policy, fecha_vigencia, apellido_asegurado, nombre_asegurado, num_documento, coalesce(num_telefono,'')num_telefono,
                                   coalesce(celular,'') celular, correo, direccion, chasis, placa, anio, marca, modelo, motor, tipo_documento, color, compania, plan, servicio_adicional,
                                  precio_lista, tiempo, campana, canal, canal_nombre, ejecutivo_canal, razonSocial_Factura, ruc_factura, direccion_factura, 
                                  register_at, estado, forma_pago, idoperacion, idsuariocargo,doc_factura,cobra_comision
                                  FROM tramaQualitas 
                                  where 1=1 and idoperacion is null and estado <> 99 order by fecha_registro desc 
                                  LIMIT $posisionDePagina, $registrosPorPagina 
                               ");
        return !empty($obj)?$obj:false;
    }

    public static function getSEg($idtramaf){
        $obj=self::sentencia("select * from seguimientos_detalle where class_id='$idtramaf' and class = 'tramaQualitas'");
        return !empty($obj)?$obj:[];
    }



    public static function obtenerTramaIntegracionPagn($datos){
        global $db;

        if($datos<>''){
            $datos="and (
                                 (CONCAT(apellido_asegurado,' ',nombre_asegurado) like '%".$datos."%') or (placa like '%".$datos."%') or (policy  like '%".$datos."%') or (chasis  like '%".$datos."%') or (marca  like '%".$datos."%'))";
        }

        $obj=self::sentencia("SELECT idtramaql, fecha_registro, policy, fecha_vigencia, apellido_asegurado, nombre_asegurado, num_documento, coalesce(num_telefono,'')num_telefono,
                                   coalesce(celular,'') celular, correo, direccion, chasis, placa, anio, marca, modelo, motor, tipo_documento, color, compania, plan, servicio_adicional,
                                  precio_lista, tiempo, campana, canal, canal_nombre, ejecutivo_canal, razonSocial_Factura, ruc_factura, direccion_factura, 
                                  register_at, estado, forma_pago, idoperacion, idsuariocargo,doc_factura,cobra_comision
                                  FROM tramaQualitas  
                                 where 1=1 and idoperacion is null and estado <> 99  ".$datos."    
                                 ");
        return !empty($obj)?$obj:false;
    }

    public static function obtenerTramaIntegracion($idtrama,$datos){
        global $db;
        if($idtrama<>''){
            $idtrama=' and idtramaql='.$idtrama.'';
        }

        if($datos<>''){
            $datos="and (
                                 (CONCAT(apellido_asegurado,' ',nombre_asegurado) like '%".$datos."%') or (placa like '%".$datos."%') or (policy  like '%".$datos."%')  or (chasis  like '%".$datos."%') or (modelo  like '%".$datos."%'))";
        }

        $obj=self::sentencia("SELECT idtramaql, fecha_registro, policy, fecha_vigencia,fecha_fin_vigencia, apellido_asegurado, nombre_asegurado, num_documento, coalesce(num_telefono,'')num_telefono,
                                   coalesce(celular,'') celular, correo, direccion, chasis, placa, anio, marca, modelo, motor, tipo_documento, color, compania, plan, servicio_adicional,
                                  precio_lista, tiempo, campana, canal, canal_nombre, ejecutivo_canal, razonSocial_Factura, ruc_factura, direccion_factura, 
                                  register_at, estado, forma_pago, idoperacion, idsuariocargo,doc_factura,cobra_comision
                                  FROM tramaQualitas  
                                 where 1=1 ".$datos."  ".$idtrama."  
                                 ");
        return !empty($obj)?array_shift($obj):false;
    }


    public static function asignarTrama($idTrama){
        global $session;
        global $db;
        $sql="UPDATE tramaQualitas SET idsuariocargo='".$session->idusuario."' WHERE idtramaql ='".$idTrama."' ";
        if ($db->query($sql)) {

        }
    }




    public function updateTrama(){
        global $db;
        $sqlicid="UPDATE tramaQualitas SET idoperacion='$this->idoperacion' WHERE idtramaql='$this->idtramaql'";
        return ($db->query($sqlicid))?true:false;
    }


    public static function saveSeg($seg){
        global $session;
        global $db;
        $sql="INSERT INTO seguimientos_detalle (body,class,class_id,idusuario) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString($seg['body'])."' , '";
        $sql.= $db->escapeString('tramaQualitas')."' , '";
        $sql.= $db->escapeString($seg['idtrama'])."' , '";
        $sql.= $db->escapeString($session->idusuario)."' )";
        if ($db->query($sql)) {
            return True;
        }
        else{
            return False;
        }
    }



    public function updateTrama2(){
        global $db;
        $sqlicid="UPDATE tramaQualitas SET chasis=TRIM('$this->chasis') , placa=TRIM('$this->placa') WHERE idtramaql='$this->idtramaql'";

        return ($db->query($sqlicid))?true:false;
    }


    public static function obtenerCantidadTramaIntegracionPagGENERAL(){
        $obj=self::sentencia("SELECT COUNT(*) as cantidad  FROM tramaQualitas where 1=1  LIMIT 1 ");
        return !empty($obj)? array_shift($obj):false;
    }

    public static function obtenerTramaIntegracionPagGENERAL($posisionDePagina,$registrosPorPagina){
        $obj=self::sentencia("SELECT idtramaql, fecha_registro, policy, fecha_vigencia, apellido_asegurado, nombre_asegurado, num_documento, coalesce(num_telefono,'')num_telefono,
                                   coalesce(celular,'') celular, correo, direccion, chasis, placa, anio, marca, modelo, motor, tipo_documento, color, compania, plan, servicio_adicional,
                                  precio_lista, tiempo, campana, canal, canal_nombre, ejecutivo_canal, razonSocial_Factura, ruc_factura, direccion_factura, 
                                  register_at, estado, forma_pago, idoperacion, idsuariocargo,doc_factura,cobra_comision,estado
                                  FROM tramaQualitas 
                                  where 1=1  order by fecha_registro  desc  LIMIT $posisionDePagina, $registrosPorPagina  
                                 ");
        return !empty($obj)?$obj:false;
    }


    public static function obtenerBUSQUEDAPagGENERAL2($datos){
        global $db;

        if($datos<>''){
            $datos=" and (
                                 (CONCAT(apellido_asegurado,' ',nombre_asegurado) like '%".$datos."%') or (policy like '%".$datos."%') or (placa like '%".$datos."%') or (chasis  like '%".$datos."%') or (marca  like '%".$datos."%'))";
        }

        $obj=self::sentencia("SELECT idtramaql, fecha_registro, policy, fecha_vigencia, apellido_asegurado, nombre_asegurado, num_documento, coalesce(num_telefono,'')num_telefono,
                                   coalesce(celular,'') celular, correo, direccion, chasis, placa, anio, marca, modelo, motor, tipo_documento, color, compania, plan, servicio_adicional,
                                  precio_lista, tiempo, campana, canal, canal_nombre, ejecutivo_canal, razonSocial_Factura, ruc_factura, direccion_factura, 
                                  register_at, estado, forma_pago, idoperacion, idsuariocargo,doc_factura,cobra_comision,estado
                                  FROM tramaQualitas  
                                 where 1=1  ".$datos."    
                                 ");
        return !empty($obj)?$obj:false;

    }

    public static function verEstadoOperacion($idoperacion){
        global $db;

        $obj=self::sentencia("select estado from operaciones where idoperacion=$idoperacion ");
        return !empty($obj)?array_shift($obj):false;

    }

    public function obtenerTramaExcel($fechaInicio,$fechaFin){
        $sql="SELECT idtramaql, fecha_registro, policy, fecha_vigencia,fecha_fin_vigencia, apellido_asegurado, nombre_asegurado,CONCAT(apellido_asegurado,' ',nombre_asegurado) asegurado, num_documento, coalesce(num_telefono,'')num_telefono,
                                   coalesce(celular,'') celular, correo, direccion, chasis, placa, anio, marca, modelo, motor, tipo_documento, color, compania, plan, servicio_adicional,
                                  precio_lista, tiempo, campana, canal, canal_nombre, ejecutivo_canal, razonSocial_Factura, ruc_factura, direccion_factura, 
                                  register_at, estado, forma_pago, idoperacion, idsuariocargo,doc_factura,cobra_comision,
                                  case when tramaQualitas.estado=1 then 'Pendiente' when tramaQualitas.estado =16 then 'Activo' when estado=99 then 'Anulado' END estado,
                                   IFNULL((SELECT est.estado FROM estados est WHERE est.codigo=(SELECT estado FROM operaciones op where op.idoperacion=tramaQualitas.idoperacion)),'Pendiente')estadoOpera,
                                     DATE_FORMAT((SELECT CONCAT(tp.fecha,' ',tp.hora) FROM tablaProgramacion tp WHERE tp.idoperacion = tramaQualitas.idoperacion limit 1),'%Y-%m-%d') fechaInstalacion,
                                   (SELECT GROUP_concat(body,'(',DATE_FORMAT(fecha,'%d/%m/%Y'),')') FROM seguimientos_detalle sd where sd.class_id =tramaQualitas.idtramaql and sd.class='tramaQualitas') seguimiento
                                  FROM tramaQualitas  where 1=1 and fecha_registro>='$fechaInicio' and fecha_registro<='$fechaFin'
        order by idtramaql asc ";

        $dtp=self::sentencia($sql);
        return !empty($dtp)? $dtp:false;
    }


}
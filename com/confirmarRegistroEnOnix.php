<?php $nav="confirmarRegistroEnOnix" ;?>
<?php include("../includes/head.php") ?>
<link href="../assets/calendario/fullcalendar.min.css" rel="stylesheet">
<link href="../assets/calendario/fullcalendar.print.min.css" rel='stylesheet' media='print'>
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<link href='../assets/calendario/scheduler.min.css' rel='stylesheet' />
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->confirmarRegistroEnOnix=="si"): ?>
<?php 
	$vhObj=new Vehiculo;    
    $echo=array();
    $errores=array();
 ?>
<?php 
    if (isset($_POST['idOperacion'])) {
        Log::registrarLog('operaciones',$_POST['idOperacion'],$_POST,"Confirmar Registros");
        $clienteObj=new Cliente;
        $vehiculoobj=new Vehiculo;          
        $datosFacturacionObj=new DatosFacturacion; 
        $opeObj=new Operacion;

        $datosFacturacionObj->tipoDocumentoFacturacion=limpiar($_POST['tipoDocumentoFacturacion']);
        ///APLICAR CONDICIONAL
        if (isset($_POST['voucherAnterior'])) {
            $datosFacturacionObj->voucher=$_POST['voucherAnterior'];
        }else{
           if (!empty($_FILES['voucher']['name'])){
              $extencionesAbilitadas= array('jpg','jpeg','png','gif','pdf','docx');
              $nombreArchivo=$_FILES['voucher']['name'];
              $extencionArchivo=explode('.', $nombreArchivo);
              $extencionArchivo=strtolower(end($extencionArchivo));
              $tamanioArchivo=$_FILES['voucher']['size'];
              $ubicacionTemporal=$_FILES['voucher']['tmp_name'];
              if (!in_array($extencionArchivo,$extencionesAbilitadas)) {
              $errores[]='La extencion de la imagen deve ser jpg jpeg png gif pdf';
              }
              if ($tamanioArchivo>5242880) {
              $errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
              }
              if ($nombreArchivo=='') {
                $errores[]="Ingrese nombre de archivo o el archivo correctamente";
              }
              $fecha=date('YmdHis');
                move_uploaded_file($_FILES['voucher']['tmp_name'], '../public/vouchers/'.$fecha.quitarEspacio($_FILES['voucher']['name'])); 
              $datosFacturacionObj->voucher=$fecha.quitarEspacio($nombreArchivo);
            }
        }
        if (isset($_POST['rucFacturacion'])) {
            $datosFacturacionObj->rucFacturacion=limpiar($_POST['rucFacturacion']);
        }
        if (isset($_POST['razonSocialFacturacion'])) {
            $datosFacturacionObj->razonSocialFacturacion=limpiar($_POST['razonSocialFacturacion']);
        }
        if (isset($_POST['direccionFiscal'])) {
            $datosFacturacionObj->direccionFiscal=limpiar($_POST['direccionFiscal']);
        }
        if (isset($_POST['telefonoFacturacion'])) {
            $datosFacturacionObj->telefonoFacturacion=limpiar($_POST['telefonoFacturacion']);
        }
        if (isset($_POST['entregaEnInstalacion'])) {
            $datosFacturacionObj->entregaEnInstalacion=limpiar($_POST['entregaEnInstalacion']);
        }else{
            $datosFacturacionObj->direccionEntrega=limpiar($_POST['direccionEntrega']);        
            $datosFacturacionObj->referenciaEntrega=limpiar($_POST['referenciaEntrega']);
            $datosFacturacionObj->fechaHoraDeEntrega=limpiar($_POST['horarioEntrega']);
            $datosFacturacionObj->distritoEntrega=limpiar($_POST['distritoEntrega']);
            $datosFacturacionObj->recibidoPor=limpiar($_POST['personaEntrega']);            
        }
        if (isset($_POST['dniFacturacion'])) {
            $datosFacturacionObj->dniFacturacion=limpiar($_POST['dniFacturacion']);
        }        
        $datosFacturacionObj->datosfacturacionid=limpiar($_POST['idFacturacion']);
        if (!$datosFacturacionObj->actualizarDatosDeFacturacion()) {
            $errores[]="error en el registro de los datos de facutracion";
        }  

        if (!empty($errores) )  
            die("error here x32131231241");

        if (!isset($_POST['enOnix'])) {
            $clienteObj->idcliente=limpiar($_POST['idcliente']);
            if($clienteObj->registradoEnOnix()){

            }else{
                $errores[]="Erro en dar check :v 21312412";
            }
        }
        $vehiculoobj->idvehiculo=limpiar($_POST['idvehiculo']);
        if (isset($_POST['campana'])) {
            $vehiculoobj->campana=limpiar($_POST['campana']);                                
        }else{
            $vehiculoobj->campana='no';                    
        }
        if ($vehiculoobj->confirmarRegistroEnOnix()) {
            $echo[]="Guardado como registrado en Onix";
            if(!$opeObj->actualizarDatosDeComercialesOperaciones($_POST['idOperacion'])){
                die("ERROR FATAL escribir inmediatamente a pvasquez@tracklink.pe o al 987009687 en horario de oficia al anexo 913 gracias");
            }
        }else{
                $errores[]="Erro en dar check :v 2131241 vh";
        }
        }
if (isset($_POST['idcita'])) {
    Log::registrarLog('cita',$_POST['idcita'],$_POST,"Confirmar cita");
    $citaObj=new Cita;
    $objCalendario=new Calendario();
    $citaObj->fecha=$_POST['fecha']." ".$_POST['hora'];
    $citaObj->nombreCliente=$_POST['nombrecl'];
    $citaObj->placa=$_POST['plc'];
    $citaObj->seguro=$_POST['sgr'];
    $citaObj->observacion=$_POST['obs'];
    $citaObj->idcita=$_POST['idcita'];
    $citaObj->tiempo=$_POST['tiempo'];
    if ($citaObj->confirmarCitaOpe()) {
        $trabajo ="Cita ";
        $color="#c2c4d3";
        $colorLetra="#09090a";
        $estado="  ESTADO = PENDIENTE";
        $objCalendario->title=$trabajo." en taller. Placa: ".$_POST['plc']." ".$estado;
        $objCalendario->color=$color;
        $objCalendario->textColor=$colorLetra;
        $objCalendario->startdate=$_POST['fecha']." ".$_POST['hora'];
        $objCalendario->idcita=$citaObj->idcita;
        if ($objCalendario->registrarFechaInstalacion()) {
            $echo[]="Datos guardados correctamente";            
        }        
    }else{
        $errores[]="Fail detected ";
    }
}
 ?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<body>
<style>
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
}
</style>
<div class="se-pre-con"></div>
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <?php include("../includes/menuIzquierdo.php") ?>
            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                   <?php include("../includes/navegacionArriba.php") ?>
               </nav>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins" >
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-md-6"><h5>Clientes con programación pendiente</h5></div>
                        <div class="col-md-6 pull-right"><form action="" method="post"><button type="submit" class="btn btn-success"><i class="fa fa-refresh"></i> Actualizar</button></form></div>
                    </div>        
                </div>
                <div class="ibox-content" >               
                    <input type="text" class="form-control input-sm m-b-xs" id="filter"
                    placeholder="Filtrar aquí">
<div class="table-responsive">
                    <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Cliente(s)</th>
                                <th>Placa</th>
                                <th data-hide="phone,tablet">Plan</th>
                                <th>Estado(s)</th>
                                <th data-hide="all">Chasis</th>
                                <th>Fecha Registro</th>
                                <th>Ejecutivo comercial</th>
                                <th>Acción</th>
                            </tr>
                        </thead>                          
                        <tbody>

<?php
    if ($obMenu->administrarComercial=="si") {
?>

                          <?php $oData=$vhObj->obtenerVehiculosParaRegistrarEnOnixAdmin() ?>
<?php
    }else{
?>
                          <?php $oData=$vhObj->obtenerVehiculosParaRegistrarEnOnix() ?>
<?php
   } 
?>
                          <?php $n=1; ?>
                          <?php if ($oData): ?>
                            <?php foreach ($oData as $var): ?>
                                <tr>
                                    <td><?php echo $n++ ?></td>
                                    <td><?php $clienDatos=Cliente::obtenerDatosDeclientePorId($var->idcliente); echo $retVal = ($clienDatos->tipoCliente=="Natural") ? $clienDatos->nombre." ".$clienDatos->apellidosC : $clienDatos->razonSocial ; ?></td>
                                    <td><?php $vhDatos=Vehiculo::obtenerVehiculo($var->idvehiculo);echo $vhDatos->placa ?></td>
                                    <td><?php echo $vhDatos->plan ?></td>
                                    <td ><?=  obtenerEstado($var->estado) ?></td>
                                    <td ><?php echo $vhDatos->chasis ?></td>
                                    <td ><?php echo haceMinimo($var->fechaCreacion) ?></td>
                                    <?php $usuarioa=Usuario::obtenerUsuario($var->idcreador) ?>                 
                                    <td ><?= $usuarioa->usuarioNombre." ".$usuarioa->usuarioApellidoPaterno ?></td>
                                    <td><button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#ordenes" data-id="<?php echo $var->idoperacion ?>">Elegir <i class="fa fa-cog"> </i></button></td>
                                    <!-- contacatenar al id cita Para diferenciarlo-->
                                </tr>
                            <?php endforeach ?>                           
                          <?php endif ?>                       
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
</div>
                </div>
            </div>
                   

            
        </div>

        <?php include("../includes/footer.php") ?>
    </div>
</div>
            <div class="modal inmodal" id="ordenes" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content animated bounceIn">
                        <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                            <h4 class="modal-title">Confirmar registros</h4>
                            <small class="font-bold"><div class="form-group ">
                            </div></small>
                        </div>
                        <form  role="form" method="post" enctype="multipart/form-data">

                            <div class="modal-body" style="background: #fff;padding-top: 0">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                <button type="submit" name="verificado" id="agregarVehiculoBttn" class="btn btn-primary" >Confirmar</button>
                                <div class="row">
                                    <div class="col-lg-12">              
                                      <label id="mostrar" class="text-center text-danger">Limite de taller alcanzado por favor intenta en otro horario</label>
                                    </div>
                                  </div>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>
<!-- Mainly scripts -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/validacionFactura.pabel.js"></script>
 <?php include_once('../includes/script.php') ?>
<script>
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
<script>
    $(document).ready(function(){
        $('.footable').footable();
        $('.footable2').footable();
    });
    $('#ordenes').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        $.ajax({
            type : 'post', 
                url : 'modal/confirmarRegistroEnOnixModal', //Here you will fetch records 
                data :  'rowid='+ rowid, //Pass $id
                success : function(data){
                    $('.modal-body').html(data);//Show fetched data from database
                }
            });
    });
      $('#mostrar').hide();
 function capacidad(cantidad){
   $('#agregarVehiculoBttn').prop('disabled', true);        
   $('#mostrar').show();           
  }
 function limpiarCapacidad(){
    $('#agregarVehiculoBttn').prop('disabled', false);        
    $('#mostrar').hide();  
  }
 $( ".nombreContactoo" ).focus(function() {
  esconder();
  });
</script>

</body>

</html>

  <?php else: ?>
    <h2>presione f5</h2>
    <?php  irA("ingresar") ?> 
<?php endif ?>

<?php $nav="GestionDocumentos" ?>
<?php require("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>

<?php if (Operacion::verificarSiRolUsuario(4)||$session->idusuario=='2'||$session->idusuario =='278'): ?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">
<link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<style>
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
    }
    .pagination1{margin:0;padding:0;}
    .pagination1 li{
        display: inline;
        padding: 6px 10px 6px 10px;
        border: 1px solid #ddd;
        margin-right: -1px;
        font: 15px/20px Arial, Helvetica, sans-serif;
        background: #FFFFFF;
        box-shadow: inset 1px 1px 5px #F4F4F4;
    }
    .pagination1 li a{
        text-decoration:none;
        color: rgb(89, 141, 235);
    }
    .pagination1 li.first {
        border-radius: 5px 0px 0px 5px;
    }
    .pagination1 li.last {
        border-radius: 0px 5px 5px 0px;
    }
    .pagination1 li:hover{
        background: #CFF;
    }
    .pagination1 li.active{
        background: #F0F0F0;
        color: #333;
    }

</style>

<div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <?php include("../includes/menuIzquierdo.php") ?>

            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                   <?php include("../includes/navegacionArriba.php") ?>
               </nav>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins col-lg-12">
                <div class="ibox-title">
                    <h5>Busqueda de documentos</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
       <div class="ibox-content">
                    <input type="text" class="form-control input-sm m-b-xs" id="filter"
                    placeholder="Buscar ...">
                    <div class="table-responsive">
                        <table class=" table  table-hover" >
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Trabajo</th>
                                <th>Cliente(s)</th>
                                <th>Chasis</th>
                                <th>Placa</th>
                                <th>Fecha Instalación</th>
                                <th>Plan</th>
                                <th>Estado</th>
                                <th>Accion</th>
                            </tr>
                        </thead>    
                        <tbody id="ndata">
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>

            <!-- modal aqui desarrollaremos   -->

            <div class="modal inmodal" id="ordenes" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content animated bounceIn">
                        <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                            <h4 class="modal-title">Datos Cliente</h4>
                        </div>
                        <form  role="form" method="post" >

                            <div class="modal-body" style="background: #fff;padding-top: 0">
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                <button type="submit" name="verificado" class="btn btn-primary" value="<?php echo Token::generar() ?>">Guardar Cambios</button>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>

        </div>
        <?php include("../includes/footer.php") ?>

    </div>
</div>



<!-- Mainly scripts -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<!-- Steps -->
<script src="../assets/js/plugins/staps/jquery.steps.min.js"></script>

<!-- Jquery Validate -->
<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>

    <script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
    <script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>

 <?php include_once('../includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>


    <script>
      /*  var eventos={}
        function loadData() {
            var datosS =  $('#filter').val();
            var page = $('#resultado').attr("data-page");
    datosS = datosS.replace('<>/"', '');

        $.ajax({
            url: 'ajax/gestionDocumentosData',
            type: 'POST', // Send post data
            data: {page: page,datos:datosS},
            success: function (data) {
                $('#ndata').html("");
                $('#ndata').html(data);
            }
        });
} */
    $(document).ready(function(){
        $("#ndata").on( "click", ".pagination1 a", function (e){
            e.preventDefault();
            var datosS = $('#filter').val();
            var page = $(this).attr("data-page");
            datosS = datosS.replace('<>/"', '');
            $.ajax({
                url: 'ajax/gestionDocumentosData',
                type: 'POST', // Send post data
                data: {page: page,datos:datosS},
                success : function(data)
                {
                    $('#ndata').html(data);
                }
            });
        });

        $("#filter").keypress(function(e)
        {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code==13){
                $('#ndata').html("");
                var datosS =  $('#filter').val();
                var page = $('#ndata').attr("data-page");
                datosS = datosS.replace('<>/"', '');

                $.ajax({
                    url: 'ajax/gestionDocumentosData',
                    type: 'POST', // Send post data
                    data: {page: page,datos:datosS},
                    success: function (data) {
                        $('#ndata').html(data);
                    }
                });
            }
        });

        $('.footable').footable();
        $('.footable2').footable();
    });

    $('#ordenes').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        $('.modal-body').html("");
        $.ajax({
            type : 'post',
            url : 'modal/gestionDocumentosDataModal', //Here you will fetch records
            // url : 'modal/operacionesDataModal', //Here you will fetch records
            data :  'rowid='+ rowid, //Pass $id
            success : function(data){
            $('.modal-body').html(data);//Show fetched data from database
        }
    });
    });
</script>
</body>

</html>
    <script>
        $(window).load(function() {
            $(".se-pre-con").fadeOut("slow");
        });
    </script>

<?php else: ?>
    <?php $session->salir();
    echo "NO NO NO NO ; presiona F5"
    ?>
<?php endif ?>

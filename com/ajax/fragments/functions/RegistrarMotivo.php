<?php 
require_once("../../../../includes/init.php");

if($_POST){
    $idcomision = $_POST['idcomision'];
    $comision = Comisiones::obtenerDatosDeComision($idcomision);
    $clienteAnterior = Cliente::obtenerDatosDeclientePorId($comision->idcliente);
    
    $auditoriaCom = new AuditoriaComisiones();
    $auditoriaCom->idcomision = $idcomision;
    $auditoriaCom->codigoFacturacion = $comision->numeroFacturacion;
    $auditoriaCom->idTitularAnterior = $comision->idcliente;
    $auditoriaCom->titularAnterior = $clienteAnterior->nombre;
    $auditoriaCom->motivo = $_POST['reason'];
    $auditoriaCom->idUsuarioCreador = $session->idusuario;
    $auditoriaCom->fechaCreada = date("YmdHis");
    $auditoriaCom->fechaActualizada = date("YmdHis");

    //* Registrar en auditoria y eliminar titular
    $auditoriaCom->registrarAuditoria();
    $comision->cambiarTitularNew();
    // //? Enviar Mail
    // ob_start();
    // include("../../../../assets/email_templates/com/AlertaCambioTitular.php");
    // $mail->isSMTP();
    // // $mail->SetLanguage('es', ');
    // $mail->CharSet = 'UTF-8';
    // $mail->isSMTP();
    // $mail->Host = MAIL_HOST;
    // $mail->SMTPAuth = true;
    // $mail->Username = MAIL_USERNAME;
    // $mail->Password = MAIL_PASSWORD;
    // $mail->SMTPSecure = MAIL_SECURE;
    // $mail->Port = MAIL_PORT;
    // $mail->setFrom(MAIL_USERNAME, 'Tracklink');
    // $mail->addAddress("aloayza@tracklink.pe");
    // $mail->isHTML(true);                                  // Set email format to HTML
    // $mail->Body = ob_get_clean();
    // $mail->Subject = "Cambio de titularidad - $comision->numeroFacturacion";
    // $mail->send();
    // echo json_encode(["send"=>1, "msg"=>"Se envió notificación al area correspondiente."], JSON_UNESCAPED_UNICODE);
}else{
    echo "Bad Request: No es tipo POST";
}
?>
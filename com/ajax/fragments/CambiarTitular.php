<!-- //*```````````````````````````Template ```````````````````` -->
<style>
    /*** Estilos de este componente */
    .mx-smart{
        margin-left: 0.5rem;
        margin-right: 0.5rem;
    }
    .my-2{
        margin-top: 2rem;
        margin-bottom: 2rem;
    }
    .mt-1{
        margin-top: 1rem;
        margin-bottom: 1rem;
    }
    .w-full{
        width: 100% !important;
    }
    .flex{
        display: flex;
    }
    .justify-end{
        justify-content: end;
    }
    #reasonChangeRH{
        border: #1ab394;
        padding: 8px;
    }
    #reasonChangeRH:focus{
        outline: #1ab394;
        border: #1ab394;
    }
    /** Fix swal alert */
    .swal2-popup{
        z-index: 999999999 !important;
    }
</style>
<div class='col-md-12 my-2'>
    <div class='row'>
        <div class='col-md-12'>
            <p>
                <strong class='col-md-12'>
                    Por favor, es necesario ingresar el motivo y/o comentario por el cual se desea realizar el cambio de titularidad
                </strong>
            </p>
            <br>
        </div>
        <div class='col-md-12'>
            <textarea name='reasonChangeRH' id='reasonChangeRH' rows='5' class="w-full"></textarea>
        </div>
    </div>
    <div class='w-full flex justify-end'>
        <button type="button" id='confirmReason' class='btn btn-primary mt-1'>Confirmar</button>
    </div>
</div>
<!--//*````````````````````````` Script JS ```````````````` -->
<script>
    $('#confirmReason').click(() => {
        var reason = $('#reasonChangeRH').val();
        if(reason == null || reason == ""){
            swal({
                icon: 'error',
                title: "Error!",
                text: "Por favor ingrese un motivo de cambio de titular antes de continuar",
                // type: "success",
                confirmButtonText: "De acuerdo",
                closeOnConfirm: false
            });
            return;
        }
        var fragmentRH = $('.change-owner');
        $.ajax({
            type: 'POST',
            url: 'ajax/fragments/functions/RegistrarMotivo',
            data: {reason: reason, idcomision: <?= $_POST['idcomision']?>},
            success: function(data){
                reason = "";
                fragmentRH.html('');
                swal({
                    title: "Ok",
                    text: "Se guardó el motivo correctamente",
                    type: "success",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                });
                
            }
        })
    });
    
</script>

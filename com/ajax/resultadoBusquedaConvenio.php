     <?php require_once("../../includes/init.php") ?>
  <?php if ($_POST): ?> 
<?php 

   $datos=limpiar($_POST['datos']); 
   $convenio=$_POST['convenio'];
   $datos="%".$datos."%";
   $fecha='';
  if (isset($_POST["fecha"])) {
    $fecha=$_POST['fecha'];
    $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
    $desde=date("YmdHis",strtotime($desde));  
    $hasta=date("Ymd",strtotime($hasta)); 
    $hasta=$hasta."235959";
  }

 ?>  
<?php 
$comisionesObj= new Comisiones;
$comisionesObj->idconvenio=limpiar($convenio);
if (empty($fecha)) {
  $datosA=$comisionesObj->buscarParaFiltroConvenios($datos);
} else {
  $datosA=$comisionesObj->buscarParaFiltroConveniosFecha($datos,$desde,$hasta);  
}

$n=1;
 ?>
  <?php if ($datosA): ?>                
  <?php foreach ($datosA as $var): ?>    
    <?php $usuario=Usuario::obtenerUsuario($var->idCreador)  ?>
    <?php $convenio=Canales::obtenerDatosDeConvenioPorId($var->idconvenio) ?>
    <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>
    <tr class="animated bounceIn">
         <td><?php echo $n++ ?></td>  
          <td><?= $convenio->nombreConvenio ?></td>       
          <td><?= (!empty($var->fechaFacturador))?formato($var->fechaFacturador):obtenerEstado($var->estadoFacturacion) ?></td>
         <td><?= ($clienteObj->tipoCliente=="Natural") ? $clienteObj->nombre." ".$clienteObj->apellidosC : $clienteObj->razonSocial ; ?></td>
          <td><?= (!empty($var->numeroFacturacion))?$var->numeroFacturacion:obtenerEstado($var->estadoFacturacion)  ?></td>
          <td><?= limpiarPALABRAGPSSMART($datosVehiculo->plan) ?></td>
          <td><?= $var->tiempo ?></td>
          <td><?= obtenerEstado($var->estadoPago) ?></td>
          <td><?= $usuario->usuarioNombre." ".$usuario->usuarioApellidoPaterno ?></td>
          <td><?= haceMinimo($var->fechaRegistro) ?></td>
         <!--  <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" data-id="<?php echo $var->datosfacturacionid ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;</td> -->
    </tr>
<?php endforeach ?>
   <?php else: ?>
   <tr>   
        <td colspan="8" class="text-center">No se encontro ningun registro con <h2><?= $_POST['datos'] ?>.</h2> </td>
   </tr>
   <?php endif ?>   
 <?php endif ?> 

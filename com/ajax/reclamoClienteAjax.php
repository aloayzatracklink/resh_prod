<?php
require __DIR__ . "/../../vendor/autoload.php";
include_once("../../includes/init.php");

if(!$session->yaEstaIngresado()) {
    $response["error"] = true;
    $response["msg"]= "No está en session";
    $response["status"]= "session";
    echo json_encode($response, JSON_INVALID_UTF8_IGNORE);
    return; 
}

use Curl\Curl;

$response = ["error"=>false, "msg"=>""];
parse_str($_POST['formData'], $formData);
$response["formData"]=$formData;
//if (Token::verificar($formData['hdSecuritySave'])) {
    try {
        $reclamo                             = new ReclamoClienteClass();
        $reclamo->correo_cliente             = $formData['correo_cliente']; //AGREGADO
        $reclamo->vreclamo_cliente           = $formData['vreclamo_cliente'];
        $reclamo->idreclamo_cliente          = $formData['idreclamo_cliente'];
        $reclamo->fk_idarea_detecta          = $formData['idarea_detecta'];
        $reclamo->fk_idarea_responsable      = $formData['area_responsable'];
        $reclamo->fk_idejecutivo_responsable = $formData['idejecutivo_responsable'];
        $reclamo->nombre_cliente              = $formData['cliente_nombre']; //por nombre  cliente_id cliente_nombre
        $reclamo->chasis_cliente              = $formData['idchasis']; //por chasis idvehiculo idchasis
        $reclamo->fk_idreclamo_plazo         = $formData['idreclamo_plazo'];
        $reclamo->tmotivo_reclamo            = $formData['motivo_reclamo'];
        $reclamo->tinformacion_brindada      = $formData['informacion_brindada'];
        $reclamo->fk_idpersona_cargo_reclamo = $formData['persona_cargo'];
        $reclamo->tsolicitud_reclamo         = $formData['solicitud_reclamo'];
        $reclamo->tresolucion_reclamo        = $formData['resolucion_reclamo'];
        $reclamo->fk_idreclamo_estado        = $formData['idreclamo_estado'];
        $reclamo->fk_idreclamo_canal         = $formData['idreclamo_canal'];
        $reclamo->fk_idreclamo_nivel_atencion= $formData['idreclamo_nivel_atencion'];

        if(is_numeric($formData['idreclamo_cliente'])){
            $reclamo->actualizarReclamoCliente();
            $response["msg"].="Se actualizó el registro satisfactoriamente";
        }else{
            $reclamo->registrarReclamoCliente();
            if(is_numeric($reclamo->idreclamo_cliente)){
                $response["msg"].="Se guardó el nuevo registro satisfactoriamente";

                $baseUrl = getBaseUrl();

                $curl = new Curl();

                /**
                 * Notificacion al area responsable
                 */
                $urlNotificacion = "$baseUrl"."/com/email/ReclamoClienteNotificacion";

                $curl->post($urlNotificacion,[
                    'idreclamo_cliente' => $reclamo->idreclamo_cliente
                    //'cliente_nombre' => $reclamo->fk_idcliente
                ]);

                if ($curl->error) {
                    #echo 'Error: ' . $curl->errorMessage . "\n";
                    $curl->diagnose();
                } else {
                    $respSendingNotif = json_decode($curl->response);
                    $response["respSendingNotif"]=$respSendingNotif;

                    if ($respSendingNotif->send == 0) {
                        $response["error"] = true;
                        $response["status"] = 'correo';
                    }


                }

                /**
                 * Notificacion al cliente
                 */
                $urlNotificacionCliente = "$baseUrl"."/com/email/ReclamoClienteNotificacionCliente";

                $curl->post($urlNotificacionCliente,[
                    'idreclamo_cliente' => $reclamo->idreclamo_cliente
                ]);

                if ($curl->error) {
                    #echo 'Error: ' . $curl->errorMessage . "\n";
                    $curl->diagnose();
                } else {
                    $respSendingNotifClient = json_decode($curl->response);
                    $response["respSendingNotifClient"]=$respSendingNotifClient;
                    if ($respSendingNotifClient->send == 0) {
                        $response["error"] = true;
                        $response["status"] = 'correo';
                    }
                }

            }else{

            }
        }
    
    } catch (Exception $e) {
        $response["error"] = true;
        $response["status"]= "correo";
        $response["msg"]=$e->getMessage();
        //echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    //$response["formData"]=$formData;
    echo json_encode($response);
// }else{
//     $response["error"] = true;
//     echo json_encode($response);
// }
?>
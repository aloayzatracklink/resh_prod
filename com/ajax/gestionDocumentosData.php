<?php 
require_once("../../includes/init.php");

if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    if(isset($_POST["page"])){
        $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
        if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
    }else{
        $numeroDePagina = 1;
    }

    $objPSV= new Operacion;
    $datos=limpiar($_POST['datos']);
    $cantidadO=$objPSV->countGetDatosInstalacionReact($datos);
    $registrosPorPagina=15;
    $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
    $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
    $vehiculoObjOp=$objPSV->getDatosInstalacionReact($datos,$posisionDePagina,$registrosPorPagina);

    $n=$posisionDePagina+1;
	/*$vehiculoObjOp=Operacion::vehiculosParaBienvenida();	*/
 ?>
 <?php if ($vehiculoObjOp): ?>
 <?php foreach ($vehiculoObjOp as $var): ?>
 	<tr>
 		<td><?= $n++ ?></td>
 		<td><?= $var->trabajo ?></td>
 		<td><?= $retVal = ($var->tipoCliente=="Natural") ? strtoupper($var->apellidosC." ".$var->nombre) : strtoupper($var->razonSocial) ; ?></td>
 		<td><?= $var->chasis ?></td>
        <td><?= $var->placa ?></td>
 		<td><?= formatoSinHora($var->fechaInstalacion) ?></td>
        <td class="center"><?= $var->plan ?> <?= $var->campana ?></td>
        <td class="center"><?= obtenerEstado($var->estado) ?></td>
 		<td class="center "><button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#ordenes" data-id="<?php echo $var->datosfacturacionid ?>">Elegir <i class="fa fa-cog"> </i></button></td>
 	</tr>
 <?php endforeach ?>

        <tr>
            <td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
            <td colspan="9">
                <?= paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
            </td>
        </tr>

    <?php else: ?>
        <tr>
            <td colspan="9" class="text-center">No se encontro registro alguno
            </td>
        </tr>
    <?php endif ?>
<?php } ?>
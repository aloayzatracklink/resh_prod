
<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $fecha='';
  if (isset($_POST["fecha"])) {
    $fecha=strip_tags($_POST['fecha']);
  $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
    $desde=date("YmdHis",strtotime($desde));  
    $hasta=date("Ymd",strtotime($hasta)); 
    $hasta=$hasta."235959";
  }
  $trama=new TramaIntegracionQL();
    $tramac = $trama::obtenerCantidadTramaIntegracionPagGENERAL();
    $registrosPorPagina=30;
    $paginasEnTotal=ceil($tramac->cantidad/$registrosPorPagina);
    $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
    $datos=$trama::obtenerTramaIntegracionPagGENERAL($posisionDePagina,$registrosPorPagina);    

  } 

?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>    
    <?php $usuarioOPE = Usuario::obtenerUsuario(is_null($var->idsuariocargo)?0:$var->idsuariocargo)  ?>
    <?php $operaci = Operacion::obtenerOperacionPorId($var->idoperacion)  ?>
     <!-- bg-warning success-->
    <tr>
          <td><?= $var->fecha_registro ?></td>       
          <td><?= $var->policy ?></td>
          <td><?= $var->apellido_asegurado." ".$var->nombre_asegurado ?></td>
          <td><?= $var->placa ?> | <?= $var->chasis ?></td>
          <td><?= $var->marca ?></td>
          <td><?= $var->anio ?></td>
        <td><?= ($operaci) ? obtenerEstado($operaci->estado):"<span class='label label-warning'>Pendiente</span>" ?></td>
        <!--<td><?=  obtenerEstadoTRAMA($var->estado) ?></td> -->
          <td><?= $var->fecha_vigencia ?></td>
          <td><?= ($operaci)?$operaci->fechaInstalacion:'-' ?></td>  
          <td><?= $usuarioOPE->usuarioNombre." ".$usuarioOPE->usuarioApellidoPaterno ?></td>
          <td>
          <?php foreach (TramaIntegracionQL::getSeg($var->idtramaql) as $tr ): ?>
                <?= $tr->body ?> <br> 
          <?php endforeach ?>
            </td>
            <td class="center ">
                    <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0" data-toggle="modal"
                            data-target="#bateriasOModal" onclick="limpiarTrama()"  data-idInt="<?= $var->idtramaql ?>"><i class="fa fa-eye"></i></button>
            </td>
    </tr>

<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($tramac->cantidad<$registrosPorPagina) ? $tramac->cantidad : $registrosPorPagina ; ?> de <?= $tramac->cantidad  ?></td>
  <td colspan="8">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$tramac->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="78" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>

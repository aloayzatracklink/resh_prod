
<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $fecha='';
  if (isset($_POST["fecha"])) {
    $fecha=strip_tags($_POST['fecha']);
  $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
    $desde=date("YmdHis",strtotime($desde));  
    $hasta=date("Ymd",strtotime($hasta)); 
    $hasta=$hasta."235959";
  }

  if(isset($_POST['selectOption'])){
    $option = $_POST['selectOption'];
    if($option == null){
      $trama=new TramaIntegracion();
      $tramac = $trama::obtenerCantidadTramaIntegracionPagGENERAL();
      $registrosPorPagina=30;
      $paginasEnTotal=ceil($tramac->cantidad/$registrosPorPagina);
      $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
      $datos=$trama::obtenerTramaIntegracionPagGENERAL($posisionDePagina,$registrosPorPagina);
    }
    if($option == "reasignacion"){
      $trama=new TramaIntegracion();
      $tramac = $trama::obtenerCantidadTramaIntegracionPagGENERAL();
      $registrosPorPagina=30;
      $paginasEnTotal=ceil($tramac->cantidad/$registrosPorPagina);
      $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
      $datos=$trama::obtenerTramasPorReasignacion($posisionDePagina,$registrosPorPagina);
      // $datos = [];
      // foreach($datosFilter as $dato){
      //   if($dato->cant_reasignacion > 1){
      //     var_dump($dato->cant_reasignacion);
      //     array_push($datos, $dato);
      //   }
      // }
    }
  }else{
    $trama=new TramaIntegracion();
    $tramac = $trama::obtenerCantidadTramaIntegracionPagGENERAL();
    $registrosPorPagina=30;
    $paginasEnTotal=ceil($tramac->cantidad/$registrosPorPagina);
    $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
    $datos=$trama::obtenerTramaIntegracionPagGENERAL($posisionDePagina,$registrosPorPagina); 
  }

} 

?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>    
    <?php $usuarioOPE=Usuario::obtenerUsuario(is_null($var->idsuariocargo)?0:$var->idsuariocargo)  ?>
    <?php $operaci=Operacion::obtenerOperacionPorId($var->idoperacion)  ?>
     <!-- bg-warning success-->
    <tr style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>">
          <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>"><?= $var->fecha_registro ?></td>       
          <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>"><?= $var->numero_plaza ?></td>
          <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>"><?= $var->apellido_asegurado." ".$var->nombre_asegurado ?></td>
          <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>"><?= $var->placa ?></td>
          <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>"><?= $var->marca ?></td>
          <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>"><?= $var->anio ?></td>
          <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>"><?= ($operaci) ? obtenerEstado($operaci->estado):obtenerEstadoTRAMA($var->estado) ?></td>  
          <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>"><?= obtenerEstadoTRAMA($var->estado) ?></td>
        <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>"><?= obtenerEstadoSeguimientoTRAMA($var->statusseg) ?></td>
        <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>"><?= $var->fecha_inicio ?></td>
          <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>"><?= ($operaci)?$operaci->fechaInstalacion:'-' ?></td>
        <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>"><?= $var->creation_at ?></td>
        <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>"><?= $var->atraso ?></td>
          <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>"><?= $usuarioOPE->usuarioNombre." ".$usuarioOPE->usuarioApellidoPaterno ?></td>
          <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>">
          <?php foreach (TramaIntegracion::getSeg($var->idtrama) as $tr ): ?>
                <?= $tr->body ?> <br> 
          <?php endforeach ?>
            </td>
          <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>">
		<?php if(!is_null($var->statusv)): ?>
		FOTOS: <?= $var->statusv ?> | <?= $var->catidadregv ?>
		<br>
		A365: <?= $var->completadov ?>
		<?php endif ?>
          </td>

        <td style="<?= ($var->cant_reasignacion > 1) ? 'background-color: #F0DBAF;' : ''?>"><?= $var->reasignacion ?></td>
            <td class="center ">
                    <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0" data-toggle="modal"
                            data-target="#bateriasOModal" onclick="limpiarTrama()"  data-idInt="<?= $var->idtrama ?>"><i class="fa fa-eye"></i></button>
            </td>
    </tr>

<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($tramac->cantidad<$registrosPorPagina) ? $tramac->cantidad : $registrosPorPagina ; ?> de <?= $tramac->cantidad  ?></td>
  <td colspan="8">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$tramac->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="78" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>

<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  
  $criterio = trim($_POST['criterio']);

  $clientes =new Cliente();
  //$cantidadO=$clientes->obtenerBuscarClientesCantidad($criterio);
  $clientesOnix = new Onix();
  $cantidadO=$clientesOnix->obtenerBuscarClientesCantidadOnix($criterio);

  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  $n=1;

  //$datos=$clientes->obtenerBuscarClientesPaginacion($criterio, $posisionDePagina,$registrosPorPagina);

  $datos=$clientesOnix->obtenerBuscarClientesPaginacionOnix($criterio, $posisionDePagina,$registrosPorPagina);         

?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>    
    <tr>
        <td><?php echo $n++ ?></td>  
        <td data-id="<?=$var->idcustomer;?>"><?=$var->identdoc;?></td>
        <td><?=$cliente=$var->firstname." ".$var->lastname;?></td>
        <!-- <td><//?=$dni=$var->identdoc;?></td> -->
        <td>
          <button class="btn btn-primary " type="button"  style="padding-top: 0;padding-bottom: 0" onclick="seleccionarCliente('<?=$var->idcustomer?>', '<?=$cliente;?>', '<?=$var->identdoc;?>','<?=$var->mail;?>')">Elegir <i class="fa fa-cog"></i></button>&nbsp;
        </td>
    </tr>
<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="7">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="78" class="text-left">No se encontró registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>

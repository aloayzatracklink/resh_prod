<?php require_once("../../includes/init.php") ?>  
<?php require_once("../class/ReclamoClienteReporteClass.php")?>
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $fecha='';

  if (isset($_POST["fecha"])) {
    $fecha=strip_tags($_POST['fecha']);
  $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
  $desde=date("YmdHis",strtotime($desde));  
  $hasta=date("Ymd",strtotime($hasta)); 
  $hasta=$hasta."235959";
  }
  // $estado= $_POST["estado"];
  $estado = isset($_POST["estado"]) ? $_POST["estado"] : null;

  $reporte=new ReclamoClienteReporteClass();
  #$comisionObj->idconvenio=limpiar($convenio);
  if (empty($fecha) || $fecha == null){    
    $cantidadO=$reporte->obtenerReporteReclamosClienteCantidad();    
  }else{
    $cantidadO=$reporte->obtenerReporteReclamosClienteRANGOCantidad($desde,$hasta, $estado);  
  }
  #print_r($cantidadO); die(); 

  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  // echo $cantidadO->cantidad; 
  $n=1;
  if (empty($fecha) || $fecha == null) {
    $datos=$reporte->TablaListaReclamos($posisionDePagina,$registrosPorPagina);  
  } else {
    $datos=$reporte->obtenerReporteReclamosClientePaginacionRANGO($posisionDePagina,$registrosPorPagina,$desde,$hasta, $estado);        
  }  

?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>    
    <tr class="animated bounceIn">
        <td><?php echo $n++ ?></td>  
        <td data-id="<?=$var->idreclamo_cliente;?>"><?=$var->vreclamo_cliente;?></td>
        <td><?=$var->fecha_reclamo;?></td>
        <td><?=$var->canal_descripcion;?></td>
        <td><?=$var->nivel_atencion_descripcion;?></td>
        <td><?=$var->area_detecta_nombre;?></td>
        <td><?=$var->ejecutivo_responsable_nombre;?></td>
        <td><?=$var->area_responsable_nombre;?></td>
        <td><?=$var->cliente_nombre;?></td>
        <td><?=$var->vehiculo_chasis;?></td>
        <td><?=$var->plazo_descripcion;?></td>
        <td><div style="max-height:200px; overflow:auto;"><?=$var->tmotivo_reclamo;?><div></td>
        <td><div style="max-height:200px; overflow:auto;"><?=$var->tinformacion_brindada;?><div></td>
        <td><?=$var->persona_cargo_reclamo_nombre;?></td>
        <td><?=$var->tsolicitud_reclamo;?></td>
        <td><div style="max-height:200px; overflow:auto;"><?=$var->tresolucion_reclamo;?><div></td>
        <td class="text-center"><div style="width: 120px; padding:2px;color:#fff; background:<?=$var->estado_color;?>;"><?=$var->estado_descripcion;?></div></td>
        <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#modalCrud" data-id="<?php echo $var->idreclamo_cliente ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;</td>
    </tr>
<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="7">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="78" class="text-left">No se encontró registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>
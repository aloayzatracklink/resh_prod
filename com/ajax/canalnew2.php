<html lang="en">
<head>
	<meta charset="UTF-8">
</head>
<body>
<?php 
if($_POST) 
  {
require_once("../../includes/init.php");	
$datos= limpiar(strip_tags($_POST['canalId'])); 
$objCanal=new Intermediarios();
$obMenu=Usuario::obtenerMenu($session->idusuario);
$obUsu = Usuario::obtenerUsuarioxId($session->idusuario);
$nusuio = str_replace("@tracklink.pe","",$obUsu->usuario);

if ($obMenu->comisiones=="si"){
    $datoss=$objCanal->obetenerDatosTodos($datos);

}else{
    $datoss=$objCanal->obetenerDatos($datos,$nusuio);
}


?>
<div class='row'>
<div class='col-md-12'>
<div id='lista' class='table-responsive'>
<table class='table table-hover'>
    <thead>
        <tr>
            <th>N° Documento</th>
            <th>Descripcion</th>
            <th>Mail</th>
            <th>Telefonos</th>
            <th>canal</th>

            <?php
            if ($obMenu->comisiones=="si"){
                ?>
                <th>Creado Por</th>
                <?php
            }
            ?>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php
    if ($datoss) {
        foreach($datoss as $var):
       ?>
        <tr>
            <td><?= $var->dni ?></td>
            <td><?= $var->executive ?></td>
            <td><?= $var->mail ?></td>
            <td><?= $var->phone." - ".$var->phone2 ?></td>
            <td><?= $var->type ?></td>
            <?php
            if ($obMenu->comisiones=="si"){
                $nidcr = Usuario::obtenerUsuarioxUsu($var->created."@tracklink.pe");
                $ou=Usuario::obtenerUsuario($nidcr->idusuario)
                ?>
                <td><?php  echo $ou->usuarioNombre." ".$ou->usuarioApellidoPaterno;  ?></td>
                <?php
            }
            ?>
            <td><button type="button"  class="btn btn-primary seleccionar" value="<?= $var->id ?>">Seleccionar</button></td>
        </tr>
       <?php 
            endforeach;
    }
?>
</tbody>
</table>
</div>
<div id='resultadoSeleccionado'>
</div>
</div>
</div>
  </body>
</html>
<script src="../../assets/js/jquery-2.1.1.js"></script>
<script>
    $("#resultadoSeleccionado").hide();
    $(".seleccionar").click(function(){
                var idcanal=$(this).val();
        $('#idInterm').val(idcanal)
            $.ajax({
                type : 'POST',
                url  : 'ajax/seleccionado2',
                data : {idcanal:idcanal},
                success : function(data)
                {
                    $("#resultadoSeleccionado").show(500);
                    $("#lista").hide('slow');
                    $("#resultadoSeleccionado").html(data);
                }
            });
        // console.log($('#changeOwner').val());
    })
</script>
  <?php
  }
 ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
</head>
<body>
<?php
if($_POST){
require_once("../../includes/init.php");
$idcanal= limpiar(strip_tags($_POST['idcanal']));
$datosCanal=Canales::obtenerDatosTITULARPorId($idcanal);
if ($datosCanal){
		?>
		<input type="hidden" name="canalid" value="<?= $datosCanal->idtitular ?>">
		<input type="hidden" name="agregarId" value="<?= $datosCanal->idtitular ?>" >
    <input type="hidden" name="changeOwner" id="changeOwner" value="yes">
<div class="row">
<div class="form-group col-md-4">
    <label for="ruc" >RUC</label>
    <input style="border: none;" readonly type="text" placeholder="Ruc *" id="ruc" name="ruc" class="border-bottom form-control"  value="<?= strtoupper($datosCanal->ruc) ?>" >
  </div>
<div class="form-group col-md-8">
    <label for="titular" >Titular de la cuenta y del recibo por honorarios *</label>
    <input style="border: none;" readonly type="text" placeholder="Titular *" id="titular" name="titular" class="border-bottom form-control" value="<?= strtoupper($datosCanal->titular) ?>"  >
  </div>
</div>
<div class="row">
<div class="form-group col-md-3">
    <label for="bancoCanal" class="">Banco</label>
    <select style="border: none;"  name="banco" class="cd-selec form-control border-bottom   siCobra" required id="bancoCanal">
       <option value="<?php echo $datosCanal->banco ?>" selected><?php echo $datosCanal->banco ?></option>
    </select>
</div>
        <div class='form-group col-md-3'> 
		<label for='monedaCanal' class=''>Moneda</label>
			<select style="border: none;"  name="moneda" class="cd-selec form-control border-bottom" required id="monedaCanal">
	      		<option value="<?php echo $datosCanal->moneda ?>" selected><?php echo $datosCanal->moneda ?></option>
    		</select>
		</div>
		<div class='form-group col-md-6'>
			<label for='numeroCuenta' class=''>Número de cuenta</label>
            <input style='border: none;' type='text' placeholder='Número de cuenta' id='numeroCuenta' name='numeroCuenta' class='border-bottom form-control text-capitalize' autocomplete='off'  readonly value="<?=  $datosCanal->numeroCuenta ?>">
        </div>  

</div>

<div class='row' >

<div class="form-group col-md-12" >
    <label for="titular" >CCI</label>
    <input style="border: none;" readonly type="text" placeholder="CCI *" id="cci2" name="cci" class="border-bottom form-control" value="<?= strtoupper($datosCanal->cci) ?>"  >
  </div>
  </div>
				<?php
}else{
    echo "FAIL";
  }
  ?>
<script src="../../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script>
            $('#otroBanco').hide();
            $('#cci2').prop('required',false);
            $('#bancoCanal').change(function(){
                if ($(this).val()=='OTRO BANCO') {
                    $('#otroBanco').show();
                    $('#numeroCuenta').hide('slow');
                    $('#numeroCuenta').prop('required',false);
                    $('#nombreBanco').prop('required',true);
                    $('#cci').prop('required',true);
                    $('#cci1').prop('disabled',true);
                    $('#cci1').hide('slow');
                    $('#cci2').prop('name','cciOLD');
                }else{
                    $('#numeroCuenta').show();
                    $('#numeroCuenta').prop('required',true);
                    $('#otroBanco').hide('slow');
                    $('#nombreBanco').prop('required',false);
                    $('#cci').prop('required',false);
                    $('#cci1').show();
                    $('#cci2').prop('name','cci');
                }
            });
</script>
  <?php
  }

 ?>


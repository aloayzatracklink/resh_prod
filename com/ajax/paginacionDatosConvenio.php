
<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $fecha='';
  $convenio=htmlspecialchars($_POST['convenio']);

  if (isset($_POST["fecha"])) {
    $fecha=strip_tags($_POST['fecha']);
  $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
  $desde=date("YmdHis",strtotime($desde));  
  $hasta=date("Ymd",strtotime($hasta)); 
  $hasta=$hasta."235959";
  }
  $comisionObj=new Comisiones();
  $comisionObj->idconvenio=limpiar($convenio);
  if (empty($fecha)){    
    $cantidadO=$comisionObj->obtenerComisionesConveniosCantidad();    
  }else{
    $cantidadO=$comisionObj->obtenerComisionesConveniosRANGOCantidad($desde,$hasta);
  }
  
  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  // echo $cantidadO->cantidad; 
  $n=1;
  if (empty($fecha)) {
    $datos=$comisionObj->obtenerComisionesConvenios($posisionDePagina,$registrosPorPagina);    
  } else {
    $datos=$comisionObj->obtenerComisionesConveniosRANGO($posisionDePagina,$registrosPorPagina,$desde,$hasta);        
  }  
             ?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>    
    <?php $usuario=Usuario::obtenerUsuario($var->idCreador)  ?>
    <?php $convenio=Canales::obtenerDatosDeConvenioPorId($var->idconvenio) ?>
    <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>
    <tr class="animated bounceIn">
         <td><?php echo $n++ ?></td>  
          <td><?= $convenio->nombreConvenio ?></td>       
          <td><?= (!empty($var->fechaFacturador))?formato($var->fechaFacturador):obtenerEstado($var->estadoFacturacion) ?></td>
         <td><?= ($clienteObj->tipoCliente=="Natural") ? $clienteObj->nombre." ".$clienteObj->apellidosC : $clienteObj->razonSocial ; ?></td>
          <td><?= (!empty($var->numeroFacturacion))?$var->numeroFacturacion:obtenerEstado($var->estadoFacturacion)  ?></td>
          <td><?= limpiarPALABRAGPSSMART($datosVehiculo->plan) ?></td>
          <td><?= $var->tiempo ?></td>
          <td><?= obtenerEstado($var->estadoPago) ?></td>
          <td><?= $usuario->usuarioNombre." ".$usuario->usuarioApellidoPaterno ?></td>
          <td><?= haceMinimo($var->fechaRegistro) ?></td>
         <!--  <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" data-id="<?php echo $var->datosfacturacionid ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;</td> -->
    </tr>
<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="7">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="78" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>
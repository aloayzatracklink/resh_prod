
<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $fecha='';

  if (isset($_POST["fecha"])) {
    $fecha=strip_tags($_POST['fecha']);
  $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
  $desde=date("YmdHis",strtotime($desde));  
  $hasta=date("Ymd",strtotime($hasta)); 
  $hasta=$hasta."235959";
  }

  $onixobj=new ReportesOnix();
  #$comisionObj->idconvenio=limpiar($convenio);
  if (empty($fecha)){    
    $cantidadO=$onixobj->obtenerReporteContratosOctoCantidad();    
  }else{
    $cantidadO=$onixobj->obtenerReporteContratosOctoRANGOCantidad($desde,$hasta);  
  }
  #print_r($cantidadO); die(); 

  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  // echo $cantidadO->cantidad; 
  $n=1;
  if (empty($fecha)) {
    #$datos=$onixobj->obtenerComisionesConvenios($posisionDePagina,$registrosPorPagina);
    $datos=$onixobj->obtenerReporteContratosOctoPaginacionRANGOInit($posisionDePagina,$registrosPorPagina);        
  } else {
    $datos=$onixobj->obtenerReporteContratosOctoPaginacionRANGO($posisionDePagina,$registrosPorPagina,$desde,$hasta);        
  }  

?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>    
    <tr class="animated bounceIn">
         <td><?php echo $n++ ?></td>  
        <td><?=$var->idcontrato;?></td>
        <td><?=$var->cliente;?></td>
        <td><?=$var->id_vehiculo;?></td>
        <td><?=$var->nro_contrato;?></td>
        <td><?=$var->firmado;?></td>
        <td><?=($var->estado_contrato == "P") ? "Pendiente" : "Activo";?></td>
        <td><?=$var->observacion;?></td>
        <td style="padding: 10px 0;"><?= utf8_encode($var->servicio);?></td>
        <td><?=$var->tipo_servicio;?></td>
        <td><?=$var->desde;?></td>
        <td><?=$var->hasta;?></td>
        <td><?=$var->id_cliente;?></td>
        <td><?=$var->ci_cliente;?></td>
        <td><?=$var->localidad_contrato;?></td>
        <td><?=$var->provincia;?></td>
        <td><?=$var->ciudad;?></td>
        <td><?=$var->sector;?></td>
        <td><?=utf8_encode($var->direccion_principal);?></td>
        <td><?=$var->telefono_principal;?></td>
        <td><?=$var->celular;?></td>
        <td><?=$var->oficina;?></td>
        <td><?=$var->direccion_oficina;?></td>
        <td><?=$var->telefono_oficina;?></td>
        <td><?=$var->extension;?></td>
        <td><?=$var->mail1;?></td>
        <td><?=$var->mail2;?></td>
        <td><?=$var->datos_vehiculo;?></td>
        <td><?=$var->ci_usuario;?></td>
        <td><?=$var->nombre_usuario;?></td>
        <td><?=utf8_encode($var->direccion_usuario);?></td>
        <td><?=$var->telefono_usuario;?></td>
        <td><?=$var->celular_usuario;?></td>
        <td><?=$var->oficina_usuario;?></td>
        <td><?=$var->direccion_oficina_usuario;?></td>
        <td><?=$var->telefono_oficina_usuario;?></td>
        <td><?=$var->telefono_adicional_usuario;?></td>
        <td><?=$var->mail_usuario;?></td>
        <td><?=$var->ci_familiar;?></td>
        <td><?=$var->nombre_familiar;?></td>
        <td><?=$var->telefono_familiar;?></td>
        <td><?=$var->consesionario;?></td>
        <td><?=$var->broker;?></td>
        <td><?=$var->vend_interno;?></td>
        <td><?=$var->vend_externo;?></td>
         <!--  <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" data-id="<?php echo $var->datosfacturacionid ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;</td> -->
    </tr>
<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="7">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="78" class="text-left">No se encontró registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>
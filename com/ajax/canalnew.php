<html lang="en">
<head>
	<meta charset="UTF-8">
</head>
<body>
<?php 
if($_POST) 
  {
require_once("../../includes/init.php");	
$datos= limpiar(strip_tags($_POST['canalId'])); 
$objCanal=new Canales; 
$obMenu=Usuario::obtenerMenu($session->idusuario);
if ($obMenu->comisiones=="si"){
    $datoss=$objCanal->otenerDatosCanalPORCAMPOSTODOST($datos);
}else{
    $datoss=$objCanal->otenerDatosCanalPORCAMPOST($datos);
}
?>
<div class='row'>
<div class='col-md-12'>
<div id='lista' class='table-responsive'>
<table class='table table-hover'>
    <thead>
        <tr>
            <th>RUC</th>
            <th>Títular</th>
            <th>Banco</th>
            <th>Moneda</th>
<?php
    if ($obMenu->comisiones=="si"){
        ?>
            <th>Creado Por</th>
        <?php
    }
?>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php
    if ($datoss) {
        foreach($datoss as $var):
       ?>
        <tr>
            <td><?= $var->ruc ?></td>
            <td><?= $var->titular ?></td>
            <td><?= $var->banco ?></td>
            <td><?= $var->moneda ?></td>
        <?php 
        if ($obMenu->comisiones=="si"){
        $ou=Usuario::obtenerUsuario($var->idCreador)
        ?>
            <td><?php  echo $ou->usuarioNombre." ".$ou->usuarioApellidoPaterno;  ?></td>
        <?php
            }
        ?>
            <td><button type="button"  class="btn btn-primary seleccionar" value="<?= $var->idtitular ?>">Seleccionar</button></td>
        </tr>
       <?php 
            endforeach;
    }
?>
</tbody>
<tfoot>
<tr>
<td colspan='7'></td>
</tr>
        <tr>
            <td colspan='3'>Nuevo títular para el canal</td>
            <td><button type="button"  class="btn btn-info agregarNewCanal" >Agregar nuevo títular</button></td>
            <td colspan='2'></td>
        </tr>
</tfoot>
</table>
</div>
<div id='resultadoSeleccionado'>
</div>
</div>
</div>
  </body>
</html>
<script src="../../assets/js/jquery-2.1.1.js"></script>
<script>
$('.agregarNewCanal').click(function(){
    $('#registrarNuevo').show();
    var datos=`
 <div class="row">
<div class="form-group col-md-12">
<input type="hidden" name="canalid" value="0" >
<input type="hidden" name="canalid" value="0" >
<input type="hidden" name="agregarId" value="0" >
<div class="row">
<div class="form-group col-md-4">
    <label for="ruc" >RUC</label>
    <input style="border: none;" required type="text" placeholder="Ruc *" id="ruc" name="ruc" class="border-bottom form-control"   >
  </div>
  <div class="form-group col-md-8">
    <label for="titular" >Titular de la cuenta y del recibo por honorarios *</label>
    <input style="border: none;" required type="text" placeholder="Titular *" id="titular" name="titular" class="border-bottom form-control"   >
  </div>
</div>
<div class="row">
<div class="form-group col-md-3">                        
    <label for="bancoCanal" class="">Banco</label>                        
        <select style="border: none;"  name="bancoCanal" class="cd-selec form-control border-bottom   siCobra" required id="bancoCanal">
       <option value="" disabled selected>Seleccione Banco *</option>
      <option value="BANCO CONTINENTAL BBVA">BANCO CONTINENTAL BBVA</option>
      <option value="BANCO DE CREDITO">BANCO DE CREDITO</option>
      <option value="BANCO FINANCIERO">BANCO FINANCIERO</option>      
      <option value="OTRO BANCO">OTRO BANCO</option>      
    </select>
</div>
    <div class="form-group col-md-3">                       
               <label for="monedaCanal" class="">Moneda</label>                              
                       <select style="border: none;"  name="monedaCanal" class="cd-selec form-control border-bottom" required id="monedaCanal">
                       <option value="" disabled selected>Seleccione Moneda *</option>
                       <option value="SOLES">SOLES</option>
                       <option value="DOLARES">DOLARES</option>
               </select>
               </div>
               <div class="form-group col-md-6">                        
                       <label for="numeroCuenta" class="">Número de cuenta</label>       
                       <input style="border: none;" type="text" placeholder="Número de cuenta" id="numeroCuenta" name="numeroCuenta" class="border-bottom form-control text-capitalize"  required autocomplete="off">                     
                </div>  
        </div>
        <div class='row' id='otroBanco'>
                <div class="form-group col-md-6">
                    <label for="nombreBanco" class="">Nombre del banco</label>                       
                    <input style="border: none;" type="text" placeholder="Nombre del banco" id="nombreBanco" name="nombreBanco" class="cci border-bottom form-control text-capitalize"  autocomplete="off">
                </div>
                <div class="form-group col-md-6">
                    <label for="cci" class="">CODIGO INTERBANCARIO</label>                       
                    <input style="border: none;" type="text" placeholder="CCI" id="cci" name="cci" class="cci border-bottom form-control text-capitalize"  autocomplete="off">
                </div>
        </div>
        </div>
        </div>
`;
            $('.data').html(datos);
            $('#agregar').hide('slow');
            $('#otroBanco').hide();
            $('#bancoCanal').change(function(){
                if ($(this).val()=='OTRO BANCO') {
                    $('#otroBanco').show();
                    $('#numeroCuenta').hide('slow');
                    $('#numeroCuenta').prop('required',false);
                    $('#nombreBanco').prop('required',true);
                    $('#cci').prop('required',true);
                }else{
                    $('#numeroCuenta').show();
                    $('#numeroCuenta').prop('required',true);
                    $('#otroBanco').hide('slow');
                    $('#nombreBanco').prop('required',false);
                    $('#cci').prop('required',false);
                }
            });
        });
    $("#resultadoSeleccionado").hide();
    $(".seleccionar").click(function(){
                var idcanal=$(this).val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/seleccionado',
                data : {idcanal:idcanal},
                success : function(data)
                {
                    $("#resultadoSeleccionado").show(500);
                    $("#lista").hide('slow');
                    $("#resultadoSeleccionado").html(data);
                }
            });
    })
</script>

  <?php
  }

 ?>


<?php require_once("../../includes/init.php");

$reporte = new ReclamoClienteReporteClass();

if (isset($_GET["fecha"])) {
    $fecha = strip_tags($_GET['fecha']);
    $desde = trim(current(explode('/', $fecha)));
    $tmp = explode('/', $fecha);
    $hasta = end($tmp);
    $desde = date("YmdHis", strtotime($desde));
    $hasta = date("Ymd", strtotime($hasta));
    $hasta = $hasta . "235959";
}

if (empty($_GET["estado"])){
    $datos = $reporte->TablaListaReclamosParaFecha($desde,$hasta);
}
else{
    $estado = $_GET["estado"];
    $datos = $reporte->TablaListaReclamosParaFechayEstado($desde,$hasta,$estado);
}

//var_dump($datos);

echo json_encode($datos, JSON_UNESCAPED_UNICODE); //Array transformada en Json


?>
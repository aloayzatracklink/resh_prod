<?php if ($_POST): ?>
	<?php require_once("../../includes/init.php"); ?>
	<?php $idprovincia=$_POST['idprovincia'] ?>
	<option class="text-capitalize" value="" disabled selected >Seleccione Distrito</option>
	<?php $objDistr=Cliente::obtenerDistritos($idprovincia) ?>
	<?php if ($objDistr): ?>
		<?php foreach ($objDistr as $var): ?>
			<option  value="<?php echo $var->iddistrito ?>" ><?php echo utf8_encode($var->distrito) ?></option>
		<?php endforeach ?>
	<?php else: ?>
	<?php endif ?>
<?php endif ?>
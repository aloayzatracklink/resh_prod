<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
</head>
<body>
<?php 
if($_POST) 
  {
require_once("../../includes/init.php");	
$canalId= soloNumero(strip_tags($_POST['canalId'])); 
$objCanal=new Canales; 
$datosCanal=$objCanal->otenerDatosCanal(soloNumero($canalId));
if ($datosCanal) {	
		?>			
			<div class='form-group col-md-4'> <label for='dni' class=''>Canal nombre</label>
				<input style='border: none;' type='text' placeholder='Canal nombre' id='nombreCanal' name='nombreCanal' class='dni border-bottom form-control text-capitalize' required autocomplete='off' value="<?php echo $retVal = ($datosCanal) ? $datosCanal->nombreCanal : '' ; ?>"> 
			</div>
			
			<div class='form-group col-md-4'> <label for='ejecutivoCanal' class=''>Ejecutivo de canal</label>                        <input style='border: none;' type='text' placeholder='Ejecutivo de canal' id='ejecutivoCanal' name='ejecutivoCanal' class='ejecutivoCanal border-bottom form-control text-capitalize' required autocomplete='off' value="<?php echo $retVal = ($datosCanal) ? $datosCanal->ejecutivoCanal : '' ; ?>" > </div>			
		       
			<div class='form-group col-md-4'>                    
			    <label for='montoComision' class=''>Monto de comisión</label>                       
			 <input style='border: none;' type='text' placeholder='Monto de comisión' id='montoComision' name='montoComision' class='border-bottom form-control text-capitalize'   required>                    
			</div>
<div class='form-group col-md-2'>                        
			<label for='bancoCanal' class=''>Banco</label>                        
			<input style='border: none;' type='text' placeholder='Banco' id='bancoCanal' name='bancoCanal' class='bancoCanal border-bottom form-control text-capitalize' required autocomplete="off" value="<?php echo $retVal = ($datosCanal) ? utf8_encode($datosCanal->banco) : '' ; ?>">       
		</div>               
		<div class='form-group col-md-2'> 
		<label for='monedaCanal' class=''>Moneda</label>    			   
			<select style="border: none;"  name="monedaCanal" class="cd-selec form-control border-bottom" required id="monedaCanal">
			<?php if ($datosCanal): ?>
	      		<option value="<?php echo $datosCanal->moneda ?>" selected><?php echo $datosCanal->moneda ?></option>
			<?php else: ?>
	      		<option value="" disabled selected>Seleccione Moneda *</option>				
			<?php endif ?>	
	      		<option value="SOLES">Soles</option>
	      		<option value="DOLARES">Dolares</option>
    		</select>               
		</div>                   
		<div class='form-group col-md-4'>                        
			<label for='numeroCuenta' class=''>Número de cuenta</label>       
			<input style='border: none;' type='text' placeholder='Número de cuenta' id='numeroCuenta' name='numeroCuenta' class='border-bottom form-control text-capitalize'  required value="<?php echo $retVal = ($datosCanal) ? $datosCanal->numeroCuenta : '' ; ?>">                      </div>  

			<div class='form-group col-md-4'> <label for='cci' class=''>CCI</label>       
			<input style='border: none;' type='text' placeholder='CCI' id='cci' name='cci' class='cci border-bottom form-control text-capitalize'  autocomplete='off' value="<?php echo $retVal = ($datosCanal) ? $datosCanal->cci : '' ; ?>"> </div></div>
				<?php				
}else{
?>

			<div class='form-group col-md-4'> <label for='nombreCanal' class=''>Canal nombre</label>         
				<input style='border: none;' type='text' placeholder='Canal nombre' id='nombreCanal' name='nombreCanal' class='nombreCanal border-bottom form-control text-capitalize' required autocomplete='off' value="PENDIENTE"> 
			</div>
			<div class='form-group col-md-4'> <label for='ejecutivoCanal' class=''>Ejecutivo de Canal</label>                        <input style='border: none;' type='text' placeholder='Ejecutivo de Canal' id='ejecutivoCanal' name='ejecutivoCanal' class='ejecutivoCanal border-bottom form-control text-capitalize' required autocomplete='off' value="PENDIENTE"> </div>

       
			<div class='form-group col-md-4'>                    
			    <label for='montoComision' class=''>Monto de comisión</label>                       
			 <input style='border: none;' type='number' placeholder='Monto de comisión' id='montoComision' name='montoComision' class='border-bottom form-control text-capitalize'   required autocomplete='off'>                    
			</div>

<div class='form-group col-md-2'>                        
			<label for='bancoCanal' class=''>Banco</label>                        
			<input style='border: none;' type='text' placeholder='Banco' id='bancoCanal' name='bancoCanal' class='bancoCanal border-bottom form-control text-capitalize' required  autocomplete='off'  >       
		</div> 
		<div class='form-group col-md-2'>  			
		<label for='monedaCanal' class=''>Moneda</label>    			      
			<select style="border: none;"  name="monedaCanal" class="cd-selec form-control border-bottom" required id="monedaCanal">
	      		<option value="" disabled selected>Seleccione Moneda *</option>
	      		<option value="SOLES">Soles</option>
	      		<option value="DOLARES">Dolares</option>
    		</select>               
		</div>                  
		<div class='form-group col-md-4'>                        
			<label for='numeroCuenta' class=''>Número de cuenta</label>       
			<input style='border: none;' type='text' placeholder='Número de cuenta' id='numeroCuenta' name='numeroCuenta' class='border-bottom form-control text-capitalize'  required value="PENDIENTE" autocomplete='off'>                      </div>  
			<div class='form-group col-md-4'> <label for='cci' class=''>CCI</label>                        <input style='border: none;' type='text' placeholder='CCI' id='cci' name='cci' class='cci border-bottom form-control text-capitalize'  autocomplete='off'> </div>
			<script>limpiar();</script>
<?php
  }
  ?>

  </body>
</html>
<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script>
	 $('.bancoCanal').typeahead({
        source: [        
        {"name": "BANCO FINANCIERO"},        
        {"name": "BANCO DE CREDITO"},
        {"name": "BANCO CONTINENTAL BBVA"},
        {"name": "BANCO BANBIF"},
      
      ]
    });
    var banco = document.getElementById('bancoCanal');
		bancoCanal=banco.value.toUpperCase();
		// alert(bancoCanal);
		if (bancoCanal=="BANCO FINANCIERO" || bancoCanal=="BANCO DE CREDITO"|| bancoCanal=="BANCO CONTINENTAL BBVA"|| bancoCanal=="BANCO BANBIF" ){
			document.getElementById("cci").removeAttribute("required");
		}else{
			var cci = document.getElementById("cci"); 
			var att = document.createAttribute("required");     
			att.cci = "true";
			cci.setAttributeNode(att);
		} 
  $(".bancoCanal").bind("keyup focus",function()
    {
    	var banco = document.getElementById('bancoCanal');
		bancoCanal=banco.value.toUpperCase();
		// alert(bancoCanal);
		if (bancoCanal=="BANCO FINANCIERO" || bancoCanal=="BANCO DE CREDITO"|| bancoCanal=="BANCO CONTINENTAL BBVA"|| bancoCanal=="BANCO BANBIF" ){
			document.getElementById("cci").removeAttribute("required");
		}else{
			var cci = document.getElementById("cci"); 
			var att = document.createAttribute("required");     
			att.cci = "true";
			cci.setAttributeNode(att);
		}
    }); 
</script>

  <?php
  }

 ?>


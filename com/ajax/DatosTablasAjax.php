<?php require_once("../../includes/init.php") ?>  
<?php
$tabla = $_POST['tabla'];
$response = "";

// if($tabla=="vehiculos"){
//     $idcliente = $_POST['idcliente'];
//     $datosTablas = new DatosTablasClass();
//     $response = $datosTablas->datosVehiculos($idcliente);
//     echo json_encode($response);
// }

if($tabla=="vehiculos"){
    $cliente_id = $_POST['cliente_id'];
    $onix = new Onix();
    $response = $onix->chasisOnixPorIdCustomer($cliente_id);
    echo json_encode($response);
}

if($tabla=="reclamoPlazo"){
    $id = $_POST['id'];
    $datosTablas = new DatosTablasClass();
    $response = $datosTablas->datosReclamoPlazo($id);
    echo json_encode($response);
}

if($tabla=="area_usuarios"){
    $idarea = $_POST['idarea'];
    $idusuario = $_POST['idusuario'];
    $datosTablas = new DatosTablasClass();
    $response = $datosTablas->datosUsuariosArea($idarea, $idusuario);
    echo json_encode($response);
}

if($tabla=="getTokenSecurity"){
    $token  = Token::generar();
    echo $token;
}?>
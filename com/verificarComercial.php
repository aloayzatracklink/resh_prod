<?php $nav="verificarComercial" ;?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
	irA("../ingresar");
} 
?> 
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->verificarComercial=="si"): ?>
<body>
	<div id="wrapper">
		<nav class="navbar-default navbar-static-side" role="navigation">
			<div class="sidebar-collapse">
				<?php include("../includes/menuIzquierdo.php") ?>
			</div>
		</nav>
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div class="row border-bottom">
				<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
					<?php include("../includes/navegacionArriba.php") ?>

				</nav>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="wrapper wrapper-content">


						<div class="row">
							<div class="col-lg-12">
								<div class="ibox float-e-margins">
									<div class="ibox-title">
										<h5>Ventas del mes - <?php  $mes=date("m"); echo $retVal = ($mes==01) ? "Enero" : "Febrero" ;  ?> del <?php echo $anio=date("Y") ?></h5>
									</div>
									<div class="ibox-content">
										<div>
											<div id="slineChart" ></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">

							<div class="col-lg-3">
								<div class="ibox float-e-margins">
									<div class="ibox-title">
										<h5>Ventas total diaria de hoy <?php $fecha=date("l") ?>
										<?php if ($fecha=="Monday"): ?>
											Lunes
										<?php endif ?>
										<?php if ($fecha=="Tuesday"): ?>
											Martes
										<?php endif ?>										
										<?php if ($fecha=="Wednesday"): ?>
											Miercoles
										<?php endif ?>
										<?php if ($fecha=="Thursday"): ?>
											Jueves
										<?php endif ?>
										<?php if ($fecha=="Friday"): ?>
											Viernes
										<?php endif ?>
										</h5>
									</div>
									<div class="ibox-content">
										<div>
											<div id="pie"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-9">
								<div class="ibox float-e-margins">
									<div class="ibox-title">
										<h5>Vista Anual del <?php echo date("Y") ?> </h5>

									</div>
									<div class="ibox-content">
										<div>
											<div id="stocked"></div>
										</div>
									</div>
								</div>
							</div>
						</div>


					</div>




				</div>



			</div>

			<?php include("../includes/footer.php") ?>
		</div>
	</div>

</div>


<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- FooTable -->
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/d3/d3.min.js"></script>
<script src="../assets/js/plugins/c3/c3.min.js"></script>
 <?php include_once('../includes/script.php') ?>
<?php 


$estadiditaObj=new Estadistica();
?>

<script>
	$(document).ready(function(){
		c3.generate({
			bindto: '#slineChart',
			data:{
				columns: [				  
					<?php $estadiditaObj->obtenerVentasMensuales() ?>
				],
				
				type: 'spline'
			}
		});
		c3.generate({
			bindto: '#pie',
			data:{
				columns: [
				<?php $estadiditaObj->obtenerVentasDiarias() ?>	
				],
				// colors:{
				// 	Ejecutivo1: '#1ab394',
				// 	Ejecutivo2: '#BABABA',
				// 	Ejecutivo3: '#adebBA'
				// },
				type : 'pie'
			}
		});
		c3.generate({
			bindto: '#stocked',
			data:{
				x: 'x',
				columns: [
				['x', 'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Junio','Septiembre','Octubre','Noviembre','Diciembre'],
				['Vendidos', <?php  $ob=$estadiditaObj->obtenerVistaAnual('01');echo $ob->ventas; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnual('02');echo $ob->ventas; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnual('03');echo $ob->ventas; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnual('04');echo $ob->ventas; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnual('05');echo $ob->ventas; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnual('06');echo $ob->ventas; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnual('07');echo $ob->ventas; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnual('08');echo $ob->ventas; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnual('09');echo $ob->ventas; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnual('10');echo $ob->ventas; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnual('11');echo $ob->ventas; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnual('12');echo $ob->ventas; ?>],
				['Cancelados', 0,0,0,0,0,0,0,0,0,0,0,0,0]
				],
				colors:{
					Vendidos: '#71c173',
					Cancelados: '#e40e0e'
				},
				type: 'bar',
				groups: [
				['Vendidos', 'Cancelados']
				]
			},
			 axis: {
        x: {
            type: 'category',
            tick: {
                rotate: 75,
                multiline: false
            },
            height: 130
        }
    }
		});

	});

</script>
<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>
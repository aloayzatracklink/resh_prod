$('.footable').footable();
$('.footable2').footable();

$(".servicioAdicional").select2({
  placeholder: "Servicios adcionales",
  allowClear: true
});

jQuery.datetimepicker.setLocale('es');


var maxField = 300; //Input fields increment limitation
var addButton = $('.add_button'); //Add button selector
var wrapper = $('.field_wrapper'); //Input field wrapper
var fieldHTML = "<div class='field_wrapper'><div class='hr-line-dashed'></div><div class='row'><div class='form-group col-md-3'><label for='dniC' class='sr-only'>DNI No Obligatorio</label><input style='border: none;' type='text' placeholder='DNI no obligatorio' id='dniC' name='field_name[0][]' class='form-control border-bottom '   data-mask='99999999' id='dniI'></div><div class='form-group col-md-3'><label for='nombreC' class='sr-only'>Nombres *</label><input style='border: none;' type='text' placeholder='Nombres *' id='nombreC' name='field_name[1][]' class='form-control border-bottom text-capitalize'  required id='name'></div><div class='form-group col-md-3'><label for='apellidos' class='sr-only'>Apellidos</label><input style='border: none;' type='text' placeholder='Apellidos' id='apellidos' name='field_name[2][]' class='form-control x2 text-capitalize border-bottom text-capitalize' required ></div><div class='form-group col-md-3'><label for='dniC' class='sr-only'>Celular *</label><input style='border: none;' type='text' placeholder='Celular *' id='celular' name='field_name[3][]' class='form-control border-bottom ' required  data-mask='999 999 999' id='celular'></div></div><div class='row'><div class='form-group col-md-4'><label for='telefono' class='sr-only'>Telefono</label><input style='border: none;' type='text' placeholder='telefono' id='telefono' name='field_name[4][]' class='form-control border-bottom text-capitalize' id='telefono'></div><div class='form-group col-md-4'><label for='correo' class='sr-only'>Correo</label><input style='border: none;' type='text' placeholder='Correo' id='correo' name='field_name[5][]' class='form-control x2 text-capitalize border-bottom text-capitalize'></div><div class='form-group col-md-4'><label for='Parentesco' class='sr-only'>Parentesco *</label><select  style='border: none;' class='tecnico border-bottom form-control' required name='field_name[6][]'><option class='' value='' disabled selected >Seleccione Parentesco *</option><option value='Usuario'>Usuario(a)</option><option value='padre'>Padre</option><option value='Madre'>Madre</option><option value='Hermano'>Hermano(a)</option><option value='Conyuge'>Conyuge</option><option value='Hijo'>Hijo(a)</option><option value='Nieto'>Nieto(a)</option><option value='Cuñado'>Cuñado(a)</option><option value='Suegro'>Suegro(a)</option></select></div></div><a href='javascript:void(0);' class='remove_button  col-md-offset-10 btn btn-warning' title='quitar contacto'><strong>Quitar </strong><i class='fa fa-minus-circle'></i></a></div>";    
var x = 1; //Initial field counter is 1
$(addButton).click(function(){ //Once add button is clicked
    if(x < maxField){ //Check maximum number of input fields
        x++; //Increment field counter
        $(wrapper).append(fieldHTML); // Add field html
      }
    });
$(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
  e.preventDefault();
    $(this).parent('div').remove(); //Remove field html
    x--; //Decrement field counter
  });

  $('.concesionario').typeahead({
    source: [
    {"name": "No tengo"},
    {"name": "Alese perú"},
    {"name": "Gildemeister"},
    {"name": "Mitsui"},
    {"name": "Maquinaria Nacional S.A"},
    {"name": "Rosales Motors SA"},
    {"name": "Almacenes Santa Clara S.A."},
    {"name": "Auto Scorpio Motors S.A."},
    {"name": "EAFC MAQUISISTEMA"},
    {"name": "Braillard S.A."},
    {"name": "Braillard S.A. Arequipa"},
    {"name": "Fre Wagenmotors S.C.R.Ltda."},
    {"name": "Multimarkas"},
    {"name": "Scorpio Motors S.A."},
    {"name": "Alberto Torres S.A.C."},
    {"name": "Alejandrina S.A."},
    {"name": "Fys Rojas Motors S.A.C."},
    {"name": "Wong Hnos Automoviles S.A"},
    {"name": "Carrion Automotriz S.A."},
    {"name": "Eurosport"},
    {"name": "Resersur S.A.C."},
    {"name": "Ssangyong"},
    {"name": "Carrion Automotriz S.A."},
    {"name": "Pandero 4x4"},
    {"name": "Indumotora Del Perú"},
    {"name": "Autos Del Sur S.A.C."},
    {"name": "Ciro Arribasplata S.A."},
    {"name": "Amsa"},
    {"name": "Euroshop S.A"},
    {"name": "Flechelle"},
    {"name": "Hauswagen"},
    {"name": "Moliwagen"},
    {"name": "Peruwagen"},
    {"name": "Derco"},
    {"name": "Pandero VW"}
    ]
  });

  $('.tiempo').typeahead({
    source: [
    {"name": "1 año"},
    {"name": "2 años"},
    {"name": "3 años"},
    {"name": "4 años"},
    {"name": "5 años"},
    {"name": "6 años"},
    {"name": "8 años"},
    {"name": "7 años"}
    ]
  }); 


  $('.seguro').typeahead({
    source: [
    {"name": "Interseguro"},
    {"name": "No tiene seguro"},
    {"name": "Rimac Seguros"},
    {"name": "Pacifico Seguros"},
    {"name": "La Positiva"},
    {"name": "Mapfre Peru"},
    {"name": "HDI Seguros"}
    ]
  });
  $('.marca').typeahead({
    source: [
    {"name": "Abarth"},
    {"name": "Alfa Romeo"},
    {"name": "Aro"},
    {"name": "Asia"},
    {"name": "Asia Motors"},
    {"name": "Aston Martin"},
    {"name": "Audi"},
    {"name": "Austin"},
    {"name": "Auverland"},
    {"name": "Bentley"},
    {"name": "Bertone"},
    {"name": "Bmw"},
    {"name": "Cadillac"},
    {"name": "Chevrolet"},
    {"name": "Chrysler"},
    {"name": "Citroen"},
    {"name": "Corvette"},
    {"name": "Dacia"},
    {"name": "Daewoo"},
    {"name": "Daf"},
    {"name": "Daihatsu"},
    {"name": "Daimler"},
    {"name": "Dodge"},
    {"name": "Ferrari"},
    {"name": "Fiat"},
    {"name": "Ford"},
    {"name": "Galloper"},
    {"name": "Gmc"},
    {"name": "Honda"},
    {"name": "Hummer"},
    {"name": "Hyundai"},
    {"name": "Infiniti"},
    {"name": "Isuzu"},
    {"name": "Iveco"},
    {"name": "Iveco pegaso"},
    {"name": "Jaguar"},
    {"name": "Jeep"},
    {"name": "Kia"},
    {"name": "Lada"},
    {"name": "Lamborghini"},
    {"name": "Lancia"},
    {"name": "Land-rover"},
    {"name": "Ldv"},
    {"name": "Lexus"},
    {"name": "Lotus"},
    {"name": "Mahindra"},
    {"name": "Maserati"},
    {"name": "Maybach"},
    {"name": "Mazda"},
    {"name": "Mercedes-benz"},
    {"name": "Mg"},
    {"name": "Mini"},
    {"name": "Mitsubishi"},
    {"name": "Morgan"},
    {"name": "Nissan"},
    {"name": "Opel"},
    {"name": "Peugeot"},
    {"name": "Pontiac"},
    {"name": "Porsche"},
    {"name": "Renault"},
    {"name": "Rolls-royce"},
    {"name": "Rover"},
    {"name": "Saab"},
    {"name": "Santana"},
    {"name": "Seat"},
    {"name": "Skoda"},
    {"name": "Ssangyong"},
    {"name": "Subaru"},
    {"name": "Suzuki"},
    {"name": "Talbot"},
    {"name": "Tata"},
    {"name": "Talbot"},
    {"name": "Toyota"},
    {"name": "Umm"},
    {"name": "Vaz"},
    {"name": "Volkswagen"},
    {"name": "Volvo"},
    {"name": "Wartburg"},                    
    {"name": "Austin"}
    ]
  });

$("#registrarVehiculoF").validate({
  rules:{
      plan:{
        required:true
      },
      rucFacturacion:{
      required:true,
      minlength:11,
      maxlength:11, 

    }
  },
  messages:{
    plan:{
      required:"Es necesario seleccionar el plan"
    },
      rucFacturacion: {
    minlength: jQuery.validator.format("Se nececita al menos {0} caracteres!"),           
    maxlength: jQuery.validator.format("Se nececita al menos {0} caracteres!")            

    }

  }

});




function validarPago(){
var remember = document.getElementById('pagoencaja');
if (remember.checked){
  document.getElementById("voucherImg").removeAttribute("required");
}else{    
  var voucher = document.getElementById("voucherImg"); 
  var att = document.createAttribute("required");     
  att.value = "true";
  voucher.setAttributeNode(att);
}

var banco = document.getElementById('bancoCanal');
  bancoCanal=banco.value.toUpperCase();
  // alert(bancoCanal);
if (bancoCanal=="BANCO FINANCIERO" || bancoCanal=="BANCO DE CREDITO"|| bancoCanal=="BANCO CONTINENTAL BBVA"|| bancoCanal=="BANCO BANBIF" ){
  document.getElementById("cci").removeAttribute("required");
}else{
  var cci = document.getElementById("cci"); 
  var att = document.createAttribute("required");     
  att.cci = "true";
  cci.setAttributeNode(att);
}


}

function validarComision(){
var comision = document.getElementById('comision');
  if (comision.checked){    
    $("#comisionHtml").html("");
  // $('#canalId').prop('disabled', true); 
  // alert("xd");

  var cci = document.getElementById("nombreCanal"); 
  var ejecutivoCanalN = document.getElementById("ejecutivoCanalN"); 
  var att = document.createAttribute("disabled");     
  var att1= document.createAttribute("disabled");     
  att.cci = "true";
  att1.ejecutivoCanalN = "true";
  cci.setAttributeNode(att);
  ejecutivoCanalN.setAttributeNode(att1);
      
// document.getElementById('canalId').contentEditable = true;
//     document.getElementById('canalId').focus();

  
// $("#canalId").slideDown(function(){
//     $('input', this).focus();
// });

  document.getElementById("canalId").removeAttribute("disabled");
  $('#canalId').focus();
  // var cc3 = document.getElementById("canalId"); 
  // var att3 = document.createAttribute("autofocus");     
  // att3.cc3 = "true";
  // cc3.setAttributeNode(att3);

  }else{  
  document.getElementById("nombreCanal").removeAttribute("disabled");
  document.getElementById("ejecutivoCanalN").removeAttribute("disabled");
  $('#nombreCanal').focus();
  $("#comisionHtml").html("");
  var cc2 = document.getElementById("canalId"); 
  var att2 = document.createAttribute("disabled");     
  att2.cc2 = "true";
  cc2.setAttributeNode(att2);
  
  
  }
}

validarComision();

$('#selectNa').on("change",function () {
  
var tipoDePagoSeleccionado=document.getElementById('selectNa');
value = tipoDePagoSeleccionado.options[tipoDePagoSeleccionado.selectedIndex].value;
if (value=="Boleta") {
  document.getElementById('rucFacturacion').removeAttribute("required");
  document.getElementById('razonSocialFacturacion').removeAttribute("required");
  document.getElementById('direccionFiscalFacturacion').removeAttribute("required");
  document.getElementById('direccionEntrega').removeAttribute("required");
}
if(value=="Factura"){
// alert(value);
  var rucFacturacion=document.getElementById('rucFacturacion');
  var razonSocialFacturacion=document.getElementById('razonSocialFacturacion');
  var direccionFiscalFacturacion=document.getElementById('direccionFiscalFacturacion');
  var direccionEntrega=document.getElementById('direccionEntrega');
  var att = document.createAttribute("required");
  var att1 = document.createAttribute("required");
  var att2 = document.createAttribute("required");
  var att3 = document.createAttribute("required");
  att.value = "true";  
  rucFacturacion.setAttributeNode(att);
  razonSocialFacturacion.setAttributeNode(att1);
  direccionFiscalFacturacion.setAttributeNode(att2);
  direccionEntrega.setAttributeNode(att3);
}

  });

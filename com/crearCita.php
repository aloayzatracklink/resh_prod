<?php $nav="crearCita" ;?>
<?php include("../includes/head.php") ?>
<link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->programacion=='si'): ?>
<?php
   if ($obMenu->administrarComercial=="si") {
       
   }else{
       //header("Location:http://resh.tracklink.pe/programacionDisponibilidad.php");
   } 
  require_once('../includes/tablaProgramacion.php');
  $errores=array();
  $echo="";
  $citaObj=new Cita();
  $objCanales= new Canales(); 
  $objtablaP= new tablaProgramacion();
    $objServAdicionales=new OperacionServAdicionales();
//$objtablaP->fecha='2018-03-20';
//$objtablaP->hora='09:00';
//echo $objtablaP->obtenerTrabajosParaLafechayHoraDisponibleTaller();
if (isset($_POST['crearCitaTaller'])) {
  if (Token::verificar($_POST['crearCitaTaller'])) { 
    $citaObj->nombreCliente=ucwords(strtolower($_POST['nombreCompleto']));
    $citaObj->plan=$_POST['plan'];
    $citaObj->celular=$_POST['celular'];
    $citaObj->costo=$_POST['costo'];
    $citaObj->seguro=$_POST['seguro'];
    $citaObj->placa=$_POST['placa'];    
    $citaObj->fecha=$_POST['fecha']." ".$_POST['hora'];    
    $citaObj->observacion=$_POST['observaciones'];     
    $citaObj->tiempo=$_POST['tiempo'];    
    $citaObj->tipoCanal=$_POST['tipoCanal'];    
    $citaObj->nombreCanal=$_POST['nombreCanal'];    
    if (isset($_POST['tienda'])) {
        if (!empty($_POST['tienda'])) {
            $citaObj->oficina=$_POST['tienda'];    
        }
    }
    if (isset($_POST['tienda1'])) {
        if (!empty($_POST['tienda1'])) {
            $citaObj->oficina=$_POST['tienda1'];    
        }
    }
    if (!empty($_POST['convenio'])) {
        $citaObj->nombreCanal=$_POST['convenio'];    
    }else{
        $citaObj->nombreCanal=$_POST['nombreCanal'];    
    }
    if (isset($_POST['ejecutivoCanalN'])) {
        $citaObj->ejecutivoCanal=$_POST['ejecutivoCanalN'];    
    }
    if (!empty($_FILES['voucher']['name'])){
      $extencionesAbilitadas= array('jpg','jpeg','png','gif','pdf','docx');
      $nombreArchivo=$_FILES['voucher']['name'];
      $extencionArchivo=explode('.', $nombreArchivo);
      $extencionArchivo=strtolower(end($extencionArchivo));
      $tamanioArchivo=$_FILES['voucher']['size'];
      $ubicacionTemporal=$_FILES['voucher']['tmp_name'];
      if (!in_array($extencionArchivo,$extencionesAbilitadas)) {
      $errores[]='La extencion de la imagen deve ser jpg jpeg png gif pdf';
      }
      if ($tamanioArchivo>5242880) {
      $errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
      }
      if ($nombreArchivo=='') {
        $errores[]="Ingrese nombre de archivo o el archivo correctamente";
      }
      $fecha=date('YmdHis');
      move_uploaded_file($_FILES['voucher']['tmp_name'], '../public/vouchers/'.$fecha.quitarEspacio($_FILES['voucher']['name'])); 
      $citaObj->voucher=$fecha.quitarEspacio($nombreArchivo);
      $citaObj->estadoPago='Pagado';

    }else{
      $citaObj->estadoPago='Pendiente';      
    }

    if (isset($_POST['campana'])) {
      $citaObj->campana=$_POST['campana'];  
    }else{
      $citaObj->campana='no';
    }      
    if (isset($_POST['comision'])) {
      $citaObj->comsiona="si";
    }
    if ($_POST['tipoCanal']=='CONVENIO') {
      $citaObj->idconvenio=$_POST['convenio'];         
      $citaObj->comsiona="si";
    }

$usu=Usuario::obtenerUsuario($session->idusuario);
$objtablaP->fecha=$_POST['fecha'];
$objtablaP->hora=$_POST['hora'];
      $objtablaP->idtecnico=$_POST['pos'];
      $objtablaP->iddistrito='1291';
      $objtablaP->tth_instalacion="0".$_POST['canthoraprog'].":00";
      $objtablaP->tth_trasalado='00:00';
      $objtablaP->ubicacion='Taller';
//////////Si es de operaciones Comerciales /////
if ($usu->idrol=='778'){
    $citaObj->idcreador=$_POST['idEjecutvio'];  
    Log::registrarLog('citaTaller',$citaObj->idcita,$_POST,'citaCreada desde ope');  
        if (true) {
            if (!$objtablaP->registrarTrabajoEntabla()) {
                $errores[]="no se creo en tallba cita";
            }else{
                if ($citaObj->crearCitaOPE()) { 
                    $objtablaP->idcita=$citaObj->idcita;
                    ($objtablaP->insertaridcita())?'':die('error en crear cita contactate con pvasquez@tracklink.pe ');
                    $echo="Cita creada";

                }else{
                    $errores[]="Error en crear Cita";
                }
            }
        }
        /*{
            if($turnoNew=$objtablaP->obtenerTrabajosParaLafechayHoraDisponibleTaller()){
                $objtablaP->idturno=$turnoNew;
                if (!$objtablaP->registrarTrabajoEntabla()) {
                    $errores[]="no se creo en tabla programaciokn ";
                }else{
                    if ($citaObj->crearCitaOPE()) { 
                        $objtablaP->idcita=$citaObj->idcita;
                    ($objtablaP->insertaridcita())?'':die('error en crear cita contactate con pvasquez@tracklink.pe ');
                    }else{
                        $errores[]="Error en crear Cita";
                    }
                }
            }else{
                $errores[]="No se registro ninguna cita el horario que selecionaste ya esta ocupado";
            }
        }*/
}else{
	$citaObj->idcreador=$session->idusuario; 
	$citaObj->idestado='14'; 
	Log::registrarLog('citaTaller',$citaObj->idcita,$_POST,'citaCreada');  
	//if (!$dataa=$objtablaP->obtenerTrabajosParaLafechayHoraYespacio()) {
		if (!$objtablaP->registrarTrabajoEntabla()) {
			$errores[]="no se creo en tabla programaciokn ";
		}else{
			if ($citaObj->crearCita()) {  
				$objtablaP->idcita=$citaObj->idcita;
				($objtablaP->insertaridcita())?'':die('error en crear cita contactate con pvasquez@tracklink.pe ');
				$echo="Cita creada exitosamente";
			}else{
				$errores[]="Error en crear Cita";
			}
		}
//	}else{
//		if($turnoNew=$objtablaP->obtenerTrabajosParaLafechayHoraDisponibleTaller()){
//			$objtablaP->idturno=$turnoNew;
//			if (!$objtablaP->registrarTrabajoEntabla()) {
//				$errores[]="no se creo en tabla programaciokn ";
//			}else{
//				if ($citaObj->crearCita()) { 
//					$objtablaP->idcita=$citaObj->idcita;
//					($objtablaP->insertaridcita())?'':die('error en crear cita contactate con pvasquez@tracklink.pe ');
//					$echo="Cita creada";
//				}else{
//					$errores[]="Error en crear Cita";
//				}
//			}
//		}else{
//			$errores[]="No se registro ninguna cita el horario que selecionaste ya esta ocupado";
//		}
//	}
}
      $marca_select = $_POST["servad"];
      foreach($marca_select as $marcas){
          $objServAdicionales->idser=$marcas;
          $objServAdicionales->idoperacion=$objtablaP->idcita;
          $objServAdicionales->registrarOperacionServAdicionalCita();

      }


}else{
  $errores[]="Error token no se realizo ningun registro <br> Por favor intentalo nuevamente";
}
}

if (isset($_POST['guardarCambio'])) {
//    echo "<pre>";
//    die(var_dump($_POST));
  if (isset($_POST['motivoE'])) {
    $citaObj->observacion=$_POST['motivoE'];
    $citaObj->idcita=$_POST['idcita'];
    Log::registrarLog('citaTaller',$_POST['idcita'],$_POST,'Cambios cita');  
    if (!$citaObj->guardarCitaComoEliminado()) {
      $errores[]="No se guardo cita";
    }else{
      $echo.="Elminado por ".$_POST['motivoE'];
    }
  }else{
      $citaObj->idestado=14;
      $citaObj->nombreCliente=$_POST['nombreCompleto'];
    //  $citaObj->plan=$_POST['planR'];
      $citaObj->celular=$_POST['celular'];
      $citaObj->correo=$_POST['correo'];
      $citaObj->costo=$_POST['costo'];
      $citaObj->seguro=$_POST['seguro'];
      $citaObj->placa=$_POST['placa'];
          if (!isset($_POST['noActualizarF'])) {
            $citaObj->fecha=$_POST['fecham']." ".$_POST['hora'];
          }else{
            $citaObj->fecha=$_POST['noActualizarF'];
          }
      $citaObj->tiempo=$_POST['tiempo'];
      if (!isset($_POST['vouCHERANT'])) {            
      if (!empty($_FILES['voucher']['name'])){
          $extencionesAbilitadas= array('jpg','jpeg','png','gif','pdf','docx');
          $nombreArchivo=$_FILES['voucher']['name'];
          $extencionArchivo=explode('.', $nombreArchivo);
          $extencionArchivo=strtolower(end($extencionArchivo));
          $tamanioArchivo=$_FILES['voucher']['size'];
          $ubicacionTemporal=$_FILES['voucher']['tmp_name'];
          if (!in_array($extencionArchivo,$extencionesAbilitadas)) {
          $errores[]='La extencion de la imagen deve ser jpg jpeg png gif pdf';
          }
          if ($tamanioArchivo>5242880) {
          $errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
          }
          if ($nombreArchivo=='') {
            $errores[]="Ingrese nombre de archivo o el archivo correctamente";
          }
          $fecha=date('YmdHis');
          move_uploaded_file($_FILES['voucher']['tmp_name'], '../public/vouchers/'.$fecha.quitarEspacio($_FILES['voucher']['name'])); 
          $citaObj->voucher=$fecha.quitarEspacio($nombreArchivo);
          $citaObj->estadoPago='Pagado';

        }else{
          $citaObj->estadoPago='Pendiente';      
        }
      }else{
          $citaObj->voucher=$_POST['vouCHERANT'];
          $citaObj->estadoPago='Pagado';
      }
      if (isset($_POST['observaciones'])) {
        $citaObj->observacion=$_POST['observaciones'];        
      }
      $citaObj->idcita=$_POST['idcita'];
      if ($citaObj->actualizarDatosDeCita()) {
          if (!isset($_POST['noActualizarF'])) {
            $citaObj->borrardeProgramacioAUTO();
            $objtablaP->fecha=$_POST['fecham'];
            $objtablaP->hora=$_POST['hora'];
            $objtablaP->idtecnico=$_POST['pos'];
              $objtablaP->iddistrito='1291';
              $objtablaP->tth_instalacion="0".$_POST['canthoraprogR'].":00";
              $objtablaP->tth_trasalado='00:00';
              $objtablaP->ubicacion='Taller';
        //if (!$dataa=$objtablaP->obtenerTrabajosParaLafechayHoraYespacio()) {
            if (!$objtablaP->registrarTrabajoEntabla()) {
                $errores[]="no se creo en tallba cita";
            }else{
                    $objtablaP->idcita=$citaObj->idcita;
                    $objtablaP->insertaridcita();
            }
       // }else{
       //     if($turnoNew=$objtablaP->obtenerTrabajosParaLafechayHoraDisponibleTaller()){
       //         $objtablaP->idturno=$turnoNew;
       //         if (!$objtablaP->registrarTrabajoEntabla()) {
       //             $errores[]="no se creo en tabla programaciokn ";
       //         }else{
       //             $objtablaP->idcita=$citaObj->idcita;
       //             $objtablaP->insertaridcita();
       //         }
       //     }else{
       //         $errores[]="No se registro ninguna cita el horario que selecionaste ya esta ocupado";
       //     }
       // }
          }else{

          }
        $echo.="Actualizado";
      }else{
        $errores[]="Error en actualizar";        
      }
  }
}
?>
<link rel="stylesheet" href="../assets/css2/style2.css">
<link rel="stylesheet" href="../assets/css2/style3.css">
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<link href="../assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<link href="../assets/css/animate.css" rel="stylesheet">
<link href="../assets/css/style.css" rel="stylesheet">
    <style>
        .ibox-content > .sk-spinner {
            display: none;
        }

        .ibox-content.sk-loading {
            position: relative;
        }
        .ibox-content.sk-loading:after {
            content: '';
            background-color: rgba(255, 255, 255, 0.7);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        .ibox-content.sk-loading > .sk-spinner {
            display: block;
            position: absolute;
            top: 40%;
            left: 0;
            right: 0;
            z-index: 2000;
        }
    </style>
<body>
  <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
      <div class="sidebar-collapse">
        <?php include("../includes/menuIzquierdo.php") ?>
      </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
          <?php include("../includes/navegacionArriba.php") ?>
        </nav>        
      </div>
      <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
          <div class="col-lg-12">
            <div class="ibox float-e-margins">         
            <div class="ibox-title">
                <h5>Nueva cita Taller</h5>
              </div>
              <div role="tablist">
                <div class="panels"> 
                 <form role="form" method="post" id="personaNatural" class="" enctype="multipart/form-data">
                      <input type="radio" title="Starks" name="tipocotizar" id="starks" role="tab" aria-controls="starks-panel" checked value="Natural" style="display: none;" />
                      <div class="row">                      
                        <div class="form-group col-md-6">
                          <label for="nombreCompleto" class="">Nombre completo * </label>
                          <input style="border: none;" type="text" placeholder="Nombre completo *" id="nombreCompleto" name="nombreCompleto" class="form-control border-bottom text-capitalize"  required >
                        </div>
                      <div class="form-group col-md-3">
                          <label for="celular" class="">Celular</label>
                          <input style="border: none;" type="text" placeholder="Celular" id="celular" name="celular" class="form-control border-bottom text-capitalize" >
                      </div>
                          <div class="form-group col-md-3">
                              <label for="placa" class="">Placa * </label>
                              <input style="border: none;" type="text" placeholder="Placa *" id="placa" name="placa" class="form-control x2 border-bottom" required >
                          </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-md-3">
                          <label for="tiempo" class="">Tiempo *</label>
                          <input style="border: none;" type="number" placeholder="Tiempo *" id="tiempo" name="tiempo" class="form-control border-bottom text-capitalize" required  autocomplete="off">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="costo" class="">Costo *</label>
                          <input style="border: none;" type="text" placeholder="Costo *" id="costo" name="costo" class="form-control border-bottom text-capitalize" required >
                        </div>
                        <div class="form-group col-md-3">
                          <label for="seguro" class="">Seguro * </label>
                          <input style="border: none;" type="text" placeholder="Seguro *" id="seguro" name="seguro" class="seguro form-control border-bottom" required="" autocomplete="off">
                        </div>
                      </div>
                        <!-- Campañas  -->
                      <!-- <?php include("../includes/campanas.php") ?> -->
                        <!-- Campañas  -->
                     <div class="row">
                         <div class="col-md-6">
                             <label for="plan" class="">Plan *</label>
                             <select  style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
                                 <?php if (isset($cotizarObj->plan)): ?>
                                     <option class="" value="<?php echo $cotizarObj->plan ?>"  selected ><?php echo $cotizarObj->plan ?></option>
                                 <?php endif ?>
                                 <option value="cero" data-type="0" >Seleccione Plan *</option>
                                 <?php Cliente::obtenerPlan() ?>
                             </select>
                         </div>
                         <div class="form-group col-md-6 ">
                             <p style="font-weight:bold">Servicios Adicionales *: <label class="canthoraprog2 label-success"> 0 Horas </label></p>
                             <div class="servAdi">
                                 <?php ServiciosAdicionales::obtenerServAdicional('') ?>
                             </div>
                             <input type="hidden" id="canthoraprog" name="canthoraprog" value="0">
                         </div>
                     </div>
                     <div class="row">
                  <div class="form-group col-md-3">
                    <label for="fecha" class="">Fecha * </label>
                    <input style="border: none;" type="text" placeholder="Fecha *" id="fecha" name="fecha" class="form-control border-bottom" required autocomplete="off">
                  </div>
                <div class="form-group col-md-2">
                    <label for="fecha" class="">Turno</label>
                <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn form-control border-bottom btn btn-info" >Seleccionar hora</button>
                <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="pos" class="hora form-control border-bottom"  required="" readonly>
                </div>
                <div class="form-group col-md-2">
                    <label for="fecha" class="">Hora seleccionada</label>
                    <input style="border: none;" type="text" placeholder="Hora *" id="" name="hora" class="horaVer form-control border-bottom"  required="" readonly>
                </div>
                <div class="form-group col-md-5">
                <label for="observaciones" class="">Observaciones </label>
                <input style="border: none;" type="text" placeholder="Observaciones " id="observaciones" name="observaciones" class="form-control border-bottom focusUrgenteta"  >
                </div>
            </div>  
<div class="table-responsive">
    <div class="ibox" id="ibox1">
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-double-bounce">
                <div class="sk-double-bounce1"></div>
                <div class="sk-double-bounce2"></div>
            </div>
            <div id="programacionHtml">
            </div>
        </div>
    </div>
</div>
            <br>
<hr><h2 class="text-center">Datos de canal</h2><hr>
<div class="row">
<div class="form-group col-md-3">
<label for="tipoCanal" class="">Seleccione canal *</label>
    <select style="border: none;"  name="tipoCanal" class="cd-selec form-control border-bottom " required id="tipoCanal">
      <option value="" disabled selected>Seleccione canal *</option>
      <option value="BROKER">Broker</option>
      <option value="CONCESIONARIO">Concesionario</option>
      <option value="CORPORATIVO">Corporativo</option>      
      <option value="ASEGURADORA">Aseguradora</option>
      <option value="CONVENIO">Convenio</option>
      <option value="BANCO">Banco</option>
      <option value="DIGITAL">Digital</option>
      <option value="TRACKLINK OCTO">Tracklink octo</option>
    </select>   
  </div>
  <div class="form-group col-md-2 a">
    <label for="nombreCanal" class="">Canal nombre</label>
    <input style="border: none;" type="text" placeholder="Canal nombre" id="nombreCanal" name="nombreCanal" class="border-bottom form-control text-uppercase nombreCanal " autocomplete="off" required="" >
  </div>
<div class="tiendaAparece1">
   <div class="form-group col-md-2 ">
    <label for="tienda" class="">Tienda</label>
    <input style="border: none;" type="text" placeholder="Tienda" id="tienda" name="tienda" class="border-bottom form-control text-uppercase tienda " autocomplete="off" required="" >
  </div>    
  </div>
  <div class="b">    
  <div class="form-group col-md-2 ">
    <label for="convenio" class="">Seleccione Convenio *</label>
     <select style="border: none;"  name="convenio" class="cd-selec form-control border-bottom" required id="convenio">
     <?php $ConD=Canales::obtenerConvenios() ?>
     <option value="" disabled selected>Seleccione Convenio *</option>
     <?php foreach ($ConD as $var): ?>
        <option value="<?= $var->idconvenio  ?>"><?= $var->nombreConvenio ?></option>
     <?php endforeach ?>
    </select> 
  </div>
  <div class="tiendaAparece">
   <div class="form-group col-md-2 ">
    <label for="tienda" class="">Tienda</label>
    <input style="border: none;" type="text" placeholder="Tienda" id="tienda" name="tienda1" class="border-bottom form-control text-uppercase tienda " autocomplete="off" required="" >
  </div>    
  </div>
  </div>
  <div class="form-group col-md-3">
    <label for="ejecutivoCanalN" class="">Ejecutivo canal</label>
    <input style="border: none;" type="text" placeholder="Ejecutivo canal" id="ejecutivoCanalN" name="ejecutivoCanalN" class="border-bottom form-control text-uppercase ejecutivoCanalN" autocomplete="off" required="" >
  </div>
  <div class="col-md-2">
    <label for="comision">Cobra Comision</label>
    <div class="switch">
      <div class="onoffswitch">
        <input type="checkbox" class="onoffswitch-checkbox" id="comision" value="si" name="comision" >
        <label class="onoffswitch-label" for="comision">
          <span class="onoffswitch-inner"></span>
          <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </div>
  </div>
  </div>
<?php 
  $usu=Usuario::obtenerUsuario($session->idusuario);
 ?>
<?php if($usu->idrol=='778'){ ?>
<div class="row">
  <div class="form-group col-md-12">
      <label for="idEjecutvio" class="">Seleccione Ejecutivo</label>
<?php
    if ($obMenu->administrarComercial=="si") {
?>
 <select  style="border: none;" class="border-bottom form-control text-capitalize" required name="idEjecutvio" id="idEjecutvio">
                <option class='text-capitalize' value='' disabled selected >Seleccione Ejecutivo</option>                
                <?php if ($datosUsu=Usuario::obtenerEjecutivos()): ?>
                	<?php foreach ($datosUsu as $var): ?>
                	<option class='text-capitalize' value='<?= $var->idusuario ?>' > <?= $var->usuarioNombre." ".$var->usuarioApellidoPaterno;  ?></option>
                	<?php endforeach ?>
                <?php endif ?>                
             </select>           
<?php
    }else{
?>
 <select  style="border: none;" class="border-bottom form-control text-capitalize" required name="idEjecutvio" id="idEjecutvio">
<option class='text-capitalize' value='' disabled selected >Seleccione Ejecutivo</option>                
<?php if ($datosUsu=Usuario::obtenerEjecutivosAsignados($session->idusuario)): ?>
    <?php foreach ($datosUsu as $var): ?>
    <option class='text-capitalize' value='<?= $var->idusuario ?>' > <?= $var->usuarioNombre." ".$var->usuarioApellidoPaterno;  ?></option>
    <?php endforeach ?>
<?php endif ?>                
</select>
<?php
   } 
?>
  </div>
</div>
<?php } ?>
<div class="row">
  <div class="col-md-12"><label for="">Adjuntos</label></div>
</div>
        <div class="row">
        <div class="col-md-12">
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <div class="form-control" data-trigger="fileinput">
                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                <span class="fileinput-filename"></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                <span class="fileinput-new">Si el pago ya se realizo adjuntar voucher o el recorte del deposito</span>
                <span class="fileinput-exists">Cambiar</span>
                
                <input type="file" name="voucher"  />
                </span>
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
            </div>   
        </div>
        </div>
            <div class="row">
              <div class="col-lg-12">
                <button class="btn btn-success col-lg-5" id="crearCitaaa" type="submit" name="crearCitaTaller" value="<?php echo Token::generar() ?>">Crear cita en taller</button>                
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">              
                <label id="mostrar" class="text-center text-danger">Limite de taller alcanzado por favor intenta en otro horario</label>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- hasta aqui  -->
<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
      <div class="row">
        <h5 class="col-md-6">Últimas citas creadas</h5> <div class="col-md-6"><form action="" method="post"> <button type="submit" class="btn btn-primary">Actualizar</button></form></div>        
      </div>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-wrench"></i>
          </a>
          <ul class="dropdown-menu dropdown-user">
            <li><a href="#">Config option 1</a>
            </li>
            <li><a href="#">Config option 2</a>
            </li>
          </ul>
          <a class="close-link">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content">
        <input type="text" class="form-control input-sm m-b-xs" id="filter1"
        placeholder="Fitrar citas">
<div class="table-responsive">
        <table class="footable table table-stripped" data-page-size="8" data-filter=#filter1>
          <thead>
            <tr>
              <tr>
                <th>#</th>
                <th >Nombre cliente</th>
                <th data-hide="phone,tablet">Placa</th>
                <th data-hide="phone,tablet">Plan</th>
                <th data-hide="phone,tablet">Comisionado</th>
                <th>Fecha cita</th>
            <?php if($usu->idrol=='778'){ ?>
                <th>Ejecutivo</th>
            <?php
            } ?>
                <th data-hide="phone,tablet">Fecha creación</th>
                <th>Acción</th>
              </tr>
            </tr>
          </thead>
          <tbody>
            <?php if($usu->idrol=='778'){ ?>
                <?php $citaObjDAta=Cita::obtenerCitaADV() ?>
            <?php  }else{
            ?>
                <?php $citaObjDAta=Cita::obtenerCitasEnCreador() ?>
            <?php
            } ?>
            <?php $n=1 ?>
            <?php if ($citaObjDAta): ?>
              <?php foreach ($citaObjDAta as $var): ?>
                <?php $o=Usuario::obtenerUsuario($var->idcreador); ?>
                <tr>
                 <td><?= $n++ ?></td>
                 <td><?= $var->nombreCliente ?></td>
                 <td><?= $var->placa ?></td>
                 <td><?= $var->plan ?></td>
                 <td><?= ($var->comsiona=='si')?$var->comsiona:'no' ?></td>
                 <td><?= ($var->idestado==14)?$var->fecha:obtenerEstado($var->idestado)  ?></td>
            <?php if($usu->idrol=='778'){ ?>
                 <td> <?= $o->usuarioNombre." ".$o->usuarioApellidoPaterno;  ?></td>
<?php
    }
?>
                 <td><?= haceMinimo($var->fechaRegistro)  ?></td>
                 <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#cotizacion" data-id="<?php echo $var->idcita ?>">Elegir <i class="fa fa-cog"> </i></button></td>
              <?php endforeach ?>
            </tr>
          <?php endif ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="5">
              <ul class="pagination pull-right"></ul>
            </td>
          </tr>
        </tfoot>
      </table>
</div>
    </div>
  </div>
</div>
</div>
<div class="modal inmodal" id="cotizacion" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false"style="overflow-y: scroll;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceIn">
      <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="">Cerrar</span></button>

        <h4 class="modal-title">Gestionar</h4>
        <small class="font-bold">Instalación, Eliminar<div class="form-group ">
        </div></small>
      </div>
      <form  role="form" method="post" id="citaEditarR" enctype="multipart/form-data">

        <div class="modal-body" style="background: #fff;padding-top: 0">
        </div>

        <div class="modal-footer" >
          <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
          <button type="submit" name="guardarCambio" class="btn btn-primary">Guardar Cambios</button>
        </div>
      </form>   
    </div>
  </div>
</div>
</div>
<?php include("../includes/footer.php") ?>
</div>
</div>
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<!-- Steps -->
<script src="../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="../assets/js/cotizar.js"></script>
<script src="../assets/build/jquery.datetimepicker.full.js"></script>
<!-- <script src="../assets/js/cotizarEmpresa.js"></script> -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
  <script src="../assets/bootbox/bootbox.all.js"></script>

<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true}) 
  <?php endforeach ?> 
<?php endif ?>
<?php if (!empty($echo)): ?>
  toastr.success('<?php echo $echo ?>', 'Exito', {timeOut: 5000,progressBar:true,closeButton:true})
<?php endif ?>  
<?php if (isset($_POST['enviarCorreo'])): ?>
  toastr.success('Correo re enviado', 'Exito', {timeOut: 5000,progressBar:true,closeButton:true})
<?php endif ?>
</script>  
<script>
      <?php if($canales=Canales::verNombreCanal()): ?>
      $('.nombreCanal').typeahead({
        source: [
        <?php foreach ($canales as $var):?>
        {"name": "<?php echo $var->nombreCanal ?>"},        
      <?php endforeach ?>
      ]
    }); 
    <?php endif ?>   
      <?php if($canales1=Canales::verEjecutiVodeCanal()): ?>
      $('.ejecutivoCanalN').typeahead({
        source: [
        <?php foreach ($canales1 as $var):?>
        {"name": "<?php echo $var->ejecutivoCanal ?>"},        
      <?php endforeach ?>
      ]
    }); 
    <?php endif ?>    
    <?php if($canales=Canales::verTiendadeCanal()): ?>
      $('.tienda').typeahead({
          source: [
          <?php foreach ($canales as $var):?>
          {"name": "<?php echo $var->tiendaCanal ?>"},        
        <?php endforeach ?>
        ]
      }); 
    <?php endif ?>  

$("#fecha").focus(function(){
   $(this).blur(); 
});
<?php $fechaA=date('Y-m-d') ?>
  jQuery.datetimepicker.setLocale('es');
  $('#fecha').datetimepicker({
    minDate:'<?= $fechaA ?>',
      format:'Y/m/d',
      scrollInput: false,
    scrollMonth : false,
    timepicker:false
    // allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });
////////////////////////////////////
  $(document).ready(function(){
    $('.footable').footable();

    $('#cotizacion').on('show.bs.modal', function (e) {

      $("#programacionHtml").html("");
      $('#plan option[value="E-call"]').attr("selected", true);
      $('#plan').change();

      var rowid = $(e.relatedTarget).data('id');
      $.ajax({
        type : 'post',
            url : 'modal/citaDataModal', //Here you will fetch records 
            data :  'rowid='+ rowid, //Pass $id
            success : function(data){
            $('.modal-body').html(data);//Show fetched data from database
          }
        });
    });
function lstTBProg(){
    var fecha = $('#fecha').val();
    var tthinstala=$('#canthoraprog').val();
    var cita = 'General';
    var distrito="1291";
    var lugarT='Taller'; var Clugar='ccita'
    var cod = document.getElementById("plan").value;
    servAd = $('[name="servad[]"]:checked').length;
    if (fecha!='' && cod!='' && servAd!=0 &&(distrito==0 || distrito!=null)) {
        $(".horabtn").off( "click" );
        $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
        $.ajax({
            type : 'POST',
            url  : '../includes/tbProgramacion',
            data : {fecha:fecha.replace("/","-"),cita:cita,tthinstala:tthinstala,iddist:distrito,lugart:lugarT,clugar:Clugar},
            success : function(data)
            {
                $("#programacionHtml").html(data);
                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                $(".horabtn").on("click",lstTBProg).find(".horabtn");
            }
        });
    }else{
        $("#programacionHtml").html('<h3 class="text-center text-info">Primero debe seleccionar Plan,Servicios Adicionales,fecha.</h3>');
    }
    return false;
}

   $(".horabtn").click(function()
    {
        lstTBProg();
    });
$("#mostrar").hide();
$('#fecha').change(function(){
    $("#programacionHtml").html('');
    $(".horabtn").html('Seleccionar hora');
    $("#horaVer").val('');
});

    });
// validacion de cantidad de values en ruc y dni , etc ,etc  
  $("#personaNatural").validate({  });
  $("#citaEditarR").validate({  });
/////////////
$('.b').hide();           
$('.tiendaAparece').hide();           
$('.tiendaAparece1').hide();           
//$('#horabtn').hide();           
$('#tipoCanal').change( function() {
   if(this.value=='CONVENIO'){
      $('.b').show(); 
      $('.a').hide(); 
    $('.tiendaAparece1').hide();          
      $('#convenio').on('change', function() {
        if(this.value=='1'){
          $('.tiendaAparece').show();
          $('.tiendaAparece').prop('disabled',false);
        }else{
          $('.tiendaAparece').prop('disabled',true);
          $('.tiendaAparece').hide();          
        }
      });

      $('#ejecutivoCanalN').prop('disabled', false);
      $('#comision').prop('checked', false);
      $('#comision').prop('disabled', true);
      $('#ejecutivoCanalN').prop('disabled', false);            
   }else if(this.value=='CONCESIONARIO'){
        $('.tiendaAparece1').show();
      $('#ejecutivoCanalN').prop('disabled', false);
      $('#comision').prop('disabled', false);
      $('.b').hide(); 
      $('.a').show(); 
   }else if(this.value=='TRACKLINK OCTO'){
      $('.tiendaAparece1').hide('slow');
      $('#comision').prop('disabled', true);
      $('#ejecutivoCanalN').prop('disabled', true);
      $('.b').hide(); 
      $('.a').show(); 
   }else{
      $('#ejecutivoCanalN').prop('disabled', false);
    $('.tiendaAparece1').hide('slow');
      $('#comision').prop('disabled', false);
    $('.b').hide(); 
      $('.a').show(); 
   }
});



      $('#plan').change(function(){
          $("#programacionHtml").html('');
          $(".horabtn").html('Seleccionar hora');
          $(".horaVer").val('');
          var arr="";
          var seleccionado = $("#plan").find('option:selected');
          var idtec = seleccionado.data('type');

          arr = $('[name="servad[]"]:checked').map(function(){
              return this.value;
          }).get();
          var str = arr.join(',');
          if(str==='' && (idtec === undefined)){
              str='0';
              idtec='0';
          }

          $.ajax({
              type : 'POST',
              url  : 'ajax/ServiciosAdicionales',
              data :  "idTec="+idtec,
              success : function(data)
              {
                  if(data!=''){
                      $(".servAdi").html(data);
                  }else{
                      console.log("No hay data");
                  }
              }
          });
          $("#canthoraprog").val("0");
          $(".canthoraprog2").text("0 Horas");
          /*$.ajax({
              type : 'POST',
              url  : 'ajax/cantidadHoraProgramacion',
              data :  'idCheck='+str+"&idTec="+idtec,
              success : function(data)
              {
                  if(data!=''){
                      $("#canthoraprog").val(data);
                      $(".canthoraprog2").text(data+" Horas");
                  }else{
                      console.log("No hay data");
                  }
              }
          });*/
          return false;

      });

      function validaDP() {
          var fecha = $('#fecha').val();
          var hora = $(".horaVer").val();
          var canthoraprog = $('#canthoraprog').val();
          var idtec = $('input[name="pos"]').val();
          // console.log("fecha: "+fecha+" idtec:"+idtec+" canhora:"+canthoraprog.trim());
          $.ajax({
              type: 'POST',
              url: 'ajax/validarRegistroTB',
              data:'tipo=save&fecha='+fecha+'&idtec='+idtec+"&canthoraprog=" + canthoraprog.trim() + "&hora=" + hora,
             //  data :  'tipo=save&fecha=2019-11-27&idtec=2&canthoraprog=2&hora=12:00',
              success: function (data) {
                  if (data.trim() == 'dp') {
                      //dp='1';
                      $("#crearCitaaa").prop("disabled",true);
                      //      clearInterval(eventos.interval);
                      $("#programacionHtml").html('');
                      $(".horabtn").html('Seleccionar hora');
                      $(".horaVer").val('');
                      // fn(dp);
                      bootbox.alert({
                          title: "<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                          message: "<label>El horario Seleccionado("+hora+") ya fue Registrado,Por Favor Seleccione otro*</label>",
                          size: 'small',
                          callback: function () {
                              /*  l.ladda( 'stop' );*/
                          }
                      });
                  }else if(data.trim()=='Ok'){
                      $("#crearCitaaa").prop("disabled",false)
                  }
              }
          });
      }

      var eventos={};
      eventos.frejcar =function(){
          validaDP();
      };
      eventos.interval = setInterval(eventos.frejcar, 3000);


      <?php if ($datos=ServiciosAdicionales::obtenerServicesAdicional()): ?>
      <?php foreach ($datos as $var): ?>
      $("#<?php echo $var->codigo ?>").on("change",function (e) {
          $("#programacionHtml").html('');
          $(".horabtn").html('Seleccionar hora');
          $(".horaVer").val('');

          var isChecked = e.currentTarget.checked;
          var seleccionado = $("#plan").find('option:selected');
          var idtec = seleccionado.data('type');
          //  alert(isChecked);
          var arr = $('[name="servad[]"]:checked').map(function(){
              return this.value;
          }).get();
          var str = arr.join(',');
          // $("#canthoraprog").val("0");
//    $(".canthoraprog2").text("0 Horas");
          if(!isChecked && (idtec === undefined)){
              str='0';
              idtec='0';
          }
          $.ajax({
              type : 'POST',
              url  : 'ajax/cantidadHoraProgramacion',
              data :  'idCheck='+str+"&idTec="+idtec,
              success : function(data)
              {
                  if(data!=''){
                      $("#canthoraprog").val(data);
                      $("#canthoraprog2").text(data+" Horas");
                  }else{
                      console.log("No hay data");
                  }
              }
          });
          return false;
      });
      <?php endforeach ?>
      <?php endif ?>
///////////////////////////////////////////////////
  $('.seguro').typeahead({
    source: [
    {"name": "Interseguro"},
    {"name": "No tiene seguro"},
    {"name": "Por definir"},
    {"name": "Rimac Seguros"},
    {"name": "Pacifico Seguros"},
    {"name": "La Positiva"},
    {"name": "Mapfre Peru"},
    {"name": "HDI Seguros"}
    ]
  });
  </script>

 <?php include_once('../includes/script.php') ?>
</body>
</html>
<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>

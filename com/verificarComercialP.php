<?php $nav="verificarComercialP" ;?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
	irA("../ingresar");
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->verificarComercialP=="si"): ?>
<body>
	<div id="wrapper">
		<nav class="navbar-default navbar-static-side" role="navigation">
			<div class="sidebar-collapse">
				<?php include("../includes/menuIzquierdo.php") ?>
			</div>
		</nav>
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div class="row border-bottom">
				<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
					<?php include("../includes/navegacionArriba.php") ?>

				</nav>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="wrapper wrapper-content">


						<div class="row">
							<div class="col-lg-12">
								<div class="ibox float-e-margins">
									<div class="ibox-title">
										<h5>Ventas del mes - <?php  $mes=date("m"); echo $retVal = ($mes==01) ? "Enero" : "Febrero" ;  ?> del <?php echo $anio=date("Y") ?></h5>
									</div>
									<div class="ibox-content">
										<div>
											<div id="slineChart" ></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">

							<div class="col-lg-3">
								<div class="ibox float-e-margins">
									<div class="ibox-title">
										<h5>Ventas total diaria <?php $fecha=date("l") ?>
										<?php if ($fecha=="Monday"): ?>
											Lunes
										<?php endif ?>
										<?php if ($fecha=="Tuesday"): ?>
											Martes
										<?php endif ?>										
										<?php if ($fecha=="Wednesday"): ?>
											Miercoles
										<?php endif ?>
										<?php if ($fecha=="Thursday"): ?>
											Jueves
										<?php endif ?>
										<?php if ($fecha=="Friday"): ?>
											Viernes
										<?php endif ?></h5>
									</div>
									<div class="ibox-content">
										<div>
											<div id="pie"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-9">
								<div class="ibox float-e-margins">
									<div class="ibox-title">
										<h5>Estadistica mensual </h5>

									</div>
									<div class="ibox-content">
										<div>
											<div id="stocked"></div>
										</div>
									</div>
								</div>
							</div>
						</div>


					</div>




				</div>



			</div>

			<?php include("../includes/footer.php") ?>
		</div>
	</div>

</div>

<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- FooTable -->
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/d3/d3.min.js"></script>
<script src="../assets/js/plugins/c3/c3.min.js"></script>
 <?php include_once('../includes/script.php') ?>
 <?php 
$objEstaditica=new Estadistica();

?>

<script>
	$(document).ready(function(){
		c3.generate({
			bindto: '#slineChart',
			data:{
				columns: [
				  <?php $objEstaditica->obtenerVentasMensualesPorPlan() ?>
				
				],
				colors:{
					InstalacionesMax: '#FF0000',
					APAZA: '#00FF7F',
					MONTERREY: '#D2B48C',
					CHINCHAY: '#9ACD32',
					MEDINA: '#FFA500',
					MEDINA: '#FFA500',
					MEDINA: '#FFA500',
					MEDINA: '#FFA500',
					MEDINA: '#FFA500',
					MEDINA: '#FFA500',
					MEDINA: '#FFA500',
					MEDINA: '#FFA500',
					MEDINA: '#FFA500',
					Disponible: '#fff',
					Disponible: '#fff',
					Disponible: '#fff',
					DURAN: '#BABABA'
				},
				type: 'spline'
			}
		});
		c3.generate({
			bindto: '#pie',
			data:{
				columns: [
				<?php $objEstaditica->obtenerVentasDiariasPorPlan() ?>
				],
				
				type : 'pie'
			}
		});
		c3.generate({
				<?php $o=$objEstaditica->obtenerPlanes()  ?>
			bindto: '#stocked',
			data:{
				x: 'x',
				columns: [
				['x', 'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Junio','Septiembre','Octubre','Noviembre','Diciembre'],
				<?php foreach ($o as $var): ?>	
				['<?php echo $var->plan ?>', <?php $ob=$objEstaditica->obtenerVistaAnualPorPlan('01',$var->plan);echo $ob->ventas ?>,<?php $ob=$objEstaditica->obtenerVistaAnualPorPlan('02',$var->plan);echo $ob->ventas ?>,<?php $ob=$objEstaditica->obtenerVistaAnualPorPlan('03',$var->plan);echo $ob->ventas ?>,<?php $ob=$objEstaditica->obtenerVistaAnualPorPlan('04',$var->plan);echo $ob->ventas ?>,<?php $ob=$objEstaditica->obtenerVistaAnualPorPlan('05',$var->plan);echo $ob->ventas ?>,<?php $ob=$objEstaditica->obtenerVistaAnualPorPlan('06',$var->plan);echo $ob->ventas ?>,<?php $ob=$objEstaditica->obtenerVistaAnualPorPlan('07',$var->plan);echo $ob->ventas ?>,<?php $ob=$objEstaditica->obtenerVistaAnualPorPlan('08',$var->plan);echo $ob->ventas ?>,<?php $ob=$objEstaditica->obtenerVistaAnualPorPlan('09',$var->plan);echo $ob->ventas ?>],<?php $ob=$objEstaditica->obtenerVistaAnualPorPlan('10',$var->plan);echo $ob->ventas ?>,<?php $ob=$objEstaditica->obtenerVistaAnualPorPlan('11',$var->plan);echo $ob->ventas ?>,<?php $ob=$objEstaditica->obtenerVistaAnualPorPlan('12',$var->plan);echo $ob->ventas ?>,
				<?php endforeach ?>],
				colors:{
					Vendidos: '#71c173',
					Cancelados: '#e40e0e'
				},
				type: 'bar',
				// groups: [
				// [
				// <?php foreach ($o as $var): ?>	

				// '<?php echo $var->plan ?>',
				// <?php endforeach ?>
				// 	],
				
				// ]
			},
			 axis: {
        x: {
            type: 'category',
            tick: {
                rotate: 75,
                multiline: false
            },
            height: 130
        }
    }
		});

	});

</script>
<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>

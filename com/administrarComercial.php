<?php $nav="administrarComercial" ;?>
<?php include("../includes/head.php") ?>
<link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->administrarComercial=="si"): ?>
<?php 
	$usuarioObj=new Usuario;
    $echo=array();
    $errores=array();
 ?>
<?php 

    if (isset($_POST['ejecutivoDeOperacionesCoMercial'])) {
    	$usuarioObj->idusuario=limpiar($_POST['usuarioid']);
    	$usuarioObj->comercialOperaciones=limpiar($_POST['ejecutivoDeOperacionesCoMercial']);
    	if ($usuarioObj->asignarEjecutivoDeOperaciones()) {
    		$echo[]="Acualizado exitosamente";
    	}else{
    		$errores[]="Algun error acurrio";
    	}
    }
    if (isset($_POST['elimiarEjecutioOperacionesModal'])) {    
    	$usuarioObj->idusuario=limpiar($_POST['usuarioid']);
    	if ($usuarioObj->quitarEjecutivoDeOperaciones()) {
    		$echo[]="Acualizado exitosamente";    		
    	}else{
    		$errores[]="Algun error acurrio 132123";
    	}
    }

 ?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<body>
<style>
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
}
</style>
<div class="se-pre-con"></div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>


    <div id="wrapper">

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <?php include("../includes/menuIzquierdo.php") ?>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                   <?php include("../includes/navegacionArriba.php") ?>
               </nav>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
                 
         <div class="ibox">
                        <div class="ibox-title">
                            <h5>Consulta de reporte personalizado</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                           
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <form action="PHPExel/adminComReporte" method="post">
                             <div class="row">
                             <div class="form-group">
                                 
                                <label class="col-lg-2 control-label">Rango de fecha</label>
                                <div class="col-lg-3">
                                <input style="border: none;"  class="cd-select form-control border-bottom" required  type="text" placeholder="Seleccione Rango de fecha" class="form-control" value="" name="fecha" id="rango">
                                </div>
                                <label class="col-lg-1 control-label">Tema</label>
                                <div class="col-lg-3"> 
                                    <select style="border: none;"  class="cd-select form-control border-bottom" required name="tema" id="tema" >
                                        <option value="" disabled selected>Seleccione tema *</option>
                                        <option value="canales">Canales</option>
                                        <option value="pacifico">Pacifico</option>
                                        <option value="instalaciones">Instalaciones</option>
                                        <option value="concesionario">Ventas por concesionario</option>
                                        <option value="aseguradora">Ventas por aseguradora</option>
                                        <option value="cliente">Reporte de clientes</option>
                                    </select> 
                                </div>

                                <div id="resultado1" class="col-lg-3"></div>
                             </div>
                                <!-- <button class="btn btn-primary" name="consultar" type="submit">Consultar</button> -->
                                </div>
                               </form>
                            </div>
                        </div>




            <div class="ibox" >
                <div class="ibox-title">
                    <div class="row"><div class="col-md-6"><h5>Ejecutivos comerciales</h5></div>
                    <div class="col-md-6 pull-right"><form action="" method="post"><button type="submit" class="btn btn-success"><i class="fa fa-refresh"></i> Actualizar</button></form></div></div>
                </div>

                <div class="ibox-content" >
               
                    <input type="text" class="form-control input-sm m-b-xs" id="filter"
                    placeholder="Filtrar aquí">

                    <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre(s)</th>
                                <th>Rol(s)</th>
                                <th>Acción</th>
                            </tr>
                        </thead>  
                        
                        <tbody>
                       <?php $usD=Usuario::obtenerParaVenta() ?>
                       <?php $n=1 ?>
                       <?php foreach ($usD as $var): ?>
                       	<tr>
                       		<td><?= $n++ ?></td>
                       		<td><?= $var->usuarioNombre." ".$var->usuarioApellidoPaterno." ".$var->usuarioApellidoMaterno ?></td>
                       		<?php $rolD=Usuario::obtenerRoldeUsuario($var->idrol) ?>
                       		<td><?= $rolD->cargo ?></td>
                       		<td><button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#ordenes" data-id="<?php echo $var->idusuario ?>">Editar <i class="fa fa-cog"> </i></button></td>
                       	</tr>
                       <?php endforeach ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="modal inmodal" id="ordenes" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content animated bounceIn">
                        <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                            <h4 class="modal-title">Administrar</h4>
                            <!-- <small class="font-bold">pendiente</small> -->
                            <div class="form-group ">
                            </div>
                        </div>
                        <form  role="form" method="post" >

                            <div class="modal-body" style="background: #fff;padding-top: 0">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                <!-- <button type="submit" name="actualizar" class="btn btn-primary" >Guardar Cambios</button> -->
                            </div>
                        </form>   
                    </div>
                </div>
            </div>




            <!-- modal fin -->



            <!-- modal aqui fin  desarrollaremos   -->




        </div>
        <?php include("../includes/footer.php") ?>

    </div>
</div>


<!-- Mainly scripts -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>

<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>

    <script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
    <script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
 <?php include_once('../includes/script.php') ?>


 <script>
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>


<script>


<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
<script>
    $(document).ready(function(){
        $('.footable').footable();
        $('.footable2').footable();
    });
    $('#ordenes').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        $.ajax({
            type : 'post',
                url : 'modal/administrarDataModal', //Here you will fetch records 
                data :  'rowid='+ rowid, //Pass $id
                success : function(data){
                    $('.modal-body').html(data);//Show fetched data from database
                }
            });
    });

          $('#rango').daterangepicker({
    format: 'DD-MM-YYYY',
    endDate: moment(),
    showDropdowns: true,
    showWeekNumbers: true,
    timePicker: false,
    timePickerIncrement: 1,
    timePicker12Hour: false,
    ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
        'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Anterior Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    opens: 'right',
    drops: 'down',
    buttonClasses: ['btn', 'btn-sm'],
    applyClass: 'btn-primary',
    cancelClass: 'btn-default',
    separator: '/',
    locale: {
        applyLabel: 'Seleccionar',
        cancelLabel: 'Cancelar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Seleccionar Rango',
        daysOfWeek: ['Do', 'Lun', 'Mar', 'Mi', 'Ju', 'Vi','Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
    }
}, function(start, end, label) {               
     // $('#rango').focus();
});  
 $( "#rango" ).change(function() {
    $('#rango').focus();
}); 
 $( "#limpair" ).click(function() {
    $('#rango').val('');
    $('#rango').focus();
});


    $("#rango").bind("focus",function()
      {
          var fecha=$(this).val();          
          var tema = $("#tema").val();           
          $.ajax({
            type : 'POST',
            url  : 'ajax/reporteAdmiCom',          
            data : {fecha:fecha,tema:tema},
            success : function(data)
            {
             $("#resultado1").html(data);
           }
         });

      });
    $("#tema").change(function() {        
          var fecha=$('#rango').val();          
          var tema = $(this).val();           
          $.ajax({
            type : 'POST',
            url  : 'ajax/reporteAdmiCom',          
            data : {fecha:fecha,tema:tema},
            success : function(data)
            {
             $("#resultado1").html(data);
           }
         });

      });
        

</script>

</body>

</html>

  <?php else: ?>
    <h2>presione f5</h2>
    <?php  irA("ingresar") ?> 
<?php endif ?>

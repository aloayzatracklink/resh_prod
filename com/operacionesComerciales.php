<?php $nav="operacionesComerciales";?>
<?php include("../includes/head.php") ?>
<link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<link rel="stylesheet" href="../assets/css/jquery-confirm.min.css">
<?php if (!$session->yaEstaIngresado()){
  irA("ingresar");
} ?>
<?php
$obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->operacionesComerciales=="si"): ?>
<?php
require_once '../mailphp/PHPMailer/PHPMailerAutoload.php';
require_once '../includes/tablaProgramacion.php';
$mail = new PHPMailer;
$errores=array();
$echo=array();
$clienteObj=new Cliente();
$agregarVehiculoObj=new Vehiculo();
$facturacionObj=new DatosFacturacion();
$operacionesObj=new Operacion();
$objCalendario=new Calendario();
$objtablaP=new tablaProgramacion();
$objServAdicionales=new OperacionServAdicionales();
$objCanales= new Canales();
$objComision=new Comisiones();
$tramaIntegra=new TramaIntegracion();
$tramaIntegrapf=new TramaIntegracionPF();
$tramaIntegraql=new TramaIntegracionQL();
$tramaIntegraty=new TramaIntegracionTY();

$datosAINT=$tramaIntegra->obtenerTramaIntegracionPagn("");
$datosAINTPF=$tramaIntegrapf->obtenerTramaIntegracionPagn("");

$operacionObj;
$calObj;
$datosFacturacionObj;
$dniORuc='';
if (isset($_POST['crearDesdeChasis'])) {
//    echo "<pre>";
//    die(var_dump($_POST));
if (Token::verificar($_POST['crearDesdeChasis'])) {
    if (isset($_POST['dniORuc'])) {
        $dniORuc=$_POST['dniORuc'];
    }else{
        $dniORuc=false;
        $errores[]='Ningun registro y o actualización realizada';
    }
    if (!empty($_FILES['voucher']['name'])) {
    $extencionesAbilitadas= array('jpg','jpeg','png','gif','PNG','JPG','JPEG','PDF','DOCX','GIF','docx','pdf');
    $nombreArchivo=$_FILES['voucher']['name'];
    $tmp=explode('.', $nombreArchivo);
    $extencionArchivo=end($tmp);
    $tamanioArchivo=$_FILES['voucher']['size'];
    $ubicacionTemporal=$_FILES['voucher']['tmp_name'];
        if (!in_array($extencionArchivo,$extencionesAbilitadas)) {
            $errores[]='La extencion de la imagen debe ser jpg jpeg png gif';
        }
        if ($tamanioArchivo>5242880) {
            $errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
        }
    }
    if ($n= strlen($dniORuc)==8){
        $clienteObj->tipoCliente=ucwords(strtolower("Natural"));
        $clienteObj->dni=$dniORuc;
        $clienteObj->nombre=ucwords(strtolower($_POST['nombres']));
        $clienteObj->apellidosC=ucwords(strtolower($_POST['apellidos']));
        $clienteDatos=$_POST['nombreCliente'];
        $clienteObj->telefono=$_POST['celular'];
        $clienteObj->telefono1=$_POST['telefono5'];
        $clienteObj->telefono2=$_POST['telefono2'];
        $clienteObj->telefono3=$_POST['telefono'];
        $clienteObj->correo=$_POST['correoCliente'];
        $clienteObj->idCreador=$session->idusuario;
        if ($clienteObj->registrarClienteNatural()) {

        }else{
            $errores[]="Se detecto un error al registrar el cliente en empresa ";
        }
    }else
    if ($n=strlen($dniORuc)==11) {
        $clienteObj->tipoCliente="Empresa";
        $clienteObj->ruc=$dniORuc;
        $clienteObj->razonSocial=ucwords(strtolower($_POST['apellidos']));
        $clienteDatos=$_POST['nombreCliente'];
        $clienteObj->nombre=ucwords(strtolower($_POST['nombreCliente']));
        $clienteObj->telefono=$_POST['telefono'];
        if (isset($_POST['telefono1'])||isset($_POST['telefono3'])) {
            $clienteObj->telefono1=$_POST['telefono1'];
            $clienteObj->telefono3=$_POST['telefono3'];
        }
        $clienteObj->telefono2=$_POST['telefono2'];
        $clienteObj->correo=$_POST['correoCliente'];
        $clienteObj->idCreador=$session->idusuario;
        $clienteObj->enOnix="si";
        if ($clienteObj->registrarClienteEmpresa()) {

        }else{
            $errores[]="Se detecto un error al registrar el cliente en empresa ";
        }
    }else{
        $clienteObj->tipoCliente=ucwords(strtolower("Natural"));
        $clienteObj->dni=$_POST['dniORuc'];
        $clienteObj->nombre=ucwords(strtolower($_POST['nombres']));
        $clienteObj->apellidosC=ucwords(strtolower($_POST['apellidos']));
        $clienteDatos=$_POST['nombreCliente'];
        $clienteObj->telefono=$_POST['celular'];
        $clienteObj->telefono1=$_POST['telefono5'];
        $clienteObj->telefono2=$_POST['telefono2'];
        $clienteObj->telefono3=$_POST['telefono'];
        $clienteObj->correo=$_POST['correoCliente'];
        $clienteObj->idCreador=$session->idusuario;
        if(empty($errores)){
            if ($clienteObj->registrarClienteNatural()) {

            }else{
                $errores[]="Se detecto un error al registrar el cliente en empresa ";
            }
        }
    }
    if (empty($errores)) {
            $agregarVehiculoObj->idcliente=$clienteObj->idcliente;
            $agregarVehiculoObj->plan=$_POST['plan'];
            $agregarVehiculoObj->placa=$_POST['placaVehiculo'];
            $agregarVehiculoObj->marca=$_POST['marca'];
            $agregarVehiculoObj->modelo=$_POST['modelo'];
            $agregarVehiculoObj->color=$_POST['color'];
            $agregarVehiculoObj->anio=$_POST['anioF'];
            $agregarVehiculoObj->chasis=$_POST['chasis'];
            $agregarVehiculoObj->motor=$_POST['motor'];
            $agregarVehiculoObj->planTiempo=$_POST['tiempo'];
            $agregarVehiculoObj->companiaSeguro=$_POST['seguro'];
            if (isset($_POST['campana'])) {
                $agregarVehiculoObj->campana=htmlspecialchars($_POST['campana']);
            }else{
                $agregarVehiculoObj->campana='no';
            }
            $agregarVehiculoObj->enOnix="si";
            if ($agregarVehiculoObj->registrarVehiculo($clienteObj->idcliente)) {
                $facturacionObj->idcliente=$clienteObj->idcliente;
                $facturacionObj->idvehiculos=$agregarVehiculoObj->idvehiculo;

    //$entrega=limpiar($_POST['entregaEnInstalacion']);
    $obsEntrega='';
      //if ($entrega=='no' || $entrega=='noE') {
      //  $facturacionObj->entregaEnInstalacion='si';
      //  if($entrega=='no'){
      //      $obsEntrega.=' Entregar documento de facturacion al momento de la instalacion';
      //  }
      //}else
      //{
      //   $facturacionObj->entregaEnInstalacion='';
      //   $facturacionObj->fechaHoraDeEntrega=$_POST['horarioEntrega'];
      //   $facturacionObj->registrarSugerenciaDeFechaEntrega();
      //   $facturacionObj->fecha=$_POST['horarioEntrega'];
      //   $facturacionObj->direccionEntrega=$_POST['direccionEntrega'];
      //   $facturacionObj->referenciaEntrega=$_POST['referenciaEntrega'];
      //   $facturacionObj->distritoEntrega=$_POST['distritoEntrega'];
      //   $facturacionObj->recibidoPor=$_POST['personaEntrega'];
      //}

                if (isset($_POST['tipoDocumentoFacturacion'])) {
                    $facturacionObj->tipoDocumentoFacturacion=$_POST['tipoDocumentoFacturacion'];
                    $facturacionObj->rucFacturacion=$_POST['rucFacturacion'];
                    $facturacionObj->dniFacturacion=$dniORuc;
                    $facturacionObj->razonSocialFacturacion=$_POST['razonSocialFacturacion'];
                    $facturacionObj->direccionFiscal=$_POST['direccionFiscalFacturacion'];
                    $facturacionObj->telefonoFacturacion=$_POST['telefonoFacturacion'];
                }else{
                    $facturacionObj->tipoDocumentoFacturacion=$_POST['formaPago'];
                        if ($_POST['formaPago']=='pacificoPandero'|| $_POST['formaPago']=='pacificoBanbif') {
                        $facturacionObj->rucFacturacion='20332970411';
                        $facturacionObj->dniFacturacion='20332970411';
                        $facturacionObj->razonSocialFacturacion='PACIFICO COMPAÑIA DE SEGUROS Y REASEGUROS';
                        $facturacionObj->direccionFiscal='AV. JUAN DE ARONA NRO. 830 LIMA - LIMA - SAN ISIDRO';
                        $facturacionObj->telefonoFacturacion='5184000';
                    }else if($_POST['formaPago']=='Banbif'){
                        $facturacionObj->rucFacturacion='20101036813';
                        $facturacionObj->dniFacturacion='20101036813';
                        $facturacionObj->razonSocialFacturacion='BANCO INTERAMERICANO DE FINANZAS';
                        $facturacionObj->direccionFiscal='AV. RICARDO RIVERA NAVARRETE NRO. 600 LIMA - LIMA - SAN ISIDRO';
                        $facturacionObj->telefonoFacturacion='6133000';
                    }
                }
                $facturacionObj->idUsuarioCreado=$_POST['idEjecutvio'];
                $facturacionObj->plan=$_POST['plan'];
                $facturacionObj->tiempo=$_POST['tiempo'];
                $facturacionObj->costo=$_POST['precioLista'];


                /////////////////// Canales y comissión //////////////////
                // if ($_POST['formaPago']=='Deposito'||$_POST['formaPago']=='OrdenDeCompra') {
                // }

                $facturacionObj->observaciones=$_POST['formaPago'];
                if (isset($_POST['observaciong'])) {
                    $facturacionObj->observaciones.=" ".$_POST['observaciong'];
                }
                $fecha=date("YmdHis");
                if (!empty($_FILES['voucher']['name'])) {
                    move_uploaded_file($ubicacionTemporal, '../public/vouchers/'.$fecha.limpiar($nombreArchivo));
                    $facturacionObj->voucher=$fecha.limpiar($nombreArchivo);
                }
                $facturacionObj->canalOrigen="";
                if (isset($_POST['corigen'])){
                    $facturacionObj->canalOrigen=$_POST['corigen'];
                }
                $facturacionObj->tipoCanal=$_POST['tipoCanal'];
                if (isset($_POST['tienda'])) {
                    if (!empty($_POST['tienda'])) {
                        $facturacionObj->oficina=$_POST['tienda'];
                    }
                }
                if (isset($_POST['tienda1'])) {
                    if (!empty($_POST['tienda1'])) {
                        $facturacionObj->oficina=$_POST['tienda1'];
                    }
                }
                if (!empty($_POST['convenio'])) {
                    $facturacionObj->nombreCanal=$_POST['convenio'];
                }else{
                    $facturacionObj->nombreCanal=$_POST['nombreCanal'];
                }
                if (isset($_POST['ejecutivoCanalN'])) {
                    $facturacionObj->ejecutivoCanal=$_POST['ejecutivoCanalN'];
                }

                if(isset($_POST['formaPago'])){
                    $facturacionObj->formaPago=$_POST['formaPago'];
                }

if ($facturacionObj->registrarDatosDeFacturacion()) {
            $operacionesObj->datosfacturacionid=$facturacionObj->datosfacturacionid;
/////////////////// Canales y comissión //////////////////

    if (isset($_POST['comision']) && $_POST['comision']=='si') {
        $objComision->tiempo=$_POST['tiempo'][0];
        $objComision->idcliente=$clienteObj->idcliente;
        $objComision->idcanal='0';
        $objComision->idvehiculo=$agregarVehiculoObj->idvehiculo;
        $objComision->idfacturacion=$facturacionObj->datosfacturacionid;
        $objComision->montoTotal=$_POST['precioLista'];
        $objComision->comision="PENDIENTE";
        $objComision->idCreador=$_POST['idEjecutvio'];
        $objComision->fechapago=date("YmdHis");
        if (isset($_POST['tipoDocumentoFacturacion'])) {
            $objComision->documentoFacturacion=$facturacionObj->tipoDocumentoFacturacion;
        }else{
            $objComision->documentoFacturacion=$_POST['formaPago'];
        }
        if (!$objComision->registrarComision()) {
            $errores[]="Error en registrar datos de comision";
        }
    }

/////////////////// Canales y comissión //////////////////
              if (isset($_POST['lugarInstacion'])  && $_POST['tipo']=='Instalacion') {
                $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
              }
              if (isset($_POST['cordenadasD']) ) {
                $operacionesObj->coordenadasInstalacion= $_POST['cordenadasD'];
              }
              if (isset($_POST['concesionarioInstalacion'])) {
                $operacionesObj->nombreConcecionarioInstalicion=$_POST['concesionarioInstalacion'];
              }
              if (isset($_POST['observacion'])) {
                $operacionesObj->observacion=$_POST['observacion']." ".$obsEntrega;
              }
    $operacionesObj->nombreContacto=$_POST['nombreContacto'];
    $operacionesObj->numeroContacto=$_POST['numeroContacto'];
    $operacionesObj->tth_instalacion="0".$_POST['canthoraprog'].":00";
    $operacionesObj->tth_trasalado='01:30';

if ($_POST['lugarInstacion']=='Taller' && $_POST['tipo']=='Instalacion') {
    if (isset($_POST['fechaIT'])) {
    	if(!isset($_POST['horaT'])){
		$_POST['horaT']='09:00';

	}
        $operacionesObj->fechaInstalacion=$_POST['fechaIT']." ".$_POST['horaT'];
    }

    $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
    $operacionesObj->departamentoi=$_POST['depT'];
    $operacionesObj->provinciai=$_POST['proT'];
    $operacionesObj->distritoi=$_POST['disT'];
    $operacionesObj->direccionInstacion=$_POST['direccionTaller'];

    if (isset($_POST['fechaIT'])) {
        $objtablaP->fecha=$_POST['fechaIT'];
        $objtablaP->hora=$_POST['horaT'];
        $objtablaP->idtecnico=$_POST['posD'];
        $objtablaP->idtablaProgramacion=$_POST['delRD'];
        $objtablaP->iddistrito=$_POST['disT'];
        $objtablaP->tth_instalacion="0".$_POST['canthoraprog'].":00";
        $objtablaP->tth_trasalado='00:00';
        $objtablaP->ubicacion='Taller';
    //if (!$dataa=$objtablaP->obtenerTrabajosParaLafechayHoraYespacio()) {
       if (!$objtablaP->registrarTrabajoEntabla()) {
            $errores[]="no se creo en tabla programaciokn ";
        }
   // }else{
   //     if($turnoNew=$objtablaP->obtenerTrabajosParaLafechayHoraDisponibleTaller()){
   //         $objtablaP->idturno=$turnoNew;
   //         if (!$objtablaP->registrarTrabajoEntabla()) {
   //             $errores[]="no se creo en tabla programaciokn ";
   //         }
   //     }else{
   //         $errores[]="No se registro ninguna cita el horario que selecionaste ya esta ocupado";
   //     }
   //     }
    }
}else
if($_POST['lugarInstacion']=='Domicilio Cliente' && $_POST['tipo']=='Instalacion'){
    if (isset($_POST['fechaID'])) {
        $operacionesObj->fechaInstalacion=$_POST['fechaID']." ".$_POST['horaD'];
        $operacionesObj->departamentoi=$_POST['departamentoInstalacionD'];
        $operacionesObj->provinciai=$_POST['provinciaInstalacionD'];
        $operacionesObj->distritoi=$_POST['distritoInstalacionD'];
        $operacionesObj->direccionInstacion=$_POST['direccionInstalacionD'];
        $operacionesObj->referenciaInstalacion=$_POST['referenciaInstalacionD'];
    }
    $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
    if (isset($_POST['fechaID'])) {
        $objtablaP->fecha=$_POST['fechaID'];
        $objtablaP->hora=$_POST['horaD'];
        $objtablaP->idtecnico=$_POST['posD'];
        $objtablaP->idtablaProgramacion=$_POST['delRD'];
        $objtablaP->iddistrito=$_POST['distritoInstalacionD'];
        $objtablaP->tth_instalacion="0".$_POST['canthoraprog'].":00";
        $objtablaP->tth_trasalado='00:00';
        $objtablaP->ubicacion='Domicilio';
        if (!$objtablaP->registrarTrabajoEntabla()) {
            $errores[]="no se creo en tabla programaciokn ";
        }
    }
}else
if($_POST['lugarInstacion']=='Concesionario' && $_POST['tipo']=='Instalacion'){
    if (isset($_POST['fechaIC'])) {
        $operacionesObj->fechaInstalacion=$_POST['fechaIC']." ".$_POST['horaC'];
        $operacionesObj->departamentoi=$_POST['departamentoInstalacionC'];
        $operacionesObj->provinciai=$_POST['provinciaInstalacionC'];
        $operacionesObj->distritoi=$_POST['distritoInstalacionC'];
        $operacionesObj->direccionInstacion=$_POST['direccionInstalacionC'];
        $operacionesObj->referenciaInstalacion=$_POST['referenciaInstalacionC'];
    }
    $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
    $operacionesObj->nombreConcecionarioInstalicion=$_POST['concesionarioInstalacion'];

    if (isset($_POST['fechaIC'])) {
        $objtablaP->fecha=$_POST['fechaIC'];
        $objtablaP->hora=$_POST['horaC'];
        $objtablaP->idtecnico=$_POST['posD'];
        $objtablaP->idtablaProgramacion=$_POST['delRD'];
        $objtablaP->iddistrito=$_POST['distritoInstalacionC'];
        $objtablaP->tth_instalacion="0".$_POST['canthoraprog'].":00";
        $objtablaP->tth_trasalado='00:00';
        $objtablaP->ubicacion='Concesionario';
        if (!$objtablaP->registrarTrabajoEntabla()) {
            $errores[]="no se creo en tabla programacion";
        }
    }
}

if (isset($_POST['pendienteInstacion'])) {
    $operacionesObj->observacion.=" ".$_POST['observaciong']." ".$obsEntrega;
    $operacionesObj->estado='0';
}else{
    $operacionesObj->estado='14';
    $operacionesObj->observacion.=" ".$_POST['observaciong']." ".$obsEntrega;
}
    if (isset($_POST['tipo'])) {
        $operacionesObj->trabajo=$_POST['tipo'];
    }


if ($operacionesObj->programarInstalacion($agregarVehiculoObj->idvehiculo,$clienteObj->idcliente) && $_POST['tipo']=='Instalacion') {
    Log::registrarLog('operaciones',$operacionesObj->idoperacion,$_POST,"agregado desde operacionesComerciales");
    $operacionesObj->agregarIdOperacionesCOmercialesF();
    if(isset($_POST['idTrama'])){

        $tramaIntegra->reasignacion = 0;
        if(isset($_POST['nreasignacion'])){
            $tramaIntegra->reasignacion = 1;
        }


        $tramaIntegra->idoperacion=$operacionesObj->idoperacion;
        $tramaIntegra->idtrama=$_POST['idTrama'];
        $tramaIntegra->updateTrama();

        $tramaIntegra->chasis=$_POST['chasis'];
        $tramaIntegra->verifyChasisCorrected();
    }

    if(isset($_POST['idTramaf'])){


        $tramaIntegrapf->idoperacion=$operacionesObj->idoperacion;
        $tramaIntegrapf->idtramaf=$_POST['idTramaf'];
        $tramaIntegrapf->updateTrama();

    }


    if(isset($_POST['idTramaql'])){


        $tramaIntegraql->idoperacion=$operacionesObj->idoperacion;
        $tramaIntegraql->idtramaql=$_POST['idTramaql'];
        $tramaIntegraql->updateTrama();

    }


    if(isset($_POST['idTramaty'])){

        $tramaIntegraty->idoperacion=$operacionesObj->idoperacion;
        $tramaIntegraty->idtrama=$_POST['idTramaty'];
        $tramaIntegraty->updateTrama();

    }

    if (isset($_POST['idSolRadar'])) {
        WebTrack::asignarOperacionId($_POST['idSolRadar'], $operacionesObj->idoperacion);
    }

    if (isset($_POST['pendienteInstacion'])) {

    }else{
        if ($_POST['lugarInstacion']=='Taller') {
            $objtablaP->idoperacion=$operacionesObj->idoperacion;
            $objtablaP->insertaridoperacion();
        }else{
            $objtablaP->idoperacion=$operacionesObj->idoperacion;
            $objtablaP->insertaridoperacionDoble();
        }
    }
}else{
    $operacionesObj->agregarIdOperacionesCOmercialesF();
}
    $marca_select = $_POST["servad"];
    foreach($marca_select as $marcas){
        $objServAdicionales->idser=$marcas;
        $objServAdicionales->idoperacion=$operacionesObj->idoperacion;
        $objServAdicionales->registrarOperacionServAdicional();

    }
                    $echo[]="Cliente agregado con exito";
                    }else{
                        $errores[]="Error al registrar datos de facturación";
                    }
            }else{
                    $errores[]="Se detectaron errores al momento de registrar Vehículo";
            }
        }



///////////////////////////TOE///

        }else{
        $errores[]="El formulario ya fue procesado, vericar.";
    }
    }

?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="../assets/css/sweetalert2.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/css2/style2.css">
<link rel="stylesheet" href="../assets/css2/style3.css">
   <style>
    .widget p {
        display: inline-block;
    }

    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }

    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;
        margin-top: 6px;
    }

    .reloj .caja-segundos {
        display: inline-block;
    }

    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }</style>
       <style>
    .widget p {
        display: inline-block;
    }

    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }

    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;
        margin-top: 6px;
    }

    .reloj .caja-segundos {
        display: inline-block;
    }

    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }</style>
    <body>

    <style>
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
}
.pagination1{margin:0;padding:0;}
.pagination1 li{
    display: inline;
    padding: 6px 10px 6px 10px;
    border: 1px solid #ddd;
    margin-right: -1px;
    font: 15px/20px Arial, Helvetica, sans-serif;
    background: #FFFFFF;
    box-shadow: inset 1px 1px 5px #F4F4F4;
}
.pagination1 li a{
    text-decoration:none;
    color: rgb(89, 141, 235);
}
.pagination1 li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination1 li.last {
    border-radius: 0px 5px 5px 0px;
}
.pagination1 li:hover{
    background: #CFF;
}
.pagination1 li.active{
    background: #F0F0F0;
    color: #333;
}
</style>
<div class="se-pre-con"></div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
    <body>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <?php include("../includes/menuIzquierdo.php") ?>
               </div>
           </nav>

           <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>
                </nav>
            </div>
               <br>
<div role="tablist">
    <input type="radio" title="operaciones" name="tpoperacion" class='tpoperacion' id="operaciones" role="tab" aria-controls="operaciones-panel" checked value="Operaciones" class="tpoperacion ope" />
    <input type="radio" title="integracionpacifico" name="tpoperacion"  class='tpoperacion' id="integracionpacifico" role="tab" aria-controls="integracionpacifico-panel" value="integracionpacifico" class="tpoperacion intp" />
    <input type="radio" title="integracionwebtrack" name="tpoperacion"  class='tpoperacion' id="integracionwebtrack" role="tab" aria-controls="integracionwebtrack-panel" value="integracionwebtrack" class="tpoperacion intw" />
    <input type="radio" title="integracionplanfull" name="tpoperacion"  class='tpoperacion' id="integracionplanfull" role="tab" aria-controls="integracionplanfull-panel" value="integracionplanfull" class="tpoperacion intpf" />
    <input type="radio" title="integracionqualitas" name="tpoperacion"  class='tpoperacion' id="integracionqualitas" role="tab" aria-controls="integracionqualitas-panel" value="integracionqualitas" class="tpoperacion intql" />
    <input type="radio" title="integracionToyota" name="tpoperacion"  class='tpoperacion' id="integracionToyota" role="tab" aria-controls="integracionToyota-panel" value="integracionToyota" class="tpoperacion intty" />
    <div class="tabs" aria-hidden="true">
        <label for="operaciones" id="operaciones-tab" class="tab"><h4>Operaciones</h4></label>
        <label for="integracionpacifico" id="integracionpacifico-tab" class="tab"><h4>Integración Pacífico
        <?php if ($datosAINT): ?>
        &nbsp; &nbsp;<span class="label label-danger pull-right bg-danger">  <?= count($datosAINT) ?>  Nuevos </span></h4></label>
        <?php endif; ?>
        <label for="integracionwebtrack" id="integracionwebtrack-tab" class="tab"><h4>GPS Radar-Pro</h4></label>
        <label for="integracionplanfull" id="integracionplanfull-tab" class="tab"><h4>Integración Plan Full</h4></label>
        <label for="integracionqualitas" id="integracionqualitas-tab" class="tab"><h4>Integración Qualitas</h4></label>
        <label for="integracionToyota" id="integracionToyota-tab" class="tab"><h4>Integración Toyota</h4></label>
    </div>
    <div class="panels">
        <ul id="operaciones-panel" class="panel active" role="tabpanel" aria-labelledby="operaciones-tab" style="padding:0px">
            <div class="wrapper wrapper-content">
                <div class="row ibox float-e-margins ibox-content ">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="chasis" class="">Chasis</label>
                                        <input type="text" style="border:none;" class="col-md-6 form-control border-bottom" placeholder="CHASIS" id="chasis" name="chasis" >
                                    </div>
                                    <div class="col-md-4">
                                        <label for="" class=""></label>
                                        <input type="hidden" name="token" value="<?php echo Token::generar() ?>">
                                        <div class="btn btn-primary" name="subir" data-toggle="modal" data-target="#bateriasOModal">Verificar</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <form action="exel/gestionContabilidad" method="POST">
                                        <?php if ($obMenu->administrarComercial=='si'): ?>
                                            <div class="col-md-4 esconder">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label for="convenio" class=""></label>
                                                        <select style="border: none;"  name="convenio" class="cd-selec form-control border-bottom" required id="convenio">
                                                            <option value="<?= $o->idusuario ?>"><?= $o->usuarioNombre." ".$o->usuarioApellidoPaterno;  ?></option>
                                                            <?php if ($obMenu->administrarComercial=="si"){ ?>
                                                                <?php if ($data=Usuario::obtenerEJecutiovsDeOperaciones()): ?>
                                                                    <?php foreach ($data as $var): ?>
                                                                        <option value="<?= $var->idusuario ?>"><?= $var->usuarioNombre." ".$var->usuarioApellidoPaterno; ?></option>
                                                                    <?php endforeach ?>
                                                                <?php endif ?>
                                                            <?php }  ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="col-md-4 esconder">
                                                            <label for="estado">Estado</label>
                                                            <select style="border: none;"  name="estado" class="form-control border-bottom" required id="estado">
                                                                <option value="333" selected="" >Todos</option>
                                                                <option value="0">Pendiente fecha</option>
                                                                <option value="14">Pendiente Orte</option>
                                                                <option value="36">Listo CEM</option>
                                                                <option value="15">Instalado</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php else: ?>
                                            <div class="col-md-4 esconder">
                                                <label for="estado">Estado</label>
                                                <select style="border: none;"  name="estado" class="form-control border-bottom" required id="estado">
                                                    <option value="333" selected="" >Todos</option>
                                                    <option value="0">Pendiente fecha</option>
                                                    <option value="14">Pendiente Orte</option>
                                                    <option value="36">Listo CEM</option>
                                                    <option value="15">Instalado</option>
                                                </select>
                                            </div>
                                        <?php endif ?>
                                        <div class="col-lg-6">
                                            <label for="rango" class="">Seleccione rango fecha *</label>
                                            <input style="border: none;"  class="cd-select form-control border-bottom" required  type="text" placeholder="Seleccione Rango de fecha" class="form-control" value="" name="fecha" id="rango" autocomplete="off">
                                        </div>
                                        <div class="col-md-2" id="resultado1">
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="widget" style="line-height: 0px;
                           padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px">
                                    <div class="fecha">
                                        <p id="diaSemana" class="diaSemana">Martes</p>
                                        <p id="dia" class="dia">27</p>
                                        <p>de </p>
                                        <p id="mes" class="mes">Octubre</p>
                                        <p>del </p>
                                        <p id="year" class="year">2015</p>
                                    </div>
                                    <div class="reloj">
                                        <p id="horas" class="horas">11</p>
                                        <p>:</p>
                                        <p id="minutos" class="minutos">48</p>
                                        <p>:</p>
                                        <div class="caja-segundos">
                                            <p id="ampm" class="ampm">AM</p>
                                            <p id="segundos" class="segundos">12</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ibox-content" id="resultados">
                    <input type="text" class="form-control input-sm m-b-xs" id="filter"
                           placeholder="Buscar">
                    <div class="table-responsive">
                        <table class=" table  table-hover" >
                            <thead>
                            <tr>
                                <th style="width:50px">#</th>
                                <th>Tipo Servicio</th>
                                <th>Nombres</th>
                                <th>Placa</th>
                                <th>Fecha Enviada</th>
                                <th>Fecha Programada</th>
                                <th>Estado Vehículo</th>
                                <th>Estado Facturación</th>
                                <th>Orden</th>
                                <th>Campaña</th>
                                <th>Tipo Registro</th>
                                <th>Vendedor</th>
                                <th>Acción</th>
                            </tr>
                            </thead>
                            <tbody  id="resultado">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </ul>
        <ul id="integracionpacifico-panel" class="panel active" role="tabpanel" aria-labelledby="integracionpacifico-tab">
            <div class="wrapper wrapper-content">
                <div class="ibox-content" id="resultadosInt">
                    <input type="text" class="form-control input-sm m-b-xs" id="filterInt"
                           placeholder="Buscar">
                    <div class="table-responsive">
                        <table class=" table  table-hover" >
                            <thead>
                            <tr>
                                <th style="width:50px">#</th>
                                <th>Recibido</th>
                                <th>Cliente</th>
                                <th>Placa | Marca</th>
                                <th>Inicio Vigencia</th>
                                <th>Seguimiento</th>
                                <th>A cargo de: </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody  id="resultadoInt">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </ul>
        <ul id="integracionwebtrack-panel" class="panel active" role="tabpanel" aria-labelledby="integracionwebtrack-tab">
            <div class="wrapper wrapper-content">
                <div class="ibox-content" id="resultadosIntWeb">
                    <input type="text" class="form-control input-sm m-b-xs" id="filterInt"
                           placeholder="Buscar">
                    <div class="table-responsive">
                        <table class=" table  table-hover" >
                            <thead>
                            <tr>
                                <th style="width:50px">#</th>
                                <th>Recibido</th>
                                <th>DNI/RUC</th>
                                <th>Cliente</th>
                                <th>Direccion</th>
                                <th>A cargo de: </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody  id="resultadoIntWeb">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </ul>
        <ul id="integracionplanfull-panel" class="panel active" role="tabpanel" aria-labelledby="integracionplanfull-tab">
            <div class="wrapper wrapper-content">
                <div class="ibox-content" id="resultadosIntpf">
                    <input type="text" class="form-control input-sm m-b-xs" id="filterIntpf"
                           placeholder="Buscar">
                    <div class="table-responsive">
                        <table class=" table  table-hover" >
                            <thead>
                            <tr>
                                <th style="width:50px">#</th>
                                <th>Recibido</th>
                                <th>Cliente</th>
                                <th>Placa | Marca</th>
                                <th>Inicio Vigencia</th>
                                <th>Seguimiento</th>
                                <th>A cargo de: </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody  id="resultadoIntpf">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </ul>
        <ul id="integracionqualitas-panel" class="panel active" role="tabpanel" aria-labelledby="integracionqualitas-tab">
            <div class="wrapper wrapper-content">
                <div class="ibox-content" id="resultadosIntql">
                    <input type="text" class="form-control input-sm m-b-xs" id="filterIntql"
                           placeholder="Buscar">
                    <div class="table-responsive">
                        <table class=" table  table-hover" >
                            <thead>
                            <tr>
                                <th style="width:50px">#</th>
                                <th>Recibido</th>
                                <th>Cliente</th>
                                <th>Placa | Marca</th>
                                <th>Inicio Vigencia</th>
                                <th>Seguimiento</th>
                                <th>A cargo de: </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody  id="resultadoIntql">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </ul>
        <ul id="integracionToyota-panel" class="panel active" role="tabpanel" aria-labelledby="integracionToyota-tab">
            <div class="wrapper wrapper-content">
                <div class="ibox-content" id="resultadosIntty">
                    <input type="text" class="form-control input-sm m-b-xs" id="filterIntty"
                           placeholder="Buscar">
                    <div class="table-responsive">
                        <table class=" table  table-hover" >
                            <thead>
                            <tr>
                                <th style="width:50px">#</th>
                                <th>Tipo Registro</th>
                                <th>Recibido</th>
                                <th>Cliente</th>
                                <th>Placa | Chasis</th>
                                <th>Seguimiento</th>
                                <th>A cargo de: </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody  id="resultadoIntty">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </ul>
    </div>

</div>
<?php include("../includes/footer.php") ?>
</div>
</div>
</div>
<div class="modal inmodal" id="bateriasOModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" style="overflow-y: scroll;" >
    <div class="modal-dialog modal-lg" style="width:1000px">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 5px;padding-bottom: 2px">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title" id="modal-opecomerciales">Operaciones Comerciales</h4>
                <small class="font-bold"><div class="form-group ">
                </div></small>
            </div>
            <form  role="form" method="post" enctype="multipart/form-data" id="formOpe">

                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>
            </form>
        </div>
    </div>
</div>

<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/clipboard.min.js"></script>
<script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
<script src="../assets/calendario/dist/sweetalert.min.js"></script>
<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
    <!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
 <script src="../assets/js/jquery-confirm.min.js"></script>

<script src="../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../assets/build/jquery.datetimepicker.full.js"></script>
<script src="../assets/js/plugins/select2/select2.full.min.js"></script>
<script src="../assets/js/plugins/iCheck/icheck.min.js"></script>
 <?php include_once('../includes/script.php') ?>
<script>
  $("#formOpe").validate({  });
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
<?php endforeach ?>
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
<?php endif ?>
</script>
<script>
    function validar(){
        var lugarI=$( "input[name='tpoperacion']:checked" ).val();
        if (lugarI=='Operaciones') {
            $('.ope').prop('disabled', false);
            $('.intp').prop('disabled', true);
            $('.intw').prop('disabled', true);
            $('.intpf').prop('disabled', true);
            $('.intql').prop('disabled', true);
            $('.intty').prop('disabled', true);
        }else if (lugarI=='integracionpacifico') {
            $('.ope').prop('disabled', true);
            $('.intw').prop('disabled', true);
            $('.intp').prop('disabled', false);
            $('.intpf').prop('disabled', true);
            $('.intql').prop('disabled', true);
            $('.intty').prop('disabled', true);
        }else if (lugarI=='integracionwebtrack') {
            $('.ope').prop('disabled', true);
            $('.intw').prop('disabled', false);
            $('.intp').prop('disabled', true);
            $('.intpf').prop('disabled', true);
            $('.intql').prop('disabled', true);
            $('.intty').prop('disabled', true);
        }else if (lugarI=='integracionplanfull') {
            $('.ope').prop('disabled', true);
            $('.intw').prop('disabled', true);
            $('.intp').prop('disabled', true);
            $('.intpf').prop('disabled', false);
            $('.intql').prop('disabled', true);
            $('.intty').prop('disabled', true);
        }else if (lugarI=='integracionqualitas') {
            $('.ope').prop('disabled', true);
            $('.intw').prop('disabled', true);
            $('.intp').prop('disabled', true);
            $('.intpf').prop('disabled', true);
            $('.intql').prop('disabled', false);
            $('.intty').prop('disabled', true);
        }else if (lugarI=='integracionToyota') {
            $('.ope').prop('disabled', true);
            $('.intw').prop('disabled', true);
            $('.intp').prop('disabled', true);
            $('.intpf').prop('disabled', true);
            $('.intql').prop('disabled', true);
            $('.intty').prop('disabled', false);
        }
    }
    $(document).ready(function(){
        $( "input[name='tpoperacion']" ).on( "click",function(){
          /*  $('#programacionHtml').html('');
            $('.horaVer').val('');
            $('.fecha').val('');*/
            validar();
        });
        validar();
    });


    $('.tpoperacion').on("change",function(){
        $('.ope').prop('disabled', false);
        $('.intp').prop('disabled', false);
        $('.intw').prop('disabled', false);
        $('.intpf').prop('disabled', false);
        $('.intql').prop('disabled', false);
        $('.intty').prop('disabled', false);
    })
</script>
<script>
$('#rango').daterangepicker({
    format: 'DD-MM-YYYY',
    endDate: moment(),
    showDropdowns: true,
    showWeekNumbers: true,
    timePicker: false,
    timePickerIncrement: 1,
    timePicker12Hour: false,
    ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
        'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Anterior Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    opens: 'right',
    drops: 'down',
    buttonClasses: ['btn', 'btn-sm'],
    applyClass: 'btn-primary',
    cancelClass: 'btn-default',
    separator: '/',
    locale: {
        applyLabel: 'Seleccionar',
        cancelLabel: 'Cancelar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Seleccionar Rango',
        daysOfWeek: ['Do', 'Lun', 'Mar', 'Mi', 'Ju', 'Vi','Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
    }
}, function(start, end, label) {
     $('#rango').focus();
});

var eventos={}
eventos.frejcar =function(){
    var page = $("#resultadoInt").attr("data-page");
    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionOperacionesCInt',
        data : {page:page},
        success : function(data)
        {
            $("#resultadoInt").html(data);
        }
    });
}
eventos.interval = setInterval(eventos.frejcar, 250000);

eventos.freshDataWebTrack =function(){
    var page = $("#resultadoIntWeb").attr("data-page");
    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionSolicitudesRadar',
        data : {page:page},
        success : function(data) {
            $("#resultadoIntWeb").html(data);
        }
    });
}
eventos.interval2 = setInterval(eventos.freshDataWebTrack, 250000);


eventos.freshDataPlanFull =function(){
    var page = $("#resultadoIntpf").attr("data-page");
    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionOperacionesCIntpf',
        data : {page:page},
        success : function(data) {
            $("#resultadoIntpf").html(data);
        }
    });
}
eventos.interval3 = setInterval(eventos.freshDataPlanFull, 250000);



eventos.freshDataQualitas =function(){
    var page = $("#resultadoIntql").attr("data-page");
    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionOperacionesCIntql',
        data : {page:page},
        success : function(data) {
            $("#resultadoIntql").html(data);
        }
    });
}
eventos.interval3 = setInterval(eventos.freshDataQualitas, 250000);


eventos.freshDataToyota =function(){
    var page = $("#resultadoIntty").attr("data-page");
    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionOperacionesCIntty',
        data : {page:page},
        success : function(data) {
            $("#resultadoIntty").html(data);
        }
    });
}
eventos.interval3 = setInterval(eventos.freshDataToyota, 250000);


$(document).ready(function(){

    var convenio=$("#convenio").val();
    var estado=$("#estado").val();
    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionOperacionesC',
        data : {convenio:convenio,estado},
        success : function(data)
        {
         $("#resultado").html(data);
    }
    });
    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionOperacionesCInt',
        data : {},
        success : function(data)
        {
            $("#resultadoInt").html(data);
        }
    });
    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionSolicitudesRadar',
        data : {},
        success : function(data) {
            //console.log("Data: ", data);
            $("#resultadoIntWeb").html(data);
        }
    });

    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionOperacionesCIntpf',
        data : {},
        success : function(data)
        {
            //console.log("Data: ", data);
            $("#resultadoIntpf").html(data);
        }
    });


    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionOperacionesCIntql',
        data : {},
        success : function(data)
        {
            //console.log("Data: ", data);
            $("#resultadoIntql").html(data);
        }
    });


    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionOperacionesCIntty',
        data : {},
        success : function(data)
        {
            //console.log("Data: ", data);
            $("#resultadoIntty").html(data);
        }
    });

    $( "#convenio" ).change(function() {
        var estado=$("#estado").val();
        var convenio = $(this).val();
        var fecha=$("#rango").val();
        var page = $('#resultado').attr("data-page");
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionOperacionesC',
        data : {convenio:convenio,page:page,fecha:fecha,estado},
        success : function(data)
        {
         $("#resultado").html(data);
        }
      });
    });

    $("#rango").bind("keyup focus",function()
    {
        var fecha=$(this).val();
        var page = $("#resultado").attr("data-page");
        var convenio=$("#convenio").val();
        $.ajax({
            type : 'POST',
            url  : 'pag/paginacionOperacionesC',
            data : {convenio:convenio,page:page,fecha:fecha,estado:estado},
            success : function(data)
            {
             $("#resultado").html(data);
            }
        });
    });
    $( "#rango" ).change(function() {
        var estado=$("#estado").val();
        var fecha=$(this).val();
        var page = $("#resultado").attr("data-page");
        var convenio=$("#convenio").val();
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionOperacionesC',
        data : {convenio:convenio,page:page,fecha:fecha,estado},
        success : function(data)
        {
         $("#resultado").html(data);
        }
        });
    });

    $( "#estado" ).change(function() {
        var estado=$(this).val();
        var fecha=$("#rango").val();
        var page = $("#resultado").attr("data-page");
        var convenio=$("#convenio").val();
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionOperacionesC',
        data : {convenio:convenio,page:page,fecha:fecha,estado:estado},
        success : function(data)
        {
         $("#resultado").html(data);
        }
        });
    });
    $("#resultado").on( "click", ".pagination1 a", function (e){
        e.preventDefault();
        var fecha=$("#rango").val();
        var estado=$("#estado").val();
        var convenio=$("#convenio").val();
        var page = $(this).attr("data-page");
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionOperacionesC',
        data : {convenio:convenio,page:page,fecha:fecha,estado},
        success : function(data)
        {
         $("#resultado").html(data);
        }
        });

    });

$("#filter").bind("keyup focus",function()
    {
        var datosS = $(this).val();
        var convenio=$("#convenio").val();
        var fecha=$("#rango").val();
        datosS = datosS.replace('<>/"', '');
        if(datosS.length >= 1)
            {
        $.ajax({
                type : 'POST',
                url  : 'pag/resultadoBusquedaOpeC',
                data :  {convenio:convenio,fecha:fecha,datos:datosS},
                success : function(data)
                {
                    $("#resultado").html(data);
                }
            });
            return false;
        }else{
            var convenio=$("#convenio").val();
            var fecha=$("#rango").val();
            var page = $("#resultado").attr("data-page");
            var estado=$("#estado").val();
            $.ajax({
            type : 'POST',
            url  : 'pag/paginacionOperacionesC',
            data : {convenio:convenio,page:page,fecha:fecha,estado},
            success : function(data)
            {
             $("#resultado").html(data);
            }
            });

        }
  });

  $("#filterInt").bind("keyup focus",function()
  {
    var datosS = $(this).val();
    datosS = datosS.replace('<>/"', '');
    if(datosS.length >= 1)
    {
        $.ajax({
            type : 'POST',
            url  : 'pag/resultadoBusquedaOpeCInt',
            data :  {datos:datosS},
            success : function(data)
            {
                $("#resultadoInt").html(data);
            }
        });
        return false;
    }else{
        var page = $("#resultadoInt").attr("data-page");
        $.ajax({
            type : 'POST',
            url  : 'pag/paginacionOperacionesCInt',
            data : {page:page},
            success : function(data)
            {
                $("#resultadoInt").html(data);
            }
        });
    }
  });


    $("#filterIntpf").bind("keyup focus",function()
    {
        var datosS = $(this).val();
        datosS = datosS.replace('<>/"', '');
        if(datosS.length >= 1)
        {
            $.ajax({
                type : 'POST',
                url  : 'pag/resultadoBusquedaOpeCIntpf',
                data :  {datos:datosS},
                success : function(data)
                {
                    $("#resultadoIntpf").html(data);
                }
            });
            return false;
        }else{
            var page = $("#resultadoIntpf").attr("data-page");
            $.ajax({
                type : 'POST',
                url  : 'pag/paginacionOperacionesCIntpf',
                data : {page:page},
                success : function(data)
                {
                    $("#resultadoIntpf").html(data);
                }
            });
        }
    });




    $("#filterIntql").bind("keyup focus",function()
    {
        var datosS = $(this).val();
        datosS = datosS.replace('<>/"', '');
        if(datosS.length >= 1)
        {
            $.ajax({
                type : 'POST',
                url  : 'pag/resultadoBusquedaOpeCIntql',
                data :  {datos:datosS},
                success : function(data)
                {
                    $("#resultadoIntql").html(data);
                }
            });
            return false;
        }else{
            var page = $("#resultadoIntql").attr("data-page");
            $.ajax({
                type : 'POST',
                url  : 'pag/paginacionOperacionesCIntql',
                data : {page:page},
                success : function(data)
                {
                    $("#resultadoIntql").html(data);
                }
            });
        }
    });



    $("#filterIntty").bind("keyup focus",function()
    {
        var datosS = $(this).val();
        datosS = datosS.replace('<>/"', '');
        if(datosS.length >= 1)
        {
            $.ajax({
                type : 'POST',
                url  : 'pag/resultadoBusquedaOpeCIntty',
                data :  {datos:datosS},
                success : function(data)
                {
                    $("#resultadoIntty").html(data);
                }
            });
            return false;
        }else{
            var page = $("#resultadoIntql").attr("data-page");
            $.ajax({
                type : 'POST',
                url  : 'pag/paginacionOperacionesCIntty',
                data : {page:page},
                success : function(data)
                {
                    $("#resultadoIntty").html(data);
                }
            });
        }
    });


$("#rango").bind("keyup focus",function(){
        var convenio = $("#convenio").val();
        var fecha = $(this).val();
        $.ajax({
          type : 'POST',
          url  : 'pag/opeCreporte',
          data : {fecha:fecha,convenio:convenio},
          success : function(data)
          {
           $("#resultado1").html(data);
         }
       });
        return false;
});

$("#convenio").change(function()
    {
        var convenio = $(this).val();
        var fecha = $("#rango").val();
        $.ajax({
          type : 'POST',
          url  : 'pag/opeCreporte',
          data : {fecha:fecha,convenio:convenio},
          success : function(data)
          {
           $("#resultado1").html(data);
         }
       });
        return false;
});
});

$('#bateriasOModal').on('show.bs.modal', function (e) {
    var lugarI = $("input[name='tpoperacion']:checked").val();
    var chasis = $("#chasis").val();
    if(lugarI=='integracionpacifico'){
        chasis='';
    }
    var idperacion = $(e.relatedTarget).data('id');
    var idInt = $(e.relatedTarget).data('idint');
    var idIntpf = $(e.relatedTarget).data('idintpf');
    var idIntql = $(e.relatedTarget).data('idintql');
    var idIntty = $(e.relatedTarget).data('idintty');
    var idSolRadar = $(e.relatedTarget).data('idsolradar');
    $('.modal-body').html("");
    if (chasis!='' && chasis!==undefined) {
        $("#modal-opecomerciales").html("Operaciones Comerciales");
            $.ajax({
                type : 'post',
                url : 'opeC/desdeOnixdataModal',
                data :  'chasis='+ chasis,
                success : function(data){
               //     console.log(data);
                $('.modal-body').html(data);
            }
        });
    }else if(idperacion!='' && idperacion!==undefined){
        $("#modal-opecomerciales").html("Operaciones Comerciales");
        $.ajax({
            type : 'post',
            url : 'opeC/desdeOnixdataModal',
            data :  'idperacion='+ idperacion,
            success : function(data){
            $('.modal-body').html(data);
            }
        });
    }else if(idInt!='' && idInt!==undefined){
        $("#modal-opecomerciales").html("Operaciones Comerciales");
        $.ajax({
            type : 'post',
            url : 'opeC/desdeOnixdataModal',
            data :  'idTrama='+ idInt,
            success : function(data){
                $('.modal-body').html(data);
            }
        });
    }else if(idSolRadar!='' && idSolRadar!==undefined){
        $("#modal-opecomerciales").html("GPS Radar-Pro");
        $.ajax({
            type : 'post',
            url : 'opeC/desdeOnixdataModal',
            data :  'idSolRadar='+ idSolRadar,
            success : function(data){
                $('.modal-body').html(data);
            }
        });
    }else if(idIntpf!='' && idIntpf!==undefined){
    $("#modal-opecomerciales").html("Plan Full");
    $.ajax({
        type : 'post',
        url : 'opeC/desdeOnixdataModal',
        data :  'idTramaf='+ idIntpf,
        success : function(data){
            $('.modal-body').html(data);
        }
    });
   }else if(idIntql!='' && idIntql!==undefined){
        $("#modal-opecomerciales").html("Qualitas");
        $.ajax({
            type : 'post',
            url : 'opeC/desdeOnixdataModal',
            data :  'idTramaql='+ idIntql,
            success : function(data){
                $('.modal-body').html(data);
            }
        });
    }else if(idIntty!='' && idIntty!==undefined){
        $("#modal-opecomerciales").html("Toyota");
        $.ajax({
            type : 'post',
            url : 'opeC/desdeOnixdataModal',
            data :  'idTramaty='+ idIntty,
            success : function(data){
                $('.modal-body').html(data);
            }
        });
    }
});




function limpiarChasis(){
    var chasis = $("#chasis").val('');
}

function limpiarTrama(){
    var idTrama = $("#idTrama").val('');
}

function seguir(){
    $('.showOPERA').show('slow');
    $('.showReg').hide();
}
// function validarlugarInstalacion(){}

      $('#mostrar').hide();
      $('#esconderestos').hide();
 function capacidad(cantidad){
   $('#agregarVehiculoBttn').prop('disabled', true);
   $('#mostrar').show();
  }
 function limpiarCapacidad(){
    $('#agregarVehiculoBttn').prop('disabled', false);
    $('#mostrar').hide();
  }
 $( ".nombreContactoo" ).focus(function() {
  esconder();
  });

(function(){
    var actualizarHora = function(){
        var fecha = new Date(),
        horas = fecha.getHours(),
        ampm,
        minutos = fecha.getMinutes(),
        segundos = fecha.getSeconds(),
        diaSemana = fecha.getDay(),
        dia = fecha.getDate(),
        mes = fecha.getMonth(),
        year = fecha.getFullYear();
        var pHoras = document.getElementById('horas'),
        pAMPM = document.getElementById('ampm'),
        pMinutos = document.getElementById('minutos'),
        pSegundos = document.getElementById('segundos'),
        pDiaSemana = document.getElementById('diaSemana'),
        pDia = document.getElementById('dia'),
        pMes = document.getElementById('mes'),
        pYear = document.getElementById('year');
        var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        pDiaSemana.textContent = semana[diaSemana];
        pDia.textContent = dia;
        var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        pMes.textContent = meses[mes];
        pYear.textContent = year;
        if (horas >= 12) {
            horas = horas - 12;
            ampm = 'PM';
        } else {
            ampm = 'AM';
        }
        if (horas == 0 ){
            horas = 12;
        }
        pHoras.textContent = horas;
        pAMPM.textContent = ampm;
        if (minutos < 10){ minutos = "0" + minutos; }
        if (segundos < 10){ segundos = "0" + segundos; }
        pMinutos.textContent = minutos;
        pSegundos.textContent = segundos;
    };


    actualizarHora();
    var intervalo = setInterval(actualizarHora, 1000);
}())
</script>
  <script>
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
<?php else: ?>
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?>
<?php endif ?>

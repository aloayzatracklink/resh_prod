<?php
    if (isset($_POST['idTrama'])) {
    include_once("../../includes/init.php");
    $idTrama=limpiar(trim($_POST['idTrama']));
    $varTI=TramaIntegracionTY::obtenerTramaIntegracion($idTrama,'');

?>
    <div class="showOPERA">
        <div class="well" style="padding-top: 11px;padding-bottom: 10px; ">
            <div class='row' >
                <div class='form-group col-md-12'>
                    <div class='checkbox-primary'>
                        <input type="hidden" name="idTrama" id="idTrama" style="display: none" value="<?=$varTI->idtrama ?>">
                        <input type="hidden" name="tipoTransaccion" id="tipoTransaccion" style="display: none" value="<?=$varTI->tipo_transaccion ?>">
                        <input type="hidden" name="idTrama_vinculacion" id="idTrama_vinculacion" style="display: none" value="<?=$varTI->id_trama_vinculacion ?>">
                        <input type="hidden" name="idOperacion" id="idOperacion" style="display: none" value="<?= $varTI->idoperacion ?>">
                        <input type="hidden" name="chasis" id="chasis" style="display: none" value="<?= $varTI->chasis ?>">
                        <input id="chkSolRech" type="radio"  checked name="anulacion" value="rechazado"/>
                        <label class="form-check-label" for='chkSolRech'> solicitud ha sido rechazado </label>
                    </div>
                    <div class="checkbox-warning form-check">
                        <input id="chkSolCanc"  type="radio"   name="anulacion" value="cancelado"/>
                        <label class="form-check-label"  for="chkSolCanc"> solicitud ha sido cancelada a solicitud del usuario </label>
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='form-group col-md-12'>
                    <h3>Motivo</h3>
                    <input type='text' required name='motivoAnulacion' style="width: 100%">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id='canpemodalplkm' class="btn btn-white" data-dismiss="modal">Cerrar</button>
            <button type="submit" name="guardarAnulaToyota" id="guardarAnulaToyota" class="btn btn-primary" value="<?php echo Token::generar() ?>">Guardar</button>
        </div>
    </div>


<?php
}
?>

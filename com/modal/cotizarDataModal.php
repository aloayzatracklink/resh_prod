<?php 
if ($_POST['rowid']) {
	include_once("../../includes/init.php");
	$cotizacionId=htmlspecialchars(htmlentities($_POST['rowid']));
	$cotizarObj=Cotizar::obtenerCotizacionPorId($cotizacionId);
	$clienteObjModal=new Cliente;
	?>
	 <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>

	<div class="well" style="padding-top: 11px;
	padding-bottom: 10px;">
	<div class="row">
		<div class="form-group col-md-4 text-primary text-capitalize">
			<?php echo $retVal = ($cotizarObj->tipoCliente=="Natural") ? "Nombres: " : "Razon social: " ; ?> <?php echo $retVal = ($cotizarObj->tipoCliente=="Natural") ? $cotizarObj->nombre." ".$cotizarObj->apellidos : $cotizarObj->razonSocial ; ?>

		</div>
		<div class="form-group col-md-4">
			Contacto: <?php echo $cotizarObj->contacto ?>
		</div>  
		<div class="form-group col-md-4">
			Estado: <?php echo obtenerEstado($cotizarObj->estado) ?>
		</div>

	</div> 
	<div class="row">

		<div class="form-group col-md-4">
			Estado de Pago: <?php echo obtenerEstado($cotizarObj->estadoPago) ?>
		</div>  
		<div class="form-group col-md-4">
			Plan: <?php echo $cotizarObj->plan ?>
		</div>
		<div class="form-group col-md-4">
			Fecha Creacion: <?php echo hace($cotizarObj->fecha) ?>
		</div>

	</div>
	<div class="row">

		<div class="form-group col-md-4">
			Costo: <?= $cotizarObj->costo ?>
		</div>  
		<div class="form-group col-md-4">
			Cantidad de vehículos: <?= $cotizarObj->cantidad ?>
		</div>
		<div class="form-group col-md-4">
			Creado por: <?php $usuario=Usuario::obtenerUsuario($cotizarObj->idCreador);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno;echo "&nbsp;"; echo $usuario->usuarioApellidoPaterno; ?>
		</div>

	</div>
	<div class="row">
		<div class="form-group col-md-12">
			Observaciones <?= $cotizarObj->observaciones ?>
		</div>
	</div>

	<?php if (!empty($cotizarObj->voucher)): ?>
		<div class="row">
			<div class="col-md-12 text-center">
			 <?php $voucherdir="../../public/vouchers/".$cotizarObj->voucher; ?>
				<img alt="<?= $retVal = (is_file($voucherdir)) ? $voucherdir : 'http://192.168.2.20/public/vouchers/'.$cotizarObj->voucher ; ?>" src="<?= $retVal = (is_file($voucherdir)) ? $voucherdir : 'http://192.168.2.20/public/vouchers/'.$cotizarObj->voucher ; ?>" width="400"   >
				<input type="hidden" name="voucher" value="<?php echo $cotizarObj->voucher ?>" >
				<?php 
				if (!is_file($voucherdir)) {
					$voucherOtherServerDir='http://192.168.2.20/public/vouchers/'.$cotizarObj->voucher;

					$voucherNombreArchivoYdir='../../public/vouchers/'.$cotizarObj->voucher;
					copy($voucherOtherServerDir, $voucherNombreArchivoYdir);
				}
				  ?>
			</div>
		</div>
	<?php endif ?>
</div>
<?php 
if ($cotizarObj->estado==17) {
	?>
	<?php if ($cotizarObj->tipoCliente=="Natural"): ?>
		
		<div class="row">
			<div class="form-group col-md-3">
				<h4>Modificar Dni</h4>
				<label for="trabajo" class="sr-only">DNI</label>
				<input style="border: none;"  type="text" placeholder="Dni" id="dni" name="dniEditado" class="dni border-bottom form-control" required value="<?php echo  $cotizarObj->dni  ?>">
			</div>
			<div class="form-group col-md-5">
				<h4>Razon de cierre</h4>
				<label for="trabajo" class="sr-only">Razon de Eliminar</label>
				<input style="border: none;"  type="text" placeholder="Razon de Eliminar" id="" name="razonEliminar" class=" border-bottom form-control"  >
			</div>
			<input type="hidden" name="cotizacionidEditar" value="<?php echo $cotizarObj->cotizacionid ?>" >
			<div class="form-group col-md-4">
				<h4>Acción</h4>
				<label for="trabajo" class="sr-only">Acción</label>
				<button class="btn btn-danger col-md-12" name="cambiarEstadoCotizacion">Eliminar</button>
			</div>
		</div>
<hr>
			<input type="hidden" name="nombre" value="<?php echo $cotizarObj->nombre ?>" >
			<input type="hidden" name="apellidos" value="<?php echo $cotizarObj->apellidos ?>" >
		<div class="row">
			<div class="form-group col-md-5">
				<h4>Correo</h4>
				<label for="trabajo" class="sr-only">Correo</label>
				<input style="border: none;"  type="text" placeholder="Correo" id="" name="correo" class=" border-bottom form-control" value="<?= $cotizarObj->correo  ?>" >
			</div>
			<div class="form-group col-md-4">
				<h4>Acción</h4>
				<label for="trabajo" class="sr-only">Acción</label>
				<button class="btn btn-primary" name="enviarCorreo">Re enviar correo</button>
			</div>
		</div>
	<?php else: ?>		 
		<div class="row">
			<div class="form-group col-md-3">
				<h4>Modificar Ruc</h4>
				<label for="trabajo" class="sr-only">Ruc</label>
				<input style="border: none;"  type="text" placeholder="Ruc" id="ruc" name="rucEditado" class="dni border-bottom form-control" required value="<?php echo  $cotizarObj->ruc  ?>">
			</div>
			<div class="form-group col-md-5">
				<h4>Razon de cierre</h4>
				<label for="trabajo" class="sr-only">Razon de Eliminar</label>
				<input style="border: none;"  type="text" placeholder="Razon de Eliminar" id="" name="razonEliminar" class=" border-bottom form-control"  >
			</div>
			<div class="form-group col-md-4">
				<h4>Acción</h4>
				<label for="trabajo" class="sr-only">Acción</label>
				<button class="btn btn-danger" name="cambiarEstadoCotizacion">Eliminar</button>
			</div>


			<input type="hidden" name="cotizacionidEditar" value="<?php echo $cotizarObj->cotizacionid ?>" >

		</div>
		<hr>
			<input type="hidden" name="razonSocial" value="<?php echo $cotizarObj->razonSocial ?>" >
		<div class="row">
			<div class="form-group col-md-5">
				<h4>Correo</h4>
				<label for="trabajo" class="sr-only">Correo</label>
				<input style="border: none;"  type="text" placeholder="Correo" id="" name="correo" class=" border-bottom form-control" value="<?= $cotizarObj->correo  ?>" >
			</div>
			<div class="form-group col-md-4">
				<h4>Acción</h4>
				<label for="trabajo" class="sr-only">Acción</label>
				<button class="btn btn-primary" name="enviarCorreo">Re enviar correo</button>
			</div>
		</div>
	<?php endif ?>



	<?php
}else if($cotizarObj->estado==23){
	echo "estado 23";
}else if($cotizarObj->estado==20){	
			// No importa si es que cliente con ruc s
	$clienteObjTemporal=Cliente::obtenerDatosDeClienteDesdeCotizacion($cotizacionId);
	?>

	<h1 class="text-center">Confirmar el registro.</h1>

	<input type="hidden" name="cotizacionid" value="<?php echo $cotizarObj->cotizacionid ?>" >
	<input type="hidden" name="plan" value="<?php echo $cotizarObj->plan ?>" >
	<div class="row">
		<div class="col-md-4">
			<strong>Formulario creado el:</strong><?php echo hace($clienteObjTemporal->fechaCreada) ?><br>
			<input type="hidden" name="fechaCreada" value="<?php echo $clienteObjTemporal->fechaCreada ?>">
		</div>
		<div class="col-md-4">
			<strong>Cantidad de vehiculos asignado: </strong><?php echo $cotizarObj->cantidad ?><br>
			<input type="hidden" name="cantidad" value="<?php echo $cotizarObj->cantidad ?>">
			</div
			>		<div class="col-md-4">
			<strong>Tiempo de contrato: </strong><?php echo $cotizarObj->tiempoContrato ?><br>
			<input type="hidden" name="tiempoContrato" value="<?php echo $cotizarObj->tiempoContrato ?>">
			<input type="hidden" name="costo" value="<?php echo $cotizarObj->costo ?>">
		</div>
	</div> 
	<div class="row">
		<div class="col-md-8"><strong>Observaciones: </strong><p><?php echo $cotizarObj->observaciones ?></p></div>
		<input type="hidden" name="observaciones" value="<?php echo $cotizarObj->observaciones ?>">
		<div class="col-md-4">
			<label for=""> Costo:</label> <?php echo $cotizarObj->costo ?>
		</div>
	</div>
	<br>  
	<div class="row">
		<div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
			<input type="hidden" name="tipoCliente" value="<?php echo $clienteObjTemporal->tipoCliente ?>">
			<?php if ($clienteObjTemporal->tipoCliente=="Natural"): ?>
				
				<input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dniC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObjTemporal->dni ?>">
				<!-- <input type="hidden" name="dni" value="<?php echo $clienteObjTemporal->dni ?>"> -->
			<?php else: ?>
				<input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObjTemporal->ruc ?>">
				<!-- <input type="hidden" name="ruc" value="<?php echo $clienteObjTemporal->ruc ?>"> -->
			<?php endif ?>	
		</div>

		<div class="col-md-9">
			<?php if ($clienteObjTemporal->tipoCliente=="Natural"): ?>
				<div class="row">
				<div class="col-md-4 form-group">
					<label for="nombrecl">Nombre: </label>
					<input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObjTemporal->nombre ?>">
				</div>
				<div class="col-md-8 ">
					<label for="apellidosC">Apellidos</label>
					<input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObjTemporal->apellidosC ?>">
				</div>
				</div>
			<?php else: ?>
				<label for="razonSocial"> Razon Social: </label> 
				<input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObjTemporal->razonSocial ?>">				
			<?php endif ?>
		</div>
		

	</div>	
	<?php 
		$cliobj=new Cliente;
	  $depatamentoObj=$cliobj->obtenerDepartamentoPorId($clienteObjTemporal->departamento); 
  $provinciaObj=$cliobj->otenerProvinciaPorId($clienteObjTemporal->provincia); 
  $distritoObj=$cliobj->obtenerDistritoPorId($clienteObjTemporal->distrito); 
 

	 ?>	
	<?php if ($clienteObjTemporal->tipoCliente=="Natural"): ?>
		<div class="row">
			<div class="form-group col-lg-4">
				<label for="departamento1" class="sr-only">Departamento</label>
				<select  style="border: none;" class="departamento border-bottom form-control text-capitalize" required name="departamento" id="departamento1">
					<option class="text-capitalize" value="<?php echo $depatamentoObj->iddepartamento ?>" selected ><?php echo $depatamentoObj->departamento ?></option>
					<?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
					<?php if ($objDepartamento): ?>
						<?php foreach ($objDepartamento as $var): ?>
							<option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
						<?php endforeach ?>
					<?php else: ?>
					<?php endif ?>
				</select>
			</div>
			<div class="form-group col-lg-4" >
				<label for="corredor" class="sr-only">Provincia</label>
				<select  style="border: none;" class="provincia1 border-bottom form-control text-capitalize" required name="provincia"  >
				<option class="text-capitalize" value="<?php echo $provinciaObj->idprovincia ?>"  selected ><?php echo $provinciaObj->provincia ?></option>
				</select>
			</div>

			<div class="form-group col-lg-4" >
					<label for="distrito" class="sr-only">Distrito</label>
					<select  style="border: none;" class="distritos1 border-bottom form-control text-capitalize" required name="distrito"  >
					<option class="text-capitalize" value="<?php echo $distritoObj->iddistrito ?>"  selected ><?php echo $distritoObj->distrito ?></option>
					</select>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<label for="direccion">Direccion: </label>
				<input style="border: none;" type="text" placeholder="Direccion" id="direccion" name="direccion" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObjTemporal->direccion ?>">	
			</div>
			<div class="col-md-6">
				<label for="referencia">Referencia: </label>
				<input style="border: none;" type="text" placeholder="Referencia:" id="referencia" name="referencia" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObjTemporal->referencia ?>">	
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<label for="fechaNacimiento">Fecha de nacimiento: </label> 
				<input style="border: none;" type="text" placeholder="Fecha de nacimiento:" id="fechaNacimiento" name="fechaNacimiento" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->fechaNacimiento ?>">
				</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<label for="telefono">Celular: </label>
			<input style="border: none;" type="text" placeholder="Celular:" id="telefono" name="telefono" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObjTemporal->telefono ?>">
		</div>
		<div class="col-md-3">
			<label for="telefono1">Telefono : </label>
			<input style="border: none;" type="text" placeholder="Telefono :" id="telefono1" name="telefono1" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->telefono1 ?>">
		</div>
		<div class="col-md-3">
			<label for="correo">Correo: </label>
			<input style="border: none;" type="text" placeholder="Correo:" id="correo" name="correo" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObjTemporal->correo ?>">	
		</div>
		<div class="col-md-3">
			<label for="correo1">Correo alternativo: </label>
			<input style="border: none;" type="text" placeholder="Correo alternativo:" id="correo1" name="correo1" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->correo1 ?>">	
		</div>
	</div>
<?php else: ?>
<?php 	
	$depatamentoObjL=$cliobj->obtenerDepartamentoPorId($clienteObjTemporal->departamentoL); 
  	$provinciaObjL=$cliobj->otenerProvinciaPorId($clienteObjTemporal->provinciaL); 
  	$distritoObjL=$cliobj->obtenerDistritoPorId($clienteObjTemporal->distritoL); 
  	?>
	<div class="row">	
		<div class="form-group col-lg-4">
				<label for="departamento1" class="">Departamento</label>
				<select  style="border: none;" class="departamento2 border-bottom form-control text-capitalize" required name="departamentoL" id="departamento1">
					<option class="text-capitalize" value="<?php echo $depatamentoObjL->iddepartamento ?>"  selected ><?php echo $depatamentoObjL->departamento ?></option>
					<?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
					<?php if ($objDepartamento): ?>
						<?php foreach ($objDepartamento as $var): ?>
							<option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
						<?php endforeach ?>
					<?php else: ?>
					<?php endif ?>
				</select>
			</div>
			<div class="form-group col-lg-4" >
				<label for="corredor" class="">Provincia</label>
				<select  style="border: none;" class="provincia2 border-bottom form-control text-capitalize" required name="provinciaL"  >
				<option class="text-capitalize" value="<?php echo $provinciaObjL->idprovincia ?>"  selected ><?php echo $provinciaObjL->provincia ?></option>
				</select>
			</div>

			<div class="form-group col-lg-4" >
					<label for="distrito" class="">Distrito</label>
					<select  style="border: none;" class="distritos2 border-bottom form-control text-capitalize" required name="distritoL"  >
					<option class="text-capitalize" value="<?php echo $distritoObjL->iddistrito ?>"  selected ><?php echo $distritoObjL->distrito ?></option>
					</select>
			</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<label for="direccionL">Direccion: </label>
			<input style="border: none;" type="text" placeholder="Direccion: " id="direccionL" name="direccionL" class="border-bottom form-control x2 text-uppercase" required  value="<?php echo $clienteObjTemporal->direccionL ?>" >
		</div>

	</div>
	<div class="row">
		<div class="col-md-3">
			<label for="telefono">Celular: </label>
			<input style="border: none;" type="text" placeholder="Celular: " id="telefono" name="telefono" class="border-bottom form-control x2 text-uppercase" required=""  value="<?php echo $clienteObjTemporal->telefono ?>">
		</div>
		<div class="col-md-3">
			<label for="telefono1">Telefono : </label>
			<input style="border: none;" type="text" placeholder="Telefono: " id="telefono1" name="telefono1" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->telefono1 ?>">
		</div>
		<div class="col-md-3">
			<label for="correo">Correo: </label>
			<input style="border: none;" type="text" placeholder="Correo: " id="correo" name="correo" class="border-bottom form-control x2 text-uppercase" required="" value="<?php echo $clienteObjTemporal->correo ?>">
		</div>
		<div class="col-md-3">
			<label for="correo1">Correo alternativo: </label>
			<input style="border: none;" type="text" placeholder="Correo alternativo: " id="correo1" name="correo1" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->correo1 ?>">
		</div>
	</div>
<?php endif ?>

<?php if ($clienteObjTemporal->tipoCliente!="Natural"): ?>
	
	<hr>
	<div class="row">
		<div class="row">
			<div class="col-md-12">
				<h4>Representante legal </h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<label for="dni">DNI: </label>
				<input style="border: none;" type="text" placeholder="DNI: " id="dni" name="dniC" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->dni ?>">
			</div>
			<div class="col-md-4">
				<label for="nombre">Nombres: </label>
				<input style="border: none;" type="text" placeholder="Nombres: " id="nombre" name="nombre" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->nombre ?>">
			</div>
			<div class="col-md-4">
				<label for="apellidosC">Apellidos </label>
				<input style="border: none;" type="text" placeholder="Nombres: " id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->apellidosC ?>">
			</div>
		</div><div class="row">
			<div class="col-md-12">
				<label for="fechaNacimiento">Fecha de nacimiento: </label> 
				<input style="border: none;" type="text" placeholder="Fecha de nacimiento: " id="fechaNacimiento" name="fechaNacimiento" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->fechaNacimiento ?>">
				<input type="hidden" name="fechaNacimiento" value="<?php echo $clienteObjTemporal->fechaNacimiento ?>"> 
			</div>
		</div> 
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="form-group col-lg-4">
				<label for="departamento1" class="">Departamento</label>
				<select  style="border: none;" class="departamento border-bottom form-control text-capitalize" required name="departamento" id="departamento1">
					<option class="text-capitalize" value="<?php echo $depatamentoObj->iddepartamento ?>"  selected ><?php echo $depatamentoObj->departamento ?></option>
					<?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
					<?php if ($objDepartamento): ?>
						<?php foreach ($objDepartamento as $var): ?>
							<option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
						<?php endforeach ?>
					<?php else: ?>
					<?php endif ?>
				</select>
			</div>
			<div class="form-group col-lg-4" >
				<label for="corredor" class="">Provincia</label>
				<select  style="border: none;" class="provincia1 border-bottom form-control text-capitalize" required name="provincia"  >
				<option class="text-capitalize" value="<?php echo $provinciaObj->idprovincia ?>"  selected ><?php echo $provinciaObj->provincia ?></option>
				</select>
			</div>

			<div class="form-group col-lg-4" >
					<label for="distrito" class="">Distrito</label>
					<select  style="border: none;" class="distritos1 border-bottom form-control text-capitalize" required name="distrito"  >
					<option class="text-capitalize" value="<?php echo $distritoObj->iddistrito ?>"  selected ><?php echo $distritoObj->distrito ?></option>
					</select>
			</div>
				</div>
							
			</div>
			<div class="row">
				<div class="col-md-6">
					<label for="direccion">Direccion: </label>
					<input style="border: none;" type="text" placeholder="Direccion:" id="direccion" name="direccion" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->direccion ?>">
					
				</div>
				<div class="col-md-6">
					<label for="referencia">Referencia: </label>
					<input style="border: none;" type="text" placeholder="Referencia:" id="referencia" name="referencia" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->referencia ?>">
				</div> 

			</div>
			<div class="row">
				<div class="col-md-3">
					<label for="telefono2">Celular: </label>
					<input style="border: none;" type="text" placeholder="Celular :" id="telefono2" name="telefono2" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->telefono2 ?>">
					
				</div>
				<div class="col-md-3">
					<label for="telefono3">Telefono : </label>
					<input style="border: none;" type="text" placeholder="Telefono:" id="telefono3" name="telefono3" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->telefono3 ?>">
					
				</div>
				<div class="col-md-3">
					<label for="correo2">Correo: </label>
					<input style="border: none;" type="text" placeholder="Correo:" id="correo2" name="correo2" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->correo2 ?>">
					
				</div>
				<div class="col-md-3">
					<label for="correo3">Correo alternativo: </label>
					<input style="border: none;" type="text" placeholder="Correo alternativo:" id="correo3" name="correo3" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->correo3 ?>">
					
				</div>
			</div>


		</div>
		
	</div>
<?php endif ?>


<hr>
<div class="row">
	<h3>&nbsp;&nbsp;&nbsp;Datos de facturación:</h3>
	<?php if ($clienteObjTemporal->tipoDocumentoFacturacion!="Boleta"): ?>
		<input type="hidden" name="tipoDocumentoFacturacion" value="<?php echo $clienteObjTemporal->tipoDocumentoFacturacion ?>">
		<h4>&nbsp;&nbsp;&nbsp;<?php echo $clienteObjTemporal->tipoDocumentoFacturacion ?>: </h4>
		<div class="row">
				<div class="col-md-4">
					<label for="rucFacturacion">Ruc Facturacion: </label>
					<input style="border: none;" type="text" placeholder="Ruc Facturacion:" id="rucFacturacion" name="rucFacturacion" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->rucFacturacion ?>">
					
				</div>
				<div class="col-md-5">
					<label for="razonSocialFacturacion">Razon Social Facturacion: </label>
					<input style="border: none;" type="text" placeholder="Razon Social Facturacion:" id="razonSocialFacturacion" name="razonSocialFacturacion" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->razonSocialFacturacion ?>">
				</div> 
				<div class="col-md-3">
					<label for="telefonoFacturacion">Telefono facturación: </label>
					<input style="border: none;" type="text" placeholder="Telefono facturación:" id="telefonoFacturacion" name="telefonoFacturacion" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->telefonoFacturacion ?>">
				</div> 

			</div>
		<div class="row">
			<div class="col-md-12">
				<label for="direccionFiscalFacturacion">Direccion fiscal: </label>
				<input style="border: none;" type="text" placeholder="Direccion Fiscal:" id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->direccionFiscalFacturacion ?>">
				</div>
		</div>
		<?php if ($clienteObjTemporal->entregaEnInstalacion=='si'): ?>
		<input type="hidden" name="entregaEnInstalacion" value="si">
		<h3 class="text-center">El cliente optó por recibir la factura al momento de la instalación</h3>
		<?php else: ?>			
			<div class="row">
				<div class="col-md-6">
					<label for="direccionEntrega">Direccion Entrega: </label>
					<input style="border: none;" type="text" placeholder="Direccion Entrega:" id="direccionEntrega" name="direccionEntrega" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->direccionEntrega ?>">
				</div>
				<div class="col-md-6">
					<label for="referenciaEntrega">Referencia entrega </label>
					<input style="border: none;" type="text" placeholder="Referencia entrega" id="referenciaEntrega" name="referenciaEntrega" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->referenciaEntrega ?>">
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<label for="distritoEntrega">Distrito de entrega: </label>
					<input style="border: none;" type="text" placeholder="Distrito de entrega:" id="distritoEntrega" name="distritoEntrega" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->distritoEntrega ?>">
				</div>
				<div class="col-md-4">
					<label for="fechaHoraEntrega">Fecha de atención: </label>
					<input style="border: none;" type="text" placeholder="Fecha de atención:" id="fechaHoraEntrega" name="fechaHoraEntrega" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->fechaHoraEntrega ?>">
				</div>
				<div class="col-md-4">
					<label for="recibidoPor">Recepcion por: </label>
					<input style="border: none;" type="text" placeholder="Recepcion por:" id="recibidoPor" name="recibidoPor" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObjTemporal->recibidoPor ?>">
				</div>
			</div>
	<?php endif ?>	
	<?php else: ?>
		<h4>&nbsp;&nbsp;&nbsp;<?php echo $clienteObjTemporal->tipoDocumentoFacturacion ?></h4>
		<input type="hidden" name="tipoDocumentoFacturacion" value="<?php echo $clienteObjTemporal->tipoDocumentoFacturacion ?>">
		<input type="hidden" name="dniFacturacion" value="<?php echo $clienteObjTemporal->dni ?>" >

	<?php endif ?>
</div>
<hr>

<h3>Datos del vehiculo</h3>
<?php 
$vehiculoTemporalObj=Vehiculo::obtenerClienteTemporal($clienteObjTemporal->idclientetemporal);
$n=1;

?>


<?php foreach ($vehiculoTemporalObj as $var): ?> 
	<div class="row"><hr>
		<h3 class="text-center"># <?php echo $n++ ?></h3>

		<div class="col-lg-12 field_wrapper"> 
			<div class="row">
				<div class="form-group col-md-2">
					<label for="placa" class="">Placa</label>
					<input style="border: none;" type="text" placeholder="Placa" id="placa" name="field_name[0][]" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $var->placa ?>">
				</div>
				<div class="form-group col-md-4">
					<label for="chasis" class="">Chasis</label>
					<input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="field_name[1][]" class="border-bottom form-control text-uppercase" required value="<?php echo $var->chasis ?>">
				</div>
				<div class="form-group col-md-3">
					<label for="motor" class="">Motor</label>
					<input style="border: none;" type="text" placeholder="Motor" id="motor" name="field_name[2][]" class="border-bottom form-control text-uppercase" required value="<?php echo $var->motor ?>" >
				</div>
				<div class="form-group col-md-3">
					<label for="marca" class="">Marca</label>
					<input style="border: none;" type="text" placeholder="Marca" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $var->marca ?>" >
				</div>
			</div> 
			<?php include("../../includes/campanasCrearAcceso.php") ?>
			<div class="row">
				<div class="form-group col-md-3">
					<label for="modelo" class="">Modelo</label>
					<input style="border: none;" type="text" placeholder="Modelo" id="modelo" name="field_name[4][]" class="border-bottom form-control text-capitalize"  required  value="<?php echo $var->modelo ?>">
				</div>
				<div class="form-group col-md-2">
					<label for="color" class="">Color</label>
					<input style="border: none;" type="text" placeholder="Color" id="color" name="field_name[5][]" class="border-bottom form-control x2 text-capitalize" required value="<?php echo $var->color ?>" >
				</div>
				<div class="form-group col-md-2">
					<label for="anio" class="">Año</label>
					<input style="border: none;" type="text" placeholder="Año" id="anio" name="field_name[6][]" class="border-bottom form-control" required value="<?php echo $var->anio ?>" >
				</div>
				<div class="form-group col-md-3">
					<label for="seguro" class="">Compañia de seguros</label>
					<input style="border: none;" type="text" placeholder="Compañia de seguros" id="seguro" name="field_name[7][]" class="seguro border-bottom form-control" required autocomplete="off" value="<?php echo $var->seguro ?>">
				</div>
				<div class="form-group col-md-2">
					<label for="concecionario" class="">Concesionario</label>
					<input style="border: none;" type="text" placeholder="Concesionario" id="concecionario" name="field_name[11][]" class="seguro border-bottom form-control" required autocomplete="off" value="<?php echo $var->concesionario ?>">
				</div>

			</div>                                      


			<div class="row">
				<div class="form-group col-md-4">
					<label for="numeroTramiteSeguro" class="">Numero tramite de seguro</label>
					<input style="border: none;" type="text" placeholder="Numero tramite de seguro" id="numeroTramiteSeguro" name="field_name[8][]" class="numeroTramiteSeguro border-bottom form-control text-capitalize"  value="<?php echo $var->numeroTramiteSeguro ?>"  >
				</div>
				<div class="form-group col-md-4">
					<label for="numeroPoliza" class="">N° de poliza</label>
					<input style="border: none;" type="text" placeholder="N° de poliza" id="numeroPoliza" name="field_name[9][]" class="border-bottom form-control text-capitalize"   value="<?php echo $var->numeroPoliza ?>">
				</div>
				<div class="form-group col-md-4">
					<label for="corredor" class="">Corredor de seguros</label>

			<!-- 		<select  style="border: none;" class="corredor border-bottom form-control text-capitalize" required name="field_name[10][]">
						<option class='text-capitalize' value='"<?php echo $var->corredor ?>"'  selected ><?php 
							echo $var->corredor
							?></option>
							<?php Vehiculo::obtenerCorredores() ?>
						</select> -->
						 <input style="border: none;" type="text" placeholder="Corredor de seguros." id="corredor" name="field_name[10][]" class="border-bottom form-control text-capitalize"   value="<?= limpiar($var->idcorredordeseguro) ?>">

				</div>
				<!-- <div class="form-group col-md-3">
					<a class="add_button btn btn-info col-md-12" title="Agregar contacto" name="agregarVehículo">Agregar <i class="fa fa-plus-circle "></i></a> 
				</div> -->
			</div> 
		</div>
	</div> 

	<hr>
<?php endforeach ?> 
<hr><div class="hr-line-dashed"></div>
<?php $contactosAutorizadosTObj=Cliente::obtenerContactosAutorizadosTemporal($clienteObjTemporal->idclientetemporal);
$m=1;
?>
<h3>Contactos autorizados: </h3>
<?php foreach ($contactosAutorizadosTObj as $var): ?>
	<h3 class="text-center">#<?php echo $m++ ?></h3>
	<div class="row">
		<div class="form-group col-md-3">
			<label for="dniC" class="">DNI: </label> <?php echo $var->dni ?>
			<input type="hidden" name="contactos[0][]" value="<?php echo $var->dni ?>" >
		</div>
		<div class="form-group col-md-3">
			<label for="nombreC" class="">Nombres: </label> <?php echo $var->nombre ?>
			<input type="hidden" name="contactos[1][]" value="<?php echo $var->nombre ?>" >
		</div>
		<div class="form-group col-md-3">
			<label for="apellidos" class="">Apellidos : </label> <?php echo $var->apellidos ?>
			<input type="hidden" name="contactos[2][]" value="<?php echo $var->apellidos ?>" >

		</div>
		<div class="form-group col-md-3">
			<label for="dniC" class="">Celular: </label> <?php echo $var->telefono ?>
			<input type="hidden" name="contactos[3][]" value="<?php echo $var->telefono ?>" >
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-3">
			<label for="telefono" class="">Telefono: </label> <?php echo $var->telefono1 ?>
			<input type="hidden" name="contactos[4][]" value="<?php echo $var->telefono1 ?>" >
		</div>
		<div class="form-group col-md-5">
			<label for="correo" class="">Correo: </label> <?php echo $var->correo ?>
			<input type="hidden" name="contactos[5][]" value="<?php echo $var->correo ?>" >
		</div>
		<div class="form-group col-md-4">
			<label for="Parentesco" class="">Parentesco: </label> <?php echo $var->parentesco ?>
			<input type="hidden" name="contactos[6][]" value="<?php echo $var->parentesco ?>" >
		</div>

		<div class="hr-line-dashed"></div>

	</div>
	
<?php endforeach ?>


<hr>
<?php if (!empty($cotizarObj->comisiona)): ?>
	<h1>Comisiona: <?= $cotizarObj->comisiona ?></h1>
<?php endif ?>
<input type="hidden" value="<?= $cotizarObj->idcanal ?>" name="idcanal">
<input type="hidden" value="<?= $cotizarObj->idconvenio ?>" name="idconvenio">
<input type="hidden" value="<?= $cotizarObj->comision ?>" name="comision">
<input type="hidden" value="<?= $cotizarObj->comisiona ?>" name="comisiona">
<div class="row">
	<div class="col-md-12 text-center"><h1>Datos de instalación</h1></div>
</div>
<div role="tablist">

	<input type="radio" title="taller" name="lugarInstacion" id="taller" role="tab" aria-controls="taller-panel" checked value="Taller" onclick="validarlugarInstalacion()" />
	<input type="radio" title="domicilio" name="lugarInstacion" id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente" onclick="validarlugarInstalacion()" />
	<input type="radio" title="concesionario" name="lugarInstacion" id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario" onclick="validarlugarInstalacion()" />

	<div class="tabs" aria-hidden="true">
		<label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
		<label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
		<label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
	</div>
	<div class="panels">
		<ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">
			<h2>TALLER</h2>
			<p>Av. Paseo de la República 4925 con Angamos Este </p>
			<p>Surquillo, Lima, Lima</p>
			<input type="hidden" name="direccionTaller" value="Av. Paseo de la República 4925 con Angamos Este - Surquillo, Lima, Lima">
			<div class="row">
				<div class="row">
					<div class="form-group col-md-5" id="data_1">
						 <div class="row">
				            <div class="form-group col-md-6">   
				              <label  class="">Fecha</label>
				              <div class="input-group date">
				                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control"  id="fechaIT" name="fechaIT" autocomplete="off" required >
				              </div>
				            </div>
				            <div class="form-group col-md-6">
				              <label  class="">Hora</label>
				              <div class="input-group" data-autoclose="true">
				                <input type="text" class="form-control" id="horaIT" name="horaIT" autocomplete="off" required >
				                <span class="input-group-addon">
				                  <span class="fa fa-clock-o"></span>
				                </span>
				              </div>
				            </div>
				          </div>  
					</div> 
					<div class="form-group col-md-4">
						<label for="nombreContactoIT" class="">Nombre Contacto *</label>
						<input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIT" name="nombreContactoIT" class="form-control x2 text-capitalize border-bottom" required="" >
					</div><div class="form-group col-md-3">
					<label for="numeroContactoIT" class="">Número Contacto *</label>
					<input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContactoIT" name="numeroContactoIT" class="form-control x2 text-capitalize border-bottom" required="" >
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label for="observacion" class="sr-only">Observaciones *</label>
					<input style="border: none;" type="text" placeholder="Observaciones *" id="observacion" name="observacion" class="form-control x2 text-capitalize border-bottom" >
				</div>
			</div>
		</div>



		</ul>
		<ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab">	
			<h2>Domicilio Cliente</h2>
			<div class="row">
				<div class="row">
					<div class="form-group  col-md-4">
						<label for="departamentoInstalacionD" class="">Departamento*</label>
						 <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize" required name="departamentoInstalacionD" id="departamentoInstalacionD">
			                <option class="text-capitalize" value="<?php echo $clienteObjTemporal->departamento ?>" disabled selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($clienteObjTemporal->departamento) ?><?= $departamento->departamento ?></option>
			                <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
			                <?php if ($objDepartamento): ?>
			                  <?php foreach ($objDepartamento as $var): ?>
			                    <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
			                  <?php endforeach ?>
			                <?php else: ?>
			                <?php endif ?>
			              </select>
					</div>
					<div class="form-group  col-md-4">
						<label for="provinciaInstalacionD" class="">Provincia*</label>
						<select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
			              <option class="text-capitalize" value="<?php echo $clienteObjTemporal->provincia ?>" disabled selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($clienteObjTemporal->provincia) ?><?php echo $provincia->provincia ?></option>
			             </select>
					</div>
					<div class="form-group  col-md-4">
						<label for="distritoInstalacionD" class="">Distrito*</label>
						<select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize" required name="distritoInstalacionD"  id="distritoInstalacionD" >
				          <option class="text-capitalize" value="<?php echo $clienteObjTemporal->distrito ?>" disabled selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($clienteObjTemporal->distrito) ?><?= $distrito->distrito ?></option>
				          </select>						
					</div>

				</div>
				<div class="row">
					<div class="form-group  col-md-6">
						<label for="direccionInstalacionD" class="sr-only">Direccion*</label>
						<input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionD" name="direccionInstalacionD" required="" class="form-control border-bottom" value="<?php echo $clienteObjTemporal->direccion ?>" >
					</div>
					<div class="form-group  col-md-6">
						<label for="referenciaInstalacionD" class="sr-only">Referencia *</label>
						<input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" name="referenciaInstalacionD" required="" class="form-control border-bottom" value="<?php echo $clienteObjTemporal->referencia ?>">
					</div>
				</div>	
				<div class="row">
					<div class="form-group col-md-2">
						<label for="cod" class="sr-only">Cordenadas Domicilio *</label>
						<input style="border: none;" type="text" placeholder="Cordenadas Domicilio *" id="cod" required name="cordenadasD" class="form-control x2 text-capitalize border-bottom" >
					</div>
					<div class="form-group col-md-8">
						<label for="observacion" class="sr-only">Observaciones *</label>
						<input style="border: none;" type="text" placeholder="Observaciones *" id="observacion"  name="observacion" class="form-control x2 text-capitalize border-bottom" >
					</div>
				</div>	
			   <div class="form-group col-md-5" id="data_1">
      					<div class="row">
				            <div class="form-group col-md-6">   
				              <label  class="">Fecha</label>
				              <div class="input-group date">
				                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control"  id="fechaID" name="fechaID" autocomplete="off" required>
				              </div>
				            </div>
				            <div class="form-group col-md-6">
				              <label  class="">Hora</label>
				              <div class="input-group" data-autoclose="true">
				                <input type="text" class="form-control" id="horaID" name="horaID" autocomplete="off" required>
				                <span class="input-group-addon">
				                  <span class="fa fa-clock-o"></span>
				                </span>
				              </div>
				            </div>
				          </div>      
   			 </div> 
				<div class="row">
					<div class="form-group col-md-6">
						<label for="nombreContactoID" class="">Nombre Contacto *</label>
						<input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoID" required name="nombreContactoID" class="form-control x2 text-capitalize border-bottom" >
					</div><div class="form-group col-md-6">
					<label for="numeroContactoID" class="">Número Contacto *</label>
					<input style="border: none;" type="number" required placeholder=" Número Contacto *" id="numeroContactoID" name="numeroContactoID" class="form-control x2 text-capitalize border-bottom" >
				</div>
			</div>
		</div>
		



	</ul>
	<ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">
		
		<div class="row">
			<div class="col-md-6"><h2>Concecionario</h2></div>
			<div class="col-md-6">
				<label for="concesionarioInstalacion" class="sr-only">Nombre de concesionario*</label>
				<input style="border: none;" type="text" placeholder="Nombre de concesionario*" id="concesionarioInstalacion" name="concesionarioInstalacion" class="form-control border-bottom" required >
			</div>
		</div>	 	

		<div class="row">
			<div class="row">
				<div class="form-group  col-md-4">
					<label for="departamentoInstalacionC" class="">Departamento*</label>
					 <select  style="border: none;" class="departamentoInstalacionC border-bottom form-control text-capitalize" required name="departamentoInstalacionC" id="departamentoInstalacionC">    
				        <option class="text-capitalize" disabled selected >Seleccione Departamento</option>           
				                <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
				                <?php if ($objDepartamento): ?>
				                  <?php foreach ($objDepartamento as $var): ?>
				                    <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
				                  <?php endforeach ?>
				                <?php else: ?>
				                <?php endif ?>				        
				     </select>
				</div>
				<div class="form-group  col-md-4">
					<label for="provinciaInstalacionC" class="">Provincia*</label>
					 <select  style="border: none;" class="provinciaInstalacionC border-bottom form-control text-capitalize" required name="provinciaInstalacionC" id="provinciaInstalacionC" >
              </select>
				</div>
				<div class="form-group  col-md-4">
					<label for="distritoInstalacionC" class="">Distrito*</label>
					<select  style="border: none;" class="distritoInstalacionC border-bottom form-control text-capitalize" required name="distritoInstalacionC"  id="distritoInstalacionC" >         
          			</select>	
				</div>

			</div>
			<div class="row">
				<div class="form-group  col-md-6">
					<label for="direccionInstalacionC" class="sr-only">Direccion*</label>
					<input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom" required >
				</div>
				<div class="form-group  col-md-6">
					<label for="referenciaInstalacionC" class="sr-only">Referencia *</label>
					<input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom" required>
				</div>
			</div>		
			<div class="row">
				  <div class="form-group col-md-5" id="data_1">
      					<div class="row">
		                  <div class="form-group col-md-6">   
		                      <label  class="">Fecha</label>
		                      <div class="input-group date">
		                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control"  id="fechaIC" name="fechaIC" autocomplete="off" required>
		                      </div>
		                  </div>
		                  <div class="form-group col-md-6">
		                                <label  class="">Hora</label>
		                    <div class="input-group" data-autoclose="true">
		                        <input type="text" class="form-control" id="horaIC" name="horaIC" autocomplete="off" required>
		                        <span class="input-group-addon">
		                            <span class="fa fa-clock-o"></span>
		                        </span>
		                    </div>
		                  </div>
		                </div>      
   			 </div> 
			</div>
			<div class="row">
				<div class="form-group col-md-6">	
					<label for="nombreContactoIC" class="">Nombre Contacto *</label>
					<input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIC" name="nombreContactoIC" class="form-control x2 text-capitalize border-bottom" required >
				</div>
				<div class="form-group col-md-6">
					<label for="numeroContactoIC" class="">Número Contacto *</label>
					<input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContactoIC" name="numeroContactoIC" class="form-control x2 text-capitalize border-bottom" required >
				</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label for="observacion" class="sr-only">Observaciones *</label>
				<input style="border: none;" type="text" placeholder="Observaciones *" id="observacion" name="observacion" class="form-control x2 text-capitalize border-bottom" >
			</div>
		</div>
	</div>
</ul>
</div>
</div>
<div class="row">
	<div class="col-md-6"></div>
	<div class="col-md-6">
		<button class="btn btn-info col-md-12" name="verificado" onclick="validarPago()" >CONTINUAR CON LA INSTALACION</button>
	</div>
</div>


<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../../assets/js/plugins/validate/jquery.validate.min.js"></script>

<script src="../../assets/build/jquery.datetimepicker.full.js"></script>
<script>
  $(".departamento").change(function(){provincias();});
  $(".departamento2").change(function(){provincias2();});
  $(".departamento3").change(function(){provincias3();});
  $(".departamentoInstalacionD").change(function(){provincias4();});
  $(".departamentoInstalacionC").change(function(){provincias5();});
  $(".provincia1").change(function(){distritos();});
  $(".provincia2").change(function(){distritos2();});
  $(".provincia3").change(function(){distritos3();});  
  $(".provinciaInstalacionD").change(function(){distritos4();});
  $(".provinciaInstalacionC").change(function(){distritos5();});
  function provincias(){
    var iddepartamento = $(".departamento").val();
    $.ajax({        
      type : 'POST',
      url  : 'ajax/provincia',
      data :  'iddepartamento='+ iddepartamento,
      success : function(data)
      {
        $(".provincia1").html(data);

               }
           });
    return false;
  }
  function provincias2(){
    var iddepartamento = $(".departamento2").val();
    $.ajax({        
      type : 'POST',
      url  : 'ajax/provincia',
      data :  'iddepartamento='+ iddepartamento,
      success : function(data)
      {
        $(".provincia2").html(data);

               }
           });
    return false;
  }
  function provincias3(){
    var iddepartamento = $(".departamento3").val();
    $.ajax({        
      type : 'POST',
      url  : 'ajax/provincia',
      data :  'iddepartamento='+ iddepartamento,
      success : function(data)
      {
        $(".provincia3").html(data);

               }
           });
    return false;
  }
   function provincias4(){
      var iddepartamento = $(".departamentoInstalacionD").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/provincia',
        data :  'iddepartamento='+ iddepartamento,
        success : function(data)
        {
          $(".provinciaInstalacionD").html(data);
        }
      });
      return false;
    }
    function provincias5(){
      var iddepartamento = $(".departamentoInstalacionC").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/provincia',
        data :  'iddepartamento='+ iddepartamento,
        success : function(data)
        {
          $(".provinciaInstalacionC").html(data);
        }
      });
      return false;
    }
  function distritos(){
    var idprovincia = $(".provincia1").val();
    $.ajax({        
      type : 'POST',
      url  : 'ajax/distrito',
      data :  'idprovincia='+ idprovincia,
      success : function(data)
      {
        $(".distritos1").html(data)
        }
           });
    return false;
  }
  function distritos2(){
    var idprovincia = $(".provincia2").val();
    $.ajax({        
      type : 'POST',
      url  : 'ajax/distrito',
      data :  'idprovincia='+ idprovincia,
      success : function(data)
      {
        $(".distritos2").html(data)
        }
           });
    return false;
  }
  function distritos3(){
    var idprovincia = $(".provincia3").val();
    $.ajax({        
      type : 'POST',
      url  : 'ajax/distrito',
      data :  'idprovincia='+ idprovincia,
      success : function(data)
      {
        $(".distritos3").html(data)
        }
           });
    return false;
  }
  function distritos4(){
      var idprovincia = $(".provinciaInstalacionD").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionD").html(data)
        }
      });
      return false;
    }
    function distritos5(){
      var idprovincia = $(".provinciaInstalacionC").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionC").html(data)
        }
      });
      return false;
    }
</script>
<script>
  <?php $fechaA=date('Y-m-d') ?>  
  jQuery.datetimepicker.setLocale('es');
  $('#fechaIT').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d',
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
    // allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });
  $('#horaIT').datetimepicker({
    minDate:'<?= $fechaA ?>',
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });
  $('#fechaIC').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d',
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
  });   
  $('#horaIC').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });
   $('#fechaID').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d',
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
  });
  $('#horaID').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });
$(document).ready(function(){
         $("#corizarForm").validate({
         	rules: {
         		nombre: {
         			required:true
         		},
         		dniEditado:{
         			required:true,
         			minlength:8,
         			maxlength:8,
         		},
         		rucEditado:{
         			required:true,
         			minlength:11,
         			maxlength:11,
         		},
         		telefono:{
         			required:true,
         			minlength:9	
         		}

         	},
         	messages: {
         		nombre: {
         			required: "Es necesario ingresar nombres",

         		},
         		dniEditado: {
         			required: "Es necesario ingresar Dni",
         			minlength: jQuery.validator.format("Se nececita {0} caracteres !"),
         			maxlength: jQuery.validator.format("No exeder de {0} caracteres !"),
         		},rucEditado: {
         			required: "Es necesario ingresar Ruc",
         			minlength: jQuery.validator.format("Se nececita {0} caracteres !"),
         			maxlength: jQuery.validator.format("No exeder de {0} caracteres !")

         		},departamento: {
         			required: "Seleccione departamento",

         		},provincia: {
         			required: "Seleccione provincia",

         		},distrito: {
         			required: "Seleccione distrito",

         		},direccion: {
         			required: "Por favor ingrese dirección",

         		},referencia: {
         			required: "Por favor ingrese referencia",

         		},correo: {
         			required: "es necesario un correo ",
         			email:"Ingrese un correo valido",

         		},
         		dni: {
         			required: "Numero de dni necesario"
         		}

         	}
         });
     });

validarlugarInstalacion();
  function validarlugarInstalacion(){ 
  var lugarInstalacion=$('input[name="lugarInstacion"]:checked').val();  
  if (lugarInstalacion=='Taller'){   
  document.getElementById("fechaIT").removeAttribute("disabled");
  document.getElementById("horaIT").removeAttribute("disabled");
  var horaCon = document.getElementById("horaIC"); 
  var fechaCon = document.getElementById("fechaIC"); 
  var horaDo = document.getElementById("horaID"); 
  var fechaDo = document.getElementById("fechaID"); 
  var departamentoInstalacionD = document.getElementById("departamentoInstalacionD"); 
  var provinciaInstalacionD = document.getElementById("provinciaInstalacionD"); 
  var distritoInstalacionD = document.getElementById("distritoInstalacionD"); 
  var provinciaInstalacionC = document.getElementById("provinciaInstalacionC"); 
  var distritoInstalacionC = document.getElementById("distritoInstalacionC"); 
  var concesionarioInstalacion = document.getElementById("concesionarioInstalacion"); 
  var cod = document.getElementById("cod");
  var att = document.createAttribute("disabled");     
  var att1= document.createAttribute("disabled");     
  var att2= document.createAttribute("disabled");     
  var att3= document.createAttribute("disabled");     
  var att4= document.createAttribute("disabled");     
  var att5= document.createAttribute("disabled");     
  var att6= document.createAttribute("disabled");     
  var att7= document.createAttribute("disabled");     
  var att8= document.createAttribute("disabled");     
  var att9= document.createAttribute("disabled");     
  var att10= document.createAttribute("disabled");
  horaCon.setAttributeNode(att);
  fechaCon.setAttributeNode(att1);
  horaDo.setAttributeNode(att2);
  fechaDo.setAttributeNode(att3);
  cod.setAttributeNode(att4);
  concesionarioInstalacion.setAttributeNode(att5);
  departamentoInstalacionD.setAttributeNode(att6);
  provinciaInstalacionD.setAttributeNode(att7);
  distritoInstalacionD.setAttributeNode(att8);
  provinciaInstalacionC.setAttributeNode(att9);
  distritoInstalacionC.setAttributeNode(att10);
  
  }else if(lugarInstalacion=='Domicilio Cliente'){
  document.getElementById("horaID").removeAttribute("disabled");
  document.getElementById("fechaID").removeAttribute("disabled");
  document.getElementById("departamentoInstalacionD").removeAttribute("disabled");
  document.getElementById("distritoInstalacionD").removeAttribute("disabled");
  document.getElementById("provinciaInstalacionD").removeAttribute("disabled");
  document.getElementById("cod").removeAttribute("disabled");  
  var concesionarioInstalacion = document.getElementById("concesionarioInstalacion"); 
  var horaCon = document.getElementById("horaIC"); 
  var fechaCon = document.getElementById("fechaIC"); 
  var horaDo = document.getElementById("fechaIT"); 
  var fechaDo = document.getElementById("horaIT"); 
  // var cod = document.getElementById("cod"); 
  var att = document.createAttribute("disabled");     
  var att1= document.createAttribute("disabled");     
  var att2= document.createAttribute("disabled");     
  var att3= document.createAttribute("disabled");     
  // var att4= document.createAttribute("disabled");     
  var att5= document.createAttribute("disabled");     
  horaCon.setAttributeNode(att);
  fechaCon.setAttributeNode(att1);
  horaDo.setAttributeNode(att2);
  fechaDo.setAttributeNode(att3);
  concesionarioInstalacion.setAttributeNode(att5);
  // cod.setAttributeNode(att4);

  }else if(lugarInstalacion=='Concesionario'){
  // alert(lugarInstalacion);
  document.getElementById("horaIC").removeAttribute("disabled");
  document.getElementById("fechaIC").removeAttribute("disabled");
  document.getElementById("concesionarioInstalacion").removeAttribute("disabled");
  document.getElementById("provinciaInstalacionC").removeAttribute("disabled");
  document.getElementById("distritoInstalacionC").removeAttribute("disabled");
  var horaCon = document.getElementById("horaID"); 
  var fechaCon = document.getElementById("fechaID"); 
  var horaDo = document.getElementById("fechaIT"); 
  var fechaDo = document.getElementById("horaIT"); 
  var cod = document.getElementById("cod"); 
  var att = document.createAttribute("disabled");     
  var att1= document.createAttribute("disabled");     
  var att2= document.createAttribute("disabled");     
  var att3= document.createAttribute("disabled");     
  var att4= document.createAttribute("disabled");     
  att.horaCon = "true";
  att1.fechaCon = "true";
  att2.horaDo = "true";
  att3.fechaDo = "true";
  att4.cod = "true";
  horaCon.setAttributeNode(att);
  fechaCon.setAttributeNode(att1);
  horaDo.setAttributeNode(att2);
  fechaDo.setAttributeNode(att3);
  cod.setAttributeNode(att4);
  }
  }
 </script>

<?php
}
?>
<?php
} 
?>


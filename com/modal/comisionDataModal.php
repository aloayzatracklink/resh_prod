<?php 
if ($_POST["rowid"]) {
    include_once("../../includes/init.php");
    // require_once("../../conta/ajax/Comisiones.php");
    $comisionid=htmlspecialchars(htmlentities($_POST["rowid"]));
    $comisionData=Comisiones::obtenerDatosDeComision($comisionid);
    // $estadoFacPorCom=Comisiones::EstadoPagoDeFacturacionPorComision($comisionData->idfacturacion);
    // $datosFac= new DatosFacturacion();
    // $estadoFacPorCom = $datosFac->obtenerDatosFacturacion($comisionData->idfacturacion);
    //var_dump($comisionData);

if ($comisionData->idrenovacion ==0) {
    $clienDatos=Cliente::obtenerDatosDeclientePorId($comisionData->idcliente);
    $vhDatos=Vehiculo::obtenerVehiculo($comisionData->idvehiculo);
}else{
    $reno=Renovaciones::obtenerDatosDeRenovacionPorId($comisionData->idrenovacion);
}


    if ($comisionData->idcanal==0) {
        $canalDatos=Canales::obtenerDatosTITULARPorId($comisionData->idtitular);
    }else{
        $canalDatos=Canales::obtenerDatosCanalPorId($comisionData->idcanal);
    }


    $obMenu=Usuario::obtenerMenu($session->idusuario);
    $o=Usuario::obtenerUsuario($comisionData->idCreador); 
    $objFact=DatosFacturacion::obtenerDatosFacturacionPorId($comisionData->idfacturacion);
    //var_dump($objFact);
  ?>

  <input type="hidden" name="comisionid" value="<?= $comisionid ?>" >
  <input type="hidden" name="idfacturacion" value="<?= $comisionData->idfacturacion ?>" >
  <input type="hidden" name="estado" value="<?= $comisionData->estado ?>" >
  <?php if ($comisionData): ?>
  <input type="hidden" name="canalid" value="<?= $comisionData->idcanal ?>" >    
  <?php endif ?>
  <div class="well" style="padding-top: 11px;padding-bottom: 10px;">
  <div class="row">
    <div class="row">
        <div class="form-group col-md-3">
            <label for="Observaciones" >Trabajo</label>
            <input name="" style="border: none;" type="text" placeholder="Web" id="trabajo" class="border-bottom form-control" readonly  value="<?=$comisionData->trabajo?>">
        </div>
      <div class="form-group col-md-3">
        <label for="Observaciones" ><?php

            if ($comisionData->idrenovacion ==0) {
                echo $retVal = ($clienDatos->tipoCliente=="Natural") ? "DNI" : "RUC";
              }else{
                echo  "N° Documento";
            }

            ?></label>
        <input name="" style="border: none;" type="text" placeholder="Web" id="Observaciones" class="border-bottom form-control" readonly
               value="<?php
               if ($comisionData->idrenovacion ==0) {
                   echo $retval = ($clienDatos->tipoCliente=="Natural") ? $clienDatos->dni : $clienDatos->ruc;

              }else{
                   echo   $retval =$reno->ndoc;
               }
               ?>">
      </div>
      <div class="form-group col-md-6">
        <label for="numeroTecnico" ><?php
            if ($comisionData->idrenovacion ==0) {
                ($clienDatos->tipoCliente=="Natural") ? "Nombres" : "Razon Social";
            }else{
                echo "Descripcion";
            }
           ?></label>
        <input  required readonly style="border: none;" type="text" id="numeroTecnico" name="" class="border-bottom form-control"  value="<?php

        if ($comisionData->idrenovacion ==0) {
            echo $datosCliente = ($clienDatos->tipoCliente=="Natural") ? $clienDatos->nombre." ".$clienDatos->apellidosC : $clienDatos->razonSocial;

        }else{
            echo   $datosCliente =$reno->nombreCliente;
        }

        ?> ">
        <input type="hidden" value="<?= $datosCliente ?>" name="datosCliente">
      </div>
    </div>
    <hr>
     <div class="row">
      <div class="form-group col-md-2">
        <label for="placa" >Placa</label>
        <input name="" style="border: none;" type="text" placeholder="Web" id="placa" class="border-bottom form-control" readonly  value="<?php
        if ($comisionData->idrenovacion ==0) {
            echo $vhDatos->placa;

        }else{
            echo  $reno->placa;
        }?>">
      </div>
      <div class="form-group col-md-2">
        <label for="marca" >Marca</label>
        <input  required readonly style="border: none;" type="text" id="marca" name="" class="border-bottom form-control"  value="<?php
        if ($comisionData->idrenovacion ==0) {
            echo $vhDatos->marca;

        }else{
            echo  $reno->marca;
        }
        ?> ">
      </div>
         <div class="form-group col-md-2">
             <label for="marca" >tiempo servicio</label>
             <input  required readonly style="border: none;" type="text" id="marca" name="" class="border-bottom form-control"  value="<?php
             if ($comisionData->idrenovacion ==0) {
                 echo $vhDatos->planTiempo;

             }else{
                 echo  $reno->tiempo;
             } ?> ">
         </div>
      <div class="form-group col-md-3">
        <label for="planN" >Plan</label>
        <input  required readonly style="border: none;" type="text" id="planN" name="" class="border-bottom form-control"  value="<?php
        if ($comisionData->idrenovacion ==0) {
            echo $vhDatos->plan;

        }else{
            echo  $objFact->plan;
        }
        ?> ">
      </div>
      <div class="form-group col-md-3">
        <label for="chasis" >Chasis</label>
        <input  required readonly style="border: none;" type="text"  id="chasis" name="" class="border-bottom form-control"  value="<?php
        if ($comisionData->idrenovacion ==0) {
            echo $vhDatos->chasis;

        }else{
            echo  $reno->chasis;
        }
        ?>
        ">
      </div>
    </div>
<div class="row">
<div class="col-md-12">
          <h3 class="text-center">Ejecutivo Tracklink: <?= $o->usuarioNombre." ".$o->usuarioApellidoPaterno;   ?></h3>
      </div>
  </div>
    <div class="row">
      <div class="col-md-4">
        <label for="tipoCanal" >Tipo canal</label>
        <input  required <?= (empty($objFact->tipoCanal))?'':'readonly' ?> style="border: none;" type="text"  id="tipoCanal" name="tipoCanal" class="border-bottom form-control"  value="<?= strtoupper($objFact->tipoCanal);   ?>">
      </div>
      <div class="col-md-4">

        <label for="nombreCanal" >Nombre Canal</label>
        <input  required <?= (empty($objFact->nombreCanal))?'':'readonly' ?> style="border: none;" type="text"  id="nombreCanal" name="nombreCanal" class="border-bottom form-control"  value="<?= strtoupper($objFact->nombreCanal) ?> ">
      </div>
<div class="col-md-4">
        <label for="ejecutivoCanal" >Ejecutivo canal</label>
        <input  required <?= (empty($objFact->ejecutivoCanal))?'':'readonly' ?> style="border: none;" type="text"  id="ejecutivoCanal" name="ejecutivoCanal" class="border-bottom form-control"  value="<?= strtoupper($objFact->ejecutivoCanal) ?> ">
      </div>
    </div>

</div>
<hr>
<input type="hidden" value="<?= $comisionData->type_commission ?>" name="type_commision">
<?php if($comisionData->type_commission == 2){ ?>
<?php if ($canalDatos){ ?>
<input type="hidden" value="<?= $canalDatos->idtitular ?>" name="agregarId">
<div class="row">
<div class="col-md-4">
    <label for="ruc" >Ruc</label>
    <input style="border: none;"  type="text" placeholder="Ruc *"  id="ruc" name="ruc" required class="border-bottom form-control" value="<?= strtoupper($canalDatos->ruc) ?>"  >
</div>
<div class="form-group col-md-8">
    <label for="titular" >Titular de la cuenta y del recibo por honorarios *</label>
    <input style="border: none;" type="text" placeholder="Titular *" id="titular" name="titular" required class="border-bottom form-control" value="<?= strtoupper($canalDatos->titular) ?>"  >
  </div>
</div>
 <div class="row">
<div class="form-group col-md-4">
    <label for="moneda" >Banco</label>
    <select style="border: none;"  name="bancoCanal" class="cd-selec form-control border-bottom" required readonly id="monedaCanal">
            <option value="<?php echo $canalDatos->banco ?>" selected><?php echo $canalDatos->banco ?></option>
    </select>
  </div>
<div class="form-group col-md-4">
    <label for="moneda" >Numero Cuenta</label>
        <input style="border: none;" readonly  type="text" placeholder="Numero Cuenta" id="numeroCuenta" required name="numeroCuenta" class="border-bottom form-control" value="<?= strtoupper($canalDatos->numeroCuenta) ?>"  >
    </select>
  </div>
  <div class="form-group col-md-4">
    <label for="moneda" >Moneda *</label>
    <select style="border: none;"  name="moneda" class="cd-selec form-control border-bottom" required readonly id="monedaCanal">
	      		<option value="<?php echo $canalDatos->moneda ?>" selected><?php echo $canalDatos->moneda ?></option>
    </select>
  </div>
</div> 
    
<div class="row">
    <div class="form-group col-md-4" id='cci1'>
    <label for="cci2" >CCI</label>
    <input style="border: none;" type="text" placeholder="CCI *" id="cci2" name="cci" class="border-bottom form-control" readonly value="<?= strtoupper($canalDatos->cci) ?>" >
  </div>
</div>
<?php }else{ ?>
<div id="agregar">
  <div class="row">
    <div class="form-group col-md-12">
      <label for="canalIdcom" class="">Títular del RH y cuenta </label>
      <input style="border: none;" type="text" placeholder="Títular del RH y cuenta " id="canalIdnew" name="dni" class="canalId border-bottom form-control text-capitalize text-uppercase canalIdnew" autocomplete="off" required="" >
    </div>      
  </div>
    <div id="comisionHt">
    </div>
</div>
<div id="registrarNuevo">
    <button type="button" class="btn btn-white text-center" id="buscarExistente">Buscar registro existente</button>
  <div class="row data"></div>
</div>
<?php } }else{ if ($canalDatos){  if($canalDatos->idIntermediario!=0 || $canalDatos->idIntermediario!=""){

    $datosCanal=Intermediarios::obetenerDatosPorId($canalDatos->idIntermediario);
    ?>
    <input type="hidden" name="agregarId" value="<?= $canalDatos->idtitular ?>" >
    <input type="hidden" name="idInterm" value="<?php $canalDatos->idIntermediario ?>" >
    <div class="row">
        <div class="form-group col-md-4">
            <label for="ndni">N° Documento</label>
            <input style="border: none;" readonly type="text" placeholder="N° Doc.*" id="ndni" name="ndni"  class="border-bottom form-control"  value="<?= strtoupper($datosCanal->dni) ?>" >
        </div>
        <div class="form-group col-md-8">
            <label for="interm" >Intermediario *</label>
            <input style="border: none;" readonly type="text" placeholder="Intermediario *" id="interm" name="interm" class="border-bottom form-control" value="<?= strtoupper($datosCanal->executive) ?>"  >
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-3">
            <label for="nmail" class="">Mail</label>
            <input style="border: none;" readonly type="text" placeholder="Mail *" id="nmail" name="nmail" class="border-bottom form-control" value="<?= strtoupper($datosCanal->mail) ?>"  >
        </div>
        <div class='form-group col-md-4'>
            <label for='ntlef' class=''>Telefonos</label>
            <input style="border: none;" readonly type="text" placeholder="N° Telefono *" id="nmail" name="nmail" class="border-bottom form-control" value="<?php echo $datosCanal->phone." - ".$datosCanal->phone2 ?>"  >
        </div>

        <div class='form-group col-md-3'>
            <label for='nCanal' class=''>Canal</label>
            <input style="border: none;" readonly type="text" placeholder="Canal*" id="nCanal" name="nCanal" class="border-bottom form-control" value="<?php echo $datosCanal->type ?>"  >
        </div>
    </div>

    <?php }} else{?>
    <div id="agregar">
        <div class="row">
            <div class="form-group col-md-12">
                <label for="findInt" class=""> Buscar Intermediario </label>
                <input type="hidden" name="idInterm" id="idInterm" value="0" >
                <input style="border: none;" type="text" placeholder="Intermediario" id="canalIdnew2" name="dni2" class="canalId2 border-bottom form-control text-capitalize text-uppercase canalIdnew2" autocomplete="off" required="" >
            </div>
        </div>
        <div id="intermeFind">
        </div>
    </div>
<?php }}?>
<hr>
<div class="row">
  <div class="form-group col-md-3">
    <label for="totalM" >Monto total *</label>
    <input style="border: none;" type="text" placeholder="Monto total" id="totalM" name="montoTotal" class="border-bottom form-control" value="<?= $comisionData->montoTotal ?>" readonly required >
  </div>  
  <div class="form-group col-md-3">
    <label for="comi" >Comision *</label>
    <input style="border: none;" type="text" placeholder="Comision" id="comi" name="comision" class="border-bottom form-control" value="<?= limpiarPendiente($comisionData->comision) ?>" required >
  </div>  
 
  <div class="form-group col-md-6">
    <label for="fe" >Fecha registro *</label>    
    <span class="help-block m-b-none"><?= hace($comisionData->fechaRegistro) ?></span>
  </div>  
</div>

      <?php if($comisionData->type_commission == 2){ ?>
<?php if (empty($comisionData->reciboHonorarios)): ?>
<?php if ($comisionData->estadoFacturacion=='34'): ?>  
<div class="row">
    <div class="col-lg-12">
    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
      <div class="form-control" data-trigger="fileinput">
        <i class="glyphicon glyphicon-file fileinput-exists"></i>
        <span class="fileinput-filename"></span>
      </div>
      <span class="input-group-addon btn btn-default btn-file">
        <span class="fileinput-new">Adjuntar recibo por honorarios </span>
        <span class="fileinput-exists">Cambiar</span>
        <input type="file" name="recibo" />
      </span>
      <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
    </div> 
  </div>
</div>
<?php else: ?>
<div class="row">
    <div class="col-lg-12">
     <label class='text-danger' > Para adjuntar el RH la venta tiene que estar facturada </label>
  </div>
</div>
<?php endif ?>

<?php else: ?>
  <div class="row">
    <a  class="btn btn-primary text-center  col-md-6 col-md-offset-3" href="../../public/vouchers/<?= $comisionData->reciboHonorarios ?>" target="_blank" >Descargar/ver recibo por Honorarios</a>    
  </div>
  <input type="hidden" name="reciboH" value="<?= $comisionData->reciboHonorarios  ?>">
<?php endif ?>
<?php }
if ($obMenu->comisiones=="si"){ ?>
<div class="row">

    <?php
     $ntip = "Factura";
     $nidp = 2;
    if($comisionData->type_commission == 2){
        $ntip="Tarjeta";
        $nidp = 1;
        ?>
    <div class="form-group col-md-4">
        <label for="rhonorario" class="text-danger"> RECIBO HONORARIO *</label>
        <input style="border: none;" type="text" placeholder=" RECIBO HONORARIO* " id="rhonorario" name="rhonorario" required class="border-bottom form-control" value="<?= $comisionData->numeroReciboHonorario ?>">
      </div>
      
<?php if (!empty($comisionData->reciboHonorarios)){ ?>  
    <div class="form-group col-md-4">
        <br>
        <button type="button" class="btn btn-info text-center boorar" value="<?= $comisionData->idcomision ?>" >Eliminar RH</button>    
      </div>
    <?php } ?>  
</div>
    <?php } ?>

        <div class="row">
            <div class="form-group col-md-4">
                <br>
                <input type="hidden" name="ntipc"  id="ntipc"  value="<?= $nidp ?>">
                <input type="hidden" name="ndesc" id="ndesc" value="<?= $ntip ?>">
                <button type="button" class="btn btn-success text-center cambiaarTC" value="<?= $comisionData->idcomision ?>" >Cambiar Tipo pago comision a:<?= $ntip ?> </button>
            </div>
        </div>


<div class="row ">
    <div class="form-group col-md-4">
      <br>
      <button type="button" class="btn btn-warning text-center cambiaar" value="<?= $comisionData->idcomision ?>" >Cambiar titular</button>    
    </div>
  <div class="col-md-4">
      <div class="form-group col-md-4">
          <br>
          <button type="button" class="btn btn-danger text-center eliminar" value="<?= $comisionData->idcomision ?>" >Eliminar de comisiones</button>    
        </div>
  </div>

  <div class="col-md-4">

  </div>
</div>
<div class="row change-owner mb-2">

</div>
<?php } ?>
<div class="row">
  <div class="form-group col-md-4">
    <label for="documento" >Documento *</label>
    <input required  style="border: none;" type="text" placeholder="Documento Factura" id="documento" name="Documento" readonly class="border-bottom form-control"  value="<?= $comisionData->documentoFacturacion ?>"  >
  </div>
  <div class="form-group col-md-3">
    <label for="numFactura">Numero de facturacion</label>
    <input  style="border: none;" type="text" placeholder="Numero de facturacion" id="numFactura" name="numeroFacturacion" class="border-bottom form-control" required readonly value="<?= $comisionData->numeroFacturacion ?>"  >
  </div>
  <div class="form-group col-md-5">
    <label for="rhnorario" >Recibo Honorario</label>
    <input  required  style="border: none;" type="text" placeholder="Recibo Honorario" id="rhnorario" class="border-bottom form-control"   value="<?= $comisionData->numeroReciboHonorario ?>" readonly>
  </div>
  
</div>
<?php if ($comisionData->estado==67): ?>
<div class="row">
<div class="col-md-12 form-group">&nbsp;&nbsp;<label for="">PAGADO EN LA FECHA <?= $comisionData->observacion ?></label></div>
  </div>
<?php endif ?>
<?php if ($comisionData->estado==62): ?>
  <div class="row">
    <div class="col-md-12 form-group">&nbsp;&nbsp;<label for="">Observación de pago: <?= $comisionData->observacion ?></label></div>
  </div>  
<?php endif ?>
      <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
          <button type="submit" name="procesar" id="procesar"  class="btn btn-primary" value="<?php echo Token::generar() ?>">Guardar</button>
      </div>
  </div>
    <script src="../../assets/bootbox/bootbox.all.js"></script>
<script src="../../assets/js/jquery-confirm.min.js"></script>
<script>

    $(document).ready(function() {
        $("#titular").keyup(function () {
            var ta = $("#titular");
            var letras =  ""
            var letras =  ta.val().trim();
            if (letras.length === 0) {
                 letras =letras.replace(/ /g, "");
            }
            ta.val(letras)
        });
        $("#canalIdnew").keyup(function () {
            var ta = $("#canalIdnew");
            var letras =  ""
            var letras =  ta.val().trim();
            if (letras.length === 0) {
                letras =letras.replace(/ /g, "");
            }
            ta.val(letras)
        });

        $('#procesar').click(function() {
                var nid = $('#idInterm').val().trim();
                var ncomi = $('#comi').val().trim();
            if(nid == 0) {
                    bootbox.alert({
                        title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                        message: "<label>Seleccione un Intermediario*</label>",
                        size: 'small',
                        callback: function () {
                        }
                    });
                    console.log("Seleccione un Intermediario*");
                    return false;
                }

            if(ncomi == "") {
                bootbox.alert({
                    title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                    message: "<label>Ingrese comisión*</label>",
                    size: 'small',
                    callback: function () {
                    }
                });
                console.log("Ingrese comisión*");
                return false;
            }
        });
    })

    $('.boorar').click(function(e) {
  var idcomision=$(this).val();
  $.confirm({
      theme: 'modern',
      title: '¿Cambiar de RH ?',
      content: 'Al confirmar este dialogo, el ejecutivo comercia debe subir el nuevo RH desde su perfil.  ',
      buttons: {
          Confirmar: {
            btnClass: 'btn-blue',
            action:function () {   
                var rh='si';
             $.ajax({
                type : 'POST',
                url  : 'controlador/borrarRH',
                data :  {id:idcomision,rh:rh},
                success : function(data)
                {
                    $("#filter").focus();                 
                    //$("#bateriasOModal.bs.modal").hide();
                    $.ajax({
                        type : 'POST',
                        url : 'modal/comisionDataModal', 
                        data : {rowid:idcomision},
                        success : function(data)
                        {
                            $('.modal-body').html(data);
                        }
                    });
                }
            });        
            }
          },          
          Cancelar: {
            btnClass: 'btn-warning',
            action:function () {  
              $("#filter").focus(); 
              $.alert('No se realizo nigun cambio!');
            }
          }
      }
  });
});

//* Cambiar titular
$('.cambiaar').click(function(e) {
  var idcomision=$(this).val();
    $.ajax({
      type: 'POST',
      url  : 'ajax/fragments/CambiarTitular',
      data :  { idcomision: idcomision },
      success: (data) => {
        var fragmentRH = $('.change-owner');
        fragmentRH.html(data);
      }
    });
  });

$('.cambiaarTC').click(function(e) {
        var idcomision=$(this).val();
    var ntipc=$("#ntipc").val();
    var ndesc=$("#ndesc").val();
    $.confirm({
            theme: 'modern',
            title: '¿Cambiar de Tipo pago de comisión ?',
            content: 'cambiar te Tipo pago de comisión a '+ndesc,
            buttons: {
                Confirmar: {
                    btnClass: 'btn-blue',
                    action:function () {
                        var titularTC='si';
                        $.ajax({
                            type : 'POST',
                            url  : 'controlador/borrarRH',
                            data :  {id:idcomision,titularTC:titularTC,idtipc:ntipc},
                            success : function(data)
                            {
                                $("#filter").focus();
                                //$("#bateriasOModal.bs.modal").hide();
                                $.ajax({
                                    type : 'POST',
                                    url : 'modal/comisionDataModal',
                                    data : {rowid:idcomision},
                                    success : function(data)
                                    {
                                        $('.modal-body').html(data);
                                    }
                                });
                            }
                        });
                    }
                },
                Cancelar: {
                    btnClass: 'btn-warning',
                    action:function () {
                        $("#filter").focus();
                        $.alert('No se realizo nigun cambio!');
                    }
                }
            }
        });
    });



$('.eliminar').click(function(e) {
  var idcomision=$(this).val();
  $.confirm({
      theme: 'modern',
      title: '¿Desea eliminar esta comisión ?',
      content: 'eliminar de comisiones',
      buttons: {
          Confirmar: {
            btnClass: 'btn-blue',
            action:function () {   
                var eliminar='si';
             $.ajax({
                type : 'POST',
                url  : 'controlador/borrarRH',
                data :  {id:idcomision,eliminar:eliminar},
                success : function(data)
                {
                    $("#filter").focus();                 
                   $("#bateriasOModal").modal('toggle');
                 //   $.ajax({
                 //       type : 'POST',
                 //       url : 'modal/comisionDataModal', 
                 //       data : {rowid:idcomision},
                 //       success : function(data)
                 //       {
                 //           $('.modal-body').html(data);
                 //       }
                 //   });
                }
            });        
            }
          },          
          Cancelar: {
            btnClass: 'btn-warning',
            action:function () {  
              $("#filter").focus(); 
              $.alert('No se realizo nigun cambio!');
            }
          }
      }
  });
  });
   </script>
<script>
   $("#canalIdnew").bind("keyup focus",function()
           { 
           var canalId = $(this).val();
           if (canalId.length>=3){
           $.ajax({
                type : 'POST',
                url  : 'ajax/canalnew',
                data : {canalId:canalId},
                success : function(data)
                {
                $("#comisionHt").html(data);
                }
                });
           }else{
           $("#comisionHt").html("<p>Tienes que agregar el canal de la comisión</p>");
           $('#registrarNuevo').hide('slow');
           }
           return false;
           });


   $("#canalIdnew2").bind("keyup focus",function()
   {
       var canalId = $(this).val();
       if (canalId.length>=3){
           $.ajax({
               type : 'POST',
               url  : 'ajax/canalnew2',
               data : {canalId:canalId},
               success : function(data)
               {
                   $("#intermeFind").html(data);
               }
           });
       }else{
           $("#intermeFind").html("<p>Intermediario no encontrado</p>");
       }
       return false;
   });


   $('#agregar').show();
   $('#registrarNuevo').hide();
        $('#buscarExistente').click(function(){
            $('#registrarNuevo').hide('slow');
            $('.data').html('');
            $('#agregar').show();
        });

            $('#otroBanco').hide();
            $('#bancoCanal').change(function(){
                if ($(this).val()=='OTRO BANCO') {
                    $('#otroBanco').show();
                    $('#numeroCuenta').hide('slow');
                    $('#numeroCuenta').prop('required',false);
                    $('#nombreBanco').prop('required',true);
                    $('#cci').prop('required',true);
                    $('#cci1').prop('disabled',true);
                    $('#cci1').hide('slow');
                    $('#cci2').prop('name','cciOLD');
                }else{
                    $('#numeroCuenta').show();
                    $('#numeroCuenta').prop('required',true);
                    $('#otroBanco').hide('slow');
                    $('#nombreBanco').prop('required',false);
                    $('#cci').prop('required',false);
                    $('#cci1').show();
                    $('#cci2').prop('name','cci');
                }
            });
</script>
<?php
} 
?>


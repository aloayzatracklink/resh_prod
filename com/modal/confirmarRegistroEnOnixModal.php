 <?php 
if ($_POST['rowid']) {
  include_once("../../includes/init.php");
    if (strpos($_POST['rowid'], '.') !== false) {

    }else{
    $idOperacion=htmlspecialchars($_POST['rowid']);
    $ObjetoOperacion=Operacion::obtenerOperacionPorId($idOperacion);
    $vehiculoObj=Vehiculo::obtenerVehiculo($ObjetoOperacion->idvehiculo);
    $clienteObj=Cliente::obtenerClientePorVehiculo($ObjetoOperacion->idvehiculo,$ObjetoOperacion->idcliente);
  ?>
  <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
  <input type="hidden" name="idOperacion" value="<?= $idOperacion  ?>">
<?php if (!empty($ObjetoOperacion->datosfacturacionid)): ?>    
<?php 
  $objFacturacion=DatosFacturacion::obtenerDatosFacturacionPorId($ObjetoOperacion->datosfacturacionid);
 ?>
  <?php if (empty($objFacturacion->voucher)): ?> 
    <h3 class="text-center text-danger">Atención pago en caja</h3>
    <input type="hidden" name="pagoEnCaja" value="ok">
            <div class="row">
        <div class="col-md-12">
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
              <div class="form-control" data-trigger="fileinput">
                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                <span class="fileinput-filename"></span>
              </div>
              <span class="input-group-addon btn btn-default btn-file">
                <span class="fileinput-new">Si el pago ya se realizo adjuntar voucher o el recorte del deposito</span>
                <span class="fileinput-exists">Cambiar</span>
                <!-- <label for="">Seleccionar voucher de pago</label> -->
                <input type="file" name="voucher"  />
              </span>
              <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
            </div>  
        </div>
        </div>

  <?php else: ?>
    <div class="row">
    <img class="text-center col-md-6 col-md-offset-3" width="400" src="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" alt="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" >      
    </div>
  <div class="row">
    <a  class="btn btn-primary text-center  col-md-6 col-md-offset-3" href="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" target="_blank" >Descargar/ver Voucher</a>    
  </div>
  <input type="hidden" value="<?= $objFacturacion->voucher ?>" name="voucherAnterior">
  <?php endif ?>
<?php endif ?>
<?php if ($vehiculoObj): ?>
<?php 
if ($vehiculoObj->estado==1) {
  ?>
  <?php if ($clienteObj->enOnix=='si'): ?>
    <h3 class="text-center text-danger">Cliente ya esta registrado en onix</h3>
    <input type="hidden" name="enOnix" value="si">
  <?php endif ?>
    <input type="hidden" name="idcliente" value="<?= $clienteObj->idcliente ?>">
    <input type="hidden" name="idvehiculo" value="<?= $vehiculoObj->idvehiculo ?>">
    <input type="hidden" name="idFacturacion" value="<?= $ObjetoOperacion->datosfacturacionid ?>">
  <br>  
  <div class="row">
    <div class="col-md-12">
      <h3 class="text-center text-success">PLAN: <?= $vehiculoObj->plan ?></h3>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->dni ?>">       
      <?php else: ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->ruc ?>">        
      <?php endif ?>  
    </div>
    <div class="col-md-9">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->razonSocial ?>">       
      <?php endif ?>
    </div>
  </div>    
  <?php if ($clienteObj->tipoCliente=="Natural"): ?>
    <?php 
    if (!empty($clienteObj->departamento)) {
      $depatamentoObj=$clienteObj->obtenerDepartamentoPorId($clienteObj->departamento); 
      $provinciaObj=$clienteObj->otenerProvinciaPorId($clienteObj->provincia); 
      $distritoObj=$clienteObj->obtenerDistritoPorId($clienteObj->distrito);      
    }
           
    ?>

    <div class="row">
      <div class="col-md-4"><label for="departamento">Departamento:</label>
        <input style="border: none;" type="text" placeholder="Departamento" id="departamento" name="departamento" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $depatamentoObj->departamento ?>">                
      </div>
      <div class="col-md-4">
        <label for="provincia">Provincia:</label> 
        <input style="border: none;" type="text" placeholder="Departamento" id="provincia" name="provincia" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $provinciaObj->provincia ?>">       
      </div>
      <div class="col-md-4">
        <label for="distrito">Distrito: </label>
        <input style="border: none;" type="text" placeholder="Distrito" id="distrito" name="distrito" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $distritoObj->distrito ?>">        
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <label for="direccion">Direccion: </label>
        <input style="border: none;" type="text" placeholder="Direccion" id="direccion" name="direccion" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->direccion ?>">       
      </div>
      <div class="col-md-6">
        <label for="referencia">Referencia: </label>
        <input style="border: none;" type="text" placeholder="Referencia:" id="referencia" name="referencia" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->referencia ?>">     
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <label for="fechaNacimiento">Fecha de nacimiento: </label> 
        <input style="border: none;" type="text" placeholder="Fecha de nacimiento:" id="fechaNacimiento" name="fechaNacimiento"  class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->fechaNacimiento ?>" > 
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3">
      <label for="telefono">Celular: </label>
      <input style="border: none;" type="text" placeholder="Celular:" id="telefono" name="telefono" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->telefono ?>">        
    </div>
    <div class="col-md-3">
      <label for="telefono1">Telefono : </label>
      <input style="border: none;" type="text" placeholder="Telefono :" id="telefono1" name="telefono1" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->telefono1 ?>">     
    </div>
    <div class="col-md-3">
      <label for="correo">Correo: </label>
      <input style="border: none;" type="text" placeholder="Correo:" id="correo" name="correo" class="border-bottom form-control x2 text-lowercase" required value="<?php echo strtolower($clienteObj->correo) ?>">       
    </div>
    <div class="col-md-3">
      <label for="correo1">Correo alternativo: </label>
      <input style="border: none;" type="text" placeholder="Correo alternativo:" id="correo1" name="correo1" class="border-bottom form-control x2 text-lowercase"  value="<?php echo strtolower($clienteObj->correo1) ?>">        
    </div>
  </div>
<?php else: ?>
<!--   <?php if ($clienteObj->departamentoL==''): ?>
    <?php else: ?>
  <?php $depatamentoObjL=$clienteObj->obtenerDepartamentoPorId($clienteObj->departamentoL); 
          $provinciaObjL=$clienteObj->otenerProvinciaPorId($clienteObj->provinciaL); 
          $distritoObjL=$clienteObj->obtenerDistritoPorId($clienteObj->distritoL); ?>

  <?php endif ?> -->
  <div class="row">
    <div class="col-md-4">
      <label for="departamentoL">Departamento: </label>
      <input style="border: none;" type="text" placeholder="Departamento: " id="departamentoL" name="departamentoL" class="border-bottom form-control x2 text-uppercase" required  value="<?php echo $depatamentoObjL->departamento ?>">      
    </div>
    <div class="col-md-4">
      <label for="provinciaL">Provincia: </label>
      <input style="border: none;" type="text" placeholder="Provincia: " id="provinciaL" name="provinciaL" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $provinciaObjL->provincia ?>">
    </div>
    <div class="col-md-4">
      <label for="distritoL">Distrito: </label>
      <input style="border: none;" type="text" placeholder="Distrito: " id="distritoL" name="distritoL" class="border-bottom form-control x2 text-uppercase"   required value="<?php echo $distritoObjL->distrito ?>">      
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <label for="direccionL">Direccion: </label>
      <input style="border: none;" type="text" placeholder="Direccion: " id="direccionL" name="direccionL" class="border-bottom form-control x2 text-uppercase" required  value="<?php echo $clienteObj->direccionL ?>" >
    </div>

  </div>
  <div class="row">
    <div class="col-md-3">
      <label for="telefono">Celular: </label>
      <input style="border: none;" type="text" placeholder="Celular: " id="telefono" name="telefono" class="border-bottom form-control x2 text-uppercase" required=""  value="<?php echo $clienteObj->telefono ?>">
    </div>
    <div class="col-md-3">
      <label for="telefono1">Telefono : </label>
      <input style="border: none;" type="text" placeholder="Telefono: " id="telefono1" name="telefono1" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->telefono1 ?>">
    </div>
    <div class="col-md-3">
      <label for="correo">Correo: </label>
      <input style="border: none;" type="text" placeholder="Correo: " id="correo" name="correo" class="border-bottom form-control x2 text-uppercase" required="" value="<?php echo $clienteObj->correo ?>">
    </div>
    <div class="col-md-3">
      <label for="correo1">Correo alternativo: </label>
      <input style="border: none;" type="text" placeholder="Correo alternativo: " id="correo1" name="correo1" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->correo1 ?>">
    </div>
  </div>
<?php endif ?>
<?php if ($clienteObj->tipoCliente!="Natural"): ?>
  <?php if (!empty($clienteObj->departamento)): ?>
    
  <?php 
          // $depatamentoObj=$clienteObj->obtenerDepartamentoPorId($clienteObj->departamento); 
          // $provinciaObj=$clienteObj->otenerProvinciaPorId($clienteObj->provincia); 
          // $distritoObj=$clienteObj->obtenerDistritoPorId($clienteObj->distrito); 

          ?>
  <?php endif ?>
  <hr>
  <div class="row">
    <div class="row">
      <div class="col-md-12">
        <h4>Representante legal </h4>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <label for="dni">DNI: </label>
        <input style="border: none;" type="text" placeholder="DNI: " id="dni" name="dni" class="border-bottom form-control x2 text-uppercase"  value="<?= (!isset($clienteObj->dni))? $clienteObj->dni:'' ?>">
      </div>
      <div class="col-md-4">
        <label for="nombre">Nombres: </label>
        <input style="border: none;" type="text" placeholder="Nombres: " id="nombre" name="nombre" class="border-bottom form-control x2 text-uppercase"  value="<?= (!isset($clienteObj->nombre))?$clienteObj->nombre:''  ?>">
      </div>
      <div class="col-md-4">
        <label for="apellidosC">Apellidos </label>
        <input style="border: none;" type="text" placeholder="Nombres: " id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase"  value="<?= $clienteObj->apellidosC ?>">
      </div>
    </div><div class="row">
    <div class="col-md-12">
      <label for="fechaNacimiento">Fecha de nacimiento: </label> 
      <input style="border: none;" type="text" placeholder="Fecha de nacimiento: " id="fechaNacimiento" name="fechaNacimiento" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->fechaNacimiento ?>">
      <input type="hidden" name="fechaNacimiento" value="<?php echo $clienteObj->fechaNacimiento ?>"> 
    </div>
  </div> 
</div>
<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-4">
        <label for="departamento">Departamento: </label>
        <input style="border: none;" type="text" placeholder="Departamento:" id="departamento" name="departamento" class="border-bottom form-control x2 text-uppercase"  value="<?= (!empty($clienteObj->departamento))?$depatamentoObj->departamento:'' ?>">
      </div>
      <div class="col-md-4">
        <label for="provincia">Provincia: </label>
        <input style="border: none;" type="text" placeholder="Provincia:" id="provincia" name="provincia" class="border-bottom form-control x2 text-uppercase"  value="<?= (!empty($clienteObj->provincia))?$provinciaObj->provincia:'' ?>">
      </div>
      <div class="col-md-4">
        <label for="distrito">Distrito: </label>
        <input style="border: none;" type="text" placeholder="Distrito:" id="distrito" name="distrito" class="border-bottom form-control x2 text-uppercase"  value="<?= (!empty($clienteObj->distrito))?$distritoObj->distrito:'' ?>">
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <label for="direccion">Direccion: </label>
        <input style="border: none;" type="text" placeholder="Direccion:" id="direccion" name="direccion" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->direccion ?>">
      </div>
      <div class="col-md-6">
        <label for="referencia">Referencia: </label>
        <input style="border: none;" type="text" placeholder="Referencia:" id="referencia" name="referencia" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->referencia ?>">
      </div> 
    </div>
    <div class="row">
      <div class="col-md-3">
        <label for="telefono2">Celular: </label>
        <input style="border: none;" type="text" placeholder="Celular :" id="telefono2" name="telefono2" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->telefono2 ?>">
      </div>
      <div class="col-md-3">
        <label for="telefono3">Telefono : </label>
        <input style="border: none;" type="text" placeholder="Telefono:" id="telefono3" name="telefono3" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->telefono3 ?>">
      </div>
      <div class="col-md-3">
        <label for="correo2">Correo: </label>
        <input style="border: none;" type="text" placeholder="Correo:" id="correo2" name="correo2" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->correo2 ?>">
      </div>
      <div class="col-md-3">
        <label for="correo3">Correo alternativo: </label>
        <input style="border: none;" type="text" placeholder="Correo alternativo:" id="correo3" name="correo3" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->correo3 ?>">
      </div>
    </div>
  </div>    
</div>
<?php endif ?>
<hr>
<h3>Datos del vehiculo</h3>
<?php 
$vehiculoObj1=Vehiculo::obtenerClientePorIdCliente($clienteObj->idcliente,$vehiculoObj->idvehiculo);
// $n=1;
?>
<?php foreach ($vehiculoObj1 as $var): ?> 
  <div class="row"><hr>
    <div class="col-lg-12 field_wrapper"> 
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="field_name[0][]" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $var->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="">Chasis</label>
          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="field_name[1][]" class="border-bottom form-control text-uppercase" required value="<?php echo $var->chasis ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="motor" class="">Motor</label>
          <input style="border: none;" type="text" placeholder="Motor" id="motor" name="field_name[2][]" class="border-bottom form-control text-uppercase" required value="<?php echo $var->motor ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="marca" class="">Marca</label>
          <input style="border: none;" type="text" placeholder="Marca" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $var->marca ?>" >
        </div>
      </div> 
      <div class="row">
        <div class="form-group col-md-3">
          <label for="modelo" class="">Modelo</label>
          <input style="border: none;" type="text" placeholder="Modelo" id="modelo" name="field_name[4][]" class="border-bottom form-control text-capitalize"  required  value="<?php echo $var->modelo ?>">
        </div>
        <div class="form-group col-md-2">
          <label for="color" class="">Color</label>
          <input style="border: none;" type="text" placeholder="Color" id="color" name="field_name[5][]" class="border-bottom form-control x2 text-capitalize" required value="<?php echo $var->color ?>" >
        </div>
        <div class="form-group col-md-2">
          <label for="anio" class="">Año</label>
          <input style="border: none;" type="text" placeholder="Año" id="anio" name="field_name[6][]" class="border-bottom form-control" required value="<?php echo $var->anio ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="seguro" class="">Compañia de seguros</label>
          <input style="border: none;" type="text" placeholder="Compañia de seguros" id="seguro" name="field_name[7][]" class="seguro border-bottom form-control" required autocomplete="off" value="<?php echo $var->companiaSeguro ?>">
        </div>
        <div class="form-group col-md-2">
          <label for="concecionario" class="">Concesionario</label>
          <input style="border: none;" type="text" placeholder="Concesionario" id="concecionario" name="field_name[11][]" class="seguro border-bottom form-control" required autocomplete="off" value="<?php echo $var->concesionario ?>">
        </div>
      </div>
        <div class="row">
          <div class="col-md-6 form-group">
            <label for=""> Plan: <?= $var->plan ?></label>
            <input type="hidden" value="<?= $var->plan ?>" id="plan">
          </div>
          <div class="col-md-6 form-group">
             <?php include("../../includes/campanas.php") ?>
          </div>
        </div>   
      <div class="row">
        <div class="form-group col-md-4">
          <label for="numeroTramiteSeguro" class="">Numero tramite de seguro</label>
          <input style="border: none;" type="text" placeholder="Numero tramite de seguro" id="numeroTramiteSeguro" name="field_name[8][]" class="numeroTramiteSeguro border-bottom form-control text-capitalize"  value="<?php echo $var->numeroTramiteSeguro ?>"  >
        </div>
        <div class="form-group col-md-4">
          <label for="numeroPoliza" class="">N° de poliza</label>
          <input style="border: none;" type="text" placeholder="N° de poliza" id="numeroPoliza" name="field_name[9][]" class="border-bottom form-control text-capitalize"   value="<?php echo $var->numeroPoliza ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="corredor" class="">Corredor de seguros</label>
         <!--  <select  style="border: none;" class="corredor border-bottom form-control text-capitalize" required name="field_name[10][]">
            <option class='text-capitalize' value='"<?php echo $var->idcorredordeseguro ?>"'  selected ><?php 
              
              ?></option>
              <?php Vehiculo::obtenerCorredores() ?>
            </select> -->
             <input style="border: none;" type="text" placeholder="Corredor de seguros." id="corredor" name="field_name[10][]" class="border-bottom form-control text-capitalize"   value="<?= limpiar($var->idcorredordeseguro) ?>">
          </div>
      </div> 
    </div>
  </div> 
<?php endforeach ?> 
<hr><div class="hr-line-dashed"></div>
<?php if (($contactosAutorizadosTObj=Cliente::obtenerContactosAutorizados($clienteObj->idcliente))<>false): ?>
  <?php 
  $m=1;
  $contactosAutorizadosTObj=Cliente::obtenerContactosAutorizados($clienteObj->idcliente);
  ?>
  <h3>Contactos autorizados: </h3>
  <?php foreach ($contactosAutorizadosTObj as $var): ?>
    <h3 class="text-center">#<?php echo $m++ ?></h3>
    <div class="row">
      <div class="form-group col-md-3">
        <label for="dniC" class="">DNI: </label> <?php echo $var->dni ?>
        <input type="hidden" name="contactos[0][]" value="<?php echo $var->dni ?>" >
      </div>
      <div class="form-group col-md-3">
        <label for="nombreC" class="">Nombres: </label> <?php echo $var->nombre ?>
        <input type="hidden" name="contactos[1][]" value="<?php echo $var->nombre ?>" >
      </div>
      <div class="form-group col-md-3">
        <label for="apellidos" class="">Apellidos : </label> <?php echo $var->apellidos ?>
        <input type="hidden" name="contactos[2][]" value="<?php echo $var->apellidos ?>" >

      </div>
      <div class="form-group col-md-3">
        <label for="dniC" class="">Celular: </label> <?php echo $var->telefono ?>
        <input type="hidden" name="contactos[3][]" value="<?php echo $var->telefono ?>" >
      </div>

    </div>

    <div class="row">
      <div class="form-group col-md-3">
        <label for="telefono" class="">Telefono: </label> <?php echo $var->telefono1 ?>
        <input type="hidden" name="contactos[4][]" value="<?php echo $var->telefono1 ?>" >
      </div>
      <div class="form-group col-md-5">
        <label for="correo" class="">Correo: </label> <?php echo $var->correo ?>
        <input type="hidden" name="contactos[5][]" value="<?php echo $var->correo ?>" >
      </div>
      <div class="form-group col-md-4">
        <label for="Parentesco" class="">Parentesco: </label> <?php echo $var->parentesco ?>
        <input type="hidden" name="contactos[6][]" value="<?php echo $var->parentesco ?>" >
      </div>

      <div class="hr-line-dashed"></div>

    </div>

  <?php endforeach ?>
<?php else: ?>
  <h2 class="text-center">Sin contactos Autorizados</h2>
  <hr>  
<?php endif ?>
<hr>
Datos de canal.
<div class="row">
        <div class="form-group col-md-4">
          <label for="tipoCCnal" class="">Tipo</label>
          <input style="border: none;" type="text" placeholder="tipoCCnal" id="tipoCCnal" name="tipoCCnal" class="border-bottom form-control x2 text-uppercase" required value="<?= $objFacturacion->tipoCanal ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="nombreCCCanl" class="">Nombre</label>
          <input style="border: none;" type="text" placeholder="nombreCCCanl" id="nombreCCCanl" name="nombreCCCanl" class="border-bottom form-control text-uppercase" required value="<?= $objFacturacion->nombreCanal ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="EjecutCCanl" class="">Ejecutivo canal</label>
          <input style="border: none;" type="text" placeholder="EjecutCCanl" id="EjecutCCanl" name="EjecutCCanl" class="border-bottom form-control text-uppercase" required value="<?= $objFacturacion->ejecutivoCanal ?>" >
        </div>
</div> 
<hr><div class="hr-line-dashed"></div>
<div class="row">
  <h3>&nbsp;&nbsp;&nbsp;Datos de facturación:</h3>
  <h4 class='text-center text-warning'>&nbsp;&nbsp;&nbsp;<?=    $objFacturacion->observaciones ?></h4>
  <?php if ($objFacturacion->tipoDocumentoFacturacion!="Boleta"): ?>
  <div class="row">
        <div class="form-group col-md-3">
          <label for="tipoDocumentoFacturacion" class=""><?php echo $objFacturacion->tipoDocumentoFacturacion ?></label>
          <input style="border: none;" type="text" placeholder="tipoDocumentoFacturacion" id="tipoDocumentoFacturacion" name="tipoDocumentoFacturacion" class="border-bottom form-control text-capitalize"  required  value="<?php echo $objFacturacion->tipoDocumentoFacturacion ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="rucFacturacion" class="">rucFacturacion</label>
          <input style="border: none;" type="text" placeholder="rucFacturacion" id="rucFacturacion" name="rucFacturacion" class="border-bottom form-control x2 text-capitalize" required value="<?= strtoupper($objFacturacion->rucFacturacion) ?>" >
        </div>
        <div class="form-group col-md-5">
          <label for="razonSocialFacturacion" class="">razonSocialFacturacion</label>
          <input style="border: none;" type="text" placeholder="razonSocialFacturacion" id="razonSocialFacturacion" name="razonSocialFacturacion" class="border-bottom form-control" required value="<?=  strtoupper($objFacturacion->razonSocialFacturacion) ?>" > 
        </div>
      </div> 
      <div class="row">
        <div class="form-group col-md-8">
          <label for="direccionFiscal" class="">direccionFiscal</label>
          <input style="border: none;" type="text" placeholder="direccionFiscal" id="direccionFiscal" name="direccionFiscal" class="border-bottom form-control" required value="<?= strtoupper($objFacturacion->direccionFiscal) ?>" >
        </div>
        <div class="form-group col-md-4">
          <label for="telefonoFacturacion" class="">telefonoFacturacion</label>
          <input style="border: none;" type="text" placeholder="telefonoFacturacion" id="telefonoFacturacion" name="telefonoFacturacion" class="border-bottom form-control" required value="<?php echo $objFacturacion->telefonoFacturacion ?>" >
        </div>
      </div>

   <div class="row">
    <div class="col-md-8">
      <label for="entregaEnInstalacion">¿ Cliente desea que se entregue la factura al momento de la instalación ?</label>        
          </div>  
    <div class="col-md-2">
      <div class="switch">
        <div class="onoffswitch">
          <input type="checkbox" class="onoffswitch-checkbox" id="entregaEnInstalacion" value="si" name="entregaEnInstalacion"  onclick="direccionInstacion()" <?= $retVal = ($objFacturacion->entregaEnInstalacion=='si') ? 'checked' : "" ; ?>>
          <label class="onoffswitch-label" for="entregaEnInstalacion">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
          </label>
        </div>
      </div>
    </div>
    <div class="col-md-2"></div>
    </div>
      <div class="row">
        <div class="form-group col-md-6">
          <label for="direccionEntrega" class="sr-only">Dirección de engrega</label>
          <input style="border: none;" type="text" placeholder="Dirección de engrega" id="direccionEntrega" name="direccionEntrega" class="form-control border-bottom text-capitalize" value="<?= $objFacturacion->direccionEntrega ?>">
        </div>
        <div class="form-group col-md-6">
          <label for="referenciaEntrega" class="sr-only">Referencia de engrega</label>
          <input style="border: none;" type="text" placeholder="Referencia de engrega" id="referenciaEntrega" name="referenciaEntrega" class="form-control border-bottom text-capitalize" required="" value="<?= $objFacturacion->referenciaEntrega ?>">
        </div>
      </div>
      <div class="row">
        <div class="form-group col-md-4">
          <label for="distritoEntrega" class="sr-only">Distrito de entrega</label>
          <input style="border: none;" type="text" placeholder="Distrito de entrega" id="distritoEntrega" name="distritoEntrega" class="form-control border-bottom text-capitalize" required value="<?= $objFacturacion->distritoEntrega ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="horarioEntrega" class="sr-only">Horario de entrega</label>
          <input style="border: none;" type="text" placeholder="Horario de entrega" id="horarioEntrega" name="horarioEntrega" class="form-control border-bottom text-capitalize" required value="<?= $objFacturacion->fechaHoraDeEntrega ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="personaEntrega" class="sr-only">Persona de recepción de factura </label>
          <input style="border: none;" type="text" placeholder="Persona de recepción de factura" id="personaEntrega" name="personaEntrega" class="form-control border-bottom text-capitalize" required value="<?= $objFacturacion->recibidoPor ?>">
        </div>
      </div>
  <?php else: ?>
    <h4>&nbsp;&nbsp;&nbsp;<?php echo $objFacturacion->tipoDocumentoFacturacion ?></h4>
    <input type="hidden" name="tipoDocumentoFacturacion" value="<?php echo $objFacturacion->tipoDocumentoFacturacion ?>">
    <div class="row">
         <div class="form-group col-md-2">
          <label for="dniFacturacion" class="">DNI </label>
          <input style="border: none;" type="text" placeholder="dni boleta" id="dniFacturacion" name="dniFacturacion" class="form-control border-bottom text-capitalize" required value="<?= $objFacturacion->dniFacturacion ?>">
        </div>
         <div class="form-group col-md-4">
          <label for="Nombre:" class="">Nombre:</label>
          <input style="border: none;" type="text" placeholder="razonSocialFacturacion" id="razonSocialFacturacion" name="razonSocialFacturacion" class="border-bottom form-control" required value="<?=  strtoupper($objFacturacion->razonSocialFacturacion) ?>" > 
        </div>
        <div class="form-group col-md-6">
          <label for="direccionFiscal" class="">direccionFiscal</label>
          <input style="border: none;" type="text" placeholder="direccionFiscal" id="direccionFiscal" name="direccionFiscal" class="border-bottom form-control" required value="<?= strtoupper($objFacturacion->direccionFiscal) ?>" >
        </div>
      </div>    
       <div class="row">
    <div class="col-md-8">
      <label for="entregaEnInstalacion">¿ Cliente desea que se entregue la factura al momento de la instalación ?</label>        
          </div>  
    <div class="col-md-2">
      <div class="switch">
        <div class="onoffswitch">
          <input type="checkbox" class="onoffswitch-checkbox" id="entregaEnInstalacion" value="si" name="entregaEnInstalacion"  onclick="direccionInstacion()" checked >
          <label class="onoffswitch-label" for="entregaEnInstalacion">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
          </label>
        </div>
      </div>
    </div>
    <div class="col-md-2"></div>
    </div>
      <div class="row">
        <div class="form-group col-md-6">
          <label for="direccionEntrega" class="sr-only">Dirección de engrega</label>
          <input style="border: none;" type="text" placeholder="Dirección de engrega" id="direccionEntrega" name="direccionEntrega" class="form-control border-bottom text-capitalize" value="<?= $objFacturacion->direccionEntrega ?>">
        </div>
        <div class="form-group col-md-6">
          <label for="referenciaEntrega" class="sr-only">Referencia de engrega</label>
          <input style="border: none;" type="text" placeholder="Referencia de engrega" id="referenciaEntrega" name="referenciaEntrega" class="form-control border-bottom text-capitalize" required="" value="<?= $objFacturacion->referenciaEntrega ?>">
        </div>
      </div>
      <div class="row">
        <div class="form-group col-md-4">
          <label for="distritoEntrega" class="sr-only">Distrito de entrega</label>
          <input style="border: none;" type="text" placeholder="Distrito de entrega" id="distritoEntrega" name="distritoEntrega" class="form-control border-bottom text-capitalize" required value="<?= $objFacturacion->distritoEntrega ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="horarioEntrega" class="sr-only">Horario de entrega</label>
          <input style="border: none;" type="text" placeholder="Horario de entrega" id="horarioEntrega" name="horarioEntrega" class="form-control border-bottom text-capitalize" required value="<?= $objFacturacion->fechaHoraDeEntrega ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="personaEntrega" class="sr-only">Persona de recepción de factura </label>
          <input style="border: none;" type="text" placeholder="Persona de recepción de factura" id="personaEntrega" name="personaEntrega" class="form-control border-bottom text-capitalize" required value="<?= $objFacturacion->recibidoPor ?>">
        </div>
      </div>
  <?php endif ?>

</div>

<?php
}
?>

<?php endif ?>
<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../../assets/js/validacionFactura.pabel.js"></script>
<script> $('.camapanaEsconder').hide(); $('#campana').prop('disabled',true);
var plan = $("#plan").val();
  if (plan=='E-call' || plan=='E-call Plus' || plan=='Proteccion') {
    $('.camapanaEsconder').show();
  if (plan=='Basico' || plan=='Tranquilidad' || plan=='Proteccion') { }else{
    $('.camapanaEsconder').hide();
    $('#campana').prop('disabled',true);    
  }

</script>
<script>
<?php if ($datos=DatosFacturacion::horariosSugeridosParaEntregaDeFactura()): ?>
// entregaEnInstalacion
var entregaEnInstalacion=document.getElementById("entregaEnInstalacion");
  if (entregaEnInstalacion.checked) {
     direccionInstacion();
  }
$('#horarioEntrega').typeahead({
  source: [
    <?php foreach ($datos as $var): ?>
      {"name": "<?php echo $var->fecha ?>"},  
    <?php endforeach ?>
    ]
  }); 
<?php endif ?>

<?php if ($datosD=Cliente::otenerTodoLosDistritos()): ?>
$('#distritoEntrega').typeahead({
  source: [
    <?php foreach ($datosD as $var): ?>
      {"name": "<?php echo $var->distrito ?>"},  
    <?php endforeach ?>
    ]
  }); 
<?php endif ?>
  </script>


<?php
} 
} 
?> 

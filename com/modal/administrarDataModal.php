<?php 
if ($_POST['rowid']) {
    include_once("../../includes/init.php");
    $usuarioid=htmlspecialchars(htmlentities($_POST['rowid']));
    $usuarioData=Usuario::obtenerUsuario($usuarioid);
    $rolD=Usuario::obtenerRoldeUsuario($usuarioData->idrol) 
    // $comisionData=Comisiones::obtenerDatosDeComision($comisionid);
    // $clienDatos=Cliente::obtenerDatosDeclientePorId($comisionData->idcliente);
    // $vhDatos=Vehiculo::obtenerVehiculo($comisionData->idvehiculo);
    // $canalDatos=Canales::obtenerDatosCanalPorId($comisionData->idcanal);
    // $obMenu=Usuario::obtenerMenu($session->idusuario);
  ?>
  <input type="hidden" name="usuarioid" value="<?php echo $usuarioid ?>" >

  <div class="well" style="padding-top: 11px;padding-bottom: 10px;">
  <div class="row">
    <div class="row">
      <div class="form-group col-md-8">
        <label for="Observaciones" >Usuario</label>
        <input name="" style="border: none;" type="text" placeholder="Web" id="Observaciones" class="border-bottom form-control" readonly  value="<?= $usuarioData->usuarioNombre." ".$usuarioData->usuarioApellidoPaterno." ".$usuarioData->usuarioApellidoMaterno ?>">
      </div>
      <div class="form-group col-md-4">
        <label for="Observaciones" >Puesto</label>
        <input name="" style="border: none;" type="text" placeholder="Web" id="Observaciones" class="border-bottom form-control" readonly  value="<?= $rolD->cargo ?>">
      </div>
    </div>
   
  </div>
</div>
<hr>
<?php if ($usuarioData->idrol==778): ?>
    <div class="row">
        <label for="" class="text-center">Ejecutivo comerciales asignados</label>
        <?php if ($ejeAsO=Usuario::obtenerEjecutivosAsignados($usuarioid)): ?>
          <table class="table">                      
          <?php foreach ($ejeAsO as $var ): ?>
            <tr>
              <td><?= $var->usuarioNombre." ".$var->usuarioApellidoPaterno." ".$var->usuarioApellidoMaterno ?>              
              </td>
            </tr>
          <?php endforeach ?>
          </table>
        <?php else: ?>     
            <label>No tiene asigunado ningun ejecutivo comercial</label>
        <?php endif ?>
    </div>
<?php endif ?>
  <div class="row">
    <?php if ($usuarioData->comercialOperaciones==0): ?>
      <div class="form-group col-md-12">
      <h3 class="text-danger">Ejecutivo no tiene asignado ningun ejecutivo de operaciones comecial</h3>
      </div>
    <div class="form-group col-md-12">
      <label for="idEjecutivoOperaciones">Buscar</label>
      <input  style="border: none;" type="text" placeholder="Ejecutivo de operaciones" id="idEjecutivoOperaciones" name="idEjecutivoOperaciones" class="border-bottom form-control" required  >
    </div>
    <div class="form-group col-md-12">
      <div class="resultado" id="resultado"></div>
    </div>
    <?php else: ?>
      <?php $ejeOperaciones=Usuario::obtenerUsuario($usuarioData->comercialOperaciones); ?>
      <label for="" class="text-center">ejecutivo de operaciones</label>
      <table class="table">
        <thead>
          <th>Nombre</th>
          <th>Acción</th>
        </thead>
        <tbody>
          <tr>
            <td><?= $ejeOperaciones->usuarioNombre." ".$ejeOperaciones->usuarioApellidoPaterno." ".$ejeOperaciones->usuarioApellidoMaterno ?></td>
            <td><button class="btn btn-warning" style="padding-top: 0;padding-bottom: 0" value="<?= $ejeOperaciones->idusuario ?>" name="elimiarEjecutioOperacionesModal">Eliminar</button></td>
          </tr>
        </tbody>
      </table>
    <?php endif ?>
  </div>

<hr>


<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/js/bootstrap.min.js"></script>
<script src="../../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../../assets/js/inspinia.js"></script>
<script src="../../assets/js/plugins/pace/pace.min.js"></script>
<script src="../../assets/js/plugins/footable/footable.all.min.js"></script>
<!-- Steps -->
<script src="../../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script>
  
 $(document).ready(function(){
    

   
  
      // remote: 'ajax/ejecutivoCanal.php?query=%QUERY'

///7/para saber  
    $("#idEjecutivoOperaciones").bind("keyup focus",function()
    { 
      var idEjecutivoOperaciones = $(this).val();
      if (idEjecutivoOperaciones){
      $.ajax({
        type : 'POST',
        url  : 'ajax/ejecutivoOperacionesAjax',
        data : {nombre:idEjecutivoOperaciones},
        success : function(data)
        {
         $("#resultado").html(data);
        }
      });
      }
      return false;

    });
}); 
</script>

<?php
} 
?>


<?php
    if (isset($_POST['idTrama'])) {
    include_once("../../includes/init.php");
    $idTrama=limpiar(trim($_POST['idTrama']));
    $varTI=TramaIntegracionQL::obtenerTramaIntegracion($idTrama,'');
    $vTInt="";
    if($varTI->idoperacion!=""){
        $vTInt=TramaIntegracionQL::verEstadoOperacion($varTI->idoperacion);
    }

    #TramaIntegracion::asignarTrama($_POST['idTrama']);
    ?>

    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>
    <div class="showOPERA">
        <div class="well" style="padding-top: 11px;padding-bottom: 10px; ">
                    <?php if ($varTI): ?>
                    <?php if (!is_null($varTI->idsuariocargo)) {
                        $usuarioObj = Usuario::obtenerUsuario($varTI->idsuariocargo);
                    } ?>

                        <div class="col-md-12 text-center"><label  class='text-success' for="">A Cargo de : <?php if(!is_null($varTI->idsuariocargo)){echo $usuarioObj->usuarioNombre.' '.$usuarioObj->usuarioApellidoPaterno;}else{ echo 'Libre';}?></label></divas>
                        </div>

                        <div class="row">
                        <div class="form-group col-md-3">
                            <label for="Observaciones" ><?= $varTI->tipo_documento?> </label>
                            <input name="dniORuc" style="border: none;" type="text" placeholder="Web" id="Observaciones" readonly class="border-bottom form-control dnioruconix"  value="<?= $varTI->num_documento ?>">

                        </div>
                        <div class="form-group col-md-5">
                            <?php $apellidos=utf8_encode($varTI->apellido_asegurado) ?>
                            <input type="hidden" name="nombres" value="<?= $varTI->nombre_asegurado ?>">
                            <input type="hidden" name="apellidos" value="<?= $apellidos ?>">
                            <input type="hidden" name="anioF" value="<?= $varTI->anio ?>">
                            <input type="hidden" id='idtrama' name="idTrama" value="<?= $idTrama ?>">
                            <label for="numeroTecnico" >Apellidos :</label>
                            <input  style="border: none;" type="hidden" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->nombre_asegurado)) ? "$apellidos"." "."$varTI->nombre_asegurado" : "$apellidos" ; ?> ">
                            <input  style="border: none;" type="text" readonly id="apeSegura" name="apeSegura" class="border-bottom form-control" required value="<?= $apellidos ?> ">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="numeroTecnico" >Nombres :</label>
                           <input  style="border: none;" type="text" readonly id="nomSegura" name="nomSegura" class="border-bottom form-control" required value="<?= $varTI->nombre_asegurado ?> ">
                        </div>
                        </div>
                        <div class="row">
                        <div class="form-group col-md-4">
                            <label for="numeroTecnico" >Dirección :</label>
                            <input  style="border: none;" type="text" readonly id="direcSegura" name="direcSegura" class="border-bottom form-control" required value="<?= $varTI->direccion ?> ">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="numeroTecnico" >Correo :</label>
                            <input  style="border: none;" type="text" readonly id="correoSegura" name="correoSegura" class="border-bottom form-control" required value="<?= $varTI->correo ?> ">
                        </div>
                        <div class="col-md-2"><label for="estado" >Celular</label>
                            <input style="border: none;" type="text" readonly name="celular" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->celular)) ? "$varTI->celular" : "" ; ?>" >
                        </div>
                        <div class="col-md-2"><label for="estado" >Telefono</label>
                            <input style="border: none;" type="text" readonly name="telefono5" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->num_telefono)) ? "$varTI->num_telefono" : "" ; ?>">
                        </div>
                        </div>
                        <div class="row">
                        <div class="form-group col-md-4 text-danger">
                            <label for="plzdd" class=""># Póliza :</label>
                            <input  type="text" id='plzdd' readonly class="form-control border-bottom text-capitalize" required value="<?= $retVal = $varTI->policy ?>">
                        </div>
                        <div class="col-md-4 text-danger">
                            <label for="fechInivvv">Fecha Inicio Vigencia :</label>
                            <input style="border: none;" type="text" readonly placeholder="Fecha Inicio" id="fechInivvv" class="form-control border-bottom text-capitalize" value="<?= $retVal = $varTI->fecha_vigencia ?>">
                        </div>
                        <div class="col-md-4 text-danger">
                                <label for="fechInivvv">Fecha Fin Vigencia :</label>
                                <input style="border: none;" type="text" readonly placeholder="Fecha Fin" id="fechFInvvv" class="form-control border-bottom text-capitalize" value="<?= $retVal = $varTI->fecha_fin_vigencia ?>">
                       </div>
                    </div>
                    <input type="hidden" value="<?php echo $retVal = (!empty($varTI->num_telefono)) ? "$varTI->num_telefono" : "" ; ?>" name="telefono">
                    <input type="hidden" value="" name="telefono2">
                    <input type="hidden" name="correoCliente" value="<?php echo $retVal = (!empty($varTI->correo)) ? "$varTI->correo," : "$varTI->correo" ;echo $retVal = (!empty($varTI->correo)) ? "$varTI->correo" : "" ;  ?>">

                    </div>
        <hr>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="numeroTecnico" >Placa Vehiculo*</label>
                <input  style="border: none;"  type="text" placeholder="Placa" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->placa)) ? "$varTI->placa" : "" ; ?>"  >
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones">Marca *</label>
                <input required <?php echo $retVal = (!empty($varTI->marca)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->marca)) ? "$varTI->marca" : "" ; ?>" >
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones">Modelo*</label>
                <input required <?php echo $retVal = (!empty($varTI->modelo)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($varTI->modelo)) ? "$varTI->modelo" : "" ; ?>">
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones" >Color</label>
                <input  <?php echo $retVal = (!empty($varTI->color)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Color" id="Observaciones" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->color)) ? "$varTI->color" : "" ; ?>">
            </div>

        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="Observaciones" >Chasis*</label>
                <input required style="border: none;" type="text" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = $varTI->chasis; ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="Observaciones" >Motor*</label>
                <input required <?php echo $retVal = (!empty($varTI->motor)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Motor" id="Observaciones" name="motor" class="border-bottom form-control" value="<?php echo $retVal = $varTI->motor; ?>">
            </div>
            <input type="hidden" name="motor" value="<?php echo $retVal = $varTI->motor ?>">
            <div class="form-group col-md-4">
                <label for="seguro" class="">Seleccione Compañía de Seguros</label>
                <select  readonly style="border: none;" class="border-bottom form-control text-capitalize" required name="seguro" id="seguro">
                    <option class='text-capitalize' value="<?php echo $retVal = $varTI->compania ?>" selected ><?php echo $retVal = $varTI->compania ?></option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="Observaciones" >Año*</label>
                <input required <?php echo $retVal = (!empty($varTI->anio)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Año" id="anio" name="anioF" class="border-bottom form-control" value="<?php echo $retVal = $varTI->anio; ?>">
            </div>
            <div class="col-md-3">
                <label for="plan" class="">Plan *</label>
                <select readonly style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
                        <option class="" value="<?php echo $varTI->plan ?>"  selected ><?php echo $varTI->plan ?></option>
                </select>
            </div>

            <div class="form-group col-md-6 ">
                <?php $tt = TiempoProgramServAdic::obtenerTotalHorasProgramacion2($varTI->plan,$varTI->servicio_adicional)?>
                <p style="font-weight:bold">Servicios Adicionales *: <label class="canthoraprog2 label-success"> <?= $tt->total ?> Horas </label></p>
                <div class="servAdi">
                    <?php ServiciosAdicionales::obtenerServAdicionalSeleccionadoVerIdServ($varTI->servicio_adicional) ?>
                </div>
                <input type="hidden" id="canthoraprog" name="canthoraprog" value="<?= $tt->total ?>">
            </div>
        </div><br>
        <div class="row ">
            <div class="col-md-3">
                <label for="ejecutivoCanal" >Precio lista</label>
                <input  style="border: none;"  readonly type="text" placeholder="Precio lista" id="ejecutivoCanal" name="precioLista" class="border-bottom form-control"  value="<?php echo $varTI->precio_lista?>" >
            </div>
            <div class="col-md-3">
                <label for="time" >Tiempo *</label>
                <input  style="border: none;" readonly   type="text" placeholder="Tiempo *" id="time" name="tiempo" class=" tiempo border-bottom form-control"  autocomplete="off" value="<?php echo $varTI->tiempo ?>">
            </div>

            <div class="form-group col-md-6 camapanaEsconder">
                <label for="campana" class="">¿ Campaña Pacífico Gps Smart ?</label>
                <select  style="border: none;" readonly  class="tecnico border-bottom form-control" required name="campana" id="campana">
                    <option class="" value="<?php echo $varTI->campana ?>" selected><?php echo utf8_encode($varTI->campana)?></option>
                </select>
            </div>
            <input  style="border: none;"    type="hidden" placeholder="Tiempo *" id="idEjecutvio" name="idEjecutvio" class=" tiempo border-bottom form-control"  autocomplete="off" value="<?php echo $session->idusuario ?>">
            <input  style="border: none;" readonly   type="hidden" placeholder="Tiempo *" id="tipo" name="tipo" class=" tiempo border-bottom form-control"  autocomplete="off" value="<?php echo $varTI->tipo_transacción ?>">
        </div>
        <br>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="tipoCanal" class="">Seleccione canal *</label>
                <select style="border: none;"  name="tipoCanal" readonly="" class="cd-selec form-control border-bottom " required id="tipoCanal">
                    <option class='text-capitalize' value='<?php echo $varTI->canal ?>'  selected ><?php echo $varTI->canal ?></option>
                </select>
            </div>
            <div class="form-group col-md-2 a">
                <label for="nombreCanal" class="">Canal nombre</label>
                <input style="border: none;" readonly type="text" placeholder="Canal nombre" id="nombreCanal" name="nombreCanal" class="border-bottom form-control text-uppercase nombreCanal " autocomplete="off" required=""   value="<?php echo $varTI->canal_nombre ?>">
                <input style="border: none;" type="hidden" placeholder="Tienda" id="tienda" name="tienda" class="border-bottom form-control text-uppercase tienda " autocomplete="off"  value="" >
                <input style="border: none;" type="hidden" placeholder="Tienda" id="convenio" name="convenio" class="border-bottom form-control text-uppercase tienda " autocomplete="off"  value="" >
                <input style="border: none;" type="hidden" placeholder="Tienda" id="tienda" name="tienda" class="border-bottom form-control text-uppercase tienda " autocomplete="off"  value="" >
                <input style="border: none;" type="hidden" placeholder="Cobra Comision" id="comision" name="comision" class="border-bottom form-control text-uppercase ejecutivoCanalN" autocomplete="off" value="<?php echo $varTI->cobra_comision ?>" >
            </div>
            <div class="form-group col-md-4">
                <label for="ejecutivoCanalN" class="">Ejecutivo canal</label>
                <input style="border: none;"  readonly type="text" placeholder="Ejecutivo canal" id="ejecutivoCanalN" name="ejecutivoCanalN" class="border-bottom form-control text-uppercase ejecutivoCanalN" autocomplete="off" value="<?php echo $varTI->ejecutivo_canal ?>" >
            </div>
        </div>
        <div class="row">
            <h3 class="col-md-6">Datos de facturación </h3>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="Forma*" >Forma Pago</label>
                <select style="border: none;" id="formaPago" name="formaPago" class="form-control border-bottom" required  readonly >
                    <option value="<?php echo $varTI->forma_pago ?>" selected><?php echo $varTI->forma_pago ?></option>
                </select>
            </div>
            <div class="col-md-4">
                <label for="Forma*" >Documento Pago</label>
                <select style="border: none;" id="selectNa" name="tipoDocumentoFacturacion" class="cd-selec form-control border-bottom" required readonly >
                    <option value="<?php echo $varTI->doc_factura ?>" selected><?php echo $varTI->doc_factura ?></option>
                </select>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-8">
                <div class=" eFactura" style="">
                    <div class="row"><br>
                        <div class="form-group col-md-4">
                            <label for="rucFacturacion" class="">Ruc *</label>
                            <input readonly style="border: none;" type="number" placeholder="Ruc *" id="rucFacturacion" name="rucFacturacion" class="form-control x2 border-bottom" maxlength="12" minlength="10" value="<?= $retVal = $varTI->ruc_factura; ?>"  >
                        </div>
                        <div class="form-group col-md-8">
                            <label for="razonSocialFacturacion" class="">Razón Social *</label>
                            <input readonly style="border: none;" type="text" placeholder="Razón Social *" id="razonSocialFacturacion" name="razonSocialFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = $varTI->razonSocial_Factura; ?>" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-8">
                            <label for="direccionFiscalFacturacion" class="sr-only">Dirección Fiscal *</label>
                            <input readonly style="border: none;" type="text" placeholder="Dirección Fiscal *" id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="form-control border-bottom text-capitalize" value="<?= $varTI->direccion_factura ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="telefonoFacturacion" class="sr-only">Teléfono</label>
                            <input style="border: none;" readonly type="text" placeholder="Teléfono" id="telefonoFacturacion" name="telefonoFacturacion" class="form-control border-bottom text-capitalize" value="">

                            <input style="border: none;" type="hidden" placeholder="Dirección de engrega" id="direccionEntrega" name="direccionEntrega" class="form-control border-bottom text-capitalize" value="">
                            <input style="border: none;" type="hidden" placeholder="Referencia de engrega" id="referenciaEntrega" name="referenciaEntrega" class="form-control border-bottom text-capitalize" required="" value="">
                            <input style="border: none;" type="hidden" placeholder="Distrito de entrega" id="distritoEntrega" name="distritoEntrega" class="form-control border-bottom text-capitalize" required autocomplete="off" value="">
                            <input style="border: none;" type="hidden" placeholder="Horario de entrega" id="horarioEntrega" name="horarioEntrega" class="form-control border-bottom text-capitalize" autocomplete="off" required value="">
                            <input style="border: none;" type="hidden" placeholder="Persona de recepción de factura" id="personaEntrega" name="personaEntrega" class="form-control border-bottom text-capitalize" required value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                    <label for="fechIni" class="text-success">Fecha Inicio Vigencia :</label>
                    <input style="border: none;" type="text" readonly placeholder="Fecha Inicio" id="fechIni" name="fechIni" class="form-control border-bottom text-capitalize" required value="<?= $retVal = $varTI->fecha_vigencia ?>">
            </div>
         x
            <div class="col-md-4">
                <label for="Poliza" class="text-success"># Póliza :</label>
                <input style="border: none;" type="text" readonly placeholder="Poliza" id="Poliza" name="Poliza" class="form-control border-bottom text-capitalize" required value="<?= $retVal = $varTI->policy ?>">
            </div>
        </div>
        <br>
        <div class="showTipo">
            <div class="row">
                <h3 class="col-md-6">Datos de instalación</h3>
                <div class="col-md-6">
                    <label for="pendienteInstacion">Pendiente datos de instalación</label>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="nombreContacto" class="">Nombre Contacto *</label>
                    <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContacto" name="nombreContacto" class="form-control x2 text-capitalize border-bottom " required="" value="<?= $varTI->apellido_asegurado." ".$varTI->nombre_asegurado ?>"" >
                </div>
                <div class="form-group col-md-6">
                    <label for="numeroContacto" class="">Número Contacto *</label>
                    <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContacto" name="numeroContacto" class="form-control x2 text-capitalize border-bottom" required="" value="<?= $varTI->celular ?>"">
                </div>
            </div>

            <hr>
            <div class="row">

                <die class="form-group col-md-12">

                    <label for="seguimientoTrama" class="">Seguimiento</label>
<div class="input-group">
                <input type="text" class="form-control" placeholder="Seguimiento" id="seguimientoTrama" >
                <span class="input-group-btn">
                    <button type="button" id="registrarSeguiminto" class="btn btn-primary">Registrar Seguimiento </button>
                </span>
            </div>
                </die>
                
            </div>

            <ul id='segimientohtml'>
                <?php foreach(TramaIntegracionQL::getSEg($idTrama) as $var ): ?>
                    <li><?= $var->fecha ?> <b><?= $var->body ?></b></li>
                <?php endforeach ?>
            </ul>
            <hr>



        </div>
        <div class="modal-footer">
            <button type="button" id='cancelopemodalplkm' class="btn btn-white" data-dismiss="modal">Cancelar</button>
            <button type="submit" name="guardarQL" id="guardarQL" class="btn btn-primary" value="<?php echo Token::generar() ?>">Guardar</button>
        </div>
    </div>
    
    <script>
        $("#registrarSeguiminto").click(function(){
            // alert(servAd) ;seguimientoTrama
            var body=$('#seguimientoTrama').val()
            var idtrama = $("#idtrama").val()
            var today = new Date();
            var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()+' '+today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            if(body.length>0){
                $.ajax({
                    type : 'POST',
                    url  : 'ajax/registrarSegQL',
                    data : {body:body,idtrama:idtrama},
                    success : function(data)
                    {
                        $('#seguimientoTrama').val('')
                        $("#segimientohtml").append('<li> '+date+' <b>'+body+'</b></li>');
                    }
                });
             }
            return false;
        });

    </script>

<?php endif ?>

<?php
}
?>

<?php   
if ($_POST['rowid']) {
  include_once("../../includes/init.php");
  $idOperacion=htmlspecialchars(htmlentities($_POST['rowid']));
  $ObjetoOperacion=Operacion::obtenerOperacionPorId($idOperacion);
  $vehiculoObj=Vehiculo::obtenerVehiculo($ObjetoOperacion->idvehiculo);
  $clienteObj=Cliente::obtenerClientePorVehiculo($ObjetoOperacion->idvehiculo,$ObjetoOperacion->idcliente);
  $clienteObjModal=new Cliente;


  $lista = Operacion::obtenerFrecuentes($session->idusuario);
?>
<input type="hidden" name="numeroOrte" value="<?= $ObjetoOperacion->numeroOrte ?>">
<link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
<link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<link href="../assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<link href="../assets/css/animate.css" rel="stylesheet">
<style>
    .ibox-content > .sk-spinner {
        display: none;
    }

    .ibox-content.sk-loading {
        position: relative;
    }
    .ibox-content.sk-loading:after {
        content: '';
        background-color: rgba(255, 255, 255, 0.7);
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }
    .ibox-content.sk-loading > .sk-spinner {
        display: block;
        position: absolute;
        top: 40%;
        left: 0;
        right: 0;
        z-index: 2000;
    }
</style>
<?php if (!empty($ObjetoOperacion->datosfacturacionid)): ?>
    <?php
    $objFacturacion=DatosFacturacion::obtenerDatosFacturacionPorId($ObjetoOperacion->datosfacturacionid);
   ?>
  <?php if (empty($objFacturacion->voucher)): ?> 
    <h3 class="text-center text-danger">Atención pago en caja</h3>
    <input type="hidden" name="pagoEnCaja" value="ok">
  <?php else: ?>
 <img class="text-center" width="800" src="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" alt="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" >
 <div class="row">
    <a  class="btn btn-primary text-center  col-md-6 col-md-offset-3" href="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" target="_blank" >Descargar/ver Voucher</a>
    
  </div>
  <?php endif ?> 
<hr>
<?php endif ?>
<?php if ($vehiculoObj){ ?>  
    <br>  
  <div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>

        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->dni ?>">       
      <?php else: ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->ruc ?>">        
      <?php endif ?>  
    </div>
    <div class="col-md-9">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->razonSocial ?>">       
      <?php endif ?>
    </div>
  </div>    

<?php if ($clienteObj->tipoCliente!="Natural"): ?>

  <hr>
    <div class="row">
      <div class="col-md-12">
        <h4>Representante legal </h4>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <label for="dni">DNI: </label>
        <input style="border: none;" type="text" placeholder="DNI: " id="dni" name="dni" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->dni ?>">
      </div>
      <div class="col-md-4">
        <label for="nombre">Nombres: </label>
        <input style="border: none;" type="text" placeholder="Nombres: " id="nombre" name="nombre" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->nombre ?>">
      </div>
      <div class="col-md-4">
        <label for="apellidosC">Apellidos </label>
        <input style="border: none;" type="text" placeholder="Nombres: " id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->apellidosC ?>">
      </div>
    </div>

<?php endif ?>
<!-- Datos de contacto here please -->
<h3>Datos del vehiculo</h3>
<?php 
  $vehiculoObj1=Vehiculo::obtenerClientePorIdCliente($clienteObj->idcliente,$vehiculoObj->idvehiculo);
?>


<?php foreach ($vehiculoObj1 as $var): ?> 
  <div class="row"><hr>
    <div class="col-lg-12 field_wrapper"> 
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="placa" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $var->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="">Chasis</label>
          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="chasis" class="border-bottom form-control text-uppercase" required value="<?php echo $var->chasis ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="motor" class="">Motor</label>
          <input style="border: none;" type="text" placeholder="Motor" id="motor" name="motor" class="border-bottom form-control text-uppercase" required value="<?php echo $var->motor ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="marca" class="">Marca</label>
          <input style="border: none;" type="text" placeholder="Marca" id="marca" name="marca" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $var->marca ?>" >
        </div>
      </div> 
      <div class="row">
        <div class="form-group col-md-3">
          <label for="modelo" class="">Modelo</label>
          <input style="border: none;" type="text" placeholder="Modelo" id="modelo" name="modelo" class="border-bottom form-control text-capitalize"  required  value="<?php echo $var->modelo ?>">
        </div>
        <div class="form-group col-md-2">
          <label for="color" class="">Color</label>
          <input style="border: none;" type="text" placeholder="Color" id="color" name="color" class="border-bottom form-control x2 text-capitalize" required value="<?php echo $var->color ?>" >
        </div>
        <div class="form-group col-md-2">
          <label for="anio" class="">Año</label>
          <input style="border: none;" type="text" placeholder="Año" id="anio" name="anio" class="border-bottom form-control" required value="<?php echo $var->anio ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="seguro" class="">Compañia de seguros</label>
          <input style="border: none;" type="text" placeholder="Compañia de seguros" id="seguro" name="seguro" class="seguro border-bottom form-control" required autocomplete="off" value="<?php echo $var->companiaSeguro ?>">
        </div>
        <div class="form-group col-md-2">
          <label for="concecionario" class="">Concesionarios</label>
          <input style="border: none;" type="text" placeholder="Concesionario" id="concecionario" name="concecionario" class="seguro border-bottom form-control"   autocomplete="off" value="<?php echo $var->concesionario ?>">
        </div>
      </div> 
    </div>
  </div>
    <div class="row">
        <div class="col-md-6">
            <label for="plan" class="">Plan *</label>
            <select  style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan" disabled>
                <option value="cero" data-type="0" >Seleccione Plan *</option>
                <?php Cliente::obtenerPlanSelected($var->plan) ?>
            </select>
        </div>
        <div class="form-group col-md-6 ">
            <p style="font-weight:bold" class="text-danger">Servicios Adicionales Seleccionados : <label class="canthoraprog2 label-success"> 0 Horas </label></p>
            <?php ServiciosAdicionales::obtenerServAdicionalSeleccionadoVer($idOperacion) ?>
            <input type="hidden" id="canthoraprog" name="canthoraprog" value="0">
        </div>
    </div>
<?php endforeach ?>
<hr><div class="hr-line-dashed"></div>
Datos de canal.
<?php $datosC=Canales::obtenerDatosCanalPorId($objFacturacion->idcanal) ?>
<div class="row">
        <div class="form-group col-md-4">
          <label for="tipoCCnal" class="">Tipo</label>
          <input style="border: none;" type="text" placeholder="tipoCCnal" id="tipoCCnal" name="tipoCCnal" class="border-bottom form-control x2 text-uppercase" value="<?= $objFacturacion->tipoCanal ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="nombreCCCanl" class="">Nombre</label>
          <input style="border: none;" type="text" placeholder="nombreCCCanl" id="nombreCCCanl" name="nombreCCCanl" class="border-bottom form-control text-uppercase" value="<?= $objFacturacion->nombreCanal ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="EjecutCCanl" class="">Ejecutivo canal</label>
          <input style="border: none;" type="text" placeholder="EjecutCCanl" id="EjecutCCanl" name="EjecutCCanl" class="border-bottom form-control text-uppercase" value="<?= $objFacturacion->ejecutivoCanal ?>" >
        </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12 text-center"><h1>Datos de instalación</h1></div>
</div>
<hr>
<!-- MEJORAR PARA QUE APLIQUE CUANDO SEAN INSTALACIONES EN DOMICILIO DE LCIENTE  -->
<h4> <small>Lugar de instalación sugerido por el ejecutivo de ventas </small><?php echo $ObjetoOperacion->lugarInstalacion ?> <?php echo $retVal = (!empty($ObjetoOperacion->nombreConcecionarioInstalicion)) ? $ObjetoOperacion->nombreConcecionarioInstalicion : "" ; ?> <small>Hora sugerida:</small> <?= ($ObjetoOperacion->fechaInstalacion=='' ? 'Sin fecha y hora sugerida':formatoHaceEn($ObjetoOperacion->fechaInstalacion)) ?></h4>
<h3 class='text-center text-danger'><?= $ObjetoOperacion->observacion ?></h3>
<?php
   if ($ObjetoOperacion->estado=='321') {
       ?>
           <h3 class='text-center text-danger'>Motivo de la reprogramación:</br><b> <?= $ObjetoOperacion->observacion ?></b> </h3>
       <?php
   } 
?>
<hr>
<input type="hidden" name="idoperacion" value="<?php echo $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idvh" value="<?php echo $vehiculoObj->idvehiculo ?>">
<input type="hidden" name="datosfacturacionid" value="<?php echo $ObjetoOperacion->datosfacturacionid ?>">
<div role="tablist">
  <input type="radio" title="taller" name="lugarInstacion" id="taller" role="tab" aria-controls="taller-panel"  value="Taller" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Taller")?"checked":"" ?> />
  <input type="radio" title="domicilio" name="lugarInstacion" id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Domicilio Cliente")?"checked":"" ?> />
  <input type="radio" title="concesionario" name="lugarInstacion" id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Concesionario")?"checked":"" ?> />
  <div class="tabs" aria-hidden="true">
    <label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
    <label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
    <label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
  </div>
  <div class="panels">
    <ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">
      <!-- <h2>TALLER</h2>
      <p>Av. Paseo de la República 4925 con Angamos Este </p>
      <p>Surquillo, Lima, Lima</p> -->

        <h2><?= $lista->lugarInstalacion ?> | <?= $lista->observacion ?></h2>
        <p><?= $lista->direccionInstacion  ?></p>
        <p>
            <?php $dep=$clienteObjModal->obtenerDepartamentoPorId($lista->departamentoi) ?><?= $dep->departamento ?>
            &nbsp;|&nbsp;
            <?php $prov=$clienteObjModal->otenerProvinciaPorId($lista->provinciai) ?><?php echo $prov->provincia ?>
            &nbsp;|&nbsp;
            <?php $di=$clienteObjModal->obtenerDistritoPorId($lista->distritoi) ?><?= $di->distrito ?>
        </p>

        <input type="hidden" name="direccionTaller" value="<?= $lista->direccionInstacion  ?>">
        <input type="hidden" name="disT" id="distritoTallerid" value="<?= $lista->distritoi  ?>">
        <input type="hidden" name="disT" value="<?= $lista->distritoi  ?>">
        <input type="hidden" name="proT" value="<?= $lista->provinciai  ?>">
        <input type="hidden" name="depT" value="<?= $lista->departamentoi  ?>">
      <div class="row">
        <!--  <div class="form-group  col-md-4">
            <label for="departamentoInstalacionT" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionT border-bottom form-control text-capitalize tal" required name="departamentoInstalacionT" id="departamentoInstalacionT">                  
                    <option class="text-capitalize" value="15"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId('15') ?><?= $departamento->departamento ?></option>
            </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionT" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionT border-bottom form-control text-capitalize tal" required name="provinciaInstalacionT" id="provinciaInstalacionT" >
                    <?php //if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId('127') ?><?php echo $provincia->provincia ?></option>
                     <?php
                      //} ?>                 
            </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionT" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionT border-bottom form-control text-capitalize tal" required name="distritoInstalacionT"  id="distritoInstalacionT" >
                <?php// if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId('1291') ?><?= $distrito->distrito ?></option>
                    <?php
                      //} ?>                 
              </select>           
          </div> -->
        </div>
     <!-- <input type="hidden" name="direccionTaller" value="Av. Paseo de la República 4925 con Angamos Este - Surquillo ">
      <input type="hidden" name="referenciaT" value=""> -->
<div class="row">
        <div class="form-group col-md-4">   
                    <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control tal tala fecha"  id="fechaIT" name="fechaIT" autocomplete="off" required>
          </div>
        </div>
        <div class="form-group col-md-4">
            <label for="turno" class="">Turno</label>
                <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn form-control border-bottom btn btn-info" >Seleccionar hora</button>
                <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posT" class="hora form-control border-bottom"  required="" readonly>
        </div>
        <div class="form-group col-md-4">
                <label for="horaS" class="">Hora seleccionada</label>
                <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaT" class="horaVer tala tal form-control border-bottom"  required="" readonly>
        </div>
    </div>
    </ul>

    <ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab"> 
      <h2>Domicilio Cliente</h2>
       <?php 
       if (!empty($clienteObj->departamento) && !empty($clienteObj->provincia)&& !empty($clienteObj->distrito)) {
          $depatamentoObj=$clienteObj->obtenerDepartamentoPorId($clienteObj->departamento); 
          $provinciaObj=$clienteObj->otenerProvinciaPorId($clienteObj->provincia); 
          $distritoObj=$clienteObj->obtenerDistritoPorId($clienteObj->distrito);        
       }           
    ?> 
      <div class="row">
         <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionD" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize dom" required name="departamentoInstalacionD" id="departamentoInstalacionD">
                    <?php if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                        <option class="text-capitalize " value=""  selected  disabled="">Seleccione departamento</option>
                            <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                          <?php if ($objDepartamento): ?>
                            <?php foreach ($objDepartamento as $var): ?>
                              <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                            <?php endforeach ?>
                          <?php else: ?>
                          <?php endif ?>
                      <?php
                    }else{
                      ?>
                         <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
                      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                      <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                      <?php else: ?>
                      <?php endif ?>

                      <?php
                      } ?>                 
                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionD" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize dom" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
                    <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
                     <?php
                      } ?>                 
            </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionD" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize dom" required name="distritoInstalacionD"  id="distritoInstalacionD" >
                <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
                    <?php
                      } ?>                 
              </select>           
          </div>

        </div>
        <div class="row">
          <div class="form-group  col-md-12">
            <label for="direccionInstalacionD" class="">Direccion*</label>
            <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionD" name="direccionInstalacionD"  class="form-control border-bottom dom" value="<?= (!empty($ObjetoOperacion->direccionInstacion))?$ObjetoOperacion->direccionInstacion : ''  ?>">
          </div>
        </div>  
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="referenciaInstalacionD" class="">Referencia *</label>
            <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" name="referenciaInstalacionD" class="form-control border-bottom  dom" value="<?= (!empty($ObjetoOperacion->referenciaInstalacion)) ? $ObjetoOperacion->referenciaInstalacion:'' ?>" >
          </div>
          
          <!-- <div class="form-group col-md-6">
            <label for="cod" class="">Cordenadas Domicilio *</label>
            <input style="border: none;" type="text" placeholder="Cordenadas Domicilio *" id="cod" name="cordenadasD" class="form-control x2 text-capitalize border-bottom dom" value="<?= (!empty($ObjetoOperacion->coordenadasInstalacion))? $ObjetoOperacion->coordenadasInstalacion:'' ?>">
          </div> taller-link-r -->
          <input style="border: none;" type="hidden" placeholder="Cordenadas Domicilio *" id="cod" name="cordenadasD" class="form-control x2 text-capitalize border-bottom dom" value="<?= (!empty($ObjetoOperacion->coordenadasInstalacion))? $ObjetoOperacion->coordenadasInstalacion:'' ?>">

          <div class="form-group col-md-3">
            <label for="latitud" class="">Latitud*</label>
            <input style="border: none;" type="text" placeholder="EJEMPLO: -12.098523" id="latitud" name="latitud" class="form-control x2 text-capitalize border-bottom dom" value="<?= (!empty($ObjetoOperacion->latitud))? $ObjetoOperacion->latitud:'' ?>">
          </div>
          <div class="form-group col-md-3">
            <label for="longitud" class="">Longitud*</label>
            <input style="border: none;" type="text" placeholder="EJEMPLO: -77.013813" id="longitud" name="longitud" class="form-control x2 text-capitalize border-bottom dom" value="<?= (!empty($ObjetoOperacion->longitud))? $ObjetoOperacion->longitud:'' ?>">
          </div>
          
        </div>  
    <div class="row">
        <div class="form-group col-md-4">   
            <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
              <input type="text" class="form-control dom domo fecha"  id="fechaID" name="fechaID" autocomplete="off" required>
          </div>
        </div>
        <div class="form-group col-md-4">
            <label for="turno" class="">Turno</label>
                <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Seleccionar hora</button>
                <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posD" class="hora form-control border-bottom"  required="" readonly>
        </div>
        <div class="form-group col-md-4">
                <label for="horaS" class="">Hora seleccionada</label>
                <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaD" class="horaVer domo dom  form-control border-bottom"  required="" readonly>
        </div>
         </div>
    </div> 
  </ul>

  <ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">
    <div class="row">
      <div class="col-md-6"><h2>Concesionario</h2></div>
      <div class="col-md-6">
        <label for="concesionarioInstalacion" class="">Nombre de concesionario*</label>
        <input style="border: none;" type="text" placeholder="Nombre de concesionario*" required id="concesionarioInstalacion" name="concesionarioInstalacion" class="form-control border-bottom con" value="<?= (!empty($ObjetoOperacion->nombreConcecionarioInstalicion))?$ObjetoOperacion->nombreConcecionarioInstalicion:''  ?>" >
      </div>
    </div>
    <div class="row">
     <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionC" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionC border-bottom form-control text-capitalize con" required name="departamentoInstalacionC" id="departamentoInstalacionC">
                    <?php if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                        <option class="text-capitalize" value=""  selected  disabled="">Seleccione departamento</option>
                            <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                          <?php if ($objDepartamento): ?>
                            <?php foreach ($objDepartamento as $var): ?>
                              <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                            <?php endforeach ?>
                          <?php else: ?>
                          <?php endif ?>
                      <?php
                    }else{
                      ?>
                         <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
                      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                      <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                      <?php else: ?>
                      <?php endif ?>

                      <?php
                      } ?>                 


                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionC" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionC border-bottom form-control text-capitalize con" required name="provinciaInstalacionC" id="provinciaInstalacionC" >
                    <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
                     <?php
                      } ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionC" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionC border-bottom form-control text-capitalize con" required name="distritoInstalacionC"  id="distritoInstalacionC" >
                <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
                    <?php
                      } ?>                 
              </select>           
          </div>

        </div>
      <div class="row">
        <div class="form-group  col-md-6">
          <label for="direccionInstalacionC" class="">Direccion*</label>
          <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom con" value="<?= $ObjetoOperacion->direccionInstacion ?>" >
        </div>
        <div class="form-group  col-md-6">
          <label for="referenciaInstalacionC" class="">Referencia *</label>
          <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom con"  value="<?= $ObjetoOperacion->referenciaInstalacion ?>" >
        </div>
      </div>    
    <div class="row">
                  <div class="form-group col-md-4">   
                      <label  class="">Fecha</label>
                      <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control con cono fecha"  id="fechaIC" name="fechaIC" autocomplete="off" required>
                      </div>
                  </div>

        <div class="form-group col-md-4">
            <label for="turno" class="">Turno</label>
                <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Seleccionar hora</button>
                <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posC" class="hora form-control border-bottom"  required="" readonly>
        </div>
        <div class="form-group col-md-4">
                <label for="horaS" class="">Hora seleccionada</label>
                <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaC" class="horaVer cono con form-control border-bottom"  required="" readonly>
        </div>

                </div>
           </div>
</ul>
</div>
    <div class="ibox" id="ibox1">
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-double-bounce">
                <div class="sk-double-bounce1"></div>
                <div class="sk-double-bounce2"></div>
            </div>
            <div id="programacionHtml">
            </div>
        </div>
    </div>
<div class="row">
        <div class="form-group col-md-6">
          <label for="nombreContacto" class="">Nombre Contacto *</label>
          <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContacto" name="nombreContacto" class="form-control x2 text-capitalize border-bottom " required="" value= "<?= $ObjetoOperacion->nombreContacto ?>">
        </div>
        <div class="form-group col-md-6">
        <label for="numeroContacto" class="">Número Contacto *</label>
        <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContacto" name="numeroContacto" class="form-control x2 text-capitalize border-bottom" required="" value="<?= $ObjetoOperacion->numeroContacto ?>">
      </div>
    </div>
<div class="row">
  <div class="form-group col-md-12">
    <label for="observaciong" class="">Observaciones </label>
    <input style="border: none;" type="text" placeholder="Observaciones " id="observaciong" name="observaciong" class="form-control x2 border-bottom focusUrgenteta" >
  </div>
</div>
    <script src="../../assets/bootbox/bootbox.all.js"></script>
<script>
//horaVer
$(document).ready(function(){
    $( "input[name='lugarInstacion']" ).on( "click",function(){
        $('#programacionHtml').html('');
        $('.horaVer').val('');
        $('.fecha').val('');
        validar();
    });
    validar();
});
$(".departamentoInstalacionD").click(function(){provincias4();});
$(".departamentoInstalacionC").click(function(){provincias5();});
$(".provinciaInstalacionD").click(function(){distritos4();});
$(".provinciaInstalacionC").click(function(){distritos5();});
function provincias4(){
  var iddepartamento = $(".departamentoInstalacionD").val();
  $.ajax({        
    type : 'POST',
    url  : '../com/ajax/provincia',
    data :  'iddepartamento='+ iddepartamento,
    success : function(data)
    {
      $(".provinciaInstalacionD").html(data);
    }
  });
  return false;
}
function provincias5(){
  var iddepartamento = $(".departamentoInstalacionC").val();
  $.ajax({        
    type : 'POST',
    url  : '../com/ajax/provincia',
    data :  'iddepartamento='+ iddepartamento,
    success : function(data)
    {
      $(".provinciaInstalacionC").html(data);
    }
  });
  return false;
}

  function distritos4(){
      var idprovincia = $(".provinciaInstalacionD").val();
      $.ajax({        
        type : 'POST',
        url  : '../com/ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionD").html(data)
        }
      });
      return false;
    }
    function distritos5(){
      var idprovincia = $(".provinciaInstalacionC").val();
      $.ajax({        
        type : 'POST',
        url  : '../com/ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionC").html(data)
        }
      });
      return false;
    }   

<?php $fechaA=date('Y-m-d') ?>
  jQuery.datetimepicker.setLocale('es');
  $('.fecha').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y-m-d', 
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
  });

  function lstProg(){
      var lugarI=$( "input[name='lugarInstacion']:checked" ).val();
      var distrito="0";
      var tthinstala=$('#canthoraprog').val();
      var cita = 'General';
      var concecion="0";var lugarT='';

      if (lugarI=='Taller') {
          var fecha = $('#fechaIT').val();
          distrito='1291';
          lugarT='Taller';
      }else if (lugarI=='Domicilio Cliente') {
          var fecha = $('#fechaID').val();
          distrito=$('#distritoInstalacionD').val();
          lugarT='Domicilio';
      }else if (lugarI=='Concesionario') {
          var fecha = $('#fechaIC').val();
          var concecion=$('#concesionarioInstalacion').val();
          var cita = concecion;
          distrito=$('#distritoInstalacionC').val();
          lugarT='Concesionario';
      }
      var cod = document.getElementById("plan").value;
      servAd = $('[name="servad[]"]:checked').length;

      if (fecha!='' && cod!='' && servAd!=0 &&(distrito==0 || distrito!=null) && concecion!='') {
          $(".horabtn").off( "click" );
          $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
          $.ajax({
              type : 'POST',
              url  : '../includes/tbProgramacion',
              data : {fecha:fecha,cita:cita,tthinstala:tthinstala,iddist:distrito,lugart:lugarT},
              success : function(data)
              {
                  $("#programacionHtml").html(data);
                  $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                  $(".horabtn").on("click",lstProg).find(".horabtn");
              }
          });
      }else{
          $("#programacionHtml").html('<h3 class="text-center text-info">Primero debe seleccionar Plan,Servicios Adicionales,fecha y/o Distrito y/o Nombre Concesionario.</h3>');
      }
      return false;

  }


function validaDP() {
    var lugarI = $("input[name='lugarInstacion']:checked").val();
    if (lugarI == 'Taller') {
        var fecha = $('#fechaIT').val();
    } else if (lugarI == 'Domicilio Cliente') {
        var fecha = $('#fechaID').val();
    } else if (lugarI == 'Concesionario') {
        var fecha = $('#fechaIC').val();
    }
    var hora = $(".horaVer").val();
    var canthoraprog = $('#canthoraprog').val();
    var idtec = $('input[name="posT"]').val();

    $.ajax({
        type: 'POST',
        url: 'ajax/validarRegistroTB',
        data:'tipo=save&fecha='+fecha+'&idtec='+idtec+"&canthoraprog=" + canthoraprog.trim() + "&hora=" + hora,
       //  data :  'tipo=save&fecha=2019-11-27&idtec=2&canthoraprog=2&hora=12:00',
        success: function (data) {
            if (data.trim() == 'dp') {
                $("#agregarVehiculoBttn").prop("disabled",true);
                $("#programacionHtml").html('');
                $(".horabtn").html('Seleccionar hora');
                $(".horaVer").val('');
                bootbox.alert({
                    title: "<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                    message: "<label>El horario Seleccionado("+hora+") ya fue Registrado,Por Favor Seleccione otro*</label>",
                    size: 'small',
                    callback: function () {
                    }
                });
            }else if(data.trim()=='Ok'){
                $("#agregarVehiculoBttn").prop("disabled",false)
            }
        }
    });
}


$('#departamentoInstalacionD,#departamentoInstalacionC,#provinciaInstalacionD,#provinciaInstalacionC,#distritoInstalacionD,#distritoInstalacionC').change(function(){
    $("#programacionHtml").html('');
    $(".horabtn").html('Seleccionar hora');
    $(".horaVer").val('');
});

var eventos={};
eventos.frejcar =function(){
    validaDP();
};
eventos.interval = setInterval(eventos.frejcar, 3000);


$(".horabtn").click(function(){
     lstProg();
    });
$("#mostrar").hide();
$('.fecha').change(function(){
    $("#programacionHtml").html('');
    $(".horabtn").html('Seleccionar hora');
    $(".horaVer").val('');
});
  ///////////////////////////////////////////////////

function validar(){
  var lugarI=$( "input[name='lugarInstacion']:checked" ).val()
  if (lugarI=='Taller') {
      $('.con').prop('disabled', true);
      $('.dom').prop('disabled', true);
      $('.tal').prop('disabled', false);
  }else if (lugarI=='Domicilio Cliente') {
    $('.con').prop('disabled', true);
      $('.dom').prop('disabled', false);
      $('.tal').prop('disabled', true);     
  }else if (lugarI=='Concesionario') {
    $('.con').prop('disabled', false);
      $('.dom').prop('disabled', true);
      $('.tal').prop('disabled', true);
  }
}

function tthoras() {
    var arr="";
    var seleccionado = $("#plan").find('option:selected');
    var idtec = seleccionado.data('type');
    var arr = $('[name="servad[]"]:checked').map(function(){
        return this.value;
    }).get();

    var str = arr.join(',');
    if( str==='' && (idtec === undefined)){
        str='0';
        idtec='0';
    }

    $.ajax({
        type : 'POST',
        url  : 'ajax/cantidadHoraProgramacion',
        data :  'idCheck='+str+"&idTec="+idtec,
        success : function(data)
        {
            if(data!=''){
                $("#canthoraprog").val(data);
                $(".canthoraprog2").text(data+" Horas");
            }else{
                console.log("No hay data");
            }
        }
    });
    return false;
}
<?php if ($datosDopeCon=Operacion::obtenerConcesionarioInstalacion()): ?>
$('#concesionarioInstalacion').typeahead({
    source: [
        <?php foreach ($datosDopeCon as $var): ?>
        {"name": "<?php echo $var->nombreConcecionarioInstalicion ?>"},
        <?php endforeach ?>
    ]
});
<?php endif ?>

 $(document).ready(function(){  tthoras();
  $( "input[name='lugarInstacion']" ).on( "click",validar);   
  validar();
<?php
if($objFacturacion->tipoDocumentoFacturacion=='telefonica' or $objFacturacion->rucFacturacion=='20550858461'){
?>
      $('.horaVer').prop('disabled', true);
      $('.horaVer').prop('required', false);
<?php
}
?>
});
</script>
<?php
}
?>
<?php
} 
?> 


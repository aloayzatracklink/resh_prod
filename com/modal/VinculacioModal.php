<?php
    if (isset($_POST['idTrama'])) {
    include_once("../../includes/init.php");
    $idTrama=limpiar(trim($_POST['idTrama']));
    $varTI=TramaIntegracionTY::getInfoVinculacionById($idTrama);
    $vTInt="";


        $idPlan = 0;
        if(strpos($varTI->plan,"PERSONAS")){
            $idPlan=20;
        }
        if(strpos($varTI->plan,"EMPRESAS")){
            $idPlan=21;
        }


        $idPlan2 = 0;
        if(strpos($varTI->plan2,"PERSONAS")){
            $idPlan2=20;
        }
        if(strpos($varTI->plan2,"EMPRESAS")){
            $idPlan2=21;
        }
    $precioTra = TramaIntegracionTY::getPrecioPlan($idPlan,$varTI->tiempo);
    $precioTra2 = TramaIntegracionTY::getPrecioPlan($idPlan2,$varTI->tiempo2);
    #TramaIntegracion::asignarTrama($_POST['idTrama']);
    ?>

    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>
    <div class="showOPERA row ">


        <div class="col-xs-6" style="border-right: 1.5px solid #837e7e;">
            <div class="well" style="padding-top: 11px;padding-bottom: 10px; ">
                <div class="row">
                    <div class="col-xs-12 center" style="text-align: center;margin-bottom: 25px;">
                        <label style=" font-weight: 800; font-size: 20px; color: #1c7cc1;">Datos Pre-Registro</label>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="Observaciones" ><?= $varTI->tipo_documento2?> </label>
                        <input name="dniORuc" style="border: none;" type="text" placeholder="Web" id="Observaciones" readonly class="border-bottom form-control dnioruconix"  value="<?= $varTI->num_documento2 ?>">

                    </div>
                    <div class="form-group col-md-5">
                        <?php $apellidos2=utf8_encode($varTI->apellido_asegurado2) ?>
                        <label for="numeroTecnico" >Apellidos :</label>
                        <input  style="border: none;" type="hidden" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->nombre_asegurado2)) ? "$apellidos2"." "."$varTI->nombre_asegurado2" : "$apellidos2" ; ?> ">
                        <input  style="border: none;" type="text" readonly id="apeSegura" name="apeSegura" class="border-bottom form-control" required value="<?= $apellidos2 ?> ">

                    </div>
                    <div class="form-group col-md-4">
                        <label for="numeroTecnico" >Nombres :</label>
                        <input  style="border: none;" type="text" readonly id="nomSegura" name="nomSegura" class="border-bottom form-control" required value="<?= $varTI->nombre_asegurado2?> ">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="numeroTecnico" >Dirección :</label>
                        <input  style="border: none;" type="text" readonly id="direcSegura" name="direcSegura" class="border-bottom form-control" required value="<?= $varTI->direccion2?> ">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="numeroTecnico" >Correo :</label>
                        <input  style="border: none;" type="text" readonly id="correoSegura" name="correoSegura" class="border-bottom form-control" required value="<?= $varTI->correo2 ?> ">
                    </div>
                    <div class="col-md-2"><label for="estado" >Celular</label>
                        <input style="border: none;" type="text" readonly name="celular" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal =((!empty($varTI->celular21)) ? "$varTI->celular21": "").((!empty($varTI->celular21)) ? ",".($varTI->celular22=='NULL' ? '':$varTI->celular22):""); ?>" >
                    </div>
                    <div class="col-md-2"><label for="estado" >Telefono</label>
                        <input style="border: none;" type="text" readonly name="telefono5" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->num_telefono2)) ? "$varTI->num_telefono2" : "" ; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4 text-danger">
                        <label for="mailocto11" >CORREO OCTO (CC) :</label>
                        <input type="text" id="mailocto11" readonly class=" danger border-bottom form-control" required value="toyota-sftp@tracklink.pe">
                    </div>
                    <div class="form-group col-md-4 text-danger">
                        <label for="mailocto11" >Concesionario madre:</label>
                        <input type="text" id="dlMadre" readonly class=" border-bottom form-control" value="<?= $varTI->dlr_madre2 ?>">
                    </div>
                    <div class="form-group col-md-4 ">
                        <label for="mailocto11" >Ejecutivo:</label>
                        <input type="text" id="dlMadre" readonly class=" border-bottom form-control" value="<?= $varTI->ejecutivo_venta2 ?>">
                    </div>
                </div>

            </div>
            <hr>
            <div class="row">
                <div class="form-group col-md-2">
                    <label for="numeroTecnico" >Placa Vehiculo*</label>
                    <input readonly  style="border: none;"  <?php echo $retVal = (!empty($vTInt)) ? ($varTI->estado2!="16"?"":"readonly") : "" ; ?> type="text" placeholder="Placa" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->placa2)) ? "$varTI->placa2" : "" ; ?>"  >
                </div>
                <div class="form-group col-md-2">
                    <label for="numeroTecnico" >CBU*</label>
                    <input   style="border: none;"  type="text"  readonly placeholder="cbu" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->cbu2)) ? "$varTI->cbu2" : "" ; ?>"  >
                </div>
                <div class="form-group col-md-2">
                    <label for="Observaciones">Marca *</label>
                    <input readonly required <?php echo $retVal = (!empty($varTI->marca2)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="marca" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?= $varTI->marca2 ?>" >
                </div>
                <div class="form-group col-md-2">
                    <label for="Observaciones">Modelo*</label>
                    <input readonly required <?php echo $retVal = (!empty($varTI->modelo2)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="modelo" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($varTI->modelo2)) ? "$varTI->modelo2" : "" ; ?>">
                </div>
                <div class="form-group col-md-2">
                    <label for="Observaciones" >Version</label>
                    <input readonly <?php echo $retVal = (!empty($varTI->version2)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Version" id="Observaciones" name="version" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->version2)) ? "$varTI->version2" : "" ; ?>">
                </div>

            </div>
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="Observaciones" >Chasis*</label>
                    <input readonly required <?php echo $retVal = (!empty($vTInt)) ? ($varTI->estado2!="16"?"":"readonly") : "" ; ?> style="border: none;" type="text" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = $varTI->chasis2; ?>">
                </div>
                <div class="form-group col-md-3">
                    <label for="Observaciones" >Motor*</label>
                    <input required <?php echo $retVal = (!empty($varTI->motor2)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Motor" id="Observaciones" name="motor" class="border-bottom form-control" value="<?php echo $retVal = $varTI->motor2; ?>">
                </div>

                <div class="form-group col-md-2">
                    <label for="Observaciones" >Color</label>
                    <input readonly <?php echo $retVal = (!empty($varTI->colorcelular2)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Color" id="Observaciones" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->color2)) ? "$varTI->color2" : "" ; ?>">
                </div>
                <input type="hidden" name="motor" value="<?php echo $retVal = $varTI->motor2 ?>">
                <div class="form-group col-md-3">
                    <label for="seguro" class="">Seleccione Compañía de Seguros</label>
                    <select  readonly style="border: none;" class="border-bottom form-control text-capitalize" required name="seguro" id="seguro">
                        <option class='text-capitalize' value="<?php echo $retVal = $varTI->compania2 ?>" selected ><?php echo $retVal = $varTI->compania2 ?></option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="Observaciones" >Año Fabricación*</label>
                    <input required <?php echo $retVal = (!empty($varTI->anio2)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Año" id="anio" name="anioF" class="border-bottom form-control" value="<?php echo $retVal = $varTI->anio2; ?>">
                </div>
                <div class="col-md-3">
                    <label for="plan" class="">Plan *</label>
                    <select readonly style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
                        <option class="" value="<?php echo $varTI->plan2 ?>"  selected ><?php echo $varTI->plan2 ?></option>
                    </select>
                </div>

            </div><br>
            <div class="row ">
                <div class="form-group col-md-2">
                    <label for="Observaciones" >Año Modelo*</label>
                    <input required <?php echo $retVal = (!empty($varTI->anio_modelo2)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Año" id="anio_modelo" name="anioMD" class="border-bottom form-control" value="<?php echo $retVal = $varTI->anio_modelo2; ?>">
                </div>
                <div class="col-md-2">
                    <label for="ejecutivoCanal" >Precio Tracklink</label>
                    <input  style="border: none;"  readonly required type="text" placeholder="Precio lista" id="ejecutivoCanal" name="precioLista" class="border-bottom form-control"  value="<?=($precioTra2)?($precioTra2->precio):""?>" >
                </div>
                <div class="col-md-2">
                    <label for="ejecutivoCanal" >Precio Toyota</label>
                    <input  style="border: none;"  readonly type="text" placeholder="Precio toyota" id="ejecutivoCanal" name="precioListaTy" class="border-bottom form-control"  value="<?php echo $varTI->precio_lista2?>" >
                </div>
                <div class="col-md-3">
                    <label for="time" >Tiempo *</label>
                    <input  style="border: none;" readonly   type="text" placeholder="Tiempo *" id="time" name="tiempo" class=" tiempo border-bottom form-control"  autocomplete="off" value="<?php echo $varTI->tiempo2 ?>">
                </div>
            </div>

        </div>
        <div class="col-xs-6">
        <div class="well" style="padding-top: 11px;padding-bottom: 10px; ">
                    <?php if ($varTI): ?>

            <div class="row">
                <div class="col-xs-12 center" style="text-align: center;margin-bottom: 25px;">
                    <label style=" font-weight: 800; font-size: 20px; color: #1c7cc1;">Datos Vinculación</label>
                </div>
            </div>
            <div class="row">
                        <div class="form-group col-md-3">
                            <label for="Observaciones" ><?= $varTI->tipo_documento?> </label>
                            <input name="dniORuc" style="border: none;" type="text" placeholder="Web" id="Observaciones" readonly class="border-bottom form-control dnioruconix"  value="<?= $varTI->num_documento ?>">

                        </div>
                        <div class="form-group col-md-5">
                            <?php $apellidos=utf8_encode($varTI->apellido_asegurado) ?>
                            <input type="hidden" name="nombres" value="<?= $varTI->nombre_asegurado ?>">
                            <input type="hidden" name="apellidos" value="<?= $apellidos ?>">
                            <input type="hidden" name="anioF" value="<?= $varTI->anio ?>">
                            <input type="hidden" id='idtrama' name="idTrama" value="<?= $idTrama ?>">
                            <label for="numeroTecnico" >Apellidos :</label>
                            <input  style="border: none;" type="hidden" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->nombre_asegurado)) ? "$apellidos"." "."$varTI->nombre_asegurado" : "$apellidos" ; ?> ">
                            <input  style="border: none;" type="text" readonly id="apeSegura" name="apeSegura" class="border-bottom form-control" required value="<?= $apellidos ?> ">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="numeroTecnico" >Nombres :</label>
                           <input  style="border: none;" type="text" readonly id="nomSegura" name="nomSegura" class="border-bottom form-control" required value="<?= $varTI->nombre_asegurado ?> ">
                        </div>
                        </div>
                        <div class="row">
                        <div class="form-group col-md-4">
                            <label for="numeroTecnico" >Dirección :</label>
                            <input  style="border: none;" type="text" readonly id="direcSegura" name="direcSegura" class="border-bottom form-control" required value="<?= $varTI->direccion ?> ">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="numeroTecnico" >Correo :</label>
                            <input  style="border: none;" type="text" readonly id="correoSegura" name="correoSegura" class="border-bottom form-control" required value="<?= $varTI->correo ?> ">
                        </div>
                        <div class="col-md-2"><label for="estado" >Celular</label>
                            <input style="border: none;" type="text" readonly name="celular" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal =((!empty($varTI->celular)) ? "$varTI->celular": "").((!empty($varTI->celular2)) ? ",".($varTI->celular2=='NULL' ? '':$varTI->celular2):""); ?>" >
                        </div>
                        <div class="col-md-2"><label for="estado" >Telefono</label>
                            <input style="border: none;" type="text" readonly name="telefono5" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->num_telefono)) ? "$varTI->num_telefono" : "" ; ?>">
                        </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4 text-danger">
                                <label for="mailocto11" >CORREO OCTO (CC) :</label>
                                <input type="text" id="mailocto11" readonly class=" danger border-bottom form-control" required value="toyota-sftp@tracklink.pe">
                            </div>
                            <div class="form-group col-md-4 text-danger">
                                <label for="mailocto11" >Concesionario madre:</label>
                                <input type="text" id="dlMadre" readonly class=" border-bottom form-control" value="<?= $varTI->dlr_madre ?>">
                            </div>
                            <div class="form-group col-md-4 ">
                                <label for="mailocto11" >Ejecutivo:</label>
                                <input type="text" id="dlMadre" readonly class=" border-bottom form-control" value="<?= $varTI->ejecutivo_venta ?>">
                            </div>
                    </div>

                    </div>
        <hr>
        <div class="row">
            <div class="form-group col-md-2">
                <label for="numeroTecnico" >Placa Vehiculo*</label>
                <input readonly  style="border: none;"  <?php echo $retVal = (!empty($vTInt)) ? ($varTI->estado!="16"?"":"readonly") : "" ; ?> type="text" placeholder="Placa" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->placa)) ? "$varTI->placa" : "" ; ?>"  >
            </div>
            <div class="form-group col-md-2">
                <label for="numeroTecnico" >CBU*</label>
                <input   style="border: none;"  type="text"  readonly placeholder="Placa" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->cbu)) ? "$varTI->cbu" : "" ; ?>"  >
            </div>
            <div class="form-group col-md-2">
                <label for="Observaciones">Marca *</label>
                <input readonly required <?php echo $retVal = (!empty($varTI->marca)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->marca)) ? "$varTI->marca" : "" ; ?>" >
            </div>
            <div class="form-group col-md-2">
                <label for="Observaciones">Modelo*</label>
                <input readonly required <?php echo $retVal = (!empty($varTI->modelo)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($varTI->modelo)) ? "$varTI->modelo" : "" ; ?>">
            </div>
            <div class="form-group col-md-2">
                <label for="Observaciones" >Version</label>
                <input readonly <?php echo $retVal = (!empty($varTI->version)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Version" id="Observaciones" name="version" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->version)) ? "$varTI->version" : "" ; ?>">
            </div>

        </div>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="Observaciones" >Chasis*</label>
                <input readonly required <?php echo $retVal = (!empty($vTInt)) ? ($varTI->estado!="16"?"":"readonly") : "" ; ?> style="border: none;" type="text" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = $varTI->chasis; ?>">
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones" >Motor*</label>
                <input required <?php echo $retVal = (!empty($varTI->motor)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Motor" id="Observaciones" name="motor" class="border-bottom form-control" value="<?php echo $retVal = $varTI->motor; ?>">
            </div>

            <div class="form-group col-md-2">
                <label for="Observaciones" >Color</label>
                <input readonly <?php echo $retVal = (!empty($varTI->color)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Color" id="Observaciones" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->color)) ? "$varTI->color" : "" ; ?>">
            </div>
            <input type="hidden" name="motor" value="<?php echo $retVal = $varTI->motor ?>">
            <div class="form-group col-md-3">
                <label for="seguro" class="">Seleccione Compañía de Seguros</label>
                <select  readonly style="border: none;" class="border-bottom form-control text-capitalize" required name="seguro" id="seguro">
                    <option class='text-capitalize' value="<?php echo $retVal = $varTI->compania ?>" selected ><?php echo $retVal = $varTI->compania ?></option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="Observaciones" >Año Fabricación*</label>
                <input required <?php echo $retVal = (!empty($varTI->anio)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Año" id="anio" name="anioF" class="border-bottom form-control" value="<?php echo $retVal = $varTI->anio; ?>">
            </div>
            <div class="col-md-3">
                <label for="plan" class="">Plan *</label>
                <select readonly style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
                    <option class="" value="<?php echo $varTI->plan ?>"  selected ><?php echo $varTI->plan ?></option>
                </select>
            </div>

        </div><br>
        <div class="row ">
            <div class="form-group col-md-2">
                <label for="Observaciones" >Año Modelo*</label>
                <input required <?php echo $retVal = (!empty($varTI->anio_modelo)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Año" id="anio_modelo" name="anioMD" class="border-bottom form-control" value="<?php echo $retVal = $varTI->anio_modelo; ?>">
            </div>
            <div class="col-md-2">
                <label for="ejecutivoCanal" >Precio Tracklink</label>
                <input  style="border: none;"  readonly required type="text" placeholder="Precio lista" id="ejecutivoCanal" name="precioLista" class="border-bottom form-control"  value="<?=($precioTra)?($precioTra->precio):""?>" >
            </div>
            <div class="col-md-2">
                <label for="ejecutivoCanal" >Precio Toyota</label>
                <input  style="border: none;"  readonly type="text" placeholder="Precio lista" id="ejecutivoCanal" name="precioListaTy" class="border-bottom form-control"  value="<?php echo $varTI->precio_lista?>" >
            </div>
            <div class="col-md-3">
                <label for="time" >Tiempo *</label>
                <input  style="border: none;" readonly   type="text" placeholder="Tiempo *" id="time" name="tiempo" class=" tiempo border-bottom form-control"  autocomplete="off" value="<?php echo $varTI->tiempo ?>">
            </div>
        </div>
            </div>
    </div>
<?php endif ?>


        <?php
}
?>

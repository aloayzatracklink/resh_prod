<?php 

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

ini_set('memory_limit', '800M');


if ($_POST['rowid']) {
  include_once("../../includes/init.php");

  //$lista = Operacion::obtenerFrecuentes($session->idusuario);
  $idrow=htmlspecialchars(htmlentities($_POST['rowid']));

  // Get past uid and add 1
  $newConn = new BaseDatos();
  
  $tablas               = new DatosTablasClass();
  $reclamoPlazo         = $tablas->datosReclamoPlazo();
  $reclamoEstado        = $tablas->datosReclamoEstado();
  $reclamoCanal         = $tablas->datosReclamoCanal();
  $reclamoNivelAtencion = $tablas->datosReclamoNivelAtencion();
  $areas                = $tablas->datosAreas();
  $clientes             = $tablas->datosClientes(); //agregado

  $dtClass = new DatosTablasClass();
  $uid = $dtClass->getLastUidReclamo();

  if($idrow!="null"){
    $modo = "editar";
    $reclamoClienteReporte = new ReclamoClienteReporteClass();
    $reclamoCliente = $reclamoClienteReporte->obtenerReporteReclamosClientePorId($idrow);
    $reclamoCliente = $reclamoCliente[0];
    $onix = new Onix();
    
    $vreclamo_cliente         = $reclamoCliente->vreclamo_cliente;
    $fecha_reclamo            = $reclamoCliente->fecha_reclamo;
    $idejecutivo_responsable  = $reclamoCliente->fk_idejecutivo_responsable;
    $idarea_detecta           = $reclamoCliente->fk_idarea_detecta;
    $idreclamo_estado         = $reclamoCliente->fk_idreclamo_estado;
    $idreclamo_canal          = $reclamoCliente->fk_idreclamo_canal;
    $idreclamo_nivel_atencion = $reclamoCliente->fk_idreclamo_nivel_atencion;
    $cliente_nombre           = $reclamoCliente->cliente_nombre;
    // $cliente_id               = $reclamoCliente->nombre_cliente;
    $idvehiculo               = $reclamoCliente->vehiculo_chasis;
    $idchasis                 = $onix->chasislOnixPorIdChasis($reclamoCliente->vehiculo_chasis);
    // $vehiculos                = $tablas->datosVehiculos($cliente_id, $idvehiculo);
    $solicitud_reclamo        = $reclamoCliente->tsolicitud_reclamo;
    $motivo_reclamo           = $reclamoCliente->tmotivo_reclamo;
    $informacion_brindada     = $reclamoCliente->tinformacion_brindada;
    $area_responsable         = $reclamoCliente->fk_idarea_responsable;
    $persona_cargo            = $reclamoCliente->fk_idpersona_cargo_reclamo;
    $personasACargo           = $tablas->datosUsuariosArea($area_responsable, $persona_cargo);
    $idreclamo_plazo          = $reclamoCliente->idreclamo_plazo;
    $resolucion_reclamo       = $reclamoCliente->tresolucion_reclamo;

    $usuarioLogin=Usuario::obtenerUsuario($idejecutivo_responsable);
    $areaEjecutivo = $tablas->datosAreas($idarea_detecta);
    $ejecutivo_responsable = $usuarioLogin->usuarioNombre." ".$usuarioLogin->usuarioApellidoPaterno." ".$usuarioLogin->usuarioApellidoMaterno;
 
  }else{
    $modo                     = "nuevo";
    $vreclamo_cliente         = $uid;
    $fecha_reclamo            = "";
    $idreclamo_estado         = "";
    $idreclamo_canal          = "";
    $idreclamo_nivel_atencion = "";
    $cliente_nombre           = "";
    $cliente_id               = "";
    $idvehiculo               = "";
    $idchasis                   = [];
    $vehiculos                = [];
    $solicitud_reclamo        = "";
    $motivo_reclamo           = "";
    $informacion_brindada     = "";
    $area_responsable         = "";
    $persona_cargo            = "";
    $personasACargo           = [];
    $idreclamo_plazo          = "";
    $resolucion_reclamo       = "";

    //Datos del ejecutivo
    $usuarioLogin=Usuario::obtenerUsuario($session->idusuario);
    $areaEjecutivo = $tablas->datosAreas($usuarioLogin->idrol);
    $idarea_detecta = $usuarioLogin->idrol;
    $ejecutivo_responsable = $usuarioLogin->usuarioNombre." ".$usuarioLogin->usuarioApellidoPaterno." ".$usuarioLogin->usuarioApellidoMaterno;
    $idejecutivo_responsable = $session->idusuario;
  }
  $form = new InputFormClass();
?>
  <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>

    <style>
        .ibox-content > .sk-spinner {
            display: none;
        }

        .ibox-content.sk-loading {
            position: relative;
        }
        .ibox-content.sk-loading:after {
            content: '';
            background-color: rgba(255, 255, 255, 0.7);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        .ibox-content.sk-loading > .sk-spinner {
            display: block;
            position: absolute;
            top: 40%;
            left: 0;
            right: 0;
            z-index: 2000;
        }
    </style>
  <div class="" style="padding-top: 11px; padding-bottom: 10px;">
    <input type="hidden" name="idreclamo_cliente" id="idreclamo_cliente" value="<?=$idrow;?>">
    <?php
    if($modo=="editar"){
    ?>
    <div class="row">
      <div class="form-group col-lg-4">
        <?php
        $params = [
          'type'        => 'text',
          'label'       => 'Nro de Reclamo',
          'placeholder' => '',
          'id'          => '',
          'name'        => '',
          'value'       => $vreclamo_cliente,
          'required'    => False,
          'disabled'    => True
        ];
        echo $form->getInputText($params);
        ?>
      </div>
      <div class="form-group col-lg-4">
        <?php
        $params = [
          'type'        => 'text',
          'label'       => 'Fecha del Reclamo',
          'placeholder' => '',
          'id'          => '',
          'name'        => '',
          'value'       => $fecha_reclamo,
          'required'    => False,
          'disabled'    => True
        ];
        echo $form->getInputText($params);
        ?>
      </div>
    </div>
    <?php
    }
    ?>
    <input type="hidden" name="vreclamo_cliente" id="vreclamo_cliente" value="<?=$vreclamo_cliente;?>">
    <div class="row">
    <div class="form-group col-md-4">
        <?php
        $params = [
          'label'       => 'Canal del reclamo',
          'id'          => 'idreclamo_canal',
          'name'        => 'idreclamo_canal',
          'data'        => $reclamoCanal,
          'keyValue'    => 'idreclamo_canal',
          'keyLabel'    => 'vdescripcion',
          'value'       => $idreclamo_canal,
          'required'    => True
        ];
        echo $form->getInputSelect($params);
        ?>
      </div>
      <div class="form-group col-md-4">
        <?php
        $params = [
          'label'       => 'Nivel de atención',
          'id'          => 'idreclamo_nivel_atencion',
          'name'        => 'idreclamo_nivel_atencion',
          'data'        => $reclamoNivelAtencion,
          'keyValue'    => 'idreclamo_nivel_atencion',
          'keyLabel'    => 'vdescripcion',
          'value'       => $idreclamo_nivel_atencion,
          'required'    => True
        ];
        echo $form->getInputSelect($params);
        ?>
      </div>
      <div class="form-group col-md-4">
        <?php
        $params2 = [
          'label'       => 'Estado',
          'id'          => 'idreclamo_estado',
          'name'        => 'idreclamo_estado',
          'data'        => $reclamoEstado,
          'keyValue'    => 'idreclamo_estado',
          'keyLabel'    => 'vdescripcion',
          'value'       => $idreclamo_estado,
          'required'    => True
        ];
        echo $form->getInputSelect($params2);
        ?>
      </div>
    </div>
    
    <div class="row">
      <div class="form-group col-lg-4">
        <?php
        $params = [
          'type'        => 'text',
          'label'       => 'Area detecta',
          'placeholder' => 'Area detecta',
          'id'          => 'area_detecta',
          'name'        => 'area_detecta',
          'value'       => $areaEjecutivo[0]->area,
          'required'    => True,
          'disabled'    => True
        ];
        echo $form->getInputText($params);
        echo $form->getInputHidden([
          "id"=>"idarea_detecta",
          "name"=>"idarea_detecta",
          "value"=>$idarea_detecta
        ]);
        ?>
      </div>
      <div class="form-group col-lg-4">
      <?php
        $params = [
          'type'        => 'text',
          'label'       => 'Ejecutivo responsable',
          'placeholder' => '',
          'id'          => 'ejecutivo_responsable',
          'name'        => 'ejecutivo_responsable',
          'value'       => $ejecutivo_responsable,
          'required'    => True,
          'disabled'    => True
        ];
        echo $form->getInputText($params);
        echo $form->getInputHidden([
          "id"=>"idejecutivo_responsable",
          "name"=>"idejecutivo_responsable",
          "value"=>$idejecutivo_responsable
        ]);
        ?>
      </div>
      <div class="form-group col-md-4">
        
      </div>
    </div>
    
    <div class="row">
      <div class="col-lg-4">
        <div class="input-group">
          <?php
          $params = [
            'type'        => 'text',
            'label'       => 'Cliente',
            'placeholder' => '',
            'id'          => 'cliente_nombre',
            'name'        => 'cliente_nombre',
            'value'       => $cliente_nombre,
            'disabled'    => True,
            'readonly'    => True,
            'required'    => True
          ];
          echo $form->getInputText($params);
          echo $form->getInputHidden(["id"=>"cliente_nombre","name"=>"cliente_nombre","value"=>$cliente_nombre]);
          ?>
          <?php
          echo $form->getInputHidden(["id"=>"correo_cliente","name"=>"correo_cliente"]);
          ?>
          
          <span class="input-group-btn" >
          <button <?= $modo=='editar' ? 'disabled':'' ?> class="btn btn-primary" style="margin-top:24px;" type="button" data-toggle="modal" data-target="#modalClientes"><span class="glyphicon glyphicon-search" aria-hidden="true">
          </span></button>
          </span>
        </div>  
      </div>
      <div class="form-group col-lg-4">
        <?php
        $params = [
          'type'        => 'text',
          'label'       => 'Chasis',
          'id'          => 'idchasis',
          'name'        => 'idchasis',
          'data'        => $idchasis,
          'keyValue'    => 'idchasis',
          'keyLabel'    => 'idchasis',
          'value'       => $idvehiculo,
          'required'    => True
        ];
        // if($modo=='editar'){
          
        // }
        echo $form->getInputSelect($params);
        ?>
      </div>
    </div>
    
    <div class="row">
      <div class="form-group col-md-12">
        <?php
          $params = [
            'type'        => 'text',
            'label'       => 'Solicitud del reclamo',
            'placeholder' => '',
            'id'          => 'solicitud_reclamo',
            'name'        => 'solicitud_reclamo',
            'value'       => $solicitud_reclamo,
            'required'    => True
          ];
          echo $form->getInputText($params);
        ?>
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-6">
        <?php
          $params = [
            'type'        => 'textarea',
            'label'       => 'Motivo del reclamo',
            'placeholder' => '',
            'id'          => 'motivo_reclamo',
            'name'        => 'motivo_reclamo',
            'value'       => $motivo_reclamo,
            'required'    => True
          ];
          echo $form->getInputText($params);
        ?>
      </div>
      <div class="form-group col-md-6">
        <?php
          $params = [
            'type'        => 'textarea',
            'label'       => 'Información brindada',
            'placeholder' => '',
            'id'          => 'informacion_brindada',
            'name'        => 'informacion_brindada',
            'value'       => $informacion_brindada,
            'required'    => True
          ];
          echo $form->getInputText($params);
        ?>
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-4">
        <?php
        $params = [
          'label'       => 'Area responsable',
          'id'          => 'area_responsable',
          'name'        => 'area_responsable',
          'data'        => $areas,
          'keyValue'    => 'idrol',
          'keyLabel'    => 'area',
          'value'       => $area_responsable,
          'required'    => True
        ];
        echo $form->getInputSelect($params);
        ?>
      </div>
      <div class="form-group col-md-4">
        <?php
        $params = [
          'label'       => 'Persona a cargo',
          'id'          => 'persona_cargo',
          'name'        => 'persona_cargo',
          'data'        =>  $personasACargo,
          'keyValue'    => 'idusuario',
          'keyLabel'    => 'usuario_nombre',
          'value'       => $persona_cargo,
          'required'    => True
        ];
        echo $form->getInputSelect($params);
        ?>
      </div>
      <div class="form-group col-md-4">
        <?php
        $params = [
          'label'       => 'Plazo del reclamo',
          'id'          => 'idreclamo_plazo',
          'name'        => 'idreclamo_plazo',
          'data'        => $reclamoPlazo,
          'keyValue'    => 'idreclamo_plazo',
          'keyLabel'    => 'vdescripcion',
          'value'       => $idreclamo_plazo,
          'required'    => True
        ];
        echo $form->getInputSelect($params);
        ?>
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-12">
        <?php
          $params = [
            'type'        => 'textarea',
            'label'       => 'Resolución del reclamo',
            'placeholder' => '',
            'id'          => 'resolucion_reclamo',
            'name'        => 'resolucion_reclamo',
            'value'       => $resolucion_reclamo,
            'required'    => False
          ];
          echo $form->getInputText($params);
        ?>
      </div>
    </div>
  </div>
<script>
    

    // function seleccionarCliente(idcliente, nombre, identdoc){
    //     console.log(idcliente);
    //     console.log(nombre);
    //     console.log(identdoc); //agregado
    //     $("#reclamoForm #cliente_id").val(idcliente);
    //     $("#reclamoForm #cliente_nombre").val(nombre);
    //     $("#reclamoForm #identdoc").val(identdoc); //agregado
    //     cargaVehiculosxd(idcliente);
    //     $('#modalClientes').modal('hide');
    // }

    function seleccionarCliente(idcliente, nombre, identdoc,cliente_mail){
        console.log(idcliente);
        console.log(nombre);
        console.log(identdoc); //agregado
        console.log(cliente_mail); //agregado
        $("#reclamoForm #cliente_id").val(idcliente);
        $("#reclamoForm #cliente_nombre").val(nombre);
        //$("#reclamoForm #identdoc").val(identdoc); //agregado
        $("#reclamoForm #correo_cliente").val(cliente_mail); //agregado
        cargaVehiculosxd(idcliente);
        $('#modalClientes').modal('hide');
    }

    // function cargaVehiculos(idcliente, idvehiculo){
    //     var idcliente = idcliente || null;
    //     var idvehiculo = idvehiculo || null;
    //     $.ajax({
    //         type : 'POST',
    //         url : 'ajax/DatosTablasAjax', //Here you will fetch records 
    //         data :  {
    //             "tabla":"vehiculos",
    //             "idcliente":idcliente,
    //             "idvehiculo":idvehiculo
    //         },
    //         success : function(data){
    //             data = JSON.parse(data);
    //             var textFirst = $("#idvehiculo option").eq(0).html();
    //             var strOptions = `<option value="">${textFirst}</option>`;
    //             data.forEach(function(row){
    //                 strOptions+=`<option value="${row.idvehiculo}">${row.chasis}</option>`;
    //             });
    //             $("#idvehiculo").html(strOptions);
    //             console.log(strOptions);
    //         }
    //     });
    // }

    function cargaVehiculosxd(idcliente, idchasis) {
    var idcliente = idcliente || null;
    var idchasis = idchasis || null;
    $.ajax({
        type: 'POST',
        url: 'ajax/DatosTablasAjax', // Aquí debes ajustar la URL según tu configuración
        data: {
            "tabla": "vehiculos",
            "cliente_id": idcliente,
            "idchasis": idchasis
        },
        success: function(data) {
            data = JSON.parse(data);
            //console.log(data);
            var textFirst = $("#idchasis option").eq(0).html();
            var strOptions = `<option value="">${textFirst}</option>`;
            data.forEach(function(row) {
                strOptions += `<option value="${row.idchasis}">${row.idchasis}</option>`;
            });
            $("#idchasis").html(strOptions);
            console.log(strOptions);
        },
        error: function(e){
          console.log(e);
        }
    });
}


    //// xd

    function cargaAreaPersonaACargo(idarea, idusuario){
        var idarea = idarea || null;
        var idusuario = idusuario || null;
        $.ajax({
            type : 'POST',
            url : 'ajax/DatosTablasAjax', //Here you will fetch records 
            data :  {
                "tabla":"area_usuarios",
                "idarea":idarea,
                "idusuario":idusuario
            },
            success : function(data){
                data = JSON.parse(data);
                //console.log(data);
                var textFirst = $("#persona_cargo option").eq(0).html();
                var strOptions = `<option value="">${textFirst}</option>`;
                data.forEach(function(row){
                    strOptions+=`<option value="${row.idusuario}">${row.usuario_nombre}</option>`;
                });
                $("#persona_cargo").html(strOptions);
                // console.log(strOptions);
            }
        });
    }

    $(document).ready(function(){
        $("#area_responsable").change(function(){
            cargaAreaPersonaACargo($(this).val(), null)
        });
    });
</script>
<script src="../../assets/bootbox/bootbox.all.js"></script>
<?php
}
?>

<?php 
if ($_POST['rowid']) {
	include_once("../../includes/init.php");
	$citaId=htmlspecialchars(htmlentities($_POST['rowid']));
	$citaObj=Cita::obtenerCitaPorId($citaId);
	?>
	 <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="../assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="../assets/css/animate.css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">
	<input type="hidden" name="idcita" value="<?= $citaId ?>">    <style>
        .ibox-content > .sk-spinner {
            display: none;
        }

        .ibox-content.sk-loading {
            position: relative;
        }
        .ibox-content.sk-loading:after {
            content: '';
            background-color: rgba(255, 255, 255, 0.7);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        .ibox-content.sk-loading > .sk-spinner {
            display: block;
            position: absolute;
            top: 40%;
            left: 0;
            right: 0;
            z-index: 2000;
        }
    </style>
	<div class="well" style="padding-top: 11px;
	padding-bottom: 10px;">
		<div class="row">                      
            <div class="form-group col-md-6">
              <label for="nombreCompleto" class="">Nombre completo * </label>
              <input style="border: none;" type="text" placeholder="Nombre completo *" id="nombreCompleto" name="nombreCompleto" class="form-control border-bottom text-capitalize"  required id="name" value="<?= $citaObj->nombreCliente ?>">
            </div>
          <div class="form-group col-md-3">
              <label for="celular" class="">Celular</label>
              <input style="border: none;" type="text" placeholder="Celular" id="celular" name="celular" class="form-control border-bottom text-capitalize"  value="<?= $citaObj->celular ?>">
          </div>
       	</div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="planR" class="">Plan *</label>
                <select  style="border: none;" class="tecnico border-bottom form-control" required name="planR" id="planR" disabled>
                    <?php Cliente::obtenerPlanSelected($citaObj->plan) ?>
                </select>
            </div>
            <div class="form-group col-md-6 ">
                <p style="font-weight:bold" class="text-danger">Servicios Adicionales Seleccionados : <label class="canthoraprog2R label-success"> 0 Horas </label></p>
                <?php ServiciosAdicionales::obtenerServAdicionalSeleccionadoVerCita($citaId) ?>
                <input type="hidden" id="canthoraprogR" name="canthoraprogR" value="0">
            </div>
        </div>
       	</div> 
           <div class="row">
            <div class="form-group col-md-3">
              <label for="tiempo" class="">Tiempo *</label>
              <input style="border: none;" type="number" placeholder="Tiempo *" id="tiempo" name="tiempo" class="form-control border-bottom text-capitalize" required  autocomplete="off" value="<?= $citaObj->tiempo ?>">
            </div>
            <div class="form-group col-md-3">
              <label for="costo" class="">Costo *</label>
              <input style="border: none;" type="text" placeholder="Costo *" id="costo" name="costo" class="form-control border-bottom text-capitalize" required value="<?= $citaObj->costo ?>" >
            </div>
            <div class="form-group col-lg-3">
            <!-- tipehead sugerencia de seguros  -->
              <label for="seguro" class="">Seguro * </label>
              <input style="border: none;" type="text" placeholder="Seguro *" id="seguro" name="seguro" class=" form-control border-bottom" value="<?= $citaObj->seguro ?>" >
            </div>
            <div class="form-group col-md-3">
              <label for="placa" class="">Placa * </label>
              <input style="border: none;" type="text" placeholder="Placa *" id="placa" name="placa" class="form-control x2 border-bottom" required value="<?= $citaObj->placa ?>" >
            </div>                        
          </div> 
<?php
    if ($citaObj->idestado==321) {
        ?>
    <div class="row">
                  <div class="form-group col-md-4">
                    <label for="fecham" class="">Fecha * </label>
                    <input style="border: none;" type="text" placeholder="Fecha *" id="fecham" name="fecham" class="form-control border-bottom" required autocomplete="off">
                  </div>
                <div class="form-group col-md-4">
                    <label for="fecha" class="">Turno</label>
                <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtnm form-control border-bottom btn btn-info" >Seleccionar hora</button>
                <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="pos" class="hora form-control border-bottom"  required="" readonly>
                </div>
                <div class="form-group col-md-4">
                    <label for="fecha" class="">Hora seleccionada</label>
                    <input style="border: none;" type="text" placeholder="Hora *" id="" name="hora" class="horaVer form-control border-bottom"  required="" readonly>
                </div>
            </div>
        <div class="ibox" id="ibox1R">
            <div class="ibox-content">
                <div class="sk-spinner sk-spinner-double-bounce">
                    <div class="sk-double-bounce1"></div>
                    <div class="sk-double-bounce2"></div>
                </div>
                <div id="programacionHtmlm">
                </div>
            </div>
        </div>
        <?php
    }else{
    $fecha=new DateTime;
    $fechaF=$fecha->format('YmdHm');
    $fechaCita=new DateTime($citaObj->fecha);
    $fechaCita->modify('-1 hour');
    $fechaCitaF=$fechaCita->format('YmdHm');
        ?>
<div class="row">
<br>
<div class="col-md-8">
<label>Programado para la fecha:<?= $citaObj->fecha ?></label>
<div style="overflow:hidden;">
    <div class="form-group">
        <div class="row">
            <div class="col-md-8">
                <div id="datetimepicker12"></div>
            </div>
        </div>
    </div>
</div>
</div>
	<div class="col-md-4"><button type="button" id='reprogramar' class="btn btn-info" <?= ($fechaF>$fechaCitaF)?"disabled":""  ?>>Reprogramar</button></div>
<br>
</div>
<input style="border: none;" type="hidden" name="noActualizarF" value="<?= $citaObj->fecha ?>"   >
<script>
  jQuery.datetimepicker.setLocale('es');
  $('#datetimepicker12').datetimepicker({
    defaultDate:'<?= $fechaCita->format('Y/m/d') ?>',
    scrollInput: false,
    scrollMonth : false,
    timepicker:false,
    inline: true
  });
            </script>
        <?php
    }
?>

    <div class="row">
      <div class="form-group col-md-12">
        <label for="observaciones" class="">Observaciones </label>
        <input style="border: none;" type="text" placeholder="Observaciones " id="observaciones" name="observaciones" class="form-control border-bottom" value="<?= $citaObj->observacion ?>" >
      </div>
      </div>
    <br>
	<div class="row">
		<div class="form-group col-md-5">
			<label for="trabajo" class="">correo </label>
			<input style="border: none;"  type="text" placeholder="correo" id="" name="correo" class=" border-bottom form-control" value="<?= $citaObj->correo ?>" >
	</div>
		<div class="form-group col-md-3">
		<label for="">Estado:</label><br>
			 <?= obtenerEstado($citaObj->idestado) ?>
			 <input type="hidden" name="estado" value="<?= $citaObj->idestado ?>">
		</div> 
		<div class="form-group col-md-4">
		<label for="">Fecha creación:</label><br>
			 <?php echo hace($citaObj->fechaRegistro) ?>
		</div>

	</div>


	<?php if (!empty($citaObj->idestado==321)): ?>
	<div class="row">
		<div class="form-group col-md-4">
		<label for="">Reprogramado sac Por:</label><br>
		<?php 
$usuarioRep=Usuario::obtenerUsuario($citaObj->idSac);  echo $usuarioRep->usuarioNombre." ".$usuarioRep->usuarioApellidoPaterno; 
		 ?>
		</div>
		<div class="form-group col-md-8">
		<label for="">Observacion</label><br>
			 <?php echo $citaObj->obsSac ?>
		</div>
	</div>
	<?php endif ?>
	<br>
    <hr><h2 class="text-center">Datos de canal</h2><hr>
    <?php 
    	$datosCanal=Canales::obtenerDatosCanalPorId($citaObj->idcanal);
     ?>
<?php
    if ($datosCanal) {
        ?>
     <div class="row">
		<div class="form-group col-md-4">
		<label for="">Canal:</label><br>
			 <?= $datosCanal->tipoCanal ?>
		</div>
		<?php if ($datosCanal->tipoCanal=='CONVENIO'): ?>
			<?php $conData=Canales::obtenerDatosDeConvenioPorId($datosCanal->nombreCanal) ?>
		<div class="form-group col-md-4">
		<label for="">Nombre canal</label><br>
			 <?= $conData->nombreConvenio ?>
		</div>
		<?php else: ?>
		<div class="form-group col-md-4">
		<label for="">Nombre canal</label><br>
			 <?= $datosCanal->nombreCanal ?>
		</div>
		<?php endif ?>
		<div class="form-group col-md-4">
		<label for="">Ejecutivo canal</label><br>
			 <?= $datosCanal->ejecutivoCanal ?>
		</div>
	</div>
       <?php if ($citaObj->comsiona=='si'): ?>
	<div class="row">
		<div class="form-group col-md-12">
		<label for="">Monto Comision:</label><br>
			 <?= ($citaObj->comision==0)?'Convenio':$citaObj->comision ?>
		</div>
	</div>
       <?php endif ?>
        <?php
        
    }else{
        ?>
      <div class="row">
		<div class="form-group col-md-4">
		<label for="">Canal:</label><br>
			 <?= $citaObj->tipoCanal ?>
		</div>
		<?php if ($citaObj->tipoCanal=='CONVENIO'): ?>
			<?php $conData=Canales::obtenerDatosDeConvenioPorId($citaObj->nombreCanal) ?>
		<div class="form-group col-md-4">
		<label for="">Nombre canal</label><br>
			 <?= $conData->nombreConvenio ?>
		</div>
		<?php else: ?>
		<div class="form-group col-md-4">
		<label for="">Nombre canal</label><br>
			 <?= $citaObj->nombreCanal ?>
		</div>
		<?php endif ?>
		<div class="form-group col-md-4">
		<label for="">Ejecutivo canal</label><br>
			 <?= $citaObj->ejecutivoCanal ?>
		</div>
<div class="form-group col-md-4">
		<label for="">Tienda</label><br>
			 <?= $citaObj->oficina ?>
		</div>
	</div>      
        <?php
    }
?>
       <?php if ($citaObj->estadoPago=='Pagado') {
       	?>
<hr><br>
         <div class="row">
    <img class="text-center col-md-6 col-md-offset-3" width="500" src="../../public/vouchers/<?php echo $citaObj->voucher ?>" alt="../../public/vouchers/<?php echo $citaObj->voucher ?>" >      
    </div>
  <div class="row">
    <a  class="btn btn-primary text-center  col-md-6 col-md-offset-3" href="../../public/vouchers/<?php echo $citaObj->voucher ?>" target="_blank" >Descargar/Ver</a>    
  </div>
  <input type="hidden" name="vouCHERANT" value="<?php echo $citaObj->voucher ?>" >
       	<?php
       }else if($citaObj->idestado!='777'){
?>
<div class="row">
  <div class="col-md-12"><label for="">Estado de pago</label></div>
</div>
        <div class="row">
        <div class="col-md-12">
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
              <div class="form-control" data-trigger="fileinput">
                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                <span class="fileinput-filename"></span>
              </div>
              <span class="input-group-addon btn btn-default btn-file">
                <span class="fileinput-new">Si el pago ya se realizo adjuntar voucher o el recorte del deposito</span>
                <span class="fileinput-exists">Cambiar</span>
                <!-- <label for="">Seleccionar voucher de pago</label> -->
                <input type="file" name="voucher"  />
              </span>
              <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
            </div>  
        </div>
        </div>
<?php

        } ?>

<?php 
if ($citaObj->idestado !=777) {
 ?>
       <div class="row">
       		<div class="col-md-2">
	<label for="clientes">Eliminar</label>
	<div class="switch">
		<div class="onoffswitch">
			<input type="checkbox" class="onoffswitch-checkbox eliminarC" id="clientes" value="si" name="ok">
			<label class="onoffswitch-label" for="clientes">
				<span class="onoffswitch-inner"></span>
				<span class="onoffswitch-switch"></span>
			</label>
		</div>
	</div>
</div>	
       </div>
       <br>
       <div class="row ocultar">
       	<div class="col-md-12 form-group">
       		<input style="border: none;"  type="text" placeholder="motivo" id="" name="motivoE" class=" border-bottom form-control motivoE" required="">
       	</div>
       </div>
<?php 
}
 ?>

	</div>
    <script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script>
<?php $fechaA=date('Y-m-d') ?>
	$(".ocultar").hide();
	$('.motivoE').prop('disabled', true);
	$('.eliminarC').on('change', function() {
		if(this.checked) {
			$(".ocultar").show();			
			$('.motivoE').prop('disabled', false);
			$('#fecham').prop('disabled', true);
			$('.horaVer').prop('disabled', true);
		}else{
			$(".ocultar").hide();
			$('.motivoE').prop('disabled', true);
			$('#fecham').prop('disabled', false);
			$('.horaVer').prop('disabled', false);
		}
    });
$("#fecham").focus(function(){
   $(this).blur(); 
});
<?php $fechaA=date('Y-m-d') ?>
  jQuery.datetimepicker.setLocale('es');
  $('#fecham').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d',
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
    // allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });

function lstTBProgm(){
    var fecha = $('#fecham').val();
    var tthinstala=$('#canthoraprogR').val();
    var cita = 'General';
    var distrito="1291";
    var cod = document.getElementById("planR").value;
    servAd = $('[name="servadR[]"]:checked').length;
    if (fecha!='' && cod!='' && servAd!=0 &&(distrito==0 || distrito!=null)) {
        $(".horabtnm").off( "click" );
        $('#ibox1R').children('.ibox-content').toggleClass('sk-loading');
        $.ajax({
            type : 'POST',
            url  : '../includes/tbProgramacion',
            data : {fecha:fecha.replace("/","-"),cita:cita,tthinstala:tthinstala,iddist:distrito},
            success : function(data)
            {
                $("#programacionHtmlm").html(data);
                $('#ibox1R').children('.ibox-content').toggleClass('sk-loading');
                $(".horabtnm").on("click",lstTBProgm).find(".horabtnm");
            }
        });
    }else{
        $("#programacionHtmlm").html('<h3 class="text-center text-info">Primero debe seleccionar Plan,Servicios Adicionales,fecha.</h3>');
    }
    return false;
}


function validaDPR() {
    var fecha = $('#fecham').val();
    var hora = $(".horaVer").val();
    var canthoraprog = $('#canthoraprogR').val();
    var idtec = $('input[name="pos"]').val();
    // console.log("fecha: "+fecha+" idtec:"+idtec+" canhora:"+canthoraprog.trim());
    $.ajax({
        type: 'POST',
        url: 'ajax/validarRegistroTB',
        data:'tipo=save&fecha='+fecha+'&idtec='+idtec+"&canthoraprog=" + canthoraprog.trim() + "&hora=" + hora,
        //  data :  'tipo=save&fecha=2019-11-27&idtec=2&canthoraprog=2&hora=12:00',
        success: function (data) {
            if (data.trim() == 'dp') {
                //dp='1';
                $('input[name="guardarCambio"]').prop("disabled",true);
                //      clearInterval(eventos.interval);
                $("#programacionHtmlm").html('');
                $(".horabtnm").html('Seleccionar hora');
                $(".horaVer").val('');
                // fn(dp);
                bootbox.alert({
                    title: "<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                    message: "<label>El horario Seleccionado("+hora+") ya fue Registrado,Por Favor Seleccione otro*</label>",
                    size: 'small',
                    callback: function () {
                        /*  l.ladda( 'stop' );*/
                    }
                });
            }else if(data.trim()=='Ok'){
                $('input[name="guardarCambio"]').prop("disabled",false)
            }
        }
    });
}

var eventos={};
eventos.frejcar =function(){
    validaDPR();
};
eventos.interval = setInterval(eventos.frejcar, 3000);


$(".horabtnm").click(function()
{
    lstTBProgm();
});
$("#mostrar").hide();
$('#fecham').change(function(){
    $("#programacionHtmlm").html('');
    $(".horabtnm").html('Seleccionar hora');
    $(".horaVer").val('');
});

$("#reprogramar").click(function(){ 
        var cita = '<?= $citaId ?>';
        $.ajax({
type : 'POST',
url  : 'controlador/rprcit',
data : {cita:cita},
success : function(data)
{
$.ajax({
type : 'post',
url : 'modal/citaDataModal', //Here you will fetch records 
data :  'rowid='+ cita, //Pass $id
success : function(data){
    $('.modal-body').html(data);//Show fetched data from database
}
});
}
});
        });

function tthoras() {
    var arr="";
    var seleccionado = $("#planR").find('option:selected');
    var idtec = seleccionado.data('type');
    var arr = $('[name="servadR[]"]:checked').map(function(){
        return this.value;
    }).get();

    var str = arr.join(',');
    if( str==='' && (idtec === undefined)){
        str='0';
        idtec='0';
    }

    $.ajax({
        type : 'POST',
        url  : 'ajax/cantidadHoraProgramacion',
        data :  'idCheck='+str+"&idTec="+idtec,
        success : function(data)
        {
            if(data!=''){
                $("#canthoraprogR").val(data);
                $(".canthoraprog2R").text(data+" Horas");
            }else{
                console.log("No hay data");
            }
        }
    });
    return false;
}


$(document).ready(function(){
    tthoras();
});
</script>
    <?php
}
?>



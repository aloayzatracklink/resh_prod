<?php 
if ($_POST['rowid']) {
  include_once("../../includes/init.php");

  // $lista = Operacion::obtenerFrecuentes($session->idusuario);
  // $idrow=htmlspecialchars(htmlentities($_POST['rowid']));
  
  ?>

  <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>

    <style>
        .ibox-content > .sk-spinner {
            display: none;
        }

        .ibox-content.sk-loading {
            position: relative;
        }
        .ibox-content.sk-loading:after {
            content: '';
            background-color: rgba(255, 255, 255, 0.7);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        .ibox-content.sk-loading > .sk-spinner {
            display: block;
            position: absolute;
            top: 40%;
            left: 0;
            right: 0;
            z-index: 2000;
        }
    </style>
  <div>
      <div class="row ibox float-e-margins ibox-content" style="margin:0px !important; padding:5px !important;">
        <div class="col-md-12">
            <div class="row">
                <form role="form" id="buscarClientesForm" method="POST">
                    <div class="col-lg-4">
                        <div class="input-group">
                            <label for="texto_buscar_cliente" class="">Búsqueda:</label>
                            <input style="border: none;"  class="cd-select form-control border-bottom" required  type="text" placeholder="Ingrese criterio de búsqueda" class="form-control" id="texto_buscar_cliente">
                            <span class="input-group-btn" >
                            <button class="btn btn-primary" style="margin-top:24px;" type="submit" id="buscar_cliente"><span class="glyphicon glyphicon-search" aria-hidden="true">
                            </span></button>
                            </span>
                        </div>
                    </div>
                    <!-- <div class="col-md-2" style="margin-top:10px;">
                        <button type="submit" class="btn btn-primary" id="buscar_cliente">Buscar</button>
                    </div> -->
                </form>
            </div>
        </div>
    </div>

    <div class="ibox-content" id="resultados_clientes">
        <div class="table-responsive">
          <table class="table table-condensed  table-hover" >
              <thead>
                  <tr>                   
                  <th></th>
                  <th class="text-center">DNI/RUC</th>
                  <th class="text-center">Cliente</th>
                  <th class="text-center"></th>
                  </tr>
              </thead>  
              <tbody  id="resultado_busqueda_clientes">          
              </tbody>  
          </table>
        </div>
    </div>
  </div>
  <script>
    // function buscarClientes(){
    //     var criterio = $.trim($("#texto_buscar_cliente").val());
    //     $.ajax({
    //         type : 'POST',
    //         url  : 'ajax/paginacionBuscarClientes',
    //         data : {criterio:criterio},
    //         success : function(data)
    //         {
    //             $("#resultado_busqueda_clientes").html(data);
    //             //console.log(data)
    //         }
    //     });
    // }

    $(document).ready(function(){

        $("#buscarClientesForm").submit(function (event) {
            //var formData = $("#buscarClientesForm").serialize();
            var criterio = $.trim($("#texto_buscar_cliente").val());   
            $.ajax({
                type: "POST",
                url: "ajax/paginacionBuscarClientes",
                data: {criterio: criterio}
            }).done(function (data) {
                $("#resultado_busqueda_clientes").html(data);
            });

            event.preventDefault();
        });

        $("#resultado_busqueda_clientes").on( "click", ".pagination1 a", function (e){
            e.preventDefault();
            var criterio = $.trim($("#texto_buscar_cliente").val());       
            var page = $(this).attr("data-page"); 
            $.ajax({
            type : 'POST',
            url  : 'ajax/paginacionBuscarClientes',
            data : {page: page, criterio:criterio},
            success : function(data)
            {
                $("#resultado_busqueda_clientes").html(data);
            }
            });

        });
    });

  </script>
<script src="../../assets/bootbox/bootbox.all.js"></script>
<?php
}
?>
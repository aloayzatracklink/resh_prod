<?php 
if (isset($_POST['rowid'])) {
	include_once("../../includes/init.php");
	$idoperacion=limpiar($_POST['rowid']);	
    $obMenu=Usuario::obtenerMenu($session->idusuario);
	$objetoOpe=Operacion::obtenerOperacionPorId($idoperacion);
    $VhObj=Vehiculo::obtenerVehiculo($objetoOpe->idvehiculo);
    $cliObjR=Cliente::obtenerDatosDeclientePorId($objetoOpe->idcliente);
    $datosFac=DatosFacturacion::obtenerDatosFacturacionPorId($objetoOpe->datosfacturacionid);
	$clienteObj=Onix::ObtenerDatosDesdeChasis($VhObj->chasis);
    if(!$clienteObj){
      $clienteObj=Onix::obtenerDatosDesdeOrden($objetoOpe->numeroOrden);
    }
   ?>

    <input type="hidden" name="idvehiculo" value="<?= $VhObj->idvehiculo ?>">
    <input type="hidden" name="idcliente" value="<?= $cliObjR->idcliente ?>">
    <input type="hidden" name="idfacturacion" value="<?= $datosFac->datosfacturacionid ?>">
    <input type="hidden" name="idoperacion" value="<?= $objetoOpe->idoperacion ?>">
    <input type="hidden" name="idcanal" value="<?= $datosFac->idcanal ?>">
    <?php
    if ($clienteObj) {
        $cantidadObjeto=Onix::obtenerCantididadVehiculos($clienteObj->idcustomer,$clienteObj->idlocation);
        $planobj=Onix::obtenerPlanDeServicio($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation);
        $vencimientoDatos=Onix::obtenerFechaDeVencimiento($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation);	
        $tiempo="";
        ?>                        	
            <!--<input type="hidden" name="datosOnyx" value="<?= print_r($clienteObj) ?>">-->
<div class="well" style="padding-top: 11px; padding-bottom: 10px;">
<div class="row">
    <div class="row">
                    <?php if ($cantidadObjeto){ ?>
                        <?php if ($cantidadObjeto->cantidad>1){ ?>
                        <h4 class="text-center text-warning">ATENCION ! Cliente tiene <?php echo $cantidadObjeto->cantidad ?> Vehiculos en Tracklink</h4>	
                        <?php } ?>
                    <?php } ?>
        <div class="form-group col-md-3">
            <label for="Observaciones" >DNI/RUC</label>
            <input style="border: none;" name='dniO' type="text" placeholder="Web" id="Observaciones" <?php echo $retVal = (!empty($clienteObj->dni)) ? "readonly" : "" ; ?> class="border-bottom form-control"  value="<?php echo $dniRuc = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>">
<input type="hidden" value="<?php echo $dniRuc ?>">
        </div>
        <div class="form-group col-md-5">
            <label for="numeroTecnico" >Nombre de cliente*</label>
            <input  style="border: none;" type="text" readonly id="numeroTecnico" class="border-bottom form-control" required value="<?php echo $nombres = (!empty($clienteObj->nombre)) ? "$clienteObj->apellidos"." "."$clienteObj->nombre" : "$clienteObj->apellidos" ; ?> ">

            <input type="hidden" value="<?php echo $nombres ?>">
            <input type="hidden" name='nombreO' value="<?php $clienteObj->nombre ?>">
            <input type="hidden" name='apellidoO' value="<?php $clienteObj->apellidos  ?>">
        </div>
        <div class="col-md-2"><label for="estado" >Celular</label>
            <input style="border: none;" type="text" <?php echo $retVal = (!empty($clienteObj->celular)) ? "readonly" : "" ; ?> id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">
        </div>
        <div class="col-md-2"><label for="estado" >Celular</label>
            <input style="border: none;" type="text" readonly id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular2)) ? "$clienteObj->celular2" : "" ; ?>">
        </div>
    </div>
    
    <div class="row">
        <div class="form-group col-md-12">
            <label for="Observaciones"  >Correo cliente. </label>
            <input style="border: none;" type="text" placeholder="Correo" id="Observaciones"  readonly class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo," : "$clienteObj->correo" ;echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo2" : "" ;  ?>">
        </div>
    </div>
<div class="row">
<div class="form-group col-md-3">
    <label for="numeroTecnico" >Placa Vehiculo*</label>
    <input  style="border: none;" name='placaO' readonly type="text" placeholder="Placa" id="numeroTecnico" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->placa)) ? "$clienteObj->placa" : "" ; ?>"  >
</div>
<div class="form-group col-md-3">
    <label for="Observaciones"  >Marca *</label>
    <input required readonly name='marcaO' style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->marca)) ? "$clienteObj->marca" : "" ; ?>" >
</div>
<div class="form-group col-md-3">
    <label for="Observaciones" >Modelo*</label>
    <input required readonly name='modeloO' style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($clienteObj->modelo)) ? "$clienteObj->modelo" : "" ; ?>">
</div>
<div class="form-group col-md-3">
    <label for="color"  >Color</label>
    <input  style="border: none;" name='colorO' readonly type="text" placeholder="Color" id="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->color)) ? "$clienteObj->color" : "" ; ?>">

</div>	
</div>
<div class="row">
<div class="form-group col-md-6">
    <label for="Observaciones" >Chasis*</label>
    <input required  readonly style="border: none;" name='chasisO' type="text" placeholder="Chasis" id="Observaciones" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->chasis)) ? "$clienteObj->chasis" : "$chasis" ; ?>">
</div>
<div class="form-group col-md-6">
    <label for="Observaciones" >Motor</label>
    <input  style="border: none;" name='motorO' readonly type="text" placeholder="Observaciones" id="Observaciones" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->motor)) ? "$clienteObj->motor" : "Actualizar" ; ?>">
</div>
</div>
    <div class="row">
        <div class="form-group col-md-6" >
            <label for="Observaciones"  >Observaciones Onyx </label>
            <div style="border-right: solid;height: 90px;
overflow-y: auto;">
            <label class="border-" ><?php echo utf8_encode($clienteObj->comentarios) ?></label>
            </div>
        </div>
        <div class="col-md-6 form-group"  >
        <label for="">Ordenes de Vehículo</label>
    <div style="height: 90px;
overflow-y: auto;">
            <?php $objOrdenes=Onix::obtenerOrdenesVehiculo($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation) ?>
            <?php foreach ($objOrdenes as $var): ?>
                <?= $orden=$var->idlocation."-".$var->idorder ?> <?= utf8_encode($var->articulo) ?><br>
                    <?php   $DataOrd=Onix::obtenerDatosFacturacion($orden) ?>
                    <?php   $DataOrt=Onix::obtenerORTE($orden) ?>
                    <?= ($DataOrd)?$DataOrd->serie.$DataOrd->numeroFactura."  $".round($DataOrd->precioO*1.18,2):""; ?>
                    <?= ($DataOrt->orteO!='')?" ORTE ".$DataOrt->orteO." ":'' ?> <small><?= haceMinimoAnio($var->fechaOrden) ?></small>
                    <hr>
            <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<?php } ?>
<hr>
    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $cliObjR->tipoCliente ?>">
      <?php if ($cliObjR->tipoCliente=="Natural"): ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $cliObjR->dni ?>">       
      <?php else: ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $cliObjR->ruc ?>">        
      <?php endif ?>  
    </div>

    <div class="col-md-9">
      <?php if ($cliObjR->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" value="<?php echo $cliObjR->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $cliObjR->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $cliObjR->razonSocial ?>">       
      <?php endif ?>
    </div> 
    </div>
<h3>Datos del vehiculo</h3>
<div class="row"><hr>
    <div class="col-lg-12 field_wrapper"> 

<?php $vhOC=Vehiculo::verificarSiEstaDuplicado($VhObj->chasis) ?>
<?php if($vhOC->cantidad>1){
?>
    <label class="text-danger text-center"> SE DETECTO MAS DE UN REGISTRO CON EL MISMO CHASIS </label> 
<table class="table table-hover">
  <thead>
    <tr>
      <th>#</th>
      <th>Tipo Servicio</th>
      <th>Nombre Cliente</th>
      <th>Placa-Chasis</th>
      <th>Facturación</th>
      <th>Vehículo</th>
      <th>Acción</th>
    </tr>
  </thead>
  <tbody>
<?php $DatosVhD=Vehiculo::obtenerVehiculosConElMismoChasis($VhObj->chasis) ?>
<?php $n=1 ?>
<?php foreach ($DatosVhD as $var): ?>
<?php $datodCl=Cliente::obtenerDatosDeclientePorId($var->idcliente);
$datosFacdeta=DatosFacturacion::obtenerDatosFacturacionPorIdVehiculo($var->idvehiculo);
$datosOpera = Operacion::obtenerOperacionesPorVehiculoIDClienteId($var->idvehiculo, $var->idcliente);
    ?>
    <tr <?= ($VhObj->idvehiculo == $var->idvehiculo )?"class='success'":"" ?>>
      <th scope="row"><?= $n++ ?></th>
         <td ><span class="label label-primary"><?= $datosOpera->trabajo ?></span></td>
      <td><?= ($datodCl->tipoCliente=="Natural")? $datodCl->apellidosC." ".$datodCl->nombre:$datodCl->nombre ?></td>
      <td><?= $var->placa." ".$var->chasis ?></td>
      <td><?= obtenerEstado($datosFacdeta->estadoFacturacion) ?>  </td>
      <td><?= obtenerEstado($var->estado) ?></td>
      <td>
<?php if ($VhObj->idvehiculo != $var->idvehiculo){ ?>
        <button type='button' class="btn btn-warning disabled" style="padding-top: 0;padding-bottom: 0"  value='<?= $datosFac->datosfacturacionid ?>'>Anular<i class="fa fa-cog"></i></button>
<?php } ?>
      </td>  
    </tr>
<?php endforeach ?>
  </tbody>
</table>
<hr>
<?php
    }
?>
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="placa" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $VhObj->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="text-danger">Chasis</label>
          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="chasis" class="text-danger border-bottom form-control text-uppercase" required value="<?php echo $VhObj->chasis ?>">
        </div>
 <div class="form-group col-md-3"> <label for="motor" class="">Motor</label>
          <input style="border: none;" type="text" placeholder="Motor" id="motor" name="motor" class="border-bottom form-control text-uppercase" required value="<?php echo $VhObj->motor ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="marca" class="">Marca</label>
          <input style="border: none;" type="text" placeholder="Marca" id="marca" name="marca" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $VhObj->marca ?>" >
        </div>
      </div> 
      <div class="row">
        <div class="form-group col-md-3">
          <label for="modelo" class="">Modelo</label>
          <input style="border: none;" type="text" placeholder="Modelo" id="modelo" name="modelo" class="border-bottom form-control text-capitalize"  required  value="<?php echo $VhObj->modelo ?>">
        </div>
        <div class="form-group col-md-2">
          <label for="color" class="">Color</label>
          <input style="border: none;" type="text" placeholder="Color" id="color" name="color" class="border-bottom form-control x2 text-capitalize" required value="<?php echo $VhObj->color ?>" >
        </div>
        <div class="form-group col-md-2">
          <label for="anio" class="">Año</label>
          <input style="border: none;" type="text" placeholder="Año" id="anio" name="anio" class="border-bottom form-control" required value="<?php echo $VhObj->anio ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="seguro" class="">Compañia de seguros</label>
            <select  style="border: none;" class="border-bottom form-control text-capitalize" required name="seguro" id="seguro">
                 <?php Cliente::obtenerAseguradora($VhObj->companiaSeguro) ?>
            </select>
        </div>
        <div class="form-group col-md-2">
          <label for="concecionario" class="">Concesionario</label>
          <input style="border: none;" type="text" placeholder="Concesionario" id="concecionario" name="concesionario" class="seguro border-bottom form-control" autocomplete="off" value="<?php echo $VhObj->concesionario ?>">
        </div>
      </div>
    <div class="row">
        <div class="form-group col-md-12 text-center">
          <input style="border: none;" type="submit"   name="actualizarDeOnyx" class="btn btn-danger"  value="Actualizar datos del cliente y vehículo desde Onyx con chasis">
        </div>
      </div>
      <hr>
    <div class="row">
        <div class="form-group col-md-4">
        <label for="plan" class="">Plan </label>
        <select  style="border: none;" class="tecnico border-bottom form-control"   required name="plan" id="plan">
            <option class="" value="<?php echo $VhObj->plan ?>"  selected ><?php echo $VhObj->plan ?></option>
            <?php Cliente::obtenerPlan() ?>
        </select> 
        </div>
        <div class="col-md-4 form-group">
        <div class="row camapanaEsconder">
        <div class="form-group col-md-12">
            <label for="campana" class="">¿ Campaña Pacífico Gps Smart ?</label>
            <select  style="border: none;" class="tecnico border-bottom form-control" required name="campana" id="campana">
                <?php Cliente::obtenerAseguradoraSelected($VhObj->campana,$VhObj->companiaSeguro,$VhObj->plan) ?>
            </select>
        </div>
        </div>
        </div>
        <div class="form-group col-md-4">
            <label for="corigen" class="">Concesionario Origen</label>
            <input style="border: none;" type="text" placeholder="Concesionario Origen" id="corigen" name="corigen" class="border-bottom form-control text-uppercase corigen " autocomplete="off" readonly value="<?= $datosFac->canalOrigen ?>" >
        </div>
        <div class="form-group col-md-12 ">
            <p style="font-weight:bold" class="text-danger">Servicios Adicionales Seleccionados :</p>
            <?php ServiciosAdicionales::obtenerServAdicionalSeleccionadoVerAdmin($idoperacion) ?>
        </div>
    </div>
        <br>
    <div class="row">
        <div class="form-group col-md-2">
            <label for="modelo" class="">Documento</label>
            <input style="border: none;" type="text" placeholder="Documento Facturación" id="documentoFacturacion" name="documentoFacturacion" class="border-bottom form-control text-capitalize"  required  value="<?php echo $datosFac->tipoDocumentoFacturacion  ?>">
        </div>
        <div class="form-group col-md-2">
        <label for="color" class=""><?= obtenerEstado($datosFac->estadoFacturacion) ?></label>
            <input style="border: none;" type="text" placeholder="Numero Facturacion" id="numeroFacturacion" name="numeroFacturacion" class="border-bottom form-control x2 text-capitalize" value="<?=  (!empty($datosFac->numeroBoleta))?$datosFac->numeroBoleta:$datosFac->numeroFactura ?>" >
        </div>
        <div class="form-group col-md-2">
            <label for="tiempo" class="">Tiempo</label>
            <input style="border: none;" type="text" placeholder="Tiempo" id="tiempo" name="tiempo" class="border-bottom form-control" required value="<?= $datosFac->tiempo[0] ?>" >
        </div>
        <div class="form-group col-md-2">
            <label for="anio" class="">Precio</label>
            <input style="border: none;" type="text" placeholder="Precio" id="precio" name="precio" class="border-bottom form-control" required value="<?= $datosFac->costo ?>" >
        </div>
        <div class="form-group col-md-2">
            <label for="seguro" class="">Precio sin IGV</label>
            <input style="border: none;" type="text"class="seguro border-bottom form-control" readonly autocomplete="off" value="<?= $totalS=round($datosFac->costo/1.18,2) ?>">
        </div>
        <div class="form-group col-md-2">
            <label for="concecionario" class="">Numero Orden</label>
            <input style="border: none;" type="text" placeholder="Numero Orden Fact" id="numeroOrden" name="numeroOrden" class="numeroOrden border-bottom form-control" autocomplete="off" value="<?php echo $datosFac->orden ?>">
        </div>
        </div>   
    </div>
  </div>
<div class="row">
  <div class="form-group col-md-4">
		<label for="ejecutivoComercial" class="">Ejecutivo comercial *</label>
         <select  style="border: none;" class="tecnico border-bottom form-control" required name="ejecutivoComercial" id="ejecutivoComercial">
    <?php $o=Usuario::obtenerUsuario($datosFac->idUsuarioCreado); ?>
          <option class="" value="<?= $o->idusuario ?>" selected ><?=  $o->usuarioNombre." ".$o->usuarioApellidoPaterno; ?></option>
         <?php $ejecutiosCom=Usuario::obtenerParaVenta() ?>            
          <?php foreach ($ejecutiosCom as $var): ?>
          <option class="" value="<?= $var->idusuario ?>"><?=  $var->usuarioNombre." ".$var->usuarioApellidoPaterno; ?></option>          	
          <?php endforeach ?>
        </select>
  </div> <?php $co=Usuario::obtenerUsuario($VhObj->idcreador); ?>
    <div class="form-group col-md-4">
        <label for="cantreg" class="">Cantidad Registros</label>
        <?php
        $cantReg=CantidadRegistroOP::obtenerCantidadOperacionPorTipoServicio($VhObj->chasis,$objetoOpe->trabajo);
        IF($cantReg) {
            foreach ($cantReg as $var):
                $cantReg=$var->cantreg;
            endforeach;
        }else{
            $cantReg=1;
        }

        ?>
        <input style="border: none;" type="hidden" placeholder="Cantidad Registros" id="tipo" name="tipo" class="border-bottom form-control"  value="<?php echo $objetoOpe->trabajo ?>">
        <input style="border: none;" type="text" placeholder="Cantidad Registros" id="cantreg" name="cantreg" class="border-bottom form-control"  value="<?php echo $cantReg ?>">

    </div>

    <div class="form-group col-md-4">
        <label for="" class="">Creado por <?=  $co->usuarioNombre." ".$co->usuarioApellidoPaterno; ?></label>
        <label for="" class=""> <?= hace($VhObj->fechaCreacion) ?><label>
    </div>
</div>

    <div class="row">
        <div class="col-md-12">
            <?php
            $modfiaciones=SeguimientoConsulta::obtenerSEguimiento($idoperacion);
            if($modfiaciones):
                ?>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"> <b>Ultimas modificaciones </b></li>
                    <?php foreach($modfiaciones as $var): ?>
                        <?php $ooo=Usuario::obtenerUsuario($var->idusuario); ?>
                        <li class="list-group-item"><?= $var->register_at ?>    ===> <?= $ooo->usuarioNombre." ".$ooo->usuarioApellidoPaterno ?> </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>

    </div>
  <?php if (empty($datosFac->voucher)): ?>
    <h3 class="text-center text-danger">Atención pago en caja</h3>
    <input type="hidden" name="pagoEnCaja" value="ok">
  <?php else: ?>
 <div class="row">
    <img class="text-center" style="width:100%; max-width:800px;"
    src="../../public/vouchers/<?php echo $datosFac->voucher ?>" >
    <a  class="btn btn-primary text-center  col-md-6 col-md-offset-3" href="../../public/vouchers/<?php echo $datosFac->voucher ?>" target="_blank" >Descargar/ver Voucher</a>
    <input type="hidden" name="currentVoucher" value="<?= $datosFac->voucher ?>">
 </div>
  <?php endif ?> 
<div class="hr-line-dashed"></div> 
    <div class="row">
        <div class="form-group col-md-6">
            <label for="cambiarvoucher">¿desea cambiar el voucher?</label>
            <input id='cambiarvoucher' class="form-control checkbox" type="checkbox">
        </div>

        <div class="form-group col-md-6 fasdfasdfsdff">
            <label for="voucer">Seleccione voucher</label>
            <input name="voucherssss" class="form-control " id='voucer' type="file" >
        </div>
     </div>
<hr>

<h3 class="text-center">Cobra comsión: <?= (Comisiones::obtenerDatosDeComisionPorIdFacturacion($objetoOpe->datosfacturacionid))?'SI':'NO' ?></h3>
<?php $datosC=Canales::obtenerDatosCanalPorId($datosFac->idcanal) ?>Datos de canal.
<div class="row">
        <div class="form-group col-md-3">
          <label for="tipoCCnal" class="">Tipo</label>
          <input style="border: none;" type="text" placeholder="tipoCCnal" id="tipoCCnal" name="tipoCCnal" class="border-bottom form-control x2 text-uppercase" value="<?= $datosFac->tipoCanal ?>">
        </div>
<div class="form-group col-md-3">
          <label for="tipoCCnal" class="">Tienda</label>
          <input style="border: none;" type="text" placeholder="tipoCCnal" id="tiendaCCT" name="tiendaCCT" class="border-bottom form-control x2 text-uppercase" value="<?= $datosFac->oficina ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="nombreCCCanl" class="">Nombre</label>
          <input style="border: none;" type="text" placeholder="nombreCCCanl" id="nombreCCCanl" name="nombreCCCanl" class="border-bottom form-control text-uppercase" value="<?= $datosFac->nombreCanal ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="EjecutCCanl" class="">Ejecutivo canal</label>
          <input style="border: none;" type="text" placeholder="EjecutCCanl" id="EjecutCCanl" name="EjecutCCanl" class="border-bottom form-control text-uppercase" value="<?= $datosFac->ejecutivoCanal ?>" >
        </div>

    <div class="form-group col-md-3">
        <label for="EjecutCCanl" class="">Forma de Pago</label>
        <input disabled style="border: none;" type="text" placeholder="EjecutCCanl" id="formaPago" name="formaPago" class="border-bottom form-control text-uppercase" value="<?= $datosFac->formaPago ?>" >
    </div>
</div>


    <h3 class="text-center">Datos Facturación</h3>

    <div class="row">
        <div class="form-group col-md-3">
            <label for="tipoCCnal" class="">Tipo Documentación</label>
            <?php if($datosFac->tipoDocumentoFacturacion=="Boleta" or $datosFac->tipoDocumentoFacturacion=="Factura"){?>
            <select style="border: none;" id="selectNa" name="tipoDocumentoFacturacion" class="cd-selec form-control border-bottom">
                <option value="" disabled>Datos de facturación*</option>
                <option value="Boleta" <?php if($datosFac->tipoDocumentoFacturacion=="Boleta"){echo "selected";} ?>>Boleta</option>
                <option value="Factura" <?php if($datosFac->tipoDocumentoFacturacion=="Factura"){echo "selected";} ?>>Factura</option>
            </select>
            <?php } else { ?>
                <input style="border: none;" type="text" placeholder="Tipo Documento Facturación"  id="tipoDocumentoFacturacion" name="tipoDocumentoFacturacion" readonly class="form-control x2 border-bottom" value="<?= $datosFac->tipoDocumentoFacturacion ?>"  >
            <?php }?>
        </div>
        <div class="form-group col-md-4">
            <label for="tipoCCnal" class="">numero documento</label>
            <input style="border: none;" type="number" placeholder="Numero Documento"  id="ndocfact" name="ndocfact" class="form-control x2 border-bottom" value="<?php if($datosFac->tipoDocumentoFacturacion=="Boleta"){echo $datosFac->dniFacturacion; }else{ echo $datosFac->rucFacturacion; } ?>"  >
        </div>
        <div class="form-group col-md-5">
            <label for="nombreCCCanl" class="">Razon Social</label>
            <input style="border: none;" type="text" placeholder="Razón Social *"  id="razonSocialFacturacion" name="razonSocialFacturacion" class="form-control border-bottom text-capitalize" value="<?= $datosFac->razonSocialFacturacion ?>" >
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-7">
            <label for="direccionFiscalFacturacion" >Dirección Fiscal *</label>
            <input style="border: none;" type="text" placeholder="Dirección Fiscal *"  id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="form-control border-bottom text-capitalize" value="<?= $datosFac->direccionFiscal; ?>">
        </div>

        <div class="form-group col-md-5">
            <label for="telefonoFacturacion" >Teléfono</label>
            <input style="border: none;" type="text" placeholder="Teléfono"  id="telefonoFacturacion" name="telefonoFacturacion" class="form-control border-bottom text-capitalize" value="<?= $datosFac->telefonoFacturacion?>">
        </div>
    </div>

    <br>

    <script src="../../assets/bootbox/bootbox.all.js"></script>
    <?php
    if ($obMenu->administrarComercial=="si") {
		if (!Comisiones::obtenerDatosDeComisionPorIdFacturacion($datosFac->datosfacturacionid)) {
			if ($datosFac->estadoFacturacion=='34') {
		?>
		<div class="hr-line-dashed"></div> 
		<div class="row">
			<div class="col-md-6"> <label for="" class="text-center">Marcar como comsionado: <button class="btn btn-primary formmm "  onclick="return" style="padding-top: 0;padding-bottom: 0" type="button" value="<?= $datosFac->datosfacturacionid ?>">Marcar <i class="fa fa-check-square-o"></i></button> </div>
            <div class="col-md-4">
            <select style="border: none;" name="typeCommision" id="typeCommision" class="form-control border-bottom">
                <option value="" disabled selected>tipo pago de comisión*</option>
                <option value="1">Credito</option>
                <option value="2">Factura</option>
            </select>
            </div>
		</div>


		<script>
			$('.formmm').click(function(e) {

  var idfacturacion=$(this).val();
  var ntypecom = $("#typeCommision").val()
  if(ntypecom) {
      $.confirm({
          theme: 'modern',
          title: '¿Marcar como comisionado?',
          content: 'Al marcar como comisionado  el ejecutivo comercial verificara la información',
          buttons: {
              Confirmar: {
                  btnClass: 'btn-blue',
                  action: function () {
                      $.ajax({
                          type: 'POST',
                          url: 'controlador/comTC',
                          data: {idfacturacion: idfacturacion,ntypecom:ntypecom,trabajo:"Instalacion"},
                          success: function (data) {
                              $('#bateriasOModal').modal("hide");
                              $("#filter").focus();
                              $.alert('Se marco como comisionado');
                          }
                      });
                  }
              },
              Cancelar: {
                  btnClass: 'btn-warning',
                  action: function () {
                      $("#filter").focus();
                      $.alert('No se realizo nigun cambio!');

                  }
              }
          }
      });
  }else{
      bootbox.alert({
          title: "<h3 class='text-danger'><strong>Alerta!</strong></h3>",
          message: "<label>Debe seleccionar el tipo de comisión</label>",
          size: 'small',
          callback: function () {
              /*  l.ladda( 'stop' );*/
          }
      });
  }
});
		</script>
		<?php
			}else{
				?>
				<label for="" class="text-danger text-center">La venta tiene que estar facturada para poder comisionar</label>
				<?php
			}

		}
	}
 ?>
<hr>
<label class="text-center" >Datos de instalación </label>
<div class="row">
    <div class="col-md-4"><label><?= $objetoOpe->lugarInstalacion ?></label></div>
    <div class="col-md-4"><label><?= $objetoOpe->fechaInstalacion ?></label></div>
    <div class="col-md-4"><label><?= obtenerEstado($objetoOpe->estado) ?></label></div>
</div>
<br>
<?php if($datosFac->estadoFacturacion!='18'){ ?>
<div class="row">
    <div class="col-md-10">
            <label for="anio" class="">Motivo</label>
            <input style="border: none;" type="text" placeholder="Motivo" id="motivo" name="motivo" class="border-bottom form-control" >
    </div>
    <div class="col-md-2">
	<label for="eliminarVenta">Anular</label>
	<div class="switch">
		<div class="onoffswitch">
			<input type="checkbox" class="onoffswitch-checkbox" id="eliminarVenta" value="si" name="eliminarVenta">
			<label class="onoffswitch-label" for="eliminarVenta">
				<span class="onoffswitch-inner"></span>
				<span class="onoffswitch-switch"></span>
			</label>
		</div>
	</div>
    </div>
</div>
<?php } ?>

    <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
        <button type="submit" name="guardarCambios" id="guardarCambios" class="btn btn-primary" value="<?php echo Token::generar() ?>">Guardar cambios</button>
    </div>

    <script>
        $(document).ready(function () {

            $('#guardarCambios').click(function() {

                console.log("holaaa ->")

                checked = $('[name="servad[]"]:checked').length;

                console.log("aca p ---->"+checked)

                if(checked==0) {
                    bootbox.alert({
                        title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                        message: "<label>Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*</label>",
                        size: 'small',
                        callback: function () {
                            /*  l.ladda( 'stop' );*/
                        }
                    });
                    /* alert("Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*");*/
                    console.log("Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*");
                    // $('[name="servad[]"]:checked')[0].focus();
                    return false;
                }

            });
        });
    </script>
    <script>
        $("#plan").bind("change",function(){
            var plan=$("#plan option:selected").text();
        var aseguradora=$("#seguro option:selected").text();
        if (plan=='E-call' || plan=='E-call Plus' || plan=='Proteccion') {
            $("#campana").val("");
            if(aseguradora=="Pacifico Seguros"){
                // console.log(aseguradora+" aseguradora es");
                $("#campana option[value='GpsSmart']").show();
            }else{
                $("#campana option[value='GpsSmart']").hide();
            }
            $('.camapanaEsconder').show();
            $('#campana').prop('disabled',false);
        }else{
            $('.camapanaEsconder').hide();
            $('#campana').prop('disabled',true);
        }
    });

    $('#seguro').change(function(){
        var plan=$("#plan option:selected").text();
        var aseguradora=$("#seguro option:selected").text();
        if (plan=='E-call' || plan=='E-call Plus' || plan=='Proteccion') {
            //$('#campana').trigger("chosen:updated");
            $("#campana").val("");
            if(aseguradora=="Pacifico Seguros"){
                // console.log(aseguradora+" aseguradora es");
                $("#campana option[value='GpsSmart']").show();
            }else{
                $("#campana option[value='GpsSmart']").hide();
            }
            $('.camapanaEsconder').show();
            $('#campana').prop('disabled',false);
        }else{
            $('.camapanaEsconder').hide();
            $('#campana').prop('disabled',true);
        }
    });

    $('#eliminarVenta').click( function() {
        var eliminarVenta = document.getElementById('eliminarVenta');
        if (eliminarVenta.checked){    
            $('#motivo').prop('required', true);
            $('#motivo').focus();
        }else{  
            $('#motivo').prop('required', false);
        }
    });


$('.fasdfasdfsdff').hide();
$('#cambiarvoucher').click(function(){
    //$('.fasdfasdfsdff').hide();
    $('#voucer').prop('required', false);
    if ($('#cambiarvoucher').is(':checked')) {
         $('#voucer').prop('required', true);
        $('.fasdfasdfsdff').show();
    }else{
        $('.fasdfasdfsdff').hide();
         $('#voucer').prop('required', false);
    }
});
</script>

<?php } ?>

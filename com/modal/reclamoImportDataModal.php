<div class="row" style="margin:10px;">
    <div class="col-12 col-md-12">
        <input type="file" name="myfile" id="myfile" required class="file" onchange="checkfile(this);">
        <div>Archivos *.xlsx</div>
    </div>
</div>
<div class="row" id="dvResult" style="display:none;">
    <div class="col-12 col-md-12" id="result">
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#btnImportar").hide();
    });

    function checkfile(sender) {
        var validExts = [".xlsx"];
        var fileExt = sender.value;
        fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
        if (validExts.indexOf(fileExt) < 0) {
            // alert("Archivo no válido seleccionado, los archivos válidos son de " + validExts.toString() + " types.");
            swal({
                title: "",
                text: "Archivo no válido seleccionado, los archivos válidos son del tipo " + validExts.toString(),
                type: "error",
                //showCancelButton: true,
                //confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });

            document.getElementById("importForm").reset();
            $("#btnImportar").hide();
            return false;
        } else {
            $("#btnImportar").show();
            return true;
        }
    }

    function startUpload(){
        $("#importForm").hide();
        $("#f1_upload_process").show();
        return true;
    }
    function stopUpload(success, msgError){
        var result = '';
        $("#dvResult").show();
        if (success == 1){
            document.getElementById('result').innerHTML =
            '<span class="msg" style="color:green;">El archivo se cargó correctamente!<\/span><br/><br/>'+(($.trim(msgError)!="")? `<div style="height:100px; overflow:auto;">${msgError}</div>` : `` );
        }
        else {
            // document.getElementById('result').innerHTML = 
            // '<span class="emsg" style="color:red;">Hubo un error durante la carga del archivo!<\/span><br/><br/>'+(($.trim(msgError)!="")? `<div style="height:100px; overflow:auto;">${msgError}</div>` : `` );
            swal({
                title: "Error!",
                text: msgError,
                type: "error",
                //showCancelButton: true,
                //confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });
        }
        const file = document.querySelector('.file');
            file.value = '';
    
        $("#btnImportar").hide();
        $("#f1_upload_process").hide();
        $("#importForm").show();
      return true;   
    }
</script>
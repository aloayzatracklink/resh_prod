	<?php $nav="registrarCliente" ;?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
	irA("../ingresar");
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->registrarCliente=="si"): ?>
	<?php
	$mensaje="";
	$clienteObj=new Cliente();
	if (isset($_POST['registrarNatural'])) {
		if ($clienteObj) {
			$clienteObj->tipoCliente=ucwords(strtolower($_POST['tipoCliente']));
			$clienteObj->dni=$_POST['dni'];
			$clienteObj->nombre=ucwords(strtolower($_POST['nombre']));
			$clienteObj->apellidosC=ucwords(strtolower($_POST['apellidoss']));
			$clienteObj->departamento=$_POST['departamento'];
			$clienteObj->provincia=$_POST['provincia'];
			$clienteObj->distrito=$_POST['distrito'];
			$clienteObj->direccion=$_POST['direccion'];
			$clienteObj->referencia=$_POST['referencia'];
			$clienteObj->fechaNacimiento=$_POST['FechaNacimiento'];
			$clienteObj->telefono=$_POST['telefono'];
			$clienteObj->telefono1=$_POST['telefono1'];
			$clienteObj->correo=strtolower($_POST['correo']);
			$clienteObj->correo1=strtolower($_POST['correo1']);
			$clienteObj->idCreador=$session->idusuario;
			if ($clienteObj->registrarClienteNatural()) {
				header("Location:registrarVehiculo.php?id=$clienteObj->idcliente");
			}else{
	    		// irA("robo");
			}
		}
	}
	if (isset($_POST['registrarEmpresa'])) {
		if ($clienteObj) {
			$clienteObj->direccionL=$_POST['direccionL'];
			$clienteObj->provinciaL=$_POST['provinciaL'];
			$clienteObj->departamentoL=$_POST['departamentoL'];
			$clienteObj->distritoL=$_POST['distritoL'];
			$clienteObj->tipoCliente=$_POST['tipoCliente'];
			$clienteObj->dni=$_POST['dni'];
			$clienteObj->ruc=$_POST['ruc'];
			$clienteObj->razonSocial=ucwords(strtolower($_POST['razonSocial']));
			$clienteObj->nombre=ucwords(strtolower($_POST['nombre']));
			$clienteObj->apellidosC=ucwords(strtolower($_POST['apellidoss']));
			$clienteObj->departamento=$_POST['departamento'];
			$clienteObj->provincia=$_POST['provincia'];
			$clienteObj->distrito=$_POST['distrito'];
			$clienteObj->direccion=$_POST['direccion'];		
			$clienteObj->referencia=$_POST['referencia'];
			$clienteObj->fechaNacimiento=$_POST['FechaNacimiento'];
			$clienteObj->telefono=$_POST['telefono'];
			$clienteObj->telefono1=$_POST['telefono1'];
			$clienteObj->telefono2=$_POST['telefono2'];
			$clienteObj->telefono3=$_POST['telefono3'];
			$clienteObj->correo=$_POST['correo'];
			$clienteObj->correo1=$_POST['correo1'];
			$clienteObj->correo2=$_POST['correo2'];
			$clienteObj->sectorDesempenia=ucwords(strtolower($_POST['sectorDesempenia']));
			$clienteObj->idCreador=$session->idusuario;
			if ($clienteObj->registrarClienteEmpresa()) {
				header("Location:registrarVehiculo.php?id=$clienteObj->idcliente");
			}else{
	    		// irA("robo");
			}
		}
	}
	if (isset($_POST['verificar'])) {

	}
	?>
	<link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
	<link rel="stylesheet" href="../assets/css2/style2.css">
	<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>
	<style>
		.pagination1{margin:0;padding:0;}
.pagination1 li{
    display: inline;
    padding: 6px 10px 6px 10px;
    border: 1px solid #ddd;
    margin-right: -1px;
    font: 15px/20px Arial, Helvetica, sans-serif;
    background: #FFFFFF;
    box-shadow: inset 1px 1px 5px #F4F4F4;
}
.pagination1 li a{
    text-decoration:none;
    color: rgb(89, 141, 235);
}
.pagination1 li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination1 li.last {
    border-radius: 0px 5px 5px 0px;
}
.pagination1 li:hover{
    background: #CFF;
}
.pagination1 li.active{
    background: #F0F0F0;
    color: #333;
}
	</style>
	<body>
		<div id="wrapper">
			<nav class="navbar-default navbar-static-side" role="navigation">
				<div class="sidebar-collapse">
					<?php include("../includes/menuIzquierdo.php") ?>
				</div>
			</nav>
			<div id="page-wrapper" class="gray-bg">
				<div class="row border-bottom">
					<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
						<?php include("../includes/navegacionArriba.php") ?>
					</nav>
				</div>
				<div class="wrapper wrapper-content animated fadeInRight">
		<!-- nuevos clientes  -->
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Nuevos clientes</h5>
					</div>
					<div role="tablist">
						<input type="radio" title="Starks" name="tipoCliente" id="starks" role="tab" aria-controls="starks-panel" checked value="Natural"  />
						<input type="radio" title="Lanisters" name="tipoCliente" id="lannisters" role="tab" aria-controls="lannisters-panel" value="Empresa" />
						<div class="tabs" aria-hidden="true">
							<label for="starks" id="starks-tab" class="tab"><h4>Persona natural</h4></label>
							<label for="lannisters" id="lannisters-tab" class="tab"><h4>Empresa</h4></label>
						</div>
						<div class="panels">
							<ul id="starks-panel" class="panel active" role="tabpanel" aria-labelledby="starks-tab">

								<form role="form" method="POST" id="personaNatural" >
									<input type="radio" title="Starks" name="tipoCliente" id="starks" role="tab" aria-controls="starks-panel" checked value="Natural" />
									<div class="row">
										<div class="form-group col-md-2">
											<label for="dni" class="sr-only">DNI *</label>
											<input style="border: none;" type="text" placeholder="DNI *"  name="dni" class="form-control border-bottom " required   id="dni" autocomplete="off">
											<span id="resultado"></span>
										</div>
										<div class="form-group col-md-4">
											<label for="nombre" class="sr-only">Nombre *</label>
											<input style="border: none;" type="text" placeholder="Nombres *" id="nombre" name="nombre" class="form-control border-bottom text-capitalize"  required id="name">
										</div>
										<div class="form-group col-md-6">
											<label for="apellidoss" class="sr-only">Apellidos</label>
											<input style="border: none;" type="text" placeholder="Apellido Paterno *" id="Apellidoss *" name="apellidoss" class="form-control x2 text-capitalize border-bottom text-capitalize" required >
										</div>
									</div> 
									<div class="row">
										<div class="form-group col-lg-2">
											<label for="departamento1" class="sr-only">Departamento</label>
											<select  style="border: none;" class="departamento border-bottom form-control text-capitalize" required name="departamento" id="departamento1">
												<option class="text-capitalize" value="" disabled selected >Seleccione Departamento</option>
												<?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
												<?php if ($objDepartamento): ?>
													<?php foreach ($objDepartamento as $var): ?>
														<option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
													<?php endforeach ?>
												<?php else: ?>
												<?php endif ?>
											</select>
										</div>
										<div class="form-group col-lg-2" >
											<label for="corredor" class="sr-only">Provincia</label>
											<select  style="border: none;" class="provincia1 border-bottom form-control text-capitalize" required name="provincia"  >
											</select>
										</div>
										<div class="form-group col-lg-2" >
												<label for="distrito" class="sr-only">Distrito</label>
												<select  style="border: none;" class="distritos1 border-bottom form-control text-capitalize" required name="distrito"  >
												</select>
										</div>

												<div class="form-group col-lg-6">
													<label for="direccion" class="sr-only">Dirección</label>
													<input style="border: none;" type="text" placeholder="Dirección *" id="direccion *" name="direccion" class="form-control border-bottom text-capitalize" required autocomplete="off">
												</div>		                    
											</div>
											<div class="row">
												<div class="form-group col-lg-6">
													<label for="referencia" class="sr-only">Referencia *</label>
													<input style="border: none;" type="text" placeholder="Referencia *" id="referencia" name="referencia" class=" form-control border-bottom text-capitalize" required >
												</div>
												<div class="form-group col-lg-6">
													<label for="FechaNacimiento" class="sr-only">Fecha Nacimiento</label>

													<input type="text" style="border: none;" type="text" placeholder="FechaNacimiento "  name="Fecha Nacimiento *" class=" form-control border-bottom text-capitalize"  data-mask="99/99/9999" id="datetimepicker2"/>
												</div>
											</div> 
											<div class="row">
												<div class="form-group col-md-3">
													<label for="telefono" class="sr-only">Celular *</label>
													<input style="border: none;" type="text" placeholder="Celular *" id="telefono" name="telefono" class="form-control border-bottom" required data-mask="999 999 999">
												</div>
												<div class="form-group col-md-3">
													<label for="telefono1" class="sr-only">Teléfono </label>
													<input style="border: none;" type="text" placeholder="Telefono " id="telefono1" name="telefono1" class="form-control border-bottom" >
												</div>
												<div class="form-group col-md-3">
													<label for="correo" class="sr-only">Correo *</label>
													<input style="border: none;" type="email" placeholder="Correo *" id="correo" name="correo" class="form-control x2  border-bottom" required >
												</div>
												<div class="form-group col-md-3">
													<label for="correo1" class="sr-only">Correo Alternativo</label>
													<input style="border: none;" type="text" placeholder="Correo Alternativo" id="correo1" name="correo1" class="form-control border-bottom"  >
												</div>
											</div> 
											<div class="row">
												<div class="col-lg-6"><button class="btn btn-success col-lg-7" type="submit" name="registrarNatural">Registrar</button></div>
		                 </div>
		            </form>
		        </ul>
		        <ul id="lannisters-panel" class="panel" role="tabpanel" aria-labelledby="lannisters-tab">
		        	<form action="" method="post" id="empresa">
		        		<input type="radio" title="Lanisters" name="tipoCliente" id="lannisters" role="tab" aria-controls="lannisters-panel" value="Empresa" checked />

		        		<div class="row">
		        			<div class="form-group col-lg-3">
		        				<label for="ruc" class="sr-only">Ruc *</label>
		        				<input style="border: none;" type="text" placeholder="Ruc *" id="ruc" name="ruc" class="form-control  border-bottom" required >
		        				<span id="resultado1"></span>
		        			</div>
		        			<div class="form-group col-lg-6">
		        				<label for="razonSocial" class="sr-only">Razon Social *</label>
		        				<input style="border: none;"  type="text" placeholder="Razon Social *" id="razonSocial" name="razonSocial" class="form-control border-bottom text-capitalize"  required >
		        			</div>
		        			<div class="form-group col-lg-3">
		        				<label for="telefono" class="sr-only">Celular *</label>
		        				<input style="border: none;" type="text" placeholder="Celular *" id="telefono" name="telefono" class="form-control border-bottom text-capitalize" required data-mask="999 999 999">
		        			</div>			                     
		        		</div>
		        		<div class="row">
		        			<div class="form-group col-lg-2">
											<label for="departamento1" class="sr-only">Departamento</label>
											<select  style="border: none;" class="departamento2 border-bottom form-control text-capitalize" required name="departamentoL" >
												<option class="text-capitalize" value="" disabled selected >Seleccione Departamento</option>
												<?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
												<?php if ($objDepartamento): ?>
													<?php foreach ($objDepartamento as $var): ?>
														<option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
													<?php endforeach ?>
												<?php else: ?>
												<?php endif ?>
											</select>
										</div>
										<div class="form-group col-lg-2" >
											<label for="corredor" class="sr-only">Provincia</label>
											<select  style="border: none;" class="provincia2 border-bottom form-control text-capitalize" required name="provinciaL" >
											</select>
										</div>
										<div class="form-group col-lg-2" >
												<label for="distrito" class="sr-only">Distrito</label>			
												<select  style="border: none;" class="distritos2 border-bottom form-control text-capitalize" required name="distritoL"  >
												</select>
										</div>
		        			<div class="form-group col-lg-6">
		        				<label for="direccionL" class="sr-only">Dirección legal *</label>
		        				<input style="border: none;" type="text" placeholder="Dirección Legal *" id="direccion legal" name="direccionL" class="form-control border-bottom text-capitalize" required >
		        			</div>		                    
		        		</div> 
		        		<div class="row">
		        			<div class="form-group col-md-4">
		        				<label for="telefono1" class="sr-only">Teléfono </label>
		        				<input style="border: none;" type="text" placeholder="Telefono " id="telefono1" name="telefono1" class="form-control border-bottom" >
		        			</div>
		        			<div class="form-group col-md-4">
		        				<label for="correo" class="sr-only">Correo *</label>
		        				<input style="border: none;" type="email" placeholder="Correo *" id="correo" name="correo" class="form-control x2 border-bottom" required >
		        			</div>
		        			<div class="form-group col-md-4">
		        				<label for="sectorDesempenia" class="sr-only">Sector que desempeña</label>
		        				<input style="border: none;"	 type="text" placeholder="Sector que desempeña" id="sectorDesempenia" name="sectorDesempenia" class="form-control border-bottom text-capitalize"  >
		        			</div>
		        		</div>
		        		<div class="hr-line-dashed"></div>
		        		<h3>Representante legal</h3>
		        		<div class="row">
		        			<div class="form-group col-md-2">
		        				<label for="dniI" class="sr-only">DNI </label>
		        				<input style="border: none;" type="text" placeholder="DNI " id="numero" name="dni" class="form-control border-bottom "   data-mask="99999999" id="dniI">
		        			</div>
		        			<div class="form-group col-md-4">
		        				<label for="nombre" class="sr-only">Nombres </label>
		        				<input style="border: none;" type="text" placeholder="Nombres " id="nombre" name="nombre" class="form-control border-bottom text-capitalize"   id="name">
		        			</div>
		        			<div class="form-group col-md-6">
		        				<label for="apellidoss" class="sr-only">Apellidos </label>
		        				<input style="border: none;" type="text" placeholder="Apellidos " id="apellidoss" name="apellidoss" class="form-control x2 text-capitalize border-bottom text-capitalize"  >
		        			</div>
		        		</div> 
		        		<div class="row">		        			

		        			<div class="form-group col-lg-2">
											<label for="departamento1" class="sr-only">Departamento</label>
											<select  style="border: none;" class="departamento3 border-bottom form-control text-capitalize"  name="departamento" >
												<option class="text-capitalize" value="" disabled selected >Seleccione Departamento</option>
												<?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
												<?php if ($objDepartamento): ?>
													<?php foreach ($objDepartamento as $var): ?>
														<option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
													<?php endforeach ?>
												<?php else: ?>
												<?php endif ?>
											</select>
										</div>
										<div class="form-group col-lg-2" >
											<label for="corredor" class="sr-only">Provincia</label>
											<select  style="border: none;" class="provincia3 border-bottom form-control text-capitalize"  name="provincia" >
											</select>
										</div>
										<div class="form-group col-lg-2" >
												<label for="distrito" class="sr-only">Distrito</label>
												<select  style="border: none;" class="distritos3 border-bottom form-control text-capitalize"  name="distrito"  >
												</select>
										</div>
		        			<div class="form-group col-lg-6">
		        				<label for="direccion" class="sr-only">Dirección </label>
		        				<input style="border: none;" type="text" placeholder="Dirección " id="direccion" name="direccion" class="form-control border-bottom text-capitalize"  autocomplete="off">
		        			</div>		                    
		        		</div> 
		        		<div class="row">
		        			<div class="form-group col-lg-6">
		        				<label for="referencia" class="sr-only">Referencia </label>
		        				<input style="border: none;" type="text" placeholder="Referencia " id="referencia" name="referencia" class=" form-control border-bottom text-capitalize"  >
		        			</div>
		        			<div class="form-group col-lg-6">
		        				<label for="FechaNacimiento" class="sr-only">Fecha Nacimiento </label>
		        				<input style="border: none;" type="text" placeholder="FechaNacimiento " id="datetimepicker1" name="FechaNacimiento" class=" form-control border-bottom text-capitalize"  data-mask="99/99/9999">
		        			</div>

		        		</div> 
		        		<div class="row">
		        			<div class="form-group col-md-3">
		        				<label for="telefono2" class="sr-only">Celular </label>
		        				<input style="border: none;" type="text" placeholder="Celular " id="telefono2" name="telefono2" class="form-control border-bottom"  data-mask="999 999 999">
		        			</div>
		        			<div class="form-group col-md-3">
		        				<label for="telefono3" class="sr-only">Teléfono </label>
		        				<input style="border: none;" type="text" placeholder="Telefono " id="telefono3" name="telefono3" class="form-control border-bottom" >
		        			</div>
		        			<div class="form-group col-md-3">
		        				<label for="correo1" class="sr-only">Correo </label>
		        				<input style="border: none;" type="email" placeholder="Correo " id="correo1" name="correo1" class="form-control x2 border-bottom"  >
		        			</div>
		        			<div class="form-group col-md-3">
		        				<label for="correo2" class="sr-only">Correo Alternativo</label>
		        				<input style="border: none;" type="text" placeholder="Correo Alternativo" id="correo2" name="correo2" class="form-control border-bottom"  >
		        			</div>
		        		</div> 
		        		<div class="row">
		        			<div class="col-lg-12"><button class="btn btn-success col-lg-3" type="submit" name="registrarEmpresa">Registrar </button></div>
		        		</div>
		        	</form>
		        </ul>
		    </div>
		</div>
	</div>
</div>
</div>
  <div class="ibox-content" id="resultados">
		<div class="row">
	         <form action="exel/historialComH" method="POST">
	         <!--    <div class="col-md-4 esconder">
	               <label for="convenio" class=""></label>
	                 <select style="border: none;"  name="convenio" class="cd-selec form-control border-bottom" required id="convenio">
	                    <option value="Ventas">Ventas</option>	             
	                </select> 
	            </div>  -->
	            <div class="col-lg-4">
	               <label for="rango" class="">Seleccione rango fecha *</label>
	                <input style="border: none;"  class="cd-select form-control border-bottom" required  type="text" placeholder="Seleccione Rango de fecha" class="form-control" value="" name="fecha" id="rango" autocomplete="off">
	            </div>
	            <div class="col-md-4" id="resultadoRpor"> 
	            </div>    
			</form>
	    </div><br>
        <input type="text" class="form-control input-sm m-b-xs" id="filter"
        placeholder="Buscar">
        <div class="table-responsive">
        <table class=" table  table-hover" >
            <thead>
                <tr>
                    <th>#</th>                    
                    <th>Nombres</th>
                    <th>Placa</th>
                    <th>Fecha Enviada</th>
                    <th>Estado Vehículo</th>
                    <th>Estado Facturación</th>
                    <th>Canal</th>
                    <!-- <th>Vendedor</th> -->
                    <th>Acción</th> 
                </tr>
            </thead>  
             <tbody  id="resultadoh">          
            </tbody>  
        </table>
        </div>
    </div>
    <br><br><br><br><br>
</div>
<?php include("../includes/footer.php") ?>
</div>
</div>
<!-- Mainly scripts -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<!-- Steps -->
<script src="../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<?php include_once('../includes/script.php') ?>
<script>
$('#rango').daterangepicker({
    format: 'DD-MM-YYYY',
    endDate: moment(),
    showDropdowns: true,
    showWeekNumbers: true,
    timePicker: false,
    timePickerIncrement: 1,
    timePicker12Hour: false,
    ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
        'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Anterior Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    opens: 'right',
    drops: 'down',
    buttonClasses: ['btn', 'btn-sm'],
    applyClass: 'btn-primary',
    cancelClass: 'btn-default',
    separator: '/',
    locale: {
        applyLabel: 'Seleccionar',
        cancelLabel: 'Cancelar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Seleccionar Rango',
        daysOfWeek: ['Do', 'Lun', 'Mar', 'Mi', 'Ju', 'Vi','Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
    }
}, function(start, end, label) {               
     $('#rango').focus();
});        


$(document).ready(function(){
 	// var convenio=$("#convenio").val();
    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionComercialH',
        // data : {convenio:convenio},
        success : function(data)
        {
         $("#resultadoh").html(data);
        }
    });
    // $( "#convenio" ).change(function() { 
    //     var convenio = $(this).val(); 
    //     var fecha=$("#rango").val();      
    //     var page = $('#resultadoh').attr("data-page"); 
    //     $.ajax({
    //     type : 'POST',
    //     url  : 'pag/paginacionComercialH',
    //     data : {convenio:convenio,page:page,fecha:fecha},
    //     success : function(data)
    //     {
    //      $("#resultadoh").html(data);
    //     }
    //   });
    // });
     $("#rango").bind("keyup focus",function()
    {
        var fecha=$(this).val();
        var page = $("#resultadoh").attr("data-page"); 
        var convenio=$("#convenio").val();        
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionComercialH',
        data : {convenio:convenio,page:page,fecha:fecha},
        success : function(data)
        {
         $("#resultadoh").html(data);
        }
        });
    });
    $( "#rango" ).change(function() { 
        var fecha=$(this).val();
        var page = $("#resultadoh").attr("data-page"); 
        var convenio=$("#convenio").val();        
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionComercialH',
        data : {convenio:convenio,page:page,fecha:fecha},
        success : function(data)
        {
         $("#resultadoh").html(data);
        }
        });
    });
     
    $("#resultadoh").on( "click", ".pagination1 a", function (e){
        e.preventDefault();
        var fecha=$("#rango").val();
        var convenio=$("#convenio").val();        
        var page = $(this).attr("data-page"); 
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionComercialH',
        data : {convenio:convenio,page:page,fecha:fecha},
        success : function(data)
        {
         $("#resultadoh").html(data);
        }
        });

    });

$("#filter").bind("keyup focus",function()
    {   
        var datosS = $(this).val();
        var convenio=$("#convenio").val();        
        var fecha=$("#rango").val();
        datosS = datosS.replace('<>/"', '');
        if(datosS.length >= 1)
            {
        $.ajax({
                type : 'POST',
                url  : 'pag/resultadoBusquedaComercialH',
                data :  {convenio:convenio,fecha:fecha,datos:datosS},
                success : function(data)
                {
                    $("#resultadoh").html(data);
                }
            });
            return false;
        }else{
            var convenio=$("#convenio").val();        
            var fecha=$("#rango").val();
            var page = $("#resultadoh").attr("data-page");
            $.ajax({
            type : 'POST',
            url  : 'pag/paginacionComercialH',
            data : {convenio:convenio,page:page,fecha:fecha},
            success : function(data)
            {
             $("#resultadoh").html(data);
            }
            });
    
        }
  });
// $("#rango").bind("keyup focus",function()
//     {
//         var convenio = $("#convenio").val();
//         var fecha = $(this).val();  
//         $.ajax({
//           type : 'POST',
//           url  : 'pag/opeCreporte',          
//           data : {fecha:fecha,convenio:convenio},
//           success : function(data)
//           {
//            $("#resultadoRpor").html(data);
//          }
//        });
//         return false;
// });
// $("#convenio").change(function()
//     {
//         var convenio = $(this).val();  
//         var fecha = $("#rango").val();
//         $.ajax({
//           type : 'POST',
//           url  : 'pag/opeCreporte',          
//           data : {fecha:fecha,convenio:convenio},
//           success : function(data)
//           {
//            $("#resultado1").html(data);
//          }
//        });
//         return false;
// });
});
	$(".departamento").change(function(){provincias();});
	$(".departamento2").change(function(){provincias2();});
	$(".departamento3").change(function(){provincias3();});
	$(".provincia1").change(function(){distritos();});
	$(".provincia2").change(function(){distritos2();});
	$(".provincia3").change(function(){distritos3();});
	function provincias(){
		var iddepartamento = $(".departamento").val();
		$.ajax({				
			type : 'POST',
			url  : 'ajax/provincia',
			data :  'iddepartamento='+ iddepartamento,
			success : function(data)
			{
				$(".provincia1").html(data);
				         // $("#departamento").attr("disabled",false);
		    }
		});	
		return false;
	}
	function provincias2(){
		var iddepartamento = $(".departamento2").val();
		$.ajax({				
			type : 'POST',
			url  : 'ajax/provincia',
			data :  'iddepartamento='+ iddepartamento,
			success : function(data)
			{
				$(".provincia2").html(data);
					         // $("#departamento").attr("disabled",false);
			    }
			});
		return false;
	}
	function provincias3(){
		var iddepartamento = $(".departamento3").val();
		$.ajax({				
			type : 'POST',
			url  : 'ajax/provincia',
			data :  'iddepartamento='+ iddepartamento,
			success : function(data)
			{
				$(".provincia3").html(data);
					         // $("#departamento").attr("disabled",false);

					     }
					 });
		return false;
	}
	function distritos(){
		var idprovincia = $(".provincia1").val();
		$.ajax({				
			type : 'POST',
			url  : 'ajax/distrito',
			data :  'idprovincia='+ idprovincia,
			success : function(data)
			{
				$(".distritos1").html(data)
		    }
					 });
		return false;
	}
	function distritos2(){
		var idprovincia = $(".provincia2").val();
		$.ajax({				
			type : 'POST',
			url  : 'ajax/distrito',
			data :  'idprovincia='+ idprovincia,
			success : function(data)
			{
				$(".distritos2").html(data)
		    }
					 });
		return false;
	}
	function distritos3(){
		var idprovincia = $(".provincia3").val();
		$.ajax({				
			type : 'POST',
			url  : 'ajax/distrito',
			data :  'idprovincia='+ idprovincia,
			success : function(data)
			{
				$(".distritos3").html(data)
		    }
					 });
		return false;
	}
	
</script>
<script>

	$(document).ready(function(){
		$("#dni").keyup(function()
		{		
			var dni = $(this).val();
			if(dni.length == 8)
			{		
				$("#resultado").html('Verficando...');
				$.ajax({
					type : 'POST',
					url  : 'ajax/dni-check',
					data : $(this).serialize(),
					success : function(data)
					{
						$("#resultado").html(data);
					}
				});
				return false;
			}
			else
			{
				$("#resultado").html('');
			}			
		});
		$("#ruc").keyup(function()
		{		
			var ruc = $(this).val();
			if(ruc.length >10)
			{		
				$("#resultado1").html('Verficando...');
				$.ajax({
					type : 'POST',
					url  : 'ajax/ruc-check',
					data : $(this).serialize(),
					success : function(data)
					{
						$("#resultado1").html(data);
					}
				});
				return false;

			}
			else
			{
				$("#resultado1").html('');
			}
		});
		$('.footable').footable();
		$('.footable2').footable();

// 	 jQuery.datetimepicker.setLocale('es');
// 		$('#datetimepicker2').datetimepicker({	
	
// 	i18n:{
//         es:{
//          months:[
//          'Enero','Febrero','Marzo','Abril',
//          'Mayo','Junio','Julio','Agosto',
//          'Setiembre','Octubre','Noviembre','Diciembre',
//          ],
//          dayOfWeek:[
//          "Dom.", "Lun", "Mar", "Mier", 
//          "Jue", "Vie", "Sab.",
//          ]
//        }
//      },
// 	timepicker:false,
// 	format:'d/m/Y',
// 	formatDate:'Y/m/d',
	
// });
// 		$('#datetimepicker1').datetimepicker({	
// 	timepicker:false,
// 	format:'d/m/Y',
// 	formatDate:'Y/m/d'
// });

jQuery.validator.addMethod("exactlength", function(value, element, param) {
 return this.optional(element) || value.length == param;
}, $.validator.format("Ingrese {0} caracateres."));
$("#personaNatural").validate({
            rules: {
                nombre: {
                    required:true
                },
                dni:{
                    required:true,
                    exactlength:8
                },
                apellidoPaterno:{
                    required:true
                },
                apellidoMaterno:{
                    required:true
                },
                telefono:{
                    required:true,
                    minlength:9 
                }

            },
            messages: {

                nombre: {
                    required: "Es necesario ingresar nombres",

                },
                telefono: {
                    required: "Es necesario ingresar un numero de contacto",
                    minlength: jQuery.validator.format("Se nececita {0} caracteres!")
                },apellidoPaterno: {
                    required: "Es necesario ingresar el apellido Paterno",

                },apellidoMaterno: {
                    required: "Es necesario ingresar el apellido Materno",

                },departamento: {
                    required: "Seleccione departamento",

                },provincia: {
                    required: "Seleccione provincia",

                },distrito: {
                    required: "Seleccione distrito",

                },direccion: {
                    required: "Ingrese dirección",

                },correo: {
                    required: "es necesario un correo ",
                    email:"Ingrese un correo valido",

                },
                dni: {
                    required: "Numero de dni necesario"
                }

            }
        });$("#empresa").validate({});

	});

</script>

</body>

</html>
<?php else: ?> 
	<?php $session->salir();
	echo "NO NO NO NO ; presiona F5"
	?> 
<?php endif ?>

<?php
header('Content-Type: application/json');

require __DIR__ . "/../vendor/autoload.php";
require("../includes/configuracion.php");
require_once("../includes/Recursos.php");
require_once("../includes/baseDatos.php");
require("../includes/baseDatosOnix.php");
//require_once("../includes/Onix.php");
require("../includes/Onix.php");
require("../includes/Usuario.php");
require("class/AutentificacionSQLSrv.php");

$onix = new Onix();

$dni_ruc = isset($_GET['txtdni']) ? $_GET['txtdni'] : null;

$allChasis = $onix->chasisPlacaPorDniRuc($dni_ruc);

if ($allChasis !== false && !empty($allChasis)) {
    echo json_encode([
        "error" => false,
        "allChasis" => array_column($allChasis, 'idchasis'),
        "allPlacas" => array_column($allChasis, 'license'),
        "allidcustomer" => array_column($allChasis, 'idcustomer'),
        "allidvehicle" => array_column($allChasis, 'idvehicle')
    ], JSON_INVALID_UTF8_IGNORE);
} else {
    echo json_encode([
        "error" => true,
        "mensaje" => "No se encontraron chasis o placas asociadas."
    ], JSON_INVALID_UTF8_IGNORE);
}
?>
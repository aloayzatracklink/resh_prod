<?php $nav="comisionesR" ;?>
<link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<?php include("../includes/head.php") ?>
  <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/css/jquery-confirm.min.css">
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->comisiones=="si"): ?>
<?php 

    require_once '../mailphp/PHPMailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;

    $objetoComision=new Comisiones;
    $objetoCanal=new Canales;
    $facturacion=new DatosFacturacion;
    $echo=array();
    $errores=array();
 ?>
<?php 
    if (isset($_POST['procesar'])) {

if (Token::verificar($_POST['procesar'])) {
//        echo '<pre>';
//        die(var_dump($_POST));
        Log::registrarLog('comisiones',$_POST['comisionid'],$_POST,'editadoDesdeComisionesR');
     $facturacion->tipoCanal=$_POST['tipoCanal'];
     $facturacion->nombreCanal=$_POST['nombreCanal'];
     $facturacion->ejecutivoCanal=$_POST['ejecutivoCanal'];
     $facturacion->datosfacturacionid=$_POST['idfacturacion'];
     if (!$facturacion->actualizarCanales(null,null)) {
         die("ERROR EN ACTUALIZAR CANAL");
     }

        $objetoComision->idcomision=$_POST['comisionid'];
        $objetoComision->comision=$_POST['comision'];

        if (isset($_POST['moneda'])){
            $objetoCanal->moneda=$_POST['moneda'];
        }

        if (isset($_POST['monedaCanal'])){
            $objetoCanal->moneda=$_POST['monedaCanal'];
        }

        if ($_POST['agregarId']==0) {
            if(isset($_POST['type_commision'])){
                if($_POST['type_commision']==2) {
            if (isset($_POST['canalid'])) {
                $objetoCanal->moneda=$_POST['moneda'];

               if (isset($_POST['monedaCanal'])){
                    $objetoCanal->moneda=$_POST['monedaCanal'];
               }
                $objetoCanal->banco=$_POST['bancoCanal'];
                $objetoComision->borrarIDCANAL();

            }else{
                $objetoCanal->moneda=$_POST['monedaCanal'];
                if ($_POST['nombreBanco']=='') {
                    $objetoCanal->banco=$_POST['bancoCanal'];
                }else{
                    $objetoCanal->banco=$_POST['nombreBanco'];
                }
            }
            }}

            if (isset($_POST['numeroCuenta'])) {
                $objetoCanal->numeroCuenta = $_POST['numeroCuenta'];
            }

            if (isset($_POST['cci'])) {
                $objetoCanal->cci = $_POST['cci'];
            }


            if(isset($_POST['type_commision'])) {
                if ($_POST['type_commision'] == 1) {
                    $objetoCanal->ruc=$_POST['ndni'];
                }else{
                    if (isset($_POST['ruc'])) {
                        $objetoCanal->ruc=$_POST['ruc'];
                    }
                }
            }else{
                if (isset($_POST['ruc'])) {
                    $objetoCanal->ruc=$_POST['ruc'];
                }
            }

            if(isset($_POST['type_commision'])){
                if($_POST['type_commision'] == 1) {
                    $objetoCanal->titular=$_POST['interm'];
                }else{
                    if(isset($_POST['titular'])) {
                        $objetoCanal->titular=$_POST['titular'];
                    }
                }
            }else{
                if(isset($_POST['titular'])) {
                    $objetoCanal->titular=$_POST['titular'];
                }
            }

            if(isset($_POST['type_commision'])){
                if($_POST['type_commision'] == 1) {
                    $objetoCanal->idIntermediario = $_POST['idInterm'];
                }
            }

            if ($objetoCanal->registrarTitular()) {
                $objetoComision->idtitular=$objetoCanal->idtitular;
                $objetoComision->agregarIdTitular();
            }
        }else{
            $objetoCanal->idtitular=$_POST['agregarId'];
            if(isset($_POST['type_commision'])){
                if($_POST['type_commision'] == 1) {
                    $objetoCanal->titular=$_POST['interm'];
                }else{
                    if(isset($_POST['titular'])) {
                        $objetoCanal->titular=$_POST['titular'];
                    }
                }
            }else{
                if(isset($_POST['titular'])) {
                    $objetoCanal->titular=$_POST['titular'];
                }
            }
            $objetoCanal->actualizarDatoTIUTALARtitular();

            $objetoComision->idtitular=$_POST['agregarId'];
            $objetoComision->agregarIdTitular();
        }

if(isset($_POST['type_commision'])){
if($_POST['type_commision']==1) {
    $objetoComision->numeroReciboHonorario = 0;
    if (!$objetoComision->registrarNumeroFacturacion())
        $errores[] = "Error en registrar número de facturación";
}else{

    if (!empty($_POST['rhonorario'])) {
        $objetoComision->numeroReciboHonorario = $_POST['rhonorario'];
        if (!$objetoComision->registrarNumeroFacturacion())
            $errores[] = "Error en registrar número de facturación";
    }
}
}

        if(isset($_POST['type_commision'])){
            if($_POST['type_commision']==2) {
if (!empty($_FILES['recibo']['name'])) {

        $extencionesAbilitadas= array('jpg','jpeg','png','gif','pdf','JPG','JPEG','PNG','GIF','PDF','docx','DOCX');
        $nombreArchivo=$_FILES['recibo']['name'];        
        $tmp=explode('.', $nombreArchivo);
        $extencionArchivo=end($tmp);
        $tamanioArchivo=$_FILES['recibo']['size'];
        $ubicacionTemporal=$_FILES['recibo']['tmp_name'];
        if (!in_array($extencionArchivo,$extencionesAbilitadas)) {  
          $errores[]='La extencion de la imagen deve ser jpg jpeg png gif o pdf';
        }
        if ($tamanioArchivo>10485760) {
          $errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
        }
        $fecha=date("YmdHis");
        move_uploaded_file($ubicacionTemporal, '../public/vouchers/'.$fecha.$nombreArchivo); 
        $objetoComision->reciboHonorarios=$fecha.$nombreArchivo;

$clienteDatos=$_POST['datosCliente'];
    $numFac = $_POST['numeroFacturacion'];
$usuarioCrea=Usuario::obtenerUsuario('66');
$usuarioEje=Usuario::obtenerUsuario($session->idusuario);
$htmlContenido1="<body style=' margin: 0;padding: 0;font-family:Helvetica, Arial, sans-serif;    box-sizing: border-box;    font-size: 14px; background-color: #f6f6f6; '>
<table style=' background-color: #f6f6f6; width: 100%;'>
    <tr>
        <td></td>
        <td  width='600' style=' display: block !important;    max-width: 600px !important;    margin: 0 auto !important;    /* makes it centered */    clear: both !important;'>
            <div class='content' style='  max-width: 600px;    margin: 0 auto;    display: block;    padding: 20px;'>
                <table class='main' width='100%' cellpadding='0' cellspacing='0' style='background: #fff;    border: 1px solid #e9e9e9;    border-radius: 3px;'>
                    <tr>
                        <td style='font-size: 16px;    color: #fff;    font-weight: 500;    padding: 20px;    text-align: center;    border-radius: 3px 3px 0 0;  background: #1970b2;'>
                            COMPLETAR DATOS DE COMISION.
                        </td>
                    </tr>
                    <tr>
                        <td style='padding: 10px !important ;padding: 20px; vertical-align: top;'>
                            <table width='100%' cellpadding='0' cellspacing='0'>
                                
                                <tr>
                                    <td  style='padding: 0 0 20px;'>
                                        Estimada(o) $usuarioCrea->usuarioNombre $usuarioCrea->usuarioApellidoPaterno, el recibo de honorarios fue cargado para la comision del cliente:<strong>$clienteDatos </strong>, n° Facturación ($numFac) del ejecutivo : $usuarioEje->usuarioNombre $usuarioEje->usuarioApellidoPaterno<br> <br> completa los datos del canal para el pago de la comision.  </td>
                                </tr>
                                <tr>
                                    <td style='padding: 0 0 20px;text-align: center; '>
                                        <a href='http://resh.tracklink.pe/com/comisionesR' style='text-decoration: none;    color: #FFF;    background-color: #1970b2;    border: solid #1970b2;    border-width: 5px 10px;    line-height: 2;
    font-weight: bold;    text-align: center;    cursor: pointer;
    display: inline-block;    border-radius: 5px;    text-transform: capitalize;'>Completar datos</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='padding: 0 0 20px;'>
                                        Gracias por usar Resh.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div style=' width: 100%;    clear: both;    color: #999;    padding: 20px;'>
                    <table width='100%'>
                        <tr>
                            <td style='font-size: 12px; text-align: center;padding: 0 0 20px;' ><a href='#' style='color: #999;'>Desuscriberse</a> de estas alertas.</td>                        </tr>
                    </table>
                </div></div>
        </td>
        <td></td>
    </tr>
</table>
</body>
</html>";
/*
$mail->isSMTP();
$mail->SetLanguage( 'es', '../mailphp/PHPMailer/language/' );
$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->Host = gethostbyname('mail.tracklink.pe');
$mail->Port = 25;
$mail->SMTPAuth = false;
$mail->CharSet = 'UTF-8';
$mail->setFrom('no-responder@tracklink.pe','Resh');  
$mail->addReplyTo("kcabrera@tracklink.pe", 'Soporte resh');
$mail->addAddress("asistentecomercial@tracklink.pe");
$mail->addAddress("aacuna@tracklink.pe");
$mail->addAddress("kmelendez@tracklink.pe");
$mail->addAddress("raguedo@tracklink.pe");
 $mail->addAddress("kcabrera@tracklink.pe","test");
$mail->addCC($usuarioEje->usuario,$usuarioEje->usuarioNombre." ".$usuarioEje->usuarioApellidoPaterno);
$mail->addBCC("pruebas-ti@tracklink.pe");
$mail->Subject='Completar datos de comisión del ejecutivo: '.$usuarioEje->usuarioNombre.' ';
$mail->msgHTML($htmlContenido1);
$mail->AltBody = 'Estimado(as) '.$usuarioCrea->usuarioNombre." ".$usuarioCrea->usuarioApellidoPaterno.' Completa los datos del canal'; 
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {     

}*/
}
}else{

                $clienteDatos=$_POST['datosCliente'];
                $numFac = $_POST['numeroFacturacion'];
                $usuarioCrea=Usuario::obtenerUsuario('66');
                $usuarioEje=Usuario::obtenerUsuario($session->idusuario);
                $htmlContenido1="<body style=' margin: 0;padding: 0;font-family:Helvetica, Arial, sans-serif;    box-sizing: border-box;    font-size: 14px; background-color: #f6f6f6; '>
<table style=' background-color: #f6f6f6; width: 100%;'>
    <tr>
        <td></td>
        <td  width='600' style=' display: block !important;    max-width: 600px !important;    margin: 0 auto !important;    /* makes it centered */    clear: both !important;'>
            <div class='content' style='  max-width: 600px;    margin: 0 auto;    display: block;    padding: 20px;'>
                <table class='main' width='100%' cellpadding='0' cellspacing='0' style='background: #fff;    border: 1px solid #e9e9e9;    border-radius: 3px;'>
                    <tr>
                        <td style='font-size: 16px;    color: #fff;    font-weight: 500;    padding: 20px;    text-align: center;    border-radius: 3px 3px 0 0;  background: #1970b2;'>
                            COMPLETAR DATOS DE COMISION.
                        </td>
                    </tr>
                    <tr>
                        <td style='padding: 10px !important ;padding: 20px; vertical-align: top;'>
                            <table width='100%' cellpadding='0' cellspacing='0'>
                                
                                <tr>
                                    <td  style='padding: 0 0 20px;'>
                                        Estimada(o) $usuarioCrea->usuarioNombre $usuarioCrea->usuarioApellidoPaterno, el recibo de honorarios fue cargado para la comision del cliente:<strong>$clienteDatos </strong>, n° Facturación ($numFac) del ejecutivo : $usuarioEje->usuarioNombre $usuarioEje->usuarioApellidoPaterno<br> <br> completa los datos del canal para el pago de la comision.  </td>
                                </tr>
                                <tr>
                                    <td style='padding: 0 0 20px;text-align: center; '>
                                        <a href='http://resh.tracklink.pe/com/comisionesR' style='text-decoration: none;    color: #FFF;    background-color: #1970b2;    border: solid #1970b2;    border-width: 5px 10px;    line-height: 2;
    font-weight: bold;    text-align: center;    cursor: pointer;
    display: inline-block;    border-radius: 5px;    text-transform: capitalize;'>Completar datos</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='padding: 0 0 20px;'>
                                        Gracias por usar Resh.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div style=' width: 100%;    clear: both;    color: #999;    padding: 20px;'>
                    <table width='100%'>
                        <tr>
                            <td style='font-size: 12px; text-align: center;padding: 0 0 20px;' ><a href='#' style='color: #999;'>Desuscriberse</a> de estas alertas.</td>                        </tr>
                    </table>
                </div></div>
        </td>
        <td></td>
    </tr>
</table>
</body>
</html>";
                /*
                $mail->isSMTP();
                $mail->SetLanguage( 'es', '../mailphp/PHPMailer/language/' );
                $mail->SMTPDebug = 0;
                $mail->Debugoutput = 'html';
                $mail->Host = gethostbyname('mail.tracklink.pe');
                $mail->Port = 25;
                $mail->SMTPAuth = false;
                $mail->CharSet = 'UTF-8';
                $mail->setFrom('no-responder@tracklink.pe','Resh');
                $mail->addReplyTo("kcabrera@tracklink.pe", 'Soporte resh');
$mail->addAddress("asistentecomercial@tracklink.pe");
$mail->addAddress("aacuna@tracklink.pe");
$mail->addAddress("kmelendez@tracklink.pe");
$mail->addAddress("raguedo@tracklink.pe");
//                $mail->addAddress("kcabrera@tracklink.pe","test");
$mail->addCC($usuarioEje->usuario,$usuarioEje->usuarioNombre." ".$usuarioEje->usuarioApellidoPaterno);
$mail->addBCC("pruebas-ti@tracklink.pe");
                $mail->Subject='Completar datos de comisión del ejecutivo: '.$usuarioEje->usuarioNombre.' ';
                $mail->msgHTML($htmlContenido1);
                $mail->AltBody = 'Estimado(as) '.$usuarioCrea->usuarioNombre." ".$usuarioCrea->usuarioApellidoPaterno.' Completa los datos del canal';
                if (!$mail->send()) {
                    echo "Mailer Error: " . $mail->ErrorInfo;
                } else {

                }
*/
            }

        }
     if (isset($_POST['reciboH'])) {
        $objetoComision->reciboHonorarios=$_POST['reciboH'];          
      }

    $objetoComision->estado='59';
        if ($objetoComision->actualizarComision()) {
                $echo[]="Comision actutalizado con exito.";
        }else{
            die("fail");
        }
} else {
    $errores[] = "El formulario ya fue procesado, vericar.";
}
    }
 ?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
  <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<body>
   <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
   <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
   <link href="../assets/css/sweetalert2.css" rel="stylesheet">
<script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
<link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<link href="../assets/calendario/fullcalendar.min.css" rel="stylesheet">
<link href="../assets/calendario/fullcalendar.print.min.css" rel='stylesheet' media='print'>
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<link href='../assets/calendario/scheduler.min.css' rel='stylesheet' />
<link rel="stylesheet" href="../assets/css/jquery-confirm.min.css">

   <style>
    .widget p {
        display: inline-block;        
    }
    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }

    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }

    .reloj .caja-segundos {
        display: inline-block;
    }

    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }
    .xl65
    {
        mso-style-parent:style0 !important;
        mso-number-format:"\@" !important;
    }
    .my-2{
        margin-top: 2rem;
        margin-bottom: 2rem;
    }
   </style>
    <body>
    <style>
.pagination1{margin:0;padding:0;}
.pagination1 li{
    display: inline;
    padding: 6px 10px 6px 10px;
    border: 1px solid #ddd;
    margin-right: -1px;
    font: 15px/20px Arial, Helvetica, sans-serif;
    background: #FFFFFF;
    box-shadow: inset 1px 1px 5px #F4F4F4;
}
.pagination1 li a{
    text-decoration:none;
    color: rgb(89, 141, 235);
}
.pagination1 li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination1 li.last {
    border-radius: 0px 5px 5px 0px;
}
.pagination1 li:hover{
    background: #CFF;
}
.pagination1 li.active{
    background: #F0F0F0;
    color: #333;
}
</style>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <?php include("../includes/menuIzquierdo.php") ?>
               </div>
           </nav>
           <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>
                </nav>
            </div>
            <div class="wrapper wrapper-content">
                <div class="row ibox float-e-margins ibox-content ">
                    <div class="col-md-12">
                        <form action="PHPExel/phpExel" method="post">
                        <div class="row">
                            <div class="col-md-2">
                               <label for="rango" class="">fecha *</label>
                                <input style="border: none;"  class="cd-select form-control border-bottom" required  type="text" placeholder="Seleccione Rango de fecha" class="form-control" value="" name="fecha" id="rango" autocomplete="off">
                            </div>
                            <div class="col-md-2">  
                              <label class="control-label">Ejecutivo</label>
                                    <select style="border: none;" id="ejecutivoId"  class="cd-select form-control border-bottom" required name="ejecutivoId">
                                        <option value="" disabled selected>Seleccione ejecutivo *</option>
                                        <?php $ejecutivosComercial=Usuario::obtenerEjecutivosComercialConComision() ?>
                                        <?php foreach ($ejecutivosComercial as $var): ?>
                                            <option value="<?= $var->idusuario ?>"><?php echo $var->usuarioNombre ?> <?php echo $var->usuarioApellidoPaterno ?></option>
                                        <?php endforeach ?>
                                    </select>               
                            </div> 
                            <div class="col-md-2">                            
                                <span id="resultado1"></span>
                            </div>
                            <div class="col-md-2">
                               <label for="estado" class="">Estado </label>
                                 <select  style="border: none;" class="corredor border-bottom form-control text-capitalize" name="estado" id="estado">
                                    <option value="" disabled selected>Seleccione *</option>
                                        <option value="91">Para autorizacion</option>
                                        <option value="0">Pendiente</option>
                                        <option value="67">pagado</option>
                                        <option value="62">no pagado</option>
                                        <option value="59">Pendiente pago</option>
                                        <option value="">Todos</option>
                                  </select> 
                            </div>
                        <div class=" col-md-4 ">
                           <div class="widget" style="line-height: 0px;       
                           padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px">
                           <div class="fecha">
                            <p id="diaSemana" class="diaSemana">Martes</p>
                            <p id="dia" class="dia">27</p>
                            <p>de </p>
                            <p id="mes" class="mes">Octubre</p>
                            <p>del </p>
                            <p id="year" class="year">2015</p>
                        </div>

                        <div class="reloj">
                            <p id="horas" class="horas">11</p>
                            <p>:</p>
                            <p id="minutos" class="minutos">48</p>
                            <p>:</p>
                            <div class="caja-segundos">
                                <p id="ampm" class="ampm">AM</p>
                                <p id="segundos" class="segundos">12</p>
                            </div>
                        </div>
                    </div>

                         </form>
                </div>

            </div>
                <hr>
                <div class="row">
                    <a class="btn btn-primary" href="/ope/downinvdoc?tnk=eef56f6a7c51ab8999" title="Generar">
                        Enviar Reporte Comisiones
                        <span><i class="fa fa-send"></i></span>
                    </a>
                </div>
        </div>
    </div>
    <div class="ibox">      
    </div>
    <div class="ibox-content" id="resultados" style="overflow:auto;">
        <input type="text" class="form-control input-sm m-b-xs" id="filter"
        placeholder="Buscar" autofocus="">
        <table class="table  table-stripped table-hover table-responsive" >
             <thead>
                <tr>
                    <th>Trabajo</th>
                    <th>Cliente(s)</th>
                    <th>Placa</th>
                    <th>Chasis</th>
                    <th>Canal</th>
                    <th>Facturacion</th>
                    <th>Pago</th>
                    <th>Estado comisión</th>
                    <th>Tipo Pago Comisión</th>
                    <th>Fecha Registro</th>
                    <th>Ejecutivo</th>
                    <th>Acción</th>
                </tr>
            </thead> 
             <tbody  id="resultado">          
            </tbody>  
        </table>
    </div>
</div>
<?php include("../includes/footer.php") ?>
</div>
</div>
</div>
<!-- LOS MODALES AQUI  -->
<div class="modal inmodal" id="bateriasOModal"  tabindex="-1" role="dialog" aria-hidden="true"  data-keyboard="false" style="overflow-y: scroll;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">comisiones</h4>
                <small class="font-bold"><div class="form-group ">
                </div></small>
            </div>
            <form  role="form" method="post" enctype="multipart/form-data" >
                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>
                <!--<div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="submit" name="verificado" class="btn btn-primary" value="<?php echo Token::generar() ?>">Guardar</button>
                </div>-->
            </form>   
        </div>
    </div>
</div>
<!-- LOS MODALES AQUI  -->
<!-- <button class='btn' data-clipboard-action='copy' data-clipboard-target='p'>Copy</button> -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>

<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/clipboard.min.js"></script>
<script src="../assets/calendario/dist/sweetalert.min.js"></script>

    <!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="../assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Custom and plugin javascript -->
 <?php include_once('../includes/script.php') ?>
<script>
$('#rango,#rangoc').daterangepicker({
    format: 'DD-MM-YYYY',
    endDate: moment(),
    showDropdowns: true,
    showWeekNumbers: true,
    timePicker: false,
    timePickerIncrement: 1,
    timePicker12Hour: false,
    ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
        'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Anterior Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    opens: 'right',
    drops: 'down',
    buttonClasses: ['btn', 'btn-sm'],
    applyClass: 'btn-primary',
    cancelClass: 'btn-default',
    separator: '/',
    locale: {
        applyLabel: 'Seleccionar',
        cancelLabel: 'Cancelar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Seleccionar Rango',
        daysOfWeek: ['Do', 'Lun', 'Mar', 'Mi', 'Ju', 'Vi','Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
    }
}, function(start, end, label) {               
     // $('#rango').focus();
});  
 $( "#rango" ).change(function() {
    $('#rango').focus();
}); 
$( "#ejecutivoId" ).bind("click", function() {
        var fecha = $("#rango").val();
        var ejecutivoId = $(this).val();  

       
        $("#resultado1").html('Verficando...');
        $.ajax({

          type : 'POST',
          url  : 'ajax/comisionesR',          
          data : {ejecutivoId:ejecutivoId, fecha:fecha},
          success : function(data)
          {
           $("#resultado1").html(data);
         }
       });
        return false;
});
//////////
// $('#rango').length.change( function() {
   
$( "#rango" ).focus(function() {
        var fecha = $(this).val();  
        var ejecutivoId = $("#ejecutivoId").val();

        $.ajax({

          type : 'POST',
          url  : 'ajax/comisionesR',          
          data : {fecha:fecha,ejecutivoId:ejecutivoId},
          success : function(data)
          {
           $("#resultado1").html(data);
         }
       });
        return false;
});

<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
<?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
<?php
	endforeach
?>
 <?php endif ?> 
$(document).ready(function(){
    $("#resultado" ).load( "pag/paginacionComNew"); 
    $('.footable2').footable();
    $("#resultado").on( "click", ".pagination1 a", function (e){
        e.preventDefault();
        $(".se-pre-con").show(); 
        var page = $(this).attr("data-page"); 
        var ejecutivoComercial = $("#ejecutivoId").val();
        var fecha=$("#rango").val();
        var estado = $("#estado").val();
        $("#resultado").load("pag/paginacionComNew",{"page":page,"ejecutivoComercial":ejecutivoComercial,"fecha":fecha,"estado":estado}, function(){
            $(".se-pre-con").hide(); 
        });        
    });
    $("#filter").bind("focus  keyup", function() 
    {   
        var datosS = $(this).val();
        datosS = datosS.replace('<>/"', '');
        var ejecutivoComercial = $("#ejecutivoId").val();
        if(datosS.length >= 1)  
            {
        $.ajax({
                type : 'POST',
                url  : 'pag/resBusquedaComisiones',
                data :  'datosS='+ datosS+'&ejecutivoComercial='+ejecutivoComercial,
                success : function(data)
                {
                    $("#resultado").html(data);
                }
            });
            return false;
        }else{
            $("#resultado" ).load("pag/paginacionComNew");     
        }
    });
$("#rango").bind("focus",function()
{
  var fecha=$(this).val();
  var page = $("#resultado").attr("data-page"); 
  var estado = $("#estado").val();
  var ejecutivoComercial = $("#ejecutivoId").val();
  $.ajax({
      type : 'POST',
      url  : 'pag/paginacionComNew',
      data : {page:page,fecha:fecha,ejecutivoComercial:ejecutivoComercial,estado:estado},
      success : function(data)
      {
       $("#resultado").html(data);
      }
  });
});


$( "#estado" ).change(function() {
          var fecha=$("#rango").val();
          var page = $("#resultado").attr("data-page"); 
          var estado = $(this).val();
          var ejecutivoComercial = $("#ejecutivoId").val();
        $("#resultado").load("pag/paginacionComNew",{"page":page,"ejecutivoComercial":ejecutivoComercial,"fecha":fecha,"estado":estado}, function(){
        });
    });
$("#ejecutivoId").change(function() {
          var fecha=$("#rango").val();
          var page = $("#resultado").attr("data-page"); 
          var estado = $("#estado").val();
          var ejecutivoComercial = $(this).val();
        $("#resultado").load("pag/paginacionComNew",{"page":page,"ejecutivoComercial":ejecutivoComercial,"fecha":fecha,"estado":estado}, function(){
        });
    });



});

$('#bateriasOModal').on('show.bs.modal', function (e) {
    var rowid = $(e.relatedTarget).data('id');
    $.ajax({
        type : 'post',
            url : 'modal/comisionDataModal', 
            data :  'rowid='+ rowid, 
            success : function(data){
                $('.modal-body').html(data);
            }
        });
});

(function(){
    var actualizarHora = function(){
        var fecha = new Date(),
        horas = fecha.getHours(),
        ampm,
        minutos = fecha.getMinutes(),
        segundos = fecha.getSeconds(),
        diaSemana = fecha.getDay(),
        dia = fecha.getDate(),
        mes = fecha.getMonth(),
        year = fecha.getFullYear();
        var pHoras = document.getElementById('horas'),
        pAMPM = document.getElementById('ampm'),
        pMinutos = document.getElementById('minutos'),
        pSegundos = document.getElementById('segundos'),
        pDiaSemana = document.getElementById('diaSemana'),
        pDia = document.getElementById('dia'),
        pMes = document.getElementById('mes'),
        pYear = document.getElementById('year');                
        var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        pDiaSemana.textContent = semana[diaSemana];        
        pDia.textContent = dia;
        var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        pMes.textContent = meses[mes];
        pYear.textContent = year;
        if (horas >= 12) {
            horas = horas - 12;
            ampm = 'PM';
        } else {
            ampm = 'AM';
        }
        if (horas == 0 ){
            horas = 12;
        }
        pHoras.textContent = horas;
        pAMPM.textContent = ampm;
        if (minutos < 10){ minutos = "0" + minutos; }
        if (segundos < 10){ segundos = "0" + segundos; }

        pMinutos.textContent = minutos;
        pSegundos.textContent = segundos;
    };

    actualizarHora();
    var intervalo = setInterval(actualizarHora, 1000);
}())
</script>
   <script>
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
<?php else: ?> 
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?> 
<?php endif ?>

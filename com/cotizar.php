<?php 


///////////////////


















/////////////////// /////////////// 
/////////////////   ///         ///
//////   ///////    //          //
/////    //////    //          //
///      ////      //         //
 //       //       ////////////

 ?>
<?php header("Location:crearAcceso.php") ?>
<?php $nav="registrarcotizar" ;?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->cotizar=="si"): ?>    
<?php
$mensaje=""; 
$cotizarObj=new Cotizar();
        // COTIZAR PERSONAS NATURALES AQUI 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (isset($_POST['cotizarNatural'])) {

  unset($errores);
  $errores=array();
  $echo="";
    // Verificar existencia de cliente en cotizacions
  if ($cotizarObj->verificarExistenciaDni($_POST['dni'])) {
    $errores[]='EL número de DNI ya se encuentra registrado consulta, de este error con wilmanpabel@gmail.com';
  }

  $extencionesAbilitadas= array('jpg','jpeg','png','gif','');
        # code...
  $nombreArchivo=str_replace(' ', '', $_FILES['cotizar']['name']);
  $extencionArchivo=explode('.', $nombreArchivo);
  $extencionArchivo=strtolower(end($extencionArchivo));
  $tamanioArchivo=$_FILES['cotizar']['size'];
  $ubicacionTemporal=$_FILES['cotizar']['tmp_name'];


  if (!in_array($extencionArchivo,$extencionesAbilitadas)) {
    $errores[]='La extencion de la imagen deve ser jpg jpeg png gif';
  }
  if ($tamanioArchivo>5242880) {
    $errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
  }

  
  if ($cotizarObj && empty($errores) ){
    $cotizarObj->fecha=date("YmdHis");

    move_uploaded_file($ubicacionTemporal, '../public/vouchers/'.$cotizarObj->fecha.$nombreArchivo);            
    
    $cotizarObj->tipoCliente=$_POST['tipocotizar'];
    $cotizarObj->dni=$_POST['dni'];
    $cotizarObj->nombre=ucwords(strtolower($_POST['nombre']));
    $cotizarObj->apellidos=ucwords(strtolower($_POST['apellidos']));
    $cotizarObj->contacto=$_POST['celular'];
    $cotizarObj->contacto2=$_POST['telefono'];
    $cotizarObj->correo=$_POST['correo'];    
    $cotizarObj->cantidad=$_POST['cantidad'];    
    $cotizarObj->tiempoContrato=$_POST['tiempoContrato'];    
    $cotizarObj->plan=$_POST['plan'];    
    $cotizarObj->costo=$_POST['costo'];     
    $cotizarObj->estadoPago=$_POST['estadoPago'];
    $cotizarObj->observaciones=$_POST['observaciones'];
    if (!empty($_FILES['cotizar']['name'])) {
      $cotizarObj->voucher=$cotizarObj->fecha.$nombreArchivo;  
    }   
        // $cotizarObj->planid=$_POST['planid'];
    $cotizarObj->idCreador=$session->idusuario;
    if ($cotizarObj->registrarCotizacionNatural()) {
      $echo.="Cotizacio para ".$_POST['nombre']." creada";
                // header("Location:registrarVehiculo.php?id=$cotizarObj->idcotizar");
    }else{
                // irA("robo");
    }
  }



}
///////////////////////////////////////////////////////////////////////////////////////////
          // ///////////////COTIZAR EMPRESAS
//////////////////////////////////////////////////////////////////////////////////
if (isset($_POST['cotizarEmpresa'])) {
  unset($errores);
  $cotizarObj->fecha=date("YmdHis");
  $errores=array();
  $echo="";
    // Verificar existencia de cliente en cotizacion
  
  if ($cotizarObj->verificarPorRuc($_POST['ruc'])) {
    $errores[]='EL número de RUC ya se encuentra registrado consulta, de este error con wilmanpabel@gmail.com';
  }

  $extencionesAbilitadas= array('jpg','jpeg','png','gif','');
  $nombreArchivo=str_replace(' ', '', $_FILES['cotizarEmpresa']['name']);
  $extencionArchivo=strtolower(end(explode('.', $nombreArchivo)));
  $tamanioArchivo=$_FILES['cotizarEmpresa']['size'];
  $ubicacionTemporal=$_FILES['cotizarEmpresa']['tmp_name'];


  if (!in_array($extencionArchivo,$extencionesAbilitadas)) {
    $errores[]='La extencion de la imagen deve ser jpg jpeg png gif';
  }
  if ($tamanioArchivo>5242880) {
    $errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
  }
  if ($cotizarObj && empty($errores)) {

    $cotizarObj->tipoCliente=$_POST['tipocotizar'];
    $cotizarObj->ruc=$_POST['ruc'];
    $cotizarObj->razonSocial=ucwords(strtolower($_POST['razonSocial']));
    $cotizarObj->contacto=$_POST['telefono'];
    $cotizarObj->contacto1=$_POST['telefono1'];         
    $cotizarObj->cantidad=$_POST['cantidad']; 
    $cotizarObj->correo=$_POST['correo'];
    $cotizarObj->tiempoContrato=$_POST['tiempoContrato'];            
    $cotizarObj->plan=$_POST['plan'];           
    $cotizarObj->costo=$_POST['costo'];    
    $cotizarObj->estadoPago=$_POST['estadoPago'];
    $cotizarObj->observaciones=$_POST['observaciones'];
    if (!empty($_FILES['cotizarEmpresa']['name'])) {
      $cotizarObj->voucher=$cotizarObj->fecha.$nombreArchivo;  
      move_uploaded_file($ubicacionTemporal, '../public/vouchers/'.$cotizarObj->fecha.$nombreArchivo);
    }   
        // $cotizarObj->planid=$_POST['planid'];
    $cotizarObj->idCreador=$session->idusuario;
    if ($cotizarObj->registrarCotizacionEmpresa()) {
            $echo.=" Cotizacion para empresa ".$_POST['razonSocial']." creado";
                // header("Location:registrarVehiculo.php?id=$cotizarObj->idcotizar");
    }else{
                // irA("robo");
    }
  }
}
///////////////////////////////////////////////////////////////////////////////////////////
            // ///////////////FIN ----- COTIZAR EMPRESAS
///////////////////////////////////////////////////////////////////////////////////////////////////

if (isset($_POST['gestinarCotizacion'])) {
  if (isset($_POST['dniEditado'])) {
    
    $cotizarObj->dni=$_POST['dniEditado'];
    if($cotizarObj->actualizarDni($_POST['cotizacionidEditar'])){
      $echo="";
      $echo="Dni Editado";
    }

  } 
  if (isset($_POST['rucEditado'])) {
    $cotizarObj->ruc=$_POST['rucEditado'];
    if ($cotizarObj->actualizarRuc($_POST['cotizacionidEditar'])) {
      $echo="";
      $echo="Ruc editado ";
    }
  }
}
if (isset($_POST['cambiarEstadoCotizacion'])) {
  if (empty($_POST['razonEliminar'])) {
    $errores[]="Espeficique la razon por la que desea eliminar el registro ";
  }else{
    $echo="";   
    $estado="18";   
    $cotizarObj->razonCierre=$_POST['razonEliminar'];
    $cotizarObj->cambiarEstadoCotizacion($estado,$_POST['cotizacionidEditar']);
    $echo="Eliminado";
  }   

}



///lA VERIFICACION Y MIGRACION DE REGISTROS DEL CLIENTE HACIA LA OTRA TABLA DEL CLIENTE ....


if (isset($_POST['verificado'])){

  $clienteObj=new Cliente();
  $vehiculoObj=new Vehiculo();
  $datosFacturacionObj=new Contabilidad();
  $operacionesObj=new Operacion();
  $objCalendario=new Calendario();
  $objCanales=new Canales();
  $objComision=new Comisiones();
  $cotizacionid=$_POST['cotizacionid'];
  unset($errores);
  $errores=array();
      $programado="";

      if ($_POST['lugarInstacion']=='Taller') {
        $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
        $operacionesObj->direccionInstacion=$_POST['direccionTaller'];
       //  if (empty($_POST['FechaInstalacion'])) {
       //    $errores[]="Ingrese Fecha de instalacion en taller";
       // }

       if (empty($_POST['nombreContactoIT']) || empty($_POST['numeroContactoIT']) || empty($_POST['FechaInstalacion'])) {
        $operacionesObj->estado=0;
        }else{
        $operacionesObj->estado=14;
        $programado="si";
        $operacionesObj->fechaInstalacion=$_POST['FechaInstalacion'];
        $operacionesObj->nombreContacto=$_POST['nombreContactoIT'];
        $operacionesObj->numeroContacto=$_POST['numeroContactoIT'];
      //agregar la fecha de instalacion en calendario aqui---->
        $trabajo ="Instalacion ";
        $color="#1ab394";
        $colorLetra="#ffffff";
        $estado="  ESTADO = FALTA ASIGNAR ORTE Y TECNICO";
        $objCalendario->title=$trabajo." en ".$operacionesObj->lugarInstalacion." placa: ".$_POST['placa']." Marca ".$_POST['marca']." Modelo: ".$_POST['modelo']."".$estado;
        $objCalendario->color=$color;
        $objCalendario->textColor=$colorLetra;
        $objCalendario->startdate=$operacionesObj->fechaInstalacion;
      }
    }else
    if($_POST['lugarInstacion']=='Domicilio Cliente'){
      $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
      if (empty($_POST['departamentoInstalacionD'])||empty($_POST['provinciaInstalacionD'])||empty($_POST['distritoInstalacionD'])||empty($_POST['direccionInstalacionD'])||empty($_POST['referenciaInstalacionD']) || empty($_POST['FechaInstalacionD'])|| empty($_POST['nombreContactoID']) || empty($_POST['numeroContactoID'])) {
         $operacionesObj->estado=0;
     }
    else  {
      $operacionesObj->estado=14;
      $programado="si";
      $operacionesObj->fechaInstalacion=$_POST['FechaInstalacionD'];
      $operacionesObj->nombreContacto=$_POST['nombreContactoID'];
      $operacionesObj->numeroContacto=$_POST['numeroContactoID'];
      $operacionesObj->coordenadasInstalacion=$_POST['coordenadasInstalacion'];
      $operacionesObj->direccionInstacion=$_POST['departamentoInstalacionD']." ".$_POST['provinciaInstalacionD']." ".$_POST['distritoInstalacionD']." ".$_POST['direccionInstalacionD']." Referencia ..".$_POST['referenciaInstalacionD'];
      $trabajo ="Instalacion ";
      $color="#2ea1ea";
      $colorLetra="#ffffff";
      $estado="  ESTADO = FALTA ASIGNAR ORTE Y TECNICO";
      $objCalendario->title=$trabajo." en ".$operacionesObj->lugarInstalacion." placa: ".$_POST['placa']." Marca ".$_POST['marca']." Modelo: ".$_POST['modelo']."   .....    ".$estado;
      $objCalendario->color=$color;
      $objCalendario->textColor=$colorLetra;
      $objCalendario->startdate=$operacionesObj->fechaInstalacion;
    }
  }else
  if($_POST['lugarInstacion']=='Concesionario'){
   $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];  
  if (empty($_POST['departamentoInstalacionC'])||empty($_POST['provinciaInstalacionC'])||empty($_POST['distritoInstalacionC'])||empty($_POST['direccionInstalacionC'])||empty($_POST['referenciaInstalacionC']) || empty($_POST['concesionarioInstalacion']) || empty($_POST['FechaInstalacionC'])||empty($_POST['nombreContactoIC']) || empty($_POST['numeroContactoIC'])) {
     $operacionesObj->estado=0;
 }else{
  $operacionesObj->estado=14;
  $programado="si";
  $operacionesObj->fechaInstalacion=$_POST['FechaInstalacionC'];
  $operacionesObj->nombreContacto=$_POST['nombreContactoIC'];
  $operacionesObj->numeroContacto=$_POST['numeroContactoIC'];
  $operacionesObj->direccionInstacion=$_POST['departamentoInstalacionC']." ".$_POST['provinciaInstalacionC']." ".$_POST['distritoInstalacionC']." ".$_POST['direccionInstalacionC']." Referencia ..".$_POST['referenciaInstalacionC'];
  $operacionesObj->nombreConcecionarioInstalicion=$_POST['concesionarioInstalacion'];

  $trabajo ="Instalacion ";
  $color="#23c6c8";
  $colorLetra="#ffffff";
  $estado="  ESTADO = FALTA ASIGNAR ORTE Y TECNICO";
  $objCalendario->title=$trabajo." en ".$operacionesObj->lugarInstalacion." placa: ".$_POST['placa']." Marca ".$_POST['marca']." Modelo: ".$_POST['modelo']."   .....    ".$estado;
  $objCalendario->color=$color;
  $objCalendario->textColor=$colorLetra;
  $objCalendario->startdate=$operacionesObj->fechaInstalacion;

}
}


//fin de la información de la instalación--->


//hasta aqui lo estamos recibiendo----
    // registro de clientes
if (empty($errores)) {
  if ($_POST['tipoCliente']=="Natural") { 
   $clienteObj->tipoCliente=$_POST['tipoCliente'];
   $clienteObj->dni=$_POST['dniC'];     
   $clienteObj->nombre=$_POST['nombre'];     
   $clienteObj->apellidosC=$_POST['apellidosC'];     
   $clienteObj->departamento=$_POST['departamento'];     
   $clienteObj->provincia=$_POST['provincia'];     
   $clienteObj->distrito=$_POST['distrito'];     
   $clienteObj->direccion=$_POST['direccion'];     
   $clienteObj->referencia=$_POST['referencia'];     
   $clienteObj->telefono=$_POST['telefono'];     
   $clienteObj->telefono1=$_POST['telefono1'];     
   $clienteObj->correo=$_POST['correo'];     
   $clienteObj->correo1=$_POST['correo1'];     
   $clienteObj->fechaNacimiento=$_POST['fechaNacimiento'];
   $clienteObj->observaciones['observaciones'];
   if($clienteObj->registrarClienteNatural()){
    $cotizarObj->cambiarEstadoCotizacion('31',$cotizacionid);
  }

}else 
if ($_POST['tipoCliente']!="Natural") {
 
     // recibiendoo la datos de la empresa 
 $clienteObj->tipoCliente=$_POST['tipoCliente'];
 $clienteObj->ruc=$_POST['ruc'];
 $clienteObj->razonSocial=$_POST['razonSocial'];
 $clienteObj->departamentoL=$_POST['departamentoL'];
 $clienteObj->provinciaL=$_POST['provinciaL'];
 $clienteObj->distritoL=$_POST['distritoL'];
 $clienteObj->direccionL=$_POST['direccionL'];
 $clienteObj->telefono=$_POST['telefono'];     
 $clienteObj->telefono1=$_POST['telefono1'];     
 $clienteObj->correo=$_POST['correo'];  
 $clienteObj->correo1=$_POST['correo1'];      
   // RECIBIMOS LOS DATOS DEL RERESENTATNE LEGAL AQUI
 $clienteObj->dni=$_POST['dniC'];
 $clienteObj->nombre=$_POST['nombre'];     
 $clienteObj->apellidosC=$_POST['apellidosC'];     
 $clienteObj->departamento=$_POST['departamento'];     
 $clienteObj->provincia=$_POST['provincia'];     
 $clienteObj->distrito=$_POST['distrito'];     
 $clienteObj->direccion=$_POST['direccion'];     
 $clienteObj->referencia=$_POST['referencia'];     
 $clienteObj->telefono2=$_POST['telefono2'];
 $clienteObj->correo2=$_POST['correo2'];    
 $clienteObj->correo3=$_POST['correo3'];    
 $clienteObj->fechaNacimiento=$_POST['fechaNacimiento'];
 $clienteObj->observaciones['observaciones'];
 
 
   // $clienteObj->
 if($clienteObj->registrarClienteEmpresa()){
   $cotizarObj->cambiarEstadoCotizacion('31',$cotizacionid);
 }
}
      // die();
         //registrar los clientes de tipo empresa ...


    // fin de registro de clientes
///the voucher here  ///
$voucherr="";
if (isset($_POST['voucher'])) {
  $voucherr=$_POST['voucher'];  
}
////////////////

    //registro de todo los vehículos
$field_values_array=$_POST['field_name'];
$a=0;
foreach ($field_values_array[0] as $val) {
  $placa[$a]=$val;
  $a++;
}

$b=0;
foreach ($field_values_array[1] as $val) {
  $chasis[$b]=$val;
  $b++;
}

$b=0;
foreach ($field_values_array[2] as $val) {
  $motor[$b]=$val;
  $b++;
}

$c=0;
foreach ($field_values_array[3] as $val) {
  $marca[$c]=$val;
  $c++;
}

$d=0;
foreach ($field_values_array[4] as $val) {
  $modelo[$d]=$val;
  $d++;
}

$e=0;
foreach ($field_values_array[5] as $val) {
  $color[$e]=$val;
  $e++;
}

$f=0;
foreach ($field_values_array[6] as $val) {
  $anio[$f]=$val;
  $f++;
}

$g=0;
foreach ($field_values_array[7] as $val) {
  $seguro[$g]=$val;
  $g++;
}

$h=0;
foreach ($field_values_array[11] as $val) {
  $concesionario[$h]=$val;
  $h++;
}

$i=0;
foreach ($field_values_array[8] as $val) {
  $numeroTramiteSeguro[$i]=$val;
  $i++;
}

$j=0;
foreach ($field_values_array[9] as $val) {
  $numeroPoliza[$j]=$val;
  $j++;
}

$k=0;
foreach ($field_values_array[10] as $val) {
  $corredor[$k]=$val;
  $k++;
}

$datosVehiculo['placa']=$placa;
$datosVehiculo['chasis']=$chasis;
$datosVehiculo['motor']=$motor;
$datosVehiculo['marca']=$marca;
$datosVehiculo['modelo']=$modelo;
$datosVehiculo['color']=$color;
$datosVehiculo['anio']=$anio;
$datosVehiculo['seguro']=$seguro;
$datosVehiculo['concesionario']=$concesionario;
$datosVehiculo['numeroTramiteSeguro']=$numeroTramiteSeguro;
$datosVehiculo['numeroPoliza']=$numeroPoliza;
$datosVehiculo['corredor']=$corredor;


$datosVehiculo['plan']=$_POST['plan'];
$datosVehiculo['tiempo']=$_POST['tiempoContrato'];
//Fin de la recoleccion de los Datos de vehiculo aqui .... ---> 


for ($i=0; $i <count($datosVehiculo['placa']) ; $i++) { 


  $idcliente=$clienteObj->idcliente;

  $datosVehiculo['placa_i']=$datosVehiculo['placa'][$i];
  $datosVehiculo['chasis_i']=$datosVehiculo['chasis'][$i];
  $datosVehiculo['motor_i']=$datosVehiculo['motor'][$i];
  $datosVehiculo['marca_i']=$datosVehiculo['marca'][$i];
  $datosVehiculo['modelo_i']=$datosVehiculo['modelo'][$i];
  $datosVehiculo['color_i']=$datosVehiculo['color'][$i];
  $datosVehiculo['anio_i']=$datosVehiculo['anio'][$i];
  $datosVehiculo['seguro_i']=$datosVehiculo['seguro'][$i];
  $datosVehiculo['concesionario_i']=$datosVehiculo['concesionario'][$i];
  $datosVehiculo['numeroTramiteSeguro_i']=$datosVehiculo['numeroTramiteSeguro'][$i];
  $datosVehiculo['numeroPoliza_i']=$datosVehiculo['numeroPoliza'][$i];
  $datosVehiculo['corredor_i']=$datosVehiculo['corredor'][$i];
  $datosVehiculo['plan'];
  $datosVehiculo['tiempo'];
  
    ///calendario aqui /////

  $objCalendario->title=$trabajo." en ".$operacionesObj->lugarInstalacion." placa: ".$datosVehiculo['placa'][$i]." Marca ".$datosVehiculo['marca'][$i]." Modelo: ".$datosVehiculo['modelo'][$i]."".$estado;
  $objCalendario->textColor=$colorLetra;
  $objCalendario->startdate=$operacionesObj->fechaInstalacion;
    //fin calendario ///
  if ($vehiculoObj->registrarVehiculosDedeVerificacion($idcliente,$datosVehiculo)) {   
        ////DATOS DE FACTURACIÓN ////// HERE :v
    $idvehiculo=$vehiculoObj->idvehiculo;
            //Registro de datos de facturacion.
    if ($_POST['tipoDocumentoFacturacion']=='Boleta') {
      $datosFacturacionObj->idcliente=$clienteObj->idcliente;
      $datosFacturacionObj->idvehiculos=$idvehiculo;
      $datosFacturacionObj->tipoDocumentoFacturacion=$_POST['tipoDocumentoFacturacion'];
      $datosFacturacionObj->dniFacturacion=$_POST['dniFacturacion'];
      $datosFacturacionObj->razonSocialFacturacion=$_POST['nombre']." ".$_POST['apellidosC']; 
      $datosFacturacionObj->direccionFiscal=$_POST['departamento']." ".$_POST['provincia']." ".$_POST['distrito']." ".$_POST['direccion'];
      $datosFacturacionObj->telefonoFacturacion=$_POST['telefono'];
      $datosFacturacionObj->voucher=$voucherr;
      $datosFacturacionObj->plan=$_POST['plan'];
      $datosFacturacionObj->tiempo=$_POST['tiempoContrato'];
      $datosFacturacionObj->costo=$_POST['costo'];
      $datosFacturacionObj->observaciones=$_POST['observaciones'];
      if ($datosFacturacionObj->registrarDatosDeFacturacion()) {
        
      }
    }else
    if($_POST['tipoDocumentoFacturacion']!='Boleta'){
      $datosFacturacionObj->idcliente=$clienteObj->idcliente;
      $datosFacturacionObj->idvehiculos=$idvehiculo;
      $datosFacturacionObj->tipoDocumentoFacturacion=$_POST['tipoDocumentoFacturacion'];
      $datosFacturacionObj->rucFacturacion=$_POST['rucFacturacion'];
      $datosFacturacionObj->razonSocialFacturacion=$_POST['razonSocialFacturacion'];
      $datosFacturacionObj->direccionFiscal=$_POST['direccionFiscal'];
      $datosFacturacionObj->telefonoFacturacion=$_POST['telefonoFacturacion'];
      $datosFacturacionObj->direccionEntrega=$_POST['direccionEntrega'];
      $datosFacturacionObj->voucher=$voucherr;
      $datosFacturacionObj->plan=$_POST['plan'];
      $datosFacturacionObj->tiempo=$_POST['tiempoContrato'];
      $datosFacturacionObj->costo=$_POST['costo'];        
      $datosFacturacionObj->observaciones=$_POST['observaciones'];
      if ($datosFacturacionObj->registrarDatosDeFacturacion()) {

      }
    }else{
      $errores="Fail detected facturacion data";
    }
//////////////////////////
   //fin de registro de datos de facturacion here ... ... :v :v :v 
    $operacionesObj->datosfacturacionid=$datosFacturacionObj->datosfacturacionid;
    $operacionesObj->programarInstalacion($idvehiculo,$idcliente);
        //calendario -----///
    $objCalendario->idoperacion=$operacionesObj->idoperacion;
    
    if ($programado="si") {
      $objCalendario->registrarFechaInstalacion();
    } 
    
    
        ///the calendario --//
    //////////////////////////// CANAL CANAL CANAL CANAL /////////////
    if (isset($_POST['canalId'])) {      
      $datosCanales=$objCanales->otenerDatosCanal($_POST['canalId']);
    }else{
      $datosCanales=false;
    }
    
    if ($datosCanales) {
      //Update
      $objCanales->idcanal=$datosCanales->idcanal;
      $datosFacturacionObj->idcanal=$objCanales->idcanal;
      if (!$datosFacturacionObj->agregarIdCanal()) {$errores[]="Eror en reugistra id de canal";}
      if (isset($_POST['comision'])) {
        $objCanales->banco=strtoupper($_POST['bancoCanal']);
        $objCanales->numeroCuenta=$_POST['numeroCuenta'];
        $objCanales->moneda=strtoupper($_POST['monedaCanal']);
        $objCanales->cci=strtoupper($_POST['cci']);
        $objCanales->canalId=strtoupper($_POST['canalId']);
        $objCanales->ejecutivoCanal=strtoupper($_POST['ejecutivoCanal']);
        $objCanales->titular=strtoupper($_POST['ejecutivoCanal']);
        $objCanales->nombreCanal=strtoupper($_POST['nombreCanal']);
      }      
      if (!$objCanales->actualizarDatosCanal()) {
        $errores[]="Error en actualizar datos de canal";
      }else{
         //registramos las comisiones para este canal  con este cliente y vehículo :V 
        if (isset($_POST['comision'])) {
          $objComision->idcliente=$idcliente;
          $objComision->idcanal=$datosCanales->idcanal;
          $objComision->idvehiculo=$idvehiculo;
          $objComision->idfacturacion=$datosFacturacionObj->datosfacturacionid;
          $objComision->montoTotal=$_POST['costo'];

          $objComision->comision=$_POST['montoComision']/count($datosVehiculo['placa']);

          ////////
          if (!empty($voucherr)) {
            $objComision->fechapago=date("YmdHis");
          }
          ////////////
          $objComision->documentoFacturacion=$_POST['tipoDocumentoFacturacion'];
          if (!$objComision->registrarComision()) {
            $errores[]="Error en registrar datos de comision";
          }
        }


      }

    }else{
      //Ingresar nuevo
      $objCanales->tipoCanal=$_POST['tipoCanal'];
      $objCanales->nombreCanal=strtoupper($_POST['nombreCanal']);
      if (isset($_POST['ejecutivoCanal'])) {
        $objCanalesD=Canales::obtenerCanalDesdeNombresYEjecutivo($_POST['canal'],$_POST['ejecutivoCanal']);
            if ($objCanalesD) {              
                if (isset($_POST['comision'])) {
                $objComision->idcliente=$idcliente;
                $objComision->idcanal=$objCanalesD->idcanal;
                $objComision->idvehiculo=$idvehiculo;
                $objComision->idfacturacion=$datosFacturacionObj->datosfacturacionid;
                $objComision->montoTotal=$_POST['costo'];
                $objComision->comision=$_POST['montoComision']/count($datosVehiculo['placa']);
                  if (!empty($voucherr)) {
                    $objComision->fechapago=date("YmdHis");
                  }
                $objComision->documentoFacturacion=$_POST['tipoDocumentoFacturacion'];
                  if (!$objComision->registrarComision()) {
                    $errores[]="Error en registrar datos de comision";
                  }
                }                
                $datosFacturacionObj->idcanal=$objCanalesD->idcanal;
                if (!$datosFacturacionObj->agregarIdCanal()) {$errores[]="Eror en reugistra id de canal";}  

            }else{
                $objCanales->nombreCanal=strtoupper($_POST['canal']);
                $objCanales->ejecutivoCanal=strtoupper($_POST['ejecutivoCanal']);
                if (isset($_POST['comision'])) {
                  $objCanales->cobraComision=$_POST['comision'];
                  $objCanales->banco=strtoupper($_POST['bancoCanal']);
                  $objCanales->numeroCuenta=$_POST['numeroCuenta'];
                  $objCanales->moneda=strtoupper($_POST['monedaCanal']);
                  $objCanales->cci=strtoupper($_POST['cci']);
                  $objCanales->canalId=strtoupper($_POST['canalId']);
                  $objCanales->titular=strtoupper($_POST['ejecutivoCanal']);
                }      
                if (!$objCanales->registrarCanal()) {
                  $errores[]="Error en registrar canal";
                }else{
                  $datosFacturacionObj->idcanal=$objCanales->idcanal;
                  if (!$datosFacturacionObj->agregarIdCanal()) {$errores[]="Eror en reugistra id de canal";}  
                  if (isset($_POST['comision'])) {
                  $objComision->idcliente=$idcliente;
                  $objComision->idcanal=$objCanales->idcanal;
                  $objComision->idvehiculo=$idvehiculo;
                  $objComision->idfacturacion=$datosFacturacionObj->datosfacturacionid;
                  $objComision->montoTotal=$_POST['costo'];
                  $objComision->comision=$_POST['montoComision']/count($datosVehiculo['placa']);
                    if (!empty($voucherr)) {
                      $objComision->fechapago=date("YmdHis");
                    }
                  $objComision->documentoFacturacion=$_POST['tipoDocumentoFacturacion'];
                    if (!$objComision->registrarComision()) {
                      $errores[]="Error en registrar datos de comision";
                    }
                  }            
                }
            }                 
      }


  



    }
        ////////////////////////END CANAL CANAL///////////////////////////
    
  }else{
    die("Fail en registrar los vehiculos");
  }
}

    //fin del registro de todo los vehículos

    // inicio de registro de contacto autorizaados here  ** *

$contenidoContactos=$_POST['contactos'];

$z=0;
foreach ($contenidoContactos[0] as $val) {
  $dni[$z]=$val;
  $z++;
}
$y=0;
foreach ($contenidoContactos[1] as $val) {
  $nombre[$y]=$val;
  $y++;
}
$x=0;
foreach ($contenidoContactos[2] as $val) {
  $apellidos[$x]=$val;
  $x++;
}
$v=0;
foreach ($contenidoContactos[3] as $val) {
  $telefono[$v]=$val;
  $v++;
}
$u=0;
foreach ($contenidoContactos[4] as $val) {
  $telefono1[$u]=$val;
  $u++;
}
$w=0;
foreach ($contenidoContactos[5] as $val) {
  $correo[$w]=$val;
  $w++;
}

$t=0;
foreach ($contenidoContactos[6] as $val) {
  $parentesco[$t]=$val;
  $t++;
}


$datosContactosAutorizados['dni']=$dni;
$datosContactosAutorizados['nombre']=$nombre;
$datosContactosAutorizados['apellidos']=$apellidos;
$datosContactosAutorizados['telefono']=$telefono;
$datosContactosAutorizados['telefono1']=$telefono1;
$datosContactosAutorizados['correo']=$correo;
$datosContactosAutorizados['parentesco']=$parentesco;


for ($i=0; $i < count($datosContactosAutorizados['dni']) ; $i++) { 
  $idcliente=$clienteObj->idcliente;
  $datosContactosAutorizados['dni_i']=$datosContactosAutorizados['dni'][$i];
  $datosContactosAutorizados['nombre_i']=$datosContactosAutorizados['nombre'][$i];
  $datosContactosAutorizados['apellidos_i']=$datosContactosAutorizados['apellidos'][$i];
  $datosContactosAutorizados['telefono_i']=$datosContactosAutorizados['telefono'][$i];
  $datosContactosAutorizados['telefono1_i']=$datosContactosAutorizados['telefono1'][$i];
  $datosContactosAutorizados['correo_i']=$datosContactosAutorizados['correo'][$i];
  $datosContactosAutorizados['parentesco_i']=$datosContactosAutorizados['parentesco'][$i];
  if($clienteObj->registrarContactoAutorizadoDesdeVerificacion($datosContactosAutorizados,$idcliente)){
    $echo="Instalación programada existosamente.";
  }
}

}    

//Fin de ingreso de clientes ... fin here :V


//registrar datos de instalación aquui ##




}


?>
<link rel="stylesheet" href="../assets/css2/style2.css">
<link rel="stylesheet" href="../assets/css2/style3.css">
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

<body>

  <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
      <div class="sidebar-collapse">
        <?php include("../includes/menuIzquierdo.php") ?>
      </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
          <?php include("../includes/navegacionArriba.php") ?>

        </nav>        
      </div>
      <div class="wrapper wrapper-content animated fadeInRight">


        <div class="row">
          <div class="col-lg-12">
            <div class="ibox float-e-margins">
           
            
              <div class="ibox-title">
                <h5>Nuevos cotizar</h5>
              </div>

              <div role="tablist">

                <input type="radio" title="Starks" name="tipocotizar" id="starks" role="tab" aria-controls="starks-panel" checked value="Natural" style="display: none" />
                <input type="radio" title="Lanisters" name="tipocotizar" id="lannisters" role="tab" aria-controls="lannisters-panel" value="Empresa" style="display: none;" />

                <div class="tabs" aria-hidden="true">
                  <label for="starks" id="starks-tab" class="tab"><h4>Persona natural</h4></label>
                  <label for="lannisters" id="lannisters-tab" class="tab"><h4>Empresa</h4></label>
                </div>

                <div class="panels">
                  <ul id="starks-panel" class="panel active" role="tabpanel" aria-labelledby="starks-tab">

                    <form role="form" method="post" id="personaNatural"  enctype="multipart/form-data">
                      <input type="radio" title="Starks" name="tipocotizar" id="starks" role="tab" aria-controls="starks-panel" checked value="Natural" style="display: none;" />

                      <div class="row">

                        <div class="form-group col-md-2">
                          <label for="dniE" class="sr-only">DNI *</label>
                          <input style="border: none;" type="number" placeholder="DNI *" name="dni" class="form-control border-bottom " required   id="dniE" autofocus="" autocomplete="off">
                          <span id="resultado"></span>
                        </div>
                        <div class="form-group col-md-4">
                          <label for="nombre" class="sr-only">Nombre</label>
                          <input style="border: none;" type="text" placeholder="Nombres *" id="nombre" name="nombre" class="form-control border-bottom text-capitalize"  required id="name">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="apellidos" class="sr-only">Apellidos</label>
                          <input style="border: none;" type="text" placeholder="Apellidos *" id="apellidos" name="apellidos" class="form-control x2 text-capitalize border-bottom text-capitalize" required >
                        </div>

                      </div> 
                      <div class="row">
                        <div class="form-group col-md-3">
                          <label for="celular" class="sr-only">Celular *</label>
                          <input style="border: none;" type="text" placeholder="Celular *" id="celular" name="celular" class="form-control border-bottom text-capitalize" required >
                        </div>
                        
                        <div class="form-group col-lg-3">
                          <label for="telefono" class="sr-only">Telefono </label>
                          <input style="border: none;" type="number" placeholder="Telefono " id="telefono" name="telefono" class=" form-control border-bottom" >
                        </div>

                        <div class="form-group col-md-4">
                          <label for="correo" class="sr-only">Correo</label>
                          <input style="border: none;" type="email" placeholder="Correo *" id="correo" name="correo" class="form-control x2 border-bottom" required >
                        </div>
                        
                        <div class="form-group col-md-2">
                          <label for="cantidad" class="sr-only">Cantidad Vehiculos*</label>
                          <input style="border: none;" type="number" placeholder="Cantidad Vehiculos*" id="cantidad" name="cantidad" class="form-control border-bottom"  required="">
                        </div>

                      </div> 
                      <div class="row">
                       <div class="form-group col-md-3">
                         <label for="plan" class="sr-only">Plan *</label>
                         <select  style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
                          <?php if (isset($cotizarObj->plan)): ?>
                            <option class="" value="<?php echo $cotizarObj->plan ?>"  selected ><?php echo $cotizarObj->plan ?></option>
                          <?php endif ?>
                          <option class="" value="" disabled selected >Seleccione Plan *</option>
                          <?php Cliente::obtenerPlan() ?>      
                        </select> 
                      </div>

                      <div class="form-group col-md-2">
                        <label for="costo" class="sr-only">Costo *</label>
                        <input style="border: none;" type="number" placeholder="Costo *" id="costo" name="costo" class="form-control x2 text-capitalize border-bottom" required >
                      </div>
                      <div class="col-md-2">
                        <select style="border: none;" id="selectNav"  class="cd-select form-control border-bottom" required name="estadoPago" >
                          <option value="" disabled selected>Seleccione pago *</option>
                          <option value="27">No Pagado</option>
                          <option value="28">Pagó</option>
                          <option value="41">pago en caja o taller</option>
                        </select>   
                      </div>
                      <div class="form-group col-md-5">
                       <!-- iniciooooo -->
                       <div class="row">

                        <div class="col-md-8">
                         <div class="content-panel d1">
                          <h3>Elegir opción anterior</h3>
                        </div>
                        <div class="content-panel d27">
                          <h3>El cliente subira el voucher</h3>
                        </div>
                        <div class="content-panel d28">
                          <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                              <i class="glyphicon glyphicon-file fileinput-exists"></i>
                              <span class="fileinput-filename"></span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                              <span class="fileinput-new">Seleccionar Voucher</span>
                              <span class="fileinput-exists">Cambiar</span>
                              <input type="file" name="cotizar" />
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
                          </div> 
                          <h2 class="text-danger">No olvidar Campo obligatorio *</h2>
                        </div>
                      </div> 
                    </div>
                    <!-- fin iniciooooo -->
                  </div>
                  <!-- FIN DEL COL -->


                </div> 
                <div class="row">

                  <div class="form-group col-md-3">
                    <label for="tiempoContrato" class="sr-only">Tiempo Contrato</label>
                    <input style="border: none;" type="text" placeholder="Tiempo Contrato" id="tiempoContrato" name="tiempoContrato" class="tiempo form-control border-bottom" required autocomplete="off">
                  </div>

                  <div class="form-group col-md-9">
                    <label for="observaciones" class="sr-only">Observaciones</label>
                    <input style="border: none;" type="text" placeholder="Observaciones" id="observaciones" name="observaciones" class="form-control border-bottom"  >
                  </div>

                </div>  
                
                <div class="row">
                  <div class="col-lg-12">
                    <button class="btn btn-success col-lg-5" type="submit" name="cotizarNatural">Crear acceso</button>
                  </div>
                </div>
              </form>
            </ul>
            <!-- COTIZAR PARA EMPRESASS AQUI  -->
            <!-- =============================================================================================== -->
            <ul id="lannisters-panel" class="panel" role="tabpanel" aria-labelledby="lannisters-tab">

              <form action="" method="post" id="empresa" enctype="multipart/form-data">

                <input type="radio" title="Lanisters" name="tipocotizar" id="lannisters" role="tab" aria-controls="lannisters-panel" value="Empresa" checked  style="display: none" />

                <div class="row">
                  <div class="form-group col-lg-3">
                    <label for="ruc" class="sr-only">Ruc *</label>
                    <input style="border: none;" type="number" placeholder="Ruc *" id="ruc" name="ruc" class="form-control  border-bottom" required  autocomplete="off">
                    <span id="resultado1"></span>
                  </div>
                  <div class="form-group col-lg-6">
                    <label for="razonSocial" class="sr-only">Razon Social *</label>
                    <input style="border: none;"  type="text" placeholder="Razon Social *" id="razonSocial" name="razonSocial" class="form-control border-bottom text-capitalize"  required >
                  </div>
                  <div class="form-group col-lg-3">
                    <label for="telefono" class="sr-only">Celular *</label>
                    <input style="border: none;" type="text" placeholder="Celular *" id="telefono" name="telefono" class="form-control border-bottom text-capitalize" required data-mask="999 999 999">
                  </div>                               
                </div>
                <div class="row">

                  <div class="form-group col-md-4">
                    <label for="telefono2" class="sr-only">Telefono *</label>
                    <input style="border: none;" type="text" placeholder="Telefono *" id="telefono2" name="telefono1" class="form-control border-bottom" >
                  </div>

                  <div class="form-group col-md-4">
                    <label for="correo" class="sr-only">Correo *</label>
                    <input style="border: none;" type="email" placeholder="Correo *" id="correo" name="correo" class="form-control x2  border-bottom" required >
                  </div>
                  <div class="form-group col-md-4">
                    <label for="cantidad" class="sr-only">Cantidad Vehiculos*</label>
                    <input style="border: none;" type="number" placeholder="Cantidad Vehiculos*" id="cantidad" name="cantidad" class="form-control border-bottom" >
                  </div>
                </div>
                <div class="row">


                 <div class="form-group col-md-6">
                   <label for="plan" class="sr-only">Plan *</label>
                   <select  style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
                    <?php if (isset($cotizarObj->plan)): ?>
                      <option class="" value="<?php echo $cotizarObj->plan ?>"  selected ><?php echo $cotizarObj->plan ?></option>
                    <?php else: ?>
                    <option class="" value="" disabled selected >Seleccione Plan *</option>
                    <?php endif ?>
                    <?php Cliente::obtenerPlan() ?>      
                  </select>
                </div>
                <div class="form-group col-md-6">
                  <label for="costo" class="sr-only">Costo *</label>
                  <input style="border: none;" type="number" placeholder="Costo *" id="costo" name="costo" class="form-control x2 text-capitalize border-bottom" required >
                </div>
              </div>


              <div class="row">
                <div class="form-group col-md-6">
                  <div class="row">
                    <div class="col-md-4">
                      <select style="border: none;" id="selectNa"  class="cd-selec form-control border-bottom" required name="estadoPago" >
                        <option value="" disabled selected>Seleccione pago *</option>
                        <option value="29">No Pagado</option>
                        <option value="30">Pagó</option>
                        <option value="42">pago en caja o taller</option>
                      </select>   
                    </select>   
                  </div>
                  <div class="col-md-8">
                   <div class="content-pane e1">
                    <h3>Seleccione pago</h3>
                  </div>
                  <div class="content-pane e29">
                    <h3>El cliente subira el voucher</h3>
                  </div>
                  <div class="content-pane e30">
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                      <div class="form-control" data-trigger="fileinput">
                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                        <span class="fileinput-filename"></span>
                      </div>
                      <span class="input-group-addon btn btn-default btn-file">
                        <span class="fileinput-new">Seleccionar Voucher</span>
                        <span class="fileinput-exists">Cambiar</span>
                        <input type="file" name="cotizarEmpresa" />
                      </span>
                      <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
                    </div> 
                    <h2 class="text-danger">No olvidar Campo obligatorio *</h2>
                  </div>
                </div> 
              </div>
            </div>
            <div class="form-group col-md-3">
              <label for="tiempoContrato" class="sr-only">Tiempo Contrato</label>
              <input style="border: none;" type="text" placeholder="Tiempo Contrato" id="tiempoContrato" name="tiempoContrato" class="tiempo Contrato form-control border-bottom" autocomplete="off" required="">
            </div>
            <div class="form-group col-md-3">
              <label for="observaciones" class="sr-only">Observaciones</label>
              <input style="border: none;" type="text" placeholder="Observaciones" id="observaciones" name="observaciones" class="form-control border-bottom" >
            </div>
          </div>
        <!--     <?php if (!empty($errores)) {
                foreach ($errores as $error) {
                    ?>
                    <span class="label label-warning"><?php echo $error ?></span><br>
                    <?php
                }
              } ?> -->
              <div class="row">
                <div class="col-lg-12"><button class="btn btn-success col-lg-5" type="submit" name="cotizarEmpresa">Crear</button></div>
              </div>
            </form>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>







<!-- hasta aqui  -->


<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Últimas Cotizaciones</h5>

        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-wrench"></i>
          </a>
          <ul class="dropdown-menu dropdown-user">
            <li><a href="#">Config option 1</a>
            </li>
            <li><a href="#">Config option 2</a>
            </li>
          </ul>
          <a class="close-link">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content">
        <input type="text" class="form-control input-sm m-b-xs" id="filter1"
        placeholder="Fitrar Cotizaciones">

        <table class="footable table table-stripped" data-page-size="8" data-filter=#filter1>
          <thead>
            <tr>
              <tr>
                <th>#</th>
                <th>DNI o RUC</th>
                <th>Nombres(s)</th>
                <th>Estado Pago(s)</th>
                <th>Estados(s)</th>
                <th data-hide="phone,tablet">Fecha creación</th>
                <th>Acción</th> 
                <!-- <th data-hide="phone,tablet">Task</th> -->
              </tr>
            </tr>
          </thead>
          <tbody>
            <?php $cotizarObj=Cotizar::obtenerCotizacion() ?>
            <?php $n=1 ?>
            <?php if ($cotizarObj): ?>

              <?php foreach ($cotizarObj as $var): ?>
                <tr>

                 <td><?php echo $n++ ?></td>
                 <td><?php echo $retVal = ($var->tipoCliente=="Natural") ? $var->dni : $var->ruc ; ?></td>
                 <td><?php echo $retVal1 = ($var->tipoCliente=="Natural") ? $var->nombre." ".$var->apellidos : $var->razonSocial ; ?></td>
                 <td><?php echo obtenerEstado($var->estadoPago)  ?></td>
                 <td><?php echo obtenerEstado($var->estado)  ?></td>
                 <td><?php echo haceMinimo($var->fecha)  ?></td>
                 <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#cotizacion" data-id="<?php echo $var->cotizacionid ?>">Elegir <i class="fa fa-cog"> </i></button></td>
              <?php endforeach ?>
            </tr>
          <?php endif ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="5">
              <ul class="pagination pull-right"></ul>
            </td>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
</div>


<!-- el modal para asignarles nuevamente el privilegio de ingresar a la web -->

<div class="modal inmodal" id="cotizacion" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated bounceIn">
      <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

        <h4 class="modal-title">Gestionar</h4>
        <small class="font-bold">Instalación, Eliminar<div class="form-group ">
        </div></small>
      </div>
      <form  role="form" method="post" id="corizarForm">

        <div class="modal-body" style="background: #fff;padding-top: 0">
        </div>

        <div class="modal-footer" >
          <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
          <button type="submit" name="gestinarCotizacion" class="btn btn-primary">Guardar Cambios</button>
        </div>
      </form>   
    </div>
  </div>
</div>
<!-- fin del modal para asignarles nuevamente el privilegio de ingresar a la web -->
<!-- <?php echo $session->idusuario ?> -->
<!-- correos aqui -->
<?php 
if (isset($_POST['enviarCorreo']) && isset($_POST['correo'])): 
  $cotizarObjNew=new Cotizar(); 
  $cotizarObjNew->correo=$_POST['correo'];  
  $cotizacionid=htmlspecialchars($_POST['cotizacionidEditar']);
  if ($cotizarObjNew->actualizarCorreoCliente($cotizacionid))
    echo "";
  else
    echo "Error en actualizar Correo";

endif

 ?>

<?php if (isset($_POST['cotizarNatural']) || isset($_POST['cotizarEmpresa']) || isset($_POST['enviarCorreo'])): ?>
  

<?php 
$usuario=Usuario::obtenerUsuario($session->idusuario);

$clienteDatos="";
if (isset($_POST['nombre'])) {
  $clienteDatos= $_POST['nombre']." ".$_POST['apellidos'];
  $clienteDatos=ucwords(strtolower($clienteDatos));
  
}else{  
  $clienteDatos=$_POST['razonSocial'];
  $clienteDatos=ucwords(strtolower($clienteDatos));
}
$correo21321="";
$correoEnviador=$usuario->usuario;
$nombreEnviador=$usuario->usuarioNombre;
if (isset($cotizarObj->correo)) {
  $correo21321=$_POST['correo'];
}


// $asunto1 =$nombreEnviador." de Tracklink, Registrate aqui ";

// $para1=$correo21321.",wilmanpabel@gmail.com,pvasquez@tracklink.pe";
$contenidoHtml="<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
    <meta name='viewport' content='width=device-width' />
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
    <title>Actionable emails e.g. reset password</title>
    <!-- <link href='styles.css' media='all' rel='stylesheet' type='text/css' /> -->
</head>

<body style=' margin: 0;padding: 0;font-family:Helvetica, Arial, sans-serif;    box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none;    width: 100% !important; height: 100%; line-height: 1.6; background-color: #f6f6f6;'>

<table  style='background-color: #f6f6f6; width: 100%;'>
    <tr>
        <td style='vertical-align: top;'></td>
        <td style=' /*Container y centra*/display: block !important; max-width: 600px !important;  margin: 0 auto !important; clear: both !important;' width='600'>
            <div style='max-width: 600px;margin: 0 auto;display: block; padding: 20px;'>
                <table style=' background: #fff;    border: 1px solid #e9e9e9; border-radius: 3px;' width='100%' cellpadding='0' cellspacing='0'>
                    <tr>
                        <td style='vertical-align: top;' >
                            <table  cellpadding='0' cellspacing='0'>
                                <tr>
                                    <td style='vertical-align: top; padding: 10 20px 20px;'>
                                        <a href='http://tracklink.pe' target='_blank'><img style='max-width: 100%;/*Container y centra*/display: block !important; max-width: 600px !important;  margin: 0 auto !important; clear: both !important;padding-top: 20px;' class='img-responsive' src='cid:tracklink'/></a>
                                </tr>
                             <!--    <tr>
                                    <td style='vertical-align: top; padding: 10px 20px 20px; text-align: center;' >
                                        <h3>TRACKLINK</h3>
                                    </td>
                                </tr> -->
                                <tr>
                                    <td style='vertical-align: top; padding: 10px 20px 20px;' >
                                         Estimado/a ".$clienteDatos."<br><br> Estamos en espera de los datos requeridos para la instalación del dispositivo GPS en su vehículo.
                                    </td>
                                </tr>
                                <tr>
                                    <td style='vertical-align: top; padding: 10px 20px 20px;' >
                                         Para ello presione el boton EMPIEZA EL REGISTRO. luego ingresa el número de tu DNI o RUC segun corresponda.
                                    </td>
                                </tr>
                                <tr>
                                    <td style='vertical-align: top; padding: 10px 20px 20px; text-align: center;'  >
                                        <a href='http://registroclientes.tracklink.pe' style='color: #0069b3;text-decoration: underline; text-decoration: none; color: #FFF;background-color: #0069b3;    border: solid #0069b3;    border-width: 5px 10px;    line-height: 2;   font-weight: bold;    text-align: center;    cursor: pointer;    display: inline-block;    border-radius: 5px;    text-transform: capitalize;'>EMPIEZA EL REGISTRO </a>
                                    </td>
                                </tr>
                              </table>
                        </td>
                    </tr>
                </table>
                <div style='width: 100%; clear: both;color: #999;padding: 20px;'>
                    <table width='100%'>
                        <tr>
                            <td style='font-size: 12px; text-align: center; padding: 0 0 20px;' >Seguir  <a style='color: #0069b3;text-decoration: underline;' target='_blank' href='https://twitter.com/TracklinkPE'>@TracklinkPE</a> en Twitter.</td>
                            <td style='font-size: 12px; text-align: center; padding: 0 0 20px;' >Like en <a style='color: #0069b3;text-decoration: underline;' target='_blank' href='https://www.facebook.com/tracklinkperu/'>Facebook</a></td> 
                            <td style='font-size: 12px; text-align: center; padding: 0 0 20px;' >siguenos en  <a style='color: #0069b3;text-decoration: underline;' target='_blank' href='https://www.youtube.com/channel/UCylRCGZ4LBaNKk-XMJliURw'>Youtube</a></td>
                        </tr>
                    </table>
                </div></div>
        </td>
        <td style='vertical-align: top; padding: 0 0 20px;'></td>
    </tr>
</table>

</body>
</html>";
///Aqui se envia el correo ///
///Aqui se envia el correo ///
///Aqui se envia el correo ///

require_once '../mailphp/PHPMailer/PHPMailerAutoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
$mail->SetLanguage( 'es', 'mailphp/PHPMailer/language/' );
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = "mail.tracklink.pe";
//Set the SMTP port number - likely to be 25, 465 or 587
$mail->Port = 25;
//Whether to use SMTP authentication
$mail->SMTPAuth = false;
///the utf here ---
$mail->CharSet = 'UTF-8';
//Set who the message is to be sent from
$mail->setFrom('no-responder@tracklink.pe', $nombreEnviador.' De Tracklink');  // Desde donde se envia 
//Set an alternative reply-to address
$mail->addReplyTo($correoEnviador, $nombreEnviador.' De Tracklink'); // donde deven responder
//Set who the message is to be sent to
$mail->addAddress($_POST['correo'], $clienteDatos); // aquien se manda el mensaje
//CC and BCC
$mail->addCC("pvasquez@tracklink.pe");
$mail->addBCC("pruebas-ti@tracklink.pe");
//Set the subject line
$mail->Subject = 'Estimado: '.$clienteDatos.' Registrate aquí'; //// ASUNTO //////
//Read an HTML message body from an external file, convert referenced images to embedded,
$mail->AddEmbeddedImage('../assets/img/tracklink.png', 'tracklink');
//convert HTML into a basic plain-text alternative body
$mail->msgHTML($contenidoHtml);
//Replace the plain text body with one created manually
$mail->AltBody = 'Estimado '.$clienteDatos.' agradecemos su preferencia'; ////////ESTE SE EL CONTENIDO//// QUE SE BERA EN PRIERA INSTANACIA
//Attach an image file
  //aqui se adjunta los archivos que queremos enviar :D

// $mail->addAttachment('images/phpmailer_mini.png');

// //send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    // echo "Correo Enviado";
}

// /////Aqui se envia el correo ///
// /////Aqui se envia el correo ///
// /////Aqui se envia el correo ///  



 ?>
<?php endif ?>

</div>
<?php include("../includes/footer.php") ?>

</div>
</div>

<!-- Mainly scripts -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<!-- Steps -->
<script src="../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="../assets/js/cotizar.js"></script>
<script src="../assets/js/cotizarEmpresa.js"></script>
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<?php include_once('../includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true}) 
  <?php endforeach ?> 
<?php endif ?>
<?php if (!empty($echo)): ?>
  toastr.success('<?php echo $echo ?>', 'Exito', {timeOut: 5000,progressBar:true,closeButton:true})
<?php endif ?>  
<?php if (isset($_POST['enviarCorreo'])): ?>
  toastr.success('Correo re enviado', 'Exito', {timeOut: 5000,progressBar:true,closeButton:true})
<?php endif ?>

</script>
  
<script>
  $(document).ready(function(){
    $("#dniE").keyup(function()
    {   
      var dniE = $(this).val();
      if(dniE.length == 8)
      {   
        $("#resultado").html('Verficando...');

        
      $.ajax({

        type : 'POST',
        url  : 'ajax/dni-check',
        data : $(this).serialize(),
        success : function(data)
        {
         $("#resultado").html(data);
       }
     });
      return false;
      
    }
    else
    {
      $("#resultado").html('');
    }

  });
    $("#ruc").keyup(function()
    {   
      var ruc = $(this).val();  

      if(ruc.length ==11)
      {   
        $("#resultado1").html('Verficando...');
        $.ajax({

          type : 'POST',
          url  : 'ajax/ruc-check',
          data : $(this).serialize(),
          success : function(data)
          {
           $("#resultado1").html(data);
         }
       });
        return false;
        
      }
      else
      {
        $("#resultado1").html('');
      }
    });
    $('.footable').footable();
    $('.tiempo').typeahead({
      source: [
      {"name": "1 año"},
      {"name": "2 años"},
      {"name": "3 años"},
      {"name": "4 años"},
      {"name": "5 años"},
      {"name": "6 años"},
      {"name": "8 años"},
      {"name": "7 años"}
      ]
    }); 
    $('#cotizacion').on('show.bs.modal', function (e) {
      var rowid = $(e.relatedTarget).data('id');
      $.ajax({
        type : 'post',
            url : 'modal/cotizarDataModal', //Here you will fetch records 
            data :  'rowid='+ rowid, //Pass $id
            success : function(data){
            $('.modal-body').html(data);//Show fetched data from database
          }
        });
    });

// validacion de cantidad de values en ruc y dni , etc ,etc
  
         $("#personaNatural").validate({
          rules: {
            dni:{
              required:true,
              minlength:8,
              maxlength:8,
            },
            correo:{
              required:true
            }
          },
          messages: {
            dni:{
              required: "Es necesario ingresar Dni",
              minlength: jQuery.validator.format("Se nececita {0} caracteres!"),
              maxlength: jQuery.validator.format("Se nececita {0} caracteres!")
            },
            correo:{
              required: "Ingrese correo",
              email: "Ingrese un correo valido.",
            }
          }
         });
         $("#empresa").validate({
          rules: {
            ruc:{
              required:true,
              minlength:11,
              maxlength:11,
            },
            correo:{
              required:true
            }
          },
          messages: {
            ruc:{
              required: "Es necesario ingresar Dni",
              minlength: jQuery.validator.format("Se nececita {0} caracteres!"),
              maxlength: jQuery.validator.format("Se nececita {0} caracteres!")
            },
            correo:{
              required: "Ingrese correo",
              email: "Ingrese un correo valido.",
            }
          }
         });







  });
  </script>
</body>
</html>
<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>

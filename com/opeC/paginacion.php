  <?php require_once("../../includes/init.php") ?>
  
  <?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $chObj=new Chequeos();
  
  $cantidadO=$chObj->obtenerRegistrosSubidosCantida();
  $registrosPorPagina=7;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  // echo $cantidadO->cantidad; 
  $n=1;
  $datos=$chObj->obtenerRegistrosSubidos($posisionDePagina,$registrosPorPagina);

             ?>    

             <?php if ($datos): ?>
              
<?php foreach ($datos as $var): ?>    
    <tr class="animated bounceIn">
        <td><?= $n++ ?></td>
        <td><?= $var->nombres ?></td>
        <td><?= $var->placa ?></td>
        <td><?= haceMinimo($var->fechaRegistro) ?></td>
        <td><?= formatoHaceEn($var->fechaHoraChequeoLinea) ?></td>
        <td><?= obtenerEstado($var->estado) ?></td>
        <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#chVerModal" data-id="<?php echo $var->idchequeo ?>">Ver <i class="fa fa-eye"></i></button>&nbsp;</td>
    </tr>
<?php endforeach ?>
 
              <tr>
                    <td colspan="8">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
                    </td>
                </tr>
            
 <?php endif ?>




<?php } ?>
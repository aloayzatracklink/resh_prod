<?php  
if (isset($_POST['chasis'])) {


    include_once("../../includes/init.php");

    $lista = Operacion::obtenerFrecuentes($session->idusuario);
       //print_r($a);
    $chasis=limpiar(trim($_POST['chasis']));		
    $clienteObj=Onix::ObtenerDatosDesdeChasis($chasis);
    $obMenu=Usuario::obtenerMenu($session->idusuario);
    if ($clienteObj) {
        $cantidadObjeto=Onix::obtenerCantididadVehiculos($clienteObj->idcustomer,$clienteObj->idlocation);
        $planobj=Onix::obtenerPlanDeServicio($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation);
    }
    $filTOP="";$filTopCant="";

  $clienteObjModal=new Cliente;
?>
<?php $vhOC=Vehiculo::verificarSiEstaDuplicado($chasis) ?>
<?php if($vhOC->cantidad>0){ ?>
        <div class="showReg">
<label class="text-danger text-center">EL CHASIS YA SE ENCUENTRA REGISTRADO </label> <hr>
<table class="table table-hover">
  <thead>
    <tr>
      <th>#</th>
       <th>Tipo Servicio</th>
      <th>Cliente</th>
      <th>Placa-Chasis</th>
      <th>Facturación</th>
      <th>Vehículo</th>
      <th>Fecha - usuario creador</th>
    </tr>
  </thead>
  <tbody>
<?php $DatosVhD=Vehiculo::obtenerVehiculosConElMismoChasisOP($chasis) ?>
<?php $n=1;$nI=0;$nR=0;$nt1=0;$nt2=0;$conct='';$conctt='' ?>
<?php foreach ($DatosVhD as $var): ?>
<?php $datodCl=Cliente::obtenerDatosDeclientePorId($var->idcliente);
$datosFac=DatosFacturacion::obtenerDatosFacturacionPorIdVehiculo($var->idvehiculo);

$datosOpera=Operacion::obtenerOperacionesPorVehiculoIDClienteId($var->idvehiculo,$var->idcliente);

if($n>1){
    $conct=",";
}
if($datosOpera->trabajo=='Instalacion')
    $nI++;
if($datosOpera->trabajo=='Reactivacion')
    $nR++;

$filTOP=$filTOP.$conct.'"'.$datosOpera->trabajo.'"';
$o=Usuario::obtenerUsuario($var->idcreador);
?>
    <tr>
      <th scope="row"><?= $n++ ?></th>
        <td><span class="label label-primary"><?= $datosOpera->trabajo ?></span></td>
      <td><?= ($datodCl->tipoCliente=="Natural")? $datodCl->apellidosC." ".$datodCl->nombre:$datodCl->razonSocial ?></td>
      <td><?= $var->placa." ".$var->chasis ?></td>
      <td><?= obtenerEstado($datosFac->estadoFacturacion) ?>  </td>
      <td><?= obtenerEstado($var->estado) ?></td>
      <td><?= formato($var->fechaCreacion)." - ".$o->usuarioNombre." ".$o->usuarioApellidoPaterno ?></td>  
    </tr>
<?php endforeach ?>
  </tbody>
</table>
<?php
    $cantReg=CantidadRegistroOP::obtenerCantidadOperacionPorTipoServicio($chasis,'');
    IF($cantReg){
        foreach ($cantReg as $var):
            if($var->tipo=='Instalacion' and $nI<(($var->cantreg))){
                if($nt2>0){
                    $conctt=",";
                }
                $filTopCant=$filTopCant.$conctt.'"'.$var->tipo.'"';
                $nt1++;
            }
            if($var->tipo=='Reactivacion' and $nR<(($var->cantreg))){
                if($nt1>0){
                    $conctt=",";
                }
                $filTopCant=$filTopCant.$conctt.'"'.$var->tipo.'"';
                $nt2++;
            }

        endforeach;
    }

?>
<?php if(!is_null(Cliente::obtenerTpServicio($filTOP,$filTopCant))){ ?>
        <label class="text-success text-left" style="display: inline-block">Si desea realizarle una  <?php echo Cliente::obtenerTpServicio($filTOP,$filTopCant) ?> haga click en  </label>
        <input type="hidden" name="token" value="<?php echo Token::generar() ?>" >
            <div class="btn btn-warning" style="font-size: 12px" onclick="seguir()">Seguir</div>

    <?php } ?>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
<?php //die() ?>
<?php } ?>
<link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<link href="../assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<link href="../assets/css/animate.css" rel="stylesheet">
    <style>
        .ibox-content > .sk-spinner {
            display: none;
        }

        .ibox-content.sk-loading {
            position: relative;
        }
        .ibox-content.sk-loading:after {
            content: '';
            background-color: rgba(255, 255, 255, 0.7);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        .ibox-content.sk-loading > .sk-spinner {
            display: block;
            position: absolute;
            top: 40%;
            left: 0;
            right: 0;
            z-index: 2000;
        }
    </style>
<link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>
    <div class="showOPERA">
<div class="well" style="padding-top: 11px;padding-bottom: 10px; ">
    <div class="row">
    <div class="row">
    <div>
    <h2 class="text-center"><?php
    if($clienteObj){
        // echo vence($clienteObj->hasta);
    }else{
        echo "CHASIS $chasis No esta en la base de datos consulta este mensaje con pvasquez@tracklink.pe";
    }
    ?></h2>
        <?php if ($clienteObj): ?>
        <?php if ($cantidadObjeto): ?>
            <?php if ($cantidadObjeto->cantidad>1): ?>
            <h4 class="text-center text-warning">ATENCION ! Cliente tiene <?php echo $cantidadObjeto->cantidad ?> Vehiculos en Tracklink</h4>
            <?php endif ?>
        <?php endif ?>
        <input type="hidden" name="idcustomer" value="<?= $clienteObj->idcustomer ?>">
        <input type="hidden" name="idvehicle" value="<?= $clienteObj->idvehicle ?>">
        <input type="hidden" name="idlocation" value="<?= $clienteObj->idlocation ?>">
                <div class="form-group col-md-3">
                    <label for="Observaciones" >Dni O Ruc </label>
                    <input name="dniORuc" style="border: none;" type="text" placeholder="Web" id="Observaciones" <?php echo $retVal = (!empty($clienteObj->dni)) ? "readonly" : "" ; ?> class="border-bottom form-control dnioruconix"  value="<?php echo $retVal = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>">
                </div>
                <div class="form-group col-md-5">
                <?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
        <input type="hidden" name="nombres" value="<?= $clienteObj->nombre ?>">
        <input type="hidden" name="apellidos" value="<?= $apellidos ?>">
        <input type="hidden" name="anioF" value="<?= $clienteObj->anio ?>">
                    <label for="numeroTecnico" >Nombre de cliente*</label>
                    <input  style="border: none;" type="text" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->nombre)) ? "$apellidos"." "."$clienteObj->nombre" : "$apellidos" ; ?> ">
                </div>
                <div class="col-md-2"><label for="estado" >Celular</label>
                    <input style="border: none;" type="text" readonly name="celular" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>" >
                </div>
                <div class="col-md-2"><label for="estado" >Celular</label>
                    <input style="border: none;" type="text" readonly name="telefono5" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular2)) ? "$clienteObj->celular2" : "" ; ?>">
                </div>
            </div>
            <input type="hidden" value="<?php echo $retVal = (!empty($clienteObj->telefonoTrabajo)) ? "$clienteObj->telefonoTrabajo" : "" ; ?>" name="telefono">

            <input type="hidden" value="<?php echo $retVal = (!empty($clienteObj->telefono)) ? "$clienteObj->telefono" : "" ; ?>" name="telefono2">

            <input type="hidden" name="correoCliente" value="<?php echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo," : "$clienteObj->correo" ;echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo2" : "" ;  ?>">
	
        </div>

	</div>
</div>
<hr>
<div class="row">
	<div class="form-group col-md-3">
		<label for="numeroTecnico" >Placa Vehiculo*</label>
		<input  style="border: none;" readonly type="text" placeholder="Placa" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->placa)) ? "$clienteObj->placa" : "" ; ?>"  >
	</div>
	<div class="form-group col-md-3">
		<label for="Observaciones">Marca *</label>
		<input required <?php echo $retVal = (!empty($clienteObj->marca)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->marca)) ? "$clienteObj->marca" : "" ; ?>" >
	</div>
	<div class="form-group col-md-3">
		<label for="Observaciones">Modelo*</label>
		<input required <?php echo $retVal = (!empty($clienteObj->modelo)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($clienteObj->modelo)) ? "$clienteObj->modelo" : "" ; ?>">
	</div>
	<div class="form-group col-md-3">
		<label for="Observaciones" >Color</label>
		<input <?php echo $retVal = (!empty($clienteObj->color)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Color" id="Observaciones" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->color)) ? "$clienteObj->color" : "" ; ?>">
	</div>
	
</div>
<div class="row">
	<div class="form-group col-md-6">
		<label for="Observaciones" >Chasis*</label>
		<input required <?php echo $retVal = (!empty($clienteObj->chasis)) ? "readonly" : "" ; ?>  style="border: none;" type="text" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->chasis)) ? "$clienteObj->chasis" : "$chasis" ; ?>">
	</div>
	<input type="hidden" name="motor" value="<?php echo $retVal = (!empty($clienteObj->motor)) ? "$clienteObj->motor" : "Actualizar" ; ?>">	
	<div class="form-group col-md-6">
		<label for="seguro" class="">Seleccione Compañía de Seguros</label>
            <select  style="border: none;" class="border-bottom form-control text-capitalize" required name="seguro" id="seguro">
                 <option class='text-capitalize' value='' disabled selected >Seleccione Compañía de Seguros</option>
                <?php Cliente::obtenerAseguradora('') ?>
                <!--<option class='text-capitalize' value='Por confirmar' >Por confirmar</option>
               <option class='text-capitalize' value='No tiene' >No tiene</option>
               <option class='text-capitalize' value='La Positiva' >La Positiva</option>
               <option class='text-capitalize' value='Pacifico Seguros' >Pacifico Seguros</option>
                <option class='text-capitalize' value='Rimac seguros' >Rimac seguros</option>
               <option class='text-capitalize' value='Mapfre Peru' >Mapfre Peru</option>
               <option class='text-capitalize' value='MHDI Seguros' >HDI Seguros</option>
               <option class='text-capitalize' value='Qualitas Seguros' >Qualitas Seguros</option> -->

             </select>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		 <label for="plan" class="">Plan *</label>
         <select  style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
          <?php if (isset($cotizarObj->plan)): ?>
            <option class="" value="<?php echo $cotizarObj->plan ?>"  selected ><?php echo $cotizarObj->plan ?></option>
          <?php endif ?>
          <option class="" value="" disabled selected >Seleccione Plan *</option>
          <?php Cliente::obtenerPlan() ?>      
        </select> 
	</div>
    <div class="form-group col-md-6 ">
        <p style="font-weight:bold">Servicios Adicionales *: <label class="canthoraprog2 label-success"> 0 Horas </label></p>
        <div class="servAdi">
        <?php ServiciosAdicionales::obtenerServAdicional('') ?>
        </div>
        <input type="hidden" id="canthoraprog" name="canthoraprog" value="0">
    </div>
</div><br>
<?php include("../../includes/campanas2.php") ?>
        <br>
<div class="row">

    <div class="form-group col-md-4">
        <label for="corigen" class="">Concesionario Origen</label>
        <input style="border: none;" type="text" placeholder="Concesionario Origen" id="corigen" name="corigen" class="border-bottom form-control text-uppercase corigen " autocomplete="off" required="">
    </div>
	<div class="form-group col-md-4">
		  <label for="idEjecutvio" class="">Seleccione Ejecutivo</label>
<?php
if ($obMenu->administrarComercial=="si") {
    ?>
<select  style="border: none;" class="border-bottom form-control text-capitalize" required name="idEjecutvio" id="idEjecutvio">
        <option class='text-capitalize' value='' disabled selected >Seleccione Ejecutivo</option>                
    <?php if ($datosUsu=Usuario::obtenerEjecutivos()): ?>
        <?php foreach ($datosUsu as $var): ?>
            <option class='text-capitalize' value='<?= $var->idusuario ?>' > <?= $var->usuarioNombre." ".$var->usuarioApellidoPaterno;  ?></option>
        <?php endforeach ?>
    <?php endif ?>                
</select>
    <?php
}else{
    ?>
<select  style="border: none;" class="border-bottom form-control text-capitalize" required name="idEjecutvio" id="idEjecutvio">
<option class='text-capitalize' value='' disabled selected >Seleccione Ejecutivo</option>                
<?php if ($datosUsu=Usuario::obtenerEjecutivosAsignados($session->idusuario)): ?>
    <option class="text-capitalize" value="153"> Motorlink </option>
    <?php foreach ($datosUsu as $var): ?>
    <option class='text-capitalize' value='<?= $var->idusuario ?>' > <?= $var->usuarioNombre." ".$var->usuarioApellidoPaterno;  ?></option>
    <?php endforeach ?>
<?php endif ?>                
</select>
    <?php
}
    ?>
	</div>

    <div class="col-md-4">
        <label for="plan" class="">Tipo de Sevicio *</label>
        <select  style="border: none;" class="border-bottom form-control" required name="tipo" id="tipo">

            <?php Cliente::obtenerTipoServicio($filTOP,$filTopCant) ?>
        </select>
    </div>
</div>

<div class="row">
<div class="form-group col-md-3">
<label for="tipoCanal" class="">Seleccione canal *</label>
    <select style="border: none;"  name="tipoCanal" class="cd-selec form-control border-bottom " required id="tipoCanal">
      <option value="" disabled selected>Seleccione canal *</option>
      <option value="BROKER">Broker</option>
      <option value="CONCESIONARIO">Concesionario</option>
      <option value="CORPORATIVO">Corporativo</option>      
      <option value="ASEGURADORA">Aseguradora</option>
      <option value="CONVENIO">Convenio</option>
      <option value="BANCO">Banco</option>
      <option value="TRACKLINK OCTO">Tracklink octo</option>
      <option value="DIGITAL">DIGITAL</option>
    </select>   
  </div>
  <div class="form-group col-md-2 a">
    <label for="nombreCanal" class="">Canal nombre</label>
    <input style="border: none;" type="text" placeholder="Canal nombre" id="nombreCanal" name="nombreCanal" class="border-bottom form-control text-uppercase nombreCanal " autocomplete="off" required="" >
  </div>
<div class="tiendaAparece1">
   <div class="form-group col-md-2 ">
    <label for="tienda" class="">Tienda</label>
    <input style="border: none;" type="text" placeholder="Tienda" id="tienda" name="tienda" class="border-bottom form-control text-uppercase tienda " autocomplete="off" required="" >
  </div>    
  </div>
  <div class="b">    
  <div class="form-group col-md-2 ">
    <label for="convenio" class="">Seleccione Convenio *</label>
     <select style="border: none;"  name="convenio" class="cd-selec form-control border-bottom" required id="convenio">
     <?php $ConD=Canales::obtenerConvenios() ?>
     <option value="" disabled selected>Seleccione Convenio *</option>
     <?php foreach ($ConD as $var): ?>
        <option value="<?= $var->idconvenio  ?>"><?= $var->nombreConvenio ?></option>
     <?php endforeach ?>
    </select> 
  </div>
  <div class="tiendaAparece">
   <div class="form-group col-md-2 ">
    <label for="tienda" class="">Tienda</label>
    <input style="border: none;" type="text" placeholder="Tienda" id="tienda" name="tienda1" class="border-bottom form-control text-uppercase tienda " autocomplete="off" required="" >
  </div>    
  </div>
  </div>

  <div class="form-group col-md-3">
    <label for="ejecutivoCanalN" class="">Ejecutivo canal</label>
    <input style="border: none;" type="text" placeholder="Ejecutivo canal" id="ejecutivoCanalN" name="ejecutivoCanalN" class="border-bottom form-control text-uppercase ejecutivoCanalN" autocomplete="off" required="" >
  </div>
  <div class="col-md-2">
    <label for="comision">Cobra Comision</label>
    <div class="switch">
      <div class="onoffswitch">
        <input type="checkbox" class="onoffswitch-checkbox" id="comision" value="si" name="comision" >
        <label class="onoffswitch-label" for="comision">
          <span class="onoffswitch-inner"></span>
          <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </div>
  </div>
  </div>	


<div class="row">
<h3 class="col-md-6">Datos de facturación </h3>
</div>
<div class="row">
	<div class="col-md-6">
		<label for="Forma*" >Forma</label>
		<select style="border: none;" id="formaPago" name="formaPago" class="form-control border-bottom" required >
		<option value="" disabled selected>Datos de pago*</option>
		<option value="Deposito">Deposito</option>
		<option value="OrdenDeCompra">Órden de compra</option>
		<option value="pacificoPandero">Pacífico Pandero</option>
		<option value="pacificoBanbif">Pacífico BanBif</option>
		<option value="concesionarioPandero">Concesionario Pandero</option>
		<option value="Banbif">BanBif</option>
		<option value="LeyendaAPositiva">Leyenda A La Positiva</option>
		<option value="PagoEntaler" > Págo en taller</option>
		<option value="telefonica">Telefonica</option>
		<option value="AmericaBrokers">American Brokers</option>
		<option value="CEDEA">CEDEA</option>
		<option value="Moviliza">Moviliza</option>
            <option value="BamcoSantander">Banco Santander</option>
	</select>
	</div>
	<div class="col-md-6 voucherImg">
		<label for="Forma*" >Voucher</label>
			<div class="fileinput fileinput-new input-group" data-provides="fileinput">
				<div class="form-control" data-trigger="fileinput">
					<i class="glyphicon glyphicon-file fileinput-exists"></i>
					<span class="fileinput-filename"></span>
				</div>
				<span class="input-group-addon btn btn-default btn-file">
					<span class="fileinput-new">Seleccionar Voucher de pago*</span>
					<span class="fileinput-exists">Cambiar</span>
					<input type="file" name="voucher"  id="voucherImg" required="" />
				</span>
				<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
			</div> 
		</div>
</div>
<br>
<div class="row">
<div class="col-md-4">
	<select style="border: none;" id="selectNa" name="tipoDocumentoFacturacion" class="cd-selec form-control border-bottom" required >
		<option value="" disabled selected>Datos de facturación*</option>
		<option value="Boleta">Boleta</option>
		<option value="Factura">Factura</option>
	</select>   
</div>
<div class="col-md-8">
	<div class="content-pane e1">
		<h3>Seleccione pago</h3>
	</div>
	<div class="content-pane eFactura">
		<div class="row"><br>
			<div class="form-group col-md-4">
				<label for="rucFacturacion" class="">Ruc *</label>
				<input style="border: none;" type="number" placeholder="Ruc *" id="rucFacturacion" name="rucFacturacion" class="form-control x2 border-bottom" maxlength="12" minlength="10" value="<?= $retVal = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>"  >
			</div>
			<div class="form-group col-md-8">
				<label for="razonSocialFacturacion" class="">Razón Social *</label>
				<input style="border: none;" type="text" placeholder="Razón Social *" id="razonSocialFacturacion" name="razonSocialFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ; ?>" >
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-8">
				<label for="direccionFiscalFacturacion" class="sr-only">Dirección Fiscal *</label>
				<input style="border: none;" type="text" placeholder="Dirección Fiscal *" id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="form-control border-bottom text-capitalize" value="<?= $clienteObj->direccion ?>">

			</div>
			<div class="form-group col-md-4">
				<label for="telefonoFacturacion" class="sr-only">Teléfono</label>
				<input style="border: none;" type="text" placeholder="Teléfono" id="telefonoFacturacion" name="telefonoFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">

			</div>
		</div>      
	</div> 
</div>  
</div>
<br>	
<!-- 
<div class="row">
<div class="col-md-12">
<select style="border: none;" id="entrega" name="entregaEnInstalacion" class="cd-selec form-control border-bottom text-center" required >
<option value="" disabled selected>¿Enviar Factura o boleta?</option>
<option value="noE">No enviar</option>
<option value="si">Enviar a domicilio</option>
<option value="no">Entregar cuando se realiza la instalación.</option>
</select>
</div>  
</div>    
-->
<div class="esconderNo">
<div class="row">
        <div class="form-group col-md-6">
          <label for="direccionEntrega" class="">Dirección de engrega</label>
          <input style="border: none;" type="text" placeholder="Dirección de engrega" id="direccionEntrega" name="direccionEntrega" class="form-control border-bottom text-capitalize" value="">
        </div>
        <div class="form-group col-md-6">
          <label for="referenciaEntrega" class="">Referencia de engrega</label>
          <input style="border: none;" type="text" placeholder="Referencia de engrega" id="referenciaEntrega" name="referenciaEntrega" class="form-control border-bottom text-capitalize" required="" value="">
        </div>
      </div>
<div class="row">
        <div class="form-group col-md-4">
          <label for="distritoEntrega" class="">Distrito de entrega</label>
          <input style="border: none;" type="text" placeholder="Distrito de entrega" id="distritoEntrega" name="distritoEntrega" class="form-control border-bottom text-capitalize" required autocomplete="off" value="">
        </div>
        <div class="form-group col-md-4">
          <label for="horarioEntrega" class="">Horario de entrega</label>
          <input style="border: none;" type="text" placeholder="Horario de entrega" id="horarioEntrega" name="horarioEntrega" class="form-control border-bottom text-capitalize" autocomplete="off" required value="">
        </div>
        <div class="form-group col-md-4">
          <label for="personaEntrega" class="">Persona de recepción de factura </label>
          <input style="border: none;" type="text" placeholder="Persona de recepción de factura" id="personaEntrega" name="personaEntrega" class="form-control border-bottom text-capitalize" required value="">
        </div>
      </div>
</div>
<br>
 <br>
<div class="showTipo">
<div class="row">
	<h3 class="col-md-6">Datos de instalación</h3>
	<div class="col-md-6">
      <label for="pendienteInstacion">Pendiente datos de instalación</label>
      <div class="switch">
        <div class="onoffswitch">
          <input type="checkbox"  class="onoffswitch-checkbox" id="pendienteInstacion" value="si" name="pendienteInstacion">
          <label class="onoffswitch-label" for="pendienteInstacion">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
          </label>
        </div>
      </div>
    </div>	
</div>

<div role="tablist">
  <input type="radio" title="taller" name="lugarInstacion" class='lugarDeInstalacion' id="taller" role="tab" aria-controls="taller-panel" checked value="Taller" class="lugarDeInstalacion tall" />
  <input type="radio" title="domicilio" name="lugarInstacion"  class='lugarDeInstalacion' id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente" class="lugarDeInstalacion domm" />
  <input type="radio" title="concesionario" name="lugarInstacion" class='lugarDeInstalacion'  id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario" class="lugarDeInstalacion conn" >

  <div class="tabs" aria-hidden="true">
    <label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
    <label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
    <label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
  </div>
  <div class="panels">
    <ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">


              <h2><?= $lista->lugarInstalacion ?> | <?= $lista->observacion ?></h2>
              <p><?= $lista->direccionInstacion  ?></p>
              <p>
                <?php $dep=$clienteObjModal->obtenerDepartamentoPorId($lista->departamentoi) ?><?= $dep->departamento ?>
        &nbsp;|&nbsp;
                <?php $prov=$clienteObjModal->otenerProvinciaPorId($lista->provinciai) ?><?php echo $prov->provincia ?>
        &nbsp;|&nbsp;
                <?php $di=$clienteObjModal->obtenerDistritoPorId($lista->distritoi) ?><?= $di->distrito ?>
              </p>

      <input type="hidden" name="direccionTaller" value="<?= $lista->direccionInstacion  ?>">
      <input type="hidden" name="disT" id="distritoTallerid" value="<?= $lista->distritoi  ?>">
      <input type="hidden" name="disT" value="<?= $lista->distritoi  ?>">
      <input type="hidden" name="proT" value="<?= $lista->provinciai  ?>">
      <input type="hidden" name="depT" value="<?= $lista->departamentoi  ?>">

      <div class="row">
      <div class="row">

    <div class="row" >
        <div class="form-group col-md-4">   
                      <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control tal tala fecha"  id="fechaIT" name="fechaIT" autocomplete="off" required>
          </div>
        </div>
        <div class="form-group col-md-4">
            <label for="turno" class="">Turno a</label>
                <button  type="button" placeholder="Hora *" id="horabtn" value='' required  class="horabtn form-control border-bottom btn btn-info" >Seleccionar hora</button>
                <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posT" class="hora form-control border-bottom"  required="" readonly>
                <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRT" class="delR form-control border-bottom"  required="" readonly>
        </div>
        <div class="form-group col-md-4">
                <label for="horaS" class="">Hora seleccionada</label>
                <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaT" class="horaVer tala tal form-control border-bottom"  required="" readonly>
        </div>
    </div>  
        </div>      
      </div>
    </ul>
    <ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab"> 
      <h2>Domicilio Cliente</h2>
      <div class="row">
        <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionD" class="">Departamento*</label>          
            <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize dom domo" required name="departamentoInstalacionD" id="departamentoInstalacionD">
                <option class="text-capitalize" value="" disabled selected >Seleccione Departamento* </option>
                <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                <?php if ($objDepartamento): ?>
                  <?php foreach ($objDepartamento as $var): ?>
                    <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                  <?php endforeach ?>
                <?php else: ?>
                <?php endif ?>
              </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionD" class="">Provincia*</label>          
            <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize dom domo" required name="provinciaInstalacionD" id="provinciaInstalacionD" >			              
		     </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionD" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize dom domo" required name="distritoInstalacionD"  id="distritoInstalacionD" >
          </select>	
          </div>

        </div>
        <div class="row">
          <div class="form-group  col-md-12">
            <label for="direccionInstalacionD" class="sr-only">Direccion*</label>
            <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionD" name="direccionInstalacionD"  class="form-control border-bottom dom domo" >
          </div>
        </div>  
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="referenciaInstalacionD" class="sr-only">Referencia *</label>
            <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" required="" name="referenciaInstalacionD" class="form-control border-bottom dom domo"  >
          </div>
          <div class="form-group col-md-6">
            <label for="cod" class="sr-only">Cordenadas Domicilio *</label>
            <input style="border: none;" type="text" placeholder="Cordenadas Domicilio *" id="cod" name="cordenadasD" class="form-control x2 text-capitalize border-bottom dom domo" required>
          </div>
        </div>  
      <div class="row">
        <div class="form-group col-md-4">   
            <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
              <input type="text" class="form-control dom domo fecha"  id="fechaID" name="fechaID" autocomplete="off" required>
          </div>
        </div>
        <div class="form-group col-md-4">
            <label for="turno" class="">Turno b</label>
                <button  type="button" placeholder="Hora *" id="horabtn" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Seleccionar hora</button>
                <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posD" class="hora form-control border-bottom"  required="" readonly>
            <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRD" class="delR form-control border-bottom"  required="" readonly>
        </div>
          <div class="form-group col-md-4">
                <label for="horaS" class="">Hora seleccionada</label>
                <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaD" class="horaVer domo dom  form-control border-bottom"  required="" readonly>
        </div>
         </div> 
    </div> 
  </ul>
  <ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">
    
    <div class="row">
      <div class="col-md-6"><h2>Concesionario</h2></div>
      <div class="col-md-6">
        <label for="concesionarioInstalacion" class="sr-only">Nombre de concesionario*</label>
        <input style="border: none;" type="text" placeholder="Nombre de concesionario*" id="concesionarioInstalacion" name="concesionarioInstalacion" class="form-control border-bottom con" required="" >
      </div>
    </div>    

    <div class="row">
      <div class="row">
        <div class="form-group  col-md-4">
          <label for="departamentoInstalacionC" class="">Departamento*</label>         
          <select  style="border: none;" class="departamentoInstalacionC border-bottom form-control text-capitalize con cono" required name="departamentoInstalacionC" id="departamentoInstalacionC">    
	        <option class="text-capitalize" disabled selected >Seleccione Departamento</option>           
	                <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
	                <?php if ($objDepartamento): ?>
	                  <?php foreach ($objDepartamento as $var): ?>
	                    <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
	                  <?php endforeach ?>
	                <?php else: ?>
	                <?php endif ?>				        
	     </select>
        </div>
        <div class="form-group  col-md-4">
          <label for="provinciaInstalacionC" class="">Provincia*</label>
          <select  style="border: none;" class="provinciaInstalacionC border-bottom form-control text-capitalize con cono" required name="provinciaInstalacionC" id="provinciaInstalacionC" >
            </select>
        </div>
        <div class="form-group  col-md-4">
          <label for="distritoInstalacionC" class="">Distrito*</label>
          <select  style="border: none;" class="distritoInstalacionC border-bottom form-control text-capitalize con cono" required name="distritoInstalacionC"  id="distritoInstalacionC" > 		</select>			
        </div>

      </div>
      <div class="row">
        <div class="form-group  col-md-6">
          <label for="direccionInstalacionC" class="">Direccion*</label>
          <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom con cono" >
        </div>
        <div class="form-group  col-md-6">
          <label for="referenciaInstalacionC" class="">Referencia *</label>
          <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom con cono" >
        </div>
      </div>    
      <div class="row">
                <div class="row">
                  <div class="form-group col-md-4">   
                      <label  class="">Fecha</label>
                      <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control con cono fecha"  id="fechaIC" name="fechaIC" autocomplete="off" required>
                      </div>
                  </div>

        <div class="form-group col-md-4">
            <label for="turno" class="">Turno c</label>
                <button  type="button" placeholder="Hora *" id="horabtn" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Seleccionar hora</button>
                <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posC" class="hora form-control border-bottom"  required="" readonly>
            <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRC" class="delR form-control border-bottom"  required="" readonly>
        </div>
                    <div class="form-group col-md-4">
                <label for="horaS" class="">Hora seleccionada</label>
                <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaC" class="horaVer cono con form-control border-bottom"  required="" readonly>
        </div>


                </div>
      </div>
  </div>
</ul>
</div>
</div>
    <div class="ibox" id="ibox1">
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-double-bounce">
                <div class="sk-double-bounce1"></div>
                <div class="sk-double-bounce2"></div>
            </div>
            <div id="programacionHtml">
            </div>
        </div>
    </div>
      <div class="row">
        <div class="form-group col-md-6">
          <label for="nombreContacto" class="">Nombre Contacto *</label>
          <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContacto" name="nombreContacto" class="form-control x2 text-capitalize border-bottom " required="" value="<?= $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ; ?>" >
        </div>
        <div class="form-group col-md-6">
        <label for="numeroContacto" class="">Número Contacto *</label>
        <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContacto" name="numeroContacto" class="form-control x2 text-capitalize border-bottom" required="" value="<?= (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">
      </div>
    </div>
<div class="row">
  <div class="form-group col-md-12">
    <label for="observaciong" class="">Observaciones </label>
    <input style="border: none;" type="text" placeholder="Observaciones " id="observaciong" name="observaciong" class="form-control x2 border-bottom focusUrgenteta" >
  </div>
</div>
</div>
    <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
        <button type="submit" name="crearDesdeChasis" id="agregarVehiculoBttn" class="btn btn-primary" value="<?php echo Token::generar() ?>">Confirmar</button>
        <div class="row">
            <div class="col-lg-12">
                <label id="mostrar" class="text-center text-danger">Limite de tecnicos alcanzado por favor intenta en otro horario</label>
            </div>
        </div>
    </div>
    </div>
<script src="../../assets/js/registrarme.js"></script>
<script src="../../assets/js/validacionFactura.pabel.js"></script>
<script src="../../assets/bootbox/bootbox.all.js"></script>
<?php include_once("../../includes/campanasScript.php") ?>
<script>
//$("#fselected").hide()

    <?php if($vhOC->cantidad>0){ ?>
$('.showOPERA').hide();
<?php } ?>
$('.showTipo').hide();
$(".esconderNo").hide();
$(".esconderNo").prop("disabled",true);
$("#entrega").change(function(){
        var r=$(this).val();
        if(r=='si'){
            $(".esconderNo").show('slow');
            $(".esconderNo").prop("disabled",false);
        }else{
            $(".esconderNo").hide('slow');
            $(".esconderNo").prop("disabled",true);
        }
        });
function validar(){
	var lugarI=$( "input[name='lugarInstacion']:checked" ).val()
	if (lugarI=='Taller') {
    	$('.con').prop('disabled', true);
    	$('.dom').prop('disabled', true);
    	$('.tal').prop('disabled', false);
	}else if (lugarI=='Domicilio Cliente') {
		$('.con').prop('disabled', true);
    	$('.dom').prop('disabled', false);
    	$('.tal').prop('disabled', true);    	
	}else if (lugarI=='Concesionario') {
		$('.con').prop('disabled', false);
    	$('.dom').prop('disabled', true);
    	$('.tal').prop('disabled', true);
	}
}
 $(document).ready(function(){	
     $("input[name='lugarInstacion']").on( "click",function(){
            $('#programacionHtml').html('');
            $('.horaVer').val('');
            $('.fecha').val('');
            $("#distritoInstalacionD").val('');
            $("#distritoInstalacionC").val('');
         validar();
     });		
     validar();
});

 function validaDP() {
    // var dp=1;
     var lugarI = $("input[name='lugarInstacion']:checked").val();
     if (lugarI == 'Taller') {
         var fecha = $('#fechaIT').val();
     } else if (lugarI == 'Domicilio Cliente') {
         var fecha = $('#fechaID').val();
     } else if (lugarI == 'Concesionario') {
         var fecha = $('#fechaIC').val();
     }
     var hora = $(".horaVer").val();
     var canthoraprog = $('#canthoraprog').val();
     var idtec = $('input[name="posT"]').val();
     // console.log("fecha: "+fecha+" idtec:"+idtec+" canhora:"+canthoraprog.trim());
     $.ajax({
         type: 'POST',
         url: 'ajax/validarRegistroTB',
         data:'tipo=save&fecha='+fecha+'&idtec='+idtec+"&canthoraprog=" + canthoraprog.trim() + "&hora=" + hora,
          // data :  'tipo=save&fecha=2019-11-27&idtec=2&canthoraprog=2&hora=12:00',
         success: function (data) {
             if (data.trim() == 'dp') {
                 //dp='1';
                 $("#agregarVehiculoBttn").prop("disabled",true);
           //      clearInterval(eventos.interval);
                 $("#programacionHtml").html('');
                 $(".horabtn").html('Seleccionar hora');
                 $(".horaVer").val('');
                // fn(dp);
                 bootbox.alert({
                     title: "<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                     message: "<label>El horario Seleccionado("+hora+") ya fue Registrado,Por Favor Seleccione otro*</label>",
                     size: 'small',
                     callback: function () {
                         /*  l.ladda( 'stop' );*/
                     }
                 });
             }else if(data.trim()=='Ok'){
                 $("#agregarVehiculoBttn").prop("disabled",false)
             }
         }
     });
 }

    var eventos={};
    eventos.frejcar =function(){
        validaDP();
    };
    eventos.interval = setInterval(eventos.frejcar, 3000);

    $(document).ready(function () {
        $('#agregarVehiculoBttn').click(function(e) {
            checked = $('[name="servad[]"]:checked').length;
            //var dp=1;
            if(checked==0) {
                bootbox.alert({
                    title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                    message: "<label>Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*</label>",
                    size: 'small',
                    callback: function () {
                  /*  l.ladda( 'stop' );*/
                    }
                });
             /*            alert("Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*");*/
                console.log("Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*");
            // $('[name="servad[]"]:checked')[0].focus();
                return false;
            }
            // return false;
        });
    });




    $('#rucFacturacion').on("change",function(){
	ruc=$('#rucFacturacion').val();
	if (ruc.length==11) {

	}else{
		alert("El ruc debe contener 11 caracteres actualmente tiene: "+ruc.length );
	}

	});

</script>
<script>  
$(".departamentoInstalacionD").change(function(){provincias4();});
 $(".departamentoInstalacionC").change(function(){provincias5();});
  $(".provinciaInstalacionD").change(function(){distritos4();});
  $(".provinciaInstalacionC").change(function(){distritos5();});
function provincias4(){
  var iddepartamento = $(".departamentoInstalacionD").val();
  $.ajax({        
    type : 'POST',
    url  : 'ajax/provincia',
    data :  'iddepartamento='+ iddepartamento,
    success : function(data)
    {
      $(".provinciaInstalacionD").html(data);
    }
  });
  return false;
}
function provincias5(){
  var iddepartamento = $(".departamentoInstalacionC").val();
  $.ajax({        
    type : 'POST',
    url  : 'ajax/provincia',
    data :  'iddepartamento='+ iddepartamento,
    success : function(data)
    {
      $(".provinciaInstalacionC").html(data);
    }
  });
  return false;
}

  function distritos4(){
      var idprovincia = $(".provinciaInstalacionD").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionD").html(data)
        }
      });
      return false;
    }
    function distritos5(){
      var idprovincia = $(".provinciaInstalacionC").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionC").html(data)
        }
      });
      return false;
}

if($('.dnioruconix').val()=="20550858461" ){
    $('.horaVer').prop('disabled', true);
}

$('.voucherImg').hide();
$('#formaPago').on("change",function(){
        var formaPagoS=document.getElementById('formaPago');
        value = formaPagoS.options[formaPagoS.selectedIndex].value;
        if (value=='Deposito'||value=='OrdenDeCompra') {		  
            $('#voucherImg').prop('disabled', false);
            $('.voucherImg').show('slow');
            $('#selectNa').prop('disabled', false);
            $('#entrega').prop('disabled', false);

            if($('.dnioruconix').val()=="20550858461" ){
                $('.horaVer').prop('disabled', true);
            }

        }else
        if (value=='PagoEntaler'|| value=='pacificoPandero'|| value=='pacificoBanbif'|| value=='Banbif'|| value=='LeyendaAPositiva' || value=='concesionarioPandero'|| value=='telefonica' || value=='AmericaBrokers' || value=='CEDEA' || value=='Moviliza') {
            //AmericaBrokers
            $('#voucherImg').prop('disabled', true);
            if(value=='PagoEntaler' || value=='Moviliza'){
                $('#selectNa').prop('disabled',false);
            }else{
                $('#selectNa').prop('disabled', true);
            }
            $('.voucherImg').hide('slow');
            $('#entrega').prop('disabled', true);
            if(value=='telefonica' || $('.dnioruconix').val()=="20550858461" ){
                $('.horaVer').prop('disabled', true);
            }
        }else{
            $('#entrega').prop('disabled', false);
            $('#selectNa').prop('disabled', false);
            $('.horaVer').prop('disabled', false);
            $('.voucherImg').show('slow');
        }

});

$('#tipo').on("change",function(){
    var tipoS=document.getElementById('tipo');
    value = tipoS.options[tipoS.selectedIndex].value;
    if (value=='Instalacion') {
//        $('#voucherImg').prop('disabled', false);
        $('.showTipo').show('slow');
    }else{
        $('.showTipo').hide();
    }

});

$('.lugarDeInstalacion').on("change",function(){
    var pendienteInstacion=document.getElementById("pendienteInstacion");
	if (pendienteInstacion.checked) {
  		$('.cono').prop('disabled', true);
    	$('.domo').prop('disabled', true);
    	$('.tala').prop('disabled', true);
         $("#programacionHtml").html('');
    	limpiarCapacidad();
	}else{
  		$('.cono').prop('disabled', false);
    	$('.domo').prop('disabled', false);
    	$('.tala').prop('disabled', false);
	}
})

$('#pendienteInstacion').on("change",function(){	
	var pendienteInstacion=document.getElementById("pendienteInstacion");
	if (pendienteInstacion.checked) {
  		$('.cono').prop('disabled', true);
    	$('.domo').prop('disabled', true);
    	$('.tala').prop('disabled', true);
         $("#programacionHtml").html('');
    	limpiarCapacidad();
	}else{
  		$('.cono').prop('disabled', false);
    	$('.domo').prop('disabled', false);
    	$('.tala').prop('disabled', false);
	}
});
$('#selectNa').on("change",function () {
var tipoDePagoSeleccionado=document.getElementById('selectNa');
value = tipoDePagoSeleccionado.options[tipoDePagoSeleccionado.selectedIndex].value;
if (value=="Boleta") {
  document.getElementById('rucFacturacion').removeAttribute("required");
  document.getElementById('razonSocialFacturacion').removeAttribute("required");
  document.getElementById('direccionFiscalFacturacion').removeAttribute("required");
  document.getElementById('direccionEntrega').removeAttribute("required");
}
if(value=="Factura"){
ruc=$('#rucFacturacion').val();
	if (ruc.length==11) {

	}else{
		alert("El ruc debe contener 11 caracteres actualmente tiene: "+ruc.length );
	}
	
  var rucFacturacion=document.getElementById('rucFacturacion');
  var razonSocialFacturacion=document.getElementById('razonSocialFacturacion');
  var direccionFiscalFacturacion=document.getElementById('direccionFiscalFacturacion');
  var direccionEntrega=document.getElementById('direccionEntrega');
  var att = document.createAttribute("required");
  var att1 = document.createAttribute("required");
  var att2 = document.createAttribute("required");
  var att3 = document.createAttribute("required");
  att.value = "true";  
  rucFacturacion.setAttributeNode(att);
  razonSocialFacturacion.setAttributeNode(att1);
  direccionFiscalFacturacion.setAttributeNode(att2);
  direccionEntrega.setAttributeNode(att3);
}

  });
  </script>
<script>

<?php $fechaA=date('Y-m-d') ?>
	 jQuery.datetimepicker.setLocale('es');

  $('.fecha').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y-m-d', 
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
  });


   $('#fechaEngrega').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d', 
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
  });
  $('#horaEntrega').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','11:00','14:00','16:00']
  });

$('.tiempo').typeahead({
    source: [
    {"name": "1 año"},
    {"name": "2 años"},
    {"name": "3 años"},
    {"name": "4 años"},
    {"name": "5 años"},
    {"name": "6 años"},
    {"name": "8 años"},
    {"name": "7 años"}
    ]
  });

<?php if ($datos=DatosFacturacion::horariosSugeridosParaEntregaDeFactura()): ?>
$('#horarioEntrega').typeahead({
  source: [
    <?php foreach ($datos as $var): ?>
      {"name": "<?php echo $var->fecha ?>"},  
    <?php endforeach ?>
    ]
  }); 
<?php endif ?>

<?php if ($datosD=Cliente::otenerTodoLosDistritos()): ?>
$('#distritoEntrega').typeahead({
  source: [
    <?php foreach ($datosD as $var): ?>
      {"name": "<?php echo $var->distrito ?>"},  
    <?php endforeach ?>
    ]
  }); 
<?php endif ?>

<?php if ($datosDopeCon=Operacion::obtenerConcesionarioInstalacion()): ?>
$('#concesionarioInstalacion').typeahead({
  source: [
    <?php foreach ($datosDopeCon as $var): ?>
      {"name": "<?php echo $var->nombreConcecionarioInstalicion ?>"},  
    <?php endforeach ?>
    ]
  }); 
<?php endif ?>

function lstTProg(){
    var lugarI=$("input[name='lugarInstacion']:checked" ).val();
    var distrito="0";
    var tthinstala=$('#canthoraprog').val();
    var cita = 'General';
    var concecion="0";
    var lugarT='';

    if (lugarI=='Taller') {
        var fecha = $('#fechaIT').val();
        distrito=$("#distritoTallerid").val()
        console.log($("#distritoTallerid").val())
        lugarT='Taller';
    }else if (lugarI=='Domicilio Cliente') {
        var fecha = $('#fechaID').val();
        distrito=$('#distritoInstalacionD').val();
        lugarT='Domicilio';
    }else if (lugarI=='Concesionario') {
        var fecha = $('#fechaIC').val();
        var concecion=$('#concesionarioInstalacion').val();
        var cita = concecion;
        distrito=$('#distritoInstalacionC').val();
        lugarT='Concecionario';
    }

    var cod = document.getElementById("plan").value;
    servAd = $('[name="servad[]"]:checked').length;
    if (fecha!='' && cod!='' && servAd!=0 &&(distrito==0 || distrito!=null) && concecion!='') {
        $(".horabtn").off( "click" );
        $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
        $.ajax({
            type : 'POST',
            url  : '../includes/tbProgramacion',
            data : {fecha:fecha,cita:cita,tthinstala:tthinstala,iddist:distrito,lugart:lugarT},
            success : function(data)
            {
                $("#programacionHtml").html(data);
                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                $(".horabtn").on("click",lstTProg).find(".horabtn");
            }
        });
    }else{
        $("#programacionHtml").html('<h3 class="text-center text-info">Primero debe seleccionar Plan,Servicios Adicionales,fecha y/o Distrito y/o Nombre Concesionario.</h3>');
    }
    return false;
}

 $(".horabtn").click(function(){
     lstTProg();
 });
$("#mostrar").hide();
$('.fecha').change(function(){
    $("#programacionHtml").html('');
    $(".horabtn").html('Seleccionar hora');
    $(".horaVer").val('');
});

<?php ?>

$('#seguro').change(function(){
    var plan=$("#plan option:selected").text();
    var aseguradora=$("#seguro option:selected").text();
    if (plan=='E-call' || plan=='E-call Plus' || plan=='Proteccion') {
        //$('#campana').trigger("chosen:updated");
        $("#campana").val("");
        if(aseguradora=="Pacifico Seguros"){
            // console.log(aseguradora+" aseguradora es");
            $("#campana option[value='GpsSmart']").show();
        }else{
            $("#campana option[value='GpsSmart']").hide();
        }
        $('.camapanaEsconder').show();
        $('#campana').prop('disabled',false);
    }else{
        $('.camapanaEsconder').hide();
        $('#campana').prop('disabled',true);
    }
});

$('#departamentoInstalacionD,#departamentoInstalacionC,#provinciaInstalacionD,#provinciaInstalacionC,#distritoInstalacionD,#distritoInstalacionC').change(function(){
    $("#programacionHtml").html('');
    $(".horabtn").html('Seleccionar hora');
    $(".horaVer").val('');
});


$('#plan').change(function(){
    $("#programacionHtml").html('');
    $(".horabtn").html('Seleccionar hora');
    $(".horaVer").val('');
    var arr="";
    var seleccionado = $("#plan").find('option:selected');
    var idtec = seleccionado.data('type');

     arr = $('[name="servad[]"]:checked').map(function(){
        return this.value;
    }).get();
    var str = arr.join(',');
    if(str==='' && (idtec === undefined)){
        str='0';
        idtec='0';
    }

    $.ajax({
        type : 'POST',
        url  : 'ajax/ServiciosAdicionales',
        data :  "idTec="+idtec,
        success : function(data)
        {
            if(data!=''){
                $(".servAdi").html(data);
            }else{
                console.log("No hay data");
            }
        }
    });
    $("#canthoraprog").val("0");
    $(".canthoraprog2").text("0 Horas");
    /*$.ajax({
        type : 'POST',
        url  : 'ajax/cantidadHoraProgramacion',
        data :  'idCheck='+str+"&idTec="+idtec,
        success : function(data)
        {
            if(data!=''){
                $("#canthoraprog").val(data);
                $(".canthoraprog2").text(data+" Horas");
            }else{
                console.log("No hay data");
            }
        }
    });*/
    return false;

});



<?php if ($datos=ServiciosAdicionales::obtenerServicesAdicional()): ?>
<?php foreach ($datos as $var): ?>
$("#<?php echo $var->codigo ?>").on("change",function (e) {
    $("#programacionHtml").html('');
    $(".horabtn").html('Seleccionar hora');
    $(".horaVer").val('');

    var isChecked = e.currentTarget.checked;
    var seleccionado = $("#plan").find('option:selected');
    var idtec = seleccionado.data('type');
  //  alert(isChecked);
    var arr = $('[name="servad[]"]:checked').map(function(){
        return this.value;
    }).get();
    var str = arr.join(',');
   // $("#canthoraprog").val("0");
//    $(".canthoraprog2").text("0 Horas");
    if(!isChecked && (idtec === undefined)){
        str='0';
        idtec='0';
    }
    $.ajax({
        type : 'POST',
        url  : 'ajax/cantidadHoraProgramacion',
        data :  'idCheck='+str+"&idTec="+idtec,
        success : function(data)
        {
            if(data!=''){
                $("#canthoraprog").val(data);
                $("#canthoraprog2").text(data+" Horas");
            }else{
                console.log("No hay data");
            }
        }
    });
    return false;
});
<?php endforeach ?>
<?php endif ?>


$('.b').hide();           
$('.tiendaAparece').hide();           
$('.tiendaAparece1').hide();           
function validarComisionPorIdcreador(ideje){
    if (ideje=='<?= $session->idusuario ?>') {
        $('#comision').prop('disabled', true);
    }else{
        $('#comision').prop('disabled', false);
    }
}
$('#tipoCanal').change( function() {
    var idjE=$('#idEjecutvio').val();
   if(this.value=='CONVENIO'){
      $('.b').show(); 
      $('.a').hide(); 
    $('.tiendaAparece1').hide();          
      $('#convenio').on('change', function() {
        if(this.value=='1'){
          $('.tiendaAparece').show();
          $('.tiendaAparece').prop('disabled', false);
        }else{
          $('.tiendaAparece').prop('disabled', true);
          $('.tiendaAparece').hide();          
        }
      });
      $('#ejecutivoCanalN').prop('disabled', false);
      $('#comision').prop('checked', false);
      $('#comision').prop('disabled', true);
      $('#ejecutivoCanalN').prop('disabled', false);            
        validarComisionPorIdcreador(idjE);
   }else if(this.value=='CONCESIONARIO'){
        $('.tiendaAparece1').show();
      $('#ejecutivoCanalN').prop('disabled', false);
      $('#comision').prop('disabled', false);
      $('.b').hide(); 
      $('.a').show(); 
        validarComisionPorIdcreador(idjE);
   }else if(this.value=='TRACKLINK OCTO'){
      $('.tiendaAparece1').hide('slow');
      $('#comision').prop('disabled', true);
      $('#ejecutivoCanalN').prop('disabled', true);
      $('.b').hide(); 
      $('.a').show(); 
   }else{
      $('#ejecutivoCanalN').prop('disabled', false);
    $('.tiendaAparece1').hide('slow');
      $('#comision').prop('disabled', false);
    $('.b').hide(); 
      $('.a').show(); 
    validarComisionPorIdcreador(idjE);
   }

});

$("#idEjecutvio").change(function() {
        var canal=$('#tipoCanal').val();
       var idje=$(this).val();
        if(canal=='TRACKLINK OCTO'){
            $('#comision').prop('disabled', true);
        }else{
            validarComisionPorIdcreador(idje);
        }
});


</script>

<?php endif ?>

<?php
} 
?>
<?php if (isset($_POST['idperacion'])){

?>
<?php 
if ($_POST['idperacion']) {
  include_once("../../includes/init.php");
  $idOperacion=htmlspecialchars(htmlentities($_POST['idperacion']));
  $clienteObjModal=new Cliente;
  $ObjetoOperacion=Operacion::obtenerOperacionPorId($idOperacion);
     $cl="" ;
    $df=DatosFacturacion::obtenerDatosFacturacionPorId($ObjetoOperacion->datosfacturacionid);
$obMenu=Usuario::obtenerMenu($session->idusuario);
  if ($ObjetoOperacion->idlocation==0) {
    $vehiculoObj=Vehiculo::obtenerVehiculo($ObjetoOperacion->idvehiculo);
    $clienteObj=Cliente::obtenerClientePorVehiculo($ObjetoOperacion->idvehiculo,$ObjetoOperacion->idcliente);  
         if($clienteObj->razonSocial!=""){
             $cl=strtoupper($clienteObj->razonSocial);
         }else{
             $cl=strtoupper($clienteObj->apellidosC." ".$clienteObj->nombre);
         }
  }else{
    $clienteObj=Onix::obtenerDatosDesdeIds($ObjetoOperacion->idcliente,$ObjetoOperacion->idvehiculo,$ObjetoOperacion->idlocation) ;
    $chequeoObj=Chequeos::obtenerChequeoTrackPorId($ObjetoOperacion->idChequeo);
    $vehiculoObj=false;
  }
  ?>
  <?php if ($ObjetoOperacion->estado=='36'||$ObjetoOperacion->estado=='14'){ ?>
  <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
        <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
  <h2 class="text-center"><?php echo $ObjetoOperacion->trabajo ?></h2>
  <input type="hidden" name="trabajoO" value="<?= $ObjetoOperacion->trabajo  ?>" >
  <?php if ($ObjetoOperacion->idChequeo!=0): ?>
    <h3 id="mot" class="text-center text-danger">MOTIVO: <?= $chequeoObj->motivo ?></h3>
    <h3 id="seguimiento" class="text-center text-danger">Seguimiento realizado: <?= $chequeoObj->seguimientoRealizado ?></h3>
  <?php endif ?>

<?php if ($vehiculoObj): ?>

<?php if (!empty($vehiculoObj->serviciosAdicionales)): ?>
 <div class="row">
   <div class="form-group col-md-12">
     Servicios adicionales: <?php echo $vehiculoObj->serviciosAdicionales ?>
   </div>
 </div>
<?php endif ?>
</div> 
<!-- ORDENES INSTALACIONES FIN END END END END  --> 
  <div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>

        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->dni ?>">       
      <?php else: ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->ruc ?>">        
      <?php endif ?>  
    </div>

    <div class="col-md-9">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->razonSocial ?>">       
      <?php endif ?>
    </div> 
    </div> 
<hr>
<h3>Datos del vehiculo</h3>
  <div class="row"><hr>
    <div class="col-lg-12 field_wrapper"> 
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="field_name[0][]" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $vehiculoObj->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="">Chasis</label>
          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="field_name[1][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->chasis ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="motor" class="">Motor</label>
          <input style="border: none;" type="text" placeholder="Motor" id="motor" name="field_name[2][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->motor ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="marca" class="">Marca</label>
          <input style="border: none;" type="text" placeholder="Marca" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $vehiculoObj->marca ?>" >
        </div>
      </div> 
  <div class="row">
  		 <div class="form-group col-md-4">
          <label for="marca" class="">Plan</label>
          <input style="border: none;" type="text" placeholder="Plan" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?= $vehiculoObj->plan ?>" >
        </div>
         <div class="form-group col-md-4">
          <label for="marca" class="">Campaña</label>
          <input style="border: none;" type="text" placeholder="Campaña" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?= $vehiculoObj->campana ?>" >
        </div>
         <div class="form-group col-md-4">
          <label for="marca" class="">Seguro</label>
          <input style="border: none;" type="text" placeholder="Campaña" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?= $vehiculoObj->companiaSeguro ?>" >
        </div>
  </div>
    </div>
  </div>
<hr>
<div class="form-group col-md-12 ">
   <p style="font-weight:bold" class="text-danger">Servicios Adicionales Seleccionados :</p>
   <?php ServiciosAdicionales::obtenerServAdicionalSeleccionadoVer($idOperacion) ?>
</div>
<h4 class="text-danger tex-center"> <?= $ObjetoOperacion->observacion ?></h4>
<?php if($ObjetoOperacion->trabajo=="Instalacion"){ ?>
<h2 class="text-center">Datos de instalacíon</h2>
<div class="row">
  <div class="form-group col-md-6">
    <label for="tecnico" class="">Tecnicos</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="tecnico">
      <?php if ($ObjetoOperacion->idTecnicoAsignado!=0): ?>
        <?php 
          $usuarioDato=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoAsignado);
         ?>
      <option class="text-capitalize" value="<?= $ObjetoOperacion->idTecnicoAsignado ?>" selected ><?= $usuarioDato->usuarioNombre ?> <?= $usuarioDato->usuarioApellidoPaterno ?></option>
      <?php else: ?>
      <option class="text-capitalize" value="" disabled selected >Seleccione técnico</option>
      <?php endif ?>
      <?php Usuario::obtenerTecnicos() ?>            
    </select>
  </div>
  <div class="form-group col-md-6">
    <label for="orte" class="">Numero de Orte</label>
    <input style="border: none;" type="text" placeholder="Numero de Orte" id="orte" name="orte" class="border-bottom form-control x2 " required  value="<?= $ObjetoOperacion->numeroOrte ?>">
  </div>
</div> 
<?php if ($ObjetoOperacion->lugarInstalacion=="Domicilio"): ?>
<div class="row">
   <div class="col-md-3"><h3>Coordenadas de instalación --></h3></div>
   <div class="col-md-6">
    <input style="border: none;" type="text" placeholder="Coordenadas insalaicon" id="orden" name="orden" class="border-bottom form-control x2 " value="<?php echo $ObjetoOperacion->coordenadasInstalacion ?>" >
  </div>
</div>
<?php endif ?>
<hr>
<h4>Lugar de instalación <?php echo $ObjetoOperacion->lugarInstalacion ?> <?php echo $retVal = (!empty($ObjetoOperacion->nombreConcecionarioInstalicion)) ? $ObjetoOperacion->nombreConcecionarioInstalicion : "" ; ?>  del dia <?= !empty($ObjetoOperacion->fechaInstalacion)? formatoHaceEn($ObjetoOperacion->fechaInstalacion):'Pediente' ?></h4>
<hr>

<input type="hidden" name="idoperacion" value="<?php echo $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idvh" value="<?php echo $vehiculoObj->idvehiculo ?>">
<input type="hidden" name="datosfacturacionid" value="<?php echo $ObjetoOperacion->datosfacturacionid ?>">
<div class="row">
  <div class="form-group  col-md-4">
    <label for="departamentoInstalacionD" class="">Departamento*</label>
    <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize" required name="departamentoInstalacionD" id="departamentoInstalacionD">
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>" disabled selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
      <?php if ($objDepartamento): ?>
        <?php foreach ($objDepartamento as $var): ?>
          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
        <?php endforeach ?>
      <?php else: ?>
      <?php endif ?>
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="provinciaInstalacionD" class="">Provincia*</label>
    <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>" disabled selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="distritoInstalacionD" class="">Distrito*</label>
    <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize" required name="distritoInstalacionD"  id="distritoInstalacionD" >
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>" disabled selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
    </select>           
  </div>
</div>
<div class="row">          
  <div class="form-group col-md-3">
    <label for="lugar" class="">Lugar</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="lugar">
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->lugarInstalacion ?>"  selected ><?php echo $ObjetoOperacion->lugarInstalacion ?></option>
      <option class="text-capitalize" value="Taller" >Taller</option>
      <option class="text-capitalize" value="Domicilio Cliente" >Domicilio Cliente</option>
      <option class="text-capitalize" value="Concesionario" >Concesionario</option>
    </select>
  </div>
  <div class="form-group col-md-9">
    <label for="direccionInstalacion" class="">Dirección Instalación </label>
    <input style="border: none;" type="text" placeholder="Direccion Instalcion" id="direccionInstalacion" name="direccionInstalacion" class="border-bottom form-control" required value="<?php echo $ObjetoOperacion->direccionInstacion ?>">
  </div>
</div>
<?php if ($ObjetoOperacion->coordenadasInstalacion != ''): ?>
  <div class="row"><div class="col-lg-12"><label for="cordenadas">Coordenadas de instalacion: </label><label > <?php echo $ObjetoOperacion->coordenadasInstalacion ?></label><br><h1 class="text-success"><a  target="_blank" href="mapa.php?coordenadas=<?php echo $ObjetoOperacion->coordenadasInstalacion ?>&lugar=<?php echo $ObjetoOperacion->direccionInstacion ?>">ver mapa</a></h1></div></div>    
<?php endif ?>

<div class="row">
  <div class="form-group col-md-5" >
    <!-- <label  class="sr-only">Fecha</label>
    <input type="text" id="datetimepicker3" name="fecha" required/> -->      
    <div class="row">
      <div class="form-group col-md-6">   
        <label>Fecha</label>
        <div class="input-group date">
          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control"  id="fechaCh" name="fecha" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10) ?>" >
        </div>
      </div>
      <div class="form-group col-md-6">
        <label>Hora</label>
        <div class="input-group" data-autoclose="true">
          <input type="text" class="form-control" id="horaCh" name="hora" autocomplete="off" required value="<?=  substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>"> 
          <span class="input-group-addon">
            <span class="fa fa-clock-o"></span>
          </span>
        </div>
      </div>
    </div> 
  </div> 
  <div class="form-group col-md-4">
     <label for="personaContacto" class="">Persona contacto </label>
    <input style="border: none;" type="text" placeholder="Direccion Instalcion" id="personaContacto" name="personaContacto" class="border-bottom form-control" required value="<?php echo $ObjetoOperacion->nombreContacto ?>">
  </div>
  <div class="form-group col-md-3">
     <label for="numeroContacto" class="">Número contacto </label>
    <input style="border: none;" type="text" placeholder="Número contacto" id="numeroContacto" name="numeroContacto" class="border-bottom form-control" required value="<?php echo $ObjetoOperacion->numeroContacto ?>">
  </div>
</div>
      <?php } ?>
<?php else: ?>
  <div class="row">
        <div class="form-group col-md-3">
        <label for="Observaciones" > Usuario Web</label>
        <input name="usuarioWeb" style="border: none;" type="text" placeholder="Web" id="Observaciones" <?php echo $retVal = (!empty($clienteObj->dni)) ? "readonly" : "" ; ?> class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>">
      </div>
      <div class="form-group col-md-5">
      <?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
        <label for="numeroTecnico" >Nombre de cliente*</label>
        <input  style="border: none;" type="text" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ; ?> ">
      </div>
      <div class="col-md-2"><label for="estado" >Celular</label>
        <input style="border: none;" type="text" <?php echo $retVal = (!empty($clienteObj->celular)) ? "readonly" : "" ; ?> id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">
      </div>
      <div class="col-md-2"><label for="estado" >Celular</label>
        <input style="border: none;" type="text" readonly id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular2)) ? "$clienteObj->celular2" : "" ; ?>">
      </div>
    </div>
     </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="form-group col-md-3">
    <label for="numeroTecnico" >Placa Vehiculo*</label>
    <input  style="border: none;" readonly type="text" placeholder="Placa" id="numeroTecnico" name="placa" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->placa)) ? "$clienteObj->placa" : "" ; ?>"   >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Marca *</label>
    <input required <?php echo $retVal = (!empty($clienteObj->marca)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->marca)) ? "$clienteObj->marca" : "" ; ?>" >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Modelo*</label>
    <input  required <?php echo $retVal = (!empty($clienteObj->modelo)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Modelo" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($clienteObj->modelo)) ? "$clienteObj->modelo" : "" ; ?>">
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Color</label>
    <input <?php echo $retVal = (!empty($clienteObj->color)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Color" id="Observaciones" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->color)) ? "$clienteObj->color" : "" ; ?>">
  </div>  
</div>

<span class="ocultarThis">
<div class="row">
  <div class="form-group col-md-4">
    <label for="Observaciones"  >Chasis*</label>
    <input required <?php echo $retVal = (!empty($clienteObj->chasis)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->chasis)) ? "$clienteObj->chasis" : "" ; ?>">
  </div>
  <div class="form-group col-md-4">
    <label for="Observaciones" >Motor</label>
    <input <?php echo $retVal = (!empty($clienteObj->motor)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="motor" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->motor)) ? "$clienteObj->motor" : "Actualizar" ; ?>"  >
  </div>
  <div class="form-group col-md-4">
    <label for="Observaciones" >Plan</label>
    <input  style="border: none;" type="text" placeholder="Observaciones" readonly id="Observaciones" name="plan" class="border-bottom form-control" value="<?= $chequeoObj->plan ?>"  >
  </div>
</div>
<input type="hidden" name="idoperacion" value="<?= $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idChequeo" value="<?= $ObjetoOperacion->idChequeo ?>">
  <?php if ($ObjetoOperacion->trabajo=='Chequeo en linea'): ?>
    <div class="row"> 
  <div class="form-group col-md-12">
    <label for="Observaciones" >Número de orden</label>
    <input  style="border: none;" type="text" placeholder="Número de orden" id="numeroOrden" name="numeroOrden" class="border-bottom form-control"  >
    <input type="hidden" name="chequeo" value="ok">
    <input type="hidden" name="chequeoLinea" value="ok">

  </div>
</div>
  <?php endif ?>
<div class="row">
  <div class="form-group col-md-6">
    <label for="numeroTecnico" >Persona de contacto</label>
    <input  style="border: none;" readonly type="text" placeholder="Persona de contacto" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?= $chequeoObj->personaContacto ?>"   >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Numero contacto *</label>
    <input required readonly="" style="border: none;" type="text" placeholder="Numero contacto *" id="Observaciones" name="numcontacto" class="border-bottom form-control"  value="<?= $chequeoObj->numeroContacto ?>" >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Ubicacion de sistema*</label>
    <input  required readonly style="border: none;" type="text" placeholder="Ubicacion de sistema" id="Observaciones" name="ubsistema" class="border-bottom form-control"   value="<?= $chequeoObj->ubicacionSistema ?>">
  </div>
  </div>
  <div class="row">
  <div class="form-group col-md-2">
    <label for="numeroTecnico" >Lugar</label>
    <input  style="border: none;" readonly type="text" placeholder="Persona de contacto" id="numeroTecnico" name="lugar" class="border-bottom form-control" required value="<?= $chequeoObj->lugar ?>"   >
  </div>
  <div class="form-group col-md-5">
    <label for="Observaciones" >Direccion *</label> <a target="_blank" href="mapa.php?coordenadas=<?= $chequeoObj->coordenadas ?>&lugar=<?= $chequeoObj->direccion ?>" class="text-center">Ver mapa</a>
    <input required readonly="" style="border: none;" type="text" placeholder="Numero contacto *" id="Observaciones" name="dir" class="border-bottom form-control"  value="<?= $chequeoObj->direccion ?>" >
  </div>
  <div class="form-group col-md-5">
    <label for="Observaciones" >Referencia *</label>
    <input  required readonly style="border: none;" type="text" placeholder="Ubicacion de sistema" id="Observaciones" name="ref" class="border-bottom form-control"   value="<?= $chequeoObj->referencia ?>">
  </div>  
</div>  
  <div class="row">
  <div class="form-group col-md-5">
    <label for="numeroTecnico" >Fecha instalacion</label>
    <input  style="border: none;" readonly type="text" placeholder="Persona de contacto" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?= $chequeoObj->fechaInstalacion ?>" >
  </div>
  <div class="form-group col-md-7">
    <label for="Observaciones" >Tecnico instalador *</label>
    <input required readonly="" style="border: none;" type="text" placeholder="Numero contacto *" id="Observaciones" name="tei" class="border-bottom form-control"  value="<?= $chequeoObj->instalador ?>">
  </div>
  <?php 
      $chequeoObj->fechaChequeoEnSac;
      $fecha=date("Y-m-d",strtotime($chequeoObj->fechaChequeoEnSac));
      $hora=date("H:i",strtotime($chequeoObj->fechaChequeoEnSac));

   ?>
    <div class="row">
    <h4 class="text-center">hora y fecha estimada de instalación: <?= formatoHaceEn($chequeoObj->fechaChequeoEnSac) ?></h4>
  </div>
  
</div>
  <div class="row"> 
  <div class="form-group col-md-6">
     <label for="tecnico" class="">Tecnicos</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="tecnico">
    <?php if ($ObjetoOperacion->idTecnicoAsignado!=0): ?>
        <?php 
          $usuarioDato=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoAsignado);
         ?>
      <option class="text-capitalize" value="<?= $ObjetoOperacion->idTecnicoAsignado ?>" selected ><?= $usuarioDato->usuarioNombre ?> <?= $usuarioDato->usuarioApellidoPaterno ?></option>
      <?php else: ?>
      <option class="text-capitalize" value="" disabled selected >Seleccione técnico</option>
      <?php endif ?>    
      <?php Usuario::obtenerTecnicos() ?>            
    </select>
  </div>
  <div class="form-group col-md-6">
    <label for="numeroOrte" >Número de orte</label>
    <input  style="border: none;" type="text" placeholder="Número de orte" id="numeroOrte" name="numeroOrte" class="border-bottom form-control"  required="" value="<?= $ObjetoOperacion->numeroOrte  ?>">
  </div>
</div>
<div class="row">
  <div class="col-md-6 form-group"><label for="Observaciones" >Fecha *</label>
    <input required  style="border: none;" type="text" placeholder="" id="fechaCh" name="fecha" class="border-bottom form-control"  value="<?= $fecha ?>" ></div>
  <div class="col-md-6 form-group"><label for="Observaciones" >Hora *</label>
    <input required  style="border: none;" type="text" placeholder=" *" id="horaCh" name="hora" class="border-bottom form-control"  value="<?= $hora ?>" ></div>
</div>
  
<?php //en los dos tipos de chequeo que queda en SAC confirman orden orte fecha y lugar  ?>
</span>
<?php endif ?>
<?php }else{ ?>

        <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>

        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->dni ?>">       
      <?php else: ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->ruc ?>">        
      <?php endif ?>  
    </div>

    <div class="col-md-9">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->razonSocial ?>">       
      <?php endif ?>
    </div> 
    </div> 
<hr>
<h3>Datos del vehiculo</h3>
  <div class="row"><hr>
    <div class="col-lg-12 field_wrapper"> 
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="field_name[0][]" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $vehiculoObj->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="">Chasis</label>
          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="field_name[1][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->chasis ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="motor" class="">Motor</label>
          <input style="border: none;" type="text" placeholder="Motor" id="motor" name="field_name[2][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->motor ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="marca" class="">Marca</label>
          <input style="border: none;" type="text" placeholder="Marca" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $vehiculoObj->marca ?>" >
        </div>
      </div>
      </div>
  </div>
  <div class="row">
  		 <div class="form-group col-md-4">
          <label for="marca" class="">Plan</label>
          <input style="border: none;" type="text" placeholder="Plan" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?= $vehiculoObj->plan ?>" >
        </div>
         <div class="form-group col-md-4">
          <label for="marca" class="">Campaña</label>
          <input style="border: none;" type="text" placeholder="Campaña" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?= $vehiculoObj->campana ?>" >
        </div>
         <div class="form-group col-md-4">
          <label for="marca" class="">Seguro</label>
          <input style="border: none;" type="text" placeholder="Campaña" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?= $vehiculoObj->companiaSeguro ?>" >
        </div>

      <div class="form-group col-md-12 ">
          <p style="font-weight:bold" class="text-danger">Servicios Adicionales Seleccionados :</p>
          <?php ServiciosAdicionales::obtenerServAdicionalSeleccionadoVer($idOperacion) ?>
      </div>

  </div>
<h2 class="text-center">Datos de instalacíon</h2>
<div class="row">
  <div class="form-group col-md-6">
    <label for="tecnico" class="">Tecnicos</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="tecnico" disabled>
      <?php if ($ObjetoOperacion->idTecnicoAsignado!=0): ?>
        <?php 
          $usuarioDato=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoAsignado);
         ?>
      <option class="text-capitalize" value="<?= $ObjetoOperacion->idTecnicoAsignado ?>" selected ><?= $usuarioDato->usuarioNombre ?> <?= $usuarioDato->usuarioApellidoPaterno ?></option>
      <?php else: ?>
      <option class="text-capitalize" value="" disabled selected >Seleccione técnico</option>
      <?php endif ?>
      <?php Usuario::obtenerTecnicos() ?>            
    </select>
  </div>
  <div class="form-group col-md-6">
    <label for="orte" class="">Numero de Orte</label>
    <input style="border: none;" type="text" placeholder="Numero de Orte" id="orte" name="orte" class="border-bottom form-control x2 " required  value="<?= $ObjetoOperacion->numeroOrte ?>">
  </div>
</div> 
<?php if ($ObjetoOperacion->lugarInstalacion=="Domicilio"): ?>
<div class="row">
   <div class="col-md-3"><h3>Coordenadas de instalación --></h3></div>
   <div class="col-md-6">
    <input style="border: none;" type="text" placeholder="Coordenadas insalaicon" id="orden" name="orden" class="border-bottom form-control x2 " value="<?php echo $ObjetoOperacion->coordenadasInstalacion ?>" >
  </div>
</div>
<?php endif ?>
<hr>
<h4>Lugar de instalación <?php echo $ObjetoOperacion->lugarInstalacion ?> <?php echo $retVal = (!empty($ObjetoOperacion->nombreConcecionarioInstalicion)) ? $ObjetoOperacion->nombreConcecionarioInstalicion : "" ; ?>  del dia <?= !empty($ObjetoOperacion->fechaInstalacion)? formatoHaceEn($ObjetoOperacion->fechaInstalacion):'Pendiente' ?></h4>
<hr>

<input type="hidden" name="idoperacion" value="<?php echo $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idvh" value="<?php echo $vehiculoObj->idvehiculo ?>">
<input type="hidden" name="datosfacturacionid" value="<?php echo $ObjetoOperacion->datosfacturacionid ?>">
<div class="row">
  <div class="form-group  col-md-4">
    <label for="departamentoInstalacionD" class="">Departamento*</label>
    <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize" required name="departamentoInstalacionD" id="departamentoInstalacionD">
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>" disabled selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
      <?php if ($objDepartamento): ?>
        <?php foreach ($objDepartamento as $var): ?>
          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
        <?php endforeach ?>
      <?php else: ?>
      <?php endif ?>
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="provinciaInstalacionD" class="">Provincia*</label>
    <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>" disabled selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="distritoInstalacionD" class="">Distrito*</label>
    <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize" required name="distritoInstalacionD"  id="distritoInstalacionD" >
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>" disabled selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
    </select>           
  </div>
</div>
<div class="row">          
  <div class="form-group col-md-3">
    <label for="lugar" class="">Lugar</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="lugar">
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->lugarInstalacion ?>"  selected ><?php echo $ObjetoOperacion->lugarInstalacion ?></option>
      <option class="text-capitalize" value="Taller" >Taller</option>
      <option class="text-capitalize" value="Domicilio Cliente" >Domicilio Cliente</option>
      <option class="text-capitalize" value="Concesionario" >Concesionario</option>
    </select>
  </div>
  <div class="form-group col-md-9">
    <label for="direccionInstalacion" class="">Dirección Instalación </label>
    <input style="border: none;" type="text" placeholder="Direccion Instalcion" id="direccionInstalacion" name="direccionInstalacion" class="border-bottom form-control" required value="<?php echo $ObjetoOperacion->direccionInstacion ?>">
  </div>
</div>
<?php if ($ObjetoOperacion->coordenadasInstalacion != ''): ?>
  <div class="row"><div class="col-lg-12"><label for="cordenadas">Coordenadas de instalacion: </label><label > <?php echo $ObjetoOperacion->coordenadasInstalacion ?></label><br><h1 class="text-success"><a  target="_blank" href="mapa.php?coordenadas=<?php echo $ObjetoOperacion->coordenadasInstalacion ?>&lugar=<?php echo $ObjetoOperacion->direccionInstacion ?>">ver mapa</a></h1></div></div>    
<?php endif ?>

<div class="row">
  <div class="form-group col-md-5" >
    <div class="row">
      <div class="form-group col-md-6">   
        <label>Fecha</label>
        <div class="input-group date">
          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control"  id="fechaCh" name="fecha" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10) ?>" >
        </div>
      </div>
      <div class="form-group col-md-6">
        <label>Hora</label>
        <div class="input-group" data-autoclose="true">
          <input type="text" class="form-control" id="horaCh" name="hora" autocomplete="off" required value="<?=  substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>"> 
          <span class="input-group-addon">
            <span class="fa fa-clock-o"></span>
          </span>
        </div>
      </div>
    </div> 
  </div> 
  <div class="form-group col-md-4">
     <label for="personaContacto" class="">Persona contacto </label>
    <input style="border: none;" type="text" placeholder="Direccion Instalcion" id="personaContacto" name="personaContacto" class="border-bottom form-control" required value="<?php echo $ObjetoOperacion->nombreContacto ?>">
  </div>
  <div class="form-group col-md-3">
     <label for="numeroContacto" class="">Número contacto </label>
    <input style="border: none;" type="text" placeholder="Número contacto" id="numeroContacto" name="numeroContacto" class="border-bottom form-control" required value="<?php echo $ObjetoOperacion->numeroContacto ?>">
  </div>
</div>
  <div class="row">
       <div class="form-group col-md-4">
          <label for="placa" class="">Tecnico Asignado</label>
          <?php $usuarioAsgi=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoAsignado) ?>
          <input style="border: none;" type="text" placeholder="Tecnico Asignado" id="placa" name="" class="border-bottom form-control x2 text-uppercase" required value="<?= $usuarioAsgi->usuarioNombre.' '.$usuarioAsgi->usuarioApellidoPaterno ?>">
        </div>
       <div class="form-group col-md-4">
       <?php $usuarioInst=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoInstalador) ?>
          <label for="placa" class="">Tecnico instalador</label>
          <input style="border: none;" type="text" placeholder="Tecnico instalador" id="placa" name="" class="border-bottom form-control x2 text-uppercase" required value="<?= $usuarioInst->usuarioNombre.' '.$usuarioInst->usuarioApellidoPaterno ?>">
        </div>
       <div class="form-group col-md-4">
       <?php $usuarioCentr=Usuario::obtenerUsuario($ObjetoOperacion->idusuarioActivador) ?>
          <label for="placa" class="">Usuario cetral Hora</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="" class="border-bottom form-control x2 text-uppercase" required value="<?= $usuarioCentr->usuarioNombre.' '.$usuarioCentr->usuarioApellidoPaterno ?>">
          <br><small class="text-danger">hora <?= haceMinimo($ObjetoOperacion->fechaActivacion) ?></small>
        </div>
  </div>


<?php } ?>
<?php
} 
?>
<?php if (!empty($ObjetoOperacion->datosfacturacionid)): ?>
  <?php
    $objFacturacion=DatosFacturacion::obtenerDatosFacturacionPorId($ObjetoOperacion->datosfacturacionid);
   ?>
<hr>
<h2 class="text-info text-center">Datos de facturación</h2>
<h2 class="text-danger text-center"><?= Operacion::obtenerObservacionesHistorialAndCOnact($ObjetoOperacion->idoperacion) ?></h2>

  <hr>

<div class="row">
    <div class="form-group col-md-4">
        <label for="corigen" class="">Concesionario Origen</label>
        <input style="border: none;" type="text" placeholder="Concesionario Origen" id="corigen" name="corigen" class="border-bottom form-control text-uppercase corigen " autocomplete="off" readonly value="<?= $objFacturacion->canalOrigen ?>" >
    </div>
</div>
<div class="row">
        <div class="form-group col-md-3">
          <label for="tipoCCnal" class="">Tipo</label>
          <input style="border: none;" type="text" placeholder="tipoCCnal" id="tipoCCnal" name="tipoCCnal" class="border-bottom form-control x2 text-uppercase" required value="<?= $objFacturacion->tipoCanal ?>">
        </div>
<div class="form-group col-md-3">
          <label for="tipoCCnal" class="">Tienda</label>
          <input style="border: none;" type="text" placeholder="tipoCCnal" id="tiendaCCT" name="tiendaCCT" class="border-bottom form-control x2 text-uppercase" required value="<?= $objFacturacion->oficina ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="nombreCCCanl" class="">Nombre</label>
          <input style="border: none;" type="text" placeholder="nombreCCCanl" id="nombreCCCanl" name="nombreCCCanl" class="border-bottom form-control text-uppercase" required value="<?= $objFacturacion->nombreCanal ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="EjecutCCanl" class="">Ejecutivo canal</label>
          <input style="border: none;" type="text" placeholder="EjecutCCanl" id="EjecutCCanl" name="EjecutCCanl" class="border-bottom form-control text-uppercase" required value="<?= $objFacturacion->ejecutivoCanal ?>" >
        </div>
</div> 
<div class="row">
	<div class="col-md-3">Documento:  <strong><?= $objFacturacion->tipoDocumentoFacturacion ?></strong></div>
	<div class="col-md-3">Numero Orden: <strong><?=  $objFacturacion->orden ?></strong></div>
	<div class="col-md-3">Plan: <strong><?= $objFacturacion->plan." ".$objFacturacion->tiempo." años "  ?></strong></div>
	<div class="col-md-3">Precio: <strong><?= $objFacturacion->costo ?></strong></div>
</div>
<div class="hr-line-dashed"></div> 
<div class="row">
	<div class="col-md-3">Estado Facturación:  <strong><?= obtenerEstado($objFacturacion->estadoFacturacion) ?></strong></div>
	<div class="col-md-3">Numero Facturación: <strong><?=  (!empty($objFacturacion->numeroBoleta))?$objFacturacion->numeroBoleta:$objFacturacion->numeroFactura ?></strong></div>
	<div class="col-md-3">Estado de entrega: <strong><?= obtenerEstado($objFacturacion->estadoEntrega)  ?></strong></div>
    <div class="col-md-3">Forma de Pago: <strong><?= $objFacturacion->formaPago  ?></strong></div>
</div>
<br>
<div class="row">
    <div class="col-md-12">

        <?php $filess=DatosFacturacion::obtenerFiles($df->datosfacturacionid) ?>
        <?php if($filess):  ?>
        <table class="table table-hover">
        <?php foreach($filess as $file): ?>
            <tr>
                <td><?= $file->fecha ?></td>
                <td><?= $file->oficina ?>&nbsp;&nbsp;<span class="fa fa-file-pdf-o"></span></td>
                <td><?= $file->tipoCanal ?></td>
                <td><?= $file->tipoCanal ?>&nbsp;
                    <a
                    href="/ope/downinvdoc?tnk=fef56f6a7c51ab802&b=<?= $file->voucher ?>&f=<?= $file->apunte ?>&fn=<?= strtoupper($file->oficina) ?> <?= trim($cl)." ".$vehiculoObj->placa ?>"
                    target="_blank"><span class="fa fa-download"></span></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </table>
        <?php else: ?>
            <?php if(!is_null($ObjetoOperacion->ordenContrato)){ ?>
                <!--<br>
                <a class="btn btn-primary" href="/ope/downinvdoc?tnk=eef56f6a7c51ab800&o=<?= $ObjetoOperacion->ordenContrato ?>&d=<?=$df->datosfacturacionid ?>" title="Generar">
                    <span class="fa fa-file"><span><i class="fa fa-plus"></i></span>
                </a>-->
            <?php }  ?>
        <?php endif; ?>

                <br>
                
                <a class="btn btn-primary" href="/ope/downinvdoc?tnk=eef56f6a7c51ab800&o=<?= $ObjetoOperacion->ordenContrato ?>&d=<?=$df->datosfacturacionid ?>" title="Generar">
                    <span class="fa fa-file"><span><i class="fa fa-plus"></i></span>
                </a>
    </div>
    </div>
<div class="hr-line-dashed"></div> 
<h3 class="text-center">Cobra comisión: <?= (Comisiones::obtenerDatosDeComisionPorIdFacturacion($ObjetoOperacion->datosfacturacionid))?'SI':'NO' ?></h3>
<div class="row">
        <div class="form-group col-md-6">
          <label for="direccionEntrega" class="sr-only">Dirección de engrega</label>
          <input style="border: none;" type="text" placeholder="Dirección de engrega" id="direccionEntrega" name="direccionEntrega" class="form-control border-bottom text-capitalize" value="<?= $objFacturacion->direccionEntrega ?>">
        </div>
        <div class="form-group col-md-6">
          <label for="referenciaEntrega" class="sr-only">Referencia de engrega</label>
          <input style="border: none;" type="text" placeholder="Referencia de engrega" id="referenciaEntrega" name="referenciaEntrega" class="form-control border-bottom text-capitalize" required="" value="<?= $objFacturacion->referenciaEntrega ?>">
        </div>
      </div>
      <div class="row">
        <div class="form-group col-md-4">
          <label for="distritoEntrega" class="sr-only">Distrito de entrega</label>
          <input style="border: none;" type="text" placeholder="Distrito de entrega" id="distritoEntrega" name="distritoEntrega" class="form-control border-bottom text-capitalize" required value="<?= $objFacturacion->distritoEntrega ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="horarioEntrega" class="sr-only">Horario de entrega</label>
          <input style="border: none;" type="text" placeholder="Horario de entrega" id="horarioEntrega" name="horarioEntrega" class="form-control border-bottom text-capitalize" required value="<?= $objFacturacion->fechaHoraDeEntrega ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="personaEntrega" class="sr-only">Persona de recepción de factura </label>
          <input style="border: none;" type="text" placeholder="Persona de recepción de factura" id="personaEntrega" name="personaEntrega" class="form-control border-bottom text-capitalize" required value="<?= $objFacturacion->recibidoPor ?>">
        </div>
      </div>

<hr>
<?php $os=Usuario::obtenerUsuario($objFacturacion->idUsuarioCreado); ?>
<h4 class="text-center">Ejecutivo: <?php  echo $os->usuarioNombre." ".$os->usuarioApellidoPaterno;  ?></h4>           
  <?php if (empty($objFacturacion->voucher)): ?> 
    <h3 class="text-center text-danger">Atención pago en caja</h3>
    <input type="hidden" name="pagoEnCaja" value="ok">
  <?php else: ?>
  <h3 class="text-center">Voucher</h3>
 <img class="text-center" width="800" src="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" alt="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" >
 <div class="row">
    <a  class="btn btn-primary text-center  col-md-6 col-md-offset-3" href="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" target="_blank" >Descargar/ver Voucher</a>    
  </div>
  <?php endif ?> 
  <?php endif ?>
    <hr>
    <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
    </div>
<script>

<?php $fechaA=date('Y-m-d') ?>
  jQuery.datetimepicker.setLocale('es');
  $('#datetimepicker3').datetimepicker({
    minDate:'<?= $fechaA ?>',
    inline:true,
    scrollInput: false,
    scrollMonth : false,
    allowTimes:['9:00','11:00','14:00','16:00']

  }); 
  $('#fechaCh').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d',
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
  });
  $('#horaCh').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','11:00','14:00','16:00']
  });


</script>

<?php


	} ?>

<?php
    if (isset($_POST['idTrama'])) {
    include_once("../../includes/init.php");

    $lista = Operacion::obtenerFrecuentes($session->idusuario);
    $idTrama=limpiar(trim($_POST['idTrama']));
    $varTI=TramaIntegracion::obtenerTramaIntegracion($idTrama,'');

    TramaIntegracion::asignarTrama($_POST['idTrama']);

    $clienteObjModal=new Cliente;

    ?>

    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>

        <style>
            .ibox-content > .sk-spinner {
                display: none;
            }

            .ibox-content.sk-loading {
                position: relative;
            }
            .ibox-content.sk-loading:after {
                content: '';
                background-color: rgba(255, 255, 255, 0.7);
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
            }
            .ibox-content.sk-loading > .sk-spinner {
                display: block;
                position: absolute;
                top: 40%;
                left: 0;
                right: 0;
                z-index: 2000;
            }
        </style>
    <div class="showOPERA">
        <div class="well" style="padding-top: 11px;padding-bottom: 10px; ">
                    <?php if ($varTI): ?>
                    <?php if (!is_null($varTI->idsuariocargo)) {
                        $usuarioObj = Usuario::obtenerUsuario($varTI->idsuariocargo);
                    } ?>

                        <div class="col-md-12 text-center"><label  class='text-success' for="">A Cargo de : <?php if(!is_null($varTI->idsuariocargo)){echo $usuarioObj->usuarioNombre.' '.$usuarioObj->usuarioApellidoPaterno;}else{ echo 'Libre';}?></label></divas>
                        </div>

                        <div class="row">
                        <div class="form-group col-md-3">
                            <label for="Observaciones" ><?= $varTI->tipo_documento?> </label>
                            <input name="dniORuc" style="border: none;" type="text" placeholder="Web" id="Observaciones" readonly class="border-bottom form-control dnioruconix"  value="<?= $varTI->num_documento ?>">

                        </div>
                        <div class="form-group col-md-5">
                            <?php $apellidos=utf8_encode($varTI->apellido_asegurado) ?>
                            <input type="hidden" name="nombres" value="<?= $varTI->nombre_asegurado ?>">
                            <input type="hidden" name="apellidos" value="<?= $apellidos ?>">
                            <input type="hidden" name="anioF" value="<?= $varTI->anio ?>">
                            <input type="hidden" id='idtrama' name="idTrama" value="<?= $idTrama ?>">
                            <label for="numeroTecnico" >Apellidos :</label>
                            <input  style="border: none;" type="hidden" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->nombre_asegurado)) ? "$apellidos"." "."$varTI->nombre_asegurado" : "$apellidos" ; ?> ">
                            <input  style="border: none;" type="text" readonly id="apeSegura" name="apeSegura" class="border-bottom form-control" required value="<?= $apellidos ?> ">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="numeroTecnico" >Nombres :</label>
                           <input  style="border: none;" type="text" readonly id="nomSegura" name="nomSegura" class="border-bottom form-control" required value="<?= $varTI->nombre_asegurado ?> ">
                        </div>
                        </div>
                        <div class="row">
                        <div class="form-group col-md-4">
                            <label for="numeroTecnico" >Dirección :</label>
                            <input  style="border: none;" type="text" readonly id="direcSegura" name="direcSegura" class="border-bottom form-control" required value="<?= $varTI->direccion ?> ">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="numeroTecnico" >Correo :</label>
                            <input  style="border: none;" type="text" readonly id="correoSegura" name="correoSegura" class="border-bottom form-control" required value="<?= strtolower($varTI->correo) ?> ">
                        </div>
                        <div class="col-md-2"><label for="estado" >Celular</label>
                            <input style="border: none;" type="text" readonly name="celular" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->celular)) ? "$varTI->celular" : "" ; ?>" >
                        </div>
                        <div class="col-md-2"><label for="estado" >Telefono</label>
                            <input style="border: none;" type="text" readonly name="telefono5" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->num_telefono)) ? "$varTI->num_telefono" : "" ; ?>">
                        </div>
                        </div>
                        <div class="row">
                        <div class="form-group col-md-4 text-danger">
                            <label for="plzdd" class=""># Póliza :</label>
                            <input  type="text" id='plzdd' readonly class="form-control border-bottom text-capitalize" required value="<?= $retVal = $varTI->policy ?>">
                        </div>
                        <div class="form-group col-md-4 text-danger">
                            <label for="mailocto11" >CORREO OCTO (CC) :</label>
                            <input type="text" id="mailocto11" readonly class=" danger border-bottom form-control" required value="pac-sftp@tracklink.pe">
                        </div>
                        <div class="col-md-4 text-danger">
                            <label for="fechInivvv">Fecha Inicio Vigencia :</label>
                            <input style="border: none;" type="text" readonly placeholder="Fecha Inicio" id="fechInivvv" class="form-control border-bottom text-capitalize" value="<?= $retVal = $varTI->fecha_inicio ?>">
                        </div>
                    </div>
                    <input type="hidden" value="<?php echo $retVal = (!empty($varTI->num_telefono)) ? "$varTI->num_telefono" : "" ; ?>" name="telefono">
                    <input type="hidden" value="" name="telefono2">
                    <input type="hidden" name="correoCliente" value="<?php echo $retVal = (!empty($varTI->correo)) ? "$varTI->correo," : "$varTI->correo" ;echo $retVal = (!empty($varTI->correo)) ? "$varTI->correo" : "" ;  ?>">

                    </div>
        <hr>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="numeroTecnico" >Placa Vehiculo*</label>
                <input  style="border: none;" readonly type="text" placeholder="Placa" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->placa)) ? "$varTI->placa" : "" ; ?>"  >
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones">Marca *</label>
                <input required <?php echo $retVal = (!empty($varTI->marca)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->marca)) ? "$varTI->marca" : "" ; ?>" >
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones">Modelo*</label>
                <input required <?php echo $retVal = (!empty($varTI->modelo)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($varTI->modelo)) ? "$varTI->modelo" : "" ; ?>">
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones" >Color</label>
                <input readonly <?php echo $retVal = (!empty($varTI->color)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Color" id="Observaciones" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->color)) ? "$varTI->color" : "" ; ?>">
            </div>

        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="Observaciones" >Chasis*</label>
                <input required style="border: none;" type="text" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = $varTI->chasis; ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="Observaciones" >Motor*</label>
                <input required <?php echo $retVal = (!empty($varTI->motor)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Motor" id="Observaciones" name="motor" class="border-bottom form-control" value="<?php echo $retVal = $varTI->motor; ?>">
            </div>
            <input type="hidden" name="motor" value="<?php echo $retVal = $varTI->motor ?>">
            <div class="form-group col-md-4">
                <label for="seguro" class="">Seleccione Compañía de Seguros</label>
                <select  readonly style="border: none;" class="border-bottom form-control text-capitalize" required name="seguro" id="seguro">
                    <option class='text-capitalize' value="<?php echo $retVal = $varTI->compania ?>" selected ><?php echo $retVal = $varTI->compania ?></option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-3">
                <label for="Observaciones" >Año*</label>
                <input required <?php echo $retVal = (!empty($varTI->anio)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Año" id="anio" name="anioF" class="border-bottom form-control" value="<?php echo $retVal = $varTI->anio; ?>">
            </div>
            <div class="col-md-3">
                <label for="plan" class="">Plan *</label>
                <select readonly style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
                        <option class="" value="<?php echo $varTI->plan ?>"  selected ><?php echo $varTI->plan ?></option>
                </select>
            </div>

            <div class="form-group col-md-6 ">
                <?php $tt = TiempoProgramServAdic::obtenerTotalHorasProgramacion2($varTI->plan,$varTI->servicio_adicional)?>
                <p style="font-weight:bold">Servicios Adicionales *: <label class="canthoraprog2 label-success"> <?= $tt->total ?> Horas </label></p>
                <div class="servAdi">
                    <?php ServiciosAdicionales::obtenerServAdicionalSeleccionadoVerIdServ($varTI->servicio_adicional) ?>
                </div>
                <input type="hidden" id="canthoraprog" name="canthoraprog" value="<?= $tt->total ?>">
            </div>
        </div><br>
        <div class="row ">
            <div class="col-md-3">
                <label for="ejecutivoCanal" >Precio lista</label>
                <input  style="border: none;"  readonly type="text" placeholder="Precio lista" id="ejecutivoCanal" name="precioLista" class="border-bottom form-control"  value="<?php echo $varTI->precio_lista?>" >
            </div>
            <div class="col-md-3">
                <label for="time" >Tiempo *</label>
                <input  style="border: none;" readonly   type="text" placeholder="Tiempo *" id="time" name="tiempo" class=" tiempo border-bottom form-control"  autocomplete="off" value="<?php echo $varTI->tiempo ?>">
            </div>

            <div class="form-group col-md-6 camapanaEsconder">
                <label for="campana" class="">¿ Campaña Pacífico Gps Smart ?</label>
                <select  style="border: none;" readonly  class="tecnico border-bottom form-control" required name="campana" id="campana">
                    <option class="" value="<?php echo $varTI->campana ?>" selected><?php echo utf8_encode($varTI->campana)?></option>
                </select>
            </div>
            <input  style="border: none;"    type="hidden" placeholder="Tiempo *" id="idEjecutvio" name="idEjecutvio" class=" tiempo border-bottom form-control"  autocomplete="off" value="<?php echo $session->idusuario ?>">
            <input  style="border: none;" readonly   type="hidden" placeholder="Tiempo *" id="tipo" name="tipo" class=" tiempo border-bottom form-control"  autocomplete="off" value="<?php echo $varTI->tipoServicio ?>">
        </div>
        <br>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="tipoCanal" class="">Seleccione canal *</label>
                <select style="border: none;"  name="tipoCanal" readonly="" class="cd-selec form-control border-bottom " required id="tipoCanal">
                    <option class='text-capitalize' value='<?php echo $varTI->canal ?>'  selected ><?php echo $varTI->canal ?></option>
                </select>
            </div>
            <div class="form-group col-md-2 a">
                <label for="nombreCanal" class="">Canal nombre</label>
                <input style="border: none;" readonly type="text" placeholder="Canal nombre" id="nombreCanal" name="nombreCanal" class="border-bottom form-control text-uppercase nombreCanal " autocomplete="off" required=""   value="<?php echo $varTI->canal_nombre ?>">
                <input style="border: none;" type="hidden" placeholder="Tienda" id="tienda" name="tienda" class="border-bottom form-control text-uppercase tienda " autocomplete="off"  value="" >
                <input style="border: none;" type="hidden" placeholder="Tienda" id="convenio" name="convenio" class="border-bottom form-control text-uppercase tienda " autocomplete="off"  value="" >
                <input style="border: none;" type="hidden" placeholder="Tienda" id="tienda" name="tienda" class="border-bottom form-control text-uppercase tienda " autocomplete="off"  value="" >
                <input style="border: none;" type="hidden" placeholder="Cobra Comision" id="comision" name="comision" class="border-bottom form-control text-uppercase ejecutivoCanalN" autocomplete="off" value="<?php echo $varTI->cobra_comision ?>" >
            </div>
            <div class="form-group col-md-4">
                <label for="ejecutivoCanalN" class="">Ejecutivo canal</label>
                <input style="border: none;"  readonly type="text" placeholder="Ejecutivo canal" id="ejecutivoCanalN" name="ejecutivoCanalN" class="border-bottom form-control text-uppercase ejecutivoCanalN" autocomplete="off" value="<?php echo $varTI->ejecutivo_canal ?>" >
            </div>
        </div>
        <div class="row">
            <h3 class="col-md-6">Datos de facturación </h3>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="Forma*" >Forma Pago</label>
                <select style="border: none;" id="formaPago" name="formaPago" class="form-control border-bottom" required  readonly >
                    <option value="<?php echo $varTI->forma_pago ?>" selected><?php echo $varTI->forma_pago ?></option>
                </select>
            </div>
            <div class="col-md-4">
                <label for="Forma*" >Documento Pago</label>
                <select style="border: none;" id="selectNa" name="tipoDocumentoFacturacion" class="cd-selec form-control border-bottom" required readonly >
                    <option value="<?php echo $varTI->doc_factura ?>" selected><?php echo $varTI->doc_factura ?></option>
                </select>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-8">
                <div class=" eFactura" style="">
                    <div class="row"><br>
                        <div class="form-group col-md-3">
                            <label for="rucFacturacion" class="">Ruc *</label>
                            <input readonly style="border: none;" type="number" placeholder="Ruc *" id="rucFacturacion" name="rucFacturacion" class="form-control x2 border-bottom" maxlength="12" minlength="10" value="<?= $retVal = $varTI->ruc_factura; ?>"  >
                        </div>
                        <div class="form-group col-md-7">
                            <label for="razonSocialFacturacion" class="">Razón Social *</label>
                            <input readonly style="border: none;" type="text" placeholder="Razón Social *" id="razonSocialFacturacion" name="razonSocialFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = $varTI->razonSocial_Factura; ?>" >
                        </div>
                        <div class="form-group col-md-2">
                            <div class="checkbox checkbox-danger">
                                <input id="nreasignacion" type="checkbox" name="nreasignacion" value="si">
                                <label for="nreasignacion" style="color:#da2626">
                                    <strong>REASIGNACIÓN</strong>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-8">
                            <label for="direccionFiscalFacturacion" class="sr-only">Dirección Fiscal *</label>
                            <input readonly style="border: none;" type="text" placeholder="Dirección Fiscal *" id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="form-control border-bottom text-capitalize" value="<?= $varTI->direccion_factura ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="telefonoFacturacion" class="sr-only">Teléfono</label>
                            <input style="border: none;" readonly type="text" placeholder="Teléfono" id="telefonoFacturacion" name="telefonoFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = $varTI->numero_fatura ?>">

                            <input style="border: none;" type="hidden" placeholder="Dirección de engrega" id="direccionEntrega" name="direccionEntrega" class="form-control border-bottom text-capitalize" value="">
                            <input style="border: none;" type="hidden" placeholder="Referencia de engrega" id="referenciaEntrega" name="referenciaEntrega" class="form-control border-bottom text-capitalize" required="" value="">
                            <input style="border: none;" type="hidden" placeholder="Distrito de entrega" id="distritoEntrega" name="distritoEntrega" class="form-control border-bottom text-capitalize" required autocomplete="off" value="">
                            <input style="border: none;" type="hidden" placeholder="Horario de entrega" id="horarioEntrega" name="horarioEntrega" class="form-control border-bottom text-capitalize" autocomplete="off" required value="">
                            <input style="border: none;" type="hidden" placeholder="Persona de recepción de factura" id="personaEntrega" name="personaEntrega" class="form-control border-bottom text-capitalize" required value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                    <label for="fechIni" class="text-success">Fecha Inicio Vigencia :</label>
                    <input style="border: none;" type="text" readonly placeholder="Fecha Inicio" id="fechIni" name="fechIni" class="form-control border-bottom text-capitalize" required value="<?= $retVal = $varTI->fecha_inicio ?>">
            </div>
            <div class="col-md-4">
                    <label for="fechFin" class="text-success">Fecha Fin Vigencia :</label>
                    <input style="border: none;" type="text" readonly placeholder="Fecha Fin" id="fechFin" name="fechFin" class="form-control border-bottom text-capitalize" required value="<?= $retVal = $varTI->fecha_fin ?>">
            </div>
            <div class="col-md-4">
                <label for="Poliza" class="text-success"># Póliza :</label>
                <input style="border: none;" type="text" readonly placeholder="Poliza" id="Poliza" name="Poliza" class="form-control border-bottom text-capitalize" required value="<?= $retVal = $varTI->policy ?>">
            </div>
        </div>
        <br>
        <div class="showTipo">
            <div class="row">
                <h3 class="col-md-6">Datos de instalación</h3>
                <div class="col-md-6">
                    <label for="pendienteInstacion">Pendiente datos de instalación</label>
                    <div class="switch">
                        <div class="onoffswitch">
                            <input type="checkbox"  class="onoffswitch-checkbox" id="pendienteInstacion" value="si" name="pendienteInstacion">
                            <label class="onoffswitch-label" for="pendienteInstacion">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tablist">
                <input type="radio" title="taller" name="lugarInstacion" class='lugarDeInstalacion' id="taller" role="tab" aria-controls="taller-panel" checked value="Taller" class="lugarDeInstalacion tall" />
                <input type="radio" title="domicilio" name="lugarInstacion"  class='lugarDeInstalacion' id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente" class="lugarDeInstalacion domm" />
                <input type="radio" title="concesionario" name="lugarInstacion" class='lugarDeInstalacion'  id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario" class="lugarDeInstalacion conn" >

                <div class="tabs" aria-hidden="true">
                    <label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
                    <label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
                    <label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
                </div>
                <div class="panels">
                    <ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">
                        <h2><?= $lista->lugarInstalacion ?> | <?= $lista->observacion ?></h2>
                        <p><?= $lista->direccionInstacion  ?></p>
                        <p>
                                  <?php $dep=$clienteObjModal->obtenerDepartamentoPorId($lista->departamentoi) ?><?= $dep->departamento ?>
                          &nbsp;|&nbsp;
                                  <?php $prov=$clienteObjModal->otenerProvinciaPorId($lista->provinciai) ?><?php echo $prov->provincia ?>
                          &nbsp;|&nbsp;
                                  <?php $di=$clienteObjModal->obtenerDistritoPorId($lista->distritoi) ?><?= $di->distrito ?>
                                </p>

                        <input type="hidden" name="direccionTaller" value="<?= $lista->direccionInstacion  ?>">
                        <input type="hidden" name="disT" id="distritoTallerid" value="<?= $lista->distritoi  ?>">
                        <input type="hidden" name="proT" value="<?= $lista->provinciai  ?>">
                        <input type="hidden" name="depT" value="<?= $lista->departamentoi  ?>">

                        <div class="row">
                            <div class="row">
                                <div class="row" >
                                    <div class="form-group col-md-4">
                                        <label  class="">Fecha</label>
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control tal tala fecha"  id="fechaIT" name="fechaIT" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="turno" class="">Turno a</label>
                                        <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn form-control border-bottom btn btn-info" >Seleccionar hora</button>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posT" class="hora form-control border-bottom"  required="" readonly>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRT" class="delR form-control border-bottom"  required="" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="horaS" class="">Hora seleccionada</label>
                                        <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaT" class="horaVer tala tal form-control border-bottom"  required="" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab">
                        <h2>Domicilio Cliente</h2>
                        <div class="row">
                            <div class="row">
                                <div class="form-group  col-md-4">
                                    <label for="departamentoInstalacionD" class="">Departamento*</label>
                                    <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize dom domo" required name="departamentoInstalacionD" id="departamentoInstalacionD">
                                        <option class="text-capitalize" value="" disabled selected >Seleccione Departamento* </option>
                                        <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                                        <?php if ($objDepartamento): ?>
                                            <?php foreach ($objDepartamento as $var): ?>
                                                <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                                            <?php endforeach ?>
                                        <?php else: ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="provinciaInstalacionD" class="">Provincia*</label>
                                    <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize dom domo" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="distritoInstalacionD" class="">Distrito*</label>
                                    <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize dom domo" required name="distritoInstalacionD"  id="distritoInstalacionD" >
                                    </select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group  col-md-12">
                                    <label for="direccionInstalacionD" class="sr-only">Direccion*</label>
                                    <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionD" name="direccionInstalacionD"  class="form-control border-bottom dom domo" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for="referenciaInstalacionD" class="sr-only">Referencia *</label>
                                    <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" required="" name="referenciaInstalacionD" class="form-control border-bottom dom domo"  >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="cod" class="sr-only">Cordenadas Domicilio *</label>
                                    <input style="border: none;" type="text" placeholder="Cordenadas Domicilio *" id="cod" name="cordenadasD" class="form-control x2 text-capitalize border-bottom dom domo" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label  class="">Fecha</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control dom domo fecha"  id="fechaID" name="fechaID" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="turno" class="">Turno b</label>
                                    <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Seleccionar hora</button>
                                    <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posD" class="hora form-control border-bottom"  required="" readonly>
                                    <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRD" class="delR form-control border-bottom"  required="" readonly>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="horaS" class="">Hora seleccionada</label>
                                    <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaD" class="horaVer domo dom  form-control border-bottom"  required="" readonly>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">

                        <div class="row">
                            <div class="col-md-6"><h2>Concesionario</h2></div>
                            <div class="col-md-6">
                                <label for="concesionarioInstalacion" class="sr-only">Nombre de concesionario*</label>
                                <input style="border: none;" type="text" placeholder="Nombre de concesionario*" id="concesionarioInstalacion" name="concesionarioInstalacion" class="form-control border-bottom con" required="" >
                            </div>
                        </div>

                        <div class="row">
                            <div class="row">
                                <div class="form-group  col-md-4">
                                    <label for="departamentoInstalacionC" class="">Departamento*</label>
                                    <select  style="border: none;" class="departamentoInstalacionC border-bottom form-control text-capitalize con cono" required name="departamentoInstalacionC" id="departamentoInstalacionC">
                                        <option class="text-capitalize" disabled selected >Seleccione Departamento</option>
                                        <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                                        <?php if ($objDepartamento): ?>
                                            <?php foreach ($objDepartamento as $var): ?>
                                                <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                                            <?php endforeach ?>
                                        <?php else: ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="provinciaInstalacionC" class="">Provincia*</label>
                                    <select  style="border: none;" class="provinciaInstalacionC border-bottom form-control text-capitalize con cono" required name="provinciaInstalacionC" id="provinciaInstalacionC" >
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="distritoInstalacionC" class="">Distrito*</label>
                                    <select  style="border: none;" class="distritoInstalacionC border-bottom form-control text-capitalize con cono" required name="distritoInstalacionC"  id="distritoInstalacionC" > 		</select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for="direccionInstalacionC" class="">Direccion*</label>
                                    <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom con cono" >
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for="referenciaInstalacionC" class="">Referencia *</label>
                                    <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom con cono" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label  class="">Fecha</label>
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control con cono fecha"  id="fechaIC" name="fechaIC" autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="turno" class="">Turno c</label>
                                        <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Seleccionar hora</button>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posC" class="hora form-control border-bottom"  required="" readonly>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRC" class="delR form-control border-bottom"  required="" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="horaS" class="">Hora seleccionada</label>
                                        <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaC" class="horaVer cono con form-control border-bottom"  required="" readonly>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </ul>
                </div>
            </div>
            <div class="ibox" id="ibox1">
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-double-bounce">
                        <div class="sk-double-bounce1"></div>
                        <div class="sk-double-bounce2"></div>
                    </div>
                    <div id="programacionHtml">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="nombreContacto" class="">Nombre Contacto *</label>
                    <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContacto" name="nombreContacto" class="form-control x2 text-capitalize border-bottom " required="" value="<?= $varTI->contacto_i ?>" >
                </div>
                <div class="form-group col-md-6">
                    <label for="numeroContacto" class="">Número Contacto *</label>
                    <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContacto" name="numeroContacto" class="form-control x2 text-capitalize border-bottom" required="" value="<?= $varTI->numero_i ?>">
                </div>
            </div>

            <hr>
            <div class="row">

                <div class="form-group col-md-12">

                    <label for="seguimientoTrama" class="">Seguimiento</label>
<div class="input-group">
                <input type="text" class="form-control" placeholder="Seguimiento" id="seguimientoTrama" >
                <span class="input-group-btn">
                    <button type="button" id="registrarSeguiminto" class="btn btn-primary">Registrar Seguimiento </button>
                </span>
            </div>
                </div>
                
            </div>
            
            <ul id='segimientohtml'>
            <?php foreach(TramaIntegracion::getSEg($idTrama) as $var ): ?>
                <li><?= $var->fecha ?> <b><?= $var->body ?></b></li>
            <?php endforeach ?>
            </ul>
            <hr>
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="observaciong" class="">Observaciones </label>
                    <input style="border: none;" type="text" placeholder="Observaciones " id="observaciong" name="observaciong" class="form-control x2 border-bottom focusUrgenteta" >
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
            <button type="submit" name="crearDesdeChasis" id="agregarVehiculoBttn" class="btn btn-primary" value="<?php echo Token::generar() ?>">Confirmar</button>
        </div>
    </div>
    <script src="../../assets/js/registrarme.js"></script>
    <script src="../../assets/js/validacionFactura.pabel.js"></script>
    <script src="../../assets/bootbox/bootbox.all.js"></script>
    <script>

        function validar(){
            var lugarI=$( "input[name='lugarInstacion']:checked" ).val()
            if (lugarI=='Taller') {
                $('.con').prop('disabled', true);
                $('.dom').prop('disabled', true);
                $('.tal').prop('disabled', false);
            }else if (lugarI=='Domicilio Cliente') {
                $('.con').prop('disabled', true);
                $('.dom').prop('disabled', false);
                $('.tal').prop('disabled', true);
            }else if (lugarI=='Concesionario') {
                $('.con').prop('disabled', false);
                $('.dom').prop('disabled', true);
                $('.tal').prop('disabled', true);
            }
        }

        $(document).ready(function(){
            $( "input[name='lugarInstacion']" ).on( "click",function(){
                $('#programacionHtml').html('');
                $('.horaVer').val('');
                $('.fecha').val('');
                validar();
            });
            validar();
        });


    </script>
    <script>
        $(".departamentoInstalacionD").change(function(){provincias4();});
        $(".departamentoInstalacionC").change(function(){provincias5();});
        $(".provinciaInstalacionD").change(function(){distritos4();});
        $(".provinciaInstalacionC").change(function(){distritos5();});
        function provincias4(){
            var iddepartamento = $(".departamentoInstalacionD").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/provincia',
                data :  'iddepartamento='+ iddepartamento,
                success : function(data)
                {
                    $(".provinciaInstalacionD").html(data);
                }
            });
            return false;
        }
        function provincias5(){
            var iddepartamento = $(".departamentoInstalacionC").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/provincia',
                data :  'iddepartamento='+ iddepartamento,
                success : function(data)
                {
                    $(".provinciaInstalacionC").html(data);
                }
            });
            return false;
        }

        function distritos4(){
            var idprovincia = $(".provinciaInstalacionD").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/distrito',
                data :  'idprovincia='+ idprovincia,
                success : function(data)
                {
                    $(".distritoInstalacionD").html(data)
                }
            });
            return false;
        }
        function distritos5(){
            var idprovincia = $(".provinciaInstalacionC").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/distrito',
                data :  'idprovincia='+ idprovincia,
                success : function(data)
                {
                    $(".distritoInstalacionC").html(data)
                }
            });
            return false;
        }



        $('.lugarDeInstalacion').on("change",function(){
            var pendienteInstacion=document.getElementById("pendienteInstacion");
            if (pendienteInstacion.checked) {
                $('.cono').prop('disabled', true);
                $('.domo').prop('disabled', true);
                $('.tala').prop('disabled', true);
                $("#programacionHtml").html('');
                limpiarCapacidad();
            }else{
                $('.cono').prop('disabled', false);
                $('.domo').prop('disabled', false);
                $('.tala').prop('disabled', false);
            }
        })


        $('#pendienteInstacion').on("change",function(){
            var pendienteInstacion=document.getElementById("pendienteInstacion");
            if (pendienteInstacion.checked) {
                $('.cono').prop('disabled', true);
                $('.domo').prop('disabled', true);
                $('.tala').prop('disabled', true);
                $("#programacionHtml").html('');
                limpiarCapacidad();
            }else{
                $('.cono').prop('disabled', false);
                $('.domo').prop('disabled', false);
                $('.tala').prop('disabled', false);
            }
        });

        function validaDP() {
            // var dp=1;
            var lugarI = $("input[name='lugarInstacion']:checked").val();
            if (lugarI == 'Taller') {
                var fecha = $('#fechaIT').val();
            } else if (lugarI == 'Domicilio Cliente') {
                var fecha = $('#fechaID').val();
            } else if (lugarI == 'Concesionario') {
                var fecha = $('#fechaIC').val();
            }
            var hora = $(".horaVer").val();
            var canthoraprog = $('#canthoraprog').val();
            var idtec = $('input[name="posT"]').val();
            // console.log("fecha: "+fecha+" idtec:"+idtec+" canhora:"+canthoraprog.trim());
            $.ajax({
                type: 'POST',
                url: 'ajax/validarRegistroTB',
                data:'tipo=save&fecha='+fecha+'&idtec='+idtec+"&canthoraprog=" + canthoraprog.trim() + "&hora=" + hora,
                // data :  'tipo=save&fecha=2019-11-27&idtec=2&canthoraprog=2&hora=12:00',
                success: function (data) {
                    if (data.trim() == 'dp') {
                        //dp='1';
                        $("#agregarVehiculoBttn").prop("disabled",true);
                        //      clearInterval(eventos.interval);
                        $("#programacionHtml").html('');
                        $(".horabtn").html('Seleccionar hora');
                        $(".horaVer").val('');
                        // fn(dp);
                        bootbox.alert({
                            title: "<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                            message: "<label>El horario Seleccionado("+hora+") ya fue Registrado,Por Favor Seleccione otro*</label>",
                            size: 'small',
                            callback: function () {
                                /*  l.ladda( 'stop' );*/
                            }
                        });
                    }else if(data.trim()=='Ok'){
                        $("#agregarVehiculoBttn").prop("disabled",false)
                    }
                }
            });
        }

        var eventos={};
        eventos.frejcar =function(){
            validaDP();
        };
        eventos.interval = setInterval(eventos.frejcar, 3000);

        $(document).ready(function () {

            $('#agregarVehiculoBttn').click(function() {

                checked = $('[name="servad[]"]:checked').length;

                if(checked==0) {
                    bootbox.alert({
                        title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                        message: "<label>Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*</label>",
                        size: 'small',
                        callback: function () {
                          /*  l.ladda( 'stop' );*/
                        }
                    });
                   /* alert("Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*");*/
                    console.log("Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*");
                    // $('[name="servad[]"]:checked')[0].focus();
                    return false;
                }

            });
        });
    </script>
    <script>

        <?php $fechaA=date('Y-m-d') ?>
        jQuery.datetimepicker.setLocale('es');

        $('.fecha').datetimepicker({
            minDate:'<?= $fechaA ?>',
            format:'Y-m-d',
            scrollInput: false,
            scrollMonth : false,
            timepicker:false
        });


        $('#fechaEngrega').datetimepicker({
            minDate:'<?= $fechaA ?>',
            format:'Y/m/d',
            scrollInput: false,
            scrollMonth : false,
            timepicker:false
        });
        $('#horaEntrega').datetimepicker({
            datepicker:false,
            format:'H:i',
            allowTimes:['9:00','11:00','14:00','16:00']
        });

        $('.tiempo').typeahead({
            source: [
                {"name": "1 año"},
                {"name": "2 años"},
                {"name": "3 años"},
                {"name": "4 años"},
                {"name": "5 años"},
                {"name": "6 años"},
                {"name": "8 años"},
                {"name": "7 años"}
            ]
        });

        <?php if ($datos=DatosFacturacion::horariosSugeridosParaEntregaDeFactura()): ?>
        $('#horarioEntrega').typeahead({
            source: [
                <?php foreach ($datos as $var): ?>
                {"name": "<?php echo $var->fecha ?>"},
                <?php endforeach ?>
            ]
        });
        <?php endif ?>

        <?php if ($datosD=Cliente::otenerTodoLosDistritos()): ?>
        $('#distritoEntrega').typeahead({
            source: [
                <?php foreach ($datosD as $var): ?>
                {"name": "<?php echo $var->distrito ?>"},
                <?php endforeach ?>
            ]
        });
        <?php endif ?>

        <?php if ($datosDopeCon=Operacion::obtenerConcesionarioInstalacion()): ?>
        $('#concesionarioInstalacion').typeahead({
            source: [
                <?php foreach ($datosDopeCon as $var): ?>
                {"name": "<?php echo $var->nombreConcecionarioInstalicion ?>"},
                <?php endforeach ?>
            ]
        });
        <?php endif ?>

        $("#registrarSeguiminto").click(function(){
            // alert(servAd) ;seguimientoTrama
            var body=$('#seguimientoTrama').val();
            var idtrama = $("#idtrama").val();
            var today = new Date();
            var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()+' '+today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            if(body.length>0){
                $.ajax({
                    type : 'POST',
                    url  : 'ajax/registrarSeg',
                    data : {body:body,idtrama:idtrama},
                    success : function(data)
                    {
                        $('#seguimientoTrama').val('');
                        $("#segimientohtml").append('<li> '+date+' <b>'+body+'</b></li>');
                        //segimientohtml
                    }
                });
             }
            return false;
        });

        function lstTProg(){
            var lugarI=$("input[name='lugarInstacion']:checked" ).val();
            var distrito="0";
            var tthinstala=$('#canthoraprog').val();
            var cita = 'General';
            var concecion="0";
            var lugarT='';

            if (lugarI=='Taller') {
                var fecha = $('#fechaIT').val();
                distrito=$("#distritoTallerid").val()

                console.log($("#distritoTallerid").val())
                lugarT='Taller';
            }else if (lugarI=='Domicilio Cliente') {
                var fecha = $('#fechaID').val();
                distrito=$('#distritoInstalacionD').val();
                lugarT='Domicilio';
            }else if (lugarI=='Concesionario') {
                var fecha = $('#fechaIC').val();
                var concecion=$('#concesionarioInstalacion').val();
                var cita = concecion;
                distrito=$('#distritoInstalacionC').val();
                lugarT='Concesionario';
            }
            var cod = document.getElementById("plan").value;
            servAd = $('[name="servad[]"]:checked').length;
            if (fecha!='' && cod!='' && servAd!=0 &&(distrito==0 || distrito!=null) && concecion!='') {
                $(".horabtn").off( "click" );
                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                $.ajax({
                    type : 'POST',
                    url  : '../includes/tbProgramacion',
                    data : {fecha:fecha,cita:cita,tthinstala:tthinstala,iddist:distrito,lugart:lugarT},
                    success : function(data)
                    {
                        $("#programacionHtml").html(data);
                        $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                        $(".horabtn").on("click",lstTProg).find(".horabtn");
                    }
                });
            }else{
                $("#programacionHtml").html('<h3 class="text-center text-info">Primero debe seleccionar Plan,Servicios Adicionales,fecha y/o Distrito y/o Nombre Concesionario.</h3>');
            }
            return false;
        }

        $(".horabtn").click(function(){
            lstTProg();
        });

        $('.fecha').change(function(){
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');
        });
        $("#concesionarioInstalacion").blur(function(){
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');
        });


        $('#departamentoInstalacionD,#departamentoInstalacionC,#provinciaInstalacionD,#provinciaInstalacionC,#distritoInstalacionD,#distritoInstalacionC').change(function(){
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');
        });

    </script>

<?php endif ?>

<?php
}
?>


<?php
if (isset($_POST['idTramaf'])) {
    include_once("../../includes/init.php");

    $lista = Operacion::obtenerFrecuentes($session->idusuario);
    $idTrama=limpiar(trim($_POST['idTramaf']));
    $varTI=TramaIntegracionPF::obtenerTramaIntegracion($idTrama,'');

    TramaIntegracionPF::asignarTrama($_POST['idTramaf']);

    $clienteObjModal=new Cliente;

    ?>

    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>

    <style>
        .ibox-content > .sk-spinner {
            display: none;
        }

        .ibox-content.sk-loading {
            position: relative;
        }
        .ibox-content.sk-loading:after {
            content: '';
            background-color: rgba(255, 255, 255, 0.7);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        .ibox-content.sk-loading > .sk-spinner {
            display: block;
            position: absolute;
            top: 40%;
            left: 0;
            right: 0;
            z-index: 2000;
        }
    </style>
    <div class="showOPERA">
        <div class="well" style="padding-top: 11px;padding-bottom: 10px; ">
            <?php if ($varTI): ?>
            <?php if (!is_null($varTI->idsuariocargo)) {
                $usuarioObj = Usuario::obtenerUsuario($varTI->idsuariocargo);
            } ?>

            <div class="col-md-12 text-center"><label  class='text-success' for="">A Cargo de : <?php if(!is_null($varTI->idsuariocargo)){echo $usuarioObj->usuarioNombre.' '.$usuarioObj->usuarioApellidoPaterno;}else{ echo 'Libre';}?></label></divas>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="Observaciones" >N° Documento</label>
                    <input name="dniORuc" style="border: none;" type="text" placeholder="Web" id="Observaciones" readonly class="border-bottom form-control dnioruconix"  value="<?= $varTI->num_documento ?>">

                </div>
                <div class="form-group col-md-5">
                    <?php $apellidos=utf8_encode($varTI->apellido_asegurado) ?>
                    <input type="hidden" name="nombres" value="<?= $varTI->nombre_asegurado ?>">
                    <input type="hidden" name="apellidos" value="<?= $apellidos ?>">
                    <input type="hidden" name="anioF" value="<?= $varTI->anio ?>">
                    <input type="hidden" id='idtramaf' name="idTramaf" value="<?= $idTrama ?>">
                    <label for="numeroTecnico" >Apellidos :</label>
                    <input  style="border: none;" type="hidden" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->nombre_asegurado)) ? "$apellidos"." "."$varTI->nombre_asegurado" : "$apellidos" ; ?> ">
                    <input  style="border: none;" type="text" readonly id="apeSegura" name="apeSegura" class="border-bottom form-control" required value="<?= $apellidos ?> ">

                </div>
                <div class="form-group col-md-4">
                    <label for="numeroTecnico" >Nombres :</label>
                    <input  style="border: none;" type="text" readonly id="nomSegura" name="nomSegura" class="border-bottom form-control" required value="<?= $varTI->nombre_asegurado ?> ">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="numeroTecnico" >Dirección :</label>
                    <input  style="border: none;" type="text" readonly id="direcSegura" name="direcSegura" class="border-bottom form-control" required value="<?= $varTI->direccion ?> ">
                </div>
                <div class="form-group col-md-4">
                    <label for="numeroTecnico" >Correo :</label>
                    <input  style="border: none;" type="text" readonly id="correoSegura" name="correoSegura" class="border-bottom form-control" required value="<?= strtolower($varTI->correo) ?> ">
                </div>
                <div class="col-md-2"><label for="estado" >Celular</label>
                    <input style="border: none;" type="text" readonly name="celular" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->celular)) ? "$varTI->celular" : "" ; ?>" >
                </div>
                <div class="col-md-2"><label for="estado" >Telefono</label>
                    <input style="border: none;" type="text" readonly name="telefono5" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->num_telefono)) ? "$varTI->num_telefono" : "" ; ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4 text-danger">
                    <label for="plzdd" class=""># Póliza :</label>
                    <input  type="text" id='plzdd' readonly class="form-control border-bottom text-capitalize" required value="<?= $retVal = $varTI->policy ?>">
                </div>
                <div class="form-group col-md-4 text-danger">
                    <label for="mailocto11" >CORREO OCTO (CC) :</label>
                    <input type="text" id="mailocto11" readonly class=" danger border-bottom form-control" required value="pac-sftp@tracklink.pe">
                </div>
                <div class="col-md-4 text-danger">
                    <label for="fechInivvv">Fecha Inicio Vigencia :</label>
                    <input style="border: none;" type="text" readonly placeholder="Fecha Inicio" id="fechInivvv" class="form-control border-bottom text-capitalize" value="<?= $retVal = $varTI->fecha_vigencia ?>">
                </div>
            </div>
            <input type="hidden" value="<?php echo $retVal = (!empty($varTI->num_telefono)) ? "$varTI->num_telefono" : "" ; ?>" name="telefono">
            <input type="hidden" value="" name="telefono2">
            <input type="hidden" name="correoCliente" value="<?php echo $retVal = (!empty($varTI->correo)) ? "$varTI->correo," : "$varTI->correo" ;echo $retVal = (!empty($varTI->correo)) ? "$varTI->correo" : "" ;  ?>">

        </div>
        <hr>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="numeroTecnico" >Placa Vehiculo*</label>
                <input  style="border: none;"  type="text" placeholder="Placa" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->placa)) ? "$varTI->placa" : "" ; ?>"  >
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones">Marca *</label>
                <input required <?php echo $retVal = (!empty($varTI->marca)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->marca)) ? "$varTI->marca" : "" ; ?>" >
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones">Modelo*</label>
                <input required <?php echo $retVal = (!empty($varTI->modelo)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($varTI->modelo)) ? "$varTI->modelo" : "" ; ?>">
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones" >Color</label>
                <input readonly <?php echo $retVal = (!empty($varTI->color)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Color" id="Observaciones" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->color)) ? "$varTI->color" : "" ; ?>">
            </div>

        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="Observaciones" >Chasis*</label>
                <input required style="border: none;" type="text" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = $varTI->chasis; ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="Observaciones" >Motor*</label>
                <input required <?php echo $retVal = (!empty($varTI->motor)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Motor" id="Observaciones" name="motor" class="border-bottom form-control" value="<?php echo $retVal = $varTI->motor; ?>">
            </div>
            <input type="hidden" name="motor" value="<?php echo $retVal = $varTI->motor ?>">
            <div class="form-group col-md-4">
                <label for="seguro" class="">Seleccione Compañía de Seguros</label>
                <select  readonly style="border: none;" class="border-bottom form-control text-capitalize" required name="seguro" id="seguro">
                    <option class='text-capitalize' value="<?php echo $retVal = $varTI->compania ?>" selected ><?php echo $retVal = $varTI->compania ?></option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-3">
                <label for="Observaciones" >Año*</label>
                <input required <?php echo $retVal = (!empty($varTI->anio)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Año" id="anio" name="anioF" class="border-bottom form-control" value="<?php echo $retVal = $varTI->anio; ?>">
            </div>
            <div class="col-md-3">
                <label for="plan" class="">Plan *</label>
                <select readonly style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
                    <option class="" value="<?php echo $varTI->plan ?>"  selected ><?php echo $varTI->plan ?></option>
                </select>
            </div>

            <div class="form-group col-md-6 ">
                <?php $tt = TiempoProgramServAdic::obtenerTotalHorasProgramacion2($varTI->plan,$varTI->servicio_adicional)?>
                <p style="font-weight:bold">Servicios Adicionales *: <label class="canthoraprog2 label-success"> <?= $tt->total ?> Horas </label></p>
                <div class="servAdi">
                    <?php ServiciosAdicionales::obtenerServAdicionalSeleccionadoVerIdServ($varTI->servicio_adicional) ?>
                </div>
                <input type="hidden" id="canthoraprog" name="canthoraprog" value="<?= $tt->total ?>">
            </div>
        </div><br>
        <div class="row ">
            <div class="col-md-3">
                <label for="ejecutivoCanal" >Precio lista</label>
                <input  style="border: none;"  readonly type="text" placeholder="Precio lista" id="ejecutivoCanal" name="precioLista" class="border-bottom form-control"  value="<?php echo $varTI->precio_lista?>" >
            </div>
            <div class="col-md-3">
                <label for="time" >Tiempo *</label>
                <input  style="border: none;" readonly   type="text" placeholder="Tiempo *" id="time" name="tiempo" class=" tiempo border-bottom form-control"  autocomplete="off" value="<?php echo $varTI->tiempo ?>">
            </div>

            <div class="form-group col-md-6 camapanaEsconder">
                <label for="campana" class="">¿ Campaña Pacífico Gps Smart ?</label>
                <select  style="border: none;" readonly  class="tecnico border-bottom form-control" required name="campana" id="campana">
                    <option class="" value="<?php echo $varTI->campana ?>" selected><?php echo utf8_encode($varTI->campana)?></option>
                </select>
            </div>
            <input  style="border: none;"    type="hidden" placeholder="Tiempo *" id="idEjecutvio" name="idEjecutvio" class=" tiempo border-bottom form-control"  autocomplete="off" value="<?php echo $session->idusuario ?>">
            <input  style="border: none;" readonly   type="hidden" placeholder="Tiempo *" id="tipo" name="tipo" class=" tiempo border-bottom form-control"  autocomplete="off" value="Instalacion">
        </div>
        <br>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="tipoCanal" class="">Seleccione canal *</label>
                <select style="border: none;"  name="tipoCanal" readonly="" class="cd-selec form-control border-bottom " required id="tipoCanal">
                    <option class='text-capitalize' value='<?php echo $varTI->canal ?>'  selected ><?php echo $varTI->canal ?></option>
                </select>
            </div>
            <div class="form-group col-md-2 a">
                <label for="nombreCanal" class="">Canal nombre</label>
                <input style="border: none;" readonly type="text" placeholder="Canal nombre" id="nombreCanal" name="nombreCanal" class="border-bottom form-control text-uppercase nombreCanal " autocomplete="off" required=""   value="<?php echo $varTI->canal_nombre ?>">
                <input style="border: none;" type="hidden" placeholder="Tienda" id="tienda" name="tienda" class="border-bottom form-control text-uppercase tienda " autocomplete="off"  value="" >
                <input style="border: none;" type="hidden" placeholder="Tienda" id="convenio" name="convenio" class="border-bottom form-control text-uppercase tienda " autocomplete="off"  value="" >
                <input style="border: none;" type="hidden" placeholder="Tienda" id="tienda" name="tienda" class="border-bottom form-control text-uppercase tienda " autocomplete="off"  value="" >
                <input style="border: none;" type="hidden" placeholder="Cobra Comision" id="comision" name="comision" class="border-bottom form-control text-uppercase ejecutivoCanalN" autocomplete="off" value="<?php echo $varTI->cobra_comision ?>" >
            </div>
            <div class="form-group col-md-4">
                <label for="ejecutivoCanalN" class="">Ejecutivo canal</label>
                <input style="border: none;"  readonly type="text" placeholder="Ejecutivo canal" id="ejecutivoCanalN" name="ejecutivoCanalN" class="border-bottom form-control text-uppercase ejecutivoCanalN" autocomplete="off" value="<?php echo $varTI->ejecutivo_canal ?>" >
            </div>
        </div>
        <div class="row">
            <h3 class="col-md-6">Datos de facturación </h3>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="Forma*" >Forma Pago</label>
                <select style="border: none;" id="formaPago" name="formaPago" class="form-control border-bottom" required  readonly >
                    <option value="<?php echo $varTI->forma_pago ?>" selected><?php echo $varTI->forma_pago ?></option>
                </select>
            </div>
            <div class="col-md-4">
                <label for="Forma*" >Documento Pago</label>
                <select style="border: none;" id="selectNa" name="tipoDocumentoFacturacion" class="cd-selec form-control border-bottom" required readonly >
                    <option value="<?php echo $varTI->doc_factura ?>" selected><?php echo $varTI->doc_factura ?></option>
                </select>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-8">
                <div class=" eFactura" style="">
                    <div class="row"><br>
                        <div class="form-group col-md-4">
                            <label for="rucFacturacion" class="">Ruc *</label>
                            <input readonly style="border: none;" type="number" placeholder="Ruc *" id="rucFacturacion" name="rucFacturacion" class="form-control x2 border-bottom" maxlength="12" minlength="10" value="<?= $retVal = $varTI->ruc_factura; ?>"  >
                        </div>
                        <div class="form-group col-md-8">
                            <label for="razonSocialFacturacion" class="">Razón Social *</label>
                            <input readonly style="border: none;" type="text" placeholder="Razón Social *" id="razonSocialFacturacion" name="razonSocialFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = $varTI->razonSocial_Factura; ?>" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-8">
                            <label for="direccionFiscalFacturacion" class="sr-only">Dirección Fiscal *</label>
                            <input readonly style="border: none;" type="text" placeholder="Dirección Fiscal *" id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="form-control border-bottom text-capitalize" value="<?= $varTI->direccion_factura ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="telefonoFacturacion" class="sr-only">Teléfono</label>
                            <input style="border: none;" readonly type="text" placeholder="Teléfono" id="telefonoFacturacion" name="telefonoFacturacion" class="form-control border-bottom text-capitalize" value="">

                            <input style="border: none;" type="hidden" placeholder="Dirección de engrega" id="direccionEntrega" name="direccionEntrega" class="form-control border-bottom text-capitalize" value="">
                            <input style="border: none;" type="hidden" placeholder="Referencia de engrega" id="referenciaEntrega" name="referenciaEntrega" class="form-control border-bottom text-capitalize" required="" value="">
                            <input style="border: none;" type="hidden" placeholder="Distrito de entrega" id="distritoEntrega" name="distritoEntrega" class="form-control border-bottom text-capitalize" required autocomplete="off" value="">
                            <input style="border: none;" type="hidden" placeholder="Horario de entrega" id="horarioEntrega" name="horarioEntrega" class="form-control border-bottom text-capitalize" autocomplete="off" required value="">
                            <input style="border: none;" type="hidden" placeholder="Persona de recepción de factura" id="personaEntrega" name="personaEntrega" class="form-control border-bottom text-capitalize" required value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="fechIni" class="text-success">Fecha Inicio Vigencia :</label>
                <input style="border: none;" type="text" readonly placeholder="Fecha Inicio" id="fechIni" name="fechIni" class="form-control border-bottom text-capitalize" required value="<?= $retVal = $varTI->fecha_vigencia ?>">
            </div>
            <div class="col-md-4">
                <label for="Poliza" class="text-success"># Póliza :</label>
                <input style="border: none;" type="text" readonly placeholder="Poliza" id="Poliza" name="Poliza" class="form-control border-bottom text-capitalize" required value="<?= $retVal = $varTI->policy ?>">
            </div>
        </div>
        <br>
        <div class="showTipo">
            <div class="row">
                <h3 class="col-md-6">Datos de instalación</h3>
                <div class="col-md-6">
                    <label for="pendienteInstacion">Pendiente datos de instalación</label>
                    <div class="switch">
                        <div class="onoffswitch">
                            <input type="checkbox"  class="onoffswitch-checkbox" id="pendienteInstacion" value="si" name="pendienteInstacion">
                            <label class="onoffswitch-label" for="pendienteInstacion">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tablist">
                <input type="radio" title="taller" name="lugarInstacion" class='lugarDeInstalacion' id="taller" role="tab" aria-controls="taller-panel" checked value="Taller" class="lugarDeInstalacion tall" />
                <input type="radio" title="domicilio" name="lugarInstacion"  class='lugarDeInstalacion' id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente" class="lugarDeInstalacion domm" />
                <input type="radio" title="concesionario" name="lugarInstacion" class='lugarDeInstalacion'  id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario" class="lugarDeInstalacion conn" >

                <div class="tabs" aria-hidden="true">
                    <label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
                    <label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
                    <label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
                </div>
                <div class="panels">
                    <ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">
                        <h2><?= $lista->lugarInstalacion ?> | <?= $lista->observacion ?></h2>
                        <p><?= $lista->direccionInstacion  ?></p>
                        <p>
                            <?php $dep=$clienteObjModal->obtenerDepartamentoPorId($lista->departamentoi) ?><?= $dep->departamento ?>
                            &nbsp;|&nbsp;
                            <?php $prov=$clienteObjModal->otenerProvinciaPorId($lista->provinciai) ?><?php echo $prov->provincia ?>
                            &nbsp;|&nbsp;
                            <?php $di=$clienteObjModal->obtenerDistritoPorId($lista->distritoi) ?><?= $di->distrito ?>
                        </p>

                        <input type="hidden" name="direccionTaller" value="<?= $lista->direccionInstacion  ?>">
                        <input type="hidden" name="disT" id="distritoTallerid" value="<?= $lista->distritoi  ?>">
                        <input type="hidden" name="proT" value="<?= $lista->provinciai  ?>">
                        <input type="hidden" name="depT" value="<?= $lista->departamentoi  ?>">

                        <div class="row">
                            <div class="row">
                                <div class="row" >
                                    <div class="form-group col-md-4">
                                        <label  class="">Fecha</label>
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control tal tala fecha"  id="fechaIT" name="fechaIT" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="turno" class="">Turno a</label>
                                        <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn form-control border-bottom btn btn-info" >Seleccionar hora</button>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posT" class="hora form-control border-bottom"  required="" readonly>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRT" class="delR form-control border-bottom"  required="" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="horaS" class="">Hora seleccionada</label>
                                        <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaT" class="horaVer tala tal form-control border-bottom"  required="" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab">
                        <h2>Domicilio Cliente</h2>
                        <div class="row">
                            <div class="row">
                                <div class="form-group  col-md-4">
                                    <label for="departamentoInstalacionD" class="">Departamento*</label>
                                    <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize dom domo" required name="departamentoInstalacionD" id="departamentoInstalacionD">
                                        <option class="text-capitalize" value="" disabled selected >Seleccione Departamento* </option>
                                        <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                                        <?php if ($objDepartamento): ?>
                                            <?php foreach ($objDepartamento as $var): ?>
                                                <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                                            <?php endforeach ?>
                                        <?php else: ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="provinciaInstalacionD" class="">Provincia*</label>
                                    <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize dom domo" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="distritoInstalacionD" class="">Distrito*</label>
                                    <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize dom domo" required name="distritoInstalacionD"  id="distritoInstalacionD" >
                                    </select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group  col-md-12">
                                    <label for="direccionInstalacionD" class="sr-only">Direccion*</label>
                                    <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionD" name="direccionInstalacionD"  class="form-control border-bottom dom domo" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for="referenciaInstalacionD" class="sr-only">Referencia *</label>
                                    <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" required="" name="referenciaInstalacionD" class="form-control border-bottom dom domo"  >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="cod" class="sr-only">Cordenadas Domicilio *</label>
                                    <input style="border: none;" type="text" placeholder="Cordenadas Domicilio *" id="cod" name="cordenadasD" class="form-control x2 text-capitalize border-bottom dom domo" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label  class="">Fecha</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control dom domo fecha"  id="fechaID" name="fechaID" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="turno" class="">Turno b</label>
                                    <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Seleccionar hora</button>
                                    <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posD" class="hora form-control border-bottom"  required="" readonly>
                                    <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRD" class="delR form-control border-bottom"  required="" readonly>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="horaS" class="">Hora seleccionada</label>
                                    <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaD" class="horaVer domo dom  form-control border-bottom"  required="" readonly>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">

                        <div class="row">
                            <div class="col-md-6"><h2>Concesionario</h2></div>
                            <div class="col-md-6">
                                <label for="concesionarioInstalacion" class="sr-only">Nombre de concesionario*</label>
                                <input style="border: none;" type="text" placeholder="Nombre de concesionario*" id="concesionarioInstalacion" name="concesionarioInstalacion" class="form-control border-bottom con" required="" >
                            </div>
                        </div>

                        <div class="row">
                            <div class="row">
                                <div class="form-group  col-md-4">
                                    <label for="departamentoInstalacionC" class="">Departamento*</label>
                                    <select  style="border: none;" class="departamentoInstalacionC border-bottom form-control text-capitalize con cono" required name="departamentoInstalacionC" id="departamentoInstalacionC">
                                        <option class="text-capitalize" disabled selected >Seleccione Departamento</option>
                                        <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                                        <?php if ($objDepartamento): ?>
                                            <?php foreach ($objDepartamento as $var): ?>
                                                <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                                            <?php endforeach ?>
                                        <?php else: ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="provinciaInstalacionC" class="">Provincia*</label>
                                    <select  style="border: none;" class="provinciaInstalacionC border-bottom form-control text-capitalize con cono" required name="provinciaInstalacionC" id="provinciaInstalacionC" >
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="distritoInstalacionC" class="">Distrito*</label>
                                    <select  style="border: none;" class="distritoInstalacionC border-bottom form-control text-capitalize con cono" required name="distritoInstalacionC"  id="distritoInstalacionC" > 		</select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for="direccionInstalacionC" class="">Direccion*</label>
                                    <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom con cono" >
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for="referenciaInstalacionC" class="">Referencia *</label>
                                    <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom con cono" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label  class="">Fecha</label>
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control con cono fecha"  id="fechaIC" name="fechaIC" autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="turno" class="">Turno c</label>
                                        <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Seleccionar hora</button>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posC" class="hora form-control border-bottom"  required="" readonly>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRC" class="delR form-control border-bottom"  required="" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="horaS" class="">Hora seleccionada</label>
                                        <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaC" class="horaVer cono con form-control border-bottom"  required="" readonly>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </ul>
                </div>
            </div>
            <div class="ibox" id="ibox1">
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-double-bounce">
                        <div class="sk-double-bounce1"></div>
                        <div class="sk-double-bounce2"></div>
                    </div>
                    <div id="programacionHtml">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="nombreContacto" class="">Nombre Contacto *</label>
                    <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContacto" name="nombreContacto" class="form-control x2 text-capitalize border-bottom " required="" value="<?= $varTI->apellido_asegurado." ".$varTI->nombre_asegurado ?>"" >
                </div>
                <div class="form-group col-md-6">
                    <label for="numeroContacto" class="">Número Contacto *</label>
                    <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContacto" name="numeroContacto" class="form-control x2 text-capitalize border-bottom" required="" value="<?= $varTI->celular ?>"">
                </div>
            </div>

            <hr>
            <div class="row">

                <div class="form-group col-md-12">

                    <label for="seguimientoTrama" class="">Seguimiento</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Seguimiento" id="seguimientoTrama" >
                        <span class="input-group-btn">
                    <button type="button" id="registrarSeguiminto" class="btn btn-primary">Registrar Seguimiento </button>
                </span>
                    </div>
                </div>

            </div>

            <ul id='segimientohtml'>
                <?php foreach(TramaIntegracionPF::getSEg($idTrama) as $var ): ?>
                    <li><?= $var->fecha ?> <b><?= $var->body ?></b></li>
                <?php endforeach ?>
            </ul>
            <hr>
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="observaciong" class="">Observaciones </label>
                    <input style="border: none;" type="text" placeholder="Observaciones " id="observaciong" name="observaciong" class="form-control x2 border-bottom focusUrgenteta" >
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
            <button type="submit" name="crearDesdeChasis" id="agregarVehiculoBttn" class="btn btn-primary" value="<?php echo Token::generar() ?>">Confirmar</button>
        </div>
    </div>
    <script src="../../assets/js/registrarme.js"></script>
    <script src="../../assets/js/validacionFactura.pabel.js"></script>
    <script src="../../assets/bootbox/bootbox.all.js"></script>
    <script>

        function validar(){
            var lugarI=$( "input[name='lugarInstacion']:checked" ).val()
            if (lugarI=='Taller') {
                $('.con').prop('disabled', true);
                $('.dom').prop('disabled', true);
                $('.tal').prop('disabled', false);
            }else if (lugarI=='Domicilio Cliente') {
                $('.con').prop('disabled', true);
                $('.dom').prop('disabled', false);
                $('.tal').prop('disabled', true);
            }else if (lugarI=='Concesionario') {
                $('.con').prop('disabled', false);
                $('.dom').prop('disabled', true);
                $('.tal').prop('disabled', true);
            }
        }

        $(document).ready(function(){
            $( "input[name='lugarInstacion']" ).on( "click",function(){
                $('#programacionHtml').html('');
                $('.horaVer').val('');
                $('.fecha').val('');
                validar();
            });
            validar();
        });


    </script>
    <script>
        $(".departamentoInstalacionD").change(function(){provincias4();});
        $(".departamentoInstalacionC").change(function(){provincias5();});
        $(".provinciaInstalacionD").change(function(){distritos4();});
        $(".provinciaInstalacionC").change(function(){distritos5();});
        function provincias4(){
            var iddepartamento = $(".departamentoInstalacionD").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/provincia',
                data :  'iddepartamento='+ iddepartamento,
                success : function(data)
                {
                    $(".provinciaInstalacionD").html(data);
                }
            });
            return false;
        }
        function provincias5(){
            var iddepartamento = $(".departamentoInstalacionC").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/provincia',
                data :  'iddepartamento='+ iddepartamento,
                success : function(data)
                {
                    $(".provinciaInstalacionC").html(data);
                }
            });
            return false;
        }

        function distritos4(){
            var idprovincia = $(".provinciaInstalacionD").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/distrito',
                data :  'idprovincia='+ idprovincia,
                success : function(data)
                {
                    $(".distritoInstalacionD").html(data)
                }
            });
            return false;
        }
        function distritos5(){
            var idprovincia = $(".provinciaInstalacionC").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/distrito',
                data :  'idprovincia='+ idprovincia,
                success : function(data)
                {
                    $(".distritoInstalacionC").html(data)
                }
            });
            return false;
        }



        $('.lugarDeInstalacion').on("change",function(){
            var pendienteInstacion=document.getElementById("pendienteInstacion");
            if (pendienteInstacion.checked) {
                $('.cono').prop('disabled', true);
                $('.domo').prop('disabled', true);
                $('.tala').prop('disabled', true);
                $("#programacionHtml").html('');
                limpiarCapacidad();
            }else{
                $('.cono').prop('disabled', false);
                $('.domo').prop('disabled', false);
                $('.tala').prop('disabled', false);
            }
        })


        $('#pendienteInstacion').on("change",function(){
            var pendienteInstacion=document.getElementById("pendienteInstacion");
            if (pendienteInstacion.checked) {
                $('.cono').prop('disabled', true);
                $('.domo').prop('disabled', true);
                $('.tala').prop('disabled', true);
                $("#programacionHtml").html('');
                limpiarCapacidad();
            }else{
                $('.cono').prop('disabled', false);
                $('.domo').prop('disabled', false);
                $('.tala').prop('disabled', false);
            }
        });

        function validaDP() {
            // var dp=1;
            var lugarI = $("input[name='lugarInstacion']:checked").val();
            if (lugarI == 'Taller') {
                var fecha = $('#fechaIT').val();
            } else if (lugarI == 'Domicilio Cliente') {
                var fecha = $('#fechaID').val();
            } else if (lugarI == 'Concesionario') {
                var fecha = $('#fechaIC').val();
            }
            var hora = $(".horaVer").val();
            var canthoraprog = $('#canthoraprog').val();
            var idtec = $('input[name="posT"]').val();
            // console.log("fecha: "+fecha+" idtec:"+idtec+" canhora:"+canthoraprog.trim());
            $.ajax({
                type: 'POST',
                url: 'ajax/validarRegistroTB',
                data:'tipo=save&fecha='+fecha+'&idtec='+idtec+"&canthoraprog=" + canthoraprog.trim() + "&hora=" + hora,
                // data :  'tipo=save&fecha=2019-11-27&idtec=2&canthoraprog=2&hora=12:00',
                success: function (data) {
                    if (data.trim() == 'dp') {
                        //dp='1';
                        $("#agregarVehiculoBttn").prop("disabled",true);
                        //      clearInterval(eventos.interval);
                        $("#programacionHtml").html('');
                        $(".horabtn").html('Seleccionar hora');
                        $(".horaVer").val('');
                        // fn(dp);
                        bootbox.alert({
                            title: "<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                            message: "<label>El horario Seleccionado("+hora+") ya fue Registrado,Por Favor Seleccione otro*</label>",
                            size: 'small',
                            callback: function () {
                                /*  l.ladda( 'stop' );*/
                            }
                        });
                    }else if(data.trim()=='Ok'){
                        $("#agregarVehiculoBttn").prop("disabled",false)
                    }
                }
            });
        }

        var eventos={};
        eventos.frejcar =function(){
            validaDP();
        };
        eventos.interval = setInterval(eventos.frejcar, 3000);

        $(document).ready(function () {

            $('#agregarVehiculoBttn').click(function() {

                checked = $('[name="servad[]"]:checked').length;

                if(checked==0) {
                    bootbox.alert({
                        title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                        message: "<label>Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*</label>",
                        size: 'small',
                        callback: function () {
                            /*  l.ladda( 'stop' );*/
                        }
                    });
                    /* alert("Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*");*/
                    console.log("Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*");
                    // $('[name="servad[]"]:checked')[0].focus();
                    return false;
                }

            });
        });
    </script>
    <script>

        <?php $fechaA=date('Y-m-d') ?>
        jQuery.datetimepicker.setLocale('es');

        $('.fecha').datetimepicker({
            minDate:'<?= $fechaA ?>',
            format:'Y-m-d',
            scrollInput: false,
            scrollMonth : false,
            timepicker:false
        });


        $('#fechaEngrega').datetimepicker({
            minDate:'<?= $fechaA ?>',
            format:'Y/m/d',
            scrollInput: false,
            scrollMonth : false,
            timepicker:false
        });
        $('#horaEntrega').datetimepicker({
            datepicker:false,
            format:'H:i',
            allowTimes:['9:00','11:00','14:00','16:00']
        });

        $('.tiempo').typeahead({
            source: [
                {"name": "1 año"},
                {"name": "2 años"},
                {"name": "3 años"},
                {"name": "4 años"},
                {"name": "5 años"},
                {"name": "6 años"},
                {"name": "8 años"},
                {"name": "7 años"}
            ]
        });

        <?php if ($datos=DatosFacturacion::horariosSugeridosParaEntregaDeFactura()): ?>
        $('#horarioEntrega').typeahead({
            source: [
                <?php foreach ($datos as $var): ?>
                {"name": "<?php echo $var->fecha ?>"},
                <?php endforeach ?>
            ]
        });
        <?php endif ?>

        <?php if ($datosD=Cliente::otenerTodoLosDistritos()): ?>
        $('#distritoEntrega').typeahead({
            source: [
                <?php foreach ($datosD as $var): ?>
                {"name": "<?php echo $var->distrito ?>"},
                <?php endforeach ?>
            ]
        });
        <?php endif ?>

        <?php if ($datosDopeCon=Operacion::obtenerConcesionarioInstalacion()): ?>
        $('#concesionarioInstalacion').typeahead({
            source: [
                <?php foreach ($datosDopeCon as $var): ?>
                {"name": "<?php echo $var->nombreConcecionarioInstalicion ?>"},
                <?php endforeach ?>
            ]
        });
        <?php endif ?>

        $("#registrarSeguiminto").click(function(){
            // alert(servAd) ;seguimientoTrama
            var body=$('#seguimientoTrama').val();
            var idtrama = $("#idtramaf").val();
            var today = new Date();
            var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()+' '+today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            if(body.length>0){
                $.ajax({
                    type : 'POST',
                    url  : 'ajax/registrarSegpf',
                    data : {body:body,idtrama:idtrama},
                    success : function(data)
                    {
                        $('#seguimientoTrama').val('');
                        $("#segimientohtml").append('<li> '+date+' <b>'+body+'</b></li>');
                        //segimientohtml
                    }
                });
            }
            return false;
        });

        function lstTProg(){
            var lugarI=$("input[name='lugarInstacion']:checked" ).val();
            var distrito="0";
            var tthinstala=$('#canthoraprog').val();
            var cita = 'General';
            var concecion="0";
            var lugarT='';

            if (lugarI=='Taller') {
                var fecha = $('#fechaIT').val();
                distrito=$("#distritoTallerid").val()

                console.log($("#distritoTallerid").val())
                lugarT='Taller';
            }else if (lugarI=='Domicilio Cliente') {
                var fecha = $('#fechaID').val();
                distrito=$('#distritoInstalacionD').val();
                lugarT='Domicilio';
            }else if (lugarI=='Concesionario') {
                var fecha = $('#fechaIC').val();
                var concecion=$('#concesionarioInstalacion').val();
                var cita = concecion;
                distrito=$('#distritoInstalacionC').val();
                lugarT='Concesionario';
            }
            var cod = document.getElementById("plan").value;
            servAd = $('[name="servad[]"]:checked').length;
            if (fecha!='' && cod!='' && servAd!=0 &&(distrito==0 || distrito!=null) && concecion!='') {
                $(".horabtn").off( "click" );
                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                $.ajax({
                    type : 'POST',
                    url  : '../includes/tbProgramacion',
                    data : {fecha:fecha,cita:cita,tthinstala:tthinstala,iddist:distrito,lugart:lugarT},
                    success : function(data)
                    {
                        $("#programacionHtml").html(data);
                        $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                        $(".horabtn").on("click",lstTProg).find(".horabtn");
                    }
                });
            }else{
                $("#programacionHtml").html('<h3 class="text-center text-info">Primero debe seleccionar Plan,Servicios Adicionales,fecha y/o Distrito y/o Nombre Concesionario.</h3>');
            }
            return false;
        }

        $(".horabtn").click(function(){
            lstTProg();
        });

        $('.fecha').change(function(){
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');
        });
        $("#concesionarioInstalacion").blur(function(){
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');
        });


        $('#departamentoInstalacionD,#departamentoInstalacionC,#provinciaInstalacionD,#provinciaInstalacionC,#distritoInstalacionD,#distritoInstalacionC').change(function(){
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');
        });

    </script>

<?php endif ?>

    <?php
}
?>

<?php
if (isset($_POST['idSolRadar'])) {
    include_once("../../includes/init.php");

    $lista = Operacion::obtenerFrecuentes($session->idusuario);
    $idTrama=limpiar(trim($_POST['idSolRadar']));
    $varTI=WebTrack::obtenerSolicitudRadar($idTrama);

    WebTrack::asignarSolicitud($_POST['idSolRadar']);
    $filTOP="'Reactivacion'";$filTopCant="";
    $clienteObjModal=new Cliente;

    ?>

    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>

    <style>
        .ibox-content > .sk-spinner {
            display: none;
        }

        .ibox-content.sk-loading {
            position: relative;
        }
        .ibox-content.sk-loading:after {
            content: '';
            background-color: rgba(255, 255, 255, 0.7);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        .ibox-content.sk-loading > .sk-spinner {
            display: block;
            position: absolute;
            top: 40%;
            left: 0;
            right: 0;
            z-index: 2000;
        }
    </style>
    <div class="showOPERA">
        <div class="well" style="padding-top: 11px;padding-bottom: 10px; ">
            <?php if ($varTI): ?>
            <?php if (!is_null($varTI->idsuariocargo)) {
                $usuarioObj = Usuario::obtenerUsuario($varTI->idsuariocargo);
            } ?>

            <div class="col-md-12 text-center"><label  class='text-success' for="">A Cargo de : <?php if(!is_null($varTI->idsuariocargo)){echo $usuarioObj->usuarioNombre.' '.$usuarioObj->usuarioApellidoPaterno;}else{ echo 'Libre';}?></label></divas>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="Observaciones" ><?= $varTI->tipo_documento == 1 ? 'DNI' : 'RUC'  ?> </label>
                    <input name="dniORuc" style="border: none;" type="text" placeholder="Web" id="Observaciones" class="border-bottom form-control dnioruconix"  value="<?= $varTI->numero_documento ?>">

                </div>
                <div class="form-group col-md-5">
                    <?php $apellidos='' ?>
                    <input type="hidden" name="idSolRadar" value="<?= $idTrama ?>">
                    <input type="hidden" id='idtrama' name="idTrama" value="<?= $idTrama ?>">
                    <label for="apellidos" >Apellidos :</label>
                    <input  style="border: none;" type="hidden" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->nombre_asegurado)) ? "$apellidos"." "."$varTI->nombre_asegurado" : "$apellidos" ; ?> ">
                    <input  style="border: none;" type="text" id="apellidos" name="apellidos" class="border-bottom form-control" required value="<?= $varTI->apellido ?> ">

                </div>
                <div class="form-group col-md-4">
                    <label for="nombres" >Nombres :</label>
                    <input  style="border: none;" type="text" id="nombres" name="nombres" class="border-bottom form-control" required value="<?= $varTI->nombre ?> ">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="numeroTecnico" >Dirección :</label>
                    <input  style="border: none;" type="text" id="direcSegura" name="direcSegura" class="border-bottom form-control" required value="<?= $varTI->direccion ?> ">
                </div>
                <div class="form-group col-md-4">
                    <label for="correoCliente" >Correo :</label>
                    <input  style="border: none;" type="text" id="correoCliente" name="correoCliente" class="border-bottom form-control" required value="<?= strtolower($varTI->correo) ?> ">
                </div>
                <div class="col-md-2"><label for="estado" >Celular</label>
                    <input style="border: none;" type="text" name="celular" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->celular)) ? "$varTI->celular" : "" ; ?>" >
                </div>
                <div class="col-md-2"><label for="estado" >Telefono</label>
                    <input style="border: none;" type="text" name="telefono5" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->num_telefono)) ? "$varTI->num_telefono" : "" ; ?>">
                </div>
            </div>
            <input type="hidden" value="<?php echo $retVal = (!empty($varTI->num_telefono)) ? "$varTI->num_telefono" : "" ; ?>" name="telefono">
            <input type="hidden" value="" name="telefono2">

        </div>

        <h4>Documentos del cliente: </h4>
        <hr>
        <div class="row">
            <div class="col-md-6"><a target="_blank" href="<?= $varTI->url_poliza ?>">- Póliza vehicular</a></div>
            <div class="col-md-6"><a target="_blank" href="<?= $varTI->url_tarjeta_propiedad_anverso ?>">- Tarjeta de propiedad anverso</a></div>
            <div class="col-md-6"><a target="_blank" href="<?= $varTI->url_tarjeta_propiedad_reverso ?>">- Tarjeta de propiedad reverso</a></div>
            <div class="col-md-6"><a target="_blank" href="<?= $varTI->url_comprobante_pago ?>">- Comprobante de abono a las cuentas</a></div>
        </div>
        <hr>

        <div class="row">
            <div class="form-group col-md-3">
                <label for="numeroTecnico" >Placa Vehiculo*</label>
                <input  style="border: none;" type="text" placeholder="Placa" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->placa)) ? "$clienteObj->placa" : "" ; ?>"  >
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones">Marca *</label>
                <input required <?php echo $retVal = (!empty($clienteObj->marca)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->marca)) ? "$clienteObj->marca" : "" ; ?>" >
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones">Modelo*</label>
                <input required <?php echo $retVal = (!empty($clienteObj->modelo)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($clienteObj->modelo)) ? "$clienteObj->modelo" : "" ; ?>">
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones" >Color</label>
                <input <?php echo $retVal = (!empty($clienteObj->color)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Color" id="Observaciones" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->color)) ? "$clienteObj->color" : "" ; ?>">
            </div>

        </div>
        <div class="row">
            <div class="form-group col-md-2">
                <label for="Anio" >Año*</label>
                <input id="Anio" type="text" name="anioF" placeholder="Año" class="border-bottom form-control" value="" style="border: none;" required>
            </div>
            <div class="form-group col-md-3">
                <label for="Motor" >Motor*</label>
                <input id="Motor" type="text" name="motor" placeholder="Motor" class="border-bottom form-control" value="" style="border: none;" required>
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones" >Chasis*</label>
                <input required <?php echo $retVal = (!empty($clienteObj->chasis)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->chasis)) ? "$clienteObj->chasis" : "$chasis" ; ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="seguro" class="">Seleccione Compañía de Seguros</label>
                <select  style="border: none;" class="border-bottom form-control text-capitalize" required name="seguro" id="seguro">
                    <option class='text-capitalize' value='' disabled selected >Seleccione Compañía de Seguros</option>
                    <?php Cliente::obtenerAseguradora('') ?>
                    <!--<option class='text-capitalize' value='Por confirmar' >Por confirmar</option>
                   <option class='text-capitalize' value='No tiene' >No tiene</option>
                   <option class='text-capitalize' value='La Positiva' >La Positiva</option>
                   <option class='text-capitalize' value='Pacifico Seguros' >Pacifico Seguros</option>
                    <option class='text-capitalize' value='Rimac seguros' >Rimac seguros</option>
                   <option class='text-capitalize' value='Mapfre Peru' >Mapfre Peru</option>
                   <option class='text-capitalize' value='MHDI Seguros' >HDI Seguros</option>
                   <option class='text-capitalize' value='Qualitas Seguros' >Qualitas Seguros</option> -->

                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="plan" class="">Plan *</label>
                <select  style="border: none;" class="tecnico border-bottom form-control" readonly required name="plan" id="plan">
                    <option value="Radar Pro" data-type="1" selected>Radar Pro</option>
                </select>
            </div>
            <div class="form-group col-md-6 ">
                <p style="font-weight:bold">Servicios Adicionales *: <label class="canthoraprog2 label-success"> 2 Horas </label></p>
                <div class="servAdi">
                    <div class="col-md-4" style="padding:0.5px;">
                        <div class="checkbox checkbox-primary  disabled  " style="margin-top:1px; margin-bottom: 1px">
                            <input id="bloq" disabled="" type="checkbox" name="servad[]" value="1">
                            <label for="bloq">Bloqueo</label>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding:0.5px;">
                        <div class="checkbox checkbox-primary  disabled  " style="margin-top:1px; margin-bottom: 1px">
                            <input id="btnpan" disabled="" type="checkbox" name="servad[]" value="2">
                            <label for="btnpan">Boton de Panico</label>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding:0.5px;">
                        <div class="checkbox checkbox-primary  disabled  " style="margin-top:1px; margin-bottom: 1px">
                            <input id="pstll" disabled="" type="checkbox" name="servad[]" value="3">
                            <label for="pstll">Pestillos</label>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding:0.5px;">
                        <div class="checkbox checkbox-primary  disabled  " style="margin-top:1px; margin-bottom: 1px">
                            <input id="sensorcom" disabled="" type="checkbox" name="servad[]" value="4">
                            <label for="sensorcom">Sensor de Combustible</label>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding:0.5px;">
                        <div class="checkbox checkbox-primary   " style="margin-top:1px; margin-bottom: 1px">
                            <input id="ngn" type="checkbox" checked readonly name="servad[]" value="5" onclick="return false;">
                            <label for="ngn">Ninguno</label>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="canthoraprog" name="canthoraprog" value="2">
            </div>
        </div><br>
        <div class="row ">
            <div class="col-md-3">
                <label for="ejecutivoCanal" >Precio lista</label>
                <input  style="border: none;" type="text" readonly placeholder="Precio lista" id="ejecutivoCanal" name="precioLista" class="border-bottom form-control" value="150.00" required >
            </div>
            <div class="col-md-3">
                <label for="time" >Tiempo *</label>
                <input  style="border: none;" type="text" readonly placeholder="Tiempo *" id="time" name="tiempo" class=" tiempo border-bottom form-control" value="1 año" required autocomplete="off" >
            </div>

            <div class="form-group col-md-6 camapanaEsconder">
                <label for="campana" class="">¿ Campaña Pacífico Gps Smart ?</label>
                <select  style="border: none;" class="tecnico border-bottom form-control" required name="campana" id="campana">
                    <option class="" value="" disabled selected >Seleccione *</option>
                    <option class="" value="GpsSmart">Si</option>
                    <option class="" value="no">No</option>
                </select>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="idEjecutvio" class="">Seleccione Ejecutivo</label>
                <?php
                if ($obMenu->administrarComercial=="si") {
                    ?>
                    <select  style="border: none;" class="border-bottom form-control text-capitalize" required name="idEjecutvio" id="idEjecutvio">
                        <option class='text-capitalize' value='' disabled selected >Seleccione Ejecutivo</option>
                        <?php if ($datosUsu=Usuario::obtenerEjecutivos()): ?>
                            <?php foreach ($datosUsu as $var): ?>
                                <option class='text-capitalize' value='<?= $var->idusuario ?>' > <?= $var->usuarioNombre." ".$var->usuarioApellidoPaterno;  ?></option>
                            <?php endforeach ?>
                        <?php endif ?>
                    </select>
                    <?php
                }else{
                    ?>
                    <select  style="border: none;" class="border-bottom form-control text-capitalize" readonly required name="idEjecutvio" id="idEjecutvio">
                        <!--                        <option class='text-capitalize' value='' disabled selected >Seleccione Ejecutivo</option>-->
                        <option class="text-capitalize" value="153" selected> Motorlink </option>
                    </select>
                    <?php
                }
                ?>
            </div>

            <div class="col-md-6">
                <label for="plan" class="">Tipo de Sevicio *</label>
                <select  style="border: none;" class="border-bottom form-control" readonly required name="tipo" id="tipo">
                    <option value="Instalacion" selected>Instalacion</option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-3">
                <label for="tipoCanal" class="">Seleccione canal *</label>
                <select style="border: none;"  name="tipoCanal" readonly="" class="cd-selec form-control border-bottom " required id="tipoCanal">
                    <!--<option value="" disabled selected>Seleccione canal *</option>
                    <option value="BROKER">Broker</option>
                    <option value="CONCESIONARIO">Concesionario</option>
                    <option value="CORPORATIVO">Corporativo</option>
                    <option value="ASEGURADORA">Aseguradora</option>
                    <option value="CONVENIO">Convenio</option>
                    <option value="BANCO">Banco</option>
                    <option value="DIGITAL">DIGITAL</option>-->
                    <option value="TRACKLINK OCTO" selected>Tracklink octo</option>
                </select>
            </div>
            <div class="form-group col-md-2 a">
                <label for="nombreCanal" class="">Canal nombre</label>
                <input style="border: none;" type="text" readonly placeholder="Canal nombre" id="nombreCanal" name="nombreCanal" value="DIRECTO OFICINA" class="border-bottom form-control text-uppercase nombreCanal " autocomplete="off" required="" >
            </div>
            <div class="tiendaAparece1">
                <div class="form-group col-md-2 ">
                    <label for="tienda" class="">Tienda</label>
                    <input style="border: none;" type="text" placeholder="Tienda" id="tienda" name="tienda" class="border-bottom form-control text-uppercase tienda " autocomplete="off" required="" >
                </div>
            </div>
            <div class="b">
                <div class="form-group col-md-2 ">
                    <label for="convenio" class="">Seleccione Convenio *</label>
                    <select style="border: none;"  name="convenio" class="cd-selec form-control border-bottom" required id="convenio">
                        <?php $ConD=Canales::obtenerConvenios() ?>
                        <option value="" disabled selected>Seleccione Convenio *</option>
                        <?php foreach ($ConD as $var): ?>
                            <option value="<?= $var->idconvenio  ?>"><?= $var->nombreConvenio ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="tiendaAparece">
                    <div class="form-group col-md-2 ">
                        <label for="tienda" class="">Tienda</label>
                        <input style="border: none;" type="text" placeholder="Tienda" id="tienda" name="tienda1" class="border-bottom form-control text-uppercase tienda " autocomplete="off" required="" >
                    </div>
                </div>
            </div>

            <div class="form-group col-md-3">
                <label for="ejecutivoCanalN" class="">Ejecutivo canal</label>
                <input style="border: none;" type="text" readonly placeholder="Ejecutivo canal" id="ejecutivoCanalN" name="ejecutivoCanalN" value="DIRECTO OFICINA" class="border-bottom form-control text-uppercase ejecutivoCanalN" autocomplete="off" required="" >
            </div>
            <div class="col-md-2">
                <label for="comision">Cobra Comision</label>
                <div class="switch">
                    <div class="onoffswitch" readonly>
                        <input type="checkbox" readonly class="onoffswitch-checkbox" id="comision" value="si" name="comision" onclick="return false;">
                        <label class="onoffswitch-label" for="comision">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>











        <div class="row">
            <h3 class="col-md-6">Datos de facturación </h3>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="Forma*" >Forma</label>
                <select style="border: none;" id="formaPago" name="formaPago" class="form-control border-bottom" readonly required >
                    <option value="Deposito" selected>Deposito</option>
                    <!--<option value="OrdenDeCompra">Órden de compra</option>
                    <option value="pacificoPandero">Pacífico Pandero</option>
                    <option value="pacificoBanbif">Pacífico BanBif</option>
                    <option value="concesionarioPandero">Concesionario Pandero</option>
                    <option value="Banbif">BanBif</option>
                    <option value="LeyendaAPositiva">Leyenda A La Positiva</option>
                    <option value="PagoEntaler" > Págo en taller</option>
                    <option value="telefonica">Telefonica</option>
                    <option value="AmericaBrokers">American Brokers</option>
                    <option value="CEDEA">CEDEA</option>
                    <option value="Moviliza">Moviliza</option>-->
                </select>
            </div>
            <div class="col-md-6 voucherImg">
                <label for="Forma*" >Voucher</label>
                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                    <div class="form-control" data-trigger="fileinput">
                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                        <span class="fileinput-filename"></span>
                    </div>
                    <span class="input-group-addon btn btn-default btn-file">
					<span class="fileinput-new">Seleccionar Voucher de pago*</span>
					<span class="fileinput-exists">Cambiar</span>
					<input type="file" name="voucher"  id="voucherImg" required="" />
				</span>
                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4">
                <select style="border: none;" id="selectNa" name="tipoDocumentoFacturacion" class="cd-selec form-control border-bottom" required >
                    <option value="" disabled selected>Datos de facturación*</option>
                    <option value="Boleta">Boleta</option>
                    <option value="Factura">Factura</option>
                </select>
            </div>
            <div class="col-md-8">
                <div class="content-pane e1">
                    <h3>Seleccione pago</h3>
                </div>
                <div class="content-pane eFactura">
                    <div class="row"><br>
                        <div class="form-group col-md-4">
                            <label for="rucFacturacion" class="">Ruc *</label>
                            <input style="border: none;" type="number" placeholder="Ruc *" id="rucFacturacion" name="rucFacturacion" class="form-control x2 border-bottom" maxlength="12" minlength="10" value="<?= $retVal = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>"  >
                        </div>
                        <div class="form-group col-md-8">
                            <label for="razonSocialFacturacion" class="">Razón Social *</label>
                            <input style="border: none;" type="text" placeholder="Razón Social *" id="razonSocialFacturacion" name="razonSocialFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ; ?>" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-8">
                            <label for="direccionFiscalFacturacion" class="sr-only">Dirección Fiscal *</label>
                            <input style="border: none;" type="text" placeholder="Dirección Fiscal *" id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="form-control border-bottom text-capitalize" value="<?= $clienteObj->direccion ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="telefonoFacturacion" class="sr-only">Teléfono</label>
                            <input style="border: none;" type="text" placeholder="Teléfono" id="telefonoFacturacion" name="telefonoFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <!--
        <div class="row">
        <div class="col-md-12">
        <select style="border: none;" id="entrega" name="entregaEnInstalacion" class="cd-selec form-control border-bottom text-center" required >
        <option value="" disabled selected>¿Enviar Factura o boleta?</option>
        <option value="noE">No enviar</option>
        <option value="si">Enviar a domicilio</option>
        <option value="no">Entregar cuando se realiza la instalación.</option>
        </select>
        </div>
        </div>
        -->
        <div class="esconderNo">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="direccionEntrega" class="">Dirección de engrega</label>
                    <input style="border: none;" type="text" placeholder="Dirección de engrega" id="direccionEntrega" name="direccionEntrega" class="form-control border-bottom text-capitalize" value="">
                </div>
                <div class="form-group col-md-6">
                    <label for="referenciaEntrega" class="">Referencia de engrega</label>
                    <input style="border: none;" type="text" placeholder="Referencia de engrega" id="referenciaEntrega" name="referenciaEntrega" class="form-control border-bottom text-capitalize" required="" value="">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="distritoEntrega" class="">Distrito de entrega</label>
                    <input style="border: none;" type="text" placeholder="Distrito de entrega" id="distritoEntrega" name="distritoEntrega" class="form-control border-bottom text-capitalize" required autocomplete="off" value="">
                </div>
                <div class="form-group col-md-4">
                    <label for="horarioEntrega" class="">Horario de entrega</label>
                    <input style="border: none;" type="text" placeholder="Horario de entrega" id="horarioEntrega" name="horarioEntrega" class="form-control border-bottom text-capitalize" autocomplete="off" required value="">
                </div>
                <div class="form-group col-md-4">
                    <label for="personaEntrega" class="">Persona de recepción de factura </label>
                    <input style="border: none;" type="text" placeholder="Persona de recepción de factura" id="personaEntrega" name="personaEntrega" class="form-control border-bottom text-capitalize" required value="">
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="showTipo">
            <div class="row">
                <h3 class="col-md-6">Datos de instalación</h3>
                <div class="col-md-6">
                    <label for="pendienteInstacion">Pendiente datos de instalación</label>
                    <div class="switch">
                        <div class="onoffswitch">
                            <input type="checkbox"  class="onoffswitch-checkbox" id="pendienteInstacion" value="si" name="pendienteInstacion">
                            <label class="onoffswitch-label" for="pendienteInstacion">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tablist">
                <input type="radio" title="taller" name="lugarInstacion" class='lugarDeInstalacion' id="taller" role="tab" aria-controls="taller-panel" checked value="Taller" class="lugarDeInstalacion tall" />
                <input type="radio" title="domicilio" name="lugarInstacion"  class='lugarDeInstalacion' id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente" class="lugarDeInstalacion domm" />
                <input type="radio" title="concesionario" name="lugarInstacion" class='lugarDeInstalacion'  id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario" class="lugarDeInstalacion conn" >

                <div class="tabs" aria-hidden="true">
                    <label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
                    <label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
                    <label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
                </div>
                <div class="panels">
                    <ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">


                        <h2><?= $lista->lugarInstalacion ?> | <?= $lista->observacion ?></h2>
                        <p><?= $lista->direccionInstacion  ?></p>
                        <p>
                            <?php $dep=$clienteObjModal->obtenerDepartamentoPorId($lista->departamentoi) ?><?= $dep->departamento ?>
                            &nbsp;|&nbsp;
                            <?php $prov=$clienteObjModal->otenerProvinciaPorId($lista->provinciai) ?><?php echo $prov->provincia ?>
                            &nbsp;|&nbsp;
                            <?php $di=$clienteObjModal->obtenerDistritoPorId($lista->distritoi) ?><?= $di->distrito ?>
                        </p>

                        <input type="hidden" name="direccionTaller" value="<?= $lista->direccionInstacion  ?>">
                        <input type="hidden" name="disT" id="distritoTallerid" value="<?= $lista->distritoi  ?>">
                        <input type="hidden" name="disT" value="<?= $lista->distritoi  ?>">
                        <input type="hidden" name="proT" value="<?= $lista->provinciai  ?>">
                        <input type="hidden" name="depT" value="<?= $lista->departamentoi  ?>">

                        <div class="row">
                            <div class="row">

                                <div class="row" >
                                    <div class="form-group col-md-4">
                                        <label  class="">Fecha</label>
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control tal tala fecha"  id="fechaIT" name="fechaIT" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="turno" class="">Turno a</label>
                                        <button  type="button" placeholder="Hora *" id="horabtn" value='' required  class="horabtn form-control border-bottom btn btn-info" >Seleccionar hora</button>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posT" class="hora form-control border-bottom"  required="" readonly>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRT" class="delR form-control border-bottom"  required="" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="horaS" class="">Hora seleccionada</label>
                                        <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaT" class="horaVer tala tal form-control border-bottom"  required="" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab">
                        <h2>Domicilio Cliente</h2>
                        <div class="row">
                            <div class="row">
                                <div class="form-group  col-md-4">
                                    <label for="departamentoInstalacionD" class="">Departamento*</label>
                                    <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize dom domo" required name="departamentoInstalacionD" id="departamentoInstalacionD">
                                        <option class="text-capitalize" value="" disabled selected >Seleccione Departamento* </option>
                                        <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                                        <?php if ($objDepartamento): ?>
                                            <?php foreach ($objDepartamento as $var): ?>
                                                <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                                            <?php endforeach ?>
                                        <?php else: ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="provinciaInstalacionD" class="">Provincia*</label>
                                    <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize dom domo" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="distritoInstalacionD" class="">Distrito*</label>
                                    <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize dom domo" required name="distritoInstalacionD"  id="distritoInstalacionD" >
                                    </select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group  col-md-12">
                                    <label for="direccionInstalacionD" class="sr-only">Direccion*</label>
                                    <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionD" name="direccionInstalacionD"  class="form-control border-bottom dom domo" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for="referenciaInstalacionD" class="sr-only">Referencia *</label>
                                    <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" required="" name="referenciaInstalacionD" class="form-control border-bottom dom domo"  >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="cod" class="sr-only">Cordenadas Domicilio *</label>
                                    <input style="border: none;" type="text" placeholder="Cordenadas Domicilio *" id="cod" name="cordenadasD" class="form-control x2 text-capitalize border-bottom dom domo" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label  class="">Fecha</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control dom domo fecha"  id="fechaID" name="fechaID" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="turno" class="">Turno b</label>
                                    <button  type="button" placeholder="Hora *" id="horabtn" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Seleccionar hora</button>
                                    <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posD" class="hora form-control border-bottom"  required="" readonly>
                                    <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRD" class="delR form-control border-bottom"  required="" readonly>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="horaS" class="">Hora seleccionada</label>
                                    <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaD" class="horaVer domo dom  form-control border-bottom"  required="" readonly>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">

                        <div class="row">
                            <div class="col-md-6"><h2>Concesionario</h2></div>
                            <div class="col-md-6">
                                <label for="concesionarioInstalacion" class="sr-only">Nombre de concesionario*</label>
                                <input style="border: none;" type="text" placeholder="Nombre de concesionario*" id="concesionarioInstalacion" name="concesionarioInstalacion" class="form-control border-bottom con" required="" >
                            </div>
                        </div>

                        <div class="row">
                            <div class="row">
                                <div class="form-group  col-md-4">
                                    <label for="departamentoInstalacionC" class="">Departamento*</label>
                                    <select  style="border: none;" class="departamentoInstalacionC border-bottom form-control text-capitalize con cono" required name="departamentoInstalacionC" id="departamentoInstalacionC">
                                        <option class="text-capitalize" disabled selected >Seleccione Departamento</option>
                                        <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                                        <?php if ($objDepartamento): ?>
                                            <?php foreach ($objDepartamento as $var): ?>
                                                <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                                            <?php endforeach ?>
                                        <?php else: ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="provinciaInstalacionC" class="">Provincia*</label>
                                    <select  style="border: none;" class="provinciaInstalacionC border-bottom form-control text-capitalize con cono" required name="provinciaInstalacionC" id="provinciaInstalacionC" >
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="distritoInstalacionC" class="">Distrito*</label>
                                    <select  style="border: none;" class="distritoInstalacionC border-bottom form-control text-capitalize con cono" required name="distritoInstalacionC"  id="distritoInstalacionC" > 		</select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for="direccionInstalacionC" class="">Direccion*</label>
                                    <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom con cono" >
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for="referenciaInstalacionC" class="">Referencia *</label>
                                    <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom con cono" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label  class="">Fecha</label>
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control con cono fecha"  id="fechaIC" name="fechaIC" autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="turno" class="">Turno c</label>
                                        <button  type="button" placeholder="Hora *" id="horabtn" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Seleccionar hora</button>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posC" class="hora form-control border-bottom"  required="" readonly>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRC" class="delR form-control border-bottom"  required="" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="horaS" class="">Hora seleccionada</label>
                                        <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaC" class="horaVer cono con form-control border-bottom"  required="" readonly>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </ul>
                </div>
            </div>
            <div class="ibox" id="ibox1">
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-double-bounce">
                        <div class="sk-double-bounce1"></div>
                        <div class="sk-double-bounce2"></div>
                    </div>
                    <div id="programacionHtml">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="nombreContacto" class="">Nombre Contacto *</label>
                    <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContacto" name="nombreContacto" class="form-control x2 text-capitalize border-bottom " required="" value="<?= $varTI->nombre_contacto ?>" >
                </div>
                <div class="form-group col-md-6">
                    <label for="numeroContacto" class="">Número Contacto *</label>
                    <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContacto" name="numeroContacto" class="form-control x2 text-capitalize border-bottom" required="" value="<?= $varTI->telefono_contacto ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="observaciong" class="">Observaciones </label>
                    <input style="border: none;" type="text" placeholder="Observaciones " id="observaciong" name="observaciong" class="form-control x2 border-bottom focusUrgenteta" >
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
            <button type="submit" name="crearDesdeChasis" id="agregarVehiculoBttn" class="btn btn-primary" value="<?php echo Token::generar() ?>">Confirmar</button>
            <div class="row">
                <div class="col-lg-12">
                    <label id="mostrar" class="text-center text-danger">Limite de tecnicos alcanzado por favor intenta en otro horario</label>
                </div>
            </div>
        </div>
    </div>
    <script src="../../assets/js/registrarme.js"></script>
    <script src="../../assets/js/validacionFactura.pabel.js"></script>
    <script src="../../assets/bootbox/bootbox.all.js"></script>
    <?php include_once("../../includes/campanasScript.php") ?>
    <script>
        //$("#fselected").hide()

        <?php if($vhOC->cantidad>0){ ?>
        $('.showOPERA').hide();
        <?php } ?>
        $(".esconderNo").hide();
        $(".esconderNo").prop("disabled",true);
        $("#entrega").change(function(){
            var r=$(this).val();
            if(r=='si'){
                $(".esconderNo").show('slow');
                $(".esconderNo").prop("disabled",false);
            }else{
                $(".esconderNo").hide('slow');
                $(".esconderNo").prop("disabled",true);
            }
        });
        function validar(){
            var lugarI=$( "input[name='lugarInstacion']:checked" ).val()
            if (lugarI=='Taller') {
                $('.con').prop('disabled', true);
                $('.dom').prop('disabled', true);
                $('.tal').prop('disabled', false);
            }else if (lugarI=='Domicilio Cliente') {
                $('.con').prop('disabled', true);
                $('.dom').prop('disabled', false);
                $('.tal').prop('disabled', true);
            }else if (lugarI=='Concesionario') {
                $('.con').prop('disabled', false);
                $('.dom').prop('disabled', true);
                $('.tal').prop('disabled', true);
            }
        }
        $(document).ready(function(){
            $("input[name='lugarInstacion']").on( "click",function(){
                $('#programacionHtml').html('');
                $('.horaVer').val('');
                $('.fecha').val('');
                $("#distritoInstalacionD").val('');
                $("#distritoInstalacionC").val('');
                validar();
            });
            validar();
        });

        function validaDP() {
            // var dp=1;
            var lugarI = $("input[name='lugarInstacion']:checked").val();
            if (lugarI == 'Taller') {
                var fecha = $('#fechaIT').val();
            } else if (lugarI == 'Domicilio Cliente') {
                var fecha = $('#fechaID').val();
            } else if (lugarI == 'Concesionario') {
                var fecha = $('#fechaIC').val();
            }
            var hora = $(".horaVer").val();
            var canthoraprog = $('#canthoraprog').val();
            var idtec = $('input[name="posT"]').val();
            // console.log("fecha: "+fecha+" idtec:"+idtec+" canhora:"+canthoraprog.trim());
            $.ajax({
                type: 'POST',
                url: 'ajax/validarRegistroTB',
                data:'tipo=save&fecha='+fecha+'&idtec='+idtec+"&canthoraprog=" + canthoraprog.trim() + "&hora=" + hora,
                // data :  'tipo=save&fecha=2019-11-27&idtec=2&canthoraprog=2&hora=12:00',
                success: function (data) {
                    if (data.trim() == 'dp') {
                        //dp='1';
                        $("#agregarVehiculoBttn").prop("disabled",true);
                        //      clearInterval(eventos.interval);
                        $("#programacionHtml").html('');
                        $(".horabtn").html('Seleccionar hora');
                        $(".horaVer").val('');
                        // fn(dp);
                        bootbox.alert({
                            title: "<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                            message: "<label>El horario Seleccionado("+hora+") ya fue Registrado,Por Favor Seleccione otro*</label>",
                            size: 'small',
                            callback: function () {
                                /*  l.ladda( 'stop' );*/
                            }
                        });
                    }else if(data.trim()=='Ok'){
                        $("#agregarVehiculoBttn").prop("disabled",false)
                    }
                }
            });
        }

        var eventos={};
        eventos.frejcar =function(){
            validaDP();
        };
        eventos.interval = setInterval(eventos.frejcar, 3000);

        $(document).ready(function () {
            $('#agregarVehiculoBttn').click(function(e) {
                checked = $('[name="servad[]"]:checked').length;
                //var dp=1;
                if(checked==0) {
                    bootbox.alert({
                        title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                        message: "<label>Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*</label>",
                        size: 'small',
                        callback: function () {
                            /*  l.ladda( 'stop' );*/
                        }
                    });
                    /*            alert("Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*");*/
                    console.log("Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*");
                    // $('[name="servad[]"]:checked')[0].focus();
                    return false;
                }
                // return false;
            });
        });




        $('#rucFacturacion').on("change",function(){
            ruc=$('#rucFacturacion').val();
            if (ruc.length==11) {

            }else{
                alert("El ruc debe contener 11 caracteres actualmente tiene: "+ruc.length );
            }

        });

    </script>
    <script>
        $(".departamentoInstalacionD").change(function(){provincias4();});
        $(".departamentoInstalacionC").change(function(){provincias5();});
        $(".provinciaInstalacionD").change(function(){distritos4();});
        $(".provinciaInstalacionC").change(function(){distritos5();});
        function provincias4(){
            var iddepartamento = $(".departamentoInstalacionD").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/provincia',
                data :  'iddepartamento='+ iddepartamento,
                success : function(data)
                {
                    $(".provinciaInstalacionD").html(data);
                }
            });
            return false;
        }
        function provincias5(){
            var iddepartamento = $(".departamentoInstalacionC").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/provincia',
                data :  'iddepartamento='+ iddepartamento,
                success : function(data)
                {
                    $(".provinciaInstalacionC").html(data);
                }
            });
            return false;
        }

        function distritos4(){
            var idprovincia = $(".provinciaInstalacionD").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/distrito',
                data :  'idprovincia='+ idprovincia,
                success : function(data)
                {
                    $(".distritoInstalacionD").html(data)
                }
            });
            return false;
        }
        function distritos5(){
            var idprovincia = $(".provinciaInstalacionC").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/distrito',
                data :  'idprovincia='+ idprovincia,
                success : function(data)
                {
                    $(".distritoInstalacionC").html(data)
                }
            });
            return false;
        }

        if($('.dnioruconix').val()=="20550858461" ){
            $('.horaVer').prop('disabled', true);
        }

        $('#voucherImg').prop('disabled', false);
        $('.voucherImg').show('slow');
        $('#selectNa').prop('disabled', false);
        $('#entrega').prop('disabled', false);

        $('#tipo').on("change",function(){
            var tipoS=document.getElementById('tipo');
            value = tipoS.options[tipoS.selectedIndex].value;
            if (value=='Instalacion') {
//        $('#voucherImg').prop('disabled', false);
                $('.showTipo').show('slow');
            }else{
                $('.showTipo').hide();
            }

        });

        $('.lugarDeInstalacion').on("change",function(){
            var pendienteInstacion=document.getElementById("pendienteInstacion");
            if (pendienteInstacion.checked) {
                $('.cono').prop('disabled', true);
                $('.domo').prop('disabled', true);
                $('.tala').prop('disabled', true);
                $("#programacionHtml").html('');
                limpiarCapacidad();
            }else{
                $('.cono').prop('disabled', false);
                $('.domo').prop('disabled', false);
                $('.tala').prop('disabled', false);
            }
        })

        $('#pendienteInstacion').on("change",function(){
            var pendienteInstacion=document.getElementById("pendienteInstacion");
            if (pendienteInstacion.checked) {
                $('.cono').prop('disabled', true);
                $('.domo').prop('disabled', true);
                $('.tala').prop('disabled', true);
                $("#programacionHtml").html('');
                limpiarCapacidad();
            }else{
                $('.cono').prop('disabled', false);
                $('.domo').prop('disabled', false);
                $('.tala').prop('disabled', false);
            }
        });
        $('#selectNa').on("change",function () {
            var tipoDePagoSeleccionado=document.getElementById('selectNa');
            value = tipoDePagoSeleccionado.options[tipoDePagoSeleccionado.selectedIndex].value;
            if (value=="Boleta") {
                document.getElementById('rucFacturacion').removeAttribute("required");
                document.getElementById('razonSocialFacturacion').removeAttribute("required");
                document.getElementById('direccionFiscalFacturacion').removeAttribute("required");
                document.getElementById('direccionEntrega').removeAttribute("required");
            }
            if(value=="Factura"){
                ruc=$('#rucFacturacion').val();
                if (ruc.length==11) {

                }else{
                    alert("El ruc debe contener 11 caracteres actualmente tiene: "+ruc.length );
                }

                var rucFacturacion=document.getElementById('rucFacturacion');
                var razonSocialFacturacion=document.getElementById('razonSocialFacturacion');
                var direccionFiscalFacturacion=document.getElementById('direccionFiscalFacturacion');
                var direccionEntrega=document.getElementById('direccionEntrega');
                var att = document.createAttribute("required");
                var att1 = document.createAttribute("required");
                var att2 = document.createAttribute("required");
                var att3 = document.createAttribute("required");
                att.value = "true";
                rucFacturacion.setAttributeNode(att);
                razonSocialFacturacion.setAttributeNode(att1);
                direccionFiscalFacturacion.setAttributeNode(att2);
                direccionEntrega.setAttributeNode(att3);
            }

        });
    </script>
    <script>

        <?php $fechaA=date('Y-m-d') ?>
        jQuery.datetimepicker.setLocale('es');

        $('.fecha').datetimepicker({
            minDate:'<?= $fechaA ?>',
            format:'Y-m-d',
            scrollInput: false,
            scrollMonth : false,
            timepicker:false
        });


        $('#fechaEngrega').datetimepicker({
            minDate:'<?= $fechaA ?>',
            format:'Y/m/d',
            scrollInput: false,
            scrollMonth : false,
            timepicker:false
        });
        $('#horaEntrega').datetimepicker({
            datepicker:false,
            format:'H:i',
            allowTimes:['9:00','11:00','14:00','16:00']
        });

        $('.tiempo').typeahead({
            source: [
                {"name": "1 año"},
                {"name": "2 años"},
                {"name": "3 años"},
                {"name": "4 años"},
                {"name": "5 años"},
                {"name": "6 años"},
                {"name": "8 años"},
                {"name": "7 años"}
            ]
        });

        <?php if ($datos=DatosFacturacion::horariosSugeridosParaEntregaDeFactura()): ?>
        $('#horarioEntrega').typeahead({
            source: [
                <?php foreach ($datos as $var): ?>
                {"name": "<?php echo $var->fecha ?>"},
                <?php endforeach ?>
            ]
        });
        <?php endif ?>

        <?php if ($datosD=Cliente::otenerTodoLosDistritos()): ?>
        $('#distritoEntrega').typeahead({
            source: [
                <?php foreach ($datosD as $var): ?>
                {"name": "<?php echo $var->distrito ?>"},
                <?php endforeach ?>
            ]
        });
        <?php endif ?>

        <?php if ($datosDopeCon=Operacion::obtenerConcesionarioInstalacion()): ?>
        $('#concesionarioInstalacion').typeahead({
            source: [
                <?php foreach ($datosDopeCon as $var): ?>
                {"name": "<?php echo $var->nombreConcecionarioInstalicion ?>"},
                <?php endforeach ?>
            ]
        });
        <?php endif ?>

        function lstTProg(){
            var lugarI=$("input[name='lugarInstacion']:checked" ).val();
            var distrito="0";
            var tthinstala=$('#canthoraprog').val();
            var cita = 'General';
            var concecion="0";
            var lugarT='';

            if (lugarI=='Taller') {
                var fecha = $('#fechaIT').val();
                distrito=$("#distritoTallerid").val()
                console.log($("#distritoTallerid").val())
                lugarT='Taller';
            }else if (lugarI=='Domicilio Cliente') {
                var fecha = $('#fechaID').val();
                distrito=$('#distritoInstalacionD').val();
                lugarT='Domicilio';
            }else if (lugarI=='Concesionario') {
                var fecha = $('#fechaIC').val();
                var concecion=$('#concesionarioInstalacion').val();
                var cita = concecion;
                distrito=$('#distritoInstalacionC').val();
                lugarT='Concecionario';
            }

            var cod = document.getElementById("plan").value;
            servAd = $('[name="servad[]"]:checked').length;
            if (fecha!='' && cod!='' && servAd!=0 &&(distrito==0 || distrito!=null) && concecion!='') {
                $(".horabtn").off( "click" );
                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                $.ajax({
                    type : 'POST',
                    url  : '../includes/tbProgramacion',
                    data : {fecha:fecha,cita:cita,tthinstala:tthinstala,iddist:distrito,lugart:lugarT},
                    success : function(data)
                    {
                        $("#programacionHtml").html(data);
                        $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                        $(".horabtn").on("click",lstTProg).find(".horabtn");
                    }
                });
            }else{
                $("#programacionHtml").html('<h3 class="text-center text-info">Primero debe seleccionar Plan,Servicios Adicionales,fecha y/o Distrito y/o Nombre Concesionario.</h3>');
            }
            return false;
        }

        $(".horabtn").click(function(){
            lstTProg();
        });
        $("#mostrar").hide();
        $('.fecha').change(function(){
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');
        });

        <?php ?>

        $('#seguro').change(function(){
            var plan=$("#plan option:selected").text();
            var aseguradora=$("#seguro option:selected").text();
            if (plan=='E-call' || plan=='E-call Plus' || plan=='Proteccion') {
                //$('#campana').trigger("chosen:updated");
                $("#campana").val("");
                if(aseguradora=="Pacifico Seguros"){
                    // console.log(aseguradora+" aseguradora es");
                    $("#campana option[value='GpsSmart']").show();
                }else{
                    $("#campana option[value='GpsSmart']").hide();
                }
                $('.camapanaEsconder').show();
                $('#campana').prop('disabled',false);
            }else{
                $('.camapanaEsconder').hide();
                $('#campana').prop('disabled',true);
            }
        });

        $('#departamentoInstalacionD,#departamentoInstalacionC,#provinciaInstalacionD,#provinciaInstalacionC,#distritoInstalacionD,#distritoInstalacionC').change(function(){
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');
        });


        $('#plan').change(function(){
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');
            var arr="";
            var seleccionado = $("#plan").find('option:selected');
            var idtec = seleccionado.data('type');

            arr = $('[name="servad[]"]:checked').map(function(){
                return this.value;
            }).get();
            var str = arr.join(',');
            if(str==='' && (idtec === undefined)){
                str='0';
                idtec='0';
            }

            $.ajax({
                type : 'POST',
                url  : 'ajax/ServiciosAdicionales',
                data :  "idTec="+idtec,
                success : function(data)
                {
                    if(data!=''){
                        $(".servAdi").html(data);
                    }else{
                        console.log("No hay data");
                    }
                }
            });
            $("#canthoraprog").val("0");
            $(".canthoraprog2").text("0 Horas");
            /*$.ajax({
                type : 'POST',
                url  : 'ajax/cantidadHoraProgramacion',
                data :  'idCheck='+str+"&idTec="+idtec,
                success : function(data)
                {
                    if(data!=''){
                        $("#canthoraprog").val(data);
                        $(".canthoraprog2").text(data+" Horas");
                    }else{
                        console.log("No hay data");
                    }
                }
            });*/
            return false;

        });



        <?php if ($datos=ServiciosAdicionales::obtenerServicesAdicional()): ?>
        <?php foreach ($datos as $var): ?>
        $("#<?php echo $var->codigo ?>").on("change",function (e) {
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');

            var isChecked = e.currentTarget.checked;
            var seleccionado = $("#plan").find('option:selected');
            var idtec = seleccionado.data('type');
            //  alert(isChecked);
            var arr = $('[name="servad[]"]:checked').map(function(){
                return this.value;
            }).get();
            var str = arr.join(',');
            // $("#canthoraprog").val("0");
//    $(".canthoraprog2").text("0 Horas");
            if(!isChecked && (idtec === undefined)){
                str='0';
                idtec='0';
            }
            $.ajax({
                type : 'POST',
                url  : 'ajax/cantidadHoraProgramacion',
                data :  'idCheck='+str+"&idTec="+idtec,
                success : function(data)
                {
                    if(data!=''){
                        $("#canthoraprog").val(data);
                        $("#canthoraprog2").text(data+" Horas");
                    }else{
                        console.log("No hay data");
                    }
                }
            });
            return false;
        });
        <?php endforeach ?>
        <?php endif ?>


        $('.b').hide();
        $('.tiendaAparece').hide();
        $('.tiendaAparece1').hide();
        function validarComisionPorIdcreador(ideje){
            if (ideje=='<?= $session->idusuario ?>') {
                $('#comision').prop('disabled', true);
            }else{
                $('#comision').prop('disabled', false);
            }
        }
        $('#tipoCanal').change( function() {
            var idjE=$('#idEjecutvio').val();
            if(this.value=='CONVENIO'){
                $('.b').show();
                $('.a').hide();
                $('.tiendaAparece1').hide();
                $('#convenio').on('change', function() {
                    if(this.value=='1'){
                        $('.tiendaAparece').show();
                        $('.tiendaAparece').prop('disabled', false);
                    }else{
                        $('.tiendaAparece').prop('disabled', true);
                        $('.tiendaAparece').hide();
                    }
                });
                $('#ejecutivoCanalN').prop('disabled', false);
                $('#comision').prop('checked', false);
                $('#comision').prop('disabled', true);
                $('#ejecutivoCanalN').prop('disabled', false);
                validarComisionPorIdcreador(idjE);
            }else if(this.value=='CONCESIONARIO'){
                $('.tiendaAparece1').show();
                $('#ejecutivoCanalN').prop('disabled', false);
                $('#comision').prop('disabled', false);
                $('.b').hide();
                $('.a').show();
                validarComisionPorIdcreador(idjE);
            }else if(this.value=='TRACKLINK OCTO'){
                $('.tiendaAparece1').hide('slow');
                $('#comision').prop('disabled', true);
                $('#ejecutivoCanalN').prop('disabled', true);
                $('.b').hide();
                $('.a').show();
            }else{
                $('#ejecutivoCanalN').prop('disabled', false);
                $('.tiendaAparece1').hide('slow');
                $('#comision').prop('disabled', false);
                $('.b').hide();
                $('.a').show();
                validarComisionPorIdcreador(idjE);
            }

        });

        $("#idEjecutvio").change(function() {
            var canal=$('#tipoCanal').val();
            var idje=$(this).val();
            if(canal=='TRACKLINK OCTO'){
                $('#comision').prop('disabled', true);
            }else{
                validarComisionPorIdcreador(idje);
            }
        });


    </script>

<?php endif ?>

    <?php
}
?>

<?php
if (isset($_POST['idTramaql'])) {
    include_once("../../includes/init.php");

    $lista = Operacion::obtenerFrecuentes($session->idusuario);
    $idTrama=limpiar(trim($_POST['idTramaql']));
    $varTI=TramaIntegracionQL::obtenerTramaIntegracion($idTrama,'');

    TramaIntegracionQL::asignarTrama($_POST['idTramaql']);

    $clienteObjModal=new Cliente;

    ?>

    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>

    <style>
        .ibox-content > .sk-spinner {
            display: none;
        }

        .ibox-content.sk-loading {
            position: relative;
        }
        .ibox-content.sk-loading:after {
            content: '';
            background-color: rgba(255, 255, 255, 0.7);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        .ibox-content.sk-loading > .sk-spinner {
            display: block;
            position: absolute;
            top: 40%;
            left: 0;
            right: 0;
            z-index: 2000;
        }
    </style>
    <div class="showOPERA">
        <div class="well" style="padding-top: 11px;padding-bottom: 10px; ">
            <?php if ($varTI): ?>
            <?php if (!is_null($varTI->idsuariocargo)) {
                $usuarioObj = Usuario::obtenerUsuario($varTI->idsuariocargo);
            } ?>

            <div class="col-md-12 text-center"><label  class='text-success' for="">A Cargo de : <?php if(!is_null($varTI->idsuariocargo)){echo $usuarioObj->usuarioNombre.' '.$usuarioObj->usuarioApellidoPaterno;}else{ echo 'Libre';}?></label></divas>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="Observaciones" >N° Documento</label>
                    <input name="dniORuc" style="border: none;" type="text" placeholder="Web" id="Observaciones" readonly class="border-bottom form-control dnioruconix"  value="<?= $varTI->num_documento ?>">

                </div>
                <div class="form-group col-md-5">
                    <?php $apellidos=utf8_encode($varTI->apellido_asegurado) ?>
                    <input type="hidden" name="nombres" value="<?= $varTI->nombre_asegurado ?>">
                    <input type="hidden" name="apellidos" value="<?= $apellidos ?>">
                    <input type="hidden" name="anioF" value="<?= $varTI->anio ?>">
                    <input type="hidden" id='idtramaf' name="idTramaql" value="<?= $idTrama ?>">
                    <label for="numeroTecnico" ><?= $varTI->tipo_documento!= "RUC"? 'Apellidos' : 'Razón Social' ?> </label>
                    <input  style="border: none;" type="hidden" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->nombre_asegurado)) ? "$apellidos"." "."$varTI->nombre_asegurado" : "$apellidos" ; ?> ">
                    <input  style="border: none;" type="text" readonly id="apeSegura" name="apeSegura" class="border-bottom form-control" required value="<?= $apellidos ?> ">

                </div>
                <?php if($varTI->tipo_documento != "RUC"){ ?>
                <div class="form-group col-md-4">
                    <label for="numeroTecnico" >Nombres :</label>
                    <input  style="border: none;" type="text" readonly id="nomSegura" name="nomSegura" class="border-bottom form-control" required value="<?= $varTI->nombre_asegurado ?> ">
                </div>
                <?php }?>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="numeroTecnico" >Dirección :</label>
                    <input  style="border: none;" type="text" readonly id="direcSegura" name="direcSegura" class="border-bottom form-control" required value="<?= $varTI->direccion ?> ">
                </div>
                <div class="form-group col-md-4">
                    <label for="numeroTecnico" >Correo :</label>
                    <input  style="border: none;" type="text" readonly id="correoSegura" name="correoSegura" class="border-bottom form-control" required value="<?= strtolower($varTI->correo) ?> ">
                </div>
                <div class="col-md-2"><label for="estado" >Celular</label>
                    <input style="border: none;" type="text" readonly name="celular" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->celular)) ? "$varTI->celular" : "" ; ?>" >
                </div>
                <div class="col-md-2"><label for="estado" >Telefono</label>
                    <input style="border: none;" type="text" readonly name="telefono5" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->num_telefono)) ? "$varTI->num_telefono" : "" ; ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4 text-danger">
                    <label for="plzdd" class=""># Póliza :</label>
                    <input  type="text" id='plzdd' readonly class="form-control border-bottom text-capitalize" required value="<?= $retVal = $varTI->policy ?>">
                </div>

                <div class="col-md-4 text-danger">
                    <label for="fechInivvv">Fecha Inicio Vigencia :</label>
                    <input style="border: none;" type="text" readonly placeholder="Fecha Inicio" id="fechInivvv" class="form-control border-bottom text-capitalize" value="<?= $retVal = $varTI->fecha_vigencia ?>">
                </div>
                <div class="col-md-4 text-danger">
                    <label for="fechInivvv">Fecha Fin Vigencia :</label>
                    <input style="border: none;" type="text" readonly placeholder="Fecha Fin" id="fechFInvvv" class="form-control border-bottom text-capitalize" value="<?= $retVal = $varTI->fecha_fin_vigencia ?>">
                </div>
            </div>
            <input type="hidden" value="<?php echo $retVal = (!empty($varTI->num_telefono)) ? "$varTI->num_telefono" : "" ; ?>" name="telefono">
            <input type="hidden" value="" name="telefono2">
            <input type="hidden" name="correoCliente" value="<?php echo $retVal = (!empty($varTI->correo)) ? "$varTI->correo," : "$varTI->correo" ;echo $retVal = (!empty($varTI->correo)) ? "$varTI->correo" : "" ;  ?>">

        </div>
        <hr>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="numeroTecnico" >Placa Vehiculo*</label>
                <input  style="border: none;"  type="text" placeholder="Placa" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->placa)) ? "$varTI->placa" : "" ; ?>"  >
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones">Marca *</label>
                <input required <?php echo $retVal = (!empty($varTI->marca)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->marca)) ? "$varTI->marca" : "" ; ?>" >
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones">Modelo*</label>
                <input required <?php echo $retVal = (!empty($varTI->modelo)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($varTI->modelo)) ? "$varTI->modelo" : "" ; ?>">
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones" >Color</label>
                <input readonly <?php echo $retVal = (!empty($varTI->color)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Color" id="Observaciones" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->color)) ? "$varTI->color" : "" ; ?>">
            </div>

        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="Observaciones" >Chasis*</label>
                <input required style="border: none;" type="text" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = $varTI->chasis; ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="Observaciones" >Motor*</label>
                <input required <?php echo $retVal = (!empty($varTI->motor)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Motor" id="Observaciones" name="motor" class="border-bottom form-control" value="<?php echo $retVal = $varTI->motor; ?>">
            </div>
            <input type="hidden" name="motor" value="<?php echo $retVal = $varTI->motor ?>">
            <div class="form-group col-md-4">
                <label for="seguro" class="">Seleccione Compañía de Seguros</label>
                <select  readonly style="border: none;" class="border-bottom form-control text-capitalize" required name="seguro" id="seguro">
                    <option class='text-capitalize' value="<?php echo $retVal = $varTI->compania ?>" selected ><?php echo $retVal = $varTI->compania ?></option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-3">
                <label for="Observaciones" >Año*</label>
                <input required <?php echo $retVal = (!empty($varTI->anio)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Año" id="anio" name="anioF" class="border-bottom form-control" value="<?php echo $retVal = $varTI->anio; ?>">
            </div>
            <div class="col-md-3">
                <label for="plan" class="">Plan *</label>
                <select readonly style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
                    <option class="" value="<?php echo $varTI->plan ?>"  selected ><?php echo $varTI->plan ?></option>
                </select>
            </div>

            <div class="form-group col-md-6 ">
                <?php $tt = TiempoProgramServAdic::obtenerTotalHorasProgramacion2($varTI->plan,$varTI->servicio_adicional)?>
                <p style="font-weight:bold">Servicios Adicionales *: <label class="canthoraprog2 label-success"> <?= $tt->total ?> Horas </label></p>
                <div class="servAdi">
                    <?php ServiciosAdicionales::obtenerServAdicionalSeleccionadoVerIdServ($varTI->servicio_adicional) ?>
                </div>
                <input type="hidden" id="canthoraprog" name="canthoraprog" value="<?= $tt->total ?>">
            </div>
        </div><br>
        <div class="row ">
            <div class="col-md-3">
                <label for="ejecutivoCanal" >Precio lista</label>
                <input  style="border: none;"  readonly type="text" placeholder="Precio lista" id="ejecutivoCanal" name="precioLista" class="border-bottom form-control"  value="<?php echo $varTI->precio_lista?>" >
            </div>
            <div class="col-md-3">
                <label for="time" >Tiempo *</label>
                <input  style="border: none;" readonly   type="text" placeholder="Tiempo *" id="time" name="tiempo" class=" tiempo border-bottom form-control"  autocomplete="off" value="<?php echo $varTI->tiempo ?>">
            </div>

            <div class="form-group col-md-6 camapanaEsconder">
                <label for="campana" class="">¿ Campaña Pacífico Gps Smart ?</label>
                <select  style="border: none;" readonly  class="tecnico border-bottom form-control" required name="campana" id="campana">
                    <option class="" value="<?php echo $varTI->campana ?>" selected><?php echo utf8_encode($varTI->campana)?></option>
                </select>
            </div>
            <input  style="border: none;"    type="hidden" placeholder="Tiempo *" id="idEjecutvio" name="idEjecutvio" class=" tiempo border-bottom form-control"  autocomplete="off" value="<?php echo $session->idusuario ?>">
            <input  style="border: none;" readonly   type="hidden" placeholder="Tiempo *" id="tipo" name="tipo" class=" tiempo border-bottom form-control"  autocomplete="off" value="Instalacion">
        </div>
        <br>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="tipoCanal" class="">Seleccione canal *</label>
                <select style="border: none;"  name="tipoCanal" readonly="" class="cd-selec form-control border-bottom " required id="tipoCanal">
                    <option class='text-capitalize' value='<?php echo $varTI->canal ?>'  selected ><?php echo $varTI->canal ?></option>
                </select>
            </div>
            <div class="form-group col-md-2 a">
                <label for="nombreCanal" class="">Canal nombre</label>
                <input style="border: none;" readonly type="text" placeholder="Canal nombre" id="nombreCanal" name="nombreCanal" class="border-bottom form-control text-uppercase nombreCanal " autocomplete="off" required=""   value="<?php echo $varTI->canal_nombre ?>">
                <input style="border: none;" type="hidden" placeholder="Tienda" id="tienda" name="tienda" class="border-bottom form-control text-uppercase tienda " autocomplete="off"  value="" >
                <input style="border: none;" type="hidden" placeholder="Tienda" id="convenio" name="convenio" class="border-bottom form-control text-uppercase tienda " autocomplete="off"  value="" >
                <input style="border: none;" type="hidden" placeholder="Tienda" id="tienda" name="tienda" class="border-bottom form-control text-uppercase tienda " autocomplete="off"  value="" >
                <input style="border: none;" type="hidden" placeholder="Cobra Comision" id="comision" name="comision" class="border-bottom form-control text-uppercase ejecutivoCanalN" autocomplete="off" value="<?php echo $varTI->cobra_comision ?>" >
            </div>
            <div class="form-group col-md-4">
                <label for="ejecutivoCanalN" class="">Ejecutivo canal</label>
                <input style="border: none;"  readonly type="text" placeholder="Ejecutivo canal" id="ejecutivoCanalN" name="ejecutivoCanalN" class="border-bottom form-control text-uppercase ejecutivoCanalN" autocomplete="off" value="<?php echo $varTI->ejecutivo_canal ?>" >
            </div>
        </div>
        <div class="row">
            <h3 class="col-md-6">Datos de facturación </h3>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="Forma*" >Forma Pago</label>
                <select style="border: none;" id="formaPago" name="formaPago" class="form-control border-bottom" required  readonly >
                    <option value="<?php echo $varTI->forma_pago ?>" selected><?php echo $varTI->forma_pago ?></option>
                </select>
            </div>
            <div class="col-md-4">
                <label for="Forma*" >Documento Pago</label>
                <select style="border: none;" id="selectNa" name="tipoDocumentoFacturacion" class="cd-selec form-control border-bottom" required readonly >
                    <option value="<?php echo $varTI->doc_factura ?>" selected><?php echo $varTI->doc_factura ?></option>
                </select>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-8">
                <div class=" eFactura" style="">
                    <div class="row"><br>
                        <div class="form-group col-md-4">
                            <label for="rucFacturacion" class="">Ruc *</label>
                            <input readonly style="border: none;" type="number" placeholder="Ruc *" id="rucFacturacion" name="rucFacturacion" class="form-control x2 border-bottom" maxlength="12" minlength="10" value="<?= $retVal = $varTI->ruc_factura; ?>"  >
                        </div>
                        <div class="form-group col-md-8">
                            <label for="razonSocialFacturacion" class="">Razón Social *</label>
                            <input readonly style="border: none;" type="text" placeholder="Razón Social *" id="razonSocialFacturacion" name="razonSocialFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = $varTI->razonSocial_Factura; ?>" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-8">
                            <label for="direccionFiscalFacturacion" class="sr-only">Dirección Fiscal *</label>
                            <input readonly style="border: none;" type="text" placeholder="Dirección Fiscal *" id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="form-control border-bottom text-capitalize" value="<?= $varTI->direccion_factura ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="telefonoFacturacion" class="sr-only">Teléfono</label>
                            <input style="border: none;" readonly type="text" placeholder="Teléfono" id="telefonoFacturacion" name="telefonoFacturacion" class="form-control border-bottom text-capitalize" value="">

                            <input style="border: none;" type="hidden" placeholder="Dirección de engrega" id="direccionEntrega" name="direccionEntrega" class="form-control border-bottom text-capitalize" value="">
                            <input style="border: none;" type="hidden" placeholder="Referencia de engrega" id="referenciaEntrega" name="referenciaEntrega" class="form-control border-bottom text-capitalize" required="" value="">
                            <input style="border: none;" type="hidden" placeholder="Distrito de entrega" id="distritoEntrega" name="distritoEntrega" class="form-control border-bottom text-capitalize" required autocomplete="off" value="">
                            <input style="border: none;" type="hidden" placeholder="Horario de entrega" id="horarioEntrega" name="horarioEntrega" class="form-control border-bottom text-capitalize" autocomplete="off" required value="">
                            <input style="border: none;" type="hidden" placeholder="Persona de recepción de factura" id="personaEntrega" name="personaEntrega" class="form-control border-bottom text-capitalize" required value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="fechIni" class="text-success">Fecha Inicio Vigencia :</label>
                <input style="border: none;" type="text" readonly placeholder="Fecha Inicio" id="fechIni" name="fechIni" class="form-control border-bottom text-capitalize" required value="<?= $retVal = $varTI->fecha_vigencia ?>">
            </div>
            <div class="col-md-4">
                <label for="Poliza" class="text-success"># Póliza :</label>
                <input style="border: none;" type="text" readonly placeholder="Poliza" id="Poliza" name="Poliza" class="form-control border-bottom text-capitalize" required value="<?= $retVal = $varTI->policy ?>">
            </div>
        </div>
        <br>
        <div class="showTipo">
            <div class="row">
                <h3 class="col-md-6">Datos de instalación</h3>
                <div class="col-md-6">
                    <label for="pendienteInstacion">Pendiente datos de instalación</label>
                    <div class="switch">
                        <div class="onoffswitch">
                            <input type="checkbox"  class="onoffswitch-checkbox" id="pendienteInstacion" value="si" name="pendienteInstacion">
                            <label class="onoffswitch-label" for="pendienteInstacion">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tablist">
                <input type="radio" title="taller" name="lugarInstacion" class='lugarDeInstalacion' id="taller" role="tab" aria-controls="taller-panel" checked value="Taller" class="lugarDeInstalacion tall" />
                <input type="radio" title="domicilio" name="lugarInstacion"  class='lugarDeInstalacion' id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente" class="lugarDeInstalacion domm" />
                <input type="radio" title="concesionario" name="lugarInstacion" class='lugarDeInstalacion'  id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario" class="lugarDeInstalacion conn" >

                <div class="tabs" aria-hidden="true">
                    <label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
                    <label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
                    <label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
                </div>
                <div class="panels">
                    <ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">
                        <h2><?= $lista->lugarInstalacion ?> | <?= $lista->observacion ?></h2>
                        <p><?= $lista->direccionInstacion  ?></p>
                        <p>
                            <?php $dep=$clienteObjModal->obtenerDepartamentoPorId($lista->departamentoi) ?><?= $dep->departamento ?>
                            &nbsp;|&nbsp;
                            <?php $prov=$clienteObjModal->otenerProvinciaPorId($lista->provinciai) ?><?php echo $prov->provincia ?>
                            &nbsp;|&nbsp;
                            <?php $di=$clienteObjModal->obtenerDistritoPorId($lista->distritoi) ?><?= $di->distrito ?>
                        </p>

                        <input type="hidden" name="direccionTaller" value="<?= $lista->direccionInstacion  ?>">
                        <input type="hidden" name="disT" id="distritoTallerid" value="<?= $lista->distritoi  ?>">
                        <input type="hidden" name="proT" value="<?= $lista->provinciai  ?>">
                        <input type="hidden" name="depT" value="<?= $lista->departamentoi  ?>">

                        <div class="row">
                            <div class="row">
                                <div class="row" >
                                    <div class="form-group col-md-4">
                                        <label  class="">Fecha</label>
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control tal tala fecha"  id="fechaIT" name="fechaIT" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="turno" class="">Turno a</label>
                                        <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn form-control border-bottom btn btn-info" >Seleccionar hora</button>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posT" class="hora form-control border-bottom"  required="" readonly>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRT" class="delR form-control border-bottom"  required="" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="horaS" class="">Hora seleccionada</label>
                                        <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaT" class="horaVer tala tal form-control border-bottom"  required="" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab">
                        <h2>Domicilio Cliente</h2>
                        <div class="row">
                            <div class="row">
                                <div class="form-group  col-md-4">
                                    <label for="departamentoInstalacionD" class="">Departamento*</label>
                                    <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize dom domo" required name="departamentoInstalacionD" id="departamentoInstalacionD">
                                        <option class="text-capitalize" value="" disabled selected >Seleccione Departamento* </option>
                                        <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                                        <?php if ($objDepartamento): ?>
                                            <?php foreach ($objDepartamento as $var): ?>
                                                <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                                            <?php endforeach ?>
                                        <?php else: ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="provinciaInstalacionD" class="">Provincia*</label>
                                    <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize dom domo" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="distritoInstalacionD" class="">Distrito*</label>
                                    <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize dom domo" required name="distritoInstalacionD"  id="distritoInstalacionD" >
                                    </select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group  col-md-12">
                                    <label for="direccionInstalacionD" class="sr-only">Direccion*</label>
                                    <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionD" name="direccionInstalacionD"  class="form-control border-bottom dom domo" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for="referenciaInstalacionD" class="sr-only">Referencia *</label>
                                    <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" required="" name="referenciaInstalacionD" class="form-control border-bottom dom domo"  >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="cod" class="sr-only">Cordenadas Domicilio *</label>
                                    <input style="border: none;" type="text" placeholder="Cordenadas Domicilio *" id="cod" name="cordenadasD" class="form-control x2 text-capitalize border-bottom dom domo" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label  class="">Fecha</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control dom domo fecha"  id="fechaID" name="fechaID" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="turno" class="">Turno b</label>
                                    <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Seleccionar hora</button>
                                    <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posD" class="hora form-control border-bottom"  required="" readonly>
                                    <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRD" class="delR form-control border-bottom"  required="" readonly>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="horaS" class="">Hora seleccionada</label>
                                    <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaD" class="horaVer domo dom  form-control border-bottom"  required="" readonly>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">

                        <div class="row">
                            <div class="col-md-6"><h2>Concesionario</h2></div>
                            <div class="col-md-6">
                                <label for="concesionarioInstalacion" class="sr-only">Nombre de concesionario*</label>
                                <input style="border: none;" type="text" placeholder="Nombre de concesionario*" id="concesionarioInstalacion" name="concesionarioInstalacion" class="form-control border-bottom con" required="" >
                            </div>
                        </div>

                        <div class="row">
                            <div class="row">
                                <div class="form-group  col-md-4">
                                    <label for="departamentoInstalacionC" class="">Departamento*</label>
                                    <select  style="border: none;" class="departamentoInstalacionC border-bottom form-control text-capitalize con cono" required name="departamentoInstalacionC" id="departamentoInstalacionC">
                                        <option class="text-capitalize" disabled selected >Seleccione Departamento</option>
                                        <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                                        <?php if ($objDepartamento): ?>
                                            <?php foreach ($objDepartamento as $var): ?>
                                                <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                                            <?php endforeach ?>
                                        <?php else: ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="provinciaInstalacionC" class="">Provincia*</label>
                                    <select  style="border: none;" class="provinciaInstalacionC border-bottom form-control text-capitalize con cono" required name="provinciaInstalacionC" id="provinciaInstalacionC" >
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="distritoInstalacionC" class="">Distrito*</label>
                                    <select  style="border: none;" class="distritoInstalacionC border-bottom form-control text-capitalize con cono" required name="distritoInstalacionC"  id="distritoInstalacionC" > 		</select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for="direccionInstalacionC" class="">Direccion*</label>
                                    <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom con cono" >
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for="referenciaInstalacionC" class="">Referencia *</label>
                                    <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom con cono" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label  class="">Fecha</label>
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control con cono fecha"  id="fechaIC" name="fechaIC" autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="turno" class="">Turno c</label>
                                        <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Seleccionar hora</button>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posC" class="hora form-control border-bottom"  required="" readonly>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRC" class="delR form-control border-bottom"  required="" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="horaS" class="">Hora seleccionada</label>
                                        <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaC" class="horaVer cono con form-control border-bottom"  required="" readonly>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </ul>
                </div>
            </div>
            <div class="ibox" id="ibox1">
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-double-bounce">
                        <div class="sk-double-bounce1"></div>
                        <div class="sk-double-bounce2"></div>
                    </div>
                    <div id="programacionHtml">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="nombreContacto" class="">Nombre Contacto *</label>
                    <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContacto" name="nombreContacto" class="form-control x2 text-capitalize border-bottom " required="" value="<?= $varTI->apellido_asegurado." ".$varTI->nombre_asegurado ?>"" >
                </div>
                <div class="form-group col-md-6">
                    <label for="numeroContacto" class="">Número Contacto *</label>
                    <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContacto" name="numeroContacto" class="form-control x2 text-capitalize border-bottom" required="" value="<?= $varTI->celular ?>"">
                </div>
            </div>

            <hr>
            <div class="row">

                <div class="form-group col-md-12">

                    <label for="seguimientoTrama" class="">Seguimiento</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Seguimiento" id="seguimientoTrama" >
                        <span class="input-group-btn">
                    <button type="button" id="registrarSeguiminto" class="btn btn-primary">Registrar Seguimiento </button>
                </span>
                    </div>
                </div>

            </div>

            <ul id='segimientohtml'>
                <?php foreach(TramaIntegracionQL::getSEg($idTrama) as $var ): ?>
                    <li><?= $var->fecha ?> <b><?= $var->body ?></b></li>
                <?php endforeach ?>
            </ul>
            <hr>
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="observaciong" class="">Observaciones </label>
                    <input style="border: none;" type="text" placeholder="Observaciones " id="observaciong" name="observaciong" class="form-control x2 border-bottom focusUrgenteta" >
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
            <button type="submit" name="crearDesdeChasis" id="agregarVehiculoBttn" class="btn btn-primary" value="<?php echo Token::generar() ?>">Confirmar</button>
        </div>
    </div>
    <script src="../../assets/js/registrarme.js"></script>
    <script src="../../assets/js/validacionFactura.pabel.js"></script>
    <script src="../../assets/bootbox/bootbox.all.js"></script>
    <script>

        function validar(){
            var lugarI=$( "input[name='lugarInstacion']:checked" ).val()
            if (lugarI=='Taller') {
                $('.con').prop('disabled', true);
                $('.dom').prop('disabled', true);
                $('.tal').prop('disabled', false);
            }else if (lugarI=='Domicilio Cliente') {
                $('.con').prop('disabled', true);
                $('.dom').prop('disabled', false);
                $('.tal').prop('disabled', true);
            }else if (lugarI=='Concesionario') {
                $('.con').prop('disabled', false);
                $('.dom').prop('disabled', true);
                $('.tal').prop('disabled', true);
            }
        }

        $(document).ready(function(){
            $( "input[name='lugarInstacion']" ).on( "click",function(){
                $('#programacionHtml').html('');
                $('.horaVer').val('');
                $('.fecha').val('');
                validar();
            });
            validar();
        });


    </script>
    <script>
        $(".departamentoInstalacionD").change(function(){provincias4();});
        $(".departamentoInstalacionC").change(function(){provincias5();});
        $(".provinciaInstalacionD").change(function(){distritos4();});
        $(".provinciaInstalacionC").change(function(){distritos5();});
        function provincias4(){
            var iddepartamento = $(".departamentoInstalacionD").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/provincia',
                data :  'iddepartamento='+ iddepartamento,
                success : function(data)
                {
                    $(".provinciaInstalacionD").html(data);
                }
            });
            return false;
        }
        function provincias5(){
            var iddepartamento = $(".departamentoInstalacionC").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/provincia',
                data :  'iddepartamento='+ iddepartamento,
                success : function(data)
                {
                    $(".provinciaInstalacionC").html(data);
                }
            });
            return false;
        }

        function distritos4(){
            var idprovincia = $(".provinciaInstalacionD").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/distrito',
                data :  'idprovincia='+ idprovincia,
                success : function(data)
                {
                    $(".distritoInstalacionD").html(data)
                }
            });
            return false;
        }
        function distritos5(){
            var idprovincia = $(".provinciaInstalacionC").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/distrito',
                data :  'idprovincia='+ idprovincia,
                success : function(data)
                {
                    $(".distritoInstalacionC").html(data)
                }
            });
            return false;
        }



        $('.lugarDeInstalacion').on("change",function(){
            var pendienteInstacion=document.getElementById("pendienteInstacion");
            if (pendienteInstacion.checked) {
                $('.cono').prop('disabled', true);
                $('.domo').prop('disabled', true);
                $('.tala').prop('disabled', true);
                $("#programacionHtml").html('');
                limpiarCapacidad();
            }else{
                $('.cono').prop('disabled', false);
                $('.domo').prop('disabled', false);
                $('.tala').prop('disabled', false);
            }
        })


        $('#pendienteInstacion').on("change",function(){
            var pendienteInstacion=document.getElementById("pendienteInstacion");
            if (pendienteInstacion.checked) {
                $('.cono').prop('disabled', true);
                $('.domo').prop('disabled', true);
                $('.tala').prop('disabled', true);
                $("#programacionHtml").html('');
                limpiarCapacidad();
            }else{
                $('.cono').prop('disabled', false);
                $('.domo').prop('disabled', false);
                $('.tala').prop('disabled', false);
            }
        });

        function validaDP() {
            // var dp=1;
            var lugarI = $("input[name='lugarInstacion']:checked").val();
            if (lugarI == 'Taller') {
                var fecha = $('#fechaIT').val();
            } else if (lugarI == 'Domicilio Cliente') {
                var fecha = $('#fechaID').val();
            } else if (lugarI == 'Concesionario') {
                var fecha = $('#fechaIC').val();
            }
            var hora = $(".horaVer").val();
            var canthoraprog = $('#canthoraprog').val();
            var idtec = $('input[name="posT"]').val();
            // console.log("fecha: "+fecha+" idtec:"+idtec+" canhora:"+canthoraprog.trim());
            $.ajax({
                type: 'POST',
                url: 'ajax/validarRegistroTB',
                data:'tipo=save&fecha='+fecha+'&idtec='+idtec+"&canthoraprog=" + canthoraprog.trim() + "&hora=" + hora,
                // data :  'tipo=save&fecha=2019-11-27&idtec=2&canthoraprog=2&hora=12:00',
                success: function (data) {
                    if (data.trim() == 'dp') {
                        //dp='1';
                        $("#agregarVehiculoBttn").prop("disabled",true);
                        //      clearInterval(eventos.interval);
                        $("#programacionHtml").html('');
                        $(".horabtn").html('Seleccionar hora');
                        $(".horaVer").val('');
                        // fn(dp);
                        bootbox.alert({
                            title: "<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                            message: "<label>El horario Seleccionado("+hora+") ya fue Registrado,Por Favor Seleccione otro*</label>",
                            size: 'small',
                            callback: function () {
                                /*  l.ladda( 'stop' );*/
                            }
                        });
                    }else if(data.trim()=='Ok'){
                        $("#agregarVehiculoBttn").prop("disabled",false)
                    }
                }
            });
        }

        var eventos={};
        eventos.frejcar =function(){
            validaDP();
        };
        eventos.interval = setInterval(eventos.frejcar, 3000);

        $(document).ready(function () {

            $('#agregarVehiculoBttn').click(function() {

                checked = $('[name="servad[]"]:checked').length;

                if(checked==0) {
                    bootbox.alert({
                        title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                        message: "<label>Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*</label>",
                        size: 'small',
                        callback: function () {
                            /*  l.ladda( 'stop' );*/
                        }
                    });
                    /* alert("Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*");*/
                    console.log("Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*");
                    // $('[name="servad[]"]:checked')[0].focus();
                    return false;
                }

            });
        });
    </script>
    <script>

        <?php $fechaA=date('Y-m-d') ?>
        jQuery.datetimepicker.setLocale('es');

        $('.fecha').datetimepicker({
            minDate:'<?= $fechaA ?>',
            format:'Y-m-d',
            scrollInput: false,
            scrollMonth : false,
            timepicker:false
        });


        $('#fechaEngrega').datetimepicker({
            minDate:'<?= $fechaA ?>',
            format:'Y/m/d',
            scrollInput: false,
            scrollMonth : false,
            timepicker:false
        });
        $('#horaEntrega').datetimepicker({
            datepicker:false,
            format:'H:i',
            allowTimes:['9:00','11:00','14:00','16:00']
        });

        $('.tiempo').typeahead({
            source: [
                {"name": "1 año"},
                {"name": "2 años"},
                {"name": "3 años"},
                {"name": "4 años"},
                {"name": "5 años"},
                {"name": "6 años"},
                {"name": "8 años"},
                {"name": "7 años"}
            ]
        });

        <?php if ($datos=DatosFacturacion::horariosSugeridosParaEntregaDeFactura()): ?>
        $('#horarioEntrega').typeahead({
            source: [
                <?php foreach ($datos as $var): ?>
                {"name": "<?php echo $var->fecha ?>"},
                <?php endforeach ?>
            ]
        });
        <?php endif ?>

        <?php if ($datosD=Cliente::otenerTodoLosDistritos()): ?>
        $('#distritoEntrega').typeahead({
            source: [
                <?php foreach ($datosD as $var): ?>
                {"name": "<?php echo $var->distrito ?>"},
                <?php endforeach ?>
            ]
        });
        <?php endif ?>

        <?php if ($datosDopeCon=Operacion::obtenerConcesionarioInstalacion()): ?>
        $('#concesionarioInstalacion').typeahead({
            source: [
                <?php foreach ($datosDopeCon as $var): ?>
                {"name": "<?php echo $var->nombreConcecionarioInstalicion ?>"},
                <?php endforeach ?>
            ]
        });
        <?php endif ?>

        $("#registrarSeguiminto").click(function(){
            // alert(servAd) ;seguimientoTrama
            var body=$('#seguimientoTrama').val();
            var idtrama = $("#idtramaf").val();
            var today = new Date();
            var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()+' '+today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            if(body.length>0){
                $.ajax({
                    type : 'POST',
                    url  : 'ajax/registrarSegQL',
                    data : {body:body,idtrama:idtrama},
                    success : function(data)
                    {
                        $('#seguimientoTrama').val('');
                        $("#segimientohtml").append('<li> '+date+' <b>'+body+'</b></li>');
                        //segimientohtml
                    }
                });
            }
            return false;
        });

        function lstTProg(){
            var lugarI=$("input[name='lugarInstacion']:checked" ).val();
            var distrito="0";
            var tthinstala=$('#canthoraprog').val();
            var cita = 'General';
            var concecion="0";
            var lugarT='';

            if (lugarI=='Taller') {
                var fecha = $('#fechaIT').val();
                distrito=$("#distritoTallerid").val()

                console.log($("#distritoTallerid").val())
                lugarT='Taller';
            }else if (lugarI=='Domicilio Cliente') {
                var fecha = $('#fechaID').val();
                distrito=$('#distritoInstalacionD').val();
                lugarT='Domicilio';
            }else if (lugarI=='Concesionario') {
                var fecha = $('#fechaIC').val();
                var concecion=$('#concesionarioInstalacion').val();
                var cita = concecion;
                distrito=$('#distritoInstalacionC').val();
                lugarT='Concesionario';
            }
            var cod = document.getElementById("plan").value;
            servAd = $('[name="servad[]"]:checked').length;
            if (fecha!='' && cod!='' && servAd!=0 &&(distrito==0 || distrito!=null) && concecion!='') {
                $(".horabtn").off( "click" );
                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                $.ajax({
                    type : 'POST',
                    url  : '../includes/tbProgramacion',
                    data : {fecha:fecha,cita:cita,tthinstala:tthinstala,iddist:distrito,lugart:lugarT},
                    success : function(data)
                    {
                        $("#programacionHtml").html(data);
                        $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                        $(".horabtn").on("click",lstTProg).find(".horabtn");
                    }
                });
            }else{
                $("#programacionHtml").html('<h3 class="text-center text-info">Primero debe seleccionar Plan,Servicios Adicionales,fecha y/o Distrito y/o Nombre Concesionario.</h3>');
            }
            return false;
        }

        $(".horabtn").click(function(){
            lstTProg();
        });

        $('.fecha').change(function(){
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');
        });
        $("#concesionarioInstalacion").blur(function(){
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');
        });


        $('#departamentoInstalacionD,#departamentoInstalacionC,#provinciaInstalacionD,#provinciaInstalacionC,#distritoInstalacionD,#distritoInstalacionC').change(function(){
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');
        });

    </script>

<?php endif ?>

    <?php
}
?>

<?php
if (isset($_POST['idTramaty'])) {
    include_once("../../includes/init.php");

    $lista = Operacion::obtenerFrecuentes($session->idusuario);
    $idTrama=limpiar(trim($_POST['idTramaty']));
    $varTI=TramaIntegracionTY::obtenerTramaIntegracion($idTrama,'');

    $idPlan = 0;
    if(strpos($varTI->plan,"PERSONAS")){
        $idPlan=20;
    }
    if(strpos($varTI->plan,"EMPRESAS")){
        $idPlan=20;
    }
    $precioTra = TramaIntegracionTY::getPrecioPlan($idPlan,$varTI->tiempo);
    TramaIntegracionTY::asignarTrama($_POST['idTramaty']);

    $clienteObjModal=new Cliente;

    ?>

    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>

    <style>
        .ibox-content > .sk-spinner {
            display: none;
        }

        .ibox-content.sk-loading {
            position: relative;
        }
        .ibox-content.sk-loading:after {
            content: '';
            background-color: rgba(255, 255, 255, 0.7);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        .ibox-content.sk-loading > .sk-spinner {
            display: block;
            position: absolute;
            top: 40%;
            left: 0;
            right: 0;
            z-index: 2000;
        }
    </style>
    <div class="showOPERA">
        <div class="well" style="padding-top: 11px;padding-bottom: 10px; ">
            <?php if ($varTI): ?>
            <?php if (!is_null($varTI->idsuariocargo)) {
                $usuarioObj = Usuario::obtenerUsuario($varTI->idsuariocargo);
            } ?>

            <div class="col-md-12 text-center"><label  class='text-success' for="">A Cargo de : <?php if(!is_null($varTI->idsuariocargo)){echo $usuarioObj->usuarioNombre.' '.$usuarioObj->usuarioApellidoPaterno;}else{ echo 'Libre';}?></label></divas>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="Observaciones" ><?= $varTI->tipo_documento?> </label>
                    <input name="dniORuc" style="border: none;" type="text" placeholder="Web" id="Observaciones" readonly class="border-bottom form-control dnioruconix"  value="<?= $varTI->num_documento ?>">

                </div>
                <div class="form-group col-md-5">
                    <?php $apellidos=utf8_encode($varTI->apellido_asegurado) ?>
                    <input type="hidden" name="nombres" value="<?= $varTI->nombre_asegurado ?>">
                    <input type="hidden" name="apellidos" value="<?= $apellidos ?>">
                    <input type="hidden" name="anioF" value="<?= $varTI->anio ?>">
                    <input type="hidden" id='idTramaty' name="idTramaty" value="<?= $idTrama ?>">
                    <label for="numeroTecnico" >Apellidos :</label>
                    <input  style="border: none;" type="hidden" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->nombre_asegurado)) ? "$apellidos"." "."$varTI->nombre_asegurado" : "$apellidos" ; ?> ">
                    <input  style="border: none;" type="text" readonly id="apeSegura" name="apeSegura" class="border-bottom form-control" required value="<?= $apellidos ?> ">

                </div>
                <div class="form-group col-md-4">
                    <label for="numeroTecnico" >Nombres :</label>
                    <input  style="border: none;" type="text"  id="nomSegura" name="nomSegura" class="border-bottom form-control"  value="<?= $varTI->nombre_asegurado ?> ">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="numeroTecnico" >Dirección :</label>
                    <input  style="border: none;" type="text" readonly id="direcSegura" name="direcSegura" class="border-bottom form-control" required value="<?= $varTI->direccion ?> ">
                </div>
                <div class="form-group col-md-4">
                    <label for="numeroTecnico" >Correo :</label>
                    <input  style="border: none;" type="text"  id="correoSegura" name="correoSegura" class="border-bottom form-control"  value="<?= strtolower($varTI->correo) ?> ">
                </div>
                <div class="col-md-2"><label for="estado" >Celular(s)</label>
                    <input style="border: none;" type="text" readonly name="celular" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal =((!empty($varTI->celular)) ? "$varTI->celular": "").((!empty($varTI->celular2)) ? ",".($varTI->celular2=='NULL' ? '':$varTI->celular2):""); ?>" >
                </div>
                <div class="col-md-2"><label for="estado" >Telefono</label>
                    <input style="border: none;" type="text" readonly name="telefono5" id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->num_telefono)) ? "$varTI->num_telefono" : "" ; ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4 text-danger">
                    <label for="mailocto11" >CORREO OCTO (CC) :</label>
                    <input type="text" id="mailocto11" readonly class=" danger border-bottom form-control" required value="toyota-sftp@tracklink.pe">
                </div>
                <div class="form-group col-md-4 text-danger">
                    <label for="mailocto11" >Concesionario madre:</label>
                    <input type="text" id="dlMadre" readonly class=" border-bottom form-control" value="<?= $varTI->dlr_madre ?>">
                </div>
                <div class="form-group col-md-4 ">
                    <label for="mailocto11" >Ejecutivo:</label>
                    <input type="text" id="dlMadre" readonly class=" border-bottom form-control" value="<?= $varTI->ejecutivo_venta ?>">
                </div>
            </div>
            <input type="hidden" value="<?php echo $retVal = (!empty($varTI->num_telefono)) ? "$varTI->num_telefono" : "" ; ?>" name="telefono">
            <input type="hidden" value="" name="telefono2">
            <input type="hidden" name="correoCliente" value="<?php echo $retVal = (!empty($varTI->correo)) ? "$varTI->correo," : "$varTI->correo" ;echo $retVal = (!empty($varTI->correo)) ? "$varTI->correo" : "" ;  ?>">

        </div>
        <hr>
        <div class="row">
            <div class="form-group col-md-2">
                <label for="numeroTecnico" >Placa Vehiculo*</label>
                <input  style="border: none;"  type="text" readonly placeholder="Placa" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->placa)) ? "$varTI->placa" : "" ; ?>"  >
            </div>
            <div class="form-group col-md-2">
                <label for="numeroTecnico" >CBU*</label>
                <input  style="border: none;"  type="text"  readonly placeholder="Placa" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($varTI->cbu)) ? "$varTI->cbu" : "" ; ?>"  >
            </div>
            <div class="form-group col-md-2">
                <label for="Observaciones">Marca *</label>
                <input readonly required <?php echo $retVal = (!empty($varTI->marca)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->marca)) ? "$varTI->marca" : "" ; ?>" >
            </div>
            <div class="form-group col-md-2">
                <label for="Observaciones">Modelo*</label>
                <input readonly required <?php echo $retVal = (!empty($varTI->modelo)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($varTI->modelo)) ? "$varTI->modelo" : "" ; ?>">
            </div>
            <div class="form-group col-md-2">
                <label for="Observaciones" >Version</label>
                <input readonly <?php echo $retVal = (!empty($varTI->version)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Version" id="Observaciones" name="version" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->version)) ? "$varTI->version" : "" ; ?>">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="Observaciones" >Chasis*</label>
                <input required style="border: none;" type="text" readonly placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = $varTI->chasis; ?>">
            </div>
            <div class="form-group col-md-3">
                <label for="Observaciones" >Motor*</label>
                <input required <?php echo $retVal = (!empty($varTI->motor)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Motor" id="Observaciones" name="motor" class="border-bottom form-control" value="<?php echo $retVal = $varTI->motor; ?>">
            </div>
            <input type="hidden" name="motor" value="<?php echo $retVal = $varTI->motor ?>">

            <div class="form-group col-md-3">
                <label for="Observaciones" >Color</label>
                <input readonly <?php echo $retVal = (!empty($varTI->color)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Color" id="Observaciones" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($varTI->color)) ? "$varTI->color" : "" ; ?>">
            </div>
            <div class="form-group col-md-3">
                <label for="seguro" class="">Seleccione Compañía de Seguros</label>
                <select  readonly style="border: none;" class="border-bottom form-control text-capitalize" required name="seguro" id="seguro">
                    <option class='text-capitalize' value="<?php echo $retVal = $varTI->compania ?>" selected ><?php echo $retVal = $varTI->compania ?></option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-3">
                <label for="Observaciones" >Año Fabricación*</label>
                <input required <?php echo $retVal = (!empty($varTI->anio)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Año" id="anio" name="anioF" class="border-bottom form-control" value="<?php echo $retVal = $varTI->anio; ?>">
            </div>
            <div class="col-md-3">
                <label for="plan" class="">Plan *</label>
                <select readonly style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
                    <option class="" value="<?php echo $varTI->plan ?>"  selected ><?php echo $varTI->plan ?></option>
                </select>
            </div>

            <div class="form-group col-md-6 ">
                <?php $tt = TiempoProgramServAdic::obtenerTotalHorasProgramacion2($varTI->plan,$varTI->servicio_adicional)?>
                <p style="font-weight:bold">Servicios Adicionales *: <label class="canthoraprog2 label-success"> <?= $tt->total ?> Horas </label></p>
                <div class="servAdi">
                    <?php ServiciosAdicionales::obtenerServAdicionalSeleccionadoVerIdServ($varTI->servicio_adicional) ?>
                </div>
                <input type="hidden" id="canthoraprog" name="canthoraprog" value="<?= $tt->total ?>">
            </div>
        </div><br>

        <div class="row ">
            <div class="form-group col-md-2">
                <label for="Observaciones" >Año Modelo*</label>
                <input required <?php echo $retVal = (!empty($varTI->anio_modelo)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Año" id="anio_modelo" name="anioMD" class="border-bottom form-control" value="<?php echo $retVal = $varTI->anio_modelo; ?>">
            </div>
            <div class="col-md-2">
                <label for="ejecutivoCanal" >Precio Tracklink</label>
                <input  style="border: none;"  readonly required type="text" placeholder="Precio lista" id="ejecutivoCanal" name="precioLista" class="border-bottom form-control"  value="<?=($precioTra)?($precioTra->precio):""?>" >
            </div>
            <div class="col-md-2">
                <label for="ejecutivoCanal" >Precio Toyota</label>
                <input  style="border: none;"  readonly type="text" placeholder="Precio lista" id="ejecutivoCanal" name="precioListaTy" class="border-bottom form-control"  value="<?php echo $varTI->precio_lista?>" >
            </div>
            <div class="col-md-3">
                <label for="time" >Tiempo *</label>
                <input  style="border: none;" readonly required   type="text" placeholder="Tiempo *" id="time" name="tiempo" class=" tiempo border-bottom form-control"  autocomplete="off" value="<?php echo $varTI->tiempo ?>">
            </div>

            <div class="form-group col-md-3 camapanaEsconder">
                <label for="campana" class="">¿ Campaña Pacífico Gps Smart ?</label>
                <select  style="border: none;" readonly  class="tecnico border-bottom form-control" required name="campana" id="campana">
                    <option class="" value="<?php echo $varTI->campana ?>" selected><?php echo utf8_encode($varTI->campana)?></option>
                </select>
            </div>
            <input  style="border: none;"    type="hidden" placeholder="Tiempo *" id="idEjecutvio" name="idEjecutvio" class=" tiempo border-bottom form-control"  autocomplete="off" value="<?php echo $session->idusuario ?>">
            <input  style="border: none;" readonly   type="hidden" placeholder="Tiempo *" id="tipo" name="tipo" class=" tiempo border-bottom form-control"  autocomplete="off" value="<?php echo $varTI->tipoServicio ?>">
        </div>
        <br>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="tipoCanal" class="">Seleccione canal *</label>
                <select style="border: none;"  name="tipoCanal" readonly="" class="cd-selec form-control border-bottom " required id="tipoCanal">
                    <option class='text-capitalize' value='<?php echo $varTI->canal ?>'  selected ><?php echo $varTI->canal ?></option>
                </select>
            </div>
            <div class="form-group col-md-2 a">
                <label for="nombreCanal" class="">Canal nombre</label>
                <input style="border: none;" readonly type="text" placeholder="Canal nombre" id="nombreCanal" name="nombreCanal" class="border-bottom form-control text-uppercase nombreCanal " autocomplete="off" required=""   value="<?php echo $varTI->canal_nombre ?>">
                <input style="border: none;" type="hidden" placeholder="Tienda" id="tienda" name="tienda" class="border-bottom form-control text-uppercase tienda " autocomplete="off"  value="" >
                <input style="border: none;" type="hidden" placeholder="Tienda" id="convenio" name="convenio" class="border-bottom form-control text-uppercase tienda " autocomplete="off"  value="" >
                <input style="border: none;" type="hidden" placeholder="Tienda" id="tienda" name="tienda" class="border-bottom form-control text-uppercase tienda " autocomplete="off"  value="" >
                <input style="border: none;" type="hidden" placeholder="Cobra Comision" id="comision" name="comision" class="border-bottom form-control text-uppercase ejecutivoCanalN" autocomplete="off" value="<?php echo $varTI->cobra_comision ?>" >
            </div>
            <div class="form-group col-md-4">
                <label for="ejecutivoCanalN" class="">Solicitante</label>
                <input style="border: none;"  readonly type="text" placeholder="Ejecutivo canal" id="ejecutivoCanalN" name="ejecutivoCanalN" class="border-bottom form-control text-uppercase ejecutivoCanalN" autocomplete="off" value="<?php echo $varTI->ejecutivo_canal ?>" >
            </div>
        </div>
        <div class="row">
            <h3 class="col-md-6">Datos de facturación </h3>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="Forma*" >Forma Pago</label>
                <select style="border: none;" id="formaPago" name="formaPago" class="form-control border-bottom" required  readonly >
                    <option value="<?php echo $varTI->forma_pago ?>" selected><?php echo $varTI->forma_pago ?></option>
                </select>
            </div>
            <div class="col-md-4">
                <label for="Forma*" >Documento Pago</label>
                <select style="border: none;" id="selectNa" name="tipoDocumentoFacturacion" class="cd-selec form-control border-bottom" required readonly >
                    <option value="<?php echo $varTI->doc_factura ?>" selected><?php echo $varTI->doc_factura ?></option>
                </select>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-8">
                <div class=" eFactura" style="">
                    <div class="row"><br>
                        <div class="form-group col-md-3">
                            <label for="rucFacturacion" class="">Ruc *</label>
                            <input readonly style="border: none;" type="number" placeholder="Ruc *" id="rucFacturacion" name="rucFacturacion" class="form-control x2 border-bottom" maxlength="12" minlength="10" value="<?= $retVal = $varTI->ruc_factura; ?>"  >
                        </div>
                        <div class="form-group col-md-7">
                            <label for="razonSocialFacturacion" class="">Razón Social *</label>
                            <input readonly style="border: none;" type="text" placeholder="Razón Social *" id="razonSocialFacturacion" name="razonSocialFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = $varTI->razonSocial_Factura; ?>" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-8">
                            <label for="direccionFiscalFacturacion" class="sr-only">Dirección Fiscal *</label>
                            <input readonly style="border: none;" type="text" placeholder="Dirección Fiscal *" id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="form-control border-bottom text-capitalize" value="<?= $varTI->direccion_factura ?>">

                        </div>
                        <div class="form-group col-md-4">
                            <label for="telefonoFacturacion" class="sr-only">Teléfono</label>
                            <input style="border: none;" readonly type="text" placeholder="Teléfono" id="telefonoFacturacion" name="telefonoFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = $varTI->numero_fatura ?>">

                            <input style="border: none;" type="hidden" placeholder="Dirección de engrega" id="direccionEntrega" name="direccionEntrega" class="form-control border-bottom text-capitalize" value="">
                            <input style="border: none;" type="hidden" placeholder="Referencia de engrega" id="referenciaEntrega" name="referenciaEntrega" class="form-control border-bottom text-capitalize" required="" value="">
                            <input style="border: none;" type="hidden" placeholder="Distrito de entrega" id="distritoEntrega" name="distritoEntrega" class="form-control border-bottom text-capitalize" required autocomplete="off" value="">
                            <input style="border: none;" type="hidden" placeholder="Horario de entrega" id="horarioEntrega" name="horarioEntrega" class="form-control border-bottom text-capitalize" autocomplete="off" required value="">
                            <input style="border: none;" type="hidden" placeholder="Persona de recepción de factura" id="personaEntrega" name="personaEntrega" class="form-control border-bottom text-capitalize" required value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="showTipo">
            <div class="row">
                <h3 class="col-md-6">Datos de instalación</h3>
                <div class="col-md-6">
                    <label for="pendienteInstacion">Pendiente datos de instalación</label>
                    <div class="switch">
                        <div class="onoffswitch">
                            <input type="checkbox"  class="onoffswitch-checkbox" id="pendienteInstacion" value="si" name="pendienteInstacion">
                            <label class="onoffswitch-label" for="pendienteInstacion">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tablist">
                <input type="radio" title="taller" name="lugarInstacion" class='lugarDeInstalacion' id="taller" role="tab" aria-controls="taller-panel" checked value="Taller" class="lugarDeInstalacion tall" />
                <input type="radio" title="domicilio" name="lugarInstacion"  class='lugarDeInstalacion' id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente" class="lugarDeInstalacion domm" />
                <input type="radio" title="concesionario" name="lugarInstacion" class='lugarDeInstalacion'  id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario" class="lugarDeInstalacion conn" >

                <div class="tabs" aria-hidden="true">
                    <label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
                    <label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
                    <label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
                </div>
                <div class="panels">
                    <ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">
                        <h2><?= $lista->lugarInstalacion ?> | <?= $lista->observacion ?></h2>
                        <p><?= $lista->direccionInstacion  ?></p>
                        <p>
                            <?php $dep=$clienteObjModal->obtenerDepartamentoPorId($lista->departamentoi) ?><?= $dep->departamento ?>
                            &nbsp;|&nbsp;
                            <?php $prov=$clienteObjModal->otenerProvinciaPorId($lista->provinciai) ?><?php echo $prov->provincia ?>
                            &nbsp;|&nbsp;
                            <?php $di=$clienteObjModal->obtenerDistritoPorId($lista->distritoi) ?><?= $di->distrito ?>
                        </p>

                        <input type="hidden" name="direccionTaller" value="<?= $lista->direccionInstacion  ?>">
                        <input type="hidden" name="disT" id="distritoTallerid" value="<?= $lista->distritoi  ?>">
                        <input type="hidden" name="proT" value="<?= $lista->provinciai  ?>">
                        <input type="hidden" name="depT" value="<?= $lista->departamentoi  ?>">

                        <div class="row">
                            <div class="row">
                                <div class="row" >
                                    <div class="form-group col-md-4">
                                        <label  class="">Fecha</label>
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control tal tala fecha"  id="fechaIT" name="fechaIT" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="turno" class="">Turno a</label>
                                        <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn form-control border-bottom btn btn-info" >Seleccionar hora</button>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posT" class="hora form-control border-bottom"  required="" readonly>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRT" class="delR form-control border-bottom"  required="" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="horaS" class="">Hora seleccionada</label>
                                        <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaT" class="horaVer tala tal form-control border-bottom"  required="" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab">
                        <h2>Domicilio Cliente</h2>
                        <div class="row">
                            <div class="row">
                                <div class="form-group  col-md-4">
                                    <label for="departamentoInstalacionD" class="">Departamento*</label>
                                    <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize dom domo" required name="departamentoInstalacionD" id="departamentoInstalacionD">
                                        <option class="text-capitalize" value="" disabled selected >Seleccione Departamento* </option>
                                        <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                                        <?php if ($objDepartamento): ?>
                                            <?php foreach ($objDepartamento as $var): ?>
                                                <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                                            <?php endforeach ?>
                                        <?php else: ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="provinciaInstalacionD" class="">Provincia*</label>
                                    <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize dom domo" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="distritoInstalacionD" class="">Distrito*</label>
                                    <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize dom domo" required name="distritoInstalacionD"  id="distritoInstalacionD" >
                                    </select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group  col-md-12">
                                    <label for="direccionInstalacionD" class="sr-only">Direccion*</label>
                                    <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionD" name="direccionInstalacionD"  class="form-control border-bottom dom domo" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for="referenciaInstalacionD" class="sr-only">Referencia *</label>
                                    <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" required="" name="referenciaInstalacionD" class="form-control border-bottom dom domo"  >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="cod" class="sr-only">Cordenadas Domicilio *</label>
                                    <input style="border: none;" type="text" placeholder="Cordenadas Domicilio *" id="cod" name="cordenadasD" class="form-control x2 text-capitalize border-bottom dom domo" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label  class="">Fecha</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control dom domo fecha"  id="fechaID" name="fechaID" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="turno" class="">Turno b</label>
                                    <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Seleccionar hora</button>
                                    <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posD" class="hora form-control border-bottom"  required="" readonly>
                                    <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRD" class="delR form-control border-bottom"  required="" readonly>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="horaS" class="">Hora seleccionada</label>
                                    <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaD" class="horaVer domo dom  form-control border-bottom"  required="" readonly>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">

                        <div class="row">
                            <div class="col-md-6"><h2>Concesionario</h2></div>
                            <div class="col-md-6">
                                <label for="concesionarioInstalacion" class="sr-only">Nombre de concesionario*</label>
                                <input style="border: none;" type="text" placeholder="Nombre de concesionario*" id="concesionarioInstalacion" name="concesionarioInstalacion" class="form-control border-bottom con" required="" >
                            </div>
                        </div>

                        <div class="row">
                            <div class="row">
                                <div class="form-group  col-md-4">
                                    <label for="departamentoInstalacionC" class="">Departamento*</label>
                                    <select  style="border: none;" class="departamentoInstalacionC border-bottom form-control text-capitalize con cono" required name="departamentoInstalacionC" id="departamentoInstalacionC">
                                        <option class="text-capitalize" disabled selected >Seleccione Departamento</option>
                                        <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                                        <?php if ($objDepartamento): ?>
                                            <?php foreach ($objDepartamento as $var): ?>
                                                <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                                            <?php endforeach ?>
                                        <?php else: ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="provinciaInstalacionC" class="">Provincia*</label>
                                    <select  style="border: none;" class="provinciaInstalacionC border-bottom form-control text-capitalize con cono" required name="provinciaInstalacionC" id="provinciaInstalacionC" >
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="distritoInstalacionC" class="">Distrito*</label>
                                    <select  style="border: none;" class="distritoInstalacionC border-bottom form-control text-capitalize con cono" required name="distritoInstalacionC"  id="distritoInstalacionC" > 		</select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label for="direccionInstalacionC" class="">Direccion*</label>
                                    <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom con cono" >
                                </div>
                                <div class="form-group  col-md-6">
                                    <label for="referenciaInstalacionC" class="">Referencia *</label>
                                    <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom con cono" >
                                </div>
                            </div>
                            <div class="row">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label  class="">Fecha</label>
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control con cono fecha"  id="fechaIC" name="fechaIC" autocomplete="off" required>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="turno" class="">Turno c</label>
                                        <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Seleccionar hora</button>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posC" class="hora form-control border-bottom"  required="" readonly>
                                        <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="delRC" class="delR form-control border-bottom"  required="" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="horaS" class="">Hora seleccionada</label>
                                        <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaC" class="horaVer cono con form-control border-bottom"  required="" readonly>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </ul>
                </div>
            </div>
            <div class="ibox" id="ibox1">
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-double-bounce">
                        <div class="sk-double-bounce1"></div>
                        <div class="sk-double-bounce2"></div>
                    </div>
                    <div id="programacionHtml">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="nombreContacto" class="">Nombre Contacto *</label>
                    <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContacto" name="nombreContacto" class="form-control x2 text-capitalize border-bottom " required="" value="<?= $varTI->contacto_i ?>" >
                </div>
                <div class="form-group col-md-6">
                    <label for="numeroContacto" class="">Número Contacto *</label>
                    <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContacto" name="numeroContacto" class="form-control x2 text-capitalize border-bottom" required="" value="<?= $varTI->numero_i ?>">
                </div>
            </div>

            <hr>
            <div class="row">

                <div class="form-group col-md-12">

                    <label for="seguimientoTrama" class="">Seguimiento</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Seguimiento" id="seguimientoTrama" >
                        <span class="input-group-btn">
                    <button type="button" id="registrarSeguiminto" class="btn btn-primary">Registrar Seguimiento </button>
                </span>
                    </div>
                </div>

            </div>

            <ul id='segimientohtml'>
                <?php foreach(TramaIntegracionTY::getSEg($idTrama) as $var ): ?>
                    <li><?= $var->fecha ?> <b><?= $var->body ?></b></li>
                <?php endforeach ?>
            </ul>
            <hr>
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="observaciong" class="">Observaciones </label>
                    <input style="border: none;" type="text" placeholder="Observaciones " id="observaciong" name="observaciong" class="form-control x2 border-bottom focusUrgenteta" >
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
            <button type="submit" name="crearDesdeChasis" id="agregarVehiculoBttn" class="btn btn-primary" value="<?php echo Token::generar() ?>">Confirmar</button>
        </div>
    </div>
    <script src="../../assets/js/registrarme.js"></script>
    <script src="../../assets/js/validacionFactura.pabel.js"></script>
    <script src="../../assets/bootbox/bootbox.all.js"></script>
    <script>

        function validar(){
            var lugarI=$( "input[name='lugarInstacion']:checked" ).val()
            if (lugarI=='Taller') {
                $('.con').prop('disabled', true);
                $('.dom').prop('disabled', true);
                $('.tal').prop('disabled', false);
            }else if (lugarI=='Domicilio Cliente') {
                $('.con').prop('disabled', true);
                $('.dom').prop('disabled', false);
                $('.tal').prop('disabled', true);
            }else if (lugarI=='Concesionario') {
                $('.con').prop('disabled', false);
                $('.dom').prop('disabled', true);
                $('.tal').prop('disabled', true);
            }
        }

        $(document).ready(function(){
            $( "input[name='lugarInstacion']" ).on( "click",function(){
                $('#programacionHtml').html('');
                $('.horaVer').val('');
                $('.fecha').val('');
                validar();
            });
            validar();
        });


    </script>
    <script>
        $(".departamentoInstalacionD").change(function(){provincias4();});
        $(".departamentoInstalacionC").change(function(){provincias5();});
        $(".provinciaInstalacionD").change(function(){distritos4();});
        $(".provinciaInstalacionC").change(function(){distritos5();});
        function provincias4(){
            var iddepartamento = $(".departamentoInstalacionD").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/provincia',
                data :  'iddepartamento='+ iddepartamento,
                success : function(data)
                {
                    $(".provinciaInstalacionD").html(data);
                }
            });
            return false;
        }
        function provincias5(){
            var iddepartamento = $(".departamentoInstalacionC").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/provincia',
                data :  'iddepartamento='+ iddepartamento,
                success : function(data)
                {
                    $(".provinciaInstalacionC").html(data);
                }
            });
            return false;
        }

        function distritos4(){
            var idprovincia = $(".provinciaInstalacionD").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/distrito',
                data :  'idprovincia='+ idprovincia,
                success : function(data)
                {
                    $(".distritoInstalacionD").html(data)
                }
            });
            return false;
        }
        function distritos5(){
            var idprovincia = $(".provinciaInstalacionC").val();
            $.ajax({
                type : 'POST',
                url  : 'ajax/distrito',
                data :  'idprovincia='+ idprovincia,
                success : function(data)
                {
                    $(".distritoInstalacionC").html(data)
                }
            });
            return false;
        }



        $('.lugarDeInstalacion').on("change",function(){
            var pendienteInstacion=document.getElementById("pendienteInstacion");
            if (pendienteInstacion.checked) {
                $('.cono').prop('disabled', true);
                $('.domo').prop('disabled', true);
                $('.tala').prop('disabled', true);
                $("#programacionHtml").html('');
                limpiarCapacidad();
            }else{
                $('.cono').prop('disabled', false);
                $('.domo').prop('disabled', false);
                $('.tala').prop('disabled', false);
            }
        })


        $('#pendienteInstacion').on("change",function(){
            var pendienteInstacion=document.getElementById("pendienteInstacion");
            if (pendienteInstacion.checked) {
                $('.cono').prop('disabled', true);
                $('.domo').prop('disabled', true);
                $('.tala').prop('disabled', true);
                $("#programacionHtml").html('');
                limpiarCapacidad();
            }else{
                $('.cono').prop('disabled', false);
                $('.domo').prop('disabled', false);
                $('.tala').prop('disabled', false);
            }
        });

        function validaDP() {
            // var dp=1;
            var lugarI = $("input[name='lugarInstacion']:checked").val();
            if (lugarI == 'Taller') {
                var fecha = $('#fechaIT').val();
            } else if (lugarI == 'Domicilio Cliente') {
                var fecha = $('#fechaID').val();
            } else if (lugarI == 'Concesionario') {
                var fecha = $('#fechaIC').val();
            }
            var hora = $(".horaVer").val();
            var canthoraprog = $('#canthoraprog').val();
            var idtec = $('input[name="posT"]').val();
            // console.log("fecha: "+fecha+" idtec:"+idtec+" canhora:"+canthoraprog.trim());
            $.ajax({
                type: 'POST',
                url: 'ajax/validarRegistroTB',
                data:'tipo=save&fecha='+fecha+'&idtec='+idtec+"&canthoraprog=" + canthoraprog.trim() + "&hora=" + hora,
                // data :  'tipo=save&fecha=2019-11-27&idtec=2&canthoraprog=2&hora=12:00',
                success: function (data) {
                    if (data.trim() == 'dp') {
                        //dp='1';
                        $("#agregarVehiculoBttn").prop("disabled",true);
                        //      clearInterval(eventos.interval);
                        $("#programacionHtml").html('');
                        $(".horabtn").html('Seleccionar hora');
                        $(".horaVer").val('');
                        // fn(dp);
                        bootbox.alert({
                            title: "<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                            message: "<label>El horario Seleccionado("+hora+") ya fue Registrado,Por Favor Seleccione otro*</label>",
                            size: 'small',
                            callback: function () {
                                /*  l.ladda( 'stop' );*/
                            }
                        });
                    }else if(data.trim()=='Ok'){
                        $("#agregarVehiculoBttn").prop("disabled",false)
                    }
                }
            });
        }

        var eventos={};
        eventos.frejcar =function(){
            validaDP();
        };
        eventos.interval = setInterval(eventos.frejcar, 3000);

        $(document).ready(function () {

            $('#agregarVehiculoBttn').click(function() {

                checked = $('[name="servad[]"]:checked').length;

                if(checked==0) {
                    bootbox.alert({
                        title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                        message: "<label>Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*</label>",
                        size: 'small',
                        callback: function () {
                            /*  l.ladda( 'stop' );*/
                        }
                    });
                    /* alert("Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*");*/
                    console.log("Seleccione un Servicio Adicional, en caso no tenga seleccione Ninguno*");
                    // $('[name="servad[]"]:checked')[0].focus();
                    return false;
                }

            });
        });
    </script>
    <script>

        <?php $fechaA=date('Y-m-d') ?>
        jQuery.datetimepicker.setLocale('es');

        $('.fecha').datetimepicker({
            minDate:'<?= $fechaA ?>',
            format:'Y-m-d',
            scrollInput: false,
            scrollMonth : false,
            timepicker:false
        });


        $('#fechaEngrega').datetimepicker({
            minDate:'<?= $fechaA ?>',
            format:'Y/m/d',
            scrollInput: false,
            scrollMonth : false,
            timepicker:false
        });
        $('#horaEntrega').datetimepicker({
            datepicker:false,
            format:'H:i',
            allowTimes:['9:00','11:00','14:00','16:00']
        });

        $('.tiempo').typeahead({
            source: [
                {"name": "1 año"},
                {"name": "2 años"},
                {"name": "3 años"},
                {"name": "4 años"},
                {"name": "5 años"},
                {"name": "6 años"},
                {"name": "8 años"},
                {"name": "7 años"}
            ]
        });

        <?php if ($datos=DatosFacturacion::horariosSugeridosParaEntregaDeFactura()): ?>
        $('#horarioEntrega').typeahead({
            source: [
                <?php foreach ($datos as $var): ?>
                {"name": "<?php echo $var->fecha ?>"},
                <?php endforeach ?>
            ]
        });
        <?php endif ?>

        <?php if ($datosD=Cliente::otenerTodoLosDistritos()): ?>
        $('#distritoEntrega').typeahead({
            source: [
                <?php foreach ($datosD as $var): ?>
                {"name": "<?php echo $var->distrito ?>"},
                <?php endforeach ?>
            ]
        });
        <?php endif ?>

        <?php if ($datosDopeCon=Operacion::obtenerConcesionarioInstalacion()): ?>
        $('#concesionarioInstalacion').typeahead({
            source: [
                <?php foreach ($datosDopeCon as $var): ?>
                {"name": "<?php echo $var->nombreConcecionarioInstalicion ?>"},
                <?php endforeach ?>
            ]
        });
        <?php endif ?>

        $("#registrarSeguiminto").click(function(){
            // alert(servAd) ;seguimientoTrama
            var body=$('#seguimientoTrama').val();
            var idtrama = $("#idtrama").val();
            var today = new Date();
            var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()+' '+today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            if(body.length>0){
                $.ajax({
                    type : 'POST',
                    url  : 'ajax/registrarSegTY',
                    data : {body:body,idtrama:idtrama},
                    success : function(data)
                    {
                        $('#seguimientoTrama').val('');
                        $("#segimientohtml").append('<li> '+date+' <b>'+body+'</b></li>');
                        //segimientohtml
                    }
                });
            }
            return false;
        });

        function lstTProg(){
            var lugarI=$("input[name='lugarInstacion']:checked" ).val();
            var distrito="0";
            var tthinstala=$('#canthoraprog').val();
            var cita = 'General';
            var concecion="0";
            var lugarT='';

            if (lugarI=='Taller') {
                var fecha = $('#fechaIT').val();
                distrito=$("#distritoTallerid").val()

                console.log($("#distritoTallerid").val())
                lugarT='Taller';
            }else if (lugarI=='Domicilio Cliente') {
                var fecha = $('#fechaID').val();
                distrito=$('#distritoInstalacionD').val();
                lugarT='Domicilio';
            }else if (lugarI=='Concesionario') {
                var fecha = $('#fechaIC').val();
                var concecion=$('#concesionarioInstalacion').val();
                var cita = concecion;
                distrito=$('#distritoInstalacionC').val();
                lugarT='Concesionario';
            }
            var cod = document.getElementById("plan").value;
            servAd = $('[name="servad[]"]:checked').length;
            if (fecha!='' && cod!='' && servAd!=0 &&(distrito==0 || distrito!=null) && concecion!='') {
                $(".horabtn").off( "click" );
                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                $.ajax({
                    type : 'POST',
                    url  : '../includes/tbProgramacion',
                    data : {fecha:fecha,cita:cita,tthinstala:tthinstala,iddist:distrito,lugart:lugarT},
                    success : function(data)
                    {
                        $("#programacionHtml").html(data);
                        $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                        $(".horabtn").on("click",lstTProg).find(".horabtn");
                    }
                });
            }else{
                $("#programacionHtml").html('<h3 class="text-center text-info">Primero debe seleccionar Plan,Servicios Adicionales,fecha y/o Distrito y/o Nombre Concesionario.</h3>');
            }
            return false;
        }

        $(".horabtn").click(function(){
            lstTProg();
        });

        $('.fecha').change(function(){
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');
        });
        $("#concesionarioInstalacion").blur(function(){
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');
        });


        $('#departamentoInstalacionD,#departamentoInstalacionC,#provinciaInstalacionD,#provinciaInstalacionC,#distritoInstalacionD,#distritoInstalacionC').change(function(){
            $("#programacionHtml").html('');
            $(".horabtn").html('Seleccionar hora');
            $(".horaVer").val('');
        });

    </script>

<?php endif ?>

    <?php
}
?>

<?php if ($_POST): ?>
	<?php 
	require_once("../../includes/init.php");
	$dataProgramacion=Chequeos::obtenerProgramacionChequeo($_POST['fecha']);
	if ($dataProgramacion) {
		?>
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>DniRuc</th>
					<th>Nombres</th>
					<th>Fecha</th>
					<th>Creado por</th>
				</tr>
			</thead>
			<tbody>
		<?php
		$n=1;
		foreach ($dataProgramacion as $var) {
			?>
<tr>
	<td><?= $n++ ?></td>
	<td><?= $var->dniRuc ?></td>
	<td><?= $var->nombres ?></td>
	<td><?= formato($var->fechaHoraChequeoLinea) ?></td>
	<td><?php $usuario=Usuario::obtenerUsuario($var->idCreador);echo $usuario->usuarioNombre;   ?> <?= $usuario->usuarioApellidoPaterno;  ?></td>
</tr>
				
			<?php
		}
		?>
		</tbody>
			</table>
		<?php
	}else{
		echo "No se encontraro programacion de chequeo en linea.";
	}



	 ?>
<?php endif ?>
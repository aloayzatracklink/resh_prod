<?php 
require_once("../../includes/init.php");
 if (!$session->yaEstaIngresado()) {
  irA("../../ingresar");
}else{
	$obMenu=Usuario::obtenerMenu($session->idusuario);
	if ($obMenu->administrarComercial=="si"){		
		if (isset($_POST['idfacturacion'])) {
			$obFact=new DatosFacturacion;
			$obFact->datosfacturacionid=$_POST['idfacturacion'];
            $obFact->type_commission=$_POST['ntypecom'];
            $obFact->trabajo=$_POST['trabajo'];
            $trab = $_POST['trabajo'];
            $nidrenova = 0;
            if(isset($_POST['idrenova'])){
                $nidrenova  =  $_POST['idrenova'];
            }
            $obFact->idrenovacion = $nidrenova;

			if(!$obFact->marcarComoComisionado()) {
				die("FAIL HERE");
			}else{		
				$var="Comision directa";
				Log::registrarLog("comisiones",$_POST['idfacturacion'],$_POST,$var);
					$datosF=DatosFacturacion::obtenerDatosFacturacionPorId($_POST['idfacturacion']);
                    $datosR=Renovaciones::obtenerDatosDeRenovacionPorId($nidrenova);
                    $usuarioCrea=Usuario::obtenerUsuario($datosF->idUsuarioCreado);
                    if($nidrenova == 0){
					   $datosCL=Cliente::obtenerDatosDeclientePorId($datosF->idcliente);
                       $clD=(!empty($datosCL->nombre))?$datosCL->apellidosC." ".$datosCL->nombre:$datosCL->razonSocial;
                    }else{
                        $clD=$datosR->nombreCliente;
                        $da = Renovaciones::obtenerDatosDeRenovacionPorId($nidrenova);
                        $usuarioCrea=Usuario::obtenerUsuario($da->idejecutivo);
                    }


					$agregadoPor=Usuario::obtenerUsuario($session->idusuario);
					require_once '../../mailphp/PHPMailer/PHPMailerAutoload.php';
					$mail = new PHPMailer; 
					$htmlContenido1="<body style=' margin: 0;padding: 0;font-family:Helvetica, Arial, sans-serif;    box-sizing: border-box;    font-size: 14px; background-color: #f6f6f6; '>
					<table style=' background-color: #f6f6f6; width: 100%;'>
					    <tr>
					        <td></td>
					        <td  width='600' style=' display: block !important;    max-width: 600px !important;    margin: 0 auto !important;    /* makes it centered */    clear: both !important;'>
					            <div class='content' style='  max-width: 600px;    margin: 0 auto;    display: block;    padding: 20px;'>
					                <table class='main' width='100%' cellpadding='0' cellspacing='0' style='background: #fff;    border: 1px solid #e9e9e9;    border-radius: 3px;'>
					                    <tr>
					                        <td style='font-size: 16px;    color: #fff;    font-weight: 500;    padding: 20px;    text-align: center;    border-radius: 3px 3px 0 0;  background: #4e8487;'>
					                            COMISIÓN AGREGADA
					                        </td>
					                    </tr>
					                    <tr>
					                        <td style='padding: 10px !important ;padding: 20px; vertical-align: top;'>
					                            <table width='100%' cellpadding='0' cellspacing='0'>                                
					                                <tr>
					                                    <td  style='padding: 0 0 20px;'>
					                                        Estimados se agrego como comisionado la  $trab siguiente: <br>Cliente:<strong>$clD </strong><br>Plan: $datosF->plan <br>Tiempo : $datosR->tiempo  <br> Precio : $datosF->costo </td>
					                                    
					                                </tr>                              
					                                <tr>
					                                   <td>Agregado por $agregadoPor->usuarioNombre $agregadoPor->usuarioApellidoPaterno</td>
					                                </tr>
					                            </table>
					                        </td>
					                    </tr>
					                </table>
					                <div style=' width: 100%;    clear: both;    color: #999;    padding: 20px;'>
					                    <table width='100%'>
					                        <tr>
					                            <td style='font-size: 12px; text-align: center;padding: 0 0 20px;' ><a href='#' style='color: #999;'>Desuscriberse</a> de estas alertas.</td>                        </tr>
					                    </table>
					                </div></div>
					        </td>
					        <td></td>
					    </tr>
					</table>
					</body>
					</html>";
					$mail->isSMTP();
					$mail->SetLanguage( 'es', '../mailphp/PHPMailer/language/' );
					$mail->SMTPDebug = 0;
					$mail->Debugoutput = 'html';
					$mail->Host = gethostbyname('mail.tracklink.pe');
					$mail->Port = 25;
					$mail->SMTPAuth = false;
					$mail->CharSet = 'UTF-8';
					$mail->setFrom('no-responder@tracklink.pe','Resh');  
					$mail->addReplyTo("kcabrera@tracklink.pe", 'Soporte resh');

					$mail->addAddress($usuarioCrea->usuario,$usuarioCrea->usuarioNombre." ".$usuarioCrea->usuarioApellidoPaterno);
					//$mail->addAddress("kcabrera@tracklink.pe","test");
					$mail->addCC("asistentecomercial@tracklink.pe");
					$mail->addBCC("pruebas-ti@tracklink.pe");
					$mail->Subject='COMISION AGREGADA: '.$clD.' ';
					$mail->msgHTML($htmlContenido1);
					$mail->AltBody = 'Estimado(as) se agrego como comisionado a cliente: '.$clienteDatos; 
					if (!$mail->send()) {
					    echo "Mailer Error: " . $mail->ErrorInfo;
					} else {     

					}
		}
	}else{
  		irA("../../ingresar");
	}
	}	
}	

?>
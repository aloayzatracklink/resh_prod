<?php 
if (isset($_POST)) {
include_once("../../includes/init.php");
Log::registrarLog('comisiones',$_POST['id'],$_POST,'editadoDesdeComision cambio de RH');       
$obMenu=Usuario::obtenerMenu($session->idusuario);
    if ($obMenu->comisiones=="si"){
        $objCom=new Comisiones;
            $objCom->idcomision=limpiar($_POST['id']);
        if (isset($_POST['titular'])) {
            if(!$objCom->cambiarTituar()){
                die("ERROR en borrar el RH");
            }else{
                die("DOING ");
            }
        }
        if(isset($_POST['rh'])){
            if(!$objCom->cambiarRH()){
                die("ERROR en borrar el RH");
            }else{
                die("DOING ");
            }
        }
        if(isset($_POST['eliminar'])){
            if(!$objCom->eliminarComision()){
                die("ERROR en borrar el RH");
            }else{
                die("DOING ");
            }
        }
        if (isset($_POST['titularTC'])) {
            $objCom->type_commission=limpiar($_POST['idtipc']);
            if(!$objCom->cambiarTituarTC()){
                die("ERROR en borrar el RH");
            }else{
                die("DOING ");
            }
        }
    }else{
        header("Location:http://resh.tracklink.pe");
    }
}else{
    header("Location:http://resh.tracklink.pe");
}

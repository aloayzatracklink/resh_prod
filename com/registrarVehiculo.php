  <?php $nav="registrarVehiculo" ;?>
<?php include("../includes/head.php") ?>
<link href="../assets/calendario/fullcalendar.min.css" rel="stylesheet">
<link href="../assets/calendario/fullcalendar.print.min.css" rel='stylesheet' media='print'>
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<link href='../assets/calendario/scheduler.min.css' rel='stylesheet' />
<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->registrarCliente=="si"): ?>
<?php
$id = (isset($_GET['id'])) ? htmlspecialchars(htmlentities($_GET['id'])) : "" ;
$errores=array();
$objCliente= new Cliente;
$vehiculoObj=new Vehiculo;
if (isset($_POST['registrarContacto'])) {
      $errores=array();
      $field_values_array=$_POST['field_name'];
      $objCliente->borrarContactosAutorizados($id);
      $n=0;
      foreach($field_values_array[0] as $val){
          $dni[$n]=$val;
          $n++;
      }
      $m=0;
      foreach($field_values_array[1] as $val) {
          $nombre[$m]=$val;
          $m++;
      }

      $x=0;
      foreach($field_values_array[2] as $val) {
          $apellidos[$x]=$val;
          $x++;
      }

      $y=0;
      foreach($field_values_array[3] as $val) {
          $celular[$y]=$val;
          $y++;
      }

      $z=0;
      foreach($field_values_array[4] as $val) {
          $telefono[$z]=$val;
          $z++;
      }

      $a=0;
      foreach($field_values_array[5] as $val) {
          $correo[$a]=$val;
          $a++;
      }
      $b=0;
      foreach($field_values_array[6] as $val) {
          $parentesco[$b]=$val;
          $b++;
      }
      $datosDeContactoAutorizado['dni']=$dni;
      $datosDeContactoAutorizado['nombre']=$nombre;
      $datosDeContactoAutorizado['apellidos']=$apellidos;
      $datosDeContactoAutorizado['celular']=$celular;
      $datosDeContactoAutorizado['telefono']=$telefono;
      $datosDeContactoAutorizado['correo']=$correo;
      $datosDeContactoAutorizado['parentesco']=$parentesco;
      for ($i=0; $i < count($datosDeContactoAutorizado['dni']) ; $i++) { 
          $datosDeContactoAutorizado['dni_i']=$datosDeContactoAutorizado['dni'][$i];
          $datosDeContactoAutorizado['nombre_i']=$datosDeContactoAutorizado['nombre'][$i];
          $datosDeContactoAutorizado['apellidos_i']=$datosDeContactoAutorizado['apellidos'][$i];
          $datosDeContactoAutorizado['celular_i']=$datosDeContactoAutorizado['celular'][$i];
          $datosDeContactoAutorizado['telefono_i']=$datosDeContactoAutorizado['telefono'][$i];
          $datosDeContactoAutorizado['correo_i']=$datosDeContactoAutorizado['correo'][$i];
          $datosDeContactoAutorizado['parentesco_i']=$datosDeContactoAutorizado['parentesco'][$i];
          if ($objCliente->registrarContactoAutorizado($datosDeContactoAutorizado,$id)) {

          }
      }
  }


if (isset($_POST['agregarVehículo'])) {
 //   echo "<pre>";
 //   die(var_dump($_POST));
    $errores=array();
    $echo=array();
    //en vez de validar token que valide el chasis
    if (Token::verificar($_POST['token'])) {
      if (!empty($_FILES['voucher']['name'])) {
        $extencionesAbilitadas= array('jpg','jpeg','png','gif','pdf','JPG','JPEG','PNG','GIF','PDF','docx','DOCX');
        $nombreArchivo=$_FILES['voucher']['name'];        
        $tmp=explode('.', $nombreArchivo);
        $extencionArchivo=end($tmp);
        $tamanioArchivo=$_FILES['voucher']['size'];
        $ubicacionTemporal=$_FILES['voucher']['tmp_name'];
        if (!in_array($extencionArchivo,$extencionesAbilitadas)) {  
          $errores[]='La extencion de la imagen deve ser jpg jpeg png gif o pdf';
        }
        if ($tamanioArchivo>10485760) {
          $errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
        }
      }
      if (isset($_POST['tipoDocumentoFacturacion']) && $_POST['tipoDocumentoFacturacion']=="Factura") {
        if (empty($_POST['rucFacturacion'])) {
          $errores[]='Completar el RUC para la facturacion';
        }else 
        if (empty($_POST['razonSocialFacturacion'])) {
          $errores[]='Completar el RUC para la facturacion';
        }else
        if (empty($_POST['direccionFiscalFacturacion'])) {
          $errores[]='Completar el RUC para la facturacion';
        }       
      }
      $operacionesObj=new Operacion();
      $agregarVehiculoObj=new Vehiculo();
      $facturacionObj=new DatosFacturacion();
      $objComision=new Comisiones();
      $objCanales= new Canales();
      $programado="";

    $entrega=$_POST['entregaEnInstalacion'];
    $obsEntrega='';
      if ($entrega=='no' || $entrega=='noE') {
        $facturacionObj->entregaEnInstalacion='si';        
        if($entrega=='no'){
            $obsEntrega.=' Entregar documento de facturacion al momento de la instalacion';
        }
      }else
      {
         $facturacionObj->entregaEnInstalacion='';        
         $facturacionObj->fechaHoraDeEntrega=$_POST['horarioEntrega'];
         $facturacionObj->registrarSugerenciaDeFechaEntrega(); 
         $facturacionObj->fecha=$_POST['horarioEntrega'];
         $facturacionObj->direccionEntrega=$_POST['direccionEntrega'];
         $facturacionObj->referenciaEntrega=$_POST['referenciaEntrega'];
         $facturacionObj->distritoEntrega=$_POST['distritoEntrega'];
         $facturacionObj->recibidoPor=$_POST['personaEntrega'];        
      }
    if ($_POST['lugarInstacion']=='Taller') {
        $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
        // $operacionesObj->departamentoi='15';
        // $operacionesObj->provinciai='127';
        // $operacionesObj->distritoi='1291';
        $operacionesObj->direccionInstacion=$_POST['direccionTaller'];
        $operacionesObj->estado=0;
        // $operacionesObj->fechaInstalacion=$_POST['fechaIT']." ".$_POST['horaIT'];
        $operacionesObj->nombreContacto=$_POST['nombreContactoIT'];
        $operacionesObj->numeroContacto=$_POST['numeroContactoIT'];
        $operacionesObj->observacion=$_POST['observaciones']." ".$_POST['lugarInstacion']." ".$obsEntrega;
    }else
    if($_POST['lugarInstacion']=='Domicilio Cliente'){
      $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
      $operacionesObj->estado=0;      
      // $operacionesObj->coordenadasInstalacion=$_POST['coordenadasInstalacion'];
      // $operacionesObj->departamentoi=$_POST['departamentoInstalacionD'];
      // $operacionesObj->provinciai=$_POST['provinciaInstalacionD'];
      // $operacionesObj->distritoi=$_POST['distritoInstalacionD'];
      $operacionesObj->direccionInstacion=$_POST['direccionInstalacionD'];
        $operacionesObj->observacion=$_POST['observaciones']." ".$_POST['direccionInstalacionD']." ".$_POST['lugarInstacion']." ".$obsEntrega;
      $operacionesObj->numeroContacto=$_POST['numeroContactoID'];
      // $operacionesObj->referenciaInstalacion=$_POST['referenciaInstalacionD'];
      $operacionesObj->nombreContacto=$_POST['nombreContactoID'];
      // $operacionesObj->fechaInstalacion=$_POST['fechaID']." ".$_POST['horaID'];      
  }else
  if($_POST['lugarInstacion']=='Concesionario'){
      $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];  
      $operacionesObj->estado=0;      
      $operacionesObj->nombreContacto=$_POST['nombreContactoIC'];
      $operacionesObj->numeroContacto=$_POST['numeroContactoIC'];
      // $operacionesObj->fechaInstalacion=$_POST['fechaIC']." ".$_POST['horaIC'];
      // $operacionesObj->departamentoi=$_POST['departamentoInstalacionC'];
      // $operacionesObj->provinciai=$_POST['provinciaInstalacionC'];
      // $operacionesObj->distritoi=$_POST['distritoInstalacionC'];
      $operacionesObj->direccionInstacion=$_POST['direccionInstalacionC'];
        $operacionesObj->observacion=$_POST['observaciones']." ".$_POST['direccionInstalacionC']." ".$_POST['lugarInstacion']." ".$obsEntrega;
      // $operacionesObj->referenciaInstalacion=$_POST['referenciaInstalacionC'];
      $operacionesObj->nombreConcecionarioInstalicion=$_POST['concesionarioInstalacion'];
  }

///fin de comporvacion de lugar de instalación ----
//Registrar datos de Vehiculo--
$res="";
if (isset($_POST['serviciosAdicionales'])) {
  foreach ($_POST['serviciosAdicionales'] as $var) {
   $res .=$var.", ";
 }     
}
$agregarVehiculoObj->plan=htmlspecialchars($_POST['plan']);
$agregarVehiculoObj->costo=htmlspecialchars($_POST['costo']);
$agregarVehiculoObj->planTiempo=htmlspecialchars($_POST['tiempo']);
$agregarVehiculoObj->placa=htmlspecialchars($_POST['placa']);
$agregarVehiculoObj->chasis=htmlspecialchars($_POST['chasis']);
$agregarVehiculoObj->motor=htmlspecialchars($_POST['motor']);
// if (isset($_POST['campana'])) {
//   $agregarVehiculoObj->campana=htmlspecialchars($_POST['campana']);
// }else{
//   $agregarVehiculoObj->campana='no';  
// }
$agregarVehiculoObj->marca=htmlspecialchars($_POST['marca']);
$agregarVehiculoObj->modelo=htmlspecialchars($_POST['modelo']);
$agregarVehiculoObj->color=htmlspecialchars($_POST['color']);
$agregarVehiculoObj->anio=htmlspecialchars($_POST['anio']);
$agregarVehiculoObj->serviciosAdicionales=$res;
$agregarVehiculoObj->companiaSeguro=htmlspecialchars($_POST['seguro']);
$agregarVehiculoObj->concesionario=htmlspecialchars($_POST['concesionario']);
$agregarVehiculoObj->observaciones=htmlspecialchars($_POST['observaciones']);
if(empty($errores)) {
  if ($agregarVehiculoObj->registrarVehiculo($id)) {
    $fecha="";
    if (!empty($_FILES['voucher']['name'])) {
      $fecha=date("YmdHis");
      move_uploaded_file($ubicacionTemporal, '../public/vouchers/'.$fecha.$nombreArchivo); 
    }
    $objClienteFacturacion = Cliente::obtenerDatosDeclientePorId($id);

    if ($_POST['tipoDocumentoFacturacion']=='Boleta') {
      $facturacionObj->idcliente=$id;
      $facturacionObj->idvehiculos=$agregarVehiculoObj->idvehiculo;
       $facturacionObj->idUsuarioCreado=$session->idusuario;
      $facturacionObj->tipoDocumentoFacturacion=$_POST['tipoDocumentoFacturacion'];
      if ($objClienteFacturacion->tipoCliente=="Natural") {
        $facturacionObj->dniFacturacion=$objClienteFacturacion->dni;
        $facturacionObj->razonSocialFacturacion=$objClienteFacturacion->nombre." ".$objClienteFacturacion->apellidosC;
        // $departamento=$objCliente->obtenerDepartamentoPorId($objClienteFacturacion->departamento);
        // $provincia=$objCliente->otenerProvinciaPorId($objClienteFacturacion->provincia);
        // $distrito=$objCliente->obtenerDistritoPorId($objClienteFacturacion->distrito);
        $facturacionObj->direccionFiscal=$objClienteFacturacion->direccion; 
      }else{
        $facturacionObj->dniFacturacion=$objClienteFacturacion->dni;
        $facturacionObj->razonSocialFacturacion=$objClienteFacturacion->nombre." ".$objClienteFacturacion->apellidosC;
        //  $departamento=$objCliente->obtenerDepartamentoPorId($objClienteFacturacion->departamento);
        // $provincia=$objCliente->otenerProvinciaPorId($objClienteFacturacion->provincia);
        // $distrito=$objCliente->obtenerDistritoPorId($objClienteFacturacion->distrito);
        $facturacionObj->direccionFiscal=$objClienteFacturacion->direccion;
      }    
      $facturacionObj->telefonoFacturacion=$objClienteFacturacion->telefono;
      if (!empty($_FILES['voucher']['name'])) {        
        $facturacionObj->voucher=$fecha.$nombreArchivo;
      }
      $facturacionObj->direccionEntrega=$_POST['direccionEntrega'];
      $facturacionObj->plan=$_POST['plan'];
      $facturacionObj->tiempo=$_POST['tiempo'];
      $facturacionObj->costo=$_POST['costo'];
      $facturacionObj->observaciones=$_POST['observaciones'].$obsEntrega;

    $facturacionObj->tipoCanal=$_POST['tipoCanal'];
    if (isset($_POST['tienda'])) {
        if (!empty($_POST['tienda'])) {
            $facturacionObj->oficina=$_POST['tienda'];
        }
    }
    if (isset($_POST['tienda1'])) {
        if (!empty($_POST['tienda1'])) {
            $facturacionObj->oficina=$_POST['tienda1'];
        }
    }
    if (!empty($_POST['convenio'])) {
        $facturacionObj->nombreCanal=$_POST['convenio'];    
    }else{
        $facturacionObj->nombreCanal=$_POST['nombreCanal'];    
    }
    if (isset($_POST['ejecutivoCanalN'])) {
        $facturacionObj->ejecutivoCanal=$_POST['ejecutivoCanalN'];    
    }

if (!$facturacionObj->registrarDatosDeFacturacion()) {
           $errores[]='NO SE REGISTRO LOS DATOS DE FACTURACION';
      }

    }else
    if($_POST['tipoDocumentoFacturacion']!='Boleta'){
      $facturacionObj->idcliente=$id;
      $facturacionObj->idvehiculos=$agregarVehiculoObj->idvehiculo;
      $facturacionObj->tipoDocumentoFacturacion=$_POST['tipoDocumentoFacturacion'];
      $facturacionObj->dniFacturacion=$_POST['rucFacturacion'];
      $facturacionObj->rucFacturacion=$_POST['rucFacturacion'];
      $facturacionObj->razonSocialFacturacion=$_POST['razonSocialFacturacion'];
      $facturacionObj->direccionFiscal=$_POST['direccionFiscalFacturacion'];
      $facturacionObj->telefonoFacturacion=$_POST['telefonoFacturacion'];      
      if (!empty($_FILES['voucher']['name'])) {   
        $facturacionObj->voucher=$fecha.$nombreArchivo;
      }
      $facturacionObj->plan=$_POST['plan'];
      $facturacionObj->tiempo=$_POST['tiempo'];
      $facturacionObj->costo=$_POST['costo'];        
      $facturacionObj->observaciones=$_POST['observaciones'].$obsEntrega;
      $facturacionObj->idUsuarioCreado=$session->idusuario;

    $facturacionObj->tipoCanal=$_POST['tipoCanal'];
    if (isset($_POST['tienda'])) {
        if (!empty($_POST['tienda'])) {
            $facturacionObj->oficina=$_POST['tienda'];
        }
    }
    if (isset($_POST['tienda1'])) {
        if (!empty($_POST['tienda1'])) {
            $facturacionObj->oficina=$_POST['tienda1'];
        }
    }
    if (!empty($_POST['convenio'])) {
        $facturacionObj->nombreCanal=$_POST['convenio'];    
    }else{
        $facturacionObj->nombreCanal=$_POST['nombreCanal'];    
    }
    if (isset($_POST['ejecutivoCanalN'])) {
        $facturacionObj->ejecutivoCanal=$_POST['ejecutivoCanalN'];    
    }
if (!$facturacionObj->registrarDatosDeFacturacion()) {
        $errores[]='NO SE REGISTRO LOS DATOS DE FACTURACION';
}

    }else{
      $errores="Fail detected facturacion data";
    }
    if (isset($_POST['comision']) && $_POST['comision']=='si') {
        $objComision->tiempo=$_POST['tiempo'][0];
        $objComision->idcliente=limpiar($id);
        $objComision->idcanal='0';
        $objComision->idvehiculo=$agregarVehiculoObj->idvehiculo;
        $objComision->idfacturacion=$facturacionObj->datosfacturacionid;
        $objComision->montoTotal=$_POST['costo'];
        $objComision->comision="PENDIENTE";
        $objComision->idCreador=$session->idusuario;
        $objComision->fechapago=date("YmdHis");
        if (isset($_POST['tipoDocumentoFacturacion'])) {
            $objComision->documentoFacturacion=$facturacionObj->tipoDocumentoFacturacion;
        }else{
            $objComision->documentoFacturacion=$_POST['formaPago'];
        }
        if (!$objComision->registrarComision()) {
            $errores[]="Error en registrar datos de comision";
        }
    }
//finalizamos la orden de instalacion aqui ....
    unset($_POST['costo']);
    unset($_POST['tiempo']);      
    unset($_POST['placa']);
    unset($_POST['chasis']);
    unset($_POST['motor']);
    unset($_POST['marca']);
    unset($_POST['modelo']);
    unset($_POST['anio']);
    unset($_POST['color']);
    unset($_POST['seguro']);
    unset($_POST['numeroTramiteSeguro']);
    unset($_POST['concesionario']);
    unset($_POST['observaciones']);

  } else {
    $errores[]="Error en el registro de vehículo";
  }
  // enviarMensaje("Vehiculo registrado, instalacion programada.");
     ///Inicio Registrar datos de instalcion here ----
  $operacionesObj->datosfacturacionid=$facturacionObj->datosfacturacionid;
  $operacionesObj->programarInstalacion($agregarVehiculoObj->idvehiculo,$id);
     ///Fin Registrar datos de instalcion here ----
  $echo[]="Vehiculo registrado ";
  // header("Location:registrarVehiculo.php?id=$id&estado=ok");
}
}else{
  $errores[]="Error el formulario ya ha sido enviado. Error Token"  ;
}
} 

?>
  <link rel="stylesheet" href="../assets/css2/style2.css">
  <link rel="stylesheet" href="../assets/css2/style3.css">
  <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
  <link href="../assets/css/plugins/select2/select2.min.css" rel="stylesheet">
  <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>
  <body>
    <div id="wrapper">
      <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
          <?php include("../includes/menuIzquierdo.php") ?>
        </div>
      </nav>
      <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
          <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include("../includes/navegacionArriba.php") ?>
          </nav>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
          <div class="row">
           <div class="col-lg-12">
             <div class="ibox float-e-margins">
               <div class="ibox-title">
                <div class="row">
                <div class="col-md-12">
                  <h3>Agregar vehículo a cliente </h3>                  
                </div>
                </div>
              </div>
              <div class="ibox-content">
                <div class="well">
                  <?php 
                  if ($clienteObj=Cliente::obtenerDatosDeclientePorId($id)) {
                    ?>
                    <h3>
                      Datos de cliente: <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? "Natural " : "Empresa " ; ?>
                    </h3>
                    <div class="row">
                      <div class="row">
                        <div class="col-lg-2 text-primary "><h2><?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? "DNI: " : "RUC: " ; ?> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? $clienteObj->dni : $clienteObj->ruc ; ?></h2></div>   
                        <div class="col-lg-6 text-primary text-capitalize"><h2> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? "Nombres: " : "Razon social: " ; ?> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? $clienteObj->nombre." ".$clienteObj->apellidosC : $clienteObj->razonSocial ; ?></h2></div>  
                        <div class="col-md-4">
                               <div class="row">     
                    <div class="col-md-12"><div class="btn btn-primary col-md-12" name="registrar" data-toggle="modal" data-target="#autorizados" >Contactos autorizados <i class="fa fa-child"> </i></div>
                    <h3>Cantidad de contactos autorizados actuales <?php  $o=Cliente::obtenerCantidadContactosAutorizados($id); echo $o->cantidad; ?>
                     <?php if ($o->cantidad==0): ?>
                       <span class="label label-warning">Ingrese al menos 2 contactos autorizados</span>
                     <?php endif ?>
                   </h3></div>
                 </div>
                        </div> 
                      </div>
                      <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4"></div>
                      </div>

                      <?php

                    }else{
                      ?>
                      <div class="row">
                        <div class="col-lg-12 text-primary  text-center"><h2> Algo anda mal escriba a wilmanpabel@gmail.com</h2></div>   
                        <?php

                      }

                      ?>

                    </div>
                  </div>
                  <hr>
                 <form action="" method="post" enctype="multipart/form-data" id="registrarVehiculoF">
                  <div class="row">
                    <div class="col-lg-12"> 
                      <div class="row">
                        <div class="form-group col-md-4">
                          <label for="plan" class="">Plan *</label>
                          <select  style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
                            <?php if (isset($agregarVehiculoObj->plan)): ?>
                              <option class="" value="<?php echo $agregarVehiculoObj->plan ?>"  selected ><?php echo $agregarVehiculoObj->plan ?></option>
                            <?php endif ?>
                            <option class="" value="" disabled selected >Seleccione Plan *</option>
                            <?php Cliente::obtenerPlan() ?>      
                          </select>
                        </div>
                        <div class="form-group col-md-4">
                          <label for="plan" class="">Precio *</label>
                          <input style="border: none;" type="text" placeholder="Precio" id="costo" name="costo" class="costo border-bottom form-control" required autofocus autocomplete="off" value="<?php echo $retVal = (isset($_POST['costo'])) ? $_POST['costo'] : "" ; ?>" >
                        </div>
                        <div class="form-group col-md-4">
                          <label for="tiempo" class="">Tiempo *</label>
                          <input style="border: none;" type="text" placeholder="Tiempo" id="tiempo" name="tiempo" class="tiempo border-bottom form-control"  required autocomplete="off" value="<?php echo $retVal = (isset($_POST['tiempo'])) ? $_POST['tiempo'] : "" ; ?>" >
                        </div>
                      </div> 
                      <div class="row">
                            <div class="form-group col-md-2">
                          <label for="placa" class="">Placa *</label>
                          <input style="border: none;" type="text" placeholder="Placa " id="placa" name="placa" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $retVal = (isset($_POST['placa'])) ? $_POST['placa'] : "" ; ?>" >
                        </div>
                        <div class="form-group col-md-5">
                          <label for="chasis" class="">Chasis *</label>
                          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="chasis" class="border-bottom form-control text-uppercase" required autocomplete="off" value="<?php echo $retVal = (isset($_POST['chasis'])) ? $_POST['chasis'] : "" ; ?>">
                        </div>
                        <div class="form-group col-md-5">
                          <label for="motor" class="">Motor *</label>
                          <input style="border: none;" type="text" placeholder="Motor" id="motor" name="motor" class="border-bottom form-control text-uppercase" required autocomplete="off"  value="<?php echo $retVal = (isset($_POST['motor'])) ? $_POST['motor'] : "" ; ?>" >
                        </div>
                      </div>
                      <!-- <?php include("../includes/campanas.php") ?> -->
                      <div class="row">
                        <div class="form-group col-md-3">
                          <label for="marca" class="">Marca *</label>
                          <input style="border: none;" type="text" placeholder="Marca" id="marca" name="marca" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $retVal = (isset($_POST['marca'])) ? $_POST['marca'] : "" ; ?>" >
                        </div>
                        <div class="form-group col-md-3">
                          <label for="modelo" class="">Modelo *</label>
                          <input style="border: none;" type="text" placeholder="Modelo" id="modelo" name="modelo" class="border-bottom form-control text-capitalize"  required value="<?php echo $retVal = (isset($_POST['modelo'])) ? $_POST['modelo'] : "" ; ?>" >
                        </div>
                        <div class="form-group col-md-3">
                          <label for="color" class="">Color *</label>
                          <input style="border: none;" type="text" placeholder="Color " id="color" name="color" class="border-bottom form-control x2 text-capitalize" required value="<?php echo $retVal = (isset($_POST['color'])) ? $_POST['color'] : "" ; ?>" >
                        </div>
                        <div class="form-group col-md-3">
                          <label for="anio" class="">Año de fabricacion *</label>
                          <input style="border: none;" type="text" placeholder="Año de fabricacion" id="anio" name="anio" class="border-bottom form-control" required value="<?php echo $retVal = (isset($_POST['anio'])) ? $_POST['anio'] : "" ; ?>" >
                        </div>
                      </div>                                      
                      <div class="row">
                        <br>
                        <div class="form-group col-md-6">
                          <label for="seguro" class="">Compañia de seguros *</label>
                          <input style="border: none;" type="text" placeholder="Compañia de seguros" id="seguro" name="seguro" class="seguro border-bottom form-control"  autocomplete="off"  required="" value="<?php echo $retVal = (isset($_POST['seguro'])) ? $_POST['seguro'] : "" ; ?>" >
                        </div>
                        <div class="col-md-6 border-bottom" ><label for="concesionario" class="">Concesionario *</label>
                          <input style="border: none;" type="text" placeholder="Concesionario *" id="condscesionario" name="concesionario" class="concesionario border-bottom form-control" required autocomplete="off" value="<?php echo $retVal = (isset($_POST['concesionario'])) ? $_POST['concesionario'] : "" ; ?>" >
                        </div>
                      </div> 
                      <br><br>
                       <div class="row"> 
                        <div class="col-lg-8 voucherimn">
                          <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                              <i class="glyphicon glyphicon-file fileinput-exists"></i>
                              <span class="fileinput-filename"></span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                              <span class="fileinput-new">Seleccionar Voucher de pago</span>
                              <span class="fileinput-exists">Cambiar</span>
                              <input type="file" name="voucher" required id="voucherImg" />
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
                          </div> 
                        </div>
                        <div class="col-md-4">
                          <label for="pagoencaja">Voucher pendiente</label>
                          <div class="switch">
                            <div class="onoffswitch">
                              <input type="checkbox" class="onoffswitch-checkbox" id="pagoencaja" value="si" name="ad">
                              <label class="onoffswitch-label" for="pagoencaja">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                            </div>
                          </div>
                        </div>
                       </div> 
                        </div>
                     <?php 
    $objDatosC = Cliente::obtenerDatosDeclientePorId($id);
    ?>
            <hr><h2 class="text-center">Datos facturación</h2><hr>
                      <div class="row">
                        <div class="col-md-4">
                          <select style="border: none;" id="selectNa" name="tipoDocumentoFacturacion" class="cd-selec form-control border-bottom" required >
                            <option value="" disabled selected>Seleccione datos de facturación *</option>
                            <option value="Boleta">Boleta</option>
                            <option value="Factura">Factura</option>
                          </select>   
                        </div>
                        <div class="col-md-8">
                          <div class="content-pane e1">
                            <h3>Seleccione pago</h3>
                          </div>
                          <div class="content-pane eBoleta">
                            <h3>Continuar</h3>
                          </div>
                          <div class="content-pane eFactura">
                            <div class="row"><br>
                              <div class="form-group col-md-4">
                                <label for="rucFacturacion" class="">Ruc *</label>
                                <input style="border: none;" type="text" placeholder="Ruc *" id="rucFacturacion" name="rucFacturacion" class="form-control x2 border-bottom"  value="<?php echo $retVal = (isset($clienteObj->ruc)) ? $clienteObj->ruc : "" ; ?>" >
                              </div>
                              <div class="form-group col-md-8">
                                <label for="razonSocialFacturacion" class="">Razon Social *</label>
                                <input style="border: none;" type="text" placeholder="Razon Social *" id="razonSocialFacturacion" name="razonSocialFacturacion" class="form-control border-bottom text-capitalize" value="<?php echo $retVal = (isset($clienteObj->razonSocial)) ? $clienteObj->razonSocial : "" ; ?>" >
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-md-8">
                                <label for="direccionFiscalFacturacion" class="">Direccion Fiscal *</label>
                                <input style="border: none;" type="text" placeholder="Direccion *" id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="form-control border-bottom text-capitalize"  value="<?= $objDatosC->direccion  ?>">

                            </div>
                            <div class="form-group col-md-4">
                              <label for="telefonoFacturacion" class="">Telefono </label>
                              <input style="border: none;" type="text" placeholder="Telefono" id="telefonoFacturacion" name="telefonoFacturacion" class="form-control border-bottom text-capitalize" value="<?php echo $retVal = (isset($clienteObj->telefono)) ? $clienteObj->telefono : "" ; ?>">

                            </div>
                          </div>
</div> 
</div>  
</div>
<br>
<br>
<div class="row">
<div class="col-md-12">
<select style="border: none;" id="entrega" name="entregaEnInstalacion" class="cd-selec form-control border-bottom text-center" required >
<option value="" disabled selected>¿Enviar Factura o boleta?</option>
<option value="noE">No enviar</option>
<option value="si">Enviar a domicilio</option>
<option value="no">Entregar cuando se realiza la instalación.</option>
</select>
</div>  
</div>
<div class="esconderNo">
<div class="row">
        <div class="form-group col-md-6">
          <label for="direccionEntrega" class="">Dirección de engrega</label>
          <input style="border: none;" type="text" placeholder="Dirección de engrega" id="direccionEntrega" name="direccionEntrega" class="form-control border-bottom text-capitalize" value="">
        </div>
        <div class="form-group col-md-6">
          <label for="referenciaEntrega" class="">Referencia de engrega</label>
          <input style="border: none;" type="text" placeholder="Referencia de engrega" id="referenciaEntrega" name="referenciaEntrega" class="form-control border-bottom text-capitalize" required="" value="">
        </div>
      </div>
<div class="row">
        <div class="form-group col-md-4">
          <label for="distritoEntrega" class="">Distrito de entrega</label>
          <input style="border: none;" type="text" placeholder="Distrito de entrega" id="distritoEntrega" name="distritoEntrega" class="form-control border-bottom text-capitalize" required autocomplete="off" value="">
        </div>
        <div class="form-group col-md-4">
          <label for="horarioEntrega" class="">Horario de entrega</label>
          <input style="border: none;" type="text" placeholder="Horario de entrega" id="horarioEntrega" name="horarioEntrega" class="form-control border-bottom text-capitalize" autocomplete="off" required value="">
        </div>
        <div class="form-group col-md-4">
          <label for="personaEntrega" class="">Persona de recepción de factura </label>
          <input style="border: none;" type="text" placeholder="Persona de recepción de factura" id="personaEntrega" name="personaEntrega" class="form-control border-bottom text-capitalize" required value="">
        </div>
      </div>
</div>
<br> <br>
<hr><h2 class="text-center">Datos de canal</h2><hr>
<div class="row">
<div class="form-group col-md-3">
<label for="tipoCanal" class="">Seleccione canal *</label>
    <select style="border: none;"  name="tipoCanal" class="cd-selec form-control border-bottom " required id="tipoCanal">
      <option value="" disabled selected>Seleccione canal *</option>
      <option value="BROKER">Broker</option>
      <option value="CONCESIONARIO">Concesionario</option>
      <option value="CORPORATIVO">Corporativo</option>      
      <option value="ASEGURADORA">Aseguradora</option>
      <option value="CONVENIO">Convenio</option>
      <option value="BANCO">Banco</option>
      <option value="TRACKLINK OCTO">Tracklink octo</option>
    </select>   
  </div>
  <div class="form-group col-md-2 a">
    <label for="nombreCanal" class="">Canal nombre</label>
    <input style="border: none;" type="text" placeholder="Canal nombre" id="nombreCanal" name="nombreCanal" class="border-bottom form-control text-uppercase nombreCanal " autocomplete="off" required="" >
  </div>
<div class="tiendaAparece1">
   <div class="form-group col-md-2 ">
    <label for="tienda" class="">Tienda</label>
    <input style="border: none;" type="text" placeholder="Tienda" id="tienda" name="tienda" class="border-bottom form-control text-uppercase tienda " autocomplete="off" required="" >
  </div>    
  </div>
  <div class="b">    
  <div class="form-group col-md-2 ">
    <label for="convenio" class="">Seleccione Convenio *</label>
     <select style="border: none;"  name="convenio" class="cd-selec form-control border-bottom" required id="convenio">
     <?php $ConD=Canales::obtenerConvenios() ?>
     <option value="" disabled selected>Seleccione Convenio *</option>
     <?php foreach ($ConD as $var): ?>
        <option value="<?= $var->idconvenio  ?>"><?= $var->nombreConvenio ?></option>
     <?php endforeach ?>
    </select> 
  </div>
  <div class="tiendaAparece">
   <div class="form-group col-md-2 ">
    <label for="tienda" class="">Tienda</label>
    <input style="border: none;" type="text" placeholder="Tienda" id="tienda" name="tienda1" class="border-bottom form-control text-uppercase tienda " autocomplete="off" required="" >
  </div>    
  </div>
  </div>

  <div class="form-group col-md-3">
    <label for="ejecutivoCanalN" class="">Ejecutivo canal</label>
    <input style="border: none;" type="text" placeholder="Ejecutivo canal" id="ejecutivoCanalN" name="ejecutivoCanalN" class="border-bottom form-control text-uppercase ejecutivoCanalN" autocomplete="off" required="" >
  </div>
  <div class="col-md-2">
    <label for="comision">Cobra Comision</label>
    <div class="switch">
      <div class="onoffswitch">
        <input type="checkbox" class="onoffswitch-checkbox" id="comision" value="si" name="comision" >
        <label class="onoffswitch-label" for="comision">
          <span class="onoffswitch-inner"></span>
          <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </div>
  </div>
  </div>

<div class="row" id="comisionHtml">
</div>
<hr><div class="row">
<div class="col-md-12 text-center"><h2>Datos de instalación</h2></div>
</div><hr>


<div role="tablist">
  <input type="radio" title="taller" name="lugarInstacion" id="taller" role="tab" aria-controls="taller-panel" checked value="Taller"  />
  <input type="radio" title="domicilio" name="lugarInstacion" id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente"  />
  <input type="radio" title="concesionario" name="lugarInstacion" id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario"  />

  <div class="tabs" aria-hidden="true">
    <label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
    <label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
    <label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
  </div>
  <div class="panels">
    <ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">
      <h2>TALLER</h2>
      <p>Paseo de la republica 4925 </p>
      <p>Surquillo, Lima, Lima</p>
      <input type="hidden" name="direccionTaller" value="Paseo de la Republica 4925,Surquillo, Lima, Lima">

      <div class="row">
        <div class="row">
<!--           <div class="form-group col-md-4">
           <div class="row">
            <div class="form-group col-md-6">   
              <label  class="">Fecha</label>
              <div class="input-group date">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control"  id="fechaIT" name="fechaIT" autocomplete="off" required >
              </div>
            </div>
            <div class="form-group col-md-6">
              <label  class="">Hora</label>
              <div class="input-group" data-autoclose="true">
                <input type="text" class="form-control " id="horaIT" name="horaIT" autocomplete="off" required >
                <span class="input-group-addon">
                  <span class="fa fa-clock-o"></span>
                </span>
              </div>
            </div>
          </div>
          </div>   -->                                      
          <div class="form-group col-md-6">
            <label for="nombreContactoIT" class="">Nombre Contacto *</label>
            <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIT" name="nombreContactoIT" class="form-control x2 text-capitalize border-bottom nombreContactoo" required="" >
          </div><div class="form-group col-md-6">
          <label for="numeroContactoIT" class="">Número Contacto *</label>
          <input style="border: none;" type="text" placeholder=" Número Contacto *" id="numeroContactoIT" name="numeroContactoIT" class="form-control x2 text-capitalize border-bottom" required="" >
        </div>
      </div>
    </div>
  </ul>
  <ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab"> 
    <h2>Domicilio Cliente</h2>

    <div class="row">
      <div class="row">

       <!--  <div class="form-group  col-md-4">          
           <label for="departamentoInstalacionD" class="">Departamento</label>
              <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize" required name="departamentoInstalacionD" id="departamentoInstalacionD">
              <?php if (!empty($objDatosC->departamento)): ?>
                <option class="text-capitalize" value="<?php echo $objDatosC->departamento ?>" selected ><?php $departamento=$objDatosC->obtenerDepartamentoPorId($objDatosC->departamento) ?><?= $departamento->departamento ?></option>                
              <?php endif ?>
                <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                <?php if ($objDepartamento): ?>
                  <?php foreach ($objDepartamento as $var): ?>
                    <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                  <?php endforeach ?>
                <?php else: ?>
                <?php endif ?>
              </select>
        </div>
        <div class="form-group  col-md-4">
          <label for="provinciaInstalacionD" class="">Provincia</label>
              <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
              <?php if (!empty($objDatosC->provincia)): ?>
              <option class="text-capitalize" value="<?php echo $objDatosC->provincia ?>"  selected ><?php $provincia=$objDatosC->otenerProvinciaPorId($objDatosC->provincia) ?><?php echo $provincia->provincia ?></option>
              <?php endif ?>
              </select>

        </div>
        <div class="form-group  col-md-4">
          <label for="distritoInstalacionD" class="">Distrito</label>
          <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize" required name="distritoInstalacionD"  id="distritoInstalacionD" >
          <?php if (!empty($objDatosC->distrito)): ?>
          <option class="text-capitalize" value="<?php echo $objDatosC->distrito ?>"  selected ><?php $distrito=$objDatosC->obtenerDistritoPorId($objDatosC->distrito) ?><?= $distrito->distrito ?></option>
            
          <?php endif ?>
          </select>
        </div> -->

      </div>
      <div class="row">
        <div class="form-group  col-md-12">
          <label for="direccionInstalacionD" class="">Observacion de instalacion *</label>
          <input style="border: none;" type="text" placeholder="Observacion de instalacion *" id="direccionInstalacionD" name="direccionInstalacionD" class="form-control border-bottom"  >
        </div>

     <!--    <div class="form-group  col-md-5">
          <label for="referenciaInstalacionD" class="">Referencia *</label>
          <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" name="referenciaInstalacionD" class="form-control border-bottom" value="<?php echo $objDatosC->referencia ?>" required>
        </div>
        <div class="form-group  col-md-2">
          <label for="coordenadasInstalacion" class="">Cordenadas Instalacion *</label>
          <input style="border: none;" type="text" placeholder="Cordenadas Instalacion *" id="coordenadasInstalacion" name="coordenadasInstalacion" class="form-control border-bottom"  >
        </div> -->
      </div>      
      <div class="row">

    <!--     <div class="form-group col-md-4">
          <div class="row">
            <div class="form-group col-md-6">   
              <label  class="">Fecha</label>
              <div class="input-group date">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control fechaa"  id="fechaID" name="fechaID" autocomplete="off" required>
              </div>
            </div>
            <div class="form-group col-md-6">
              <label  class="">Hora</label>
              <div class="input-group" data-autoclose="true">
                <input type="text" class="form-control horaa" id="horaID" name="horaID" autocomplete="off" required>
                <span class="input-group-addon">
                  <span class="fa fa-clock-o"></span>
                </span>
              </div>
            </div>
          </div>
        </div>   -->                    
        <div class="form-group col-md-6">
          <label for="nombreContactoID" class="">Nombre Contacto *</label>
          <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoID" name="nombreContactoID" class="form-control x2 text-capitalize border-bottom nombreContactoo" required >
        </div><div class="form-group col-md-6">
        <label for="numeroContactoID" class="">Número Contacto *</label>
        <input style="border: none;" type="text" placeholder=" Número Contacto *" id="numeroContactoID" name="numeroContactoID" class="form-control x2 text-capitalize border-bottom" required>
      </div>
    </div>
  </div>
</ul>

<ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">
  <div class="row">
    <div class="col-md-6"><h2>Concecionario</h2></div>
   
  </div>
  <div class="row">
 <!--    <div class="row">
      <div class="form-group  col-md-4">
        <label for="departamentoInstalacionC" class="">Departamento*</label>     
        <select  style="border: none;" class="departamentoInstalacionC border-bottom form-control text-capitalize" required name="departamentoInstalacionC" id="departamentoInstalacionC">    
        <option class="text-capitalize" disabled selected >Seleccione Departamento</option>           
                <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                <?php if ($objDepartamento): ?>
                  <?php foreach ($objDepartamento as $var): ?>
                    <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                  <?php endforeach ?>
                <?php else: ?>
                <?php endif ?>
        </select>
      </div>
      <div class="form-group  col-md-4">
        <label for="provinciaInstalacionC" class="">Provincia*</label>
         <select  style="border: none;" class="provinciaInstalacionC border-bottom form-control text-capitalize" required name="provinciaInstalacionC" id="provinciaInstalacionC" >
              </select>

      </div>
      <div class="form-group  col-md-4">
        <label for="distritoInstalacionC" class="">Distrito*</label>
       <select  style="border: none;" class="distritoInstalacionC border-bottom form-control text-capitalize" required name="distritoInstalacionC"  id="distritoInstalacionC" >         
          </select>

      </div>

    </div> -->
    <div class="row">
      <div class="form-group  col-md-8">
        <label for="direccionInstalacionC" class="">Observacion de instalacion *</label>
        <input style="border: none;" type="text" placeholder="Observacion de instalacion *" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom" required >

      </div>
         <div class="form-group col-md-4">
        <label for="concesionarioInstalacion" class="">Nombre de concesionario*</label>
        <input style="border: none;" type="text" placeholder="Nombre de concesionario*" id="concesionarioInstalacion" name="concesionarioInstalacion" class="concesionario form-control border-bottom" autocomplete="off" required >
      </div>
  <!--     <div class="form-group  col-md-6">
        <label for="referenciaInstalacionC" class="">Referencia *</label>
        <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom" required>
      </div> -->
    </div>      
    <div class="row">
<!--      <div class="form-group col-md-4">
       <div class="row">
                  <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
                      <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control fechaa"  id="fechaIC" name="fechaIC" autocomplete="off" required>
                      </div>
                  </div>
                  <div class="form-group col-md-6">
                                <label  class="">Hora</label>
                    <div class="input-group" data-autoclose="true">
                        <input type="text" class="form-control horaa" id="horaIC" name="horaIC" autocomplete="off" required>
                        <span class="input-group-addon">
                            <span class="fa fa-clock-o"></span>
                        </span>
                    </div>
                  </div>
                </div>
      </div> -->
    <div class="form-group col-md-6">
      <label for="nombreContactoIC" class="">Nombre Contacto *</label>
      <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIC" name="nombreContactoIC" class="form-control x2 text-capitalize border-bottom nombreContactoo" required>
    </div><div class="form-group col-md-6">
    <label for="numeroContactoIC" class="">Número Contacto *</label>
    <input style="border: none;" type="text" placeholder="Número Contacto *" id="numeroContactoIC" name="numeroContactoIC" class="form-control x2 text-capitalize border-bottom" required>
  </div>
</div>
</div>
</ul>
</div>

</div>
<div class="row">  <div class="form-group col-md-12">
  <label for="observaciones" class="">OBSERVACIONES</label>
  <input style="border: none;" type="text" placeholder="Observacion" id="observaciones" name="observaciones" class="border-bottom form-control text-capitalize"   value="<?php echo $retVal = (isset($_POST['observaciones'])) ? $_POST['observaciones'] : "" ; ?>">
</div></div>
<div class="row">
 <div class="form-group col-md-12">
   <input type="hidden" name="token" value="<?php echo Token::generar() ?>">
   <button class="add_button btn btn-success col-md-12" id="agregarVehiculoBttn" title="Agregar Vehiculos" name="agregarVehículo" >Registrar Vehículo <i class="fa fa-plus-circle "></i></button> 
 </div>
</div>
</div>
</div>  
</form>
<div class="hr-line-dashed"></div>
<h2 class="text-center">Vehiculos existentes de cliente</h2>
<div class="hr-line-dashed"></div>
<?php if ($vehiculoObj->verificarExistenciaVehiculos($id)): ?>
  <div class="ibox float-e-margins">

    <div class="ibox-content">
      <input type="text" class="form-control input-sm m-b-xs" id="filter"
      placeholder="Filtrar vehículo">

      <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
        <thead>
          <tr>
            <th>#</th>
            <th>Placa</th>
            <th>Marca(s)</th>
            <th>modelo(s)</th>
            <th>Estado(s)</th>
            <th data-hide="all">Plan</th>
            <th data-hide="all">Plan Tiempo</th>
            <th data-hide="all">Chasis</th>
            <th data-hide="all">Motor</th>
            <th data-hide="phone,tablet">Color</th>
            <th data-hide="phone,tablet">Año</th>
            <th data-hide="all">Seguro</th>
            <th data-hide="all">Poliza n°</th>
            <!-- <th data-hide="all">Task</th> -->
          </tr>
        </thead>    
        <tbody>

          <?php $vehiculoObj->obtenerVehiculos($id) ?>

        </tbody>
        <tfoot>
          <tr>
            <td colspan="5">
              <ul class="pagination pull-right"></ul>
            </td>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>


<?php else: ?>
</div>
<?php endif ?>
<!-- MODAL DE CONTACTOS AUTORIZADOS -->
<div class="modal inmodal" id="autorizados" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated flipInX">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="">Cerrar</span></button>
        <h4 class="modal-title">Contactos Autorizados</h4>
        <small class="font-bold">Ingrese aquí uno o varios contactos autorizados.   <div class="form-group ">
         <a href="javascript:void(0);" class="add_button btn btn-info col-md-12 " title="Agregar contacto">Agregar <i class="fa fa-plus-circle "></i></a>
       </div></small>
     </div>
     <form  role="form" method="post" >

      <div class="modal-body" style="background: #fff">
        <?php 

        echo '<div class="field_wrapper">';
        if($objCliente->mostrarContactosAutorizados($id)){  
         echo '</div>';
       }else{
         ?>
         <div class="field_wrapper">
          <div class="row">
            <div class="form-group col-md-3">
              <label for="dniC" class="">DNI no Obligatorio</label>
              <input style="border: none;" type="text" placeholder="DNI no Obligatorio" id="dniC" name="field_name[0][]" class="form-control border-bottom "   data-mask="99999999" id="dniI" >
            </div>
            <div class="form-group col-md-3">
              <label for="nombreC" class="">Nombres *</label>
              <input style="border: none;" type="text" placeholder="Nombres *" id="nombreC" name="field_name[1][]" class="form-control border-bottom text-capitalize"  required id="name" >
            </div>
            <div class="form-group col-md-3">
              <label for="apellidos" class="">Apellidos *</label>
              <input style="border: none;" type="text" placeholder="Apellidos *" id="apellidos" name="field_name[2][]" class="form-control x2 text-capitalize border-bottom text-capitalize" required >
            </div>
            <div class="form-group col-md-3">
             <label for="dniC" class="">Celular *</label>
             <input style="border: none;" type="text" placeholder="celular *" id="celular" name="field_name[3][]" class="form-control border-bottom " required  data-mask="999 999 999" id="celular" >
           </div>

         </div> 
         <div class="row">

          <div class="form-group col-md-4">
            <label for="telefono" class="">Telefono</label>
            <input style="border: none;" type="text" placeholder="telefono" id="telefono" name="field_name[4][]" class="form-control border-bottom text-capitalize"   id="telefono" >
          </div>
          <div class="form-group col-md-4">
            <label for="correo" class="">Correo</label>
            <input style="border: none;" type="text" placeholder="Correo" id="correo" name="field_name[5][]" class="form-control x2 text-capitalize border-bottom text-capitalize" >
          </div>               
          <div class="form-group col-md-4">
           <label for="Parentesco" class="">Parentesco *</label>
           <select  style="border: none;" class="tecnico border-bottom form-control" required name="field_name[6][]">
            <option class="" value="" disabled selected >Seleccione Parentesco *</option>
            <option value="Usuario">Usuario(a)</option>           
            <option value="padre">Padre</option>           
            <option value="Madre">Madre</option>           
            <option value="Hermano">Hermano(a)</option>           
            <option value="Conyuge">Conyuge</option>           
            <option value="Hijo">Hijo(a)</option>           
            <option value="Nieto">Nieto(a)</option>           
            <option value="Cuñado">Cuñado(a)</option>           
            <option value="Suegro">Suegro(a)</option>           
          </select>
        </div>


        <div class="hr-line-dashed"></div>
        <hr>
      </div>
    </div>
    <?php 
  } ?>

</div>

<div class="modal-footer">
  <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
  <button type="submit" name="registrarContacto" class="btn btn-primary">Guardar Cambios</button>
</div>
</form>   
</div>
</div>
</div>
<!-- FIN DE CONTACTOS AUTORIZADOS -->





</div>
</div>

</div>
<?php include("../includes/footer.php") ?>
</div>
</div>
<!-- Mainly scripts -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<!-- Steps -->
<script src="../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="../assets/js/cotizar.js"></script>
<script src="../assets/js/cotizarEmpresa.js"></script>
<script src="../assets/js/plugins/select2/select2.full.min.js"></script>

<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/build/jquery.datetimepicker.full.js"></script>

<script src="../assets/js/validacionFactura.pabel.js"></script>
<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
 <!-- <?php include_once('../includes/script.php') ?>  -->
 <!-- <?php include_once('../includes/campanasScript.php') ?>  -->
<script>
  <?php if (!empty($errores)): ?>
  <?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
<?php endforeach ?> 
<?php endif ?>

<?php if(!empty($echo)): ?>
  <?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
<?php endforeach ?>
<?php endif ?> 
<?php if ($datos=DatosFacturacion::horariosSugeridosParaEntregaDeFactura()): ?>
$('#horarioEntrega').typeahead({
  source: [
    <?php foreach ($datos as $var): ?>
      {"name": "<?= $var->fecha ?>"},  
    <?php endforeach ?>
    ]
  }); 
<?php endif ?>
<?php if($canales=Canales::verTiendadeCanal()): ?>
    $('.tienda').typeahead({
        source: [
        <?php foreach ($canales as $var):?>
        {"name": "<?php echo $var->tiendaCanal ?>"},        
      <?php endforeach ?>
      ]
    }); 
<?php endif ?>
<?php if ($datosD=Cliente::otenerTodoLosDistritos()): ?>
$('#distritoEntrega').typeahead({
  source: [
    <?php foreach ($datosD as $var): ?>
      {"name": "<?php echo $var->distrito ?>"},  
    <?php endforeach ?>
    ]
  }); 
<?php endif ?>
    $(".departamento").change(function(){provincias();});
    $(".departamento2").change(function(){provincias2();});
    $(".departamento3").change(function(){provincias3();});
    $(".departamentoInstalacionD").change(function(){provincias4();});
    $(".departamentoInstalacionC").change(function(){provincias5();});
    $(".provincia1").change(function(){distritos();});
    $(".provincia2").change(function(){distritos2();});
    $(".provincia3").change(function(){distritos3();});
    $(".provinciaInstalacionD").change(function(){distritos4();});
    $(".provinciaInstalacionC").change(function(){distritos5();});
    function provincias(){
      var iddepartamento = $(".departamento").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/provincia',
        data :  'iddepartamento='+ iddepartamento,
        success : function(data)
        {
          $(".provincia1").html(data);
        }
      });
      return false;
    }
    function provincias2(){
      var iddepartamento = $(".departamento2").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/provincia',
        data :  'iddepartamento='+ iddepartamento,
        success : function(data)
        {
          $(".provincia2").html(data);
        }
      });
      return false;
    }
    function provincias3(){
      var iddepartamento = $(".departamento3").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/provincia',
        data :  'iddepartamento='+ iddepartamento,
        success : function(data)
        {
          $(".provincia3").html(data);
        }
      });
      return false;
    }
    function provincias4(){
      var iddepartamento = $(".departamentoInstalacionD").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/provincia',
        data :  'iddepartamento='+ iddepartamento,
        success : function(data)
        {
          $(".provinciaInstalacionD").html(data);
        }
      });
      return false;
    }
    function provincias5(){
      var iddepartamento = $(".departamentoInstalacionC").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/provincia',
        data :  'iddepartamento='+ iddepartamento,
        success : function(data)
        {
          $(".provinciaInstalacionC").html(data);
        }
      });
      return false;
    }
    function distritos(){
      var idprovincia = $(".provincia1").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritos1").html(data)
        }
      });
      return false;
    }
    function distritos2(){
      var idprovincia = $(".provincia2").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritos2").html(data)
        }
      });
      return false;
    }
    function distritos3(){
      var idprovincia = $(".provincia3").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritos3").html(data)
        }
      });
      return false;
    }
    function distritos4(){
      var idprovincia = $(".provinciaInstalacionD").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionD").html(data)
        }
      });
      return false;
    }
    function distritos5(){
      var idprovincia = $(".provinciaInstalacionC").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionC").html(data)
        }
      });
      return false;
    }

$('#pagoencaja').click(function(){
    if($(this).is(':checked')){
        $('.voucherimn').hide('slow');
        $('#voucherImg').prop('disabled',true);
        $('#comision').prop('disabled', true);
    }else{
        $('.voucherimn').show('slow');
        $('#voucherImg').prop('disabled',false);
        $('#comision').prop('disabled', false);
    }
});


$(document).ready(function(){
$("#registrarVehiculoF").validate();
      <?php if($canales=Canales::verNombreCanal()): ?>
      $('.nombreCanal').typeahead({
        source: [
        <?php foreach ($canales as $var):?>
        {"name": "<?php echo $var->nombreCanal ?>"},        
      <?php endforeach ?>
      ]
    }); 
    <?php endif ?>   
      <?php if($canales1=Canales::verEjecutiVodeCanal()): ?>
      $('.ejecutivoCanalN').typeahead({
        source: [
        <?php foreach ($canales1 as $var):?>
        {"name": "<?php echo $var->ejecutivoCanal ?>"},        
      <?php endforeach ?>
      ]
    }); 
    <?php endif ?>
    
/////////////
$('.b').hide();           
$('.tiendaAparece').hide();           
$('.tiendaAparece1').hide();           
//$('#horabtn').hide();           
$('#tipoCanal').change( function() {
   if(this.value=='CONVENIO'){
      $('.b').show(); 
      $('.a').hide(); 
    $('.tiendaAparece1').hide();          
      $('#convenio').on('change', function() {
        if(this.value=='1'){
          $('.tiendaAparece').show();
          $('.tiendaAparece').prop('disabled',false);
        }else{
          $('.tiendaAparece').prop('disabled',true);
          $('.tiendaAparece').hide();          
        }
      });
      $('#ejecutivoCanalN').prop('disabled', false);
      $('#comision').prop('checked', false);
      $('#comision').prop('disabled', true);
      $('#ejecutivoCanalN').prop('disabled', false);            
   }else if(this.value=='CONCESIONARIO'){
        $('.tiendaAparece1').show();
      $('#ejecutivoCanalN').prop('disabled', false);
        if($("#pagoencaja").is(':checked')){
            $('#comision').prop('disabled', true);
        }else{
            $('#comision').prop('disabled', false);
        }
      $('.b').hide(); 
      $('.a').show(); 
   }else if(this.value=='TRACKLINK OCTO'){
      $('.tiendaAparece1').hide('slow');
      $('#comision').prop('disabled', true);
      $('#ejecutivoCanalN').prop('disabled', true);
      $('.b').hide(); 
      $('.a').show(); 
   }else{
      $('#ejecutivoCanalN').prop('disabled', false);
    $('.tiendaAparece1').hide('slow');
      $('#comision').prop('disabled', false);
        if($("#pagoencaja").is(':checked')){
            $('#comision').prop('disabled', true);
        }else{
            $('#comision').prop('disabled', false);
        }
    $('.b').hide(); 
      $('.a').show(); 
   }
});

$(".esconderNo").hide();
$(".esconderNo").prop("disabled",true);
$("#entrega").change(function(){
        var r=$(this).val();
        if(r=='si'){
            $(".esconderNo").show('slow');
            $(".esconderNo").prop("disabled",false);
        }else{
            $(".esconderNo").hide('slow');
            $(".esconderNo").prop("disabled",true);
        }
        });
}); 

///////////////////////////////////////////////////

</script>
</body>
</html>
<?php else: ?>
  <h2>presione f5</h2>
  <?php  irA("ingresar") ?> 
<?php endif ?>

<?php
require __DIR__ . "/../../vendor/autoload.php";
require_once("../../includes/init.php");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// $loader = new \Twig\Loader\ArrayLoader([
//     'index' => 'Hello {{ name }}!',
// ]);
// $twig = new \Twig\Environment($loader);

// echo $twig->render('index', ['name' => 'Fabien']);

//$template = $twig->load('./templates/ReclamoClienteNotificacion.html');

$idreclamo_cliente = $_POST['idreclamo_cliente'];
$reporte = new  ReclamoClienteReporteClass();
$datosReclamo = $reporte->obtenerReporteReclamosClientePorId($idreclamo_cliente);
$datosReclamo = $datosReclamo[0];

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

$loader = new FilesystemLoader(__DIR__ . '/templates');
$twig = new Environment($loader);

$words = ['campeon', null, 'rock', 'police', 'forest'];

$vreclamo_cliente        = $datosReclamo->vreclamo_cliente;
$area_responsable_nombre = $datosReclamo->area_responsable_nombre;
$area_responsable_email  = $datosReclamo->area_responsable_email;
$cliente_nombre          = $datosReclamo->cliente_nombre;
$canal_descripcion       = $datosReclamo->canal_descripcion;
$area_responsable_nombre = $datosReclamo->area_responsable_nombre;
$tmotivo_reclamo         = $datosReclamo->tmotivo_reclamo;
$tsolicitud_reclamo      = $datosReclamo->tsolicitud_reclamo;

$dataTemplate = [
    "urlImage"                => getBaseUrlImages(),
    "vreclamo_cliente"        => $vreclamo_cliente,
    "tmotivo_reclamo"         => $tmotivo_reclamo,
    "tsolicitud_reclamo"      => $tsolicitud_reclamo,
    "cliente_nombre"          => mb_strtoupper($cliente_nombre, "UTF-8"),
    "canal_descripcion"       => mb_strtolower($canal_descripcion,"UTF-8"),
    "area_responsable_nombre" => mb_strtoupper($area_responsable_nombre, "UTF-8")
];

$bodyEmail = $twig->render('ReclamoClienteNotificacion.html.twig', $dataTemplate);

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->CharSet = 'UTF-8';
    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = MAIL_HOST;              // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = MAIL_USERNAME;                // SMTP username
    $mail->Password = MAIL_PASSWORD;                           // SMTP password
    $mail->SMTPSecure = MAIL_SECURE;                            // Enable SSL encryption, TLS also accepted with port 587
    $mail->Port = MAIL_PORT;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom(MAIL_USERNAME, 'Tracklink');
    // $mail->addAddress($area_responsable_email, $area_responsable_nombre);
    $mail->addAddress('centralemergencia@tracklink.pe', $area_responsable_nombre);    // Add a recipient ---------------- DESCOMENTAR
    //$mail->addAddress('ezarate@tracklink.pe', $area_responsable_nombre);
    $mail->addCC('kvega@tracklink.pe', $area_responsable_nombre);    // Add a recipient
    //$mail->addAddress('contact@example.com');               // Name is optional
    // $mail->addReplyTo('aloayza@tracklink.pe', $area_responsable_nombre);
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    $mail->AddEmbeddedImage('templates/images/tracklink2.png','logo');
    $mail->AddEmbeddedImage('templates/images/facebook.png','facebook');
    $mail->AddEmbeddedImage('templates/images/twitter.png','twitter');
    //$mail->AddEmbeddedImage('templates/images/youtube.png','youtube');
    $mail->AddEmbeddedImage('templates/images/instagram.png','instagram');
    $mail->AddEmbeddedImage('templates/images/linkedin.png','linkedin');
    
    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = '🚨 Acaba de ingresar un nuevo reclamo!';
    #$mail->Body ='<img src="cid:logo">';
    $mail->Body    = $bodyEmail;
    //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    //echo 'Message has been sent';
    echo json_encode(["send"=>1, "msg"=>"Se envió notificación al area correspondiente."], JSON_UNESCAPED_UNICODE);
} catch (Exception $e) {
    //echo 'Message could not be sent.';
    //echo 'Mailer Error: ' . $mail->ErrorInfo;
    echo json_encode(["send"=>0, "msg"=>$mail->ErrorInfo], JSON_UNESCAPED_UNICODE);
}
?>
<?php
require __DIR__ . "/../../vendor/autoload.php";
require_once("../../includes/init.php");

$baseUrl = getBaseUrl();
$urlNotificacion = "http://localhost/com/email/ReclamoClienteNotificacion";

use Curl\Curl;

$curl = new Curl();
$curl->post($urlNotificacion,[
    'idreclamo_cliente' => 4
]);

if ($curl->error) {
    echo 'Error: ' . $curl->errorMessage . "\n";
    $curl->diagnose();
} else {
    //echo 'Response:' . "\n";
    //echo($curl->response);
    $response = json_decode($curl->response);
    echo $response->send;
}
?>
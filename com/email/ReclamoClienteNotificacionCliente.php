<?php
require __DIR__ . "/../../vendor/autoload.php";
require_once("../../includes/init.php");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$idreclamo_cliente = $_POST['idreclamo_cliente'];
//$cliente_nombre = $_POST['cliente_nombre'];
$reporte = new  ReclamoClienteReporteClass();
$datosReclamo = $reporte->obtenerReporteReclamosClientePorId($idreclamo_cliente);
$datosReclamo = $datosReclamo[0];

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

$loader = new FilesystemLoader(__DIR__ . '/templates');
$twig = new Environment($loader);

$vreclamo_cliente        = $datosReclamo->vreclamo_cliente;
$fecha_reclamo           = $datosReclamo->fecha_reclamo;
$area_responsable_nombre = $datosReclamo->area_responsable_nombre;
$area_responsable_email  = $datosReclamo->area_responsable_email;
$cliente_nombre          = $datosReclamo->cliente_nombre;
$canal_descripcion       = $datosReclamo->canal_descripcion;
$area_responsable_nombre = $datosReclamo->area_responsable_nombre;
$cliente_email           = explode(',', $datosReclamo->cliente_email);
if(count($cliente_email)>0){
    $cliente_email = trim($cliente_email[0]);
}else{
    $cliente_email = "";
}

if($cliente_email==""){
    echo json_encode(["send"=>0, "msg"=>"No se encontró email del cliente."], JSON_UNESCAPED_UNICODE);
}else{

    $dataTemplate = [
        "urlImage"                => getBaseUrlImages(),
        "vreclamo_cliente"        => $vreclamo_cliente, 
        "fecha_reclamo"           => str_replace('-','/',$fecha_reclamo),
        "cliente_nombre"          => $cliente_nombre,
        "canal_descripcion"       => mb_strtolower($canal_descripcion,"UTF-8"),
        "area_responsable_nombre" => mb_strtoupper($area_responsable_nombre, "UTF-8")
    ];

    $bodyEmail = $twig->render('ReclamoClienteNotificacionCliente.html.twig', $dataTemplate);

    $mail = new PHPMailer(true); 
    try {
        //Server settings
        $mail->CharSet = 'UTF-8';
        //$mail->SMTPDebug = 2;
        $mail->isSMTP();   
        $mail->Host = MAIL_HOST;
        $mail->SMTPAuth = true;
        $mail->Username = MAIL_USERNAME;
        $mail->Password = MAIL_PASSWORD;
        $mail->SMTPSecure = MAIL_SECURE;
        $mail->Port = MAIL_PORT;

        //Recipients
        $mail->setFrom(MAIL_USERNAME, 'Tracklink');
        $mail->addAddress($cliente_email, $cliente_nombre);   
        //$mail->addAddress('ezarate@tracklink.pe', 'EDWARD DANIEL ZARATE');         
        #$mail->addAddress('erikgv20@gmail.com', $cliente_nombre);     // Add a recipient
        $mail->addReplyTo('centralemergencia@tracklink.pe', $area_responsable_nombre);     // Add a recipient  -----------DESCOMENTAR
        //$mail->addReplyTo('ezarate@tracklink.pe', 'EDWARD DANIEL ZARATE');       // borrar xd
        $mail->addCC('centralemergencia@tracklink.pe', $area_responsable_nombre);     // Add a recipient       -----------DESCOMENTAR
        $mail->addCC('kvega@tracklink.pe', 'Desarrollo de Sistemas');     // Add a recipient

        //$mail->addAddress('contact@example.com');               // Name is optional
        //$mail->addReplyTo('info@example.com', 'Information');
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');

        //Attachments
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        $mail->AddEmbeddedImage('templates/images/tracklink2.png','logo');
        $mail->AddEmbeddedImage('templates/images/facebook.png','facebook');
        $mail->AddEmbeddedImage('templates/images/twitter.png','twitter');
        //$mail->AddEmbeddedImage('templates/images/youtube.png','youtube');
        $mail->AddEmbeddedImage('templates/images/instagram.png','instagram');
        $mail->AddEmbeddedImage('templates/images/linkedin.png','linkedin');
        
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = '📑 Hola, acaba de registrar un nuevo reclamo!';
        #$mail->Body ='<img src="cid:logo">';
        $mail->Body    = $bodyEmail;
        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        //echo 'Message has been sent';
        echo json_encode(["send"=>1, "msg"=>"Se envió notificación al cliente."], JSON_UNESCAPED_UNICODE);
    } catch (Exception $e) {
        //echo 'Message could not be sent.';
        //echo 'Mailer Error: ' . $mail->ErrorInfo;
        echo json_encode(["send"=>0, "msg"=>$mail->ErrorInfo], JSON_UNESCAPED_UNICODE);
    }

}
?>
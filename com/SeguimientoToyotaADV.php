<?php
    $nav="SeguimientoToyotaADV";
    include("../includes/head.php");
    if (!$session->yaEstaIngresado()) {
        irA("../ingresar");
    }
    $obMenu=Usuario::obtenerMenu($session->idusuario);

    if (isset($_POST['guardarAnulaToyota'])) {
       if(Token::verificar($_POST['guardarAnulaToyota'])) {
           $tramaToyota = new TramaIntegracionTY();
           $idTrama = $_POST['idTrama'];
           $tipoAnulacion =limpiar($_POST['anulacion']);
           $motivo  = limpiar($_POST['motivoAnulacion']);
           $descestado = "";
           if($tipoAnulacion=="rechazado"){
                $descestado="rejected";
           }
           if($tipoAnulacion=="cancelado"){
               $descestado="cancelled";
           }

           if($descestado) {
               // notificar instalacion toyota ---  inicio
               $path = "/sendStatusVehicle";
               $param = array(
                   "chasis" => $_POST["chasis"],
                   "estado" => $descestado,
                   "message" => $motivo
               );
               $rptaService = APIToyota::PostService($path, $param);

               if($rptaService) {
                   if (isset($_POST['idOperacion'])) {
                       $idopera = $_POST['idOperacion'];
                       if ($idopera != "") {
                           $operacionesObj = new Operacion();
                           $vehiculoObj = new Vehiculo();
                           $facturacionObj = new DatosFacturacion();
                           $datosOld = new SeguimientoConsulta();
                           $ultimoid = $datosOld->ctraerultimoseguimiento();
                           //anular vehículo
                           $datosOperacion = $operacionesObj->obtenerTodolosDatosPoridOperacion($idopera);
                           $vehiculoObj->idvehiculo = $datosOperacion->idvehiculo;
                           $vehiculoObj->obervaciones = $motivo;
                           if (!$vehiculoObj->guardarComoEliminado($ultimoid, $idopera)) {
                               $errores[] = "No se pudo actualizar vehículo ";
                           }
                           // anular facturación
                           $facturacionObj->datosfacturacionid = $datosOperacion->datosfacturacionid;
                           $facturacionObj->apunte = $motivo;
                           if (!$facturacionObj->marcarComoAnulado($ultimoid, $idopera)) {
                               $errores[] = "No se pudo actualizar datos de facturación ";
                           }
                           // anular operaciones
                           $operacionesObj->observacion = $motivo;
                           $operacionesObj->idoperacion = $idopera;
                           if (!$operacionesObj->marcarComoAnulado($ultimoid, $idopera)) {
                               $errores[] = "No se pudo actualizar datos de operación ";
                           }
                       }
                   }
                   $tramaToyota->idtrama = $idTrama;
                   $tramaToyota->anulacionTrama($motivo,$tipoAnulacion);
                   if($_POST['tipoTransaccion']=="Vinculacion"){
                       $tramaToyota->idtrama = limpiar($_POST['idTrama_vinculacion']);
                       $tramaToyota->anulacionTrama($motivo,$tipoAnulacion);
                   }

                   $echo[] = "Se Anulo Correctamente";
               }
           }
       }
    }

?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="../assets/css/sweetalert2.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/css2/style2.css">
<link rel="stylesheet" href="../assets/css2/style3.css">
<link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<style>
    .table-cabecera-horario{
        background:#075598!important;
        color:white !important;
        vertical-align: middle!important;
    }
    .table-cabecera-horario2{
        background:#164764 !important;
        color:white !important;
        vertical-align: middle!important;
    }
    .table-cabecera-horario3{
        background: #096452 !important;
        color:white !important;
    }
    .table-cabecera-horario4{
        background: #0e2e79 !important;
        color:white !important;
    }

    .table-scroll {
        position:relative;
        max-width:1600px;
        margin:auto;
        overflow:hidden;
        /*border:.5px solid #000;*/
    }

    .table-wrap {
        width:100%;
        overflow:auto;
    }

    .table-scroll table {
        width:100%;
        margin:auto;
        border-collapse:separate;
        border-spacing:0;
    }

    .table-scroll th, .table-scroll td {
        padding:5px 10px;
        border-bottom:1px solid;
        border-right:1px solid;
        border-color: #DDDDDD;;
        #background:#fff;
        color:rgb(103, 106, 108) ;
        white-space:nowrap;
        vertical-align:top;
        font-size: 12px
    }

    .table-scroll thead, .table-scroll tfoot {
        background: #f9f9f9;
    }

    .bordertab{
        border-bottom: 1px #e1d0d0 solid !important;
        border-left: 1px #e1d0d0 solid !important;
    }
</style>
<body>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <?php include("../includes/menuIzquierdo.php") ?>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../includes/navegacionArriba.php") ?>
            </nav>
        </div>
        <div class="wrapper wrapper-content">
            <div class="row ibox float-e-margins ibox-content">
                <div class="col-md-12" style="padding: 0">
                    <div id="table-scroll" class="table-scroll">
                        <div class="table-wrap">
                            <form action="PHPExel/TramaToyotaExcel" method="POST">
                                <div class="col-md-6">
                                    <div class="col-lg-7">
                                        <label for="convenio" class="">Seleccione rango fecha Recibida *</label>
                                        <input style="border: none;"  class="cd-select form-control border-bottom" required  type="text" placeholder="Seleccione Rango de fecha" class="form-control" value="" name="fecha" id="rango" autocomplete="off">
                                    </div>
                                    <div class="col-md-5" id="resultado1">
                                        <button class="btn btn-primary" name="Descargar" id="resultado1">Descargar Trama en Excel</button>
                                    </div>
                            </div>
                            <div class="col-md-2">
                                <label for="cboEstado">Tipo Registro</label>
                                <select style="border: none;margin-bottom: 15px" class="border-bottom form-control text-capitalize"  id="cboTipoReg" name="cboTipoReg" aria-required="true">
                                    <option value="" selected>Todos</option>
                                    <option value="'Directa'">Directa</option>
                                    <option value="'Pre-Registro'" >Pre-Registro</option>
                                    <option value="'Vinculacion'" >Vinculacion</option>
                                    <option value="'Pre-Registro','Vinculacion'" >Pre-Registro y Vinculación</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="cboEstado">Estado Activación</label>
                                <select style="border: none;margin-bottom: 15px" class="border-bottom form-control text-capitalize" id="cboEstadoAct" name="cboEstadoAct" aria-required="true">
                                    <option value="" selected>Todos</option>
                                    <option value="1">Activado</option>
                                    <option value="0" >No Activado</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="cboEstado">Estado Vinculacion</label>
                                <select style="border: none;margin-bottom: 15px" class="border-bottom form-control text-capitalize"  id="cboEstadoVin" name="cboEstadoVin" aria-required="true">
                                    <option value="" selected>Todos</option>
                                    <option value="1">Vinculado</option>
                                    <option value="0" >No Vinculado</option>
                                </select>
                            </div>
                            </form>
                            <br><br><br>


                            <input type="text" class="form-control input-sm m-b-xs" id="filterrr"
                                   placeholder="Buscar" style="margin-bottom: 15px">
                            <table class="table main-table" >
                                <thead>
                                    <tr>
                                        <th rowspan="2" class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"></th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2">Tipo Registro</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Fecha Recibido</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2">Asegurado</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2">N° Documento</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Placa</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Chasis</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> CBU</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2">Plan</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2">TIempo</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Precio Toyota</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Precio Tracklink</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Concesionario Madre</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2">Fecha Instalación</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Fecha Pre-Activacion</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Estado Pre-Activacion</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Fecha Activacion</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Estado Activacion</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Fecha Vinculación</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Estado Vinculación</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Estado Trama</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Estado Operación</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Motivo Anulación</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Fecha Anulación</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="5" rowspan="1"> Cargo </th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Seguimiento</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Acción</th>
                                    </tr>
                                 <tr>
                                     <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Registro </th>
                                     <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Vinculacion </th>
                                     <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Pre-Activación </th>
                                     <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Activación </th>
                                     <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Anulación </th>
                                 </tr>
                                </thead>
                                <tbody id="resultado">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("../includes/footer.php") ?>
    </div>
</div>


<div class="modal inmodal" id="bateriasOModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 5px;padding-bottom: 2px">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                <h4 class="modal-title">Plan Kilometros</h4>
                <small class="font-bold"><div class="form-group ">
                    </div></small>
            </div>
            <form  role="form" method="post" enctype="multipart/form-data" id="formOpe">

                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal inmodal" id="anularOModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 5px;padding-bottom: 2px">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                <h4 class="modal-title">Anular Registro</h4>
                <small class="font-bold"><div class="form-group ">
                    </div></small>
            </div>
            <form  role="form" method="post" enctype="multipart/form-data" id="formOpeAn">

                <div class="modal-body2" style="background: #fff;padding-top: 0">
                </div>
            </form>
        </div>
    </div>
</div>


<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/plugins/clockpicker/clockpicker.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/select2/select2.full.min.js"></script>
<script src="../assets/bootbox/bootbox.all.js"></script>

<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/jquery-confirm.min.js"></script>

<script src="../assets/js/plugins/ladda/spin.min.js"></script>
<script src="../assets/js/plugins/ladda/ladda.min.js"></script>
<script src="../assets/js/plugins/ladda/ladda.jquery.min.js"></script>
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>

<script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="../assets/bootbox/bootbox.all.js"></script>
<style>
    .popover {
        z-index: 999999;
    }
</style>
<?php include_once('../includes/script.php') ?>
<script>

    <?php if (!empty($errores)): ?>
    <?php foreach ($errores as $error): ?>
    toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
    <?php endforeach ?>
    <?php endif ?>
    <?php if(!empty($echo)): ?>
    <?php foreach($echo as $ok): ?>
    toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
    <?php endforeach ?>
    <?php endif ?>
</script>

<script>
    function limpiarTrama(){
        var idTrama = $("#idTrama").val('');
    }

    $('#rango').daterangepicker({
        format: 'DD-MM-YYYY',
        endDate: moment(),
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: false,
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
            'Este Mes': [moment().startOf('month'), moment().endOf('month')],
            'Anterior Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'right',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-primary',
        cancelClass: 'btn-default',
        separator: '/',
        locale: {
            applyLabel: 'Seleccionar',
            cancelLabel: 'Cancelar',
            fromLabel: 'Desde',
            toLabel: 'Hasta',
            customRangeLabel: 'Seleccionar Rango',
            daysOfWeek: ['Do', 'Lun', 'Mar', 'Mi', 'Ju', 'Vi','Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            firstDay: 1
        }
    }, function(start, end, label) {
        $('#rango').focus();
    });
</script>

<script>
    $('#bateriasOModal').on('show.bs.modal', function (e) {
        $('.modal-body').html("");
        var rowid = $(e.relatedTarget).data('idint');
        $.ajax({
            type : 'post',
            url : 'modal/seguimientoToyotaModal',
            data :  'idTrama='+ rowid,
            success : function(data){
                $('.modal-body').html(data);
            }
        });
    });


    $('#anularOModal').on('show.bs.modal', function (e) {
        $('.modal-body').html("");
        var rowid = $(e.relatedTarget).data('idint');
        $.ajax({
            type : 'post',
            url : 'modal/anularRegistroToyotaModal',
            data :  'idTrama='+ rowid,
            success : function(data){
                $('.modal-body2').html(data);
            }
        });
    });

    $('body').on('hidden.bs.modal', function () {
        if($('.modal.in').length > 0)
        {
            $('body').addClass('modal-open');
        }
    });


    $(document).ready(function(){
      $("#rango").bind("keyup focus",function()
      {
        $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
        var fecha=$(this).val();
        var page = $("#resultado").attr("data-page");
          var tr=$("#cboTipoReg option:selected").val()
          var estdAc=$("#cboEstadoAct option:selected").val()
          var estdVn=$("#cboEstadoVin option:selected").val()
        $.ajax({
            type : 'POST',
            url  : 'pag/resultadoBusquedaSeguimientoToyotaADV',
            data : {datos:'',tr:tr,estdAc:estdAc,estdVn:estdVn,page:page,fecha:fecha},
            success : function(data)
            {
                $("#resultado").html(data);
            }
         });
       });
    });


    $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
    $.ajax({
        type : 'POST',
        url  : 'pag/resultadoBusquedaSeguimientoToyotaADV',
        data :  {datos:'',tr:'',estdAc:'',estdVn:'',fecha:'',page:1},
        success : function(data)
        {
            $("#resultado").html(data);
        }
    });

    $("#cboTipoReg,#cboEstadoAct,#cboEstadoVin").change(function() {
        $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
        var datosS = $("#filterrr").val();
        datosS = datosS.replace('<>/"', '');
        var tr=$("#cboTipoReg option:selected").val()
        var estdAc=$("#cboEstadoAct option:selected").val()
        var estdVn=$("#cboEstadoVin option:selected").val()
        var fecha=$("#rango").val()
        $.ajax({
            type : 'POST',
            url  : 'pag/resultadoBusquedaSeguimientoToyotaADV',
            data :  {datos:datosS,tr:tr,estdAc:estdAc,estdVn:estdVn,fecha:fecha},
            success : function(data)
            {
                $("#resultado").html(data);
            }
        });
        return false;

    })



    $("#filterrr").keypress(function(e)
    {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){
            $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
            var datosS = $(this).val();
            datosS = datosS.replace('<>/"', '');
            var tr=$("#cboTipoReg option:selected").val()
            var estdAc=$("#cboEstadoAct option:selected").val()
            var estdVn=$("#cboEstadoVin option:selected").val()
            var fecha=$("#rango").val()
            $.ajax({
                type : 'POST',
                url  : 'pag/resultadoBusquedaSeguimientoToyotaADV',
                data :  {datos:datosS,tr:tr,estdAc:estdAc,estdVn:estdVn,fecha:fecha},
                success : function(data)
                {
                    $("#resultado").html(data);
                }
            });
            return false;
        }
    });

    function btnLink(e){
        var rowid = $(e).data('id');
        var ndata = $(e).data('hlink');

        var l = $('.ladda-button-dlink'+rowid+'').ladda();
        l.ladda( 'start' );

        /*bootbox.confirm({
            title: "<h3><strong>Atención!</strong></h3>",
            message: "Esta Seguro que deseas  Vincular  el Chasis    "+ndata+" ?",
            size: 'small',
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancelar'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirmar'
                }
            },
            callback: function (result) {
                if(result){
                    $("#btnblink"+rowid).prop('disabled',false);
                    $("#idlinkHomo"+rowid).prop('disabled',false);
                    $("#btnblink"+rowid).val($("#guardarVinculacion").val());
                    $("#FRMLINK"+rowid).submit();
                    return true;
                }else{
                    l.ladda('stop');
                    //  $("#liberarTec").on("click",$(this)).find("#liberarTec");
                }
            }
        });*/
        return false;
    };



</script>

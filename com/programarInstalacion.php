<?php $nav="programarInstalacion" ;?>
<?php include("../includes/head.php") ?>

<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->programarInstalacion=="si"): ?>
<?php 
	$objetoOperaciones=new Operacion;    
    $objetoCanal=new Canales;
    $echo=array();
    $errores=array();
 ?>
<?php 
    if (isset($_POST['verificado'])) {
      Log::registrarLog('Operaiones',$_POST['idoperacion'],$_POST,"Programando fecha de instalación");
//      echo "<pre>";
//      die(var_dump($_POST));
        require_once '../includes/tablaProgramacion.php';
        $objCalendario=new Calendario;
        $operacionesObj=new Operacion;
        $facturacionObj=new DatosFacturacion; 
        $objtablaP=new tablaProgramacion();
        $operacionesObj->idoperacion=$_POST['idoperacion'];
        $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
        $operacionesObj->numeroOrte=$_POST['numeroOrte'];
        if (isset($_POST['observaciong'])) {
            $operacionesObj->observacion=$_POST['observaciong'];
        }
        $operacionesObj->nombreContacto=$_POST['nombreContacto'];
        $operacionesObj->numeroContacto=$_POST['numeroContacto'];

        $facturacionObj->datosfacturacionid=$_POST['datosfacturacionid'];
        
        if ($_POST['lugarInstacion']=="Taller") {
            $operacionesObj->estado=14;
            $operacionesObj->fechaInstalacion=$_POST['fechaIT']." ".$_POST['horaT'];

            $operacionesObj->departamentoi=$_POST['depT'];
            $operacionesObj->provinciai=$_POST['proT'];
            $operacionesObj->distritoi=$_POST['disT'];
            $operacionesObj->direccionInstacion=$_POST['direccionTaller'];
            $operacionesObj->referenciaInstalacion="";

/*            $operacionesObj->departamentoi='15';
            $operacionesObj->provinciai='127';
            $operacionesObj->distritoi='1291';
            $operacionesObj->referenciaInstalacion=$_POST['referenciaT'];
            $operacionesObj->direccionInstacion=$_POST['direccionTaller'];*/
            if (empty($errores)) {                
                if ($operacionesObj->programarFechaDeinstalacon()) {
    if (isset($_POST['fechaIT'])) {
        $objtablaP->fecha=$_POST['fechaIT'];
        $objtablaP->hora=$_POST['horaT'];
        $objtablaP->idtecnico=$_POST['posD'];
        $objtablaP->iddistrito=$_POST['disT'];
        $objtablaP->tth_instalacion="0".$_POST['canthoraprog'].":00";
        $objtablaP->tth_trasalado='00:00';
        $objtablaP->ubicacion='Taller';
        //if (!$dataa=$objtablaP->obtenerTrabajosParaLafechayHoraYespacio()) {
        if (!$objtablaP->registrarTrabajoEntabla()) {
            $errores[]="no se creo en tabla programaciokn ";
        }else{
            $objtablaP->idoperacion=$operacionesObj->idoperacion;
            $objtablaP->insertaridoperacion();       
        }
   // }else{
   //     if($turnoNew=$objtablaP->obtenerTrabajosParaLafechayHoraDisponibleTaller()){
   //         $objtablaP->idturno=$turnoNew;
   //         if (!$objtablaP->registrarTrabajoEntabla()) {
   //             $errores[]="no se creo en tabla programaciokn ";
   //         }else{
   //             $objtablaP->idoperacion=$operacionesObj->idoperacion;
   //             $objtablaP->insertaridoperacion();       
   //         }
   //     }else{
   //         $errores[]="No se registro ninguna cita el horario que selecionaste ya esta ocupado";
   //     }
//}
    }
                }else{
                    $errores[]=" Error en registrar calendario en taller 2";
                 }
            }
        }else
            if ($_POST['lugarInstacion']=="Domicilio Cliente") {
              $operacionesObj->estado=14;             
              $operacionesObj->departamentoi=$_POST['departamentoInstalacionD'];
              $operacionesObj->provinciai=$_POST['provinciaInstalacionD'];
              $operacionesObj->distritoi=$_POST['distritoInstalacionD'];
              $operacionesObj->referenciaInstalacion=$_POST['referenciaInstalacionD'];
              $operacionesObj->coordenadasInstalacion=$_POST['cordenadasD'];

              //taller-link-r

              $operacionesObj->latitud=$_POST['latitud'];
              $operacionesObj->longitud=$_POST['longitud'];


              $operacionesObj->fechaInstalacion=$_POST['fechaID']." ".$_POST['horaD'];
              $operacionesObj->coordenadasInstalacion=$_POST['cordenadasD'];
              $operacionesObj->direccionInstacion=$_POST['direccionInstalacionD'];
                if (empty($errores)) {                
                if ($operacionesObj->programarFechaDeinstalacon()) {
                    $objtablaP->fecha=$_POST['fechaID'];
                    $objtablaP->hora=$_POST['horaD'];
                    $objtablaP->idtecnico=$_POST['posD'];
                    $objtablaP->iddistrito=$_POST['distritoInstalacionD'];
                    $objtablaP->tth_instalacion="0".$_POST['canthoraprog'].":00";
                    $objtablaP->tth_trasalado='00:00';
                    $objtablaP->ubicacion='Domicilio';
                    if (!$objtablaP->registrarTrabajoEntabla()) {
                        $errores[]="no se creo en tabla programaciokn ";
                    }else{
                        $objtablaP->idoperacion=$operacionesObj->idoperacion;
                        $objtablaP->insertaridoperacionDoble();       
                    }
                }else{
                    $errores[]=" Error en registrar calendario en Domicilio 1 ";
                 }
            }
        }else
            if ($_POST['lugarInstacion']=="Concesionario") {              
              $operacionesObj->estado=14;
            $operacionesObj->nombreConcecionarioInstalicion=$_POST['concesionarioInstalacion'];
            $operacionesObj->departamentoi=$_POST['departamentoInstalacionC'];
            $operacionesObj->provinciai=$_POST['provinciaInstalacionC'];
            $operacionesObj->distritoi=$_POST['distritoInstalacionC'];
            $operacionesObj->referenciaInstalacion=$_POST['referenciaInstalacionC'];
              $operacionesObj->fechaInstalacion=$_POST['fechaIC']." ".$_POST['horaC'];;
              $operacionesObj->direccionInstacion=$_POST['direccionInstalacionC'];
            if (empty($errores)) {                
                if ($operacionesObj->programarFechaDeinstalacon()) {
                    $objtablaP->fecha=$_POST['fechaIC'];
                    $objtablaP->hora=$_POST['horaC'];
                    $objtablaP->idtecnico=$_POST['posC'];
                    $objtablaP->iddistrito=$_POST['distritoInstalacionC'];
                    $objtablaP->tth_instalacion="0".$_POST['canthoraprog'].":00";
                    $objtablaP->tth_trasalado='00:00';
                    $objtablaP->ubicacion='Concesionario';
                    if (!$objtablaP->registrarTrabajoEntabla()) {
                        $errores[]="no se creo en tabla programaciokn ";
                    }else{
                        $objtablaP->idoperacion=$operacionesObj->idoperacion;
                        $objtablaP->insertaridoperacionDoble();       
                    }
                }else{
                    $errores[]=" Error en registrar calendario en consecionario 2";
                 }
            }
        }
    }

 ?>
<link rel="stylesheet" href="../assets/css2/style2.css">
<link rel="stylesheet" href="../assets/css2/style3.css">
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<body>
<style>
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
}
</style>
<div class="se-pre-con"></div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>


    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <?php include("../includes/menuIzquierdo.php") ?>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                   <?php include("../includes/navegacionArriba.php") ?>
               </nav>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox " >
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-md-6"><h5>Vehículos con Instalación  pendiente</h5></div>
                        <div class="col-md-6 pull-right"><form action="" method="post" ><button type="submit" class="btn btn-success"><i class="fa fa-refresh"></i> Actualizar</button></form></div>
                    </div>
                </div>

                <div class="ibox-content" >
                    <input type="text" class="form-control input-sm m-b-xs" id="filter"
                    placeholder="Filtrar aquí">
                    <table class="footable table table-hover " data-page-size="6" data-filter=#filter>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Cliente(s)</th>
                                <th>Placa</th>
                                <th data-hide="phone,tablet">Plan</th>
                                <th>Campaña</th>
                                <th>Tipo Registro</th>
                                <th>Estado(s)</th>
                                <th data-hide="all">Chasis</th>
                                <th>Fecha Registro</th>
                                <th>Ejecutivo comercial</th>
                                <th>Acción</th>
                            </tr>
                        </thead>                          
                        <tbody>

<?php
    if ($obMenu->administrarComercial=="si") {
        $oData=$objetoOperaciones->obtenerInstalacionesPendientesTodos();
    }else{
        $oData=$objetoOperaciones->obtenerInstalacionesPendientes();
   } 
?>
                          <?php $n=1; ?>
                          <?php if ($oData): ?>
						<?php foreach ($oData as $var): ?>
                                  <?php $datosTrama = TramaIntegracionTY::obtenerTramaIntegracionPorIdOperacion($var->idoperacion);  ?>
                                  <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>
							<tr  <?= obtenerClass($var->lugarInstalacion) ?>  style="<?= $datosVehiculo->campana=="Toyota"?"border: 2px solid red !important":"" ?>">
								<td><?php echo $n++ ?></td>
								<td><?php $clienDatos=Cliente::obtenerDatosDeclientePorId($var->idcliente); echo $retVal = ($clienDatos->tipoCliente=="Natural") ? $clienDatos->apellidosC." ".$clienDatos->nombre : $clienDatos->razonSocial ; ?></td>
								<td><?php $vhDatos=Vehiculo::obtenerVehiculo($var->idvehiculo);echo $vhDatos->placa ?></td>
								<td><?php echo $vhDatos->plan ?></td>
                                <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosVehiculo->campana</span>":$datosVehiculo->campana) ?></td>
                                <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosTrama->tipo_transaccion</span>":'') ?></td>
								<td ><?=  obtenerEstado($var->estado) ?></td>
                                <td ><?php echo $vhDatos->chasis ?></td>
                                <?php $usuarioa=Usuario::obtenerUsuario($var->idCreador) ?>
                                <td ><?php echo haceMinimo($var->fechaCreacion) ?></td>
                                <td ><?= $usuarioa->usuarioNombre." ".$usuarioa->usuarioApellidoPaterno ?></td>
                                <td><button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#ordenes" data-id="<?php echo $var->idoperacion ?>">Elegir <i class="fa fa-cog"> </i></button></td>
							</tr>
						<?php endforeach ?>
                          <?php endif ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

        </div>
            <div class="modal inmodal" id="ordenes" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content animated bounceIn">
                        <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                            <h4 class="modal-title">Programar Instalaciones </h4>
                        </div>
                        <form  role="form" method="post" id='formPendienes'>
                            <div class="modal-body" style="background: #fff;padding-top: 0">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                <button type="submit" name="verificado" id="agregarVehiculoBttn" class="btn btn-primary" >Guardar Cambios</button>
                                 <div class="row">
                                    <div class="col-lg-12">              
                                      <label id="mostrar" class="text-center text-danger">Limite de taller alcanzado por favor intenta en otro horario</label>
                                    </div>
                                  </div>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>

        <?php include("../includes/footer.php") ?>


        </div>
</div>

      
        
<!-- Mainly scripts -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>

<script src="../assets/build/jquery.datetimepicker.full.js"></script>
<?php include_once('../includes/script.php') ?>


 <script>
$('#formPendienes').validate();
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>


<script>


<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
<script>
    $(document).ready(function(){
        $('.footable').footable();
        $('.footable2').footable();
    });
    $('#ordenes').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        $.ajax({
            type : 'post',
                url : 'modal/programarInstalacionModal', //Here you will fetch records 
                data :  'rowid='+ rowid, //Pass $id
                success : function(data){
                    $('.modal-body').html(data);//Show fetched data from database
                }
            });
    });


      $('#mostrar').hide();
 $( ".nombreContactoo" ).focus(function() {
  esconder();
  });
</script>

</body>

</html>

  <?php else: ?>
    <h2>presione f5</h2>
    <?php  irA("ingresar") ?> 
<?php endif ?>

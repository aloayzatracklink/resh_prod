<?php
require __DIR__ . "/../../vendor/autoload.php";
require_once("../../includes/init.php");
// require_once("../../includes/configuracion.php");
// require_once("../../includes/Recursos.php");
// require_once("../../includes/baseDatos.php");
// require_once("../class/ReclamoClienteClass.php");
// require_once("../class/ReclamoClienteReporteClass.php");
// require_once("../class/DatosTablasClass.php");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\IOFactory;

$reporte = new ReclamoClienteReporteClass();
$datosTablas = new DatosTablasClass();


$destination_path = getcwd().DIRECTORY_SEPARATOR;

$result = 1;
 
//    $target_path = $destination_path . basename( $_FILES['myfile']['name']);
 
// //    if(move_uploaded_file($_FILES['myfile']['tmp_name'], $target_path)) {
// //       $result = 1;
// //    }
//     $msg = "";
//    try {
//         //throw exception if can't move the file
//         if (!move_uploaded_file($_FILES['myfile']['tmp_name'], $target_path)) {
//             throw new Exception('Could not move file');
//         }
//         $result = 1;
//     } catch (Exception $e) {
//         $msg = $e->getMessage();
//     }

    $msgError = "";
    // try {
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($_FILES['myfile']['tmp_name']);
    $spreadsheet = $spreadsheet->getActiveSheet();
    $dataArray =  $spreadsheet->toArray();
    if(count($dataArray)>1){
        // Index 0 es el título del archivo, por ello cargar desde  la segunda línea
        // Validamos la cabecera
        $header = [];
        foreach($dataArray[1] as $h){
            if(trim($h)!="")
                $header[] = $h;
        }

        if(count($header)<14){
            $result = 0;
            $msgError = "No número de columnas no es la correcta.";
        }else{

            for($i=1; $i<count($dataArray); $i++){
                
                if( trim($dataArray[$i][0])!=""){
                    $vreclamo_cliente             = trim($dataArray[$i][0]);
                    $fecha_reclamo                = trim($dataArray[$i][1]);
                    $canal_descripcion            = trim($dataArray[$i][2]);
                    $nivel_atencion_descripcion   = trim($dataArray[$i][3]);
                    $area_detecta_nombre          = trim($dataArray[$i][4]);
                    $ejecutivo_responsable_nombre = trim($dataArray[$i][5]);
                    $area_responsable_nombre      = trim($dataArray[$i][6]);
                    $cliente_nombre               = trim($dataArray[$i][7]);
                    $vehiculo_chasis              = trim($dataArray[$i][8]);
                    $plazo_descripcion            = trim($dataArray[$i][9]);
                    $tmotivo_reclamo              = trim($dataArray[$i][10]);
                    $tinformacion_brindada        = trim($dataArray[$i][11]);
                    $persona_cargo_reclamo_nombre = trim($dataArray[$i][12]);
                    $tsolicitud_reclamo           = trim($dataArray[$i][13]);
                    $tresolucion_reclamo          = trim($dataArray[$i][14]);
                    $estado_descripcion           = trim($dataArray[$i][15]);

                    $reclamoPorNroReclamo = $reporte->obtenerReclamoClientePorNroReclamo($vreclamo_cliente);
                    if(count($reclamoPorNroReclamo)>0){
                        $msgError.="<span style='color:#E0450F;'>ERROR[N.R. $vreclamo_cliente]: El nro de reclamo ya existe.</span><br>";
                    }else{
                        $error = false;
                        ##################
                        # FECHA DE RECLAMO
                        ##################
                        if(trim($fecha_reclamo)!=""){
                            $fecha_reclamo = strtotime($fecha_reclamo);
                            $dtreclamo = date('Y-m-d',$fecha_reclamo);
                        }

                        ##################################################
                        # CANAL DEL RECLAMO
                        ##################################################
                        $buscarDatosCanalPorNombre = $datosTablas->datosReclamoCanalPorNombre($canal_descripcion);
                        if(count($buscarDatosCanalPorNombre)==0){
                            $msgError .=  "<span style='color:#E09D0F;'>CUIDADO[N.R. $vreclamo_cliente]: El canal con nombre : $canal_descripcion no existe.</span<br>"; 
                            #$error = true;
                        }else{
                            $fk_idreclamo_canal = $buscarDatosCanalPorNombre[0]->idreclamo_canal;
                        }

                        ##################################################
                        # NIVEL DEL RECLAMO
                        ##################################################
                        $buscarDatosNivelAtencionPorNombre = $datosTablas->datosReclamoNivelAtencionPorNombre($nivel_atencion_descripcion);
                        if(count($buscarDatosNivelAtencionPorNombre)==0){
                            $msgError .=  "<span style='color:#E09D0F;'>CUIDADO[N.R. $vreclamo_cliente]: El nivel de atención con nombre : $nivel_atencion_descripcion no existe.</span<br>"; 
                            #$error = true;
                        }else{
                            $fk_idreclamo_nivel_atencion = $buscarDatosNivelAtencionPorNombre[0]->idreclamo_nivel_atencion;
                        }
                        
                        ############################################
                        # AREA  QUE DETECTA |  EJECUTIVO RESPONSABLE
                        ############################################
                        $buscarDatosUsuarioPorNonbreYAreaPorNombre1 = $datosTablas->datosUsuarioPorNonbreYAreaPorNombre($ejecutivo_responsable_nombre, $area_detecta_nombre);
                        if(count($buscarDatosUsuarioPorNonbreYAreaPorNombre1)==0){
                            $msgError .=  "<span style='color:#E0450F;'>ERROR[N.R. $vreclamo_cliente]: No se detectó el Area detecta: $area_detecta_nombre y el Ejcutivo responsable $ejecutivo_responsable_nombre.</span><br>"; 
                            $error = true;
                        }else{
                            $fk_idarea_detecta = $buscarDatosUsuarioPorNonbreYAreaPorNombre1[0]->idrol;
                            $fk_idejecutivo_responsable = $buscarDatosUsuarioPorNonbreYAreaPorNombre1[0]->idusuario;
                        }

                        ##################################################
                        # AREA  RESPONSABLE |  PERSONA A CARGO DEL RECLAMO
                        ##################################################
                        $buscarDatosUsuarioPorNonbreYAreaPorNombre2 = $datosTablas->datosUsuarioPorNonbreYAreaPorNombre($persona_cargo_reclamo_nombre, $area_responsable_nombre);
                        if(count($buscarDatosUsuarioPorNonbreYAreaPorNombre2)==0){
                            $msgError .=  "<span style='color:#E0450F;'>ERROR[N.R. $vreclamo_cliente]: No se detectó el Area responsable: $area_responsable_nombre y el Ejcutivo responsable solucionar reclamo $persona_cargo_reclamo_nombre.</span><br>"; 
                            $error = true;
                        }else{
                            $fk_idarea_responsable = $buscarDatosUsuarioPorNonbreYAreaPorNombre2[0]->idrol;
                            $fk_idpersona_cargo_reclamo = $buscarDatosUsuarioPorNonbreYAreaPorNombre2[0]->idusuario;
                        }

                        ##################################################
                        # CLIENTE
                        ##################################################
                        $buscarDatosClientePorNombre = $datosTablas->datosClientesPorNombre($cliente_nombre);
                        if(count($buscarDatosClientePorNombre)==0){
                            $msgError .=  "<span style='color:#E0450F;'>ERROR[N.R. $vreclamo_cliente]: El cliente con nombre : $cliente_nombre no existe.</span><br>"; 
                            $error = true;
                        }else{
                            $fk_idcliente = $buscarDatosClientePorNombre[0]->idcliente;
                        }

                        ##################################################
                        # VEHICULO POR PLACA
                        ##################################################
                        $buscarDatosVehiculoPorChasis = $datosTablas->datosVehiculosPorPlaca($vehiculo_chasis);
                        if(count($buscarDatosVehiculoPorChasis)==0){
                            $msgError .=  "<span style='color:#E0450F;'>ERROR[N.R. $vreclamo_cliente]: El vehículo con placa : $vehiculo_chasis no existe.</span><br>"; 
                            $error = true;
                        }else{
                            $fk_idvehiculo = $buscarDatosVehiculoPorChasis[0]->idvehiculo;
                        }

                        ##################################################
                        # PLAZO DEL RECLAMO
                        ##################################################
                        $buscarDatosPlazoPorNombre = $datosTablas->datosReclamoPlazoPorNombre($plazo_descripcion);
                        if(count($buscarDatosPlazoPorNombre)==0){
                            $msgError .=  "<span style='color:#E09D0F;'>CUIDADO[N.R. $vreclamo_cliente]: El plazo con nombre : $plazo_descripcion no existe.</span><br>"; 
                            #$error = true;
                        }else{
                            $fk_idreclamo_plazo = $buscarDatosPlazoPorNombre[0]->idreclamo_plazo;
                        }

                        ##################################################
                        # ESTADO DEL RECLAMO
                        ##################################################
                        $buscarDatosEstadoPorNombre = $datosTablas->datosReclamoEstadoPorNombre($estado_descripcion);
                        if(count($buscarDatosEstadoPorNombre)==0){
                            $msgError .=  "<span style='color:#E09D0F;'>CUIDADO[N.R. $vreclamo_cliente]: El estado con nombre : $estado_descripcion no existe.</span<br>"; 
                            #$error = true;
                        }else{
                            $fk_idreclamo_estado = $buscarDatosEstadoPorNombre[0]->idreclamo_estado;
                        }

                        if($error){

                        }else{
                            // Guardamos el nuevo reclamo
                            $reclamo                             = new ReclamoClienteClass();
                            $reclamo->vreclamo_cliente           = $vreclamo_cliente;
                            $reclamo->fk_idarea_detecta          = $fk_idarea_detecta;
                            $reclamo->fk_idarea_responsable      = $fk_idarea_responsable;
                            $reclamo->fk_idejecutivo_responsable = $fk_idejecutivo_responsable;
                            $reclamo->fk_idcliente               = $fk_idcliente;
                            $reclamo->fk_idvehiculo              = $fk_idvehiculo;
                            $reclamo->fk_idreclamo_plazo         = $fk_idreclamo_plazo;
                            $reclamo->tmotivo_reclamo            = $tmotivo_reclamo;
                            $reclamo->tinformacion_brindada      = $tinformacion_brindada;
                            $reclamo->fk_idpersona_cargo_reclamo = $fk_idpersona_cargo_reclamo;
                            $reclamo->tsolicitud_reclamo         = $tsolicitud_reclamo;
                            $reclamo->tresolucion_reclamo        = $tresolucion_reclamo;
                            $reclamo->fk_idreclamo_estado        = $fk_idreclamo_estado;
                            $reclamo->fk_idreclamo_canal         = $fk_idreclamo_canal;
                            $reclamo->fk_idreclamo_nivel_atencion= $fk_idreclamo_nivel_atencion;
                            $reclamo->registrarReclamoCliente();
                            
                        }
                        
                    }

                }

            }

            echo $msgError;
            $result = 1;
        }
    }else{
        $result = 0;
        $msgError = "No hay registros para importar";
    }
    // } catch (Exception $e) {
    //     $result = 0;
    //     $msgError = $e->getMessage();
    // }
//    sleep(1);

   if($result==1){
    if(trim($msgError)!=""){
        $msgError = "<span style='color:red; font-size:14px;'><strong>Pero con algunas obervaciones:</strong><span><br>".$msgError;

    }
   }
?>
<script>
    var msgError = `<?=$msgError;?>`;
    window.parent.window.stopUpload(<?php echo $result; ?>, msgError);
</script>


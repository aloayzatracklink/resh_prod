
<?php
include("../../includes/init.php") ;
 if (isset($_POST['Descargar'])) {
	$convenio=limpiar($_POST['convenio']);
	$fechaConsulta=$_POST['fecha'];	
    $desde=trim(current(explode('/', $fechaConsulta)));
    $tmp=explode('/', $fechaConsulta);
    $hasta=end($tmp);	
	$desde=date("YmdHis",strtotime($desde));	
	$hasta=date("Ymd",strtotime($hasta));	
	$hasta=$hasta."235959";

header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=$fechaConsulta.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false); 

 ?>

 <?php if ($convenio=='1'){ ?>
 	
<head profile="http://dublincore.org/documents/dcmi-terms/"><meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8"/><title xml:lang="en-US">Comisiones</title><meta name="DCTERMS.title" content="" xml:lang="en-US"/><meta name="DCTERMS.language" content="en-US" scheme="DCTERMS.RFC4646"/><meta name="DCTERMS.source" content="http://xml.openoffice.org/odf2xhtml"/><meta name="DCTERMS.creator" content="Cinthya Ruiz"/><meta name="DCTERMS.issued" content="2017-05-02T16:52:42" scheme="DCTERMS.W3CDTF"/><meta name="DCTERMS.modified" content="2017-08-23T10:31:51.549009866" scheme="DCTERMS.W3CDTF"/><meta name="DCTERMS.provenance" content="" xml:lang="en-US"/><meta name="DCTERMS.subject" content="," xml:lang="en-US"/><link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" hreflang="en"/><style type="text/css">
	@page {  }
	table { border-collapse:collapse; border-spacing:0; empty-cells:show }
	td, th { vertical-align:top; font-size:16pt;}
	h1, h2, h3, h4, h5, h6 { clear:both }
	ol, ul { margin:0; padding:0;}
	li { list-style: none; margin:0; padding:0;}
	<!-- "li span.odfLiEnd" - IE 7 issue-->
	li span. { clear: both; line-height:0; width:0; height:0; margin:0; padding:0; }
	span.footnodeNumber { padding-right:1em; }
	span.annotation_style_by_filter { font-size:95%; font-family:Arial; background-color:#fff000;  margin:0; border:0; padding:0;  }
	* { margin:0;}
	.gr1 { font-size:12pt; writing-mode:page; padding-top:3.54pt; padding-bottom:3.54pt; padding-left:7.09pt; padding-right:7.09pt; }
	.P1 { text-align:left ! important; font-size:18pt; }
	.ta1 { writing-mode:lr-tb; }
	.ta2 { writing-mode:lr-tb; }
	.ce1 { color:#000000; font-size:11pt; font-family:Calibri; vertical-align:bottom; }
	.ce10 { color:#000000; font-size:10pt; font-family:Arial; vertical-align:bottom; background-color:#ffffff; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:left ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce11 { color:#000000; font-size:10pt; font-family:Tahoma; vertical-align:middle; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; background-color:#ffffff; border-left-width:0.0261cm; border-left-style:solid; border-left-color:#000000; border-right-width:0.0261cm; border-right-style:solid; border-right-color:#000000; border-top-style:none; text-align:left ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce12 { color:#000000; font-size:9pt; font-family:Calibri; vertical-align:middle; background-color:#ffffff; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce13 { color:#000000; font-size:9pt; font-family:Tahoma; vertical-align:middle; background-color:#4f81bd; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce14 { color:#000000; font-size:10pt; font-family:Tahoma; vertical-align:middle; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; background-color:#ffffff; border-left-width:0.0261cm; border-left-style:solid; border-left-color:#000000; border-right-width:0.0261cm; border-right-style:solid; border-right-color:#000000; border-top-style:none; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce15 { color:#000000; font-size:10pt; font-family:Tahoma; vertical-align:middle; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; background-color:#ffffff; border-left-style:none; border-right-width:0.0261cm; border-right-style:solid; border-right-color:#000000; border-top-width:0.0261cm; border-top-style:solid; border-top-color:#000000; text-align:left ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce16 { color:#000000; font-size:10pt; font-family:Tahoma; vertical-align:middle; background-color:#ffffff; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:left ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce17 { color:#000000; font-size:11pt; font-family:Calibri; vertical-align:bottom; background-color:#ffff00; border-width:0.0621cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce18 { color:#000000; font-size:9pt; font-family:Tahoma; vertical-align:middle; background-color:#ffffff; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce19 { color:#000000; font-size:11pt; font-family:Calibri; vertical-align:bottom; background-color:#ffff00; border-width:0.0621cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce2 { color:#000000; font-size:9pt; font-family:Calibri; vertical-align:middle; background-color:#ffffff; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	
	


	.ce3 { color:#000000; font-size:9pt; font-family:Calibri; vertical-align:middle; background-color:#ffffff; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	
	
	
	.ce4 { color:#1f497d; font-size:18pt; font-family:Calibri; vertical-align:middle; background-color:#ffffff; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce5 { color:#000000; font-size:9pt; font-family:Tahoma; vertical-align:middle; background-color:#4f81bd; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce6 { color:#000000; font-size:9pt; font-family:Tahoma; vertical-align:middle; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; background-color:#ffffff; border-left-width:0.0261cm; border-left-style:solid; border-left-color:#000000; border-right-width:0.0261cm; border-right-style:solid; border-right-color:#000000; border-top-style:none; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce7 { color:#000000; font-size:10pt; font-family:Tahoma; vertical-align:top; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; background-color:#ffffff; border-left-width:0.0261cm; border-left-style:solid; border-left-color:#000000; border-right-width:0.0261cm; border-right-style:solid; border-right-color:#000000; border-top-style:none; text-align:left ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce8 { color:#000000; font-size:9pt; font-family:Calibri; vertical-align:middle; background-color:#ffffff; text-align:left ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce9 { color:#000000; font-size:10pt; font-family:Tahoma; vertical-align:middle; background-color:#ffffff; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:left ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.co1 { width:15.25pt; }
	.co2 { width:33.76pt; }
	.co4 { width:346.25pt; }
	.co6 { width:255.26pt; }
	.co7 { width:80.19pt; }
	.co8 { width:131.1pt; }
	.ro2 { height:15.76pt; }
	.ro3 { height:29.25pt; }
	.ro4 { height:17.26pt; }
	.ta_extref  { }
	</style>
	</head>
	<body dir="ltr" style="max-width:612pt;margin-top:54pt; margin-bottom:54pt; margin-left:50.4pt; margin-right:50.4pt; ">
	<table border="0" cellspacing="0" cellpadding="0" class="ta1">
	<colgroup><col width="24"/><col width="52"/><col width="131"/><col width="534"/><col width="170"/><col width="394"/><col width="124"/><col width="202"/><col width="278"/><col width="259"/><col width="124"/></colgroup>

	<tr class="ro2"><td style="text-align:left;width:15.25pt; " class="ce2"> </td><td style="text-align:left;width:33.76pt; " class="ce2" colspan="9"> </td>
		</tr>
		<tr class="ro2"><td style="text-align:left;width:15.25pt; " class="ce2"> </td><td style="text-align:left;width:33.76pt; " class="ce2" colspan="9"> </td>
		</tr>

	<tr class="ro2">
		<td style="text-align:left;width:15.25pt; " class="ce2"> </td>
		<td style="text-align:left;width:33.76pt; " class="ce4" colspan="4"><p>DEL <?= formatoMesSinHora($desde) ?> AL <?= formatoMesSinHora($hasta) ?></p></td><td style="text-align:left;width:85.24pt; " class="ce2"> </td>
	
		<td style="text-align:left;width:131.1pt; " class="ce2"> </td>
		<td style="text-align:left;width:180.11pt; " class="ce2"> </td>
		<td style="text-align:left;width:168.04pt; " class="ce2"> </td><td style="text-align:left;width:80.19pt; " class="ce2"> </td>
	</tr>

		<tr class="ro2"><td style="text-align:left;width:15.25pt; " class="ce2"> </td><td style="text-align:left;width:33.76pt; " class="ce2" colspan="9"> </td>
		</tr>
	

	<tr class="ro3">
		<td style="text-align:left;width:15.25pt; " class="ce3"> </td>
		<td style="text-align:left;width:33.76pt; " class="ce5"><p>N°</p></td>
		<td style="text-align:left;width:85.24pt; " class="ce5"><p>FECHA</p></td>
		<td style="text-align:left;width:380.25pt; " class="ce5"><p>CLIENTE</p></td>
		<td style="text-align:left;width:300.1pt; " class="ce5"><p>BOLETA</p></td>
		<td style="text-align:left;width:300.26pt; " class="ce5"><p>PLAN</p></td>
		<td style="text-align:left;width:80.19pt; " class="ce13"><p>SIN IGV</p></td>
		<td style="text-align:left;width:131.1pt; " class="ce5"><p>EJECUTIVO</p></td>
		<td style="text-align:left;width:180.11pt; " class="ce5"><p>TIENDA</p></td>
		<td style="text-align:left;width:168.04pt; " class="ce5"><p>VENDEDOR</p></td>
		<td style="text-align:left;width:80.19pt; " class="ce5"><p>COMISIÓN</p></td>
	</tr>
	<?php 
	$suma=0;
	$n=1 ;
	?>
<?php 
	$datos=Comisiones::obtenerConvenios($convenio,$desde,$hasta);
 ?>
<?php if ($datos): ?>
<?php foreach ($datos as $var): ?>	
  <?php $usuario=Usuario::obtenerUsuario($var->idCreador)  ?>
    <?php //$convenio=Canales::obtenerDatosDeConvenioPorId($var->idconvenio) ?>
    <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>	
    <?php $datosCanal=Canales::obtenerDatosCanalPorId($var->idcanal) ?>
	<tr class="ro4">
		<td style="text-align:left;width:15.25pt; " class="ce3"> </td>
		<td style="text-align:right; width:33.76pt; " class="ce6"><p><?= $n++ ?></p></td>
		<td style="text-align:right; width:85.24pt; " class="ce7"><p><?= (!empty($var->fechaFacturador))?formatoSinHoraMesPrimero($var->fechaFacturador):obtenerEstado($var->estadoFacturacion) ?></p></td>
		<td style="text-align:left;width:380.25pt; " class="ce9"><p><?= ($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->nombre)." ".strtoupper($clienteObj->apellidosC) : strtoupper($clienteObj->razonSocial) ; ?></p></td>
		<td style="text-align:left;width:300.1pt; " class="ce10"><p><?= (!empty($var->numeroFacturacion))?$var->numeroFacturacion:obtenerEstado($var->estadoFacturacion)  ?></p></td>
		<td style="text-align:left;width:300.26pt; " class="ce11"><p><?= $plan=limpiarPALABRAGPSSMART(strtoupper($datosVehiculo->plan)) ?> <?=  $var->tiempo ?> AÑO(S)</p></td><td style="text-align:right; width:80.19pt; " class="ce14"><p> $ <?= round($var->montoTotal/1.18,2) ?> </p></td>
		<td style="text-align:left;width:131.1pt; " class="ce9"><p><?= strtoupper($usuario->usuarioNombre)." ".strtoupper($usuario->usuarioApellidoPaterno) ?></p></td>
		<td style="text-align:left;width:180.11pt; " class="ce15"><p><?= $datosCanal->tiendaCanal ?></p></td>
		<td style="text-align:left;width:168.04pt; " class="ce16"><p><?= $datosCanal->ejecutivoCanal ?></p></td>
		<?php $datosCom=Comisiones::calcularComisionConvenio($plan,$var->tiempo,1); ?>
		<td style="text-align:right; width:80.19pt; " class="ce18"><p>$ <?php $comisoncal=($datosCom) ? $var->montoTotal-$datosCom->comision : "0";  echo number_format($comisoncal, 2, '.', ',') ?></p></td>
		<?php $suma+=$comisoncal ?>
	</tr>
	<?php endforeach ?>	
<?php endif ?>
<script>
</script>

	<tr class="ro4">
		<td style="text-align:left;width:15.25pt; " class="ce3"> </td>
		<td style="text-align:left;width:33.76pt; " class="ce2"> </td>
		<td style="text-align:left;width:85.24pt; " class="ce2"> </td>
		<td style="text-align:left;width:380.25pt; " class="ce8"> </td>
		<td style="text-align:left;width:300.1pt; " class="ce2"> </td>
		<td style="text-align:left;width:300.26pt; " class="ce2"> </td>
		<td style="text-align:left;width:80.19pt; " class="ce12"> </td>
		<td style="text-align:left;width:131.1pt; " class="ce2"> </td>
		<td style="text-align:left;width:180.11pt; " class="ce2"></td>
		<td style="text-align:left;width:168.04pt; " class="ce17"><p>TOTAL </p></td>
		<td style="text-align:right; width:80.19pt; " class="ce19"><p>$ <?= number_format($suma, 2, '.', ',') ?> </p></td>
	</tr>

	</table>
	</body>
 <?php } ?>
<?php if ($convenio=='2'){ ?>
 	<head profile="http://dublincore.org/documents/dcmi-terms/">
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8"/><title xml:lang="en-US">BAN BAIF</title>
<meta name="DCTERMS.title" content="" xml:lang="en-US"/>
<meta name="DCTERMS.language" content="en-US" scheme="DCTERMS.RFC4646"/>
<meta name="DCTERMS.source" content="http://xml.openoffice.org/odf2xhtml"/><meta name="DCTERMS.creator" content="Cinthya Ruiz"/>
<meta name="DCTERMS.issued" content="2017-05-02T16:52:42" scheme="DCTERMS.W3CDTF"/><meta name="DCTERMS.modified" content="2017-08-23T11:07:13.759665668" scheme="DCTERMS.W3CDTF"/><meta name="DCTERMS.provenance" content="" xml:lang="en-US"/>
<meta name="DCTERMS.subject" content="," xml:lang="en-US"/>


<style type="text/css">
	@page {  }
	table { border-collapse:collapse; border-spacing:0; empty-cells:show }
	td, th { vertical-align:top; font-size:10pt;}
	h1, h2, h3, h4, h5, h6 { clear:both }
	ol, ul { margin:0; padding:0;}
	li { list-style: none; margin:0; padding:0;}
	<!-- "li span.odfLiEnd" - IE 7 issue-->
	li span. { clear: both; line-height:0; width:0; height:0; margin:0; padding:0; }
	span.footnodeNumber { padding-right:1em; }
	span.annotation_style_by_filter { font-size:95%; font-family:Arial; background-color:#fff000;  margin:0; border:0; padding:0;  }
	* { margin:0;}
	.ta1 { writing-mode:lr-tb; }
	.ce1 { color:#000000; font-size:11pt; font-family:Calibri; vertical-align:bottom; }
	.ce10 { color:#000000; font-size:11pt; font-family:Calibri; vertical-align:bottom; background-color:#ffffff; border-style:none; }
	.ce11 { color:#000000; font-size:15pt; font-family:Calibri; vertical-align:bottom; background-color:#ffffff; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:underline; font-weight:bold; }
	.ce12 { color:#000000; font-size:9pt; font-family:Calibri Light; vertical-align:bottom; border-bottom-width:0.0261cm; border-bottom-style:fine-dashed; border-bottom-color:#4f81bd; background-color:transparent; border-left-width:0.0621cm; border-left-style:dash-dot; border-left-color:#4f81bd; border-right-width:0.0621cm; border-right-style:dash-dot; border-right-color:#4f81bd; border-top-width:0.0261cm; border-top-style:fine-dashed; border-top-color:#4f81bd; text-align:center ! important; margin-left:0pt; writing-mode:lr-tb; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce13 { color:#ffffff; font-size:9pt; font-family:Calibri Light; vertical-align:bottom; border-bottom-width:0.0261cm; border-bottom-style:fine-dashed; border-bottom-color:#4f81bd; background-color:#1f497d; border-left-width:0.0621cm; border-left-style:dash-dot; border-left-color:#4f81bd; border-right-width:0.0621cm; border-right-style:dash-dot; border-right-color:#4f81bd; border-top-width:0.0261cm; border-top-style:fine-dashed; border-top-color:#4f81bd; text-align:center ! important; margin-left:0pt; writing-mode:lr-tb; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce14 { color:#ffffff; font-size:9pt; font-family:Calibri Light; vertical-align:bottom; border-bottom-width:0.0261cm; border-bottom-style:fine-dashed; border-bottom-color:#4f81bd; background-color:#1f497d; border-left-width:0.0621cm; border-left-style:dash-dot; border-left-color:#4f81bd; border-right-width:0.0621cm; border-right-style:dash-dot; border-right-color:#4f81bd; border-top-width:0.0261cm; border-top-style:fine-dashed; border-top-color:#4f81bd; text-align:center ! important; margin-left:0pt; writing-mode:lr-tb; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce15 { color:#000000; font-size:11pt; font-family:Calibri; vertical-align:middle; background-color:#ffffff; text-align:center ! important; margin-left:0pt; writing-mode:page; }
	.ce2 { color:#000000; font-size:11pt; font-family:Calibri; vertical-align:bottom; background-color:#ffffff; }
	.ce3 { color:#000000; font-size:11pt; font-family:Calibri; vertical-align:middle; background-color:#ffffff; border-style:none; text-align:center ! important; margin-left:0pt; writing-mode:page; }
	.ce4 { color:#000000; font-size:11pt; font-family:Calibri; vertical-align:bottom; background-color:#ffffff; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce5 { color:#ffffff; font-size:11pt; font-family:Calibri; vertical-align:middle; background-color:#1f497d; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce6 { color:#000000; font-size:9pt; font-family:Calibri Light; vertical-align:bottom; border-bottom-width:0.0261cm; border-bottom-style:fine-dashed; border-bottom-color:#4f81bd; background-color:transparent; border-left-width:0.0621cm; border-left-style:dash-dot; border-left-color:#4f81bd; border-right-width:0.0621cm; border-right-style:dash-dot; border-right-color:#4f81bd; border-top-width:0.0261cm; border-top-style:fine-dashed; border-top-color:#4f81bd; text-align:center ! important; margin-left:0pt; writing-mode:lr-tb; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce7 { color:#ffffff; font-size:11pt; font-family:Calibri; vertical-align:middle; background-color:#1f497d; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce8 { color:#000000; font-size:9pt; font-family:Calibri Light; vertical-align:bottom; border-bottom-width:0.0261cm; border-bottom-style:fine-dashed; border-bottom-color:#4f81bd; background-color:transparent; border-left-width:0.0621cm; border-left-style:dash-dot; border-left-color:#4f81bd; border-right-width:0.0621cm; border-right-style:dash-dot; border-right-color:#4f81bd; border-top-width:0.0261cm; border-top-style:fine-dashed; border-top-color:#4f81bd; text-align:center ! important; margin-left:0pt; writing-mode:lr-tb; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce9 { color:#000000; font-size:15pt; font-family:Calibri; vertical-align:bottom; background-color:#ffffff; border-style:none; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:underline; font-weight:bold; }
	.co1 { width:40.11pt; }
	.co10 { width:96.09pt; }
	.co2 { width:94.2pt; }
	.co3 { width:97.99pt; }
	.co4 { width:327.15pt; }
	.co5 { width:96.75pt; }
	.co6 { width:83.99pt; }
	.co7 { width:83.94pt; }
	.co8 { width:159.11pt; }
	.co9 { width:80.19pt; }
	.ro1 { height:14.51pt; }
	.ro2 { height:19.5pt; }
	.ro3 { height:43.51pt; }
	.ro4 { height:12.81pt; }
	.ta_extref  { }
	</style>
	</head>
	<body dir="ltr" style="max-width:612pt;margin-top:54pt; margin-bottom:54pt; margin-left:50.4pt; margin-right:50.4pt; ">
	<table border="0" cellspacing="0" cellpadding="0" class="ta1">
	<colgroup><col width="62"/><col width="145"/><col width="151"/><col width="504"/><col width="149"/><col width="129"/><col width="316"/><col width="245"/><col width="124"/><col width="148"/><col width="124"/></colgroup>

	<tr class="ro1"><td style="text-align:left;width:40.11pt; " class="ce2"> </td><td style="text-align:left;width:94.2pt; " class="ce2"> </td><td style="text-align:left;width:97.99pt; " class="ce2"> </td><td style="text-align:left;width:327.15pt; " class="ce2"> </td><td style="text-align:left;width:96.75pt; " class="ce2"> </td><td style="text-align:left;width:83.99pt; " class="ce2"> </td><td style="text-align:left;width:83.94pt; " class="ce2"> </td><td style="text-align:left;width:159.11pt; " class="ce2"> </td><td style="text-align:left;width:80.19pt; " class="ce2"> </td><td style="text-align:left;width:96.09pt; " class="ce2"> </td><td style="text-align:left;width:80.19pt; " class="ce2"> </td></tr>



	<tr class="ro2">
		<td style="text-align:left;width:40.11pt; " class="ce2"> </td>
		<td style="text-align:left;width:94.2pt; " class="ce2"> </td>
		<td style="text-align:left;width:97.99pt; " class="ce2"> </td>
		<td colspan="6" style="text-align:left;width:327.15pt; " class="ce9"><p>COMISIONES BANBIF – <?= formatoMesSinHora($desde) ?> A <?= formatoMesSinHora($hasta) ?></p></td>
		<td style="text-align:left;width:96.09pt; " class="ce2"> </td>
		<td style="text-align:left;width:80.19pt; " class="ce2"> </td>
	</tr>

	<tr class="ro1">
		<td style="text-align:left;width:40.11pt; " class="ce2"> </td>
		<td style="text-align:left;width:94.2pt; " class="ce2"> </td>
		<td style="text-align:left;width:97.99pt; " class="ce2"> </td>
		<td style="text-align:left;width:327.15pt; " class="ce2"> </td>
		<td style="text-align:left;width:96.75pt; " class="ce2"> </td>
		<td style="text-align:left;width:83.99pt; " class="ce2"> </td>
		<td style="text-align:left;width:83.94pt; " class="ce2"> </td>
		<td style="text-align:left;width:159.11pt; " class="ce2"> </td>
		<td style="text-align:left;width:80.19pt; " class="ce2"> </td>
		<td style="text-align:left;width:96.09pt; " class="ce2"> </td>
		<td style="text-align:left;width:80.19pt; " class="ce2"> </td>
	</tr>

	<tr class="ro1">
		<td style="text-align:left;width:40.11pt; " class="ce2"> </td>
		<td style="text-align:left;width:94.2pt; " class="ce4"><p>RESPONSABLE :</p></td>
		<td style="text-align:left;width:97.99pt; " class="ce2"><p>DIEGO CASTRO</p></td>
		<td style="text-align:left;width:327.15pt; " class="ce2"> </td>
		<td style="text-align:left;width:96.75pt; " class="ce2"> </td>
		<td style="text-align:left;width:83.99pt; " class="ce2"> </td>
		<td style="text-align:left;width:83.94pt; " class="ce2"> </td>
		<td style="text-align:left;width:159.11pt; " class="ce2"> </td>
		<td style="text-align:left;width:80.19pt; " class="ce2"> </td>
		<td style="text-align:left;width:96.09pt; " class="ce2"> </td>
		<td style="text-align:left;width:80.19pt; " class="ce2"> </td>
	</tr>

	<tr class="ro1">
		<td style="text-align:left;width:40.11pt; " class="ce2"> </td>
		<td style="text-align:left;width:94.2pt; " class="ce4"><p>DESDE :</p></td>
		<td style="text-align:left;width:97.99pt; " class="ce2"><p><?= formatoMesSinHora($desde) ?></p></td>
		<td style="text-align:left;width:327.15pt; " class="ce2"> </td>
		<td style="text-align:left;width:96.75pt; " class="ce2"> </td>
		<td style="text-align:left;width:83.99pt; " class="ce2"> </td>
		<td style="text-align:left;width:83.94pt; " class="ce2"> </td>
		<td style="text-align:left;width:159.11pt; " class="ce2"> </td>
		<td style="text-align:left;width:80.19pt; " class="ce2"> </td>
		<td style="text-align:left;width:96.09pt; " class="ce2"> </td>
		<td style="text-align:left;width:80.19pt; " class="ce2"> </td>
	</tr>

	<tr class="ro1">
		<td style="text-align:left;width:40.11pt; " class="ce2"> </td>
		<td style="text-align:left;width:94.2pt; " class="ce4"><p>HASTA :</p></td>
		<td style="text-align:left;width:97.99pt; " class="ce2"><p><?= formatoMesSinHora($hasta) ?></p></td>
		<td style="text-align:left;width:327.15pt; " class="ce2"> </td>
		<td style="text-align:left;width:96.75pt; " class="ce2"> </td>
		<td style="text-align:left;width:83.99pt; " class="ce2"> </td>
		<td style="text-align:left;width:83.94pt; " class="ce2"> </td>
		<td style="text-align:left;width:159.11pt; " class="ce2"> </td>
		<td style="text-align:left;width:80.19pt; " class="ce2"> </td>
		<td style="text-align:left;width:96.09pt; " class="ce2"> </td>
		<td style="text-align:left;width:80.19pt; " class="ce2"> </td>
	</tr>

	<tr class="ro1">
		<td style="text-align:left;width:40.11pt; " class="ce2"> </td>
		<td style="text-align:left;width:94.2pt; " class="ce2"> </td>
		<td style="text-align:left;width:97.99pt; " class="ce2"> </td>
		<td style="text-align:left;width:327.15pt; " class="ce2"> </td>
		<td style="text-align:left;width:96.75pt; " class="ce2"> </td>
		<td style="text-align:left;width:83.99pt; " class="ce2"> </td>
		<td style="text-align:left;width:83.94pt; " class="ce2"> </td>
		<td style="text-align:left;width:159.11pt; " class="ce2"> </td>
		<td style="text-align:left;width:80.19pt; " class="ce2"> </td>
		<td style="text-align:left;width:96.09pt; " class="ce2"> </td>
		<td style="text-align:left;width:80.19pt; " class="ce2"> </td>
	</tr>
	<tr class="ro3">
		<td style="text-align:left;width:40.11pt; " class="ce3"> </td>
		<td style="text-align:left;width:94.2pt; " class="ce5"><p>CANTIDAD</p></td>
		<td style="text-align:left;width:97.99pt; " class="ce7"><p>FECHA DE FACTURACIÓN</p></td>
		<td style="text-align:left;width:327.15pt; " class="ce5"><p>CLIENTE</p></td>
		<td style="text-align:left;width:96.75pt; " class="ce5"><p>FACTURA</p></td>
		<td style="text-align:left;width:83.99pt; " class="ce7"><p>PRECIO TOTAL CON IGV</p></td>
		<td style="text-align:left;width:83.94pt; " class="ce7"><p>PRECIO TOTAL SIN IGV</p></td>
		<td style="text-align:left;width:159.11pt; " class="ce7"><p>SISTEMA</p></td>
		<td style="text-align:left;width:80.19pt; " class="ce5"><p>TIEMPO</p></td>
		<td style="text-align:left;width:96.09pt; " class="ce5"><p>COMISIÓN $</p></td>
		<td style="text-align:left;width:80.19pt; " class="ce15"> </td>
	</tr>
		<?php 
		 $datos=Comisiones::obtenerConvenios($convenio,$desde,$hasta);
		 $n=1;
		 $suma=0;
		 ?>
	<?php if ($datos): ?>
		<?php foreach ($datos as $var): ?>
		<?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>	
    <?php $datosCanal=Canales::obtenerDatosCanalPorId($var->idcanal) ?>				
	<tr class="ro1">
		<td style="text-align:left;width:40.11pt; " class="ce2"> </td>
		<td style="text-align:right; width:94.2pt; " class="ce6"><p><?= $n++ ?></p></td>
		<td style="text-align:right; width:97.99pt; " class="ce8"><p><?= (!empty($var->fechaFacturador))?formatoSinHoraMesPrimero($var->fechaFacturador):obtenerEstado($var->estadoFacturacion) ?></p></td>
		<td style="text-align:left;width:327.15pt; " class="ce6"><p><?= ($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->nombre)." ".strtoupper($clienteObj->apellidosC) : strtoupper($clienteObj->razonSocial) ; ?></p></td>
		<td style="text-align:left;width:96.75pt; " class="ce6"><p><?= (!empty($var->numeroFacturacion))?$var->numeroFacturacion:obtenerEstado($var->estadoFacturacion)  ?></p></td>
		<td style="text-align:right; width:83.99pt; " class="ce12"><p><?= number_format($var->montoTotal, 2, '.', ',') ?></p></td>
		<td style="text-align:right; width:83.94pt; " class="ce12"><p> <?= round($var->montoTotal/1.18,2) ?></p></td>
		<td style="text-align:left;width:159.11pt; " class="ce6"><p><?= $plan=limpiarPALABRAGPSSMART(strtoupper($datosVehiculo->plan)) ?></p></td>
		<td style="text-align:left;width:80.19pt; " class="ce6"><p> <?=  $var->tiempo*12 ?>  MESES</p></td>
		<?php $datosCom=Comisiones::calcularComisionConvenio($plan,$var->tiempo,2); ?>
		<td style="text-align:right; width:80.19pt; " class="ce18"><p>$ <?= $comisoncal=($datosCom) ? calcularComision($datosCom->comision,$datosCom->precioConvenio,$var->montoTotal) : "0"  ?></p></td>
		<!-- <td style="text-align:right; width:96.09pt; " class="ce12"><p>50.00</p></td> -->
		<td style="text-align:left;width:80.19pt; " class="ce2"> </td>
		<?php $suma+=$comisoncal ?>

	</tr>
		<?php endforeach ?>		
	<?php endif ?>

	<tr class="ro1">
		<td style="text-align:left;width:40.11pt; " class="ce2"> </td>
		<td style="text-align:left;width:94.2pt; " class="ce2"> </td>
		<td style="text-align:left;width:97.99pt; " class="ce2"> </td>
		<td style="text-align:left;width:327.15pt; " class="ce10"> </td>
		<td style="text-align:left;width:96.75pt; " class="ce10"> </td>
		<td style="text-align:left;width:83.99pt; " class="ce2"> </td>
		<td style="text-align:left;width:83.94pt; " class="ce2"> </td>
		<td style="text-align:left;width:159.11pt; " class="ce2"> </td>
		<td style="text-align:left;width:80.19pt; " class="ce13"><p>TOTAL $</p></td>
		<td style="text-align:right; width:96.09pt; " class="ce14"><p>$ <?= number_format($suma, 2, '.', ',') ?></p></td>
		<td style="text-align:left;width:80.19pt; " class="ce2"> </td>
	</tr>	
	</body>
</html> 	
 <?php } ?> 
 <?php if ($convenio=='3'){ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN" "http://www.w3.org/Math/DTD/mathml2/xhtml-math11-f.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><!--This file was converted to xhtml by LibreOffice - see http://cgit.freedesktop.org/libreoffice/core/tree/filter/source/xslt for the code.--><head profile="http://dublincore.org/documents/dcmi-terms/"><meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8"/><title xml:lang="en-US">- no title specified</title><meta name="DCTERMS.title" content="" xml:lang="en-US"/><meta name="DCTERMS.language" content="en-US" scheme="DCTERMS.RFC4646"/><meta name="DCTERMS.source" content="http://xml.openoffice.org/odf2xhtml"/><meta name="DCTERMS.creator" content="Eva Araujo"/><meta name="DCTERMS.issued" content="2017-09-04T17:36:22" scheme="DCTERMS.W3CDTF"/><meta name="DCTERMS.modified" content="2017-09-07T11:14:10.151526717" scheme="DCTERMS.W3CDTF"/><meta name="DCTERMS.provenance" content="" xml:lang="en-US"/><meta name="DCTERMS.subject" content="," xml:lang="en-US"/><link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" hreflang="en"/><link rel="schema.DCTERMS" href="http://purl.org/dc/terms/" hreflang="en"/><link rel="schema.DCTYPE" href="http://purl.org/dc/dcmitype/" hreflang="en"/><link rel="schema.DCAM" href="http://purl.org/dc/dcam/" hreflang="en"/><style type="text/css">
	@page {  }
	table { border-collapse:collapse; border-spacing:0; empty-cells:show }
	td, th { vertical-align:top; font-size:10pt;}
	h1, h2, h3, h4, h5, h6 { clear:both }
	ol, ul { margin:0; padding:0;}
	li { list-style: none; margin:0; padding:0;}
	<!-- "li span.odfLiEnd" - IE 7 issue-->
	li span. { clear: both; line-height:0; width:0; height:0; margin:0; padding:0; }
	span.footnodeNumber { padding-right:1em; }
	span.annotation_style_by_filter { font-size:95%; font-family:Arial; background-color:#fff000;  margin:0; border:0; padding:0;  }
	* { margin:0;}
	.ta1 { writing-mode:lr-tb; }
	.Default { font-family:Calibri; vertical-align:bottom; color:#000000; font-size:11pt; }
	.ce1 { color:#000000; font-size:10pt; font-family:Calibri; vertical-align:bottom; background-color:#ffffff; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce10 { color:#000000; font-size:10pt; font-family:Calibri Light; vertical-align:bottom; background-color:#ffffff; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:lr-tb; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce11 { color:#ffffff; font-size:11pt; font-family:Calibri; vertical-align:bottom; background-color:#1f497d; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce12 { color:#000000; font-size:10pt; font-family:Calibri Light; vertical-align:middle; background-color:#ffffff; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce13 { color:#000000; font-size:10pt; font-family:Calibri Light; vertical-align:bottom; background-color:#ffffff; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce14 { color:#ffffff; font-size:11pt; font-family:Calibri; vertical-align:bottom; background-color:#1f497d; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce15 { color:#000000; font-size:10pt; font-family:Calibri Light; vertical-align:bottom; background-color:#ffffff; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:lr-tb; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce16 { color:#000000; font-size:10pt; font-family:Calibri Light; vertical-align:middle; background-color:#ffffff; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce17 { color:#ffffff; font-size:11pt; font-family:Calibri; vertical-align:bottom; background-color:#ffffff; border-style:none; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce18 { color:#000000; font-size:10pt; font-family:Arial; vertical-align:middle; background-color:#ffffff; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce19 { color:#000000; font-size:10pt; font-family:Calibri Light; vertical-align:bottom; background-color:#ffffff; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce2 { color:#000000; font-size:10pt; font-family:Calibri; vertical-align:middle; background-color:#ffffff; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce20 { color:#000000; font-size:10pt; font-family:Calibri Light; vertical-align:middle; background-color:#ffffff; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce3 { color:#000000; font-size:10pt; font-family:Calibri Light; vertical-align:middle; background-color:#ffffff; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce4 { color:#000000; font-size:10pt; font-family:Calibri; vertical-align:middle; background-color:#ffffff; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce5 { color:#000000; font-size:10pt; font-family:Arial; vertical-align:middle; background-color:#ffffff; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce6 { color:#000000; font-size:10pt; font-family:Arial; vertical-align:middle; background-color:#b7dee8; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce7 { color:#000000; font-size:10pt; font-family:Calibri Light; vertical-align:bottom; background-color:#ffffff; border-width:0.0261cm; border-style:solid; border-color:#000000; text-align:center ! important; margin-left:0pt; writing-mode:lr-tb; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce8 { color:#000000; font-size:10pt; font-family:Calibri; vertical-align:bottom; background-color:#ffffff; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce9 { color:#000000; font-size:10pt; font-family:Arial; vertical-align:middle; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; background-color:#ffffff; border-left-style:none; border-right-style:none; border-top-style:none; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.co1 { width:28.01pt; }
	.co10 { width:112.99pt; }
	.co11 { width:107.01pt; }
	.co12 { width:155.06pt; }
	.co13 { width:242.05pt; }
	.co14 { width:183.06pt; }
	.co15 { width:79.99pt; }
	.co16 { width:141pt; }
	.co17 { width:148.05pt; }
	.co2 { width:87pt; }
	.co3 { width:133.06pt; }
	.co4 { width:380.04pt; }
	.co5 { width:186.04pt; }
	.co6 { width:86pt; }
	.co7 { width:99.01pt; }
	.co8 { width:92.01pt; }
	.co9 { width:103.01pt; }
	.ro1 { height:13.8pt; }
	.ro2 { height:23.41pt; }
	.ro3 { height:15pt; }
	.ro4 { height:12.81pt; }
	
	</style>
	</head>
	<body dir="ltr" style="max-width:612pt;margin-top:54pt; margin-bottom:54pt; margin-left:50.4pt; margin-right:50.4pt; ">

	<table border="0" cellspacing="0" cellpadding="0" class="ta1">
	<colgroup><col width="43"/><col width="134"/><col width="205"/><col width="586"/><col width="287"/><col width="133"/><col width="153"/><col width="142"/><col width="159"/><col width="174"/><col width="174"/><col width="165"/><col width="239"/><col width="373"/><col width="282"/><col width="123"/><col width="217"/><col width="123"/><col width="228"/><col width="123"/>
	</colgroup>

	<tr class="ro1">
		<td style="text-align:left;width:28.01pt; " class="ce1"> </td>
			<td style="text-align:left;width:87pt; " class="Default"> </td>
			<td style="text-align:left;width:133.06pt; " class="ce5"><p>RANGO DE FECHAS:</p></td>
			<td style="text-align:left;width:380.04pt; " class="ce5"><p>DEL <?= formatoMesSinHora($desde) ?> AL <?= formatoMesSinHora($hasta) ?></p></td>
			<td style="text-align:left;width:186.04pt; " class="ce8"> </td>
			<td style="text-align:left;width:86pt; " class="ce8"> </td>
			<td style="text-align:left;width:99.01pt; " class="ce8"> </td>
			<td style="text-align:left;width:92.01pt; " class="ce8"> </td>
			<td style="text-align:left;width:103.01pt; " class="ce17"> </td>
			<td style="text-align:left;width:112.99pt; " class="ce17"> </td>
			<td style="text-align:left;width:112.99pt; " class="ce18"> </td>
			<td style="text-align:left;width:107.01pt; " class="ce18"> </td>
			<td style="text-align:left;width:155.06pt; " class="ce18"> </td>
			<td style="text-align:left;width:242.05pt; " class="ce18"> </td>
			<td style="text-align:left;width:183.06pt; " class="ce18"> </td>
			<td style="text-align:left;width:79.99pt; " class="ce18"> </td>
			<td style="text-align:left;width:141pt; " class="ce18"> </td>
			<td style="text-align:left;width:79.99pt; " class="ce8"> </td>
			<td style="text-align:left;width:148.05pt; " class="ce8"> </td>
			<td style="text-align:left;width:79.99pt; " class="ce1"> </td>
	</tr>
	<tr class="ro1">
			<td style="text-align:left;width:28.01pt; " class="ce1"> </td>
			<td style="text-align:left;width:87pt; " class="ce4"> </td>
			<td style="text-align:left;width:133.06pt; " class="ce8"> </td>
			<td style="text-align:left;width:380.04pt; " class="ce8"> </td>
			<td style="text-align:left;width:186.04pt; " class="ce8"> </td>
			<td style="text-align:left;width:86pt; " class="ce8"> </td>
			<td style="text-align:left;width:99.01pt; " class="ce8"> </td>
			<td style="text-align:left;width:92.01pt; " class="ce8"> </td>
			<td style="text-align:left;width:103.01pt; " class="ce17"> </td>
			<td style="text-align:left;width:112.99pt; " class="ce17"> </td>
			<td style="text-align:left;width:112.99pt; " class="ce18"> </td>
			<td style="text-align:left;width:107.01pt; " class="ce18"> </td>
			<td style="text-align:left;width:155.06pt; " class="ce18"> </td>
			<td style="text-align:left;width:242.05pt; " class="ce18"> </td>
			<td style="text-align:left;width:183.06pt; " class="ce18"> </td>
			<td style="text-align:left;width:79.99pt; " class="ce18"> </td>
			<td style="text-align:left;width:141pt; " class="ce18"> </td>
			<td style="text-align:left;width:79.99pt; " class="ce8"> </td>
			<td style="text-align:left;width:148.05pt; " class="ce8"> </td><td style="text-align:left;width:79.99pt; " class="ce1"> </td>
	</tr>

	<tr class="ro2">
			<td style="text-align:left;width:28.01pt; " class="ce2"> </td>
			<td style="text-align:left;width:87pt; " class="ce6"><p>FECHA</p></td>
			<td style="text-align:left;width:133.06pt; " class="ce6"><p>EJECUTIVO</p></td>
			<td style="text-align:left;width:380.04pt; " class="ce6"><p>CLIENTE </p></td>
			<td style="text-align:left;width:186.04pt; " class="ce6"><p>SISTEMA</p></td>
			<td style="text-align:left;width:86pt; " class="ce6"><p>CANT</p></td>
			<td style="text-align:left;width:99.01pt; " class="ce6"><p>FACT </p><p>CON IGV</p></td>
			<td style="text-align:left;width:92.01pt; " class="ce6"><p>FACT</p><p>SIN IGV</p></td>
			<td style="text-align:left;width:103.01pt; " class="ce6"><p>ESTADO PAGO/FACTURACION</p></td>
			<td style="text-align:left;width:112.99pt; " class="ce6"><p>ESTADO INSTALACIÓN</p></td>
			<td style="text-align:left;width:112.99pt; " class="ce6"><p>BOLETA </p></td>
			<td style="text-align:left;width:107.01pt; " class="ce6"><p>FACTURA </p></td>
			<td style="text-align:left;width:155.06pt; " class="ce6"><p>CANAL</p></td>
			<td style="text-align:left;width:242.05pt; " class="ce6"><p>CANAL </p><p>(CONCESIONARIO O BROKER)</p></td>
			<td style="text-align:left;width:183.06pt; " class="ce6"><p>NOMBRE DEL EJECUTIVO </p><p>DEL CANAL</p></td>
			<td style="text-align:left;width:79.99pt; " class="ce6"><p>OFICINA</p></td>
			<td style="text-align:left;width:141pt; " class="ce6"><p>ASEGURADORA</p></td>
			<td style="text-align:left;width:79.99pt; " class="ce6"><p>APLICA MANEJA </p><p>BIEN Y GANA</p></td>
			<td style="text-align:left;width:148.05pt; " class="ce6"><p>LUGAR DE INSTALACIÓN</p><p>(NOMBRE DEL CONCESIONARIO)</p></td>
			<td style="text-align:left;width:79.99pt; " class="ce2"> </td>
	</tr>
<?php $datos=Comisiones::obtenerConvenios($convenio,$desde,$hasta); ?>
<?php $cantidad=1 ?>
<?php $total=0 ?>
<?php $totalSinIgv=0 ?>
<?php $totalConIgv=0 ?>
	<?php foreach ($datos as $var): ?>
	<?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
	<?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>	
	<?php $datosCanal=Canales::obtenerDatosCanalPorId($var->idcanal) ?>		
	<tr class="ro3">
			<td style="text-align:left;width:28.01pt; " class="ce3"> </td>
			<td style="text-align:right; width:87pt; " class="ce7"><p><?= formato($datosVehiculo->fechaCreacion) ?></p></td>
			<td style="text-align:left;width:133.06pt; " class="ce10"><p><?php $eje=Usuario::obtenerUsuario($var->idCreador); ?><?= $eje->usuarioNombre." ".$eje->usuarioApellidoPaterno; ?></p></td>
			<td style="text-align:left;width:380.04pt; " class="ce10"><p><?= ($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->nombre)." ".strtoupper($clienteObj->apellidosC) : strtoupper($clienteObj->razonSocial) ; ?></p></td>
			<td style="text-align:left;width:186.04pt; " class="ce10"><p><?=  $plan=limpiarPALABRAGPSSMART(strtoupper($datosVehiculo->plan)) ?></p></td>
			<td style="text-align:right; width:86pt; " class="ce12"><p><?= $cantidad ?></p></td>
			<td style="text-align:right; width:99.01pt; " class="ce15"><p><?= $totalM=$var->montoTotal ?></p></td>
			<td style="text-align:right; width:92.01pt; " class="ce16"><p><?= $totalS=round($var->montoTotal/1.18,2) ?></p></td>
			<td style="text-align:left;width:103.01pt; " class="ce10"><p><?= obtenerEstado($var->estadoPago) ?></p></td>
			<td style="text-align:left;width:112.99pt; " class="ce10"><p><?= obtenerEstado($datosVehiculo->estado) ?></p></td>
			<td style="text-align:left;width:112.99pt; " class="ce19"><p><?= (!empty($var->numeroFacturacion))?$var->numeroFacturacion:obtenerEstado($var->estadoFacturacion)  ?></p></td>
			<td style="text-align:left;width:107.01pt; " class="ce19"><p><?= (!empty($var->numeroFacturacion))?$var->numeroFacturacion:obtenerEstado($var->estadoFacturacion)  ?></p></td>
			<td style="text-align:left;width:155.06pt; " class="ce10"><p><?= $datosCanal->nombreCanal ?></p></td>
			<td style="text-align:left;width:242.05pt; " class="ce10"><p><?= $datosCanal->tipoCanal ?></p></td>
			<td style="text-align:left;width:183.06pt; " class="ce10"><p><?= $datosCanal->ejecutivoCanal ?></p></td>
			<td style="text-align:left;width:79.99pt; " class="ce20"><p>-</p></td>
			<td style="text-align:left;width:141pt; " class="ce7"><p><?= $datosVehiculo->companiaSeguro ?></p></td>
			<td style="text-align:left;width:79.99pt; " class="ce20"><p><?= ($datosVehiculo->campana=="GpsSmart")?'Si':'No' ?></p></td>
			<td style="text-align:left;width:148.05pt; " class="ce20"><p>-</p></td>
			<td style="text-align:left;width:79.99pt; " class="ce3"> </td>
	</tr>
	<?php $total+=$cantidad ?>
	<?php $totalSinIgv+=$totalS  ?>
	<?php $totalConIgv+=$totalM  ?>
	<?php endforeach ?>
		<tr class="ro1">
			<td style="text-align:left;width:28.01pt; " class="ce1"> </td>
			<td style="text-align:left;width:87pt; " class="ce5"> </td>
			<td style="text-align:left;width:133.06pt; " class="ce9"> </td>
			<td style="text-align:left;width:380.04pt; " class="ce9"> </td>
			<td style="text-align:left;width:186.04pt; " class="ce11"><p>TOTAL:</p></td>
			<td style="text-align:right; width:86pt; " class="ce11"><p><?= $total ?></p></td>
			<td style="text-align:right; width:99.01pt; " class="ce14"><p><?= number_format($totalConIgv, 2, '.', ',')  ?></p></td>
			<td style="text-align:right; width:92.01pt; " class="ce14"><p><?= number_format($totalSinIgv, 2, '.', ',')  ?></p></td>
			<td style="text-align:left;width:103.01pt; " class="ce17"> </td>
			<td style="text-align:left;width:112.99pt; " class="ce17"> </td>
			<td style="text-align:left;width:112.99pt; " class="ce18"> </td>
			<td style="text-align:left;width:107.01pt; " class="ce18"> </td>
			<td style="text-align:left;width:155.06pt; " class="ce18"> </td>
			<td style="text-align:left;width:242.05pt; " class="ce18"> </td>
			<td style="text-align:left;width:183.06pt; " class="ce18"> </td>
			<td style="text-align:left;width:79.99pt; " class="ce18"> </td>
			<td style="text-align:left;width:141pt; " class="ce18"> </td>
			<td style="text-align:left;width:79.99pt; " class="ce8"> </td>
			<td style="text-align:left;width:148.05pt; " class="ce8"> </td>
			<td style="text-align:left;width:79.99pt; " class="ce1"> </td>
	</tr>
	<tr class="ro4">
		<td style="text-align:left;width:28.01pt; " class="ce1"> </td><td style="text-align:left;width:87pt; " class="ce4"> </td>
		<td style="text-align:left;width:133.06pt; " class="ce8"> </td><td style="text-align:left;width:380.04pt; " class="ce8"> </td><td style="text-align:left;width:186.04pt; " class="ce8"> </td><td style="text-align:left;width:86pt; " class="ce8"> </td><td style="text-align:left;width:99.01pt; " class="ce8"> </td><td style="text-align:left;width:92.01pt; " class="ce8"> </td><td style="text-align:left;width:103.01pt; " class="ce8"> </td><td style="text-align:left;width:112.99pt; " class="ce8"> </td><td style="text-align:left;width:112.99pt; " class="ce8"> </td><td style="text-align:left;width:107.01pt; " class="ce8"> </td><td style="text-align:left;width:155.06pt; " class="ce8"> </td><td style="text-align:left;width:242.05pt; " class="ce8"> </td><td style="text-align:left;width:183.06pt; " class="ce8"> </td><td style="text-align:left;width:79.99pt; " class="ce1"> </td><td style="text-align:left;width:141pt; " class="ce1"> </td><td style="text-align:left;width:79.99pt; " class="ce8"> </td><td style="text-align:left;width:148.05pt; " class="ce8"> </td><td style="text-align:left;width:79.99pt; " class="ce1"> </td>
	</tr>
	<tr class="ro4">
		<td style="text-align:left;width:28.01pt; " class="ce1"> </td><td style="text-align:left;width:87pt; " class="ce4"> </td><td style="text-align:left;width:133.06pt; " class="ce8"> </td><td style="text-align:left;width:380.04pt; " class="ce8"> </td><td style="text-align:left;width:186.04pt; " class="ce8"> </td><td style="text-align:left;width:86pt; " class="ce8"> </td><td style="text-align:left;width:99.01pt; " class="ce8"> </td><td style="text-align:left;width:92.01pt; " class="ce8"> </td><td style="text-align:left;width:103.01pt; " class="ce8"> </td><td style="text-align:left;width:112.99pt; " class="ce8"> </td><td style="text-align:left;width:112.99pt; " class="ce8"> </td><td style="text-align:left;width:107.01pt; " class="ce8"> </td><td style="text-align:left;width:155.06pt; " class="ce8"> </td><td style="text-align:left;width:242.05pt; " class="ce8"> </td><td style="text-align:left;width:183.06pt; " class="ce8"> </td><td style="text-align:left;width:79.99pt; " class="ce1"> </td><td style="text-align:left;width:141pt; " class="ce1"> </td><td style="text-align:left;width:79.99pt; " class="ce8"> </td><td style="text-align:left;width:148.05pt; " class="ce8"> </td><td style="text-align:left;width:79.99pt; " class="ce1">
			
		</td>
	</tr>

	</table>

	</body>

	</html>









 <?php } ?>
 <?php }else{
 	header("Locate:http://resh.tracklink.pe");
 	} ?>
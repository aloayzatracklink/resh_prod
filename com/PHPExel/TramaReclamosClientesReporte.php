<?php 
require __DIR__ . "/../../vendor/autoload.php";
require_once("../../includes/configuracion.php");
require_once("../../includes/Recursos.php");
require_once("../../includes/baseDatos.php");
require_once("../class/ReclamoClienteReporteClass.php");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\IOFactory;

if (isset($_POST['Descargar'])) {
    if(isset($_POST) ){
        $fechaConsulta       = $_POST['fecha'];
        //$fk_idreclamo_estado = $_POST['estado'];
        $fk_idreclamo_estado = isset($_POST["estado"]) ? $_POST["estado"] : null;

        $desde=trim(current(explode('/', $fechaConsulta)));
        $tmp=explode('/', $fechaConsulta);
        $hasta=end($tmp);
        $desde=date("YmdHis",strtotime($desde));  
        $hasta=date("Ymd",strtotime($hasta)); 
        $hasta=$hasta."235959";
        $reporte=new ReclamoClienteReporteClass();
        $datos=$reporte->obtenerReporteReclamosClienteRANGO($desde,$hasta, $fk_idreclamo_estado);
        
        #echo count($datos); 

        //$helper->log('Create new Spreadsheet object');
        $fileName = "EXPORTLDATARECLAMOSCLIENTES".date("YmdHis").".xlsx";
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Tracklink')
        ->setLastModifiedBy('Tracklink')
        ->setTitle('Tracklink Export Doc')
        ->setSubject('Tracklink Export Doc')
        ->setDescription('Tracklink Export Doc')
        ->setKeywords('Tracklink Export Doc')
        ->setCategory('Tracklink Export Doc');

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Nro de Reclamo')
            ->setCellValue('B1', 'Fecha de reclamo')
            ->setCellValue('C1', 'Canal del reclamo')
            ->setCellValue('D1', 'Nivel de atención')
            ->setCellValue('E1', 'Area que detecta')
            ->setCellValue('F1', 'Ejecutivo responsable')
            ->setCellValue('G1', 'Area responsable')
            ->setCellValue('H1', 'Cliente')
            ->setCellValue('I1', 'Chasis')
            ->setCellValue('J1', 'Plazo del reclamo (07 Dìas Habiles)')
            ->setCellValue('K1', 'Motivo del reclamo')
            ->setCellValue('L1', 'Informacion brindada')
            ->setCellValue('M1', 'Persona a cargo del reclamo')
            ->setCellValue('N1', 'Solicitud del reclamo')
            ->setCellValue('O1', 'Resolucion del reclamo')
            ->setCellValue('P1', 'Estado');
            
        
        $rows = 2;
        foreach ($datos as $item){
        //     // Miscellaneous glyphs, UTF-8
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A' . $rows, $item->vreclamo_cliente)
                ->setCellValue('B' . $rows, $item->fecha_reclamo)
                ->setCellValue('C' . $rows, $item->canal_descripcion)
                ->setCellValue('D' . $rows, $item->nivel_atencion_descripcion)
                ->setCellValue('E' . $rows, $item->area_detecta_nombre)
                ->setCellValue('F' . $rows, $item->ejecutivo_responsable_nombre)
                ->setCellValue('G' . $rows, $item->area_responsable_nombre)
                ->setCellValue('H' . $rows, $item->cliente_nombre)
                ->setCellValue('I' . $rows, $item->vehiculo_chasis)
                ->setCellValue('J' . $rows, utf8_encode($item->plazo_descripcion))
                ->setCellValue('K' . $rows, $item->tmotivo_reclamo)
                ->setCellValue('L' . $rows, $item->tinformacion_brindada)
                ->setCellValue('M' . $rows, $item->persona_cargo_reclamo_nombre)
                ->setCellValue('N' . $rows, $item->tsolicitud_reclamo)
                ->setCellValue('O' . $rows, $item->tresolucion_reclamo)
                ->setCellValue('P' . $rows, $item->estado_descripcion);
            $rows++;
        } 

        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Items Selected');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment;filename="' . $fileName . '"');
        // header('Cache-Control: max-age=0');

        // $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        // $writer->save('php://output');
        // exit;
        
        // $writer = new Xlsx($spreadsheet);
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        // $writer->save('php://output');

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;

    }
}
?>
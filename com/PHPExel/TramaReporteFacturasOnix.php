<?php 
require __DIR__ . "../../../vendor/autoload.php";
require_once("../../includes/configuracion.php");
require_once("../../includes/Recursos.php");
require_once("../../includes/baseDatos.php");
require_once("../../includes/baseDatosOnix.php");
require_once("../class/ReportesOnix.php");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\IOFactory;

if (isset($_POST['Descargar'])) {
    if(isset($_POST) ){
        $fechaConsulta=$_POST['fecha'];
        $desde=trim(current(explode('/', $fechaConsulta)));
        $tmp=explode('/', $fechaConsulta);
        $hasta=end($tmp);
        $desde=date("YmdHis",strtotime($desde));  
        $hasta=date("Ymd",strtotime($hasta)); 
        $hasta=$hasta."235959";
        $onixobj=new ReportesOnix();
        $datos=$onixobj->obtenerReporteContratosOctoRANGO($desde,$hasta);
        #echo count($datos); 

        //$helper->log('Create new Spreadsheet object');
        $fileName = "EXPORTLDATAFACOCTO".date("YmdHis").".xlsx";
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Tracklink')
        ->setLastModifiedBy('Tracklink')
        ->setTitle('Tracklink Export Doc')
        ->setSubject('Tracklink Export Doc')
        ->setDescription('Tracklink Export Doc')
        ->setKeywords('Tracklink Export Doc')
        ->setCategory('Tracklink Export Doc');

        $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Cliente')
        ->setCellValue('B1', 'Desde')
        ->setCellValue('C1', 'Hasta')
        //->setCellValue('D1', 'Datos Vehículo');
        ->setCellValue('D1', 'Placa')
        ->setCellValue('E1', 'Marca')
        ->setCellValue('F1', 'Modelo')
        ->setCellValue('G1', 'Chasis')
        ->setCellValue('H1', 'Año')
        ->setCellValue('I1', 'Color');
            // ->setCellValue('A1', 'Id Contrato')
            // ->setCellValue('B1', 'Cliente')
            // ->setCellValue('C1', 'Id Vehículo')
            // ->setCellValue('D1', 'Num Contrato')
            // ->setCellValue('E1', 'Firmado')
            // ->setCellValue('F1', 'Estado Contrato')
            // ->setCellValue('G1', 'Observacion')
            // ->setCellValue('H1', 'Servicio')
            // ->setCellValue('I1', 'Tipo Servicio')
            // ->setCellValue('J1', 'Desde')
            // ->setCellValue('K1', 'Hasta')
            // ->setCellValue('L1', 'Id Cliente')
            // ->setCellValue('M1', 'CI Cliente')
            // ->setCellValue('N1', 'Localidad Contrato')
            // ->setCellValue('O1', 'Provincia')
            // ->setCellValue('P1', 'Ciudad')
            // ->setCellValue('Q1', 'Sector')
            // ->setCellValue('R1', 'Dirección Principal')
            // ->setCellValue('S1', 'Teléfono Principal')
            // ->setCellValue('T1', 'Celular')
            // ->setCellValue('U1', 'Oficina')
            // ->setCellValue('V1', 'Dirección Oficina')
            // ->setCellValue('W1', 'Teléfono Oficina')
            // ->setCellValue('X1', 'Extención')
            // ->setCellValue('Y1', 'Mail1')
            // ->setCellValue('Z1', 'Mail2')
            // ->setCellValue('AA1', 'Datos Vehículo')
            // ->setCellValue('AB1', 'CI Usuario')
            // ->setCellValue('AC1', 'Nombre Usuario')
            // ->setCellValue('AD1', 'Dirección Usuario')
            // ->setCellValue('AE1', 'Teléfono Usuario')
            // ->setCellValue('AF1', 'Celular Usuario')
            // ->setCellValue('AG1', 'Oficina Usuario')
            // ->setCellValue('AH1', 'Dirección Oficina Usuario')
            // ->setCellValue('AI1', 'Teléfono Oficina Usuario')
            // ->setCellValue('AJ1', 'Teléfono Adicional Usuario')
            // ->setCellValue('AK1', 'Mail Usuario')
            // ->setCellValue('AL1', 'CI Familiar')
            // ->setCellValue('AM1', 'Nombre Familiar')
            // ->setCellValue('AN1', 'Teléfono Familiar')
            // ->setCellValue('AO1', 'Concesionario')
            // ->setCellValue('AP1', 'Broker')
            // ->setCellValue('AQ1', 'Vend Interno')
            // ->setCellValue('AR1', 'Vend Externo');
            
        $datosSeparados = array();            
        $rows = 2;
        foreach ($datos as $item){
            $desde = date_create($item->desde);
            $desde = date_format($desde,"d-m-Y");
            $hasta = date_create($item->hasta);
            $hasta = date_format($hasta,"d-m-Y");
            $caracteristicas = explode('|', $item->datos_vehiculo);
            $item->placa = isset($caracteristicas[0]) ? $caracteristicas[0] : '';
            $item->marca = isset($caracteristicas[1]) ? $caracteristicas[1] : '';
            $item->modelo = isset($caracteristicas[2]) ? $caracteristicas[2] : '';
            $item->chasis = isset($caracteristicas[4]) ? $caracteristicas[4] : '';
            $item->anio= isset($caracteristicas[6]) ? $caracteristicas[6] : '';
            $item->color = isset($caracteristicas[7]) ? $caracteristicas[7] : '';
            $datosSeparados[] = $item;
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A' . $rows, $item->cliente)
                ->setCellValue('B' . $rows, $desde)
                ->setCellValue('C' . $rows, $hasta)
                ->setCellValue('D' . $rows, $item->placa)
                ->setCellValue('E' . $rows, $item->marca)
                ->setCellValue('F' . $rows, $item->modelo)
                ->setCellValue('G' . $rows, $item->chasis)
                ->setCellValue('H' . $rows, $item->anio)
                ->setCellValue('I' . $rows, $item->color);

                //->setCellValue('D' . $rows, $item->datos_vehiculo);
            /*
                ->setCellValue('A' . $rows, $item->idcontrato)
                ->setCellValue('B' . $rows, $item->cliente)
                ->setCellValue('C' . $rows, $item->id_vehiculo)
                ->setCellValue('D' . $rows, $item->nro_contrato)
                ->setCellValue('E' . $rows, $item->firmado)
                ->setCellValue('F' . $rows, $item->estado_contrato)
                ->setCellValue('G' . $rows, $item->observacion)
                ->setCellValue('H' . $rows, $item->servicio)
                ->setCellValue('I' . $rows, $item->tipo_servicio)
                ->setCellValue('J' . $rows, $item->desde)
                ->setCellValue('K' . $rows, $item->hasta)
                ->setCellValue('L' . $rows, $item->id_cliente)
                ->setCellValue('M' . $rows, $item->ci_cliente)
                ->setCellValue('N' . $rows, $item->localidad_contrato)
                ->setCellValue('O' . $rows, $item->provincia)
                ->setCellValue('P' . $rows, $item->ciudad)
                ->setCellValue('Q' . $rows, $item->sector)
                ->setCellValue('R' . $rows, $item->direccion_principal)
                ->setCellValue('S' . $rows, $item->telefono_principal)
                ->setCellValue('T' . $rows, $item->celular)
                ->setCellValue('U' . $rows, $item->oficina)
                ->setCellValue('V' . $rows, $item->direccion_oficina)
                ->setCellValue('W' . $rows, $item->telefono_oficina)
                ->setCellValue('X' . $rows, $item->extension)
                ->setCellValue('Y' . $rows, $item->mail1)
                ->setCellValue('Z' . $rows, $item->mail2)
                ->setCellValue('AA' . $rows, $item->datos_vehiculo)
                ->setCellValue('AB' . $rows, $item->ci_usuario)
                ->setCellValue('AC' . $rows, $item->nombre_usuario)
                ->setCellValue('AD' . $rows, $item->direccion_usuario)
                ->setCellValue('AE' . $rows, $item->telefono_usuario)
                ->setCellValue('AF' . $rows, $item->celular_usuario)
                ->setCellValue('AG' . $rows, $item->oficina_usuario)
                ->setCellValue('AH' . $rows, $item->direccion_oficina_usuario)
                ->setCellValue('AI' . $rows, $item->telefono_oficina_usuario)
                ->setCellValue('AJ' . $rows, $item->telefono_adicional_usuario)
                ->setCellValue('AK' . $rows, $item->mail_usuario)
                ->setCellValue('AL' . $rows, $item->ci_familiar)
                ->setCellValue('AM' . $rows, $item->nombre_familiar)
                ->setCellValue('AN' . $rows, $item->telefono_familiar)
                ->setCellValue('AO' . $rows, $item->consesionario)
                ->setCellValue('AP' . $rows, $item->broker)
                ->setCellValue('AQ' . $rows, $item->vend_interno)
                ->setCellValue('AR' . $rows, $item->vend_externo);
            */
            $rows++;
        } 

        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Items Selected');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
        
        // $writer = new Xlsx($spreadsheet);
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        // $writer->save('php://output');


    }
}

?>
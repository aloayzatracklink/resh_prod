<?php 
if ($_POST['rowid']) {
    include_once("../../includes/init.php");
    $comisionid=htmlspecialchars(htmlentities($_POST['rowid']));
    $comisionData=Comisiones::obtenerDatosDeComision($comisionid);
    $clienDatos=Cliente::obtenerDatosDeclientePorId($comisionData->idcliente);
    $vhDatos=Vehiculo::obtenerVehiculo($comisionData->idvehiculo);
    $canalDatos=Canales::obtenerDatosCanalPorId($comisionData->idcanal);
    $obMenu=Usuario::obtenerMenu($session->idusuario);
    $o=Usuario::obtenerUsuario($comisionData->idCreador); 
  ?>
  <input type="hidden" name="comisionid" value="<?= $comisionid ?>" >
  <input type="hidden" name="canalid" value="<?= $comisionData->idcanal ?>" >
  <div class="well" style="padding-top: 11px;padding-bottom: 10px;">
  <div class="row">
    <div class="row">
      <div class="form-group col-md-4">
        <label for="Observaciones" ><?= $retVal = ($clienDatos->tipoCliente=="Natural") ? "DNI" : "RUC" ?></label>
        <input name="" style="border: none;" type="text" placeholder="Web" id="Observaciones" class="border-bottom form-control" readonly  value="<?= $retVal = ($clienDatos->tipoCliente=="Natural") ? $clienDatos->dni : $clienDatos->ruc ?>">
      </div>
      <div class="form-group col-md-8">
        <label for="numeroTecnico" ><?=  ($clienDatos->tipoCliente=="Natural") ? "Nombres" : "Razon Social"  ?></label>
        <input  required readonly style="border: none;" type="text" id="numeroTecnico" name="" class="border-bottom form-control"  value="<?= $retVal = ($clienDatos->tipoCliente=="Natural") ? $clienDatos->nombre." ".$clienDatos->apellidosC : $clienDatos->razonSocial ?> ">
      </div>
    </div>
    <hr>
     <div class="row">
      <div class="form-group col-md-3">
        <label for="placa" >Placa</label>
        <input name="" style="border: none;" type="text" placeholder="Web" id="placa" class="border-bottom form-control" readonly  value="<?= $vhDatos->placa ?>">
      </div>
      <div class="form-group col-md-3">
        <label for="marca" >Marca</label>
        <input  required readonly style="border: none;" type="text" id="marca" name="" class="border-bottom form-control"  value="<?= $vhDatos->marca ?> ">
      </div>
      <div class="form-group col-md-3">
        <label for="planN" >Plan</label>
        <input  required readonly style="border: none;" type="text" id="planN" name="" class="border-bottom form-control"  value="<?= $vhDatos->plan ?> ">
      </div>
      <div class="form-group col-md-3">
        <label for="chasis" >Chasis</label>
        <input  required readonly style="border: none;" type="text"  id="chasis" name="" class="border-bottom form-control"  value="<?= $vhDatos->chasis ?> ">
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
          <h3 class="text-center">Ejecutivo Tracklink: <?= $o->usuarioNombre." ".$o->usuarioApellidoPaterno;   ?></h3>
      </div>
    </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="form-group col-md-3">
    <label for="ticanal">Tipo canal*</label>
    <input  style="border: none;" type="text" placeholder="Canal" id="ticanal" name="tipoCanal" class="border-bottom form-control" required readonly value="<?= $canalDatos->tipoCanal ?>"  >
  </div>
  <div class="form-group col-md-4">
    <label for="nomCanal" >Nombre Canal *</label>
    <input required  style="border: none;" type="text" placeholder="nombrecanal" id="nomCanal" name="nombreCanal" class="border-bottom form-control"  value="<?= strtoupper($canalDatos->nombreCanal) ?>" >
  </div>
  <div class="form-group col-md-5">
    <label for="ejeCanal" >Ejecutivo canal*</label>
    <input  required  style="border: none;" type="text" placeholder="Ejecutivo" id="ejeCanal" name="ejecutivoCanal" class="border-bottom form-control"   value="<?= strtoupper($canalDatos->ejecutivoCanal) ?>">
  </div>
  
</div>
<div class="row">
  <div class="form-group col-md-4">
    <label for="banco"  >Banco*</label>
    <input required style="border: none;" type="text" placeholder="Banco" id="bancoCanal" name="banco" autocomplete="off" class="bancoCanal border-bottom form-control" value="<?= strtoupper($canalDatos->banco) ?>" >
  </div>
  <div class="form-group col-md-4">
    <label for="nrcuenta" >Numero de cuenta *</label>
    <input required style="border: none;" type="text" placeholder="Numero de cuenta" id="nrcuenta" name="numeroCuenta" class="border-bottom form-control" value="<?= $canalDatos->numeroCuenta ?>"  >
  </div>
  <div class="form-group col-md-4">
    <label for="moneda" >Moneda *</label>
    <input required style="border: none;" type="text" placeholder="Moneda" id="moneda" name="moneda" class="border-bottom form-control" value="<?= $canalDatos->moneda ?>"  >
  </div>
</div> 
<div class="row">
  <div class="form-group col-md-2">
    <label for="dni"  >DNI *</label>
    <input required style="border: none;" type="text" placeholder="DNI" id="dni" name="dni" class="border-bottom form-control" value="<?= $canalDatos->canalId ?>">
  </div>
  <div class="form-group col-md-6">
    <label for="titular" >Titular *</label>
    <input style="border: none;" required type="text" placeholder="Titular *" id="titular" name="titular" class="border-bottom form-control" value="<?= strtoupper($canalDatos->titular) ?>"  >
  </div>
  <div class="form-group col-md-4">
    <label for="cci" >CCI </label>
    <input style="border: none;" type="text" placeholder="CCI " id="cci" name="cci" class="border-bottom form-control" value="<?= $canalDatos->cci ?>"  >
  </div>
</div>
<hr>
<div class="row">
  <div class="form-group col-md-3">
    <label for="totalM" >Monto total *</label>
    <input style="border: none;" type="text" placeholder="Monto total" id="totalM" name="montoTotal" class="border-bottom form-control" value="<?= $comisionData->montoTotal ?>" required >
  </div>  
  <div class="form-group col-md-3">
    <label for="comi" >Comision *</label>
    <input style="border: none;" type="text" placeholder="Comision" id="comi" name="comision" class="border-bottom form-control" value="<?= $comisionData->comision ?>" required >
  </div>  
 
  <div class="form-group col-md-6">
    <label for="fe" >Fecha registro *</label>    
    <span class="help-block m-b-none"><?= hace($comisionData->fechaRegistro) ?></span>
  </div>  
</div>

<?php if (empty($comisionData->reciboHonorarios)): ?>
  
<div class="row">
    <div class="col-lg-12">
    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
      <div class="form-control" data-trigger="fileinput">
        <i class="glyphicon glyphicon-file fileinput-exists"></i>
        <span class="fileinput-filename"></span>
      </div>
      <span class="input-group-addon btn btn-default btn-file">
        <span class="fileinput-new">Adjuntar recibo por honorarios </span>
        <span class="fileinput-exists">Cambiar</span>
        <input type="file" name="recibo" />
      </span>
      <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
    </div> 
  </div>
</div>
<?php else: ?>
         <div class="row">
    <img class="text-center col-md-6 col-md-offset-3" width="500" src="../../public/vouchers/<?= $comisionData->reciboHonorarios ?>" >      
    </div>
  <div class="row">
    <a  class="btn btn-primary text-center  col-md-6 col-md-offset-3" href="../../public/vouchers/<?= $comisionData->reciboHonorarios ?>" target="_blank" >Descargar/ver recibo por Honorarios</a>    
  </div>
<?php endif ?>


<div class="row">
<?php if ($obMenu->comisiones=="si"): ?>  
  <div class="form-group col-md-6">
    <label for="rucParaFacturacion" class="text-danger" >RUC *</label>
    <input style="border: none;" type="text" placeholder="RUC " id="rucParaFacturacion" name="rucParaFacturacion" class="border-bottom form-control" required value="<?= $canalDatos->ruc ?>" >
  </div>
  <div class="form-group col-md-6">
    <label for="rhonorario" class="text-danger"> RECIBO HONORARIO *</label>
    <input style="border: none;" type="text" placeholder=" RECIBO HONORARIO* " id="rhonorario" name="rhonorario" required class="border-bottom form-control" value="<?= $comisionData->numeroReciboHonorario ?>">
  </div>
<?php endif ?>
</div>
<div class="row">
  <div class="form-group col-md-4">
    <label for="documento" >Documento *</label>
    <input required  style="border: none;" type="text" placeholder="Documento Factura" id="documento" name="Documento" readonly class="border-bottom form-control"  value="<?= $comisionData->documentoFacturacion ?>"  >
  </div>
  <div class="form-group col-md-3">
    <label for="numFactura">Numero de facturacion</label>
    <input  style="border: none;" type="text" placeholder="Numero de facturacion" id="numFactura" name="numeroFacturacion" class="border-bottom form-control" required readonly value="<?= $comisionData->numeroFacturacion ?>"  >
  </div>
  <div class="form-group col-md-5">
    <label for="rhnorario" >Recibo Honorario</label>
    <input  required  style="border: none;" type="text" placeholder="Recibo Honorario" id="rhnorario" class="border-bottom form-control"   value="<?= $comisionData->numeroReciboHonorario ?>" readonly>
  </div>
  
</div>
<?php if ($comisionData->estado==67): ?>
  <div class="row">
    <div class="col-md-12 form-group">&nbsp;&nbsp;<label for="">Pagado en la fecha <?= hace($comisionData->fechaPagado) ?></label></div>
  </div>  
<?php endif ?>
<?php if ($comisionData->estado==62): ?>
  <div class="row">
    <div class="col-md-12 form-group">&nbsp;&nbsp;<label for="">Observación de pago: <?= $comisionData->observacion ?></label></div>
  </div>  
<?php endif ?>
<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script>
  function validarCanal(){

var banco = document.getElementById('bancoCanal');
  bancoCanal=banco.value.toUpperCase();
  // alert(bancoCanal);
if (bancoCanal=="BANCO FINANCIERO" || bancoCanal=="BANCO DE CREDITO"|| bancoCanal=="BANCO CONTINENTAL BBVA"|| bancoCanal=="BANCO BANBIF" ){
  document.getElementById("cci").removeAttribute("required");
}else{
  var cci = document.getElementById("cci"); 
  var att = document.createAttribute("required");     
  att.cci = "true";
  cci.setAttributeNode(att);
}}
</script>
<script>
   $('.bancoCanal').typeahead({
        source: [        
        {"name": "BANCO FINANCIERO"},        
        {"name": "BANCO DE CREDITO"},
        {"name": "BANCO CONTINENTAL BBVA"},
        {"name": "BANCO BANBIF"},
      
      ]
    }); 
</script>

<?php
} 
?>


<?php   
if ($_POST['rowid']) {
  include_once("../../includes/init.php");
  $idOperacion=htmlspecialchars(htmlentities($_POST['rowid']));
  $ObjetoOperacion=Operacion::obtenerOperacionPorId($idOperacion);
  $vehiculoObj=Vehiculo::obtenerVehiculo($ObjetoOperacion->idvehiculo);
  $clienteObj=Cliente::obtenerClientePorVehiculo($ObjetoOperacion->idvehiculo,$ObjetoOperacion->idcliente);
  $clienteObjModal=new Cliente;

   $lista = Operacion::obtenerFrecuentes($session->idusuario);
  ?>
<link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
<?php if (!empty($ObjetoOperacion->datosfacturacionid)): ?>    
  <?php 
    $objFacturacion=DatosFacturacion::obtenerDatosFacturacionPorId($ObjetoOperacion->datosfacturacionid);
   ?>
  <?php if (empty($objFacturacion->voucher)): ?> 
    <h3 class="text-center text-danger">Atención pago en caja</h3>
    <input type="hidden" name="pagoEnCaja" value="ok">
  <?php else: ?>
 <img class="text-center" width="800" src="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" alt="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" >
 <div class="row">
    <a  class="btn btn-primary text-center  col-md-6 col-md-offset-3" href="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" target="_blank" >Descargar/ver Voucher</a>
    
  </div>
  <?php endif ?> 
<hr>
<?php endif ?>
<?php if ($vehiculoObj): ?>
  <?php 
  if ($vehiculoObj->estado==1) {
    ?>
    <br>  
  <div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>

        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->dni ?>">       
      <?php else: ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->ruc ?>">        
      <?php endif ?>  
    </div>
    <div class="col-md-9">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->razonSocial ?>">       
      <?php endif ?>
    </div>
    

  </div>    



<?php if ($clienteObj->tipoCliente!="Natural"): ?>

  <hr>
  <div class="row">
    <div class="row">
      <div class="col-md-12">
        <h4>Representante legal </h4>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <label for="dni">DNI: </label>
        <input style="border: none;" type="text" placeholder="DNI: " id="dni" name="dni" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->dni ?>">
      </div>
      <div class="col-md-4">
        <label for="nombre">Nombres: </label>
        <input style="border: none;" type="text" placeholder="Nombres: " id="nombre" name="nombre" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->nombre ?>">
      </div>
      <div class="col-md-4">
        <label for="apellidosC">Apellidos </label>
        <input style="border: none;" type="text" placeholder="Nombres: " id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->apellidosC ?>">
      </div>
    </div>


<?php endif ?>
<!-- Datos de contacto here please -->
<div class="row">
    <div class="col-md-3">
      <label for="telefono">Celular: </label>
      <input style="border: none;" type="text" placeholder="Celular:" id="telefono" name="telefono" class="border-bottom form-control x2 text-uppercase"  value="<?= $clienteObj->telefono ?>">        
    </div>
    <div class="col-md-3">
      <label for="telefono1">Telefono : </label>
      <input style="border: none;" type="text" placeholder="Telefono :" id="telefono1" name="telefono1" class="border-bottom form-control x2 text-uppercase"  value="<?= $clienteObj->telefono1 ?>">     
    </div>
    <div class="col-md-3">
      <label for="correo">Correo: </label>
      <input style="border: none;" type="text" placeholder="Correo:" id="correo" name="correo" class="border-bottom form-control x2 text-uppercase" value="<?= $clienteObj->correo ?>">       
    </div>
    <div class="col-md-3">
      <label for="correo1">Correo alternativo: </label>
      <input style="border: none;" type="text" placeholder="Correo alternativo:" id="correo1" name="correo1" class="border-bottom form-control x2 text-uppercase"  value="<?= $clienteObj->correo1 ?>">        
    </div>
  </div> 
<hr>
<h3>Datos del vehiculo</h3>
<?php 
  $vehiculoObj1=Vehiculo::obtenerClientePorIdCliente($clienteObj->idcliente,$vehiculoObj->idvehiculo);
?>


<?php foreach ($vehiculoObj1 as $var): ?> 
  <div class="row"><hr>
    <div class="col-lg-12 field_wrapper"> 
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="placa" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $var->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="">Chasis</label>
          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="chasis" class="border-bottom form-control text-uppercase" required value="<?php echo $var->chasis ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="motor" class="">Motor</label>
          <input style="border: none;" type="text" placeholder="Motor" id="motor" name="motor" class="border-bottom form-control text-uppercase" required value="<?php echo $var->motor ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="marca" class="">Marca</label>
          <input style="border: none;" type="text" placeholder="Marca" id="marca" name="marca" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $var->marca ?>" >
        </div>
      </div> 
      <div class="row">
        <div class="form-group col-md-3">
          <label for="modelo" class="">Modelo</label>
          <input style="border: none;" type="text" placeholder="Modelo" id="modelo" name="modelo" class="border-bottom form-control text-capitalize"  required  value="<?php echo $var->modelo ?>">
        </div>
        <div class="form-group col-md-2">
          <label for="color" class="">Color</label>
          <input style="border: none;" type="text" placeholder="Color" id="color" name="color" class="border-bottom form-control x2 text-capitalize" required value="<?php echo $var->color ?>" >
        </div>
        <div class="form-group col-md-2">
          <label for="anio" class="">Año</label>
          <input style="border: none;" type="text" placeholder="Año" id="anio" name="anio" class="border-bottom form-control" required value="<?php echo $var->anio ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="seguro" class="">Compañia de seguros</label>
          <input style="border: none;" type="text" placeholder="Compañia de seguros" id="seguro" name="seguro" class="seguro border-bottom form-control" required autocomplete="off" value="<?php echo $var->companiaSeguro ?>">
        </div>
        <div class="form-group col-md-2">
          <label for="concecionario" class="">Concesionario</label>
          <input style="border: none;" type="text" placeholder="Concesionario" id="concecionario" name="concecionario" class="seguro border-bottom form-control"  autocomplete="off" value="<?php echo $var->concesionario ?>">
        </div>
      </div> 
    </div>
  </div> 
<?php endforeach ?> 
<hr><div class="hr-line-dashed"></div>
Datos de canal.
<?php $datosC=Canales::obtenerDatosCanalPorId($objFacturacion->idcanal) ?>
<div class="row">
        <div class="form-group col-md-4">
          <label for="tipoCCnal" class="">Tipo</label>
          <input style="border: none;" type="text" placeholder="tipoCCnal" id="tipoCCnal" name="tipoCCnal" class="border-bottom form-control x2 text-uppercase" required value="<?= $datosC->tipoCanal ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="nombreCCCanl" class="">Nombre</label>
          <input style="border: none;" type="text" placeholder="nombreCCCanl" id="nombreCCCanl" name="nombreCCCanl" class="border-bottom form-control text-uppercase" required value="<?= $datosC->nombreCanal ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="EjecutCCanl" class="">Ejecutivo canal</label>
          <input style="border: none;" type="text" placeholder="EjecutCCanl" id="EjecutCCanl" name="EjecutCCanl" class="border-bottom form-control text-uppercase" required value="<?= $datosC->ejecutivoCanal ?>" >
        </div>
</div> 
<hr><div class="hr-line-dashed"></div>

<div class="row">
  <div class="col-md-12 text-center"><h1>Datos de instalación</h1></div>
</div>
<hr>
<!-- MEJORAR PARA QUE APLIQUE CUANDO SEAN INSTALACIONES EN DOMICILIO DE LCIENTE  -->
<h4> <small>Lugar de instalación sugerido por el ejecutivo de ventas </small><?php echo $ObjetoOperacion->lugarInstalacion ?> <?php echo $retVal = (!empty($ObjetoOperacion->nombreConcecionarioInstalicion)) ? $ObjetoOperacion->nombreConcecionarioInstalicion : "" ; ?> <small>Hora sugerida:</small> <?= ($ObjetoOperacion->fechaInstalacion=='' ? 'Sin fecha y hora sugerida':formatoHaceEn($ObjetoOperacion->fechaInstalacion)) ?></h4>
<hr>
<input type="hidden" name="idoperacion" value="<?php echo $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idvh" value="<?php echo $vehiculoObj->idvehiculo ?>">
<input type="hidden" name="datosfacturacionid" value="<?php echo $ObjetoOperacion->datosfacturacionid ?>">


<div role="tablist">
  <input type="radio" title="taller" name="lugarInstacion" id="taller" role="tab" aria-controls="taller-panel"  value="Taller" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Taller")?"checked":"" ?> />
  <input type="radio" title="domicilio" name="lugarInstacion" id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Domicilio Cliente")?"checked":"" ?> />
  <input type="radio" title="concesionario" name="lugarInstacion" id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Concesionario")?"checked":"" ?> />
  <div class="tabs" aria-hidden="true">
    <label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
    <label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
    <label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
  </div>
  <div class="panels">
    <ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">
     <!-- <h2>TALLER</h2>
      <p>Av. Paseo de la República 4925 con Angamos Este </p>
      <p>Surquillo, Lima, Lima</p> -->

        <h2><?= $lista->lugarInstalacion ?> | <?= $lista->observacion ?></h2>
        <p><?= $lista->direccionInstacion  ?></p>
        <p>
            <?php $dep=$clienteObjModal->obtenerDepartamentoPorId($lista->departamentoi) ?><?= $dep->departamento ?>
            &nbsp;|&nbsp;
            <?php $prov=$clienteObjModal->otenerProvinciaPorId($lista->provinciai) ?><?php echo $prov->provincia ?>
            &nbsp;|&nbsp;
            <?php $di=$clienteObjModal->obtenerDistritoPorId($lista->distritoi) ?><?= $di->distrito ?>
        </p>

        <input type="hidden" name="direccionTaller" value="<?= $lista->direccionInstacion  ?>">
        <input type="hidden" name="disT" id="distritoTallerid" value="<?= $lista->distritoi  ?>">
        <input type="hidden" name="disT" value="<?= $lista->distritoi  ?>">
        <input type="hidden" name="proT" value="<?= $lista->provinciai  ?>">
        <input type="hidden" name="depT" value="<?= $lista->departamentoi  ?>">

      <div class="row">
         <!-- <div class="form-group  col-md-4">
            <label for="departamentoInstalacionT" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionT border-bottom form-control text-capitalize tal" required name="departamentoInstalacionT" id="departamentoInstalacionT">
                  
                         <option class="text-capitalize" value="15"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId('15') ?><?= $departamento->departamento ?></option>
                      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                      <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                      <?php else: ?>
                      <?php endif ?>                    

                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionT" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionT border-bottom form-control text-capitalize tal" required name="provinciaInstalacionT" id="provinciaInstalacionT" >
                    <?php //if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId('127') ?><?php echo $provincia->provincia ?></option>
                     <?php
                      //} ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionT" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionT border-bottom form-control text-capitalize tal" required name="distritoInstalacionT"  id="distritoInstalacionT" >
                <?php// if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId('1291') ?><?= $distrito->distrito ?></option>
                    <?php
                      //} ?>                 
              </select>           
          </div> -->

    </div>
      <!--<input type="hidden" name="direccionTaller" value="Av. Paseo de la República 4925 con Angamos Este - Surquillo ">
      <input type="hidden" name="referenciaT" value="">-->
      <div class="row">
    <div class="form-group col-md-5" id="data_1">
      <div class="row">
        <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon "><i class="fa fa-calendar"></i></span><input type="text" class="form-control fechaa tal"  id="fechaIT" name="fechaIT" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10)  ?>">
          </div>
        </div>
        <div class="form-group col-md-6">
            <label  class="">Hora</label>
          <div class="input-group" data-autoclose="true">
              <input type="text" class="form-control horaa tal" id="horaIT" name="horaIT" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>" >
              <span class="input-group-addon">
                  <span class="fa fa-clock-o"></span>
              </span>
          </div>
        </div>
      </div>
    </div> 
            <div class="form-group col-md-4">
              <label for="nombreContactoIT" class="">Nombre Contacto *</label>
              <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIT" name="nombreContactoIT" class="form-control x2 text-capitalize border-bottom tal" value="<?= ($ObjetoOperacion->nombreContacto=='')?'':$ObjetoOperacion->nombreContacto ?>">
            </div><div class="form-group col-md-3">
            <label for="numeroContactoIT" class="">Número Contacto *</label>
            <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContactoIT" name="numeroContactoIT" class="form-control x2 text-capitalize border-bottom tal"  value="<?= ($ObjetoOperacion->numeroContacto=='')?'':$ObjetoOperacion->numeroContacto ?>">
          </div>
        </div>
        <?php if ($ObjetoOperacion->idReprogramado!=0): ?>
          <div class="row">
            <h3 for="" class="text-danger text-center col-md-12">Reprogramacion: <?=  $ObjetoOperacion->observacion ?></h3><br>
          </div>
        <?php endif ?>
        <div class="row">
          <div class="form-group col-md-12">
            <label for="observacionT" class="">Seguimiento realizado *</label>
            <input style="border: none;" type="text" placeholder="Seguimiento realizado *" id="observacionT" name="observacion" class="form-control x2  border-bottom tal"  required="">
          </div>      
      </div>
      
    </ul>
    <ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab"> 
      <h2>Domicilio Cliente</h2>
       <?php 
       if (!empty($clienteObj->departamento)) {
          $depatamentoObj=$clienteObj->obtenerDepartamentoPorId($clienteObj->departamento); 
          $provinciaObj=$clienteObj->otenerProvinciaPorId($clienteObj->provincia); 
          $distritoObj=$clienteObj->obtenerDistritoPorId($clienteObj->distrito);        
       }           
    ?> 
      <div class="row">
         <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionD" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize dom" required name="departamentoInstalacionD" id="departamentoInstalacionD">
                    <?php if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                        <option class="text-capitalize " value=""  selected  disabled="">Seleccione departamento</option>
                            <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                          <?php if ($objDepartamento): ?>
                            <?php foreach ($objDepartamento as $var): ?>
                              <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                            <?php endforeach ?>
                          <?php else: ?>
                          <?php endif ?>
                      <?php
                    }else{
                      ?>
                         <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
                      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                      <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                      <?php else: ?>
                      <?php endif ?>

                      <?php
                      } ?>                 


                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionD" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize dom" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
                    <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
                     <?php
                      } ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionD" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize dom" required name="distritoInstalacionD"  id="distritoInstalacionD" >
                <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
                    <?php
                      } ?>                 
              </select>           
          </div>

        </div>
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="direccionInstalacionD" class="">Direccion*</label>
            <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionD" name="direccionInstalacionD"  class="form-control border-bottom dom" value="<?= (!empty($ObjetoOperacion->direccionInstacion))?$ObjetoOperacion->direccionInstacion : ''  ?>">
          </div>
          <div class="form-group  col-md-6">
            <label for="referenciaInstalacionD" class="">Referencia *</label>
            <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" name="referenciaInstalacionD" class="form-control border-bottom  dom" value="<?= (!empty($ObjetoOperacion->referenciaInstalacion)) ? $ObjetoOperacion->referenciaInstalacion:'' ?>" >
          </div>
        </div>  
        <div class="row">
          <div class="form-group col-md-6">
            <label for="cod" class="">Cordenadas Domicilio *</label>
            <input style="border: none;" type="text" placeholder="Cordenadas Domicilio *" id="cod" name="cordenadasD" class="form-control x2 text-capitalize border-bottom dom" value="<?= (!empty($ObjetoOperacion->coordenadasInstalacion))? $ObjetoOperacion->coordenadasInstalacion:'' ?>">
          </div>
          <div class="form-group col-md-6">
            <label for="observacionD" class="">Observaciones *</label>
            <input style="border: none;" type="text" placeholder="Observaciones *" id="observacionD" name="observacion" class="form-control x2 text-capitalize border-bottom dom" required="">
          </div>
        </div>  
         <div class="form-group col-md-5" id="data_1">
      <div class="row">
        <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control fechaa dom"  id="fechaID" name="fechaID" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10)  ?>"  >
          </div>
        </div>
        <div class="form-group col-md-6">
                      <label  class="">Hora</label>
          <div class="input-group" data-autoclose="true">
              <input type="text" class="form-control horaa dom" id="horaID" name="horaID" autocomplete="off" value="<?= substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>" required>
              <span class="input-group-addon">
                  <span class="fa fa-clock-o"></span>
              </span>
          </div>
        </div>
      </div>
   </div> 
        <div class="row">
          <div class="form-group col-md-6">
            <label for="nombreContactoID" class="">Nombre Contacto *</label>
            <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoID" name="nombreContactoID" class="form-control x2 text-capitalize border-bottom dom"  value="<?= ($ObjetoOperacion->nombreContacto=='')?'':$ObjetoOperacion->nombreContacto ?>">
          </div><div class="form-group col-md-6">
          <label for="numeroContactoID" class="">Número Contacto *</label>
          <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContactoID" name="numeroContactoID" class="form-control x2 text-capitalize border-bottom dom"  value="<?= ($ObjetoOperacion->numeroContacto=='')?'':$ObjetoOperacion->numeroContacto ?>" >
        </div>
      </div>
    </div> 
  </ul>
  <ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">
    
    <div class="row">
      <div class="col-md-6"><h2>Concesionario</h2></div>
      <div class="col-md-6">
        <label for="concesionarioInstalacion" class="">Nombre de concesionario*</label>
        <input style="border: none;" type="text" placeholder="Nombre de concesionario*" id="concesionarioInstalacion" name="concesionarioInstalacion" class="form-control border-bottom con" value="<?= (!empty($ObjetoOperacion->nombreConcecionarioInstalicion))?$ObjetoOperacion->nombreConcecionarioInstalicion:''  ?>" >
      </div>
    </div>
    <div class="row">
     <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionC" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionC border-bottom form-control text-capitalize con" required name="departamentoInstalacionC" id="departamentoInstalacionC">
                    <?php if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                        <option class="text-capitalize" value=""  selected  disabled="">Seleccione departamento</option>
                            <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                          <?php if ($objDepartamento): ?>
                            <?php foreach ($objDepartamento as $var): ?>
                              <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                            <?php endforeach ?>
                          <?php else: ?>
                          <?php endif ?>
                      <?php
                    }else{
                      ?>
                         <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
                      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                      <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                      <?php else: ?>
                      <?php endif ?>

                      <?php
                      } ?>                 


                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionC" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionC border-bottom form-control text-capitalize con" required name="provinciaInstalacionC" id="provinciaInstalacionC" >
                    <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
                     <?php
                      } ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionC" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionC border-bottom form-control text-capitalize con" required name="distritoInstalacionC"  id="distritoInstalacionC" >
                <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
                    <?php
                      } ?>                 
              </select>           
          </div>

        </div>
      <div class="row">
        <div class="form-group  col-md-6">
          <label for="direccionInstalacionC" class="">Direccion*</label>
          <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom con" value="<?= $ObjetoOperacion->direccionInstacion ?>" >
        </div>
        <div class="form-group  col-md-6">
          <label for="referenciaInstalacionC" class="">Referencia *</label>
          <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom con"  value="<?= $ObjetoOperacion->referenciaInstalacion ?>" >
        </div>
      </div>    
      <div class="row">
          <div class="form-group col-md-5" id="data_1">
                <div class="row">
                  <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
                      <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control fechaa con"  id="fechaIC" name="fechaIC" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10)  ?>">
                      </div>
                  </div>
                  <div class="form-group col-md-6">
                                <label  class="">Hora</label>
                    <div class="input-group" data-autoclose="true">
                        <input type="text" class="form-control horaa con" id="horaIC" name="horaIC" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>">
                        <span class="input-group-addon">
                            <span class="fa fa-clock-o"></span>
                        </span>
                    </div>
                  </div>
                </div>
         </div> 
      </div>
      <div class="row">
        <div class="form-group col-md-6">
          <label for="nombreContactoIC" class="">Nombre Contacto *</label>
          <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIC con" name="nombreContactoIC" class="form-control x2 text-capitalize border-bottom" value="<?= ($ObjetoOperacion->nombreContacto=='')?'':$ObjetoOperacion->nombreContacto ?>" >
        </div>
        <div class="form-group col-md-6">
        <label for="numeroContactoIC" class="">Número Contacto *</label>
        <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContactoIC" name="numeroContactoIC" class="form-control x2 text-capitalize border-bottom con"  value="<?= ($ObjetoOperacion->numeroContacto=='')?'':$ObjetoOperacion->numeroContacto ?>">
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-12">
        <label for="observacionC" class="">Observaciones *</label>
        <input style="border: none;" type="text" placeholder="Observaciones *" id="observacionC" name="observacion" class="form-control x2 text-capitalize border-bottom con" required="">
      </div>
    </div>
  </div>
</ul>
</div>

</div> 
  <div id="programacionHtml"></div>
<?php
}
?>
<?php endif ?>
<?php
} 
?> 
<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../../assets/build/jquery.datetimepicker.full.js"></script>
<!-- Mañana 9 am callle calos fereeiros 377 san isidro .... javier prado con guardia civil ... -->
<script>  
$(".departamentoInstalacionD").click(function(){provincias4();});
$(".departamentoInstalacionC").click(function(){provincias5();});
$(".provinciaInstalacionD").click(function(){distritos4();});
$(".provinciaInstalacionC").click(function(){distritos5();});
function provincias4(){
  var iddepartamento = $(".departamentoInstalacionD").val();
  $.ajax({        
    type : 'POST',
    url  : '../com/ajax/provincia',
    data :  'iddepartamento='+ iddepartamento,
    success : function(data)
    {
      $(".provinciaInstalacionD").html(data);
    }
  });
  return false;
}
function provincias5(){
  var iddepartamento = $(".departamentoInstalacionC").val();
  $.ajax({        
    type : 'POST',
    url  : '../com/ajax/provincia',
    data :  'iddepartamento='+ iddepartamento,
    success : function(data)
    {
      $(".provinciaInstalacionC").html(data);
    }
  });
  return false;
}

  function distritos4(){
      var idprovincia = $(".provinciaInstalacionD").val();
      $.ajax({        
        type : 'POST',
        url  : '../com/ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionD").html(data)
        }
      });
      return false;
    }
    function distritos5(){
      var idprovincia = $(".provinciaInstalacionC").val();
      $.ajax({        
        type : 'POST',
        url  : '../com/ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionC").html(data)
        }
      });
      return false;
    }   
<?php $fechaA=date('Y-m-d') ?>
  jQuery.datetimepicker.setLocale('es');
  $('#fechaIT').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d',        
    scrollMonth : false,
    scrollInput: false,
    timepicker:false
    // allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });
  $('#horaIT').datetimepicker({
    minDate:'<?= $fechaA ?>',
    // inline:true,
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','11:00','14:00','16:00']
  });

  $('#fechaIC').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d',
    scrollInput: false,
    timepicker:false,
    scrollMonth : false
  });   
  $('#horaIC').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','11:00','14:00','16:00']
  });


   $('#fechaID').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d',
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
  });
  $('#horaID').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','11:00','14:00','16:00']
  });
 

  ///////////////////////////////////////////////////
  $( document ).ready(function() {
    var fecha= $("#fechaIT").val(); 
    var hora= $("#horaIT").val();     
      fecha=fecha+' '+hora;
    $.ajax({
        type : 'POST',
        url  : '../includes/programacionAjax',
        data : {fecha:fecha},
        success : function(data)
        {
         $("#programacionHtml").html(data);
        }
      });

});
 $("#horaIT").bind("load change ",function()
    { 
      var hora = $(this).val(); 
      var fecha= $("#fechaIT").val();     
      fecha=fecha+' '+hora;      
      $.ajax({
        type : 'POST',
        url  : '../includes/programacionAjax',
        data : {fecha:fecha},
        success : function(data)
        {
         $("#programacionHtml").html(data);
        }
      });
      
      return false;

    });
 $("#fechaIT").bind("change",function()
    { 
      var fecha = $(this).val(); 
      var hora= $("#horaIT").val();     
      fecha=fecha+' '+hora;
      if (hora!='') {
        $.ajax({
          type : 'POST',
          url  : '../includes/programacionAjax',
          data : {fecha:fecha},
          success : function(data)
          {
           $("#programacionHtml").html(data);
          }
        });
      }      
      return false;
    });
 $("#horaIC").bind("load change ",function()
    { 
      var horaIC = $(this).val(); 
      var fechaIC= $("#fechaIC").val();     
      fechaIC=fechaIC+' '+horaIC;      
      $.ajax({
        type : 'POST',
        url  : '../includes/programacionAjax',
        data : {fecha:fechaIC},
        success : function(data)
        {
         $("#programacionHtml").html(data);
        }
      });
      
      return false;

    });
 $("#fechaIC").bind("change",function()
    { 
      var fechaIC = $(this).val(); 
      var horaIC= $("#horaIC").val();     
      fechaIC=fechaIC+' '+horaIC;
      if (horaIC!='') {
        $.ajax({
          type : 'POST',
          url  : '../includes/programacionAjax',
          data : {fecha:fechaIC},
          success : function(data)
          {
           $("#programacionHtml").html(data);
          }
        });
      }      
      return false;
    });

 $("#horaID").bind("load change ",function()
    { 
      var horaID = $(this).val(); 
      var fechaID= $("#fechaID").val();     
      fechaID=fechaID+' '+horaID;      
      $.ajax({
        type : 'POST',
        url  : '../includes/programacionAjax',
        data : {fecha:fechaID},
        success : function(data)
        {
         $("#programacionHtml").html(data);
        }
      });
      
      return false;

    });
 $("#fechaID").bind("change",function()
    { 
      var fechaID = $(this).val(); 
      var horaID= $("#horaID").val();     
      fechaID=fechaID+' '+horaID;
      if (horaID!='') {
        $.ajax({
          type : 'POST',
          url  : '../includes/programacionAjax',
          data : {fecha:fechaID},
          success : function(data)
          {
           $("#programacionHtml").html(data);
          }
        });
      }      
      return false;
    });

 function esconder(){  
  $("#programacionHtml").html("");
 }

function validar(){
  var lugarI=$( "input[name='lugarInstacion']:checked" ).val()
  if (lugarI=='Taller') {
      $('.con').prop('disabled', true);
      $('.dom').prop('disabled', true);
      $('.tal').prop('disabled', false);
  }else if (lugarI=='Domicilio Cliente') {
    $('.con').prop('disabled', true);
      $('.dom').prop('disabled', false);
      $('.tal').prop('disabled', true);     
  }else if (lugarI=='Concesionario') {
    $('.con').prop('disabled', false);
      $('.dom').prop('disabled', true);
      $('.tal').prop('disabled', true);
  }
}
 $(document).ready(function(){  
  $( "input[name='lugarInstacion']" ).on( "click",validar);   
  validar();
});




///////////////////////////////////////////////////

</script>

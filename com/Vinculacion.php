<?php
    $nav="Vinculacion";
    include("../includes/head.php");
    if (!$session->yaEstaIngresado()) {
        irA("../ingresar");
    }
    $obMenu=Usuario::obtenerMenu($session->idusuario);

if (isset($_POST['btnblink'])) {
    if (Token::verificar($_POST['btnblink'])) {
        if(isset($_POST['idlinkHomo'])) {
            $objTramaTY = new TramaIntegracionTY();

            $datosTrama = TramaIntegracionTY::getInfoVinculacionOperacion($_POST['idlinkHomo']);
            $objTramaTY->idoperacion =$datosTrama->idoperacion;
            $objTramaTY->updateVinculacion($_POST['idlinkHomo'],1);
            $objTramaTY->updateVinculacion($datosTrama->idtramavinculacion,16);

            $objCliente = new Cliente();
            $objCliente->tipoCliente=$datosTrama->tipoCliente;
            $objCliente->apellidosC = $datosTrama->apellido_asegurado;
            $objCliente->nombre = $datosTrama->nombre_asegurado;
            $objCliente->razonSocial = $datosTrama->razonSocial;
            $objCliente->ruc = $datosTrama->ruc;
            $objCliente->dni = $datosTrama->dni;
            $objCliente->correo = $datosTrama->correo;
            $objCliente->telefono = $datosTrama->telefono;
            $objCliente->telefono2= $datosTrama->telefono2;
            $objCliente->direccion = $datosTrama->direccion;
            $objCliente->actualizarDatosViculacion($datosTrama->idcliente);

            $objDatosInstalacion =new DatosInstalacion();
            $objDatosInstalacion->cliente = $datosTrama->cliente;
            $objDatosInstalacion->idoperacion = $datosTrama->idoperacion;
            $objDatosInstalacion->actualizarDatosVinculacion();

            $echo[] = "Se  Vinculo con exito ";
        }

    }   else {
        $errores[] = "El formulario ya fue procesado, vericar.";
    }
}



if (isset($_POST['btnblink2'])) {
    if (Token::verificar($_POST['btnblink2'])) {
        if(isset($_POST['idlinkHomo2'])) {
            $objTramaTY = new TramaIntegracionTY();

            $objTramaTY = TramaIntegracionTY::getInfoVinculacionOperacion($_POST['idlinkHomo2']);
            $objTramaTY->idoperacion =$objTramaTY->idoperacion;
            $objTramaTY->updatePreActivacion($_POST['idlinkHomo2']);

            $path =  "/sendStatusVehicle";
            $param = array(
                "chasis" => $objTramaTY->chasis,
                "estado" => "preactivated"
            );
            $rptaService = APIToyota::PostService($path,$param);


            $echo[] = "Se  PreActivo con exito ";
        }

    }   else {
        $errores[] = "El formulario ya fue procesado, vericar.";
    }
}


?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="../assets/css/sweetalert2.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/css2/style2.css">
<link rel="stylesheet" href="../assets/css2/style3.css">

<style>
    .table-cabecera-horario{
        background:#075598!important;
        color:white !important;
        vertical-align: middle!important;
    }
    .table-cabecera-horario2{
        background:#164764 !important;
        color:white !important;
        vertical-align: middle!important;
    }
    .table-cabecera-horario3{
        background: #096452 !important;
        color:white !important;
    }
    .table-cabecera-horario4{
        background: #0e2e79 !important;
        color:white !important;
    }

    .table-scroll {
        position:relative;
        max-width:1600px;
        margin:auto;
        overflow:hidden;
        /*border:.5px solid #000;*/
    }

    .table-wrap {
        width:100%;
        overflow:auto;
    }

    .table-scroll table {
        width:100%;
        margin:auto;
        border-collapse:separate;
        border-spacing:0;
    }

    .table-scroll th, .table-scroll td {
        padding:5px 10px;
        border-bottom:1px solid;
        border-right:1px solid;
        border-color: #DDDDDD;;
        #background:#fff;
        color:rgb(103, 106, 108) ;
        white-space:nowrap;
        vertical-align:top;
        font-size: 12px
    }

    .table-scroll thead, .table-scroll tfoot {
        background: #f9f9f9;
    }

    .bordertab{
        border-bottom: 1px #e1d0d0 solid !important;
        border-left: 1px #e1d0d0 solid !important;
    }
</style>
<body>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <?php include("../includes/menuIzquierdo.php") ?>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../includes/navegacionArriba.php") ?>
            </nav>
        </div>
        <div class="wrapper wrapper-content">
            <div class="row ibox float-e-margins ibox-content">
                <div class="col-md-12" style="padding: 0">
                    <div id="table-scroll" class="table-scroll">
                        <div class="table-wrap">
                            <div class="col-md-2">
                                <label for="cboEstado">Estado</label>
                                <select style="border: none;margin-bottom: 15px" class="border-bottom form-control text-capitalize" required=""  id="cboEstado" name="cboEstado" aria-required="true">
                                    <option value="">Todos</option>
                                    <option value="1">Vinculado</option>
                                    <option value="0" selected>No Vinculado</option>
                                </select>
                            </div>


                            <input type="text" class="form-control input-sm m-b-xs" id="filterrr"
                                   placeholder="Buscar" style="margin-bottom: 15px">
                            <table class="table main-table" >
                                <thead>
                                    <tr>
                                        <th rowspan="2" class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"></th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="5">Datos Pre - Registro</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="5">Datos Vinculación </th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2">Estado</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2">Fecha Vinculacion</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2">Cargo</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Acción</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Asegurado</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">n° Documento</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Chasis</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Placa</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Fecha Registro</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Asegurado</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">n° Documento</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Chasis</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Placa</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Fecha Registro</th>
                                    </tr>
                                </thead>
                                <tbody id="resultado">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("../includes/footer.php") ?>
    </div>
</div>


<div class="modal inmodal" id="vinculacionOModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog " style="width: 95%!important;">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 5px;padding-bottom: 2px">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Vinculación Toyota</h4>
                <small class="font-bold"><div class="form-group">
                    </div></small>
            </div>
            <form  role="form" method="post" enctype="multipart/form-data" id="formOpe" name="formOpe">
                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                    <button type="hidden" style="display: none" name="guardarVinculacion" id="guardarVinculacion" class="btn btn-primary btn-modal" value="<?php echo Token::generar() ?>" >Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/plugins/clockpicker/clockpicker.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/select2/select2.full.min.js"></script>
<script src="../assets/bootbox/bootbox.all.js"></script>

<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="../assets/js/jquery-confirm.min.js"></script>

<script src="../assets/js/plugins/ladda/spin.min.js"></script>
<script src="../assets/js/plugins/ladda/ladda.min.js"></script>
<script src="../assets/js/plugins/ladda/ladda.jquery.min.js"></script>
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>

<script src="../assets/bootbox/bootbox.all.js"></script>
<style>
    .popover {
        z-index: 999999;
    }
</style>
<?php include_once('../includes/script.php') ?>
<script>

    <?php if (!empty($errores)): ?>
    <?php foreach ($errores as $error): ?>
    toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
    <?php endforeach ?>
    <?php endif ?>
    <?php if(!empty($echo)): ?>
    <?php foreach($echo as $ok): ?>
    toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
    <?php endforeach ?>
    <?php endif ?>
</script>
<script>


    function limpiarTrama(){
        var idTrama = $("#idTrama").val('');
    }


    $(document).ready(function(){
        $('.footable').footable();
        $('.footable2').footable();
    });

    $('#vinculacionOModal').on('show.bs.modal', function (e) {
        $('.modal-body2').html("");
        var rowid = $(e.relatedTarget).data('idint');
        $.ajax({
            type : 'post',
            url : 'modal/VinculacioModal',
            data :  'idTrama='+ rowid,
            success : function(data){
                $('.modal-body').html(data);
            }
        });
    });

    $('body').on('hidden.bs.modal', function () {
        if($('.modal.in').length > 0)
        {
            $('body').addClass('modal-open');
        }
    });


    $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
    $.ajax({
        type : 'POST',
        url  : 'pag/resultadoBusquedaVinculacionToyota',
        data :  {datos:'',estd:'0',page:1},
        success : function(data)
        {
            $("#resultado").html(data);
        }
    });

    $("#cboEstado").change(function() {
        $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
        var datosS = $("#filterrr").val();
        datosS = datosS.replace('<>/"', '');
        var estd=$("#cboEstado option:selected").val()
        $.ajax({
            type : 'POST',
            url  : 'pag/resultadoBusquedaVinculacionToyota',
            data :  {datos:datosS,estd:estd},
            success : function(data)
            {
                $("#resultado").html(data);
            }
        });
        return false;

    })



    $("#filterrr").keypress(function(e)
    {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){
            $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
            var datosS = $(this).val();
            datosS = datosS.replace('<>/"', '');
            var estd=$("#cboEstado option:selected").val()
            var sist=$("#cboSistema option:selected").val()
            $.ajax({
                type : 'POST',
                url  : 'pag/resultadoBusquedaVinculacionToyota',
                data :  {datos:datosS,estd:estd},
                success : function(data)
                {
                    $("#resultado").html(data);
                }
            });
            return false;
        }
    });






    function btnLink(e){
        var rowid = $(e).data('id');
        var ndata = $(e).data('hlink');

        var l = $('.ladda-button-dlink'+rowid+'').ladda();
        l.ladda( 'start' );

        bootbox.confirm({
            title: "<h3><strong>Atención!</strong></h3>",
            message: "Esta Seguro que deseas  Vincular  el Chasis    "+ndata+" ?",
            size: 'small',
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancelar'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirmar'
                }
            },
            callback: function (result) {
                if(result){
                    $("#btnblink"+rowid).prop('disabled',false);
                    $("#idlinkHomo"+rowid).prop('disabled',false);
                    $("#btnblink"+rowid).val($("#guardarVinculacion").val());
                    $("#FRMLINK"+rowid).submit();
                    return true;
                }else{
                    l.ladda('stop');
                    //  $("#liberarTec").on("click",$(this)).find("#liberarTec");
                }
            }
        });
        return false;
    };




    function btnLink2(e){
        var rowid = $(e).data('id');
        var ndata = $(e).data('hlink');

        var l = $('.ladda-button-dlink2'+rowid+'').ladda();
        l.ladda( 'start' );

        bootbox.confirm({
            title: "<h3><strong>Atención!</strong></h3>",
            message: "Esta Seguro que deseas  Pre Activar  el Chasis    "+ndata+" ?",
            size: 'small',
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancelar'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirmar'
                }
            },
            callback: function (result) {
                if(result){
                    $("#btnblink2"+rowid).prop('disabled',false);
                    $("#idlinkHomo2"+rowid).prop('disabled',false);
                    $("#btnblink2"+rowid).val($("#guardarVinculacion").val());
                    $("#FRMLINK2"+rowid).submit();
                    return true;
                }else{
                    l.ladda('stop');
                    //  $("#liberarTec").on("click",$(this)).find("#liberarTec");
                }
            }
        });
        return false;
    }




</script>

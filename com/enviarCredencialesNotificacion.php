<?php
require __DIR__ . "/../vendor/autoload.php";
require_once("../includes/init.php");
require_once("templateMail.php");
//require_once("../com/DatosTablasClass.php");
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$datotabla = new DatosTablasClass();
$infoAreas = $datotabla->correoDeAreaPorPermisoAlModulo($session->idusuario);
//var_dump($infoAreas[0]->arearesponsable);
$corredearea = $infoAreas[0]->correoarea;
$arearesponsable = $infoAreas[0]->arearesponsable;
$nombredelcargo = $infoAreas[0]->nombredelcargo;


$NombreCompletoORazonSocial = $_POST['nombre'];
$correodelcliente = $_POST['correo'];
$usuario = $_POST['usuario'];
$contrasena = $_POST['contra'];

//$tipoSERVICIO = $_POST['selectorTipo'];
$tipoSERVICIO = $_POST['sistemaInput'];
//sistemaInput tipoTrackInput
//$tipoTRACK = isset($_POST['tipoTrack']) ? $_POST['tipoTrack'] : NULL;
$tipoTRACK = isset($_POST['tipoTrackInput']) ? $_POST['tipoTrackInput'] : NULL;

$correoCopiaAdicional = $_POST['correoCopiaAdicional'];
//$correoInvisible = $_POST['correoInvisible']; //860rynf48-EZ

if ($NombreCompletoORazonSocial == "" || $correodelcliente == "" || $usuario == "") {

    $arrResponse = array('status' => false, 'response' => 'Debe completar todos los campos obligatorios');
    echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
    die();
}

$asunto = "";
$bodyEmail = '';

if ($tipoSERVICIO == 'TRACK') {
    // if($tipoTRACK == ''){
    //     $arrResponse = array('status' => false, 'response' => 'Debe completar todos los campos obligatorios');
    //     echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
    //     die();
    // }
    if ($tipoTRACK == 'INTERACTIVO') {
        $asunto = 'Usuario y clave de GPS Tracklink'.' '.$tipoTRACK.' '.'-'.' '.$NombreCompletoORazonSocial;
        $bodyEmail = mailInteractivo($NombreCompletoORazonSocial, $usuario, $contrasena);

    } elseif ($tipoTRACK == 'CONTROL TOTAL') {
        $asunto = 'Usuario y clave de GPS Tracklink'.' '.$tipoTRACK.' '.'-'.' '.$NombreCompletoORazonSocial;
        $bodyEmail = mailControlTotal($NombreCompletoORazonSocial, $usuario, $contrasena);
    }
} else {
    $asunto = 'Usuario y clave de GPS Tracklink'.' '.$tipoSERVICIO.' '.'-'.' '.$NombreCompletoORazonSocial;
    $bodyEmail = mailOcto($NombreCompletoORazonSocial, $usuario, $contrasena);
}

$mail = new PHPMailer(true);
try {
    $mail->CharSet = 'UTF-8';
    $mail->isSMTP();
    $mail->Host = MAIL_HOST;
    $mail->SMTPAuth = true;
    $mail->Username = MAIL_USERNAME;
    $mail->Password = MAIL_PASSWORD;
    $mail->SMTPSecure = MAIL_SECURE;
    $mail->Port = MAIL_PORT;

    $mail->addEmbeddedImage('logotrackfinal.png', 'logotrackfinal');
    $mail->addEmbeddedImage('logoapple.png', 'logoapple');
    $mail->addEmbeddedImage('logoplaystore.png', 'logoplaystore');
    $mail->addEmbeddedImage('logoyoutube.png', 'logoyoutube');

    //$mail->setFrom(MAIL_USERNAME, 'CENTRAL DE EMERGENCIAS - TRACKLINK');

    if($nombredelcargo){
        $mail->setFrom($corredearea, $nombredelcargo);
    }
    $mail->addAddress($correodelcliente, $NombreCompletoORazonSocial);
    //$mail->addAddress('ezarate@tracklink.pe');

    // if($correoCopiaAdicional=''){
    //     $mail->addCC('');
    // } else{
    //     $mail->addCC($correoCopiaAdicional);
    // }

    if($correoCopiaAdicional){
        //$mail->addCC($correoCopiaAdicional);
        $mail->addBCC($correoCopiaAdicional);
    }

    //Copia invisible

    //$mail->addBCC('kvega@tracklink.pe');
    $mail->addBCC($corredearea);

    // if($correoInvisible){
    //     $mail->addBCC($correoInvisible);  //860rynf48-EZ
    // }
    //$mail->addCC('kevinwillianvm@gmail.com');
    //$mail->addCC('echahua@tracklink.pe');
    //$mail->addBCC('edwardzte98@gmail.com');
    $mail->isHTML(true);
    $mail->Subject = $asunto;
    $mail->Body = '' . $bodyEmail;

    if ($tipoSERVICIO == 'TRACK') {
        // if($tipoTRACK == ''){
        //     $arrResponse = array('status' => false, 'response' => 'Debe completar todos los campos obligatorios');
        //     echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
        //     die();
        // }
        if ($tipoTRACK == 'INTERACTIVO') {
            $asunto = 'Credenciales de Acceso a plataforma web - Track interactivo';
            $bodyEmail = mailInteractivo($NombreCompletoORazonSocial, $usuario, $contrasena);

            // Adjuntar los archivos solo si es INTERACTIVO
            $mail->addAttachment('MANUAL_INTERACTIVO (17).pdf', 'MANUAL INTERACTIVO.pdf');
            $mail->addAttachment('MANUAL DE INGRESO Y OPERACIÓN DEL SISTEMA DE LOCALIZACIÓN TRACK.pdf', 'MANUAL DE INGRESO Y OPERACIÓN DEL SISTEMA DE LOCALIZACIÓN TRACK.pdf');
        } elseif ($tipoTRACK == 'CONTROL TOTAL') {
            $asunto = 'Credenciales de Acceso a plataforma web - Track Control Total';
            $bodyEmail = mailControlTotal($NombreCompletoORazonSocial, $usuario, $contrasena);
        }
    } else {
        $asunto = 'Tracklink Octo - Credenciales acceso';
        $bodyEmail = mailOcto($NombreCompletoORazonSocial, $usuario, $contrasena);

        // Adjuntar el archivo solo si es OCTO
        $mail->addAttachment('Manual de plataforma Octo.pdf', 'MANUAL DE PLATAFORMA OCTO.pdf');
    }
    //$mail->addAttachment($rutadeldocumento, $nombredeldocumento);

    $mail->send();

    $arrResponse = array('status' => true, 'response' => 'Se envió el correo al cliente.');
} catch (Exception $e) {
    $arrResponse = array('status' => false, 'response' => 'Hubo un error:' . $mail->ErrorInfo);
}
echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
die();
?>

<?php $nav="Seguimiento Comercial" ;?>
<?php include("../includes/head.php") ?>
<link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/css/jquery-confirm.min.css">
<?php if (!$session->yaEstaIngresado()){ 
  irA("ingresar");
} ?>
<?php 
$obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->administrarComercial=="si"): ?> 
<?php 
require_once '../mailphp/PHPMailer/PHPMailerAutoload.php';    
$errores=array();
$echo=array();
$clienteObj=new Cliente();
$vehiculoObj=new Vehiculo();
$facturacionObj=new DatosFacturacion();
$objCantReg=new CantidadRegistroOP();
$operacionesObj=new Operacion();
$objCalendario=new Calendario();
$objCanales= new Canales();
$objComisiones= new Comisiones();
$objServAdicionales=new OperacionServAdicionales();
$mailObj= new PHPMailer;
$datosOld=new SeguimientoConsulta();
if (isset($_POST['actualizarDeOnyx'])){
        //Log::registrarLog("actualizarDeOnyx",$_POST['chasis'].",".$_POST['idfacturacion'].",".$_POST['idvehiculo'].",".$_POST['idcliente'],$_POST,"editado desde administrar comercial");          
        $clienteObjO=Onix::ObtenerDatosDesdeChasis($_POST['chasis']);
        $nombreCl=$clienteObjO->apellidos." ".$clienteObjO->nombre;
        $nombreClA=$clienteObjO->apellidos." ".$clienteObjO->nombre;

        if($clienteObjO->nombre!=''){
            $clienteObj->tipoCliente="Natural";
            $clienteObj->dni=$clienteObjO->dni;
            $clienteObj->nombre=$clienteObjO->nombre;
            $clienteObj->apellidosC=$clienteObjO->apellidos;
        }else{
           $clienteObj->tipoCliente="Empresa";
           $clienteObj->ruc=$clienteObjO->dni;
           $clienteObj->razonSocial=$clienteObjO->apellidos;
        }

        $clienteObj->actualizarDatosDesdeOnyx(limpiar($_POST['idcliente']));

        $vehiculoObj->idvehiculo=limpiar($_POST['idvehiculo']);
        $vehiculoObj->placa=$clienteObjO->placa;
        $vehiculoObj->chasis=$clienteObjO->chasis;
        $placaA='';
        $placaN='';
        $chasisC=true;
        $vehiculoObj->motor=$clienteObjO->motor;
        $vehiculoObj->marca=$clienteObjO->marca;
        $vehiculoObj->modelo=$clienteObjO->modelo;
        $vehiculoObj->color=$clienteObjO->color;
        $vehiculoObj->anio=$clienteObjO->anio;
        $vehiculoObj->actulizarDatosDeVehiculoDeOnyx();

        ob_start();
        include('../assets/email_templates/notifiModal/notifiCoNux.php');
        $mailObj->isSMTP();
        $mailObj->SetLanguage( 'es', $path.'mailphp/PHPMailer/language/' );
        $mailObj->SMTPDebug = 0;
        $mailObj->Debugoutput = 'html';
        $mailObj->Host = gethostbyname('mail.tracklink.pe');
        $mailObj->Port = 25;
        $mailObj->SMTPAuth = false;
        $mailObj->CharSet = 'UTF-8';
        $mailObj->setFrom('resh@tracklink.pe','Resh de Tracklink');  
        $mailObj->addReplyTo("pvasquez@tracklink.pe ", 'Soporte Resh'); 
        $mailObj->addAddress("postventa@tracklink.pe"); 

        $ejecutivoVenta=Usuario::obtenerUsuario($_POST['ejecutivoComercial']);

        $ejecutivoOpec=Usuario::obtenerUsuario($ejecutivoVenta->comercialOperaciones);

        $ejecutivo=$ejecutivoVenta->usuarioNombre." ".$ejecutivoVenta->usuarioApellidoPaterno;
        $ejecutivoOpe=$ejecutivoOpec->usuarioNombre." ".$ejecutivoOpec->usuarioApellidoPaterno;

        $echo[]="Se actualizó desde Onyx";
       //$mailObj->addAddress("pvasquez@tracklink.pe"); 
        $mailObj->addAddress("asistentecomercial@tracklink.pe"); 
        $mailObj->addAddress("cruiz@tracklink.pe"); 
        $mailObj->addAddress($ejecutivoOpec->usuario,$ejecutivoOpe); 
        $mailObj->addCC($ejecutivoVenta->usuario,$ejecutivo);
        $mailObj->addBCC("pruebas-ti@tracklink.pe");
        $mailObj->Subject = 'Informe de actualización de datos desde Onyx : '.$nombreCl;
        $content = ob_get_clean();
        $mailObj->Body = $content;
        $mailObj->IsHTML(true); 
        $mailObj->AltBody = 'Informe de actualización de datos desde Onyx : '.$nombreCl;
        if (!$mailObj->send()) {
            echo "Mailer Error: " . $mailObj->ErrorInfo;
        } else {
        }
}

if (isset($_POST['guardarCambios'])){
    if(Token::verificar($_POST['guardarCambios'])){
    Log::registrarLog("modificacionComAdmin",$_POST['idoperacion'].",".$_POST['idfacturacion'].",".$_POST['idvehiculo'].",".$_POST['idcliente'],$_POST,"editado desde administrar comercial");
      $ultimoid=$datosOld->ctraerultimoseguimiento();
      $idopera=$_POST['idoperacion'];
    if (isset($_POST['eliminarVenta'])) {
        $vehiculoObj->idvehiculo=limpiar($_POST['idvehiculo']);
        $vehiculoObj->obervaciones=limpiar($_POST['motivo']);
        if(!$vehiculoObj->guardarComoEliminado($ultimoid,$idopera)){
            $errores[] = "No se pudo actualizar vehículo ";
        }
        $facturacionObj->datosfacturacionid=$_POST['idfacturacion'];
        $facturacionObj->apunte=limpiar($_POST['motivo']);
        if(!$facturacionObj->marcarComoAnulado($ultimoid,$idopera)){
            $errores[] = "No se pudo actualizar datos de facturación ";
        }

        $objComisiones->observacion=limpiar($_POST['motivo']);
        $objComisiones->idfacturacion=$_POST['idfacturacion'];
        if($objComisiones->verificarSiexiste()){
            if(!$objComisiones->marcarComoAnulado($ultimoid,$idopera)){
                $errores[] = "No se pudo actualizar datos de comision ";
            }
        }
        $operacionesObj->observacion=limpiar($_POST['motivo']);
        $operacionesObj->idoperacion=$_POST['idoperacion'];
        if(!$operacionesObj->marcarComoAnulado($ultimoid,$idopera)){
                $errores[] = "No se pudo actualizar datos de operación ";
        }
        $echo[]='El registro se anulo con exito';
    }else{


            // echo "<pre>";
            // die(var_dump($_FILES));
            // echo "</pre>";

        if (isset($_FILES['voucherssss'])) {
            //die(var_dump($_FILES));
            #echo $_FILES['voucherssss']['size'] ;
            $tamanioArchivo=$_FILES['voucherssss']['size'];
            if($tamanioArchivo>0){
                $extencionesAbilitadas= array('jpg','jpeg','png','gif','PNG','JPG','JPEG','PDF','DOCX','GIF','docx','pdf');
                $nombreArchivo=$_FILES['voucherssss']['name'];        
                $tmp=explode('.', $nombreArchivo);
                $extencionArchivo=end($tmp);
                $ubicacionTemporal=$_FILES['voucherssss']['tmp_name'];
                if (!in_array($extencionArchivo,$extencionesAbilitadas)) {
                    $errores[]='La extencion de la imagen debe ser jpg jpeg png gif';
                }
                if ($tamanioArchivo>5242880) {
                    $errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
                }
                move_uploaded_file($ubicacionTemporal, '../public/vouchers/'.date("YmdHms").$nombreArchivo); 
                ///CAMBIAR EN DB
                $facturacionObj->voucher=date("YmdHms").$nombreArchivo;
                $facturacionObj->datosfacturacionid=$_POST['idfacturacion'];
                if(!$facturacionObj->modificarDesdeAdmimcomVoucher($ultimoid,$idopera)){
                    die("ERROR EN MODIFICAR VOUCHER DESDE ADMIN CONTACTATE CON wilmanpabel@gmail.com");
                }
            }
            ///CAMBIAR EN DB
        }

        $clienteObj->idcliente=$_POST['idcliente'];
        $clienteObj->tipoCliente=$_POST['tipoCliente'];
        $nombreCl='';
        if(isset($_POST['dni'])){
            $clienteObj->dni=$_POST['dni'];
            $clienteObj->nombre=$_POST['nombre'];
            $clienteObj->apellidosC=$_POST['apellidosC'];
            $nombreCl=$_POST['apellidosC']." ".$_POST['nombre'];
        }
        if(isset($_POST['ruc'])){
            $clienteObj->ruc=$_POST['ruc'];
            $clienteObj->razonSocial=$_POST['razonSocial'];
            $nombreCl=$_POST['razonSocial'];
            }
       // $clienteObj->actuDBasicosCliente($_POST['idvehiculo']);
        $VhObj=Vehiculo::obtenerVehiculo($_POST['idvehiculo']);
        $vehiculoObj->idvehiculo=$_POST['idvehiculo'];
        $vehiculoObj->placa=$_POST['placa'];
        $vehiculoObj->chasis=$_POST['chasis'];
        $vehiculoObj->motor=$_POST['motor'];
        $vehiculoObj->marca=$_POST['marca'];
        $vehiculoObj->modelo=$_POST['modelo'];
        $vehiculoObj->color=$_POST['color'];
        $vehiculoObj->anio=$_POST['anio'];
        $vehiculoObj->companiaSeguro=$_POST['seguro'];
        $vehiculoObj->concesionario=$_POST['concesionario'];
        $vehiculoObj->plan=$_POST['plan']; // verificar bn si es q se actualiza
        if (isset($_POST['campana'])) {
            $vehiculoObj->campana=htmlspecialchars($_POST['campana']);
        }else{
            $vehiculoObj->campana='no';
        }
        //$vehiculoObj->campana=$_POST['campana'];

        $vehiculoObj->planTiempo=$_POST['tiempo'];
        if (isset($_POST['campana'])) {
            if ($VhObj->campana != $_POST['campana']) {
                if($VhObj->estado==3 OR $VhObj->estado==4){
                    ob_start();
                    include('../assets/email_templates/notifiModal/notiPost.php');
                    $mailObj->isSMTP();
                    $mailObj->SetLanguage( 'es', $path.'mailphp/PHPMailer/language/' );
                    $mailObj->SMTPDebug = 0;
                    $mailObj->Debugoutput = 'html';
                    $mailObj->Host = gethostbyname('mail.tracklink.pe');
                    $mailObj->Port = 25;
                    $mailObj->SMTPAuth = false;
                    $mailObj->CharSet = 'UTF-8';
                    $mailObj->setFrom('resh@tracklink.pe','Resh de Tracklink');
                    $mailObj->addReplyTo("pvasquez@tracklink.pe ", 'Soporte Resh');
                    //$mailObj->addAddress("pvasquez@tracklink.pe");
                    $mailObj->addAddress("postventa@tracklink.pe");
                    $mailObj->addAddress("asistentecomercial@tracklink.pe");
                    $mailObj->addBCC("pruebas-ti@tracklink.pe");
                    $mailObj->Subject = 'Informe de cambio de campaña del cliente: '.$nombreCl;
                    $content = ob_get_clean();
                    $mailObj->Body = $content;
                    $mailObj->IsHTML(true);
                    $mailObj->AltBody = 'Informe de cambio de campaña del cliente: '.$nombreCl;
                    if (!$mailObj->send()) {
                        echo "Mailer Error: " . $mailObj->ErrorInfo;
                    } else {
                    }
                }
            }
        }
        $vehiculoObj->actulizarDatosDeVehiculoDeAdmin($ultimoid,$idopera);

        //$datosFac=DatosFacturacion::obtenerDatosFacturacionPorId($_POST['idfacturacion']);
        $facturacionObj->datosfacturacionid=$_POST['idfacturacion'];
        $facturacionObj->idcanal=$_POST['idcanal'];

     $facturacionObj->tipoCanal=$_POST['tipoCCnal'];
     $facturacionObj->nombreCanal=$_POST['nombreCCCanl'];
     $facturacionObj->ejecutivoCanal=$_POST['EjecutCCanl'];
     $facturacionObj->oficina=$_POST['tiendaCCT'];


     $facturacionObj->tipoDocumentoFacturacion=$_POST['tipoDocumentoFacturacion'];
     $facturacionObj->razonSocialFacturacion=$_POST['razonSocialFacturacion'];
     $facturacionObj->direccionFiscal=$_POST['direccionFiscalFacturacion'];
     $facturacionObj->telefonoFacturacion=$_POST['telefonoFacturacion'];
     $facturacionObj->dniFacturacion=$_POST['ndocfact'];
     $facturacionObj->rucFacturacion=$_POST['ndocfact'];

     if (!$facturacionObj->actualizarCanales($ultimoid,$idopera)) {
         die("ERROR EN ACTUALIZAR CANAL");
     }
     $facturacionObj->actualizarDatosFactSeguimiento();

        $facturacionObj->plan=$_POST['plan'];
        $facturacionObj->costo=$_POST['precio'];
        $facturacionObj->orden=$_POST['numeroOrden'];
        $facturacionObj->idUsuarioCreado=$_POST['ejecutivoComercial'];
        $facturacionObj->tiempo=$_POST['tiempo'];

        /*inicio correo al cambiar costo y plan
        $DFObj=DatosFacturacion::obtenerDatosFacturacion($_POST['idfacturacion']);
        $dprec='';$dpla='';$unn='';$dorden='';
        if (isset($_POST['plan']) || isset($_POST['precio']) || isset($_POST['numeroOrden'])) {
            if ($DFObj->plan != $_POST['plan']) {
                $dpla='ok';
            }
            if($DFObj->costo !=$_POST['precio']){
                $dprec='ok';
            }
            if($DFObj->orden !=$_POST['numeroOrden']){
                $dorden='ok';
            }
            if($dprec=='ok' &&  $dpla=='ok'){
                $unn=' y ';
            }

            if($dprec=='ok' || $dpla=='ok' || $dorden=='ok') {
                ob_start();
                include('../assets/email_templates/notifiModal/notiPlanPrecio.php');
                $mailObj->isSMTP();
                $mailObj->SetLanguage('es', $path . 'mailphp/PHPMailer/language/');
                $mailObj->SMTPDebug = 0;
                $mailObj->Debugoutput = 'html';
                $mailObj->Host = gethostbyname('mail.tracklink.pe');
                $mailObj->Port = 25;
                $mailObj->SMTPAuth = false;
                $mailObj->CharSet = 'UTF-8';
                $mailObj->setFrom('kcabrera@tracklink.pe', 'Resh de Tracklink');
                $mailObj->addReplyTo(($o->usuario),($o->usuarioNombre." ".$o->usuarioApellidoPaterno));
                //$mailObj->addAddress("pvasquez@tracklink.pe");
                $mailObj->addAddress("contabilidadtrk@tracklink.pe");
                $mailObj->addBCC(($o->usuario),($o->usuarioNombre." ".$o->usuarioApellidoPaterno));
//                 $mailObj->addBCC("pruebas-ti@tracklink.pe");
                $mailObj->Subject = 'Informe cambio de '.($dpla!=''?'Plan':'').($dorden!=''?' Orden':'').$unn.($dprec!=''?' Precio ':'').' del cliente: ' . $nombreCl;
                $content = ob_get_clean();
                $mailObj->Body = $content;
                $mailObj->IsHTML(true);
                $mailObj->AltBody = 'Informe cambio de '.($dpla!=''?'Plan':'').($dorden!=''?' Orden':'').$unn.($dprec!=''?' Precio ':'').' del cliente: ' . $nombreCl;
                if (!$mailObj->send()) {
                    echo "Mailer Error: " . $mailObj->ErrorInfo;
                } else {
                }
            }
        }

        fin correo al cambiar costo y plan */
        $facturacionObj->modificarDesdeAdmimcom($ultimoid,$idopera);
        ///Comisiones //
        $objComisiones->montoTotal=$_POST['precio'];
        $objComisiones->tiempo=$_POST['tiempo'];
        $objComisiones->idCreador=$_POST['ejecutivoComercial'];
        $objComisiones->idfacturacion=$_POST['idfacturacion'];
        if($objComisiones->verificarSiexiste()){
            $objComisiones->actualizarDatosCom($ultimoid,$idopera);
        }else{
            $objCanales->tipoCanal=$_POST['tipoCCnal'];
            $objCanales->nombreCanal=$_POST['nombreCCCanl'];
            $objCanales->ejecutivoCanal=$_POST['EjecutCCanl'];
            $objCanales->idcanal=$_POST['idcanal'];
            $objCanales->modificarCanal($ultimoid,$idopera);
        }
        $operacionesObj->idoperacion=$_POST['idoperacion'];
        $operacionesObj->numeroOrden=$_POST['numeroOrden'];
        $operacionesObj->modificarDesdeAdmi($ultimoid,$idopera);

        $objCantReg->chasis=$_POST['chasis'];
        $objCantReg->tipo=$_POST['tipo'];
        $objCantReg->cantreg=$_POST['cantreg'];
        $objCantReg->modificarCantidad($ultimoid,$idopera);

        $objServAdicionales->idoperacion=$idopera;
        $objServAdicionales->borrarServAdicionales();
        $marca_select = $_POST["servad"];
        foreach($marca_select as $marcas){
            $objServAdicionales->idser=$marcas;
            $objServAdicionales->registrarOperacionServAdicional();
        }

        $echo[]="Se modificaron los campos ";

    }
}else{
    $errores[]="La petición ya fue procesada";
}
    }
?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="../assets/css/sweetalert2.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/css2/style2.css">
<link rel="stylesheet" href="../assets/css2/style3.css">
   <style>
    .widget p {
        display: inline-block;        
    }
    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }
    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }
    .reloj .caja-segundos {
        display: inline-block;
    }
    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }
    .widget p {
        display: inline-block;        
    }
    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }
    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }
    .reloj .caja-segundos {
        display: inline-block;
    }
    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }</style>
    <body>
    <style>
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
}
.pagination1{margin:0;padding:0;}
.pagination1 li{
    display: inline;
    padding: 6px 10px 6px 10px;
    border: 1px solid #ddd;
    margin-right: -1px;
    font: 15px/20px Arial, Helvetica, sans-serif;
    background: #FFFFFF;
    box-shadow: inset 1px 1px 5px #F4F4F4;
}
.pagination1 li a{
    text-decoration:none;
    color: rgb(89, 141, 235);
}
.pagination1 li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination1 li.last {
    border-radius: 0px 5px 5px 0px;
}
.pagination1 li:hover{
    background: #CFF;
}
.pagination1 li.active{
    background: #F0F0F0;
    color: #333;
}
</style>
<div class="se-pre-con"></div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
    <body>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <?php include("../includes/menuIzquierdo.php") ?>
               </div>
           </nav>

           <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>
                </nav>
            </div>

            <div class="wrapper wrapper-content">
                <div class="row ibox float-e-margins ibox-content ">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-9">
                            <div class="row">
                                 <form action="exel/gestionContabilidad" method="POST">
                                    <div class="col-md-4">
                                       <label for="tipoV" class=""></label>
                                         <select style="border: none;"  name="tipoV" class="cd-selec form-control border-bottom" required id="tipoV">
                                            <option value="ventas">Ventas </option>
                                            <option value="citas">Citas </option>
                                        </select> 
                                    </div> 
                                    <div class="col-lg-6">
                                       <label for="rango" class="">Seleccione rango fecha *</label>
                                        <input style="border: none;"  class="cd-select form-control border-bottom" required  type="text" placeholder="Seleccione Rango de fecha" class="form-control" value="" name="fecha" id="rango" autocomplete="off">
                                    </div>
                                    <div class="col-md-2" id="resultado1"> 
                                    </div>    
                        </form>
                            </div>
                    </div>

                    <div class="col-md-3">
                           <div class="widget" style="line-height: 0px;       
                           padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px">
                           <div class="fecha">
                            <p id="diaSemana" class="diaSemana">Martes</p>
                            <p id="dia" class="dia">27</p>
                            <p>de </p>
                            <p id="mes" class="mes">Octubre</p>
                            <p>del </p>
                            <p id="year" class="year">2015</p>
                        </div>
                        <div class="reloj">
                            <p id="horas" class="horas">11</p>
                            <p>:</p>
                            <p id="minutos" class="minutos">48</p>
                            <p>:</p>
                            <div class="caja-segundos">
                                <p id="ampm" class="ampm">AM</p>
                                <p id="segundos" class="segundos">12</p>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    <div class="ibox-content" id="resultados">
        <input type="text" class="form-control input-sm m-b-xs" id="filter"
        placeholder="Buscar">
        <div class="table-responsive">
        <table class=" table  table-hover" >
            <thead>
                <tr>
                    <th>Tipo Servicio</th>
                    <th>CLiente</th>
                    <th>Vehículo</th>
                    <th>Fecha registro</th>
                    <th>Fecha Programada</th>
                    <th>Vehículo | Facturación</th>
                    <th>Ejecutivo</th>
                    <th>Canal</th>
                    <th>Accción</th>
                </tr>
            </thead>  
             <tbody  id="resultado">          
            </tbody>  
        </table>
        </div>
    </div>
</div>
<?php include("../includes/footer.php") ?>
</div>
</div>
</div>

<div class="modal inmodal" id="bateriasOModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" style="overflow-y: scroll;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                <h4 class="modal-title">Administrar Comercial</h4>
                <small class="font-bold"><div class="form-group ">
                </div></small>
            </div>
            <form  role="form" method="post" enctype="multipart/form-data">

                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>

            </form>
        </div>
    </div>
</div>

<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/clipboard.min.js"></script>
<script src="../assets/calendario/dist/sweetalert.min.js"></script>
    <!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
 <script src="../assets/js/jquery-confirm.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<?php include_once('../includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>

<script>
$('#rango').daterangepicker({
    format: 'DD-MM-YYYY',
    endDate: moment(),
    showDropdowns: true,
    showWeekNumbers: true,
    timePicker: false,
    timePickerIncrement: 1,
    timePicker12Hour: false,
    ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
        'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Anterior Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    opens: 'right',
    drops: 'down',
    buttonClasses: ['btn', 'btn-sm'],
    applyClass: 'btn-primary',
    cancelClass: 'btn-default',
    separator: '/',
    locale: {
        applyLabel: 'Seleccionar',
        cancelLabel: 'Cancelar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Seleccionar Rango',
        daysOfWeek: ['Do', 'Lun', 'Mar', 'Mi', 'Ju', 'Vi','Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
    }
}, function(start, end, label) {               
     $('#rango').focus();
});        


$(document).ready(function(){


 var tipoV=$("#tipoV").val();
    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionSeguimiento',
        data : {tipoV:tipoV},
        success : function(data)
        {
         $("#resultado").html(data);
        }
      });
    $( "#tipoV" ).change(function() { 
        var tipoV = $(this).val(); 
        var fecha=$("#rango").val();      
        var page = $('#resultado').attr("data-page"); 
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionSeguimiento',
        data : {tipoV:tipoV,page:page,fecha:fecha},
        success : function(data)
        {
         $("#resultado").html(data);
        }
      });
    });
     $("#rango").bind("keyup focus",function()
    {
        var fecha=$(this).val();
        var page = $("#resultado").attr("data-page"); 
        var tipoV=$("#tipoV").val();        
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionSeguimiento',
        data : {tipoV:tipoV,page:page,fecha:fecha},
        success : function(data)
        {
         $("#resultado").html(data);
        }
        });
    });
    $( "#rango" ).change(function() { 
        var fecha=$(this).val();
        var page = $("#resultado").attr("data-page"); 
        var tipoV=$("#tipoV").val();        
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionSeguimiento',
        data : {tipoV:tipoV,page:page,fecha:fecha},
        success : function(data)
        {
         $("#resultado").html(data);
        }
        });
    });
     
    $("#resultado").on( "click", ".pagination1 a", function (e){
        e.preventDefault();
        var fecha=$("#rango").val();
        var tipoV=$("#tipoV").val();        
        var page = $(this).attr("data-page"); 
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionSeguimiento',
        data : {tipoV:tipoV,page:page,fecha:fecha},
        success : function(data)
        {
         $("#resultado").html(data);
        }
        });

    });


$("#tipoV").change(function()
    {
        var tipoV = $(this).val();  
        var fecha = $("#rango").val();
        $.ajax({
          type : 'POST',
          url  : 'pag/opeCreporte',          
          data : {fecha:fecha,tipoV:tipoV},
          success : function(data)
          {
           $("#resultado1").html(data);
         }
       });
        return false;
});

$("#filter").keypress(function(e)
    {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){

        var datosS = $(this).val();
        var tipoV=$("#tipoV").val();        
        var fecha=$("#rango").val();
        datosS = datosS.replace('<>/"', '');
        if(datosS.length >= 1)
            {
        $.ajax({
                type : 'POST',
                url  : 'pag/resultadoBusquedaSeguimiento',
                data :  {tipoV:tipoV,fecha:fecha,datos:datosS},
                success : function(data)
                {
                    $("#resultado").html(data);
                }
            });
            return false;
        }else{
            var tipoV=$("#tipoV").val();        
            var fecha=$("#rango").val();
            var page = $("#resultado").attr("data-page");
            $.ajax({
            type : 'POST',
            url  : 'pag/paginacionSeguimiento',
            data : {tipoV:tipoV,page:page,fecha:fecha},
            success : function(data)
            {
             $("#resultado").html(data);
            }
            });
        }
        }
  });
});

$('#bateriasOModal').on('show.bs.modal', function (e) {
    var rowid = $(e.relatedTarget).data('id');
    if(rowid.indexOf(',') >= 0){
        $.ajax({
                type : 'post',
                url : 'modal/ventasModalSegmodal', 
                data :  'rowid='+ rowid,
                success : function(data){
                $('.modal-body').html(data);
            }
        });
    }else if(rowid.indexOf('.') >= 0){
            $.ajax({
                type : 'post',
                url : 'modal/citaModalSegmodal', 
                data :  'rowid='+ rowid,
                success : function(data){
                $('.modal-body').html(data);
            }
        });
    }
});

(function(){
    var actualizarHora = function(){
        var fecha = new Date(),
        horas = fecha.getHours(),
        ampm,
        minutos = fecha.getMinutes(),
        segundos = fecha.getSeconds(),
        diaSemana = fecha.getDay(),
        dia = fecha.getDate(),
        mes = fecha.getMonth(),
        year = fecha.getFullYear();
        var pHoras = document.getElementById('horas'),
        pAMPM = document.getElementById('ampm'),
        pMinutos = document.getElementById('minutos'),
        pSegundos = document.getElementById('segundos'),
        pDiaSemana = document.getElementById('diaSemana'),
        pDia = document.getElementById('dia'),
        pMes = document.getElementById('mes'),
        pYear = document.getElementById('year');
        var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        pDiaSemana.textContent = semana[diaSemana];
        pDia.textContent = dia;
        var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        pMes.textContent = meses[mes];
        pYear.textContent = year;
        if (horas >= 12) {
            horas = horas - 12;
            ampm = 'PM';
        } else {
            ampm = 'AM';
        }
        if (horas == 0 ){
            horas = 12;
        }
        pHoras.textContent = horas;
        pAMPM.textContent = ampm;
        if (minutos < 10){ minutos = "0" + minutos; }
        if (segundos < 10){ segundos = "0" + segundos; }
        pMinutos.textContent = minutos;
        pSegundos.textContent = segundos;
    };

    actualizarHora();
    var intervalo = setInterval(actualizarHora, 1000);
}())
</script>
  <script>
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
<?php else: ?> 
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?> 
<?php endif ?>

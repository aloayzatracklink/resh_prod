<?php $nav = "enviarcredenciales"; ?>
<?php require("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
}

$o2 = Usuario::obtenerUsuario($session->idusuario);
//Datos del ejecutivo
//$areaEjecutivo = $tablas->datosAreas($o2->idrol);

?>
<?php $obMenu = Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->enviarcredenciales == 'si'): ?>
    <link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
    <link href="../assets/calendario/fullcalendar.min.css" rel="stylesheet">
    <link href="../assets/calendario/fullcalendar.print.min.css" rel='stylesheet' media='print'>
    <link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
    <link href='../assets/calendario/scheduler.min.css' rel='stylesheet' />
    <link rel="stylesheet" href="../assets/css/jquery-confirm.min.css">
    <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
    <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!-- <link href="../assets/css/sweetalert2.css" rel="stylesheet"> -->
    <link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .widget p {
            display: inline-block;
        }

        .fecha {
            font-family: Helvetica, Arial;
            text-align: center;
            font-size: 14px;
        }

        .reloj {
            font-family: Helvetica, Arial;
            font-size: 23px;
            text-align: center;
            margin-top: 6px;
        }

        .reloj .caja-segundos {
            display: inline-block;
        }

        .reloj .segundos,
        .reloj .ampm {
            font-size: 10px;
            display: block;
        }

        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
        }

        .pagination1 {
            margin: 0;
            padding: 0;
        }

        .pagination1 li {
            display: inline;
            padding: 6px 10px 6px 10px;
            border: 1px solid #ddd;
            margin-right: -1px;
            font: 15px/20px Arial, Helvetica, sans-serif;
            background: #FFFFFF;
            box-shadow: inset 1px 1px 5px #F4F4F4;
        }

        .pagination1 li a {
            text-decoration: none;
            color: rgb(89, 141, 235);
        }

        .pagination1 li.first {
            border-radius: 5px 0px 0px 5px;
        }

        .pagination1 li.last {
            border-radius: 0px 5px 5px 0px;
        }

        .pagination1 li:hover {
            background: #CFF;
        }

        .pagination1 li.active {
            background: #F0F0F0;
            color: #333;
        }




        .lds-spinner {
            color: official;
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
        }

        .lds-spinner div {
            transform-origin: 40px 40px;
            animation: lds-spinner 1.2s linear infinite;
        }

        .lds-spinner div:after {
            content: " ";
            display: block;
            position: absolute;
            top: 3px;
            left: 37px;
            width: 6px;
            height: 18px;
            border-radius: 20%;
            background: #2a986f;
        }

        .lds-spinner div:nth-child(1) {
            transform: rotate(0deg);
            animation-delay: -1.1s;
        }

        .lds-spinner div:nth-child(2) {
            transform: rotate(30deg);
            animation-delay: -1s;
        }

        .lds-spinner div:nth-child(3) {
            transform: rotate(60deg);
            animation-delay: -0.9s;
        }

        .lds-spinner div:nth-child(4) {
            transform: rotate(90deg);
            animation-delay: -0.8s;
        }

        .lds-spinner div:nth-child(5) {
            transform: rotate(120deg);
            animation-delay: -0.7s;
        }

        .lds-spinner div:nth-child(6) {
            transform: rotate(150deg);
            animation-delay: -0.6s;
        }

        .lds-spinner div:nth-child(7) {
            transform: rotate(180deg);
            animation-delay: -0.5s;
        }

        .lds-spinner div:nth-child(8) {
            transform: rotate(210deg);
            animation-delay: -0.4s;
        }

        .lds-spinner div:nth-child(9) {
            transform: rotate(240deg);
            animation-delay: -0.3s;
        }

        .lds-spinner div:nth-child(10) {
            transform: rotate(270deg);
            animation-delay: -0.2s;
        }

        .lds-spinner div:nth-child(11) {
            transform: rotate(300deg);
            animation-delay: -0.1s;
        }

        .lds-spinner div:nth-child(12) {
            transform: rotate(330deg);
            animation-delay: 0s;
        }

        @keyframes lds-spinner {
            0% {
                opacity: 1;
            }

            100% {
                opacity: 0;
            }
        }

        .box-spinner {
            padding-top: 3rem;
            padding-bottom: 3rem;
        }

        .custom-button {
            margin-left: 60px;
            margin-top: 24px;
        }

        .card-title {
            /*font-family: 'Lora', serif;*/
            font-weight: bold;
        }
    </style>

    <body>
        <div class="se-pre-con"></div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <?php include("../includes/menuIzquierdo.php") ?>
                </div>
            </nav>
            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <?php include("../includes/navegacionArriba.php") ?>
                    </nav>
                </div>
                <div class="wrapper wrapper-content">
                    <div class="row ibox float-e-margins ibox-content">
                        <div class="col-lg-12 col-md-12">
                            <div class="row" style="border-bottom: 1px solid #e7eaec; margin-bottom:10px;">
                                <div class="col-lg-6 col-md-6">
                                    <h2 class="card-title">ENVIAR CREDENCIALES</h2>
                                </div>
                            </div>
                        </div>
                    </div>

                    <head>

                        <div class="wrapper wrapper-content">
                            <div class="row ibox float-e-margins ibox-content">
                                <div class="col-lg-12 col-md-12">
                                    <form id="formulariodeenvio">
                                        <div class="row" style="border-bottom: 1px solid #e7eaec; margin-bottom:10px;">

                                            <div class="col-lg-12" id="contenedorCampos" style="display: ;">
                                                <div class="input-group" style="margin-bottom:20px; margin-top:20px;">
                                                    <label for="buscadorInput">DNI o RUC:</label>
                                                    <input type="text" id="buscadorInput" name="buscadorInput"
                                                        class="form-control" placeholder="Ingrese DNI o RUC"
                                                        oninput="this.value = this.value.replace(/[^0-9]/g, '')">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-primary custom-button" type="button"
                                                            onclick="buscarUsuario()">Buscar</button>
                                                    </span>
                                                </div>
                                            </div>
                                            <div>

                                                <!-- A partir de aquí, el código del módulo -->
                                                <!-- <div class="col-lg-12">
                                                <label for="selectorTipo">SELECCIONAR SISTEMA:</label>
                                                <select id="selectorTipo" name="selectorTipo" class="form-control"
                                                    onchange="mostrarCampos(this.value)">
                                                    <option value="SELECCIONAR">SELECCIONAR</option>
                                                    <option value="TRACK">TRACK</option>
                                                    <option value="OCTO">OCTO</option>
                                                </select> -->

                                                <!-- Agregar espacio etre select :v -->
                                                <div class="col-lg-12" style="margin-bottom: 10px;"></div>

                                                <!-- Aquí añadimos el nuevo select que solo aparecerá cuando se elija TRACK -->
                                                <!-- <div class="col-lg-12" id="selectorTrackTipo" style="display: none;">
                                                    <label for="tipoTrack">TIPO DE TRACK:</label>
                                                    <select id="tipoTrack" name="tipoTrack" class="form-control">
                                                        <option disabled selected>...</option>
                                                        <option value="INTERACTIVO">INTERACTIVO</option>
                                                        <option value="CONTROL TOTAL">CONTROL TOTAL</option>
                                                    </select>
                                                </div> -->


                                                <!-- </div> -->
                                                <div class="col-lg-12" style="margin-bottom:20px;">
                                                    <label for="selectChasisPlaca">Chasis o Placa:</label>
                                                    <select id="selectChasisPlaca" name="selectChasisPlaca"
                                                        class="form-control"></select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="sistemaInput">Sistema:</label>
                                                        <input type="text" id="sistemaInput" name="sistemaInput"
                                                            class="form-control" readonly
                                                            style="background-color: {{sistemaColor}}; font-weight: bold;">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="tipoTrackInput" id="labelTipoTrack"
                                                            name="labelTipoTrack">Tipo Track:</label>
                                                        <input type="text" id="tipoTrackInput" name="tipoTrackInput"
                                                            class="form-control" readonly
                                                            style="background-color: {{tipoTrackColor}}; font-weight: bold;">
                                                    </div>
                                                </div>
                                            </div>

                                            <div>
                                                <!-- <select class="js-example-basic-single" name="state">
                                                        <option value="AL">Alabama</option>
                                                        ...
                                                        <option value="WY">Wyoming</option>
                                                    </select> -->

                                                <!-- <div class="col-lg-12">
                                                        <label for="chasisyplaca">Chasis y placa:</label>
                                                        <select  class="js-example-basic-single" id="chasisyplaca" name="chasisyplaca">
                                                        </select>
                                                    </div> -->


                                                <div class="col-lg-12" style="margin-bottom: 10px;"></div>


                                                <label for=" nombre">Nombre completo o razón social:</label>
                                                <input type="text" id="nombre" name="nombre" class="form-control" readonly>
                                                <label for="correo">Correo:</label>
                                                <input type="text" id="correo" name="correo" class="form-control" readonly>
                                                <label for="usuario">Usuario:</label>
                                                <input type="text" id="usuario" name="usuario" class="form-control"
                                                    readonly>
                                                <label for="contra" id="labelContra">Contraseña:</label>
                                                <input type="password" id="contra" name="contra" class="form-control"
                                                    readonly>
                                                <label for="correoCopiaAdicional">Correo copia adicional
                                                    (Opcional):</label>
                                                <input type="text" id="contra" name="correoCopiaAdicional"
                                                    class="form-control"
                                                    placeholder="Ingrese un correo adicional para que reciba una copia">
                                                <!-- <label for="correoInvisible">Copia invisible:</label>
                                                <input type="text" id="correoInvisible" name="correoInvisible"
                                                    class="form-control"
                                                    placeholder="Ingrese un correo adicional para que reciba una copia invisible"> -->
                                            </div>
                                            <div>
                                                <div class="col-lg-12" style="margin-bottom: 10px;"></div>
                                                <button type='button' class="btn btn-secondary"
                                                    style="background-color: #f44336; color: white;"
                                                    onclick="limpiarCampos()">LIMPIAR</button>
                                                <button type='submit' class="btn btn-primary">ENVIAR</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
                        <!-- Añadimos el JavaScript para manejar la interacción -->
                        <script>

                            function limpiarCampos() {
                                $("#buscadorInput").val(""); // Limpia el campo de DNI o RUC
                                $("#nombre").val("");
                                $("#correo").val("");
                                $("#usuario").val("");
                                $("#contra").val("");
                                $("#sistemaInput").val("").css("background-color", "").css("font-weight", ""); // Limpia y restablece estilo del campo sistema
                                $("#tipoTrackInput").val("").css("background-color", "").css("font-weight", ""); // Limpia y restablece estilo del campo tipoTrack
                                // Limpia el select sin desencadenar el evento change
                                $("#selectChasisPlaca").val(null).select2().off("change");
                                // Vuelve a agregar el evento change después de un breve período para evitar conflictos
                                setTimeout(() => {
                                    $("#selectChasisPlaca").on('change', function (e) {
                                        // Resto de tu lógica del evento change aquí...
                                    });
                                }, 100);
                            }

                            // Variables globales para almacenar datos seleccionados
                            let selectedChasis = "";
                            let selectedPlaca = "";
                            let selectedIdCustomer = "";
                            let selectedIdVehicle = "";

                            // Función para ocultar el campo "Tipo Track" al cargar el módulo
                            function ocultarTipoTrack() {
                                $("#labelTipoTrack").hide();
                                $("#tipoTrackInput").hide();
                            }

                            ocultarTipoTrack();

                            function buscarUsuario() {

                                let dni_ruc = $("#buscadorInput").val();

                                if (!dni_ruc) {
                                    Swal.fire({
                                        icon: 'warning',
                                        title: 'Oops...',
                                        text: 'Por favor, ingrese un DNI o RUC.',
                                    });
                                    return;
                                }

                                // Limpiamos todos los campos al inicio de la función si el campo no está vacío
                                $("#nombre").val("");
                                $("#correo").val("");
                                $("#usuario").val("");
                                $("#contra").val("");
                                $("#sistemaInput").val("").css("background-color", "").css("font-weight", ""); // Limpia y restablece estilo del campo sistema
                                $("#tipoTrackInput").val("").css("background-color", "").css("font-weight", ""); // Limpia y restablece estilo del campo tipoTrack
                                // Limpia el select sin desencadenar el evento change
                                $("#selectChasisPlaca").val(null).select2().off("change");
                                // Vuelve a agregar el evento change después de un breve período para evitar conflictos
                                setTimeout(() => {
                                    $("#selectChasisPlaca").on('change', function (e) {
                                        // Resto de tu lógica del evento change aquí...
                                    });
                                }, 100);

                                const data = new FormData();
                                data.append('txtdni', dni_ruc);

                                fetch(`credencialesChasisYPlaca.php?txtdni=${dni_ruc}`, {
                                    method: "GET",
                                })
                                    .then((response) => response.json())
                                    .catch((error) => console.error('Error al procesar la respuesta JSON:', error))
                                    .then((response) => {
                                        if (!response.error) {
                                            $("#selectChasisPlaca").empty();
                                            $("#selectChasisPlaca").select2();

                                            $("#selectChasisPlaca").append($('<option>', {
                                                value: '',
                                                text: 'Seleccione el chasis...'
                                            }));

                                            if (response.allChasis.length > 0) {
                                                response.allChasis.forEach((chasis, index) => {
                                                    let option = $("<option/>", {
                                                        value: chasis,
                                                        text: `${chasis} - ${response.allPlacas[index]}`
                                                    });
                                                    option.data('idcustomer', response.allidcustomer[index]);
                                                    option.data('idvehicle', response.allidvehicle[index]);
                                                    $("#selectChasisPlaca").append(option);
                                                });

                                                $("#selectChasisPlaca").on('change', function (e) {
                                                    let selectedOption = $(e.currentTarget).val();
                                                    selectedIdCustomer = $(e.currentTarget).find(':selected').data('idcustomer');
                                                    selectedIdVehicle = $(e.currentTarget).find(':selected').data('idvehicle');

                                                    // Llamada a consultacredenciales.php con los nuevos parámetros
                                                    fetch(`consultacredenciales.php?selectedIdCustomer=${selectedIdCustomer}&selectedIdVehicle=${selectedIdVehicle}`, {
                                                        method: "GET",
                                                    })
                                                        .then((response) => response.json())
                                                        .catch((error) => console.error('Error al procesar la respuesta JSON:', error))
                                                        .then((response) => {
                                                            if (response.error) {
                                                                Swal.fire({
                                                                    icon: 'warning',
                                                                    title: 'Oops...',
                                                                    text: 'No se encontraron datos para el DNI o RUC proporcionado.'
                                                                });
                                                            } else {
                                                                // Mostramos los datos de consultacredenciales.php
                                                                $("#sistemaInput").val(response.sistema).css("background-color", getColor(response.sistema));
                                                                $("#tipoTrackInput").val(response.tipoTrack).css("background-color", getColor(response.tipoTrack));
                                                                $("#nombre").val(response.cliente);
                                                                $("#correo").val(response.correo);
                                                                $("#usuario").val(response.usuario);
                                                                $("#contra").val(response.contra);

                                                                // Verificamos si el campo correo está vacío
                                                                if (!response.correo) {
                                                                    Swal.fire({
                                                                        icon: 'error',
                                                                        title: 'Oops...',
                                                                        text: 'Este usuario está mal registrado, no posee correo.'
                                                                    }).then(() => {
                                                                        // Reiniciamos el módulo
                                                                        limpiarCampos();
                                                                    });
                                                                } else {
                                                                    // Ocultamos o mostramos los campos según el sistema
                                                                    if (response.sistema === 'OCTO') {
                                                                        $("#labelContra").show();
                                                                        $("#contra").show();
                                                                        $("#contra").val('').prop('readonly', false);
                                                                        $("#labelTipoTrack").hide();  //caleta:V
                                                                        $("#tipoTrackInput").hide();
                                                                        $("#usuario").val('').prop('readonly', false);
                                                                    } else {
                                                                        $("#labelContra").show();
                                                                        $("#contra").show();

                                                                        // Mostramos el campo "Tipo Track" solo cuando el sistema es TRACK
                                                                        if (response.sistema === 'TRACK') {
                                                                            $("#labelTipoTrack").show();
                                                                            $("#tipoTrackInput").show();
                                                                        } else {
                                                                            $("#labelTipoTrack").hide();
                                                                            $("#tipoTrackInput").hide();
                                                                        }

                                                                        $("#usuario").val(response.usuario).prop('readonly', true);
                                                                    }
                                                                }


                                                            }
                                                        });
                                                });
                                            } else {
                                                Swal.fire({
                                                    icon: 'info',
                                                    title: 'Información',
                                                    text: 'No hay chasis o placas asociadas.'
                                                });
                                            }

                                        }

                                    });
                            }

                            // Función para obtener el color en función del valor
                            function getColor(value) {
                                switch (value) {
                                    case 'TRACK':
                                        return 'LightSkyBlue';
                                    case 'OCTO':
                                        return '#FFD1DC';
                                    case 'INTERACTIVO':
                                        return '#FFFF99';
                                    case 'CONTROL TOTAL':
                                        return '#7FFFD4';
                                    default:
                                        return '';
                                }
                            }
                        </script>

                        <!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> -->

                        <script>
                            const miformulario = document.getElementById("formulariodeenvio");
                            miformulario.addEventListener("submit", (e) => {
                                e.preventDefault();
                                //Inhabilitar botón y mostrar loading (ícono)
                                Swal.fire({

                                    html:
                                        '<div class="box-spinner"><div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>',
                                    showConfirmButton: false,
                                    allowOutsideClick: false,
                                });

                                console.log('Envío de formulario');
                                const data = new FormData(miformulario);

                                fetch("enviarCredencialesNotificacion", {
                                    method: 'post',
                                    body: data
                                }).then((res) => res.json())
                                    .catch((error) => console.error("Error:", error))
                                    .then((response) => {

                                        if (!response.status) {
                                            Swal.fire({
                                                icon: 'warning',
                                                title: 'Oops...',
                                                text: response.response,
                                            });

                                            //quitar carga y habilitar botón

                                            return false;
                                        }

                                        Swal.fire({
                                            icon: 'success',
                                            title: '¡MUY BIEN!',
                                            text: response.response,

                                            confirmButtonText: 'OK',

                                        }).then((result) => {
                                            /* Read more about isConfirmed, isDenied below */
                                            if (result.isConfirmed) {
                                                window.location.reload();
                                            }
                                        });
                                    }
                                    );

                            });

                        </script>

                </div>
                <?php include("../includes/footer.php") ?>
            </div>
        </div>
    </body>

    <!-- LOS MODALES AQUI  -->
    <!-- <button class='btn' data-clipboard-action='copy' data-clipboard-target='p'>Copy</button> -->
    <script src="../assets/js/jquery-2.1.1.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="../assets/js/inspinia.js"></script>
    <script src="../assets/js/plugins/pace/pace.min.js"></script>
    <script src="../assets/js/plugins/footable/footable.all.min.js"></script>
    <script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <script src="../assets/js/clipboard.min.js"></script>
    <script src="../assets/calendario/dist/sweetalert.min.js"></script>

    <!-- Toastr -->
    <script src="../assets/js/plugins/toastr/toastr.min.js"></script>
    <script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
    <script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="../assets/js/jquery-confirm.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <!-- <?php include_once('../includes/script.php') ?>  -->

    <script>
        $(window).load(function () {
            $(".se-pre-con").fadeOut("slow");;
        });
    </script>
<?php else: ?>
    <?php $session->salir();
    echo "NO NO NO NO ;"
        ?>
<?php endif ?>
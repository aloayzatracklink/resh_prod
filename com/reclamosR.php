<?php $nav="reclamosR" ?>
<?php require("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}

$o2=Usuario::obtenerUsuario($session->idusuario);

?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->reclamosClientes=="si"): ?>
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<link href="../assets/calendario/fullcalendar.min.css" rel="stylesheet">
<link href="../assets/calendario/fullcalendar.print.min.css" rel='stylesheet' media='print'>
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<link href='../assets/calendario/scheduler.min.css' rel='stylesheet' />
<link rel="stylesheet" href="../assets/css/jquery-confirm.min.css">
   <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
   <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
   <link href="../assets/css/sweetalert2.css" rel="stylesheet">
   <link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
   <style>
    .widget p {
        display: inline-block;        
    }

    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }

    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }
    .reloj .caja-segundos {
        display: inline-block;
    }
    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
}
.pagination1{margin:0;padding:0;}
.pagination1 li{
    display: inline;
    padding: 6px 10px 6px 10px;
    border: 1px solid #ddd;
    margin-right: -1px;
    font: 15px/20px Arial, Helvetica, sans-serif;
    background: #FFFFFF;
    box-shadow: inset 1px 1px 5px #F4F4F4;
}
.pagination1 li a{
    text-decoration:none;
    color: rgb(89, 141, 235);
}
.pagination1 li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination1 li.last {
    border-radius: 0px 5px 5px 0px;
}
.pagination1 li:hover{
    background: #CFF;
}
.pagination1 li.active{
    background: #F0F0F0;
    color: #333;
}
</style>
 <body>
<div class="se-pre-con"></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <?php include("../includes/menuIzquierdo.php") ?>
               </div>
           </nav>
           <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>
                </nav>
            </div>
            <div class="wrapper wrapper-content">
                <div class="row ibox float-e-margins ibox-content">
                    <div class="col-lg-12 col-md-12">
                        <div class="row" style="border-bottom: 1px solid #e7eaec; margin-bottom:10px;">
                            <div class="col-lg-6 col-md-6">
                                <h2 class"card-title">Reclamos clientes</h2>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="widget" style="line-height: 0px; padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px">
                                    <div class="fecha">
                                        <p id="diaSemana" class="diaSemana">Martes</p>
                                        <p id="dia" class="dia">27</p>
                                        <p>de </p>
                                        <p id="mes" class="mes">Octubre</p>
                                        <p>del </p>
                                        <p id="year" class="year">2015</p>
                                    </div>
                                    <div class="reloj">
                                        <p id="horas" class="horas">11</p>
                                        <p>:</p>
                                        <p id="minutos" class="minutos">48</p>
                                        <p>:</p>
                                        <div class="caja-segundos">
                                            <p id="ampm" class="ampm">AM</p>
                                            <p id="segundos" class="segundos">12</p>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <form action="PHPExel/TramaReclamosClientesReporte" method="POST">
                                <div class="col-lg-4">
                                    <label for="convenio" class="">Seleccione rango fecha Recibida *</label>
                                    <input style="border: none;"  class="cd-select form-control border-bottom" required  type="text" placeholder="Seleccione Rango de fecha" class="form-control" value="" name="fecha" id="rango" autocomplete="off">
                                </div>
                                <div class="col-md-2" id="resultado1">
                                    <button class="btn btn-primary" name="Descargar" id="resultado1">Descargar Trama en Excel</button>
                                </div>
                            </form>
                            <div class="col-md-offset-1 col-md-4 ">
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <br>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalCrud">Nuevo</button>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalImportFile">Importar (*.xlsx)</button>
                        </div>
                    </div>
                </div>
        <div class="ibox-content" id="resultados">
            <!-- <input type="text" autofocus class="form-control findinoutadfasfd input-sm  m-b-xs" id="filter"
            placeholder="Buscar" autofocus=""> -->
            <div class="table-responsive">
            <table class="table table-condensed  table-hover" >
                <thead>
                    <tr>                   
                    <th>#</th>
                    <th class="text-center">Nro de Reclamo</th>
                    <th class="text-center">Fecha de reclamo</th>
                    <th class="text-center">Canal del reclamo</th>
                    <th class="text-center">Nivel de atención</th>
                    <th class="text-center">Área que detecta</th>
                    <th class="text-center">Ejecutivo responsable</th>
                    <th class="text-center">Área responsable</th>
                    <th class="text-center">Cliente</th>
                    <th class="text-center">Chasis</th>
                    <th class="text-center">Plazo del reclamo (07 Dìas Hábiles)</th>
                    <th class="text-center">Motivo del reclamo</th>
                    <th class="text-center">Información brindada</th>
                    <th class="text-center">Persona a cargo del reclamo</th>
                    <th class="text-center">Solicitud del reclamo</th>
                    <th class="text-center">Resolución del reclamo</th>
                    <th class="text-center">Estado</th>
                    <th class="text-center"></th>
                    </tr>
                </thead>  
                <tbody  id="resultado">          
                </tbody>  
            </table>
            </div>
        </div>

        <div class="modal inmodal" id="modalCrud" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated bounceIn">
                    <div class="modal-header" style="padding-top: 5px;padding-bottom: 5px">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                        <h4 class="modal-title text-left" style="font-size:20px;">Nuevo Reclamo</h4>
                    </div>
                    <form  role="form" id='reclamoForm' method="POST">
                        <div class="modal-body" style="background: #fff;padding-top: 0">
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal" id="guardarBtn">Cancelar</button>
                            <div id="loadingIndicator" style="display: none;">
                                Cargando...
                            </div>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <input type="hidden" name="hdSecuritySave" value="<?php echo Token::generar() ?>" />
                        </div>
                    </form>   
                </div>
            </div>
        </div>

        <div class="modal inmodal" id="modalClientes" tabindex="1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated bounceIn">
                    <div class="modal-header" style="padding-top: 5px;padding-bottom: 5px">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                        <h4 class="modal-title text-left" style="font-size:20px;">Buscar cliente</h4>
                    </div>
                    <form  role="form" method="post" id='reclamoForm' method="POST">
                        <div class="modal-body" style="background: #fff;padding-top: 0">
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>   
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalImportFile" tabindex="1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated bounceIn">
                    <div class="modal-header" style="padding-top: 5px;padding-bottom: 5px">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                        <h4 class="modal-title text-left" style="font-size:20px;">Importar desde archivo</h4>
                    </div>
                    <div  id="f1_upload_process" class="text-center" style="height:200px; display:none;">
                        <span style="position: relative;display: block;"><img src="../../assets/img/Preloader_6.gif" /></span>
                    </div>
                    <form  role="form" action="PHPExel/importReclamosClientesReport" id='importForm' method="POST" enctype="multipart/form-data" target="upload_target" onsubmit="startUpload();">
                        <!-- <div id="f1_upload_process" style="height: 100%; width: 100%; z-index:2; position:relative;"><img src="../../assets/img/Preloader_6.gif" /></div> -->
                        <div class="modal-body" style="background: #fff;padding-top: 0">
                            
                        </div>
                            
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" id="btnImportar">Importar</button>
                        </div>
                       
                    </form>
                    <iframe id="upload_target" name="upload_target" src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>   
                </div>
            </div>
        </div>

</div>
<?php include("../includes/footer.php") ?>
</div>
</div>
            
</body>

<!-- LOS MODALES AQUI  -->
<!-- <button class='btn' data-clipboard-action='copy' data-clipboard-target='p'>Copy</button> -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/clipboard.min.js"></script>
<script src="../assets/calendario/dist/sweetalert.min.js"></script>

    <!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
 <script src="../assets/js/jquery-confirm.min.js"></script>

<!-- <?php include_once('../includes/script.php') ?>  -->
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
<script>

    //$("#reclamoForm").validate();

    $('#modalImportFile').on('show.bs.modal', function (e) {
        $.ajax({
            type : 'post',
            url : 'modal/reclamoImportDataModal', //Here you will fetch records 
            data :  {}, //Pass $id
            success : function(data){
                    $('#modalImportFile .modal-body').html(data);//Show fetched data from database
                }
            });
    });

    $('#modalCrud').on('show.bs.modal', function (e) {
        //var hdSecuritySave = $.ajax({type: "POST", url: "ajax/DatosTablasAjax", data:{"tabla":"getTokenSecurity"}, async: false}).responseText;
        //$("#hdSecuritySave").val(hdSecuritySave);
        var rowid = $(e.relatedTarget).data('id') || null;
        if(rowid===null){
            $("#modalCrud h4").html("Nuevo reclamo");
            $("#modalCrud button[type='submit']").text("Guardar");
        }else{
            $("#modalCrud h4").html("Editar reclamo");
            $("#modalCrud button[type='submit']").text("Actualizar");
        }

        $.ajax({
            type : 'post',
            url : 'modal/reclamoDataModal', //Here you will fetch records 
            data :  'rowid='+ rowid, //Pass $id
            success : function(data){
                    $('#modalCrud .modal-body').html(data);//Show fetched data from database
                },
            error: function(error){
                console.log(error);
            }
            });
    });

    

    $(document).ready(()=>{

        $("#reclamoForm").submit(function (event) {


            // Deshabilitar el botón al enviar la solicitud
            $("#modalCrud button[type='submit']").prop('disabled', true);

            //$("#guardarBtn").prop('disabled', true);
            $("#loadingIndicator").show();

            var formData = $("#reclamoForm").serialize();

            $.ajax({
                type: "POST",
                url: "ajax/reclamoClienteAjax",
                data: {formData: formData},
                dataType: "json",
                encode: true,
            }).done(function (data) {
                // Volver a habilitar el botón después de que la solicitud ha finalizado
                $("#modalCrud button[type='submit']").prop('disabled', false);

                //$("#guardarBtn").prop('disabled', false);
                $("#loadingIndicator").hide();

                if(data.error==false){
                    swal({
                        title: "Ok!",
                        text: "Se guardó correctamente el registro.",
                        type: "success",
                        //showCancelButton: true,
                        //confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    },
                    function(){
                        //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        window.location.reload();
                    });
                }else{
                    
                    if (data.status == "session") {
                        alert(data.msg);
                        window.location.reload();
                    } else{
                        if (data.respSendingNotif) {
                            swal({
                                type:"error",
                                title: "No se envió correo",
                                text: "No se envió el correo al interno: " + data.respSendingNotif.msg,
                            });
                        }
                    
                        if (data.respSendingNotifClient) {
                            swal({
                                type:"error",
                                title: "No se envió correo",
                                text: "No se envió el correo al cliente: " + data.respSendingNotifClient.msg,
                            });
                        }
                    }
                    //window.location.reload();
                }
            });

            event.preventDefault();
        });
    });

    function limpiarTrama(){
        var idTrama = $("#idTrama").val('');
    }

    $('#rango').daterangepicker({
        format: 'DD-MM-YYYY',
        // startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        // minDate: '01/01/2012',
        // maxDate: '12/31/2015',
        // dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: false,
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
            'Este Mes': [moment().startOf('month'), moment().endOf('month')],
            'Anterior Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'right',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-primary',
        cancelClass: 'btn-default',
        separator: '/',
        locale: {
            applyLabel: 'Seleccionar',
            cancelLabel: 'Cancelar',
            fromLabel: 'Desde',
            toLabel: 'Hasta',
            customRangeLabel: 'Seleccionar Rango',
            daysOfWeek: ['Do', 'Lun', 'Mar', 'Mi', 'Ju', 'Vi','Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            firstDay: 1
        }
    }, function(start, end, label) {               
        //$('#rango').focus();
        //console.log(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        //05-04-2023/11-04-2023
        var fecha = start.format('DD-MM-YYYY')+'/'+end.format('DD-MM-YYYY');
        console.log(fecha);
        var page = $("#resultado").attr("data-page"); 
        console.log(page);
        var convenio=$("#convenio").val();
        //var estado = $("#resultado1").val();      
        $.ajax({
            type : 'POST',
            url  : 'ajax/paginacionReclamosClientes',
            data : {page:page,fecha:fecha},
            success : function(data)
            {
                $("#resultado").html(data);
                //console.log(data)
            }
        });
    });

    /**
     * MODAL BUSCAR CLIENTES
     */

    $('#modalClientes').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id') || null;
        $.ajax({
            type : 'post',
            url : 'modal/BuscarClientesModal', //Here you will fetch records 
            data :  'rowid='+ rowid, //Pass $id
            success : function(data){
                    $('#modalClientes .modal-body').html(data);//Show fetched data from database
                }
            });
    });


   
</script>
   
<script>

    $(document).ready(function(){
        $.ajax({
            type : 'POST',
            url  : 'ajax/paginacionReclamosClientes',
            success : function(data)
            {
                $("#resultado").html(data);
            }
        });

        $("#resultado").on( "click", ".pagination1 a", function (e){
            e.preventDefault();
            var fecha=$("#rango").val();
            var convenio=$("#convenio").val();        
            var page = $(this).attr("data-page"); 
            //var estado = $("#resultado1").val(); 
            $.ajax({
            type : 'POST',
            url  : 'ajax/paginacionReclamosClientes',
            data : {page:page,fecha:fecha},
            success : function(data)
            {
                $("#resultado").html(data);
            }
            });

        });

        var actualizarHora = function(){
            // Obtenemos la fecha actual, incluyendo las horas, minutos, segundos, dia de la semana, dia del mes, mes y año;
            var fecha = new Date(),
            horas = fecha.getHours(),
            ampm,
            minutos = fecha.getMinutes(),
            segundos = fecha.getSeconds(),
            diaSemana = fecha.getDay(),
            dia = fecha.getDate(),
            mes = fecha.getMonth(),
            year = fecha.getFullYear();

            // Accedemos a los elementos del DOM para agregar mas adelante sus correspondientes valores
            var pHoras = document.getElementById('horas'),
            pAMPM = document.getElementById('ampm'),
            pMinutos = document.getElementById('minutos'),
            pSegundos = document.getElementById('segundos'),
            pDiaSemana = document.getElementById('diaSemana'),
            pDia = document.getElementById('dia'),
            pMes = document.getElementById('mes'),
            pYear = document.getElementById('year');

            
            // Obtenemos el dia se la semana y lo mostramos
            var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
            pDiaSemana.textContent = semana[diaSemana];

            // Obtenemos el dia del mes
            pDia.textContent = dia;

            // Obtenemos el Mes y año y lo mostramos
            var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
            pMes.textContent = meses[mes];
            pYear.textContent = year;

            // Cambiamos las hora de 24 a 12 horas y establecemos si es AM o PM

            if (horas >= 12) {
                horas = horas - 12;
                ampm = 'PM';
            } else {
                ampm = 'AM';
            }

            // Detectamos cuando sean las 0 AM y transformamos a 12 AM
            if (horas == 0 ){
                horas = 12;
            }

            // Si queremos mostrar un cero antes de las horas ejecutamos este condicional
            // if (horas < 10){horas = '0' + horas;}
            pHoras.textContent = horas;
            pAMPM.textContent = ampm;

            // Minutos y Segundos
            if (minutos < 10){ minutos = "0" + minutos; }
            if (segundos < 10){ segundos = "0" + segundos; }

            pMinutos.textContent = minutos;
            pSegundos.textContent = segundos;
        };
        actualizarHora();
        var intervalo = setInterval(actualizarHora, 1000);

    })
</script>
   <script>
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
<?php else: ?> 
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?> 
<?php endif ?>

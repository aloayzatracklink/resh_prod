<?php $nav="comercialPersonalizado" ;?>
<link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
} ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->comercialPersonalizado=="si"): ?>
<?php 
    if (isset($_POST['consultar'])) {
        $estadisticaOBJ=new Estadistica();
        $fecha=htmlspecialchars($_POST['fecha']);        
        $hasta=trim(end(explode('al', $fecha)));
        $desde=trim(current(explode('al', $fecha)));
        $tema=htmlspecialchars($_POST['tema']);
        if ($tema=='eje') {
            $estadisticaOBJ->obtenerReportePersonalizadoPorEjecutivo($desde,$hasta);
            echo $desde;
            echo "--se----";
            echo $hasta;     
        }

        #SELECT * FROM `vehiculos` WHERE fechaCreacion BETWEEN '10-02-2017' AND '11-02-2017' ;#MENOS UN DIA TIENE QUE SER

    }


 ?>

 <link href="../assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
             <?php include("../includes/menuIzquierdo.php") ?>
         </div>
     </nav>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../includes/navegacionArriba.php") ?>

            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Consulta de reporte personalizado</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <form action="" method="post">
                             <div class="form-group"><label class="col-lg-2 control-label">Rango de fecha</label>

                                <div class="col-lg-3"><input style="border: none;"  class="cd-select form-control border-bottom" required  type="text" placeholder="Seleccione Rango de fecha" class="form-control" value="" name="fecha" id="rango"></div>
                                <label class="col-lg-1 control-label">Tema</label>
                                <div class="col-lg-3"> 
                                    <select style="border: none;" id="selectNav"  class="cd-select form-control border-bottom" required name="tema" >
                                        <option value="" disabled selected>Seleccione tema *</option>
                                        <option value="eje">Ventas por ejecutivo</option>
                                        <option value="plan">Ventas por plan</option>
                                        <option value="brocker">Ventas por broker</option>
                                        <option value="concesionario">Ventas por concesionario</option>
                                        <option value="concesionario">Ventas por aseguradora</option>
                                        <option value="cliente">Reporte de clientes</option>
                                    </select> 
                                </div>

                                </div>
                    
                                <button class="btn btn-primary" name="consultar">Consultar</button>
                               </form>
                            </div>
                        </div>
                    <?php if (isset($_POST['consultar'])): ?>
                                   
                        <div class="ibox">
                                      
                        <div class="ibox-title">
                            <h5>Resultado.</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">                         


        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover dataTables-example" >
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Orden</th>
                        <th>Tipo documento Facturacion</th>
                        <th>Dni o Ruc</th>
                        <th>Nombres</th>
                        <th>Direccion</th>
                        <th>telefono</th>
                        <!-- <th>Cantidad Vehiculos</th> -->
                        <th>Plan</th>
                        <th>Tiempo</th>
                        <th>costo</th>
                        <th>Observaciones</th>
                        <th>Ejecutivo</th>
                        <th>Fecha creacion</th>
                        <th>Imagen de Voucher</th>
                        <th></th>
                    </tr>
                </thead>
                   
                <tbody>
                    <?php $n=1; ?>
                    <?php $contabilidadObj=Contabilidad::obtenerDatosParaFacturacion() ?>
                    <?php foreach ($contabilidadObj as $var): ?>                        
                    <tr class="gradeX">
                        <td><?php echo $n++ ?></td>      
                        <td><?php echo $retVal = (!empty($var->orden)) ? $var->orden : "Plataforma aun no asigna orden" ;  ?></td>      
                        <td><?php echo $var->tipoDocumentoFacturacion ?></td>
                        <td><?php  echo $retVal = ($var->tipoDocumentoFacturacion=="Boleta") ? $var->dniFacturacion : $var->rucFacturacion ; ?></td>
                        <td class="center"><?php echo $var->razonSocialFacturacion ?></td>
                        <td class="center"><?php echo $var->direccionFiscal ?></td>
                        <td class="center"><?php echo $var->telefonoFacturacion ?></td>
                        <!-- <td class="center"><?php echo $var->cantidadVehiculo ?></td> -->
                        <td class="center"><?php echo $var->plan ?></td>
                        <td class="center"><?php echo $var->tiempo ?></td>
                        <td class="center"><?php echo $var->costo ?></td>
                        <td class="center"><?php echo $var->observaciones ?></td>
                        <td class="center"><?php $usuario=Usuario::obtenerUsuario($var->idUsuarioCreado);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno;  ?></td><td class="center"><?php echo $var->fechaCreada ?></td>
                        <td class="center"><img src="vouchers/<?php echo $var->voucher ?>" alt="<?php echo $var->datosfacturacionid ?>" width="100" ></td>
                        <td class="center "><button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#contabilidad" data-id="<?php echo $var->datosfacturacionid ?>">Elegir <i class="fa fa-cog"> </i></button></td>
                    </tr>
                    <?php endforeach ?>
                    
                </tbody>
                    <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Tipo documento Facturacion</th>
                        <th>Dni o Ruc</th>
                        <th>Nombres</th>
                        <th>Direccion</th>
                        <th>telefono</th>
                        <th>Cantidad Vehiculos</th>
                        <th>Plan</th>
                        <th>Tiempo</th>
                        <th>costo</th>
                        <th>Observaciones</th>
                        <th>Ejecutivo y fecha creacion</th>
                        <th>Acccion</th>
                        <th>Imagen de Voucher</th>
                    </tr>
                    </tfoot>
                    </table>
                        </div>





                        </div>
                        </div>

                          <?php endif ?>  
                    </div>
             
                <?php include("../includes/footer.php") ?>
            </div>
        </div>

    </div>

    <script src="../assets/js/jquery-2.1.1.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <script src="../assets/js/plugins/dataTables/datatables.min.js"></script>
    <!-- Custom and plugin javascript -->
    <script src="../assets/js/inspinia.js"></script>
    <script src="../assets/js/plugins/pace/pace.min.js"></script>
    <script src="../assets/js/plugins/footable/footable.all.min.js"></script>
    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
    <!-- Date range picker -->
    <script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
    <?php include_once('../includes/script.php') ?>
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

            $('#rango').daterangepicker({
                format: 'DD MM YYYY',
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),

                // minDate: '01/01/2012',
                // maxDate: '12/31/2015',
                dateLimit: { days: 60 },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Hoy': [moment(), moment()],
                    'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
                    'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                    'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                    'Anterior Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: ' al ',
                locale: {
                    applyLabel: 'Seleccionar',
                    cancelLabel: 'Cancelar',
                    fromLabel: 'Desde',
                    toLabel: 'Hasta',
                    customRangeLabel: 'Seleccionar Rango',
                    daysOfWeek: ['Do', 'Lun', 'Mar', 'Mi', 'Ju', 'Vi','Sa'],
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    firstDay: 1
                }
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#rango').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            });

        });


    </script>
<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>

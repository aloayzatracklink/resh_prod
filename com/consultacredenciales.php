<?php
header('Content-Type: application/json');

require __DIR__ . "/../vendor/autoload.php";
require("../includes/configuracion.php");
require_once("../includes/Recursos.php");
require_once("../includes/baseDatos.php");
require("../includes/baseDatosOnix.php");
//require_once("../includes/Onix.php");
require("../includes/Onix.php");
require("../includes/Usuario.php");
require("class/AutentificacionSQLSrv.php");

//var_dump($abc);
// Aquí creamos una instancia de la clase Onix
$onix = new Onix();

// $dni_ruc = isset($_GET['txtdni']) ? $_GET['txtdni'] : null;
// $chasis = isset($_GET['selectedChasis']) ? $_GET['selectedChasis'] : null;
$selectedIdCustomer = $_GET['selectedIdCustomer'] ?? null;
$selectedIdVehicle = $_GET['selectedIdVehicle'] ?? null;

if ($selectedIdCustomer !== null && $selectedIdVehicle !== null) {
    $resultado = $onix->datosParaEnviarCredenciales($selectedIdCustomer, $selectedIdVehicle);

    if ($resultado) {
        $SQL_abc = new AutentificacionSQLSrv();
        $abc = $SQL_abc->obtenerCredenciales($resultado["idcustomer"]);

        echo json_encode([
            "error" => false,
            "descarticle" => $resultado["descarticle"],
            "cliente" => $resultado["cliente"],
            "correo" => $resultado["correo"],
            "usuario" => $abc["ci_ruc_cliente"],
            "contra" => $abc["password"],
            "sistema" => $resultado["sistema"], // Agregamos los nuevos campos
            "tipoTrack" => $resultado["tipoTrack"],
        ], JSON_INVALID_UTF8_IGNORE);
    } else {
        echo json_encode(["error" => true, "mensaje" => "No se encontraron datos para el DNI o RUC proporcionado."], JSON_INVALID_UTF8_IGNORE);
    }
} else {
    echo json_encode(["error" => true, "mensaje" => "No se proporcionó un idcustomer o idvehicle válido."], JSON_INVALID_UTF8_IGNORE);
}

?>
<?php 
//Funciones que traen las plantillas para enviar correos a clientes: OCTO, TRACK INTERACTIVO Y TRACK CONTROL TOTAL
function mailOcto($nombre, $usuario, $contra){
    $contraMsg = empty($contra) ? "" :"<p><strong>CONTRASEÑA: </strong>$contra</p>";
    return '
    <!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Correo con Credenciales OCTO</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 40px 0;
        }

        .container {
            background-color: #ffffff;
            border-radius: 10px;
            padding: 20px;
            max-width: 600px;
            margin: 0 auto;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }

        .bar {
            height: 50px;
            text-align: center;
            margin: 10px 0;
            position: relative;  /* Agregado para poner el logo encima de la barra */
        }

        .bar img {
            margin-top: 13px;
            top: 50%;
            left: 80%;
            transform: translate(-50%, -50%);
            height: 50%;
            width: auto;
        }

        .content {
            margin: 20px 0;
            line-height: 1.6;
        }

        .credentials {
            background-color: #e6f2ff;
            padding: 15px;
            border: 1px solid #b3daff;
            border-radius: 5px;
            margin: 15px 0;
        }

        a {
            color: #006ba4;
            text-decoration: none;
        }

        a:hover {
            text-decoration: underline;
        }

        /* Estilo para el link específico que cambia de colores */
        a.pulsing-link {
            animation: pulsate 1.5s infinite;
        }

        @keyframes pulsate {
            0%   { color: #007BFF; }
            50%  { color: #FF4500; }
            100% { color: #007BFF; }
        }

        .app-links, .video-link {
            display: flex;
            align-items: center;
            margin-bottom: 10px;
        }

        .icon {
            width: 25px;
            height: 25px;
            margin-right: 10px;
        }

    </style>
</head>

<body>

    <div class="container">
        <div class="bar">
            <img src="cid:logotrackfinal" alt="Logo Tracklink">
        </div>
        <p>Estimado(a) cliente(a),</p>
        <h2>'.$nombre.'</h2>
        <div class="content">
            <p>Nos es grato saludarle y agradecerle su interés en nuestros productos. A continuación le hacemos llegar sus credenciales para el acceso a la Plataforma Web.</p>

            <div class="credentials">
                <p><strong>USUARIO:</strong> '.$usuario.'</p>
                '.$contraMsg.'
                <p><strong>Para cambiar su contraseña, ingresar a: https://login.octotelematics.com/octo/reminder/pwd_reminder.jsp</strong></p>
            </div>

            <p><strong>Links:</strong></p>
            <ul>
                <li><a href="https://ultimaposicion.tracklink.pe/" target="_blank">https://ultimaposicion.tracklink.pe/</a></li>
                <li><a href="https://www.octotelematics.it/octo/login.jsp" target="_blank">https://www.octotelematics.it/octo/login.jsp</a></li>
                <li><a href="https://www.octotelematics.net/octo/login.jsp" target="_blank">https://www.octotelematics.net/octo/login.jsp</a></li>
                <li><a href="https://login.octotelematics.com/octo/login.jsp?lang=es" target="_blank">https://login.octotelematics.com/octo/login.jsp?lang=es</a></li>
            </ul>

            <p>Me despido,</p>
        </div>
        
        <p>Saludos cordiales.</p>

        <div class="credentials">
        <p>AVISO DE PRIVACIDAD Y PROTECCIÓN DE DATOS PERSONALES</p>
        <p>Motorlink garantiza la confidencialidad y seguridad de la informacion obtenida en el ejercicio de nuestras funciones.</p>
        <p>Ley Nº 29733, ley de Protección de Datos Personales.</p>
        </div>
    </div>
</body>

</html>
';
}

function mailInteractivo($nombre, $usuario, $contra){
    return '
    <!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Correo con Credenciales</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 40px 0;
        }

        .container {
            background-color: #ffffff;
            border-radius: 10px;
            padding: 20px;
            max-width: 600px;
            margin: 0 auto;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }

        .bar {
            height: 50px;
            text-align: center;
            margin: 10px 0;
            position: relative;  /* Agregado para poner el logo encima de la barra */
        }

        .bar img {
            margin-top: 13px;
            top: 50%;
            left: 80%;
            transform: translate(-50%, -50%);
            height: 50%;
            width: auto;
        }

        .content {
            margin: 20px 0;
            line-height: 1.6;
        }

        .credentials {
            background-color: #e6f2ff;
            padding: 15px;
            border: 1px solid #b3daff;
            border-radius: 5px;
            margin: 15px 0;
        }

        a {
            color: #006ba4;
            text-decoration: none;
        }

        a:hover {
            text-decoration: underline;
        }

        /* Estilo para el link específico que cambia de colores */
        a.pulsing-link {
            animation: pulsate 1.5s infinite;
        }

        @keyframes pulsate {
            0%   { color: #007BFF; }
            50%  { color: #FF4500; }
            100% { color: #007BFF; }
        }

        .app-links, .video-link {
            display: flex;
            align-items: center;
            margin-bottom: 10px;
        }

        .icon {
            width: 25px;
            height: 25px;
            margin-right: 10px;
        }

    </style>
</head>

<body>

    <div class="container">
        <div class="bar">
            <img src="cid:logotrackfinal" alt="Logo Tracklink">
        </div>
        <p>Estimado(a) cliente(a),</p>
        <h2>'.$nombre.'</h2>
        <div class="content">
            <p>Lo(a) saludo cordialmente. </p>
            <p>El presente correo es para poder hacerle llegar las credenciales para el acceso a su plataforma web, así como también el link de acceso a la misma.</p>

            <div class="credentials">
                <p><strong>USUARIO:</strong> '.$usuario.'</p>
                <p><strong>CONTRASEÑA:</strong> '.$contra.'</p>
            </div>

            <p><strong>LINK:</strong> <a href="https://www.tracklink.pe/mt/" target="_blank" class="pulsing-link">https://www.tracklink.pe/mt/</a></p>

            <p><strong>Descargue su APP:</strong></p>
            <div class="app-links">
                <img src="cid:logoapple" class="icon" alt="IOS Logo">
                <a href="https://apps.apple.com/us/app/tracklink-monitoreo/id1559949474?l=es" target="_blank">https://apps.apple.com/us/app/tracklink-monitoreo/id1559949474?l=es</a>
            </div>
            
            <div class="app-links">
                <img src="cid:logoplaystore" class="icon" alt="Android Logo">
                <a href="https://play.google.com/store/apps/details?id=com.pe.tracklink" target="_blank">https://play.google.com/store/apps/details?id=com.pe.tracklink</a>
            </div>

            <p><strong>Video Explicativo:</strong></p>
            <div class="video-link">
                <img src="cid:logoyoutube" class="icon" alt="YouTube Logo">
                <a href="https://youtu.be/TXT_yhbdNMA" target="_blank">https://youtu.be/TXT_yhbdNMA</a>
            </div>

            <p>Me despido,</p>
        </div>
        
        <p>Saludos cordiales.</p>
        <div class="credentials">
        <p>AVISO DE PRIVACIDAD Y PROTECCIÓN DE DATOS PERSONALES</p>
        <p>Motorlink garantiza la confidencialidad y seguridad de la informacion obtenida en el ejercicio de nuestras funciones.</p>
        <p>Ley Nº 29733, ley de Protección de Datos Personales.</p>
        </div>
    </div>
</body>

</html>
';
}

function mailControlTotal($nombre, $usuario, $contra){
    return '
    <!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Correo con Credenciales</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 40px 0;
        }

        .container {
            background-color: #ffffff;
            border-radius: 10px;
            padding: 20px;
            max-width: 600px;
            margin: 0 auto;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }

        .bar {
            height: 50px;
            text-align: center;
            margin: 10px 0;
            position: relative;  /* Agregado para poner el logo encima de la barra */
        }

        .bar img {
            margin-top: 13px;
            top: 50%;
            left: 80%;
            transform: translate(-50%, -50%);
            height: 50%;
            width: auto;
        }

        .content {
            margin: 20px 0;
            line-height: 1.6;
        }

        .credentials {
            background-color: #e6f2ff;
            padding: 15px;
            border: 1px solid #b3daff;
            border-radius: 5px;
            margin: 15px 0;
        }

        a {
            color: #006ba4;
            text-decoration: none;
        }

        a:hover {
            text-decoration: underline;
        }

        /* Estilo para el link específico que cambia de colores */
        a.pulsing-link {
            animation: pulsate 1.5s infinite;
        }

        @keyframes pulsate {
            0%   { color: #007BFF; }
            50%  { color: #FF4500; }
            100% { color: #007BFF; }
        }

        .app-links, .video-link {
            display: flex;
            align-items: center;
            margin-bottom: 10px;
        }

        .icon {
            width: 25px;
            height: 25px;
            margin-right: 10px;
        }

    </style>
</head>

<body>

    <div class="container">
        <div class="bar">
            <img src="cid:logotrackfinal" alt="Logo Tracklink">
        </div>
        <p>Estimado(a) cliente(a),</p>
        <h2>'.$nombre.'</h2>
        <div class="content">
            <p>Lo(a) saludo cordialmente. </p>
            <p>El presente correo es para poder hacerle llegar las credenciales para el acceso a su plataforma web, así como también el link de acceso a la misma.</p>

            <div class="credentials">
                <p><strong>USUARIO:</strong> '.$usuario.'</p>
                <p><strong>CONTRASEÑA:</strong> '.$contra.'</p>
            </div>

            <p><strong>LINK:</strong> <a href="http://www.tracklink.pe/" target="_blank" class="pulsing-link">http://www.tracklink.pe/</a></p>

            <p><strong>Descargue su APP:</strong></p>
            <div class="app-links">
                <img src="cid:logoapple" class="icon" alt="IOS Logo">
                <a href="https://apps.apple.com/us/app/tracklink-monitoreo/id1559949474?l=es" target="_blank">https://apps.apple.com/us/app/tracklink-monitoreo/id1559949474?l=es</a>
            </div>
            
            <div class="app-links">
                <img src="cid:logoplaystore" class="icon" alt="Android Logo">
                <a href="https://play.google.com/store/apps/details?id=com.pe.tracklink" target="_blank">https://play.google.com/store/apps/details?id=com.pe.tracklink</a>
            </div>

            <p><strong>Video Explicativo:</strong></p>
            <div class="video-link">
                <img src="cid:logoyoutube" class="icon" alt="YouTube Logo">
                <a href="https://www.youtube.com/watch?v=koboAdV-JUE&t=1431s" target="_blank">https://www.youtube.com/watch?v=koboAdV-JUE&t=1431s</a>
            </div>

            <p><strong>Manual de uso de Plataforma:</strong></p>
            <div class="manual-link">
                <a href="https://www.tracklink.pe/Tracklink/sistema/trackfront/Manuales/Manual_online/index.html" target="_blank">https://www.tracklink.pe/Tracklink/sistema/trackfront/Manuales/Manual_online/index.html</a>
            </div>

            <p>Me despido y agradezco de antemano su atención.</p>
        </div>
        
        <p>Saludos cordiales.</p>
        <div class="credentials">
        <p>AVISO DE PRIVACIDAD Y PROTECCIÓN DE DATOS PERSONALES</p>
        <p>Motorlink garantiza la confidencialidad y seguridad de la informacion obtenida en el ejercicio de nuestras funciones.</p>
        <p>Ley Nº 29733, ley de Protección de Datos Personales.</p>
        </div>
    </div>
</body>

</html>
';
}

//PARA ENVIAR CREDENCIALES DESDE EL MÓDULO GESTION ACCESOS 

function gestionAccesosCredenciales($nombreCliente, $usu, $contra){
    return '
    <!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Correo con Credenciales</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 40px 0;
        }

        .container {
            background-color: #ffffff;
            border-radius: 10px;
            padding: 20px;
            max-width: 600px;
            margin: 0 auto;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }

        .bar {
            height: 50px;
            text-align: center;
            margin: 10px 0;
            position: relative;  /* Agregado para poner el logo encima de la barra */
        }

        .bar img {
            margin-top: 13px;
            top: 50%;
            left: 80%;
            transform: translate(-50%, -50%);
            height: 50%;
            width: auto;
        }

        .content {
            margin: 20px 0;
            line-height: 1.6;
        }

        .credentials {
            background-color: #e6f2ff;
            padding: 15px;
            border: 1px solid #b3daff;
            border-radius: 5px;
            margin: 15px 0;
        }

        a {
            color: #006ba4;
            text-decoration: none;
        }

        a:hover {
            text-decoration: underline;
        }

        /* Estilo para el link específico que cambia de colores */
        a.pulsing-link {
            animation: pulsate 1.5s infinite;
        }

        @keyframes pulsate {
            0%   { color: #007BFF; }
            50%  { color: #FF4500; }
            100% { color: #007BFF; }
        }

        .app-links, .video-link {
            display: flex;
            align-items: center;
            margin-bottom: 10px;
        }

        .icon {
            width: 25px;
            height: 25px;
            margin-right: 10px;
        }

    </style>
</head>

<body>

    <div class="container">
        <div class="bar">
            <img src="cid:logotrackfinal" alt="Logo Tracklink">
        </div>
        <p>Estimado(a) cliente(a),</p>
        <h2>'.$nombreCliente.'</h2>
        <div class="content">
            <p>Lo(a) saludo cordialmente. </p>
            <p>El presente correo es para poder hacerle llegar las credenciales para el acceso a su plataforma web, así como también el link de acceso a la misma.</p>

            <div class="credentials">
                <p><strong>USUARIO:</strong> '.$usu.'</p>
                <p><strong>CONTRASEÑA:</strong> '.$contra.'</p>
            </div>

            <p><strong>LINK:</strong> <a href="http://www.tracklink.pe/" target="_blank" class="pulsing-link">http://www.tracklink.pe/</a></p>

            <p><strong>Descargue su APP:</strong></p>
            <div class="app-links">
                <img src="cid:logoapple" class="icon" alt="IOS Logo">
                <a href="https://apps.apple.com/us/app/tracklink-monitoreo/id1559949474?l=es" target="_blank">https://apps.apple.com/us/app/tracklink-monitoreo/id1559949474?l=es</a>
            </div>
            
            <div class="app-links">
                <img src="cid:logoplaystore" class="icon" alt="Android Logo">
                <a href="https://play.google.com/store/apps/details?id=com.pe.tracklink" target="_blank">https://play.google.com/store/apps/details?id=com.pe.tracklink</a>
            </div>

            <p><strong>Video Explicativo:</strong></p>
            <div class="video-link">
                <img src="cid:logoyoutube" class="icon" alt="YouTube Logo">
                <a href="https://www.youtube.com/watch?v=koboAdV-JUE&t=1431s" target="_blank">https://www.youtube.com/watch?v=koboAdV-JUE&t=1431s</a>
            </div>

            <p><strong>Manual de uso de Plataforma:</strong></p>
            <div class="manual-link">
                <a href="https://www.tracklink.pe/Tracklink/sistema/trackfront/Manuales/Manual_online/index.html" target="_blank">https://www.tracklink.pe/Tracklink/sistema/trackfront/Manuales/Manual_online/index.html</a>
            </div>

            <p>Me despido y agradezco de antemano su atención.</p>
        </div>
        
        <p>Saludos cordiales.</p>
        <div class="credentials">
        <p>AVISO DE PRIVACIDAD Y PROTECCIÓN DE DATOS PERSONALES</p>
        <p>Motorlink garantiza la confidencialidad y seguridad de la informacion obtenida en el ejercicio de nuestras funciones.</p>
        <p>Ley Nº 29733, ley de Protección de Datos Personales.</p>
        </div>
    </div>
</body>

</html>
';
}
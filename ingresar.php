<script>
document.addEventListener('DOMContentLoaded', function () {
  if (Notification.permission !== "granted")
    Notification.requestPermission();
});
</script>
<?php 

$nav="Ingresar ";
include("includes/head.php");

if (isset($_POST['ingresar'])){
    $usuario=filter_var(strtolower(trim($_POST['usuario'])), FILTER_SANITIZE_STRING);
    $contrasenia=trim($_POST['contrasenia']);
    $usuario=str_replace('@tracklink.pe', '', $usuario);
    $usuario=Onix::validarUsuarioYContrasenia($usuario,$contrasenia);
    if ($usuario){
       // echo "Ingresar";
        $usuario=filter_var(strtolower(trim($_POST['usuario'])), FILTER_SANITIZE_STRING);
        $usuario=str_replace('@tracklink.pe', '', $usuario);
        $usuario=$usuario."@tracklink.pe"; 
        Usuario::actualizarContrasenia($usuario,$contrasenia);
        if($usuarioEncontrado=Usuario::verificarUsuario($usuario,$contrasenia)){
            $session->ingresar($usuarioEncontrado);  
             Log::registrarLog("ingresos","-",$usuario,"Ok");          
        }else{
            $mensaje="Cuenta desactivada escribe a pvasquez@tracklink.pe";            
            Log::registrarLog("Nocreado","-",$_POST,$_POST['usuario']);          
        }
    }
    else{
        $mensaje="Usuario o contraseña está mal";
        Log::registrarLog("ingresoFail","-",$_POST,$_POST['usuario']);          
    }
}else{
    $usuario="";
    $contrasenia="";
    $mensaje="";
}
if ($session->yaEstaIngresado()){ 
    /*if($_POST['contrasenia']=="123"){
        ira("editarPerfil");
        die();

    }*/
    $obMenu=Usuario::obtenerMenu($session->idusuario);

    if ($obMenu) {
    if ($obMenu->baterias=="si") {         
        irA("cem/instalacion");             
    }else
    if($obMenu->panel=="si"){ 
        irA("index"); 
    }else
    if ($obMenu->instalaciones=="si") {
        irA("cem/instalacion");             
    }else
    if ($obMenu->chequeos=="si") {
        irA("cem/chequeos");             
    }else
    if ($obMenu->choques=="si") {
        irA("cem/choques");             
    }else
    if ($obMenu->robos=="si") {
        irA("cem/robo");             
    }else
    if ($obMenu->maps=="si") {
        irA("cem/maps");             
    }else
    if ($obMenu->registrarOperaciones=="si") {
        irA("ope/registrarOperaciones");             
    }else
    if ($obMenu->operacionesComerciales=="si") {
        irA("com/operacionesComerciales");             
    }else
    if ($obMenu->cotizar=="si") {
        irA("programacionDisponibilidad");
    }else
    if ($obMenu->registrarCliente=="si") {
        irA("com/registrarCliente");             
    }else
    if ($obMenu->gestionPostVenta=="si") {
        irA("rpr/gestionPostVenta");             
    }else
    if ($obMenu->gestionRenovaciones=="si") {
        irA("rpr/gestionRenovaciones");             
    }else
    if ($obMenu->verificarOperaciones=="si") {
        irA("ope/verificarOperaciones");             
    }else
    if ($obMenu->gestionTecnica=="si") {
        irA("ope/gestionTecnica");             
    }else
    if ($obMenu->editarTecnicos=="si") {
        irA("ope/editarTecnicos");             
    }else
    if ($obMenu->administrar=="si") {
        irA("admin/administrar");             
    }else
    if ($obMenu->verificarComercial=="si") {
        irA("com/verificarComercialP");             
    }else
    if ($obMenu->comercialPersonalizado=="si") {
        irA("com/comercialPersonalizado");             
    }else
    if ($obMenu->gestionContabilidad=="si") {
        irA("conta/gestionContabilidad");             
    }else
    if ($obMenu->historialContable=="si") {
        irA("conta/historialContable");             
    }else
    if ($obMenu->programacion=="si") {
        irA("programacionDisponibilidad");             
    }else
    if ($obMenu->comisiones=="si") {
        irA("com/comisionesR");             
    }else
    if ($obMenu->cobranzas=="si") {
        irA("conta/cobranzas");             
    }else
    if ($obMenu->entregaFactura=="si") {
        irA("conta/entregaFacturas");             
    }else
    if ($obMenu->pagoComision=="si") {
        irA("conta/pagoComisiones");             
    }
    else{
        $mensaje="Usuario sin Opciones configuradas";
    }
}else{
    $mensaje="Usuario sin Opciones configuradas";

}

        // }else if ($usuarioEncontrado->idrol==4){
        //  irA("registrarCliente"); 
        // }else if ($usuarioEncontrado->idrol==7){
        //  irA("verificarOperaciones"); 
        // }else if ($usuarioEncontrado->idrol==5){
        //  irA("gestionPostVenta"); 
        // }else if ($usuarioEncontrado->idrol==11){
        //  irA("gestionContabilidad"); 
        // }else{
        //     irA("index"); 
        // }

// echo $session->idusuario;
} 
?>
<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
<div class="row">

            <div class='col-md-12'>
                <h4 class="logo-name">R</h1>
            </div>
</div>
            <h3>Bienvenido a Resh CRM</h3>
            <p></p>
            <!-- <p>Ingresar aqui.</p>  -->
        <!--      <p class="label label-warning"><?php  if (isset($mensaje)) {
             echo $mensaje;
            }
            ?></p> --> 
            <form class="m-t" role="form" method="post">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Usuario" required name="usuario" value="<?php echo $retVal = (isset($_POST['usuario'])) ? $_POST['usuario'] : '' ;  ?>"> <?php //Remove values... ?>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Contraseña" required name="contrasenia" autocomplete="off" >
            </div>
            <button type="submit" name="ingresar" class="btn btn-primary block full-width m-b" >Ingresar</button>

            
<!--
                <a href="#"><small>Forgot password?</small></a>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm pbtn-white btn-block" href="register.html">Create an account</a>
            -->
        </form>
        <p class="m-t"> <small><!-- Diseñado por Wilman pabel -->  &copy; <?= date('Y') ?></small> </p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="assets/js/jquery-2.1.1.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/plugins/toastr/toastr.min.js"></script>

<script>
    <?php if (!empty($mensaje)): ?>      
    toastr.error('<?php echo $mensaje ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true}) 
<?php endif ?>
</script>
</body>

</html>

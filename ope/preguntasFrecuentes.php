<?php require_once("includes/init.php"); ?>
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}
?>
<?php 
$objF=new Faq();
if (isset($_POST['guardar'])) {
    $objF->pregunta=$_POST['pregunta'];
    $objF->nombre=$_POST['nombre'];
    $objF->tema=$_POST['tema'];
    $objF->agregarPregunta();
}
if (isset($_POST['verificar'])) {
    if (isset($_POST['codigo'])) {
        if ($_POST['codigo']=="777") {
            $_SESSION['a']=$_POST['codigo'];
        }
    }else{
        echo "inserte codigo";
    }
}
if (isset($_POST['salir'])) {
    unset($_SESSION['a']);
}
if (isset($_POST['guardarRespuesta'])) {
    $objF->respuesta=$_POST['respuesta'];
    $objF->respuesta($_POST['id']);
}

?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Track | FAQ</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/font-awesome/assets/css/font-awesome.css" rel="stylesheet">

    <link href="assets/css/animate.css" rel="stylesheet">

    <link href="assets/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="assets/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">


        <div id="wrapper" class="gray-bg">
            <div class="row border-bottom">

            </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>FAQ</h2>
                    <ol class="breadcrumb">

                        <li class="active">
                            <strong>
                                Preguntas frecuentes</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="col-sm-8">
                        <div class="title-action">
                            <a  href="#modal-form" data-toggle="modal"  class="btn btn-primary btn-sm">Añadir pregunta</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content animated fadeInRight">

                            <div class="ibox-content m-b-sm border-bottom">
                                <div class="text-center p-lg">
                                    <h2>Si no encuentra la respuesta a su pregunta</h2>
                                    <span>Agrega tu pregunta seleccionando </span>
                                    <button title="Create new cluster" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <span class="bold">Añadir pregunta</span></button> botón
                                </div>
                            </div>
                            <?php   
                            $n=1;
                            $objFaq=Faq::obtenerPreguntas();

                            ?>
                            <?php foreach ($objFaq as $var): ?>
                                <?php $n++ ?>
                                <div class="faq-item">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <a data-toggle="collapse" href="#faq<?php echo $n ?>" class="faq-question"><!-- <?php echo $n-1 ?> .- --> <?php echo utf8_encode($var->pregunta) ?></a>
                                            <small>Añadido por <strong><?php echo utf8_encode($var->nombres) ?></strong> <i class="fa fa-clock-o"></i> <?php echo $var->fecha ?></small>
                                        </div>
                                        <div class="col-md-3">
                                            <span class="small font-bold"><?php echo utf8_encode($var->nombresRespuesta) ?></span>
                                            <div class="tag-list">
                                                <span class="tag-item"><?php echo $var->tema ?></span>
                                                <!-- <span class="tag-item">License</span> -->
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <span class="small font-bold">Votación </span><br/>
                                            <?php echo $var->puntos ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="faq<?php echo $n ?>" class="panel-collapse collapse ">
                                                <div class="faq-answer">
                                                    <p>
                                                        <?php echo $var->respuesta ?>
                                                    </p>
                                                </div>
                                                <?php if (isset($_SESSION['a'])): ?>
                                                    <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#respuesta" data-id="<?php echo $var->idfaqs ?>">Responder <i class="fa fa-cog"> </i></button>

                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                            <?php endforeach ?>
                            <br><br>
                            <button class="btn btn-info" type="submit" href="#modal-form1" data-toggle="modal" >Añadir Respuestas</button> <br><br> <?php include("includes/footer.php") ?>

                        </div>
                    </div>
                </div>

                <div id="modal-form" class="modal fade" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <form role="form" method="post">
                                <div class="modal-body">
                                    <div class="row">
                                        <h2 class="text-center">Añadir Pregunta</h2>
                                        <div class="col-lg-12 b-r"><h3 class="m-t-none m-b">Registrar nueva pregunta</h3>
                                            <p>Registro de preguntas aqui.</p>

                                            <div class="form-group"><label>Nombre</label> <input type="text" placeholder="Ingrese su nombre" class="form-control" name="nombre"></div>
                                            <div class="form-group"><label>Pregunta</label> <input type="text" placeholder="Pregunta" class="form-control" name="pregunta"></div> 
                                            <div class="form-group"><label>Tema</label> <input type="text" placeholder="Tema" class="form-control" name="tema"></div>

                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button class="btn btn-default" type="submit" name="cancelar">Cancelar</button>      
                                            <button class="btn btn-primary" type="submit" name="guardar">Guardar</button>      

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="modal-form1" class="modal fade" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form role="form" method="post">
                        <div class="modal-body">
                            <div class="row">
                                <h2 class="text-center">Ingrese codigo</h2>
                                <div class="col-md-12">

                                    <div class="form-group"><label>Codigo</label> <input type="password" placeholder="Ingrese su Codigo" class="form-control" name="codigo" required=""></div>

                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-default" type="submit" name="cancelar">Cancelar</button>      
                                    <button class="btn btn-primary" type="submit" name="verificar">verificar</button>      
                                    <button class="btn btn-danger" type="submit" name="salir">salir</button>      

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>



        <div class="modal inmodal" id="respuesta" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated bounceIn">
                    <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                        <h4 class="modal-title">Respuestas</h4>
                        <small class="font-bold">faqs.<div class="form-group ">
                        </div></small>
                    </div>
                    <form  role="form" method="post" >

                        <div class="modal-body" style="background: #fff;padding-top: 0">
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>

                            <button type="submit" name="guardarRespuesta" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>   
                </div>
            </div>
        </div>

        <!-- Mainly scripts -->
        <script src="assets/js/jquery-2.1.1.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="assets/js/inspinia.js"></script>
        <script src="assets/js/plugins/pace/pace.min.js"></script>
        <?php /*include_once('includes/script.php')*/ ?>
        <script>
            $(document).ready(function(){

                $('#loading-example-btn').click(function () {
                    btn = $(this);
                    simpleLoad(btn, true)

                // Ajax example
//                $.ajax().always(function () {
//                    simpleLoad($(this), false)
//                });

simpleLoad(btn, false)
});
            });

            function simpleLoad(btn, state) {
                if (state) {
                    btn.children().addClass('fa-spin');
                    btn.contents().last().replaceWith(" Loading");
                } else {
                    setTimeout(function () {
                        btn.children().removeClass('fa-spin');
                        btn.contents().last().replaceWith(" Refresh");
                    }, 2000);
                }
            }
            $('#respuesta').on('show.bs.modal', function (e) {
                var rowid = $(e.relatedTarget).data('id');
                $.ajax({
                    type : 'post',
            url : 'includes/respuestaDataModal', //Here you will fetch records 
            data :  'rowid='+ rowid, //Pass $id
            success : function(data){
                    $('.modal-body').html(data);//Show fetched data from database
                }
            });
            });
        </script>
    </body>

    </html>

  <?php $nav="registrarVehiculo" ;?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->registrarCliente=="si"): ?>
  <?php
  $id = (isset($_GET['id'])) ? htmlspecialchars(htmlentities($_GET['id'])) : "" ;
  $errores=array();



  if (isset($_POST['registrarContacto'])) {
    $errores=array();
    $field_values_array=$_POST['field_name'];
    $objCliente= new Cliente;
    $objCliente->borrarContactosAutorizados($id);


    $n=0;
    foreach($field_values_array[0] as $val){
      $dni[$n]=$val;
      $n++;
    }

    $m=0;
    foreach($field_values_array[1] as $val) {
      $nombre[$m]=$val;
      $m++;
    }

    $x=0;
    foreach($field_values_array[2] as $val) {
      $apellidos[$x]=$val;
      $x++;
    }

    $y=0;
    foreach($field_values_array[3] as $val) {
      $celular[$y]=$val;
      $y++;
    }

    $z=0;
    foreach($field_values_array[4] as $val) {
      $telefono[$z]=$val;
      $z++;
    }

    $a=0;
    foreach($field_values_array[5] as $val) {
      $correo[$a]=$val;
      $a++;
    }

    $b=0;
    foreach($field_values_array[6] as $val) {
      $parentesco[$b]=$val;
      $b++;
    }

    $datosDeContactoAutorizado['dni']=$dni;
    $datosDeContactoAutorizado['nombre']=$nombre;
    $datosDeContactoAutorizado['apellidos']=$apellidos;
    $datosDeContactoAutorizado['celular']=$celular;
    $datosDeContactoAutorizado['telefono']=$telefono;
    $datosDeContactoAutorizado['correo']=$correo;
    $datosDeContactoAutorizado['parentesco']=$parentesco;

    for ($i=0; $i < count($datosDeContactoAutorizado['dni']) ; $i++) { 
      $datosDeContactoAutorizado['dni_i']=$datosDeContactoAutorizado['dni'][$i];
      $datosDeContactoAutorizado['nombre_i']=$datosDeContactoAutorizado['nombre'][$i];
      $datosDeContactoAutorizado['apellidos_i']=$datosDeContactoAutorizado['apellidos'][$i];
      $datosDeContactoAutorizado['celular_i']=$datosDeContactoAutorizado['celular'][$i];
      $datosDeContactoAutorizado['telefono_i']=$datosDeContactoAutorizado['telefono'][$i];
      $datosDeContactoAutorizado['correo_i']=$datosDeContactoAutorizado['correo'][$i];
      $datosDeContactoAutorizado['parentesco_i']=$datosDeContactoAutorizado['parentesco'][$i];
      if ($objCliente->registrarContactoAutorizado($datosDeContactoAutorizado,$id)) {

      }

    }

  }
  if (isset($_POST['agregarVehículo'])) {

    $errores=array();
    $echo=array();
    //comprovamos los datos del voucher....
    if (Token::verificar($_POST['token'])) {
      if (!empty($_FILES['voucher']['name'])) {
        $extencionesAbilitadas= array('jpg','jpeg','png','gif','');
        $nombreArchivo=$_FILES['voucher']['name'];        
        $tmp=explode('.', $nombreArchivo);
        $extencionArchivo=end($tmp);
        $tamanioArchivo=$_FILES['voucher']['size'];
        $ubicacionTemporal=$_FILES['voucher']['tmp_name'];

        if (!in_array($extencionArchivo,$extencionesAbilitadas)) {
          $errores[]='La extencion de la imagen deve ser jpg jpeg png gif';
        }
        if ($tamanioArchivo>5242880) {
          $errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
        }
      }
    //fin de comprovacion de los datos del voucher----
    //inicio de comprovacion de los datos de facturacion-- -

      if (isset($_POST['tipoDocumentoFacturacion']) && $_POST['tipoDocumentoFacturacion']=="Factura") {

        if (empty($_POST['rucFacturacion'])) {
          $errores[]='Completar el RUC para la facturacion';
        }else 
        if (empty($_POST['razonSocialFacturacion'])) {
          $errores[]='Completar el RUC para la facturacion';
        }else
        if (empty($_POST['direccionFiscalFacturacion'])) {
          $errores[]='Completar el RUC para la facturacion';
        }       
      }

    //fin de comprovaion

///inicio de comporvacion de lugar de instalación ----

    // creamos el objeto

      $operacionesObj=new Operacion();
      $agregarVehículoObj=new Vehiculo();
      $facturacionObj=new DatosFacturacion();
      // $objCalendario=new Calendario();
      $objComision=new Comisiones();
      $objCanales= new Canales();
      $programado="";

      if ($_POST['lugarInstacion']=='Taller') {
        $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
        $operacionesObj->direccionInstacion=$_POST['direccionTaller'];
       //  if (empty($_POST['FechaInstalacion'])) {
       //    $errores[]="Ingrese Fecha de instalacion en taller";
       // }
      $operacionesObj->estado=0;

       if (empty($_POST['nombreContactoIT']) || empty($_POST['numeroContactoIT']) )  {
          $errores[]="Registre nombre de contacto ó numero de contacto prar instalacion en taller";
        }else{
          $operacionesObj->nombreContacto=$_POST['nombreContactoIT'];
          $operacionesObj->numeroContacto=$_POST['numeroContactoIT'];
        }
    }else
    if($_POST['lugarInstacion']=='Domicilio Cliente'){
      $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
      $operacionesObj->estado=0;
      if (empty($_POST['departamentoInstalacionD'])||empty($_POST['provinciaInstalacionD'])||empty($_POST['distritoInstalacionD'])||empty($_POST['direccionInstalacionD'])||empty($_POST['referenciaInstalacionD']) ||  empty($_POST['nombreContactoID']) || empty($_POST['numeroContactoID'])) {

          $errores[]="Complete los datos de instalacon depertamento provinia referencia y nombre de contacto asi como número de contacto";
      } else{
            $operacionesObj->nombreContacto=$_POST['nombreContactoID'];
            $operacionesObj->numeroContacto=$_POST['numeroContactoID'];
      }
  }else
  if($_POST['lugarInstacion']=='Concesionario'){
   $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];  
     $operacionesObj->estado=0;
  if (empty($_POST['departamentoInstalacionC'])||empty($_POST['provinciaInstalacionC'])||empty($_POST['distritoInstalacionC'])||empty($_POST['direccionInstalacionC'])||empty($_POST['referenciaInstalacionC']) || empty($_POST['concesionarioInstalacion']) ||empty($_POST['nombreContactoIC']) || empty($_POST['numeroContactoIC'])) {
    $errores[]="Registre correctamente el departamento, provicioa direccion y distrito de instalacion en CONCESIONARIO ademas del número de contacto y nombre de contacto PARA LA INSTALACIÓN";

 }else{
    $operacionesObj->nombreContacto=$_POST['nombreContactoIC'];
  $operacionesObj->numeroContacto=$_POST['numeroContactoIC'];
 }

}
///fin de comporvacion de lugar de instalación ----
    //Registrar datos de Vehiculo--
$res="";
if (isset($_POST['serviciosAdicionales'])) {
  foreach ($_POST['serviciosAdicionales'] as $var) {
   $res .=$var.", ";
 }     
}

$agregarVehículoObj->plan=htmlspecialchars($_POST['plan']);
$agregarVehículoObj->costo=htmlspecialchars($_POST['costo']);
$agregarVehículoObj->planTiempo=htmlspecialchars($_POST['tiempo']);
$agregarVehículoObj->placa=htmlspecialchars($_POST['placa']);
$agregarVehículoObj->chasis=htmlspecialchars($_POST['chasis']);
$agregarVehículoObj->motor=htmlspecialchars($_POST['motor']);
$agregarVehículoObj->marca=htmlspecialchars($_POST['marca']);
$agregarVehículoObj->modelo=htmlspecialchars($_POST['modelo']);
$agregarVehículoObj->color=htmlspecialchars($_POST['color']);
$agregarVehículoObj->anio=htmlspecialchars($_POST['anio']);
$agregarVehículoObj->serviciosAdicionales=$res;
$agregarVehículoObj->companiaSeguro=htmlspecialchars($_POST['seguro']);
$agregarVehículoObj->numeroTramiteSeguro=htmlspecialchars($_POST['numeroTramiteSeguro']);
$agregarVehículoObj->numeroPoliza=htmlspecialchars($_POST['numeroPoliza']);
$agregarVehículoObj->idcorredordeseguro=htmlspecialchars($_POST['corredor']);
$agregarVehículoObj->concesionario=htmlspecialchars($_POST['concesionario']);
$agregarVehículoObj->observaciones=htmlspecialchars($_POST['observaciones']);
 ///fin de datos del vehículo
/////poner en el calendario aqui ----

//

if(empty($errores)) {
  if ($agregarVehículoObj->registrarVehiculo($id)) {
    $fecha="";
    //subimos el archivo al servidor...
    if (!empty($_FILES['voucher']['name'])) {
      $fecha=date("YmdHis");
      move_uploaded_file($ubicacionTemporal, '../public/vouchers/'.$fecha.$nombreArchivo); 
    //registramos los datos de contabilidad
    }
       //Registro de datos de facturacion.
    $objClienteFacturacion = Cliente::obtenerDatosDeclientePorId($id);
    ///registramos o actualizamos primero el canal //
    


    ///registramos o actualizamos primero el canal END //
    ///registramos las comisiones aqui //

    ///registramos las comisiones aqui END//
    if ($_POST['tipoDocumentoFacturacion']=='Boleta') {
      $facturacionObj->idcliente=$id;
      $facturacionObj->idvehiculos=$agregarVehículoObj->idvehiculo;
       $facturacionObj->idUsuarioCreado=$session->idusuario;
      $facturacionObj->tipoDocumentoFacturacion=$_POST['tipoDocumentoFacturacion'];
      if ($objClienteFacturacion->tipoCliente=="Natural") {
        $facturacionObj->dniFacturacion=$objClienteFacturacion->dni;
        $facturacionObj->razonSocialFacturacion=$objClienteFacturacion->nombre." ".$objClienteFacturacion->apellidosC;
        $facturacionObj->direccionFiscal=$objClienteFacturacion->departamento." ".$objClienteFacturacion->provincia." ".$objClienteFacturacion->distrito." ".$objClienteFacturacion->direccion;

      }else{
        $facturacionObj->dniFacturacion=$objClienteFacturacion->dni;
        $facturacionObj->razonSocialFacturacion=$objClienteFacturacion->nombre." ".$objClienteFacturacion->apellidosC;
        $facturacionObj->direccionFiscal=$objClienteFacturacion->departamento." ".$objClienteFacturacion->provincia." ".$objClienteFacturacion->distrito." ".$objClienteFacturacion->direccion;
      }    
      $facturacionObj->telefonoFacturacion=$objClienteFacturacion->telefono;
      if (!empty($_FILES['voucher']['name'])) {        
        $facturacionObj->voucher=$fecha.$nombreArchivo;
      }

      $facturacionObj->direccionEntrega=$_POST['direccionEntregaFactura'];
      $facturacionObj->plan=$_POST['plan'];
      $facturacionObj->tiempo=$_POST['tiempo'];
      $facturacionObj->costo=$_POST['costo'];
      $facturacionObj->observaciones=$_POST['observaciones'];
      if ($facturacionObj->registrarDatosDeFacturacion()) {
            # code...
      }
    }else
    if($_POST['tipoDocumentoFacturacion']!='Boleta'){
      $facturacionObj->idcliente=$id;
      $facturacionObj->tipoDocumentoFacturacion=$_POST['tipoDocumentoFacturacion'];
      $facturacionObj->dniFacturacion=$_POST['rucFacturacion'];
      $facturacionObj->rucFacturacion=$_POST['rucFacturacion'];
      $facturacionObj->razonSocialFacturacion=$_POST['razonSocialFacturacion'];
      $facturacionObj->direccionFiscal=$_POST['direccionFiscalFacturacion'];
      $facturacionObj->telefonoFacturacion=$_POST['telefonoFacturacion'];
      if (!empty($_FILES['voucher']['name'])) { 
        $facturacionObj->voucher=$fecha.$nombreArchivo;
      }
      $facturacionObj->direccionEntrega=$_POST['direccionEntregaFactura'];
      $facturacionObj->plan=$_POST['plan'];
      $facturacionObj->tiempo=$_POST['tiempo'];
      $facturacionObj->costo=$_POST['costo'];        
      $facturacionObj->observaciones=$_POST['observaciones'];
      $facturacionObj->idUsuarioCreado=$session->idusuario;
      if ($facturacionObj->registrarDatosDeFacturacion()) {
            # code...
      }
    }else{
      $errores="Fail detected facturacion data";
    }
 ///asignamos la orden de instacion 


  //////canal canal
    if (isset($_POST['canalId'])) {      
      $datosCanales=$objCanales->otenerDatosCanal($_POST['canalId']);
    }else{
      $datosCanales=false;
    }
    
    if ($datosCanales) {
      //Update
      $objCanales->idcanal=$datosCanales->idcanal;   
      $facturacionObj->idcanal=$objCanales->idcanal;
      if (!$facturacionObj->agregarIdCanal()){ $errores[]="Error en agregar canal id ";}
      if (isset($_POST['comision'])) {
        $objCanales->banco=strtoupper($_POST['bancoCanal']);
        $objCanales->numeroCuenta=$_POST['numeroCuenta'];
        $objCanales->moneda=strtoupper($_POST['monedaCanal']);
        $objCanales->cci=strtoupper($_POST['cci']);
        $objCanales->canalId=strtoupper($_POST['canalId']);
        $objCanales->ejecutivoCanal=strtoupper($_POST['ejecutivoCanal']);
        $objCanales->titular=strtoupper($_POST['ejecutivoCanal']);
        $objCanales->nombreCanal=strtoupper($_POST['nombreCanal']);
      }      
      if (!$objCanales->actualizarDatosCanal()) {
        $errores[]="Error en actualizar datos de canal";
      }else{
         //registramos las comisiones para este canal  con este cliente y vehículo :V 
        if (isset($_POST['comision'])) {
          $objComision->idcliente=$id;
          $objComision->idcanal=$datosCanales->idcanal;
          $objComision->idvehiculo=$agregarVehículoObj->idvehiculo;
          $objComision->idfacturacion=$facturacionObj->datosfacturacionid;
          $objComision->montoTotal=$_POST['costo'];
          $objComision->comision=$_POST['montoComision'];
          if (!empty($_FILES['voucher']['name'])) {
            $objComision->fechapago=date("YmdHis");
          }
          $objComision->documentoFacturacion=$_POST['tipoDocumentoFacturacion'];
          if (!$objComision->registrarComision()) {
            $errores[]="Error en registrar datos de comision";
          }
        }


      }

    }else{
      //Ingresar nuevo
      $objCanales->tipoCanal=$_POST['tipoCanal'];
      $objCanales->nombreCanal=strtoupper($_POST['nombreCanal']);
      $objCanales->ejecutivoCanal=strtoupper($_POST['ejecutivoCanalN']);
      // $objCanales->ejecutivoCanal=strtoupper($_POST['ejecutivoCanal']);
      if (isset($_POST['comision'])) {
        $objCanales->cobraComision=$_POST['comision'];
        $objCanales->banco=strtoupper($_POST['bancoCanal']);
        $objCanales->numeroCuenta=$_POST['numeroCuenta'];
        $objCanales->moneda=strtoupper($_POST['monedaCanal']);
        $objCanales->cci=strtoupper($_POST['cci']);
        $objCanales->canalId=strtoupper($_POST['canalId']);
        $objCanales->ejecutivoCanal=strtoupper($_POST['ejecutivoCanal']);
      }      
      if (!$objCanales->registrarCanal()) {
        $errores[]="Error en registrar canal";
      }else{
        //Registramos las comsiones ...
    $facturacionObj->idcanal=$objCanales->idcanal;
    if (!$facturacionObj->agregarIdCanal()){ $errores[]="Error en agregar canal id ";}

        if (isset($_POST['comision'])) {
          $objComision->idcliente=$id;
          $objComision->idcanal=$objCanales->idcanal;
          $objComision->idvehiculo=$agregarVehículoObj->idvehiculo;
          $objComision->idfacturacion=$facturacionObj->datosfacturacionid;
          $objComision->montoTotal=$_POST['costo'];
          $objComision->comision=$_POST['montoComision'];
          if (!empty($_FILES['voucher']['name'])) {
            $objComision->fechapago=date("YmdHis");
          }
          $objComision->documentoFacturacion=$_POST['tipoDocumentoFacturacion'];
          if (!$objComision->registrarComision()) {
            $errores[]="Error en registrar datos de comision";
          }
        }
      }

    }

    ///fin canal canal comision, etc


//finalizamos la orden de instalacion aqui ....

    unset($_POST['costo']);
    unset($_POST['tiempo']);      
    unset($_POST['placa']);
    unset($_POST['chasis']);
    unset($_POST['motor']);
    unset($_POST['marca']);
    unset($_POST['modelo']);
    unset($_POST['anio']);
    unset($_POST['color']);
    unset($_POST['seguro']);
    unset($_POST['numeroPoliza']);
    unset($_POST['numeroTramiteSeguro']);
    unset($_POST['concesionario']);
    unset($_POST['observaciones']);

  } else {
    $errores[]="Error en el registro de vehículo";
  }
  // enviarMensaje("Vehiculo registrado, instalacion programada.");
     ///Inicio Registrar datos de instalcion here ----

  $operacionesObj->datosfacturacionid=$facturacionObj->datosfacturacionid;
  $operacionesObj->programarInstalacion($agregarVehículoObj->idvehiculo,$id);



     ///Fin Registrar datos de instalcion here ----
  $echo[]="Vehiculo registrado, instalacion programada";
  // header("Location:registrarVehiculo.php?id=$id&estado=ok");


}
}else{
  $errores[]="Error el formulario ya ha sido enviado. Error Token"  ;
}

} 

$vehiculoObj= new Vehiculo();

$cliobj=new Cliente();
if (isset($_POST['actualizarDatos'])) {
  if ($_POST['tipoCliente']=="Natural") {
    $cliobj->nombre=$_POST['nombreE'];
    $cliobj->apellidosC=$_POST['apellidoE'];
    $cliobj->departamento=$_POST['departamentoE'];
    $cliobj->provincia=$_POST['provinciaE'];
    $cliobj->distrito=$_POST['distritoE'];
    $cliobj->direccion=$_POST['direccionE'];
    $cliobj->referencia=$_POST['ReferenciaE'];
    $cliobj->telefono=$_POST['telefonoE'];
    $cliobj->telefono1=$_POST['telefonoE1'];
    $cliobj->correo=$_POST['correoE'];
    $cliobj->correo1=$_POST['correoE1'];
    if($cliobj->actualizarDatosNatural($id)){
          #mensaje here -->
          // echo "gogogog";
    }else{
      die('fail en actualizar datos');
    }



  }else
  if($_POST['tipoCliente']<>"Natural"){
    $cliobj->razonSocial=$_POST['razonSocialE'];
    $cliobj->sectorDesempenia=$_POST['sectorDesempeniaE'];
    $cliobj->departamentoL=$_POST['departamentoLE'];
    $cliobj->provinciaL=$_POST['provinciaLE'];
    $cliobj->distritoL=$_POST['distritoLE'];
    $cliobj->direccionL=$_POST['direccionLE'];
    $cliobj->telefono=$_POST['telefonoE'];
    $cliobj->telefono1=$_POST['telefonoE1'];
    $cliobj->correo=$_POST['correoE'];
    $cliobj->correo1=$_POST['correoE1'];
        //representante legal here <---->
    $cliobj->nombre=$_POST['nombreE'];
    $cliobj->apellidosC=$_POST['apellidoE'];
    $cliobj->departamento=$_POST['departamentoE'];
    $cliobj->provincia=$_POST['provinciaE'];
    $cliobj->distrito=$_POST['distritoE'];
    $cliobj->direccion=$_POST['direccionE'];
    $cliobj->referencia=$_POST['referenciaE'];
    $cliobj->telefono2=$_POST['telefonoE2'];
    $cliobj->telefono3=$_POST['telefonoE3'];
    $cliobj->correo2=$_POST['correoE2'];
    $cliobj->correo3=$_POST['correoE3'];

    if ($cliobj->actualizarDatosEmpresa($id)) {
            // echo "gogog";
    }else{
      die("this is the fail actualizar empresa :V ");
    }
      // $cliobj->actualizarDatosEmpresa($id);


  }else{
    echo "fail detected ";
  }


} 
?>
  <link rel="stylesheet" href="../assets/css2/style2.css">
  <link rel="stylesheet" href="../assets/css2/style3.css">
  <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
  <link href="../assets/css/plugins/select2/select2.min.css" rel="stylesheet">
  <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>


  <body>

    <div id="wrapper">
      <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
          <?php include("../includes/menuIzquierdo.php") ?>
        </div>
      </nav>
      <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
          <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include("../includes/navegacionArriba.php") ?>

          </nav>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">

          <div class="row">
           <div class="col-lg-12">
             <div class="ibox float-e-margins">

               <div class="ibox-title">
                <div class="row">
                  <div class="col-lg-4 col-sm-4"><h3>Agregar vehículo a cliente </h3></div><div class="col-md-2 col-md-offset-6 col-sm-1 col-sm-offset-6"><div class="btn btn-info" name="registrar" data-toggle="modal" data-target="#actualizarDataModal" >Actualizar datos </div></div>

                </div>

              </div>
              <div class="ibox-content">

                <div class="well">
                  <?php 
                  if ($clienteObj=Cliente::obtenerDatosDeclientePorId($id)) {
                    ?>
                    <h3>
                      Datos de cliente: <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? "Natural " : "Empresa " ; ?>
                    </h3>
                    <div class="row">
                      <div class="row">
                        <div class="col-lg-6 text-primary "><h2><?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? "DNI: " : "RUC: " ; ?> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? $clienteObj->dni : $clienteObj->ruc ; ?></h2></div>   
                        <div class="col-lg-6 text-primary text-capitalize"><h2> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? "Nombres: " : "Razon social: " ; ?> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? $clienteObj->nombre." ".$clienteObj->apellidosC : $clienteObj->razonSocial ; ?></h2></div>   
                      </div>
                      <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4"></div>
                      </div>

                      <?php

                    }else{
                      ?>
                      <div class="row">
                        <div class="col-lg-12 text-primary  text-center"><h2> Algo anda mal escriba a wilmanpabel@gmail.com</h2></div>   
                        <?php

                      }

                      ?>

                    </div>
                  </div>
                  <hr>
                  <div class="row">     
                    <div class="col-md-12"><div class="btn btn-primary col-md-12" name="registrar" data-toggle="modal" data-target="#autorizados" >Contactos autorizados <i class="fa fa-child"> </i></div>
                    <h3>Cantidad de contactos autorizados actuales <?php  $o=Cliente::obtenerCantidadContactosAutorizados($id); echo $o->cantidad; ?>
                     <?php if ($o->cantidad==0): ?>
                       <span class="label label-warning">Ingrese al menos 2 contactos autorizados</span>
                     <?php endif ?>
                   </h3></div>
                   <br><br><hr><hr><hr>
                 </div>
                 <form action="" method="post" enctype="multipart/form-data" id="registrarVehiculoF">
                  <div class="row">
                    <div class="col-lg-12"> 

                      <div class="row">

                        <div class="form-group col-md-2">
                          <label for="plan" class="sr-only">Plan</label>


                          <select  style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
                            <?php if (isset($agregarVehículoObj->plan)): ?>
                              <option class="" value="<?php echo $agregarVehículoObj->plan ?>"  selected ><?php echo $agregarVehículoObj->plan ?></option>
                            <?php endif ?>
                            <option class="" value="" disabled selected >Seleccione Plan *</option>
                            <?php Cliente::obtenerPlan() ?>      
                          </select>
                        </div>


                        <div class="form-group col-md-1">
                          <label for="plan" class="sr-only">Costo</label>
                          <input style="border: none;" type="number" placeholder="Costo" id="costo" name="costo" class="costo border-bottom form-control" required autofocus autocomplete="off" value="<?php echo $retVal = (isset($_POST['costo'])) ? $_POST['costo'] : "" ; ?>" >
                        </div>
                        <div class="form-group col-md-1">
                          <label for="tiempo" class="sr-only">Tiempo</label>
                          <input style="border: none;" type="text" placeholder="Tiempo" id="tiempo" name="tiempo" class="tiempo border-bottom form-control"  required autocomplete="off" value="<?php echo $retVal = (isset($_POST['tiempo'])) ? $_POST['tiempo'] : "" ; ?>" >
                        </div>
                        <div class="form-group col-md-2">
                          <label for="placa" class="sr-only">Placa</label>
                          <input style="border: none;" type="text" placeholder="Placa " id="placa" name="placa" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $retVal = (isset($_POST['placa'])) ? $_POST['placa'] : "" ; ?>" >
                        </div>
                        <div class="form-group col-md-3">
                          <label for="chasis" class="sr-only">Chasis</label>
                          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="chasis" class="border-bottom form-control text-uppercase" required autocomplete="off" value="<?php echo $retVal = (isset($_POST['chasis'])) ? $_POST['chasis'] : "" ; ?>">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="motor" class="sr-only">Motor</label>
                          <input style="border: none;" type="text" placeholder="Motor" id="motor" name="motor" class="border-bottom form-control text-uppercase" required autocomplete="off"  value="<?php echo $retVal = (isset($_POST['motor'])) ? $_POST['motor'] : "" ; ?>" >
                        </div>

                      </div> 
                      <div class="row">

                        <div class="form-group col-md-3">
                          <label for="marca" class="sr-only">Marca</label>
                          <input style="border: none;" type="text" placeholder="Marca" id="marca" name="marca" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $retVal = (isset($_POST['marca'])) ? $_POST['marca'] : "" ; ?>" >
                        </div>
                        <div class="form-group col-md-3">
                          <label for="modelo" class="sr-only">Modelo</label>
                          <input style="border: none;" type="text" placeholder="Modelo" id="modelo" name="modelo" class="border-bottom form-control text-capitalize"  required value="<?php echo $retVal = (isset($_POST['modelo'])) ? $_POST['modelo'] : "" ; ?>" >
                        </div>
                        <div class="form-group col-md-2">
                          <label for="color" class="sr-only">Color</label>
                          <input style="border: none;" type="text" placeholder="Color " id="color" name="color" class="border-bottom form-control x2 text-capitalize" required value="<?php echo $retVal = (isset($_POST['color'])) ? $_POST['color'] : "" ; ?>" >
                        </div>
                        <div class="form-group col-md-2">
                          <label for="anio" class="sr-only">Año de fabricacion</label>
                          <input style="border: none;" type="text" placeholder="Año de fabricacion" id="anio" name="anio" class="border-bottom form-control" required value="<?php echo $retVal = (isset($_POST['anio'])) ? $_POST['anio'] : "" ; ?>" >
                        </div>
                        <div class="form-group col-md-2">
                          <label for="seguro" class="sr-only">Compañia de seguros</label>
                          <input style="border: none;" type="text" placeholder="Compañia de seguros" id="seguro" name="seguro" class="seguro border-bottom form-control"  autocomplete="off" value="<?php echo $retVal = (isset($_POST['seguro'])) ? $_POST['seguro'] : "" ; ?>" >
                        </div>
                      </div>                                      


                      <div class="row">
                        <div class="form-group col-md-3">
                          <label for="numeroTramiteSeguro" class="sr-only">Numero tramite de seguro</label>
                          <input style="border: none;" type="text" placeholder="Numero tramite de seguro" id="numeroTramiteSeguro" name="numeroTramiteSeguro" class="numeroTramiteSeguro border-bottom form-control text-capitalize" value="<?php echo $retVal = (isset($_POST['numeroTramiteSeguro'])) ? $_POST['numeroTramiteSeguro']: "" ; ?>" >
                        </div>
                        <div class="form-group col-md-3">
                          <label for="numeroPoliza" class="sr-only">N° de poliza</label>
                          <input style="border: none;" type="text" placeholder="N° de poliza" id="numeroPoliza" name="numeroPoliza" class="border-bottom form-control text-capitalize" value="<?php echo $retVal = (isset($_POST['numeroPoliza'])) ? $_POST['numeroPoliza'] : "" ; ?>" >
                        </div>
                        <div class="form-group col-md-3">
                          <label for="corredor" class="sr-only">Corredor de seguros</label>
                          <select  style="border: none;" class="corredor border-bottom form-control text-capitalize" required name="corredor">
                            <option value="" disabled selected>Seleccione datos de facturación *</option>
                            <option value="No tiene, No recuerda">No tiene, No recuerda</option>
                            <?php Vehiculo::obtenerCorredores() ?>
                          </select>
                        </div>
                        <div class="col-md-3 border-bottom" ><label for="concesionario" class="sr-only">Concesionario</label>
                          <input style="border: none;" type="text" placeholder="Concesionario *" id="condscesionario" name="concesionario" class="concesionario border-bottom form-control" required autocomplete="off" value="<?php echo $retVal = (isset($_POST['concesionario'])) ? $_POST['concesionario'] : "" ; ?>" >
                        </div>


                      </div> 
                      <div class="row">
                        <div class="col-md-3 border-bottom" >                                   
                          <select class="servicioAdicional form-control border-bottom" multiple="multiple" style="border-top-width: 0" name="serviciosAdicionales[]">
                            <option value="Localizacion Web">Localizacion Web</option>
                            <option value="Boton de panico">Boton de panico</option>
                            <option value="Kit habla escucha">Kit habla escucha</option>
                            <option value="Bloqueo preventivo">Bloqueo preventivo</option>
                            <option value="Sensor de combustible">Sensor de combustible</option>
                            <option value="Sesor apertura de tapa">Sesor apertura de tapa</option>
                            <option value="Apertura de pestillos">Apertura de pestillos</option>
                            <option value="lerta sonora por exeso de velocidad">Alerta sonora por exeso de velocidad</option>
                          </select>
                        </div>
                        <div class="col-lg-3">
                          <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                              <i class="glyphicon glyphicon-file fileinput-exists"></i>
                              <span class="fileinput-filename"></span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                              <span class="fileinput-new">Seleccionar Voucher de pago</span>
                              <span class="fileinput-exists">Cambiar</span>
                              <input type="file" name="voucher" required id="voucherImg" />
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
                          </div> 
                        </div>
                        <div class="col-md-2">
                          <label for="pagoencaja">Pago en Caja</label>
                          <div class="switch">
                            <div class="onoffswitch">
                              <input type="checkbox" class="onoffswitch-checkbox" id="pagoencaja" value="si" name="ad">
                              <label class="onoffswitch-label" for="pagoencaja">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group col-md-4">
                          <label for="observaciones" class="sr-only">Observacion</label>
                          <input style="border: none;" type="text" placeholder="Observacion" id="observaciones" name="observaciones" class="border-bottom form-control text-capitalize"   value="<?php echo $retVal = (isset($_POST['observaciones'])) ? $_POST['observaciones'] : "" ; ?>">
                        </div>
                        </div>
                        <hr><h2 class="text-center">Datos facturación</h2><hr>

                      <div class="row">
                        <div class="col-md-4">
                          <select style="border: none;" id="selectNa" name="tipoDocumentoFacturacion" class="cd-selec form-control border-bottom" required >
                            <option value="" disabled selected>Seleccione datos de facturación *</option>
                            <option value="Boleta">Boleta</option>
                            <option value="Factura">Factura</option>
                          </select>   
                        </div>
                        <div class="col-md-8">
                          <div class="content-pane e1">
                            <h3>Seleccione pago</h3>
                          </div>
                          <div class="content-pane eBoleta">
                            <h3>Continuar</h3>
                          </div>
                          <div class="content-pane eFactura">
                            <div class="row"><br>
                              <div class="form-group col-md-4">
                                <label for="rucFacturacion" class="sr-only">Ruc *</label>
                                <input style="border: none;" type="text" placeholder="Ruc *" id="rucFacturacion" name="rucFacturacion" class="form-control x2 border-bottom"  value="<?php echo $retVal = (isset($clienteObj->ruc)) ? $clienteObj->ruc : "" ; ?>" >
                              </div>
                              <div class="form-group col-md-8">
                                <label for="razonSocialFacturacion" class="sr-only">Razon Social *</label>
                                <input style="border: none;" type="text" placeholder="Razon Social *" id="razonSocialFacturacion" name="razonSocialFacturacion" class="form-control border-bottom text-capitalize" value="<?php echo $retVal = (isset($clienteObj->razonSocial)) ? $clienteObj->razonSocial : "" ; ?>" >
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-md-8">
                                <label for="direccionFiscalFacturacion" class="sr-only">Direccion Fiscal *</label>
                                <input style="border: none;" type="text" placeholder="Direccion *" id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="form-control border-bottom text-capitalize"  value="<?php echo $retVal = (isset($clienteObj->departamentoL)) ? $clienteObj->direccionL : '' ; ?>">
<!-- 
  value="<?php echo $retVal = (isset($clienteObj->departamentoL)) ? $clienteObj->departamentoL.' '.$clienteObj->provinciaL.' '.$clienteObj->distritoL.' '.$clienteObj->direccionL : '' ; ?>" -->

</div>
<div class="form-group col-md-4">
  <label for="telefonoFacturacion" class="sr-only">Telefono </label>
  <input style="border: none;" type="text" placeholder="Telefono" id="telefonoFacturacion" name="telefonoFacturacion" class="form-control border-bottom text-capitalize" value="<?php echo $retVal = (isset($clienteObj->telefono)) ? $clienteObj->telefono : "" ; ?>">

</div>
</div>
<div class="row">
  <div class="form-group col-md-12">
    <label for="direccionEntregaFactura" class="sr-only">Direccion de entrega de Factura </label>
    <input style="border: none;" type="text" placeholder="Direccion de entrega de Factura" id="direccionEntregaFactura" name="direccionEntregaFactura" class="form-control border-bottom text-capitalize"  >
  </div>
</div>

</div> 
</div>  
</div><br> <br>
<hr><h2 class="text-center">Datos de canal</h2><hr>
<div class="row">
  <div class="form-group col-md-2">
    <select style="border: none;"  name="tipoCanal" class="cd-selec form-control border-bottom" required id="tipoCanal">
      <option value="" disabled selected>Seleccione canal *</option>
      <option value="BROKER">Broker</option>
      <option value="CONCESIONARIO">Concesionario</option>
      <option value="CORPORATIVO">Corporativo</option>
      <option value="OTROS">Otros</option>
    </select>   
  </div>
  <div class="form-group col-md-2">
    <label for="nombreCanal" class="sr-only">Canal nombre</label>
    <input style="border: none;" type="text" placeholder="Canal nombre" id="nombreCanal" name="nombreCanal" class="border-bottom form-control text-uppercase nombreCanal" autocomplete="off" required="" >
  </div>
  <div class="form-group col-md-3">
    <label for="ejecutivoCanalN" class="sr-only">Ejecutivo canal</label>
    <input style="border: none;" type="text" placeholder="Ejecutivo canal" id="ejecutivoCanalN" name="ejecutivoCanalN" class="border-bottom form-control text-uppercase ejecutivoCanalN" autocomplete="off" required="" >
  </div>
  <div class="col-md-2">
    <label for="comision">Cobra Comision</label>
    <div class="switch">
      <div class="onoffswitch">
        <input type="checkbox" class="onoffswitch-checkbox" id="comision" value="si" name="comision"  checked="true" onclick="validarComision()">
        <label class="onoffswitch-label" for="comision">
          <span class="onoffswitch-inner"></span>
          <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </div>
  </div>
  <div class="form-group col-md-3">
    <label for="canalId" class="sr-only">DNI o RUC</label>
    <input style="border: none;" type="number" placeholder="DNI o RUC" id="canalId" name="canalId" class="canalId border-bottom form-control text-capitalize text-uppercase" autocomplete="off" required="" >
  </div>

</div>
<div class="row" id="comisionHtml">
</div>
<hr><div class="row">
<div class="col-md-12 text-center"><h2>Datos de instalación</h2></div>
</div><hr>


<div role="tablist">

  <input type="radio" title="taller" name="lugarInstacion" id="taller" role="tab" aria-controls="taller-panel" checked value="Taller" />
  <input type="radio" title="domicilio" name="lugarInstacion" id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente" />
  <input type="radio" title="concesionario" name="lugarInstacion" id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario" />

  <div class="tabs" aria-hidden="true">
    <label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
    <label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
    <label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
  </div>
  <div class="panels">
    <ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">
      <h2>TALLER</h2>
      <p>Paseo de la republica 4925 </p>
      <p>Surquillo, Lima, Lima</p>
      <input type="hidden" name="direccionTaller" value="Paseo de la Republica 4925,Surquillo, Lima, Lima">

      <div class="row">
        <div class="row">
          <div class="form-group col-md-4">
            <input style="border: none;" type="text" placeholder="Seleccione Fecha instalacion *"  name="FechaInstalacion" class="form-control x2 text-capitalize border-bottom" type="text" value="" id="datetimepicker_mask" autocomplete="off"   />
            <label for="datetimepicker_mask" class="sr-only">Dia de instalacion*</label>
          </div>                                        
          <div class="form-group col-md-4">
            <label for="nombreContactoIT" class="sr-only">Nombre Contacto *</label>
            <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIT" name="nombreContactoIT" class="form-control x2 text-capitalize border-bottom"  required="">
          </div><div class="form-group col-md-4">
          <label for="numeroContactoIT" class="sr-only">Número Contacto *</label>
          <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContactoIT" name="numeroContactoIT" class="form-control x2 text-capitalize border-bottom"  required="">
        </div>
      </div>
    </div>



  </ul>
  <ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab"> 
    <h2>Domicilio Cliente</h2>
    <?php 
    $objDatosC = Cliente::obtenerDatosDeclientePorId($id);

    ?>
    <div class="row">
      <div class="row">
        <div class="form-group  col-md-4">
          <label for="departamentoInstalacionD" class="sr-only">Departamento*</label>
          <input style="border: none;" type="text" placeholder="Departamento*" id="departamentoInstalacionD" name="departamentoInstalacionD"  class="form-control border-bottom" value="<?php echo $objDatosC->departamento ?>" >
        </div>
        <div class="form-group  col-md-4">
          <label for="provinciaInstalacionD" class="sr-only">Provincia*</label>
          <input style="border: none;" type="text" placeholder="Provincia*" id="provinciaInstalacionD" name="provinciaInstalacionD" class="form-control border-bottom" value="<?php echo $objDatosC->provincia ?>" >
        </div>
        <div class="form-group  col-md-4">
          <label for="distritoInstalacionD" class="sr-only">Distrito*</label>
          <input style="border: none;" type="text" placeholder="Distrito*" id="distritoInstalacionD" name="distritoInstalacionD" class="form-control border-bottom" value="<?php echo $objDatosC->distrito ?>" >
        </div>

      </div>
      <div class="row">
        <div class="form-group  col-md-5">
          <label for="direccionInstalacionD" class="sr-only">Direccion*</label>
          <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionD" name="direccionInstalacionD" class="form-control border-bottom" value="<?php echo $objDatosC->direccion ?>" >
        </div>
        <div class="form-group  col-md-5">
          <label for="referenciaInstalacionD" class="sr-only">Referencia *</label>
          <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" name="referenciaInstalacionD" class="form-control border-bottom" value="<?php echo $objDatosC->referencia ?>" >
        </div>
        <div class="form-group  col-md-2">
          <label for="coordenadasInstalacion" class="sr-only">Cordenadas Instalacion *</label>
          <input style="border: none;" type="text" placeholder="Cordenadas Instalacion *" id="coordenadasInstalacion" name="coordenadasInstalacion" class="form-control border-bottom"  >
        </div>
      </div>      
      <div class="row">

        <div class="form-group col-md-4">
          <label for="datetimepicker_mask1" class="sr-only">Dia de instalacion*</label>
          <input style="border: none;" type="text" placeholder="Seleccione Fecha instalacion *"  name="FechaInstalacionD" class="form-control x2 text-capitalize border-bottom" type="text" value="" id="datetimepicker_mask1" autocomplete="off"  />
        </div>                      
        <div class="form-group col-md-4">
          <label for="nombreContactoID" class="sr-only">Nombre Contacto *</label>
          <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoID" name="nombreContactoID" class="form-control x2 text-capitalize border-bottom" required="">
        </div><div class="form-group col-md-4">
        <label for="numeroContactoID" class="sr-only">Número Contacto *</label>
        <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContactoID" name="numeroContactoID" class="form-control x2 text-capitalize border-bottom" required="">
      </div>
    </div>
  </div>




</ul>
<ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">

  <div class="row">
    <div class="col-md-6"><h2>Concecionario</h2></div>
    <div class="col-md-6">
      <label for="concesionarioInstalacion" class="sr-only">Nombre de concesionario*</label>
      <input style="border: none;" type="text" placeholder="Nombre de concesionario*" id="concesionarioInstalacion" name="concesionarioInstalacion" class="concesionario form-control border-bottom" autocomplete="off" >
    </div>
  </div>      

  <div class="row">
    <div class="row">
      <div class="form-group  col-md-4">
        <label for="departamentoInstalacionC" class="sr-only">Departamento*</label>
        <input style="border: none;" type="text" placeholder="Departamento*" id="departamentoInstalacionC" name="departamentoInstalacionC" class="form-control border-bottom" value="Lima" >
      </div>
      <div class="form-group  col-md-4">
        <label for="provinciaInstalacionC" class="sr-only">Provincia*</label>
        <input style="border: none;" type="text" placeholder="Provincia*" id="provinciaInstalacionC" name="provinciaInstalacionC" class="form-control border-bottom"  value="Lima" >
      </div>
      <div class="form-group  col-md-4">
        <label for="distritoInstalacionC" class="sr-only">Distrito*</label>
        <input style="border: none;" type="text" placeholder="Distrito*" id="distritoInstalacionC" name="distritoInstalacionC" class="form-control border-bottom"  >
      </div>

    </div>
    <div class="row">
      <div class="form-group  col-md-6">
        <label for="direccionInstalacionC" class="sr-only">Direccion*</label>
        <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom" >
      </div>
      <div class="form-group  col-md-6">
        <label for="referenciaInstalacionC" class="sr-only">Referencia *</label>
        <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom" >
      </div>
    </div>      
    <div class="row">
     <div class="form-group col-md-4">
      <label for="datetimepicker_mask2" class="sr-only">Dia de instalacion*</label>
      <input style="border: none;" type="text" placeholder="Seleccione Fecha instalacion *"  name="FechaInstalacionC" class="form-control x2 text-capitalize border-bottom" type="text" value="" id="datetimepicker_mask2" autocomplete="off" />
    </div>  

    <div class="form-group col-md-4">
      <label for="nombreContactoIC" class="sr-only">Nombre Contacto *</label>
      <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIC" name="nombreContactoIC" class="form-control x2 text-capitalize border-bottom" required="" >
    </div><div class="form-group col-md-4">
    <label for="numeroContactoIC" class="sr-only">Número Contacto *</label>
    <input style="border: none;" type="number" placeholder=" Número Contacto *" id="numeroContactoIC" name="numeroContactoIC" class="form-control x2 text-capitalize border-bottom" required="">
  </div>
</div>
</div>
</ul>
</div>

</div>
<div class="row">
 <div class="form-group col-md-12">
   <input type="hidden" name="token" value="<?php echo Token::generar() ?>">
   <button class="add_button btn btn-info col-md-12" title="Agregar Vehiculos" name="agregarVehículo" onclick="validarPago()">Agregar <i class="fa fa-plus-circle "></i></button> 

 </div>
</div>
</div>
</div>  
</form>
<div class="hr-line-dashed"></div>
<h2 class="text-center">Vehiculos existentes de cliente</h2>
<div class="hr-line-dashed"></div>
<?php if ($vehiculoObj->verificarExistenciaVehiculos($id)): ?>
  <div class="ibox float-e-margins">

    <div class="ibox-content">
      <input type="text" class="form-control input-sm m-b-xs" id="filter"
      placeholder="Filtrar vehículo">

      <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
        <thead>
          <tr>
            <th>#</th>
            <th>Placa</th>
            <th>Marca(s)</th>
            <th>modelo(s)</th>
            <th>Estado(s)</th>
            <th data-hide="all">Plan</th>
            <th data-hide="all">Plan Tiempo</th>
            <th data-hide="all">Chasis</th>
            <th data-hide="all">Motor</th>
            <th data-hide="phone,tablet">Color</th>
            <th data-hide="phone,tablet">Año</th>
            <th data-hide="all">Seguro</th>
            <th data-hide="all">Poliza n°</th>
            <th data-hide="all">Corredor de seguro</th>
            <!-- <th data-hide="all">Task</th> -->
          </tr>
        </thead>    
        <tbody>

          <?php $vehiculoObj->obtenerVehiculos($id) ?>

        </tbody>
        <tfoot>
          <tr>
            <td colspan="5">
              <ul class="pagination pull-right"></ul>
            </td>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>


<?php else: ?>
</div>
<?php endif ?>





<!-- MODAL DE CONTACTOS AUTORIZADOS -->

<div class="modal inmodal" id="autorizados" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated flipInX">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

        <h4 class="modal-title">Contactos Autorizados</h4>
        <small class="font-bold">Ingrese aquí uno o varios contactos autorizados.   <div class="form-group ">

         <a href="javascript:void(0);" class="add_button btn btn-info col-md-12 " title="Agregar contacto">Agregar <i class="fa fa-plus-circle "></i></a>
       </div></small>
     </div>
     <form  role="form" method="post" >

      <div class="modal-body" style="background: #fff">
        <?php 

        echo '<div class="field_wrapper">';
        if($cliobj->mostrarContactosAutorizados($id)){  
         echo '</div>';
       }else{
         ?>
         <div class="field_wrapper">
          <div class="row">
            <div class="form-group col-md-3">
              <label for="dniC" class="sr-only">DNI no Obligatorio</label>
              <input style="border: none;" type="text" placeholder="DNI no Obligatorio" id="dniC" name="field_name[0][]" class="form-control border-bottom "   data-mask="99999999" id="dniI" >
            </div>
            <div class="form-group col-md-3">
              <label for="nombreC" class="sr-only">Nombres *</label>
              <input style="border: none;" type="text" placeholder="Nombres *" id="nombreC" name="field_name[1][]" class="form-control border-bottom text-capitalize"  required id="name" >
            </div>
            <div class="form-group col-md-3">
              <label for="apellidos" class="sr-only">Apellidos *</label>
              <input style="border: none;" type="text" placeholder="Apellidos *" id="apellidos" name="field_name[2][]" class="form-control x2 text-capitalize border-bottom text-capitalize" required >
            </div>
            <div class="form-group col-md-3">
             <label for="dniC" class="sr-only">Celular *</label>
             <input style="border: none;" type="text" placeholder="celular *" id="celular" name="field_name[3][]" class="form-control border-bottom " required  data-mask="999 999 999" id="celular" >
           </div>

         </div> 
         <div class="row">

          <div class="form-group col-md-4">
            <label for="telefono" class="sr-only">Telefono</label>
            <input style="border: none;" type="text" placeholder="telefono" id="telefono" name="field_name[4][]" class="form-control border-bottom text-capitalize"   id="telefono" >
          </div>
          <div class="form-group col-md-4">
            <label for="correo" class="sr-only">Correo</label>
            <input style="border: none;" type="text" placeholder="Correo" id="correo" name="field_name[5][]" class="form-control x2 text-capitalize border-bottom text-capitalize" >
          </div>               
          <div class="form-group col-md-4">
           <label for="Parentesco" class="sr-only">Parentesco *</label>
           <select  style="border: none;" class="tecnico border-bottom form-control" required name="field_name[6][]">
            <option class="" value="" disabled selected >Seleccione Parentesco *</option>
            <option value="Usuario">Usuario(a)</option>           
            <option value="padre">Padre</option>           
            <option value="Madre">Madre</option>           
            <option value="Hermano">Hermano(a)</option>           
            <option value="Conyuge">Conyuge</option>           
            <option value="Hijo">Hijo(a)</option>           
            <option value="Nieto">Nieto(a)</option>           
            <option value="Cuñado">Cuñado(a)</option>           
            <option value="Suegro">Suegro(a)</option>           
          </select>
        </div>


        <div class="hr-line-dashed"></div>
        <hr>
      </div>
    </div>
    <?php 
  } ?>

</div>

<div class="modal-footer">
  <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
  <button type="submit" name="registrarContacto" class="btn btn-primary">Guardar Cambios</button>
</div>
</form>   
</div>
</div>
</div>
<!-- FIN DE CONTACTOS AUTORIZADOS -->





</div>
</div>

</div>
<?php include("../includes/footer.php") ?>

</div>
</div>
<div class="modal inmodal" id="actualizarDataModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content animated swing">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

        <h4 class="modal-title">Actualizar datos</h4>
        <small class="font-bold">Datos ingresados anterior mente aqui.   <div class="form-group ">
        </div></small>
      </div>
      <form  role="form" method="post" >
        <div class="modal-body" style="background: #fff">
          <?php 
          $depatamentoObj=$cliobj->obtenerDepartamentoPorId($clienteObj->departamento); 
          $provinciaObj=$cliobj->otenerProvinciaPorId($clienteObj->provincia); 
          $distritoObj=$cliobj->obtenerDistritoPorId($clienteObj->distrito); 
          ?>
          <?php if ($clienteObj->tipoCliente=="Natural"): ?>
            <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>" >
            <div class="row">
              <div class="form-group col-md-3">
                <label for="nombre" class="">Nombre *: </label>
                <input style="border: none;" type="text" placeholder="Nombre: " id="nombre" name="nombreE" class="form-control border-bottom " value="<?php echo $clienteObj->nombre ?>" required >
              </div>
              <div class="form-group col-md-6">
                <label for="apellidoE" class="">Apellidos *</label>
                <input style="border: none;" type="text" placeholder="Apellidos *" id="apellidoE" name="apellidoE" class="form-control border-bottom text-capitalize"  required id="name" value="<?php echo $clienteObj->apellidosC ?>">
              </div>
              <div class="form-group col-lg-3">
                <label for="departamento1" class="">Departamento</label>
                <select  style="border: none;" class="departamento border-bottom form-control text-capitalize" required name="departamentoE" id="departamento1">
                  <option class="text-capitalize" value="<?php echo $depatamentoObj->iddepartamento ?>" disabled selected ><?php echo $depatamentoObj->departamento ?></option>
                  <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                  <?php if ($objDepartamento): ?>
                    <?php foreach ($objDepartamento as $var): ?>
                      <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                    <?php endforeach ?>
                  <?php else: ?>
                  <?php endif ?>
                </select>
              </div>
            </div> 
            <div class="row">

              <div class="form-group col-lg-3" >
                <label for="corredor" class="">Provincia</label>
                <select  style="border: none;" class="provincia1 border-bottom form-control text-capitalize" required name="provinciaE"  >
                  <option class="text-capitalize" value="<?php echo $provinciaObj->idprovincia ?>" disabled selected ><?php echo $provinciaObj->provincia ?></option>
                </select>
              </div>

              <div class="form-group col-lg-3" >
                <label for="distrito" class="">Distrito</label>
                <select  style="border: none;" class="distritos1 border-bottom form-control text-capitalize" required name="distritoE"  >
                  <option class="text-capitalize" value="<?php echo $distritoObj->iddistrito ?>" disabled selected ><?php echo $distritoObj->distrito ?></option>
                </select>
              </div>  
              <div class="form-group col-md-3">
                <label for="correo" class="">Direccion *</label>
                <input style="border: none;" type="text" placeholder="Direccion *" id="correo" name="direccionE" class="form-control x2 text-capitalize border-bottom text-capitalize" required value="<?php echo $clienteObj->direccion ?>" >
              </div>            
              <div class="form-group col-md-3">
                <label for="correo" class="">Referencia *</label>
                <input style="border: none;" type="text" placeholder="Referencia *" id="correo" name="ReferenciaE" class="form-control x2 text-capitalize border-bottom text-capitalize" required value="<?php echo $clienteObj->referencia ?>" >
              </div> 

              <div class="hr-line-dashed"></div>
              <hr>
            </div>
            <div class="row">

              <div class="form-group col-md-3">
               <label for="dniC" class="">Telefono *</label>
               <input style="border: none;" type="text" placeholder="Telefono *" id="celular" name="telefonoE" class="form-control border-bottom " required  data-mask="999 999 999" id="celular" value="<?php echo $clienteObj->telefono ?>" >
             </div>
             <div class="form-group col-md-3">
              <label for="telefono" class="">Telefono Alternativo *</label>
              <input style="border: none;" type="text" placeholder="Telefono Alternativo *" id="telefono" name="telefonoE1" class="form-control border-bottom text-capitalize"  value="<?php echo $clienteObj->telefono1 ?>" >
            </div>
            <div class="form-group col-md-3">
              <label for="correo" class="">Correo *</label>
              <input style="border: none;" type="text" placeholder="Direccion *" id="correo" name="correoE" class="form-control x2 text-capitalize border-bottom text-capitalize" value="<?php echo $clienteObj->correo ?>" required>
            </div>               
            <div class="form-group col-md-3">
              <label for="correo" class="">Correo alternativo *</label>
              <input  style="border: none;" type="text" placeholder="Referencia *" id="correo" name="correoE1" class="form-control x2 text-capitalize border-bottom text-capitalize" value="<?php echo $clienteObj->correo ?>">
            </div> 

            <div class="hr-line-dashed"></div>
            <hr>
          </div>
        <?php elseif ($clienteObj->tipoCliente<>"Natural") : ?>
          <?php   
          $depatamentoObjL=$cliobj->obtenerDepartamentoPorId($clienteObj->departamentoL); 
          $provinciaObjL=$cliobj->otenerProvinciaPorId($clienteObj->provinciaL); 
          $distritoObjL=$cliobj->obtenerDistritoPorId($clienteObj->distritoL); 
          ?>
          <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>" >
          <div class="row">
            <div class="form-group col-md-5">
              <label for="razonSocial" class="">Razon Social: * </label>
              <input style="border: none;" type="text" placeholder="Razon Social: *" id="razonSocial" name="razonSocialE" class="form-control border-bottom " value="<?php echo $clienteObj->razonSocial ?>" required >
            </div>
            <div class="form-group col-md-4">
              <label for="sectorDesempenia" class="">Sector que desempeña:  </label>
              <input style="border: none;" type="text" placeholder="Sector que desempeña " id="sectorDesempenia" name="sectorDesempeniaE" class="form-control border-bottom text-capitalize"   id="name" value="<?php echo $clienteObj->sectorDesempenia ?>">
            </div>

            <div class="form-group col-lg-3">
              <label for="departamento1" class="">Departamento</label>
              <select  style="border: none;" class="departamento2 border-bottom form-control text-capitalize" required name="departamentoLE" id="departamento1">
                <option class="text-capitalize" value="<?php echo $depatamentoObjL->iddepartamento ?>" disabled selected ><?php echo $depatamentoObjL->departamento ?></option>
                <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                <?php if ($objDepartamento): ?>
                  <?php foreach ($objDepartamento as $var): ?>
                    <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                  <?php endforeach ?>
                <?php else: ?>
                <?php endif ?>
              </select>
            </div>

          </div> 
          <div class="row">

      <div class="form-group col-lg-4" >
        <label for="corredor" class="">Provincia</label>
        <select  style="border: none;" class="provincia2 border-bottom form-control text-capitalize" required name="provinciaLE"  >
        <option class="text-capitalize" value="<?php echo $provinciaObjL->idprovincia ?>" disabled selected ><?php echo $provinciaObjL->provincia ?></option>
        </select>
      </div>

      <div class="form-group col-lg-4" >
          <label for="distrito" class="">Distrito</label>
          <select  style="border: none;" class="distritos2 border-bottom form-control text-capitalize" required name="distritoLE"  >
          <option class="text-capitalize" value="<?php echo $distritoObjL->iddistrito ?>" disabled selected ><?php echo $distritoObjL->distrito ?></option>
          </select>
      </div>
          <div class="form-group col-md-4">
            <label for="correo" class="">Direccion *</label>
            <input style="border: none;" type="text" placeholder="Direccion *" id="correo" name="direccionLE" class="form-control x2 text-capitalize border-bottom text-capitalize" required value="<?php echo $clienteObj->direccionL ?>">
          </div>               

          <div class="hr-line-dashed"></div>
          <hr>
        </div>
        <div class="row">

          <div class="form-group col-md-3">
           <label for="dniC" class="">Telefono *</label>
           <input style="border: none;" type="text" placeholder="Telefono *" id="celular" name="telefonoE" class="form-control border-bottom " required  data-mask="999 999 999" id="celular" value="<?php echo $clienteObj->telefono ?>" >
         </div>
         <div class="form-group col-md-3">
          <label for="telefono" class="">Telefono Alternativo *</label>
          <input style="border: none;" type="text" placeholder="Telefono Alternativo *" id="telefono" name="telefonoE1" class="form-control border-bottom text-capitalize" value="<?php echo $clienteObj->telefono1 ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="correo" class="">Correo *</label>
          <input style="border: none;" type="text" placeholder="Direccion *" id="correo" name="correoE" class="form-control x2 text-capitalize border-bottom text-capitalize" value="<?php echo $clienteObj->correo ?>" required>
        </div>               
        <div class="form-group col-md-3">
          <label for="correo" class="">Correo alternativo *</label>
          <input style="border: none;" type="text" placeholder="Referencia *" id="correo" name="correoE1" class="form-control x2 text-capitalize border-bottom text-capitalize" value="<?php echo $clienteObj->correo ?>">
        </div> 

        <div class="hr-line-dashed"></div>
        <hr>
      </div>

      <hr>
      <h3>Datos del representante Legal</h3>
      <div class="row">
        <div class="form-group col-md-3">
          <label for="nombre" class="">Nombre representante legal: * </label>
          <input style="border: none;" type="text" placeholder="Nombre representante legal: *" id="nombre" name="nombreE" class="form-control border-bottom " value="<?php echo $clienteObj->nombre ?>" required >
        </div>
        <div class="form-group col-md-6">
          <label for="apellidoE" class="">Apellidos representante legal:  *</label>
          <input style="border: none;" type="text" placeholder="Apellidos representante legal:  *" id="apellidoE" name="apellidoE" class="form-control border-bottom text-capitalize"  required id="name" value="<?php echo $clienteObj->apellidosC ?>">
        </div>
        <div class="form-group col-lg-3">
          <label for="departamento1" class="">Departamento</label>
          <select  style="border: none;" class="departamento border-bottom form-control text-capitalize" required name="departamentoE" id="departamento1">
            <option class="text-capitalize" value="<?php echo $depatamentoObj->iddepartamento ?>" disabled selected ><?php echo $depatamentoObj->departamento ?></option>
            <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
            <?php if ($objDepartamento): ?>
              <?php foreach ($objDepartamento as $var): ?>
                <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
              <?php endforeach ?>
            <?php else: ?>
            <?php endif ?>
          </select>
        </div>

      </div> 
      <div class="row">
       <div class="form-group col-lg-3" >
        <label for="corredor" class="">Provincia</label>
        <select  style="border: none;" class="provincia1 border-bottom form-control text-capitalize" required name="provinciaE"  >
          <option class="text-capitalize" value="<?php echo $provinciaObj->idprovincia ?>" disabled selected ><?php echo $provinciaObj->provincia ?></option>
        </select>
      </div>

      <div class="form-group col-lg-3" >
        <label for="distrito" class="">Distrito</label>
        <select  style="border: none;" class="distritos1 border-bottom form-control text-capitalize" required name="distritoE"  >
          <option class="text-capitalize" value="<?php echo $distritoObj->iddistrito ?>" disabled selected ><?php echo $distritoObj->distrito ?></option>
        </select>
      </div>  
      <div class="form-group col-md-3">
        <label for="correo" class="">Direccion *</label>
        <input style="border: none;" type="text" placeholder="Direccion *" id="correo" name="direccionE" class="form-control x2 text-capitalize border-bottom text-capitalize" required value="<?php echo $clienteObj->direccion ?>">
      </div>               
      <div class="form-group col-md-3">
        <label for="correo" class="">Referencia *</label>
        <input style="border: none;" type="text" placeholder="Referencia *" id="correo" name="referenciaE" class="form-control x2 text-capitalize border-bottom text-capitalize" value="<?php echo $clienteObj->referencia ?>" required>
      </div> 

      <div class="hr-line-dashed"></div>
      <hr>
    </div>
    <div class="row">

      <div class="form-group col-md-3">
       <label for="dniC" class="">Telefono *</label>
       <input style="border: none;" type="text" placeholder="Telefono *" id="celular" name="telefonoE2" class="form-control border-bottom " required  data-mask="999 999 999" id="celular" value="<?php echo $clienteObj->telefono2 ?>" >
     </div>
     <div class="form-group col-md-3">
      <label for="telefono" class="">Telefono Alternativo *</label>
      <input style="border: none;" type="text" placeholder="Telefono Alternativo *" id="telefono" name="telefonoE3" class="form-control border-bottom text-capitalize" value="<?php echo $clienteObj->telefono3 ?>" >
    </div>
    <div class="form-group col-md-3">
      <label for="correo" class="">Correo *</label>
      <input style="border: none;" type="text" placeholder="Correo *" id="correo" name="correoE2" class="form-control x2 text-capitalize border-bottom text-capitalize" value="<?php echo $clienteObj->correo2 ?>" required>
    </div>               
    <div class="form-group col-md-3">
      <label for="correo" class="">Correo alternativo *</label>
      <input style="border: none;" type="text" placeholder="Correo alternativo *" id="correo" name="correoE3" class="form-control x2 text-capitalize border-bottom text-capitalize" value="<?php echo $clienteObj->correo3 ?>">
    </div> 

    <div class="hr-line-dashed"></div>
    <hr>
  </div>


<?php endif; ?>
</div>

<div class="modal-footer">
  <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
  <button type="submit" name="actualizarDatos" class="btn btn-primary">Guardar Cambios</button>
</div>
</form>   
</div>
</div>
</div>
<!-- FIN DE ACTUALIZAR DATOS -->


<!-- Mainly scripts -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<!-- Steps -->
<script src="../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="../assets/js/cotizar.js"></script>
<script src="../assets/js/cotizarEmpresa.js"></script>
<script src="../assets/js/plugins/select2/select2.full.min.js"></script>

<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/build/jquery.datetimepicker.full.js"></script>

<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
 <?php include_once('../includes/script.php') ?> 
<script>
  <?php if (!empty($errores)): ?>
  <?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
<?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
  <?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
<?php endforeach ?>
<?php endif ?> 
</script>
<script src="js/registrarVehiculo.js"></script>
<!-- <script> 
  // $("#bancoCanal")
   $("#bancoCanal").keyup(function(){
      var bancoCanal = $(this).val();
      alert(bancoCanal);
    });
  </script> -->

  <script>
    $(".departamento").change(function(){provincias();});
    $(".departamento2").change(function(){provincias2();});
    $(".departamento3").change(function(){provincias3();});
    $(".provincia1").change(function(){distritos();});
    $(".provincia2").change(function(){distritos2();});
    $(".provincia3").change(function(){distritos3();});
    function provincias(){
      var iddepartamento = $(".departamento").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/provincia',
        data :  'iddepartamento='+ iddepartamento,
        success : function(data)
        {
          $(".provincia1").html(data);
        }
      });
      return false;
    }
    function provincias2(){
      var iddepartamento = $(".departamento2").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/provincia',
        data :  'iddepartamento='+ iddepartamento,
        success : function(data)
        {
          $(".provincia2").html(data);
        }
      });
      return false;
    }
    function provincias3(){
      var iddepartamento = $(".departamento3").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/provincia',
        data :  'iddepartamento='+ iddepartamento,
        success : function(data)
        {
          $(".provincia3").html(data);
        }
      });
      return false;
    }
    function distritos(){
      var idprovincia = $(".provincia1").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritos1").html(data)
        }
      });
      return false;
    }
    function distritos2(){
      var idprovincia = $(".provincia2").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritos2").html(data)
        }
      });
      return false;
    }
    function distritos3(){
      var idprovincia = $(".provincia3").val();
      $.ajax({        
        type : 'POST',
        url  : 'ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritos3").html(data)
        }
      });
      return false;
    }


  </script>
  <script> 
    $(document).ready(function(){
      <?php if($canales=Canales::verNombreCanal()): ?>
      $('.nombreCanal').typeahead({
        source: [
        <?php foreach ($canales as $var):?>
        {"name": "<?php echo $var->nombreCanal ?>"},        
      <?php endforeach ?>
      ]
    }); 
    <?php endif ?>
   
      <?php if($canales1=Canales::verEjecutiVodeCanal()): ?>
      $('.ejecutivoCanalN').typeahead({
        source: [
        <?php foreach ($canales1 as $var):?>
        {"name": "<?php echo $var->ejecutivoCanal ?>"},        
      <?php endforeach ?>
      ]
    }); 
    <?php endif ?>
    
    <?php if($canales=Canales::verDniCanal()): ?>
      $('.canalId').typeahead({
          source: [
          <?php foreach ($canales as $var):?>
          {"name": "<?php echo $var->canalId ?>"},        
        <?php endforeach ?>
        ]
      }); 
    <?php endif ?>
  
      // remote: 'ajax/ejecutivoCanal.php?query=%QUERY'

///7/para saber  
    $("#canalId").bind("keyup focus",function()
    { 
      var canalId = $(this).val();
      if (canalId.length>=8){
      $.ajax({
        type : 'POST',
        url  : 'ajax/canal',
        data : {canalId:canalId},
        success : function(data)
        {
         $("#comisionHtml").html(data);
        }
      });
      }
      return false;

    });
}); 
</script>
</body>
</html>
<?php else: ?>
  <h2>presione f5</h2>
  <?php  irA("ingresar") ?> 
<?php endif ?>

<?php
require_once("../includes/init.php");

$fechaInicio = isset($_POST['startDate']) ? $_POST['startDate'] : null;
$fechaFin = isset($_POST['endDate']) ? $_POST['endDate'] : null;

// Realiza la consulta a la base de datos utilizando la función modificada
//$trabajosData = Operacion::allTrabajosPorFecha($fechaInicio, $fechaFin);

$trabajosDataConRepro = Operacion::TrabajoConReprogramacion($fechaInicio, $fechaFin);
$trabajosDataSinRepro = Operacion::TrabajoSinReprogramacion($fechaInicio, $fechaFin);

//$arrayResponse = array('barras'=>$trabajosData, 'pie'=>$trabajosData, 'tabla'=>$trabajosData);
$arrayResponse = array('barrasSinRepro'=>$trabajosDataSinRepro, 'barrasConRepro'=>$trabajosDataConRepro, 'tablaSinRepro'=>$trabajosDataSinRepro, 'tablaConRepro'=>$trabajosDataConRepro);

// Devuelve los resultados en formato JSON
echo json_encode($arrayResponse, JSON_INVALID_UTF8_IGNORE);
?>

<?php
// require_once("../includes/init.php");

// $fechaInicio = isset($_POST['start']) ? $_POST['start'] : null;
// $fechaFin = isset($_POST['end']) ? $_POST['end'] : null;

// // Crear una instancia de la clase Operacion
// $operacion = new Operacion();

// // Realiza la consulta a la base de datos utilizando la función obtenerCantidadTrabajosPorDia
// $trabajosData = $operacion->obtenerCantidadTrabajosPorDia($fechaInicio, $fechaFin);

// // Asegurémonos de que graficoDatos esté presente incluso si está vacío
// $respuesta = array(
//     'totalCantidad' => array_sum(array_column($trabajosData, 'cantidad')), // Suma total de cantidades
//     'labels' => array_column($trabajosData, 'trabajo'), // Nombres de los trabajos
//     'data' => array_column($trabajosData, 'cantidad') // Cantidades de los trabajos
// );

// // Devuelve los resultados en formato JSON
// echo json_encode($respuesta, JSON_INVALID_UTF8_IGNORE);
?>



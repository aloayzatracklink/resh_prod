<?php


class Tecnicos extends Recursos
{
    public $idtec;
    public $tecnico;
    public $min_desc;
    public $hora_inicio;
    public $hora_fin;
    public $iddepartamento;
    public $idprovincia;
    public $iddistrito;
    public $ubicacion;
    public $fijo;
    public $estado;
    public $horas_trabajo;
    public $view_tciudad;


    public $departamento;
    public $provincia;
    public $distrito;
    public $distrito2;
    public $fecha;
    public $idliberar;
    public $idciudad;
    public $tt;
    public $solohorario;

    public $iddestinicio;
    public $iddestfin;
    public $tiempo;

    public $codigo;

    public static function obtenerTecnicos($idtecn){
        if($idtecn==''){
            $idtecn='';
        }else{
            $idtecn=' AND t.idtec='.$idtecn.'';
        }
         $obtenerTecnicos=self::sentencia("SELECT t.idtec,t.tecnico,t.min_desc,t.hora_inicio,t.hora_fin,t.iddepartamento,dp.departamento,t.idprovincia,p.provincia,t.iddistrito,d.distrito,t.ubicacion,t.fijo,t.estado,t.horas_trabajo,view_tciudad
                                                       FROM tecnicos t 
                                                       INNER JOIN distritos d ON t.iddistrito=d.iddistrito 
                                                       INNER JOIN provincias p ON d.idprovincia=p.idprovincia
                                                       INNER JOIN departamentos dp ON p.iddepartamento=dp.iddepartamento WHERE 1=1 ".$idtecn." order by orden asc ");
        if($idtecn==''){
         return $obtenerTecnicos;
        }else{
            return !empty($obtenerTecnicos)?array_shift($obtenerTecnicos):false;
        }
    }

    public function registrarTecnico(){
        $this->fechaCreada=date("YmdHis");
        global $db;
        global $session;
        $sql="INSERT INTO tecnicos(tecnico,min_desc,hora_inicio,hora_fin,iddepartamento,idprovincia,iddistrito,ubicacion,estado,fijo,idusuario) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString(strtoupper($this->tecnico))."' , '";
        $sql.= $db->escapeString(strtoupper($this->min_desc))."' , '";
        $sql.= $db->escapeString($this->hora_inicio)."' , '";
        $sql.= $db->escapeString($this->hora_fin)."' , '";
        $sql.= $db->escapeString($this->iddepartamento)."' , '";
        $sql.= $db->escapeString($this->idprovincia)."' , '";
        $sql.= $db->escapeString($this->iddistrito)."' , '";
        $sql.= $db->escapeString($this->ubicacion)."' , '";
        $sql.= $db->escapeString($this->estado)."' , '";
        $sql.= $db->escapeString($this->fijo)."' , '";
        $sql.= $db->escapeString($session->idusuario)."')";
        if ($db->query($sql)) {
            $this->idtec=$db->ultimoIdInsertado();
            $this->view_tciudad=2;
            $this->registrarLiberarTecCiudad();
            return true;
        }else{
            return false;
        }
    }

    public function updateTecnico(){
        global $db;
        global $session;
        $sql=" UPDATE tecnicos SET ";
        $sql.=" tecnico ='".$db->escapeString($this->tecnico)."',";
        $sql.=" min_desc ='".$db->escapeString($this->min_desc)."',";
        $sql.=" hora_inicio ='".$db->escapeString($this->hora_inicio)."',";
        $sql.=" hora_fin ='".$db->escapeString($this->hora_fin)."',";
        $sql.=" iddepartamento =".$db->escapeString($this->iddepartamento).",";
        $sql.=" idprovincia =".$db->escapeString($this->idprovincia).",";
        $sql.=" iddistrito =".$db->escapeString($this->iddistrito).",";
        $sql.=" ubicacion ='".$db->escapeString($this->ubicacion)."',";
        $sql.=" fijo =".$db->escapeString($this->fijo).", ";
        $sql.=" estado =".$db->escapeString($this->estado)." ";
        $sql.=" WHERE idtec =".$db->escapeString($this->idtec)."";
        if ($db->query($sql)) {
           // $this->registrarLiberarTecProvincia();
            return true;
        }else{
            echo "the fail";
        }
    }


    public static function obternerLiberarTecnico($idtecn){
        $obtenerLTecnicos=self::sentencia(" SELECT lt.idliberar,lt.idtec,lt.fecha,lt.hora_inicio,lt.hora_fin,lt.ubicacion,lt.solohorario,
        COALESCE((select idciudad from liberar_tecnico_ciudad where idliberar=lt.idliberar),0) iddistrito,
        COALESCE((select distrito from liberar_tecnico_ciudad ltc inner join distritos d on ltc.idciudad=d.iddistrito
            where idliberar=lt.idliberar),'Seleccionar') distrito 
        FROM liberar_tecnico lt where 1=1 AND idtec=".$idtecn." ");
        return $obtenerLTecnicos;
    }

    public static function verificarsiestarangohora($horaini,$horafin,$fecha,$ubicacion,$idtec,$idlibera){
        $obtenerLBTecnicos=self::sentencia("select count(*) tt from liberar_tecnico 
                where ((hora_inicio<'".$horaini."' and hora_fin>'".$horaini."') or (hora_inicio<'".$horafin."' and hora_fin>'".$horafin."') or (hora_inicio>'".$horaini."' and hora_fin<'".$horafin."')) 
                and ubicacion='".$ubicacion."' and fecha='".$fecha."' and idtec='".$idtec."' and idliberar!='".$idlibera."' " );
        return !empty($obtenerLBTecnicos)?array_shift($obtenerLBTecnicos):false;
    }

    public static function obtenerDepartamentos($idtec){
        global $db;
        $Obj=self::sentencia("
            SELECT dp.iddepartamento,dp.departamento,
            COALESCE((select 1 from liberar_tecnico_ciudad ltp where ltp.idciudad=dp.iddepartamento and ltp.tipo=1 and ltp.idtec=".$idtec."),0) tt
            FROM departamentos dp ");
        return !empty($Obj)?$Obj:false;
    }
    public static function obtenerprovincias($idtec){
        global $db;
        $Obj=self::sentencia("
            SELECT DISTINCT p.idprovincia,p.provincia,dp.departamento,
            COALESCE((select 1 from liberar_tecnico_ciudad ltp where ltp.idciudad=p.idprovincia and ltp.tipo=2 and ltp.idtec=".$idtec."),0) tt
            FROM provincias p inner join departamentos dp on dp.iddepartamento=p.iddepartamento  order by p.provincia asc");
        return !empty($Obj)?$Obj:false;
    }

    public static function obtenerDistritos($idtec){
        global $db;
        $filGeneral='';
        if($idtec<>0){
            $filGeneral='  and idliberar=0 ';
        }

        $Obj=self::sentencia("
            SELECT DISTINCT d.iddistrito,d.distrito,p.provincia,
            COALESCE((select 1 from liberar_tecnico_ciudad ltp where ltp.idciudad=d.iddistrito and ltp.tipo=3 and ltp.idtec=".$idtec.$filGeneral."),0) tt
            FROM distritos d  inner join provincias p on p.idprovincia=d.idprovincia  order by d.distrito asc ");
        return !empty($Obj)?$Obj:false;
    }


    public static function obtenerDistritosTraslado($iddistr){
        global $db;
        $Obj=self::sentencia("
             SELECT DISTINCT d.iddistrito,d.distrito,p.provincia,
             coalesce((SELECT tiempo FROM tiempo_traslado tt where tt.iddestfin=d.iddistrito and iddestinicio='$iddistr'),'00:00') tiempo,
             (SELECT distrito FROM distritos where iddistrito='$iddistr') distrito2
             FROM distritos d  INNER JOIN provincias p ON p.idprovincia=d.idprovincia
             WHERE d.idprovincia in ('66','127') ORDER BY d.distrito ASC; ");
        return !empty($Obj)?$Obj:false;
    }

    public static function obternerLiberarTecnicoxliberar($idlibe){
        $obtenerLTecnicos=self::sentencia(" SELECT lt.idliberar,lt.idtec,lt.fecha,lt.hora_inicio,lt.hora_fin,lt.ubicacion,lt.solohorario,
 COALESCE((select idciudad from liberar_tecnico_ciudad where idliberar=lt.idliberar),0) iddistrito,
        COALESCE((select distrito from liberar_tecnico_ciudad ltc inner join distritos d on ltc.idciudad=d.iddistrito
            where idliberar=lt.idliberar),'Seleccionar') distrito 
         FROM liberar_tecnico lt where 1=1 AND idliberar=".$idlibe." ");
        return !empty($obtenerLTecnicos)?array_shift($obtenerLTecnicos):false;

    }

    public function registrarLiberaTecnico(){
        $this->fechaCreada=date("YmdHis");
        global $db;
        global $session;
        $sql="INSERT INTO liberar_tecnico(idtec,fecha,hora_inicio,hora_fin,ubicacion,solohorario,idusuario) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString($this->idtec)."' , '";
        $sql.= $db->escapeString($this->fecha)."' , '";
        $sql.= $db->escapeString($this->hora_inicio)."' , '";
        $sql.= $db->escapeString($this->hora_fin)."' , '";
        $sql.= $db->escapeString($this->ubicacion)."' , '";
        $sql.= $db->escapeString($this->solohorario)."' , '";
        $sql.= $db->escapeString($session->idusuario)."')";
        if ($db->query($sql)) {
            $this->idliberar=$db->ultimoIdInsertado();
            $this->registrarLiberarTecCiudad2();
            return $this->idliberar;
        }else{
            return false;
        }
    }


    public function registrarLiberarTecCiudad2(){
        global $db;
        global $session;
        $sql="INSERT INTO liberar_tecnico_ciudad(tipo,idtec,idciudad,idliberar) ";
        $sql.=" VALUES ('3' , '";
        $sql.= $db->escapeString($this->idtec)."' , '";
        $sql.= $db->escapeString($this->idciudad)."' , '";
        $sql.= $db->ultimoIdInsertado()."')";
        if ($db->query($sql)) {
            return true;
        }else{
            return false;
        }
    }

    public function registrarLiberarTecCiudad(){
        global $db;
        global $session;
        $sql="INSERT INTO liberar_tecnico_ciudad(tipo,idtec,idciudad) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString($this->view_tciudad)."' , '";
        $sql.= $db->escapeString($this->idtec)."' , '";
        $sql.= $db->escapeString($this->idciudad)."')";
        if ($db->query($sql)) {
            return true;
        }else{
            return false;
        }
    }

    public function deleteLiberaTecCiudad(){
        global $db;
        global $session;
        $sql=" DELETE FROM liberar_tecnico_ciudad ";
        $sql.=" WHERE idtec =".$db->escapeString($this->idtec)." and idliberar=0";
        if ($db->query($sql)) {
            $this->updateViewTecnico();
            return true;
        }else{
            echo "the fail";
        }
    }

    public function deleteLiberaTecCiudad2(){
        global $db;
        global $session;
        $sql=" DELETE FROM liberar_tecnico_ciudad ";
        $sql.=" WHERE idliberar =".$db->escapeString($this->idliberar)." ";
        if ($db->query($sql)) {
            $this->updateViewTecnico();
            return true;
        }else{
            echo "the fail";
        }
    }
    public function updateViewTecnico(){
        global $db;
        global $session;
        $sql=" UPDATE tecnicos SET ";
        $sql.=" view_tciudad ='".$db->escapeString($this->view_tciudad)."'";
        $sql.=" WHERE idtec =".$db->escapeString($this->idtec)."";
        if ($db->query($sql)) {
            return true;
        }else{
            echo "the fail";
        }
    }

    public function deleteLiberaTecProvincia2(){
        global $db;
        global $session;
        $sql=" DELETE FROM liberar_tec_provincia ";
        $sql.=" WHERE idtec =".$db->escapeString($this->idtec)."";
        $sql.=" and idprovincia =".$db->escapeString($this->idprovincia)."";
        if ($db->query($sql)) {
            return true;
        }else{
            echo "the fail";
        }
    }

    public function updateLiberaTecnico(){
        global $db;
        global $session;
        $sql=" UPDATE liberar_tecnico SET ";
        $sql.=" fecha ='".$db->escapeString($this->fecha)."',";
        $sql.=" hora_inicio ='".$db->escapeString($this->hora_inicio)."',";
        $sql.=" hora_fin ='".$db->escapeString($this->hora_fin)."',";
        $sql.=" ubicacion ='".$db->escapeString($this->ubicacion)."',";
        $sql.=" solohorario ='".$db->escapeString($this->solohorario)."'";
        $sql.=" WHERE idliberar =".$db->escapeString($this->idliberar)."";
        if ($db->query($sql)) {
            $this->updateLiberaTecnicoCiudad2();
        }else{
            echo "the fail";
        }
    }

    public function updateLiberaTecnicoCiudad2(){
        global $db;
        global $session;
        $sql=" UPDATE liberar_tecnico_ciudad SET ";
        $sql.=" idciudad ='".$db->escapeString($this->idciudad)."'";
        $sql.=" WHERE idliberar =".$db->escapeString($this->idliberar)."";
        if ($db->query($sql)) {
        }else{
            echo "the fail";
        }
    }


    public function deleteLiberaTecnico(){
        global $db;
        global $session;
        $sql=" DELETE FROM liberar_tecnico ";
        $sql.=" WHERE idliberar =".$db->escapeString($this->idliberar)."";
        if ($db->query($sql)) {
            $this->deleteLiberaTecCiudad2();
        }else{
            echo "the fail";
        }
    }


    public function deleteTiempoTraslado(){
        global $db;
        global $session;
        $sql=" DELETE FROM tiempo_traslado ";
        $sql.=" WHERE iddestinicio =".$db->escapeString($this->iddestinicio)."";
        if ($db->query($sql)) {
            return true;
        }else{
            echo false;
        }
    }


    public function deleteTiempoTraslado2(){
        global $db;
        global $session;
        $sql=" DELETE FROM tiempo_traslado ";
        $sql.=" WHERE iddestinicio =".$db->escapeString($this->iddestinicio)." and iddestfin=".$db->escapeString($this->iddestfin)." ";
        if ($db->query($sql)) {
            return true;
        }else{
            echo false;
        }
    }

    public function registrarTiempoTraslado(){
        global $db;
        global $session;
        $sql="INSERT INTO tiempo_traslado(iddestinicio,iddestfin,tiempo,idusuario) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString($this->iddestinicio)."' , '";
        $sql.= $db->escapeString($this->iddestfin)."' , '";
        $sql.= $db->escapeString($this->tiempo)."' , '";
        $sql.= $db->escapeString($session->idusuario)."')";
        if ($db->query($sql)) {
            return true;
        }else{
            return false;
        }
    }

    public static function obtenerUbicacion(){
        global $db;
        $Obj=self::sentencia("SELECT codigo,ubicacion FROM ubicacion");
        return !empty($Obj)?$Obj:false;
    }
}
<?php


class homologation_detail extends Recursos
{
    public $idhomdetail;
    public $idhomclient;
    public $idvehicle;
    public $unitid;
    public $bodytype;
    public $vehicletype;
    public $capacity;
    public $volume;
    public $travelinter;
    public $status;
    public $statusActivacion;
    public $create_documents_at;
    public $created;
    public $creation_at;
    public $last_modify;
    public $update_at;
    public $idlocation;
    public $idcompany;
    public $idcustomer;
    public $idchasis;
    public $license;
    public $descbrand;
    public $descmodel;
    public $yearmodel;
    public $color;
    public $engine;

    public function registerHomologation_detail()
    {
        global $db;
        global $session;
        $this->creation_at = date("Y-m-d H:i");
        $sql = " INSERT INTO homologation_detail(idhomclient,idvehicle,unitid,created,creation_at,status,statusActivacion) ";
        $sql .= " VALUES ('";
        $sql .= $db->escapeString(($this->idhomclient)) . "' , '";
        $sql .= $db->escapeString(($this->idvehicle)) . "' , '";
        $sql .= $db->escapeString(($this->unitid)) . "' , '";
        $sql .= $db->escapeString($session->idusuario) . "','";
        $sql .= $this->creation_at . "' , '";
        $sql .= $db->escapeString(($this->status)) . "' , '";
        $sql .= $db->escapeString($this->statusActivacion) . "' )";
        if ($db->query($sql)) {
            $this->idhomdetail = $db->ultimoIdInsertado();
            return true;
        } else {
            return false;
        }
    }


    public function updateStatusDetHomologation()
    {
        global $db;
        global $session;
        $this->update_at = date("YmdHis");
        $sql = " UPDATE homologation_detail SET ";
        $sql .= " statusActivacion ='" . $db->escapeString($this->statusActivacion) . "',";
        $sql .= " updated_at ='" . $this->update_at . "' ,";
        $sql .= " last_modify ='" . $db->escapeString($session->idusuario) . "' ";
        $sql .= " WHERE idhomdetail ='" . $db->escapeString($this->idhomdetail) . "'";
        if ($db->query($sql)) {
            return true;
        } else {
            echo "the fail";
        }
    }


    public function updateStatDetHomologation()
    {
        global $db;
        global $session;
        $this->update_at = date("YmdHis");
        $sql = " UPDATE homologation_detail SET ";
        $sql .= " status ='" . $db->escapeString($this->status) . "',";
        $sql .= " updated_at ='" . $this->update_at . "' ,";
        $sql .= " last_modify ='" . $db->escapeString($session->idusuario) . "' ";
        $sql .= " WHERE idhomdetail ='" . $db->escapeString($this->idhomdetail) . "'";
        if ($db->query($sql)) {
            return true;
        } else {
            echo "the fail";
        }
    }


    public function updateHomologation_detail()
    {
        global $db;
        $this->update_at = date("YmdHis");
        $sql = " UPDATE homologation_detail SET ";
        $sql .= " bodytype ='" . $db->escapeString($this->bodytype) . "',";
        $sql .= " vehicleType ='" . $db->escapeString($this->vehicleType) . "',";
        $sql .= " capacity ='" . $db->escapeString($this->capacity) . "',";
        $sql .= " volume ='" . $db->escapeString($this->volume) . "',";
        $sql .= " travelInter ='" . $db->escapeString($this->travelInter) . "',";
        $sql .= " update_at ='" . $this->update_at . "' ";
        $sql .= " WHERE idhomdetail ='" . $db->escapeString($this->idhomdetail) . "'";
        if ($db->query($sql)) {
            return true;
        } else {
            echo "the fail";
        }
    }

    public function ObtenerHomologationDetail($idDetail)
    {
        $sql = "select idhomdetail, idhomclient, idvehicle, unitid, bodytype, vehicletype, capacity, volume, travelinter, status, create_documents_at, created, creation_at, last_modify, updated_at, statusActivacion
                FROM homologation_detail sc 
              WHERE  idhomdetail='". $idDetail ."' and sc.statusActivacion='A'
        ";
        $rslt = self::sentencia($sql);
        return ($rslt)?array_shift($rslt):false;
    }

    public function ObtenerHomologateTranspor($idhomclient)
    {
        $sql = "select *
              FROM homologation_client sc 
              WHERE  idhomclient='".$idhomclient."'
        ";
        $rslt = self::sentencia($sql);
        return ($rslt)?array_shift($rslt):false;
    }

    public function ObtenerVehicle($idlocation,$idcompany,$idcustomer,$idvehicle) {
        $sql="select DISTINCT sv.idvehicle,sv.idlocation,sv.idcompany,sv.idcustomer,sv.idchasis,sv.license,
              mc.descbrand ,sv.descmodel,sv.yearmodel,sv.color,sv.engine
              from sid_vehicle sv 
              inner join sid_brand mc on mc.idbrand=sv.idbrand
              WHERE sv.idlocation='$idlocation' and sv.idcompany='$idcompany' and sv.idcustomer='$idcustomer'
              and sv.idvehicle='$idvehicle'";
        $rslt = self::sentenciaOnix($sql);
        return ($rslt)?array_shift($rslt):false;
    }

}
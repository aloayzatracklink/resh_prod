<?php


class homologation_client extends Recursos
{
    public $idhomclient;
    public $idcustomer;
    public $idlocation;
    public $idcompany;
    public $token;
    public $status;
    public $lastname;
    public $firstname;
    public $phonecell;
    public $created;
    public $creation_at;
    public $last_modify;
    public $update_at;
    public $statusActivacion;
    public $cliente;
    public $identdoc;
    public $mail;
    public $phonemain;
    public $addressmain;
    public $idcli;
    public $idvehicle;
    public $idchasis;
    public $license;
    public $descbrand;
    public $descmodel;
    public $yearmodel;
    public $ttvehiculos;
    public $truevehicle;
    public $trueCustomer;
    public $ttvehiculoHomolo;
    public $cantidad;
    public $idhomdetail;
    public $ttvehiculoHomoRecb;
    public $create_documents_at;
    public $ttvehicleclient;
    public $idsim;

    public function registerHomologation(){
        global $db;
        global $session;
        $this->creation_at=date("Y-m-d H:i");
        $sql=" INSERT INTO homologation_client(idcustomer, idlocation, idcompany, token,created,creation_at,status) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString(($this->idcustomer))."' , '";
        $sql.= $db->escapeString(($this->idlocation))."' , '";
        $sql.= $db->escapeString($this->idcompany)."' , '";
        $sql.= $db->escapeString($this->token)."' , '";
        $sql.= $db->escapeString($session->idusuario)."','";
        $sql.= $this->creation_at."' , '";
        $sql.= $db->escapeString($this->status)."')";
        if ($db->query($sql)) {
            $this->idhomclient=$db->ultimoIdInsertado();
            return true;
        } else {
            return false;
        }
    }


   public function ObtenerListClient($datos) {
        $sql="SELECT CONCAT(sc.idcustomer,'',sc.idlocation) idcli,sc.idcustomer,sc.idlocation ,sc.idcompany ,
              CONCAT(sc.lastname ,' ',sc.firstname ) cliente,sc.lastname,sc.firstname,
              sc.identdoc,sc.mail ,sc.phonemain ,sc.phonecell ,sc.addressmain,max(ct.dateservto) maxContrato
              FROM sid_customer sc 
              inner join sid_contract ct on ct.idcustomer = sc.idcustomer and ct.idloccust =sc.idlocation  and ct.idcompany =sc.idcompany
              WHERE CONCAT(sc.lastname ,' ',sc.firstname ) like '%$datos%' or sc.identdoc like '%$datos%' 
              group by CONCAT(sc.idcustomer,'',sc.idlocation),sc.idcustomer,sc.idlocation ,sc.idcompany ,
              CONCAT(sc.lastname ,' ',sc.firstname ),sc.identdoc,sc.mail ,sc.phonemain ,sc.phonecell ,sc.addressmain
              HAVING maxContrato>=now()
              LIMIT 10";
        $rslt = self::sentenciaOnix($sql);
        return $rslt;
    }

    public function ObtenerIdBusqPag($datos) {
        $sql="select DISTINCT CONCAT(sc.idcompany,'',sc.idlocation,'',sc.idcustomer,'',sv.idvehicle) idcli
            FROM sid_customer sc 
            inner join sid_vehicle sv on sv.idcustomer=sc.idcustomer and sv.idlocation =sc.idlocation
            WHERE CONCAT(sc.lastname ,' ',sc.firstname ) like '%$datos%' or sc.identdoc like '%$datos%' 
            or  sv.license like '%$datos%'
            or sv.idchasis like '%$datos%'
            LIMIT 10;
        ";
        $rslt = self::sentenciaOnix($sql);
        $rpt=false;
        if($rslt){
            $rpt.="'0'";
            foreach ($rslt as $var){
                $rpt.=",'".$var->idcli."'";
            }
        }
        return $rpt;
    }

    public function buscarIdHomloListPag($id,$posisionDePagina,$registrosPorPagina){
        $fil="";
        if($id!=''){
            $fil="and CONCAT(hc.idcompany,'',hc.idlocation,'',hc.idcustomer,'',hd.idvehicle) in($id)";
        }
        $sql="SELECT hc.idhomclient,hc.idcompany,hc.idlocation,hc.idcustomer,count(hd.idhomdetail) ttvehiculoHomolo
            FROM homologation_client hc 
            INNER JOIN homologation_detail hd on hd.idhomclient=hc.idhomclient
            WHERE hd.statusActivacion='A' 
            $fil
            GROUP BY hc.idhomclient,hc.idcompany,hc.idlocation,hc.idcustomer
           order by hc.idhomclient asc  LIMIT $posisionDePagina, $registrosPorPagina  ";
        $rslt = self::sentencia($sql);
      //  echo $sql;
        return  $rslt;
    }

    public function ObtenerDatosLisPag($idcompany,$idlocation,$idcustomer) {
        $sql=" SELECT DISTINCT  CONCAT(sc.idcustomer,'',sc.idlocation) idcli,sc.idcustomer,sc.idlocation ,sc.idcompany ,CONCAT(sc.lastname ,' ',sc.firstname ) cliente,
              sc.identdoc,sc.mail ,sc.phonemain ,sc.phonecell ,sc.addressmain ,
              (select count(*) FROM sid_vehicle sv where sv.idcustomer=sc.idcustomer and sv.idlocation =sc.idlocation) ttvehiculos
              FROM sid_customer sc 
              WHERE  sc.idcompany='$idcompany' and sc.idlocation='$idlocation' and sc.idcustomer='$idcustomer'
        ";
        $rslt = self::sentenciaOnix($sql);
        return   ($rslt)?array_shift($rslt):false;
    }


    public function ObtenerListVehicle($idlocation,$idcompany,$idcustomer) {
        $sql="select DISTINCT sv.idvehicle,sv.idlocation,sv.idcompany,sv.idcustomer,sv.idchasis,sv.license,
              mc.descbrand ,sv.descmodel,sv.yearmodel,ss.idsim, max(sc.dateservto) maxContrato
              from sid_vehicle sv 
              inner join sid_brand mc on mc.idbrand=sv.idbrand
              inner join sid_contract sc on (sv.idvehicle=sc.idvehicle and sv.idcustomer=sc.idcustomer and sv.idlocation=sc.idloccust)
              left join sid_serie ss on ss.idcustomer=sv.idcustomer and ss.idloccust = sv.idlocation and ss.idcompany=sv.idcompany and ss.idvehicle=sv.idvehicle
              WHERE sv.idlocation='$idlocation' and sv.idcompany='$idcompany' and sv.idcustomer='$idcustomer'
              group by sv.idvehicle,sv.idlocation,sv.idcompany,sv.idcustomer,sv.idchasis,sv.license,
              mc.descbrand ,sv.descmodel,sv.yearmodel,ss.idsim
               having maxContrato >= now() 
              ";

        $rslt = self::sentenciaOnix($sql);
        return $rslt;
   }

    public function obtenerCliente($idlocation,$idcompany,$idcustomer) {
        $sql="select DISTINCT sc.lastname,sc.firstname,sc.phonecell,sc.mail
              from sid_customer sc
              WHERE sc.idlocation='$idlocation' and sc.idcompany='$idcompany' and sc.idcustomer='$idcustomer' 
             ";
        $rslt = self::sentenciaOnix($sql);
        return   ($rslt)?array_shift($rslt):false;
    }

    public function ObtenerdatDetVehiclePag($idlocation,$idcompany,$idcustomer,$idvehicle) {
        $sql="select DISTINCT sv.idvehicle,sv.idlocation,sv.idcompany,sv.idcustomer,sv.idchasis,sv.license,
              mc.descbrand ,sv.descmodel,sv.yearmodel
              from sid_vehicle sv 
              inner join sid_brand mc on mc.idbrand=sv.idbrand
              WHERE sv.idlocation='$idlocation' and sv.idcompany='$idcompany' and sv.idcustomer='$idcustomer' 
              and sv.idvehicle='$idvehicle'";
        $rslt = self::sentenciaOnix($sql);
        return   ($rslt)?array_shift($rslt):false;
    }


    public function ObtenerVehicleHomologation($idlocation,$idcompany,$idcustomer,$idvehicle) {
        $sql="SELECT  count(*) truevehicle
              FROM homologation_client hc 
              INNER JOIN homologation_detail hd on hc.idhomclient = hd.idhomclient
              WHERE hc.idlocation='$idlocation' and hc.idcompany='$idcompany' and hc.idcustomer='$idcustomer' and hd.idvehicle='$idvehicle'
              and hd.statusActivacion='A' ";
        $rslt = self::sentencia($sql);
        return   ($rslt)?array_shift($rslt):0;
    }

    public function ObtenerTTVehiclexClient($idcompany,$idlocation,$idcustomer) {

        $sql="
                select count(DISTINCT sv.idvehicle) ttvehicleclient FROM sid_vehicle sv  
                inner join sid_contract ct on (sv.idvehicle=ct.idvehicle and sv.idcustomer=ct.idcustomer and sv.idlocation=ct.idloccust)
                where sv.idcustomer='$idcustomer'  and sv.idlocation ='$idlocation' and sv.idcompany='$idcompany' 
                and ct.dateservto >= now();
             ";
        $rslt = self::sentenciaOnix($sql);
        return   ($rslt)?array_shift($rslt):0;
    }

    public function ObtenerTTVehicleHomologation($idcompany,$idlocation,$idcustomer) {

        $sql=" SELECT count(hd.idhomdetail) ttvehiculoHomolo FROM homologation_client hc INNER JOIN homologation_detail hd on hd.idhomclient=hc.idhomclient
                where hc.idcompany='$idcompany' and hc.idlocation ='$idlocation' and hc.idcustomer='$idcustomer'
                and hd.statusActivacion='A'
             ";
        $rslt = self::sentencia($sql);
        return   ($rslt)?array_shift($rslt):0;
    }

    public function verificarExisteCustomer($idcompany,$idlocation,$idcustomer){
        $sql="SELECT count(*)trueCustomer ,hc.idcustomer,hc.idhomclient FROM homologation_client hc WHERE hc.idcompany='$idcompany' and hc.idlocation='$idlocation' and hc.idcustomer='$idcustomer'
             GROUP BY hc.idcustomer ,hc.idhomclient ; ";
        $rslt = self::sentencia($sql);
        return   ($rslt)?array_shift($rslt):false;
    }

    public function  ttRegistros(){
        $sql="SELECT count(DISTINCT hc.idhomclient) cantidad FROM homologation_client hc INNER JOIN homologation_detail hd on hd.idhomclient=hc.idhomclient
              WHERE hd.statusActivacion='A'";
        $rslt = self::sentencia($sql);
        return !empty($rslt)? array_shift($rslt):false;
    }


    public function obtenerHomologationClient($idhomclient) {
        $sql=" SELECT DISTINCT  hc.idcustomer,hc.idlocation,hc.idcompany,hc.token
              FROM homologation_client hc
              WHERE hc.idhomclient = '$idhomclient'
              AND hc.status ='A' ";
        $rslt = self::sentencia($sql);
        return !empty($rslt)? array_shift($rslt):false;
    }


    public function ObtenerListVehicleDetPag($idhomclient) {
        $sql=" SELECT DISTINCT  idvehicle ,idhomdetail,hd.creation_at,hd.create_documents_at,hd.status
              FROM homologation_detail hd
              WHERE hd.idhomclient = '$idhomclient'
              AND hd.statusActivacion ='A' ";
        $rslt = self::sentencia($sql);

        return $rslt;
    }

    public function ObtenerTTVehicleHomologado($idcompany,$idlocation,$idcustomer,$stado) {

        $sql="  SELECT count(DISTINCT hd.idhomdetail) ttvehiculoHomoRecb 
                FROM homologation_client hc 
                INNER JOIN homologation_detail hd on hd.idhomclient=hc.idhomclient 
                INNER JOIN homologation_documents hdc on hdc.idhomdetail=hdc.idhomdetail 
                where hc.idcompany='$idcompany' and hc.idlocation ='$idlocation' and hc.idcustomer='$idcustomer'
                and hd.statusActivacion='A' and hd.status  in ($stado)
             ";
        $rslt = self::sentencia($sql);
        return   ($rslt)?array_shift($rslt):0;
    }

    public function ObtenerTTVehicleRecibidos($idcompany,$idlocation,$idcustomer,$stado) {

        $sql="  SELECT count(DISTINCT hd.idhomdetail) ttvehiculoHomoRecb 
                FROM homologation_client hc 
                INNER JOIN homologation_detail hd on hd.idhomclient=hc.idhomclient 
                INNER JOIN homologation_documents hdc on hdc.idhomdetail=hdc.idhomdetail 
                where hc.idcompany='$idcompany' and hc.idlocation ='$idlocation' and hc.idcustomer='$idcustomer'
                and hd.statusActivacion='A' and hd.status not in ($stado)
             ";
        $rslt = self::sentencia($sql);
        return   ($rslt)?array_shift($rslt):0;
    }

    public static function findHomologation($id) {
        $getHomologation=self::sentencia(" SELECT lt.idliberar,lt.idtec,lt.fecha,lt.hora_inicio,lt.hora_fin,lt.ubicacion,lt.solohorario,
        COALESCE((select idciudad from liberar_tecnico_ciudad where idliberar=lt.idliberar),0) iddistrito,
        COALESCE((select distrito from liberar_tecnico_ciudad ltc inner join distritos d on ltc.idciudad=d.iddistrito
            where idliberar=lt.idliberar),'Seleccionar') distrito 
        FROM liberar_tecnico lt where 1=1 AND idtec=".$id." ");
        return $getHomologation;
    }

    public function updateStatusHomologation(){
        global $db;
        $this->update_at=date("YmdHis");
        $sql=" UPDATE homologation_client SET ";
        $sql.=" status ='".$db->escapeString($this->status)."',";
        $sql.=" update_at ='".$this->update_at."' ";
        $sql.=" WHERE idhomclient ='".$db->escapeString($this->idhomclient)."'";
        if ($db->query($sql)) {
            return true;
        } else {
            echo "the fail";
        }
    }

    public function listFindClient(){
        $sql="select DISTINCT sv.idcompany,sv.idlocation,sv.idcustomer,sv.idvehicle,ss.idsim,scus.lastname,scus.firstname,TRIM(scus.mail) as mail
from sid_vehicle sv 
inner join sid_customer scus on scus.idlocation=sv.idlocation and scus.idcompany=sv.idcompany and scus.idcustomer=sv.idcustomer
INNER JOIN sid_serie ss on ss.idcustomer=sv.idcustomer and ss.idloccust = sv.idlocation and ss.idcompany=sv.idcompany and ss.idvehicle=sv.idvehicle
where concat(sv.idvehicle,sv.idlocation,sv.idcustomer) in (
'11100029037',
'11100029041',
'211100029060',
'231100029060',
'251100029060',
'261100029060',
'11100029119',
'11100029124',
'11100029128',
'21100029128',
'31100029128',
'41100029128',
'51100029128',
'61100029128',
'71100029128',
'81100029128',
'91100029128',
'101100029128',
'111100029128',
'121100029128',
'11100029151',
'11100029153',
'11100029197',
'11100029224',
'21100029224',
'31100029224',
'11100029236',
'11100029248',
'11100029249',
'11100029275',
'11100029295',
'11100029300',
'11100029316',
'21100029316',
'31100029316',
'11100029328',
'11100029367',
'11100029384',
'11100029390',
'11100029422',
'11100029433',
'11100029437',
'11100029464',
'11100029465',
'11100029542',
'21100029542',
'11100029615',
'11100029629',
'11100029633',
'11100029635',
'11100029648',
'11100029669',
'21100029669',
'41100029681',
'51100029681',
'61100029681',
'11100029685',
'11100029705',
'21100029713',
'51100029713',
'61100029713',
'71100029713',
'81100029713',
'91100029713',
'101100029713',
'121100029713',
'131100029713',
'141100029713',
'161100029713',
'171100029713',
'11100029714',
'11100029717',
'21100029717',
'11100029733',
'11100029746',
'11100029759',
'21100029761',
'31100029761',
'31100029763',
'11100029764',
'11100029766',
'11100029773',
'11100029781',
'11100029802',
'11100029805',
'11100029817',
'11100029822',
'11100029830',
'11100029833',
'11100029851',
'11100029867',
'11100030170',
'21100030170',
'11100030184',
'11100030200',
'11100030208',
'11100030217',
'21100030230',
'31100030230',
'41100030230',
'51100030230',
'11100030231',
'11100030241',
'21100030241',
'21100030263',
'11100030284',
'21100030284',
'31100030284',
'11100030300',
'11100030301',
'11100030342',
'11100030344',
'11100030354',
'11100030367',
'11100030368',
'21100030368',
'31100030368',
'41100030368',
'11100030372',
'21100030372',
'11100030390',
'21100030390',
'11100030395',
'11100030417',
'11100030442',
'11100030446',
'11100030468',
'11100030493',
'11100030498',
'11100030501',
'11100030504',
'11100030539',
'11100030540',
'11100030541',
'11100030543',
'11100030547',
'11100030553',
'11100030555',
'11100030561',
'11100030567',
'11100030593',
'21100030593',
'11100030606',
'11100030618',
'11100030628',
'11100030664',
'11100030688',
'11100030708',
'11100030731',
'11100030735',
'11100030736',
'11100030742',
'11100030744',
'11100030765',
'11100030766',
'21100030782',
'11100030804',
'11100030806',
'11100030815',
'11100030888',
'11100030894',
'21100030894',
'31100030894',
'41100030894',
'51100030894',
'61100030894',
'71100030894',
'91100030894',
'11100030896',
'11100030903',
'21100030904',
'11100030907',
'11100030910',
'11100030926',
'11100030979',
'11100030984',
'11100030987',
'31100030987',
'11100031027',
'11100031030',
'11100031033',
'11100031034',
'11100031073',
'11100031074',
'11100031076',
'11100031092',
'11100031118',
'21100031118',
'31100031118',
'11100031131',
'11100031147',
'11100031167',
'21100031173',
'11100031181',
'11100031210',
'11100031211',
'11100031222',
'11100031230',
'11100031233',
'11100031279',
'11100031288',
'11100031290',
'21100031303',
'11100031324',
'11100031326',
'11100031330',
'11100031337',
'11100031343',
'11100031348',
'11100031350',
'11100031357',
'11100031361',
'21100031366',
'11100031375',
'11100031401',
'11100031408',
'11100031416',
'11100031447',
'11100031458',
'11100031469',
'21100031469',
'11100031485',
'11100031493',
'21100031512',
'31100031515',
'71100031515',
'81100031515',
'11100031516',
'11100031517',
'71100031533',
'81100031533',
'161100031537',
'201100031537',
'281100031537',
'291100031537',
'301100031537',
'311100031537',
'321100031537',
'331100031537',
'341100031537',
'11100031553',
'11100031558',
'11100031588',
'11100031599',
'11100031609',
'11100031621',
'11100031645',
'11100031669',
'11100031680',
'21100031680',
'11100031686',
'11100031691',
'21100031712',
'191100031712',
'211100031712',
'221100031712',
'281100031712',
'291100031712',
'301100031712',
'311100031712',
'321100031712',
'341100031712',
'351100031712',
'361100031712',
'371100031712',
'381100031712',
'391100031712',
'401100031712',
'411100031712'
)
        ";
        $rslt = self::sentenciaOnix($sql);
        return $rslt;
    }

    public function verificarExisteCustomerVeh($idhomclient,$idvehicle)
    {
        $sql = "SELECT count(*)trueCustomer ,hc.idvehicle,hc.idhomclient 
                FROM homologation_detail hc WHERE hc.idhomclient='$idhomclient'and hc.idvehicle='$idvehicle'
             GROUP BY hc.idvehicle ,hc.idhomclient ; ";
        $rslt = self::sentencia($sql);
        return ($rslt) ? array_shift($rslt) : false;


    }

}
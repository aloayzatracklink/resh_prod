<?php 

class Operacion extends Recursos
{
	public $idoperacion;
	public $idcliente;
	public $idvehiculo;
	public $idTecnicoAsignado;
	public $idChequeo;
	public $numeroOrte;
	public $idmodificador;
	public $fechaConfirmacionOPE;
	public $idConfirmacionOPE;
	public $fechaModficacionOrte2;
	public $fechaAsignacionTec;
	public $fechaModficacion3;
	public $idmodificador3;
	public $numeroOrden;
	public $idlocation;
	public $datosfacturacionid;
	public $lugarInstalacion;
	public $nombreConcecionarioInstalicion;
	public $direccionInstacion;
	public $referenciaInstalacion;
	public $fechaInstalacion;
	public $nombreContacto;
	public $numeroContacto;
	public $idTecnicoInstalador;
	public $idCreador;
	public $descripcionOrden;
	public $imei;
	public $id;
	public $pruebas;
	public $odometro;
	public $lugarActivacion;
	public $opciones;
	public $fechaActivacion;
	public $tiempoServicio;
	public $fechaCreacion;
	public $trabajo;
	public $idusuarioActivador;
	public $placa;
	public $chasis;
	public $nombre;
	public $apellidoPaterno;
	public $apellidosC;
	public $razonSocial;
	public $dni;
	public $mailWelcome;
	public $ruc;
	public $tipoCliente;
	public $marca;
	public $modelo;
	public $plan;
	public $estado;
	public $color;
	public $idcreador;
	public $coordenadas;
	public $ubicacionSistema;
	public $observacion;
	public $idUsuarioPendiente;
	public $idUsuarioBienvenida;
	public $accionBienvenida;
	public $fechaBienvenida;
	public $serviciosAdicionales;
	public $instalaciones;
	public $idUsuarioPlataforma;
	public $fechaPlataformaV;
	public $coordenadasInstalacion;
	public $idUsuarioCargo;
	public $idReprogramado;
	public $cantidad;
	public $departamentoi;
	public $provinciai;
	public $distritoi;
	public $campana;
	public $idtecnicoAsig;

	public $idcita;
	public $ordenContrato;
	public $tth_instalacion;
	public $tth_trasalado;

	public $idtec;
	public $nomtec;
	public $correotec;
	public $statusAccept;

	public $idcustomer;
	public $idcompany;
	public $lastname;
	public $firstname;
	public $idorder;
	public $orte ;
	public $idvehicle;
    public $cliente;
    public $license;
    public $idchasis;
    public $engine;
    public $brand;
    public $descmodel;
    public $yearmodel;

    public $idtablaProgramacion;
    public $fecha;
    public $hora;

    public $sendMail;
    public $idCargoB;
    public $apellidos;
    public $dateAccept;

    public $camapana;
    public $enviados;
    public $aceptados;
    public $porc;
    public $recordmail;
    public $recordsms;

    public $dateSend;

    public $tipo;
    public $mail;
    public $ttvehiculos;
    public $identdoc;
    public $ttvehicleclient;
    public $truevehicle;
    public $phonemain;

	//EDWARD : VARIABLE AGREGADA
	public $fechaprogramacion;

	public $tecnicolist;

	public $latitud;
	public $longitud;

	public $startDate; 
	public $endDate;

	function __construct()
	{
		
	}

     public function registrarCambiosDesdeResh(){
        global $db;
		$this->fechaCreacion=date("Y-m-d H:i:s");
        global $session;
        $fechaR=date('YmdHis');
        $sqlrc="INSERT INTO cambio_web_tracks (idcustomer,created_at,idvehicle,idlocation,sim,estadoAnt,estadoNew,tipoCambio,idusuario) VALUES ('";
        $sqlrc.=$db->escapeString($this->idcliente)."','";
        $sqlrc.=$db->escapeString($this->fechaCreacion)."','";
        $sqlrc.=$db->escapeString($this->idvehiculo)."','";
        $sqlrc.=$db->escapeString($this->idlocation)."','";
        $sqlrc.=$db->escapeString($this->sim)."','";
        $sqlrc.=$db->escapeString($this->idcita)."','";
        $sqlrc.=$db->escapeString($this->ordenContrato)."','";
        $sqlrc.=" desde plataforma Resh','";
        $sqlrc.=$db->escapeString($session->idusuario)."')";
        return ($db->query($sqlrc))?true:false;
    }


	public function programarInstalacion($idvehiculo,$idcliente){
		$this->fechaCreacion=date("YmdHis");
		global $session;
		$this->idCreador=$session->idusuario;
		$this->idvehiculo=$idvehiculo;
		$this->idcliente=$idcliente;
		global $db;
		$sql="INSERT INTO operaciones (idcliente,idvehiculo,idTecnicoAsignado,numeroOrte,datosfacturacionid,lugarInstalacion,nombreConcecionarioInstalicion,departamentoi,provinciai,distritoi,direccionInstacion,referenciaInstalacion,coordenadasInstalacion,fechaInstalacion,nombreContacto,numeroContacto,observacion,idCreador,estado,fechaCreacion,trabajo,tth_instalacion,tth_trasalado) ";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString($this->idcliente)."' , '";
		$sql.= $db->escapeString($this->idvehiculo)."' , '";
		$sql.= $db->escapeString($this->idTecnicoAsignado)."' , '";
		$sql.= $db->escapeString($this->numeroOrte)."' , '";
		$sql.= $db->escapeString($this->datosfacturacionid)."' , '";
		$sql.= $db->escapeString($this->lugarInstalacion)."' , '";
		$sql.= $db->escapeString($this->nombreConcecionarioInstalicion)."' , '";
		$sql.= $db->escapeString($this->departamentoi)."' , '";
		$sql.= $db->escapeString($this->provinciai)."' , '";
		$sql.= $db->escapeString($this->distritoi)."' , '";
		$sql.= $db->escapeString($this->direccionInstacion)."' , '";
		$sql.= $db->escapeString($this->referenciaInstalacion)."' , '";
		$sql.= $db->escapeString($this->coordenadasInstalacion)."' , '";
		$sql.= $db->escapeString($this->fechaInstalacion)."' , '";
		$sql.= $db->escapeString($this->nombreContacto)."' , '";
		$sql.= $db->escapeString($this->numeroContacto)."' , '";
		$sql.= $db->escapeString($this->observacion)."' , '";
		$sql.= $db->escapeString($this->idCreador)."' , '";
		$sql.= $db->escapeString($this->estado)."' , '";
        $sql.= $db->escapeString($this->fechaCreacion)."' , '";
		$sql.= $db->escapeString($this->trabajo)."','";
        $sql.= $db->escapeString(preg_replace('/\s+/', '', $this->tth_instalacion))."' , '";
        $sql.= $db->escapeString($this->tth_trasalado)."')";

		if ($db->query($sql)) {
			$this->idoperacion=$db->ultimoIdInsertado();			
			$actualizarEstadoQ="UPDATE vehiculos SET estado = 1 WHERE idvehiculo = '$idvehiculo'";
			if ($db->query($actualizarEstadoQ)) {				
			}
			return true;
		}else{
			return false;
		}
	}

	public function programarInstalacionDesdeCita($idvehiculo,$idcliente){
		$this->fechaCreacion=date("YmdHis");
		global $session;
		$this->idvehiculo=$idvehiculo;
		$this->idcliente=$idcliente;
		global $db;
		$sql="INSERT INTO operaciones (idcliente,idvehiculo,idTecnicoAsignado,numeroOrte,datosfacturacionid,fechaConfirmacionOPE, idConfirmacionOPE,lugarInstalacion,nombreConcecionarioInstalicion,departamentoi,provinciai,distritoi,direccionInstacion,referenciaInstalacion,coordenadasInstalacion,fechaInstalacion,nombreContacto,numeroContacto,observacion,idCreador,estado,ordenContrato,fechaCreacion) ";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString($this->idcliente)."' , '";
		$sql.= $db->escapeString($this->idvehiculo)."' , '";
		$sql.= $db->escapeString($this->idTecnicoAsignado)."' , '";
		$sql.= $db->escapeString($this->numeroOrte)."' , '";
		$sql.= $db->escapeString($this->datosfacturacionid)."' , '";
		$sql.= $db->escapeString($this->fechaConfirmacionOPE)."' , '";
		$sql.= $db->escapeString($this->idConfirmacionOPE)."' , '";
		$sql.= $db->escapeString($this->lugarInstalacion)."' , '";
		$sql.= $db->escapeString($this->nombreConcecionarioInstalicion)."' , '";
		$sql.= $db->escapeString($this->departamentoi)."' , '";
		$sql.= $db->escapeString($this->provinciai)."' , '";
		$sql.= $db->escapeString($this->distritoi)."' , '";
		$sql.= $db->escapeString($this->direccionInstacion)."' , '";
		$sql.= $db->escapeString($this->referenciaInstalacion)."' , '";
		$sql.= $db->escapeString($this->coordenadasInstalacion)."' , '";
		$sql.= $db->escapeString($this->fechaInstalacion)."' , '";
		$sql.= $db->escapeString($this->nombreContacto)."' , '";
		$sql.= $db->escapeString($this->numeroContacto)."' , '";
		$sql.= $db->escapeString($this->observacion)."' , '";
		$sql.= $db->escapeString($this->idCreador)."' , '";
		$sql.= $db->escapeString($this->estado)."' , '";
		$sql.= $db->escapeString($this->ordenContrato)."', '";
		$sql.= $db->escapeString($this->fechaCreacion)."')";
		if ($db->query($sql)) {
			$this->idoperacion=$db->ultimoIdInsertado();			
			$actualizarEstadoQ="UPDATE vehiculos SET estado = '2' WHERE idvehiculo = '$idvehiculo'";
			$sqll="UPDATE citasTaller SET idoperacion='$this->idoperacion' WHERE idcita='$this->idcita'";	
			$db->query($sqll);
			if ($this->estado=='36') {
				if ($db->query($actualizarEstadoQ)) {									
				}				
			}
			return true;
		}else{
			return false;
		}
	}
	public function programarInstalacionDesdeCrearCliente($idvehiculo,$idcliente){
		$this->fechaCreacion=date("YmdHis");
		global $session;
		global $db;
		$this->idvehiculo=$idvehiculo;
		$this->idcliente=$idcliente;
		$this->idUsuarioCargo=$session->idusuario;
		$sql="INSERT INTO operaciones (idcliente,idvehiculo,idTecnicoAsignado,numeroOrte,datosfacturacionid,fechaConfirmacionOPE, idConfirmacionOPE,lugarInstalacion,nombreConcecionarioInstalicion,departamentoi,provinciai,distritoi,direccionInstacion,referenciaInstalacion,coordenadasInstalacion,fechaInstalacion,nombreContacto,numeroContacto,observacion,idUsuarioCargo,idCreador,estado,fechaCreacion) ";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString($this->idcliente)."' , '";
		$sql.= $db->escapeString($this->idvehiculo)."' , '";
		$sql.= $db->escapeString($this->idTecnicoAsignado)."' , '";
		$sql.= $db->escapeString($this->numeroOrte)."' , '";
		$sql.= $db->escapeString($this->datosfacturacionid)."' , '";
		$sql.= $db->escapeString($this->fechaConfirmacionOPE)."' , '";
		$sql.= $db->escapeString($this->idConfirmacionOPE)."' , '";
		$sql.= $db->escapeString($this->lugarInstalacion)."' , '";
		$sql.= $db->escapeString($this->nombreConcecionarioInstalicion)."' , '";
		$sql.= $db->escapeString($this->departamentoi)."' , '";
		$sql.= $db->escapeString($this->provinciai)."' , '";
		$sql.= $db->escapeString($this->distritoi)."' , '";
		$sql.= $db->escapeString($this->direccionInstacion)."' , '";
		$sql.= $db->escapeString($this->referenciaInstalacion)."' , '";
		$sql.= $db->escapeString($this->coordenadasInstalacion)."' , '";
		$sql.= $db->escapeString($this->fechaInstalacion)."' , '";
		$sql.= $db->escapeString($this->nombreContacto)."' , '";
		$sql.= $db->escapeString($this->numeroContacto)."' , '";
		$sql.= $db->escapeString($this->observacion)."' , '";
		$sql.= $db->escapeString($this->idUsuarioCargo)."' , '";
		$sql.= $db->escapeString($this->idCreador)."' , '";
		$sql.= $db->escapeString($this->estado)."' , '";
		$sql.= $db->escapeString($this->fechaCreacion)."')";
		if ($db->query($sql)) {
			$this->idoperacion=$db->ultimoIdInsertado();			
			$actualizarEstadoQ="UPDATE vehiculos SET estado = 1 WHERE idvehiculo = '$idvehiculo'";
			if ($db->query($actualizarEstadoQ)) {				
			}
			return true;
		}else{
			return false;
		}
	}


	public function confirmarInstalacionSinEstado($idoperacion,$idvehiculo){
		global $db;
		global $session;
		$this->fechaPlataformaV=date("YmdHis");
		$sql="UPDATE operaciones SET ";
		$sql.=" idTecnicoAsignado= '".$db->escapeString($this->idTecnicoAsignado)."' , ";
		$sql.=" numeroOrte= '".$db->escapeString(trim($this->numeroOrte))."' , ";
        $sql.=" numeroOrden= '".$db->escapeString(trim($this->numeroOrden))."' , ";
		$sql.=" lugarInstalacion= '".$db->escapeString($this->lugarInstalacion)."' , ";
		$sql.=" direccionInstacion= '".$db->escapeString($this->direccionInstacion)."' , ";
		$sql.=" referenciaInstalacion= '".$db->escapeString($this->referenciaInstalacion)."' , ";
		$sql.=" nombreContacto= '".$db->escapeString($this->nombreContacto)."' , ";
		$sql.=" numeroContacto= '".$db->escapeString($this->numeroContacto)."' , ";
		$sql.=" fechaInstalacion= '".$db->escapeString($this->fechaInstalacion)."' , ";
		$sql.=" departamentoi= '".$db->escapeString($this->departamentoi)."' , ";
		$sql.=" nombreConcecionarioInstalicion= '".$db->escapeString($this->nombreConcecionarioInstalicion)."' , ";
		$sql.=" provinciai= '".$db->escapeString($this->provinciai)."' , ";
        $sql.=" ordenContrato= '".$db->escapeString(trim($this->ordenContrato))."' , ";
		$sql.=" distritoi= '".$db->escapeString($this->distritoi)."' , ";
		$sql.=" idUsuarioPlataforma= '".$db->escapeString($session->idusuario)."', ";
		$sql.=" fechaPlataformaV= '".$db->escapeString($this->fechaPlataformaV)."' ";
		$sql.=" WHERE idoperacion= '".$db->escapeString($idoperacion)."' ";
		if ($db->query($sql)) {
			#$actualizarEstadoQ="UPDATE vehiculos SET estado ='2' WHERE idvehiculo = '$idvehiculo'";
			#if ($db->query($actualizarEstadoQ)) {
			#}
			return true;
		}else{
			return false;
		}
	}

	public function confirmarInstalacion($idoperacion,$idvehiculo){
		global $db;
		global $session;
		$this->fechaPlataformaV=date("YmdHis");
		$sql="UPDATE operaciones SET ";
		$sql.=" idTecnicoAsignado= '".$db->escapeString(empty($this->idTecnicoAsignado) ? 0 : $this->idTecnicoAsignado)."' , ";
		//$sql.=" idTecnicoAsignado=0, "; //taller-link-r
		$sql.=" numeroOrte= '".$db->escapeString(trim($this->numeroOrte))."' , ";
        $sql.=" numeroOrden= '".$db->escapeString(trim($this->numeroOrden))."' , ";
		$sql.=" lugarInstalacion= '".$db->escapeString($this->lugarInstalacion)."' , ";
		$sql.=" direccionInstacion= '".$db->escapeString($this->direccionInstacion)."' , ";
		$sql.=" referenciaInstalacion= '".$db->escapeString($this->referenciaInstalacion)."' , ";
		$sql.=" nombreContacto= '".$db->escapeString($this->nombreContacto)."' , ";
		$sql.=" numeroContacto= '".$db->escapeString($this->numeroContacto)."' , ";
		$sql.=" fechaInstalacion= '".$db->escapeString($this->fechaInstalacion)."' , ";
		$sql.=" departamentoi= '".$db->escapeString($this->departamentoi)."' , ";
		$sql.=" nombreConcecionarioInstalicion= '".$db->escapeString($this->nombreConcecionarioInstalicion)."' , ";
		$sql.=" provinciai= '".$db->escapeString($this->provinciai)."' , ";
        $sql.=" ordenContrato= '".$db->escapeString(trim($this->ordenContrato))."' , ";
		$sql.=" distritoi= '".$db->escapeString($this->distritoi)."' , ";
		$sql.=" idUsuarioPlataforma= '".$db->escapeString($session->idusuario)."', ";
		$sql.=" fechaPlataformaV= '".$db->escapeString($this->fechaPlataformaV)."', ";
        $sql.=" idtecnicoAsig= '".$db->escapeString($this->idtecnicoAsig)."', ";
		$sql.=" estado= '36' ";
		$sql.=" WHERE idoperacion= '".$db->escapeString($idoperacion)."' ";
		if ($db->query($sql)) {
			$actualizarEstadoQ="UPDATE vehiculos SET estado ='2' WHERE idvehiculo = '$idvehiculo'";
			if ($db->query($actualizarEstadoQ)) {
				
			}
			return true;
		}else{
			return false;
		}
	}

 public function updateOrdenContrato()
 {
     global $db;
     $sql = "UPDATE operaciones SET ";
     $sql .= " ordenContrato= '" . $db->escapeString(trim($this->ordenContrato)) . "'  ";
     $sql .= " WHERE idoperacion= '" . $db->escapeString(trim($this->idoperacion)) . "' ";
     if ($db->query($sql)) {
         return true;
     } else {
         return false;
     }
 }


    public function updateOrdenContratoTerm()
    {
        global $db;
        $sql = "UPDATE Terms SET ";
        $sql .= " orders= '" . $db->escapeString(trim($this->ordenContrato)) . "'  ";
        $sql .= " WHERE idOperation= '" . $db->escapeString(trim($this->idoperacion)) . "' ";
        if ($db->query($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function updateOrdenFactContraOpera(){
        global $db;
        $sql="UPDATE operaciones SET ";
        $sql.=" numeroOrden= '".$db->escapeString(trim($this->numeroOrden))."' , ";
        $sql.=" numeroOrte= '".$db->escapeString(trim($this->numeroOrte))."' , ";
        $sql.=" idtecnicoAsig= '".$db->escapeString(trim($this->idtecnicoAsig))."' , ";
        $sql.=" ordenContrato= '".$db->escapeString(trim($this->ordenContrato))."'  ";
        $sql.=" WHERE idoperacion= '".$db->escapeString(trim($this->idoperacion))."' ";
        if ($db->query($sql)) {
            return true;
        }else{
            return false;
        }
    }


	public static function buscarCliente($datos){
		global $db;
		$n=0;
		$existe=self::sentencia("SELECT DISTINCT V.chasis,C.nombre as nombre ,C.razonSocial,C.dni,C.ruc, C.apellidosC,V.placa FROM clientes C, vehiculos V  WHERE C.dni='".$datos."' or C.ruc='".$datos."' or V.placa='".$datos."' or V.chasis='".$datos."' ");
		if ($existe) {
			
		foreach ($existe as $var) :
		$n++;
	?>
	<tr>
       <td><?php echo $n ?></td>
        <td><?php echo $retVal = ($var->tipoCliente=="Natural") ? $var->dni : $var->ruc ; ?></td>
        <td><?php echo $retVal1 = ($var->tipoCliente=="Natural") ? $var->nombre." ".$var->apellidosC : $var->razonSocial ; ?></td>
        <td><?php echo obtenerEstado($var->estado)  ?></td>
        <td><?php echo $var->placa  ?></td><!-- falta -->
        <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#ordenes" data-id="<?php echo $var->idvehiculo ?>">Elegir <i class="fa fa-cog"> </i></button></td>
    </tr>
   
	<?php
		endforeach;
		}else{
			return false;
		}
		
	}

	public function obtenerVehiculos($estado){
		global $db;
		$n=0;
		$vhObj=self::sentencia("SELECT * FROM vehiculos WHERE estado='$estado' or estado=13");
		if ($vhObj) {
			foreach ($vhObj as $var) :
				$n++;
			?>
			<tr class="gradeX">
				<td><?php echo $n ?></td>
				<td><?php $clienteObj =Cliente::obtenerDatosDeclientePorId($var->idcliente);?> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? $clienteObj->nombre." ".$clienteObj->apellidosC : $clienteObj->razonSocial ; ?></td>
				<td><?php echo $var->placa ?></td>
				<td><?php echo $var->marca ?></td>
				<td class="center"><?php echo $var->modelo ?></td>
				<td class="center"><?php echo obtenerEstado($var->estado)  ?></td>


				<?php if ($var->estado==3||$var->estado==2||$var->estado==1): ?>
					<td class="center "><button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#ordenes" data-id="<?php echo $var->idvehiculo ?>">Elegir <i class="fa fa-cog"> </i></button></td>
				<?php endif ?>


				<td class="center"><?php echo $var->plan ?> <?= $var->campana ?></td>
			<?php if ($var->estado==3||$var->estado==2||$var->estado==1): ?>
					<td class="center"><?php echo $var->chasis ?></td>
					<td class="center"><?php echo $var->color ?></td>
					<td class="center"><strong class="font-bold">
						<?php $usuario=Usuario::obtenerUsuario($var->idcreador);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno;  ?>
					</strong>
				</span><small><?php $usuario=Usuario::obtenerRoldeUsuario($usuario->idrol);  echo $usuario->cargo;?> </small></span>&nbsp;<?php echo hace($var->fechaCreacion) ?></td>
			<?php endif ?>


		<?php if ($var->estado==4): ?>
			<td class="center"><?php echo hace($var->fechaBienvenida) ?></td>
			<td class="center"><strong class="font-bold">
				<?php $usuario=Usuario::obtenerUsuario($var->idUsuarioBienvenida);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno;  ?>
			</strong>
		</span><small><?php $usuario=Usuario::obtenerRoldeUsuario($usuario->idrol);  echo $usuario->cargo;?> </small></span>&nbsp;<?php echo hace($var->fechaCreacion) ?></td>
			<td class="center"><?php echo $var->accionBienvenida ?></td>
				
		<?php endif ?>


	</tr>
	<?php
	endforeach;			
	return true;
} else {
	return false;
}

}

	public function obtenerOrdenes($estado){
		global $db;
		$n=0;
		$operacionesInstalar=self::sentencia("SELECT * FROM operaciones WHERE estado = '$estado'");
		if ($operacionesInstalar) {
			foreach ($operacionesInstalar as $var) :
				$n++;
				?>
				<tr class="gradeX">
					<td><?php echo $n ?></td>
					<td><?php echo $var->numeroOrte ?></td>
					<td><?php echo $var->trabajo ?></td>

					<?php if ($var->idlocation == 0): ?>
						<td><?php $clienteObj =Cliente::obtenerDatosDeclientePorId($var->idcliente);?> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? $clienteObj->nombre." ".$clienteObj->apellidosC : $clienteObj->razonSocial ; ?></td>
						<td><?php $vehiculoObj = Vehiculo::obtenerVehiculo($var->idvehiculo); echo $vehiculoObj->placa ?></td>  
						<td><?php echo formatoHaceEn($var->fechaInstalacion) ?></td>
						<td><?php echo obtenerEstado($var->estado) ?></td>
						<td><button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#instalar" data-id="<?php echo $var->idoperacion ?>">Elegir <i class="fa fa-cog"> </i></button></td>
						<td><?php  echo $vehiculoObj->chasis ?></td>
						<td><?php  echo $vehiculoObj->marca ?></td>
						<td><?php  echo $vehiculoObj->modelo ?></td>
						<td><?php  echo $vehiculoObj->color ?></td>
						<td><strong class="font-bold">
							<?php $usuarioEjecutivo=Usuario::obtenerUsuario($clienteObj->idCreador);  echo $usuarioEjecutivo->usuarioNombre;?>&nbsp;<?php echo $usuarioEjecutivo->usuarioApellidoPaterno;  ?>
						</strong>
					</span><small><?php $usuarioEjecutivo=Usuario::obtenerRoldeUsuario($usuarioEjecutivo->idrol);  echo $usuarioEjecutivo->cargo;?> </small></span></td>

					<?php else: ?>

					<?php $clienteObj=Onix::obtenerDatosDesdeIds($var->idcliente,$var->idvehiculo,$var->idlocation) ?>
						<?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
					<td><?= $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ; ?></td>
					<td><?= $clienteObj->placa ?></td>
					<td><?php echo formatoHaceEn($var->fechaInstalacion) ?></td>
					<td><?php echo obtenerEstado($var->estado) ?></td>
					<td><button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#instalar" data-id="<?php echo $var->idoperacion ?>">Elegir <i class="fa fa-cog"> </i></button></td>
					<td><?php  echo $clienteObj->chasis ?></td>
					<td><?php  echo $clienteObj->marca ?></td>
					<td><?php  echo $clienteObj->modelo ?></td>
					<td><?php  echo $clienteObj->color ?></td>				
					<?php endif ?>
				</tr>

				<?php
			endforeach;			
			return true;
		} else {
			return false;
		}
	}

	public static function obtenerRealizadosPorMonitoreo(){
		$obtenerRealizadosPorMonitoreo=self::sentencia("SELECT * FROM operaciones WHERE estado='15' OR estado ='777' ORDER BY idoperacion DESC LIMIT 30");
		return !empty($obtenerRealizadosPorMonitoreo) ? $obtenerRealizadosPorMonitoreo :false;
	}

	public static function obtenerOPERACIONESCONORTE($orte){
		$obtenerRealizadosPorMonitoreo=self::sentencia("SELECT * FROM operaciones WHERE numeroOrte='$orte'");
		return !empty($obtenerRealizadosPorMonitoreo) ? $obtenerRealizadosPorMonitoreo :false;
	}

	public function obtenerInstalaciones($estado){
		global $db;
		$n=0;
		$obtenerInstalacionesQ=self::sentencia("SELECT * FROM operaciones WHERE estado = '$estado' ORDER BY idoperacion DESC");
		if ($obtenerInstalacionesQ) {
			foreach ($obtenerInstalacionesQ as $var) :
				$n++;
				?>
				<tr class="gradeX">
					<td><?php echo $n ?></td>
					<td><?php echo $var->numeroOrte ?></td>
					<td><?php echo $var->trabajo ?></td>
					<td><?php $clienteObj = Cliente::obtenerDatosDeclientePorId($var->idcliente);?> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? $clienteObj->nombre." ".$clienteObj->apellidosC : $clienteObj->razonSocial ; ?></td>
					<td><?php $vehiculoObj = Vehiculo::obtenerVehiculo($var->idvehiculo); echo $vehiculoObj->placa ?></td>		        
					<td><?php echo hace($var->fechaActivacion) ?></td>
					<td><?php echo $vehiculoObj->plan ?></td>
					<td><?php echo obtenerEstado($var->estado) ?></td>
					<td><button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#instalar" data-id="<?php echo $var->idoperacion ?>">Ver <i class="fa fa-cog"> </i></button></td>
					<td><?php  echo $var->imei ?></td>
					<td><?php  echo $var->id ?></td>
					<td><?php  echo $vehiculoObj->modelo ?></td>
					<td><?php  echo obtenerEstado($vehiculoObj->estado) ?></td>
					<td><?php  echo $vehiculoObj->chasis ?></td>

					<td><strong class="font-bold">
						<?php $usuarioEjecutivo=Usuario::obtenerUsuario($clienteObj->idCreador);  echo $usuarioEjecutivo->usuarioNombre;?>&nbsp;<?php echo $usuarioEjecutivo->usuarioApellidoPaterno;  ?>
					</strong>
				</span><small><?php $usuarioEjecutivo=Usuario::obtenerRoldeUsuario($usuarioEjecutivo->idrol);  echo $usuarioEjecutivo->cargo;?> </small></span></td>

				<td><strong class="font-bold">
						<?php $usuarioTecnico=Usuario::obtenerUsuario($var->idTecnicoAsignado);  echo $usuarioTecnico->usuarioNombre;?>&nbsp;<?php echo $usuarioTecnico->usuarioApellidoPaterno;  ?>
					</strong>
				</span><small><?php $usuarioTecnico=Usuario::obtenerRoldeUsuario($usuarioTecnico->idrol);  echo $usuarioTecnico->cargo;?> </small></span></td>

				<td><strong class="font-bold">
						<?php $usuarioTecnicoI=Usuario::obtenerUsuario($var->idTecnicoInstalador);  echo $usuarioTecnicoI->usuarioNombre;?>&nbsp;<?php echo $usuarioTecnicoI->usuarioApellidoPaterno;  ?>
					</strong>
				</span><small><?php $usuarioTecnico=Usuario::obtenerRoldeUsuario($usuarioTecnico->idrol);  echo $usuarioTecnico->cargo;?> </small></span></td>


					<td><strong class="font-bold">
						<?php $usuario=Usuario::obtenerUsuario($var->idCreador);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno;  ?>
					</strong>
				</span><small><?php $usuario=Usuario::obtenerRoldeUsuario($usuario->idrol);  echo $usuario->cargo;?> </small></span>&nbsp;<?php echo hace($var->fechaCreacion) ?></td>
					<td><strong class="font-bold">
						<?php $usuario=Usuario::obtenerUsuario($var->idusuarioActivador);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno;  ?>
					</strong>
				</span><small><?php $usuario=Usuario::obtenerRoldeUsuario($usuario->idrol);  echo $usuario->cargo;?> </small></span>&nbsp;<?php echo hace($var->fechaActivacion) ?></td>

		</tr>
				<?php
			endforeach;			
			return true;
		} else {
			return false;
		}
	}

	public static function obtenerOperacionPorId($idoperacion){
		global $db;
		$idd=htmlspecialchars(htmlentities($idoperacion));
		$obtenerOperacionPorIdQ=self::sentencia("SELECT * FROM operaciones WHERE idoperacion='".$db->escapeString($idd)."' LIMIT 1");
		return !empty($obtenerOperacionPorIdQ) ? array_shift($obtenerOperacionPorIdQ):false; 
	}

	//taller-link-r op.latitud,op.longitud
	public static function obtenerTodolosDatosPoridOperacion($idoperacion){
		global $db;
		$idd=htmlspecialchars(htmlentities($idoperacion));
		$sqlque="SELECT op.datosfacturacionid, op.nombreConcecionarioInstalicion, op.idoperacion,op.idcliente,op.idvehiculo,op.idlocation,
                op.trabajo,op.lugarInstalacion,op.observacion,op.idtecnicoAsig,op.latitud,op.longitud,
                CASE WHEN COALESCE(op.idchequeo,0)=0 THEN op.direccionInstacion
                ELSE (SELECT chq.direccion FROM chequeos chq where chq.idchequeo=op.idchequeo) END direccionInstacion, 
                CASE WHEN COALESCE(op.idchequeo,0)=0 THEN op.referenciaInstalacion
                ELSE (SELECT chq.referencia FROM chequeos chq where chq.idchequeo=op.idchequeo) END referenciaInstalacion, 
                 dis.distrito distritoi, pro.provincia provinciai ,
                 dep.departamento departamentoi, op.numeroOrte,op.numeroContacto,op.nombreContacto,op.idChequeo,op.observacion,
                 op.numeroOrte FROM operaciones op LEFT JOIN distritos dis ON (op.distritoi = dis.iddistrito) LEFT JOIN provincias pro
                 ON (op.provinciai=pro.idprovincia) INNER JOIN departamentos dep ON (op.departamentoi=dep.iddepartamento) WHERE idoperacion='".$db->escapeString($idd)."' LIMIT 1";
                $obtenerOperacionPorIdQ=self::sentencia($sqlque);
		return !empty($obtenerOperacionPorIdQ) ? array_shift($obtenerOperacionPorIdQ):false; 
	}

	public function instalar($idoperacion,$idvehiculo){
		$this->fechaActivacion=date("YmdHis");
		global $db;
		global $session;
		$this->idusuarioActivador=$session->idusuario;
		$sql="UPDATE operaciones SET ";
		$sql.="idTecnicoInstalador = '".$db->escapeString($this->idTecnicoInstalador)."' , ";
		$sql.="imei = '".$db->escapeString($this->imei)."' , ";
		$sql.="id = '".$db->escapeString($this->id)."' , ";
		$sql.="odometro = '".$db->escapeString($this->odometro)."' , ";
		$sql.="ubicacionSistema = '".$db->escapeString($this->ubicacionSistema)."' , ";
		$sql.="fechaActivacion = '".$db->escapeString($this->fechaActivacion)."' , ";
		$sql.="lugarActivacion = '".$db->escapeString($this->lugarActivacion)."' , ";
		$sql.="idusuarioActivador = '".$db->escapeString($this->idusuarioActivador)."' , ";
		$sql.="coordenadas = '".$db->escapeString($this->coordenadas)."' , ";
		$sql.="pruebas = '".$db->escapeString($this->pruebas)."' , ";
		$sql.=" estado = 15  ";
		$sql.="WHERE idoperacion = $idoperacion ";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}

	public function guardarChequeoOk($idoperacion){
		$this->fechaActivacion=date("YmdHis");
		global $db;
		global $session;
		$this->idusuarioActivador=$session->idusuario;
		$sql="UPDATE operaciones SET ";
		$sql.="idTecnicoInstalador = '".$db->escapeString($this->idTecnicoInstalador)."' , ";
		$sql.="imei = '".$db->escapeString($this->imei)."' , ";
		$sql.="id = '".$db->escapeString($this->id)."' , ";
		$sql.="odometro = '".$db->escapeString($this->odometro)."' , ";
		$sql.="ubicacionSistema = '".$db->escapeString($this->ubicacionSistema)."' , ";
		$sql.="fechaActivacion = '".$db->escapeString($this->fechaActivacion)."' , ";
		$sql.="lugarActivacion = '".$db->escapeString($this->lugarActivacion)."' , ";
		$sql.="idusuarioActivador = '".$db->escapeString($this->idusuarioActivador)."' , ";
		$sql.="coordenadas = '".$db->escapeString($this->coordenadas)."' , ";
		$sql.="pruebas = '".$db->escapeString($this->pruebas)."' , ";
		$sql.=" estado = '15'  ";
		$sql.="WHERE idoperacion = $idoperacion ";
		if ($db->query($sql)) {			
			return true;
		}else{
			return false;
		}
	}


	public function guardarComoPendiente($idoperacion){
		global $db;
		global $session;
		$this->idUsuarioPendiente=$session->idusuario;
		$sql="UPDATE operaciones SET ";
		$sql.="idTecnicoInstalador = '".$db->escapeString($this->idTecnicoInstalador)."' , ";
		$sql.="imei = '".$db->escapeString($this->imei)."' , ";
		$sql.="id = '".$db->escapeString($this->id)."' , ";
		$sql.="odometro = '".$db->escapeString($this->odometro)."' , ";
		$sql.="ubicacionSistema = '".$db->escapeString($this->ubicacionSistema)."' , ";
		$sql.="fechaActivacion = '".$db->escapeString($this->fechaActivacion)."' , ";
		$sql.="lugarActivacion = '".$db->escapeString($this->lugarActivacion)."' , ";		
		$sql.="idUsuarioPendiente = '".$db->escapeString($this->idUsuarioPendiente)."' , ";
		$sql.="coordenadas = '".$db->escapeString($this->coordenadas)."' , ";
		$sql.="pruebas = '".$db->escapeString($this->pruebas)."' , ";
		$sql.="estado = '99'  ";
		$sql.="WHERE idoperacion = $idoperacion ";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
	// Obetener operaciones listas para asignar tecnicos 
	public static function obtenerListosParaInstalacion(){
		$obtenerListosParaInstalacionQ=self::sentencia("SELECT * FROM operaciones O WHERE (O.numeroOrden<>'' OR idChequeo <> 0 ) AND (O.estado = '14') AND ('si'=(SELECT enOnix FROM vehiculos V WHERE  V.idvehiculo=O.idvehiculo)  OR idChequeo <> 0 ) ORDER BY DATE_FORMAT(fechaInstalacion,'%Y-%m-%d %H:%i') DESC"); //taller-chasis-busc-mod
		return !empty($obtenerListosParaInstalacionQ)?$obtenerListosParaInstalacionQ:false;
	}

	public static function obtenerListosParaInstalacionMenuLateral(){
		$obtenerListosParaInstalacionQ=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones O WHERE (O.numeroOrden<>'' OR idChequeo <> 0 ) AND (O.estado = '14') AND ('si'=(SELECT enOnix FROM vehiculos V WHERE  V.idvehiculo=O.idvehiculo)  OR idChequeo <> 0 ) LIMIT 1");
		return !empty($obtenerListosParaInstalacionQ)?array_shift($obtenerListosParaInstalacionQ):false;
	}
	public static function obtenerDatosInstalacion($idvehiculo){
		global $db;
		$idd=htmlspecialchars(htmlentities($idvehiculo));
		$obtenerDatosInstalacionQ=self::sentencia("SELECT * FROM operaciones WHERE idvehiculo='".$db->escapeString($idd)."' AND estado=14 LIMIT 1");
		return !empty($obtenerDatosInstalacionQ) ? array_shift($obtenerDatosInstalacionQ):false; 
	}

	public static function obteneropporidvehiclo($idvehiculo){
		global $db;
		$idd=htmlspecialchars(htmlentities($idvehiculo));
		$obtenerDatosInstalacionQ=self::sentencia("SELECT * FROM operaciones WHERE idvehiculo='".$db->escapeString($idd)."' LIMIT 1");
		return !empty($obtenerDatosInstalacionQ) ? array_shift($obtenerDatosInstalacionQ):false; 
	}

	//fin here ::v
	// the reportabilidad here 
	public static function obtenerInstalacionesPorTecnico($idtecnico,$dia){
		global $db;
		$obtenerTecnicoInstaladorQ=self::sentencia("SELECT count(idTecnicoInstalador) as instalaciones FROM operaciones WHERE trabajo='Instalacion' AND fechaActivacion LIKE '$dia' AND idTecnicoInstalador='$idtecnico' LIMIT 1");
		return !empty($obtenerTecnicoInstaladorQ) ? array_shift($obtenerTecnicoInstaladorQ):false; 
	}

	public static function obtenerOperacionesRealizadas(){
		return $obtenerListosParaInstalacionQ=self::sentencia("SELECT * FROM operaciones WHERE estado=15 LIMIT 50");
	}
	
	public static function obtenerDatosParaTaller(){
		return $obtenerDatosParaTallerQ=self::sentencia("SELECT * FROM operaciones WHERE estado=14 OR estado=36 ORDER BY idoperacion DESC LIMIT 50");
	}

	public function obtenerDatosParaTallerP($posisionDePagina,$registrosPorPagina){
		$dtp=self::sentencia("SELECT * FROM operaciones  WHERE  estado='$this->estado' ORDER BY fechaInstalacion ASC LIMIT $posisionDePagina, $registrosPorPagina  ");
		return !empty($dtp)? $dtp:false;		
	}

	public function obtenerDatosParaTallerPBusqueda($datos){
		$dtp=self::sentencia("SELECT * FROM operaciones  WHERE  estado='$this->estado' AND (numeroOrte LIKE '$datos' OR numeroOrden LIKE '$datos' OR lugarInstalacion LIKE '$datos' OR trabajo LIKE '$datos') ORDER BY idoperacion DESC ");
		return !empty($dtp)? $dtp:false;	


		// SELECT * FROM operaciones OP INNER J WHERE idTecnicoAsignado='0' AND estado ='36' AND (numeroOrte LIKE '$datos' OR numeroOrden LIKE '$datos'  lugarInstalacion LIKE '$datos' OR trabajo LIKE '$datos') ORDER BY fechaInstalacion ASC 	
	}

	public function obtenerDatosParaTallerCantidadTotal(){
		$obtenerDatosParaTallerQ=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE estado='$this->estado' ORDER BY idoperacion DESC LIMIT 1");
		return !empty($obtenerDatosParaTallerQ)? array_shift($obtenerDatosParaTallerQ) :false;
	}

	public static function asignarCagoOperaciones($idoperacion){
		global $session;
		global $db;
		$sql="UPDATE operaciones SET idUsuarioCargo='".$session->idusuario."' WHERE idoperacion ='".$idoperacion."' ";
		if ($db->query($sql)) {
			
		}
	}

    public static function obtenerTecnios(){
        global $db;
        $Obj=self::sentencia("SELECT id as idtec,last_name nomtec,correo correotec FROM tecnico_user where estado='A'");
        return !empty($Obj)?$Obj:false;
    }

    public static function obtenerTecnico($idtec){
        global $db;
        $sql="SELECT DISTINCT tu.id as idtec,tu.last_name nomtec,tu.correo correotec 
            FROM operaciones o INNER JOIN tecnico_user tu on tu.id=o.idtecnicoAsig 
            where tu.estado='A' and tu.id='$idtec'";
        $Obj=self::sentencia($sql);
        return !empty($Obj) ? array_shift($Obj):false;
    }

    public static function obtenerInfoClienteOperacion($orte,$order){
        global $db;
        $sql="SELECT DISTINCT sc.idcustomer,sc.idlocation ,sc.idcompany ,sc.lastname,sc.firstname,so.idorder ,so.orte,sov.idvehicle
        FROM sid_customer sc
        INNER JOIN sid_order so on so.idcustomer = sc.idcustomer and so.idloccust =sc.idlocation
        INNER JOIN sid_orderveh sov on sov.idorder =so.idorder and sov.idlocation =so.idlocation  and sov.idcompany =so.idcompany 
        WHERE so.orte ='$orte' and so.idorder ='$order'";
        $Obj=self::sentenciaOnix($sql);
        return !empty($Obj) ? array_shift($Obj):false;
    }

    public static function obtenerInfoOnyxMail($idcustomer,$idlocation,$idcompany,$idvehicle){
        global $db;
        $sql="SELECT CONCAT(sc.lastname,' ',sc.firstname ) cliente,sv.license ,sv.idchasis ,sv.engine,
        (SELECT sb.descbrand FROM sid_brand sb where sb.idbrand=sv.idbrand) brand,sv.descmodel,sv.yearmodel,
        sv.color
        FROM sid_customer sc 
        INNER JOIN sid_vehicle sv on sv.idcustomer=sc.idcustomer and sv.idlocation =sc.idlocation  and sv.idcompany =sc.idcompany 
        where sv.idcustomer ='$idcustomer' and sv.idlocation ='$idlocation' and sv.idcompany ='$idcompany'
        and idvehicle ='$idvehicle'";
        $Obj=self::sentenciaOnix($sql);
        return !empty($Obj) ? array_shift($Obj):false;
    }



	public static function asignarAgregarPorResoluci($idoperacion){
		global $session;
		global $db;
		$sql="UPDATE operaciones SET estado='3' WHERE idoperacion ='".$idoperacion."' ";
		if ($db->query($sql)) {
			
		}
	}

	////////////////////////////////chequeos """#"#"#"!#!"#123
	public function registrarChequeo(){
		global $session;
		global $db;
		$this->fechaCreacion=date("YmdHis");
		$this->idCreador=$session->idusuario;
		$sql="INSERT INTO operaciones (idChequeo,idvehiculo,idcliente,idlocation,numeroOrden,nombreContacto,numeroContacto,idCreador,estado,fechaCreacion,fechaInstalacion,departamentoi,provinciai,distritoi,lugarInstalacion,observacion,trabajo) VALUES ('";
		$sql.=$db->escapeString($this->idChequeo)."' , '";
		$sql.=$db->escapeString($this->idvehiculo)."' , '";
		$sql.=$db->escapeString($this->idcliente)."' , '";
		$sql.=$db->escapeString($this->idlocation)."', '";
		$sql.=$db->escapeString($this->numeroOrden)."', '";
		$sql.=$db->escapeString($this->nombreContacto)."', '";
		$sql.=$db->escapeString($this->numeroContacto)."', '";
		$sql.=$db->escapeString($this->idCreador)."', '";
		$sql.=$db->escapeString($this->estado)."', '";
		$sql.=$db->escapeString($this->fechaCreacion)."', '";
		$sql.=$db->escapeString($this->fechaInstalacion)."', '";
		$sql.=$db->escapeString($this->departamentoi)."', '";
		$sql.=$db->escapeString($this->provinciai)."', '";
		$sql.=$db->escapeString($this->distritoi)."', '";
		$sql.=$db->escapeString($this->lugarInstalacion)."', '";
		$sql.=$db->escapeString($this->observacion)."', '";
		$sql.=$db->escapeString($this->trabajo)."' )";
		if ($db->query($sql)) {
			$this->idoperacion=$db->ultimoIdInsertado();
			return true;
		} else {
			return false;
		}
		
	}
	public function guardarChequeoParaMonitoreo(){
		global $session;
		global $db;
		$this->fechaModficacionOrte=date("YmdHis");
		$this->fechaPlataformaV=date("YmdHis");
		$sql="UPDATE operaciones SET ";
		$sql.="numeroOrden='";
		$sql.=$db->escapeString($this->numeroOrden)."', ";
		 $sql.=" numeroOrte= '".$db->escapeString($this->numeroOrte)."' , ";
	    $sql.=" lugarInstalacion= '".$db->escapeString($this->lugarInstalacion)."' , ";
	    $sql.=" direccionInstacion= '".$db->escapeString($this->direccionInstacion)."' , ";
	    $sql.=" referenciaInstalacion= '".$db->escapeString($this->referenciaInstalacion)."' , ";
	    $sql.=" nombreContacto= '".$db->escapeString($this->nombreContacto)."' , ";
	    $sql.=" numeroContacto= '".$db->escapeString($this->numeroContacto)."' , ";
	    $sql.=" fechaInstalacion= '".$db->escapeString($this->fechaInstalacion)."' , ";
	    $sql.=" departamentoi= '".$db->escapeString($this->departamentoi)."' , ";
	    $sql.=" nombreConcecionarioInstalicion= '".$db->escapeString($this->nombreConcecionarioInstalicion)."' , ";
	    $sql.=" provinciai= '".$db->escapeString($this->provinciai)."' , ";
	    $sql.=" distritoi= '".$db->escapeString($this->distritoi)."' , ";
	    $sql.=" idUsuarioPlataforma= '".$db->escapeString($session->idusuario)."', ";
	    $sql.=" fechaPlataformaV= '".$db->escapeString($this->fechaPlataformaV)."', ";
	    $sql.=" estado= '".$db->escapeString("36")."',  ";
        $sql.=" idtecnicoAsig= '".$db->escapeString($this->idtecnicoAsig)."', ";
		$sql.="fechaModficacionOrte='";
		$sql.=$db->escapeString($this->fechaModficacionOrte)."', ";
		$sql.="idmodificador='";
		$sql.=$db->escapeString($session->idusuario)."' ";	
		$sql.=" WHERE idoperacion='";
		$sql.=$db->escapeString($this->idoperacion)."'";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
	public function guardarComoChequeoEnlinea(){
		global $db;
		global $session;
		$this->fechaModficacionOrte=date("YmdHis");
		$this->fechaPlataformaV=date("YmdHis");
		$sql="UPDATE operaciones SET";
		$sql.=" numeroOrden='";
		$sql.=$db->escapeString($this->numeroOrden)."',";
		$sql.=" fechaModficacionOrte='";
		$sql.=$db->escapeString($this->fechaModficacionOrte)."',";
		$sql.=" fechaPlataformaV='";
		$sql.=$db->escapeString($this->fechaPlataformaV)."',";
		$sql.=" idmodificador='";
		$sql.=$db->escapeString($session->idusuario)."',";
		$sql.=" estado='";
		$sql.="777' WHERE idoperacion='".$db->escapeString($this->idoperacion)."'";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}


	}
	public function guardarNumeroDeOrden(){
		global $db;
		global $session; 
		$this->fechaModficacionOrte=date("YmdHis");
		$this->fechaPlataformaV=date("YmdHis");
		$sql="UPDATE operaciones SET";
		$sql.=" idUsuarioCargo='";
		$sql.=$db->escapeString($session->idusuario)."',";
		$sql.=" fechaPlataformaV='";
		$sql.=$db->escapeString($this->fechaPlataformaV)."',";
		$sql.=" numeroOrden='";
		$sql.=$db->escapeString($this->numeroOrden)."',";
		$sql.=" fechaModficacionOrte='";
		$sql.=$db->escapeString($this->fechaModficacionOrte)."',";
		$sql.=" idmodificador='";
		$sql.=$db->escapeString($session->idusuario)."' ";
		$sql.="  WHERE idoperacion='".$db->escapeString($this->idoperacion)."'";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}

	}
///////////////////////////////////////////////////////////////
	public function obtenerInstalacionesPendientes(){		
		global $session;
		$obtenerInstalacionesPendientesQ=self::sentencia("SELECT O.*, DF.idUsuarioCreado as idCreador FROM operaciones O INNER JOIN datosFacturacion DF ON (O.datosfacturacionid=DF.datosfacturacionid) WHERE (estado=0 OR estado='321') AND '$session->idusuario'=(SELECT comercialOperaciones FROM usuarios U WHERE U.idusuario = DF.idUsuarioCreado) AND 'si'=(SELECT enOnix FROM vehiculos V WHERE V.idvehiculo=O.idvehiculo)");
		return !empty($obtenerInstalacionesPendientesQ)?$obtenerInstalacionesPendientesQ:false;
	}

	public function obtenerInstalacionesPendientesTodos(){		
		global $session;
		$obtenerInstalacionesPendientesQ=self::sentencia("SELECT O.*, DF.idUsuarioCreado as idCreador FROM operaciones O INNER JOIN datosFacturacion DF ON (O.datosfacturacionid=DF.datosfacturacionid) WHERE (estado=0 OR estado='321')  AND 'si'=(SELECT enOnix FROM vehiculos V WHERE V.idvehiculo=O.idvehiculo)");
		return !empty($obtenerInstalacionesPendientesQ)?$obtenerInstalacionesPendientesQ:false;
	}
	public static function obtenerInstaPendMenLat(){		
		global $session;
		$obtenerInstalacionesPendientesQ=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones O WHERE (estado=0 OR estado='321') AND $session->idusuario=(SELECT comercialOperaciones FROM usuarios U WHERE U.idusuario = O.idCreador ) AND 'si'=(SELECT enOnix FROM vehiculos V WHERE  V.idvehiculo=O.idvehiculo)");
		return !empty($obtenerInstalacionesPendientesQ)?array_shift($obtenerInstalacionesPendientesQ):false;
	}

	public function obtenerInstalacionesSinOrden(){
		$obtenerInstalacionesSinOrdenQ=self::sentencia("SELECT * FROM operaciones O WHERE O.numeroOrden='' AND (O.estado = '14' OR O.estado ='0') AND  'si'=(SELECT enOnix FROM vehiculos V WHERE  V.idvehiculo=O.idvehiculo) AND datosfacturacionid<>'' ORDER BY fechaConfirmacionOPE ASC");
		return !empty($obtenerInstalacionesSinOrdenQ)?$obtenerInstalacionesSinOrdenQ:false;
	}


	public function obtenerInstalacionesSinOrdenParaNotificacion(){
		$obtenerInstalacionesSinOrdenQ=self::sentencia("SELECT * FROM operaciones O WHERE O.numeroOrden='' AND (O.estado = '14' OR O.estado ='0') AND  'si'=(SELECT enOnix FROM vehiculos V WHERE  V.idvehiculo=O.idvehiculo) AND datosfacturacionid<>'' ORDER BY fechaConfirmacionOPE DESC LIMIT 1");
		return !empty($obtenerInstalacionesSinOrdenQ)? array_shift($obtenerInstalacionesSinOrdenQ):false;
	}
	//taller-link-r
	public function programarFechaDeinstalacon(){
		global $session;
		global $db;
		$this->fechaModficacion3=date("YmdHis");
		$this->idmodificador3=$session->idusuario;
		$sql="UPDATE operaciones SET ";
		$sql.=" lugarInstalacion='";
		$sql.=$db->escapeString($this->lugarInstalacion)."', ";
		$sql.=" numeroOrte='";
		$sql.=$db->escapeString($this->numeroOrte)."', ";
		$sql.=" observacion='";
		$sql.=$db->escapeString($this->observacion)."', ";
		$sql.=" nombreConcecionarioInstalicion='";
		$sql.=$db->escapeString($this->nombreConcecionarioInstalicion)."', ";
		$sql.=" departamentoi='";
		$sql.=$db->escapeString($this->departamentoi)."', ";
		$sql.=" provinciai='";
		$sql.=$db->escapeString($this->provinciai)."', ";
		$sql.=" distritoi='";
		$sql.=$db->escapeString($this->distritoi)."', ";
		$sql.=" referenciaInstalacion='";
		$sql.=$db->escapeString($this->referenciaInstalacion)."', ";
		$sql.=" coordenadasInstalacion='";
		$sql.=$db->escapeString($this->coordenadasInstalacion)."', ";

		$sql.=" latitud='";
		$sql.=$db->escapeString($this->latitud)."', ";
		$sql.=" longitud='";
		$sql.=$db->escapeString($this->longitud)."', ";

		$sql.=" idUsuarioCargo='0', ";
		$sql.=" estado='";
		$sql.=$db->escapeString($this->estado)."', ";
		$sql.=" fechaInstalacion='";
		$sql.=$db->escapeString($this->fechaInstalacion)."', ";
		$sql.=" numeroContacto='";
		$sql.=$db->escapeString($this->numeroContacto)."', ";
		$sql.=" nombreContacto='";
		$sql.=$db->escapeString($this->nombreContacto)."', ";
		$sql.=" idmodificador3='";
		$sql.=$db->escapeString($this->idmodificador3)."', ";
		$sql.=" fechaModficacion3='";
		$sql.=$db->escapeString($this->fechaModficacion3)."', ";
		$sql.=" direccionInstacion='";
		$sql.=$db->escapeString($this->direccionInstacion)."' WHERE ";
		$sql.=" idoperacion='";
		$sql.=$db->escapeString($this->idoperacion)."'";
		return ($db->query($sql))?true:false;
	}

	public function obtenerOperaionesSinFechaSeguimiento(){
		$obtenerOperaionesSinFechaSeguimiento1=self::sentencia("SELECT COUNT(o.idCreador) as cantidad FROM operaciones o INNER JOIN usuarios u ON (u.idusuario=o.idCreador AND u.idrol=3) WHERE o.fechaInstalacion='' AND o.estado=0 LIMIT 1");
		return !empty($obtenerOperaionesSinFechaSeguimiento1)?array_shift($obtenerOperaionesSinFechaSeguimiento1):false;
	}
	public function obtenerLasOperacionesSubidasPendientes($posisionDePagina,$registrosPorPagina){
		$obtenerLasOperacionesSubidasPendientes=self::sentencia("SELECT o.* FROM operaciones o INNER JOIN usuarios u ON (u.idusuario=o.idCreador AND u.idrol=3) WHERE o.fechaInstalacion ='' AND o.estado=0 LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($obtenerLasOperacionesSubidasPendientes)?$obtenerLasOperacionesSubidasPendientes:false;
	}
	public function buscarParaFiltroOperacionesPendientes($datos){
		// global $session;
		// global $db;	
		// $datos=$db->escapeString($datos);		
		// $obtenerRegistroSubidoQ=self::sentencia("SELECT *  FROM operaciones WHERE idCreador = ".$session->idusuario."  $datos=(SELECT nombre FROM clientes WHERE  )");
		// return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}

	public static function pendientesDeAsignarOrden(){
		$pendientesDeAsignarOrden=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones O WHERE O.numeroOrden='' AND (O.estado = '14' OR O.estado ='0') AND  'si'=(SELECT enOnix FROM vehiculos V WHERE  V.idvehiculo=O.idvehiculo) AND datosfacturacionid<>'' ");
		return !empty($pendientesDeAsignarOrden) ? array_shift($pendientesDeAsignarOrden):false;
	}

	public static function obtenerUltimaOperacionRegistradaaParaNotif(){
		global $session;
		$sqlULtim=self::sentencia("SELECT * FROM operaciones OP WHERE $session->idusuario = (SELECT comercialOperaciones FROM usuarios U  WHERE U.idusuario = CT.idCreador) ORDER BY idcita DESC LIMIT 1 ");
		return !empty($sqlULtim)? array_shift($sqlULtim):false;
	}

	public function actualizarDatosDeComercialesOperaciones($idope){
		global $session;
		global $db;
		$this->fechaConfirmacionOPE=date("YmdHis");
		$actualCOMOPE="UPDATE operaciones SET ";
		$actualCOMOPE.=" fechaConfirmacionOPE = '";
		$actualCOMOPE.=$db->escapeString($this->fechaConfirmacionOPE)."', ";
		$actualCOMOPE.=" idConfirmacionOPE = '";
		$actualCOMOPE.=$db->escapeString($session->idusuario)."' ";
		$actualCOMOPE.=" WHERE idoperacion='";
		$actualCOMOPE.=$db->escapeString(limpiar($idope))."' ";
		return ($db->query($actualCOMOPE)) ? true :false;
	}
	public function marcarComoReprogramado(){
		global $db;
		global $session;
		$this->fechaPlataformaV=date("YmdHis");
		$sql="UPDATE operaciones SET estado = '123', idUsuarioPlataforma='$session->idusuario', fechaPlataformaV='$this->fechaPlataformaV' WHERE idoperacion='";
		$sql.=$db->escapeString($this->idoperacion)."'";
		return ($db->query($sql))?true:false;
	}

	public function historialSacCantidadTotal(){
		$historialSacCantidadTotal=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE idUsuarioCargo <> '0' ORDER BY idoperacion DESC LIMIT 1");
		return !empty($historialSacCantidadTotal)? array_shift($historialSacCantidadTotal) :false;
	}


	public function historialSacCantidadTotalRangoFecha($desde,$hasta){
		$historialSacCantidadTotal=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE idUsuarioCargo <> '0' AND DATE_FORMAT(fechaInstalacion,'%Y-%m-%d %H:%i:%s') BETWEEN '$desde' AND '$hasta' ORDER BY idoperacion DESC LIMIT 1");
		return !empty($historialSacCantidadTotal)? array_shift($historialSacCantidadTotal) :false;
	}

	public function obtenerHistorialSac($posisionDePagina,$registrosPorPagina){
		$dtp=self::sentencia("SELECT * FROM operaciones  WHERE  idUsuarioCargo <> '0' ORDER BY fechaPlataformaV DESC LIMIT $posisionDePagina, $registrosPorPagina  ");
		return !empty($dtp)? $dtp:false;		
	}

	public function obtenerHistorialPendiente($posisionDePagina,$registrosPorPagina){
		$dtp=self::sentencia("SELECT OP.idoperacion,  CT.placa , CT.idcita FROM operaciones OP LEFT JOIN citasTaller CT ON(CT.idestado='0')  WHERE  OP.estado='0' AND OP.trabajo='Instalacion'    LIMIT $posisionDePagina, $registrosPorPagina  ");
		return !empty($dtp)? $dtp:false;		
	}


	public function obtenerHistorialSacRango($posisionDePagina,$registrosPorPagina,$desde,$hasta){
		$dtp=self::sentencia("SELECT * FROM operaciones  WHERE  idUsuarioCargo <> '0' AND DATE_FORMAT(fechaInstalacion,'%Y-%m-%d %H:%i:%s') BETWEEN '$desde' AND '$hasta' ORDER BY fechaPlataformaV DESC LIMIT $posisionDePagina, $registrosPorPagina  ");
		return !empty($dtp)? $dtp:false;		
	}

	public static function obtenerHistorialSacRangoRepo($desde,$hasta){
		$dtp=self::sentencia("SELECT * FROM operaciones  WHERE  trabajo<>'CHEQUEO EN LINEA'
 AND DATE_FORMAT(fechaInstalacion,'%Y/%m/%d %H:%i') BETWEEN '$desde' AND '$hasta' ORDER BY fechaInstalacion DESC");
		return !empty($dtp)? $dtp:false;		
	}

	public function obtenerHistorialSacBusqueda($datos){
		$dtp=self::sentencia("SELECT * FROM operaciones OP INNER JOIN clientes CL ON (OP.idcliente=CL.idcliente) INNER JOIN vehiculos VH ON(OP.idvehiculo=VH.idvehiculo) WHERE  OP.idUsuarioCargo <> '0' AND (OP.numeroOrte LIKE '$datos' OR OP.numeroOrden LIKE '$datos' OR CL.nombre LIKE '$datos' OR CL.apellidosC LIKE '$datos' OR CL.razonSocial LIKE '$datos' OR VH.placa LIKE '$datos' OR VH.chasis LIKE '$datos')  ORDER BY fechaPlataformaV LIMIT 20");
		return !empty($dtp)? $dtp:false;
	}

	public function datosParaInstalacionSinTecnico($posisionDePagina,$registrosPorPagina){
		$dtp=self::sentencia("SELECT * FROM operaciones  WHERE idTecnicoAsignado='0' AND trabajo<>'Chequeo en linea' AND fechaInstalacion <>'' AND numeroOrte <>'' AND DATE_FORMAT(fechaInstalacion,'%Y/%m/%d %H:%i') >= '2017/10/01 00:00' AND (estado ='36' OR estado = '15') ORDER BY fechaInstalacion ASC LIMIT $posisionDePagina, $registrosPorPagina  ");
		return !empty($dtp)? $dtp:false;
	}
	public function datosParaPendiente($posisionDePagina,$registrosPorPagina){
		$dtp=self::sentencia("SELECT * FROM operaciones  WHERE  estado ='99' ORDER BY fechaInstalacion ASC LIMIT $posisionDePagina, $registrosPorPagina  ");
		return !empty($dtp)? $dtp:false;
	}
	public function datosParaInstalacionSinTecnicoBusqueda($datos){
		$dtp=self::sentencia("SELECT * FROM operaciones  WHERE idTecnicoAsignado='0' AND (estado ='36' OR estado = '15') AND (numeroOrte LIKE '$datos' OR numeroOrden LIKE '$datos'  lugarInstalacion LIKE '$datos' OR trabajo LIKE '$datos') ORDER BY fechaInstalacion ASC  ");
		return !empty($dtp)? $dtp:false;
	}

	public static function asginarTecnicoMenuLateral(){
		$dtpml=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones  WHERE DATE_FORMAT(fechaInstalacion,'%Y/%m/%d %H:%i') >= '2017/10/01 00:00' AND idTecnicoAsignado='0' AND (estado ='36' OR estado = '15') LIMIT 1 ");
		return !empty($dtpml)? array_shift($dtpml):false;
	}

	public function datosParaInstalacionSinTecnicoCantidad(){
		$obtenerDatosParaTal=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE idTecnicoAsignado='0' AND trabajo<>'Chequeo en linea' AND fechaInstalacion <>'' AND numeroOrte <>'' AND DATE_FORMAT(fechaInstalacion,'%Y/%m/%d %H:%i') >= '2017/10/01 00:00'  AND (estado ='36' OR estado = '15') LIMIT 1");
		return !empty($obtenerDatosParaTal)? array_shift($obtenerDatosParaTal) :false;
	}

	public function datosParaInstalacionPendeiten(){
		$obtenerDatosParaTal=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE  estado ='99' LIMIT 1");
		return !empty($obtenerDatosParaTal)? array_shift($obtenerDatosParaTal) :false;
	}
	public function reprogramarInstalacion(){
		global $db;
		global $session;
		$this->fechaPlataformaV=date("YmdHis");
		$actu="UPDATE operaciones SET estado='123', observacion='".$this->observacion."', numeroOrte='".$this->numeroOrte."' ,idUsuarioPlataforma = '".$session->idusuario."', fechaPlataformaV='".$this->fechaPlataformaV."' WHERE idoperacion='".$this->idoperacion."'";
		if (!$db->query($actu)) {
			die("ERROR ____x 13");
		}
		$datosOperacion=self::obtenerOperacionPorId($this->idoperacion);
		$sql="INSERT INTO operaciones (idcliente, idvehiculo,  idlocation, numeroOrden, datosfacturacionid, lugarInstalacion, nombreConcecionarioInstalicion, departamentoi, provinciai, distritoi, direccionInstacion, referenciaInstalacion, coordenadasInstalacion, fechaInstalacion, nombreContacto, numeroContacto, idCreador, observacion,  tiempoServicio, fechaCreacion, idUsuarioPlataforma, trabajo, estado, idReprogramado) ";
		$sql.=" VALUES ('";
		$sql.=$datosOperacion->idcliente."', '";
		$sql.=$datosOperacion->idvehiculo."', '";
		$sql.=$datosOperacion->idlocation."', '";
		$sql.=$datosOperacion->numeroOrden."', '";
		$sql.=$datosOperacion->datosfacturacionid."', '";
		$sql.=$datosOperacion->lugarInstalacion."', '";
		$sql.=$datosOperacion->nombreConcecionarioInstalicion."', '";
		$sql.=$datosOperacion->departamentoi."', '";
		$sql.=$datosOperacion->provinciai."', '";
		$sql.=$datosOperacion->distritoi."', '";
		$sql.=$datosOperacion->direccionInstacion."', '";
		$sql.=$datosOperacion->referenciaInstalacion."', '";
		$sql.=$datosOperacion->coordenadasInstalacion."', '";
		$sql.=$datosOperacion->fechaInstalacion."', '";
		$sql.=$datosOperacion->nombreContacto."', '";
		$sql.=$datosOperacion->numeroContacto."', '";
		$sql.=$datosOperacion->idCreador."', '";
		$sql.=$datosOperacion->observacion."', '";
		$sql.=$datosOperacion->tiempoServicio."', '";
		$sql.=$datosOperacion->fechaCreacion."', '";
		$sql.=$session->idusuario."', '";
		$sql.=$datosOperacion->trabajo."', '321', '";
		$sql.=$this->idoperacion."')";
		return ($db->query($sql))?true:false;
	}

	public function asignarTecnicoOperacion(){
		global $session;
		global $db;
		$this->fechaAsignacionTec=date("Y-m-d H:i:s");
		$sql="UPDATE operaciones SET idTecnicoAsignado='";
		$sql.=$this->idTecnicoAsignado."', ";
		$sql.=" fechaInstalacion='";
		$sql.=$this->fechaInstalacion."', ";
		$sql.=" fechaAsignacionTec='";
		$sql.=$this->fechaAsignacionTec." _".$session->idusuario."' ";
		$sql.=" WHERE idoperacion='";
		$sql.=$this->idoperacion."'";
		return ($db->query($sql))?true:false;
	}
	public function obtenerDatosParaOPECCantidad(){
		$comOpeR=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE idConfirmacionOPE='$this->idConfirmacionOPE'  LIMIT 1");
		return !empty($comOpeR)? array_shift($comOpeR):false;
	}

	public function obtenerDatosParaOPECCantidadVentas(){
		$comOpeR=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones LIMIT 1");
		return !empty($comOpeR)? array_shift($comOpeR):false;
	}
	public function obtenerDatosParaOPECCantidadRANGO($desde,$hasta){
		$comOpeR=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE  idConfirmacionOPE='$this->idConfirmacionOPE' AND (fechaConfirmacionOPE BETWEEN '$desde' AND '$hasta') LIMIT 1");
		return !empty($comOpeR)? array_shift($comOpeR):false;
	}

	public function obtenerDatosParaOPECCantidadRANGOVentas($desde,$hasta){
		$comOpeR=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE fechaCreacion BETWEEN '$desde' AND '$hasta' LIMIT 1");
		return !empty($comOpeR)? array_shift($comOpeR):false;
	}
	public function obtenerDatosParaOPECCantidadEstado($estado){
		$comOpeR=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE idConfirmacionOPE='$this->idConfirmacionOPE' AND estado='$estado' LIMIT 1");
		return !empty($comOpeR)? array_shift($comOpeR):false;
	}
	public function obtenerDatosParaOPECCantidadRANGOEstado($desde,$hasta,$estado){
		$comOpeR=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE  idConfirmacionOPE='$this->idConfirmacionOPE' AND estado='$estado' AND (fechaConfirmacionOPE BETWEEN '$desde' AND '$hasta') LIMIT 1");
		return !empty($comOpeR)? array_shift($comOpeR):false;
	}
	public function obtenerDatosParaOpeC($posisionDePagina,$registrosPorPagina){
		$comOPERD=self::sentencia("SELECT OP.* FROM operaciones OP INNER JOIN datosFacturacion DF ON (OP.datosfacturacionid=DF.datosfacturacionid) WHERE '$this->idConfirmacionOPE'=(SELECT comercialOperaciones FROM usuarios U WHERE U.idusuario = DF.idUsuarioCreado)  ORDER BY fechaConfirmacionOPE DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($comOPERD)? $comOPERD:false;
	}

	public function obtenerDatosParaOpeCVentas($posisionDePagina,$registrosPorPagina){
		$comOPERD=self::sentencia("SELECT * FROM operaciones  ORDER BY fechaConfirmacionOPE DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($comOPERD)? $comOPERD:false;
	}
	public function obtenerDatosParaOpeCRango($posisionDePagina,$registrosPorPagina,$desde,$hasta){
		$comOPERD=self::sentencia("SELECT * FROM operaciones WHERE idConfirmacionOPE='$this->idConfirmacionOPE' AND (fechaConfirmacionOPE BETWEEN '$desde' AND '$hasta') ORDER BY fechaConfirmacionOPE DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($comOPERD)? $comOPERD:false;
	}

	public function obtenerDatosParaOpeCRangoVentas($posisionDePagina,$registrosPorPagina,$desde,$hasta){
		$comOPERD=self::sentencia("SELECT * FROM operaciones WHERE fechaCreacion BETWEEN '$desde' AND '$hasta' ORDER BY fechaConfirmacionOPE DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($comOPERD)? $comOPERD:false;
	}
	public function obtenerDatosParaOpeCEstado($posisionDePagina,$registrosPorPagina,$estado){
		$comOPERD=self::sentencia("SELECT * FROM operaciones WHERE  idConfirmacionOPE='$this->idConfirmacionOPE' AND estado='$estado'  ORDER BY fechaConfirmacionOPE DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($comOPERD)? $comOPERD:false;
	}
	public function obtenerDatosParaOpeCRangoEstado($posisionDePagina,$registrosPorPagina,$desde,$hasta,$estado){
		$comOPERD=self::sentencia("SELECT * FROM operaciones WHERE idConfirmacionOPE='$this->idConfirmacionOPE' AND estado='$estado' AND (fechaConfirmacionOPE BETWEEN '$desde' AND '$hasta') ORDER BY fechaConfirmacionOPE DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($comOPERD)? $comOPERD:false;
	}
	public function agregarIdOperacionesCOmercialesF(){
		global $db;
		global $session;
		$this->fechaConfirmacionOPE=date("YmdHis");
		$sql="UPDATE operaciones SET ";
		$sql.=" idConfirmacionOPE='".$session->idusuario."', ";
		$sql.=" fechaConfirmacionOPE='".$this->fechaConfirmacionOPE."' ";
		$sql.=" WHERE idoperacion='";
		$sql.=$this->idoperacion."'";
		return ($db->query($sql))?true:false;
	}

	public function buscarParaFiltroDatosOPEC($datos){
		global $session;
		global $db;
		$datos=$db->escapeString($datos);
		$buscarParaFiltroEntregaOPC=self::sentencia("SELECT OP.* FROM operaciones OP INNER JOIN clientes CL ON (OP.idcliente=CL.idcliente) INNER JOIN vehiculos VH ON(OP.idvehiculo=VH.idvehiculo) INNER JOIN datosFacturacion DF ON (OP.datosfacturacionid=DF.datosfacturacionid) WHERE '$this->idConfirmacionOPE'=(SELECT comercialOperaciones FROM usuarios U WHERE U.idusuario = DF.idUsuarioCreado) AND (OP.numeroOrte LIKE '$datos' OR OP.numeroOrden LIKE '$datos' OR CL.nombre LIKE '$datos' OR CL.apellidosC LIKE '$datos' OR CL.razonSocial LIKE '$datos' OR VH.placa LIKE '$datos' OR VH.chasis LIKE '$datos')");
		return !empty($buscarParaFiltroEntregaOPC) ? $buscarParaFiltroEntregaOPC : false;	
	}

	public function buscarParaFiltroDatosOPECSeguimiento($datos){
		global $db;
		$datos=$db->escapeString($datos);
		$buscarParaFiltroEntregaOPC=self::sentencia("SELECT OP.* FROM operaciones OP INNER JOIN clientes CL ON (OP.idcliente=CL.idcliente) INNER JOIN vehiculos VH ON(OP.idvehiculo=VH.idvehiculo) LEFT JOIN datosFacturacion df on OP.datosfacturacionid=df.datosfacturacionid  WHERE    OP.datosfacturacionid<>'' AND (OP.numeroOrte LIKE '$datos' OR OP.numeroOrden LIKE '$datos' OR CONCAT(CL.apellidosC,' ',CL.nombre) LIKE '$datos'  OR CL.razonSocial LIKE '$datos' OR VH.placa LIKE '$datos' OR VH.chasis LIKE '$datos' OR df.numeroBoleta LIKE '$datos'  OR df.numeroFactura LIKE '$datos')  order by OP.idoperacion desc LIMIT 30");
		return !empty($buscarParaFiltroEntregaOPC) ? $buscarParaFiltroEntregaOPC : false;	
	}

	public function buscarParaFiltroOPECFechaSeguimientoCom($datos,$desde,$hasta){
		global $db;
		$datos=$db->escapeString($datos);
		$buscarParaFilOPEC=self::sentencia("SELECT OP.* FROM operaciones OP INNER JOIN clientes CL ON (OP.idcliente=CL.idcliente) INNER JOIN vehiculos VH ON(OP.idvehiculo=VH.idvehiculo) LEFT JOIN datosFacturacion df on OP.datosfacturacionid=df.datosfacturacionid WHERE  OP.fechaCreacion BETWEEN '$desde' AND '$hasta' AND (OP.numeroOrte LIKE '$datos' OR OP.numeroOrden LIKE '$datos'  OR CONCAT(CL.apellidosC,' ',CL.nombre) LIKE '$datos' OR CL.razonSocial LIKE '$datos' OR VH.placa LIKE '$datos' OR VH.chasis LIKE '$datos' OR df.numeroBoleta LIKE '$datos'  OR df.numeroFactura LIKE '$datos' ) order by OP.idoperacion desc");
		return !empty($buscarParaFilOPEC) ? $buscarParaFilOPEC : false;
	}

	public function buscarParaFiltroOPECFecha($datos,$desde,$hasta){
		global $session;
		global $db;
		$datos=$db->escapeString($datos);
		$buscarParaFilOPEC=self::sentencia("SELECT * FROM operaciones OP INNER JOIN clientes CL ON (OP.idcliente=CL.idcliente) INNER JOIN vehiculos VH ON(OP.idvehiculo=VH.idvehiculo) WHERE idConfirmacionOPE='$this->idConfirmacionOPE' AND fechaConfirmacionOPE BETWEEN '$desde' AND '$hasta' AND (OP.numeroOrte LIKE '$datos' OR OP.numeroOrden LIKE '$datos' OR CL.nombre LIKE '$datos' OR CL.apellidosC LIKE '$datos' OR CL.razonSocial LIKE '$datos' OR VH.placa LIKE '$datos' OR VH.chasis LIKE '$datos')");
		return !empty($buscarParaFilOPEC) ? $buscarParaFilOPEC : false;
	}

	public static function obtenerOperacionesPorVehiculoID($idvehiculo){
		$selq=self::sentencia("SELECT * FROM operaciones WHERE idvehiculo='$idvehiculo' ORDER BY idoperacion DESC  LIMIT 1");
		return !empty($selq)?array_shift($selq):false;
	}
    public static function obtenerOperacionesPorVehiculoIDClienteId($idvehiculo,$idCliente){
        $selq=self::sentencia("SELECT * FROM operaciones WHERE idvehiculo='$idvehiculo' and idcliente='$idCliente' ORDER BY idoperacion DESC  LIMIT 1");
        return !empty($selq)?array_shift($selq):false;
    }

        public static function obtenerFrecuentes($usuario){
              $selq=self::sentencia("SELECT * from frecuentes where estado in ('fijo', 'add') and idUsuarioCargo  is null or idUsuarioCargo ='$usuario'");
              //return !empty($selq)?$selqfalse;
              return !empty($selq)?array_shift($selq):false;
        }




	public static function obtenereOperacionesPorIdFacturacion($datosfacturacionid){
		$selq=self::sentencia("SELECT * FROM operaciones WHERE datosfacturacionid='$datosfacturacionid' ORDER BY idoperacion DESC  LIMIT 1");
		return !empty($selq)?array_shift($selq):false;	
	}

	public static function obtenerConcesionarioInstalacion(){
		$concesionar=self::sentencia("SELECT DISTINCT(nombreConcecionarioInstalicion) FROM operaciones WHERE nombreConcecionarioInstalicion<>''");
		return !empty($concesionar)?$concesionar:false;
	}

	// public static function obtenerVehiculoParaBienvenida(){
	// 	$sqlObtener=self::sentencia("SELECT * FROM vehiculos WHERE estado ='3'");
	// 	return !empty($sqlObtener)?$sqlObtener:false;
	// }

	public static function  vehiculosParaBienvenida(){
		$parabienbenida=self::sentencia("SELECT OP.* FROM operaciones OP INNER JOIN vehiculos VH ON (OP.idvehiculo=VH.idvehiculo AND (VH.estado='3' OR VH.estado='99')) WHERE (trabajo='Instalacion' or trabajo='Reactivacion') ORDER BY DATE_FORMAT(fechaInstalacion, '%Y-%m-%d') ASC limit 20");
		return !empty($parabienbenida)?$parabienbenida:false;
	}

	public static function getDatosInstalacionReact($datos,$posisionDePagina,$registrosPorPagina){
	    $sql = null;
	    if ($datos) {
            $sql = "SELECT DISTINCT OP.trabajo,OP.idcliente,OP.fechaInstalacion,OP.mailWelcome,OP.datosfacturacionid,OP.ordenContrato,OP.estado,VH.chasis, 
              VH.placa,VH.plan,VH.campana,VH.idCargoB,VH.idvehiculo,CL.tipoCliente,CL.apellidosC,CL.nombre,CL.razonSocial
              FROM operaciones OP INNER JOIN vehiculos VH ON (OP.idvehiculo=VH.idvehiculo AND (VH.estado in ('4','99')))
              LEFT JOIN  clientes CL ON (CL.idcliente=OP.idcliente)
              WHERE (trabajo='Instalacion' or trabajo='Reactivacion')
              AND (CONCAT(CL.apellidosC,' ',CL.nombre) like '$datos' OR CL.razonSocial like '$datos' OR VH.placa like '$datos' OR VH.chasis like '$datos' or (CL.dni like '$datos' or CL.ruc like '$datos' ) )
              order by OP.idoperacion desc LIMIT $posisionDePagina, $registrosPorPagina  ";
            $parabienbenida=self::sentencia($sql);
        }

        return !empty($parabienbenida)?$parabienbenida:false;
    }


    public static function countGetDatosInstalacionReact($datos){
            $sql = " select count(*) as cantidad  from (SELECT DISTINCT OP.trabajo,OP.idcliente,OP.fechaInstalacion,OP.mailWelcome,OP.datosfacturacionid,OP.ordenContrato,
              VH.placa,VH.plan,VH.campana,VH.idCargoB,VH.idvehiculo,CL.tipoCliente,CL.apellidosC,CL.nombre,CL.razonSocial
              FROM operaciones OP INNER JOIN vehiculos VH ON (OP.idvehiculo=VH.idvehiculo AND (VH.estado in ('4','99')))
              LEFT JOIN  clientes CL ON (CL.idcliente=OP.idcliente)
              WHERE (trabajo='Instalacion' or trabajo='Reactivacion')
              AND (CONCAT(CL.apellidosC,' ',CL.nombre) like '%$datos%' OR CL.razonSocial like '%$datos%' OR VH.placa like '%$datos%' OR VH.chasis like '%$datos%')
              )dd ";

            $comOpeR = self::sentencia($sql);
        return !empty($comOpeR)? array_shift($comOpeR):false;
    }


    public static function  vehiculosParaBienvenidaPag($datos,$posisionDePagina,$registrosPorPagina,$est,$fInicio,$fFin,$sMW,$sMA){

        $fil =" ";
        if($fInicio!=''){
            $fil.=" AND (DATE_FORMAT(fechaInstalacion, '%Y-%m-%d') >='".$fInicio."' AND DATE_FORMAT(fechaInstalacion, '%Y-%m-%d') <='".$fFin."' ) " ;
        }
        if($sMW!=''){
            $fil.=" AND IFNULL(OP.mailWelcome,0) = ".$sMW."";
        }
        if($sMA != ''){
            /*$fil.=" AND IFNULL((SELECT t.statusAccept FROM Terms t where 1=1 and  t.idOperation=OP.idoperacion and t.orders=OP.ordenContrato  order by id desc limit 1),0) = ".$sMA."";*/
            $fil.=" AND IFNULL((t3.statusAccept),0) = ".$sMA."";
        }

	    $sql="SELECT DISTINCT OP.trabajo,OP.idcliente,OP.fechaInstalacion,OP.fechaActivacion,OP.mailWelcome,OP.datosfacturacionid,OP.ordenContrato,
VH.placa,VH.plan,VH.sendMail,VH.campana,VH.idCargoB,VH.idvehiculo,CL.tipoCliente,CL.apellidosC,CL.nombre,CL.razonSocial, VH.idCargoB,
IFNULL(t3.statusAccept,0) statusAccept, 
IFNULL(t3.dateAccept,'') dateAccept,
IFNULL(t3.cantSend,0) recordmail,
IFNULL(t3.cantSms,0) recordsms
/*IFNULL((SELECT t.statusAccept FROM Terms t where 1=1 and  t.idOperation=OP.idoperacion and t.orders=OP.ordenContrato order by id desc limit 1 ),0) statusAccept ,
IFNULL((SELECT t.dateAccept FROM Terms t where 1=1 and  t.idOperation=OP.idoperacion and t.orders=OP.ordenContrato order by id desc limit 1 ),'') dateAccept,
IFNULL((SELECT t.cantSend FROM Terms t where 1=1 and  t.idOperation=OP.idoperacion and t.orders=OP.ordenContrato order by id desc limit 1 ),0) recordmail,
IFNULL((SELECT t.cantSms FROM Terms t where 1=1 and  t.idOperation=OP.idoperacion and t.orders=OP.ordenContrato order by id desc limit 1 ),0) recordsms*/

FROM operaciones OP
LEFT JOIN (
	SELECT MAX(t2.idOperation) maxidope FROM Terms t2 
	GROUP BY t2.idOperation 
) as MAXTERM on OP.idoperacion = MAXTERM.maxidope
LEFT JOIN Terms t3 on t3.id = MAXTERM.maxidope
INNER JOIN vehiculos VH ON (OP.idvehiculo=VH.idvehiculo AND (VH.estado in ($est)))
LEFT JOIN  clientes CL ON (CL.idcliente=OP.idcliente)
WHERE (trabajo='Instalacion' or trabajo='Reactivacion' or trabajo='Reinstalacion') 
AND (CONCAT(CL.apellidosC,' ',CL.nombre) like '%$datos%' OR CL.razonSocial like '%$datos%' OR VH.placa like '%$datos%' OR VH.chasis like '%$datos%'
OR VH.campana like '%$datos%') $fil
-- ORDER BY OP.fechaInstalacion DESC 
LIMIT $posisionDePagina, $registrosPorPagina";
        //echo $sql;
        $parabienbenida=self::sentencia($sql);
        return !empty($parabienbenida)?$parabienbenida:false;
    }

    public static function  vehiculosParaBienvenidaPagExcel($est,$fInicio,$fFin,$sMW,$sMA){

        $fil ="";
        if($fInicio!=''){
            $fil.=" AND (DATE_FORMAT(fechaInstalacion, '%Y-%m-%d') >='".$fInicio."' AND DATE_FORMAT(fechaInstalacion, '%Y-%m-%d') <='".$fFin."' ) " ;
        }
        if($sMW!=''){
            $fil.=" AND IFNULL(OP.mailWelcome,0) = ".$sMW."";
        }
        if($sMA != ''){
          /*  $fil.=" AND IFNULL((SELECT t.statusAccept FROM Terms t where 1=1 and  t.idOperation=OP.idoperacion and t.orders=OP.ordenContrato  order by id desc limit 1),0) = ".$sMA."";*/
            $fil.=" AND IFNULL((t3.statusAccept),0) = ".$sMA."";
        }

        $sql="SELECT DISTINCT OP.trabajo,OP.idcliente,OP.fechaInstalacion,OP.fechaActivacion,OP.mailWelcome,OP.datosfacturacionid,OP.ordenContrato,
VH.placa,VH.plan,VH.sendMail,VH.campana,VH.idCargoB,VH.idvehiculo,CL.tipoCliente,CL.apellidosC,CL.nombre,CL.razonSocial, VH.idCargoB,VH.chasis,
IFNULL(t3.statusAccept,0) statusAccept, 
IFNULL(t3.dateAccept,'') dateAccept,
IFNULL(t3.cantSend,0) recordmail,
IFNULL(t3.cantSms,0) recordsms
/*IFNULL((SELECT t.statusAccept FROM Terms t where 1=1 and  t.idOperation=OP.idoperacion and t.orders=OP.ordenContrato order by id desc limit 1 ),0) statusAccept ,
IFNULL((SELECT t.dateAccept FROM Terms t where 1=1 and  t.idOperation=OP.idoperacion and t.orders=OP.ordenContrato order by id desc limit 1 ),'') dateAccept,
IFNULL((SELECT t.cantSend FROM Terms t where 1=1 and  t.idOperation=OP.idoperacion and t.orders=OP.ordenContrato order by id desc limit 1 ),0) recordmail,
IFNULL((SELECT t.cantSms FROM Terms t where 1=1 and  t.idOperation=OP.idoperacion and t.orders=OP.ordenContrato order by id desc limit 1 ),0) recordsms*/
FROM operaciones OP  
LEFT JOIN (
	SELECT MAX(t2.idOperation) maxidope FROM Terms t2 
	GROUP BY t2.idOperation 
) as MAXTERM on OP.idoperacion = MAXTERM.maxidope
LEFT JOIN Terms t3 on t3.id = MAXTERM.maxidope
INNER JOIN vehiculos VH ON (OP.idvehiculo=VH.idvehiculo AND (VH.estado in ($est)))
LEFT JOIN  clientes CL ON (CL.idcliente=OP.idcliente)  WHERE (trabajo='Instalacion' or trabajo='Reactivacion' or trabajo='Reinstalacion')  $fil
-- ORDER BY DATE_FORMAT(fechaInstalacion, '%Y-%m-%d') ASC
";
        $parabienbenida=self::sentencia($sql);
        return !empty($parabienbenida)?$parabienbenida:false;
    }

    public static function  seguimientBienvenidaPagExcel($fInicio,$fFin){

        $fil ="";
        if($fInicio!=''){
            $fil.=" AND (DATE_FORMAT(t.dateSend, '%Y-%m-%d') >='".$fInicio."' AND DATE_FORMAT(t.dateSend, '%Y-%m-%d') <='".$fFin."' ) " ;
        }
        $sql="SELECT DISTINCT o.idoperacion,o.trabajo ,CL.tipoCliente,CL.apellidosC,CL.nombre,CL.razonSocial,CASE WHEN v.plan like '%Pro%' THEN 'Otros' 
        WHEN v.campana like '%metros%' THEN 'Otros' 
        ELSE v.campana END campana,
        CASE WHEN v.campana LIKE '%metros%' THEN 'plan kilometros'
        WHEN IFNULL(t.financiadopor,'') LIKE '%CLAN%' THEN 'EL Clan'
        WHEN IFNULL(t.financiadopor,'') LIKE '%MOVILIZA%' THEN 'Moviliza'
        ELSE v.plan END plan,v.placa ,v.chasis,DATE_FORMAT(tp.fecha,'%Y-%m-%d')  fechaInstalacion,
        DATE_FORMAT(t.dateSend,'%d-%m-%Y %H:%m:%s') dateSend,DATE_FORMAT(t.dateAccept,'%d-%m-%Y %H:%m:%s') dateAccept,IFNULL(t.statusAccept,0)statusAccept,IFNULL(t.cantsend,0) recordmail,IFNULL(t.cantSms,0) recordsms
        FROM Terms t
        INNER JOIN operaciones o on o.idoperacion = t.idOperation 
        INNER JOIN  tablaProgramacion tp on tp.idoperacion = o.idoperacion and tp.estado !='321'
        INNER JOIN vehiculos v on v.idvehiculo = o.idvehiculo
        INNER JOIN clientes CL on CL.idcliente = o.idcliente 
        WHERE 1=1 $fil
        ORDER BY DATE_FORMAT(t.dateSend, '%Y-%m-%d') ASC";
        #echo $sql;
        $segparabienbenida=self::sentencia($sql);
        return !empty($segparabienbenida)?$segparabienbenida:false;
    }

    public function obtenerCantidadvehiculosParaBienvenida($datos,$est,$fInicio,$fFin,$sMW,$sMA){
        $fil = "";
	    if($fInicio!=''){
           $fil.=" AND (DATE_FORMAT(fechaInstalacion, '%Y-%m-%d') >='".$fInicio."' AND DATE_FORMAT(fechaInstalacion, '%Y-%m-%d') <='".$fFin."' ) " ;
        }
	    if($sMW!=''){
	        $fil.=" AND IFNULL(OP.mailWelcome,0) = ".$sMW." ";
        }
	    if($sMA != ''){
           /* $fil.=" AND IFNULL((SELECT t.statusAccept FROM Terms t where 1=1 and  t.idOperation=OP.idoperacion and t.orders=OP.ordenContrato  order by id desc limit 1),0) = ".$sMA."";*/
            $fil.=" AND IFNULL((t3.statusAccept),0) = ".$sMA."";
        }

	    $sql="SELECT COUNT(*) as cantidad
FROM (
SELECT DISTINCT OP.trabajo,OP.idcliente,OP.fechaInstalacion,OP.fechaActivacion,OP.mailWelcome,OP.datosfacturacionid,
VH.placa,VH.plan,VH.sendMail,VH.campana,VH.idCargoB,VH.idvehiculo,CL.tipoCliente,CL.apellidosC,CL.nombre,CL.razonSocial
FROM operaciones OP
INNER JOIN vehiculos VH ON (OP.idvehiculo=VH.idvehiculo AND (VH.estado in ($est)))
-- LEFT JOIN Terms t on t.idOperation=OP.idoperacion and t.orders=OP.ordenContrato
LEFT JOIN  clientes CL ON (CL.idcliente=OP.idcliente)
WHERE (trabajo='Instalacion' or trabajo='Reactivacion' or trabajo='Reinstalacion') 
AND (CONCAT(CL.apellidosC,' ',CL.nombre) like '%$datos%' OR CL.razonSocial like '%$datos%' OR VH.placa like '%$datos%' OR VH.chasis like '%$datos%'
OR VH.campana like '%$datos%') $fil
-- ORDER BY DATE_FORMAT(fechaInstalacion, '%Y-%m-%d') ASC
) dat ";
	    //echo $sql;
        $comOpeR=self::sentencia($sql);
        return !empty($comOpeR)? array_shift($comOpeR):false;
    }


	public function obtenerTecnicoAsignadoEnFecha($desde,$hasta){
		$obteTecnEnFecha=self::sentencia("SELECT * FROM operaciones WHERE idTecnicoAsignado='$this->idTecnicoAsignado' AND (DATE_FORMAT(fechaInstalacion,'%Y/%m/%d %H:%i') BETWEEN '$desde' AND '$hasta' OR DATE_ADD(fechaInstalacion,INTERVAL -2 HOUR) BETWEEN '$desde' AND '$hasta'  ) ");
		return !empty($obteTecnEnFecha)?true:false;
	}

	public function pendientesComCantidadTotal(){
		$historialSacCantidadTotal=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE (estado='0' or estado='321')  AND ('34'=(SELECT estadoFacturacion FROM datosFacturacion WHERE datosFacturacion.datosfacturacionid=operaciones.datosfacturacionid)  OR  '77'=(SELECT estadoFacturacion FROM datosFacturacion WHERE datosFacturacion.datosfacturacionid=operaciones.datosfacturacionid)) ORDER BY idoperacion DESC LIMIT 1");
		return !empty($historialSacCantidadTotal)? array_shift($historialSacCantidadTotal) :false;
	}
	public function obtenerpendientesCom($posisionDePagina,$registrosPorPagina){
		$dtp=self::sentencia("SELECT * FROM operaciones  WHERE  (estado='0' or estado='321') AND ('34'=(SELECT estadoFacturacion FROM datosFacturacion WHERE datosFacturacion.datosfacturacionid=operaciones.datosfacturacionid)  OR  '77'=(SELECT estadoFacturacion FROM datosFacturacion WHERE datosFacturacion.datosfacturacionid=operaciones.datosfacturacionid)) LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($dtp)? $dtp:false;		
	}

	public function obtenerpendientesCoReporEx(){
		$dtp=self::sentencia("SELECT * FROM operaciones  WHERE  (estado='0' or estado='321')  AND ('34'=(SELECT estadoFacturacion FROM datosFacturacion WHERE datosFacturacion.datosfacturacionid=operaciones.datosfacturacionid)  OR  '77'=(SELECT estadoFacturacion FROM datosFacturacion WHERE datosFacturacion.datosfacturacionid=operaciones.datosfacturacionid))  ");
		return !empty($dtp)? $dtp:false;		
	}
	
	public function obtenerpendientesComFiltro($datos){
		$dtp=self::sentencia("SELECT * FROM operaciones INNER JOIN clientes ON (operaciones.idcliente=clientes.idcliente) INNER JOIN vehiculos ON (vehiculos.idvehiculo=operaciones.idvehiculo)  WHERE   (operaciones.estado='0' OR operaciones.estado='321') AND (clientes.apellidosC LIKE '$datos' OR clientes.nombre LIKE '$datos' OR clientes.razonSocial LIKE '$datos' OR vehiculos.placa LIKE '$datos' OR vehiculos.chasis LIKE '$datos') AND ('34'=(SELECT estadoFacturacion FROM datosFacturacion WHERE datosFacturacion.datosfacturacionid=operaciones.datosfacturacionid)  OR  '77'=(SELECT estadoFacturacion FROM datosFacturacion WHERE datosFacturacion.datosfacturacionid=operaciones.datosfacturacionid)) LIMIT 10 ");
		return !empty($dtp)? $dtp:false;		
	}

	public function marcarListoParaSac(){
		global $db;
		global $session;
		$uadd=Usuario::obtenerUsuario($session->idusuario);
		$this->fechaInstalacion=date("Y-m-d H:i");
		$sql= " UPDATE operaciones SET estado ='14',";
		$sql.=" fechaInstalacion ='";
		$sql.=$this->fechaInstalacion."' , ";
		$sql.=" observacion='Agregado directamente por :".$uadd->usuarioNombre." ".$uadd->usuarioApellidoPaterno."' " ;
		$sql.="WHERE idoperacion='";
		$sql.=$this->idoperacion."'";
		return ($db->query($sql)) ? true  :false;
	}
  	public  function obtenerInstalacionesDomProgramadas($hoy1,$hoy2){	
		$sql=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE lugarInstalacion = 'Domicilio Cliente' AND  trabajo = 'Instalacion' AND fechaInstalacion BETWEEN '$hoy1' AND '$hoy2' AND numeroOrte <> '' AND estado<>'321'");
		return !empty($sql)?array_shift($sql):false;
	} 

	public  function obtenerInstalacionesDiariariasConcecionarioProgramadas($hoy1,$hoy2){
		$sql=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE lugarInstalacion = 'Concesionario' AND  trabajo = 'Instalacion' AND fechaInstalacion BETWEEN '$hoy1' AND '$hoy2' AND numeroOrte <> '' AND estado<>'321'");
		return !empty($sql)?array_shift($sql):false;
	}

	public  function obtenerInstalacionesDiariariasTallerProgramadas($hoy1,$hoy2){
		$sql=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE lugarInstalacion = 'Taller' AND  trabajo = 'Instalacion' AND fechaInstalacion BETWEEN '$hoy1' AND '$hoy2' AND numeroOrte <> '' AND estado<>'321'");
		return !empty($sql)?array_shift($sql):false;
	}
	public  function obtenerInstalacionesDiariariasTotalProgramadas($hoy1,$hoy2){		
		$sql=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE trabajo = 'Instalacion' AND fechaInstalacion BETWEEN '$hoy1' AND '$hoy2' AND numeroOrte <> '' AND estado<>'321'");
		return !empty($sql)?array_shift($sql):false;
	}
    public function marcarComoAnulado($ultimoid,$idopera){
        global $db;
        $datosOld=new SeguimientoConsulta();
        $dOld=$datosOld->consultaSeg("operaciones",$this->idoperacion,"idoperacion");
        $dOldd=$dOld;
        $dtaActo="UPDATE operaciones SET estado='18', observacion='$this->observacion' WHERE idoperacion='$this->idoperacion' ";

        if($db->query($dtaActo)){
            new SeguimientoTablas("operaciones"," Modifico como Anulado",$this->idoperacion,$this,"idoperacion",$dOldd,$ultimoid,$idopera);
            $this->marcarComoAnuladoTbPrograma();
            return true ;
        }else{
            return false;
        }
//        return ($db->query($dtaActo))?true:false;
    }

    public function marcarComoAnuladoTbPrograma(){
        global $db;
        $dtaActo="UPDATE tablaProgramacion SET estado='321', observacion='se anulo operacion por ende liberar espacio' WHERE idoperacion='$this->idoperacion' ";

        if($db->query($dtaActo)){
            return true ;
        }else{
            return false;
        }
//        return ($db->query($dtaActo))?true:false;
    }

    public function modificarDesdeAdmi($ultimoid,$idopera){
        global $db;
        $datosOld=new SeguimientoConsulta();
        $dold=$datosOld->consultaSeg("operaciones",$this->idoperacion,"idoperacion");
        $doldd=$dold;
        $sqlActop="UPDATE operaciones SET numeroOrden='$this->numeroOrden' WHERE idoperacion='$this->idoperacion'";
        if($db->query($sqlActop)){
            new SeguimientoTablas("operaciones"," Modifico numero Orden",$this->idoperacion,$this,"idoperacion",$doldd,$ultimoid,$idopera);
            return true ;
        }else{
            return false;
        }
        //return ($db->query($sqlActop))?true:false;
    }

    public function obtenerActividadesPorEstadoCantidad(){
		$sql=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE estado='$this->estado' LIMIT 1");
		return !empty($sql)?array_shift($sql):false;
    }
	public function obtenerActividadesPorEstado($posisionDePagina,$registrosPorPagina){
		$dtp=self::sentencia("SELECT * FROM operaciones  WHERE  estado='$this->estado' ORDER BY idoperacion DESC  LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($dtp)? $dtp:false;		
	}

	public function guarDarIncidencia(){
		global $session;
		global $db;
		$this->fechaCreacion=date("YmdHis");
		$sql="INSERT INTO incidencias (idoperacion,idusuario,fechaInstalacion,estado,observacion,fechaRegistro) ";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString($this->idoperacion)."' , '";
		$sql.= $db->escapeString($session->idusuario)."' , '";
		$sql.= $db->escapeString($this->fechaInstalacion)."' , '";
		$sql.= $db->escapeString($this->estado)."' , '";
		$sql.= $db->escapeString($this->observacion)."' , '";
		$sql.= $db->escapeString($this->fechaCreacion)."')";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}

    public function marcarComoReprogramar(){
		global $db;
        $sql="UPDATE operaciones SET estado='321', observacion = '$this->observacion' WHERE idoperacion='$this->idoperacion'";
        $this->guarDarIncidencia();
        return ($db->query($sql))?true:false;
    }
    
    public function buscarEnAdminOperacionConOrte36($datos){
		$badoco=self::sentencia("SELECT operaciones.* FROM operaciones LEFT JOIN clientes ON (operaciones.idcliente=clientes.idcliente) LEFT JOIN vehiculos ON (vehiculos.idvehiculo=operaciones.idvehiculo) LEFT JOIN chequeos ON (chequeos.idchequeo=operaciones.idChequeo) WHERE operaciones.estado='36' AND (operaciones.numeroOrte LIKE '$datos' OR clientes.apellidosC LIKE '$datos' OR clientes.nombre LIKE '$datos' OR clientes.razonSocial LIKE '$datos' OR vehiculos.placa LIKE '$datos' OR vehiculos.chasis LIKE '$datos' OR chequeos.nombres LIKE '$datos' OR chequeos.placa LIKE '$datos' OR  chequeos.chasis LIKE '$datos') LIMIT 10 ");
		return !empty($badoco)? $badoco:false;		
    }

    public function buscarEnoperacionesYchequeosSinImportarElestado($datos){
		$badoco=self::sentencia("SELECT operaciones.* FROM operaciones LEFT JOIN clientes ON (operaciones.idcliente=clientes.idcliente) LEFT JOIN vehiculos ON (vehiculos.idvehiculo=operaciones.idvehiculo) LEFT JOIN chequeos ON (chequeos.idchequeo=operaciones.idChequeo) WHERE  (operaciones.numeroOrte LIKE '$datos' OR clientes.apellidosC LIKE '$datos' OR clientes.nombre LIKE '$datos' OR clientes.razonSocial LIKE '$datos' OR vehiculos.placa LIKE '$datos' OR vehiculos.chasis LIKE '$datos' OR chequeos.nombres LIKE '$datos' OR chequeos.placa LIKE '$datos' OR  chequeos.chasis LIKE '$datos') LIMIT 10 ");
		return !empty($badoco)? $badoco:false;		
    }
    ////ESTADOS FALTA //
    public function obtenerInstalacionesEnfechaTaller(){
		$cantiIns=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE fechaInstalacion LIKE '".$this->fechaInstalacion."%' AND lugarInstalacion='Taller' AND trabajo='Instalacion'  LIMIT 1 ");
        $obj=array_shift($cantiIns);;
        return $obj->cantidad;
    }
    public function obtenerInstalacionesEnfechaDOm(){
		$cantiIns=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE fechaInstalacion LIKE '".$this->fechaInstalacion."%' AND lugarInstalacion='Domicilio Cliente' AND trabajo='Instalacion'  LIMIT 1 ");
        $obj=array_shift($cantiIns);;
        return $obj->cantidad;
    }
    public function obtenerInstalacionesEnfechaCon(){
		$cantiIns=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE fechaInstalacion LIKE '".$this->fechaInstalacion."%' AND lugarInstalacion='Concesionario' AND trabajo='Instalacion' LIMIT 1 ");
        $obj=array_shift($cantiIns);;
        return $obj->cantidad;
    }
    public function obtenerCitasEnfechaCon(){
		$cantiIns=self::sentencia("SELECT COUNT(*) as cantidad FROM citasTaller WHERE fecha LIKE '".$this->fechaInstalacion."%'  LIMIT 1 ");
        $obj=array_shift($cantiIns);;
        return $obj->cantidad;
    }
    public function obtenerChequeosEnfechaTaller(){
		$cantiIns=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE fechaInstalacion LIKE '".$this->fechaInstalacion."%' AND lugarInstalacion='Taller' AND trabajo like '%chequeo%'  LIMIT 1 ");
        $obj=array_shift($cantiIns);;
        return $obj->cantidad;
    }
    public function obtenerChequeosEnfechaDOm(){
		$cantiIns=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE fechaInstalacion LIKE '".$this->fechaInstalacion."%' AND lugarInstalacion='Domicilio Cliente' AND  trabajo like '%chequeo%' LIMIT 1 ");
        $obj=array_shift($cantiIns);;
        return $obj->cantidad;
    }
    public function obtenerChequeosEnfechaCon(){
		$cantiIns=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE fechaInstalacion LIKE '".$this->fechaInstalacion."%' AND lugarInstalacion='Concesionario' AND  trabajo like '%chequeo%' LIMIT 1 ");
        $obj=array_shift($cantiIns);;
        return $obj->cantidad;
    }
    public function obtenerDesinstalEnfechaTaller(){
		$cantiIns=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE fechaInstalacion LIKE '".$this->fechaInstalacion."%' AND lugarInstalacion='Taller' AND trabajo like '%desins%'  LIMIT 1 ");
        $obj=array_shift($cantiIns);;
        return $obj->cantidad;
    }
    public function obtenerDesinstalaEnfechaDOm(){
		$cantiIns=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE fechaInstalacion LIKE '".$this->fechaInstalacion."%' AND lugarInstalacion='Domicilio Cliente' AND  trabajo like '%desins%' LIMIT 1 ");
        $obj=array_shift($cantiIns);;
        return $obj->cantidad;
    }
    public function obtenerDesinstaEnfechaCon(){
		$cantiIns=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE fechaInstalacion LIKE '".$this->fechaInstalacion."%' AND lugarInstalacion='Concesionario' AND  trabajo like '%desins%' LIMIT 1 ");
        $obj=array_shift($cantiIns);;
        return $obj->cantidad;
    }
    public function obtenerTotalEnfechaTaller(){
		$cantiIns=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE fechaInstalacion LIKE '".$this->fechaInstalacion."%' AND lugarInstalacion='Taller' LIMIT 1 ");
        $obj=array_shift($cantiIns);;
        return $obj->cantidad;
    }
    public function obtenerTotalEnfechaDOm(){
		$cantiIns=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE fechaInstalacion LIKE '".$this->fechaInstalacion."%' AND lugarInstalacion='Domicilio Cliente' LIMIT 1 ");
        $obj=array_shift($cantiIns);;
        return $obj->cantidad;
    }
    public function obtenerTotalEnfechaCon(){
		$cantiIns=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE fechaInstalacion LIKE '".$this->fechaInstalacion."%' AND lugarInstalacion='Concesionario' LIMIT 1 ");
        $obj=array_shift($cantiIns);;
        return $obj->cantidad;
    }
    public function obtenerTotalHoy(){
        $hoy=date('Y-m-d');
        $canSqlHoy=self::sentencia("SELECT COUNT(*) AS cantidad FROM operaciones WHERE (estado='15' or estado ='99' or estado ='36') AND  DATE_FORMAT(fechaInstalacion,'%Y-%m-%d ') LIKE '%$hoy%'  ");
        $cantidadOb=array_shift($canSqlHoy);
        return $cantidadOb->cantidad;
    }
    public function obtenerTotalHoyDATA($posisionDePagina,$registrosPorPagina){
        $hoy=date('Y-m-d');
        $canSqlHoy=self::sentencia("SELECT * FROM operaciones WHERE (estado='15' or estado ='99' or estado ='36') AND  DATE_FORMAT(fechaInstalacion,'%Y-%m-%d ') LIKE '%$hoy%' ORDER BY fechaInstalacion DESC LIMIT $posisionDePagina, $registrosPorPagina  ");
        return !empty($canSqlHoy)?$canSqlHoy:false;
    }
    public function obtenerTotalPasado(){
        $hoy=date('Ymd');
        $canSqlHoy=self::sentencia("SELECT COUNT(*) AS cantidad FROM operaciones WHERE (estado='15' or estado ='99' or estado ='36') AND  DATE_FORMAT(fechaInstalacion,'%Y%m%d') < '$hoy' ");
        $cantidadOb=array_shift($canSqlHoy);
        return $cantidadOb->cantidad;
    }
    public function obtenerTotalPasadoDATA($posisionDePagina,$registrosPorPagina){
        $hoy=date('Ymd');
        $canSqlHoy=self::sentencia("SELECT * FROM operaciones WHERE (estado='15' or estado ='99' or estado ='36') AND  DATE_FORMAT(fechaInstalacion,'%Y%m%d') < '$hoy' ORDER BY fechaInstalacion DESC LIMIT $posisionDePagina, $registrosPorPagina");
        return !empty($canSqlHoy)?$canSqlHoy:false;
    }
    public function obtenerTotalFuturo(){
        $hoy=date('Ymd');
        $canSqlHoy=self::sentencia("SELECT COUNT(*) AS cantidad FROM operaciones WHERE (estado='15' or estado ='99' or estado ='36') AND DATE_FORMAT(fechaInstalacion,'%Y%m%d') > '$hoy' ");
        $cantidadOb=array_shift($canSqlHoy);
        return $cantidadOb->cantidad;
    }
    public function obtenerTotalFuturoDATA($posisionDePagina,$registrosPorPagina){
        $hoy=date('Ymd');
        $canSqlHoy=self::sentencia("SELECT * FROM operaciones WHERE (estado='15' or estado ='99' or estado ='36') AND DATE_FORMAT(fechaInstalacion,'%Y%m%d') > '$hoy' ORDER BY fechaInstalacion ASC LIMIT $posisionDePagina, $registrosPorPagina");
        return !empty($canSqlHoy)?$canSqlHoy:false;
    }
    public function obtenerTotalPENDIETEN(){
        $hoy=date('Ymd');
        $canSqlHoy=self::sentencia("SELECT COUNT(*) AS cantidad FROM operaciones WHERE estado ='99' ");
        $cantidadOb=array_shift($canSqlHoy);
        return $cantidadOb->cantidad;
    }
    public function obtenerTotalPENDIETENDATA($posisionDePagina,$registrosPorPagina){
        $hoy=date('Ymd');
        $canSqlHoy=self::sentencia("SELECT * FROM operaciones WHERE  estado ='99' ORDER BY fechaInstalacion ASC LIMIT $posisionDePagina, $registrosPorPagina");
        return !empty($canSqlHoy)?$canSqlHoy:false;
    }
    public function obtenerTotalLISTO(){
        $hoy=date('Ymd');
        $canSqlHoy=self::sentencia("SELECT COUNT(*) AS cantidad FROM operaciones WHERE estado ='36' ");
        $cantidadOb=array_shift($canSqlHoy);
        return $cantidadOb->cantidad;
    }
    public function obtenerTotalLISTONDATA($posisionDePagina,$registrosPorPagina){
        $hoy=date('Ymd');
        $canSqlHoy=self::sentencia("SELECT * FROM operaciones WHERE  estado ='36' ORDER BY fechaInstalacion ASC LIMIT $posisionDePagina, $registrosPorPagina");
        return !empty($canSqlHoy)?$canSqlHoy:false;
    }
    public function cambiarEstadoOperacion(){
        global $db;
        $sql="UPDATE operaciones SET estado='";
        $sql.=$db->escapeString($this->estado)."'";
        $sql.=" WHERE idoperacion='";
        $sql.=$db->escapeString($this->idoperacion)."'";
        return ($db->query($sql))?true:false;
    }
    public function cambiarEstadoVehclo(){
        global $db;
        $sql="UPDATE vehiculos SET estado=3 WHERE idvehiculo='".$this->idvehiculo."'";
        return ($db->query($sql))?true:false;
    }
    public function cambiarFecha($fechaOld,$fechaN){
        global $db;
        $fechaOld=$db->escapeString($fechaOld);
        $fechaN=$db->escapeString($fechaN);
        $cFsql="UPDATE operaciones SET fechaInstalacion = REPLACE(fechaInstalacion,'$fechaOld','$fechaN') WHERE idoperacion = '$this->idoperacion' ";
        return ($db->query($cFsql))?true:false;
    }
    /////////REPORTE OPERACIOENS ///
    //
    public function obtenerOperacionesTotalLimitesDes($fecha){
        $oOT=self::sentencia("SELECT COUNT(idoperacion) as cantidad FROM operaciones  WHERE DATE_FORMAT(fechaInstalacion,'%Y%m%d') LIKE '$fecha%' AND trabajo='Desinstalacion'  LIMIT 1");
        $data=array_shift($oOT);
        if ($data->cantidad>0) {
            return $data->cantidad;
        }else{
            return false;
        }
    }

    public function obtenerOperacionesTotalLimitesOTRO($fecha){
        $oOT=self::sentencia("SELECT COUNT(idoperacion) as cantidad FROM operaciones  WHERE DATE_FORMAT(fechaInstalacion,'%Y%m%d') LIKE '$fecha%' AND (trabajo<>'Desinstalacion' AND trabajo<>'Instalacion')  LIMIT 1");
        $data=array_shift($oOT);
        if ($data->cantidad>0) {
            return $data->cantidad;
        }else{
            return false;
        }
    }

    public function obtenerOperacionesTotal($desde,$hasta){
        $oOT=self::sentencia("SELECT COUNT(idoperacion) as cantidad FROM operaciones  WHERE DATE_FORMAT(fechaInstalacion,'%Y%m%d') BETWEEN '$desde' AND '$hasta' AND numeroOrte<>'' AND estado <>'321' LIMIT 1");
        $data=array_shift($oOT);
        if ($data->cantidad>0) {
            return $data->cantidad;
        }else{
            return false;
        }
    }
    public function obtenerActividadesProgramadas($desde,$hasta){
        $oOT=self::sentencia("SELECT trabajo FROM operaciones  WHERE DATE_FORMAT(fechaInstalacion,'%Y%m%d') BETWEEN '$desde' AND '$hasta' and numeroOrte<>'' AND estado <>'321' GROUP BY trabajo");
        return ($oOT)?$oOT:false;
    }
    public function obtenerTrabajoProgramado($desde,$hasta){
        $oOT=self::sentencia("SELECT COUNT(idoperacion) as cantidad FROM operaciones  WHERE DATE_FORMAT(fechaInstalacion,'%Y%m%d') BETWEEN '$desde' AND '$hasta' AND numeroOrte<>'' AND estado <>'321' and trabajo='$this->trabajo' LIMIT 1");
        $data=array_shift($oOT);
        if ($data->cantidad>0) {
            return $data->cantidad;
        }else{
            return false;
        }
    }
    public function obtenerLugaresDetrabajo($desde,$hasta){
        $oOT=self::sentencia("SELECT lugarInstalacion FROM operaciones WHERE DATE_FORMAT(fechaInstalacion,'%Y%m%d') BETWEEN '$desde' AND '$hasta' and numeroOrte<>'' AND estado <>'321' GROUP BY lugarInstalacion");
        return ($oOT)?$oOT:false;
    }

    public function obtenerTrabajoProgramadoLugar($desde,$hasta){
        $oOT=self::sentencia("SELECT COUNT(idoperacion) as cantidad FROM operaciones  WHERE DATE_FORMAT(fechaInstalacion,'%Y%m%d') BETWEEN '$desde' AND '$hasta' AND numeroOrte<>'' AND estado <>'321' and lugarInstalacion='$this->lugarInstalacion' LIMIT 1");
        $data=array_shift($oOT);
        if ($data->cantidad>0) {
            return $data->cantidad;
        }else{
            return false;
        }
    }
    public function obtenerCuantosPorCita($desde,$hasta){
        $oOT=self::sentencia("SELECT COUNT(idoperacion) as cantidad FROM operaciones OP  WHERE DATE_FORMAT(fechaInstalacion,'%Y%m%d') BETWEEN '$desde' AND '$hasta' AND numeroOrte<>'' AND estado <>'321' and EXISTS(SELECT idoperacion FROM citasTaller c WHERE c.idoperacion=OP.idoperacion)  LIMIT 1");
        $data=array_shift($oOT);
        return $data->cantidad;
    }
    public function obtenerReprogramadosOperaciones($desde,$hasta){
        $oOT=self::sentencia("SELECT op.*, distrito as distritoi,tb.observacion FROM incidencias tb LEFT JOIN  operaciones op ON(tb.idoperacion=op.idoperacion) LEFT JOIN distritos dis ON (op.distritoi = dis.iddistrito) WHERE DATE_FORMAT(tb.fechaRegistro,'%Y%m%d') BETWEEN '$desde' AND '$hasta' AND idcita='0' ");
        return ($oOT)?$oOT:false;
    }

    public function obtenerReprogramadosCitas($desde,$hasta){
        $oOT=self::sentencia("SELECT c.idOpe as idConfirmacionOPE,c.idcreador as idCreador,tb.observacion FROM incidencias tb LEFT JOIN  citasTaller c ON(c.idcita=tb.idcita)  WHERE DATE_FORMAT(tb.fechaRegistro,'%Y%m%d') BETWEEN '$desde' AND '$hasta' AND tb.idoperacion='0' ");
        return ($oOT)?$oOT:false;
    }

    public function obtenerInstalacionesN($desde,$hasta){
        $oOT=self::sentencia("SELECT COUNT(idoperacion) as cantidad FROM operaciones OP  WHERE DATE_FORMAT(fechaInstalacion,'%Y%m%d') BETWEEN '$desde' AND '$hasta' AND numeroOrte<>'' AND estado <>'321' AND trabajo='Instalacion'  LIMIT 1");
        $data=array_shift($oOT);
        return $data->cantidad;
    }

    public function obtenerLimiteDefecto(){
        return self::sentencia("SELECT cantidad,actividad as trabajo FROM limiteDia WHERE fechaInstalacion='DEFECTO' ");
    }
    
    public function actualizarLimiteDefecto(){
        global $db;
        $db->query("update limiteDia SET  cantidad='$this->cantidad' WHERE fechaInstalacion='DEFECTO' and actividad='$this->trabajo' ");
    }

    public function actualizarLimiteFecha($fecha){
        global $db;
        $fecha=trim(soloNumero($fecha));
        $obTlQ=self::sentencia("SELECT cantidad FROM limiteDia WHERE fechaInstalacion='$fecha' and actividad='$this->trabajo' LIMIT 1");
        $data=array_shift($obTlQ);
        if (!$data) {
            $res=$db->query("insert into limiteDia (cantidad, fechaInstalacion, actividad ) values ('$this->cantidad','$fecha','$this->trabajo')");
        }else{
            $res=$db->query("update limiteDia SET  cantidad='$this->cantidad'   WHERE fechaInstalacion='$fecha' and actividad='$this->trabajo' ");
        }

    }

    public static function obtenerLimite($fecha,$trabajo){
        $fecha=trim(soloNumero($fecha));
        $obTlQ=self::sentencia("SELECT cantidad FROM limiteDia WHERE fechaInstalacion='$fecha' and actividad='$trabajo' LIMIT 1");
        $obTlQLEFECTO=self::sentencia("SELECT cantidad FROM limiteDia WHERE fechaInstalacion='DEFECTO' and actividad='$trabajo'");
        $data=array_shift($obTlQ);
        $dataDefecto=array_shift($obTlQLEFECTO);
        if ($data) {
            return $data->cantidad;
        }else{
            return $dataDefecto->cantidad;
        }
    }
    public function obtenerSintecnicosTabla(){
        $oST=self::sentencia("SELECT idoperacion,idtecnico as idTecnicoAsignado FROM tablaProgramacion WHERE idtecnico=0");
        return ($oST) ?$oST:false;

    }
    public function actualizarsintecnicosTalba(){
        global $db;
        $sqast="UPDATE tablaProgramacion SET idtecnico='$this->idTecnicoAsignado' WHERE idoperacion='$this->idoperacion'";
        return ($db->query($sqast))?true:false;
    }
    public static function obtenerObservacionesHistorialAndCOnact($idoperacion){
        $oOHAC=self::sentencia("SELECT observacion,datosfacturacionid FROM operacionesLog WHERE idoperacion='$idoperacion' AND observacion<>'' GROUP BY observacion");
        if ($oOHAC) {
            $obsC='';
            foreach($oOHAC as $var){
                $datosfacturacionid=$var->datosfacturacionid;
                $obsC.=" ".$var->observacion." ";
                $dfq=self::sentencia("SELECT DISTINCT(observaciones) as observacion FROM datosFacturacionLog WHERE datosfacturacionid='".$datosfacturacionid."' AND observaciones<>''");
                if ($dfq) {
                    foreach($dfq as $var1){
                    $obsC.=" ".$var1->observacion." ";
                    }
                }
            }
            return $obsC;
        }else{
            return false;
        }
    }

    public function obtenerCitasPordia($fecha){
        $oCPR=self::sentencia("SELECT COUNT(*) as cantidad FROM citasTaller WHERE idestado='14' AND DATE_FORMAT(fecha,'%Y%m%d')='".$fecha."' LIMIT 1");
        $d=array_shift($oCPR);
        return $d->cantidad;
    }

    public function actualizarEstadoYFechaI($estadoVehiculo,$idvehiculo){
        global $db;
        $sqast="UPDATE operaciones SET estado='$this->estado', fechaInstalacion='$this->fechaInstalacion' , ordenContrato='$this->ordenContrato' WHERE idoperacion='$this->idoperacion'";
	    $actualizarEstadoQ="UPDATE vehiculos SET estado = '$estadoVehiculo' WHERE idvehiculo='$idvehiculo'";
        $db->query($actualizarEstadoQ);
        return ($db->query($sqast))?true:false;
    }


    public function  ObtenertbPrograma($idOpera){
        $objTP=self::sentencia("SELECT DISTINCT idtablaProgramacion, fecha, hora FROM tablaProgramacion WHERE  idoperacion='$idOpera' and estado<>'321' limit 1");
        return !empty($objTP)?array_shift($objTP):false;
    }



    public function  verficarOrden($loc,$orden){
	    $sql = " SELECT * FROM sid_contract sc where idlocord ='$loc' and idorder='$orden' ";
        $objTP=self::sentenciaOnix($sql);

        return !empty($objTP)?array_shift($objTP):false;
    }


    public static function  dataWelcomeMail ($fInicio,$fFin,$campana,$opc){

        $fil ="";
        if($fInicio!=''){
            $fil.=" AND DATE_FORMAT(t.dateSend,'%Y-%m-%d')>='".$fInicio."' AND DATE_FORMAT(t.dateSend, '%Y-%m-%d') <='".$fFin."'  " ;
        }
        if($campana!=""){
            $fil.= " AND CASE WHEN v.plan like '%Pro%' THEN 'Otros' 
        WHEN v.campana like '%metros%' THEN 'Otros' 
        ELSE v.campana END like '%$campana%' ";
        }

        $sql="  SELECT dat.campana,dat.plan,dat.enviados,dat.aceptados,ROUND(((dat.aceptados/dat.enviados) *100),2) porc 
 FROM
 (
   SELECT 
   CASE WHEN v.plan like '%Pro%' THEN 'Otros' 
        WHEN v.campana like '%metros%' THEN 'Otros' 
        ELSE v.campana END campana,
   CASE WHEN v.campana LIKE '%metros%' THEN 'plan kilometros'
        WHEN IFNULL(t.financiadopor,'') LIKE '%CLAN%' THEN 'EL Clan'
        WHEN IFNULL(t.financiadopor,'') LIKE '%MOVILIZA%' THEN 'Moviliza'
        ELSE v.plan END plan,
   count(*) enviados,sum(CASE WHEN IFNULL(statusAccept,0) = 1 THEN 1 ELSE 0 END ) aceptados
   FROM Terms t
   INNER JOIN operaciones o on o.idoperacion = t.idOperation 
   INNER JOIN vehiculos v on v.idvehiculo = o.idvehiculo
   WHERE 1=1  $fil
    and CONCAT(t.idlocation ,t.idcustomer) not in (select CONCAT(nt.idlocation ,nt.idcustomer) from no_atenders_general_automatico nt)
   GROUP BY   CASE WHEN v.plan like '%Pro%' THEN 'Otros' 
        WHEN v.campana like '%metros%' THEN 'Otros' 
        ELSE v.campana END,
   CASE WHEN v.campana LIKE '%metros%' THEN 'plan kilometros'
        WHEN IFNULL(t.financiadopor,'') LIKE '%CLAN%' THEN 'EL Clan'
        WHEN IFNULL(t.financiadopor,'') LIKE '%MOVILIZA%' THEN 'Moviliza'
        ELSE v.plan END
)dat  order by dat.campana,plan asc ";

        if($opc == 2){
            $sql2 = "SELECT SUM(dat2.enviados) enviados,sum(dat2.aceptados)aceptados,ROUND(((sum(dat2.aceptados)/sum(dat2.enviados)) *100),2) porc 
                    FROM ( $sql )dat2";
            $Databienbenida2=self::sentencia($sql2);
            return !empty($Databienbenida2)?array_shift($Databienbenida2):false;
        }else{

            $Databienbenida=self::sentencia($sql);
            return !empty($Databienbenida)?$Databienbenida:false;
        }
    }

	// Calculos totales por campañas 
	public static function  dataWelcomeMailTotales ($fInicio,$fFin,$campana,$opc){

        $fil ="";
        if($fInicio!=''){
            $fil.=" AND DATE_FORMAT(t.dateSend,'%Y-%m-%d')>='".$fInicio."' AND DATE_FORMAT(t.dateSend, '%Y-%m-%d') <='".$fFin."'  " ;
        }
        if($campana!=""){
            $fil.= " AND CASE WHEN v.plan like '%Pro%' THEN 'Otros' 
        WHEN v.campana like '%metros%' THEN 'Otros' 
        ELSE v.campana END like '%$campana%' ";
        }

        $sql="  SELECT 
			dat.campana,
			dat.plan,
			@enviados:=dat.enviados AS enviados,
			@aceptados:=dat.aceptados AS aceptados,
			ROUND(@enviados-aceptados, 0) AS pendientes,
			@porc_aceptados:=ROUND(((dat.aceptados/dat.enviados) *100),2) porc_aceptados,
			ROUND((100-@porc_aceptados),2) AS porc_pendientes
		FROM
		(
		SELECT 
		CASE WHEN v.plan like '%Pro%' THEN 'Otros' 
				WHEN v.campana like '%metros%' THEN 'Otros' 
				ELSE v.campana END campana,
		CASE WHEN v.campana LIKE '%metros%' THEN 'plan kilometros'
				WHEN IFNULL(t.financiadopor,'') LIKE '%CLAN%' THEN 'EL Clan'
				WHEN IFNULL(t.financiadopor,'') LIKE '%MOVILIZA%' THEN 'Moviliza'
				ELSE v.plan END plan,
		count(*) enviados,sum(CASE WHEN IFNULL(statusAccept,0) = 1 THEN 1 ELSE 0 END ) aceptados
		FROM Terms t
		INNER JOIN operaciones o on o.idoperacion = t.idOperation 
		INNER JOIN vehiculos v on v.idvehiculo = o.idvehiculo
		WHERE 1=1  $fil
			and CONCAT(t.idlocation ,t.idcustomer) not in (select CONCAT(nt.idlocation ,nt.idcustomer) from no_atenders_general_automatico nt)
		GROUP BY   CASE WHEN v.plan like '%Pro%' THEN 'Otros' 
				WHEN v.campana like '%metros%' THEN 'Otros' 
				ELSE v.campana END,
		CASE WHEN v.campana LIKE '%metros%' THEN 'plan kilometros'
				WHEN IFNULL(t.financiadopor,'') LIKE '%CLAN%' THEN 'EL Clan'
				WHEN IFNULL(t.financiadopor,'') LIKE '%MOVILIZA%' THEN 'Moviliza'
				ELSE v.plan END
		)dat  order by dat.campana,plan asc ";

        if($opc == 2){
            $sql2 = "SELECT SUM(dat2.enviados) enviados,sum(dat2.aceptados)aceptados,ROUND(((sum(dat2.aceptados)/sum(dat2.enviados)) *100),2) porc 
                    FROM ( $sql )dat2";
            $Databienbenida2=self::sentencia($sql2);
            return !empty($Databienbenida2)?array_shift($Databienbenida2):false;
        }else{

            $Databienbenida=self::sentencia($sql);
            return !empty($Databienbenida)?$Databienbenida:false;
        }
    }

	// Lista total de pendientes por gestionar
	public static function  dataWelcomeMailPendientes ($fInicio,$fFin,$campana,$opc){

        $fil ="";
        if($fInicio!=''){
            $fil.=" AND DATE_FORMAT(t.dateSend,'%Y-%m-%d')>='".$fInicio."' AND DATE_FORMAT(t.dateSend, '%Y-%m-%d') <='".$fFin."'  " ;
        }
        if($campana!=""){
            $fil.= " AND CASE WHEN v.plan like '%Pro%' THEN 'Otros' 
        WHEN v.campana like '%metros%' THEN 'Otros' 
        ELSE v.campana END like '%$campana%' ";
        }

        $sql="  SELECT
		*
		FROM Terms t
		INNER JOIN operaciones o on o.idoperacion = t.idOperation
		INNER JOIN vehiculos v on v.idvehiculo = o.idvehiculo
		WHERE 1=1
		AND IFNULL(statusAccept,0)=0
		$fil
		and CONCAT(t.idlocation ,t.idcustomer) not in (select CONCAT(nt.idlocation ,nt.idcustomer) from no_atenders_general_automatico nt) ";

		$Databienbenida=self::sentencia($sql);
		return !empty($Databienbenida)?$Databienbenida:false;
    }



    public static function verificarSiRolUsuario($idrol){
        global $session;
        $query= "SELECT * From usuarios u where u.idrol ='$idrol' and u.estado =37 and idusuario='$session->idusuario' LIMIT 1";

        $verEMenu=self::sentencia($query);
        return !empty($verEMenu)?true:false;
    }

    public function download_files($idinvoice){
        global $db;
        global $session;
        $sqlrc="INSERT INTO download_files (created,id_invoice) VALUES ('";
        $sqlrc.=$db->escapeString($session->idusuario)."','";
        $sqlrc.=$idinvoice."')";
        return ($db->query($sqlrc))?true:false;
    }




    public function ObtenerListOnyx($datos) {
        $sql="SELECT CONCAT(sc.idcustomer,'',sc.idlocation) idcli,sc.idcustomer,sc.idlocation ,sc.idcompany ,
              CONCAT(sc.lastname ,' ',sc.firstname ) cliente,sc.lastname,sc.firstname,
              sc.identdoc,sc.mail ,sc.phonemain ,sc.phonecell ,sc.addressmain,max(ct.dateservto) maxContrato
              FROM sid_customer sc 
              inner join sid_contract ct on ct.idcustomer = sc.idcustomer and ct.idloccust =sc.idlocation  and ct.idcompany =sc.idcompany
              WHERE CONCAT(sc.lastname ,' ',sc.firstname ) like '%$datos%' or sc.identdoc like '%$datos%' 
              group by CONCAT(sc.idcustomer,'',sc.idlocation),sc.idcustomer,sc.idlocation ,sc.idcompany ,
              CONCAT(sc.lastname ,' ',sc.firstname ),sc.identdoc,sc.mail ,sc.phonemain ,sc.phonecell ,sc.addressmain
              HAVING maxContrato>=now()
              LIMIT 10";
        $rslt = self::sentenciaOnix($sql);
        return $rslt;
    }


    public function ObtenerTTVehiclexClientOnyx($idcompany,$idlocation,$idcustomer) {

        $sql="
                select count(DISTINCT sv.idvehicle) ttvehicleclient FROM sid_vehicle sv  
                inner join sid_contract ct on (sv.idvehicle=ct.idvehicle and sv.idcustomer=ct.idcustomer and sv.idlocation=ct.idloccust)
                where sv.idcustomer='$idcustomer'  and sv.idlocation ='$idlocation' and sv.idcompany='$idcompany' 
                and ct.dateservto >= now();
             ";
        $rslt = self::sentenciaOnix($sql);
        return   ($rslt)?array_shift($rslt):0;
    }


    public function ObtenerTTVehiclexClientOnyx2($idlocation,$idcustomer) {

        $sql="
                select count(DISTINCT sv.idvehicle) ttvehicleclient FROM sid_vehicle sv  
                inner join sid_contract ct on (sv.idvehicle=ct.idvehicle and sv.idcustomer=ct.idcustomer and sv.idlocation=ct.idloccust)
                where sv.idcustomer='$idcustomer'  and sv.idlocation ='$idlocation' 
                and ct.dateservto >= now();
             ";
        $rslt = self::sentenciaOnix($sql);
        return   ($rslt)?array_shift($rslt):0;
    }



    public function ObtenerClienteOnyx($idlocation,$idcustomer) {
        $sql="SELECT CONCAT(sc.idcustomer,'',sc.idlocation) idcli,sc.idcustomer,sc.idlocation ,sc.idcompany ,
              CONCAT(sc.lastname ,' ',sc.firstname ) cliente,sc.lastname,sc.firstname,
              sc.identdoc,sc.mail ,sc.phonemain ,sc.phonecell ,sc.addressmain,max(ct.dateservto) maxContrato
              FROM sid_customer sc 
              inner join sid_contract ct on ct.idcustomer = sc.idcustomer and ct.idloccust =sc.idlocation  and ct.idcompany =sc.idcompany
              WHERE sc.idcustomer='$idcustomer'  and sc.idlocation ='$idlocation' 
              group by CONCAT(sc.idcustomer,'',sc.idlocation),sc.idcustomer,sc.idlocation ,sc.idcompany ,
              CONCAT(sc.lastname ,' ',sc.firstname ),sc.identdoc,sc.mail ,sc.phonemain ,sc.phonecell ,sc.addressmain
              HAVING maxContrato>=now()
              LIMIT 1";
        $rslt = self::sentenciaOnix($sql);
        return   ($rslt)?array_shift($rslt):0;
    }



    public function ObtenerVehicleReg($idlocation,$idcustomer) {
        $sql="SELECT  count(*) truevehicle
              FROM no_atenders_general_automatico hc
              WHERE hc.idlocation='$idlocation' and hc.idcustomer='$idcustomer'
            ";
        $rslt = self::sentencia($sql);
        return   ($rslt)?array_shift($rslt):0;
    }



    public function registerCoorporativo(){
        global $db;
        $sql=" INSERT INTO no_atenders_general_automatico(tipo,idcustomer, idlocation) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapestring(($this->tipo))."' , '";
        $sql.= $db->escapestring(($this->idcustomer))."' , '";
        $sql.= $db->escapeString($this->idlocation)."')";
        if ($db->query($sql)) {
            return true;
        } else {
            return false;
        }
    }



    public static function  listCoorporativoReg(){


        $sql=" SELECT nag.idcustomer ,nag.idlocation FROM no_atenders_general_automatico nag ";
        $segparabienbenida=self::sentencia($sql);
        return !empty($segparabienbenida)?$segparabienbenida:false;
    }



    public function actualizarDatosActivacionOperacion(){
        global $db;
        global $session;
        $sql="UPDATE operaciones SET ";
        $sql.=" estado= '15' ";
        $sql.=" WHERE idoperacion = '$this->idoperacion'";
        if ($db->query($sql)){
            return true;
        }else{
            return false;
        }
    }

	//FUNCIÓN DE RESH PARA QUERY

	public static function obtenerFechaParaValidacion($chasis){
		// global $db;
		// $chasisSS=htmlspecialchars(htmlentities($chasis));
		$sqlque="SELECT o.idoperacion, o.idChequeo, o.idvehiculo, tb.idtablaProgramacion, tb.fecha, tb.hora, tb.observacion, tb.ubicacion, 
		o.trabajo, c.nombre, c.apellidosC, v.chasis, v.placa, v.marca, v.modelo, ts.first_name, ts.last_name AS nomtec, t.min_desc AS tecnicolist
		FROM tablaProgramacion tb
		LEFT JOIN operaciones o ON tb.idoperacion = o.idoperacion
		LEFT JOIN clientes c ON o.idcliente = c.idcliente
		LEFT JOIN vehiculos v ON o.idvehiculo = v.idvehiculo
		LEFT JOIN tecnico_user ts ON ts.id = o.idtecnicoAsig
		LEFT JOIN tecnicos t ON t.idtec = tb.idtecnico
		WHERE v.chasis = '$chasis' ORDER BY tb.fecha DESC LIMIT 1";
        $obtenerCliente=self::sentencia($sqlque);
	return !empty($obtenerCliente) ? array_shift($obtenerCliente):false;  
	}

	public static function obtenerFechaPorCliente($idcliente,$idvehiculo){
		// global $db;
		// $chasisSS=htmlspecialchars(htmlentities($chasis));
		$sqlque="SELECT o.idoperacion, o.idChequeo, o.idcliente, o.idvehiculo, o.idlocation, tb.idtablaProgramacion, tb.fecha, tb.hora, ts.last_name AS nomtec, t.min_desc AS tecnicolist
		FROM tablaProgramacion tb
		LEFT JOIN operaciones o ON tb.idoperacion = o.idoperacion
		LEFT JOIN clientes c ON o.idcliente = c.idcliente
		LEFT JOIN tecnico_user ts ON ts.id = o.idtecnicoAsig
		LEFT JOIN tecnicos t ON t.idtec = tb.idtecnico
		WHERE c.idcliente = '$idcliente' AND o.idvehiculo = '$idvehiculo' ORDER BY tb.fecha DESC LIMIT 1";
        $obtenerCliente=self::sentencia($sqlque);
	return !empty($obtenerCliente) ? array_shift($obtenerCliente):false;  
	}

	// public static function allTrabajos(){
	// 	$sqlque="SELECT trabajo FROM operaciones";
    //     $trab=self::sentencia($sqlque);
	// return !empty($trab) ? array_shift($trab):false;  
	// }

	// public static function allTrabajos() {
	// 	$sqlque = "SELECT trabajo, COUNT(*) as cantidad FROM operaciones GROUP BY trabajo";
	// 	$trabajos = self::sentencia($sqlque);
		
	// 	// Inicializa un array asociativo para almacenar las cantidades de cada tipo de trabajo
	// 	$trabajosCantidades = array();
	
	// 	// Itera sobre los resultados y almacena las cantidades en el array asociativo
	// 	foreach ($trabajos as $trabajo) {
	// 		$trabajosCantidades[$trabajo->trabajo] = $trabajo->cantidad;
	// 	}
	
	// 	// Devuelve el array asociativo con las cantidades
	// 	return $trabajosCantidades;
	// }

	public static function TrabajoSinReprogramacion($startDate, $endDate) {
		// Construye la parte de la condición de fecha basada en la existencia de $startDate y $endDate
		$fechaCondition = '';
		if (!empty($startDate) && !empty($endDate)) {
			$fechaCondition = " AND DATE_FORMAT(o.fechaInstalacion, '%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";
		}
	
		// Construye la consulta SQL
		//$sqlque = "SELECT trabajo, COUNT(*) as cantidad FROM operaciones WHERE 1 $fechaCondition GROUP BY trabajo";
		$sqlque = "SELECT o.trabajo, COUNT(*) AS cantidad
		FROM operaciones o
		INNER JOIN tablaProgramacion tp ON o.idoperacion = tp.idoperacion
		where tp.estado != '321' $fechaCondition
		GROUP BY o.trabajo";
		
		// Ejecuta la consulta y obtiene los resultados
		$trabajos = self::sentencia($sqlque);
	
		// Inicializa un array asociativo para almacenar las cantidades de cada tipo de trabajo
		$trabajosCantidades = array();
	
		// Itera sobre los resultados y almacena las cantidades en el array asociativo
		foreach ($trabajos as $trabajo) {
			$trabajosCantidades[$trabajo->trabajo] = $trabajo->cantidad;
		}
	
		// Devuelve el array asociativo con las cantidades
		return $trabajosCantidades;
	}
	
	public static function TrabajoConReprogramacion($startDate, $endDate) {
		// Construye la parte de la condición de fecha basada en la existencia de $startDate y $endDate
		$fechaCondition = '';
		if (!empty($startDate) && !empty($endDate)) {
			$fechaCondition = " AND DATE_FORMAT(o.fechaInstalacion, '%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";
		}
	
		// Construye la consulta SQL
		//$sqlque = "SELECT trabajo, COUNT(*) as cantidad FROM operaciones WHERE 1 $fechaCondition GROUP BY trabajo";
		$sqlque = "SELECT o.trabajo, COUNT(*) AS cantidad
		FROM operaciones o
		INNER JOIN tablaProgramacion tp ON o.idoperacion = tp.idoperacion
		where tp.estado = '321' $fechaCondition
		GROUP BY o.trabajo";
		
		// Ejecuta la consulta y obtiene los resultados
		$trabajos = self::sentencia($sqlque);
	
		// Inicializa un array asociativo para almacenar las cantidades de cada tipo de trabajo
		$trabajosCantidades = array();
	
		// Itera sobre los resultados y almacena las cantidades en el array asociativo
		foreach ($trabajos as $trabajo) {
			$trabajosCantidades[$trabajo->trabajo] = $trabajo->cantidad;
		}
	
		// Devuelve el array asociativo con las cantidades
		return $trabajosCantidades;
	}

	public function obtenerCantidadTrabajosPorDia($fechaInicio, $fechaFin) {
		global $mysqli; // Asegúrate de tener acceso a tu conexión a la base de datos
	
		// Construye la parte de la condición de fecha basada en la existencia de $startDate y $endDate
		$fechaCondition = '';
		if (!empty($fechaInicio) && !empty($fechaFin)) {
			$fechaCondition = " AND DATE_FORMAT(FROM_UNIXTIME(fechaCreacion), '%Y-%m-%d') BETWEEN '$fechaInicio' AND '$fechaFin'";
		}
	
		// Construye la consulta SQL
		$sql = "SELECT trabajo, COUNT(*) AS cantidad FROM operaciones WHERE 1 $fechaCondition GROUP BY trabajo";
	
		$result = $mysqli->query($sql);
	
		$data = array();
	
		while ($row = $result->fetch_assoc()) {
			$data[] = $row;
		}
	
		return $data;
	}
	

}

?>

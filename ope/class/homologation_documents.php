<?php

class homologation_documents extends Recursos
{
    public $idhomdocument;
    public $idhomdetail;
    public $bucket;
    public $idvalidation;
    public $name;
    public $type;
    public $description;
    public $url;
    public $status;
    public $created;
    public $creation_at;
    public $last_modify;
    public $update_at;
    public $desctype;
    public $checkimg;


    public function registerHomologation_documents(){
        global $db;
        $sql=" INSERT INTO homologation_documents(idhomdetail,description,url) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString(($this->idhomdetail))."' , '";
        $sql.= $db->escapeString(($this->description))."' , '";
        $sql.= $db->escapeString($this->url)."')";
        if ($db->query($sql)) {
            $this->idhomdocument=$db->ultimoIdInsertado();
            return true;
        } else {
            return false;
        }
    }

    public function updateHomologation_documents(){
        global $db;
        $this->update_at=date("YmdHis");
        $sql=" UPDATE homologation_documents SET ";
        $sql.=" idhomdetail ='".$db->escapeString($this->idhomdetail)."',";
        $sql.=" description ='".$db->escapeString($this->description)."',";
        $sql.=" url ='".$db->escapeString($this->url)."',";
        $sql.=" update_at ='".$this->update_at."' ";
        $sql.=" WHERE idhomdocument ='".$db->escapeString($this->idhomdocument)."'";
        if ($db->query($sql)) {
            return true;
        } else {
            echo "the fail";
        }
    }

    public function getImages($idDetail) {
        $sql="select idhomdocument,idhomdetail,bucket,idvalidation,name,type,created,creation_at,last_modify,updated_at,
                status,sc.desctype,sc.checkimg,sc.type
              FROM homologation_documents sc 
              WHERE  idhomdetail='".$idDetail."' and status='A'
        ";
        $rslt = self::sentencia($sql);
        return $rslt;
    }

    public function getImagesCheckTrue($idDetail) {
        $sql="select idhomdocument,idhomdetail,bucket,idvalidation,name,type,created,creation_at,last_modify,updated_at,
                status,sc.desctype,sc.checkimg
              FROM homologation_documents sc 
              WHERE  idhomdetail='".$idDetail."' and status='A' and checkimg=true
        ";
        $rslt = self::sentencia($sql);
        return $rslt;
    }

    public function updatecheckDocumentimg()
    {
        global $db;
        global $session;
        $this->update_at = date("YmdHis");
        $sql = " UPDATE homologation_documents hd SET ";
        $sql .= " hd.checkimg =" . $db->escapeString($this->checkimg) . ",";
        $sql .= " updated_at ='" . $this->update_at . "' ,";
        $sql .= " last_modify ='" . $db->escapeString($session->idusuario) . "' ";
        $sql .= " WHERE hd.idhomdocument ='". $db->escapeString($this->idhomdocument)."'";

        if ($db->query($sql)) {
            return true;
        } else {
            echo "the fail";
        }
    }

}
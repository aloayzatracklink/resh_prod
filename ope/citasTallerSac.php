<?php $nav="citasTallerSac" ;?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
} 
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->registrarOperaciones=="si"): ?>
<?php
require_once '../mailphp/PHPMailer/PHPMailerAutoload.php';
require_once '../includes/tablaProgramacion.php';
$mail = new PHPMailer;  
$citaObj=new Cita();
$errores=array();
$echo=array();
$clienteObj=new Cliente();
$agregarVehiculoObj=new Vehiculo();
$facturacionObj=new DatosFacturacion();
$operacionesObj=new Operacion();
$objComision= new Comisiones();
$datosInstalacion=new DatosInstalacion;
$objTabla=new tablaProgramacion;
if (isset($_POST['go'])) {
 //   echo "<pre>";
 //   var_dump($_POST);
     if (Token::verificar($_POST['guardarCita'])) {
        if (isset($_POST['ortecnica'])) {
            $datosInstalacion->orte=limpiar($_POST['ortecnica']);
            Notificaciones::cambiarDeEstado($_POST['ortecnica']);
        }
        $dniORuc=trim(limpiar($_POST['dniORuc']));
        if (!empty($_FILES['voucher']['name'])) {
            $extencionesAbilitadas= array('jpg','jpeg','png','gif','pdf','JPG','JPEG','PNG','GIF','PDF');
            $nombreArchivo=$_FILES['voucher']['name'];        
            $tmp=explode('.', $nombreArchivo);
            $extencionArchivo=end($tmp);
            $tamanioArchivo=$_FILES['voucher']['size'];
            $ubicacionTemporal=$_FILES['voucher']['tmp_name'];
            if (!in_array($extencionArchivo,$extencionesAbilitadas)) {
              $errores[]='La extencion de la imagen deve ser jpg jpeg png gif';
            }
            if ($tamanioArchivo>5242880) {
              $errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
            }
        }
        if ($n= strlen($dniORuc)==8){
            $clienteObj->tipoCliente=ucwords(strtolower("Natural"));
            $clienteObj->dni=$dniORuc;
            $clienteObj->nombre=ucwords(strtolower($_POST['nombres']));
            $clienteObj->apellidosC=ucwords(strtolower($_POST['apellidos']));
            $clienteDatos=$_POST['apellidos']." ".$_POST['nombres'];
            $clienteObj->telefono=$_POST['celular'];
            $clienteObj->telefono1=$_POST['telefono5'];
            $clienteObj->telefono2=$_POST['telefono2'];
            $clienteObj->telefono3=$_POST['telefono'];            
            $clienteObj->correo=$_POST['correoCliente'];            
            $clienteObj->idCreador=$session->idusuario;
            $clienteObj->enOnix="si";
            if ($clienteObj->registrarClienteNatural()) {
                
            }else{
                $errores[]="Se detecto un error al registrar el cliente en empresa ";
            }
        }
        if ($n=strlen($dniORuc)==11) {
            $clienteObj->tipoCliente="Empresa";
            $clienteObj->ruc=$dniORuc;
            $clienteObj->razonSocial=ucwords(strtolower($_POST['apellidos']));
            $clienteDatos=$_POST['apellidos'];
            $clienteObj->nombre=ucwords(strtolower($_POST['nombreCliente']));
            $clienteObj->telefono=$_POST['telefono'];
            if (isset($_POST['telefono1'])||isset($_POST['telefono3'])) {
                $clienteObj->telefono1=$_POST['telefono1'];                
                $clienteObj->telefono3=$_POST['telefono3'];
            }
            $clienteObj->telefono2=$_POST['telefono2'];
            $clienteObj->correo=$_POST['correoCliente'];
            $clienteObj->idCreador=$session->idusuario;
            $clienteObj->enOnix="si";
            if ($clienteObj->registrarClienteEmpresa()) {
                
            }else{
                $errores[]="Se detecto un error al registrar el cliente en empresa ";
            }
        }
        if (empty($errores)) {
            $agregarVehiculoObj->idcliente=$clienteObj->idcliente;
            $agregarVehiculoObj->plan=$_POST['plan'];
            $agregarVehiculoObj->placa=$_POST['placaVehiculo'];
            $agregarVehiculoObj->marca=$_POST['marca'];
            $agregarVehiculoObj->modelo=$_POST['modelo'];
            $agregarVehiculoObj->color=$_POST['color'];
            $agregarVehiculoObj->anio=$_POST['anioF'];
            $agregarVehiculoObj->chasis=$_POST['chasis'];
            $agregarVehiculoObj->motor=$_POST['motor'];
            $agregarVehiculoObj->planTiempo=$_POST['tiempo'];
            $agregarVehiculoObj->companiaSeguro=$_POST['seguro'];                
            $agregarVehiculoObj->enOnix="si";
            if (isset($_POST['campana'])) {
                $agregarVehiculoObj->campana=$_POST['campana'];
            }else{
                $agregarVehiculoObj->campana='no';
            }
            if ($agregarVehiculoObj->registrarVehiculo($clienteObj->idcliente)) {       
                $facturacionObj->idcliente=$clienteObj->idcliente;
                $facturacionObj->idvehiculos=$agregarVehiculoObj->idvehiculo;
                $facturacionObj->tipoDocumentoFacturacion=$_POST['tipoDocumentoFacturacion'];
                $facturacionObj->rucFacturacion=$_POST['rucFacturacion'];
                $facturacionObj->dniFacturacion=$dniORuc;
                $facturacionObj->razonSocialFacturacion=$_POST['razonSocialFacturacion'];
                $facturacionObj->direccionFiscal=$_POST['direccionFiscalFacturacion'];
                $facturacionObj->telefonoFacturacion=$_POST['telefonoFacturacion'];  
                if (isset($_POST['entregaEnInstalacion'])) {
                    //$facturacionObj->entregaEnInstalacion=$_POST['entregaEnInstalacion'];
                }else{
                   // $facturacionObj->direccionEntrega=$_POST['direccionEntrega'];
                   // $facturacionObj->referenciaEntrega=$_POST['referenciaEntrega'];
                   // $facturacionObj->distritoEntrega=$_POST['distritoEntrega'];
                   // $facturacionObj->fechaHoraDeEntrega=$_POST['horarioEntrega'];                
                   // $facturacionObj->fecha=$_POST['horarioEntrega'];
                   // $facturacionObj->registrarSugerenciaDeFechaEntrega();                 
                   // $facturacionObj->recibidoPor=$_POST['personaEntrega'];                        
                }
                $facturacionObj->plan=$_POST['plan'];
                $facturacionObj->tiempo=$_POST['tiempo'];
                $facturacionObj->costo=$_POST['precioLista'];
                $facturacionObj->observaciones=$_POST['ordenNombre']." ".$_POST['obss']." ".$_POST['observacion'];
                $facturacionObj->idUsuarioCreado=$_POST['ejecutivoComercial'];
                $facturacionObj->oficina=$_POST['oficina'];
                $facturacionObj->nombreCanal=$_POST['nombreCanal'];
                $facturacionObj->tipoCanal=$_POST['tipoCanal'];
                $facturacionObj->ejecutivoCanal=$_POST['ejecutivoCanal'];
                /////////////////////
                $fecha=date("YmdHis");
                if (!empty($_FILES['voucher']['name'])) {  
                    move_uploaded_file($ubicacionTemporal, '../public/vouchers/'.$fecha.$nombreArchivo); 
                    $facturacionObj->voucher=$fecha.$nombreArchivo;
                }
                if ($_POST['estadoPago']=='Pagado') {
                    $facturacionObj->voucher=$_POST['voucher'];
                    $facturacionObj->observaciones=$_POST['observacion']." ".$_POST['obss'];
                }else{
                    $facturacionObj->observaciones=$_POST['formaPago']." ".$_POST['observacion']." ".$_POST['obss'];                    
                }
                $facturacionObj->idcanal=$_POST['idcanal'];    
if ($facturacionObj->registrarDatosDeFacturacion()) {
                   $operacionesObj->datosfacturacionid=$facturacionObj->datosfacturacionid;
                    if (!$facturacionObj->ingresarOrden($facturacionObj->datosfacturacionid,$_POST['orden'])) {
                        $errores[]="error en guardar numero de orden en los datos de facturación";
                    }
                    $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
                    $operacionesObj->numeroOrden=$_POST['orden'];
                    if (isset($_POST['cordenadasD']) ) {
                        $operacionesObj->coordenadasInstalacion= $_POST['cordenadasD'];
                    }
                    if (isset($_POST['concesionarioInstalacion'])) {
                        $operacionesObj->nombreConcecionarioInstalicion=$_POST['concesionarioInstalacion']; 
                    }    
                    $operacionesObj->observacion=$_POST['observacion'];
if ($_POST['comsiona']=='si') {
    $objComision->tiempo=$_POST['tiempo'][0];
    $objComision->idcliente=$clienteObj->idcliente;
    $objComision->idcanal='0';
    $objComision->idvehiculo=$agregarVehiculoObj->idvehiculo;
    $objComision->idfacturacion=$facturacionObj->datosfacturacionid;
    $objComision->montoTotal=$_POST['precioLista'];
    $objComision->comision="PENDIENTE";
    $objComision->idCreador=$_POST['ejecutivoComercial'];
    if (!empty($_FILES['voucher']['name'])) {
      $objComision->fechapago=date("YmdHis");
    }
    if ($_POST['estadoPago']=='Pagado') {
            $objComision->fechapago=substr($_POST['voucher'], 0, 14);
        }
    if (isset($_POST['tipoDocumentoFacturacion'])) {
      $objComision->documentoFacturacion=$facturacionObj->tipoDocumentoFacturacion;
    }else{
      $objComision->documentoFacturacion=$_POST['formaPago'];
    }
    if (!$objComision->registrarComision()) {
      $errores[]="Error en registrar datos de comision";
    }
}

    $citaObj->idcita=$_POST['idcita'];
    if(!$citaObj->instalacionProgramada()){
        $error[]="Error guardando cita";
    }

    $operacionesObj->idcita=$_POST['idcita'];
if ($_POST['lugarInstacion']=='Taller') {
    if (isset($_POST['pendienteInstacion']) AND $_POST['pendienteInstacion']=='si') {
        
    }else{
        if (isset($_POST['fechaIT'])||isset($_POST['horaIT'])) {
            $operacionesObj->fechaInstalacion=$_POST['fechaIT']." ".$_POST['horaIT'];
        }
        $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
        $operacionesObj->departamentoi='15';
        $operacionesObj->provinciai='127';
        $operacionesObj->distritoi='1291';
        $operacionesObj->direccionInstacion=$_POST['direccionTaller'];
        $operacionesObj->nombreContacto=$_POST['nombreContactoIT'];
        $operacionesObj->numeroContacto=$_POST['numeroContactoIT'];
    }

}else
if($_POST['lugarInstacion']=='Domicilio Cliente'){
    if (isset($_POST['pendienteInstacion']) AND $_POST['pendienteInstacion']=='si') {
        
    }else{
    $operacionesObj->fechaInstalacion=$_POST['fechaID']." ".$_POST['horaID'];
    $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
    $operacionesObj->nombreContacto=$_POST['nombreContactoID'];
    $operacionesObj->numeroContacto=$_POST['numeroContactoID'];
    $operacionesObj->departamentoi=$_POST['departamentoInstalacionD'];
    $operacionesObj->provinciai=$_POST['provinciaInstalacionD'];
    $operacionesObj->distritoi=$_POST['distritoInstalacionD'];
    $operacionesObj->direccionInstacion=$_POST['direccionInstalacionD']." Referencia ..".$_POST['referenciaInstalacionD'];
  }
}else
if($_POST['lugarInstacion']=='Concesionario'){
    if (isset($_POST['pendienteInstacion']) AND $_POST['pendienteInstacion']=='si') {
        $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];          
    }else{
    $operacionesObj->fechaInstalacion=$_POST['fechaIC']." ".$_POST['horaIC'];
    $operacionesObj->nombreContacto=$_POST['nombreContactoIC'];
    $operacionesObj->numeroContacto=$_POST['numeroContactoIC'];
     $operacionesObj->departamentoi=$_POST['departamentoInstalacionC'];
    $operacionesObj->provinciai=$_POST['provinciaInstalacionC'];
    $operacionesObj->distritoi=$_POST['distritoInstalacionC'];
    $operacionesObj->direccionInstacion=$_POST['direccionInstalacionC']." Referencia ..".$_POST['referenciaInstalacionC'];
    $operacionesObj->nombreConcecionarioInstalicion=$_POST['concesionarioInstalacion'];
    }
}   
    if (isset($_POST['pendienteInstacion']) AND $_POST['pendienteInstacion']=='si') {
        $operacionesObj->estado='0';        
    }else{
        $operacionesObj->estado='36';
        $operacionesObj->numeroOrte=$_POST['ortecnica'];
        $operacionesObj->ordenContrato=$_POST['ordenContrato'];
        Vehiculo::cambiarEstado($agregarVehiculoObj->idvehiculo);   
    }
    $operacionesObj->idCreador=$_POST['ejecutivoComercial'];
    $operacionesObj->fechaConfirmacionOPE=$_POST['fechaOpe'];
    $operacionesObj->idConfirmacionOPE=$_POST['idOpe'];
    $objTabla->idcita=$_POST['idcita'];
   if ($operacionesObj->programarInstalacionDesdeCita($agregarVehiculoObj->idvehiculo,$clienteObj->idcliente)) {
    $operacionesObj->guardarNumeroDeOrden();
    if (isset($_POST['pendienteInstacion']) AND $_POST['pendienteInstacion']=='si') {
            $obs="<span style='color:red;'><strong>Cliente solo realizo el pago, pendiente datos de instalación</strong></span>";         
            $objTabla->borrarEspacioCita();
            
    }else{        
       $objTabla->idoperacion=$operacionesObj->idoperacion;
       $objTabla->citaConfirmada();
            if($datosOb=$datosInstalacion->verificarSiOrteEstaenTMP()){
                $datosInstalacion->idoperacion=limpiar($operacionesObj->idoperacion);
                $datosInstalacion->iddatosInstalacion=$datosOb->iddatosInstalacion;
                if (!$datosInstalacion->agregaridOperacion()) {
                    $errores[]="Error en agregar orte";
                }
                Log::registrarLog("citas",$_POST['idOpe'],$_POST,'TEMPORAL DESDE SAC CITA');  
                $operacionesObj->estado=$datosOb->estado;
                $operacionesObj->idoperacion=$operacionesObj->idoperacion;
                if ($operacionesObj->cambiarEstadoOperacion()) {
                    $operacionesObj->idvehiculo=$agregarVehiculoObj->idvehiculo;
                    $operacionesObj->cambiarEstadoVehclo();
                }else{
                    $errores[]="Error en cambiar estado tmp.";
                }
            }
            $obs=$_POST['observacion']." ";
    }
    
$clienteCita=$_POST['clienteCita'];
$placa=$_POST['placaVehiculo'];
$usuarioCrea=Usuario::obtenerUsuario($_POST['ejecutivoComercial']);

$usuarioOPE=Usuario::obtenerUsuario($usuarioCrea->comercialOperaciones);
  // <tr>  //                                   <td style='padding: 0 0 20px;text-align: center; '>
  //<a href='http://resh.tracklink.pe/com/registrarCliente' style='text-decoration: none;    color: #FFF;    background-color: #007cc3;    border: solid #007cc3;    border-width: 5px 10px;    line-height: 2;
  //   font-weight: bold;    text-align: center;    cursor: pointer;
  //   display: inline-block;    border-radius: 5px;    text-transform: capitalize;'>Completar datos</a>
  //                                   </td>
  //                               </tr>
$htmlContenido1="<body style=' margin: 0;padding: 0;font-family:Helvetica, Arial, sans-serif;    box-sizing: border-box;    font-size: 14px; background-color: #f6f6f6; '>
<table style=' background-color: #f6f6f6; width: 100%;'>
    <tr>
        <td></td>
        <td  width='600' style=' display: block !important;    max-width: 600px !important;    margin: 0 auto !important;    /* makes it centered */    clear: both !important;'>
            <div class='content' style='  max-width: 600px;    margin: 0 auto;    display: block;    padding: 20px;'>
                <table class='main' width='100%' cellpadding='0' cellspacing='0' style='background: #fff;    border: 1px solid #e9e9e9;    border-radius: 3px;'>
                    <tr>
                        <td style='font-size: 16px;    color: #fff;    font-weight: 500;    padding: 20px;    text-align: center;    border-radius: 3px 3px 0 0;  background: #007cc3;'>
                            CITA CONFIRMADA
                        </td>
                    </tr>
                    <tr>
                        <td style='padding: 10px !important ;padding: 20px; vertical-align: top;'>
                            <table width='100%' cellpadding='0' cellspacing='0'>                                
                                <tr>
                                    <td  style='padding: 0 0 20px;'>
                                        Estimada(o) $usuarioCrea->usuarioNombre $usuarioCrea->usuarioApellidoPaterno. <br>Cliente:<strong>$clienteDatos </strong><br>Placa $placa <br>Plan: $agregarVehiculoObj->plan <br>Tiempo : $facturacionObj->tiempo  <br> Precio : $facturacionObj->costo </td>
                                    
                                </tr>                              
                                <tr>
                                    <td style='padding: 0 0 20px;'>
                                       Cita de $clienteCita <br>
                                       $obs
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div style=' width: 100%;    clear: both;    color: #999;    padding: 20px;'>
                    <table width='100%'>
                        <tr>
                            <td style='font-size: 12px; text-align: center;padding: 0 0 20px;' ><a href='#' style='color: #999;'>Desuscriberse</a> de estas alertas.</td>                        </tr>
                    </table>
                </div></div>
        </td>
        <td></td>
    </tr>
</table>
</body>
</html>";
$mail->isSMTP();
$mail->SetLanguage( 'es', '../mailphp/PHPMailer/language/' );
$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->Host = gethostbyname('mail.tracklink.pe');
$mail->Port = 25;
$mail->SMTPAuth = false;
$mail->CharSet = 'UTF-8';
$mail->setFrom('no-responder@tracklink.pe','Resh');  
$mail->addReplyTo("pvasquez@tracklink.pe", 'Soporte resh'); 
$mail->addAddress($usuarioCrea->usuario,$usuarioCrea->usuarioNombre." ".$usuarioCrea->usuarioApellidoPaterno); 
//$mail->addAddress($usuarioEje->usuario,$usuarioEje->usuarioNombre." ".$usuarioEje->usuarioApellidoPaterno); 
$mail->addCC($usuarioOPE->usuario,$usuarioOPE->usuarioNombre." ".$usuarioOPE->usuarioApellidoPaterno);
$mail->addBCC("pruebas-ti@tracklink.pe");
$mail->Subject='Cita confirmada: '.ucwords(strtolower($clienteDatos)).' ';
$mail->msgHTML($htmlContenido1);
$mail->AltBody = 'Estimado(as) '.$usuarioCrea->usuarioNombre." ".$usuarioCrea->usuarioApellidoPaterno.' cita confirmada de cliente '.$clienteDatos; 
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {     

}
}
}
    }
        }
    }else{
        $errores[]="Tiempo de espera agotado intentalo nuevamente.";
    }
}

if(isset($_POST['activar'])){
    $citaObj->idcita=$_POST['activar'];
    if ($citaObj->activarCitaQ()) {
        $echo[]="Activado con exito";
    }else{
        $errores[]="Error en activar";
    }
}


if (isset($_POST['reprogramar'])) {
    $citaObj->idcita=$_POST['idcita'];
    $citaObj->obsSac=$_POST['motivo'];
    $objTabla->idcita=$citaObj->idcita;
    $objTabla->borrarEspacioCita();
    if(!$citaObj->reprogramarCita()){
        $errores[]='Algun error ocurrio al momento de reprogramar la cita';
    }else{
        $echo[]="Reprogramado con exito";
    }
}
?>
<link href="../assets/css/plugins/select2/select2.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>
<link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
<link rel="stylesheet" href="../../assets/css2/style2.css">
<link rel="stylesheet" href="../../assets/css2/style3.css">
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
               <?php include("../includes/menuIzquierdo.php") ?>
           </div>
       </nav>
       <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../includes/navegacionArriba.php") ?>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">               
                <div class="ibox float-e-margins col-lg-12"> 
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-7">
                                <input type="text" class="form-control input-sm m-b-xs" id="filter1"
                        placeholder="Filtrar vehículo">                                
                            </div>
                            <div class="col-md-1">
                                <div class="col-md-12">
                                    <a class="btn btn-primary" href="historialSac">Ver Historial</a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="col-md-4">
                                    <button class="btn btn-info" data-toggle="modal" data-target="#ordenes" data-id="REPROGRAMAR" >Ver citas en estado de reprogramación</button>
                                </div>
                            </div>
                        </div>
                        <table class="footable table table-stripped" data-page-size="8" data-filter=#filter1>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Cliente</th>
                                    <th>Placa</th>
                                    <th>Fecha Programada</th>
                                    <th>Estado(s)</th>
                                    <th>A cargo de: </th>
                                    <th>Acción</th>
                                    <th>Creado por</th>
                                    <!-- data-hide="phone,tablet" -->
                                </tr>
                            </thead>    
                            <tbody id="dataOperaciones">                              
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- modal inicio -->

<div class="modal inmodal" id="ordenes" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content animated bounceIn">
                <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title">Confirmar cita taller.</h4>
                </div>
                <form  role="form" method="post" enctype="multipart/form-data" id="citatallerSac">
                    <div class="modal-body" style="background: #fff;padding-top: 0">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                        <button type="submit" name="guardarCita" class="btn btn-primary" onclick="crearCitaTallaerJS()" value="<?php echo Token::generar() ?>">Guardar Cambios</button>
                    </div>
                </form>   
            </div>
        </div>
</div>

</div>
<?php include("../includes/footer.php") ?>
</div>
</div>
</div>
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<!-- <script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script> -->
<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/build/jquery.datetimepicker.full.js"></script>
<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
 <?php include_once('../includes/script.php') ?>
 <!-- <?php include_once('../includes/campanasScript.php') ?>  -->
<script>
$("#citatallerSac").validate({});
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
<script>
var eventos={}
eventos.frejcar =function(){
    $.ajax({
            url: 'ajax/citasDatosA',
            type: 'POST', 
            async: true,
            success: function(data){
                $('#dataOperaciones').html(data);
            }
    });
}
eventos.interval = setInterval(eventos.frejcar, 10000);
eventos.frejcar();
$("#filter1").keyup(function() {
    var str=$("#filter1").val();
    var n = str.length;
    if (n==0) {
        eventos.interval = setInterval(eventos.frejcar, 5000);
        eventos.frejcar();
    }else{
    clearInterval(eventos.interval);        
    }

});
    $(document).ready(function(){
        $('.footable').footable();
        $('.footable2').footable();
    });
    $('#ordenes').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        $.ajax({
            type : 'post',
            url : 'modal/citasModal', 
            data :  'rowid='+ rowid, 
            success : function(data){
            $('.modal-body').html(data);
        }
    });
    });
    function crearCitaTallaerJS(){
         document.getElementById("motivo").removeAttribute("required");
    }
</script>
<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>

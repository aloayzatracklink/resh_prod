<?php $nav = "indicadoresTrabajo" ?>
<?php require("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
}
?>
<?php $obMenu = Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->indicadoresTrabajo == "si"): ?>
    <link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
    <link href="../assets/calendario/fullcalendar.min.css" rel="stylesheet">
    <link href="../assets/calendario/fullcalendar.print.min.css" rel='stylesheet' media='print'>
    <link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
    <link href='../assets/calendario/scheduler.min.css' rel='stylesheet' />
    <link rel="stylesheet" href="../assets/css/jquery-confirm.min.css">
    <?php
    $errores = array();
    $echo = array();
    $operacionObj = new Operacion;
    $calendarioObj = new Calendario;

    // // Inicializa las variables de fecha al cargar la página
    // $startDate = date('Y-m-d');  // Fecha actual
    // $endDate = date('Y-m-d');    // Fecha actual

    // // Verifica si se ha enviado un formulario (se ha presionado el botón "Actualizar")
    // if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['btnActualizar'])) {
    //     // Si es así, actualiza las variables de fecha
    //     $startDate = $_POST['startDate'];
    //     $endDate = $_POST['endDate'];
    // }

    ?>
    <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
    <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/sweetalert2.css" rel="stylesheet">
    <link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
    <!-- Agrega Bootstrap CSS -->


    <style>
        #wrapper {
            position: relative;
        }

        #pieChartContainer {
            position: absolute;
            top: 0;
            right: 0;
        }

        /* Estilo para los paneles o cards */
        .panel {
            margin-bottom: 20px;
            padding: 0;
            /* Asegura que no haya relleno interno */
        }

        .panel-default {
            border-color: #ddd;
        }

        .panel-heading {
            background-color: #f5f5f5;
            border-color: #ddd;
            width: 100%;
            /* Ocupa todo el ancho del panel */
        }

        .panel-title {
            color: #333;
        }

        .panel-body {
            padding: 15px;
        }

        .panel-margined {
            margin-right: 20px;
            margin-left: 20px;
        }
    </style>

    <body>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <?php include("../includes/menuIzquierdo.php") ?>
                </div>
            </nav>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <?php include("../includes/navegacionArriba.php") ?>
                    </nav>
                </div>

                <div class="wrapper wrapper-content">
                    <div class="row ibox float-e-margins ibox-content">

                        <!-- Filtro de fechas -->
                        <div class="col-md-12 panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Filtro de Fechas</h3>
                            </div>
                            <div class="panel-body">
                                <label for="startDate">Fecha de inicio:</label>
                                <input type="date" id="startDate" name="startDate" value="<?= date('Y-m-d') ?>">
                                <label for="endDate">Fecha de fin:</label>
                                <input type="date" id="endDate" name="endDate" value="<?= date('Y-m-d') ?>">
                                <button onclick="actualizarDatos()">Actualizar</button>
                            </div>
                        </div>

                        <div class="col-md-12 panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Sin reprogramación</h3>
                            </div>
                            <!-- Tabla de cantidades -->
                            <div class="col-md-6">
                                <!-- <div class="panel-heading">
                                    <h3 class="panel-title">Sin reprogramación</h3>
                                </div> -->
                                <div class="panel-body">
                                    <table id="tablaCantidadesSinRepro" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Tipo de Trabajo</th>
                                                <th>Cantidad</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- Aquí puedes agregar filas dinámicamente con PHP -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- Gráfico de barras -->
                            <div class="col-md-6">
                                <!-- <div class="panel-heading">
                                    <h3 class="panel-title">Sin reprogramación</h3>
                                </div> -->
                                <div class="panel-body">
                                    <canvas id="barChart" width="400" height="200"></canvas>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Con reprogramación</h3>
                            </div>
                            <!-- Tabla de cantidades -->
                            <div class="col-md-6">
                                <!-- <div class="panel-heading">
                                    <h3 class="panel-title">Con reprogramación</h3>
                                </div> -->
                                <div class="panel-body">
                                    <table id="tablaCantidadesConRepro" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Tipo de Trabajo</th>
                                                <th>Cantidad</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- Aquí puedes agregar filas dinámicamente con PHP -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- Gráfico de barras -->
                            <div class="col-md-6">
                                <!-- <div class="panel-heading">
                                    <h3 class="panel-title">Con reprogramación</h3>
                                </div> -->
                                <div class="panel-body">
                                    <canvas id="pieChart" class="embed-responsive-item"></canvas>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <?php include("../includes/footer.php") ?>
            </div>
        </div>
    </body>
    <!-- LOS MODALES AQUI  -->
    <!-- <button class='btn' data-clipboard-action='copy' data-clipboard-target='p'>Copy</button> -->
    <script src="../assets/js/jquery-2.1.1.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="../assets/js/inspinia.js"></script>
    <script src="../assets/js/plugins/pace/pace.min.js"></script>
    <script src="../assets/js/plugins/footable/footable.all.min.js"></script>
    <script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <script src="../assets/js/clipboard.min.js"></script>
    <script src="../assets/calendario/dist/sweetalert.min.js"></script>

    <!-- Toastr -->
    <script src="../assets/js/plugins/toastr/toastr.min.js"></script>
    <script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
    <script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="../assets/js/jquery-confirm.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>


    <?php //include_once('../includes/script.php') ?>

    <!-- Script para los gráficos -->
    <script>

        let datos;
        let myBarChart;
        let myPieChart;


        $(document).ready(function () {
            actualizarDatos();
        });


        function actualizarDatos() {
            let startDate = document.getElementById('startDate').value;
            let endDate = document.getElementById('endDate').value;

            // console.log(startDate);
            // console.log(endDate);

            // Realizar solicitud AJAX al servidor para obtener nuevos datos
            $.ajax({
                url: 'indicadoresAjax',
                type: 'POST',
                data: { startDate: startDate, endDate: endDate },
                dataType: 'json',
                success: function (data) {
                    // Actualizar gráficos y paneles con los nuevos datos recibidos
                    crearGraficos(data.barrasSinRepro, data.barrasConRepro);
                    actualizarGraficos(data.barrasSinRepro, data.barrasConRepro);
                    actualizarTabla(data.tablaSinRepro, data.tablaConRepro);
                },
                error: function (error) {
                    console.log('Error en la solicitud AJAX:', error);
                }
            });
        }

        function crearGraficos(barras, pie) {
            var ctxBar = document.getElementById('barChart').getContext('2d');
            if (myBarChart) {
                myBarChart.destroy();
            }
            myBarChart = new Chart(ctxBar, {
                type: 'bar',
                data: {
                    labels: Object.keys(barras),
                    datasets: [{
                        label: 'Trabajos',
                        data: Object.values(barras),
                        backgroundColor: 'rgba(75, 192, 192, 0.2)',
                        borderColor: 'rgba(75, 192, 192, 1)',
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });

            var ctxPie = document.getElementById('pieChart').getContext('2d');
            if (myPieChart) {
                myPieChart.destroy();
            }
            myPieChart = new Chart(ctxPie, {
                type: 'bar', // Cambiado a tipo 'doughnut' para un gráfico circular
                data: {
                    labels: Object.keys(pie),
                    datasets: [{
                        label: 'Trabajos',
                        data: Object.values(pie),
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                }
            });

        }

        // Función para actualizar los gráficos con nuevos datos
        function actualizarGraficos(barras, pie) {
            // Actualizar gráfico de barras
            myBarChart.data.labels = Object.keys(barras);
            myBarChart.data.datasets[0].data = Object.values(barras);
            myBarChart.update();

            // Actualizar gráfico de pie
            myPieChart.data.labels = Object.keys(pie);
            myPieChart.data.datasets[0].data = Object.values(pie);
            myPieChart.update();
        }

        // Función para actualizar la tabla con nuevos datos
        function actualizarTabla(tablaSinRepro, tablaConRepro) {
            // Limpiar la tabla actual
            $("#tablaCantidadesSinRepro tbody").empty();

            // Actualizar la tabla con los nuevos datos
            $.each(tablaSinRepro, function (trabajo, cantidad) {
                $("#tablaCantidadesSinRepro tbody").append("<tr><td>" + trabajo + "</td><td>" + cantidad + "</td></tr>");
            });

            // Limpiar la tabla actual
            $("#tablaCantidadesConRepro tbody").empty();

            // Actualizar la tabla con los nuevos datos
            $.each(tablaConRepro, function (trabajo, cantidad) {
                $("#tablaCantidadesConRepro tbody").append("<tr><td>" + trabajo + "</td><td>" + cantidad + "</td></tr>");
            });
        }


    </script>
    <script>
        $(window).load(function () {
            $(".se-pre-con").fadeOut("slow");;
        });
    </script>
<?php else: ?>
    <?php $session->salir();
    echo "NO NO NO NO ;"
        ?>
<?php endif ?>
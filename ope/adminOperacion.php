<?php $nav="adminOperacion" ?>
<?php include("../includes/head.php") ?>
<link rel="stylesheet" href="../assets/css2/style2.css">
<link rel="stylesheet" href="../assets/css2/style3.css">
<link rel="stylesheet" href="../assets/css/jquery-confirm.min.css">
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}
require_once '../mailphp/PHPMailer/PHPMailerAutoload.php';
require_once '../includes/tablaProgramacion.php';
$operacionObj=new Operacion();
$facturacionObj=new DatosFacturacion();
$chequeoObj=new Chequeos;
$calendarioObj=new Calendario();
$tablaObj=new tablaProgramacion();
$calObj=new Calendario();
$errores=array();
$echo=array();
$mail = new PHPMailer;
$mail->isSMTP();
$mail->SetLanguage( 'es', '../../mailphp/PHPMailer/language/' );
$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->Host = gethostbyname('mail.tracklink.pe');
$mail->Port = 25;
$mail->SMTPAuth = false;
$mail->CharSet = 'UTF-8';
$mail->setFrom('no-responder@tracklink.pe','Resh');  
$mail->addReplyTo("kcabrera@tracklink.pe", 'Soporte resh');
$mail->AddEmbeddedImage('../assets/img/divider.png', 'divider');
$mail->AddEmbeddedImage('../assets/img/facebook@2x.png', 'facebook@2x');
$mail->AddEmbeddedImage('../assets/img/twitter@2x.png', 'twitter@2x');
$mail->AddEmbeddedImage('../assets/img/googleplus@2x.png', 'googleplus@2x');
$mail->addBCC("pruebas-ti@tracklink.pe");
if (isset($_POST['GuardarCambios'])) {
    if (Token::verificar($_POST['GuardarCambios'])) {
        Log::registrarLog("operaciones", $_POST['idoperacion'], $_POST, 'Admin OPE Programacion');
        if (isset($_POST['motivoReprogramacion'])) {
            //  echo "<pre>";
            //  die(var_dump($_POST));
            $idoperacion = $_POST['idoperacion'];
            $ObjetoOperacion = Operacion::obtenerOperacionPorId($idoperacion);
            $operacionObj->idoperacion = $idoperacion;
            $tablaObj->idoperacion = $idoperacion;
            $tablaObj->borrarEstacioOperacion();
            $operacionObj->estado = $ObjetoOperacion->estado;
            $operacionObj->fechaInstalacion = $ObjetoOperacion->fechaInstalacion;
            $operacionObj->observacion = limpiar($_POST['motivoReprogramacion']);
            if ($operacionObj->marcarComoReprogramar()) {
                if ($ObjetoOperacion->idlocation == 0) {
                    $vehiculoObj = Vehiculo::obtenerVehiculo($ObjetoOperacion->idvehiculo);
                    $clienteObj = Cliente::obtenerClientePorVehiculo($ObjetoOperacion->idvehiculo, $ObjetoOperacion->idcliente);
                } else {
                    $clienteObj = Onix::obtenerDatosDesdeIds($ObjetoOperacion->idcliente, $ObjetoOperacion->idvehiculo, $ObjetoOperacion->idlocation);
                    $chequeoObj = Chequeos::obtenerChequeoTrackPorId($ObjetoOperacion->idChequeo);
                    $vehiculoObj = false;
                }
                if ($vehiculoObj) {
                    $datosCliente = (!empty($clienteObj->apellidosC)) ? $clienteObj->apellidosC . " " . $clienteObj->nombre : $clienteObj->razonSocial;
                } else {
                    $apellidos = utf8_encode($clienteObj->apellidos);
                    $datosCliente = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre" . " " . "$apellidos" : "$apellidos";
                }
                $datosF = DatosFacturacion::obtenerDatosFacturacionPorId($ObjetoOperacion->datosfacturacionid);
                $usuarioSolicitadorusuarioEje = Usuario::obtenerUsuario($session->idusuario);
                $ejecutivoVenta = Usuario::obtenerUsuario($datosF->idUsuarioCreado);
                $ejecutivoOpec = Usuario::obtenerUsuario($ejecutivoVenta->comercialOperaciones);
                $motivo = $_POST['motivoReprogramacion'];
                $orte = $ObjetoOperacion->numeroOrte;
                $trabajo = $ObjetoOperacion->trabajo;
                $lugar = $ObjetoOperacion->lugarInstalacion;
                $fecha = $ObjetoOperacion->fechaInstalacion;
                $usuarioSolicitador = $usuarioSolicitadorusuarioEje->usuarioNombre . " " . $usuarioSolicitadorusuarioEje->usuarioApellidoPaterno;
                $cel = $usuarioSolicitadorusuarioEje->celular;
                $correo = $usuarioSolicitadorusuarioEje->usuario;
                $ejecutivo = $ejecutivoVenta->usuarioNombre . " " . $ejecutivoVenta->usuarioApellidoPaterno;
                $ejecutivoOpe = $ejecutivoOpec->usuarioNombre . " " . $ejecutivoOpec->usuarioApellidoPaterno;
                $quienCambia = ' La reprogramación de fecha se realiza por el ejecutivo de operaciones comerciales asignado: <b>' . $ejecutivoOpe . "</b> <br> Esta solcitud a sido registrado como una incidencia ";
                $content = "
    <html xmlns='http://www.w3.org/1999/xhtml' xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office'><head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <meta name='viewport' content='width=device-width'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>    
    <style type='text/css' id='media-query'>
    body {
    margin: 0;
    padding: 0; }
    table, tr, td {
    vertical-align: top;
    border-collapse: collapse; }
    .ie-browser table, .mso-container table {
    table-layout: fixed; }
    * {
    line-height: inherit; }
    a[x-apple-data-detectors=true] {
    color: inherit !important;
    text-decoration: none !important; }
    .img-container div,  .img-container button {
    display: block !important; }
    .fullwidth button {
    width: 100% !important; }
    .block-grid .col {
    display: table-cell;
    float: none !important;
    vertical-align: top; }
    .ie-browser .num12, .ie-browser .block-grid,  .num12,  .block-grid {
    width: 745px !important; }
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
    line-height: 100%; }
    .ie-browser .mixed-two-up .num4,  .mixed-two-up .num4 {
    width: 248px !important; }
    .ie-browser .mixed-two-up .num8,  .mixed-two-up .num8 {
    width: 496px !important; }
    .ie-browser .block-grid.two-up .col,  .block-grid.two-up .col {
    width: 372px !important; }
    .ie-browser .block-grid.three-up .col,  .block-grid.three-up .col {
    width: 248px !important; }
    .ie-browser .block-grid.four-up .col,  .block-grid.four-up .col {
    width: 186px !important; }
    .ie-browser .block-grid.five-up .col,  .block-grid.five-up .col {
    width: 149px !important; }
    .ie-browser .block-grid.six-up .col,  .block-grid.six-up .col {
    width: 124px !important; }
    .ie-browser .block-grid.seven-up .col,  .block-grid.seven-up .col {
    width: 106px !important; }
    .ie-browser .block-grid.eight-up .col,  .block-grid.eight-up .col {
    width: 93px !important; }
    .ie-browser .block-grid.nine-up .col,  .block-grid.nine-up .col {
    width: 82px !important; }
    .ie-browser .block-grid.ten-up .col,  .block-grid.ten-up .col {
    width: 74px !important; }
    .ie-browser .block-grid.eleven-up .col,  .block-grid.eleven-up .col {
    width: 67px !important; }
    .ie-browser .block-grid.twelve-up .col,  .block-grid.twelve-up .col {
    width: 62px !important; }
    @media only screen and (min-width: 765px) {
    .block-grid {
    width: 745px !important; }
    .block-grid .col {
    vertical-align: top; }
    .block-grid .col.num12 {
    width: 745px !important; }
    .block-grid.mixed-two-up .col.num4 {
    width: 248px !important; }
    .block-grid.mixed-two-up .col.num8 {
    width: 496px !important; }
    .block-grid.two-up .col {
    width: 372px !important; }
    .block-grid.three-up .col {
    width: 248px !important; }
    .block-grid.four-up .col {
    width: 186px !important; }
    .block-grid.five-up .col {
    width: 149px !important; }
    .block-grid.six-up .col {
    width: 124px !important; }
    .block-grid.seven-up .col {
    width: 106px !important; }
    .block-grid.eight-up .col {
    width: 93px !important; }
    .block-grid.nine-up .col {
    width: 82px !important; }
    .block-grid.ten-up .col {
    width: 74px !important; }
    .block-grid.eleven-up .col {
    width: 67px !important; }
    .block-grid.twelve-up .col {
    width: 62px !important; } }
    @media (max-width: 765px) {
    .block-grid, .col {
    min-width: 320px !important;
    max-width: 100% !important;
    display: block !important; }
    .block-grid {
    width: calc(100% - 40px) !important; }
    .col {
    width: 100% !important; }
    .col > div {
    margin: 0 auto; }
    img.fullwidth, img.fullwidthOnMobile {
    max-width: 100% !important; } }
    </style>
    </head>
    <body class='clean-body' style='margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #E8EDEF'>
    <style type='text/css' id='media-query-bodytag'>
    @media (max-width: 765px) {
    .block-grid {
    min-width: 320px!important;
    max-width: 100%!important;
    width: 100%!important;
    display: block!important;
    }
    .col {
    min-width: 320px!important;
    max-width: 100%!important;
    width: 100%!important;
    display: block!important;
    }
    .col>div {
    margin: 0 auto;
    }
    img.fullwidth {
    max-width: 100%!important;
    }
    img.fullwidthOnMobile {
    max-width: 100%!important;
    }
    }
    </style>
    <table class='nl-container' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #E8EDEF;width: 100%' cellpadding='0' cellspacing='0'>
    <tbody>
    <tr style='vertical-align: top'>
    <td style='word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding: 0'>
            <div style='background-color:transparent;'>
    <div style='Margin: 0 auto;min-width: 320px;max-width: 745px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;' class='block-grid '>
    <div style='border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;'>
    <div class='col num12' style='min-width: 320px;max-width: 745px;display: table-cell;vertical-align: top;'>
    <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>

    <div style='font-family: Tahoma, sans-serif;line-height:120%;color:#0D0D0D; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>	
    <div style='font-size:12px;line-height:14px;color:#0D0D0D;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 17px;text-align: center'><span style='font-size: 28px; line-height: 33px;'><strong><span style='line-height: 33px; font-size: 28px;'>Reprogramar fecha y hora de la orte $orte</span></strong></span><br><span style='font-size: 28px; line-height: 33px;'>$motivo.</span></p></div>	
    </div>
    <div style='font-size: 16px;font-family: Tahoma, sans-serif; text-align: center;'>
    <style>
    .tabl{
    border: 0px solid #ccc;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    width: 100%;
    table-layout: fixed;
    }
    .capt{
    font-size: 1.5em;
    margin: .5em 0 .75em;
    }
    .tr {
    background: #fff;
    border: 1px solid #ddd;
    padding: .35em;
    }
    .thtd {
    padding: .625em;
    text-align: center;
    }
    .th {
    font-size: .85em;
    letter-spacing: .1em;
    text-transform: uppercase;
    }
    @media screen and (max-width: 600px) {
    #tabl {
    border: 0;
    }
    .thead {
    border: none;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
    }
    .trm {
    border-bottom: 3px solid #ddd;
    display: block;
    margin-bottom: .625em;
    }
    .tdm{
    border-bottom: 1px solid #ddd;
    display: block;
    font-size: .8em;
    text-align: center;
    }
    .tdb:before {
    content: attr(data-label);
    float: left;
    font-weight: bold;
    text-transform: uppercase;
    }
    .tdl:last-child {
    border-bottom: 0;
    }
    .catpm {
    font-size: 1.3em;
    }
    }</style>  
    <table class='tabl'>
    <thead class='thead'>
    <tr class='tr trm'>
    <th class='thtd th'>Trabajo</th>
    <th class='thtd th'>Lugar</th>
    <th class='thtd th'>Cliente</th>
    <th class='thtd th'>Fecha</th>
    </tr>
    </thead>
    <tbody>
    <tr class='tr trm'>
    <td data-label='Trabajo:' class='thtd th tdm tdb tdl'>$trabajo</td>
    <td data-label='Lugar:' class='thtd th tdm tdb tdl'>$lugar</td>
    <td data-label='Cliente:' class='thtd th tdm tdb tdl'>$datosCliente</td>
    <td data-label='Fecha:' class='thtd th tdm tdb tdl'>$fecha</td>
    </tr>
    </tbody>
    </table>  
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>          <div style='background-color:transparent;'>
    <div style='Margin: 0 auto;min-width: 320px;max-width: 745px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;' class='block-grid '>
    <div style='border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;'>                         
    <div class='col num12' style='min-width: 320px;max-width: 745px;display: table-cell;vertical-align: top;'>
    <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
    <div align='center' class='img-container center  autowidth ' style='padding-right: 0px;  padding-left: 0px;'>
    <img class='center  autowidth ' align='center' border='0' src='cid:divider' alt='div' title='Image' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 316px' width='316'>
    </div>
    <div style='font-family: Tahoma, sans-serif;line-height:150%;color:#555555; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>	
    <div style='font-size:12px;line-height:18px;color:#555555;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 21px;text-align: center'>Cambio solicitado por: <span style='color: rgb(168, 191, 111); font-size: 14px; line-height: 21px;'><strong>$usuarioSolicitador<br></strong></span></p></div>	
    </div>

    <div style='font-family: Tahoma, sans-serif;line-height:150%;color:#0D0D0D; padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 10px;'>	
    <div style='font-size:12px;line-height:18px;color:#0D0D0D;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 21px;text-align: center'>$quienCambia .</p></div>	
    </div>
    <div align='center' class='button-container center' style='padding-right: 10px; padding-left: 10px; padding-top:25px; padding-bottom:10px;'>
    <a style='color: #ffffff; background-color: #044E7A; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 181px; width: 151px;width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent; padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px; font-family:  Tahoma, sans-serif; text-align: center; mso-border-alt: none;  text-decoration: none' href='http://resh.tracklink.pe/com/programarInstalacion' target='_blank'  >
    <span style='font-size:16px;line-height:32px;'>Realizar el cambio</span>
    </a>
    </div>
    <div style='padding-right: 10px; padding-left: 10px; padding-top: 30px; padding-bottom: 10px;'>
    <div align='center'><div style='border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;'>&#160;</div></div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>          <div style='background-color:transparent;'>
    <div style='Margin: 0 auto;min-width: 320px;max-width: 745px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #044E7A;' class='block-grid three-up'>
    <div style='border-collapse: collapse;display: table;width: 100%;background-color:#044E7A;'>
    <div class='col num4' style='max-width: 320px;min-width: 248px;display: table-cell;vertical-align: top;'>
    <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'>
    <div align='center' style='padding-right: 0px; padding-left: 0px; padding-bottom: 0px;'>
    <div style='line-height:15px;font-size:1px'>&#160;</div>
    <div style='display: table; max-width:131px;'>
    <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px'>
    <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
    <a href='https://www.facebook.com/' title='Facebook' target='_blank'>
    <img src='cid:facebook@2x' alt='Facebook' title='Facebook' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
    </a>
    <div style='line-height:5px;font-size:1px'>&#160;</div>
    </td></tr>
    </tbody></table>
    <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px'>
    <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
    <a href='https://twitter.com/' title='Twitter' target='_blank'>
    <img src='cid:twitter@2x' alt='Twitter' title='Twitter' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
    </a>
    <div style='line-height:5px;font-size:1px'>&#160;</div>
    </td></tr>
    </tbody></table>
    <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 0'>
    <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
    <a href='https://plus.google.com/' title='Google+' target='_blank'>
    <img src='cid:googleplus@2x' alt='Google+' title='Google+' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
    </a>
    <div style='line-height:5px;font-size:1px'>&#160;</div>
    </td></tr>
    </tbody></table>    
    </div>
    </div>                        
    </div>
    </div>
    </div>      
    <div class='col num4' style='max-width: 320px;min-width: 248px;display: table-cell;vertical-align: top;'>
    <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
    <div style='font-family: Tahoma, sans-serif;line-height:120%;color:#a8bf6f; padding-right: 0px; padding-left: 0px; padding-top: 20px; padding-bottom: 0px;'>	
    <div style='font-size:12px;line-height:14px;color:#a8bf6f;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px;text-align: center'><span style='color: rgb(255, 255, 255); font-size: 12px; line-height: 14px;'><span style='font-size: 12px; line-height: 14px; color: rgb(168, 191, 111);'>Cel.: $cel</span></span><br></p></div>	
    </div>
    </div>
    </div>
    </div>
    <div class='col num4' style='max-width: 320px;min-width: 248px;display: table-cell;vertical-align: top;'>
    <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
    <div style='font-family: Tahoma, sans-serif;line-height:120%;color:#a8bf6f; padding-right: 0px; padding-left: 0px; padding-top: 20px; padding-bottom: 0px;'>	
    <div style='font-size:12px;line-height:14px;color:#a8bf6f;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px;text-align: center'>Email <span style='color: rgb(255, 255, 255); font-size: 12px; line-height: 14px;'>$correo</span></p></div>	
    </div>                        
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>          <div style='background-color:transparent;'>
    <div style='Margin: 0 auto;min-width: 320px;max-width: 745px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
    <div style='border-collapse: collapse;display: table;width: 100%;background-color:transparent;'>                   
    <div class='col num12' style='min-width: 320px;max-width: 745px;display: table-cell;vertical-align: top;'>
    <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
    <div style='padding-right: 30px; padding-left: 30px; padding-top: 30px; padding-bottom: 30px;'>
    <div align='center'><div style='border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;'>&#160;</div></div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>  
    </td>
    </tr>
    </tbody>
    </table>
    </body></html>
    ";
                ///$mail->addAddress("pvasquez@tracklink.pe");
                $mail->addAddress($ejecutivoOpec->usuario, $ejecutivoOpe);
                $mail->addCC($ejecutivoVenta->usuario, $ejecutivo);
                $mail->addCC($usuarioSolicitadorusuarioEje->usuario);
                $mail->addCC("sac@tracklink.pe");
                $mail->addCC("nretamozo@tracklink.pe");
                // $mail->addCC("llazaro@tracklink.pe");
                $mail->addCC("jtenorio@tracklink.pe");
				$mail->addCC("llazaro@tracklink.pe");
                $mail->Subject = 'Reprogramar fecha de instalación de cliente ' . $datosCliente . ' orte:  ' . $orte;
                $mail->msgHTML($content);
                $mail->AltBody = 'Estimado(a) ' . $ejecutivoOpe . ' reprogramar la instalación';
                if (!$mail->send()) {
                    echo "Mailer Error: " . $mail->ErrorInfo;
                } else {
                    //echo "enviado";
                }
                //CORREO
                $echo[] = "Reprogramador por  " . $_POST['motivoReprogramacion'];
            } else {
                $errores[] = "Error en reprogramacion";
            }
        }

        if (isset($_POST['motivoReprogramacionChequeo'])) {
            $idoperacion = $_POST['idoperacion'];
            $chequeoObj->observacion = $_POST['motivoReprogramacionChequeo'];
            $operacionObj->idoperacion = $idoperacion;
            $tablaObj->idoperacion = $idoperacion;
            $tablaObj->borrarEstacioOperacion();
            $operacionObj->observacion = $_POST['motivoReprogramacionChequeo'];
            $ObjetoOperacion = Operacion::obtenerOperacionPorId($idoperacion);
            $operacionObj->fechaInstalacion = $ObjetoOperacion->fechaInstalacion;
            $operacionObj->estado = $ObjetoOperacion->estado;
            $operacionObj->guarDarIncidencia();
            $chequeoObj->idchequeo = $ObjetoOperacion->idChequeo;
            $chequeoObj->observacion = $_POST['motivoReprogramacionChequeo'];
            $calendarioObj->idoperacion = $idoperacion;
            if ($chequeoObj->marcarParaReprogramado()) {
                //cambiar reprogramar chequeo solo el estado no crear nuevo reistro ... en todos
                if ($ObjetoOperacion->idlocation == 0) {
                    $vehiculoObj = Vehiculo::obtenerVehiculo($ObjetoOperacion->idvehiculo);
                    $clienteObj = Cliente::obtenerClientePorVehiculo($ObjetoOperacion->idvehiculo, $ObjetoOperacion->idcliente);
                } else {
                    $clienteObj = Onix::obtenerDatosDesdeIds($ObjetoOperacion->idcliente, $ObjetoOperacion->idvehiculo, $ObjetoOperacion->idlocation);
                    $chequeoObj = Chequeos::obtenerChequeoTrackPorId($ObjetoOperacion->idChequeo);
                    $vehiculoObj = false;
                }
                if ($vehiculoObj) {
                    $datosCliente = (!empty($clienteObj->apellidosC)) ? $clienteObj->apellidosC . " " . $clienteObj->nombre : $clienteObj->razonSocial;
                } else {
                    $apellidos = utf8_encode($clienteObj->apellidos);
                    $datosCliente = (!empty($clienteObj->nombre)) ? utf8_encode($clienteObj->nombre) . " " . "$apellidos" : "$apellidos";
                }
                $usuarioSolicitadorusuarioEje = Usuario::obtenerUsuario($session->idusuario);
                $creador = Usuario::obtenerUsuario($ObjetoOperacion->idCreador);
                $motivo = $_POST['motivoReprogramacionChequeo'];
                $orte = $ObjetoOperacion->numeroOrte;
                $trabajo = $ObjetoOperacion->trabajo;
                $lugar = $ObjetoOperacion->lugarInstalacion;
                $fecha = $ObjetoOperacion->fechaInstalacion;
                $usuarioSolicitador = $usuarioSolicitadorusuarioEje->usuarioNombre . " " . $usuarioSolicitadorusuarioEje->usuarioApellidoPaterno;
                $cel = $usuarioSolicitadorusuarioEje->celular;
                $correo = $usuarioSolicitadorusuarioEje->usuario;
                $ejecutivo = $creador->usuarioNombre . " " . $creador->usuarioApellidoPaterno;
                $quienCambia = 'La reprogramación de fecha se realiza por el asesor de central de emergencia quien lo solicito <b>' . $ejecutivo . "</b> <br> Esta solicitud sera registrado como una incidencia";
                //CORREO
                $content = "
    <html xmlns='http://www.w3.org/1999/xhtml' xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office'><head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <meta name='viewport' content='width=device-width'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>    
    <style type='text/css' id='media-query'>
    body {
    margin: 0;
    padding: 0; }
    table, tr, td {
    vertical-align: top;
    border-collapse: collapse; }
    .ie-browser table, .mso-container table {
    table-layout: fixed; }
    * {
    line-height: inherit; }
    a[x-apple-data-detectors=true] {
    color: inherit !important;
    text-decoration: none !important; }
    .img-container div,  .img-container button {
    display: block !important; }
    .fullwidth button {
    width: 100% !important; }
    .block-grid .col {
    display: table-cell;
    float: none !important;
    vertical-align: top; }
    .ie-browser .num12, .ie-browser .block-grid,  .num12,  .block-grid {
    width: 745px !important; }
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
    line-height: 100%; }
    .ie-browser .mixed-two-up .num4,  .mixed-two-up .num4 {
    width: 248px !important; }
    .ie-browser .mixed-two-up .num8,  .mixed-two-up .num8 {
    width: 496px !important; }
    .ie-browser .block-grid.two-up .col,  .block-grid.two-up .col {
    width: 372px !important; }
    .ie-browser .block-grid.three-up .col,  .block-grid.three-up .col {
    width: 248px !important; }
    .ie-browser .block-grid.four-up .col,  .block-grid.four-up .col {
    width: 186px !important; }
    .ie-browser .block-grid.five-up .col,  .block-grid.five-up .col {
    width: 149px !important; }
    .ie-browser .block-grid.six-up .col,  .block-grid.six-up .col {
    width: 124px !important; }
    .ie-browser .block-grid.seven-up .col,  .block-grid.seven-up .col {
    width: 106px !important; }
    .ie-browser .block-grid.eight-up .col,  .block-grid.eight-up .col {
    width: 93px !important; }
    .ie-browser .block-grid.nine-up .col,  .block-grid.nine-up .col {
    width: 82px !important; }
    .ie-browser .block-grid.ten-up .col,  .block-grid.ten-up .col {
    width: 74px !important; }
    .ie-browser .block-grid.eleven-up .col,  .block-grid.eleven-up .col {
    width: 67px !important; }
    .ie-browser .block-grid.twelve-up .col,  .block-grid.twelve-up .col {
    width: 62px !important; }
    @media only screen and (min-width: 765px) {
    .block-grid {
    width: 745px !important; }
    .block-grid .col {
    vertical-align: top; }
    .block-grid .col.num12 {
    width: 745px !important; }
    .block-grid.mixed-two-up .col.num4 {
    width: 248px !important; }
    .block-grid.mixed-two-up .col.num8 {
    width: 496px !important; }
    .block-grid.two-up .col {
    width: 372px !important; }
    .block-grid.three-up .col {
    width: 248px !important; }
    .block-grid.four-up .col {
    width: 186px !important; }
    .block-grid.five-up .col {
    width: 149px !important; }
    .block-grid.six-up .col {
    width: 124px !important; }
    .block-grid.seven-up .col {
    width: 106px !important; }
    .block-grid.eight-up .col {
    width: 93px !important; }
    .block-grid.nine-up .col {
    width: 82px !important; }
    .block-grid.ten-up .col {
    width: 74px !important; }
    .block-grid.eleven-up .col {
    width: 67px !important; }
    .block-grid.twelve-up .col {
    width: 62px !important; } }
    @media (max-width: 765px) {
    .block-grid, .col {
    min-width: 320px !important;
    max-width: 100% !important;
    display: block !important; }
    .block-grid {
    width: calc(100% - 40px) !important; }
    .col {
    width: 100% !important; }
    .col > div {
    margin: 0 auto; }
    img.fullwidth, img.fullwidthOnMobile {
    max-width: 100% !important; } }
    </style>
    </head>
    <body class='clean-body' style='margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #E8EDEF'>
    <style type='text/css' id='media-query-bodytag'>
    @media (max-width: 765px) {
    .block-grid {
    min-width: 320px!important;
    max-width: 100%!important;
    width: 100%!important;
    display: block!important;
    }
    .col {
    min-width: 320px!important;
    max-width: 100%!important;
    width: 100%!important;
    display: block!important;
    }
    .col>div {
    margin: 0 auto;
    }
    img.fullwidth {
    max-width: 100%!important;
    }
    img.fullwidthOnMobile {
    max-width: 100%!important;
    }
    }
    </style>
    <table class='nl-container' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #E8EDEF;width: 100%' cellpadding='0' cellspacing='0'>
    <tbody>
    <tr style='vertical-align: top'>
    <td style='word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding: 0'>
            <div style='background-color:transparent;'>
    <div style='Margin: 0 auto;min-width: 320px;max-width: 745px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;' class='block-grid '>
    <div style='border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;'>
    <div class='col num12' style='min-width: 320px;max-width: 745px;display: table-cell;vertical-align: top;'>
    <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>

    <div style='font-family: Tahoma, sans-serif;line-height:120%;color:#0D0D0D; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>	
    <div style='font-size:12px;line-height:14px;color:#0D0D0D;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 17px;text-align: center'><span style='font-size: 28px; line-height: 33px;'><strong><span style='line-height: 33px; font-size: 28px;'>Reprogramar fecha y hora de la orte $orte</span></strong></span><br><span style='font-size: 28px; line-height: 33px;'>$motivo.</span></p></div>	
    </div>
    <div style='font-size: 16px;font-family: Tahoma, sans-serif; text-align: center;'>
    <style>
    .tabl{
    border: 0px solid #ccc;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    width: 100%;
    table-layout: fixed;
    }
    .capt{
    font-size: 1.5em;
    margin: .5em 0 .75em;
    }
    .tr {
    background: #fff;
    border: 1px solid #ddd;
    padding: .35em;
    }
    .thtd {
    padding: .625em;
    text-align: center;
    }
    .th {
    font-size: .85em;
    letter-spacing: .1em;
    text-transform: uppercase;
    }
    @media screen and (max-width: 600px) {
    #tabl {
    border: 0;
    }
    .thead {
    border: none;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
    }
    .trm {
    border-bottom: 3px solid #ddd;
    display: block;
    margin-bottom: .625em;
    }
    .tdm{
    border-bottom: 1px solid #ddd;
    display: block;
    font-size: .8em;
    text-align: center;
    }
    .tdb:before {
    content: attr(data-label);
    float: left;
    font-weight: bold;
    text-transform: uppercase;
    }
    .tdl:last-child {
    border-bottom: 0;
    }
    .catpm {
    font-size: 1.3em;
    }
    }</style>  
    <table class='tabl'>
    <thead class='thead'>
    <tr class='tr trm'>
    <th class='thtd th'>Trabajo</th>
    <th class='thtd th'>Lugar</th>
    <th class='thtd th'>Cliente</th>
    <th class='thtd th'>Fecha</th>
    </tr>
    </thead>
    <tbody>
    <tr class='tr trm'>
    <td data-label='Trabajo:' class='thtd th tdm tdb tdl'>$trabajo</td>
    <td data-label='Lugar:' class='thtd th tdm tdb tdl'>$lugar</td>
    <td data-label='Cliente:' class='thtd th tdm tdb tdl'>$datosCliente</td>
    <td data-label='Fecha:' class='thtd th tdm tdb tdl'>$fecha</td>
    </tr>
    </tbody>
    </table>  
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>          <div style='background-color:transparent;'>
    <div style='Margin: 0 auto;min-width: 320px;max-width: 745px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;' class='block-grid '>
    <div style='border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;'>                         
    <div class='col num12' style='min-width: 320px;max-width: 745px;display: table-cell;vertical-align: top;'>
    <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
    <div align='center' class='img-container center  autowidth ' style='padding-right: 0px;  padding-left: 0px;'>
    <img class='center  autowidth ' align='center' border='0' src='cid:divider' alt='div' title='Image' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 316px' width='316'>
    </div>
    <div style='font-family: Tahoma, sans-serif;line-height:150%;color:#555555; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>	
    <div style='font-size:12px;line-height:18px;color:#555555;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 21px;text-align: center'>Cambio solicitado por: <span style='color: rgb(168, 191, 111); font-size: 14px; line-height: 21px;'><strong>$usuarioSolicitador<br></strong></span></p></div>	
    </div>

    <div style='font-family: Tahoma, sans-serif;line-height:150%;color:#0D0D0D; padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 10px;'>	
    <div style='font-size:12px;line-height:18px;color:#0D0D0D;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 21px;text-align: center'>$quienCambia .</p></div>	
    </div>
    <div align='center' class='button-container center' style='padding-right: 10px; padding-left: 10px; padding-top:25px; padding-bottom:10px;'>
    <a style='color: #ffffff; background-color: #044E7A; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 181px; width: 151px;width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent; padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px; font-family:  Tahoma, sans-serif; text-align: center; mso-border-alt: none;  text-decoration: none' href='http://resh.tracklink.pe/chequeos' target='_blank'  >
    <span style='font-size:16px;line-height:32px;'>Realizar el cambio</span>
    </a>
    </div>
    <div style='padding-right: 10px; padding-left: 10px; padding-top: 30px; padding-bottom: 10px;'>
    <div align='center'><div style='border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;'>&#160;</div></div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>          <div style='background-color:transparent;'>
    <div style='Margin: 0 auto;min-width: 320px;max-width: 745px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #044E7A;' class='block-grid three-up'>
    <div style='border-collapse: collapse;display: table;width: 100%;background-color:#044E7A;'>
    <div class='col num4' style='max-width: 320px;min-width: 248px;display: table-cell;vertical-align: top;'>
    <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'>
    <div align='center' style='padding-right: 0px; padding-left: 0px; padding-bottom: 0px;'>
    <div style='line-height:15px;font-size:1px'>&#160;</div>
    <div style='display: table; max-width:131px;'>
    <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px'>
    <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
    <a href='https://www.facebook.com/' title='Facebook' target='_blank'>
    <img src='cid:facebook@2x' alt='Facebook' title='Facebook' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
    </a>
    <div style='line-height:5px;font-size:1px'>&#160;</div>
    </td></tr>
    </tbody></table>
    <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px'>
    <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
    <a href='https://twitter.com/' title='Twitter' target='_blank'>
    <img src='cid:twitter@2x' alt='Twitter' title='Twitter' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
    </a>
    <div style='line-height:5px;font-size:1px'>&#160;</div>
    </td></tr>
    </tbody></table>
    <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 0'>
    <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
    <a href='https://plus.google.com/' title='Google+' target='_blank'>
    <img src='cid:googleplus@2x' alt='Google+' title='Google+' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
    </a>
    <div style='line-height:5px;font-size:1px'>&#160;</div>
    </td></tr>
    </tbody></table>    
    </div>
    </div>                        
    </div>
    </div>
    </div>      
    <div class='col num4' style='max-width: 320px;min-width: 248px;display: table-cell;vertical-align: top;'>
    <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
    <div style='font-family: Tahoma, sans-serif;line-height:120%;color:#a8bf6f; padding-right: 0px; padding-left: 0px; padding-top: 20px; padding-bottom: 0px;'>	
    <div style='font-size:12px;line-height:14px;color:#a8bf6f;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px;text-align: center'><span style='color: rgb(255, 255, 255); font-size: 12px; line-height: 14px;'><span style='font-size: 12px; line-height: 14px; color: rgb(168, 191, 111);'>Cel.: $cel</span></span><br></p></div>	
    </div>
    </div>
    </div>
    </div>
    <div class='col num4' style='max-width: 320px;min-width: 248px;display: table-cell;vertical-align: top;'>
    <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
    <div style='font-family: Tahoma, sans-serif;line-height:120%;color:#a8bf6f; padding-right: 0px; padding-left: 0px; padding-top: 20px; padding-bottom: 0px;'>	
    <div style='font-size:12px;line-height:14px;color:#a8bf6f;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px;text-align: center'>Email <span style='color: rgb(255, 255, 255); font-size: 12px; line-height: 14px;'>$correo</span></p></div>	
    </div>                        
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>          <div style='background-color:transparent;'>
    <div style='Margin: 0 auto;min-width: 320px;max-width: 745px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
    <div style='border-collapse: collapse;display: table;width: 100%;background-color:transparent;'>                   
    <div class='col num12' style='min-width: 320px;max-width: 745px;display: table-cell;vertical-align: top;'>
    <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
    <div style='padding-right: 30px; padding-left: 30px; padding-top: 30px; padding-bottom: 30px;'>
    <div align='center'><div style='border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;'>&#160;</div></div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>  
    </td>
    </tr>
    </tbody>
    </table>
    </body></html>
    ";
                //$mail->addAddress("pvasquez@tracklink.pe");
                $mail->addAddress($creador->usuario, $creador->usuarioNombre . " " . $creador->usuarioApellidoPaterno);
                $mail->addCC($usuarioSolicitadorusuarioEje->usuario, $usuarioSolicitadorusuarioEje->usuarioNombre . " " . $usuarioSolicitadorusuarioEje->usuarioApellidoPaterno);
                $mail->addCC("sac@tracklink.pe");
                $mail->addCC("aquichiz@tracklink.pe");
                //$mail->addCC("llazaro@tracklink.pe");
                $mail->Subject = 'Reprogramar fecha de ' . $trabajo . ' de cliente ' . strtolower($datosCliente) . ' orte:  ' . $orte;
                $mail->msgHTML($content);
                $mail->AltBody = 'Estimado(a) ' . $ejecutivo . ' reprogramar la instalación';
                if (!$mail->send()) {
                    echo "Mailer Error: " . $mail->ErrorInfo;
                } else {
                    //echo "enviado";
                }
                //CORREO
                if (!$operacionObj->marcarComoReprogramado()) {
                    $errores[] = 'Error al marcar como repromado';
                } else {
                    $echo[] = "Reprogramador por  " . $_POST['motivoReprogramacionChequeo'];
                }
            }
        }


        if (isset($_POST['idoperacion'])) {
            $operacionObj->idoperacion = $_POST['idoperacion'];
        }

        if (isset($_POST['idtecnico'])) {
            $operacionObj->idtecnicoAsig = $_POST['idtecnico'];
        }

        $Work="";
        if(isset($_POST['trabajoO'])){
                 $Work=$_POST['trabajoO'];
        }


        if (isset($_POST['datosfacturacionid'])) {
            $operacionObj->datosfacturacionid = $_POST['datosfacturacionid'];
        }

        $status=false;

        if($Work==="Instalacion" && !isset($_POST['motivoReprogramacion'])) {
            $orte=$_POST['orte'];
            $orden=$_POST['ordenContrat'];
            $ordenFact=$_POST['numOrdenFact'];
            $status=true;
        }

        if(($Work==="Desinstalacion" || $Work==="Reinstalacion" || $Work==="Chequeo por monitoreo") && !isset($_POST['motivoReprogramacionChequeo'])) {
            $orte = $_POST['numeroOrte'];
            $ordenFact = $_POST['numeroOrden'];
            $orden="";
            $status=true;
        }
        if($status){
            $operacionObj->numeroOrte=$orte;
            $operacionObj->ordenContrato=$orden;
            $operacionObj->numeroOrden=$ordenFact;

            $ObjetoOperacion=Operacion::obtenerOperacionPorId($operacionObj->idoperacion);
            if($ObjetoOperacion->idtecnicoAsig!=$operacionObj->idtecnicoAsig && $ObjetoOperacion->idtecnicoAsig!=""){
                if($Work==="Instalacion") {
                    $ordContr = explode("-", $ObjetoOperacion->ordenContrato);
                    $infoOpera = Operacion::obtenerInfoClienteOperacion($ObjetoOperacion->numeroOrte, $ordContr[1]);
                }else{
                    $Nordn = explode("-", $ObjetoOperacion->numeroOrden);
                    $infoOpera = Operacion::obtenerInfoClienteOperacion($ObjetoOperacion->numeroOrte, $Nordn[1]);
                }
                $datTecNew = Operacion::obtenerTecnico($_POST['idtecnico']);
                $datTec = Operacion::obtenerTecnico($ObjetoOperacion->idtecnicoAsig);

                $htmlContenido1="Estimado ".$datTec->nomtec." la Orte ".$ObjetoOperacion->numeroOrte." del Cliente ". $infoOpera->lastname . " " . $infoOpera->firstname." <br><br> es Asignada para el Técnico ".$datTecNew->nomtec."<br><br>Por Favor No tener en cuenta al Cliente y Orte en mención";
                $mail2 = new PHPMailer;
                $mail2->isSMTP();
                $mail2->SetLanguage('es', '../mailphp/PHPMailer/language/');
                $mail2->SMTPDebug = 0;
                $mail2->Debugoutput = 'html';
                $mail2->Host = gethostbyname('mail.tracklink.pe');
                $mail2->Port = 25;
                $mail2->SMTPAuth = false;
                $mail2->CharSet = 'UTF-8';
                $mail2->setFrom('no-responder@tracklink.pe', 'Resh');
                $mail2->addAddress($datTec->correotec);
                $mail2->addCC("sac@tracklink.pe");
                $mail2->addCC("rgomez@tracklink.pe");
                $mail2->addCC("jnaupari@tracklink.pe");
                $mail2->addCC("cchavez@tracklink.pe");
                $mail2->addCC("jtenorio@tracklink.pe");
				$mail2->addCC("llazaro@tracklink.pe");
                $mail2->addCC("nretamozo@tracklink.pe");
                $mail2->addBCC("pruebas-ti@tracklink.pe");
                $mail2->Subject = "REASIGNACIÓN DE TECNICO ORTE ".$ObjetoOperacion->numeroOrte." ";
                $mail2->msgHTML($htmlContenido1);
                $mail2->AltBody = '';
                if (!$mail2->send()) {
                    echo "Mailer Error: " . $mail2->ErrorInfo;
                } else {

                }
            }

            if($Work==="Instalacion") {

                if($ObjetoOperacion->idtecnicoAsig!=$operacionObj->idtecnicoAsig || $ObjetoOperacion->ordenContrato!=$operacionObj->ordenContrato || $ObjetoOperacion->numeroOrte!=$operacionObj->numeroOrte){
                    /*enviar correo a tecnico asignado*/
                    $mail3 = new PHPMailer;

                    ob_start();
                    $datTec = Operacion::obtenerTecnico($operacionObj->idtecnicoAsig);
                    $desctrab = $Work;
                    $ordContrOld = explode("-",$ObjetoOperacion->ordenContrato);
                    $ordContr = explode("-",$operacionObj->ordenContrato);

                    $infoOperaOld = Operacion::obtenerInfoClienteOperacion($ObjetoOperacion->numeroOrte, $ordContrOld[1]);

                    $infoOpera = Operacion::obtenerInfoClienteOperacion($operacionObj->numeroOrte, $ordContr[1]);

                    $asunto=$Work . "  a  " . $infoOpera->lastname . " " . $infoOpera->firstname . "  Orte : " . $operacionObj->numeroOrte;
                    if($ObjetoOperacion->idtecnicoAsig==$operacionObj->idtecnicoAsig){
                        $asunto=" Actualizacion de datos :  ".$Work . "  a  " . $infoOperaOld->lastname . " " . $infoOperaOld->firstname . "  Orte : " . $ObjetoOperacion->numeroOrte;
                        $cambio = "ok" ;
                    }


                    include("../assets/email_templates/tencnicoCorreo/UpdatemailTecnicoOperacion.php");
                    $mail3->isSMTP();
                    $mail3->SetLanguage('es', '../mailphp/PHPMailer/language/');
                    $mail3->SMTPDebug = 0;
                    $mail3->Debugoutput = 'html';
                    $mail3->Host = gethostbyname('mail.tracklink.pe');
                    $mail3->Port = 25;
                    $mail3->SMTPAuth = false;
                    $mail3->CharSet = 'UTF-8';
                    $mail3->setFrom('no-responder@tracklink.pe', 'Resh');
                    $mail3->addAddress($datTec->correotec);
 	                $mail3->addCC("sac@tracklink.pe");
                    $mail3->addCC("rgomez@tracklink.pe");
                    $mail3->addCC("jnaupari@tracklink.pe");
                    $mail3->addCC("cchavez@tracklink.pe");
                    $mail3->addCC("jtenorio@tracklink.pe");
                    $mail3->addCC("llazaro@tracklink.pe");
                    $mail3->addCC("nretamozo@tracklink.pe");
                    $mail3->addBCC("pruebas-ti@tracklink.pe");
                    $mail3->Subject = $asunto;
                    $content = ob_get_clean();
                    $mail3->msgHTML($content);
                    $mail3->AltBody = '';
                    if (!$mail3->send()) {
                        echo "Mailer Error: " . $mail3->ErrorInfo;
                    } else {

                    }
                }

                $facturacionObj->orden=$ordenFact;
                $facturacionObj->updateOrdenFact();
            }else{
                if($ObjetoOperacion->idtecnicoAsig!=$operacionObj->idtecnicoAsig || $ObjetoOperacion->numeroOrden!=$operacionObj->numeroOrden || $ObjetoOperacion->numeroOrte!=$operacionObj->numeroOrte){
                    /*enviar correo a tecnico asignado*/
                    $mail4 = new PHPMailer;

                    ob_start();
                    $datTec = Operacion::obtenerTecnico($operacionObj->idtecnicoAsig);
                    $desctrab = $Work;
                    $ordContrOld = explode("-",$ObjetoOperacion->numeroOrden);
                    $ordContr = explode("-",$operacionObj->numeroOrden);

                    $infoOperaOld = Operacion::obtenerInfoClienteOperacion($ObjetoOperacion->numeroOrte, $ordContrOld[1]);

                    $infoOpera = Operacion::obtenerInfoClienteOperacion($operacionObj->numeroOrte, $ordContr[1]);

                    $asunto=$Work . "  a  " . $infoOpera->lastname . " " . $infoOpera->firstname . "  Orte : " . $operacionObj->numeroOrte;
                    if($ObjetoOperacion->idtecnicoAsig==$operacionObj->idtecnicoAsig){
                        $asunto=" Actualizacion de datos :  ".$Work . "  a  " . $infoOperaOld->lastname . " " . $infoOperaOld->firstname . "  Orte : " . $ObjetoOperacion->numeroOrte;
                        $cambio = "ok" ;
                    }


                    include("../assets/email_templates/tencnicoCorreo/UpdatemailTecnicoOperacion.php");
                    $mail4->isSMTP();
                    $mail4->SetLanguage('es', '../mailphp/PHPMailer/language/');
                    $mail4->SMTPDebug = 0;
                    $mail4->Debugoutput = 'html';
                    $mail4->Host = gethostbyname('mail.tracklink.pe');
                    $mail4->Port = 25;
                    $mail4->SMTPAuth = false;
                    $mail4->CharSet = 'UTF-8';
                    $mail4->setFrom('no-responder@tracklink.pe', 'Resh');
                    $mail4->addAddress($datTec->correotec);
                    $mail4->addCC("sac@tracklink.pe");
                    $mail4->addCC("rgomez@tracklink.pe");
                    $mail4->addCC("jnaupari@tracklink.pe");
                    $mail4->addCC("cchavez@tracklink.pe");
                    $mail4->addCC("llazaro@tracklink.pe");
                    $mail4->addCC("nretamozo@tracklink.pe");                
                    $mail4->addCC("jtenorio@tracklink.pe");
                    $mail4->addBCC("pruebas-ti@tracklink.pe");
                    $mail4->Subject = $asunto;
                    $content = ob_get_clean();
                    $mail4->msgHTML($content);
                    $mail4->AltBody = '';
                    if (!$mail4->send()) {
                        echo "Mailer Error: " . $mail4->ErrorInfo;
                    } else {

                    }
                }

            }


            $operacionObj->updateOrdenFactContraOpera();
            $echo[] = "Operación Actualizada exitosamente";
        }

    }else{
        $errores[]="El formulario ya fue procesado, verificar.";
    }
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->registrarOperaciones=="si"): ?>
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
   <style>
    .widget p {
        display: inline-block;        
    }
    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }
    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }
    .reloj .caja-segundos {
        display: inline-block;
    }
    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
}
.pagination1{margin:0;padding:0;}
.pagination1 li{
    display: inline;
    padding: 6px 10px 6px 10px;
    border: 1px solid #ddd;
    margin-right: -1px;
    font: 15px/20px Arial, Helvetica, sans-serif;
    background: #FFFFFF;
    box-shadow: inset 1px 1px 5px #F4F4F4;
}
.pagination1 li a{
    text-decoration:none;
    color: rgb(89, 141, 235);
}
.pagination1 li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination1 li.last {
    border-radius: 0px 5px 5px 0px;
}
.pagination1 li:hover{
    background: #CFF;
}
.pagination1 li.active{
    background: #F0F0F0;
    color: #333;
}
</style>
 <body>
<div class="se-pre-con"></div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <?php include("../includes/menuIzquierdo.php") ?>
               </div>
           </nav>
           <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>
                </nav>
            </div>
            <div class="wrapper wrapper-content">
                <div class="row ibox float-e-margins ibox-content ">
                            <form action="pag/paginacionAdmOpReporteEcxel" method="post">
                    <div class="col-md-12">
                        <div class="row">
                        <div class="col-md-2">  
                            <label for="tipo" class="">Tipo </label>
                                <select  style="border: none;" class="corredor border-bottom form-control text-capitalize" name="tipo" id="tipo">          
                                        <option value="36" selected="">Instalaciones listo CEM, con Orte</option>
                                </select> 
                        </div>
                        <div class="col-md-4">
                            </div>
                            </form>
                              <div class="col-md-2">
                        </div>
                        <div class="col-md-4 ">
                           <div class="widget" style="line-height: 0px;       
                           padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px">
                           <div class="fecha">
                            <p id="diaSemana" class="diaSemana">Martes</p>
                            <p id="dia" class="dia">27</p>
                            <p>de </p>
                            <p id="mes" class="mes">Octubre</p>
                            <p>del </p>
                            <p id="year" class="year">2015</p>
                        </div>
                        <div class="reloj">
                            <p id="horas" class="horas">11</p>
                            <p>:</p>
                            <p id="minutos" class="minutos">48</p>
                            <p>:</p>
                            <div class="caja-segundos">
                                <p id="ampm" class="ampm">AM</p>
                                <p id="segundos" class="segundos">12</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox-content" id="resultados">
        <input type="text" class="form-control input-sm m-b-xs" id="filter"
        placeholder="Buscar" >
        <div class="table-responsive">
        <table class="table table-condensed  table-hover" >
            <thead>
                <tr>                   
                    <th># ORTE</th>
                    <th>Trabajo</th>
                    <th>Lugar</th>
                    <th>Fecha Trabajo</th>
                    <th>Cliente</th>
                    <th>Placa</th>
                    <th>Chasis</th>
                    <th>Campaña</th>
                    <th>Tipo Registro</th>
                    <th>Acción</th>
                </tr>
            </thead>  
            <tbody  id="resultado">          
            </tbody>  
        </table>
        </div>
    </div>
</div>
<?php include("../includes/footer.php") ?>
</div>
</div>
<div class="modal inmodal" id="bateriasOModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Detalles de trabajo</h4> 
                <small class="font-bold"><div class="form-group ">
                </div></small>
            </div>
            <form  role="form" method="post" >
                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="submit" name="GuardarCambios" class="btn btn-primary" value="<?php echo Token::generar() ?>">Guardar cambios</button>
                </div>
            </form>   
        </div>
    </div>
</div>
</body>
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/clipboard.min.js"></script>
<script src="../assets/calendario/dist/sweetalert.min.js"></script>
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
 <script src="../assets/js/jquery-confirm.min.js"></script>
<?php include_once('../includes/script.php') ?> 
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
$(document).ready(function(){
refrescar();
$("#resultado").on( "click", ".pagination1 a", function (e){
    e.preventDefault();        
    var page = $(this).attr("data-page"); 
    var tipo=$('#tipo').val();
    $.ajax({
    type : 'POST',
    url  : 'pag/paginacionAdmOp',
    data : {page:page,tipo:tipo},
    success : function(data)
    {
     $("#resultado").html(data);
    }
    });
});
$("#filter").bind("keyup focus",function()
    {   
        var tipo=$('#tipo').val();
        var datosS = $(this).val();        
        datosS = datosS.replace('<>/"', '');
        if(datosS.length >= 1)
            {
            $.ajax({
                    type : 'POST',
                    url  : 'pag/resutaldoBusquedaAdmiOp',
                    data :  {datos:datosS,tipo:tipo},
                    success : function(data)
                    {
                        $("#resultado").html(data);
                    }
                });
            return false;
        }else{
           refrescar() 
        }
    });
});
$("#tipo").change(function(){    
    var tipo=$(this).val();
    $.ajax({
    type : 'POST',
    url  : 'pag/paginacionAdmOp',
    data : {tipo:tipo},
    success : function(data)
    {
     $("#resultado").html(data);
    }
    });
});
function refrescar(){ 
  var page = $("#resultado").attr("data-page");         
    var tipo=$('#tipo').val();
    $.ajax({
    type : 'POST',
    url  : 'pag/paginacionAdmOp',
    data : {page:page,tipo:tipo},
    success : function(data)
    {
     $("#resultado").html(data);
    }
    });
}
$('#bateriasOModal').on('show.bs.modal', function (e) {
    var rowid = $(e.relatedTarget).data('id');
    $.ajax({
        type : 'post',
                url : 'modal/operacionesDataModalAdmin', 
                data :  'rowid='+ rowid, 
                success : function(data){
                $('.modal-body').html(data);
            }
        });
});
(function(){
    var actualizarHora = function(){
        // Obtenemos la fecha actual, incluyendo las horas, minutos, segundos, dia de la semana, dia del mes, mes y año;
        var fecha = new Date(),
        horas = fecha.getHours(),
        ampm,
        minutos = fecha.getMinutes(),
        segundos = fecha.getSeconds(),
        diaSemana = fecha.getDay(),
        dia = fecha.getDate(),
        mes = fecha.getMonth(),
        year = fecha.getFullYear();

        // Accedemos a los elementos del DOM para agregar mas adelante sus correspondientes valores
        var pHoras = document.getElementById('horas'),
        pAMPM = document.getElementById('ampm'),
        pMinutos = document.getElementById('minutos'),
        pSegundos = document.getElementById('segundos'),
        pDiaSemana = document.getElementById('diaSemana'),
        pDia = document.getElementById('dia'),
        pMes = document.getElementById('mes'),
        pYear = document.getElementById('year');

        
        // Obtenemos el dia se la semana y lo mostramos
        var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        pDiaSemana.textContent = semana[diaSemana];

        // Obtenemos el dia del mes
        pDia.textContent = dia;

        // Obtenemos el Mes y año y lo mostramos
        var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        pMes.textContent = meses[mes];
        pYear.textContent = year;

        // Cambiamos las hora de 24 a 12 horas y establecemos si es AM o PM

        if (horas >= 12) {
            horas = horas - 12;
            ampm = 'PM';
        } else {
            ampm = 'AM';
        }

        // Detectamos cuando sean las 0 AM y transformamos a 12 AM
        if (horas == 0 ){
            horas = 12;
        }

        // Si queremos mostrar un cero antes de las horas ejecutamos este condicional
        // if (horas < 10){horas = '0' + horas;}
        pHoras.textContent = horas;
        pAMPM.textContent = ampm;

        // Minutos y Segundos
        if (minutos < 10){ minutos = "0" + minutos; }
        if (segundos < 10){ segundos = "0" + segundos; }

        pMinutos.textContent = minutos;
        pSegundos.textContent = segundos;
    };
    actualizarHora();
    var intervalo = setInterval(actualizarHora, 1000);
}())
</script>
   <script>
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
<?php else: ?> 
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?> 
<?php endif ?>

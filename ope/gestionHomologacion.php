<?php
    $nav="gestionHomologacion";
    include("../includes/head.php");
    if (!$session->yaEstaIngresado()) {
        irA("../ingresar");
    }
    require_once '../mailphp/PHPMailer/PHPMailerAutoload.php';
    $obMenu=Usuario::obtenerMenu($session->idusuario);
    $mail= new PHPMailer;
    $mail2= new PHPMailer;


if ($obMenu->verificarComercial=="si"):
        if (isset($_POST['guardarPreHomologacion'])) {
            if(Token::verificar($_POST['guardarPreHomologacion'])) {
                $obj = new homologation_client();
                $resul= false;
                $ttchk=$_POST['nfact'];

                for($i=0; $i < count($ttchk); $i++)
                {
                    $part= explode("_",$ttchk[$i]);
                    if($i==0) {
                        $obj->idlocation = $part[0];
                        $obj->idcustomer = $part[1];
                        $obj->idcompany = $part[2];
                        $obj->status = 'A';
                        $obj->token = limpiar(base64_encode(openssl_random_pseudo_bytes(32)));
                        $res2=$obj->verificarExisteCustomer($obj->idcompany,$obj->idlocation,$obj->idcustomer);
                        if($res2) {
                            $obj->idhomclient = $res2->idhomclient;
                            $resul=true;
                        } else {
                            $resul = $obj->registerHomologation();
                            $part2= explode("||",$ttchk[$i]);
                            $clientenom=explode("_",$part2[1]);
                            $mailClient=$part2[2];

                            $username = 'external';
                            $password = 'gcklotlqrxegsbtjsve';
                            $headers = array(
                                'Content-Type: application/json',
                                'Authorization: Basic '. base64_encode("$username:$password")
                            );

                            $bodyDatta = array(
                                "apellidos" => $clientenom[0],
                                "nombres" => $clientenom[1],
                                "linkHomologacion" => $obj->token,
                                "mail" => $mailClient
                            );

                            $resjje = API::POSTB("gateway-mc.tracklink.pe/uax/external/mailMcWelcomeTransp", $headers, $bodyDatta);

                        }
                    }
                    if($resul){
                        $obj_detail = new homologation_detail();
                        $obj_detail->idhomclient=$obj->idhomclient;
                        $obj_detail->idvehicle = $part[3];
                        $obj_detail->unitid = $part[4];
                        $obj_detail->status='108';
                        $obj_detail->statusActivacion = 'A';
                        $obj_detail->registerHomologation_detail();
                    }
                }
                $echo[]="Se Registro con exito ";
            } else {
                $errores[]="El formulario ya fue procesado, verificar.";
            }
        }
    else {
        if (isset($_POST['btnanul'])) {
            if (Token::verificar($_POST['btnanul'])) {
                $obj = new homologation_detail();
                $obj->idhomdetail = $_POST['idDetHomo'];
                $obj->statusActivacion = 'I';
                $obj->updateStatusDetHomologation();
                $echo[] = "se Anuló con exito";
            } else {
                $errores[] = "El formulario ya fue procesado, vericar.";
            }
        } if (isset($_POST['btnblink'])) {
            if (Token::verificar($_POST['btnblink'])) {
                $objcl = new homologation_client();

                $username = 'external';
                $password = 'gcklotlqrxegsbtjsve';
                $headers = array(
                    'Content-Type: application/json',
                    'Authorization: Basic '. base64_encode("$username:$password")
                );

                $dobjcl=$objcl->obtenerHomologationClient($_POST['idlinkHomo']);
                $objPer= $objcl->obtenerCliente($dobjcl->idlocation,$dobjcl->idcompany,$dobjcl->idcustomer);


                $bodyDatta = array(
                    "apellidos" => $objPer->lastname,
                    "nombres" => $objPer->firstname,
                    "linkHomologacion" => $dobjcl->token,
                    "mail" => $objPer->mail
                );

                $resjje = API::POSTB("gateway-mc.tracklink.pe/uax/external/mailMcWelcomeTransp", $headers, $bodyDatta);

                $echo[] = "se envió link para homologación";
            } else {
                $errores[] = "El formulario ya fue procesado, vericar.";
            }
        }else {
            if (isset($_POST['btnHomolgat'])) {
                if (Token::verificar($_POST['btnHomolgat'])) {
                    $tchk=0;
                    if (isset($_POST['check'])) {
                        $tchk=$_POST['check'];
                    }
                    for($ii=0; $ii < count($tchk); $ii++) {
                        $objd = new homologation_documents();
                        $objd->idhomdocument =$tchk[$ii];
                        $objd->checkimg = true;
                        $objd->updatecheckDocumentimg();
                    }

                    /*fotos - inicio*/
                    for($ii=0; $ii < count($tchk); $ii++) {
                        if (!empty($_FILES['file_img_'.$tchk[$ii]]['name'])) {
                            $nombreArchivo=$_FILES['file_img_'.$tchk[$ii]]['name'];
                            $nombreArchivo=str_replace(".rtf",".doc",$nombreArchivo);
                            $tmp=explode('.', $nombreArchivo);
                            $extencionArchivo=end($tmp);
                            $tamanioArchivo=$_FILES['file_img_'.$tchk[$ii]]['size'];
                            $ubicacionTemporal=$_FILES['file_img_'.$tchk[$ii]]['tmp_name'];

                            $fidhomdetail=$_POST["idhomdetail"];
                            $fidcustomer=$_POST["idcustomer"];
                            $fidlocation=$_POST["idlocation"];
                            $fidcompany=$_POST["idcompany"];
                            $fidvehicle=$_POST["idvehicle"];
                            $ffile=new CurlFile($_FILES['file_img_'.$tchk[$ii]]['tmp_name'], $_FILES['file_img_'.$tchk[$ii]]['type'], $_FILES['file_img_'.$tchk[$ii]]['name']);
                            $ftype=$_POST["ftype_".$tchk[$ii]];

                            $username = 'external';
                            $password = 'gcklotlqrxegsbtjsve';
                            $headers = array(
                                'Content-Type: multipart/form-data',
                                'Authorization: Basic '. base64_encode("$username:$password")
                            );
                            /*
                              $bodyDatta = array();
                            $bodydatta["idhomdetail"] = $fidhomdetail;
                            $bodydatta["idcustomer"] = $fidcustomer;
                            $bodydatta["idlocation"] = $fidlocation;
                            $bodydatta["idcompany"] = $fidcompany;
                            $bodydatta["idvehicle"] = $fidvehicle;
                            $bodydatta["vehicleImages.file"] = $ffile;
                            $bodydatta["vehicleImages.type"] = $ftype;
                             *

                            $bodyDatta = array(
                                "idhomdetail" => $fidhomdetail,
                                "idcustomer" =>$fidcustomer,
                                "idlocation" => $fidlocation,
                                "idcompany" =>$fidcompany,
                                "idvehicle" => $fidvehicle,
                                "vehicleImages.file" => $ffile,
                                "vehicleImages.type" => $ftype
                            );
                            var_dump($bodyDatta);
                            var_dump($_FILES['file_img_'.$tchk[$ii]]['name']);

                            $resjje = API::POSFD("gateway-mc.tracklink.pe/uax/external/mailMcWelcomeTransp", $headers, $bodyDatta);
                            */


                        }
                      //  echo 'file_img_'.$tchk[$ii].'no hay';
                    }
                   /* fotos - fin*/

                    if($_POST['cbostatus']=='107') {

                        $parametros = array(
                            'grant_type' => 'client_credentials'
                        );
                        $rs = API::Authentication("gateway-mc.tracklink.pe/uaa/oauth/token", "resh-connect", "yOFe8CtiBq0l0ZkbDD3v", $parametros);
                        $array = API::JSON_TO_ARRAY($rs);
                        $token = $array['access_token'];

                        $bodyDatta = array(
                            "costumerId" => $_POST['idcustomer'],
                            "locationId" => $_POST['idlocation'],
                            "firstName" => $_POST['firstname'],
                            "lastName" => $_POST['lastname'],
                            "phone" => $_POST['phonecell'],
                            "status" => "Activo",
                        );
                        $bodyDatta2 = array(
                            "unitId" => $_POST['unitid'],
                            "locationId" => $_POST['idlocation'],
                            "carBodyType" => $_POST['bodytype'],
                            "vehicleType" => $_POST['vehicleType'],
                            "capacity" => $_POST['capacity'],
                            "loadVolume" => $_POST['volume'],
                            "propertyCard" => "FOTO",
                            "travelInter" => ($_POST['travelinter'] == "si" ? true : false),
                            "status" => "Activo",
                            "vehiclePhotos" => []
                        );
                        $imgCheck=homologation_documents::getImagesCheckTrue($_POST['idhomdetail']);

                        $data_array = array();
                        foreach($imgCheck as $imgChk):
                            $data= array("type" => $imgChk->type,"name" => $imgChk->desctype,"photo" => ($imgChk->bucket."/".$imgChk->name), "status" => "Activo");
                            $data_array[]=$data;
                        endforeach;

                        $bodyDatta2["vehiclePhotos"]=$data_array;

                        $resjje = API::POST("gateway-mc.tracklink.pe/tst/transportist/approved", $token, $bodyDatta);

                        $resjje2 = API::POST("gateway-mc.tracklink.pe/tst/vehicle/approved/" . $_POST['idcustomer'] . "/" . $_POST['idlocation'], $token, $bodyDatta2);
                        $obj = new homologation_detail();
                        $obj->idhomdetail = $_POST['idhomdetail'];
                        $obj->status = '107';
                        $obj->updateStatDetHomologation();
                        /*correo
                        $clientenom="";
                        $placavhiculo=$_POST["placaVehiculo"];
                        */
                        $username = 'external';
                        $password = 'gcklotlqrxegsbtjsve';
                        $headers = array(
                            'Content-Type: application/json',
                            'Authorization: Basic '. base64_encode("$username:$password")
                        );

                        $bodyDatta = array(
                            "recipients" => array($_POST['correo']),
                            "subject" => "Micarga – Proceso de Homologación completada - Placa: ".$_POST['placaVehiculo'],
                            "nombres" => $_POST['lastname'],
                            "apellidos" => $_POST['firstname'],
                            "textMessage" => "<p><span> Finalizó el proceso de Homologación del vehículo :</span></p>
                                                   <table style='margin-left: auto;margin-right: auto;'> 
                                                   <tbody> 
                                                   <tr><td><strong style='color:rgb(30, 47, 65)'>Placa :</strong></td>
                                                   <td style='color: rgb(57, 56, 55)'>".$_POST['placaVehiculo']."</td></tr>
                                                   <tr><td><strong style='color:rgb(30, 47, 65)'>Chasis :</strong></td>
                                                   <td style='color: rgb(57, 56, 55)'>".$_POST['chasis']."</td></tr>
                                                   <tr><td><strong style='color:rgb(30, 47, 65)'>Marca :</strong></td>
                                                   <td style='color: rgb(57, 56, 55)'>".$_POST['marca']."</td></tr>
                                                   <tr><td><strong style='color:rgb(30, 47, 65)'>Modelo :</strong></td>
                                                   <td style='color: rgb(57, 56, 55)'>".$_POST['modelo']."</td></tr>
                                                   <tr><td><strong style='color:rgb(30, 47, 65)'>Año :</strong></td>
                                                   <td style='color: rgb(57, 56, 55)'>".$_POST['yearmodel']."</td></tr>
                                                   </tbody></table>
                                                   <p style='font-weight: bold;background: #17b6dc;color: white;display: table;margin-left: auto;
                                                   margin-right: auto;padding: 10px;border-radius: 50px;'>Descargar Micarga Transportista</p>
                                                   <p style='display: table;margin-left: auto;margin-right: auto;'>  
                                                   <a href='https://play.google.com/store/apps/details?id=pe.tracklink.micarga.transportist' style='float: left;'>
                                                   <img alt='' src='cid:googleplay' style='display: block;height:40px'/></a></p>
                                                   <p>Adjuntamos Manual de usuario para descarga y uso del aplicativo </p>
                                                   <p><span >Atentamente.</span></p><p><span >TRACKLINK</span></p>",
                            "template" => "micargaGeneral",
                            "attachDocument" => "Manual_App_Micarga_Transportista.pdf"
                        );

                        $resjje = API::POSTB("gateway-mc.tracklink.pe/uax/Homologation/sendEmailMC", $headers, $bodyDatta);



                        $echo[] = "Se Homologo Correctamente";
                    }else{
                        if($_POST['cbostatus']=='110') {
                            $obj = new homologation_detail();
                            $obj->idhomdetail = $_POST['idhomdetail'];
                            $obj->status = '110';
                            $obj->updateStatDetHomologation();

                            $username = 'external';
                            $password = 'gcklotlqrxegsbtjsve';
                            $headers = array(
                                'Content-Type: application/json',
                                'Authorization: Basic '. base64_encode("$username:$password")
                            );

                            $bodyDatta = array(
                                "recipients" => array($_POST['correo']),
                                "subject" => "Micarga – observación en proceso de homologación - Placa : ".$_POST['placaVehiculo'],
                                "nombres" => $_POST['lastname'],
                                "apellidos" => $_POST['firstname'],
                                "textMessage" => "<p><span> Hemos encontrado una observación en el proceso de Homologación del vehículo :</span></p>
                                                   <table style='margin-left: auto;margin-right: auto;'> 
                                                   <tbody> 
                                                   <tr><td><strong style='color:rgb(30, 47, 65)'>Placa :</strong></td>
                                                   <td style='color: rgb(57, 56, 55)'>".$_POST['placaVehiculo']."</td></tr>
                                                   <tr><td><strong style='color:rgb(30, 47, 65)'>Chasis :</strong></td>
                                                   <td style='color: rgb(57, 56, 55)'>".$_POST['chasis']."</td></tr>
                                                   <tr><td><strong style='color:rgb(30, 47, 65)'>Marca :</strong></td>
                                                   <td style='color: rgb(57, 56, 55)'>".$_POST['marca']."</td></tr>
                                                   <tr><td><strong style='color:rgb(30, 47, 65)'>Modelo :</strong></td>
                                                   <td style='color: rgb(57, 56, 55)'>".$_POST['modelo']."</td></tr>
                                                   <tr><td><strong style='color:rgb(30, 47, 65)'>Año :</strong></td>
                                                   <td style='color: rgb(57, 56, 55)'>".$_POST['yearmodel']."</td></tr>
                                                   </tbody></table><p>Lo estaremos contactando dentro de las siguientes 24 horas para revisar la información enviada.</p>
                                                   <p><span >Atentamente.</span></p><p><span >TRACKLINK</span></p>",
                                "template" => "micargaGeneral"
                            );

                            $resjje = API::POSTB("gateway-mc.tracklink.pe/uax/Homologation/sendEmailMC", $headers, $bodyDatta);

                            if($resjje)
                                $echo[] = "Se Guardo Como Pendiente de Fotos";
                        }
                    }

                } else {
                    $errores[] = "El formulario ya fue procesado, vericar.";
                }
            }
        }
    }
?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="../assets/css/sweetalert2.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/css2/style2.css">
<link rel="stylesheet" href="../assets/css2/style3.css">

<style>
    .table-cabecera-horario{
        background:#075598!important;
        color:white !important;
        vertical-align: middle!important;
    }
    .table-cabecera-horario2{
        background:#164764 !important;
        color:white !important;
        vertical-align: middle!important;
    }
    .table-cabecera-horario3{
        background: #096452 !important;
        color:white !important;
    }
    .table-cabecera-horario4{
        background: #0e2e79 !important;
        color:white !important;
    }

    .table-scroll {
        position:relative;
        max-width:1600px;
        margin:auto;
        overflow:hidden;
        /*border:.5px solid #000;*/
    }

    .table-wrap {
        width:100%;
        overflow:auto;
    }

    .table-scroll table {
        width:100%;
        margin:auto;
        border-collapse:separate;
        border-spacing:0;
    }

    .table-scroll th, .table-scroll td {
        padding:5px 10px;
        border-bottom:1px solid;
        border-right:1px solid;
        border-color: #DDDDDD;;
        background:#fff;
        color:rgb(103, 106, 108) ;
        white-space:nowrap;
        vertical-align:top;
        font-size: 12px
    }

    .table-scroll thead, .table-scroll tfoot {
        background: #f9f9f9;
    }

    .bordertab{
        border-bottom: 1px #e1d0d0 solid !important;
        border-left: 1px #e1d0d0 solid !important;
    }
</style>
<body>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <?php include("../includes/menuIzquierdo.php") ?>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../includes/navegacionArriba.php") ?>
            </nav>
        </div>
        <div class="wrapper wrapper-content">
            <div class="row ibox float-e-margins ibox-content">
                <div class="col-md-12"style="padding: 0">
                    <div id="table-scroll" class="table-scroll">
                        <div class="table-wrap">
                            <div class="col-md-10"></div>
                            <div class="col-md-2">
                                <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#addTransportistaHomologar" id="regTransp"  data-tipo="save">Agregar Transportista <i class="fa fa-search"></i></button>
                            </div>
                            <input type="text" class="form-control input-sm m-b-xs" id="filterrr"
                                   placeholder="Buscar">
                            <table class="table main-table" id="historialcredito" >
                                <thead>
                                    <tr>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"></th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2">Cliente</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2">N° Documento </th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2">Correo</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="3" rowspan="1"> Homologación</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"> Acción</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Enviados </th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Recibidos </th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Homologado</th>
                                    </tr>
                                </thead>
                                <tbody id="resultado">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("../includes/footer.php") ?>
    </div>
</div>

 <div class="modal inmodal" id="addTransportistaHomologar" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom:0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Agregar Transportista a Homologar</h4>
            </div>
           <form  role="form" method="post"  enctype="multipart/form-data" name="regEnvioHomologar" id="regEnvioHomologar">
                <div class="modal-body" style="background: #fff;padding:0 15px"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                <button type="submit" name="guardarPreHomologacion" id="guardarPreHomologacion" onclick="return validarSelect()" class="btn btn-primary btn-modal" value="<?php echo Token::generar() ?>" >Guardar</button>
            </div>
           </form>
        </div>
    </div>
 </div>

<div class="modal inmodal" id="bateriasOModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 5px;padding-bottom: 2px">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                <h4 class="modal-title">Homologar</h4>
                <small class="font-bold"><div class="form-group ">
                    </div></small>
            </div>
            <form  role="form" method="post" enctype="multipart/form-data" id="formOpe" name="formOpe">
                <input  type="text" name="btnHomolgat" id="btnHomolgat" disabled value="" style="display: none">
                <div class="modal-body2" style="background: #fff;padding-top: 0">
                </div>
            </form>
        </div>
    </div>
</div>

<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/plugins/clockpicker/clockpicker.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/select2/select2.full.min.js"></script>
<script src="../assets/bootbox/bootbox.all.js"></script>

<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="../assets/js/jquery-confirm.min.js"></script>

<script src="../assets/js/plugins/ladda/spin.min.js"></script>
<script src="../assets/js/plugins/ladda/ladda.min.js"></script>
<script src="../assets/js/plugins/ladda/ladda.jquery.min.js"></script>
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<style>
    .popover {
        z-index: 999999;
    }
</style>


<?php include_once('../includes/script.php') ?>
<script>

    <?php if (!empty($errores)): ?>
    <?php foreach ($errores as $error): ?>
    toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
    <?php endforeach ?>
    <?php endif ?>
    <?php if(!empty($echo)): ?>
    <?php foreach($echo as $ok): ?>
    toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
    <?php endforeach ?>
    <?php endif ?>
</script>
<script>
    $('#addTransportistaHomologar').on('show.bs.modal', function (e) {
        $(".modal-title").html("Agregar Transportista a Homologar");
        $(".btn-modal").html('Guardar');
        $(".modal-body").html("");

        var tipo = $(e.relatedTarget).data('tipo');

       $.ajax({
            type : 'post',
            url : 'modal/addTranspHomologation',
            data :  "tipoOpera="+tipo,
            success : function(data){
                $('.modal-body').html(data);
            }
        });
    });

    $('#bateriasOModal').on('show.bs.modal', function (e) {
        $('.modal-body2').html("");
        var rowid = $(e.relatedTarget).data('idint');
        $.ajax({
            type : 'post',
            url : 'modal/HomologateModal',
            data :  'idhomdetail='+ rowid,
            success : function(data){
                $('.modal-body2').html(data);
            }
        });
    });

    $(document).ready(function(){
        $('.footable').footable();
        $('.footable2').footable();
    });



    function validarSelect() {
        arr =  $('input[id^="nfact_"]:checked').map(function(){
            return this.value;
        }).get();

        var str = arr.join(',');
        if(str===''){
            str='0';
        }
        if(str==0) {
            bootbox.alert({
                title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                message: "<label>Seleccione un Vehiculo para realizar el registro *</label>",
                size: 'small',
                callback: function () {
                    /*  l.ladda( 'stop' );*/
                }
            });
            return false;
        }

        datunit =  $('input[id^="nfact_"]:checked').map(function(){
            return $("#"+(this.id).replace("nfact","nidsim")+"").val()
        }).get();

        if(datunit.includes("")){
            bootbox.alert({
                title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                message: "<label>el o los vehiculo(s) seleccionado(s) deben tener UnitId *</label>",
                size: 'small',
                callback: function () {
                    /*  l.ladda( 'stop' );*/
                }
            });
            return false;
        }

    }

    $('body').on('hidden.bs.modal', function () {
        if($('.modal.in').length > 0)
        {
            $('body').addClass('modal-open');
        }
    });


    $.ajax({
        type : 'POST',
        url  : 'pag/resultadoBusquedaGestionHomologa',
        data :  {datos:'',page:1},
        success : function(data)
        {
            $("#resultado").html(data);
        }
    });

    $("#filterrr").keypress(function(e)
    {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){
            var datosS = $(this).val();
            datosS = datosS.replace('<>/"', '');

            $.ajax({
                type : 'POST',
                url  : 'pag/resultadoBusquedaGestionHomologa',
                data :  {datos:datosS},
                success : function(data)
                {
                    $("#resultado").html(data);
                }
            });
            return false;
        }
    });

    function btnLink(e){
        var rowid = $(e).data('id');
        var ndata = $(e).data('hlink');

        var l = $('.ladda-button-dlink'+rowid+'').ladda();
        l.ladda( 'start' );

        bootbox.confirm({
            title: "<h3><strong>Atención!</strong></h3>",
            message: "Esta Seguro que deseas enviar link homologacion a:    "+ndata+" ?",
            size: 'small',
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancelar'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirmar'
                }
            },
            callback: function (result) {
                if(result){
                    $("#btnblink"+rowid).prop('disabled',false);
                    $("#idlinkHomo"+rowid).prop('disabled',false);
                    $("#btnblink"+rowid).val($("#guardarPreHomologacion").val());
                    $("#FRMLINK"+rowid).submit();
                    return true;
                }else{
                    l.ladda('stop');
                    //  $("#liberarTec").on("click",$(this)).find("#liberarTec");
                }
            }
        });
    };



    function btnAnula(e){
        var rowid = $(e).data('id');
        var ndata = $(e).data('vehicle');
        var tipo = $(e).data('tipo');

        var l = $('.ladda-button-demod'+rowid+'').ladda();
        l.ladda( 'start' );

        bootbox.confirm({
            title: "<h3><strong>Atención!</strong></h3>",
            message: "Esta Seguro que deseas Eliminar    "+ndata+" ?",
            size: 'small',
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancelar'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirmar'
                }
            },
            callback: function (result) {
                if(result){
                    $("#btnanul"+rowid).prop('disabled',false);
                    $("#idDetHomo"+rowid).prop('disabled',false);
                    $("#btnanul"+rowid).val($("#guardarPreHomologacion").val());
                    $("#FRMANUL"+rowid).submit();
                    return true;
                }else{
                    l.ladda('stop');
                    //  $("#liberarTec").on("click",$(this)).find("#liberarTec");
                }
            }
        });
    };

    function btnHomologa(e){
        if(validarSelectImg()) {
            if(validarSelectstatus()) {

                bootbox.confirm({
                    title: "<h3><strong>Atención!</strong></h3>",
                    message: "Esta Seguro que deseas Guardar los datos ?",
                    size: 'small',
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancelar'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Confirmar'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            $("#btnHomolgat").prop('disabled', false);
                            // $("#idDetHomo"+rowid).prop('disabled',false);
                            $("#btnHomolgat").val($("#guardarPreHomologacion").val());
                            $("#formOpe").submit();
                            return true;
                        } else {

                        }
                    }
                });

            }
        }
        return false;
    };


    function validarSelectstatus() {
        var stat= $("#cbostatus").val();
        arr =  $('input[id^="check_"]:checked').map(function(){
            return this.value;
        }).get()

        if(stat=="0") {
            bootbox.alert({
                title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                message: "<label>Seleccione Estado operacion</label>",
                size: 'small',
                callback: function () {
                    /*  l.ladda( 'stop' );*/
                }
            });
            return false;
        }else  if(stat=="110" && ((arr.length)==$('input[name="check[]"]').length)){
            bootbox.alert({
                title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                message: "<label> si ya se verifico todas las fotos deberia cambiar el estado a Activo</label>",
                size: 'small',
                callback: function () {
                    /*  l.ladda( 'stop' );*/
                }
            });
            return false;
        }
        return true;
    }

    function validarSelectImg() {
        arr =  $('input[id^="check_"]:checked').map(function(){
            return this.value;
        }).get();
        var stat= $("#cbostatus").val();
        if(stat=="110"){
            return true;
        }else
        if((arr.length)!=$('input[name="check[]"]').length) {
            bootbox.alert({
                title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                message: "<label>Debe verificar todas las fotos para poder Guardar *</label>",
                size: 'small',
                callback: function () {
                    /*  l.ladda( 'stop' );*/
                }
            });
            return false;
        }
        return true;
    }
</script>
<?php else: ?>
    <?php $session->salir();
    echo "NO NO NO NO ; presiona F5"
    ?>
<?php endif ?>

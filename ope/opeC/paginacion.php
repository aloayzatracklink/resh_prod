  <?php require_once("../../includes/init.php") ?>
  <?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
}else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $opeObj=new Operacion();
  $cantidadO=$opeObj->obtenerOperaionesSinFechaSeguimiento();
  $registrosPorPagina=7;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  $n=1;
  $datos=$opeObj->obtenerLasOperacionesSubidasPendientes($posisionDePagina,$registrosPorPagina);
             ?>    
       <?php if ($datos): ?>
              
<?php foreach ($datos as $var): ?>    
<?php
    $usuarioV=Usuario::obtenerUsuario($var->idCreador);
?>
    <tr class="animated bounceIn">
        <td><?= $n++ ?></td>
        <td><?php $clienteObj =Cliente::obtenerDatosDeclientePorId($var->idcliente);?> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? $clienteObj->nombre." ".$clienteObj->apellidosC : $clienteObj->razonSocial ; ?></td>
        <td><?php $vehiculoObj = Vehiculo::obtenerVehiculo($var->idvehiculo); echo $vehiculoObj->placa ?></td>  
        <td><?= haceMinimo($var->fechaCreacion) ?></td>
        <td><?= $usuarioV->usuarioNombre;?> <?= $usuarioV->usuarioApellidoPaterno;  ?></td>
        <td><?= obtenerEstado($var->estado) ?></td>
        <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" onclick="limpiarOrden()"  data-id="<?php echo $var->idoperacion ?>">Ver <i class="fa fa-eye"></i></button>&nbsp;</td>
    </tr>
<?php endforeach ?> 
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="9">                        
  <?= paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>
              <tr>
                    <td colspan="8">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
                    </td>
                </tr>
            
 <?php endif ?>




<?php } ?>

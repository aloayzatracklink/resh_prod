<?php $nav="verificarOperaciones" ;?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
} 
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->verificarOperaciones=="si"): ?>
<?php
    $operacionObj=new Operacion();
?>   
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
             <?php include("../includes/menuIzquierdo.php") ?>
         </div>
     </nav>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../includes/navegacionArriba.php") ?>

            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                   <div class="row">
                       <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Instalaciones del mes - <?php  $mes=date("m"); echo $retVal = ($mes==01) ? "Enero" : "Febrero" ;  ?> del <?php echo $anio=date("Y") ?></h5>
                            </div>
                            <div class="ibox-content">
                                <div>
                                    <div id="slineChart" ></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include("../includes/footer.php") ?>
</div>
</div>

</div>


<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- FooTable -->
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/d3/d3.min.js"></script>
<script src="../assets/js/plugins/c3/c3.min.js"></script>
 <?php include_once('../includes/script.php') ?>
<?php 
$objTecnicos=new Usuario(); 
?>

<script>
    $(document).ready(function(){
        c3.generate({
                bindto: '#slineChart',
                data:{
                    columns: [
                        ['InstalacionesMax',4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],  
                      <?php $objTecnicos->obtenerTecnicosEstadisticas() ?>
                    ],
                    colors:{
                        InstalacionesMax: '#FF0000',
                        APAZA: '#00FF7F',
                        MONTERREY: '#D2B48C',
                        CHINCHAY: '#9ACD32',
                        MEDINA: '#FFA500',
                        MEDINA: '#FFA500',
                        MEDINA: '#FFA500',
                        MEDINA: '#FFA500',
                        MEDINA: '#FFA500',
                        MEDINA: '#FFA500',
                        MEDINA: '#FFA500',
                        MEDINA: '#FFA500',
                        MEDINA: '#FFA500',
                        Disponible: '#fff',
                        Disponible: '#fff',
                        Disponible: '#fff',
                        DURAN: '#BABABA'
                    },
                    type: 'spline'
                }
            });

    });
 
</script>
<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>

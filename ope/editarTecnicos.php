<?php $nav="editarTecnicos" ;?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
} ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->editarTecnicos=="si"): ?>
<?php
if (isset($_POST['guardarCambios'])) {
    $ObjTect=new Tecnicos();
    $ObjTect->tecnico=$_POST['nmtecnico'];
    $ObjTect->min_desc=$_POST['mnmtecnico'];
    $ObjTect->estado=$_POST['estado'];
    $ObjTect->hora_inicio=$_POST['horaini'];
    $ObjTect->hora_fin=$_POST['horafin'];
    $ObjTect->iddepartamento=$_POST['iddepartamento'];
    $ObjTect->idprovincia=$_POST['idprovincia'];
    $ObjTect->iddistrito=$_POST['iddistrito'];
    $ObjTect->ubicacion=$_POST['nubicacion'];
    $ObjTect->fijo=isset($_POST['fijo'])?'1':'0';
    if($_POST['tipoOpera']=='edit'){
        $ObjTect->idtec=$_POST['idtecnico'];
        $ObjTect->updateTecnico();
    }else{
        $ObjTect->registrarTecnico();
    }
}

?>
<body>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <?php include("../includes/menuIzquierdo.php") ?>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../includes/navegacionArriba.php") ?>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                    <div class="ibox float-e-margins col-lg-12">
                        <div class="ibox-title">
                            <h5>Técnicos</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">

                            <div class="col-md-8"></div>
                            <div class="col-md-2">
                                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#tiempoTraslado" data-id="0" data-tipo="tsave">Tiempo Traslado <i class="fa fa-clock-o"></i></button>  </label>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editarTecnicos" data-id="0" data-tipo="save">Registrar Técnico <i class="fa fa-user"></i></button>  </label>
                            </div>
                            <input type="text" class="form-control input-sm m-b-xs" id="filter"
                                   placeholder="Filtrar tecnicos">

                            <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Técnico</th>
                                    <th>Mini Descripcion</th>
                                    <th>Horario</th>
                                    <th>Ubicación</th>
                                    <th>Dirección</th>
                                    <th>Estado</th>
                                    <th>Acción</th>
                                    <!-- <th data-hide="all">Task</th> -->
                                </tr>
                                </thead>
                                <tbody>
                                <?php $objTec=Tecnicos::obtenerTecnicos('');$n=1; ?>
                                <?php foreach ($objTec as $var): ?>
                                    <tr class="gradeX">
                                        <td><?= $n++; ?></td>
                                        <td><?= $var->tecnico ?> </td>
                                        <td><?= $var->min_desc ?> </td>
                                        <td><?= $var->hora_inicio ?> - <?= $var->hora_fin ?></td>
                                        <td><?= $var->ubicacion ?></td>
                                        <td><?= $var->departamento ?> ,<?= $var->provincia?> ,<?= $var->distrito ?></td>
                                        <td><?= obtenerEstado($var->estado) ?></td>
                                        <td>
                                            <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#editarTecnicos" data-id="<?php echo $var->idtec ?>" data-tipo="edit"> <i class="fa fa-edit"></i></button>
                                            <button class="btn btn-warning " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#liberarTecnicos" data-nomtec="<?= $var->tecnico ?>"data-ubicatec="<?= $var->ubicacion ?>" data-provincia="<?= $var->provincia ?>" data-horariotec="<?= $var->hora_inicio ?> - <?= $var->hora_fin ?>"  data-id="<?php echo $var->idtec ?>" > <i class="fa fa-paper-plane"></i></button>
                                        </td>
                                    </tr>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php include("../includes/footer.php") ?>
        </div>
    </div>

<div class="modal inmodal" id="editarTecnicos" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <form  role="form" method="post"  enctype="multipart/form-data" name="editarTecnicos" id="editarTecnicos">
                <div class="modal-body" style="background: #fff;padding-top: 0"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="submit" name="guardarCambios" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
    <div class="modal inmodal" id="liberarTecnicos" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceIn">
                <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title">Liberar al Técnico : <label id="ntec"></label></h4>
                    <h5><label id="dhorario"></label>
                        <input type="text" name="dubica" id="dubica" style="display: none"></h5>
                </div>
                <form  role="form" method="post"  enctype="multipart/form-data" name="liberarTecnicos" id="liberarTecnicos">
                    <div class="modal-body2" style="background: #fff;padding-top: 0"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="tiempoTraslado" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceIn">
                <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title">Tiempo Traslado</h4>
                </div>
                <form  role="form" method="post"  enctype="multipart/form-data" name="tiempoTraslado" id="tiempoTraslado">
                    <div class="modal-body3" style="background: #fff;padding-top: 0"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/plugins/clockpicker/clockpicker.js"></script>


<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/select2/select2.full.min.js"></script>
<style>
    .popover {
        z-index: 999999;
    }
</style>
<script>
    $('#editarTecnicos').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        var tipo = $(e.relatedTarget).data('tipo');
        $(".modal-body").html("");
        $.ajax({
            type : 'post',
            url : 'modal/editarDataModal', //Here you will fetch records
            data :  'rowid='+rowid+"&tipoOpera="+tipo, //Pass $id
            success : function(data){
                $('.modal-body').html(data);//Show fetched data from database
            }
        });
    });

    $(document).ready(function(){
        $('.footable').footable();
        $('.footable2').footable();
    });

    $('#liberarTecnicos').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        var rownomtec = $(e.relatedTarget).data('nomtec');
        var rowhorario = $(e.relatedTarget).data('horariotec');
        var rowUbicacion = $(e.relatedTarget).data('ubicatec');
        var rowProvincia = $(e.relatedTarget).data('provincia');

        $(".modal-body2").html("");
        $("#ntec").text(rownomtec);
        $("#dhorario").text("Horario "+rowhorario+",  Ubicacion: "+rowUbicacion+" , Provincia: "+rowProvincia);
        $("#dubica").val(rowUbicacion);
        $.ajax({
            type : 'post',
            url : 'modal/liberarTecnico', //Here you will fetch records
            data :  'rowid='+ rowid, //Pass $id
            success : function(data){
                $('.modal-body2').html(data);//Show fetched data from database
            }
        });
    });

    $('#tiempoTraslado').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        $(".modal-body3").html("");
        $.ajax({
            type : 'post',
            url : 'modal/tiempoTraslado', //Here you will fetch records
            data :  'rowid='+ rowid, //Pass $id
            success : function(data){
                $('.modal-body3').html(data);//Show fetched data from database
            }
        });
    });
</script>
<?php else: ?>
    <?php $session->salir();
    echo "NO NO NO NO ; presiona F5"
    ?>
<?php endif ?>

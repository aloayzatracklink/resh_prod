<?php $nav="Disponibilidad Técnica" ;?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) { irA("ingresar");} ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->programacion=="si"): ?>
<?php
require_once("../includes/tablaProgramacion.php");
$proObj=new tablaProgramacion;

//!$proObj->liberarEspacioope()
//!$proObj->liberarEspaciocita()
//!$proObj->reservarTurno()
//$proObj->cambiarReserva()
//!$proObj->eliminarReserva()
?>
<link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>
<link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<link href="../assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
<link href="../assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<link href="../assets/css/animate.css" rel="stylesheet">
<body>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <?php include("../includes/menuIzquierdo.php") ?>
        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../includes/navegacionArriba.php") ?>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                    <div class="row animated fadeInDown">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title" style="margin-top:0px;padding-top:0px;margin-bottom:0px;padding-bottom:0px;height: 85px" >
                                    <div class="ibox-tools">
                                        <div class="col-md-8">
                                            <table class="" style="font-size:smaller;">
                                                <thead>
                                                    <tr>
    <!--                                                    <th></th>-->
                                                        <th width="70px" colspan="2">Taller <span id="ttt" style="color: #ee5b5b;">(0)</span> </th>
                                                        <th width="80px" colspan="2">Domicilio <span id="ttd" style="color: #ee5b5b;">(0)</span> </th>
                                                        <th width="100px" colspan="2">Concesionario <span id="tttc" style="color: #ee5b5b;">(0)</span> </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
<!--                                                        <td>Operaciones</td>-->
                                                        <td class="text-center" width="25px">
                                                            <div style="border:1px solid #000000;padding:0;width: 12.5px;display: inline-block;">
                                                                <div style="width:4px;height:0;border:5px solid #1ab394;overflow:hidden"></div>
                                                            </div>
                                                        </td >
                                                        <td class="text-left">  <p style="display: inline-block;margin:0" id="tto">0</p></td>
                                                        <td class="text-center" >
                                                            <div style="border:1px solid #000000;padding:0;width: 12.5px;display: inline-block;">
                                                                <div style="width:4px;height:0;border:5px solid #ecb5d9;overflow:hidden"></div>
                                                            </div>
                                                        </td>
                                                        <td class="text-left">
                                                            <p style="display: inline-block;margin:0" id="tdo">0</p>
                                                        </td>
                                                        <td class="text-center" >
                                                            <div style="border:1px solid #000000;padding:0;width: 12.5px;display: inline-block;">
                                                                <div style="width:4px;height:0;border:5px solid #7badd2;overflow:hidden"></div>
                                                            </div>
                                                        </td>
                                                        <td  class="text-left">
                                                            <p style="display: inline-block;margin: 0" id="tco">0</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
<!--                                                        <td>Central</td>-->
                                                        <td class="text-center">
                                                            <div style="border:1px solid #000000;padding:0;width: 12.5px;display: inline-block;">
                                                                <div style="width:4px;height:0;border-left: 10px solid #fff; border-top: 10px solid #1ab394;overflow:hidden"></div>
                                                            </div>
                                                        </td>
                                                        <td class="text-left">
                                                            <p style="display: inline-block;margin: 0" id="ttc">0</p>
                                                        </td>
                                                        <td class="text-center">
                                                            <div style="border:1px solid #000000;padding:0;width: 12.5px;display: inline-block;">
                                                                <div style="width:4px;height:0;border-left: 10px solid #fff; border-top: 10px solid #ecb5d9;overflow:hidden"></div>
                                                            </div>
                                                        </td>
                                                        <td  class="text-left">
                                                            <p style="display: inline-block;margin: 0" id="tdc">0</p>
                                                        </td>
                                                        <td class="text-center">
                                                            <div style="border:1px solid #000000;padding:0;width: 12.5px;display: inline-block;">
                                                                <div style="width:4px;height:0;border-left: 10px solid #fff; border-top: 10px solid #7badd2;overflow:hidden"></div>
                                                            </div>
                                                        </td>
                                                        <td  class="text-left">
                                                            <p style="display: inline-block;margin: 0" id="tcc">0</p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div >
                                            <a class="btn btn-danger ladda-button ladda-button-gr " data-style="zoom-in" style="margin-top:5px;padding-top: 0;padding-bottom: 0;display: none;color:white" id="cancelr">Cancelar <i class="fa fa-ban"></i></a>
                                            <a class="btn btn-warning ladda-button ladda-button-ac " data-style="zoom-in" style="margin-top:5px;padding-top: 0;padding-bottom: 0;display:none;color:white" id="actuar">Actualizar <i class="fa fa-edit">
                                                    <input type="text" name="idreservaa" id="idreservaa" style="color:black;display: none" value=""  />
                                                </i></a>
                                            <a class="btn btn-primary ladda-button ladda-button-gr " data-style="zoom-in" style="margin-top:5px;padding-top: 0;padding-bottom: 0" id="guardarr">Guardar <i class="fa fa-save"></i></a>
                                            <a class="btn btn-success ladda-button ladda-button-br " data-style="zoom-in" style="margin-top:5px;padding-top: 0;padding-bottom: 0 ;color:white" id="buscarr">Buscar <i class="fa fa-search"></i></a>
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="ibox-content" style="margin-top:15px;padding:0" >
                                    <div class="row" style="margin-bottom: 5px">
                                        <div class="col-md-2">
                                            <label for="mmotivo" class="">Motivo</label>
                                            <input type="text" class="form-control obs" placeholder='Motivo de reserva'  id="mmotivo" name="mmotivo" autocomplete="off" required  >
                                        </div>
                                        <div class="col-md-2">
                                            <label for="reservapara" class="">reservar para</label>
                                            <select  style="border: none;" class="tecnico border-bottom form-control" required name="reservapara" id="reservapara">
                                                <option class="" value="0">Nadie</option>
                                                <?php $ejecutiosCom=Usuario::obtenerPARARESERVA() ?>
                                                <?php foreach ($ejecutiosCom as $var): ?>
                                                    <option class="" value="<?= $var->idusuario ?>"><?=  $var->usuarioNombre." ".$var->usuarioApellidoPaterno; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label for="horaini" class="">Hora Inicio</label>
                                            <div class='input-group clockpicker horaini' data-autoclose='true' >
                                                <input type='text'  class='form-control' readonly name='horaini' id="horaini" required maxlength='5' value=''>
                                                <span class='input-group-addon'>
                                                <span class='fa fa-clock-o'></span>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <label for="horafin" class="">Hora Fin</label>
                                            <div class='input-group clockpicker horafin' data-autoclose='true'>
                                                <input type='text'  class='form-control' readonly name='horafin' id="horafin" required maxlength='5' value=''>
                                                <span class='input-group-addon'>
                                                <span class='fa fa-clock-o'></span>
                                            </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label  class="">Fecha</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" class="form-control tal tala fecha "  id="fechar" name="fechar" autocomplete="off" required>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="reservapara" class="">Tecnico</label>
                                            <select  style="border: none;" class="tecnico border-bottom form-control" required name="ntecnico" id="ntecnico">
                                                <?php $tecnicos=Tecnicos::obtenerTecnicos('') ?>
                                                <?php foreach ($tecnicos as $var): ?>
                                                    <option class="" value="<?= $var->idtec ?>"><?=  $var->min_desc ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                   <!-- <div class="row">
                                        <div class="col-md-6">
                                            <label for="plan" class="">Plan *</label>
                                            <select  style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
                                                <?php if (isset($cotizarObj->plan)): ?>
                                                    <option class="" value="<?php echo $cotizarObj->plan ?>"  selected ><?php echo $cotizarObj->plan ?></option>
                                                <?php endif ?>
                                                <option class="" value="" disabled selected >Seleccione Plan *</option>
                                                <?php Cliente::obtenerPlan() ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6 ">
                                            <p style="font-weight:bold">Servicios Adicionales *: <label class="canthoraprog2 label-success"> 0 Horas </label></p>
                                            <div class="servAdi">
                                                <?php ServiciosAdicionales::obtenerServAdicional('') ?>
                                            </div>
                                            <input type="hidden" id="canthoraprog" name="canthoraprog" value="0">
                                        </div>
                                    </div>-->

                                   <!-- <div class="ibox" id="ibox1">-->
                                        <div class="ibox-content" style="padding: 0">
                                            <div class="sk-spinner sk-spinner-double-bounce">
                                                <div class="sk-double-bounce1"></div>
                                                <div class="sk-double-bounce2"></div>
                                            </div>
                                            <div id="programacionHtml">
                                            </div>
                                        </div>
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include("../includes/footer.php") ?>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="programacionModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
            </div>
            <form  role="form" method="post" >
                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<!-- jQuery UI custom -->
<script src="../assets/js/jquery-ui.custom.min.js"></script>
<!-- iCheck -->
<script src="../assets/js/plugins/iCheck/icheck.min.js"></script>
<script src="../assets/build/jquery.datetimepicker.full.js"></script>
<script src="../assets/bootbox/bootbox.all.js"></script>
<script src="../assets/js/plugins/select2/select2.full.min.js"></script>
<script src="../assets/js/plugins/clockpicker/clockpicker.js"></script>

<script src="../assets/js/plugins/ladda/spin.min.js"></script>
<script src="../assets/js/plugins/ladda/ladda.min.js"></script>
<script src="../assets/js/plugins/ladda/ladda.jquery.min.js"></script>
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>

<!-- Full Calendar -->
<style>
    .ibox-content > .sk-spinner {
        display: none;
    }

    .ibox-content.sk-loading {
        position: relative;
    }
    .ibox-content.sk-loading:after {
        content: '';
        background-color: rgba(255, 255, 255, 0.7);
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }
    .ibox-content.sk-loading > .sk-spinner {
        display: block;
        position: absolute;
        top: 40%;
        left: 0;
        right: 0;
        z-index: 2000;
    }
</style>
<script>
    <?php $fechaA=date('Y-m-d') ?>
    jQuery.datetimepicker.setLocale('es');

    $('.fecha').datetimepicker({
        format:'Y-m-d',
        scrollInput: false,
        scrollMonth : false,
        timepicker:false
    });
    $('#fechar').val('<?= $fechaA ?>');

    function limpiarR(){
        $("#idreserva").val("");
        $("#reservapara").val($("#reservapara option:first").val());
        $('#horaini').val('');
        $('#horafin').val('');
        $("#ntecnico").val($("#ntecnico option:first").val());
        $('#mmotivo').val('');
        $("#mmotivo").prop("disabled",false);
        $("#reservapara").prop("disabled",false);
    }

    function listarLeyendaTBProg(){
        var fecha = $('#fechar').val();
        $.ajax({
            type : 'POST',
            url  : '../../com/ajax/ttleyendatbprog',
            data : {fecha:fecha},
            success : function(data)
            {
              //  data=JSON.stringify(data.trim());
                var obj = jQuery.parseJSON(data.trim());
                $("#ttt").text("("+(parseInt(obj.tto) + parseInt(obj.ttc))+")");
                $("#ttd").text("("+(parseInt(obj.tdo) + parseInt(obj.tdc))+")");
                $("#tttc").text("("+(parseInt(obj.tco) + parseInt(obj.tcc))+")");

                $("#tto").text(obj.tto);
                $("#tdo").text(obj.tdo);
                $("#tco").text(obj.tco);

                $("#ttc").text(obj.ttc);
                $("#tdc").text(obj.tdc);
                $("#tcc").text(obj.tcc);
            }
        });
    }

    function listardisponilidadTec(){
        var fecha = $('#fechar').val();
        var l = $('.ladda-button-br').ladda();
        $("#buscarr").off( "click" );
        l.ladda( 'start' );
        $("#guardarr").show();
        $("#actuar").hide();
        $("#cancelr").hide();
        $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
        limpiarR();
        $.ajax({
            type : 'POST',
            url  : '../includes/tbDisponibilidadR',
            data : {fecha:fecha},
            success : function(data)
            {
                $("#programacionHtml").html(data);
                $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                l.ladda( 'stop' );
                $("#buscarr").on("click",listardisponilidadTec).find("#buscarr");
                $("#buscarr").on("click",listarLeyendaTBProg).find("#buscarr");
            }
        });
    }

    $(document).ready(function() {
        $('.clockpicker').clockpicker();
        listardisponilidadTec();
        listarLeyendaTBProg();
    }); 

    $('#plan').change(function(){

        $("#programacionHtml").html('');
        $(".horabtn").html('Seleccionar hora');
        $(".horaVer").val('');
        var arr="";
        var seleccionado = $("#plan").find('option:selected');
        var idtec = seleccionado.data('type');

        arr = $('[name="servad[]"]:checked').map(function(){
            return this.value;
        }).get();
        var str = arr.join(',');
        if(str==='' && (idtec === undefined)){
            str='0';
            idtec='0';
        }

        $.ajax({
            type : 'POST',
            url  : 'ajax/ServiciosAdicionales',
            data :  "idTec="+idtec,
            success : function(data)
            {
                if(data!=''){
                    $(".servAdi").html(data);
                }else{
                    console.log("No hay data");
                }
            }
        });
        $("#canthoraprog").val("0");
        $(".canthoraprog2").text("0 Horas");
       // return false;

    });


    <?php if ($datos=ServiciosAdicionales::obtenerServicesAdicional()): ?>
    <?php foreach ($datos as $var): ?>
    $("#<?php echo $var->codigo ?>").on("change",function (e) {
        $("#programacionHtml").html('');
        $(".horabtn").html('Seleccionar hora');
        $(".horaVer").val('');

        var isChecked = e.currentTarget.checked;
        var seleccionado = $("#plan").find('option:selected');
        var idtec = seleccionado.data('type');
        //  alert(isChecked);
        var arr = $('[name="servad[]"]:checked').map(function(){
            return this.value;
        }).get();
        var str = arr.join(',');
        // $("#canthoraprog").val("0");
//    $(".canthoraprog2").text("0 Horas");
        if(!isChecked && (idtec === undefined)){
            str='0';
            idtec='0';
        }
        $.ajax({
            type : 'POST',
            url  : 'ajax/cantidadHoraProgramacion',
            data :  'idCheck='+str+"&idTec="+idtec,
            success : function(data)
            {
                if(data!=''){
                    $("#canthoraprog").val(data);
                    $("#canthoraprog2").text(data+" Horas");
                }else{
                    console.log("No hay data");
                }
            }
        });
        // return false;
    });
    <?php endforeach ?>
    <?php endif ?>


    $(".horabtn").click(function(){
        var lugarI=$("input[name='lugarInstacion']:checked" ).val();
        var distrito="0";
        var tthinstala=$('#canthoraprog').val();
        if (lugarI=='Taller') {
            var fecha = $('#fechaIT').val();
            var cita = 'Taller';
        }else if (lugarI=='Domicilio Cliente') {
            var fecha = $('#fechaID').val();
            var cita = 'Domicilio';
            distrito=$('#distritoInstalacionD').val();
        }else if (lugarI=='Concesionario') {
            var fecha = $('#fechaIC').val();
            var cita = 'Pandero';
            distrito=$('#distritoInstalacionC').val();
        }
        var cod = document.getElementById("plan").value;
        servAd = $('[name="servad[]"]:checked').length;
        if (fecha!='' && cod!='' && servAd!=0 &&(distrito==0 || distrito!=null)) {
            $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
            $.ajax({
                type : 'POST',
                url  : '../includes/tbProgramacion',
                data : {fecha:fecha,cita:cita,tthinstala:tthinstala,iddist:distrito},
                success : function(data)
                {
                    $("#programacionHtml").html(data);
                    $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                }
            });
        }else{
            $("#programacionHtml").html('<h3 class="text-center text-info">Primero debe seleccionar Plan,Servicios Adicionales,fecha y Distrito.</h3>');
        }
       // return false;
    });


/*    $(".fecha").focus(function(){
        $(this).blur();
    });*/


    $("#buscarr").click(function(){
        listarLeyendaTBProg();
        listardisponilidadTec();
        console.log("acacscsasa");
    });

    $("#guardarr").click(function(){
        var fecha = $('#fechar').val();
        var rpara =$('#reservapara option:selected').val();
        var para =$('#reservapara option:selected').text();
        var horaini =$('#horaini').val();
        var horafin =$('#horafin').val();
        var ntecnico =$('#ntecnico option:selected').val();
        var tecnico =$('#ntecnico option:selected').text();
        var motivo =$('#mmotivo').val();

        var l = $('.ladda-button-gr').ladda();
        l.ladda( 'start' );

        if(fecha.trim()==='' || horaini.trim()==='' || horafin.trim()===''){
            bootbox.alert({
                title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                message: "<label>Ingresar Datos (Fecha,Hora inicio y Hora Fin)</label>",
                size: 'small',
                callback: function () {
                    l.ladda( 'stop' );
                }
            })
        }else if(((horaini.charAt(3)!='0' && horaini.charAt(3)!='3') || horaini.charAt(4)!='0') || ((horafin.charAt(3)!='0' && horafin.charAt(3)!='3') || horafin.charAt(4)!='0')){
            bootbox.alert({
                title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                message: "<label>el tiempo seleccionado debe de ser de 30 en 30 min</label>",
                size: 'small',
                callback: function () {
                    l.ladda( 'stop' );
                }
            })
        }else {
            if(horaini<=horafin){
                bootbox.confirm({
                    title: "<h3><strong>Guardar Datos</strong></h3>",
                    message: "Deseas Registrar la Reserva para <b>"+para+"</b> con el tecnico <b>"+tecnico+"</b> el día <b>"+fecha+"</b> desde las <b>"+horaini+"</b> hasta las <b>"+horafin+"</b> ?",
                    size: 'small',
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancelar'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Confirmar'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            $.ajax({
                                type: 'POST',
                                url: 'ajax/Reg_reserva',
                                data: "fecha=" + fecha +"&rpara="+rpara+"&horaini=" + horaini + "&horafin=" + horafin + "&idtec=" + ntecnico +"&motivo="+motivo+"&tipo=save",
                                success: function (data) {
                                    if(data.trim()=="dp"){
                                        bootbox.alert({
                                            title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                                            message: "<label>El Rango de Horas ya se encuentra registrado.</label>",
                                            size: 'small',
                                            callback: function () {
                                            }
                                        })
                                        l.ladda('stop');
                                    }else {
                                        if (data.trim() === 'Ok') {
                                            listardisponilidadTec();
                                            limpiarR();
                                            toastr.success('Se Registro Correctamente', '');
                                        }
                                        l.ladda('stop');
                                    }
                                }
                            });
                        }
                        l.ladda('stop');
                    }
                });
            }else{
                bootbox.alert({
                    title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                    message: "<label class=''>La Hora de Inicio debe ser menor o igual a la Hora Fin.</label>",
                    size: 'small',
                    callback: function () {
                        l.ladda( 'stop' );
                    }
                })
            }
        }
    });


    $("#actuar").click(function(){
        var idreservaa =$('#idreservaa').val();
        var fecha = $('#fechar').val();
        var rpara =$('#reservapara option:selected').val();
        var para =$('#reservapara option:selected').text();
        var horaini =$('#horaini').val();
        var horafin =$('#horafin').val();
        var ntecnico =$('#ntecnico option:selected').val();
        var tecnico =$('#ntecnico option:selected').text();
        var motivo =$('#mmotivo').val();

        var l = $('.ladda-button-ac').ladda();
        l.ladda( 'start' );

        if(fecha.trim()==='' || horaini.trim()==='' || horafin.trim()===''){
            bootbox.alert({
                title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                message: "<label>Ingresar Datos (Fecha,Hora inicio y Hora Fin)</label>",
                size: 'small',
                callback: function () {
                    l.ladda( 'stop' );
                }
            })
        }else if(((horaini.charAt(3)!='0' && horaini.charAt(3)!='3') || horaini.charAt(4)!='0') || ((horafin.charAt(3)!='0' && horafin.charAt(3)!='3') || horafin.charAt(4)!='0')){
            bootbox.alert({
                title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                message: "<label>el tiempo seleccionado debe de ser de 30 en 30 min</label>",
                size: 'small',
                callback: function () {
                    l.ladda( 'stop' );
                }
            })
        }else {
            if(horaini<horafin){

                bootbox.confirm({
                    title: "<h3><strong>Actualizar Datos</strong></h3>",
                    message: "Deseas Actualizar la Operación para <b>"+para+"</b> con el tecnico <b>"+tecnico+"</b> el día <b>"+fecha+"</b> desde las <b>"+horaini+"</b> hasta las <b>"+horafin+"</b> ?",
                    size: 'small',
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancelar'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Confirmar'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            $.ajax({
                                type: 'POST',
                                url: 'ajax/Reg_reserva',
                                data: "fecha=" + fecha+"&idreser="+idreservaa+"&rpara="+rpara+"&horaini=" + horaini + "&horafin=" + horafin + "&idtec=" + ntecnico +"&motivo="+motivo+"&tipo=edit",
                                success: function (data) {
                                    if(data.trim()=="dp"){
                                        bootbox.alert({
                                            title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                                            message: "<label>El Rango de Horas ya se encuentra registrado.</label>",
                                            size: 'small',
                                            callback: function () {
                                            }
                                        })
                                        l.ladda('stop');
                                    }else {
                                        if (data.trim() === 'Ok') {
                                            listardisponilidadTec();
                                            limpiarR();
                                            $("#guardarr").show();
                                            $("#actuar").hide();
                                            $("#cancelr").hide();
                                            toastr.success('Se Actualizo Correctamente', '');
                                        }
                                    }
                                    l.ladda('stop');
                                }
                            });
                        }
                        l.ladda('stop');
                    }
                });
            }else{
                bootbox.alert({
                    title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                    message: "<label class=''>La Hora de Inicio debe ser menor a la Hora Fin.</label>",
                    size: 'small',
                    callback: function () {
                        l.ladda( 'stop' );
                    }
                })
            }
        }
    });

    $('#programacionModal').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        var fechaP = $('.fechaa').val();
        if (fechaP=='') {
            var fechaP = "2019-12-06";
        }
        $.ajax({
            type : 'post',
            url : 'modal/programarT',
            data :  {rowid:rowid,dia:fechaP},
            success : function(data){
                $('.modal-body').html(data);
            }
        });
    });
</script>
<?php include_once('../includes/script.php') ?>
<?php else: ?>
    <?php $session->salir();
    echo "NO NO NO NO ; presiona F5"
    ?>
<?php endif ?>


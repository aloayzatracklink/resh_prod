<?php $nav="asignarOrdenes" ;?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->registrarOperaciones=="si"): ?>
<?php
$operacionObj=new Operacion();
$facturacionObj=new DatosFacturacion();
$errores=array();
$echo=array();
if (isset($_POST['guardarOrden'])) {
    if (empty($_POST['orden'])) {
        $errores[]="Fail detected contactarse con wilmanpabe@gmail.com";
    }
    $datosfid=limpiar($_POST['idFacturacion']);
    $operacionId=limpiar($_POST['idOperacion']);
    $orden=limpiar($_POST['orden']);
    if (isset($_POST['orden2'])) {
        $orden.="  ORDEN EQUIPO ".$_POST['orden2'];
    }
    if ($facturacionObj->ingresarOrden($datosfid,$orden)) {
        $operacionObj->numeroOrden=$orden;
        $operacionObj->idoperacion=$operacionId;

        //trabajo==reactiva//
        if($_POST['trabajo']=='Reactivacion'){
            if (isset($_POST['idFacturacion'])) {
                  if (isset($_POST['orden']) and $_POST['orden'] !='') {
                    if(DatosFacturacion::crearContrato($_POST['idFacturacion'])){
                    ##GENERATE CONTRACT
                        $ordenn=explode("-",$_POST['orden']);
                        $ordenn[0];
                        $ordenn[1];
                        $command = escapeshellcmd('python3.7 ../../pdf/run_queue.py contract '.$ordenn[1].' '.$ordenn[0].' '.$_POST['idFacturacion']);
                        $output = shell_exec($command);
                    }
                }
            }

            $operacionObj->fechaInstalacion=date("Y-m-d H:m");
            $operacionObj->estado='15';
            $operacionObj->ordenContrato=$_POST['orden'];
            $operacionObj->actualizarEstadoYFechaI("3",$_POST['idvehiculo']);

        
        }
        //15 operacion
        //3 vehiculo

        if ($operacionObj->guardarNumeroDeOrden()) {
            $echo[]="Orden registrado exitosamente.";
        }
    }
}

?>
<link href="../assets/css/plugins/select2/select2.min.css" rel="stylesheet">
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
               <?php include("../includes/menuIzquierdo.php") ?>
           </div>
       </nav>

       <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../includes/navegacionArriba.php") ?>

            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                <div class="ibox float-e-margins col-lg-12">
                    <div class="ibox-content">
                        <input type="text" class="form-control input-sm m-b-xs" id="filter"
                        placeholder="Filtrar vehículo">
                    <div class="table-responsive">
                        <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th >Trabajo</th>
                                    <th data-hide="phone,tablet" >Cliente</th>
                                    <th>Placa</th>
                                    <th>Chasis</th>
                                    <th data-hide="phone,tablet">Estado(s)</th>
                                    <th>Campaña</th>
                                    <th>Tipo Registro</th>
                                    <th>A cargo de: </th>
                                    <th>Acción</th>
                                    <th data-hide="phone,tablet">Creado por</th>
                                </tr>
                            </thead>    
                            <tbody id="dataOperaciones">                              
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

     <div class="modal inmodal" id="ordenes" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceIn">
                <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                    <h4 class="modal-title">Asignar orden para facturación.</h4>
                </div>
                <form  role="form" method="post" >

                    <div class="modal-body" style="background: #fff;padding-top: 0">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                        <button type="submit" name="guardarOrden" class="btn btn-primary">Guardar Cambios</button>
                    </div>
                </form>   
            </div>
        </div>
    </div>


    <!-- modal fin -->
</div>
<?php include("../includes/footer.php") ?>
</div>
</div>

</div>


<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- FooTable -->
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/select2/select2.full.min.js"></script>
<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
 <?php include_once('../includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
<script>
var eventos={}
eventos.frejcar =function(){
    $.ajax({
        url: 'ajax/asignarOrdenes',
                type: 'POST', // Send post data
                async: true,
                success: function(data){
                    $('#dataOperaciones').html(data);
                }
            });
}
eventos.interval = setInterval(eventos.frejcar, 5000);
eventos.frejcar();
    $(document).ready(function(){
        $('.footable').footable();
        $('.footable2').footable();
        // $('#data_1 .input-group.date').datepicker({
        //     todayBtn: "linked",
        //     keyboardNavigation: false,
        //     forceParse: false,
        //     calendarWeeks: true,
        //     autoclose: true
        // });

    });
    $('#ordenes').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        $.ajax({
            type : 'post',
            url : 'modal/registrarOrdenesModal', //Here you will fetch records 
            // url : 'modal/operacionesDataModal', //Here you will fetch records 
            data :  'rowid='+ rowid, //Pass $id
            success : function(data){
            $('.modal-body').html(data);//Show fetched data from database
        }
    });
    });
</script>
<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>

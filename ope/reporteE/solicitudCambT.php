<?php 
if (isset($_POST)) {
	require_once("../../includes/init.php");
	require_once '../../mailphp/PHPMailer/PHPMailerAutoload.php';
	$mail = new PHPMailer;
 	  $idOperacion=htmlspecialchars(htmlentities($_POST['idoperacion']));
	  $ObjetoOperacion=Operacion::obtenerOperacionPorId($idOperacion);
	  if ($ObjetoOperacion->idlocation==0) {
	    $vehiculoObj=Vehiculo::obtenerVehiculo($ObjetoOperacion->idvehiculo);
	    $clienteObj=Cliente::obtenerClientePorVehiculo($ObjetoOperacion->idvehiculo,$ObjetoOperacion->idcliente);  
	  }else{
	    $clienteObj=Onix::obtenerDatosDesdeIds($ObjetoOperacion->idcliente,$ObjetoOperacion->idvehiculo,$ObjetoOperacion->idlocation) ;
	    $chequeoObj=Chequeos::obtenerChequeoTrackPorId($ObjetoOperacion->idChequeo);
	    $vehiculoObj=false;
	  }
if ($vehiculoObj){	
	$datosCliente=(!empty($clienteObj->apellidosC))?$clienteObj->apellidosC." ".$clienteObj->nombre:$clienteObj->razonSocial;

}else{	
	$apellidos=utf8_encode($clienteObj->apellidos);
	$datosCliente = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ;	
}
$usuarioEje=Usuario::obtenerUsuario($session->idusuario);
$usuarioCrea=Usuario::obtenerUsuario('66');
$motivo=' No hay técnico disponible '.$_POST['observacion'];
$orte=$ObjetoOperacion->numeroOrte;
$trabajo=$ObjetoOperacion->trabajo;
$lugar=$ObjetoOperacion->lugarInstalacion;
$fecha=$ObjetoOperacion->fechaInstalacion;
$usuarioSolicitador=$usuarioEje->usuarioNombre." ".$usuarioEje->usuarioApellidoPaterno;
$cel=$usuarioEje->celular;
$correo=$usuarioEje->usuario;
$htmlContenido1="<html xmlns='http://www.w3.org/1999/xhtml' xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office'><head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <meta name='viewport' content='width=device-width'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>    
    <style type='text/css' id='media-query'>
      body {
  margin: 0;
  padding: 0; }
table, tr, td {
  vertical-align: top;
  border-collapse: collapse; }

.ie-browser table, .mso-container table {
  table-layout: fixed; }

* {
  line-height: inherit; }

a[x-apple-data-detectors=true] {
  color: inherit !important;
  text-decoration: none !important; }

[owa] .img-container div, [owa] .img-container button {
  display: block !important; }

[owa] .fullwidth button {
  width: 100% !important; }

[owa] .block-grid .col {
  display: table-cell;
  float: none !important;
  vertical-align: top; }

.ie-browser .num12, .ie-browser .block-grid, [owa] .num12, [owa] .block-grid {
  width: 745px !important; }

.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
  line-height: 100%; }

.ie-browser .mixed-two-up .num4, [owa] .mixed-two-up .num4 {
  width: 248px !important; }

.ie-browser .mixed-two-up .num8, [owa] .mixed-two-up .num8 {
  width: 496px !important; }

.ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {
  width: 372px !important; }

.ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {
  width: 248px !important; }

.ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {
  width: 186px !important; }

.ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {
  width: 149px !important; }

.ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {
  width: 124px !important; }

.ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {
  width: 106px !important; }

.ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {
  width: 93px !important; }

.ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {
  width: 82px !important; }

.ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {
  width: 74px !important; }

.ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {
  width: 67px !important; }

.ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {
  width: 62px !important; }

@media only screen and (min-width: 765px) {
  .block-grid {
    width: 745px !important; }
  .block-grid .col {
    vertical-align: top; }
    .block-grid .col.num12 {
      width: 745px !important; }
  .block-grid.mixed-two-up .col.num4 {
    width: 248px !important; }
  .block-grid.mixed-two-up .col.num8 {
    width: 496px !important; }
  .block-grid.two-up .col {
    width: 372px !important; }
  .block-grid.three-up .col {
    width: 248px !important; }
  .block-grid.four-up .col {
    width: 186px !important; }
  .block-grid.five-up .col {
    width: 149px !important; }
  .block-grid.six-up .col {
    width: 124px !important; }
  .block-grid.seven-up .col {
    width: 106px !important; }
  .block-grid.eight-up .col {
    width: 93px !important; }
  .block-grid.nine-up .col {
    width: 82px !important; }
  .block-grid.ten-up .col {
    width: 74px !important; }
  .block-grid.eleven-up .col {
    width: 67px !important; }
  .block-grid.twelve-up .col {
    width: 62px !important; } }

@media (max-width: 765px) {
  .block-grid, .col {
    min-width: 320px !important;
    max-width: 100% !important;
    display: block !important; }
  .block-grid {
    width: calc(100% - 40px) !important; }
  .col {
    width: 100% !important; }
    .col > div {
      margin: 0 auto; }
  img.fullwidth, img.fullwidthOnMobile {
    max-width: 100% !important; } }

    </style>
</head>
<body class='clean-body' style='margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #E8EDEF'>
  <style type='text/css' id='media-query-bodytag'>
    @media (max-width: 765px) {
      .block-grid {
        min-width: 320px!important;
        max-width: 100%!important;
        width: 100%!important;
        display: block!important;
      }

      .col {
        min-width: 320px!important;
        max-width: 100%!important;
        width: 100%!important;
        display: block!important;
      }

      .col>div {
        margin: 0 auto;
      }

      img.fullwidth {
        max-width: 100%!important;
      }

      img.fullwidthOnMobile {
        max-width: 100%!important;
      }
    }
  </style>
  <table class='nl-container' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #E8EDEF;width: 100%' cellpadding='0' cellspacing='0'>
    <tbody>
      <tr style='vertical-align: top'>
        <td style='word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding: 0'>
          <div style='background-color:transparent;'>
            <div style='Margin: 0 auto;min-width: 320px;max-width: 745px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
              <div style='border-collapse: collapse;display: table;width: 100%;background-color:transparent;'>                   
                  <div class='col num12' style='min-width: 320px;max-width: 745px;display: table-cell;vertical-align: top;'>
                    <div style='background-color: transparent; width: 100% !important;'>
                    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'>
                          <div align='center' class='img-container center  autowidth  fullwidth' style='padding-right: 0px;  padding-left: 0px;'>
<div style='line-height:25px;font-size:1px'>&#160;</div>  <img class='center  autowidth  fullwidth' align='center' border='0' src='cid:rounder-up' alt='Image' title='Image' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 745px' width='745'>
</div>                       
                    </div>
                    </div>
                  </div>
                    </div>
            </div>
          </div>          <div style='background-color:transparent;'>
            <div style='Margin: 0 auto;min-width: 320px;max-width: 745px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;' class='block-grid '>
              <div style='border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;'>
                  <div class='col num12' style='min-width: 320px;max-width: 745px;display: table-cell;vertical-align: top;'>
                    <div style='background-color: transparent; width: 100% !important;'>
                    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
                          <div align='center' class='img-container center  autowidth ' style='padding-right: 0px;  padding-left: 0px;'>
  <img class='center  autowidth ' align='center' border='0' src='cid:track_new' alt='Image' title='Image' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 500px' width='500'>
</div>
<div style='font-family: Tahoma, sans-serif;line-height:120%;color:#0D0D0D; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>	
	<div style='font-size:12px;line-height:14px;color:#0D0D0D;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 17px;text-align: center'><span style='font-size: 28px; line-height: 33px;'><strong><span style='line-height: 33px; font-size: 28px;'>Solicitud de cambio de fecha y hora para la orte <span style='font-size: 36px; line-height: 43px;'>$orte</span></span></strong></span><br><span style='font-size: 28px; line-height: 33px;'>$motivo.</span></p></div>	
</div>

<div style='font-family: Tahoma, sans-serif;line-height:150%;color:#555555; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>	
	<div style='font-size:12px;line-height:18px;color:#555555;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 21px;text-align: center'>Realizar el cambio.</p></div>	
</div>                  
  <div style='font-size: 16px;font-family: Tahoma, sans-serif; text-align: center;'>
 <style>
.tabl{
  border: 0px solid #ccc;
  border-collapse: collapse;
  margin: 0;
  padding: 0;
  width: 100%;
  table-layout: fixed;
}
.capt{
  font-size: 1.5em;
  margin: .5em 0 .75em;
}
.tr {
  background: #fff;
  border: 1px solid #ddd;
  padding: .35em;
}
.thtd {
  padding: .625em;
  text-align: center;
}
.th {
  font-size: .85em;
  letter-spacing: .1em;
  text-transform: uppercase;
}
@media screen and (max-width: 600px) {
  #tabl {
    border: 0;
  }
  .thead {
    border: none;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
  }
  .trm {
    border-bottom: 3px solid #ddd;
    display: block;
    margin-bottom: .625em;
  }
  .tdm{
    border-bottom: 1px solid #ddd;
    display: block;
    font-size: .8em;
    text-align: center;
  }
   .tdb:before {
    content: attr(data-label);
    float: left;
    font-weight: bold;
    text-transform: uppercase;
  }
  .tdl:last-child {
    border-bottom: 0;
    }
  .catpm {
    font-size: 1.3em;
  }

}</style>  
  <table class='tabl'>
  <caption class='capt catpm'>Datos del evento</caption>
  <thead class='thead'>
    <tr class='tr trm'>
   <th class='thtd th'>Trabajo</th>
      <th class='thtd th'>Lugar</th>
      <th class='thtd th'>Cliente</th>
      <th class='thtd th'>Fecha</th>
    </tr>
  </thead>
  <tbody>
    <tr class='tr trm'>
   	  <td data-label='Trabajo:' class='thtd th tdm tdb tdl'>$trabajo</td>
      <td data-label='Lugar:' class='thtd th tdm tdb tdl'>$lugar</td>
      <td data-label='Cliente:' class='thtd th tdm tdb tdl'>$datosCliente</td>
      <td data-label='Fecha:' class='thtd th tdm tdb tdl'>$fecha</td>
    </tr>
  </tbody>
</table>  
</div>
        </div>
        </div>
      </div>
        </div>
            </div>
          </div>          <div style='background-color:transparent;'>
            <div style='Margin: 0 auto;min-width: 320px;max-width: 745px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;' class='block-grid '>
              <div style='border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;'>                         
                  <div class='col num12' style='min-width: 320px;max-width: 745px;display: table-cell;vertical-align: top;'>
                    <div style='background-color: transparent; width: 100% !important;'>
                    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
                          <div align='center' class='img-container center  autowidth ' style='padding-right: 0px;  padding-left: 0px;'>
  <img class='center  autowidth ' align='center' border='0' src='cid:divider' alt='div' title='Image' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 316px' width='316'>

</div>
<div style='font-family: Tahoma, sans-serif;line-height:150%;color:#555555; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>	
	<div style='font-size:12px;line-height:18px;color:#555555;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 21px;text-align: center'>Cambio solicitado por: <span style='color: rgb(168, 191, 111); font-size: 14px; line-height: 21px;'><strong>$usuarioSolicitador<br></strong></span></p></div>	
</div>

<div style='font-family: Tahoma, sans-serif;line-height:150%;color:#0D0D0D; padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 10px;'>	
	<div style='font-size:12px;line-height:18px;color:#0D0D0D;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 21px;text-align: center'>SAC REALIZAR LOS CAMBIOS A LA BREVEDAD POSIBLE.</p></div>	
</div>

                        
                          
<div align='center' class='button-container center' style='padding-right: 10px; padding-left: 10px; padding-top:25px; padding-bottom:10px;'>
    <a style='color: #ffffff; background-color: #044E7A; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 181px; width: 151px;width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent; padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px; font-family:  Tahoma, sans-serif; text-align: center; mso-border-alt: none;  text-decoration: none' href='http://resh.tracklink.pe/ope/historialSac' target='_blank'  >
      <span style='font-size:16px;line-height:32px;'>Realizar el cambio</span>
    </a>
</div>
                        
                          <div style='padding-right: 10px; padding-left: 10px; padding-top: 30px; padding-bottom: 10px;'>
  <div align='center'><div style='border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;'>&#160;</div></div>
</div>
                    </div>
                    </div>
                  </div>
                    </div>
            </div>
          </div>          <div style='background-color:transparent;'>
            <div style='Margin: 0 auto;min-width: 320px;max-width: 745px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #044E7A;' class='block-grid three-up'>
              <div style='border-collapse: collapse;display: table;width: 100%;background-color:#044E7A;'>
                  <div class='col num4' style='max-width: 320px;min-width: 248px;display: table-cell;vertical-align: top;'>
                    <div style='background-color: transparent; width: 100% !important;'>
                    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'>
                          
<div align='center' style='padding-right: 0px; padding-left: 0px; padding-bottom: 0px;'>
  <div style='line-height:15px;font-size:1px'>&#160;</div>
  <div style='display: table; max-width:131px;'>
    <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px'>
      <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
        <a href='https://www.facebook.com/' title='Facebook' target='_blank'>
          <img src='cid:facebook@2x' alt='Facebook' title='Facebook' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
        </a>
      <div style='line-height:5px;font-size:1px'>&#160;</div>
      </td></tr>
    </tbody></table>
    <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px'>
      <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
        <a href='https://twitter.com/' title='Twitter' target='_blank'>
          <img src='cid:twitter@2x' alt='Twitter' title='Twitter' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
        </a>
      <div style='line-height:5px;font-size:1px'>&#160;</div>
      </td></tr>
    </tbody></table>
    <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 0'>
      <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
        <a href='https://plus.google.com/' title='Google+' target='_blank'>
          <img src='cid:googleplus@2x' alt='Google+' title='Google+' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
        </a>
      <div style='line-height:5px;font-size:1px'>&#160;</div>
      </td></tr>
    </tbody></table>    
  </div>
</div>                        
                    </div>
                    </div>
                  </div>      
                  <div class='col num4' style='max-width: 320px;min-width: 248px;display: table-cell;vertical-align: top;'>
                    <div style='background-color: transparent; width: 100% !important;'>
                    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
<div style='font-family: Tahoma, sans-serif;line-height:120%;color:#a8bf6f; padding-right: 0px; padding-left: 0px; padding-top: 20px; padding-bottom: 0px;'>	
	<div style='font-size:12px;line-height:14px;color:#a8bf6f;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px;text-align: center'><span style='color: rgb(255, 255, 255); font-size: 12px; line-height: 14px;'><span style='font-size: 12px; line-height: 14px; color: rgb(168, 191, 111);'>Cel.: $cel</span></span><br></p></div>	
</div>
                    </div>
                    </div>
                  </div>
                  <div class='col num4' style='max-width: 320px;min-width: 248px;display: table-cell;vertical-align: top;'>
                    <div style='background-color: transparent; width: 100% !important;'>
                    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
<div style='font-family: Tahoma, sans-serif;line-height:120%;color:#a8bf6f; padding-right: 0px; padding-left: 0px; padding-top: 20px; padding-bottom: 0px;'>	
	<div style='font-size:12px;line-height:14px;color:#a8bf6f;font-family: Tahoma, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px;text-align: center'>Email <span style='color: rgb(255, 255, 255); font-size: 12px; line-height: 14px;'>$correo</span></p></div>	
</div>                        
                    </div>
                    </div>
                  </div>
                    </div>
            </div>
          </div>          <div style='background-color:transparent;'>
            <div style='Margin: 0 auto;min-width: 320px;max-width: 745px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
              <div style='border-collapse: collapse;display: table;width: 100%;background-color:transparent;'>                   
                  <div class='col num12' style='min-width: 320px;max-width: 745px;display: table-cell;vertical-align: top;'>
                    <div style='background-color: transparent; width: 100% !important;'>
                    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
                          <div style='padding-right: 30px; padding-left: 30px; padding-top: 30px; padding-bottom: 30px;'>
  <div align='center'><div style='border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;'>&#160;</div></div>
</div>
                    </div>
                    </div>
                  </div>
                    </div>
            </div>
          </div>  
       </td>
     </tr>
    </tbody>
  </table>
</body></html>";
$mail->isSMTP();
$mail->SetLanguage( 'es', '../../mailphp/PHPMailer/language/' );
$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->Host = gethostbyname('mail.tracklink.pe');
$mail->Port = 25;
$mail->SMTPAuth = false;
$mail->CharSet = 'UTF-8';
$mail->setFrom('no-responder@tracklink.pe','Resh');  
$mail->addReplyTo("pvasquez@tracklink.pe", 'Soporte resh'); 
// $mail->addAddress("asistentecomercial@tracklink.pe"); 
$mail->addAddress("sac@tracklink.pe"); 
$mail->addCC("ecavagneri@tracklink.pe");
$mail->addCC("cruiz@tracklink.pe");
$mail->addCC("llazaro@tracklink.pe");
// $mail->addAddress("scabrera@tracklink.pe",'Sergio Cabrera Torres'); 
// $mail->addAddress($usuarioEje->usuario,$usuarioEje->usuarioNombre." ".$usuarioEje->usuarioApellidoPaterno); 
// $mail->addCC($usuarioEje->usuario,$usuarioEje->usuarioNombre." ".$usuarioEje->usuarioApellidoPaterno);
$mail->AddEmbeddedImage('../../assets/img/track_new.png', 'track_new');
$mail->AddEmbeddedImage('../../assets/img/divider.png', 'divider');
$mail->AddEmbeddedImage('../../assets/img/rounder-up.png', 'rounder-up');
$mail->AddEmbeddedImage('../../assets/img/facebook@2x.png', 'facebook@2x');
$mail->AddEmbeddedImage('../../assets/img/twitter@2x.png', 'twitter@2x');
$mail->AddEmbeddedImage('../../assets/img/googleplus@2x.png', 'googleplus@2x');
$mail->addBCC("pruebas-ti@tracklink.pe");
$mail->Subject='Cambiar fecha de instalacion de ciente '.$datosCliente.' ORTE:  '.$orte;
$mail->msgHTML($htmlContenido1);
$mail->AltBody = 'Estimado(as) SAC cambiar fecha de instalación'; 
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {     
	echo "enviado";
}


	  ?>
	  
	  <?php

}
?>
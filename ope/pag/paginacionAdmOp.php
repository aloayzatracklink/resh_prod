<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }  
  $opeObj=new Operacion();
  $opeObj->estado='36';
  $cantidadO=$opeObj->obtenerActividadesPorEstadoCantidad();  
  $registrosPorPagina=15;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  $datos=$opeObj->obtenerActividadesPorEstado($posisionDePagina,$registrosPorPagina);
  $n=1;
?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?> 
    <?php if ($var->idChequeo==0): ?>

                <?php $datosTrama = TramaIntegracionTY::obtenerTramaIntegracionPorIdOperacion($var->idoperacion);  ?>
    <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>
      <?php else: ?>
      <?php $clienteObj=Onix::obtenerDatosDesdeIds($var->idcliente,$var->idvehiculo,$var->idlocation) ?>
    <?php endif ?>
    <tr   style="<?= $datosVehiculo->campana=="Toyota"?"border: 2px solid red !important":"" ?>">
    <td><?= $var->numeroOrte ?></td>  
      <td><?= $var->trabajo ?></td>
      <td><?= $var->lugarInstalacion ?></td>
        <td><?= formato($var->fechaInstalacion) ?></td>        
    <?php if ($var->idChequeo==0): ?>
       <td><?= ($clienteObj->tipoCliente=="Natural") ? $clienteObj->apellidosC." ".$clienteObj->nombre : $clienteObj->razonSocial ; ?></td>
       <td><?= $datosVehiculo->placa ?></td>
        <td><?= $datosVehiculo->chasis ?></td>
     <?php else: ?>
        <?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
     <td><?= $retVal = (!empty($clienteObj->nombre)) ? "$apellidos"." "."$clienteObj->nombre" : "$apellidos" ; ?></td>       
       <td><?= $clienteObj->placa ?></td>
        <td><?= $clienteObj->chasis ?></td>
      <?php endif ?>
        <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosVehiculo->campana</span>":$datosVehiculo->campana) ?></td>
        <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosTrama->tipo_transaccion</span>":'') ?></td>
        <td class="center ">
            <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#bateriasOModal"  data-id="<?= $var->idoperacion ?>">Elegir <i class="fa fa-cog"> </i></button>
    </td>  
    </tr>
<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="10">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="10" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>

<?php
require_once("../../includes/init.php");
if(isset($_POST)) {

   $datos=limpiar($_POST['datos']);
   $datos="%".$datos."%";

   $obj=homologation_client::ObtenerListClient($datos);$n=0;
   if($obj){
        foreach ($obj as $var):
            $n++; ?>
            <tr class="gradeX ">
                <td class="text-left "><span><i  class="atopciones fa fa-plus atplus<?= $n; ?>" id="atMostrar_Tabla<?= $n; ?>" data-id="<?= $n; ?>" style="cursor: pointer"></i>
                                                                          <i  class="atopciones fa fa-minus atminus<?= $n; ?>" id="atMostrar_Tabla<?= $n; ?>" data-id="<?= $n; ?>" style="cursor: pointer;display: none"></i></span>  <?= $n; ?></td>
                <td class="text-left">
                    <?= $var->cliente ?>
                </td>
                <td class="text-left"><?= $var->identdoc ?> </td>
                <td class="text-left">
                    <?= $var->mail?>
                </td>
                <td class="text-center"><?= $var->ttvehiculos ?>
                    <?php  $ttvehCli= homologation_client::ObtenerTTVehiclexClient($var->idcompany,$var->idlocation,$var->idcustomer);
                    echo  $ttvehCli->ttvehicleclient ;
                    ?>
                </td>
                <td class="text-center">
                    <?php  $ttveh= homologation_client::ObtenerTTVehicleHomologation($var->idcompany,$var->idlocation,$var->idcustomer);
                       echo  $ttveh->ttvehiculoHomolo ;
                    ?>
                </td>
            </tr>
            <tr id="atTabla_Mostrar<?= $n; ?>" style="display: none">
                <td></td>
                <td colspan="11">
                    <table class="table">
                        <thead>
                        <tr>
                            <th class="text-center table-cabecera-horario2" rowspan="2" width="20px">N°</th>
                            <th class="text-center table-cabecera-horario2" rowspan="2" width="40px">Chasis</th>
                            <th class="text-center table-cabecera-horario2" rowspan="2" width="30px">Placa</th>
                            <th class="text-center table-cabecera-horario2" rowspan="2" width="250px">Marca</th>
                            <th class="text-center table-cabecera-horario2" rowspan="2" width="50px">Modelo</th>
                            <th class="text-center table-cabecera-horario2" rowspan="2" width="50px">Año</th>
                            <th class="text-center table-cabecera-horario2" rowspan="2" width="50px">UnitId</th>
                            <th class="text-center table-cabecera-horario2" rowspan="2" width="50px"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $dobj=homologation_client::ObtenerListVehicle($var->idlocation,$var->idcompany,$var->idcustomer); $nn=0; ?>
                        <?php if($dobj) {
                            foreach ($dobj as $dfvar):
                                $nn++?>
                                <tr class="gradeX ">
                                    <td><?= $nn ?></td>
                                    <td class="text-left"><?= $dfvar->idchasis ?> </td>
                                    <td class="text-left"><?= $dfvar->license ?> </td>
                                    <td class="text-left"><?= $dfvar->descbrand ?> </td>
                                    <td class="text-left"><?= $dfvar->descmodel ?> </td>
                                    <td><?= $dfvar->yearmodel ?> </td>
                                    <td><?= $dfvar->idsim ?>
                                        <input  type="hidden" name="nidsim<?= "_".$n."_".$nn;?>" id="nidsim<?= "_".$n."_".$nn;?>" value="<?= $dfvar->idsim ?>"  >
                                    </td>
                                    <td>
                                        <?php $verifica=homologation_client::ObtenerVehicleHomologation($var->idlocation,$var->idcompany,$var->idcustomer,$dfvar->idvehicle);
                                        if($verifica->truevehicle > 0){
                                        ?>
                                            <input type="checkbox" disabled checked  name="nfact2" id="nfact2<?= "_".$n."_".$nn; ?>"  data-id2="<?= $n."_".$nn; ?>" data-padre2="<?= $n ?>" value="<?= $dfvar->idlocation.'_'.$dfvar->idcustomer.'_'.$dfvar->idcompany.'_'.$dfvar->idvehicle.'_'.$dfvar->idsim.'_'.$var->cliente.'_'.$var->mail; ?>">
                                        <?php } else {?>
                                            <input type="checkbox"  <?= $verifica->truevehicle>0?'disabled checked':''?>   name="nfact[]" id="nfact<?= "_".$n."_".$nn; ?>"  data-id="<?= $n."_".$nn; ?>" data-padre="<?= $n ?>" value="<?= $dfvar->idlocation.'_'.$dfvar->idcustomer.'_'.$dfvar->idcompany.'_'.$dfvar->idvehicle.'_'.$dfvar->idsim.'_||'.$var->lastname."_".$var->firstname.'||'.$var->mail; ?>">
                                        <?php } ?>
                                    </td>
                                </tr>
                             <?php endforeach; } else { ?>
                            <tr>
                                <td colspan="12" class="text-center">No hay información Registrada</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </td>
            </tr>

        <?php endforeach;?>
    <?php } else{?>
        <tr>
            <td colspan="16" class="text-center">No hay información Registrada</td>
        </tr>
    <?php }?>

<script>
    function attoggle(tableid){
        var id = $(tableid).data('id');
        $('.atplus'+id).toggle();
        $('.atminus'+id).toggle();
        $('#atTabla_Mostrar'+id).toggle();
    }

    $(document).ready(function(){
          $('.atopciones').on('click', function(){
              attoggle(this);
          });
    });

    $("input[id^='nfact_']").on("click",function(){
       var yo= $(this);
       var dpadre=$(yo).data("padre");
       $('input[id^="nfact_"]:checked').map(function(){
           $('input[id^="nfact_'+$(this).data("padre")+'"]').not('input[id^="nfact_'+dpadre+'"]').prop('checked', false);
           return this.value;
       }).get();
    });
</script>
<?php } ?>

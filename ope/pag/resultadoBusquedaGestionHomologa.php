<?php require_once("../../includes/init.php");

if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    if (isset($_POST["page"])) {
        $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
        if (!is_numeric($numeroDePagina)) {
            die('Numero de pagina incorrecta !');
        } //incase of invalid page number
    } else {
        $numeroDePagina = 1; //if there's no page number, set it to 1
    }
    $datos = limpiar($_POST['datos']);

    $objCre = new homologation_client;

    $cantidadO = $objCre->ttRegistros();
    $registrosPorPagina = 10;
    $paginasEnTotal = ceil($cantidadO->cantidad / $registrosPorPagina);
    $posisionDePagina = (($numeroDePagina - 1) * $registrosPorPagina);
    $iddabus="";
    if($datos!=="") {
        $datos = "%" . $datos . "%";
        $iddabus = homologation_client::ObtenerIdBusqPag($datos);
    }
    $obj=homologation_client::buscarIdHomloListPag($iddabus,$posisionDePagina,$registrosPorPagina);$n=0; ?>
    <?php if($obj){
        foreach ($obj as $var):
            $dataListPag = homologation_client::ObtenerDatosLisPag($var->idcompany,$var->idlocation,$var->idcustomer);
          $n++; ?>
            <tr class="gradeX">
                <td class="text-left "><span>
                    <i  class="codpciones fa fa-plus cvdplus<?= $n; ?>" data-id="<?= $n; ?>" style="cursor: pointer"></i>
                    <i  class="codpciones fa fa-minus cvdminus<?= $n; ?>"  data-id="<?= $n; ?>" style="cursor: pointer;display: none"></i></span>  <?= $n; ?>
                </td>
                <td class="text-left">
                    <?= $dataListPag->cliente ?>
                    <input type="hidden" name="idhomolo" value="<?= $var->idhomclient?>" >
                </td>
                <td class="text-left"><?= $dataListPag->identdoc ?> </td>
                <td class="text-left">
                    <?= $dataListPag->mail?>
                </td>
                <td class="text-center">
                    <?php  $ttveh= homologation_client::ObtenerTTVehicleHomologation($var->idcompany,$var->idlocation,$var->idcustomer);
                    echo  $ttveh->ttvehiculoHomolo ;
                    ?>
                </td>
                <td class="text-center">
                    <?php  $ttvehRecb= homologation_client::ObtenerTTVehicleRecibidos($var->idcompany,$var->idlocation,$var->idcustomer,108);
                    echo  $ttvehRecb->ttvehiculoHomoRecb ;
                    ?>
                </td>
                <td class="text-center">
                    <?php  $ttHomol= homologation_client::ObtenerTTVehicleHomologado($var->idcompany,$var->idlocation,$var->idcustomer,107);
                    echo  $ttHomol->ttvehiculoHomoRecb ;
                    ?>
                </td>
                <td class="text-center">
                    <form action="" method="post" name="FRMLINK" id="FRMLINK<?= $var->idhomclient ?>">
                        <button class="btn btn-primary ladda-button  ladda-button-dlink<?= $var->idhomclient?> btnlink<?= $var->idhomclient ?>" data-style="zoom-in" style="padding-top: 0;padding-bottom: 0" onclick="return btnLink(this)" data-tipo="blink"  data-Hlink="<?= $dataListPag->cliente ?>" data-id="<?= $var->idhomclient ?>"  id="btnlink<?= $var->idhomclient ?>" name="btnlink">Enviar Link</button>
                        <input type="text" name="idlinkHomo" id="idlinkHomo<?= $var->idhomclient ?>" disabled value="<?= $var->idhomclient; ?>" style="display:none;">
                        <input type="text" name="btnblink" id="btnblink<?=$var->idhomclient ?>" disabled value="" style="display:none;">
                    </form>
                </td>
            </tr>
            <tr id="dcdMostrar_Tabla<?= $n; ?>" style="display: none">
                <td></td>
                <td colspan="4">
                    <table class="table"  id="detailHomolog">
                        <thead>
                        <tr>
                            <th class="text-center table-cabecera-horario2" rowspan="2" colspan="1" width="20px">N°</th>
                            <th class="text-center table-cabecera-horario2" rowspan="2" colspan="1" width="40px">Chasis</th>
                            <th class="text-center table-cabecera-horario2" rowspan="2" colspan="1" width="30px">Placa</th>
                            <th class="text-center table-cabecera-horario2" rowspan="2" colspan="1" width="250px">Marca</th>
                            <th class="text-center table-cabecera-horario2" rowspan="2" colspan="1" width="50px">Modelo</th>
                            <th class="text-center table-cabecera-horario2" rowspan="2" colspan="1" width="50px">Año</th>
                            <th class="text-center table-cabecera-horario2" rowspan="1" colspan="2" width="50px">Fecha de Registro</th>
                            <th class="text-center table-cabecera-horario2" rowspan="2" colspan="1" width="50px">Estado</th>
                            <th class="text-center table-cabecera-horario2" rowspan="2" colspan="1" width="50px">Acciones</th>
                        </tr>
                        <tr>
                            <th class="text-center table-cabecera-horario2" rowspan="2" width="50px">Envío</th>
                            <th class="text-center table-cabecera-horario2" rowspan="2" width="50px">Recibido</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $dobj=homologation_client::ObtenerListVehicleDetPag($var->idhomclient);$nn=0;
                        ?>
                        <?php if($dobj) {
                            foreach ($dobj as $dfvar):
                                $dobj=homologation_client::ObtenerdatDetVehiclePag($var->idlocation,$var->idcompany,$var->idcustomer,$dfvar->idvehicle);
                                $nn++?>
                                <tr class="gradeX ">
                                    <td><?= $nn ?>
                                        <input type="hidden" name="datadethom" value="<?= $dfvar->idhomdetail ?>">
                                    </td>
                                    <td class="text-left"><?= $dobj->idchasis ?> </td>
                                    <td class="text-left"><?= $dobj->license ?> </td>
                                    <td class="text-left"><?= $dobj->descbrand ?> </td>
                                    <td class="text-left"><?= $dobj->descmodel ?> </td>
                                    <td><?= $dobj->yearmodel ?> </td>
                                    <td><?= $dfvar->creation_at ?></td>
                                    <td><?= $dfvar->create_documents_at ?></td>
                                    <td><?= obtenerEstadoQuery($dfvar->status) ?></td>
                                    <td class="text-center">
                                        <?php if($dfvar->status=='108'){ ?>
                                        <form action="" method="post" name="FRMANUL" id="FRMANUL<?= $dfvar->idhomdetail ?>">
                                            <button class="btn btn-danger ladda-button  ladda-button-demod<?= $dfvar->idhomdetail?> btnanular<?= $dfvar->idhomdetail ?>" data-style="zoom-in" style="padding-top: 0;padding-bottom: 0" onclick="return btnAnula(this)" data-tipo="anula"  data-vehicle="<?= $dobj->idchasis." | ".$dobj->license?>" data-id="<?= $dfvar->idhomdetail ?>"  id="btnanular<?= $dfvar->idhomdetail ?>" name="btnanular"> <i class="fa fa-trash-o"></i></button>
                                            <input type="text" name="idDetHomo" id="idDetHomo<?= $dfvar->idhomdetail ?>" disabled value="<?= $dfvar->idhomdetail; ?>" style="display:none;">
                                            <input type="text" name="btnanul" id="btnanul<?=$dfvar->idhomdetail ?>" disabled value="" style="display:none;">
                                        </form>
                                        <?php }else{ ?>
                                        <button class="btn btn-success"data-toggle="modal"  data-idInt="<?= $dfvar->idhomdetail  ?>"  data-target="#bateriasOModal" data-style="zoom-in" style="padding-top: 0;padding-bottom: 0" > <i class="fa fa-eye"></i></button>
                                        <?php }?>
                                    </td>
                                </tr>
                            <?php endforeach; } else { ?>
                            <tr>
                                <td colspan="12" class="text-center">No hay información Registrada</td>
                            </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </td>
            </tr>

        <?php endforeach;?>
        <tr>
            <td colspan="6"></td>
            <td colspan="2" class="text-right">Mostrando <?= $retVal = ($n<$registrosPorPagina) ? $n : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>

        </tr>
    <?php } else{?>
        <tr>
            <td colspan="16" class="text-center">No hay información Registrada</td>
        </tr>
    <?php }?>

<script>
    function ctdoggle(tableid){
        var id = $(tableid).data('id');
        $('.cvdplus'+id).toggle();
        $('.cvdminus'+id).toggle();
        $('#dcdMostrar_Tabla'+id).toggle();
    }
    $(document).ready(function(){
        $('.codpciones').on('click', function(){
              ctdoggle(this);
          });
    });




</script>
<?php } ?>

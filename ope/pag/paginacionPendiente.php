
<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }  
  $citaObj=false;
  $opeObj=false;
  if ($_POST['tipo']=='venta'){    
    $opeObj=new Operacion();
    $cantidadO=$opeObj->pendientesComCantidadTotal();  
  }else{    
    $citaObj=new Cita();
    $cantidadO=$citaObj->pendienteComCitaTotal();    
  }  
  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);

  if ($_POST['tipo']=='venta') {
      $datos=$opeObj->obtenerpendientesCom($posisionDePagina,$registrosPorPagina);
  } else {
      $datos=$citaObj->pendienteComCita($posisionDePagina,$registrosPorPagina);
    // $datos=$datosFacturacion->obtenerDatosParaFacturacionRango($posisionDePagina,$registrosPorPagina,$desde,$hasta);        
  }  
  $n=1;
?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?> 
  <?php if ($citaObj){ ?>
     <?php $eje=Usuario::obtenerUsuario($var->idcreador)  ?>
       <tr>
         <td><?= $n++ ?></td>
         <td><?= $var->nombreCliente ?></td>
         <td><?= $var->placa ?></td>
         <td><?= $eje->usuarioNombre." ".$eje->usuarioApellidoPaterno ?></td>
         <td><?= haceMinimo($var->fechaRegistro) ?></td>
         <td>
          <button class="btn btn-primary formCita"  style="padding-top: 0;padding-bottom: 0"  type="submit" value="<?php echo $var->idcita ?>"  >listo Sac <i class="fa fa-check-square-o"></i></button>&nbsp;          
         </td> 
       </tr>
  <?php }else{ ?>
        <?php $plataformaU=Usuario::obtenerUsuario($var->idCreador)  ?>
    <?php if ($var->idChequeo==0): ?>
    <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>
      <?php else: ?>
      <?php $clienteObj=Onix::obtenerDatosDesdeIds($var->idcliente,$var->idvehiculo,$var->idlocation) ?>
    <?php endif ?>
    <tr >
    <td><?= $n++ ?></td>  
    <?php if ($var->idChequeo==0): ?>
       <td><?= ($clienteObj->tipoCliente=="Natural") ? $clienteObj->apellidosC." ".$clienteObj->nombre : $clienteObj->razonSocial ; ?></td>
       <td><?= $datosVehiculo->placa ?></td>
     <?php else: ?>
        <?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
     <td><?= $retVal = (!empty($clienteObj->nombre)) ? "$apellidos"." "."$clienteObj->nombre" : "$apellidos" ; ?></td>       
       <td><?= $clienteObj->placa ?></td>
      <?php endif ?>
        <td><?= $plataformaU->usuarioNombre." ".$plataformaU->usuarioApellidoPaterno ?></td>
        <td><?= haceMinimo($var->fechaCreacion) ?></td>        
        <td>
            <button class="btn btn-primary formmm "  onclick="return" style="padding-top: 0;padding-bottom: 0" type="submit" value="<?php echo $var->idoperacion ?>">listo Sac <i class="fa fa-check-square-o"></i></button>&nbsp;
            
         </td>  
    </tr>

<?php } ?>
<?php endforeach ?>
<tr>
// <script>
$('.formmm').click(function(e) {      
  var idoperacion=$(this).val();
  $.confirm({
      theme: 'modern',
      title: '¿Marcar como lísto para  Sac?',
      content: 'Al marcar como sac esta información sera visualizada por Sac inmediatamente',
      buttons: {
          Confirmar: {
            btnClass: 'btn-blue',
            action:function () {   
             $.ajax({
                type : 'POST',
                url  : 'reporteE/lSac',
                data :  {idoperacion:idoperacion},
                success : function(data)
                {
                $("#filter").focus();                
              $.alert('Se realizo el cambio');                    
                }
            });        
            }
          },          
          Cancelar: {
            btnClass: 'btn-warning',
            action:function () {  
              $("#filter").focus(); 
              $.alert('No se realizo nigun cambio!');
            
            }
          }
      }
  });
});
$('.formCita').click(function(e) {      
  var idcita=$(this).val();
  $.confirm({
      theme: 'modern',
      title: '¿Marcar como lísto para  Sac?',
      content: 'Al marcar como sac esta información sera visualizada por Sac inmediatamente',
      buttons: {
          Confirmar: {
            btnClass: 'btn-blue',
            action:function () {   
             $.ajax({
                type : 'POST',
                url  : 'reporteE/lSac',
                data :  {idcita:idcita},
                success : function(data)
                {
              $("#filter").focus();                 
              $.alert('Se realizo el cambio');                    
                }
            });        
            }
          },          
          Cancelar: {
            btnClass: 'btn-warning',
            action:function () {  
              $("#filter").focus(); 
              $.alert('No se realizo nigun cambio!');
            
            }
          }
      }
  });
});

  
// });
</script>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="10">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="10" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>
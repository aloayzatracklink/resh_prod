<?php $nav="crearClientes" ;?>
<?php include("../includes/head.php") ?>
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>
<?php if (!$session->yaEstaIngresado()){ 
  irA("ingresar");
} ?>
<?php  
$obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->registrarOperaciones=="si"): ?> 
<?php 
$errores=array();
$echo=array();
$clienteObj=new Cliente();
$agregarVehiculoObj=new Vehiculo();
$facturacionObj=new DatosFacturacion();
$operacionesObj=new Operacion();
$objCalendario=new Calendario();
$objCanales= new Canales();
if (!isset($_POST['programarPendiente'])) {
    if (isset($_POST['crearCliente'])) {   
        if (Token::verificar($_POST['crearCliente'])) { 
        if (isset($_POST['dniORuc'])) {
            $dniORuc=trim(limpiar($_POST['dniORuc']));                
        }else{
            $dniORuc=false;                            
        }
        if (!$dniORuc) {
            $errores[]="Error en recibir informacion del cliente ";
        }
        if (!empty($_FILES['voucher']['name'])) {
        $extencionesAbilitadas= array('jpg','jpeg','png','gif','PNG','JPG','JPEG','PDF','DOCX','GIF','docx','pdf');
        $nombreArchivo=$_FILES['voucher']['name'];        
        $tmp=explode('.', $nombreArchivo);
        $extencionArchivo=end($tmp);
        $tamanioArchivo=$_FILES['voucher']['size'];
        $ubicacionTemporal=$_FILES['voucher']['tmp_name'];
            if (!in_array($extencionArchivo,$extencionesAbilitadas)) {
              $errores[]='La extencion de la imagen debe ser jpg jpeg png gif';
            }
            if ($tamanioArchivo>5242880) {
              $errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
            }
        }
        if ($n= strlen($dniORuc)==8){
            $clienteObj->tipoCliente=ucwords(strtolower("Natural"));
            $clienteObj->dni=$dniORuc;
            $clienteObj->nombre=ucwords(strtolower($_POST['nombres']));
            $clienteObj->apellidosC=ucwords(strtolower($_POST['apellidos']));
            $clienteObj->telefono=$_POST['celular'];
            $clienteObj->telefono1=$_POST['telefono5'];
            $clienteObj->telefono2=$_POST['telefono2'];
            $clienteObj->telefono3=$_POST['telefono'];            
            $clienteObj->correo=$_POST['correoCliente'];            
            $clienteObj->idCreador=$session->idusuario;
            if ($clienteObj->registrarClienteNatural()) {
                
            }else{
                $errores[]="Se detecto un error al registrar el cliente en empresa ";
            }
        }
        if ($n=strlen($dniORuc)==11) {
            $clienteObj->tipoCliente="Empresa";
            $clienteObj->ruc=$dniORuc;
            $clienteObj->razonSocial=ucwords(strtolower($_POST['apellidos']));
            $clienteObj->nombre=ucwords(strtolower($_POST['nombreCliente']));
            $clienteObj->telefono=$_POST['telefono'];
            if (isset($_POST['telefono1'])||isset($_POST['telefono3'])) {
                $clienteObj->telefono1=$_POST['telefono1'];                
                $clienteObj->telefono3=$_POST['telefono3'];
            }
            $clienteObj->telefono2=$_POST['telefono2'];
            $clienteObj->correo=$_POST['correoCliente'];
            $clienteObj->idCreador=$session->idusuario;
            $clienteObj->enOnix="si";
            if ($clienteObj->registrarClienteEmpresa()) {
                
            }else{
                $errores[]="Se detecto un error al registrar el cliente en empresa ";
            }
        }
        if (empty($errores)) {
                $agregarVehiculoObj->idcliente=$clienteObj->idcliente;
                $agregarVehiculoObj->plan=$_POST['plan'];
                $agregarVehiculoObj->placa=$_POST['placaVehiculo'];
                $agregarVehiculoObj->marca=$_POST['marca'];
                $agregarVehiculoObj->modelo=$_POST['modelo'];
                $agregarVehiculoObj->color=$_POST['color'];
                $agregarVehiculoObj->anio=$_POST['anioF'];
                $agregarVehiculoObj->chasis=$_POST['chasis'];
                $agregarVehiculoObj->motor=$_POST['motor'];
                $agregarVehiculoObj->planTiempo=$_POST['tiempo'];
                $agregarVehiculoObj->companiaSeguro=$_POST['seguro'];
                if (isset($_POST['campana'])) {
                  $agregarVehiculoObj->campana=htmlspecialchars($_POST['campana']);
                }else{
                  $agregarVehiculoObj->campana='no';                    
                }                  
                $agregarVehiculoObj->enOnix="si";
                if ($agregarVehiculoObj->registrarVehiculo($clienteObj->idcliente)) {
                    $facturacionObj->idcliente=$clienteObj->idcliente;
                    $facturacionObj->idvehiculos=$agregarVehiculoObj->idvehiculo;
                    $facturacionObj->tipoDocumentoFacturacion=$_POST['tipoDocumentoFacturacion'];
                    $facturacionObj->rucFacturacion=$_POST['rucFacturacion'];
                    $facturacionObj->dniFacturacion=$dniORuc;
                    $facturacionObj->razonSocialFacturacion=$_POST['razonSocialFacturacion'];
                    $facturacionObj->direccionFiscal=$_POST['direccionFiscalFacturacion'];
                    $facturacionObj->telefonoFacturacion=$_POST['telefonoFacturacion'];  
                    if (isset($_POST['entregaEnInstalacion'])) {
                        $facturacionObj->entregaEnInstalacion=$_POST['entregaEnInstalacion'];
                    }else{
                    $facturacionObj->direccionEntrega=$_POST['direccionEntrega'];
                    $facturacionObj->referenciaEntrega=$_POST['referenciaEntrega'];
                    $facturacionObj->distritoEntrega=$_POST['distritoEntrega'];
                    $facturacionObj->fechaHoraDeEntrega=$_POST['horarioEntrega'];                
                    $facturacionObj->fecha=$_POST['horarioEntrega'];
                    $facturacionObj->registrarSugerenciaDeFechaEntrega();                     
                    $facturacionObj->recibidoPor=$_POST['personaEntrega'];                        
                    }

                    $facturacionObj->plan=$_POST['plan'];
                    $facturacionObj->tiempo=$_POST['tiempo'];
                    $facturacionObj->costo=$_POST['precioLista'];
                    $facturacionObj->observaciones=$_POST['ordenNombre'];
                    $facturacionObj->idUsuarioCreado=$_POST['ejecutivoComercial'];
                $facturacionObj->tipoCanal=$_POST['tipoCanal'];
                if (isset($_POST['tienda'])) {
                    if (!empty($_POST['tienda'])) {
                        $facturacionObj->oficina=$_POST['tienda'];
                    }
                }
                if (isset($_POST['tienda1'])) {
                    if (!empty($_POST['tienda1'])) {
                        $facturacionObj->oficina=$_POST['tienda1'];
                    }
                }
                if (!empty($_POST['convenio'])) {
                    $facturacionObj->nombreCanal=$_POST['convenio'];    
                }else{
                    $facturacionObj->nombreCanal=$_POST['nombreCanal'];    
                }
                if (isset($_POST['ejecutivoCanalN'])) {
                    $facturacionObj->ejecutivoCanal=$_POST['ejecutivoCanalN'];    
                }


                    // if ($_POST['formaPago']=='Deposito'||$_POST['formaPago']=='OrdenDeCompra') {
                        
                    // }

                    $facturacionObj->observaciones=$_POST['formaPago'];
                    $fecha=date("YmdHis");
                    if (!empty($_FILES['voucher']['name'])) {  
                        move_uploaded_file($ubicacionTemporal, '../public/vouchers/'.$fecha.$nombreArchivo); 
                        $facturacionObj->voucher=$fecha.$nombreArchivo;
                    }

                    if ($facturacionObj->registrarDatosDeFacturacion()) {
                        $operacionesObj->datosfacturacionid=$facturacionObj->datosfacturacionid;
                        if (!$facturacionObj->ingresarOrden($facturacionObj->datosfacturacionid,$_POST['orden'])) {
                            $errores[]="error en guardar numero de orden en los datos de facturación";
                        }
                        $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
                        $operacionesObj->numeroOrden=$_POST['orden'];
                        if (isset($_POST['orte'])) {
                            $operacionesObj->numeroOrte=$_POST['orte'];
                        }
                        if (isset($_POST['cordenadasD']) ) {
                            $operacionesObj->coordenadasInstalacion= $_POST['cordenadasD'];
                        }
                        if (isset($_POST['concesionarioInstalacion'])) {
                            $operacionesObj->nombreConcecionarioInstalicion=$_POST['concesionarioInstalacion']; 
                        }    
                        $operacionesObj->observacion=$_POST['observacion'];
if ($_POST['lugarInstacion']=='Taller') {
    if (isset($_POST['fechaIT'])||isset($_POST['horaIT'])) {
        $operacionesObj->fechaInstalacion=$_POST['fechaIT']." ".$_POST['horaIT'];
    }
    $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
    $operacionesObj->departamentoi='15';
    $operacionesObj->provinciai='127';
    $operacionesObj->distritoi='1291';
    $operacionesObj->direccionInstacion=$_POST['direccionTaller'];
    $operacionesObj->nombreContacto=$_POST['nombreContactoIT'];
    $operacionesObj->numeroContacto=$_POST['numeroContactoIT'];

}else
if($_POST['lugarInstacion']=='Domicilio Cliente'){
    if (isset($_POST['fechaID'])||isset($_POST['horaID'])) {
        $operacionesObj->fechaInstalacion=$_POST['fechaID']." ".$_POST['horaID'];
    }
    $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
    $operacionesObj->nombreContacto=$_POST['nombreContactoID'];
    $operacionesObj->numeroContacto=$_POST['numeroContactoID'];
    $operacionesObj->departamentoi=$_POST['departamentoInstalacionD'];
    $operacionesObj->provinciai=$_POST['provinciaInstalacionD'];
    $operacionesObj->distritoi=$_POST['distritoInstalacionD'];
    $operacionesObj->direccionInstacion=$_POST['direccionInstalacionD']." Referencia ..".$_POST['referenciaInstalacionD'];    

}else
if($_POST['lugarInstacion']=='Concesionario'){
    if (isset($_POST['fechaIC'])|| isset($_POST['horaIC'])) {
        $operacionesObj->fechaInstalacion=$_POST['fechaIC']." ".$_POST['horaIC'];
    }
    $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];  
    $operacionesObj->nombreContacto=$_POST['nombreContactoIC'];
    $operacionesObj->numeroContacto=$_POST['numeroContactoIC'];
    $operacionesObj->departamentoi=$_POST['departamentoInstalacionC'];
    $operacionesObj->provinciai=$_POST['provinciaInstalacionC'];
    $operacionesObj->distritoi=$_POST['distritoInstalacionC'];
    $operacionesObj->direccionInstacion=$_POST['direccionInstalacionC']." Referencia ..".$_POST['referenciaInstalacionC'];
    $operacionesObj->nombreConcecionarioInstalicion=$_POST['concesionarioInstalacion'];

}

if (isset($_POST['pendienteInstacion'])) {
    $operacionesObj->estado='0';
}else{
    $operacionesObj->estado='36';

}

    $operacionesObj->idCreador=$_POST['ejecutivoComercial'];
    $operacionesObj->fechaConfirmacionOPE=date("YmdHis");
    $usuarioOp=Usuario::obtenerUsuario($_POST['ejecutivoComercial']);
    if ($usuarioOp) {
        $operacionesObj->idConfirmacionOPE=$usuarioOp->comercialOperaciones;        
    }

if ($operacionesObj->programarInstalacionDesdeCrearCliente($agregarVehiculoObj->idvehiculo,$clienteObj->idcliente)) {
    $operacionesObj->guardarNumeroDeOrden();
    if (isset($_POST['pendienteInstacion'])) {
         
    }else{        

    }

    
}
                    }else{
                        $errores[]="Error al registrar datos de facturación";
                    }

            }else{
                    $errores[]="Se detectaron errores al momento de registrar Vehículo";
            }
        }        

    }else{
        $errores[]="Error de token";
    }
}
}








if (isset($_POST['programarPendiente'])) {
 
   $operacionesObj->idoperacion=$_POST['idoperacion'];
   $operacionesObj->observacion=$_POST['observacion'];
   $operacionesObj->numeroOrte=$_POST['orte'];
if ($_POST['lugarInstacion']=='Taller') {
     if (isset($_POST['fechaIT'])||isset($_POST['horaIT'])) {
        $operacionesObj->fechaInstalacion=$_POST['fechaIT']." ".$_POST['horaIT'];
        $operacionesObj->estado=36;
    }else{
        $errores[]='Ingresar fecha y hora';
    }
    $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
    $operacionesObj->departamentoi='15';
    $operacionesObj->provinciai='127';
    $operacionesObj->distritoi='1291';
    $operacionesObj->direccionInstacion=$_POST['direccionTaller'];
    $operacionesObj->nombreContacto=$_POST['nombreContactoIT'];
    $operacionesObj->numeroContacto=$_POST['numeroContactoIT'];


}else
if($_POST['lugarInstacion']=='Domicilio Cliente'){
     if (isset($_POST['fechaID'])||isset($_POST['horaID'])) {
        $operacionesObj->fechaInstalacion=$_POST['fechaID']." ".$_POST['horaID'];
        $operacionesObj->estado=36;
    }else{
        $errores[]='Ingresar fecha y hora';
    }
    $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];
    $operacionesObj->nombreContacto=$_POST['nombreContactoID'];
    $operacionesObj->numeroContacto=$_POST['numeroContactoID'];
    $operacionesObj->departamentoi=$_POST['departamentoInstalacionD'];
    $operacionesObj->provinciai=$_POST['provinciaInstalacionD'];
    $operacionesObj->distritoi=$_POST['distritoInstalacionD'];
    $operacionesObj->direccionInstacion=$_POST['direccionInstalacionD']." Referencia ..".$_POST['referenciaInstalacionD'];
    

}else
if($_POST['lugarInstacion']=='Concesionario'){
     if (isset($_POST['fechaIC'])|| isset($_POST['horaIC'])) {
        $operacionesObj->fechaInstalacion=$_POST['fechaIC']." ".$_POST['horaIC'];
        $operacionesObj->estado=36;
    }else{
        $errores[]='Ingresar fecha y hora';
    }
    $operacionesObj->lugarInstalacion=$_POST['lugarInstacion'];  
    $operacionesObj->nombreContacto=$_POST['nombreContactoIC'];
    $operacionesObj->numeroContacto=$_POST['numeroContactoIC'];
    $operacionesObj->departamentoi=$_POST['departamentoInstalacionC'];
    $operacionesObj->provinciai=$_POST['provinciaInstalacionC'];
    $operacionesObj->distritoi=$_POST['distritoInstalacionC'];
    $operacionesObj->direccionInstacion=$_POST['direccionInstalacionC']." Referencia ..".$_POST['referenciaInstalacionC'];
    $operacionesObj->nombreConcecionarioInstalicion=$_POST['concesionarioInstalacion'];


}
if (empty($errores)) {    
    if($operacionesObj->programarFechaDeinstalacon()){
        $echo[]="Programado fecah de instalacion";
    }else{
        $errores[]="Algun error oucrrio x3424";
    }
}

    
}


?>
 <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
<link rel="stylesheet" href="../../assets/css2/style2.css">
<link rel="stylesheet" href="../../assets/css2/style3.css">
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">

  <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
   <style>
    .widget p {
        display: inline-block;        
    }

    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }

    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }

    .reloj .caja-segundos {
        display: inline-block;
    }

    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }</style>
       <style>
    .widget p {
        display: inline-block;        
    }

    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }

    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }

    .reloj .caja-segundos {
        display: inline-block;
    }

    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }</style>
    <body>

    <style>
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
}
</style>
<style>.pagination1{margin:0;padding:0;}
.pagination1 li{
    display: inline;
    padding: 6px 10px 6px 10px;
    border: 1px solid #ddd;
    margin-right: -1px;
    font: 15px/20px Arial, Helvetica, sans-serif;
    background: #FFFFFF;
    box-shadow: inset 1px 1px 5px #F4F4F4;
}
.pagination1 li a{
    text-decoration:none;
    color: rgb(89, 141, 235);
}
.pagination1 li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination1 li.last {
    border-radius: 0px 5px 5px 0px;
}
.pagination1 li:hover{
    background: #CFF;
}
.pagination1 li.active{
    background: #F0F0F0;
    color: #333;
}
</style>
<div class="se-pre-con"></div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

    <body>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <?php include("../includes/menuIzquierdo.php") ?>

               </div>
           </nav>

           <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>

                </nav>
            </div>
            <div class="wrapper wrapper-content">

                <div class="row ibox float-e-margins ibox-content ">
                    <div class="col-md-12">
                        <div class="row">
                           <div class="col-lg-6">
                        <!-- <form action="" method="post"> -->
                            <div class="row">
                                <div class="col-md-5">
                                    <input type="text" style="border:none;" class="col-md-6 form-control border-bottom" placeholder="orden" id="orden" name="orden" >
                                </div>
                                <div class="col-md-2">
                                    <input type="hidden" name="token" value="<?php echo Token::generar() ?>">
                                <div class="btn btn-primary" name="subir" data-toggle="modal" data-target="#bateriasOModal">Verificar</div></div>
                            </div>
                        <!-- </form> -->
                            
                        </div>

                        <div class="col-md-3 ">
                            <a class="btn btn-primary" href="historialSac">Ver Historial</a>
                        
                        </div>
                    
                        <div class="col-md-3">
                           <div class="widget" style="line-height: 0px;       
                           padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px">
                           <div class="fecha">
                            <p id="diaSemana" class="diaSemana">Martes</p>
                            <p id="dia" class="dia">27</p>
                            <p>de </p>
                            <p id="mes" class="mes">Octubre</p>
                            <p>del </p>
                            <p id="year" class="year">2015</p>
                        </div>

                        <div class="reloj">
                            <p id="horas" class="horas">11</p>
                            <p>:</p>
                            <p id="minutos" class="minutos">48</p>
                            <p>:</p>
                            <div class="caja-segundos">
                                <p id="ampm" class="ampm">AM</p>
                                <p id="segundos" class="segundos">12</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox float-e-margins col-lg-12">
        <div class="ibox-title">
            <h5>Instalaciones pendientes: Fecha y lugar</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </ul>
            <a class="close-link">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content" id="resultados">
        <input type="text" class="form-control input-sm m-b-xs" id="filter"
        placeholder="Buscar">
        <table class=" table  table-stripped table-hover table-responsive" >
            <thead>
                <tr>
                    <th>#</th>                    
                    <th>Nombres</th>
                    <th>Placa</th>
                    <th>Fecha Creada</th>
                    <th>Creado por</th>
                    <th>Estado</th>
                    <th>Acción</th>
                </tr>
            </thead>  
            <tbody  id="resultado">          
            </tbody>
        </table>
    </div>
</div>
<?php include("../includes/footer.php") ?>
</div>
</div>

</div>
<!-- LOS MODALES AQUI  -->

<div class="modal inmodal" id="bateriasOModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                <h4 class="modal-title">Crear cliente desde Onix</h4>
                <small class="font-bold"><div class="form-group ">
                </div></small>
            </div>
            <form  role="form" method="post" enctype="multipart/form-data" id="formOPE">

                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="submit" name="crearCliente" class="btn btn-primary" value="<?php echo Token::generar() ?>">Confirmar</button>
                </div>
            </form>   
        </div>
    </div>
</div>
<!-- LOS MODALES AQUI  -->


<!-- Mainly scripts -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/build/jquery.datetimepicker.full.js"></script>
 <?php include_once('../includes/script.php') ?> 
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
   
<script>

  $("#formOPE").validate({  });
$(document).ready(function(){
    $("#resultado" ).load("opeC/paginacion"); //load initial records
    $('.footable2').footable();
    //executes code below when user click on pagination links
    $("#resultado").on( "click", ".pagination1 a", function (e){
        e.preventDefault();
        $(".se-pre-con").show(); //show loading element
        var page = $(this).attr("data-page"); //get page number from link
        $("#resultado").load("opeC/paginacion",{"page":page}, function(){ //get content from PHP page
            $(".se-pre-con").hide(); //once done, hide loading element
        });        
    });

    $("#filter").keyup(function()
    {   
        var datosS = $(this).val();
        // datosS = datosS.replace(/[^0-9.]/g, '');
        if(datosS.length >= 1)
            {
        $.ajax({
                type : 'POST',
                url  : 'opeC/resultadosDeBusqueda',
                data :  'datosS='+ datosS,
                success : function(data)
                {
                    $("#resultado").html(data);
                }
            });
            return false;
        }else{
            $("#resultado" ).load("opeC/paginacion"); //load initial records    
        }
    });
});

//7/////Para importar los datos del cliente desde Onyx//ss
$('#bateriasOModal').on('show.bs.modal', function (e) {
    var rowid = $("#orden").val();
    if (rowid=='') {
        var ordenid = $(e.relatedTarget).data('id');
        $.ajax({
            type : 'post',
                    url : 'opeC/desdeOnixdataModal', //Here you will fetch records 
                    data :  'ordenid='+ ordenid, //Pass $id
                    success : function(data){
                    $('.modal-body').html(data);//Show fetched data from database
                }
            });
    }else{
        $.ajax({
        type : 'post',
                url : 'opeC/desdeOnixdataModal', //Here you will fetch records 
                data :  'rowid='+ rowid, //Pass $id
                success : function(data){
                $('.modal-body').html(data);//Show fetched data from database
            }
        });  
    } 

});

function limpiarOrden(){
     $("#orden").val('');
}
///////////////////////



$(document).ready(function(){

    $('.footable2').footable();
    $('.footable').footable();
    $('.footable3').footable();

});
function validarlugarInstalacion(){
  
  var lugarInstalacion=$('input[name="lugarInstacion"]:checked').val();
  
  if (lugarInstalacion=='Taller'){   
  document.getElementById("fechaIT").removeAttribute("disabled");
  document.getElementById("horaIT").removeAttribute("disabled");
  var horaCon = document.getElementById("horaIC"); 
  var fechaCon = document.getElementById("fechaIC"); 
  var horaDo = document.getElementById("horaID"); 
  var fechaDo = document.getElementById("fechaID"); 
  var concesionarioInstalacion = document.getElementById("concesionarioInstalacion"); 
  var cod = document.getElementById("cod"); 
  var att = document.createAttribute("disabled");     
  var att1= document.createAttribute("disabled");     
  var att2= document.createAttribute("disabled");     
  var att3= document.createAttribute("disabled");     
  var att4= document.createAttribute("disabled");     
  var att5= document.createAttribute("disabled");     
  att.horaCon = "true";
  att1.fechaCon = "true";
  att2.horaDo = "true";
  att3.fechaDo = "true";
  att4.cod = "true";
  att5.concesionarioInstalacion = "true";
  horaCon.setAttributeNode(att);
  fechaCon.setAttributeNode(att1);
  horaDo.setAttributeNode(att2);
  fechaDo.setAttributeNode(att3);
  cod.setAttributeNode(att4);
  concesionarioInstalacion.setAttributeNode(att5);

  
  }else if(lugarInstalacion=='Domicilio Cliente'){
  document.getElementById("horaID").removeAttribute("disabled");
  document.getElementById("fechaID").removeAttribute("disabled");
  document.getElementById("cod").removeAttribute("disabled");
    var horaCon = document.getElementById("horaIC"); 
  var fechaCon = document.getElementById("fechaIC"); 
  var horaDo = document.getElementById("fechaIT"); 
  var fechaDo = document.getElementById("horaIT"); 
  // var cod = document.getElementById("cod"); 
  var att = document.createAttribute("disabled");     
  var att1= document.createAttribute("disabled");     
  var att2= document.createAttribute("disabled");     
  var att3= document.createAttribute("disabled");     
  var att4= document.createAttribute("disabled");     
  var att5= document.createAttribute("disabled");     
  var concesionarioInstalacion= document.createAttribute("disabled");     
  att.horaCon = "true";
  att1.fechaCon = "true";
  att2.horaDo = "true";
  att3.fechaDo = "true";
  att4.cod = "true";
  att5.concesionarioInstalacion = "true";
  horaCon.setAttributeNode(att);
  fechaCon.setAttributeNode(att1);
  horaDo.setAttributeNode(att2);
  fechaDo.setAttributeNode(att3);
  // cod.setAttributeNode(att4);
  // concesionarioInstalacion.setAttributeNode(att5);

  }else if(lugarInstalacion=='Concesionario'){
  // alert(lugarInstalacion);
  document.getElementById("horaIC").removeAttribute("disabled");
  document.getElementById("fechaIC").removeAttribute("disabled");
  document.getElementById("concesionarioInstalacion").removeAttribute("disabled");
  var horaCon = document.getElementById("horaID"); 
  var fechaCon = document.getElementById("fechaID"); 
  var horaDo = document.getElementById("fechaIT"); 
  var fechaDo = document.getElementById("horaIT"); 
  var cod = document.getElementById("cod"); 
  var att = document.createAttribute("disabled");     
  var att1= document.createAttribute("disabled");     
  var att2= document.createAttribute("disabled");     
  var att3= document.createAttribute("disabled");     
  var att4= document.createAttribute("disabled");     
  att.horaCon = "true";
  att1.fechaCon = "true";
  att2.horaDo = "true";
  att3.fechaDo = "true";
  att4.cod = "true";
  horaCon.setAttributeNode(att);
  fechaCon.setAttributeNode(att1);
  horaDo.setAttributeNode(att2);
  fechaDo.setAttributeNode(att3);
  cod.setAttributeNode(att4);

  }

}

// function validarlugarInstalacionv(){}


(function(){
    var actualizarHora = function(){
        var fecha = new Date(),
        horas = fecha.getHours(),
        ampm,
        minutos = fecha.getMinutes(),
        segundos = fecha.getSeconds(),
        diaSemana = fecha.getDay(),
        dia = fecha.getDate(),
        mes = fecha.getMonth(),
        year = fecha.getFullYear();
        var pHoras = document.getElementById('horas'),
        pAMPM = document.getElementById('ampm'),
        pMinutos = document.getElementById('minutos'),
        pSegundos = document.getElementById('segundos'),
        pDiaSemana = document.getElementById('diaSemana'),
        pDia = document.getElementById('dia'),
        pMes = document.getElementById('mes'),
        pYear = document.getElementById('year');
        var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        pDiaSemana.textContent = semana[diaSemana];
        pDia.textContent = dia;
        var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        pMes.textContent = meses[mes];
        pYear.textContent = year;
        if (horas >= 12) {
            horas = horas - 12;
            ampm = 'PM';
        } else {
            ampm = 'AM';
        }
        // Detectamos cuando sean las 0 AM y transformamos a 12 AM
        if (horas == 0 ){
            horas = 12;
        }

        // Si queremos mostrar un cero antes de las horas ejecutamos este condicional
        // if (horas < 10){horas = '0' + horas;}
        pHoras.textContent = horas;
        pAMPM.textContent = ampm;

        // Minutos y Segundos
        if (minutos < 10){ minutos = "0" + minutos; }
        if (segundos < 10){ segundos = "0" + segundos; }

        pMinutos.textContent = minutos;
        pSegundos.textContent = segundos;
    };

    actualizarHora();
    var intervalo = setInterval(actualizarHora, 1000);
}())
</script>
  <script>
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
<?php else: ?> 
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?> 
<?php endif ?>

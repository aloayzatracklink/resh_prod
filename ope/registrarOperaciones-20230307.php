<?php $nav="registrarOperaciones" ;?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}
?>
<link href="../assets/calendario/fullcalendar.min.css" rel="stylesheet">
<link href="../assets/calendario/fullcalendar.print.min.css" rel='stylesheet' media='print'>
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<link href='../assets/calendario/scheduler.min.css' rel='stylesheet' />
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->registrarOperaciones=="si"): ?>
<?php
require_once("../includes/tablaProgramacion.php");
require_once '../mailphp/PHPMailer/PHPMailerAutoload.php';

$mail = new PHPMailer;
$operacionObj=new Operacion();
$datosInstalacion=new DatosInstalacion;
$facturacionObj=new DatosFacturacion();
$chequeoObj=new Chequeos;
$calendarioObj=new Calendario();
$calObj=new Calendario();
$tablaObj=new tablaProgramacion;
$errores=array();
$echo=array();
$tmp=false;




    if (isset($_POST['gestionarOperacion'])) {
        if (Token::verificar($_POST['gestionarOperacion'])) {
            //echo "<pre>";
            //die(var_dump($_POST));
            if (isset($_POST['orte'])) {
                $datosInstalacion->orte = limpiar($_POST['orte']);
                Notificaciones::cambiarDeEstado($_POST['orte']);
            }

            if (isset($_POST['ordenContrato'])) {
                $operacionObj->ordenContrato = $_POST['ordenContrato'];
            }

            if (isset($_POST['numeroOrte'])) {
                $datosInstalacion->orte = limpiar($_POST['numeroOrte']);
                Notificaciones::cambiarDeEstado($_POST['numeroOrte']);
            }
            if ($datosOb = $datosInstalacion->verificarSiOrteEstaenTMP()) {
                $tmp = true;
                $datosInstalacion->idoperacion = limpiar($_POST['idoperacion']);
                $datosInstalacion->iddatosInstalacion = $datosOb->iddatosInstalacion;
                if (!$datosInstalacion->agregaridOperacion()) {
                    $errores[] = "Error en agregar orte";
                }
            }
            Log::registrarLog("operaciones", $_POST['idoperacion'], $_POST, 'SAC Programacion');
            if (!isset($_POST['chequeo'])) {
                if (empty($_POST['idvh'])) {
                    $errores[] = "Fail detected contactarse con wilmanpabe@gmail.com";
                }

                if (empty($_POST['trabajo'])) {
                    $errores[] = "Error en registrar trabajo";
                }

                if (!isset($_POST['motivoReprogramacion'])) {
                    if (empty($_POST['orte'])) {
                        $errores[] = "N° orte es requerido";
                    }
                }
                if (empty($_POST['lugarInstacion'])) {
                    $errores[] = "Errror Lugar es requerido";
                } else
                    if (!empty($_POST['fechaIT'])) {
                        $fecha = $_POST['fechaIT'] . " " . $_POST['horaT'];
                        $operacionObj->direccionInstacion = 'Calle ricardo angulo 762';
                        $operacionObj->referenciaInstalacion = 'Altura del parque quiñones';
                        $operacionObj->departamentoi = '15';
                        $operacionObj->provinciai = '127';
                        $operacionObj->distritoi = '1281';
                        $operacionObj->nombreContacto = $_POST['nombreContactoIT']; //
                        $operacionObj->numeroContacto = $_POST['numeroContactoIT']; //
                    } else
                        if (!empty($_POST['fechaID'])) {
                            $fecha = $_POST['fechaID'] . " " . $_POST['horaD'];
                            $operacionObj->direccionInstacion = $_POST['direccionInstalacionD'];
                            $operacionObj->referenciaInstalacion = $_POST['referenciaInstalacionD'];
                            $operacionObj->departamentoi = $_POST['departamentoInstalacionD'];
                            $operacionObj->provinciai = $_POST['provinciaInstalacionD'];
                            $operacionObj->distritoi = $_POST['distritoInstalacionD'];
                            $operacionObj->nombreContacto = $_POST['nombreContactoID']; //
                            $operacionObj->numeroContacto = $_POST['numeroContactoID']; //
                        } else
                            if (!empty($_POST['fechaIC'])) {
                                $fecha = $_POST['fechaIC'] . " " . $_POST['horaC'];
                                $operacionObj->direccionInstacion = $_POST['direccionInstalacionC'];
                                $operacionObj->referenciaInstalacion = $_POST['referenciaInstalacionC'];
                                $operacionObj->departamentoi = $_POST['departamentoInstalacionC'];
                                $operacionObj->provinciai = $_POST['provinciaInstalacionC'];
                                $operacionObj->distritoi = $_POST['distritoInstalacionC'];
                                $operacionObj->nombreContacto = $_POST['nombreContactoIC']; //
                                $operacionObj->numeroContacto = $_POST['numeroContactoIC']; //
                            } else {
                                $errores[] = "Datos de instalacion.";
                            }
                $reprogramar = '';
                if (isset($_POST['motivoReprogramacion'])) {
                    //echo "<pre>";
                    //die(var_dump($_POST));
                    $reprogramar = 'si';
                    $idoperacion = $_POST['idoperacion'];
                    $operacionObj->idoperacion = $idoperacion;
                    $operacionObj->estado = '14';
                    $operacionObj->fechaInstalacion = $_POST['fechaInstalacionAnt'];
                    $nold_obs="";
                    if($_POST["old_observacion"])
                        $nold_obs = " , ".$_POST["old_observacion"];
                    $operacionObj->observacion = limpiar($_POST['motivoReprogramacion'])." ".$nold_obs;
                    if ($operacionObj->marcarComoReprogramar()) {
                        $calObj->idoperacion = $idoperacion;
                        $calObj->borrarChequeoConIdOperacion();
                        $echo[] = "Reprogramador por  " . $_POST['motivoReprogramacion'];
                        $tablaObj->idoperacion = $idoperacion;
                        $tablaObj->borrarEstacioOperacion();
                    } else {
                        $errores[] = "Error en reprogramacion";
                    }
                }
                if (empty($errores) && empty($reprogramar)) {
                    $idvehiculo = $_POST['idvh'];
                    $idoperacion = $_POST['idoperacion'];
                    $datosfacturacionid = $_POST['datosfacturacionid'];
                    $operacionObj->trabajo = $_POST['trabajo'];
                    $operacionObj->numeroOrte = $_POST['orte'];
                    if (isset($_POST['concesionarioInstalacion'])) {
                        $operacionObj->nombreConcecionarioInstalicion = $_POST['concesionarioInstalacion'];
                    }
                    $dir = $_POST['lugarInstacion'];
                    $operacionObj->lugarInstalacion = $dir;
                    $operacionObj->fechaInstalacion = $fecha;
                    $operacionObj->numeroOrden = $_POST['numOrden'];
                    if (isset($_POST['idtecnico'])) {
                        $operacionObj->idtecnicoAsig = $_POST['idtecnico'];
                    }
                    if ($operacionObj->confirmarInstalacion($idoperacion, $idvehiculo)) {
                        /*enviar correo a tecnico asignado*/
                        ob_start();
                        $datTec = Operacion::obtenerTecnico($_POST['idtecnico']);
                        $desctrab = $operacionObj->trabajo;
                        $ordContr = explode("-", $_POST['ordenContrato']);

                        $infoOpera = Operacion::obtenerInfoClienteOperacion($operacionObj->numeroOrte, $ordContr[1]);
                        include("../assets/email_templates/tencnicoCorreo/mailTecnicoOperacion.php");
                        $mail->isSMTP();
                        $mail->SetLanguage('es', '../mailphp/PHPMailer/language/');
                        $mail->SMTPDebug = 0;
                        $mail->Debugoutput = 'html';
                        $mail->Host = gethostbyname('mail.tracklink.pe');
                        $mail->Port = 25;
                        $mail->SMTPAuth = false;
                        $mail->CharSet = 'UTF-8';
                        $mail->setFrom('no-responder@tracklink.pe', 'Resh');
                        // $mail->addAddress("kcabrera@tracklink.pe");
                        $mail->addAddress($datTec->correotec);
                        $mail->addCC("cruiz@tracklink.pe");
                        //$mail->addCC("sramos@tracklink.pe");
                        //$mail->addCC("falvarez@tracklink.pe");
                        $mail->addCC("acarrasco@tracklink.pe");
                        $mail->addCC("malopez@tracklink.pe");
                        $mail->addCC("schocca@tracklink.pe");
                        $mail->addCC("mcarrera@tracklink.pe");
                        $mail->addCC("dmontes@tracklink.pe");
                        //$mail->addCC("kdelacruz@tracklink.pe");
                        //$mail->addCC("fsosa@tracklink.pe");
                        $mail->addCC("afeijoo@tracklink.pe");
                        $mail->addCC("scolquicocha@tracklink.pe");
                        $mail->addCC("llazaro@tracklink.pe");
                        $mail->addCC("asistentetaller@tracklink.pe");
                        $mail->addBCC("pruebas-ti@tracklink.pe");
                        //$mail->addAddress("aacuna@tracklink.pe");
                        $mail->Subject = $operacionObj->trabajo . "  a  " . $infoOpera->lastname . " " . $infoOpera->firstname . "  Orte : " . $operacionObj->numeroOrte;
                        $content = ob_get_clean();
                        $mail->msgHTML($content);
                        $mail->AltBody = '';
                        if (!$mail->send()) {
                            echo "Mailer Error: " . $mail->ErrorInfo;
                        } else {

                        }
                        //estado para actualizar...si existe tmp
                        if ($tmp) {
                            Log::registrarLog("operaciones", $_POST['idoperacion'], $_POST, 'TEMPORAL DESDE SAC INSTALACION');
                            $operacionObj->estado = $datosOb->estado;
                            $operacionObj->idoperacion = $idoperacion;
                            if ($operacionObj->cambiarEstadoOperacion()) {
                                $operacionObj->idvehiculo = $idvehiculo;
                                $operacionObj->cambiarEstadoVehclo();
                            } else {
                                $errores[] = "Error en cambiar estado tmp.";
                            }
                        }
                    }
                    $objVh = Vehiculo::obtenerVehiculo($idvehiculo);
                    $echo[] = "Installacion programada con exito <br> Placa " . $objVh->placa . " N° Orte : " . $_POST['orte'] . " ";
                }
            } else {
                if (!isset($_POST['chequeoLinea'])) {
                    if (isset($_POST['motivoReprogramacionChequeo'])) {
                        $idoperacion = $_POST['idoperacion'];
                        $chequeoObj->idchequeo = $_POST['idChequeo'];
                        $chequeoObj->observacion = $_POST['motivoReprogramacionChequeo'];
                        $operacionObj->estado = '14';
                        $operacionObj->fechaInstalacion = $_POST['fechaInstalacionAnt'];
                        $operacionObj->observacion = limpiar($_POST['motivoReprogramacionChequeo']);
                        $operacionObj->idoperacion = $idoperacion;
                        $operacionObj->observacion = $_POST['motivoReprogramacionChequeo'];
                        $operacionObj->guarDarIncidencia();
                        $chequeoObj->observacion = $_POST['motivoReprogramacionChequeo'];
                        if ($chequeoObj->marcarParaReprogramado()) {
                            if (!$operacionObj->marcarComoReprogramado()) {
                                $errores[] = 'Error al marcar como repromado';
                            } else {
                                $tablaObj->idoperacion = $idoperacion;
                                $tablaObj->borrarEstacioOperacion();
                                $echo[] = "Reprogramado con exito";
                            }
                        } else {
                            $errores[] = 'Error en guardar reprogramacion';
                        }
                    } else {
                        $calObj1 = new Calendario;
                        $operacionObj->idoperacion = $_POST['idoperacion'];
                        $_POST['idChequeo'];
                        if (empty($_POST['lugarInstacion'])) {
                            $errores[] = "Errror Lugar es requerido";
                        } else
                            if (!empty($_POST['fechaIT']) || !empty($_POST['horaIT'])) {
                                $fecha = $_POST['fechaIT'] . " " . $_POST['horaIT'];
                                $operacionObj->direccionInstacion = 'Calle ricardo angulo 762';
                                $operacionObj->referenciaInstalacion = 'Altura del parque quiñones';
                                $operacionObj->departamentoi = '15';
                                $operacionObj->provinciai = '127';
                                $operacionObj->distritoi = '1281';
                                $operacionObj->nombreContacto = $_POST['nombreContactoIT']; //
                                $operacionObj->numeroContacto = $_POST['numeroContactoIT']; //
                            } else
                                if (!empty($_POST['fechaID']) || !empty($_POST['horaID'])) {
                                    $fecha = $_POST['fechaID'] . " " . $_POST['horaID'];
                                    $operacionObj->direccionInstacion = $_POST['direccionInstalacionD'];
                                    $operacionObj->referenciaInstalacion = $_POST['referenciaInstalacionD'];
                                    $operacionObj->departamentoi = $_POST['departamentoInstalacionD'];
                                    $operacionObj->provinciai = $_POST['provinciaInstalacionD'];
                                    $operacionObj->distritoi = $_POST['distritoInstalacionD'];
                                    $operacionObj->nombreContacto = $_POST['nombreContactoID']; //
                                    $operacionObj->numeroContacto = $_POST['numeroContactoID']; //

                                } else
                                    if (!empty($_POST['fechaIC']) || !empty($_POST['horaIC'])) {
                                        $fecha = $_POST['fechaIC'] . " " . $_POST['horaIC'];
                                        $operacionObj->direccionInstacion = $_POST['direccionInstalacionC'];
                                        $operacionObj->referenciaInstalacion = $_POST['referenciaInstalacionC'];
                                        $operacionObj->departamentoi = $_POST['departamentoInstalacionC'];
                                        $operacionObj->provinciai = $_POST['provinciaInstalacionC'];
                                        $operacionObj->distritoi = $_POST['distritoInstalacionC'];
                                        $operacionObj->nombreContacto = $_POST['nombreContactoIC']; //
                                        $operacionObj->numeroContacto = $_POST['numeroContactoIC']; //
                                    } else {
                                        $errores[] = "Datos de instalacion.";
                                    }
                        // $operacionObj->idTecnicoAsignado=$_POST['idtecnico'];
                        $operacionObj->numeroOrte = $_POST['numeroOrte'];
                        $operacionObj->numeroOrden = $_POST['numeroOrden'];
                        $operacionObj->fechaInstalacion = $fecha;
                        $dir = $_POST['lugarInstacion'];
                        $operacionObj->lugarInstalacion = $dir;
                        if (isset($_POST['idtecnico'])) {
                            $operacionObj->idtecnicoAsig = $_POST['idtecnico'];
                        }
                        if ($operacionObj->guardarChequeoParaMonitoreo()) {
                            //correo tecnio en chequeo
                            if ($_POST['trabajoO'] == "Desinstalacion" || $_POST['trabajoO'] == "Chequeo por monitoreo" || $_POST['trabajoO'] == "Reinstalacion" || $_POST['trabajoO'] == "Desinstalacion") {
                                ob_start();
                                $datTec = Operacion::obtenerTecnico($_POST['idtecnico']);
                                $desctrab = $_POST['trabajoO'];
                                $ordContr = explode("-", $operacionObj->numeroOrden);
                                $infoOpera = Operacion::obtenerInfoClienteOperacion($operacionObj->numeroOrte, $ordContr[1]);
                                include("../assets/email_templates/tencnicoCorreo/mailTecnicoOperacion.php");
                                $mail->isSMTP();
                                $mail->SetLanguage('es', '../mailphp/PHPMailer/language/');
                                $mail->SMTPDebug = 0;
                                $mail->Debugoutput = 'html';
                                $mail->Host = gethostbyname('mail.tracklink.pe');
                                $mail->Port = 25;
                                $mail->SMTPAuth = false;
                                $mail->CharSet = 'UTF-8';
                                $mail->setFrom('no-responder@tracklink.pe', 'Resh');
                                //$mail->addAddress("kcabrera@tracklink.pe");
                                $mail->addAddress($datTec->correotec);
                                $mail->addCC("cruiz@tracklink.pe");
                                //$mail->addCC("fsosa@tracklink.pe");
                                $mail->addCC("afeijoo@tracklink.pe");
                                $mail->addCC("scolquicocha@tracklink.pe");
                                $mail->addCC("acarrasco@tracklink.pe");
                                $mail->addCC("malopez@tracklink.pe");
                                $mail->addCC("schocca@tracklink.pe");
                                $mail->addCC("mcarerra@tracklink.pe");
                                $mail->addCC("dmontes@tracklink.pe");
                                //$mail->addCC("kdelacruz@tracklink.pe");
                                //$mail->addCC("sramos@tracklink.pe");
                                //$mail->addCC("falvarez@tracklink.pe");
                                $mail->addBCC("pruebas-ti@tracklink.pe");
                                $mail->addCC("llazaro@tracklink.pe");
                                $mail->addCC("asistentetaller@tracklink.pe");
                                //$mail->addAddress("aacuna@tracklink.pe");
                                $mail->Subject = $desctrab . "  a  " . $infoOpera->lastname . " " . $infoOpera->firstname . "  Orte : " . $operacionObj->numeroOrte;
                                $content = ob_get_clean();
                                $mail->msgHTML($content);
                                $mail->AltBody = '';
                                if (!$mail->send()) {
                                    echo "Mailer Error: " . $mail->ErrorInfo;
                                } else {

                                }
                            }
                            if ($tmp) {
                                Log::registrarLog("operaciones", $_POST['idoperacion'], $_POST, 'TEMPORAL DESDE SAC CH DES');
                                $operacionObj->estado = $datosOb->estado;
                                if (!$operacionObj->cambiarEstadoOperacion()) {
                                    $errores[] = "Error en cambiar estado tmp.";
                                }
                            }
                            //cambiar estado verificando si esta en tmp
                            $trabajo = $_POST['trabajoO'];
                            $calObj1->idoperacion = $_POST['idoperacion'];
                            $calObj1->startdate = $operacionObj->fechaInstalacion;
                            $calObj1->title = $trabajo . " " . $_POST['lugarInstacion'] . " Placa: " . $_POST['placa'] . " Marca: " . $_POST['marca'] . " Modelo: " . $_POST['modelo'] . " " . "ORTE: " . $_POST['numeroOrte'];
                            if ($calObj1->confirmarFechaChequeo()) {
                                $echo[] = $trabajo . " con orte " . $_POST['numeroOrte'];
                            } else {
                                $errores[] = "Algun errro here :V 31231231312";
                            }
                        } else {
                            $errores[] = "Algun error ocurrio .#.3:#:#:##";
                        }
                    }


                } else {
                    $operacionObj->idoperacion = $_POST['idoperacion'];
                    $operacionObj->numeroOrden = $_POST['numeroOrden'];
                    if ($operacionObj->guardarComoChequeoEnlinea()) {
                        $echo[] = "Chequeo en linea guardado exitosamente";
                    } else {
                        $errores[] = "error fail fadf2f32";
                    }
                }
            }
        }
        else{
            $errores[]="El formulario ya fue procesado, verificar.";
        }
    }
?>
<link href="../assets/css/plugins/select2/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/css2/style2.css">
<link rel="stylesheet" href="../assets/css2/style3.css">
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
               <?php include("../includes/menuIzquierdo.php") ?>
           </div>
       </nav>
       <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../includes/navegacionArriba.php") ?>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                <div class="ibox float-e-margins col-lg-12">
                    <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-10">
                        <input type="text" class="form-control input-sm m-b-xs" id="filter"
                        placeholder="Filtrar vehículo">
                        </div>
                        <div class="col-md-2">
                                <a class="col-md-12 btn btn-primary" href="historialSac">Ver historial</a>
                        </div>
                    </div>
                        <hr>
                        <div class="table-responsive">
                        <table class="footable table table-hover"  data-filter='#filter'>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th >Trabajo</th>
                                    <th >Fecha programada</th>
                                    <th >Lugar</th>
                                    <th>Cliente</th>
                                    <th>Placa</th>
                                    <th>Campaña</th>
                                    <th>Tipo Registro</th>
                                    <th>A cargo de: </th>
                                    <th>Ejecutivo </th>
                                    <th>Acción</th>
                                    <th>Creado por</th>
                                </tr>
                            </thead>    
                            <tbody id="dataOperaciones">                              
                            </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
<!-- modal inicio -->
    <div class="modal inmodal" id="ordenes" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceIn">
                <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title">Gestionar</h4>
                </div>
                <form  role="form" method="post" >

                    <div class="modal-body" style="background: #fff;padding-top: 0">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                        <button type="submit" name="gestionarOperacion"  id="guardarOperacion"  class="btn btn-primary"  value="<?php echo Token::generar() ?>">Guardar Cambios</button>
                    </div>
                      <div class="row">
                                    <div class="col-lg-12">              
                                      <label id="mostrar" class="text-center text-danger">Limite de taller alcanzado por favor intenta en otro horario</label>
                                    </div>
                                  </div>
                </form>   
            </div>
        </div>
    </div>
    <!-- modal fin -->
</div>
<?php include("../includes/footer.php") ?>
</div>
</div>
</body>
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- FooTable -->

<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/select2/select2.full.min.js"></script>
<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
 <?php include_once('../includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
$('.footable').footable();
var eventos={}
eventos.frejcar =function(){
    $.ajax({
        url: 'ajax/datosOperaciones',
                type: 'POST', 
                async: true,
                success: function(data){
                    $('#dataOperaciones').html(data);
                }
            });
}

eventos.interval = setInterval(eventos.frejcar, 5000);
eventos.frejcar();

$("#filter").keyup(function() {
    var str=$(this).val();
    var n = str.length;
    if (n==0) {
        eventos.interval = setInterval(eventos.frejcar, 5000);
        eventos.frejcar();
    }else{
        clearInterval(eventos.interval);        
    }

});


$('#ordenes').on('show.bs.modal', function (e) {
    var rowid = $(e.relatedTarget).data('id');
    $('.modal-body').html("");
    $.ajax({
        type : 'post',
        url : 'modal/operacionesDataModal',
        data :  'rowid='+ rowid, 
        success : function(data){
        $('.modal-body').html(data);
    }
});
    });
$(".dropdown-toggle").click(function(){
   var fecha=$("#fecha").val();
   var tipo=$(this).val();
    $.ajax({
    url: 'ajax/simpleConsulta',
            type: 'POST', 
            data : {tipo:tipo,fecha:fecha},
            success: function(data){
                if (tipo='1') {
                    $('#t1').html(data);
                }
                if (tipo='2') {
                    $('#t2').html(data);
                }
                if (tipo='3') {
                    $('#t3').html(data);
                }
                if (tipo='4') {
                    $('#t4').html(data);
                }
            }
    });
});

          $('#mostrar').hide();
 function capacidad(cantidad){
   $('#agregarVehiculoBttn').prop('disabled', true);        
   $('#mostrar').show();           
  }
 function limpiarCapacidad(){
    $('#agregarVehiculoBttn').prop('disabled', false);        
    $('#mostrar').hide();  
  }


$(document).ready(function () {
    $('#guardarOperacion').click(function(e) {
        checked = $("#idtecnico").val().trim();
        //var dp=1;
        if(checked=="") {
            bootbox.alert({
                title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                message: "<label>Seleccione un Tecnico*</label>",
                size: 'small',
                callback: function () {
                    /*  l.ladda( 'stop' );*/
                }
            });
            return false;
        }
    });
});
$('body').on('hidden.bs.modal',function () {
    if($('.modal.in').length>0){
        $('body').addClass('modal-open');
    }
})
</script>
<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5";    ?> 
<?php endif ?>

<?php 
require_once("../../includes/init.php");
require_once("../../includes/tablaProgramacion.php");
if (!$session->yaEstaIngresado()) {
    echo "Actualiza e intenta nuevamente por favor";
}
$obMenu=Usuario::obtenerMenu($session->idusuario);
if ($obMenu->programacion=="si"){
$objProgr=new tablaProgramacion;
if (isset($_POST['rowid'])) {
$dato=explode(',', $_POST['rowid']);
$dato[0];
if ($dato[0]=='u') {
  	$idturno=soloNumero($dato[1]);
    $objProgr->idturno=$idturno;
  	$turnoDatos=$objProgr->obtenerDetallesDelTurno();
    ?>
    <div class="well" style="padding-top: 11px;
  padding-bottom: 10px;">
  <div class="row">
    <div class=" text-center form-group col-md-12 text-primary text-capitalize">
       <h4> Turno: <?= $idturno ?> Sede: <?php echo $turnoDatos->sede ?> </h4>
    </div>
</div>
</div>
<div class="row">
  <div class="form-group col-md-3">
    <label for="sede" class="">Sede</label>
    <input style="border: none;" type="text" placeholder="Sede" id="sede" name="sede" class="border-bottom form-control" required value="<?= $turnoDatos->sede ?>">
  </div>
  <div class="col-md-3"><label for="estado" class="">Estado</label>
  <select id='estado' readonly  style="border: none;" class="tecnico border-bottom form-control" required name="estado">
    <option class="" value="<?php  echo $turnoDatos->estado ?>"  selected ><?php  echo obtenerEstado($turnoDatos->estado) ?></option>
    <option value="37">Disponible</option>           
    <option value="92">No disponible</option>           
  </select>
   </div>
<div class="form-group col-md-3">
    <label for="actu" class="">Actualizar</label><br> 
    <button id="actualizarUsuarioEnTaller" class="btn btn-info" type='button'>Actualizar</button>
</div>
</div>
<div class="row">
    <div class="col-md-6"><h3>Turnos por defecto</h3></div>
    <div class="col-md-6"><h3><?= ($turnoDatos->fijo=='si')?"4":"2" ?>&nbsp;&nbsp;
        <form action="" method='post' class='form inline'>
            <input type="hidden" value="<?= $turnoDatos->idturno ?>" name='idturno'>
            <button class="btn btn-primary" name='fijochturnof' value='<?= ($turnoDatos->fijo=='si')?"":"si" ?>'>cambiar a <?= ($turnoDatos->fijo=='si')?"2":"4" ?></button>
        </form>
    </h3></div>

</div>
<div id="resultado"></div>
<script> 
$("#actualizarUsuarioEnTaller").click(function() {
    var idturno='<?= $idturno ?>';
    var estado=$('#estado').val();
    var sede=$('#sede').val();
    $.ajax({
        type : 'post',
        url : 'contro/arqto', 
        data :{idturno:idturno,estado:estado,sede:sede},
        success : function(data){
            $('#resultado').html(data);
        }
    });
});
</script>
    <?php
}
else if($dato[0]=='p'){
    $fecha=$_POST['dia'];
    $esSabado=new DateTime($fecha);
    $dia=$esSabado->format('D');
    if ($dia=='Sat') {
        $sabado=true;
        $t1='9';
        $t2='11';
        $t3='13';
        $t4='15';
        $tf='17';
        $tt1='09:00';
        $tt2='11:00';
        $ttp='13:00';
        $tt3='13:00';
        $tt4='15:00';
        $ttf='17:00';
    }else{
        $t1='9';
        $t2='11';
        $t3='14';
        $t4='16';
        $tf='18';
        $tt1='09:00';
        $tt2='11:00';
        $ttp='13:00';
        $tt3='14:00';
        $tt4='16:00';
        $ttf='18:00';
    }
    $hora=$dato[1];
    
    ?>
<input type="hidden" name='turno' value='<?= $dato[2] ?>'>
<input type="hidden" name='hora' value='<?= $dato[1] ?>'>
<input type="hidden" name='dia' value='<?= $_POST['dia'] ?>'>
<div class="row">
    <div class="col-md-12">
    <h3 class="text-center">Turno <?= $dato[2] ?></h3>
</div>
</div>
<div class="row">
    <div class="col-md-12">
    <input type="text" class="form-control obs" placeholder='Motivo de reserva'  id="obs" name="observacion" autocomplete="off" required  >
</div>
</div>
<div class="row obs">
    <div class="col-md-3">
	<label for="clientes"><?= $tt1 ?> - <?= $tt2 ?></label>
	<div class="switch">
		<div class="onoffswitch">
        <input type="checkbox" class="onoffswitch-checkbox  " id="1" value="<?= $t1 ?>" name="h1" <?= ($t1==$hora)?"checked":""?>>
			<label class="onoffswitch-label" for="1">
				<span class="onoffswitch-inner"></span>
				<span class="onoffswitch-switch"></span>
			</label>
		</div>
	</div>
</div>
    <div class="col-md-3">
	<label for="clientes"><?= $tt2 ?> - <?= $ttp ?></label>
	<div class="switch">
		<div class="onoffswitch">
			<input type="checkbox" class="onoffswitch-checkbox" id="2" value="<?= $t2 ?>" name="h2" <?= ($t2==$hora)?"checked":""?>>
			<label class="onoffswitch-label" for="2">
				<span class="onoffswitch-inner"></span>
				<span class="onoffswitch-switch"></span>
			</label>
		</div>
	</div>
</div>
    <div class="col-md-3">
	<label for="clientes"><?= $tt3 ?> - <?= $tt4 ?></label>
	<div class="switch">
		<div class="onoffswitch">
			<input type="checkbox" class="onoffswitch-checkbox" id="3" value="<?= $t3 ?>" name="h3" <?= ($t3==$hora)?"checked":""?>>
			<label class="onoffswitch-label" for="3">
				<span class="onoffswitch-inner"></span>
				<span class="onoffswitch-switch"></span>
			</label>
		</div>
	</div>
</div>
    <div class="col-md-3">
	<label for="clientes"> <?= $tt4 ?> - <?= $ttf ?></label>
	<div class="switch">
		<div class="onoffswitch">
            <input type="checkbox" class="onoffswitch-checkbox" id="4" value="<?= $t4 ?>" name="h4" <?= ($t4==$hora)?"checked":""?>>
			<label class="onoffswitch-label" for="4">
				<span class="onoffswitch-inner"></span>
				<span class="onoffswitch-switch"></span>
			</label>
		</div>
	</div>
    </div>
</div>
<div class="row obs">
<div class="form-group col-md-12">
		<label for="ejecutivoComercial" class="">reservar para</label>
         <select  style="border: none;" class="tecnico border-bottom form-control" required name="rpara" id="ejecutivoComercial">
          <option class="" value="0">Nadie</option>          	
         <?php $ejecutiosCom=Usuario::obtenerPARARESERVA() ?>            
          <?php foreach ($ejecutiosCom as $var): ?>
          <option class="" value="<?= $var->idusuario ?>"><?=  $var->usuarioNombre." ".$var->usuarioApellidoPaterno; ?></option>          	
          <?php endforeach ?>
        </select>
 </div>
</div>
<br>
<div class="row pri">
    <div class="col-md-12"><button class="btn btn-info col-md-12 reservar" type='button'>Reservar</button></div>
</div>
<div class="row sec">
    <div class="col-md-6"><button class="btn btn-info col-md-12 " name='reservar' type='submit'>Guardar</button></div>
    <div class="col-md-6"><button class="btn btn-default col-md-12 cancel" type='button'>Cancelar</button></div>
</div>
<script>
    $('.obs').prop('disabled',true);
    $('.obs').hide();
    $('.sec').hide();
    $('.reservar').click(function(){
        $('.pri').hide();
        $('.obs').show();
        $('.sec').show();
        $('.obs').prop('disabled',false);
        $('.obs').focus();
    })
    $('.cancel').click(function(){
        $('.sec').hide();
        $('.obs').prop('disabled',true);
        $('.obs').hide();
        $('.pri').show();
    })
</script>

    <?php
}else
    if($dato[0]=='o'){
    $idprogramacion=$dato[1];
    $objProgr->idtablaProgramacion=$idprogramacion;
    $datosPro=$objProgr->obteneDatosDeprogramacion();
    $objProgr->fecha=cambiarPorguion($datosPro->fecha);
    $objProgr->hora=$datosPro->hora;
    $datosPro->idturno;
    ?>
<input type='hidden' name='fecha' value='<?= $datosPro->fecha ?>' >
    <?php
    if ($datosPro->idoperacion<>'0') {
    $dataop=Operacion::obtenerTodolosDatosPoridOperacion($datosPro->idoperacion);

if (!empty($dataop->numeroOrte ) OR $dataop->numeroOrte<>'') {
    ?>
         <h1 class='text-center text-info'>Numero orte: <?= $dataop->numeroOrte ?></h1>

    <?php
}
?>

    <div class="row">
<!--        <div class="col-md-6"><button class="btn btn-default <?= ($datosPro->estado=='321')?'':'disabled' ?>"  name='liberarEspacioOPE' >librerar espacio</button></div>-->
    </div>
<input type='hidden' name='idoperacion' value='<?= $datosPro->idoperacion ?>' >
<div class="row">
    <div class="col-md-6"> 
              <label for="nombreContactoIT" class="">Trabajo</label>
              <input style="border: none;" type="text" placeholder="Trabajo" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $dataop->trabajo ?>">
            </div>
    <div class="col-md-6">
            <label for="numeroContactoIT" class="">Lugar</label>
            <input style="border: none;" type="text" placeholder="Lugar" id="" name="" class="form-control x2 text-capitalize border-bottom tal"  value="<?= $dataop->lugarInstalacion." ".$dataop->nombreConcecionarioInstalicion ?>">
    </div>
</div>
    <?php
    if ($dataop->idChequeo == 0){ 
        $datosF=DatosFacturacion::obtenerDatosFacturacionPorId($dataop->datosfacturacionid);
        $usD=Usuario::obtenerUsuario($datosF->idUsuarioCreado);
        $usDP=Usuario::obtenerUsuario($usD->comercialOperaciones);
        $eje=$usD->usuarioNombre." ".$usD->usuarioApellidoPaterno;
        $ope=$usDP->usuarioNombre." ".$usDP->usuarioApellidoPaterno;
        $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
        $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial); 
        $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
        ?>
<div class="row">
    <div class="col-md-12"> 
              <label for="" class="">Cliente</label>
              <input style="border: none;" type="text" placeholder="Cliente" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $nombreCliente ?>">
    </div>
</div>
<div class="row">
    <div class="col-md-3"> 
              <label for="" class="">Placa</label>
              <input style="border: none;" type="text" placeholder="Placa" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $datosVehiculo->placa ?>">
    </div>
<div class="col-md-3"> 
              <label for="" class="">Chasis</label>
              <input style="border: none;" type="text" placeholder="Chasis" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $datosVehiculo->chasis ?>">
    </div>
<div class="col-md-3"> 
              <label for="" class="">Motor</label>
              <input style="border: none;" type="text" placeholder="Motor" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $datosVehiculo->motor ?>">
    </div>
<div class="col-md-3"> 
              <label for="" class="">Marca</label>
              <input style="border: none;" type="text" placeholder="Marca" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $datosVehiculo->marca ?>">
    </div>
</div>
<div id="esconder">
<div class="row">
    <div class="col-md-3"> 
              <label for="" class="">Modelo</label>
              <input style="border: none;" type="text" placeholder="Modelo" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $datosVehiculo->modelo ?>">
    </div>
<div class="col-md-3"> 
              <label for="" class="">Color</label>
              <input style="border: none;" type="text" placeholder="Color" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $datosVehiculo->color ?>">
    </div>
<div class="col-md-3"> 
              <label for="" class="">Año</label>
              <input style="border: none;" type="text" placeholder="Año" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $datosVehiculo->anio ?>">
    </div>
<div class="col-md-3"> 
              <label for="" class="text-danger">PLAN</label>
              <input style="border: none;" type="text" placeholder="Plan" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $datosVehiculo->plan ?>">
    </div>
</div>

        <?php
    }else{ 
        $ope='';
        $eje='';
        $clienteObj = Onix::obtenerDatosDesdeIds($dataop->idcliente, $dataop->idvehiculo, $dataop->idlocation);

        if (is_object($clienteObj)) {
            $apellidos = isset($clienteObj->apellidos) ? utf8_encode($clienteObj->apellidos) : null;
            $nombreCliente = $apellidos . " " . $clienteObj->nombre;
        } else {
            // Manejar el caso en el que $clienteObj no es un objeto válido
            // Puedes mostrar un mensaje de error o realizar alguna acción alternativa
            $apellidos = null;
            $nombreCliente = '$firstname';
        }
        
    $chequeoObj=Chequeos::obtenerChequeoTrackPorId($dataop->idChequeo);
        ?>
 <div class="row">
    <div class="col-md-12"> 
              <label for="" class="">Cliente</label>
              <input style="border: none;" type="text" placeholder="Cliente" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $nombreCliente ?>">
    </div>
</div>           

<div class="row">
    <div class="col-md-3"> 
              <label for="" class="">Placa</label>
              <input style="border: none;" type="text" placeholder="Cliente" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $clienteObj->placa ?>">
    </div>
<div class="col-md-3"> 
              <label for="" class="">Chasis</label>
              <input style="border: none;" type="text" placeholder="Cliente" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $clienteObj->chasis ?>">
    </div>
<div class="col-md-3"> 
              <label for="" class="">Motor</label>
              <input style="border: none;" type="text" placeholder="Cliente" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $clienteObj->motor ?>">
    </div>
<div class="col-md-3"> 
              <label for="" class="">Marca</label>
              <input style="border: none;" type="text" placeholder="Cliente" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $clienteObj->marca ?>">
    </div>
</div>

<div id="esconder">
<div class="row">
    <div class="col-md-3"> 
              <label for="" class="">Modelo</label>
              <input style="border: none;" type="text" placeholder="Cliente" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $clienteObj->modelo ?>">
    </div>
<div class="col-md-3"> 
              <label for="" class="">Color</label>
              <input style="border: none;" type="text" placeholder="Cliente" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $clienteObj->color ?>">
    </div>
<div class="col-md-3"> 
              <label for="" class="">Año</label>
              <input style="border: none;" type="text" placeholder="Cliente" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $clienteObj->anio ?>">
    </div>
<div class="col-md-3"> 
              <label for="" class="text-danger">PLAN</label>
              <input style="border: none;" type="text" placeholder="Cliente" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $chequeoObj->plan ?>">
    </div>
</div>
<div class="row">
           <div class="form-group col-md-3">
    <label for="Observaciones" >Ubicacion de sistema*</label>
    <input  required readonly style="border: none;" type="text" placeholder="Ubicacion de sistema" id="Observaciones" name="ubsistema" class="border-bottom form-control"   value="<?= $chequeoObj->ubicacionSistema ?>">
  </div>      

    <div class="form-group col-md-5">
    <label for="numeroTecnico" >Fecha instalacion</label>
    <input  style="border: none;" readonly type="text" placeholder="Persona de contacto" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?= $chequeoObj->fechaInstalacion ?>"   >
  </div>
  <div class="form-group col-md-4">
    <label for="Observaciones" >Tecnico instalador *</label>
    <input required readonly="" style="border: none;" type="text" placeholder="Tecnico instalador* " id="Observaciones" name="tecnicoinstalador" class="border-bottom form-control"  value="<?= $chequeoObj->instalador ?>" >
  </div>

    </div>
        <?php
    }
?>
<div class="row">
    <div class="col-md-4"> 
              <label for="" class="">Distrito</label>
              <input style="border: none;" type="text" placeholder="Cliente" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $dataop->departamentoi ?>">
    </div>
<div class="col-md-4"> 
              <label for="" class="">Provincia</label>
              <input style="border: none;" type="text" placeholder="Cliente" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $dataop->provinciai ?>">
    </div>
<div class="col-md-4"> 
              <label for="" class="">Departamento</label>
              <input style="border: none;" type="text" placeholder="Cliente" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $dataop->distritoi ?>">
    </div>
</div>
<div class="row">
    <div class="col-md-4"> 
        <label for="" class="">Direccion</label>
        <input style="border: none;" type="text" placeholder="Cliente" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $dataop->direccionInstacion  ?>">
    </div>
<div class="col-md-4"> 
              <label for="" class="">Referencia</label>
              <input style="border: none;" type="text" placeholder="Cliente" name="" class="form-control x2 text-capitalize border-bottom tal" value="<?= $dataop->referenciaInstalacion ?>">
    </div>
<div class="col-md-4"> 
              <label for="" class="">Coordendas mapa</label>
  <div class="row"><div class="col-lg-12"><label for="cordenadas">Coordenadas de instalacion:  <?php echo $dataop->coordenadasInstalacion ?> </label><br><a  target="_blank" href="mapa.php?coordenadas=<?php echo $dataop->coordenadasInstalacion ?>&lugar=<?php echo $dataop->direccionInstacion ?>">ver mapa</a></div></div>    
    </div>
</div>
</div>
<div class="row">
    <div class="col-md-12">
    <button class="btn btn-info col-md-12" id='mostrarMas' type='button' >Mostrar mas datos</button>
    <button class="btn btn-info col-md-12" id='esconderDatos' type='button' >Esconder datos</button>
</div>
</div>
<div class="row">
    <div class="col-md-6"> 
              <label for="nombreContactoIT" class="">Nombre Contacto *</label>
              <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIT" name="nombreContactoIT" class="form-control x2 text-capitalize border-bottom tal" value="<?= $dataop->nombreContacto ?>">
            </div>
    <div class="col-md-6">
            <label for="numeroContactoIT" class="">Número Contacto *</label>
            <input style="border: none;" type="text" placeholder=" Número Contacto *" id="numeroContactoIT" name="numeroContactoIT" class="form-control x2 text-capitalize border-bottom tal"  value="<?= $dataop->numeroContacto ?>">
          </div>
</div>
<div class="row">
            <div class="col-md-6">
              <label for="nombreContactoIT" class="">Ejecutio ADV</label>
              <input style="border: none;" type="text" placeholder="Nombre Contacto *" class="form-control x2 text-capitalize border-bottom tal" value="<?= $ope ?>">
            </div>
            <div class="col-md-6">
                <label for="nombreContactoIT" class="">Tecnico Asignado</label>
                    <?php
                    $nomT=Operacion::obtenerTecnico($dataop->idtecnicoAsig);
                    ?>
                    <input style="border: none;" type="text" placeholder="Nombre Técnico *" class="form-control x2 text-capitalize border-bottom tal" value="<?= $nomT!=false?$nomT->nomtec:''; ?>">
            </div>
            <div class="col-md-12"> 
              <label for="nombreContactoIT" class="">Ejecutivo Comercial</label>
              <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIT" name="nombreContactoIT" class="form-control x2 text-capitalize border-bottom tal" value="<?= $eje ?>">
            </div>
    </div>
<?php
    }else if($datosPro->idcita<>'0'){
            ?>
    <input type='hidden' name='idcita' value='<?= $datosPro->idcita ?>' >
    <h1 class='text-center'><?= $datosPro->fecha ?> <?= $datosPro->hora ?></h1>
<div class="row">
        <div class="col-md-6"><button class="btn btn-default <?= ($datosPro->estado=='321')?'':'disabled' ?>"  name='liberarEspacioCita' >librerar espacio</button></div>
    </div>
<?php
        }else{
            ?>
<div class="row">
<div class="form-group col-md-12">
	<label for="ejecutivoComercial" class="">Reservado Para</label>
         <select  style="border: none;" class="tecnico border-bottom form-control" required name="rpara" id="ejecutivoComercial">
<?php 
	$ru=Usuario::obtenerUsuario($datosPro->rpara);
?>
	<option class="" value="<?= $ru->idusuario ?>"><?= $ru->usuarioNombre." ".$ru->usuarioApellidoPaterno ?></option>          	
         <?php $ejecutiosCom=Usuario::obtenerPARARESERVA() ?>            
          <?php foreach ($ejecutiosCom as $var): ?>
          <option class="" value="<?= $var->idusuario ?>"><?=  $var->usuarioNombre." ".$var->usuarioApellidoPaterno; ?></option>          	
          <?php endforeach ?>
        </select>
 </div>
 </div>
<div class="row">
<!--	<div class="col-md-6"><button class="btn btn-warning col-md-12" value="<?= $idprogramacion ?>" name='liberar'>Liberar reserva</button></div>
	<div class="col-md-6"><button class="btn btn-info col-md-12" value="<?= $idprogramacion ?>" name='cambiar'>Cambiar </button></div>-->
</div>
<br>
            <?php
        }
?>

<?php
    if ($datosPro->idtecnico=='0') {
        ?>
<br>
<div class="row">
    <div class="col-md-12"><button class="btn btn-primary col-md-12" id='cambiart'  type='button'>Asignar técnico</button></div>
</div>
<div class="row">
    <div class="col-md-12"><button class="btn btn-primary col-md-12" id='esconderT'  type='button'>Esconder técnicos</button></div>
</div>
        <?php
    }
?>
<div class="row" id='parac'>
    <div class="col-md-12">
<table class="table table-hover">
<thead>
    <tr>
        <th>Nombre</th> 
        <th>Accion</th>
    </tr>
</thead>
<tbody>
<?php
   $datosTEc=$objProgr->obtenerCantidadDETecnicosDisponiblesEnElHorario();
    if ($datosTEc) {
        foreach ($datosTEc as $var){
            ?>
    <tr>
        <td><?= $var->fecha." ".$var->hora ?></td>
        <td><button class="btn btn-success" name='tecnicoAsignado' value="<?= $var->idtecnico ?>">Seleccionar</button></td>
    </tr>
            <?php
        }
       ?>
       <?php 
    }
?>
</tbody>
</table>
</div>
</div>
<script>
$('#parac').hide();
$('#esconder').hide();
$('#esconderDatos').hide();
$('#esconderT').hide();

$('#cambiart').click(function(){
    $('#parac').show('slow');
    $('#esconderT').show();
    $('#cambiart').hide('slow');
})
$('#esconderT').click(function(){
    $('#parac').hide('slow');
    $('#esconderT').hide();
    $('#cambiart').show('slow');
})
$('#mostrarMas').click(function(){
    $('#esconder').show('slow');
    $('#mostrarMas').hide();
    $('#esconderDatos').show();
})
$('#esconderDatos').click(function(){
    $('#esconder').hide('slow');
    $('#mostrarMas').show();
    $('#esconderDatos').hide();
});

</script>

    <?php
}else{

}

?>
	<?php
}else{
    $fechaMostrar=new DateTime($_POST['dia']);
    $opOBj=new Operacion;
    $limiteDefecto=$opOBj->obtenerLimiteDefecto();
    //var_dump($limiteDefecto);
    ?>
<div class="row">
<h3 class="text-center txt-info" class='margin-top' style='margin-top:0px;margin-bottom:0px'>Limites por defecto para todos</h3>
<?php foreach ($limiteDefecto as $lim): ?> 
<div class="col-md-3">
 <label for=""><?= $lim->trabajo ?></label>
 <input class=" form-control" name='def_<?= $lim->trabajo ?>' type="text" value='<?= $lim->cantidad  ?>'>
 <input class=" form-control" name='def_last_<?= $lim->trabajo ?>' type="hidden" value='<?= $lim->cantidad  ?>'>
</div>
<?php endforeach; ?>
</div>
<hr>
<h3 class="text-center txt-info" class='margin-top' style='margin-top:0px;margin-bottom:0px'> Limites para el día <?= diaPARAMETRO($fechaMostrar->format('l'))." ".$fechaMostrar->format('d')." de ".strtolower(mesDesdeNumero($fechaMostrar->format('m'))) ?> </h3>

<hr>
<div class="row">
<?php foreach ($limiteDefecto as $limd): ?> 
<?php 
$opeD=Operacion::obtenerLimite($_POST['dia'],$limd->trabajo);
?>
<?php if($limd->trabajo!='Hora'):  ?>
<div class="col-md-4">
 <label for=""><?= $limd->trabajo ?></label>
 <input class=" form-control" name='<?= $limd->trabajo ?>' type="text" value='<?= $opeD  ?>'>
 <input class=" form-control" name='last_<?= $limd->trabajo ?>' type="hidden" value='<?= $opeD  ?>'>
</div>
<?php endif ?>
<?php endforeach; ?>
</div>

<div class="row">
<br>
<div class="col-md-12 text-center"> <button type='submit' class="text-center btn btn-warning" name='cambiarLimite' value='<?= $_POST['dia'] ?>' >Cambiar Limites</button></div>
</div>

<?php

}


}else{

    echo "Esta opcion no esta habilitado para tí";
}
?> 

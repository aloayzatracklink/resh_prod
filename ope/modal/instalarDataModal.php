	<?php 
if ($_POST['rowid']) {
  include_once("../../includes/init.php");
  	$idoperacion=htmlspecialchars(htmlentities($_POST['rowid']));
  	$operacionObj=Operacion::obtenerOperacionPorId($idoperacion);
	$clienteObj = Cliente::obtenerDatosDeclientePorId($operacionObj->idcliente);
	$vehiculoObj=Vehiculo::obtenerVehiculo($operacionObj->idvehiculo);
  	$clienteObj=Cliente::obtenerClientePorVehiculo($vehiculoObj->idvehiculo,$vehiculoObj->idcliente);

  ?>

    <link href="../../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
</head>
  <div class="well" style="padding-top: 11px;
  padding-bottom: 10px;">
  <div class="row">
    <div class="form-group col-md-4 text-primary text-capitalize">
      <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? "Nombres: " : "Razon social: " ; ?> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? $clienteObj->nombre." ".$clienteObj->apellidosC : $clienteObj->razonSocial ; ?>

    </div>
    <div class="form-group col-md-4">
      Placa: <?php echo $vehiculoObj->placa ?>
    </div>  
    <div class="form-group col-md-4">
     Estado: <?php echo obtenerEstado($vehiculoObj->estado) ?>
   </div>

 </div> 
 <div class="row">
 	
	<div class="form-group col-md-4">
      Plan: <?php echo $vehiculoObj->plan ?>
    </div>  
    <div class="form-group col-md-4">
     Chasis: <?php echo $vehiculoObj->chasis ?>
   </div>
   <div class="form-group col-md-4">
     Trabajo: <?php echo $operacionObj->trabajo ?>
   </div>

 </div>

    <?php if (!empty($vehiculoObj->serviciosAdicionales)): ?>
     <div class="row">
       <div class="form-group col-md-12">
       Servicios adicionales: <?php echo $vehiculoObj->serviciosAdicionales ?>
     </div>
     </div>
    <?php endif ?>


</div>



<?php 
if ($vehiculoObj->estado!=3) {
  ?>
  <div class="row">
    <div class="form-group col-md-4">
      <label for="trabajo" class="sr-only">Trabajo</label>
      <input style="border: none;" readonly type="text" placeholder="Trabajo" id="trabajo" name="trabajo" class="trabajo border-bottom form-control" required value="Instalacion">
    </div>

    <input type="hidden" name="idop" value="<?php echo $operacionObj->idoperacion ?>" >
    <input type="hidden" name="idve" value="<?php echo $vehiculoObj->idvehiculo ?>" >
    <input type="hidden" name="idTecnicoAsignado" value="<?php echo $operacionObj->idTecnicoAsignado ?>" >
   

    <div class="form-group col-md-4">
      <label for="tecnicoInstalador" class="sr-only">Tecnicos</label>
      <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="tecnicoInstalador">
        <option class="text-capitalize" value="<?php echo $operacionObj->idTecnicoAsignado ?>" selected ><?php $usuarioTecnico=Usuario::obtenerUsuario($operacionObj->idTecnicoAsignado);  echo $usuarioTecnico->usuarioNombre;echo "&nbsp;"; echo $usuarioTecnico->usuarioApellidoPaterno;?></option>
        <?php Usuario::obtenerTecnicos() ?>            
      </select>
    </div>
    <div class="form-group col-md-4">
      <label for="orte" class="sr-only">Orte</label>
      <input style="border: none;" type="text" placeholder="Numero de Orte " id="orte" name="orte" class="border-bottom form-control x2 " readonly value="<?php echo $operacionObj->numeroOrte ?>">
    </div>

  </div> 
  <hr>
  <h4>Datos de instalación</h4>
  <hr>
  <div class="row">          
  <div class="form-group col-md-4">
      <label for="imei" class="sr-only">IMEI</label>
      <input style="border: none;" type="text" placeholder="IMEI" id="imei" name="imei" class="border-bottom form-control" required value="<?php echo $retVal = (isset($operacionObj->imei)) ? $operacionObj->imei : "" ; ?>">
    </div>
    <div class="form-group col-md-4">
      <label for="id" class="sr-only">ID</label>
      <input style="border: none;" type="text" placeholder="ID" id="id" name="id" class="border-bottom form-control " required value="<?php echo $retVal = (isset($operacionObj->id)) ? $operacionObj->id : "" ; ?>">
    </div>
    <div class="form-group col-md-4">
      <label for="odometro" class="sr-only">Odometro</label>
      <input style="border: none;" type="text" placeholder="Odometro" id="odometro" name="odometro" class="border-bottom form-control " required value="<?php echo $retVal = (isset($operacionObj->odometro)) ? $operacionObj->odometro : "" ; ?>">
    </div>          
  </div>
  <div class="row">          
  <div class="form-group col-md-4">
      <label for="ubicacionSistema" class="sr-only">Ubicacion de sistema</label>
      <input style="border: none;" type="text" placeholder="Ubicacion de sistema" id="ubicacionSistema" name="ubicacionSistema" class="border-bottom form-control" required value="<?php echo $retVal = (isset($operacionObj->ubicacionSistema)) ? $operacionObj->ubicacionSistema : "" ; ?>">
    </div>
    <div class="form-group col-md-4">
      <label for="lugarInstalacion" class="sr-only">Lugar instalación</label>
      <input style="border: none;" type="text" placeholder="Lugar instalación" id="lugarInstalacion" name="lugarInstalacion" class="border-bottom form-control" value="<?php echo $retVal = (isset($operacionObj->lugarInstalacion)) ? $operacionObj->lugarInstalacion : "" ; ?><?php echo "&nbsp;"; echo $retVal = (isset($operacionObj->nombreConcecionarioInstalicion)) ? $operacionObj->nombreConcecionarioInstalicion : "" ; ?>">
    </div>
           
  <div class="form-group col-md-4">
      <label for="coordenadas" class="sr-only">Coordenadas</label>
      <input style="border: none;" type="text" placeholder="Coordenadas" id="coordenadas" name="coordenadas" class="border-bottom form-control" required value="<?php echo $retVal = (isset($operacionObj->coordenadas)) ? $operacionObj->coordenadas : "" ; ?>">
    </div>
  </div>
  <div class="row">          


     <div class="form-group col-md-12">
      <label for="observacion" class="sr-only">Observaciones</label>
      <input style="border: none;" type="text" placeholder="Observaciones" id="observacion" name="observacion" class="border-bottom form-control" value="<?php echo $retVal = (isset($operacionObj->observacion)) ? $operacionObj->observacion : "" ; ?>" >
    </div>

  </div> 

  		</div> <!-- name="pruebas" -->
       <fieldset>
        <h3>
         Pruebas realizadas
        </h3>

        <div class="checkbox checkbox-primary">
          <input id="checkbox2" type="checkbox"  name="ubicacion" value="si">
          <label for="checkbox2">
            Ubicación ok
          </label>
        </div>
      
        <div class="checkbox checkbox-warning">
          <input id="checkbox1" type="checkbox" checked name="noReporta" value="no">
          <label for="checkbox1">
            No reporta
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox4" type="checkbox" name="bloqueoDesbloqueo" value="si">
          <label for="checkbox4">
            Bloqueo / Desbloqueo
          </label>
        </div>
          <?php if ($vehiculoObj->plan != "Tranquilidad" ): ?>
            <?php if ($vehiculoObj->plan != "Basico"): ?>
              
        <div class="checkbox checkbox-info">
          <input id="checkbox9" type="checkbox" name="panico" value="si">
          <label for="checkbox9">
            Boton de panico
          </label>
        </div>      
        <div class="checkbox checkbox-info">
          <input id="checkbox5" type="checkbox" name="pestillos" value="si">
          <label for="checkbox5">
           Apertura de pestillos
          </label>
        </div>
            <?php if ($vehiculoObj->plan == "Control Total Carros"): ?>

        <div class="checkbox checkbox-info">
          <input id="checkbox6" type="checkbox" name="sensorCombustible" value="si">
          <label for="checkbox6">
            Sensor de combustible
          </label>
        </div>
         <div class="checkbox checkbox-info">
          <input id="checkbox7" type="checkbox" name="kit" value="si">
          <label for="checkbox7">
            Kit habla escucha
          </label>
        </div>
       
        <div class="checkbox checkbox-info">
          <input id="checkbox8" type="checkbox" name="alertaSonora" value="si">
          <label for="checkbox8">
           Alerta sonora por exeso de velocidad
          </label>
        </div>
            <?php endif ?>
            <?php endif ?>
          <?php endif ?>
      </fieldset>


<?php
} 
?>
<script src="../../assets/js/jquery-2.1.1.js"></script>

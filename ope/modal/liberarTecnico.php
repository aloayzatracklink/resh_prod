<?php
if ($_POST['rowid']) {
  include_once("../../includes/head.php");
  	$idtec=soloNumero($_POST['rowid']);
    $objtec=Tecnicos::obtenerTecnicos($idtec);
  	$view=$objtec->view_tciudad;
  	$viewubica=$objtec->ubicacion;

  	$ObjLibtec=Tecnicos::obternerLiberarTecnico($idtec);
  ?>
    <link href="../../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
    <link href="../../assets/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="../../assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <link href="../assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="../assets/css/style.css" rel="stylesheet">

    <div class="well" style="padding-top: 11px;padding-bottom: 10px;">
        <div class="row">
            <div class="col-md-12 form-group">
                <div class="col-md-3">
                    <p class="font-bold ">Alcance :</p>
                </div>
                <div class="col-md-3">
                    <select  style='border: none;' class='border-bottom form-control'  name='nviewciudad' id='nviewciudad' >
                        <option value='1' <?= $view==1?'selected':'' ?>>Departamento</option>
                        <option value='2' <?= $view==2?'selected':'' ?>>Provincia</option>
                        <option value='3' <?= $view==3?'selected':'' ?>>Distrito</option>
                    </select>
                </div>
            </div>
            <div  class="col-md-12 form-group">
                <div class="col-md-3">
                    <p class="font-bold" id="seleccionciudad">Seleccionar  :</p>
                </div>
                <div class="col-md-6 ldepartamento"  style="border:1px solid">
                    <select class="select2_demo_2 form-control" multiple="multiple" id="ldepartamento" name="ldepartamento" >
                        <?php $objDepa = Tecnicos::obtenerDepartamentos($idtec);?>
                        <?php if ($objDepa): ?>
                            <?php foreach ($objDepa as $var): ?>
                                <option value="<?= $var->iddepartamento ?>" <?= $var->tt==1?'selected':''; ?> ><?= $var->departamento?></option>
                            <?php endforeach ?>
                        <?php else: ?>
                        <?php endif ?>
                    </select>
                </div>
                <div class="col-md-6 lprovincias" style="border:1px solid" >
                    <select class="select2_demo_2 form-control" multiple="multiple" id="lprovincias" name="lprovincias"  >
                        <?php $objPronvicia = Tecnicos::obtenerProvincias($idtec);?>
                        <?php if ($objPronvicia): ?>
                            <?php foreach ($objPronvicia as $var): ?>
                                <option value="<?= $var->idprovincia ?>" <?= $var->tt==1?'selected':''; ?> >(<?= $var->departamento?>) -> <?= $var->provincia?></option>
                            <?php endforeach ?>
                        <?php else: ?>
                        <?php endif ?>
                    </select>
                </div>
                <div class="col-md-6 ldistrito"  style="border:1px solid">
                    <select class="select2_demo_2 form-control" multiple="multiple" id="ldistrito" name="ldistrito" >
                        <?php $objDistrito = Tecnicos::obtenerDistritos($idtec);?>
                        <?php if ($objDistrito): ?>
                            <?php foreach ($objDistrito as $var): ?>
                                <option value="<?= $var->iddistrito ?>" <?= $var->tt==1?'selected':''; ?> >(<?= $var->provincia?>) -> <?= $var->distrito?></option>
                            <?php endforeach ?>
                        <?php else: ?>
                        <?php endif ?>
                    </select>
                </div>
                <div class="col-md-2">
                    <a class="btn btn-primary ladda-button ladda-button-demolpt "data-style="zoom-in"  style="padding-top: 0;padding-bottom: 0"  id="guardarlp">Guardar <i class="fa fa-save"></i></a>
                    <input type="hidden" name="idtecnico" value="<?= $idtec ?>" >
                </div>
            </div>
        </div>
    </div>
    <div class="well" style="padding:11px 8px 10px">
        <div class="row">
            <div class="col-md-10">
            </div>
            <div class="col-md-2">
                <a class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  id="agregaLib">Agregar <i class="fa fa-plus-circle"></i></a>
                <input type="hidden" name="idtecnico" value="<?= $idtec ?>" >
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div id="table-scroll" class="table-scroll">
                    <div class="table-wrap" style=" height:200px; overflow:auto;">
                        <table class="main-table" id="liberarTec" >
                            <thead>
                                <tr>
                                    <th  class="text-center fixed-side table-cabecera-horario" width="20px">#</th>
                                    <th  class="text-center fixed-side table-cabecera-horario" width="260px">Fecha</th>
                                    <th  class="text-center fixed-side table-cabecera-horario" width="260px">Distrito</th>
                                    <th  class="text-center fixed-side table-cabecera-horario" width="175px">Hora Inicio</th>
                                    <th  class="text-center fixed-side table-cabecera-horario" width="175px">Hora Fin</th>
                                    <th  class="text-center fixed-side table-cabecera-horario" width="160px">Ubicación</th>
                                    <th  class="text-center fixed-side table-cabecera-horario" width="55px">Solo</th>
                                    <th  class="text-center fixed-side table-cabecera-horario" width="100px">Acción</th>
                                </tr>
                            </thead>
                            <tbody id="tbliberar">
                            <?php if(count($ObjLibtec)>0){
                             $n=1; foreach ($ObjLibtec as $var): ?>
                                <tr>
                                    <td class="text-center"><?= $var->idliberar; ?>
                                        <input type="text" style="display: none" name="idliberar" value="<?= $var->idliberar;?>" >
                                    </td>
                                    <td><p class="pfechal<?= $var->idliberar ?> "><?= $var->fecha ?></p>
                                        <div class='input-group date fechal<?= $var->idliberar ?>' style="display: none">
                                                <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                                                <input type='text' class='form-control tal tala fecha'  id='fechal' name='fechal' autocomplete='off' required value="<?= $var->fecha ?>">
                                        </div>
                                    </td>
                                    <td><p class="pdistr<?= $var->idliberar ?> "><?= $var->distrito ?></p>
                                            <select  style='border: none;display: none ' class='distr<?= $var->idliberar ?> border-bottom form-control'  id="distr" name="distr">
                                                <?php $objDist = Tecnicos::obtenerDistritos(0);?>
                                                <?php echo $var->iddistrito==0?"<option value='0' selected>Seleccionar</option>":"<option value='0'>Seleccionar</option>";  ?>
                                                <?php if ($objDist): ?>
                                                    <?php foreach ($objDist as $var2): ?>
                                                        <option value="<?= $var2->iddistrito ?>" <?= $var2->iddistrito==$var->iddistrito?'selected':''; ?> ><?= $var2->distrito ?> (<?= $var2->provincia ?>)</option>
                                                    <?php endforeach ?>
                                                <?php else: ?>
                                                <?php endif ?>
                                            </select>
                                    </td>
                                    <td><p class="phoraini<?= $var->idliberar ?>"><?= $var->hora_inicio ?></p>
                                        <div class='input-group clockpicker horaini<?= $var->idliberar ?>' data-autoclose='true' style="display: none">
                                            <input type='text'  class='form-control' readonly name='horaini' maxlength='5' value='<?= $var->hora_inicio ?>'>
                                            <span class='input-group-addon'>
                                                <span class='fa fa-clock-o'></span>
                                            </span>
                                        </div>
                                    </td>
                                    <td><p class="phorafin<?= $var->idliberar ?>"><?= $var->hora_fin ?></p>
                                        <div class='input-group clockpicker horafin<?= $var->idliberar ?>' data-autoclose='true' style="display: none">
                                            <input type='text'  class='form-control' readonly name='horafin' maxlength='5' value='<?= $var->hora_fin ?>'>
                                            <span class='input-group-addon'>
                                                <span class='fa fa-clock-o'></span>
                                            </span>
                                        </div>
                                    </td>
                                    <td><p class="pnubicacion<?= $var->idliberar ?>"><?= $var->ubicacion ?></p>
                                        <select  style='border: none; display: none' class='nubicacion<?= $var->idliberar ?> border-bottom form-control'  name='nubicacion' id='nubicacion' >
                                            <?php $objUbica = Tecnicos::obtenerUbicacion() ?>
                                            <?php if ($objUbica): ?>
                                                <?php foreach ($objUbica as $objUbi): ?>
                                                    <option value="<?=$objUbi->codigo ?>" <?= $objUbi->codigo==$var->ubicacion?'selected':'' ?> ><?= $objUbi->ubicacion ?></option>
                                                <?php endforeach ?>
                                            <?php else: ?>
                                            <?php endif ?>
                                        </select>
                                    </td>
                                    <td><p class="pshorario<?= $var->idliberar ?>"><?= $var->solohorario==1?'Si':'No' ?></p>
                                        <div class="checkbox checkbox-success shorario<?= $var->idliberar ?> <?= $var->solohorario == 1 ? 'checked' : '' ?>" <?=  $var->ubicacion==$viewubica?"":" checked disabled" ?>   style="display: none">
                                            <input id="shorario" type="checkbox" name="shorario" class="shorario<?= $var->idliberar ?>" <?= $var->solohorario == 1 ? 'checked' : '' ?> <?=  $var->ubicacion==$viewubica?"":" checked disabled" ?>
                                                   value="1" ><label for="shorario<?= $var->idliberar ?>"></label>
                                        </div>
                                    </td>
                                    <td>
                                        <a class='btn btn-info edit<?= $var->idliberar ?>'  style='padding-top: 0;padding-bottom: 0' href='#' id='editLib' ><i class='fa fa-edit'></i></a>
                                        <a class='btn btn-success ladda-button  ladda-button-demos<?= $var->idliberar ?> save<?= $var->idliberar ?>' data-style="zoom-in" style='padding-top: 0;padding-bottom: 0;display: none' href='#' id='agregaLib'><i class='fa fa-save'></i></a>
                                        <a class='btn btn-danger ladda-button  ladda-button-demod<?= $var->idliberar ?> del<?= $var->idliberar ?>' data-style="zoom-in" style='padding-top: 0;padding-bottom: 0' href='#' id='eliminarLib'><i class='fa fa-trash-o'></i></a>
                                        <a class='btn btn-warning cancel<?= $var->idliberar ?>' style='padding-top: 0;padding-bottom: 0 ;display: none' href='#' id='cancelLib'><i class='fa fa-ban'></i></a>
                                    </td>
                                </tr>
                                <?php endforeach ;} else{ echo '<tr><td class="text-center" colspan="8"> No hay Data Registrada </td></tr>';} ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../assets/build/jquery.datetimepicker.full.js"></script>
    <script src="../assets/bootbox/bootbox.all.js"></script>
    <script src="../assets/js/plugins/select2/select2.full.min.js"></script>

    <script src="../../assets/js/plugins/ladda/spin.min.js"></script>
    <script src="../../assets/js/plugins/ladda/ladda.min.js"></script>
    <script src="../../assets/js/plugins/ladda/ladda.jquery.min.js"></script>
    <script src="../../assets/js/plugins/toastr/toastr.min.js"></script>

    <style>
        .popover {
            z-index: 999999;
        }
        .select2-container--open  {
            z-index: 999999;
        }

        .table-cabecera-horario{
            background:#075598!important;
            color:white !important;
        }
        .table-scroll {
            position:relative;
            max-width:1600px;
            margin:auto;
            overflow:hidden;
            /*border:.5px solid #000;*/
        }
        .table-wrap {
            width:100%;
            overflow:auto;
        }
        .table-scroll table {
            width:100%;
            margin:auto;
            border-collapse:separate;
            border-spacing:0;
        }
        .table-scroll th, .table-scroll td {
            padding:5px 10px;
            border-bottom:1px solid;
            border-right:1px solid;
            border-color: #DDDDDD;;
            background:#fff;
            color:rgb(103, 106, 108) ;
            white-space:nowrap;
            vertical-align:top;
            font-size: 12px
        }
        .table-scroll thead, .table-scroll tfoot {
            background: #f9f9f9;
        }

    </style>
    <script>

    $(".select2_demo_2").select2();

    $nn=1;$dn="Temp_";
    $ubica=$("#dubica").val();
    var newRowContent = "";
    $("#agregaLib").click(function (){
        $("#tbliberar tr").each(function(index,element){
            //console.log($.trim($(this).context.textContent));
            /*if(index != 0) // ignoramos el primer indice que dice Option #
            {*/
                if($.trim($(this).context.textContent).indexOf("No hay Data Registrada")==0){
                    $('#tbliberar tr:nth-child(1)').hide();
                }
            /*}*/
        });
        newRowContent = "<tr><td class='text-center'>"+($dn+""+$nn)+"<input type='text' style='display: none' name='idtecnico' value="+($dn+""+$nn)+" > </td>" +
            "<td>" +
                "<div class='input-group date fechal"+($dn+""+$nn)+"'>"+
                    "<span class='input-group-addon'><i class='fa fa-calendar'></i></span>"+
                    "<input type='text' class='form-control tal tala fecha'  id='fechal' name='fechal' autocomplete='off' required>"+
                "</div>"+
            "</td>" +
            "<td> "+
            "<select  style='border: none;' class='distr<?= $var->idliberar ?> border-bottom form-control'  id='distr' name='distr'>"+
            <?php $objDist = Tecnicos::obtenerDistritos(0);?>
            "<option value='0'>Seleccionar</option>"+
            <?php if ($objDist): ?>
            <?php foreach ($objDist as $var2): ?>
           "<option value='<?= $var2->iddistrito ?>'><?= $var2->distrito ?> (<?= $var2->provincia ?>)</option>"+
            <?php endforeach ?>
            <?php else: ?>
            <?php endif ?>
           " </select>"+
            "</td>"+
        "<td>" +
        "<div class='input-group clockpicker horaini"+($dn+""+$nn)+"' data-autoclose='true'>"+
        "<input type='text'  class='form-control' readonly name='horaini' maxlength='5' value=''>"+
        "<span class='input-group-addon'>"+
        "<span class='fa fa-clock-o'></span>"+
        "</span>"+
        "</div>"+
        "</td>" +
        "<td>" +
                "<div class='input-group clockpicker horafin"+($dn+""+$nn)+"' data-autoclose='true'>"+
                    "<input type='text'  class='form-control' readonly name='horafin' maxlength='5' value=''>"+
                    "<span class='input-group-addon'>"+
                    "<span class='fa fa-clock-o'></span>"+
                    "</span>"+
                "</div>"+
            "</td>" +
            "<td>" +
                "<select  style='border: none;' class='nubicacion"+($dn+""+$nn)+" border-bottom form-control nubicacion'  name='nubicacion' id='nubicacion'>"+
                    <?php $objUbica = Tecnicos::obtenerUbicacion();?>
                    <?php if ($objUbica): ?>
                    <?php foreach ($objUbica as $objU): ?>
                    "<option value='<?= $objU->codigo ?>'><?= $objU->ubicacion ?></option>"+
                    <?php endforeach ?>
                    <?php else: ?>
                    <?php endif ?>
                "</select>"+
            "</td> " +
            "<td>"+
            "<div class='checkbox checkbox-success shorario"+($dn+""+$nn)+"' "+($ubica!='General'?'disabled checked':'')+" >"+
            "<input id='shorario' type='checkbox' name='shorario'  class='shorario"+($dn+""+$nn)+"' "+($ubica!='General'?'disabled checked':'')+" value='1' ><label for='shorario"+($dn+""+$nn)+"'></label>"+
            "</div>"+
            "</td>"+
            "<td>"+
            "<a class='btn btn-info  edit"+($dn+""+$nn)+"' style='padding-top: 0;padding-bottom: 0;display: none' href='#' id='editLib' ><i class='fa fa-edit'></i></a>"+
                " <a class='btn btn-success ladda-button  ladda-button-demos"+($dn+""+$nn)+"  save"+($dn+""+$nn)+"' data-style='zoom-in' style='padding-top: 0;padding-bottom: 0;' href='#' id='agregaLib'><i class='fa fa-save'></i></a>"+
                " <a class='btn btn-danger ladda-button  ladda-button-demod"+($dn+""+$nn)+"  del"+($dn+""+$nn)+"' data-style='zoom-in' style='padding-top: 0;padding-bottom: 0' href='#' id='eliminarLib'><i class='fa fa-trash-o'></i></a>"+
                " <a class='btn btn-warning cancel"+($dn+""+$nn)+"' style='padding-top: 0;padding-bottom: 0 ;display: none' href='#' id='cancelLib'><i class='fa fa-ban'></i></a></td>"+
        "</tr>";

        $nn++;
        $(newRowContent).appendTo($("#tbliberar"));
        $('.clockpicker').clockpicker();

        <?php $fechaA=date('Y-m-d') ?>
//        jQuery.datetimepicker.setLocale('es');

        $('.fecha').datetimepicker({
            minDate:'<?= $fechaA ?>',
            format:'Y-m-d',
            scrollInput: false,
            scrollMonth : false,
            timepicker:false
        });

    });

    $("#liberarTec").on("click", "#eliminarLib", function(){
        $raiz=$(this);
        var tr = $(this).closest('tr');
        $n=tr.find('td').eq(0).find('input[type="text"]').val();
      //  $("#liberarTec").off( "click" );
        var l = $('.ladda-button-demod'+$n+'').ladda();
        l.ladda( 'start' );

        bootbox.confirm({
            title: "<h3><strong>Atención!</strong></h3>",
            message: "Esta Seguro que deseas Eliminar el # "+$n+" ?",
            size: 'small',
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancelar'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirmar'
                }
            },
            callback: function (result) {
                if(result){
                    if(!$n.indexOf("Temp")){
                        $($raiz).parents("tr").remove();
                    }else{
                        $.ajax({
                            type: 'POST',
                            url: 'ajax/Reg_libtecnico',
                            data :  "fecha="+''+"&ddist="+''+"&horaini="+''+"&horafin="+''+"&ubicacion="+''+"&solohorario="+''+"&idlibera=del_"+$n.replace("Temp","Del")+"&idtec="+<?= $idtec ?>,
                            success: function (data) {
                                l.ladda('stop');
                                $($raiz).parents("tr").remove();

                            }
                        });
                    }
                }else{
                    l.ladda('stop');
                  //  $("#liberarTec").on("click",$(this)).find("#liberarTec");
                }
            }
        });
    });

    $("#liberarTec").on("click", "#agregaLib", function(){
        $raiz=$(this);
        var tr = $(this).closest('tr');
        $n=tr.find('td').eq(0).find('input[type="text"]').val();
        var fecha =tr.find('td').eq(1).find('input[type="text"]').val();
        var ddist=tr.find('td').eq(2).find('select').val();
        var horaini=tr.find('td').eq(3).find('input[type="text"]').val();
        var horafin=tr.find('td').eq(4).find('input[type="text"]').val();
        var ubicacion=tr.find('td').eq(5).find('select').val();
        var solohorario=tr.find('td').eq(6).find('[name="shorario"]:checked').val();
  //      console.log(solohorario + "aca p");
        solohorario=solohorario===undefined?0:1;
//        console.log(solohorario + "aca p2");


        var l = $('.ladda-button-demos'+$n+'').ladda();
        l.ladda( 'start' );
        if(fecha.trim()==='' || horaini.trim()==='' || horafin.trim()===''){
            bootbox.alert({
                title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                message: "<label>Ingresar Datos (Fecha,Hora inicio y Hora Fin)</label>",
                size: 'small',
                callback: function () {
                    l.ladda( 'stop' );
                }
            })
        }else if(((horaini.charAt(3)!='0' && horaini.charAt(3)!='3') || horaini.charAt(4)!='0') || ((horafin.charAt(3)!='0' && horafin.charAt(3)!='3') || horafin.charAt(4)!='0')){
            bootbox.alert({
                title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                message: "<label>el tiempo seleccionado debe de ser de 30 en 30 min</label>",
                size: 'small',
                callback: function () {
                    l.ladda( 'stop' );
                }
            })
        }else {
            if(horaini<horafin){

                bootbox.confirm({
                    title: "<h3><strong>Guardar Datos</strong></h3>",
                    message: "Deseas Registrar datos del # " + $n + " ?",
                    size: 'small',
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancelar'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Confirmar'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            $.ajax({
                                type: 'POST',
                                url: 'ajax/Reg_libtecnico',
                                data: "fecha=" + fecha +"&ddist="+ddist+"&horaini=" + horaini + "&horafin=" + horafin + "&ubicacion=" + ubicacion +"&solohorario="+solohorario+ "&idlibera=" + $n + "&idtec=" +<?= $idtec ?>,
                                success: function (data) {
                                    if(data.trim()=="dp"){
                                        bootbox.alert({
                                            title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                                            message: "<label>El Rango de Horas ya se encuentra registrado.</label>",
                                            size: 'small',
                                            callback: function () {
                                            }
                                        })
                                    }else {
                                        $(tr).replaceWith(data);

                                        $(".nubicacion" + ($n) + "").hide();
                                        $(".shorario" + ($n) + "").hide();
                                        $(".horafin" + ($n) + "").hide();
                                        $(".horaini" + ($n) + "").hide();
                                        $(".fechal" + ($n) + "").hide();
                                        $(".distr" + ($n) + "").hide();

                                        $(".pnubicacion" + ($n) + "").show();
                                        $(".pshorario" + ($n) + "").show();
                                        $(".phorafin" + ($n) + "").show();
                                        $(".phoraini" + ($n) + "").show();
                                        $(".pfechal" + ($n) + "").show();
                                        $(".pdistr" + ($n) + "").show();

                                        $($raiz).hide();
                                        $(".edit" + ($n) + "").show();
                                        $(".del" + ($n) + "").show();
                                        $(".cancel" + ($n) + "").hide();

                                        $('.clockpicker').clockpicker();

                                        <?php $fechaA = date('Y-m-d') ?>
                                        //jQuery.datetimepicker.setLocale('es');
                                        $('.fecha').datetimepicker({
                                            minDate: '<?= $fechaA ?>',
                                            format: 'Y-m-d',
                                            scrollInput: false,
                                            scrollMonth: false,
                                            timepicker: false
                                        });
                                    }
                                }
                            });
                        }
                        l.ladda( 'stop' );
                    }
                });
            }else{
                bootbox.alert({
                    title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                    message: "<label class=''>La Hora de Inicio debe ser menor a la Hora Fin.</label>",
                    size: 'small',
                    callback: function () {
                        l.ladda( 'stop' );
                    }
                })
            }
        }
    });

    $("#liberarTec").on("click", "#editLib", function(){
        var tr = $(this).closest('tr');
        $n=tr.find('td').eq(0).find('input[type="text"]').val();

        $(".nubicacion"+($n)+"").show();
        $(".shorario"+($n)+"").show();
        $(".horafin"+($n)+"").show();
        $(".horaini"+($n)+"").show();
        $(".fechal"+($n)+"").show();
        $(".distr"+($n)+"").show();

        $(".pnubicacion"+($n)+"").hide();
        $(".pshorario"+($n)+"").hide();
        $(".phorafin"+($n)+"").hide();
        $(".phoraini"+($n)+"").hide();
        $(".pfechal"+($n)+"").hide();
        $(".pdistr"+($n)+"").hide();
        $(this).hide();
        $(".del"+($n)+"").hide();
        $(".save"+($n)+"").show();
        $(".cancel"+($n)+"").show();
    });

    $("#liberarTec").on("click", "#cancelLib", function(){
        var tr = $(this).closest('tr');
        $n=tr.find('td').eq(0).find('input[type="text"]').val();
        $(".nubicacion"+($n)+"").hide();
        $(".shorario" + ($n) + "").hide();
        $(".horafin"+($n)+"").hide();
        $(".horaini"+($n)+"").hide();
        $(".fechal"+($n)+"").hide();
        $(".distr"+($n)+"").hide();

        $(".pnubicacion"+($n)+"").show();
        $(".pshorario" + ($n) + "").show();
        $(".phorafin"+($n)+"").show();
        $(".phoraini"+($n)+"").show();
        $(".pfechal"+($n)+"").show();
        $(".pdistr"+($n)+"").show();

        $(this).hide();
        $(".edit"+($n)+"").show();
        $(".del"+($n)+"").show();
        $(".save"+($n)+"").hide();
    });


   $("#guardarlp").on("click", function(){
       $selectid=$("#nviewciudad option:selected").val();
       $ltciudad='';
        if($selectid==1){
            $ltciudad=$("#ldepartamento").val();
        }else if($selectid==2){
            $ltciudad=$("#lprovincias").val();
        }else if($selectid==3){
            $ltciudad=$("#ldistrito").val();
        }

       var l = $('.ladda-button-demolpt').ladda();
       l.ladda( 'start' );

        $.ajax({
           type: 'POST',
           url: 'ajax/Reg_libTecProvincia',
           data: "ltCiudad="+JSON.stringify($ltciudad)+"&view="+$selectid+"&idtec="+<?= $idtec ?>,
           success: function (data) {
               if(data.trim()==="OK"){
                   toastr.success('Se Actualizo Correctamente','');
                   l.ladda( 'stop' );
               }
           }
       });
    });


    <?php $fechaA=date('Y-m-d') ?>
    jQuery.datetimepicker.setLocale('es');

    $('.fecha').datetimepicker({
        minDate:'<?= $fechaA ?>',
        format:'Y-m-d',
        scrollInput: false,
        scrollMonth : false,
        timepicker:false
    });

    $(document).ready(function(){
        $select=$("#nviewciudad option:selected").text();
        $selectid=$("#nviewciudad option:selected").val();
        $(".ldepartamento").hide();
        $(".lprovincias").hide();
        $(".ldistrito").hide();

        if($selectid==1){
            $(".ldepartamento").show();
            $(".lprovincias").hide();
            $(".ldistrito").hide();
        }else if($selectid==2){
            $(".ldepartamento").hide();
            $(".lprovincias").show();
            $(".ldistrito").hide();
        }else if($selectid==3) {
            $(".ldepartamento").hide();
            $(".lprovincias").hide();
            $(".ldistrito").show();
        }

        $("#seleccionciudad").text("Seleccionar "+$select +" :");
        $('.clockpicker').clockpicker();
    });

    $("#liberarTec").on('change', '#nubicacion', function(){
        var tr = $(this).closest('tr');
        $ubica=$("#dubica").val();
        $n=tr.find('td').eq(0).find('input[type="text"]').val();
        var ubicacion=tr.find('td').eq(5).find('select').val();

        if(ubicacion==$ubica){
       //     console.log("true -"+($n));
            $(".shorario"+($n)+"").attr("disabled",false);
            $(".shorario"+($n)+"").prop("checked",false);
        }else{
            console.log("false");
            $(".shorario"+($n)+"").attr("disabled",true);
            $(".shorario"+($n)+"").prop("checked",true);
        }
    });


    $("#nviewciudad").change(function () {
        $select=$("#nviewciudad option:selected").text();
        $("#seleccionciudad").text("Seleccionar "+$select +" :");
        $selectid=$("#nviewciudad option:selected").val();

        if($selectid==1){
            $(".ldepartamento").show();
            $(".lprovincias").hide();
            $(".ldistrito").hide();
        }else if($selectid==2){
            $(".ldepartamento").hide();
            $(".lprovincias").show();
            $(".ldistrito").hide();
        }else if($selectid==3) {
            $(".ldepartamento").hide();
            $(".lprovincias").hide();
            $(".ldistrito").show();
        }

    });

    </script>

    <?php
}
?>




<?php
  include_once("../../includes/head.php");
    $objDis=Tecnicos::obtenerDistritosTraslado(0);
  ?>
    <link href="../../assets/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="../../assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <link href="../assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">

    <div class="well" style="padding-top: 11px;padding-bottom: 10px;">
        <div class="row">
            <div class="col-md-3" >
                <div class="col-md-12" style="margin-bottom: 35px">
                    <label for=""> Seleccionar Distrito</label>
                    <select class="form-control" id="ldistritos" name="ldistritos">
                        <?php if ($objDis): ?>
                            <?php foreach ($objDis as $var): ?>
                                <option value="<?= $var->iddistrito ?>" ><?= $var->distrito?></option>
                            <?php endforeach ?>
                        <?php else: ?>
                     <?php endif ?>
                 </select>
                </div>
                <div class="col-md-12" style="margin-bottom: 35px">
                    <a class="btn btn-primary ladda-button ladda-button-demott "data-style="zoom-in"  style="padding-top: 0;padding-bottom: 0"  id="guardartt">Guardar <i class="fa fa-save"></i></a>
                </div>
                <div class="col-md-12">
                    <label for="">Nota* : los tiempos ingresados deben ser de 30 en 30 min </label>
                </div>
            </div>
            <div class="col-md-9">
                <div class="ibox table-scroll" id="ibox1">
                    <div class="ibox-content">
                        <div class="sk-spinner sk-spinner-double-bounce">
                            <div class="sk-double-bounce1"></div>
                            <div class="sk-double-bounce2"></div>
                        </div>
                        <div id="tblTiempoTraslado"  class="table-wrap" style=" height:400px; overflow:auto;">
                            <table class="main-table" id="timeTraslado" >
                                <thead>
                                    <tr>
                                        <th  class="text-center fixed-side table-cabecera-horario" width="50px">#</th>
                                        <th  class="text-center fixed-side table-cabecera-horario" width="160px">Distrito Inicio</th>
                                        <th  class="text-center fixed-side table-cabecera-horario" width="260px">Distrito Fin</th>
                                        <th  class="text-center fixed-side table-cabecera-horario" width="175px">Tiempo</th>
                                    </tr>
                                </thead>
                                <tbody id="tbttraslado">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../assets/bootbox/bootbox.all.js"></script>
    <script src="../assets/js/plugins/select2/select2.full.min.js"></script>

    <script src="../../assets/js/plugins/ladda/spin.min.js"></script>
    <script src="../../assets/js/plugins/ladda/ladda.min.js"></script>
    <script src="../../assets/js/plugins/ladda/ladda.jquery.min.js"></script>
    <script src="../../assets/js/plugins/toastr/toastr.min.js"></script>

    <style>
        .select2-container--open  {
            z-index: 999999;
        }
        .table-cabecera-horario{
            background:#075598!important;
            color:white !important;
        }
        .table-scroll {
            position:relative;
            max-width:1600px;
            margin:auto;
            overflow:hidden;
            /*border:.5px solid #000;*/
        }
        .table-wrap {
            width:100%;
            overflow:auto;
        }
        .table-scroll table {
            width:100%;
            margin:auto;
            border-collapse:separate;
            border-spacing:0;
        }
        .table-scroll th, .table-scroll td {
            padding:5px 10px;
            border-bottom:1px solid;
            border-right:1px solid;
            border-color: #DDDDDD;;
            background:#fff;
            color:rgb(103, 106, 108) ;
            white-space:nowrap;
            vertical-align:top;
            font-size: 12px
        }
        .table-scroll thead, .table-scroll tfoot {
            background: #f9f9f9;
        }

        .ibox-content > .sk-spinner {
            display: none;
        }

        .ibox-content.sk-loading {
            position: relative;
        }
        .ibox-content.sk-loading:after {
            content: '';
            background-color: rgba(255, 255, 255, 0.7);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        .ibox-content.sk-loading > .sk-spinner {
            display: block;
            position: absolute;
            top: 40%;
            left: 0;
            right: 0;
            z-index: 2000;
        }
    </style>
    <script>
        function listardata(){
            $selectid=$("#ldistritos option:selected").val();
            $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
            $.ajax({
                type : 'post',
                url : 'ajax/reg_tiempoTraslado',
                data :  'iddistri='+$selectid+"&tipo=listar",
                success : function(data){
                    $('#tbttraslado').html(data);
                    $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                }
            });

        }
        $(document).ready(function(){
            listardata();
        });

        $("#ldistritos").change(function () {
            $selectid=$("#ldistritos option:selected").val();
            $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
            $.ajax({
                type : 'post',
                url : 'ajax/reg_tiempoTraslado',
                data :  'iddistri='+$selectid+"&tipo=listar",
                success : function(data){
                    $('#tbttraslado').html(data);
                    $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                }
            });
        });

        $("#guardartt").on("click", function(){
            $selectid=$("#ldistritos option:selected").val();
            var l = $('.ladda-button-demott').ladda();
            l.ladda( 'start' );
            var parametros=[];
            $nerror=0;

            $("#timeTraslado tbody tr").each(function (index) {
                var tr = [];
                $(this).children("td").each(function (index2) {
                    if(index2 !=1 && index2!=2)
                    {
                        var td = {};
                        td["dato"+index2] = $(this).find("input").val();

                        if(index2==3 && (td["dato"+index2]).length!=5 && td["dato"+index2]!=""){
                            $(".validate"+index).html("");
                            $(".requi"+index).html("");
                            $(".formato"+index).html("");
                            $(this).append("<div class='validate"+index+"'><span class='label label-warning float-right'>Completar(hh:m0)</span></div>");
                            $nerror++;
                        }

                        if((index2==3 && (td["dato"+index2]).length==5 && ((td["dato"+index2]).charAt(2)!=':' || (td["dato"+index2]).charAt(4)!='0' || (td["dato"+index2]).charAt(3)!='0')) &&
                            (index2==3 && (td["dato"+index2]).length==5 && ((td["dato"+index2]).charAt(2)!=':' || (td["dato"+index2]).charAt(4)!='0'|| (td["dato"+index2]).charAt(3)!='3'))){
                                    $(".validate" + index).html("");
                                    $(".requi" + index).html("");
                                    $(".formato" + index).html("");
                                    $(this).append("<div class='formato" + index + "'><span class='label label-warning float-right'>Formato(hh:m0)</span></div>");
                                    $nerror++;
                        }


                        if(index2==3 && (td["dato"+index2]).length==5 && (td["dato"+index2]).charAt(2)==':' && (td["dato"+index2]).charAt(4)=='0' && ((td["dato"+index2]).charAt(3)=='0' || (td["dato"+index2]).charAt(3)=='3')){
                       //     console.log((((td["dato"+index]).charAt(4))!=0) + "aca pe viendo "+(td["dato"+index]).charAt(4) );
                            $(".validate"+index).html("");
                            $(".formato"+index).html("");
                            $(".requi"+index).html("");
                        }

                        if(td["dato"+index2]==""){
                            $(".validate"+index).html("");
                            $(".requi"+index).html("");
                            $(".formato"+index).html("");
                            $nerror++;
                            $(this).append("<div class='requi"+index+"'><span class='label label-danger float-right'>Required</span></div>");
                        }
                        tr.push(td);
                    }
                });
                parametros.push(tr);
            });

            if($nerror==0){
                $.ajax({
                    type: 'POST',
                    url : 'ajax/reg_tiempoTraslado',
                    data : 'iddistri='+$selectid+'&data='+JSON.stringify(parametros)+"&tipo=save",
                    success: function (data) {
                        if(data.trim()==="OK"){
                            toastr.success('Se Actualizo Correctamente','');
                            listardata();
                            l.ladda( 'stop' );
                        }
                    }
                });

             //   l.ladda( 'stop' );
                //console.log(parametros);
                //console.log("cant errorres :"+$nerror);
            }else{
                l.ladda('stop');
            }
        });


        function soloNumeroTiempo(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 58) ) {
                status = "This field accepts numbers only.";
                return false
            }
            status = "";
            return true
        }
    </script>
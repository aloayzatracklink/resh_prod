<?php 
if ($_POST['rowid']) {
  include_once("../../includes/init.php");
  $idOperacion=htmlspecialchars(htmlentities($_POST['rowid']));

     $cl="" ;
  $clienteObjModal=new Cliente;
  $ObjetoOperacion=Operacion::obtenerOperacionPorId($idOperacion);
    $df=DatosFacturacion::obtenerDatosFacturacionPorId($ObjetoOperacion->datosfacturacionid);
  if ($ObjetoOperacion->idlocation==0) {
    $vehiculoObj=Vehiculo::obtenerVehiculo($ObjetoOperacion->idvehiculo);
    $clienteObj=Cliente::obtenerClientePorVehiculo($ObjetoOperacion->idvehiculo,$ObjetoOperacion->idcliente);  

         if($clienteObj->razonSocial!=""){
             $cl=strtoupper($clienteObj->razonSocial);
         }else{
             $cl=strtoupper($clienteObj->apellidosC." ".$clienteObj->nombre);
         }
  }else{
    $clienteObj=Onix::obtenerDatosDesdeIds($ObjetoOperacion->idcliente,$ObjetoOperacion->idvehiculo,$ObjetoOperacion->idlocation) ;
    $chequeoObj=Chequeos::obtenerChequeoTrackPorId($ObjetoOperacion->idChequeo);
    $vehiculoObj=false;
  }
  ?>
  <?php if ($ObjetoOperacion->estado=='36'||$ObjetoOperacion->estado=='14'){ ?>
  <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>

<link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<link href="../assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<link href="../assets/css/animate.css" rel="stylesheet">
<h2 class="text-center"><?php echo $ObjetoOperacion->trabajo ?></h2>
  <input type="hidden" name="trabajoO" value="<?= $ObjetoOperacion->trabajo  ?>" >
  <?php if ($ObjetoOperacion->idChequeo!=0): ?>
    <h3 id="mot" class="text-center text-danger">MOTIVO: <?= $chequeoObj->motivo ?></h3>
    <h3 id="seguimiento" class="text-center text-danger">Seguimiento realizado: <?= $chequeoObj->seguimientoRealizado ?></h3>
  <?php endif ?>

<?php if ($vehiculoObj){ ?>

<?php if (!empty($vehiculoObj->serviciosAdicionales)): ?>
 <div class="row">
   <div class="form-group col-md-12">
     Servicios adicionales: <?php echo $vehiculoObj->serviciosAdicionales ?>
   </div>
 </div>
<?php endif ?>


</div> 
<!-- ORDENES INSTALACIONES FIN END END END END  --> 
  <div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->dni ?>">       
      <?php else: ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->ruc ?>">        
      <?php endif ?>  
    </div>
    <div class="col-md-9">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->razonSocial ?>">       
      <?php endif ?>
    </div> 
    </div> 
<hr>
<h3>Datos del vehiculo</h3>
  <div class="row"><hr>
    <div class="col-lg-12 field_wrapper"> 
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="field_name[0][]" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $vehiculoObj->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="">Chasis</label>
          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="field_name[1][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->chasis ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="motor" class="">Motor</label>
          <input style="border: none;" type="text" placeholder="Motor" id="motor" name="field_name[2][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->motor ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="marca" class="">Marca</label>
          <input style="border: none;" type="text" placeholder="Marca" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $vehiculoObj->marca ?>" >
        </div>
      </div> 
      <div class="row">
        <div class="form-group col-md-3">
          <label for="modelo" class="">Modelo</label>
          <input style="border: none;" type="text" placeholder="Modelo" id="modelo" name="field_name[4][]" class="border-bottom form-control text-capitalize"  required  value="<?php echo $vehiculoObj->modelo ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="color" class="">Color</label>
          <input style="border: none;" type="text" placeholder="Color" id="color" name="field_name[5][]" class="border-bottom form-control x2 text-capitalize" required value="<?php echo $vehiculoObj->color ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="anio" class="">Año</label>
          <input style="border: none;" type="text" placeholder="Año" id="anio" name="field_name[6][]" class="border-bottom form-control" required value="<?php echo $vehiculoObj->anio ?>" >
        </div>
        <div class="form-group col-md-3">
           <label for="plan" class="">Plan *</label>
           <select  style="border: none;" class="tecnico border-bottom form-control" required disabled name="plan" id="plan">
               <?php Cliente::obtenerPlanSelected($vehiculoObj->plan) ?>
           </select>

        </div>
          <div class="form-group col-md-12 ">
              <p style="font-weight:bold" class="text-danger">Servicios Adicionales Seleccionados : <label class="canthoraprog2 label-success"> 0 Horas </label></p>
              <?php ServiciosAdicionales::obtenerServAdicionalSeleccionadoVer($idOperacion) ?>
              <input type="hidden" id="canthoraprog" name="canthoraprog" value="0">
          </div>
      </div>     
    </div>
  </div> 
<hr>

<h2 class="text-center">Datos de instalacíon</h2>
<div class="row">
  <div class="form-group col-md-6">

      <label for="idtecnico" class="">Tecnico*</label>
      <select  style="border: none;" required class="cboTenico border-bottom form-control text-capitalize" name="idtecnico"  id="idtecnico" >
          <option class="text-capitalize" value=""  selected >Seleccione Tecnico* </option>
          <?php $objTec=Operacion::obtenerTecnios() ?>
          <?php if ($objTec): ?>
              <?php foreach ($objTec as $var): ?>
                  <option value="<?php echo $var->idtec ?>" <?php echo $var->idtec==$ObjetoOperacion->idtecnicoAsig?'selected':'jeje'  ?>><?php echo $var->nomtec ?></option>
              <?php endforeach ?>
          <?php else: ?>
          <?php endif ?>
      </select>

  </div>
  <div class="form-group col-md-3">
    <label for="orte" class="">Orte</label>
    <input style="border: none;" type="text" placeholder="Orte" id="orte" name="orte" class="border-bottom form-control x2 "  value="<?= $ObjetoOperacion->numeroOrte ?>">
    <input type="hidden" name="orteold" value="<?php echo $ObjetoOperacion->numeroOrte ?>">
  </div>

<div class="form-group col-md-3">
    <label for="orte" class="">Orden Contrato</label>
    <input style="border: none;" type="text" placeholder="Orden Contrato" id="ordenContrato" name="ordenContrato" class="border-bottom form-control x2 "  value="<?= $ObjetoOperacion->ordenContrato ?>">
  </div>
</div>

    <h2 class="text-center">Datos de Facturacion</h2>
    <div class="row">
        <div class="form-group col-md-3">
            <label for="" class="" style="color:steelblue">Numero Orden Facturacion</label>
            <input style="border: none;" type="text" placeholder="Numero Orden Fact" id="numeroOrdenF" name="numeroOrdenF" class="numeroOrden border-bottom form-control" autocomplete="off" value="<?php echo $df->orden ?>">
        </div>

        <div class="form-group col-md-4">
            <label for="corigen" class="">Concesionario Origen</label>
            <input style="border: none;" type="text" placeholder="Concesionario Origen" id="corigen" name="corigen" class="border-bottom form-control text-uppercase corigen " autocomplete="off" readonly value="<?= $df->canalOrigen ?>" >
        </div>
    </div>
<?php if (!empty($ObjetoOperacion->coordenadasInstalacion)): ?>
<div class="row">
   <div class="col-md-3"><h3>Coordenadas de instalación --></h3></div>
   <div class="col-md-6">
    <input style="border: none;" type="text" placeholder="Coordenadas insalaicon" id="orden" name="orden" class="border-bottom form-control x2 " value="<?php echo $ObjetoOperacion->coordenadasInstalacion ?>" >
  </div>
</div>
<?php endif ?>
<hr>
<h4>Lugar de instalación <?php echo $ObjetoOperacion->lugarInstalacion ?> <?php echo $retVal = (!empty($ObjetoOperacion->nombreConcecionarioInstalicion)) ? $ObjetoOperacion->nombreConcecionarioInstalicion : "" ; ?>  del dia <?= formatoHaceEn($ObjetoOperacion->fechaInstalacion) ?></h4>
<hr>

<?php 
  //////////LOS ID
 ?>
<input type="hidden" name="idoperacion" value="<?php echo $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idvh" value="<?php echo $vehiculoObj->idvehiculo ?>">
<input type="hidden" name="datosfacturacionid" value="<?php echo $ObjetoOperacion->datosfacturacionid ?>">
<input type="hidden" name="trabajo" value="<?php echo $ObjetoOperacion->trabajo ?>">
<input type="hidden" name="idope" value="<?php echo $ObjetoOperacion->idConfirmacionOPE ?>">
 <?php 
  //////////LOS ID
 ?>

<div role="tablist">
  <input type="radio" title="taller" name="lugarInstacion" id="taller" role="tab" aria-controls="taller-panel"  value="Taller" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Taller")?"checked":"" ?> />
  <input type="radio" title="domicilio" name="lugarInstacion" id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Domicilio Cliente")?"checked":"" ?> />
  <input type="radio" title="concesionario" name="lugarInstacion" id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Concesionario")?"checked":"" ?> />
  <div class="tabs" aria-hidden="true">
    <label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
    <label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
    <label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
  </div>
  <input type="hidden" name="lugarInstacionold" value="<?php echo $ObjetoOperacion->lugarInstalacion ?>">
  <div class="panels">
    <ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">
      <h2>TALLER</h2>
      <p>Av. Paseo de la República 4925 con Angamos Este </p>
      <p>Surquillo, Lima, Lima</p>
      <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionT" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionT border-bottom form-control text-capitalize tal" required name="departamentoInstalacionT" id="departamentoInstalacionT">
                  
                         <option class="text-capitalize" value="15"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId('15') ?><?= $departamento->departamento ?></option>
                      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                      <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                      <?php else: ?>
                      <?php endif ?>                    

                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionT" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionT border-bottom form-control text-capitalize tal" required name="provinciaInstalacionT" id="provinciaInstalacionT" >
                    <?php //if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId('127') ?><?php echo $provincia->provincia ?></option>
                     <?php
                      //} ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionT" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionT border-bottom form-control text-capitalize tal" required name="distritoInstalacionT"  id="distritoInstalacionT" >
                <?php// if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId('1291') ?><?= $distrito->distrito ?></option>
                    <?php
                      //} ?>                 
              </select>           
          </div>

        </div>
      <input type="hidden" name="direccionTaller" value="Av. Paseo de la República 4925 con Angamos Este - Surquillo ">      
      <input type="hidden" name="referenciaT" value="">
      <div class="row">
    <div class="form-group col-md-5" id="data_1">
      <div class="row">
        <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon "><i class="fa fa-calendar"></i></span><input type="text" class="form-control fechaa tal"  id="fechaIT" name="fechaIT" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10)  ?>">
          </div>
        </div>
        <div class="form-group col-md-6">
            <label  class="">Hora</label>
          <div class="input-group" data-autoclose="true">
              <input type="text" class="form-control horaa tal" id="horaIT" name="horaIT" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>" >
              <span class="input-group-addon">
                  <span class="fa fa-clock-o"></span>
              </span>
          </div>
        </div>
      </div>
    </div> 
            <div class="form-group col-md-4">
              <label for="nombreContactoIT" class="">Nombre Contacto *</label>
              <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIT" name="nombreContactoIT" class="form-control x2 text-capitalize border-bottom tal" value="<?= ($ObjetoOperacion->nombreContacto=='')?'':$ObjetoOperacion->nombreContacto ?>">
            </div><div class="form-group col-md-3">
            <label for="numeroContactoIT" class="">Número Contacto *</label>
            <input style="border: none;" type="text" placeholder=" Número Contacto *" id="numeroContactoIT" name="numeroContactoIT" class="form-control x2 text-capitalize border-bottom tal"  value="<?= ($ObjetoOperacion->numeroContacto=='')?'':$ObjetoOperacion->numeroContacto ?>">
          </div>
        </div>
        <?php if ($ObjetoOperacion->idReprogramado!=0): ?>
          <div class="row">
            <h3 for="" class="text-danger text-center col-md-12">Reprogramacion: <?=  $ObjetoOperacion->observacion ?></h3><br>
          </div>
        <?php endif ?>
        <div class="row">
          <div class="form-group col-md-12">
            <label for="observacionT" class="">Motivo de modificación *</label>
            <input style="border: none;" type="text" placeholder="Motivo de cambio de fecha *" id="observacionT" name="observacion" class="form-control x2  border-bottom tal"  required="">
          </div>      
      </div>
      
    </ul>
    <ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab"> 
      <h2>Domicilio Cliente</h2>
       <?php 
       if (!empty($clienteObj->departamento)) {
          $depatamentoObj=$clienteObj->obtenerDepartamentoPorId($clienteObj->departamento); 
          $provinciaObj=$clienteObj->otenerProvinciaPorId($clienteObj->provincia); 
          $distritoObj=$clienteObj->obtenerDistritoPorId($clienteObj->distrito);        
       }           
    ?> 
      <div class="row">
         <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionD" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize dom" required name="departamentoInstalacionD" id="departamentoInstalacionD">
                    <?php if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                        <option class="text-capitalize " value=""  selected  disabled="">Seleccione departamento</option>
                            <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                          <?php if ($objDepartamento): ?>
                            <?php foreach ($objDepartamento as $var): ?>
                              <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                            <?php endforeach ?>
                          <?php else: ?>
                          <?php endif ?>
                      <?php
                    }else{
                      ?>
                         <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
                      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                      <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                      <?php else: ?>
                      <?php endif ?>

                      <?php
                      } ?>                 


                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionD" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize dom" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
                    <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
                     <?php
                      } ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionD" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize dom" required name="distritoInstalacionD"  id="distritoInstalacionD" >
                <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
                    <?php
                      } ?>                 
              </select>           
          </div>

        </div>
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="direccionInstalacionD" class="">Direccion*</label>
            <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionD" name="direccionInstalacionD"  class="form-control border-bottom dom" value="<?= (!empty($ObjetoOperacion->direccionInstacion))?$ObjetoOperacion->direccionInstacion : ''  ?>">

 <input type="hidden" name="direccionInstalacionold" value="<?php echo $ObjetoOperacion->direccionInstacion ?>">

          </div>
          <div class="form-group  col-md-6">
            <label for="referenciaInstalacionD" class="">Referencia *</label>
            <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" name="referenciaInstalacionD" class="form-control border-bottom  dom" value="<?= (!empty($ObjetoOperacion->referenciaInstalacion)) ? $ObjetoOperacion->referenciaInstalacion:'' ?>" >
 <input type="hidden" name="referenciaInstalacionold" value="<?php echo $ObjetoOperacion->referenciaInstalacion ?>">
          </div>
        </div>  
        <div class="row">
          <div class="form-group col-md-6">
            <label for="cod" class="">Cordenadas Domicilio *</label>
            <input style="border: none;" type="text" placeholder="Cordenadas Domicilio *" id="cod" name="cordenadasD" class="form-control x2 text-capitalize border-bottom dom" value="<?= (!empty($ObjetoOperacion->coordenadasInstalacion))? $ObjetoOperacion->coordenadasInstalacion:'' ?>">
          </div>
          <div class="form-group col-md-6">
            <label for="observacionD" class="">Motivo Motivo de modificacióne modificación</label>
            <input style="border: none;" type="text" placeholder="Motivo Motivo de modificación" name="observacion" class="form-control x2 text-capitalize border-bottom dom" required="">
          </div>
        </div>  
         <div class="form-group col-md-5" id="data_1">
      <div class="row">
        <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control fechaa dom"  id="fechaID" name="fechaID" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10)  ?>"  >
              <input type="hidden" name="fechaInstalacionold" value="<?php echo $ObjetoOperacion->fechaInstalacion ?>">
          </div>
        </div>
        <div class="form-group col-md-6">
                      <label  class="">Hora</label>
          <div class="input-group" data-autoclose="true">
              <input type="text" class="form-control horaa dom" id="horaID" name="horaID" autocomplete="off" value="<?= substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>" required>
              <span class="input-group-addon">
                  <span class="fa fa-clock-o"></span>
              </span>
          </div>
        </div>
      </div>
   </div> 
        <div class="row">
          <div class="form-group col-md-6">
            <label for="nombreContactoID" class="">Nombre Contacto *</label>
            <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoID" name="nombreContactoID" class="form-control x2 text-capitalize border-bottom dom"  value="<?= ($ObjetoOperacion->nombreContacto=='')?'':$ObjetoOperacion->nombreContacto ?>">
             <input type="hidden" name="nombreContactoold" value="<?php echo $ObjetoOperacion->nombreContacto ?>">
          </div><div class="form-group col-md-6">
          <label for="numeroContactoID" class="">Número Contacto *</label>
          <input style="border: none;" type="text" placeholder=" Número Contacto *" id="numeroContactoID" name="numeroContactoID" class="form-control x2 text-capitalize border-bottom dom"  value="<?= ($ObjetoOperacion->numeroContacto=='')?'':$ObjetoOperacion->numeroContacto ?>" >
          <input type="hidden" name="numeroContactoold" value="<?php echo $ObjetoOperacion->numeroContacto ?>">
        </div>
      </div>
    </div> 
  </ul>
  <ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">
    
    <div class="row">
      <div class="col-md-6"><h2>Concesionario</h2></div>
      <div class="col-md-6">
        <label for="concesionarioInstalacion" class="">Nombre de concesionario*</label>
        <input style="border: none;" type="text" placeholder="Nombre de concesionario*" id="concesionarioInstalacion" name="concesionarioInstalacion" class="form-control border-bottom con" value="<?= (!empty($ObjetoOperacion->nombreConcecionarioInstalicion))?$ObjetoOperacion->nombreConcecionarioInstalicion:''  ?>" >
        <input type="hidden" name="nombreConcecionarioInstalicionold" value="<?php echo $ObjetoOperacion->nombreConcecionarioInstalicion ?>">
      </div>
    </div>
    <div class="row">
     <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionC" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionC border-bottom form-control text-capitalize con" required name="departamentoInstalacionC" id="departamentoInstalacionC">
                    <?php if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                        <option class="text-capitalize" value=""  selected  disabled="">Seleccione departamento</option>
                            <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                          <?php if ($objDepartamento): ?>
                            <?php foreach ($objDepartamento as $var): ?>
                              <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                            <?php endforeach ?>
                          <?php else: ?>
                          <?php endif ?>
                      <?php
                    }else{
                      ?>
                         <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
                      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                      <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                      <?php else: ?>
                      <?php endif ?>

                      <?php
                      } ?>                 


                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionC" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionC border-bottom form-control text-capitalize con" required name="provinciaInstalacionC" id="provinciaInstalacionC" >
                    <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
                     <?php
                      } ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionC" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionC border-bottom form-control text-capitalize con" required name="distritoInstalacionC"  id="distritoInstalacionC" >
                <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
                    <?php
                      } ?>                 
              </select>           
          </div>

        </div>
      <div class="row">
        <div class="form-group  col-md-6">
          <label for="direccionInstalacionC" class="">Direccion*</label>
          <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom con" value="<?= $ObjetoOperacion->direccionInstacion ?>" >
        </div>
        <div class="form-group  col-md-6">
          <label for="referenciaInstalacionC" class="">Referencia *</label>
          <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom con"  value="<?= $ObjetoOperacion->referenciaInstalacion ?>" >
        </div>
      </div>    
      <div class="row">
          <div class="form-group col-md-5" id="data_1">
                <div class="row">
                  <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
                      <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control fechaa con"  id="fechaIC" name="fechaIC" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10)  ?>">
                      </div>
                  </div>
                  <div class="form-group col-md-6">
                                <label  class="">Hora</label>
                    <div class="input-group" data-autoclose="true">
                        <input type="text" class="form-control horaa con" id="horaIC" name="horaIC" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>">
                        <span class="input-group-addon">
                            <span class="fa fa-clock-o"></span>
                        </span>
                    </div>
                  </div>
                </div>
         </div> 
      </div>
      <div class="row">
        <div class="form-group col-md-6">
          <label for="nombreContactoIC" class="">Nombre Contacto *</label>
          <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIC con" name="nombreContactoIC" class="form-control x2 text-capitalize border-bottom" value="<?= ($ObjetoOperacion->nombreContacto=='')?'':$ObjetoOperacion->nombreContacto ?>" >
        </div>
        <div class="form-group col-md-6">
        <label for="numeroContactoIC" class="">Número Contacto *</label>
        <input style="border: none;" type="text" placeholder=" Número Contacto *" id="numeroContactoIC" name="numeroContactoIC" class="form-control x2 text-capitalize border-bottom con"  value="<?= ($ObjetoOperacion->numeroContacto=='')?'':$ObjetoOperacion->numeroContacto ?>">
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-12">
        <label for="observacionC" class="">Motivo de modificación</label>
        <input style="border: none;" type="text" placeholder="Motivo de modificación" id="observacionC" name="observacion" class="form-control x2 text-capitalize border-bottom con" required="">
      </div>
    </div>
  </div>
</ul>
</div>

</div> 
<!--  <div id="programacionHtml"></div>-->


<?php if ($ObjetoOperacion->coordenadasInstalacion != ''): ?>
  <div class="row"><div class="col-lg-12"><label for="cordenadas">Coordenadas de instalacion: </label><label > <?php echo $ObjetoOperacion->coordenadasInstalacion ?></label><br><h1 class="text-success"><a  target="_blank" href="mapa.php?coordenadas=<?php echo $ObjetoOperacion->coordenadasInstalacion ?>&lugar=<?php echo $ObjetoOperacion->direccionInstacion ?>">ver mapa</a></h1></div></div>    
<?php endif ?>
<?php }else{ ?>
<input type="hidden" name="noModificar" value="ok">
  <?php if ($ObjetoOperacion->trabajo!='Chequeo en linea'){ ?>
  <div class="btn btn-primary" onclick="reprogramarChequeo()">Reprogramar</div>
  <br>
<?php } ?>
  <div class="row">
        <div class="form-group col-md-3">
        <label for="Observaciones" > Usuario Web</label>
        <input name="usuarioWeb" style="border: none;" type="text" placeholder="Web" id="Observaciones" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>">
      </div>
      <div class="form-group col-md-5">
      <?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
        <label for="numeroTecnico" >Nombre de cliente*</label>
        <input  style="border: none;" type="text"  id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ; ?> ">
      </div>
      <div class="col-md-2"><label for="estado" >Celular</label>
        <input style="border: none;" type="text"  id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">
      </div>
      <div class="col-md-2"><label for="estado" >Celular</label>
        <input style="border: none;" type="text"  id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular2)) ? "$clienteObj->celular2" : "" ; ?>">
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-12">
        <label for="Observaciones"  >Correo cliente. </label>
        <input style="border: none;" type="text" placeholder="Correo" id="Observaciones"  class="border-bottom form-control" name="correoCliente" value="<?php echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo," : "$clienteObj->correo" ;echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo2" : "" ;  ?>">
      </div>
    </div>
     </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="form-group col-md-3">
    <label for="numeroTecnico" >Placa Vehiculo*</label>
    <input  style="border: none;"  type="text" placeholder="Placa" id="numeroTecnico" name="placa" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->placa)) ? "$clienteObj->placa" : "" ; ?>"   >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Marca *</label>
    <input required style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->marca)) ? "$clienteObj->marca" : "" ; ?>" >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Modelo*</label>
    <input  required style="border: none;" type="text" placeholder="Modelo" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($clienteObj->modelo)) ? "$clienteObj->modelo" : "" ; ?>">
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Color</label>
    <input style="border: none;" type="text" placeholder="Color" id="Observaciones" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->color)) ? "$clienteObj->color" : "" ; ?>">
  </div>
  
</div>

  <span class="ocultarThis">
<div class="row">
  <div class="form-group col-md-4">
    <label for="Observaciones"  >Chasis*</label>
    <input required style="border: none;" type="text" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->chasis)) ? "$clienteObj->chasis" : "" ; ?>">
  </div>
  <div class="form-group col-md-4">
    <label for="Observaciones" >Motor</label>
    <input style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="motor" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->motor)) ? "$clienteObj->motor" : "Actualizar" ; ?>"  >
  </div>
  <div class="form-group col-md-4">
    <label for="Observaciones" >Plan</label>
    <input  style="border: none;" type="text" placeholder="Observaciones"  id="Observaciones" name="plan" class="border-bottom form-control" value="<?= $chequeoObj->plan ?>"  >
  </div>
</div>
<input type="hidden" name="idoperacion" value="<?= $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idChequeo" value="<?= $ObjetoOperacion->idChequeo ?>">


    <div class="row">
           <div class="form-group col-md-3">
    <label for="Observaciones" >Ubicacion de sistema*</label>
    <input  required readonly style="border: none;" type="text" placeholder="Ubicacion de sistema" id="Observaciones" name="ubsistema" class="border-bottom form-control"   value="<?= $chequeoObj->ubicacionSistema ?>">
  </div>      

    <div class="form-group col-md-5">
    <label for="numeroTecnico" >Fecha instalacion</label>
    <input  style="border: none;" readonly type="text" placeholder="Persona de contacto" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?= $chequeoObj->fechaInstalacion ?>"   >
  </div>
  <div class="form-group col-md-4">
    <label for="Observaciones" >Tecnico instalador *</label>
    <input required readonly="" style="border: none;" type="text" placeholder="Tecnico instalador* " id="Observaciones" name="tecnicoinstalador" class="border-bottom form-control"  value="<?= $chequeoObj->instalador ?>" >
  </div>

    </div>
  <div class="row"> 
  <div class="form-group col-md-6">
    <label for="numeroOrden" >Número de orden</label>
    <input  style="border: none;" type="text" placeholder="Número de orden" id="numeroOrden" name="numeroOrden" class="border-bottom form-control" required="" value="<?= $ObjetoOperacion->numeroOrden ?>" >
    <input type="hidden" name="chequeo" value="ok">
  </div>
  <div class="form-group col-md-6">
    <label for="numeroOrte" >Número de orte</label>
    <input  style="border: none;" type="text" placeholder="Número de orte" id="numeroOrte" name="numeroOrte" class="border-bottom form-control" required value="<?= $ObjetoOperacion->numeroOrte ?>">
    <input type="hidden" name="chequeo" value="ok" required>
  </div>

  <div class="form-group col-md-6">

      <label for="idtecnico" class="">Tecnico*</label>
      <select  style="border: none;" required class="cboTenico border-bottom form-control text-capitalize" name="idtecnico"  id="idtecnico" >
          <option class="text-capitalize" value=""  selected >Seleccione Tecnico* </option>
          <?php $objTec=Operacion::obtenerTecnios() ?>
          <?php if ($objTec): ?>
              <?php foreach ($objTec as $var): ?>
                  <option value="<?php echo $var->idtec ?>" <?php echo $var->idtec==$ObjetoOperacion->idtecnicoAsig?'selected':'jeje'  ?>><?php echo $var->nomtec ?></option>
              <?php endforeach ?>
          <?php else: ?>
          <?php endif ?>
      </select>

  </div>
</div>
<?php if (!empty($chequeoObj->coordenadas)){ ?>
<h1>
<a target="_blank" href="mapa.php?coordenadas=<?= $chequeoObj->coordenadas ?>&lugar=<?= $chequeoObj->direccion ?>" class="text-center">Ver mapa</a>  
</h1>  
<?php } ?>
<div role="tablist">
  <input type="radio" title="taller" name="lugarInstacion" id="taller" role="tab" aria-controls="taller-panel"  value="Taller" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Taller")?"checked":"" ?> />
  <input type="radio" title="domicilio" name="lugarInstacion" id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Domicilio Cliente")?"checked":"" ?> />
  <input type="radio" title="concesionario" name="lugarInstacion" id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Concesionario")?"checked":"" ?> />
  <div class="tabs" aria-hidden="true">
    <label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
    <label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
    <label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
  </div>
  <div class="panels">
    <ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">
      <h2>TALLER</h2>
      <p>Av. Paseo de la República 4925 con Angamos Este</p>
      <p>Surquillo, Lima, Lima</p>
      <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionT" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionT border-bottom form-control text-capitalize tal" required name="departamentoInstalacionT" id="departamentoInstalacionT">                  
                         <option class="text-capitalize" value="15"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId('15') ?><?= $departamento->departamento ?></option>
                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionT" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionT border-bottom form-control text-capitalize tal" required name="provinciaInstalacionT" id="provinciaInstalacionT" >
                    <?php //if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId('127') ?><?php echo $provincia->provincia ?></option>
                     <?php
                      //} ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionT" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionT border-bottom form-control text-capitalize tal" required name="distritoInstalacionT"  id="distritoInstalacionT" >
                <?php// if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId('1291') ?><?= $distrito->distrito ?></option>
                    <?php
                      //} ?>                 
              </select>           
          </div>

        </div>
      <input type="hidden" name="direccionTaller" value="Av. Paseo de la República 4925 con Angamos Este - Surquillo ">      
      <input type="hidden" name="referenciaT" value="">
      <div class="row">
    <div class="form-group col-md-5" id="data_1">
      <div class="row">
        <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon "><i class="fa fa-calendar"></i></span><input type="text" class="form-control fechaa tal"  id="fechaIT" name="fechaIT" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10)  ?>">
          </div>
        </div>
        <div class="form-group col-md-6">
            <label  class="">Hora</label>
          <div class="input-group" data-autoclose="true">
              <input type="text" class="form-control horaa tal" id="horaIT" name="horaIT" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>" >
              <span class="input-group-addon">
                  <span class="fa fa-clock-o"></span>
              </span>
          </div>
        </div>
      </div>
    </div> 
            <div class="form-group col-md-4">
              <label for="nombreContactoIT" class="">Nombre Contacto *</label>
              <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIT" name="nombreContactoIT" class="form-control x2 text-capitalize border-bottom tal" value="<?= ($chequeoObj->personaContacto=='')?'':$chequeoObj->personaContacto ?>">
            </div><div class="form-group col-md-3">
            <label for="numeroContactoIT" class="">Número Contacto *</label>
            <input style="border: none;" type="text" placeholder=" Número Contacto *" id="numeroContactoIT" name="numeroContactoIT" class="form-control x2 text-capitalize border-bottom tal"  value="<?= ($chequeoObj->numeroContacto=='')?'':$chequeoObj->numeroContacto ?>">
          </div>
        </div>
        <?php if ($ObjetoOperacion->idReprogramado!=0): ?>
          <div class="row">
            <h3 for="" class="text-danger text-center col-md-12">Reprogramacion: <?=  $ObjetoOperacion->observacion ?></h3><br>
          </div>
        <?php endif ?>
        <div class="row">
          <div class="form-group col-md-12">
            <label for="observacionT" class="">Motivo modificación</label>
            <input style="border: none;" type="text" placeholder="Motivo modificación" id="observacionT" name="observacion" class="form-control x2  border-bottom tal"  required="">
          </div>      
      </div>
      
    </ul>
    <ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab"> 
      <h2>Domicilio Cliente</h2>
       <?php 
       if (!empty($clienteObj->departamento)) {
          $depatamentoObj=$clienteObj->obtenerDepartamentoPorId($clienteObj->departamento); 
          $provinciaObj=$clienteObj->otenerProvinciaPorId($clienteObj->provincia); 
          $distritoObj=$clienteObj->obtenerDistritoPorId($clienteObj->distrito);        
       }           
    ?> 
      <div class="row">
         <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionD" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize dom" required name="departamentoInstalacionD" id="departamentoInstalacionD">
                    <?php if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                        <option class="text-capitalize " value=""  selected  disabled="">Seleccione departamento</option>
                            <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                          <?php if ($objDepartamento): ?>
                            <?php foreach ($objDepartamento as $var): ?>
                              <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                            <?php endforeach ?>
                          <?php else: ?>
                          <?php endif ?>
                      <?php
                    }else{
                      ?>
                         <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
                      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                      <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                      <?php else: ?>
                      <?php endif ?>

                      <?php
                      } ?>                 


                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionD" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize dom" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
                    <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
                     <?php
                      } ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionD" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize dom" required name="distritoInstalacionD"  id="distritoInstalacionD" >
                <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
                    <?php
                      } ?>                 
              </select>           
          </div>

        </div>
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="direccionInstalacionD" class="">Direccion*</label>
            <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionD" name="direccionInstalacionD"  class="form-control border-bottom dom" value="<?= (!empty($chequeoObj->direccion))?$chequeoObj->direccion : ''  ?>">
          </div>
          <div class="form-group  col-md-6">
            <label for="referenciaInstalacionD" class="">Referencia *</label>
            <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" name="referenciaInstalacionD" class="form-control border-bottom  dom" value="<?= (!empty($chequeoObj->referencia)) ? $chequeoObj->referencia:'' ?>" >
          </div>
        </div>  
        <div class="row">
          <div class="form-group col-md-6">
            <label for="cod" class="">Cordenadas Domicilio *</label>
            <input style="border: none;" type="text" placeholder="Cordenadas Domicilio *" id="cod" name="cordenadasD" class="form-control x2 text-capitalize border-bottom dom" value="<?= (!empty($chequeoObj->coordenadas))? $chequeoObj->coordenadas:'' ?>">
          </div>
          <div class="form-group col-md-6">
            <label for="observacionD" class="">Motivo de modificación</label>
            <input style="border: none;" type="text" placeholder="Motivo de modificación" id="observacionD" name="observacion" class="form-control x2 text-capitalize border-bottom dom" required="">
          </div>
        </div>  
         <div class="form-group col-md-5" id="data_1">
      <div class="row">
        <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control fechaa dom"  id="fechaID" name="fechaID" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10)  ?>"  >
          </div>
        </div>
        <div class="form-group col-md-6">
                      <label  class="">Hora</label>
          <div class="input-group" data-autoclose="true">
              <input type="text" class="form-control horaa dom" id="horaID" name="horaID" autocomplete="off" value="<?= substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>" required>
              <span class="input-group-addon">
                  <span class="fa fa-clock-o"></span>
              </span>
          </div>
        </div>
      </div>
   </div> 
        <div class="row">
          <div class="form-group col-md-6">
            <label for="nombreContactoID" class="">Nombre Contacto *</label>
            <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoID" name="nombreContactoID" class="form-control x2 text-capitalize border-bottom dom"  value="<?= ($chequeoObj->personaContacto=='')?'':$chequeoObj->personaContacto ?>">
          </div><div class="form-group col-md-6">
          <label for="numeroContactoID" class="">Número Contacto *</label>
          <input style="border: none;" type="text" placeholder=" Número Contacto *" id="numeroContactoID" name="numeroContactoID" class="form-control x2 text-capitalize border-bottom dom"  value="<?= ($chequeoObj->numeroContacto=='')?'':$chequeoObj->numeroContacto ?>" >
        </div>
      </div>
    </div> 
  </ul>
  <ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">    
    <div class="row">
      <div class="col-md-6"><h2>Concesionario</h2></div>
      <div class="col-md-6">
        <label for="concesionarioInstalacion" class="">Nombre de concesionario*</label>
        <input style="border: none;" type="text" placeholder="Nombre de concesionario*" id="concesionarioInstalacion" name="concesionarioInstalacion" class="form-control border-bottom con" value="<?= (!empty($ObjetoOperacion->nombreConcecionarioInstalicion))?$ObjetoOperacion->nombreConcecionarioInstalicion:''  ?>" >
      </div>
    </div>
    <div class="row">
     <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionC" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionC border-bottom form-control text-capitalize con" required name="departamentoInstalacionC" id="departamentoInstalacionC">
                    <?php if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                        <option class="text-capitalize" value=""  selected  disabled="">Seleccione departamento</option>
                            <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                          <?php if ($objDepartamento): ?>
                            <?php foreach ($objDepartamento as $var): ?>
                              <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                            <?php endforeach ?>
                          <?php else: ?>
                          <?php endif ?>
                      <?php
                    }else{
                      ?>
                         <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
                      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                      <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                      <?php else: ?>
                      <?php endif ?>

                      <?php
                      } ?>                 


                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionC" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionC border-bottom form-control text-capitalize con" required name="provinciaInstalacionC" id="provinciaInstalacionC" >
                    <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
                     <?php
                      } ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionC" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionC border-bottom form-control text-capitalize con" required name="distritoInstalacionC"  id="distritoInstalacionC" >
                <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
                    <?php
                      } ?>                 
              </select>           
          </div>

        </div>
      <div class="row">
        <div class="form-group  col-md-6">
          <label for="direccionInstalacionC" class="">Direccion*</label>
          <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom con" value="<?= $chequeoObj->direccion ?>" >
        </div>
        <div class="form-group  col-md-6">
          <label for="referenciaInstalacionC" class="">Referencia *</label>
          <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom con"  value="<?= $chequeoObj->referencia ?>" >
        </div>
      </div>    
      <div class="row">
          <div class="form-group col-md-5" id="data_1">
                <div class="row">
                  <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
                      <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control fechaa con"  id="fechaIC" name="fechaIC" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10)  ?>">
                      </div>
                  </div>
                  <div class="form-group col-md-6">
                                <label  class="">Hora</label>
                    <div class="input-group" data-autoclose="true">
                        <input type="text" class="form-control horaa con" id="horaIC" name="horaIC" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>">
                        <span class="input-group-addon">
                            <span class="fa fa-clock-o"></span>
                        </span>
                    </div>
                  </div>
                </div>
         </div> 
      </div>
      <div class="row">
        <div class="form-group col-md-6">
          <label for="nombreContactoIC" class="">Nombre Contacto *</label>
          <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIC con" name="nombreContactoIC" class="form-control x2 text-capitalize border-bottom" value="<?= ($chequeoObj->personaContacto=='')?'':$chequeoObj->personaContacto ?>" >
        </div>
        <div class="form-group col-md-6">
        <label for="numeroContactoIC" class="">Número Contacto *</label>
        <input style="border: none;" type="text" placeholder=" Número Contacto *" id="numeroContactoIC" name="numeroContactoIC" class="form-control x2 text-capitalize border-bottom con"  value="<?= ($chequeoObj->numeroContacto=='')?'':$chequeoObj->numeroContacto ?>">
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-12">
        <label for="observacionC" class="">Motivo de modificación</label>
        <input style="border: none;" type="text" placeholder="Motivo de modificación" id="observacionC" name="observacion" class="form-control x2 text-capitalize border-bottom con" required="">
      </div>
    </div>
  </div>
</ul>
</div>
</div> 

  <div id="programacionHtml"></div>

<?php } ?>
<?php }else{ ?>


<input type="hidden" name="noModificar" value="nomodificar">
<div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>

        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->dni ?>">       
      <?php else: ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->ruc ?>">        
      <?php endif ?>  
    </div>

    <div class="col-md-9">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->razonSocial ?>">       
      <?php endif ?>
    </div> 
    </div> 
<hr>
<h3>Datos del vehiculo</h3>
  <div class="row"><hr>
    <div class="col-lg-12 field_wrapper"> 
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="field_name[0][]" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $vehiculoObj->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="">Chasis</label>
          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="field_name[1][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->chasis ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="motor" class="">Motor</label>
          <input style="border: none;" type="text" placeholder="Motor" id="motor" name="field_name[2][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->motor ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="marca" class="">Marca</label>
          <input style="border: none;" type="text" placeholder="Marca" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $vehiculoObj->marca ?>" >
        </div>
      </div>
      </div>
  </div>


<h2 class="text-center">Datos de instalacíon</h2>
<div class="row">
  <div class="form-group col-md-6">
    <label for="tecnico" class="">Tecnicos</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="tecnico" disabled>
      <?php if ($ObjetoOperacion->idTecnicoAsignado!=0): ?>
        <?php 
          $usuarioDato=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoAsignado);
         ?>
      <option class="text-capitalize" value="<?= $ObjetoOperacion->idTecnicoAsignado ?>" selected ><?= $usuarioDato->usuarioNombre ?> <?= $usuarioDato->usuarioApellidoPaterno ?></option>
      <?php else: ?>
      <option class="text-capitalize" value="" disabled selected >Seleccione técnico</option>
      <?php endif ?>
      <?php Usuario::obtenerTecnicos() ?>            
    </select>
  </div>
  <div class="form-group col-md-3">
    <label for="orte" class="">Numero de Orte</label>
    <input style="border: none;" type="text" placeholder="Numero de Orte" id="orte" name="orte" class="border-bottom form-control x2 " required  value="<?= $ObjetoOperacion->numeroOrte ?>">
  </div>
        <div class="form-group col-md-3">
    <label for="orte" class="">Orden Contrato</label>
    <input style="border: none;" type="text" placeholder="Orden Contrato" id="ordenContrato" name="ordenContrato" class="border-bottom form-control x2 " required="" value="<?= $ObjetoOperacion->ordenContrato ?>">
  </div>
</div>




    <h2 class="text-center">Datos de Facturacion</h2>
    <div class="row">
        <div class="form-group col-md-3">
            <label for="" class="" style="color:steelblue">Numero Orden Facturacion</label>
            <input style="border: none;" type="text" placeholder="Numero Orden Fact" id="numeroOrdenF" name="numeroOrdenF" class="numeroOrden border-bottom form-control" autocomplete="off" value="<?php echo $df->orden ?>">
        </div>

    <div class="form-group col-md-4">
        <label for="corigen" class="">Concesionario Origen</label>
        <input style="border: none;" type="text" placeholder="Concesionario Origen" id="corigen" name="corigen" class="border-bottom form-control text-uppercase corigen " autocomplete="off" readonly value="<?= $df->canalOrigen ?>" >
    </div>
    </div>
<?php if ($ObjetoOperacion->lugarInstalacion=="Domicilio"): ?>
<div class="row">
   <div class="col-md-3"><h3>Coordenadas de instalación --></h3></div>
   <div class="col-md-6">
    <input style="border: none;" type="text" placeholder="Coordenadas insalaicon" id="orden" name="orden" class="border-bottom form-control x2 " value="<?php echo $ObjetoOperacion->coordenadasInstalacion ?>" >
  </div>
</div>
<?php endif ?>
<hr>
<h4>Lugar de instalación <?php echo $ObjetoOperacion->lugarInstalacion ?> <?php echo $retVal = (!empty($ObjetoOperacion->nombreConcecionarioInstalicion)) ? $ObjetoOperacion->nombreConcecionarioInstalicion : "" ; ?>  del dia <?= formatoHaceEn($ObjetoOperacion->fechaInstalacion) ?></h4>
<hr>

<input type="hidden" name="idoperacion" value="<?php echo $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idvh" value="<?php echo $vehiculoObj->idvehiculo ?>">
<input type="hidden" name="datosfacturacionid" value="<?php echo $ObjetoOperacion->datosfacturacionid ?>">

<?php if ($ObjetoOperacion->coordenadasInstalacion != ''): ?>
  <div class="row"><div class="col-lg-12"><label for="cordenadas">Coordenadas de instalacion: </label><label > <?php echo $ObjetoOperacion->coordenadasInstalacion ?></label><br><h1 class="text-success"><a  target="_blank" href="mapa.php?coordenadas=<?php echo $ObjetoOperacion->coordenadasInstalacion ?>&lugar=<?php echo $ObjetoOperacion->direccionInstacion ?>">ver mapa</a></h1></div></div>    
<?php endif ?>


  <div class="row">
       <div class="form-group col-md-4">
          <label for="placa" class="">Tecnico Asignado</label>
          <?php $usuarioAsgi=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoAsignado) ?>
          <input style="border: none;" type="text" placeholder="Tecnico Asignado" id="placa" name="" class="border-bottom form-control x2 text-uppercase" required value="<?= $usuarioAsgi->usuarioNombre.' '.$usuarioAsgi->usuarioApellidoPaterno ?>">
        </div>
       <div class="form-group col-md-4">
       <?php $usuarioInst=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoInstalador) ?>
          <label for="placa" class="">Tecnico instalador</label>
          <input style="border: none;" type="text" placeholder="Tecnico instalador" id="placa" name="" class="border-bottom form-control x2 text-uppercase" required value="<?= $usuarioInst->usuarioNombre.' '.$usuarioInst->usuarioApellidoPaterno ?>">
        </div>
       <div class="form-group col-md-4">
       <?php $usuarioCentr=Usuario::obtenerUsuario($ObjetoOperacion->idusuarioActivador) ?>
          <label for="placa" class="">Usuario cetral Hora</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="" class="border-bottom form-control x2 text-uppercase" required value="<?= $usuarioCentr->usuarioNombre.' '.$usuarioCentr->usuarioApellidoPaterno ?>">
          <br><small class="text-danger">hora <?= haceMinimo($ObjetoOperacion->fechaActivacion) ?></small>
        </div>
  </div>
<div class="row">
    <div class="col-md-12">

        <?php $filess=DatosFacturacion::obtenerFiles($df->datosfacturacionid) ?>
        <?php if($filess):  ?>
        <table class="table table-hover">
        <?php foreach($filess as $file): ?>
            <tr>
                <td><?= $file->fecha ?></td>
                <td><?= $file->oficina ?>&nbsp;&nbsp;<span class="fa fa-file-pdf-o"></span></td>
                <td><?= $file->tipoCanal ?></td>
                <td><?= $file->tipoCanal ?>&nbsp;
                    <a
                    href="/ope/downinvdoc?tnk=fef56f6a7c51ab802&b=<?= $file->voucher ?>&f=<?= $file->apunte ?>&fn=<?= strtoupper($file->oficina) ?> <?= trim($cl)." ".$vehiculoObj->placa ?>"
                    target="_blank"><span class="fa fa-download"></span></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </table>
        <?php else: ?>
            <?php if(!is_null($ObjetoOperacion->ordenContrato)){ ?>
            <a class="btn btn-primary" href="/ope/downinvdoc?tnk=eef56f6a7c51ab800&o=<?= $ObjetoOperacion->ordenContrato ?>&d=<?=$df->datosfacturacionid ?>" title="Generar">
                    <span class="fa fa-file"><span><i class="fa fa-plus"></i></span>
                </a>
            <?php }  ?>
        <?php endif; ?>
    </div>
    </div>

<?php } ?>


<?php if (!empty($ObjetoOperacion->datosfacturacionid)): ?>    
  <?php 
    $objFacturacion=DatosFacturacion::obtenerDatosFacturacionPorId($ObjetoOperacion->datosfacturacionid);
   ?>
<?php $opeusu=Usuario::obtenerUsuario($objFacturacion->idUsuarioCreado);  ?>
<h4>Ejecutivo: <?php  echo $opeusu->usuarioNombre." ".$opeusu->usuarioApellidoPaterno;  ?></h4>
  <?php if (empty($objFacturacion->voucher)): ?> 
    <h3 class="text-center text-danger">Atención pago en caja</h3>
    <input type="hidden" name="pagoEnCaja" value="ok">
  <?php else: ?>
<h3 class="text-center">Voucher</h3>
<img class="text-center" width="800" src="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" alt="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" >
<div class="row">
    <a  class="btn btn-primary text-center  col-md-6 col-md-offset-3" href="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" target="_blank" >Descargar/ver Voucher</a>    
</div>
  <?php endif ?> 
  <?php endif ?> 

<?php
} 
?> 
<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../../assets/build/jquery.datetimepicker.full.js"></script>
<!-- Mañana 9 am callle calos fereeiros 377 san isidro .... javier prado con guardia civil ... -->
<script>  
$(".departamentoInstalacionD").click(function(){provincias4();});
$(".departamentoInstalacionC").click(function(){provincias5();});
$(".provinciaInstalacionD").click(function(){distritos4();});
$(".provinciaInstalacionC").click(function(){distritos5();});
function provincias4(){
  var iddepartamento = $(".departamentoInstalacionD").val();
  $.ajax({        
    type : 'POST',
    url  : '../com/ajax/provincia',
    data :  'iddepartamento='+ iddepartamento,
    success : function(data)
    {
      $(".provinciaInstalacionD").html(data);
    }
  });
  return false;
}
function provincias5(){
  var iddepartamento = $(".departamentoInstalacionC").val();
  $.ajax({        
    type : 'POST',
    url  : '../com/ajax/provincia',
    data :  'iddepartamento='+ iddepartamento,
    success : function(data)
    {
      $(".provinciaInstalacionC").html(data);
    }
  });
  return false;
}

  function distritos4(){
      var idprovincia = $(".provinciaInstalacionD").val();
      $.ajax({        
        type : 'POST',
        url  : '../com/ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionD").html(data)
        }
      });
      return false;
    }
    function distritos5(){
      var idprovincia = $(".provinciaInstalacionC").val();
      $.ajax({        
        type : 'POST',
        url  : '../com/ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionC").html(data)
        }
      });
      return false;
    }
<?php $fechaA=date('Y-m-d') ?>
  jQuery.datetimepicker.setLocale('es');
  $('#fechaIT').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d',        
    scrollMonth : false,
    scrollInput: false,
    timepicker:false
    // allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });
  $('#horaIT').datetimepicker({
    minDate:'<?= $fechaA ?>',
    // inline:true,
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','11:00','14:00','16:00']
  });

  $('#fechaIC').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d',
    scrollInput: false,
    timepicker:false,
    scrollMonth : false
  });   
  $('#horaIC').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','11:00','14:00','16:00']
  });


   $('#fechaID').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y/m/d',
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
  });
  $('#horaID').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','11:00','14:00','16:00']
  });

function tthoras() {
    var arr="";
    var seleccionado = $("#plan").find('option:selected');
    var idtec = seleccionado.data('type');
    var arr = $('[name="servad[]"]:checked').map(function(){
        return this.value;
    }).get();

    var str = arr.join(',');
    if( str==='' && (idtec === undefined)){
        str='0';
        idtec='0';
    }

    $.ajax({
        type : 'POST',
        url  : 'ajax/cantidadHoraProgramacion',
        data :  'idCheck='+str+"&idTec="+idtec,
        success : function(data)
        {
            if(data!=''){
                $("#canthoraprog").val(data);
                $(".canthoraprog2").text(data+" Horas");
            }else{
                console.log("No hay data");
            }
        }
    });
    return false;
}

  ///////////////////////////////////////////////////
  $( document ).ready(function() {tthoras();
    var fecha= $("#fechaIT").val(); 
    var hora= $("#horaIT").val();     
      fecha=fecha+' '+hora;
    $.ajax({
        type : 'POST',
        url  : '../includes/programacionAjaxNew',
        data : {fecha:fecha},
        success : function(data)
        {
         $("#programacionHtml").html(data);
        }
      });

});
 $("#horaIT").bind("load change ",function()
    { 
      var hora = $(this).val(); 
      var fecha= $("#fechaIT").val();     
      fecha=fecha+' '+hora;      
      $.ajax({
        type : 'POST',
        url  : '../includes/programacionAjaxNew',
        data : {fecha:fecha},
        success : function(data)
        {
         $("#programacionHtml").html(data);
        }
      });
      
      return false;

    });
 $("#fechaIT").bind("change",function()
    { 
      var fecha = $(this).val(); 
      var hora= $("#horaIT").val();     
      fecha=fecha+' '+hora;
      if (hora!='') {
        $.ajax({
          type : 'POST',
          url  : '../includes/programacionAjaxNew',
          data : {fecha:fecha},
          success : function(data)
          {
           $("#programacionHtml").html(data);
          }
        });
      }      
      return false;
    });
 $("#horaIC").bind("load change ",function()
    { 
      var horaIC = $(this).val(); 
      var fechaIC= $("#fechaIC").val();     
      fechaIC=fechaIC+' '+horaIC;      
      $.ajax({
        type : 'POST',
        url  : '../includes/programacionAjaxNew',
        data : {fecha:fechaIC},
        success : function(data)
        {
         $("#programacionHtml").html(data);
        }
      });
      
      return false;

    });
 $("#fechaIC").bind("change",function()
    { 
      var fechaIC = $(this).val(); 
      var horaIC= $("#horaIC").val();     
      fechaIC=fechaIC+' '+horaIC;
      if (horaIC!='') {
        $.ajax({
          type : 'POST',
          url  : '../includes/programacionAjaxNew',
          data : {fecha:fechaIC},
          success : function(data)
          {
           $("#programacionHtml").html(data);
          }
        });
      }      
      return false;
    });

 $("#horaID").bind("load change ",function()
    { 
      var horaID = $(this).val(); 
      var fechaID= $("#fechaID").val();     
      fechaID=fechaID+' '+horaID;      
      $.ajax({
        type : 'POST',
        url  : '../includes/programacionAjaxNew',
        data : {fecha:fechaID},
        success : function(data)
        {
         $("#programacionHtml").html(data);
        }
      });
      
      return false;

    });
 $("#fechaID").bind("change",function()
    { 
      var fechaID = $(this).val(); 
      var horaID= $("#horaID").val();     
      fechaID=fechaID+' '+horaID;
      if (horaID!='') {
        $.ajax({
          type : 'POST',
          url  : '../includes/programacionAjaxNew',
          data : {fecha:fechaID},
          success : function(data)
          {
           $("#programacionHtml").html(data);
          }
        });
      }      
      return false;
    });

 function esconder(){  
  $("#programacionHtml").html("");
 }

function validar(){
  var lugarI=$( "input[name='lugarInstacion']:checked" ).val()
  if (lugarI=='Taller') {
      $('.con').prop('disabled', true);
      $('.dom').prop('disabled', true);
      $('.tal').prop('disabled', false);
  }else if (lugarI=='Domicilio Cliente') {
    $('.con').prop('disabled', true);
      $('.dom').prop('disabled', false);
      $('.tal').prop('disabled', true);     
  }else if (lugarI=='Concesionario') {
    $('.con').prop('disabled', false);
      $('.dom').prop('disabled', true);
      $('.tal').prop('disabled', true);
  }
}

 $(document).ready(function(){
  $( "input[name='lugarInstacion']" ).on( "click",validar);   
  validar();
});




///////////////////////////////////////////////////

</script>

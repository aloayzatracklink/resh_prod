<?php 
if ($_POST['rowid']) {
  include_once("../../includes/init.php");
  require_once '../../includes/tablaProgramacion.php';
  $idOperacion=htmlspecialchars(htmlentities($_POST['rowid']));
  $clienteObjModal=new Cliente;
  $prograT=new tablaProgramacion;
  $ObjetoOperacion=Operacion::obtenerOperacionPorId($idOperacion);
  $datoT=false;
  $prograT->idoperacion=$idOperacion;
  $datoT=$prograT->obtenerPorIdoperacion();
  $fail=false;
  $msj="";
  if ($ObjetoOperacion->idlocation==0) {
    $vehiculoObj=Vehiculo::obtenerVehiculo($ObjetoOperacion->idvehiculo);
    $clienteObj=Cliente::obtenerClientePorVehiculo($ObjetoOperacion->idvehiculo,$ObjetoOperacion->idcliente);
     $objFacturacion=DatosFacturacion::obtenerDatosFacturacionPorId($ObjetoOperacion->datosfacturacionid);
    $clienteObjO=Onix::obtenerDatosDesdeOrden($objFacturacion->orden);
    #var_dump($clienteObjO);
    if(!$clienteObjO){
        $fail=true;
        $msj.=" <hr><b>La orden no existe en Onyx</b>";

    }else{

        if($vehiculoObj->chasis!=$clienteObjO->chasis){
                $fail=true;
                $msj.=" <hr><b>El vehiculo no coincide con la orden </b>";
        }
        if(($clienteObj->dni==""?$clienteObj->ruc:$clienteObj->dni)!=$clienteObjO->dni){
                $fail=true;
                $msj.=" <hr><b>El cliente no coincide con la orden </b>";
        }
    }

    }else{
    $clienteObj=Onix::obtenerDatosDesdeIds($ObjetoOperacion->idcliente,$ObjetoOperacion->idvehiculo,$ObjetoOperacion->idlocation) ;
    $chequeoObj=Chequeos::obtenerChequeoTrackPorId($ObjetoOperacion->idChequeo);
    $vehiculoObj=false;
  }
   if (!isset($_POST['gestionTecnica'])) {
    Operacion::asignarCagoOperaciones($_POST['rowid']);
  }
  ?>
  <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
  <h3 class="text-center text-danger"><b><?php echo $ObjetoOperacion->trabajo ?> </b></h3>
  <input type="hidden" name="trabajoO" value="<?= $ObjetoOperacion->trabajo  ?>" >
  <input type="hidden" name="fechaInstalacionAnt" value="<?= $ObjetoOperacion->fechaInstalacion  ?>" >
  <?php if ($ObjetoOperacion->idChequeo!=0){ ?>
    <h3 id="mot" class="text-center text-danger">MOTIVO: <?= $chequeoObj->motivo ?></h3>
    <h3 id="seguimiento" class="text-center text-danger">Seguimiento realizado: <?= $chequeoObj->seguimientoRealizado ?></h3>
  <?php } ?>
  <?php if (!empty($ObjetoOperacion->datosfacturacionid)){ ?>    
  <?php 
   ?>
  <?php if (empty($objFacturacion->voucher)){ ?> 
    <?php if ($objFacturacion->tipoDocumentoFacturacion=='pacificoPandero'|| $objFacturacion->tipoDocumentoFacturacion=='pacificoBanbif'|| $objFacturacion->tipoDocumentoFacturacion=='Banbif'){ ?>
      <span class="text-center"><?=  $objFacturacion->tipoDocumentoFacturacion ?></span>
    <?php }else{ ?>
          <span class="text-center"><?=  $objFacturacion->tipoDocumentoFacturacion ?></span>
          <h3 class="text-center text-danger">Atención pago en caja</h3>
          <input type="hidden" name="pagoEnCaja" value="ok">
    <?php } ?>
  <?php } ?> 
  <?php } ?>
  <?php if ($vehiculoObj){ ?>
<!-- ORDENES INSTALACIONES FIN END END END END  -->
<?php 
if ($vehiculoObj->estado==1 OR $ObjetoOperacion->estado==14) {
  ?>
  <br>  
  <div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->dni ?>">       
      <?php else: ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->ruc ?>">        
      <?php endif ?>  
    </div>
    <div class="col-md-9">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->razonSocial ?>">       
      <?php endif ?>
    </div>   
  </div>  
  </div>
  <div class="row">
    <div class="col-lg-12 field_wrapper"> 
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="field_name[0][]" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $vehiculoObj->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="">Chasis | Motor</label>
          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" class="border-bottom form-control text-uppercase" required
          value="<?php echo $vehiculoObj->chasis ?> | <?php echo $vehiculoObj->motor ?>">
        </div>
        <div class="form-group col-md-6">
          <label for="motor" class="">Marca | Modelo |Color | Año</label>
          <input style="border: none;" type="text" placeholder="Motor" id="motor"
           class="border-bottom form-control text-uppercase"
          required value="<?php echo $vehiculoObj->marca ?> | <?php echo $vehiculoObj->modelo ?> | <?php echo $vehiculoObj->color ?> | <?php echo $vehiculoObj->anio ?>" >
        </div>
      </div> 
    </div>
  </div> 

<div class="row">
<div class="form-group col-md-3">
   <label for="plan" class="">Plan </label>
   <select  style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
      <option class="" value="<?php echo $vehiculoObj->plan ?>"  selected ><?php echo $vehiculoObj->plan ?></option>      
    
    <!-- <?php Cliente::obtenerPlan() ?>       -->
  </select> 
</div>
<div class="form-group col-md-1">
   <label for="plan" class="">Tiempo </label>
   <input style="border: none;" type="text" placeholder="Corredor de seguros." id="corredor" readonly="" class="border-bottom form-control text-capitalize"   value="<?= $objFacturacion->tiempo[0] ?>">
</div>
<div class="form-group col-md-2">
   <label for="plan" class="">Precio </label>
   <input style="border: none;" type="text" placeholder="Corredor de seguros." id="corredor" readonly="" class="border-bottom form-control text-capitalize"   value="<?= $objFacturacion->costo ?>">
</div>
<div class="form-group col-md-3">
   <label for="plan" class="">Numero orden facturación</label>
   <input style="border: none;" type="text" placeholder="Corredor de seguros." id="corredor"  name="numOrden" readonly="" class="border-bottom form-control text-capitalize"   value="<?= $objFacturacion->orden ?>">
</div>
<div class="form-group col-md-3">
   <label for="plan" class="">Estado Facturación</label><br>
  <?= obtenerEstado($objFacturacion->estadoFacturacion) ?>
</div>

    <div class="form-group col-md-12">
        <label for="corigen" class="">Concesionario Origen</label>
        <input style="border: none;" type="text" placeholder="Concesionario Origen" id="corigen" name="corigen" class="border-bottom form-control text-uppercase corigen " autocomplete="off" readonly value="<?= $objFacturacion->canalOrigen ?>" >
    </div>
    <div class="form-group col-md-12 ">
        <label  class="text-danger" style="font-size: 15px">Campaña : <?php echo $vehiculoObj->campana ?> </label>
    </div>
    <div class="form-group col-md-12 ">
        <p style="font-weight:bold" class="text-danger">Servicios Adicionales Seleccionados :</p>
        <?php ServiciosAdicionales::obtenerServAdicionalSeleccionadoVer($idOperacion) ?>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-2">
        <label for="plan" class="">Tecnico </label>
    </div>
    <div class="form-group col-md-4">
        <select  style="border: none;" class="cboTenico border-bottom form-control text-capitalize" name="idtecnico"  id="idtecnico" >
            <option class="text-capitalize" value=""  selected >Seleccione Tecnico* </option>
                <?php $objTec=Operacion::obtenerTecnios() ?>
                <?php if ($objTec): ?>
                    <?php foreach ($objTec as $var): ?>
                        <option value="<?php echo $var->idtec ?>"><?php echo $var->nomtec ?></option>
                    <?php endforeach ?>
                <?php else: ?>
                <?php endif ?>
        </select>
    </div>
</div>

<?php if (!$fail){ ?>
<div class="row well">
        <div class="form-group col-md-4" >
            <label for="Observaciones"  > OBSERVACIONES: <?= $clienteObjO->nombre." ".$clienteObjO->apellidos ?> </label>
            <div style="border-right: solid;height: 90px;
overflow-y: auto;">
            <label class="border-" ><?php echo utf8_encode($clienteObjO->comentarios) ?></label>
            </div>
        </div>
        <div class="col-md-8 form-group"  >

        <label for="">ORDENES <?= $clienteObjO->placa ?> |<?= $clienteObjO->chasis ?></label>
    <div style="height: 100px; overflow-y: auto;">
            <?php $objOrdenes=Onix::obtenerOrdenesVehiculo($clienteObjO->idcustomer,$clienteObjO->idvehicle,$clienteObjO->idlocation) ?>
            <?php foreach ($objOrdenes as $var): ?>
                    <?= $orden=$var->idlocation."-".$var->idorder ?> <?= utf8_encode($var->articulo) ?><br>
                    <?php   $DataOrd=Onix::obtenerDatosFacturacion($orden) ?>
                    <?php   $DataOrt=Onix::obtenerORTE($orden) ?>
                    <?= ($DataOrd)?$DataOrd->serie.$DataOrd->numeroFactura."  $".round($DataOrd->precioO*1.18,2):""; ?>
                    <?= ($DataOrt->orteO!='')?"<b>ORTE ".$DataOrt->orteO."</b> ":'' ?> <small><?= haceMinimoAnio($var->fechaOrden) ?></small>
                    &nbsp;&nbsp;&nbsp;<p class="btn btn-primary btn-sm"
                    onclick="completeData(<?= $var->idlocation ?>,<?= $var->idorder ?>, <?= ($DataOrt->orteO!='')?$DataOrt->orteO:'' ?>)"
                    >Seleccionar</p>
                    <hr>
            <?php endforeach ?>
            </div>
        </div>
    </div>
    <?php } ?>

<?php if (!$fail){ ?>
<input type="hidden" name="idoperacion" value="<?php echo $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idvh" value="<?php echo $vehiculoObj->idvehiculo ?>">
<input type="hidden" name="datosfacturacionid" value="<?php echo $ObjetoOperacion->datosfacturacionid ?>">
<div class="row">
  <div class="col-md-10">
    <h4 class="text-center text-danger">Observaciones: <?= $ObjetoOperacion->observacion ?></h4>
  </div>
  <div class="col-md-2">
      <div class="btn btn-warning" onclick="reprogramarChequeo()">Reprogramar</div>
  </div>
</div>
  
  <span class="ocultarThis">
<?php if ($ObjetoOperacion->coordenadasInstalacion != ''): ?>
  <div class="row"><div class="col-lg-12"><label for="cordenadas">Coordenadas de instalacion: </label><label > <?php echo $ObjetoOperacion->coordenadasInstalacion ?></label><br><h3 class="text-success"><a  target="_blank" href="mapa.php?coordenadas=<?php echo $ObjetoOperacion->coordenadasInstalacion ?>&lugar=<?php echo $ObjetoOperacion->direccionInstacion ?>">ver mapa</a></h3></div></div>    
<?php endif ?>
<input style="border: none;" readonly type="hidden" placeholder="Trabajo" id="trabajo" name="trabajo" class="trabajo border-bottom form-control" required value="<?=  $ObjetoOperacion->trabajo ?>">
<div class="row">
  <div class="form-group col-md-6">
      <label for="orte" class="">Orden contrato</label>
    <input style="border: none;" readonly value="<?= !is_null($ObjetoOperacion->ordenContrato)?$ObjetoOperacion->ordenContrato:'' ?>" type="text" placeholder="Numero de orden contrato" id="ordenContrato" name="ordenContrato" class="border-bottom form-control x2 " required  >
  </div>

    <div class="form-group col-md-6">
  <label for="orte" class="">Numero de Orte <?= !empty($ObjetoOperacion->numeroOrte)? "<span class='text-danger' > Anterior número de orte es: ".$ObjetoOperacion->numeroOrte."<span>" : '' ?> </label>
    <input style="border: none;" readonly  type="text" placeholder="Numero de Orte" id="orte" name="orte" class="border-bottom form-control x2 " required  >
  </div>
</div> 


<div role="tablist">
  <input type="radio" title="taller" name="lugarInstacion" id="taller" role="tab" aria-controls="taller-panel"  value="Taller" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Taller")?"checked":"" ?> />
  <input type="radio" title="domicilio" name="lugarInstacion" id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Domicilio Cliente")?"checked":"" ?> />
  <input type="radio" title="concesionario" name="lugarInstacion" id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Concesionario")?"checked":"" ?> />
  <div class="tabs" aria-hidden="true">
    <label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
    <label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
    <label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
  </div>
  <div class="panels">
    <ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">
      <h2>TALLER</h2>
      <p>Calle ricardo angulo 762 </p>
      <p>San Isidro, Lima, Lima</p>
      <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionT" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionT border-bottom form-control text-capitalize tal" required name="departamentoInstalacionT" id="departamentoInstalacionT">                  
                         <option class="text-capitalize" value="15"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId('15') ?><?= $departamento->departamento ?></option>
                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionT" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionT border-bottom form-control text-capitalize tal" required name="provinciaInstalacionT" id="provinciaInstalacionT" >
                    <?php //if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId('127') ?><?php echo $provincia->provincia ?></option>
                     <?php
                      //} ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionT" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionT border-bottom form-control text-capitalize tal" required name="distritoInstalacionT"  id="distritoInstalacionT" >
                <?php// if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId('1291') ?><?= $distrito->distrito ?></option>
                    <?php
                      //} ?>                 
              </select>           
          </div>

        </div>
      <input type="hidden" name="direccionTaller" value="Av. Paseo de la República 4925 con Angamos Este - Surquillo ">      
      <input type="hidden" name="referenciaT" value="">
    <div class="row" >
        <div class="form-group col-md-4">   
                      <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
              <input type="text" class="form-control tal tala fecha"  id="fechaIT" name="fechaIT" autocomplete="off" required value='<?= substr($ObjetoOperacion->fechaInstalacion, 0,10)  ?>'>
          </div>
        </div>
        <div class="form-group col-md-4">
            <label for="turno" class="">Turno</label>
                <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn form-control border-bottom btn btn-info" >Cambiar hora</button>
                <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posT" class="hora form-control border-bottom"  required="" readonly value='<?= ($datoT)? $datoT->idturno :"" ?>'>
        </div>
        <div class="form-group col-md-4">
                <label for="horaS" class="">Hora seleccionada</label>
                <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaT" class="horaVer tala tal form-control border-bottom"  required="" readonly value="<?= substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>">
        </div>
    </div> 
      <div class="row">
            <div class="form-group col-md-6">
              <label for="nombreContactoIT" class="">Nombre Contacto *</label>
              <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIT" name="nombreContactoIT" class="form-control x2 text-capitalize border-bottom tal" value="<?= ($ObjetoOperacion->nombreContacto=='')?'':$ObjetoOperacion->nombreContacto ?>">
            </div><div class="form-group col-md-6">
            <label for="numeroContactoIT" class="">Número Contacto *</label>
            <input style="border: none;" type="text" placeholder=" Número Contacto *" id="numeroContactoIT" name="numeroContactoIT" class="form-control x2 text-capitalize border-bottom tal"  value="<?= ($ObjetoOperacion->numeroContacto=='')?'':$ObjetoOperacion->numeroContacto ?>">
          </div>
        </div>
        <?php if ($ObjetoOperacion->idReprogramado!=0): ?>
          <div class="row">
            <h3 for="" class="text-danger text-center col-md-12">Reprogramacion: <?=  $ObjetoOperacion->observacion ?></h3><br>
          </div>
        <?php endif ?>
    </ul>
    <ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab"> 
      <h2>Domicilio Cliente</h2>
      <div class="row">
         <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionD" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize dom" required name="departamentoInstalacionD" id="departamentoInstalacionD">
                    <?php if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                        <option class="text-capitalize " value=""  selected  disabled="">Seleccione departamento</option>
                            <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                          <?php if ($objDepartamento): ?>
                            <?php foreach ($objDepartamento as $var): ?>
                              <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                            <?php endforeach ?>
                          <?php else: ?>
                          <?php endif ?>
                      <?php
                    }else{
                      ?>
                         <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
                      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                      <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                      <?php else: ?>
                      <?php endif ?>

                      <?php
                      } ?>                 
                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionD" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize dom" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
                    <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
                     <?php
                      } ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionD" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize dom" required name="distritoInstalacionD"  id="distritoInstalacionD" >
                <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
                    <?php
                      } ?>                 
              </select>           
          </div>

        </div>
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="direccionInstalacionD" class="">Direccion*</label>
            <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionD" name="direccionInstalacionD"  class="form-control border-bottom dom" value="<?= (!empty($ObjetoOperacion->direccionInstacion))?$ObjetoOperacion->direccionInstacion : ''  ?>">
          </div>
          <div class="form-group  col-md-6">
            <label for="referenciaInstalacionD" class="">Referencia *</label>
            <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" name="referenciaInstalacionD" class="form-control border-bottom  dom" value="<?= (!empty($ObjetoOperacion->referenciaInstalacion)) ? $ObjetoOperacion->referenciaInstalacion:'' ?>" >
          </div>
        </div>  

        <div class="row">

          <input style="border: none;" type="hidden" placeholder="Cordenadas Domicilio *" id="cod" name="cordenadasD" class="form-control x2 text-capitalize border-bottom dom" value="<?= (!empty($ObjetoOperacion->coordenadasInstalacion))? $ObjetoOperacion->coordenadasInstalacion:'' ?>">
          <!--taller-link-r-->
          <div class="form-group col-md-6">
            <label for="latitud" class="">Latitud*</label>
            <input style="border: none;" type="text" placeholder="EJEMPLO: -12.098523" id="latitud" name="latitud" class="form-control x2 text-capitalize border-bottom dom" value="<?= (!empty($ObjetoOperacion->latitud))? $ObjetoOperacion->latitud:'' ?>">
          </div>

          <div class="form-group col-md-6">
            <label for="longitud" class="">Longitud*</label>
            <input style="border: none;" type="text" placeholder="EJEMPLO: -77.013813" id="longitud" name="longitud" class="form-control x2 text-capitalize border-bottom dom" value="<?= (!empty($ObjetoOperacion->longitud))? $ObjetoOperacion->longitud:'' ?>">
          </div>


        </div>  
      <div class="row">
        <div class="form-group col-md-4">   
            <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
              <input type="text" class="form-control dom domo fecha"  id="fechaID" name="fechaID" autocomplete="off" required value='<?= substr($ObjetoOperacion->fechaInstalacion, 0,10)  ?>'>
          </div>
        </div>
        <div class="form-group col-md-4">
            <label for="turno" class="">Turno</label>
                <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Cambiar hora</button>
                <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posD" class="hora form-control border-bottom"  required="" readonly value='<?= ($datoT)? $datoT->idturno :"" ?>'>
        </div>
        <div class="form-group col-md-4">
                <label for="horaS" class="">Hora seleccionada</label>
                <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaD" class="horaVer domo dom  form-control border-bottom"  required="" readonly value="<?= substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>"> 
        </div>
         </div>
        <div class="row">
          <div class="form-group col-md-6">
            <label for="nombreContactoID" class="">Nombre Contacto *</label>
            <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoID" name="nombreContactoID" class="form-control x2 text-capitalize border-bottom dom"  value="<?= ($ObjetoOperacion->nombreContacto=='')?'':$ObjetoOperacion->nombreContacto ?>">
          </div><div class="form-group col-md-6">
          <label for="numeroContactoID" class="">Número Contacto *</label>
          <input style="border: none;" type="text" placeholder=" Número Contacto *" id="numeroContactoID" name="numeroContactoID" class="form-control x2 text-capitalize border-bottom dom"  value="<?= ($ObjetoOperacion->numeroContacto=='')?'':$ObjetoOperacion->numeroContacto ?>" >
        </div>
      </div>
    </div> 
  </ul>
  <ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">    
    <div class="row">
      <div class="col-md-6"><h2>Concesionario</h2></div>
      <div class="col-md-6">
        <label for="concesionarioInstalacion" class="">Nombre de concesionario*</label>
        <input style="border: none;" type="text" placeholder="Nombre de concesionario*" id="concesionarioInstalacion" name="concesionarioInstalacion" class="form-control border-bottom con" value="<?= (!empty($ObjetoOperacion->nombreConcecionarioInstalicion))?$ObjetoOperacion->nombreConcecionarioInstalicion:''  ?>" >
      </div>
    </div>
    <div class="row">
     <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionC" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionC border-bottom form-control text-capitalize con" required name="departamentoInstalacionC" id="departamentoInstalacionC">
                    <?php if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                        <option class="text-capitalize" value=""  selected  disabled="">Seleccione departamento</option>
                            <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                          <?php if ($objDepartamento): ?>
                            <?php foreach ($objDepartamento as $var): ?>
                              <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                            <?php endforeach ?>
                          <?php else: ?>
                          <?php endif ?>
                      <?php
                    }else{
                      ?>
                         <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
                      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                      <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                      <?php else: ?>
                      <?php endif ?>

                      <?php
                      } ?>                 


                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionC" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionC border-bottom form-control text-capitalize con" required name="provinciaInstalacionC" id="provinciaInstalacionC" >
                    <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
                     <?php
                      } ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionC" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionC border-bottom form-control text-capitalize con" required name="distritoInstalacionC"  id="distritoInstalacionC" >
                <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
                    <?php
                      } ?>                 
              </select>           
          </div>

        </div>
      <div class="row">
        <div class="form-group  col-md-6">
          <label for="direccionInstalacionC" class="">Direccion*</label>
          <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom con" value="<?= $ObjetoOperacion->direccionInstacion ?>" >
        </div>
        <div class="form-group  col-md-6">
          <label for="referenciaInstalacionC" class="">Referencia *</label>
          <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom con"  value="<?= $ObjetoOperacion->referenciaInstalacion ?>" >
        </div>
      </div>    
                <div class="row">
                  <div class="form-group col-md-4">   
                      <label  class="">Fecha</label>
                      <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control con cono fecha"  id="fechaIC" name="fechaIC" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10)  ?>">
                      </div>
                  </div>
        <div class="form-group col-md-4">
            <label for="turno" class="">Turno</label>
                <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn  form-control border-bottom btn btn-info" >Cambiar hora</button>
                <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posC" class="hora form-control border-bottom"  required="" readonly value='<?= ($datoT)? $datoT->idturno :"" ?>'>
        </div>
        <div class="form-group col-md-4">
                <label for="horaS" class="">Hora seleccionada</label>
                <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaC" class="horaVer cono con form-control border-bottom"  required="" readonly value="<?= substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>" >
        </div>
      </div> 
      <div class="row">
        <div class="form-group col-md-6">
          <label for="nombreContactoIC" class="">Nombre Contacto *</label>
          <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIC con" name="nombreContactoIC" class="form-control x2 text-capitalize border-bottom" value="<?= ($ObjetoOperacion->nombreContacto=='')?'':$ObjetoOperacion->nombreContacto ?>" >
        </div>
        <div class="form-group col-md-6">
        <label for="numeroContactoIC" class="">Número Contacto *</label>
        <input style="border: none;" type="text" placeholder=" Número Contacto *" id="numeroContactoIC" name="numeroContactoIC" class="form-control x2 text-capitalize border-bottom con"  value="<?= ($ObjetoOperacion->numeroContacto=='')?'':$ObjetoOperacion->numeroContacto ?>">
      </div>
    </div>
  </div>
</ul>
</div>
</div> 
<div class="row">
  <div class="col-md-12">
    <h4 class="text-center text-danger">Observaciones: <?= $ObjetoOperacion->observacion ?></h4>
  </div>
</div>
  <div id="programacionHtml"></div>
<div class="row">
<div class="form-group col-md-12">
<label for="observacionD" class="">Observaciones *</label>
<input style="border: none;" type="text" placeholder="Observaciones *" id="observacionD" name="observacion" class="form-control x2 text-capitalize border-bottom " >
</div>
</div>
<?php }else{ ?>
    <h2 class="text-center text-danger"><?= $msj ?> </h2>
<?php } ?>


<?php if ($ObjetoOperacion->coordenadasInstalacion != ''): ?>
  <div class="row"><div class="col-lg-12"><label for="cordenadas">Coordenadas de instalacion: </label><label > <?php echo $ObjetoOperacion->coordenadasInstalacion ?></label><br><h1 class="text-success"><a  target="_blank" href="mapa.php?coordenadas=<?php echo $ObjetoOperacion->coordenadasInstalacion ?>&lugar=<?php echo $ObjetoOperacion->direccionInstacion ?>">ver mapa</a></h1></div></div>    
<?php endif ?>
  
</span>
<div id="b" class="b">
<div class="row">  
  <div class="col-md-8 form-group">
    <input required  style="border: none;" type="text" placeholder="Motivo Reprogramacion *" id="motivoReprogramacionChequeo" name="motivoReprogramacion" class="border-bottom form-control"></div>
    <div class="btn btn-warning col-md-4" onclick="cancelar()">No reprogramar</div>
  </div>
</div>
<?php
}
?>
<?php  
// Fin de vizualizacion desde desde Onyx %$&$#%&%$& cheq
////////////////////// cehqueoafsds fsad
 ?>
<?php }else{ ?>
  <?php if ($ObjetoOperacion->trabajo!='Chequeo en linea'){ ?>
  <div class="btn btn-primary" onclick="reprogramarChequeo()">Reprogramar</div>
  <br>
<?php } ?>
  <div class="row">
        <div class="form-group col-md-3">
        <label for="Observaciones" > Usuario Web</label>
        <input name="usuarioWeb" style="border: none;" type="text" placeholder="Web" id="Observaciones" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>">
      </div>
      <div class="form-group col-md-5">
      <?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
        <label for="numeroTecnico" >Nombre de cliente*</label>
        <input  style="border: none;" type="text"  id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ; ?> ">
      </div>
      <div class="col-md-2"><label for="estado" >Celular</label>
        <input style="border: none;" type="text"  id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">
      </div>
      <div class="col-md-2"><label for="estado" >Celular</label>
        <input style="border: none;" type="text"  id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular2)) ? "$clienteObj->celular2" : "" ; ?>">
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-12">
        <label for="Observaciones"  >Correo cliente. </label>
        <input style="border: none;" type="text" placeholder="Correo" id="Observaciones"  class="border-bottom form-control" name="correoCliente" value="<?php echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo," : "$clienteObj->correo" ;echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo2" : "" ;  ?>">
      </div>
    </div>
     </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="form-group col-md-3">
    <label for="numeroTecnico" >Placa Vehiculo*</label>
    <input  style="border: none;"  type="text" placeholder="Placa" id="numeroTecnico" name="placa" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->placa)) ? "$clienteObj->placa" : "" ; ?>"   >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Marca *</label>
    <input required style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->marca)) ? "$clienteObj->marca" : "" ; ?>" >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Modelo*</label>
    <input  required style="border: none;" type="text" placeholder="Modelo" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($clienteObj->modelo)) ? "$clienteObj->modelo" : "" ; ?>">
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Color</label>
    <input style="border: none;" type="text" placeholder="Color" id="Observaciones" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->color)) ? "$clienteObj->color" : "" ; ?>">
  </div>
  
</div>

  <span class="ocultarThis">
<div class="row">
  <div class="form-group col-md-4">
    <label for="Observaciones"  >Chasis*</label>
    <input required style="border: none;" type="text" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->chasis)) ? "$clienteObj->chasis" : "" ; ?>">
  </div>
  <div class="form-group col-md-4">
    <label for="Observaciones" >Motor</label>
    <input style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="motor" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->motor)) ? "$clienteObj->motor" : "Actualizar" ; ?>"  >
  </div>
  <div class="form-group col-md-4">
    <label for="Observaciones" >Plan</label>
    <input  style="border: none;" type="text" placeholder="Observaciones"  id="Observaciones" name="plan" class="border-bottom form-control" value="<?= $chequeoObj->plan ?>"  >
  </div>
</div>
<input type="hidden" name="idoperacion" value="<?= $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idChequeo" value="<?= $ObjetoOperacion->idChequeo ?>">
  <?php if ($ObjetoOperacion->trabajo=='Chequeo en linea'){ ?>
    <div class="row"> 
  <div class="form-group col-md-12">
    <label for="Observaciones" >Número de orden</label>
    <input  style="border: none;" type="text" placeholder="Número de orden" id="numeroOrden" name="numeroOrden" class="border-bottom form-control"  >
    <input type="hidden" name="chequeo" value="ok">
    <input type="hidden" name="chequeoLinea" value="ok">

  </div>
</div>
  <?php }else{
    ?>
    <div class="row">
           <div class="form-group col-md-3">
    <label for="Observaciones" >Ubicacion de sistema*</label>
    <input  required readonly style="border: none;" type="text" placeholder="Ubicacion de sistema" id="Observaciones" name="ubsistema" class="border-bottom form-control"   value="<?= $chequeoObj->ubicacionSistema ?>">
  </div>      

    <div class="form-group col-md-5">
    <label for="numeroTecnico" >Fecha instalacion</label>
    <input  style="border: none;" readonly type="text" placeholder="Persona de contacto" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?= $chequeoObj->fechaInstalacion ?>"   >
  </div>
  <div class="form-group col-md-4">
    <label for="Observaciones" >Tecnico instalador *</label>
    <input required readonly="" style="border: none;" type="text" placeholder="Tecnico instalador* " id="Observaciones" name="tecnicoinstalador" class="border-bottom form-control"  value="<?= $chequeoObj->instalador ?>" >
  </div>

    </div>
<?php
    if($datos=$chequeoObj->verificarAnteriorExistencia()) {
        ?>
            
  <div class="row"> 
<div class="col-md-12">
<label class="text-danger">Anterior orden y orte</label>
 <table class="table">
<thead>
<tr>
<th>
Orden
</th>
<th>
Orte
</th>

</tr>
</thead>
<tbody>
<?php
    foreach($datos as $var){
        if ($var->placa!='') {
?>
<tr>
<td>
   <?= $var->chasis ?>     
 </td>
<td>
   <?= $var->placa ?>     
 </td>
</tr>
<?php
        }
    }
?>
</tbody>
 </table> 
</div>
</div>
<?php
    }
?>
<div class="row"> 
  <div class="form-group col-md-6">
    <label for="numeroOrden" >Número de orden</label>
    <input  style="border: none;" type="text" placeholder="Número de orden" id="numeroOrden" name="numeroOrden" class="border-bottom form-control" required="" >
    <input type="hidden" name="chequeo" value="ok">
  </div>
  <div class="form-group col-md-6">
    <label for="numeroOrte" >Número de orte</label>
    <input  style="border: none;" type="text" placeholder="Número de orte" id="numeroOrte" name="numeroOrte" class="border-bottom form-control" required >
    <input type="hidden" name="chequeo" value="ok" required>
  </div>
</div>

  <?php if ($ObjetoOperacion->trabajo=='Chequeo por monitoreo' || $ObjetoOperacion->trabajo=='Desinstalacion' || $ObjetoOperacion->trabajo=='Reinstalacion' ){ ?>
  <div class="row">
    <div class="form-group col-md-2">
        <label for="plan" class="">Tecnico </label>
    </div>
    <div class="form-group col-md-4">
        <select  style="border: none;" class="cboTenico border-bottom form-control text-capitalize" name="idtecnico"  id="idtecnico" >
            <option class="text-capitalize" value=""  selected >Seleccione Tecnico* </option>
                <?php $objTec=Operacion::obtenerTecnios() ?>
            <?php if ($objTec): ?>
                <?php foreach ($objTec as $var): ?>
                    <option value="<?php echo $var->idtec ?>"><?php echo $var->nomtec ?></option>
                <?php endforeach ?>
            <?php else: ?>
            <?php endif ?>
        </select>
    </div>
</div>
<?php } ?>
<?php if (!empty($chequeoObj->coordenadas)){ ?>
<h1>
<a target="_blank" href="mapa.php?coordenadas=<?= $chequeoObj->coordenadas ?>&lugar=<?= $chequeoObj->direccion ?>" class="text-center">Ver mapa</a>  
</h1>  
<?php } ?>
<div role="tablist">
  <input type="radio" title="taller" name="lugarInstacion" id="taller" role="tab" aria-controls="taller-panel"  value="Taller" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Taller")?"checked":"" ?> />
  <input type="radio" title="domicilio" name="lugarInstacion" id="domicilio" role="tab" aria-controls="domicilio-panel" value="Domicilio Cliente" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Domicilio Cliente")?"checked":"" ?> />
  <input type="radio" title="concesionario" name="lugarInstacion" id="concesionario" role="tab" aria-controls="concesionario-panel" value="Concesionario" class="lugarDeInstalacion"  <?= ($ObjetoOperacion->lugarInstalacion=="Concesionario")?"checked":"" ?> />
  <div class="tabs" aria-hidden="true">
    <label for="taller" id="taller-tab" class="tab"><h4>Taller</h4></label>
    <label for="domicilio" id="domicilio-tab" class="tab"><h4>Domicilio Cliente</h4></label>
    <label for="concesionario" id="concesionario-tab" class="tab"><h4>Concesionario</h4></label>
  </div>
  <div class="panels">
    <ul id="taller-panel" class="panel active" role="tabpanel" aria-labelledby="taller-tab">
      <h2>TALLER</h2>
      <p>Calle ricardo angulo 762</p>
      <p>San Isidro, Lima, Lima</p>
      <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionT" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionT border-bottom form-control text-capitalize tal" required name="departamentoInstalacionT" id="departamentoInstalacionT">                  
                         <option class="text-capitalize" value="15"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId('15') ?><?= $departamento->departamento ?></option>
                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionT" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionT border-bottom form-control text-capitalize tal" required name="provinciaInstalacionT" id="provinciaInstalacionT" >
                    <?php //if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId('127') ?><?php echo $provincia->provincia ?></option>
                     <?php
                      //} ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionT" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionT border-bottom form-control text-capitalize tal" required name="distritoInstalacionT"  id="distritoInstalacionT" >
                <?php// if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId('1291') ?><?= $distrito->distrito ?></option>
                    <?php
                      //} ?>                 
              </select>           
          </div>

        </div>
      <input type="hidden" name="direccionTaller" value="Av. Paseo de la República 4925 con Angamos Este - Surquillo ">      
      <input type="hidden" name="referenciaT" value="">
      <div class="row">
    <div class="form-group col-md-5" id="data_1">
      <div class="row">
        <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon "><i class="fa fa-calendar"></i></span>
<input type="text" class="form-control fechaa tal"  id="fechaIT" name="fechaIT" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10)  ?>">
          </div>
        </div>
        <div class="form-group col-md-6">
            <label  class="">Hora</label>
          <div class="input-group" data-autoclose="true">
              <input type="text" class="form-control horaa tal" id="horaIT" name="horaIT" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>" >
              <span class="input-group-addon">
                  <span class="fa fa-clock-o"></span>
              </span>
          </div>
        </div>
      </div>
    </div> 
            <div class="form-group col-md-4">
              <label for="nombreContactoIT" class="">Nombre Contacto *</label>
              <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIT" name="nombreContactoIT" class="form-control x2 text-capitalize border-bottom tal" value="<?= ($chequeoObj->personaContacto=='')?'':$chequeoObj->personaContacto ?>">
            </div><div class="form-group col-md-3">
            <label for="numeroContactoIT" class="">Número Contacto *</label>
            <input style="border: none;" type="text" placeholder=" Número Contacto *" id="numeroContactoIT" name="numeroContactoIT" class="form-control x2 text-capitalize border-bottom tal"  value="<?= ($chequeoObj->numeroContacto=='')?'':$chequeoObj->numeroContacto ?>">
          </div>
        </div>
        <?php if ($ObjetoOperacion->idReprogramado!=0): ?>
          <div class="row">
            <h3 for="" class="text-danger text-center col-md-12">Reprogramacion: <?=  $ObjetoOperacion->observacion ?></h3><br>
          </div>
        <?php endif ?>
        <div class="row">
          <div class="form-group col-md-12">
            <label for="observacionT" class="">Observación de operaciones *</label>
            <input style="border: none;" type="text" placeholder="Observación de operaciones *" id="observacionT" name="observacion" class="form-control x2  border-bottom tal" >
          </div>      
      </div>
      
    </ul>
    <ul id="domicilio-panel" class="panel" role="tabpanel" aria-labelledby="domicilio-tab"> 

      <h2>Domicilio Cliente</h2>
       <?php 
       if (!empty($clienteObj->departamento)) {
          $depatamentoObj=$clienteObj->obtenerDepartamentoPorId($clienteObj->departamento); 
          $provinciaObj=$clienteObj->otenerProvinciaPorId($clienteObj->provincia); 
          $distritoObj=$clienteObj->obtenerDistritoPorId($clienteObj->distrito);        
       }           
    ?> 
      <div class="row">
         <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionD" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize dom" required name="departamentoInstalacionD" id="departamentoInstalacionD">
                    <?php if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                        <option class="text-capitalize " value=""  selected  disabled="">Seleccione departamento</option>
                            <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                          <?php if ($objDepartamento): ?>
                            <?php foreach ($objDepartamento as $var): ?>
                              <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                            <?php endforeach ?>
                          <?php else: ?>
                          <?php endif ?>
                      <?php
                    }else{
                      ?>
                         <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
                      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                      <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                      <?php else: ?>
                      <?php endif ?>
                      <?php
                      } ?> 
                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionD" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize dom" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
                    <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
                     <?php
                      } ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionD" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize dom" required name="distritoInstalacionD"  id="distritoInstalacionD" >
                <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
                    <?php
                      } ?>                 
              </select>           
          </div>

        </div>
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="direccionInstalacionD" class="">Direccion*</label>
            <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionD" name="direccionInstalacionD"  class="form-control border-bottom dom" value="<?= (!empty($chequeoObj->direccion))?$chequeoObj->direccion : ''  ?>">
          </div>
          <div class="form-group  col-md-6">
            <label for="referenciaInstalacionD" class="">Referencia *</label>
            <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionD" name="referenciaInstalacionD" class="form-control border-bottom  dom" value="<?= (!empty($chequeoObj->referencia)) ? $chequeoObj->referencia:'' ?>" >
          </div>
        </div>  
        <div class="row">
          <div class="form-group col-md-6">
            <label for="cod" class="">Cordenadas Domicilio *</label>
            <input style="border: none;" type="text" placeholder="Cordenadas Domicilio *" id="cod" name="cordenadasD" class="form-control x2 text-capitalize border-bottom dom" value="<?= (!empty($chequeoObj->coordenadas))? $chequeoObj->coordenadas:'' ?>">
          </div>
          <div class="form-group col-md-6">
            <label for="observacionD" class="">Observaciones *</label>
            <input style="border: none;" type="text" placeholder="Observaciones *" id="observacionD" name="observacion" class="form-control x2 text-capitalize border-bottom dom" >
          </div>
        </div>  
         <div class="form-group col-md-5" id="data_1">
      <div class="row">
        <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control fechaa dom"  id="fechaID" name="fechaID" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10)  ?>"  >
          </div>
        </div>
        <div class="form-group col-md-6">
                      <label  class="">Hora</label>
          <div class="input-group" data-autoclose="true">
              <input type="text" class="form-control horaa dom" id="horaID" name="horaID" autocomplete="off" value="<?= substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>" required>
              <span class="input-group-addon">
                  <span class="fa fa-clock-o"></span>
              </span>
          </div>
        </div>
      </div>
   </div> 
        <div class="row">
          <div class="form-group col-md-6">
            <label for="nombreContactoID" class="">Nombre Contacto *</label>
            <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoID" name="nombreContactoID" class="form-control x2 text-capitalize border-bottom dom"  value="<?= ($chequeoObj->personaContacto=='')?'':$chequeoObj->personaContacto ?>">
          </div><div class="form-group col-md-6">
          <label for="numeroContactoID" class="">Número Contacto *</label>
          <input style="border: none;" type="text" placeholder=" Número Contacto *" id="numeroContactoID" name="numeroContactoID" class="form-control x2 text-capitalize border-bottom dom"  value="<?= ($chequeoObj->numeroContacto=='')?'':$chequeoObj->numeroContacto ?>" >
        </div>
      </div>
    </div> 
  </ul>
  <ul id="concesionario-panel" class="panel" role="tabpanel" aria-labelledby="concesionario-tab">    
    <div class="row">
      <div class="col-md-6"><h2>Concesionario</h2></div>
      <div class="col-md-6">
        <label for="concesionarioInstalacion" class="">Nombre de concesionario*</label>
        <input style="border: none;" type="text" placeholder="Nombre de concesionario*" id="concesionarioInstalacion" name="concesionarioInstalacion" class="form-control border-bottom con" value="<?= (!empty($ObjetoOperacion->nombreConcecionarioInstalicion))?$ObjetoOperacion->nombreConcecionarioInstalicion:''  ?>" >
      </div>
    </div>
    <div class="row">
     <div class="row">
          <div class="form-group  col-md-4">
            <label for="departamentoInstalacionC" class="">Departamento*</label>
             <select  style="border: none;" class="departamentoInstalacionC border-bottom form-control text-capitalize con" required name="departamentoInstalacionC" id="departamentoInstalacionC">
                    <?php if (empty($ObjetoOperacion->departamentoi)) {
                      ?>
                        <option class="text-capitalize" value=""  selected  disabled="">Seleccione departamento</option>
                            <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                          <?php if ($objDepartamento): ?>
                            <?php foreach ($objDepartamento as $var): ?>
                              <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                            <?php endforeach ?>
                          <?php else: ?>
                          <?php endif ?>
                      <?php
                    }else{
                      ?>
                         <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
                      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                      <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                      <?php else: ?>
                      <?php endif ?>
                      <?php
                      } ?>          
                    </select>
          </div>
          <div class="form-group  col-md-4">
            <label for="provinciaInstalacionC" class="">Provincia*</label>
            <select  style="border: none;" class="provinciaInstalacionC border-bottom form-control text-capitalize con" required name="provinciaInstalacionC" id="provinciaInstalacionC" >
                    <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                    <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
                     <?php
                      } ?>                 
            </select>

          </div>
          <div class="form-group  col-md-4">
            <label for="distritoInstalacionC" class="">Distrito*</label>
            <select  style="border: none;" class="distritoInstalacionC border-bottom form-control text-capitalize con" required name="distritoInstalacionC"  id="distritoInstalacionC" >
                <?php if (!empty($ObjetoOperacion->departamentoi)) {
                      ?>
                  <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
                    <?php
                      } ?>                 
              </select>           
          </div>

        </div>
      <div class="row">
        <div class="form-group  col-md-6">
          <label for="direccionInstalacionC" class="">Direccion*</label>
          <input style="border: none;" type="text" placeholder="Direccion*" id="direccionInstalacionC" name="direccionInstalacionC" class="form-control border-bottom con" value="<?= $chequeoObj->direccion ?>" >
        </div>
        <div class="form-group  col-md-6">
          <label for="referenciaInstalacionC" class="">Referencia *</label>
          <input style="border: none;" type="text" placeholder="Referencia *" id="referenciaInstalacionC" name="referenciaInstalacionC" class="form-control border-bottom con"  value="<?= $chequeoObj->referencia ?>" >
        </div>
      </div>    
      <div class="row">
          <div class="form-group col-md-5" id="data_1">
                <div class="row">
                  <div class="form-group col-md-6">   
                      <label  class="">Fecha</label>
                      <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control fechaa con"  id="fechaIC" name="fechaIC" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10)  ?>">
                      </div>
                  </div>
                  <div class="form-group col-md-6">
                                <label  class="">Hora</label>
                    <div class="input-group" data-autoclose="true">
                        <input type="text" class="form-control horaa con" id="horaIC" name="horaIC" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>">
                        <span class="input-group-addon">
                            <span class="fa fa-clock-o"></span>
                        </span>
                    </div>
                  </div>
                </div>
         </div> 
      </div>
      <div class="row">
        <div class="form-group col-md-6">
          <label for="nombreContactoIC" class="">Nombre Contacto *</label>
          <input style="border: none;" type="text" placeholder="Nombre Contacto *" id="nombreContactoIC con" name="nombreContactoIC" class="form-control x2 text-capitalize border-bottom" value="<?= ($chequeoObj->personaContacto=='')?'':$chequeoObj->personaContacto ?>" >
        </div>
        <div class="form-group col-md-6">
        <label for="numeroContactoIC" class="">Número Contacto *</label>
        <input style="border: none;" type="text" placeholder=" Número Contacto *" id="numeroContactoIC" name="numeroContactoIC" class="form-control x2 text-capitalize border-bottom con"  value="<?= ($chequeoObj->numeroContacto=='')?'':$chequeoObj->numeroContacto ?>">
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-12">
        <label for="observacionC" class="">Observaciones *</label>
        <input style="border: none;" type="text" placeholder="Observaciones *" id="observacionC" name="observacion" class="form-control x2 text-capitalize border-bottom con" >
      </div>
    </div>
  </div>
</ul>
</div>
</div> 

  <div id="programacionHtml"></div>

    <?php
    } ?>

  
<?php //en los dos tipos de chequeo que queda en SAC confirman orden orte fecha y lugar  ?>
</span>
<div id="b" class="b">
<div class="row">  
  <div class="col-md-8 form-group">
    <input required  style="border: none;" type="text" placeholder="Motivo Reprogramacion *" id="motivoReprogramacionChequeo" name="motivoReprogramacionChequeo" class="border-bottom form-control"></div>
    <div class="btn btn-warning col-md-4" onclick="cancelar()">No reprogramar</div>
  </div>
</div>
<?php } ?>
<script src="../../assets/build/jquery.datetimepicker.full.js"></script>

<script src="../../assets/bootbox/bootbox.all.js"></script>
<script>
$(".departamentoInstalacionD").click(function(){provincias4();});
$(".departamentoInstalacionC").click(function(){provincias5();});
$(".provinciaInstalacionD").click(function(){distritos4();});
$(".provinciaInstalacionC").click(function(){distritos5();});
function provincias4(){
  var iddepartamento = $(".departamentoInstalacionD").val();
  $.ajax({        
    type : 'POST',
    url  : '../com/ajax/provincia',
    data :  'iddepartamento='+ iddepartamento,
    success : function(data)
    {
      $(".provinciaInstalacionD").html(data);
    }
  });
  return false;
}
function provincias5(){
  var iddepartamento = $(".departamentoInstalacionC").val();
  $.ajax({        
    type : 'POST',
    url  : '../com/ajax/provincia',
    data :  'iddepartamento='+ iddepartamento,
    success : function(data)
    {
      $(".provinciaInstalacionC").html(data);
    }
  });
  return false;
}
  function distritos4(){
      var idprovincia = $(".provinciaInstalacionD").val();
      $.ajax({        
        type : 'POST',
        url  : '../com/ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionD").html(data)
        }
      });
      return false;
    }
    function distritos5(){
      var idprovincia = $(".provinciaInstalacionC").val();
      $.ajax({        
        type : 'POST',
        url  : '../com/ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionC").html(data)
        }
      });
      return false;
    }   
$( "input[name='lugarInstacion']" ).on( "click",function(){
        $('#programacionHtml').html('');
        $('.horaVer').val('');
        $('.fecha').val('');
        validar();
});		
        validar();
 $(".fecha").change(function(){ 
    $('#programacionHtml').html('');
    $('.horaVer').val('');
    $(".horabtn").click();
});		
 $(".horabtn").click(function(){ 
	var lugarI=$( "input[name='lugarInstacion']:checked" ).val();
	if (lugarI=='Taller') {
      var fecha = $('#fechaIT').val(); 
      var cita = 'si';
	}else if (lugarI=='Domicilio Cliente') {
      var fecha = $('#fechaID').val(); 
      var cita = 'dom';
	}else if (lugarI=='Concesionario') {
      var fecha = $('#fechaIC').val(); 
      var cita = 'dom';
    }
      if (fecha!='') {
        $.ajax({
          type : 'POST',
          url  : '../includes/programacionAjaxNew',
          data : {fecha:fecha,cita:cita},
          success : function(data)
          {
           $("#programacionHtml").html(data);
          }
        });
      }else{
        $("#programacionHtml").html('<h3 class="text-center text-info">Primero selecciona fecha</h3>');
      }
      return false;
    });
<?php $fechaA=date('Y-m-d') ?>
  jQuery.datetimepicker.setLocale('es');
  $('#fechaIT').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y-m-d',        
    scrollMonth : false,
    scrollInput: false,
    timepicker:false
    // allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });

  $('#fechaIC').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y-m-d',
    scrollInput: false,
    timepicker:false,
    scrollMonth : false
  });   
   $('#fechaID').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y-m-d',
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
  });
  ///////////////////////////////////////////////////


function validar(){
  var lugarI=$( "input[name='lugarInstacion']:checked" ).val()
  if (lugarI=='Taller') {
      $('.con').prop('disabled', true);
      $('.dom').prop('disabled', true);
      $('.tal').prop('disabled', false);
  }else if (lugarI=='Domicilio Cliente') {
    $('.con').prop('disabled', true);
      $('.dom').prop('disabled', false);
      $('.tal').prop('disabled', true);     
  }else if (lugarI=='Concesionario') {
    $('.con').prop('disabled', false);
      $('.dom').prop('disabled', true);
      $('.tal').prop('disabled', true);
  }
}
 $(document).ready(function(){  
  $( "input[name='lugarInstacion']" ).on( "click",validar);   
  validar();
});


</script>
<script>
  $(".b").hide();
  $('#motivoReprogramacionChequeo').prop('disabled', true);
  function reprogramarChequeo(){
      $('.ocultarThis').hide();
     $('.b').show(); 
<?php if ($vehiculoObj): ?>
     $('#orte').prop('disabled', true);
     $('#ordenContrato').prop('disabled', true);
<?php else: ?>  
     $('#numeroOrden').prop('disabled', true);
     $('#numeroOrte').prop('disabled', true);
     $('#tecnico').prop('disabled', true);

<?php endif ?>   

     $('#motivoReprogramacionChequeo').prop('disabled', false);
  }
  function cancelar(){
      $('.b').hide();
     $('.ocultarThis').show(); 
<?php if ($vehiculoObj): ?>
     $('#orte').prop('disabled', false);
     $('#ordenContrato').prop('disabled', false);
<?php else: ?>
     $('#numeroOrden').prop('disabled', false);
     $('#numeroOrte').prop('disabled', false);
     $('#tecnico').prop('disabled', false);
<?php endif ?>   
     $('#motivoReprogramacionChequeo').prop('disabled', true);
  }

<?php $fechaA=date('Y-m-d') ?>
  $('#datetimepicker3').datetimepicker({
    minDate:'<?= $fechaA ?>',
    inline:true,
    scrollInput: false,
    scrollMonth : false,
    allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']

  });
  $('#datetimepicker3').datetimepicker({value:'<?php echo $ObjetoOperacion->fechaInstalacion ?>'});
   $('#fechaCh').datetimepicker({
    minDate:'<?= $fechaA ?>',
    format:'Y-m-d',
    scrollInput: false,
    scrollMonth : false,
    timepicker:false
  });
  $('#horaCh').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']
  });

function completeData(loc,order , orte){
    //console.log(loc+" order:"+order+" orte:"+orte)
    $("#orte").val(orte)
    $("#ordenContrato").val(loc+"-"+order)
}

</script>

<?php
} 
?> 

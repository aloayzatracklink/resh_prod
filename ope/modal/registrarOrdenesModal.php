<?php 
if ($_POST['rowid']) {
include_once("../../includes/init.php");
$idOperacion=htmlspecialchars(htmlentities($_POST['rowid']));
$ObjetoOperacion=Operacion::obtenerOperacionPorId($idOperacion);
Operacion::asignarCagoOperaciones($_POST['rowid']);
// $datosFacturacion=DatosFacturacion::obtenerDatosFacturacionPorId($ObjetoOperacion->);
$vehiculoObj=Vehiculo::obtenerVehiculo($ObjetoOperacion->idvehiculo);
$vehiculoObj=Vehiculo::obtenerVehiculo($ObjetoOperacion->idvehiculo);
$clienteObj=Cliente::obtenerClientePorVehiculo($ObjetoOperacion->idvehiculo,$ObjetoOperacion->idcliente);  
$clObj=new Cliente;
$varTI=TramaIntegracion::obtenerTramaIntegracionbyOperacion($idOperacion);


?>
<input type="hidden" name="idOperacion" value="<?= $idOperacion ?>">
<input type="hidden" name="idFacturacion" value="<?=  $ObjetoOperacion->datosfacturacionid ?>">
<input type="hidden" name="idvehiculo" value="<?=  $ObjetoOperacion->idvehiculo ?>">
<input type="hidden" name="trabajo" value="<?=  $ObjetoOperacion->trabajo ?>">

<link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
 <h2 class="text-center"><?php echo $ObjetoOperacion->trabajo ?></h2>
 <?php if (!empty($ObjetoOperacion->datosfacturacionid)): ?>    
  <?php 
    $objFacturacion=DatosFacturacion::obtenerDatosFacturacionPorId($ObjetoOperacion->datosfacturacionid);
   ?>
  <?php if (empty($objFacturacion->voucher)): ?> 
<?php if ($objFacturacion->tipoDocumentoFacturacion!='Boleta'|| $objFacturacion->tipoDocumentoFacturacion!='Factura'): ?>
  <h2 class="text-center"><?= $objFacturacion->tipoDocumentoFacturacion ?></h2>
  <?php else: ?>
  <h3 class="text-center text-danger">Atención pago en caja</h3>
<?php endif ?>
  <input type="hidden" name="pagoEnCaja" value="ok">
  <?php else: ?>
    <div class="row">
    <img class="text-center col-md-6 col-md-offset-3" width="400" src="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" alt="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" >      
    </div>
  <div class="row">
    <a  class="btn btn-primary text-center  col-md-6 col-md-offset-3" href="../../public/vouchers/<?php echo $objFacturacion->voucher ?>" target="_blank" >Descargar/Ver</a>    
  </div>
  <?php endif ?> 
  <?php endif ?>
<div class="row">                      
        <div class="form-group col-md-2">
              <label for="nombreCompleto" class=""><?= ($clienteObj->tipoCliente=="Natural") ? "DNI: " : "RUC: " ; ?> </label>
              <input style="border: none;" type="text" class="form-control border-bottom text-capitalize"  required 
              readonly
              value="<?= ($clienteObj->tipoCliente=="Natural") ? $clienteObj->dni : $clienteObj->ruc ; ?>">
            </div>
            <div class="form-group col-md-5">
              <label for="nombreCompleto" class=""><?= ($clienteObj->tipoCliente=="Natural") ? "Nombres: " : "Razon social: " ; ?> </label>
              <input style="border: none;" type="text" class="form-control border-bottom text-capitalize"  required 
              readonly
              value="<?= ($clienteObj->tipoCliente=="Natural") ? $clienteObj->nombre." ".$clienteObj->apellidosC : $clienteObj->razonSocial ; ?>">
            </div>

        <div class="form-group col-md-5">
              <label for="nombreCompleto" class="">Placa | Chasis</label>
              <input style="border: none;" type="text"    class="form-control border-bottom text-capitalize"  required 
                readonly
              value="<?= $vehiculoObj->placa." | ".$vehiculoObj->chasis ?>">
            </div>
          
       	</div>
        <div class="row">                      
            <div class="form-group col-md-6">
              <label for="nombreCompleto" class="">Marca | Modelo | Color</label>
              <input style="border: none;" type="text" class="form-control border-bottom text-capitalize"  required 
              readonly
              value="<?= $vehiculoObj->marca." | ".$vehiculoObj->modelo." | ".$vehiculoObj->color ?>">
            </div>

        <div class="form-group col-md-6">
              <label for="nombreCompleto" class="">Anio | Motor</label>
              <input style="border: none;" type="text"    class="form-control border-bottom text-capitalize"  required 
                readonly
              value="<?= $vehiculoObj->anio." | ".$vehiculoObj->motor ?>">
            </div>
          
       	</div>

        <hr>
        
        <div class="row">                      
            <div class="form-group col-md-6">
              <label for="nombreCompleto" class="">Seguro</label>
              <input style="border: none;" type="text" class="form-control border-bottom text-capitalize"  required 
              readonly
              value="<?= $vehiculoObj->companiaSeguro ?>">
            </div>

        <div class="form-group col-md-6 text-warning">
              <label for="nombreCompleto" class="">Campaña</label>
              <input style="border: none;" type="text"    class="form-control border-bottom text-capitalize" 
                readonly
              value="<?= $vehiculoObj->campana ?>">
            </div>
          
       	</div>

        <div class="row">                      
            <div class="form-group col-md-12 text-warning">
              <label for="nombreCompleto" class="">OBSERVACION</label>
                <input type="text" style="display: none" name="old_observacion" id="old_observacion" value="<?= $ObjetoOperacion->observacion ?>">
              <input style="border: none;" type="text" class="form-control border-bottom text-capitalize"  
              readonly
              value="<?= $ObjetoOperacion->observacion ?>">
            </div>
       	</div>

        <div class="row">                      
            <div class="form-group col-md-6">
              <label for="nombreCompleto" class="">Trabajo</label>
              <input style="border: none;" type="text"    class="form-control border-bottom text-capitalize"  required 
                readonly
              value="<?= $ObjetoOperacion->trabajo ?>">
            </div>

            <div class="form-group col-md-6">
              <label for="nombreCompleto" class="">Fecha</label>
              <input style="border: none;" type="text" class="form-control border-bottom text-capitalize"  
              readonly
              value="<?= empty($ObjetoOperacion->fechaInstalacion)?'Pendiente':$ObjetoOperacion->fechaInstalacion  ?>">
            </div>
       	</div>
        <div class="row">                      
            <div class="form-group col-md-6">
              <label for="nombreCompleto" class="">Lugar</label>
              <input style="border: none;" type="text"    class="form-control border-bottom text-capitalize"  required 
                readonly
              value="<?= $ObjetoOperacion->lugarInstalacion ?>">
            </div>

            <div class="form-group col-md-6">
              <label for="nombreCompleto" class="">Departamento</label>
              <input style="border: none;" type="text" class="form-control border-bottom text-capitalize"  
              readonly
              value="<?php if (!empty($ObjetoOperacion->departamentoi)){ ?><?php $datosD=$clObj->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?> <?= $datosD->departamento ?><?php }else{ ?>Pendiente<?php } ?>">
            </div>
       	</div>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="corigen" class="">Concesionario Origen</label>
                <input style="border: none;" type="text" placeholder="Concesionario Origen" id="corigen" name="corigen" class="border-bottom form-control text-uppercase corigen " autocomplete="off" readonly value="<?= $objFacturacion->canalOrigen ?>" >
            </div>
        </div>
    <hr>
    <div class="row">
    <div class="form-group col-md-6">
              <label for="nombreCompleto" class="">Vendedor | Fecha</label>
              <input style="border: none;" type="text"    class="form-control border-bottom text-capitalize"  required 
                readonly
              value="<?php $usuario=Usuario::obtenerUsuario($objFacturacion->idUsuarioCreado);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno; ?> | <?= formato($ObjetoOperacion->fechaCreacion)  ?>">
            </div>

            <div class="form-group col-md-2">
              <label for="nombreCompleto" class="">Plan</label>
              <input style="border: none;" type="text" class="form-control border-bottom text-capitalize"  
              readonly
              value="<?= $vehiculoObj->plan  ?>">
            </div>
      <?php if ($objFacturacion): ?>
        <div class="form-group col-md-2">
              <label for="nombreCompleto" class="">Precio</label>
              <input style="border: none;" type="text" class="form-control border-bottom text-capitalize"  
              readonly
              value="<?= $objFacturacion->costo  ?>">
        </div>
        <div class="form-group col-md-2">
              <label for="nombreCompleto" class="">Tiempo</label>
              <input style="border: none;" type="text" class="form-control border-bottom text-capitalize"  
              readonly
              value="<?=  $vehiculoObj->planTiempo ?>">
            </div>
      <?php endif ?>
       	</div>

    <div class="well" style="padding-top: 11px;
    padding-bottom: 10px;">
<div class="row">
    <div class="form-group col-md-12 text-primary text-capitalize">
        <?php if ($objFacturacion): ?>
            <strong class="font-bold">
                Forma de Pago: <?=  $objFacturacion->formaPago ?>
            </strong>
        <?php endif ?>
    </div>
    <hr>
    <div class="form-group col-md-12 ">
        <p style="font-weight:bold" class="text-danger">Servicios Adicionales Seleccionados :</p>
        <?php ServiciosAdicionales::obtenerServAdicionalSeleccionadoVer($idOperacion) ?>
    </div>
    <div class="form-group col-md-12">
        <div class="col-md-4">
            <label for="fechIni" class="text-success">Fecha Inicio Vigencia :</label>
            <input style="border: none;" type="text" readonly placeholder="Fecha Inicio" id="fechIni" name="fechIni" class="form-control border-bottom text-capitalize" required value="<?php  if($varTI){echo $varTI->fecha_inicio;} ?>">
        </div>
        <div class="col-md-4">
            <label for="fechFin" class="text-success">Fecha Fin Vigencia:</label>
            <input style="border: none;" type="text" readonly placeholder="Fecha Fin" id="fechFin" name="fechFin" class="form-control border-bottom text-capitalize" required value="<?php if($varTI){echo $varTI->fecha_fin;} ?>">
        </div>
        <div class="col-md-4">
            <label for="Poliza" class="text-success"># Póliza :</label>
            <input style="border: none;" type="text" readonly placeholder="" id="Poliza" name="Poliza" class="form-control border-bottom text-capitalize" required value="<?php if($varTI){echo $varTI->policy;} ?>">
        </div>
    </div>
</div>

<?php if (!empty($vehiculoObj->serviciosAdicionales)): ?>
 <div class="row">
   <div class="form-group col-md-12">
     Servicios adicionales: <?php echo $vehiculoObj->serviciosAdicionales ?>
   </div>
 </div>
<?php endif ?>
</div> 
<?php if ($objFacturacion->tipoDocumentoFacturacion=='Banbif'): ?>
<div class="row">
  <div class="col-md-6">
    <label for="orte" class="sr-only">Numero de Orden servicio</label>
    <input style="border: none;" type="text" placeholder="Numero de Orden servicio" id="orden" name="orden" class="border-bottom form-control x2 " required >
  </div>
  <div class="col-md-6">
    <label for="orden2s" class="sr-only">Numero de Orden Equipo </label>
    <input style="border: none;" type="text" placeholder="Numero de Orden Equipo" id="orden2s" name="orden2" class="border-bottom form-control x2 " required >
  </div>
</div>
<?php else: ?>
<div class="row">
  <div class="col-md-6">
    <label for="orte" class="sr-only">Numero de Orden</label>
    <input style="border: none;" type="text" placeholder="Numero de Orden" id="orden" name="orden" class="border-bottom form-control x2 " required >
  </div>
</div>
<?php endif ?>
  <?php
} 
?> 

<?php 
if ($_POST['rowid'] && $_POST['rowid']<>'REPROGRAMAR') {
include_once("../../includes/init.php");
$idcita=htmlspecialchars(htmlentities($_POST['rowid']));
$objCitaDta=Cita::obtenerCitaPorId($idcita);
Cita::asignarCitaSac($_POST['rowid']);
?>
<input type="hidden" name="idcita" id="idcita" value="<?= $idcita ?>">
<link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>  
    <div class="well" style="padding-top: 11px;
    padding-bottom: 10px;">
    
   <div class="row">
    <div class="form-group col-md-9 text-primary text-capitalize">
      Cliente: <strong><?= $objCitaDta->nombreCliente; ?></strong>
      <input type="hidden" name="clienteCita" value="<?= $objCitaDta->nombreCliente; ?>">
    </div>
    <div class="form-group col-md-3">
     placa: <strong><?= $objCitaDta->placa ?></strong>
   </div>

 </div>
        <div class="row">
            <div class="form-group col-md-3">
        
             <label for="plan" class="">Plan *</label>
             <select  style="border: none;" class="plan border-bottom form-control" required name="plan" id="plan">
              <option class="text-capitalize" value="<?php echo $objCitaDta->plan ?>" selected ><?php echo $objCitaDta->plan ?></option>
              <?php Cliente::obtenerPlan() ?>      
            </select> 
           </div>
            <div class="form-group col-md-3">
              <label for="tiempo" class="">Tiempo *</label>
              <input style="border: none;" type="number" placeholder="Tiempo *" id="tiempo" name="tiempo" class="form-control border-bottom text-capitalize" required  autocomplete="off" value="<?= $objCitaDta->tiempo ?>">
            </div>
            <div class="form-group col-md-3">
              <label for="costo" class="">Costo *</label>
              <input style="border: none;" type="text" placeholder="Costo *" id="costo" name="precioLista" class="form-control border-bottom text-capitalize" required value="<?= $objCitaDta->costo ?>" >
            </div>
                  
             <div class="form-group col-md-3">
              <label for="costo" class="">seguro *</label>
              <input style="border: none;" type="text" placeholder="seguro *" id="seguro" name="seguro" class="form-control border-bottom text-capitalize" required value="<?= $objCitaDta->seguro ?>" >
            </div>         
          </div> 
 <div class="row">
 
  <div class="form-group col-md-12">
   Fecha programada: <?= formatoHaceEnSinFecha($objCitaDta->fecha) ?>
 </div>
</div>
<div class="row">
    <div class="form-group col-md-6 text-primary text-capitalize">
  <strong class="font-bold">
      <?php $usuario=Usuario::obtenerUsuario($objCitaDta->idcreador);  echo $usuario->usuarioNombre;?>&nbsp;<?= $usuario->usuarioApellidoPaterno; ?> <br><small style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px"><?= haceMinimo($objCitaDta->fechaRegistro)  ?></small>
    </strong>
    <input type="hidden" name="ejecutivoComercial" value="<?= $objCitaDta->idcreador ?>">    
  </div>  
    <div class="form-group col-md-6 text-primary text-capitalize">
    <strong class="font-bold">
      Venta con comisión: <?= !empty($objCitaDta->comsiona)?$objCitaDta->comsiona:'No'  ?>
    </strong>
    <input type="hidden" name="comsiona" value="<?= $objCitaDta->comsiona ?>">
  </div>   
</div>
<div class="row">

    
    <div class="form-group col-md-12 text-primary text-capitalize">
    <strong class="font-bold">
      Observaciones: <strong class="text-danger"><?= (!empty($objCitaDta->observacion))?$objCitaDta->observacion:''  ?></strong>
    <input type="hidden" name="obss" value="<?= $objCitaDta->observacion ?>">
    </strong>
  </div>  
</div>
    <input type="hidden" name="comsiona" value="<?= $objCitaDta->comsiona ?>">
    <input type="hidden" name="comision" value="<?= $objCitaDta->comision ?>">
    <input type="hidden" name="idcanal" value="<?= $objCitaDta->idcanal ?>">
    <input type="hidden" name="idOpe" value="<?= $objCitaDta->idOpe ?>">
    <input type="hidden" name="fechaOpe" value="<?= $objCitaDta->fechaOpe ?>">
    <?php 
      $canalDatos=Canales::obtenerDatosCanalPorId($objCitaDta->idcanal);
if ($canalDatos) {
   ?>
       
<?php
      if ($canalDatos->tipoCanal=='CONVENIO') {
        ?>
    <input type="hidden" name="idconvenio" value="<?= $canalDatos->nombreCanal ?>">
        <?php
      }
     ?>
   <?php 
}
if ($objCitaDta->tipoCanal=='CONVENIO') {
    ?>
    <input type="hidden" name="idconvenio" value="<?= $objCitaDta->nombreCanal ?>">
    <?php
    
}
?>
<input type="hidden" name="estadoPago" value="<?= $objCitaDta->estadoPago ?>">
<input type="hidden" name="oficina" value="<?= $objCitaDta->oficina ?>">
<input type="hidden" name="nombreCanal" value="<?= $objCitaDta->nombreCanal ?>">
<input type="hidden" name="ejecutivoCanal" value="<?= $objCitaDta->ejecutivoCanal ?>">
<input type="hidden" name="tipoCanal" value="<?= $objCitaDta->tipoCanal ?>">
</div> 
<?php if ($objCitaDta->estadoPago=='Pagado'): ?>
  
   <div class="row">
      <img class="text-center col-md-6 col-md-offset-3" width="500" src="../../public/vouchers/<?=  $objCitaDta->voucher ?>" alt="../../public/vouchers/<?php echo $objCitaDta->voucher ?>" >      
        <input type="hidden" name="voucher"  id="vhoimgvou" value="<?= $objCitaDta->voucher ?>" />
<a href='../../public/vouchers/<?=  $objCitaDta->voucher ?>' target='blank'> ver </a>

  </div> 
<?php else: ?>
<h2 class="text-center">Estado de pago: <?= $objCitaDta->estadoPago ?></h2>
<?php endif ?>
<hr>
<div class="row">
  <div class="col-md-12">
    <label for="orden" class="">Numero de Orden de facturación</label>
    <input style="border: none;" type="text" placeholder="Numero de Orden de facturación" id="orden" name="orden" class="border-bottom form-control x2 " required  autocomplete="off">
  </div>                         
 </div>
<hr>
<div class="row">
  <div class="col-md-6">
    <label for="motivo" class="motivo">Motivo</label>
    <input style="border: none;" type="text" placeholder="Motivo" id="motivo" name="motivo" class="border-bottom form-control x2 motivo" required >
  </div>
  <div class="col-md-6">
    <label for="orte" class="reprogramar">Reprogramar</label>
  <button type="submit" name="reprogramar" class="btn btn-success form-control reprogramar" id="reprogramar" onclick="reprogramarf()">Reprogramar</button>                          
  </div>
</div>
<span id="resultadoOrden"></span>
<script src="../../assets/js/jquery-2.1.1.js"></script>
<script>
    $("#orden").bind("keyup focus",function()
    { 
     var orden = $("#orden").val();
     var idcita = $("#idcita").val();
     var n=orden.length;
     if (n>3) {
       $( ".motivo" ).hide();
       $( ".reprogramar" ).hide();
       $.ajax({
          type : 'POST',
          url  : 'ajax/datosOrden',
          data : {orden:orden,idcita:idcita},
          success : function(data)
          {
           $("#resultadoOrden").html(data);
          }
        }); 
     }else{
        $( ".motivo" ).show();
        $( ".reprogramar" ).show();
        $("#resultadoOrden").html("");
     }        
  });    
function reprogramarf(){
  document.getElementById("orden").removeAttribute("required");
} 
</script>

	<?php
}else if ($_POST['rowid']=='REPROGRAMAR'){
include_once("../../includes/init.php");
$citaObj=new Cita;
    ?>
<h2 class="text-center text-info">Citas que no llegaron a su hora</h2>
<table class="table table-hover table-striped footable foot1">
<thead>
    <tr>
        <th>Nombre cliente</th>
        <th>Placa</th>
        <th>Ejecutivo</th>
        <th>Fecha programada</th>
        <th>Acción</th>
    </tr>
</thead>
    <tbody>
<?php
if ($data=$citaObj->obtnerCitasEnEstadoReprogramar()) {
    foreach ($data as $var):
    $o=Usuario::obtenerUsuario($var->idcreador)
    ?>
        <tr>
            <td><?= $var->nombreCliente ?></td>
            <td><?= $var->placa ?></td>
            <td><?= $o->usuarioNombre." ".$o->usuarioApellidoPaterno ?></td>
            <td><?= $var->fecha ?></td>
            <td><button class="btn btn-primary" name="activar" value="<?= $var->idcita ?>">Activar</button></td>
        </tr>
    <?php
    endforeach;
}else{
    ?>
<tr>
    <td colspan="3">No hay citas en estado reprogramar recientes</td>
</tr>
        
    <?php
}
?>
    </tbody>
</table>
    <?php

}
?> 

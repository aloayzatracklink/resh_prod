<?php
include_once("../../includes/head.php");
$idtec=soloNumero($_POST['rowid']);
$tipoOpera=$_POST['tipoOpera'];
$Objtec=Tecnicos::obtenerTecnicos($idtec);
$clienteObjModal=new Cliente;
if($tipoOpera=='edit') {
    ?>
    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <div class="well" style="padding-top: 11px;padding-bottom: 10px;">
        <div class="row">
            <div class="form-group col-md-4 text-primary text-capitalize">
                <label for="nmtecnico">Técnico</label>
                <input style="border: none;" type="text" placeholder="Nombre de Tecnico" id="nmtecnico"
                       name="nmtecnico" class="border-bottom form-control" required value="<?= $Objtec->tecnico ?>">
                <input type="hidden" name="idtecnico" value="<?= $Objtec->idtec ?>">
                <input type="hidden" name="tipoOpera" value="<?= $tipoOpera ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="mnmtecnico">Mini Descripcion</label>
                <input style="border: none;" type="text" placeholder="Mini descripcion de Tecnico" id="mnmtecnico"
                       name="mnmtecnico" class="border-bottom form-control" required
                       value="<?= $Objtec->min_desc ?>">
            </div>
            <div class="col-md-3">
                <label for="estado">Estado</label>
                <select style="border: none;" class="tecnico border-bottom form-control" required name="estado">
                    <option class="" value="<?php echo $Objtec->estado ?>"
                            selected><?php echo obtenerEstado($Objtec->estado) ?></option>
                    <option value="37">Disponible</option>
                    <option value="92">No disponible</option>
                </select>
            </div>
            <div class="col-md-1">
                <label for=""></label>
                <div class="checkbox checkbox-success <?= $Objtec->fijo == 1 ? 'checked' : '' ?>">
                    <input id="fijo" type="checkbox" name="fijo" <?= $Objtec->fijo == 1 ? 'checked' : '' ?>
                           value="1">
                    <label for="fijo">
                        <strong>Fijo</strong>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group  col-md-4">
                <label for="iddepartamento" class="">Departamento*</label>
                <select style="border: none;" class="border-bottom form-control text-capitalize iddepartamento" required name="iddepartamento" id="iddepartamento">
                    <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
                    <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                            <option value="<?php echo $var->iddepartamento ?>" <?= $var->iddepartamento == $Objtec->iddepartamento ? 'selected' : '' ?>><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                    <?php else: ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="form-group  col-md-4">
                <label for="idprovincia" class="">Provincia*</label>
                <select style="border: none;" class=" border-bottom form-control text-capitalize idprovincia" required
                        name="idprovincia" id="idprovincia">
                    <?php $objProvincia = Cliente::obtenerProvincias($Objtec->iddepartamento) ?>
                    <?php if ($objProvincia): ?>
                        <?php foreach ($objProvincia as $var): ?>
                            <option value="<?php echo $var->idprovincia ?>" <?= $var->idprovincia == $Objtec->idprovincia ? 'selected' : '' ?> ><?php echo $var->provincia ?></option>
                        <?php endforeach ?>
                    <?php else: ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="form-group  col-md-4">
                <label for="iddistrito" class="">Distrito*</label>
                <select style="border: none;" class=" border-bottom form-control text-capitalize iddistrito" required
                        name="iddistrito" id="iddistrito">
                    <?php $objDistrito = Cliente::obtenerDistritos($Objtec->idprovincia) ?>
                    <?php if ($objDistrito): ?>
                        <?php foreach ($objDistrito as $var): ?>
                            <option value="<?php echo $var->iddistrito ?>" <?= $var->iddistrito == $Objtec->iddistrito ? 'selected' : '' ?>><?php echo $var->distrito ?></option>
                        <?php endforeach ?>
                    <?php else: ?>
                    <?php endif ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="mnmtecnico">Hora Inicio</label>
                <div class="input-group clockpicker" data-autoclose="true">
                    <input type="text" class="form-control" readonly name="horaini" maxlength="5"
                           value="<?= $Objtec->hora_inicio ?>">
                    <span class="input-group-addon">
               <span class="fa fa-clock-o"></span>
            </span>
                </div>
            </div>

            <div class="col-md-4">
                <label for="mnmtecnico">Hora Fin</label>
                <div class="input-group clockpicker" data-autoclose="true">
                    <input type="text" class="form-control" readonly name="horafin" maxlength="5"
                           value="<?= $Objtec->hora_fin ?>">
                    <span class="input-group-addon">
               <span class="fa fa-clock-o"></span>
            </span>
                </div>
            </div>
            <div class="col-md-4">
                <label for="nubicacion">Ubicación</label>
                <select style="border: none;" class="tecnico border-bottom form-control" name="nubicacion"
                        id="nubicacion">
                    <?php $objUbica = Tecnicos::obtenerUbicacion() ?>
                    <?php if ($objUbica): ?>
                        <?php foreach ($objUbica as $var): ?>
                            <option value="<?php echo $var->codigo ?>" <?= $var->codigo == $Objtec->ubicacion ? 'selected' : '' ?>><?php echo $var->ubicacion ?></option>
                        <?php endforeach ?>
                    <?php else: ?>
                    <?php endif ?>
                </select>
            </div>
        </div>
    </div>

    <style>
        .popover {
            z-index: 999999;
        }
    </style>
    <script>
        $(".iddepartamento").change(function () {
            provincias();
        });
        $(".idprovincia").change(function () {
            distritos();
        });

        function provincias() {
            var iddepartame = $(".iddepartamento").val();
            $.ajax({
                type: 'POST',
                url: '../../com/ajax/provincia',
                data: 'iddepartamento=' + iddepartame,
                success: function (data) {
                    $(".idprovincia").html(data);
                }
            });
            return false;
        }

        function distritos() {
            var idprovincia = $(".idprovincia").val();
            $.ajax({
                type: 'POST',
                url: '../../com/ajax/distrito',
                data: 'idprovincia=' + idprovincia,
                success: function (data) {
                    $(".iddistrito").html(data)
                }
            });
            return false;
        }


        $(document).ready(function () {
            $('.clockpicker').clockpicker();
        });
    </script>

    <?php
}
elseif($tipoOpera=='save') {
    ?>

    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <div class="well" style="padding-top: 11px;padding-bottom: 10px;">
        <div class="row">
            <div class="form-group col-md-4 text-primary text-capitalize">
                <label for="nmtecnico">Técnico</label>
                <input style="border: none;" type="text" placeholder="Nombre de Tecnico" id="nmtecnico"
                       name="nmtecnico" class="border-bottom form-control" required value="">
                <input type="hidden" name="idtecnico" value="">
                <input type="hidden" name="tipoOpera" value="">
            </div>
            <div class="form-group col-md-4">
                <label for="mnmtecnico">Mini Descripcion</label>
                <input style="border: none;" type="text" placeholder="Mini descripcion de Tecnico" id="mnmtecnico"
                       name="mnmtecnico" class="border-bottom form-control" required
                       value="">
            </div>
            <div class="col-md-3">
                <label for="estado">Estado</label>
                <select style="border: none;" class="tecnico border-bottom form-control" required name="estado">
                    <option value="37">Disponible</option>
                    <option value="92">No disponible</option>
                </select>
            </div>
            <div class="col-md-1">
                <label for=""></label>
                <div class="checkbox checkbox-success ">
                    <input id="fijo" type="checkbox" name="fijo"
                           value="1">
                    <label for="fijo">
                        <strong>Fijo</strong>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group  col-md-4">
                <label for="iddepartamento" class="">Departamento*</label>
                <select style="border: none;" class="border-bottom form-control text-capitalize iddepartamento" required
                        name="iddepartamento" id="iddepartamento">
                    <?php $objDepartamento = Cliente::obtenerDepartamentos() ?>
                    <?php if ($objDepartamento): ?>
                        <?php foreach ($objDepartamento as $var): ?>
                            <option value="<?php echo $var->iddepartamento ?>" ><?php echo $var->departamento ?></option>
                        <?php endforeach ?>
                    <?php else: ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="form-group  col-md-4">
                <label for="idprovincia" class="">Provincia*</label>
                <select style="border: none;" class=" border-bottom form-control text-capitalize idprovincia" required
                        name="idprovincia" id="idprovincia">
                </select>
            </div>
            <div class="form-group  col-md-4">
                <label for="iddistrito" class="">Distrito*</label>
                <select style="border: none;" class=" border-bottom form-control text-capitalize iddistrito" required
                        name="iddistrito" id="iddistrito">
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="mnmtecnico">Hora Inicio</label>
                <div class="input-group clockpicker" data-autoclose="true">
                    <input type="text" class="form-control" readonly name="horaini" maxlength="5"
                           value="" required>
                    <span class="input-group-addon">
               <span class="fa fa-clock-o"></span>
            </span>
                </div>
            </div>

            <div class="col-md-4">
                <label for="mnmtecnico">Hora Fin</label>
                <div class="input-group clockpicker" data-autoclose="true">
                    <input type="text" class="form-control" readonly name="horafin" maxlength="5"
                           value="" required>
                    <span class="input-group-addon">
               <span class="fa fa-clock-o"></span>
            </span>
                </div>
            </div>
            <div class="col-md-4">
                <label for="nubicacion">Ubicación</label>
                <select style="border: none;" class="tecnico border-bottom form-control" name="nubicacion"
                        id="nubicacion">
                    <?php $objUbica = Tecnicos::obtenerUbicacion() ?>
                    <?php if ($objUbica): ?>
                        <?php foreach ($objUbica as $var): ?>
                            <option value="<?=$var->codigo ?>" ><?= $var->ubicacion ?></option>
                        <?php endforeach ?>
                    <?php else: ?>
                    <?php endif ?>
                </select>
            </div>
        </div>
    </div>

    <style>
        .popover {
            z-index: 999999;
        }
    </style>


    <script>
        $(".iddepartamento").change(function () {
            provincias();
        });
        $(".idprovincia").change(function () {
            distritos();
        });

        function provincias() {
            var iddepartame = $(".iddepartamento").val();
            $.ajax({
                type: 'POST',
                url: '../../com/ajax/provincia',
                data: 'iddepartamento=' + iddepartame,
                success: function (data) {
                    $(".idprovincia").html(data);
                }
            });
            return false;
        }

        function distritos() {
            var idprovincia = $(".idprovincia").val();
            $.ajax({
                type: 'POST',
                url: '../../com/ajax/distrito',
                data: 'idprovincia=' + idprovincia,
                success: function (data) {
                    $(".iddistrito").html(data)
                }
            });
            return false;
        }


        $(document).ready(function () {
            $('.clockpicker').clockpicker();
        });
    </script>

    <?php
}
?>




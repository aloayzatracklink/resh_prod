<?php
    if (isset($_POST['idhomdetail'])) {
    include_once("../../includes/init.php");
      $idDetail=limpiar(trim($_POST['idhomdetail']));
      $varTI=homologation_detail::ObtenerHomologationDetail($idDetail);
       $datos= homologation_detail::ObtenerHomologateTranspor($varTI->idhomclient);
       $odj = homologation_detail::ObtenerVehicle($datos->idlocation,$datos->idcompany,$datos->idcustomer,$varTI->idvehicle);
       $objPer= homologation_client::obtenerCliente($datos->idlocation,$datos->idcompany,$datos->idcustomer)

?>

    <link href="../../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
        <style>
            /*form homologate*/
            .inputfile{
                width: 0.1px;
                height: 0.1px;
                opacity: 0;
                overflow: hidden;
                position: absolute;
                z-index: -1;
            }
            .label_inputfile{
                cursor: pointer;
            }
        </style>
    <div class="showOPERA">
        <div class="well" style="padding-top: 11px;padding-bottom: 10px; ">
         <hr>
            <div class="row">
                <div class="form-group col-md-8">
                    <label for="Observaciones" >Cliente*</label>
                    <input required readonly style="border: none;" type="text" name="nomc" class="border-bottom form-control" value="<?php echo  $objPer->lastname." ".$objPer->firstname; ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="numeroTecnico" >Placa Vehiculo*</label>
                    <input  style="border: none;" readonly type="text"  name="placaVehiculo" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($odj->license)) ? "$odj->license" : "" ; ?>"  >
                    <input  type="hidden" name="idhomdetail" value="<?= $varTI->idhomdetail ?>"  >
                    <input  type="hidden" name="idcustomer" value="<?= $datos->idcustomer ?>"  >
                    <input  type="hidden" name="idcompany" value="<?=  $datos->idcompany?>"  >
                    <input  type="hidden" name="idlocation" value="<?= $datos->idlocation ?>"  >
                    <input  type="hidden" name="idvehicle" value="<?= $varTI->idvehicle ?>"  >
                    <input  type="hidden" name="unitid" value="<?= $varTI->unitid ?>"  >
                    <input  type="hidden" name="lastname" value="<?= $objPer->lastname ?>"  >
                    <input  type="hidden" name="firstname" value="<?= $objPer->firstname ?>"  >
                    <input  type="hidden" name="phonecell" value="<?= $objPer->phonecell ?>"  >
                    <input  type="hidden" name="correo" value="<?= $objPer->mail ?>"  >
                    <input  type="hidden" name="yearmodel" value="<?= $odj->yearmodel ?>"  >
                </div>
                <div class="form-group col-md-3">
                    <label for="Observaciones">Marca *</label>
                    <input required <?php echo $retVal = (!empty($odj->descbrand)) ? "readonly" : "" ; ?> style="border: none;" type="text" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($odj->descbrand)) ? "$odj->descbrand" : "" ; ?>" >
                </div>
                <div class="form-group col-md-3">
                    <label for="Observaciones">Modelo*</label>
                    <input required <?php echo $retVal = (!empty($odj->descmodel)) ? "readonly" : "" ; ?> style="border: none;" type="text" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($odj->descmodel)) ? "$odj->descmodel" : "" ; ?>">
                </div>
                <div class="form-group col-md-3">
                    <label for="Observaciones" >Color</label>
                    <input readonly <?php echo $retVal = (!empty($odj->color)) ? "readonly" : "" ; ?> style="border: none;" type="text" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($odj->color)) ? "$odj->color" : "" ; ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="Observaciones" >Chasis*</label>
                    <input required <?php echo $retVal = (!empty($odj->idchasis)) ? "readonly" : "" ; ?> style="border: none;" type="text" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = $odj->idchasis; ?>">
                </div>
                <div class="form-group col-md-4">
                    <label for="Observaciones" >Motor*</label>
                    <input required <?php echo $retVal = (!empty($odj->engine)) ? "readonly" : "" ; ?> style="border: none;" type="text" name="motor" class="border-bottom form-control" value="<?php echo $retVal = $odj->engine; ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="Observaciones">Tipo de Carrocería*</label>
                    <input required <?php echo $retVal = (!empty($varTI->bodytype)) ? "readonly" : "" ; ?> style="border: none;" type="text" name="bodytype" class="border-bottom form-control"  value="<?php echo $retVal = $varTI->bodytype; ?>" >
                </div>
                <div class="form-group col-md-3">
                    <label for="Observaciones"> Tipo de Vehículo*</label>
                    <input readonly  style="border: none;" type="text" name="vehicleType" class="border-bottom form-control"   value="<?= $varTI->vehicletype ?>">

                </div>
                <div class="form-group col-md-3">
                    <label for="Observaciones" >Capacidad*</label>
                    <input readonly <?php echo $retVal = (!empty($varTI->capacity)) ? "readonly" : "" ; ?> style="border: none;" type="text" name="capacity" class="border-bottom form-control"  value="<?php echo $retVal = $varTI->capacity; ?>">
                </div>
                <div class="form-group col-md-3">
                    <label for="numeroTecnico" >Volumen*</label>
                    <input readonly <?php echo $retVal = (!empty($varTI->volume)) ? "readonly" : "" ; ?> style="border: none;" type="text"   name="volume" class="border-bottom form-control"  value="<?php echo $retVal = $varTI->volume; ?>">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="Observaciones">Realiza Viajes Interprovincial * </label>
                </div>
                <div class="form-group col-md-3">
                    <input required readonly style="border: none;" type="text" name="travelinter" class="border-bottom form-control"  value="<?php echo $retVal = ($varTI->travelinter==1?"Si":"No"); ?>" >
                </div>
                <div class="form-group col-md-1">
                    <label for="Observaciones">Estado</label>
                </div>
                <div class="form-group col-md-3">
                    <select  style="border: none" class="border-bottom form-control text-capitalize cbostatus" name="cbostatus" id="cbostatus">
                        <option class="text-capitalize" value="0">-- Seleccionar --</option>
                        <option class="text-capitalize" value="107" <?php echo $varTI->status==="107"?"selected":"" ?> >Activo</option>
                        <option class="text-capitalize" value="110" <?php echo $varTI->status==="110"?"selected":"" ?> >Pendiente de Fotos</option>
                    </select>
                </div>
            </div>
        <div class="showTipo">
            <div class="row">
                <h3 class="col-md-6">Imagenes</h3>
            </div>
            <hr>
            <?php $images=homologation_documents::getImages($idDetail) ?>
            <?php if($images):  ?>
                <table class="table">
                    <?php foreach($images as $img): ?>
                        <tr>
                            <td><?= $img->creation_at ?></td>
                            <td><?= $img->desctype ?></td>
                            <td>
                                <a onclick="seleccionImage(<?= $img->idhomdocument ?>)" href="/ope/downinvdoc?tnkshow=fef56f6a7c51ab8&b=<?= $img->bucket ?>&f=<?= $img->name?>&fn=<?= strtoupper($img->name) ?>"
                                        target="_blank"><span class="fa fa-eye"></span></a>
                            </td>
                            <td>
                                <input type="checkbox" class="checkbox checkbox-success check<?= "_".$img->idhomdocument; ?>" <?php echo $img->checkimg==1?'checked disabled':'' ?>  name="check[]" id="check<?= "_".$img->idhomdocument; ?>" value="<?=$img->idhomdocument; ?>">
                                <input  type="hidden" name="ftype_<?= $img->idhomdocument;?>" id="ftype_<?= $img->idhomdocument;?>"  value="<?= $img->type ?>"  >
                            </td>
                            <?php if($img->checkimg!=1 && $varTI->status=='110' ){?>
                            <td>
                                <label style="color:orangered" for="file_img<?= "_".$img->idhomdocument; ?>" class="label_inputfile m-0"> <h5 [class.text-warning]='getValidImgFrontal(i)'><span class="fa fa-edit"></span> Cambiar</h5></label>
                                <input type="file" name="file_img<?= "_".$img->idhomdocument; ?>" class="inputfile" onchange="seleccionImage(<?= $img->idhomdocument ?>)" id="file_img<?= "_".$img->idhomdocument; ?>" accept=".jpg, .jpeg, .png" />
                            </td>
                            <?php }?>
                        </tr>
                    <?php endforeach ?>
                </table>
            <?php endif ?>
        </div>
        <div class="modal-footer">
            <?php if($varTI->status!='107'){?>
            <button type="button" id='cancelopemodalplkm' class="btn btn-white" data-dismiss="modal">Cancelar</button>
            <button type="submit" id="gestionarHomologacion" name="gestionarHomologacion" onclick="return btnHomologa(this)"  class="btn btn-primary">Guardar Cambios</button>
            <?php }else{?>
                <button type="button" id='cancelopemodalplkm' class="btn btn-white" data-dismiss="modal">Cerrar</button>
            <?php }?>
        </div>
    </div>
    </div>
        <script>
                function seleccionImage(i) {
                    $(".check_" + i).prop('checked', true);
                }
        </script>
<?php
}
?>

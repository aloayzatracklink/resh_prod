<?php
include_once("../../includes/head.php");
$tipoOpera=$_POST['tipoOpera'];
if($tipoOpera=='save') { ?>
    <div class="wrapper wrapper-content" style="padding: 0">
        <div class="row ibox float-e-margins ibox-content " style="padding: 10px">
            <div class="" style="padding: 0">
                <div id="table-scroll" class="table-scroll">
                    <div class="table-wrap text-center">
                        <input type="text" class="form-control input-sm m-b-xs" id="filtr" placeholder="Buscar">
                        <table class="table main-table" id="tblCobranza" >
                            <thead>
                                <tr>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2"></th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2">Cliente</th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2">N° Documento </th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="2">Correo</th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="2" rowspan="1">Cantidad</th>
                                </tr>
                                <tr>
                                    <th class="text-center fixed-side table-cabecera-horario" >Vehiculos</th>
                                    <th class="text-center fixed-side table-cabecera-horario" >Homologados</th>
                                </tr>
                            </thead>
                            <tbody id="resultadoadd">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $("#filtr").keypress(function(e)
        {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code==13){
                var datosS = $(this).val();
                datosS = datosS.replace('<>/"', '');

                $.ajax({
                    type : 'POST',
                    url  : 'pag/resultadoBusquedafAddTransportista',
                    data :  {datos:datosS},
                    success : function(data)
                    {
                        $("#resultadoadd").html(data);
                    }
                });
                return false;
            }
        });

        $.ajax({
            type : 'POST',
            url  : 'pag/resultadoBusquedafAddTransportista',
            data :  {datos:''},
            success : function(data)
            {
                $("#resultadoadd").html(data);
            }
        });
    </script>
    <?php
}
?>




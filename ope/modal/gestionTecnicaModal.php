<?php 
if ($_POST['rowid']) {
  include_once("../../includes/init.php");
  $idOperacion=htmlspecialchars(htmlentities($_POST['rowid']));
  $clienteObjModal=new Cliente;
  $ObjetoOperacion=Operacion::obtenerOperacionPorId($idOperacion);
  if ($ObjetoOperacion->idlocation==0) {
    $vehiculoObj=Vehiculo::obtenerVehiculo($ObjetoOperacion->idvehiculo);
    $clienteObj=Cliente::obtenerClientePorVehiculo($ObjetoOperacion->idvehiculo,$ObjetoOperacion->idcliente);  
  }else{
    $clienteObj=Onix::obtenerDatosDesdeIds($ObjetoOperacion->idcliente,$ObjetoOperacion->idvehiculo,$ObjetoOperacion->idlocation) ;
    $chequeoObj=Chequeos::obtenerChequeoTrackPorId($ObjetoOperacion->idChequeo);
    $vehiculoObj=false;
  }
  ?>
  <?php if ($ObjetoOperacion->estado=='36'||$ObjetoOperacion->estado=='14'||$ObjetoOperacion->estado=='15'){ ?> 
  <h2 class="text-center"><?php echo $ObjetoOperacion->trabajo ?></h2>
  <input type="hidden" name="trabajoO" value="<?= $ObjetoOperacion->trabajo  ?>" >
  <?php if ($ObjetoOperacion->idChequeo!=0): ?>
    <h3 id="mot" class="text-center text-danger">MOTIVO: <?= $chequeoObj->motivo ?></h3>
    <h3 id="seguimiento" class="text-center text-danger">Seguimiento realizado: <?= $chequeoObj->seguimientoRealizado ?></h3>
  <?php endif ?>
<?php if ($vehiculoObj): ?>
<?php if (!empty($vehiculoObj->serviciosAdicionales)): ?>
 <div class="row">
   <div class="form-group col-md-12">
     Servicios adicionales: <?php echo $vehiculoObj->serviciosAdicionales ?>
   </div>
 </div>
<?php endif ?>
</div> 
<!-- ORDENES INSTALACIONES FIN END END END END  --> 
  <div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->dni ?>">       
      <?php else: ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->ruc ?>">        
      <?php endif ?>  
    </div>

    <div class="col-md-9">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->razonSocial ?>">       
      <?php endif ?>
    </div> 
    </div> 
<hr>
<h3>Datos del vehiculo</h3>
  <div class="row"><hr>
    <div class="col-lg-12 field_wrapper"> 
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="field_name[0][]" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $vehiculoObj->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="">Chasis</label>
          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="field_name[1][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->chasis ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="motor" class="">Motor</label>
          <input style="border: none;" type="text" placeholder="Motor" id="motor" name="field_name[2][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->motor ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="marca" class="">Marca</label>
          <input style="border: none;" type="text" placeholder="Marca" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $vehiculoObj->marca ?>" >
        </div>
      </div> 
      <div class="row">
        <div class="form-group col-md-3">
          <label for="modelo" class="">Modelo</label>
          <input style="border: none;" type="text" placeholder="Modelo" id="modelo" name="field_name[4][]" class="border-bottom form-control text-capitalize"  required  value="<?php echo $vehiculoObj->modelo ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="color" class="">Color</label>
          <input style="border: none;" type="text" placeholder="Color" id="color" name="field_name[5][]" class="border-bottom form-control x2 text-capitalize" required value="<?php echo $vehiculoObj->color ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="anio" class="">Año</label>
          <input style="border: none;" type="text" placeholder="Año" id="anio" name="field_name[6][]" class="border-bottom form-control" required value="<?php echo $vehiculoObj->anio ?>" >
        </div>
        <div class="form-group col-md-3">
           <label for="plan" class="">Plan *</label>
           <select  style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
              <option class="" value="<?php echo $vehiculoObj->plan ?>"  selected ><?php echo $vehiculoObj->plan ?></option>      
          </select> 
        </div>
      </div>     
    </div>
  </div> 
<hr>

<h2 class="text-center">Datos de instalacíon</h2>
<div class="row">
  <div class="form-group col-md-6">
    <label for="tecnico" class="">Tecnicos</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="tecnico" id="tecnicoAsig">
      <?php if ($ObjetoOperacion->idTecnicoAsignado!=0): ?>
        <?php 
          $usuarioDato=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoAsignado);
         ?>
      <option class="text-capitalize" value="<?= $ObjetoOperacion->idTecnicoAsignado ?>" selected ><?= $usuarioDato->usuarioNombre ?> <?= $usuarioDato->usuarioApellidoPaterno ?></option>
      <?php else: ?>
      <option class="text-capitalize" value="" disabled selected >Seleccione técnico</option>
      <?php endif ?>
      <?php Usuario::obtenerTecnicos() ?>            
    </select>
  </div>
  <div class="form-group col-md-6">
    <label for="orte" class="">Numero de Orte</label>
    <input style="border: none;" type="text" placeholder="Numero de Orte" id="orte" name="orte" class="border-bottom form-control x2 " required  value="<?= $ObjetoOperacion->numeroOrte ?>">
  </div>
</div> 
<?php if ($ObjetoOperacion->lugarInstalacion=="Domicilio"): ?>
<div class="row">
   <div class="col-md-3"><h3>Coordenadas de instalación --></h3></div>
   <div class="col-md-6">
    <input style="border: none;" type="text" placeholder="Coordenadas insalaicon" id="orden" name="orden" class="border-bottom form-control x2 " value="<?php echo $ObjetoOperacion->coordenadasInstalacion ?>" >
  </div>
</div>
<?php endif ?>
<hr>
<h4>Lugar de instalación <?php echo $ObjetoOperacion->lugarInstalacion ?> <?php echo $retVal = (!empty($ObjetoOperacion->nombreConcecionarioInstalicion)) ? $ObjetoOperacion->nombreConcecionarioInstalicion : "" ; ?>  del dia <?= formatoHaceEn($ObjetoOperacion->fechaInstalacion) ?></h4>
<hr>

<input type="hidden" name="idoperacion" value="<?php echo $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idvh" value="<?php echo $vehiculoObj->idvehiculo ?>">
<input type="hidden" name="datosfacturacionid" value="<?php echo $ObjetoOperacion->datosfacturacionid ?>">
<input type="hidden" name="idope" value="<?php echo $ObjetoOperacion->idConfirmacionOPE ?>">
<div class="row">
  <div class="form-group  col-md-4">
    <label for="departamentoInstalacionD" class="">Departamento*</label>
    <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize" required name="departamentoInstalacionD" id="departamentoInstalacionD">
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>" disabled selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
      <?php if ($objDepartamento): ?>
        <?php foreach ($objDepartamento as $var): ?>
          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
        <?php endforeach ?>
      <?php else: ?>
      <?php endif ?>
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="provinciaInstalacionD" class="">Provincia*</label>
    <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>" disabled selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="distritoInstalacionD" class="">Distrito*</label>
    <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize" required name="distritoInstalacionD"  id="distritoInstalacionD" >
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>" disabled selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
    </select>           
  </div>
</div>
<div class="row">          
  <div class="form-group col-md-3">
    <label for="lugar" class="">Lugar</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="lugar">
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->lugarInstalacion ?>"  selected ><?php echo $ObjetoOperacion->lugarInstalacion ?></option>
      <option class="text-capitalize" value="Taller" >Taller</option>
      <option class="text-capitalize" value="Domicilio Cliente" >Domicilio Cliente</option>
      <option class="text-capitalize" value="Concesionario" >Concesionario</option>
    </select>
  </div>
  <div class="form-group col-md-9">
    <label for="direccionInstalacion" class="">Dirección Instalación </label>
    <input style="border: none;" type="text" placeholder="Direccion Instalcion" id="direccionInstalacion" name="direccionInstalacion" class="border-bottom form-control" required value="<?php echo $ObjetoOperacion->direccionInstacion ?>">
  </div>
</div>
<?php if ($ObjetoOperacion->coordenadasInstalacion != ''): ?>
  <div class="row"><div class="col-lg-12"><label for="cordenadas">Coordenadas de instalacion: </label><label > <?php echo $ObjetoOperacion->coordenadasInstalacion ?></label><br><h1 class="text-success"><a  target="_blank" href="mapa.php?coordenadas=<?php echo $ObjetoOperacion->coordenadasInstalacion ?>&lugar=<?php echo $ObjetoOperacion->direccionInstacion ?>">ver mapa</a></h1></div></div>    
<?php endif ?>

<div class="row">
  <div class="form-group col-md-5" id="data_1">   
    <div class="row">
      <div class="form-group col-md-6">   
        <label>Fecha</label>
        <div class="input-group date">
          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input  type="text" class="form-control"  id="fechaCh" name="fecha" autocomplete="off" required value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10) ?>" >
        </div>
      </div>
      <div class="form-group col-md-6">
        <label>Hora</label>
        <div class="input-group" data-autoclose="true">
          <input  type="text" class="form-control" id="horaCh" name="hora" autocomplete="off" required value="<?=  substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>"> 
          <span class="input-group-addon">
            <span class="fa fa-clock-o"></span>
          </span>
        </div>
      </div>
    </div> 
  </div> 
  <div class="form-group col-md-4">
     <label for="personaContacto" class="">Persona contacto </label>
    <input style="border: none;" type="text" placeholder="Direccion Instalcion" id="personaContacto" name="personaContacto" class="border-bottom form-control"  value="<?php echo $ObjetoOperacion->nombreContacto ?>">
  </div>
  <div class="form-group col-md-3">
     <label for="numeroContacto" class="">Número contacto </label>
    <input style="border: none;" type="text" placeholder="Número contacto" id="numeroContacto" name="numeroContacto" class="border-bottom form-control"  value="<?php echo $ObjetoOperacion->numeroContacto ?>">
  </div>
</div>
<?php else: ?>
  <div class="row">
        <div class="form-group col-md-3">
        <label for="Observaciones" > Usuario Web</label>
        <input name="usuarioWeb" style="border: none;" type="text" placeholder="Web" id="Observaciones" <?php echo $retVal = (!empty($clienteObj->dni)) ? "readonly" : "" ; ?> class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>">
      </div>
      <div class="form-group col-md-5">
      <?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
        <label for="numeroTecnico" >Nombre de cliente*</label>
        <input  style="border: none;" type="text" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ; ?> ">
      </div>
      <div class="col-md-2"><label for="estado" >Celular</label>
        <input style="border: none;" type="text" <?php echo $retVal = (!empty($clienteObj->celular)) ? "readonly" : "" ; ?> id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">
      </div>
      <div class="col-md-2"><label for="estado" >Celular</label>
        <input style="border: none;" type="text" readonly id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular2)) ? "$clienteObj->celular2" : "" ; ?>">
      </div>
    </div>
     </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="form-group col-md-3">
    <label for="numeroTecnico" >Placa Vehiculo*</label>
    <input  style="border: none;" readonly type="text" placeholder="Placa" id="numeroTecnico" name="placa" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->placa)) ? "$clienteObj->placa" : "" ; ?>"   >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Marca *</label>
    <input required <?php echo $retVal = (!empty($clienteObj->marca)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->marca)) ? "$clienteObj->marca" : "" ; ?>" >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Modelo*</label>
    <input  required <?php echo $retVal = (!empty($clienteObj->modelo)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Modelo" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($clienteObj->modelo)) ? "$clienteObj->modelo" : "" ; ?>">
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Color</label>
    <input <?php echo $retVal = (!empty($clienteObj->color)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Color" id="Observaciones" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->color)) ? "$clienteObj->color" : "" ; ?>">
  </div>  
</div>

<span class="ocultarThis">
<div class="row">
  <div class="form-group col-md-4">
    <label for="Observaciones"  >Chasis*</label>
    <input required <?php echo $retVal = (!empty($clienteObj->chasis)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->chasis)) ? "$clienteObj->chasis" : "" ; ?>">
  </div>
  <div class="form-group col-md-4">
    <label for="Observaciones" >Motor</label>
    <input <?php echo $retVal = (!empty($clienteObj->motor)) ? "readonly" : "" ; ?> style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="motor" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->motor)) ? "$clienteObj->motor" : "Actualizar" ; ?>"  >
  </div>
  <div class="form-group col-md-4">
    <label for="Observaciones" >Plan</label>
    <input  style="border: none;" type="text" placeholder="Observaciones" readonly id="Observaciones" name="plan" class="border-bottom form-control" value="<?= $chequeoObj->plan ?>"  >
  </div>
</div>
<input type="hidden" name="idoperacion" value="<?= $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idChequeo" value="<?= $ObjetoOperacion->idChequeo ?>">
  <?php if ($ObjetoOperacion->trabajo=='Chequeo en linea'): ?>
    <div class="row"> 
  <div class="form-group col-md-12">
    <label for="Observaciones" >Número de orden</label>
    <input  style="border: none;" type="text" placeholder="Número de orden" id="numeroOrden" name="numeroOrden" class="border-bottom form-control"  >
    <input type="hidden" name="chequeo" value="ok">
    <input type="hidden" name="chequeoLinea" value="ok">

  </div>
</div>
  <?php endif ?>
<div class="row">
  <div class="form-group col-md-6">
    <label for="numeroTecnico" >Persona de contacto</label>
    <input  style="border: none;" readonly type="text" placeholder="Persona de contacto" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control"  value="<?= $chequeoObj->personaContacto ?>"   >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Numero contacto *</label>
    <input  readonly="" style="border: none;" type="text" placeholder="Numero contacto *" id="Observaciones" name="numcontacto" class="border-bottom form-control"  value="<?= $chequeoObj->numeroContacto ?>" >
  </div>
  <div class="form-group col-md-3">
    <label for="Observaciones" >Ubicacion de sistema*</label>
    <input  required readonly style="border: none;" type="text" placeholder="Ubicacion de sistema" id="Observaciones" name="ubsistema" class="border-bottom form-control"   value="<?= $chequeoObj->ubicacionSistema ?>">
  </div>
  </div>
  <div class="row">
  <div class="form-group col-md-2">
    <label for="numeroTecnico" >Lugar</label>
    <input  style="border: none;" readonly type="text" placeholder="Persona de contacto" id="numeroTecnico" name="lugar" class="border-bottom form-control" required value="<?= $chequeoObj->lugar ?>"   >
  </div>
  <div class="form-group col-md-5">
    <label for="Observaciones" >Direccion *</label> <a target="_blank" href="mapa.php?coordenadas=<?= $chequeoObj->coordenadas ?>&lugar=<?= $chequeoObj->direccion ?>" class="text-center">Ver mapa</a>
    <input required readonly="" style="border: none;" type="text" placeholder="Numero contacto *" id="Observaciones" name="dir" class="border-bottom form-control"  value="<?= $chequeoObj->direccion ?>" >
  </div>
  <div class="form-group col-md-5">
    <label for="Observaciones" >Referencia *</label>
    <input  required readonly style="border: none;" type="text" placeholder="Ubicacion de sistema" id="Observaciones" name="ref" class="border-bottom form-control"   value="<?= $chequeoObj->referencia ?>">
  </div>  
</div>  
  <div class="row">
  <div class="form-group col-md-5">
    <label for="numeroTecnico" >Fecha instalacion</label>
    <input  style="border: none;" readonly type="text" placeholder="Persona de contacto" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?= $chequeoObj->fechaInstalacion ?>" >
  </div>
  <div class="form-group col-md-7">
    <label for="Observaciones" >Tecnico instalador *</label>
    <input required readonly="" style="border: none;" type="text" placeholder="Numero contacto *" id="Observaciones" name="tei" class="border-bottom form-control"  value="<?= $chequeoObj->instalador ?>">
  </div>
  <?php 
      $chequeoObj->fechaChequeoEnSac;
      $fecha=date("Y-m-d",strtotime($chequeoObj->fechaChequeoEnSac));
      $hora=date("H:i",strtotime($chequeoObj->fechaChequeoEnSac));

   ?>
    <div class="row">
    <h4 class="text-center">hora y fecha estimada de instalación: <?= formatoHaceEn($chequeoObj->fechaChequeoEnSac) ?></h4>
  </div>
  
</div>
  <div class="row"> 
  <div class="form-group col-md-6">
     <label for="tecnico" class="">Tecnicos</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="tecnico" id="tecnicoAsig">
    <?php if ($ObjetoOperacion->idTecnicoAsignado!=0): ?>
        <?php 
          $usuarioDato=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoAsignado);
         ?>
      <option class="text-capitalize" value="<?= $ObjetoOperacion->idTecnicoAsignado ?>" selected ><?= $usuarioDato->usuarioNombre ?> <?= $usuarioDato->usuarioApellidoPaterno ?></option>
      <?php else: ?>
      <option class="text-capitalize" value="" disabled selected >Seleccione técnico</option>
      <?php endif ?>    
      <?php Usuario::obtenerTecnicos() ?>            
    </select>
  </div>
  <div class="form-group col-md-6">
    <label for="numeroOrte" >Número de orte</label>
    <input  style="border: none;" type="text" placeholder="Número de orte" id="numeroOrte" name="numeroOrte" class="border-bottom form-control"  required="" value="<?= $ObjetoOperacion->numeroOrte  ?>">
  </div>
</div>
<div class="row">
  <div class="col-md-6 form-group"><label for="Observaciones" >Fecha *</label>
    <input required  style="border: none;" type="text" placeholder="" id="fechaCh" name="fecha" class="border-bottom form-control"  value="<?= $fecha ?>" ></div>
  <div class="col-md-6 form-group"><label for="Observaciones" >Hora *</label>
    <input required   style="border: none;" type="text" placeholder=" *" id="horaCh" name="hora" class="border-bottom form-control"  value="<?= $hora ?>" ></div>
</div>   
<?php //en los dos tipos de chequeo que queda en SAC confirman orden orte fecha y lugar  ?>
</span>

<?php endif ?>
<?php if ($ObjetoOperacion->estado=='15'){ ?>  

  <div class="row">
       <div class="form-group col-md-4">
          <label for="placa" class="">Tecnico Asignado</label>
          <?php $usuarioAsgi=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoAsignado) ?>
          <input style="border: none;" type="text" placeholder="Tecnico Asignado" id="placa" name="" class="border-bottom form-control x2 text-uppercase" required value="<?= $usuarioAsgi->usuarioNombre.' '.$usuarioAsgi->usuarioApellidoPaterno ?>">
        </div>
       <div class="form-group col-md-4">
       <?php $usuarioInst=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoInstalador) ?>
          <label for="placa" class="">Tecnico instalador</label>
          <input style="border: none;" type="text" placeholder="Tecnico instalador" id="placa" name="" class="border-bottom form-control x2 text-uppercase" required value="<?= $usuarioInst->usuarioNombre.' '.$usuarioInst->usuarioApellidoPaterno ?>">
        </div>
       <div class="form-group col-md-4">
       <?php $usuarioCentr=Usuario::obtenerUsuario($ObjetoOperacion->idusuarioActivador) ?>
          <label for="placa" class="">Usuario cetral Hora</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="" class="border-bottom form-control x2 text-uppercase" required value="<?= $usuarioCentr->usuarioNombre.' '.$usuarioCentr->usuarioApellidoPaterno ?>">
          <br><small class="text-danger">hora <?= haceMinimo($ObjetoOperacion->fechaActivacion) ?></small>
        </div>
  </div>
<?php } ?>
<?php }else{ ?>
<div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->dni ?>">       
      <?php else: ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->ruc ?>">        
      <?php endif ?>  
    </div>

    <div class="col-md-9">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->razonSocial ?>">       
      <?php endif ?>
    </div> 
    </div> 
<hr>
<h3>Datos del vehiculo</h3>
  <div class="row"><hr>
    <div class="col-lg-12 field_wrapper"> 
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="field_name[0][]" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $vehiculoObj->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="">Chasis</label>
          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="field_name[1][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->chasis ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="motor" class="">Motor</label>
          <input style="border: none;" type="text" placeholder="Motor" id="motor" name="field_name[2][]" class="border-bottom form-control text-uppercase" required value="<?php echo $vehiculoObj->motor ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="marca" class="">Marca</label>
          <input style="border: none;" type="text" placeholder="Marca" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $vehiculoObj->marca ?>" >
        </div>
      </div>
      </div>
  </div>
<h2 class="text-center">Datos de instalacíon</h2>
<div class="row">
  <div class="form-group col-md-6">
    <label for="tecnico" class="">Tecnicos</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="tecnico" disabled>
      <?php if ($ObjetoOperacion->idTecnicoAsignado!=0): ?>
        <?php 
          $usuarioDato=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoAsignado);
         ?>
      <option class="text-capitalize" value="<?= $ObjetoOperacion->idTecnicoAsignado ?>" selected ><?= $usuarioDato->usuarioNombre ?> <?= $usuarioDato->usuarioApellidoPaterno ?></option>
      <?php else: ?>
      <option class="text-capitalize" value="" disabled selected >Seleccione técnico</option>
      <?php endif ?>
      <?php Usuario::obtenerTecnicos() ?>            
    </select>
  </div>
  <div class="form-group col-md-6">
    <label for="orte" class="">Numero de Orte</label>
    <input style="border: none;" type="text" placeholder="Numero de Orte" id="orte" name="orte" class="border-bottom form-control x2 " required  value="<?= $ObjetoOperacion->numeroOrte ?>">
  </div>
</div> 
<?php if ($ObjetoOperacion->lugarInstalacion=="Domicilio"): ?>
<div class="row">
   <div class="col-md-3"><h3>Coordenadas de instalación --></h3></div>
   <div class="col-md-6">
    <input style="border: none;" type="text" placeholder="Coordenadas insalaicon" id="orden" name="orden" class="border-bottom form-control x2 " value="<?php echo $ObjetoOperacion->coordenadasInstalacion ?>" >
  </div>
</div>
<?php endif ?>
<hr>
<h4>Lugar de instalación <?php echo $ObjetoOperacion->lugarInstalacion ?> <?php echo $retVal = (!empty($ObjetoOperacion->nombreConcecionarioInstalicion)) ? $ObjetoOperacion->nombreConcecionarioInstalicion : "" ; ?>  del dia <?= formatoHaceEn($ObjetoOperacion->fechaInstalacion) ?></h4>
<hr>

<input type="hidden" name="idoperacion" value="<?php echo $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idvh" value="<?php echo $vehiculoObj->idvehiculo ?>">
<input type="hidden" name="datosfacturacionid" value="<?php echo $ObjetoOperacion->datosfacturacionid ?>">
<input type="hidden" name="idope" value="<?php echo $ObjetoOperacion->idConfirmacionOPE ?>">
<div class="row">
  <div class="form-group  col-md-4">
    <label for="departamentoInstalacionD" class="">Departamento*</label>
    <select  style="border: none;" class="departamentoInstalacionD border-bottom form-control text-capitalize" required name="departamentoInstalacionD" id="departamentoInstalacionD">
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->departamentoi ?>" disabled selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($ObjetoOperacion->departamentoi) ?><?= $departamento->departamento ?></option>
      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
      <?php if ($objDepartamento): ?>
        <?php foreach ($objDepartamento as $var): ?>
          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
        <?php endforeach ?>
      <?php else: ?>
      <?php endif ?>
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="provinciaInstalacionD" class="">Provincia*</label>
    <select  style="border: none;" class="provinciaInstalacionD border-bottom form-control text-capitalize" required name="provinciaInstalacionD" id="provinciaInstalacionD" >
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->provinciai ?>" disabled selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($ObjetoOperacion->provinciai) ?><?php echo $provincia->provincia ?></option>
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="distritoInstalacionD" class="">Distrito*</label>
    <select  style="border: none;" class="distritoInstalacionD border-bottom form-control text-capitalize" required name="distritoInstalacionD"  id="distritoInstalacionD" >
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->distritoi ?>" disabled selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($ObjetoOperacion->distritoi) ?><?= $distrito->distrito ?></option>
    </select>           
  </div>
</div>
<div class="row">          
  <div class="form-group col-md-3">
    <label for="lugar" class="">Lugar</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="lugar">
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->lugarInstalacion ?>"  selected ><?php echo $ObjetoOperacion->lugarInstalacion ?></option>
      <option class="text-capitalize" value="Taller" >Taller</option>
      <option class="text-capitalize" value="Domicilio Cliente" >Domicilio Cliente</option>
      <option class="text-capitalize" value="Concesionario" >Concesionario</option>
    </select>
  </div>
  <div class="form-group col-md-9">
    <label for="direccionInstalacion" class="">Dirección Instalación </label>
    <input style="border: none;" type="text" placeholder="Direccion Instalcion" id="direccionInstalacion" name="direccionInstalacion" class="border-bottom form-control" required value="<?php echo $ObjetoOperacion->direccionInstacion ?>">
  </div>
</div>
<?php if ($ObjetoOperacion->coordenadasInstalacion != ''): ?>
  <div class="row"><div class="col-lg-12"><label for="cordenadas">Coordenadas de instalacion: </label><label > <?php echo $ObjetoOperacion->coordenadasInstalacion ?></label><br><h1 class="text-success"><a  target="_blank" href="mapa.php?coordenadas=<?php echo $ObjetoOperacion->coordenadasInstalacion ?>&lugar=<?php echo $ObjetoOperacion->direccionInstacion ?>">ver mapa</a></h1></div></div>    
<?php endif ?>
<div class="row">
  <div class="form-group col-md-5" id="data_1">
    <div class="row">
      <div class="form-group col-md-6">   
        <label>Fecha</label>
        <div class="input-group date">
          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control"  id="fechaCh" name="fecha" autocomplete="off" required  value="<?= substr($ObjetoOperacion->fechaInstalacion, 0,10) ?>" >
        </div>
      </div>
      <div class="form-group col-md-6">
        <label>Hora</label>
        <div class="input-group" data-autoclose="true">
          <input type="text" class="form-control" id="horaCh" name="hora" autocomplete="off" required   value="<?=  substr($ObjetoOperacion->fechaInstalacion, 11,18) ?>"> 
          <span class="input-group-addon">
            <span class="fa fa-clock-o"></span>
          </span>
        </div>
      </div>
    </div> 
  </div> 
  <div class="form-group col-md-4">
     <label for="personaContacto" class="">Persona contacto </label>
    <input style="border: none;" type="text" placeholder="Direccion Instalcion" id="personaContacto" name="personaContacto" class="border-bottom form-control"  value="<?php echo $ObjetoOperacion->nombreContacto ?>">
  </div>
  <div class="form-group col-md-3">
     <label for="numeroContacto" class="">Número contacto </label>
    <input style="border: none;" type="text" placeholder="Número contacto" id="numeroContacto" name="numeroContacto" class="border-bottom form-control"  value="<?php echo $ObjetoOperacion->numeroContacto ?>">
  </div>
</div>
  <div class="row">
       <div class="form-group col-md-4">
          <label for="placa" class="">Tecnico Asignado</label>
          <?php $usuarioAsgi=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoAsignado) ?>
          <input style="border: none;" type="text" placeholder="Tecnico Asignado" id="placa" name="" class="border-bottom form-control x2 text-uppercase" required value="<?= $usuarioAsgi->usuarioNombre.' '.$usuarioAsgi->usuarioApellidoPaterno ?>">
        </div>
       <div class="form-group col-md-4">
       <?php $usuarioInst=Usuario::obtenerUsuario($ObjetoOperacion->idTecnicoInstalador) ?>
          <label for="placa" class="">Tecnico instalador</label>
          <input style="border: none;" type="text" placeholder="Tecnico instalador" id="placa" name="" class="border-bottom form-control x2 text-uppercase" required value="<?= $usuarioInst->usuarioNombre.' '.$usuarioInst->usuarioApellidoPaterno ?>">
        </div>
       <div class="form-group col-md-4">
       <?php $usuarioCentr=Usuario::obtenerUsuario($ObjetoOperacion->idusuarioActivador) ?>
          <label for="placa" class="">Usuario cetral Hora</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="" class="border-bottom form-control x2 text-uppercase" required value="<?= $usuarioCentr->usuarioNombre.' '.$usuarioCentr->usuarioApellidoPaterno ?>">
          <br><small class="text-danger">hora <?= haceMinimo($ObjetoOperacion->fechaActivacion) ?></small>
        </div>
  </div>
<?php } ?>
<?php
} 
?> 
<div class="row">
    <div id="solicitud" name="solicitud" class="btn btn-success col-md-6 solici ">Solicitar cambio de fecha.</div>   
    <div class="col-md-6"></div>
</div>          
<script src="../../assets/js/jquery-confirm.min.js"></script>
<div id="programacionHtml"></div>
<script>  
 $("#tecnicoAsig").change(function()
    { 
      var idtecnico = $(this).val(); 
      var fechaCh= $("#fechaCh").val();     
      var horaCh= $("#horaCh").val();     
      fecha=fechaCh+' '+horaCh;      
      $.ajax({
        type : 'POST',
        url  : '../includes/programacionAjaxATecnica',
        data : {fecha:fecha,idtecnico},
        success : function(data)
        {
         $("#programacionHtml").html(data);
        }
      });      
      return false;
    });
   function esconder(){  
  $("#programacionHtml").html("");   
  } 
         </script>

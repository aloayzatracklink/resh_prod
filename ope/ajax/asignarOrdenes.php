<?php 
	require_once("../../includes/init.php");
	$n=1;
?>
<?php $objInstalar=Operacion::obtenerInstalacionesSinOrden() ?>
<?php if ($objInstalar): ?>

<?php foreach ($objInstalar as $var): ?>
        <?php $datosTrama = TramaIntegracionTY::obtenerTramaIntegracionPorIdOperacion($var->idoperacion);  ?>
        <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>
	<tr style="<?= $datosVehiculo->campana=="Toyota"?"border: 2px solid red !important":"" ?>">
		<td><?php echo $n++ ?></td>
		<!-- <td><?= $var->idoperacion ?></td> -->
		<td><?php echo $var->trabajo ?></td>
		<!-- <td><?= $var->idChequeo ?></td> -->

		<?php if ($var->idlocation == 0): ?> 
			
		<td><?php $clienteObj =Cliente::obtenerDatosDeclientePorId($var->idcliente);?> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial) ; ?></td>
		<td><?php $vehiculoObj = Vehiculo::obtenerVehiculo($var->idvehiculo); echo $vehiculoObj->placa ?></td>
            <td><?php $vehiculoObj = Vehiculo::obtenerVehiculo($var->idvehiculo); echo $vehiculoObj->chasis ?></td>
        <?php else: ?>
		<?php $clienteObj=Onix::obtenerDatosDesdeIds($var->idcliente,$var->idvehiculo,$var->idlocation) ?>

			<?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
		<td><?= (!empty($clienteObj->nombre)) ? "strtoupper($apellidos)"." "."strtoupper($clienteObj->nombre)" : "strtoupper($apellidos)" ; ?></td>
		<td><?= $clienteObj->placa ?></td>
		<?php endif ?>	
		<td class="center"><?= obtenerEstado($var->estado)  ?></td>

        <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosVehiculo->campana</span>":$datosVehiculo->campana) ?></td>
        <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosTrama->tipo_transaccion</span>":'') ?></td>
		<?php $usuarioObj=Usuario::obtenerUsuario($var->idUsuarioCargo);  ?>
		<td style="background-color: <?php echo $retVal = ($usuarioObj->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>"><?php  echo $usuarioObj->usuarioNombre ?> <?php echo $usuarioObj->usuarioApellidoPaterno?></td>
		<td class="center "><button class="btn btn-primary "   data-toggle="modal" data-target="#ordenes" data-id="<?php echo $var->idoperacion ?>">Elegir <i class="fa fa-cog"> </i></button></td>
		<td class="center"><strong class="font-bold">
			<?php $usuario=Usuario::obtenerUsuario($var->idConfirmacionOPE);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno; ?> <br><small style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px"><?php echo haceMinimo($var->fechaConfirmacionOPE)  ?></small>
		</strong></td>
		</tr>
<?php endforeach ?>
	
<?php endif ?>
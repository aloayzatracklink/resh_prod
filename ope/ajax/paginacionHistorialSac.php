<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  if (isset($_POST["fecha"])) {
    $fecha=strip_tags($_POST['fecha']);
    $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
    $desde=date("Y-m-d 00:00:00",strtotime($desde));    
    $hasta=date("Y-m-d ",strtotime($hasta)); 
    $hasta=$hasta."23:59:59";
  }
  $operacionesObj=new Operacion();
  if (empty($fecha)){    
    $cantidadO=$operacionesObj->historialSacCantidadTotal();  
  }else{    
    $cantidadO=$operacionesObj->historialSacCantidadTotalRangoFecha($desde,$hasta);    
  }  
  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);

  if (empty($fecha)) {
      $datos=$operacionesObj->obtenerHistorialSac($posisionDePagina,$registrosPorPagina);
  } else {
      $datos=$operacionesObj->obtenerHistorialSacRango($posisionDePagina,$registrosPorPagina,$desde,$hasta);
    // $datos=$datosFacturacion->obtenerDatosParaFacturacionRango($posisionDePagina,$registrosPorPagina,$desde,$hasta);        
  }  

  $n=1;
             ?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>
            <?php $datosTrama = TramaIntegracionTY::obtenerTramaIntegracionPorIdOperacion($var->idoperacion);  ?>
    <?php $plataformaU=Usuario::obtenerUsuario($var->idUsuarioCargo)  ?>
    <?php if ($var->idChequeo==0): ?>
    <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>
      <?php else: ?>
      <?php $clienteObj=Onix::obtenerDatosDesdeIds($var->idcliente,$var->idvehiculo,$var->idlocation) ?>
    <?php endif ?>
    <tr  <?= obtenerClass($var->lugarInstalacion) ?>  style="<?= $datosVehiculo->campana=="Toyota"?"border: 2px solid red !important":"" ?>">
          <td><?= $n++ ?></td>
          <td  ><?= !empty($var->numeroOrte)?$var->numeroOrte:"Sin orte" ?></td>       
          <td><?= $var->trabajo ?></td>
          <td><?= $var->lugarInstalacion ?></td>
    <?php if ($var->idChequeo==0): ?>
         <td><?= ($clienteObj->tipoCliente=="Natural") ? $clienteObj->apellidosC." ".$clienteObj->nombre : $clienteObj->razonSocial ; ?></td>
         <td><?= $datosVehiculo->placa ?></td>
       <?php else: ?>
          <?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
       <td><?= $retVal = (!empty($clienteObj->nombre)) ? "$apellidos"." "."$clienteObj->nombre" : "$apellidos" ; ?></td>       
         <td><?= $clienteObj->placa ?></td>
        <?php endif ?>
          <td><?= (!empty($var->fechaInstalacion))?formatoHaceEnSinFecha($var->fechaInstalacion):'Pendiente' ?></td>
          <td><?= $plataformaU->usuarioNombre." ".$plataformaU->usuarioApellidoPaterno ?></td>
          <td><?= haceMinimo($var->fechaPlataformaV) ?></td>
        <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosVehiculo->campana</span>":$datosVehiculo->campana) ?></td>
        <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosTrama->tipo_transaccion</span>":'') ?></td>
          <td><?= obtenerEstado($var->estado) ?></td>
    <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" data-id="<?php echo $var->idoperacion ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;</td> 
    </tr>
<?php endforeach ?>
<tr>
<td colspan="8">Mostrando: <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?> <br>   <?php if (isset($_POST["fecha"])) { ?>Instalaciones con orte: domicilio:<?php $datosDomi=$operacionesObj->obtenerInstalacionesDomProgramadas($desde,$hasta) ?><?= ($datosDomi)? $datosDomi->cantidad : 0 ?> concesionario:<?php $datoscon=$operacionesObj->obtenerInstalacionesDiariariasConcecionarioProgramadas($desde,$hasta) ?><?= ($datoscon)? $datoscon->cantidad : 0 ?> Taller:<?php $datosta=$operacionesObj->obtenerInstalacionesDiariariasTallerProgramadas($desde,$hasta) ?><?= ($datosta)? $datosta->cantidad : 0 ?> Total: <?php $datostotal=$operacionesObj->obtenerInstalacionesDiariariasTotalProgramadas($desde,$hasta) ?><?= ($datostotal)? $datostotal->cantidad : 0 ?> <?php } ?></td>
  <td colspan="4">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="10" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>

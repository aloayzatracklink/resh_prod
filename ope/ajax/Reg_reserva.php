<?php if ($_POST): ?>
<?php
    include_once("../../includes/init.php");
    $ObjTect=new tablaProgramacion();
    $tipo=($_POST['tipo']);

    if($tipo=='save') {
        $idtec = trim($_POST['idtec']);
        $fecha = cambiarPorguion($_POST['fecha']);
        $horaini = ($_POST['horaini']);
        $horafin = ($_POST['horafin']);
        $rpara = ($_POST['rpara']);
        $motivo = ($_POST['motivo']);

        $ObjTect->idtecnico = $idtec;
        $ObjTect->fecha = $fecha;
        $ObjTect->hora = $horaini;
        $ObjTect->horafin = $horafin;
        $ObjTect->rpara = $rpara;
        $ObjTect->observacion = $motivo;
        $tt = tablaProgramacion::verificarsiestarangoTbProg($horaini, $horafin, $fecha, $idtec);
        if ($tt->tt != 0) {
            echo 'dp';
        } else {
            if ($tipo == 'save') {
                $datos = $ObjTect->reservarTurno();
                echo 'Ok';
            }
        }
    }else if($tipo=='del'){
        $idreserva= ($_POST['idReserva']);
        $ObjTect->idtablaProgramacion=$idreserva;
        $datos = $ObjTect->eliminarReserva();
        echo 'Ok';
    }else if($tipo=='edit'){

        $idtec = trim($_POST['idtec']);
        $fecha = cambiarPorguion($_POST['fecha']);
        $horaini = ($_POST['horaini']);
        $horafin = ($_POST['horafin']);
        $rpara = ($_POST['rpara']);
        $motivo = ($_POST['motivo']);
        $idreserva= ($_POST['idreser']);

        $ObjTect->idtecnico = $idtec;
        $ObjTect->fecha = $fecha;
        $ObjTect->hora = $horaini;
        $ObjTect->horafin = $horafin;
        $ObjTect->rpara = $rpara;
        $ObjTect->observacion = $motivo;
        $ObjTect->idtablaProgramacion = $idreserva;

        $tt = tablaProgramacion::verificarsiestarangoTbProg2($horaini, $horafin, $fecha, $idtec,$idreserva);
        if ($tt->tt != 0) {
            echo 'dp';
        } else {

            $datos = $ObjTect->updateReserva();
            echo 'Ok';
        }
    }
    ?>
<?php endif ?>

<?php 
	require_once("../../includes/init.php");
	$n=1;
?>
<?php $obCita=Cita::obtenerCitasSac() ?>
<?php if ($obCita): ?>
<?php foreach ($obCita as $var): ?>
	<tr>
		<td><?= $n++ ?></td>
		<td><?= $var->nombreCliente ?></td>
		<td><?= $var->placa ?></td>
		<td><?= formatoHaceEnSinFecha($var->fecha) ?></td>
		<td class="center"><?= obtenerEstado($var->idestado)  ?></td>
		<?php $usuarioObj=Usuario::obtenerUsuario($var->idSac);  ?>
		<td style="background-color: <?= $retVal = ($usuarioObj->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>"><?php  echo $usuarioObj->usuarioNombre ?> <?= $usuarioObj->usuarioApellidoPaterno?></td>
		<td class="center "><button class="btn btn-primary "   data-toggle="modal" data-target="#ordenes" data-id="<?= $var->idcita ?>">Elegir <i class="fa fa-cog"> </i></button></td>
		<td class="center"><strong class="font-bold">
			<?php $usuario=Usuario::obtenerUsuario($var->idcreador);  echo $usuario->usuarioNombre;?>&nbsp;<?= $usuario->usuarioApellidoPaterno; ?> <br><small style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px"><?= haceMinimo($var->fechaRegistro)  ?></small>
		</strong></td>
		</tr>
<?php endforeach ?>	
<?php endif ?>
<?php if ($_POST): ?>
<?php
    include_once("../../includes/init.php");
    $ObjTect=new Tecnicos();
    $idtec=trim($_POST['idtec']);
    $fecha=cambiarPorguion($_POST['fecha']);
    $horaini=($_POST['horaini']);
    $horafin=($_POST['horafin']);
    $ubicacion=($_POST['ubicacion']);
    $idlibera=$_POST['idlibera'];
    $shorario=$_POST['solohorario'];
    $iddistrito=$_POST['ddist'];

    $ObjTect->idtec=$idtec;
    $ObjTect->fecha=$fecha;
    $ObjTect->hora_inicio=$horaini;
    $ObjTect->hora_fin=$horafin;
    $ObjTect->ubicacion=$ubicacion;
    $ObjTect->idliberar=$idlibera;
    $ObjTect->solohorario=$shorario;

    $ObjTect->idciudad=$iddistrito;

    $tt=Tecnicos::verificarsiestarangohora($horaini,$horafin,$fecha,$ubicacion,$idtec,$idlibera);

    $objtec=Tecnicos::obtenerTecnicos($idtec);
    $viewubica=$objtec->ubicacion;

    if($tt->tt!=0){
        echo 'dp';
    }
    else {

        if (strpos($idlibera, "Temp") !== false) {
            $id = $ObjTect->registrarLiberaTecnico();
            $datos = $ObjTect->obternerLiberarTecnicoxliberar($id);
            ?>
            <tr>
                <td class="text-center"><?= $id; ?>
                    <input type="text" style="display: none" name="idliberar" value="<?= $id; ?>">
                </td>
                <td><p class="pfechal<?= $id ?> "><?= $datos->fecha ?></p>
                    <div class='input-group date fechal<?= $id ?>' style="display: none">
                        <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        <input type='text' class='form-control tal tala fecha' id='fechal' name='fechal'
                               autocomplete='off' required value="<?= $datos->fecha ?>">
                    </div>
                </td>
                <td><p class="pdistr<?= $id ?>"><?= $datos->distrito ?></p>
                    <select  style='border: none;display: none ' class='distr<?= $id ?> border-bottom form-control'  id="distr" name="distr">
                        <?php $objDist = Tecnicos::obtenerDistritos(0);?>
                        <?php echo $datos->iddistrito==0?"<option value='0' selected>Seleccionar</option>":"<option value='0'>Seleccionar</option>";  ?>
                        <?php if ($objDist): ?>
                            <?php foreach ($objDist as $var2): ?>
                                <option value="<?= $var2->iddistrito ?>" <?= $var2->iddistrito==$datos->iddistrito?'selected':''; ?> ><?= $var2->distrito ?> (<?= $var2->provincia ?>)</option>
                            <?php endforeach ?>
                        <?php else: ?>
                        <?php endif ?>
                    </select>
                </td>
                <td><p class="phoraini<?= $id ?>"><?= $datos->hora_inicio ?></p>
                    <div class='input-group clockpicker horaini<?= $id ?>' data-autoclose='true' style="display: none">
                        <input type='text' class='form-control' readonly name='horaini' maxlength='5'
                               value='<?= $datos->hora_inicio ?>'>
                        <span class='input-group-addon'>
                                                <span class='fa fa-clock-o'></span>
                                            </span>
                    </div>
                </td>
                <td><p class="phorafin<?= $id ?>"><?= $datos->hora_fin ?></p>
                    <div class='input-group clockpicker horafin<?= $id ?>' data-autoclose='true' style="display: none">
                        <input type='text' class='form-control' readonly name='horafin' maxlength='5'
                               value='<?= $datos->hora_fin ?>'>
                        <span class='input-group-addon'>
                                                <span class='fa fa-clock-o'></span>
                                            </span>
                    </div>
                </td>
                <td><p class="pnubicacion<?= $id ?>"><?= $datos->ubicacion ?></p>
                    <select style='border: none; display: none' class='nubicacion<?= $id ?> border-bottom form-control'
                            name='nubicacion' id='nubicacion'>
                        <?php $objUbica = Tecnicos::obtenerUbicacion() ?>
                        <?php if ($objUbica): ?>
                            <?php foreach ($objUbica as $var): ?>
                                <option value="<?=$var->codigo ?>" <?= $var->codigo == $datos->ubicacion ? 'selected' : '' ?> ><?= $var->ubicacion ?></option>
                            <?php endforeach ?>
                        <?php else: ?>
                        <?php endif ?>
                    </select>
                </td>
                <td><p class="pshorario<?= $id ?>"><?= $datos->solohorario==1?'Si':'No' ?></p>
                    <div class="checkbox checkbox-success shorario<?= $datos->idliberar ?> <?= $datos->solohorario == 1 ? 'checked' : '' ?>" <?=  $datos->ubicacion==$viewubica?"":" checked disabled" ?>  style="display: none">
                        <input id="shorario" type="checkbox" class="shorario<?= $id ?>" name="shorario" <?= $datos->solohorario == 1 ? 'checked' : '' ?> <?=  $datos->ubicacion==$viewubica?"":" checked disabled" ?>
                               value="1" ><label for="shorario<?= $id ?>"></label>
                    </div>
                </td>
                <td>
                    <a class='btn btn-info edit<?= $id ?>' style='padding-top: 0;padding-bottom: 0' href='#' id='editLib'><i class='fa fa-edit'></i></a>
                    <a class='btn btn-success ladda-button  ladda-button-demos<?= $id ?> save<?= $id ?>' data-style="zoom-in" style='padding-top: 0;padding-bottom: 0;display: none'
                       href='#' id='agregaLib'><i class='fa fa-save'></i></a>
                    <a class='btn btn-danger ladda-button  ladda-button-demod<?= $id ?> del<?= $id ?>' data-style="zoom-in" style='padding-top: 0;padding-bottom: 0' href='#'
                       id='eliminarLib'><i class='fa fa-trash-o'></i></a>
                    <a class='btn btn-warning cancel<?= $id ?>' style='padding-top: 0;padding-bottom: 0 ;display: none' href='#' id='cancelLib'><i class='fa fa-ban'></i></a>
                </td>
            </tr>

            <?php
        } else {
            if (strpos($idlibera, "del") !== false) {
                $idlibera = explode("_", $idlibera);
                $ObjTect->idliberar = $idlibera[1];
                $ObjTect->deleteLiberaTecnico();
            } else {
                $ObjTect->updateLiberaTecnico();

                $id = $idlibera;
                $datos = $ObjTect->obternerLiberarTecnicoxliberar($id);
                ?>
                <tr>
                    <td class="text-center"><?= $id; ?>
                        <input type="text" style="display: none" name="idliberar" value="<?= $id; ?>">
                    </td>
                    <td><p class="pfechal<?= $id ?> "><?= $datos->fecha ?></p>
                        <div class='input-group date fechal<?= $id ?>' style="display: none">
                            <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                            <input type='text' class='form-control tal tala fecha' id='fechal' name='fechal'
                                   autocomplete='off' required value="<?= $datos->fecha ?>">
                        </div>
                    </td>
                    <td><p class="pdistr<?= $id ?>"><?= $datos->distrito ?></p>
                        <select  style='border: none;display: none ' class='distr<?= $id ?> border-bottom form-control'  id="distr" name="distr">
                            <?php $objDist = Tecnicos::obtenerDistritos(0);?>
                            <?php echo $datos->iddistrito==0?"<option value='0' selected>Seleccionar</option>":"<option value='0'>Seleccionar</option>";  ?>
                            <?php if ($objDist): ?>
                                <?php foreach ($objDist as $var2): ?>
                                    <option value="<?= $var2->iddistrito ?>" <?= $var2->iddistrito==$datos->iddistrito?'selected':''; ?> ><?= $var2->distrito ?> (<?= $var2->provincia ?>)</option>
                                <?php endforeach ?>
                            <?php else: ?>
                            <?php endif ?>
                        </select>
                    </td>
                    <td><p class="phoraini<?= $id ?>"><?= $datos->hora_inicio ?></p>
                        <div class='input-group clockpicker horaini<?= $id ?>' data-autoclose='true'
                             style="display: none">
                            <input type='text' class='form-control' readonly name='horaini' maxlength='5'
                                   value='<?= $datos->hora_inicio ?>'>
                            <span class='input-group-addon'>
                                                <span class='fa fa-clock-o'></span>
                                            </span>
                        </div>
                    </td>
                    <td><p class="phorafin<?= $id ?>"><?= $datos->hora_fin ?></p>
                        <div class='input-group clockpicker horafin<?= $id ?>' data-autoclose='true'
                             style="display: none">
                            <input type='text' class='form-control' readonly name='horafin' maxlength='5'
                                   value='<?= $datos->hora_fin ?>'>
                            <span class='input-group-addon'>
                                                <span class='fa fa-clock-o'></span>
                                            </span>
                        </div>
                    </td>
                    <td><p class="pnubicacion<?= $id ?>"><?= $datos->ubicacion ?></p>
                        <select style='border: none; display: none'
                                class='nubicacion<?= $id ?> border-bottom form-control' name='nubicacion'
                                id='nubicacion'>
                            <?php $objUbica = Tecnicos::obtenerUbicacion() ?>
                            <?php if ($objUbica): ?>
                                <?php foreach ($objUbica as $var): ?>
                                    <option value="<?=$var->codigo ?>" <?= $var->codigo == $datos->ubicacion ? 'selected' : '' ?> ><?= $var->ubicacion ?></option>
                                <?php endforeach ?>
                            <?php else: ?>
                            <?php endif ?>
                        </select>
                    </td>
                    <td><p class="pshorario<?= $id ?>"><?= $datos->solohorario==1?'Si':'No' ?></p>
                        <div class="checkbox checkbox-success shorario<?= $id ?> <?= $datos->solohorario == 1 ? 'checked' : '' ?>"  <?=  $datos->ubicacion==$viewubica?"":"checked  disabled" ?> style="display: none">
                            <input id="shorario" type="checkbox" class="shorario<?= $id ?>" name="shorario" <?= $datos->solohorario == 1 ? 'checked' : '' ?> <?=  $datos->ubicacion==$viewubica?"":"checked  disabled" ?>
                                   value="1" ><label for="shorario<?= $id ?>"></label>
                        </div>
                    </td>
                    <td>
                        <a class='btn btn-info  edit<?= $id ?>' style='padding-top: 0;padding-bottom: 0' href='#'
                           id='editLib'><i class='fa fa-edit'></i></a>
                        <a class='btn btn-success ladda-button  ladda-button-demos<?= $id ?> save<?= $id ?>' data-style="zoom-in" style='padding-top: 0;padding-bottom: 0;display: none'
                           href='#' id='agregaLib'><i class='fa fa-save'></i></a>
                        <a class='btn btn-danger ladda-button  ladda-button-demod<?= $id ?>  del<?= $id ?>' data-style="zoom-in" style='padding-top: 0;padding-bottom: 0' href='#'
                           id='eliminarLib'><i class='fa fa-trash-o'></i></a>
                        <a class='btn btn-warning cancel<?= $id ?>'
                           style='padding-top: 0;padding-bottom: 0 ;display: none' href='#' id='cancelLib'><i
                                    class='fa fa-ban'></i></a>
                    </td>
                </tr>

                <?php

            }
        }
    }

    ?>
<?php endif ?>

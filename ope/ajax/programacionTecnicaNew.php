<?php if ($_POST): ?>
<?php 
    require_once("../../includes/init.programacion.php");
    $fecha=cambiarPorguion($_POST['fecha']);
    $programObj=new tablaProgramacion;
    $opeObjdetalle=new Operacion;
    $datosT=$programObj->obteneTurnos();
    $n=1;

    $hoy=false;
    $sabado=false;
    $ver=false;
    $du=false;

    if ($fecha==date('Y-m-d')) {
        $hoy=true;
        $hora=new DateTime();
        $hora->modify('+2 hour');
        $hora=$hora->format('Hi');
    }
    $esSabado=new DateTime($fecha);
    $dia=$esSabado->format('D');
    if ($dia=='Sat') {
        $sabado=true;
        $t1='9';
        $t2='11';
        $t3='13';
        $t4='15';
        $tf='17';
        $tt1='09:00';
        $tt2='11:00';
        $ttp='13:00';
        $tt3='13:00';
        $tt4='15:00';
        $ttf='17:00';
    }else{
        $t1='9';
        $t2='11';
        $t3='14';
        $t4='16';
        $tf='18';
        $tt1='09:00';
        $tt2='11:00';
        $ttp='13:00';
        $tt3='14:00';
        $tt4='16:00';
        $ttf='18:00';
    }

?>
<html>
<head>
  <meta charset="UTF-8">
</head>
<body>
<div class="row">
<?php
    $fechaMostrar=new DateTime($fecha);
?>
<h4 class="text-center txt-info" class='margin-top' style='margin-top:0px;margin-bottom:0px'><?= diaPARAMETRO($fechaMostrar->format('l'))." ".$fechaMostrar->format('d')." de ".strtolower(mesDesdeNumero($fechaMostrar->format('m'))) ?> </h4>
<div class="table-responsive">
  <table class="table table-bordered table-hover" >
  <tbody>
  <?php 
    $limiteCita=Operacion::obtenerLimite($fecha,'Citas');
    $citas=$opeObjdetalle->obtenerCitasPordia(soloNumero($fecha));
    
        $limiteDes=Operacion::obtenerLimite($fecha,'Desinstalacion');
        $des=$opeObjdetalle->obtenerOperacionesTotalLimitesDes(soloNumero($fecha));
        if(empty($des)){
            $des=0;
        }

        $limite=Operacion::obtenerLimite($fecha,"Otro");
        $otros=$opeObjdetalle->obtenerOperacionesTotalLimitesOTRO(soloNumero($fecha));
        if($otros==''){
            $otros=0;
        }
  ?>
    <tr>
      <th colspan="2">Citas Taller (<?= $citas ?>/<?= $limiteCita ?>)</th>
      <th colspan="2" class='info'>Desinstalaciones (<?= $des ?>/<?= $limiteDes ?>)</th>  
      <th class='warning'>Otros (<?= $otros ?>/<?= $limite ?>)</th>  
    </tr>
    <tr>
      <th colspan="4"></th>
    </tr>
    <tr>
      <th></th>
      <th><?= $tt1 ?> - <?= $tt2 ?></th>
      <th><?= $tt2 ?> - <?= $ttp ?></th>  
      <th><?= $tt3 ?> - <?= $tt4 ?></th>  
      <th><?= $tt4 ?> - <?= $ttf ?></th>  
    </tr>
<?php
    $n=0;
foreach($datosT as $var){
    $programObj->fecha=$fecha;
    $programObj->hora=$tt1;
    $programObj->idturno=$var->idturno;
    $datot1=$programObj->obtenerTrabajosParaLafechayHoraYespacio();
    $programObj->fecha=$fecha;
    $programObj->hora=$tt2;
    $programObj->idturno=$var->idturno;
    $datot2=$programObj->obtenerTrabajosParaLafechayHoraYespacio();
    $programObj->fecha=$fecha;
    $programObj->hora=$tt3;
    $programObj->idturno=$var->idturno;
    $datot3=$programObj->obtenerTrabajosParaLafechayHoraYespacio();
    $programObj->fecha=$fecha;
    $programObj->hora=$tt4;
    $programObj->idturno=$var->idturno;
    $datot4=$programObj->obtenerTrabajosParaLafechayHoraYespacio();
    $n2=false;
    $n3=false;
    $n4=false;
    $id='0';
    if ($datot1 and $datot2) {
        if ($datot1->idoperacion!='0' AND $datot2->idoperacion!='0') {
            if ($datot1->idoperacion==$datot2->idoperacion){
                $n2=true;
            }
        }
    }
    if ($datot2 and $datot3) {
        if ($datot2->idoperacion!='0' AND $datot3->idoperacion!='0') {
            if ($datot2->idoperacion==$datot3->idoperacion){
                $n3=true;
            }
        }
    }
    if ($datot3 and $datot4) {
        if ($datot3->idoperacion!='0' AND $datot4->idoperacion!='0') {
            if ($datot3->idoperacion==$datot4->idoperacion){
                $n4=true;
            }
        }
    }
?>
<tr>
    <td>
 <div class="row">
 <?php
 if($var->estado==37){
    $n++;
 }
 ?>
 <div class="col-md-6"><?= ($var->estado==37)?$n."|".$var->sede."|":"|".$var->sede."|"?><?= ($var->fijo=='si')?"4":"2"  ?></div><div class='col-md-2'><?= obtenerEstado($var->estado) ?></div><div class="col-md-4"><span class="pull-right"><button class='btn btn-default float-left' data-toggle="modal" data-target="#programacionModal"  data-id="u,<?= $var->idturno ?>">Modificar</button></span></div>
  </div>
    </td>
<!-- 09:00 -->
<?php 
if (!$n2) {
    if ($datot1) {
    if ($datot1->idtecnico!='0') {
        $du=Usuario::obtenerUsuario($datot1->idtecnico);
    }else{
        $du=false;
    }
    if ($datot1->idoperacion<>'0') {
        $dataop=Operacion::obtenerTodolosDatosPoridOperacion($datot1->idoperacion);
        $direccionInstacion=$dataop->direccionInstacion;
    if ($dataop->idChequeo== 0){ 
        $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
        $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial); 
        $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "<b>".$dataop->numeroOrte."</b><br>".strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."  ".$dataop->nombreConcecionarioInstalicion."<br>".$nombreCliente."<br><b>".$datosVehiculo->placa."</b><br>".$datosVehiculo->chasis." ".$datosVehiculo->marca." ".$datosVehiculo->modelo."<br><b>".$datosVehiculo->plan."</b><br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."</b>" ?>"><?= $dataop->distritoi ?> <?= ($datot1->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
        <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>" style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot1->idtablaProgramacion ?>"  ></span>
    </td>           
        <?php
    }else{ 
        $clienteObj=Onix::obtenerDatosDesdeIds($dataop->idcliente,$dataop->idvehiculo,$dataop->idlocation);
        $apellidos=utf8_encode($clienteObj->apellidos);
        $nombreCliente=$apellidos." ".$clienteObj->nombre;
        ?>

<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "<b>".$dataop->numeroOrte."</b><br>".strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."<br>".$nombreCliente."<br><b>".$clienteObj->placa."</b><br>".$clienteObj->chasis." ".$clienteObj->marca." ".$clienteObj->modelo."<br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."<b>" ?>"><?= $dataop->distritoi ?> <?= ($datot1->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
            <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot1->idtablaProgramacion ?>"  ></span>
    </td>       
        <?php

    }
}else if($datot1->idcita<>'0'){
        $datoscita=Cita::obtenerCitaPorId($datot1->idcita);
            ?>
    <td class="primary text-center ">
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "CITA <br>".strtoupper($datoscita->nombreCliente)." ".strtoupper($datoscita->placa)." ".strtoupper($datoscita->plan)."<br><b>".$datoscita->observacion."</b>" ?>">SURQUILLO <?= ($datot1->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
            <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot1->idtablaProgramacion ?>"  ></span>
    </td>
            <?php
}else{
    ?>
        <td class="primary  text-center ">
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "RESERVADO <br>".$datot1->observacion."</b>" ?>">RESERVADO</p>
            <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot1->idtablaProgramacion ?>"  ></span>
    </td>
    <?php
}
    ?>
        <?php
    }else{
        ?>
<?php
    if (!$ver) {
       ?>
<td class="success text-center">
     <span class="fa fa-calendar" style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="p,<?= $t1.",".$var->idturno ?>"  ></span>
</td>
       <?php 
    }else{
        ?>
<td class="success text-center">
    <?= ($hoy AND $hora > soloNumero($tt1)) ? 'Fuera de hora':'' ?>
</td>
        <?php
    }
?>
        <?php
    }
}else{
    if ($datot1->idtecnico!='0') {
        $du=Usuario::obtenerUsuario($datot1->idtecnico);
    }else{
        $du=false;
    }
        if ($datot1->idoperacion<>'0') {
        $dataop=Operacion::obtenerTodolosDatosPoridOperacion($datot1->idoperacion);
        $direccionInstacion=$dataop->direccionInstacion;
    if ($dataop->idlocation == 0){ 
        $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
        $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial); 
        $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
        ?>
 <td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center" colspan='2'>
        <?php if ($du) { ?>
            <small class="text-center"><?= $du->usuarioNombre  ?></small>       
        <br>
        <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "<b>".$dataop->numeroOrte."</b><br>".strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."  ".$dataop->nombreConcecionarioInstalicion."<br>".$nombreCliente."<br><b>".$datosVehiculo->placa."</b><br>".$datosVehiculo->chasis." ".$datosVehiculo->marca." ".$datosVehiculo->modelo."<br><b>".$datosVehiculo->plan."</b><br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."</b>" ?>"><?= $dataop->distritoi ?> <?= ($datot1->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
     <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>" style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot1->idtablaProgramacion ?>" ></span>
    </td>
        <?php
    }else{ 
        $clienteObj=Onix::obtenerDatosDesdeIds($dataop->idcliente,$dataop->idvehiculo,$dataop->idlocation);
        $apellidos=utf8_encode($clienteObj->apellidos);
        $nombreCliente=$apellidos." ".$clienteObj->nombre;
        ?>
 <td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center" colspan='2'>
        <?php if ($du) { ?>
            <small class="text-center"><?= $du->usuarioNombre  ?></small>       
        <br>
        <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "<b>".$dataop->numeroOrte."</b><br>".strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."  ".$dataop->nombreConcecionarioInstalicion."<br>".$nombreCliente."<br><b>".$datosVehiculo->placa."</b><br>".$datosVehiculo->chasis." ".$datosVehiculo->marca." ".$datosVehiculo->modelo."<br><b>".$datosVehiculo->plan."</b><br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."</b>" ?>"><?= $dataop->distritoi ?> <?= ($datot1->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
     <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>" style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot1->idtablaProgramacion ?>" ></span>
    </td>       
        <?php
    }
    }else if($datot1->idcita<>'0'){
        $datoscita=Cita::obtenerCitaPorId($datot1->idcita);
        ?>
<td class="primary text-center " colspan='2'>
        <?php if ($du) { ?>
            <small class="text-center"><?= $du->usuarioNombre  ?></small>       
        <br>
        <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "CITA <br>".strtoupper($datoscita->nombreCliente)." ".strtoupper($datoscita->placa)." ".strtoupper($datoscita->plan)."<br><b>".$datoscita->observacion."</b>" ?>">SURQUILLO <?= ($datot1->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
     <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>" style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot1->idtablaProgramacion ?>" ></span>
    </td>      
        <?php
    }else{
        ?>
<td class="primary text-center ">
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "RESERVADO <br>".$datot1->observacion."</b>" ?>">RESERVADO</p>
            <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot1->idtablaProgramacion ?>"  ></span>
    </td>
        <?php
    }
}
?>
<!-- 11:13 -->
<?php 
if (!$n2) {
    if (!$n3) {
    if ($datot2) {
    if ($datot2->idtecnico!='0') {
        $du=Usuario::obtenerUsuario($datot2->idtecnico);
    }else{
        $du=false;
    }
    if ($datot2->idoperacion<>'0') {
        $dataop=Operacion::obtenerTodolosDatosPoridOperacion($datot2->idoperacion);
        $direccionInstacion=$dataop->direccionInstacion;
    if ($dataop->idlocation == 0){ 
        $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
        $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial); 
        $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "<b>".$dataop->numeroOrte."</b><br>".strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."  ".$dataop->nombreConcecionarioInstalicion."<br>".$nombreCliente."<br><b>".$datosVehiculo->placa."</b><br>".$datosVehiculo->chasis." ".$datosVehiculo->marca." ".$datosVehiculo->modelo."<br><b>".$datosVehiculo->plan."</b><br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."</b>" ?>"><?= $dataop->distritoi ?> <?= ($datot2->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
    <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot2->idtablaProgramacion ?>"  ></span>
    </td>      
        <?php
    }else{ 
        $clienteObj=Onix::obtenerDatosDesdeIds($dataop->idcliente,$dataop->idvehiculo,$dataop->idlocation);
        $apellidos=utf8_encode($clienteObj->apellidos);
        $nombreCliente=$apellidos." ".$clienteObj->nombre;
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "<b>".$dataop->numeroOrte."</b><br>".strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."<br>".$nombreCliente."<br><b>".$clienteObj->placa."</b><br>".$clienteObj->chasis." ".$clienteObj->marca." ".$clienteObj->modelo."<br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."<b>" ?>"><?= $dataop->distritoi ?> <?= ($datot2->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
    <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot2->idtablaProgramacion ?>"  ></span>
    </td>
        <?php
    }
        }else if($datot2->idcita<>'0'){
            $datoscita=Cita::obtenerCitaPorId($datot2->idcita);
            ?>
    <td class="primary text-center ">
<?php if ($du) { ?>
    <small class="text-center"><?= $du->usuarioNombre  ?></small>       
<br>
<?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "CITA <br>".strtoupper($datoscita->nombreCliente)." ".strtoupper($datoscita->placa)." ".strtoupper($datoscita->plan)."<br><b>".$datoscita->observacion."</b>" ?>">SURQUILLO <?= ($datot2->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
    <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot2->idtablaProgramacion ?>"  ></span>
    </td>
            <?php
        }else{
            ?>
<td class="primary text-center ">
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "RESERVADO <br>".$datot2->observacion."</b>" ?>">RESERVADO</p>
            <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot2->idtablaProgramacion ?>"  ></span>
    </td>
            <?php
        }
    ?>
        <?php
    }else{
        ?>
<?php
    if (!$ver) {
       ?>
<td class="success text-center" >
     <span class="fa fa-calendar" style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="p,<?= $t2.",".$var->idturno ?>"  ></span>
</td>
       <?php 
    }else{
        ?>
<td class="success text-center">
    <?= ($hoy AND $hora > soloNumero($tt2)) ? 'Fuera de hora':'' ?>
</td>
        <?php
    }
?>
        <?php
    }
    }else{

    if ($datot2->idtecnico!='0') {
        $du=Usuario::obtenerUsuario($datot2->idtecnico);
    }else{
        $du=false;
    }
        if ($datot2->idoperacion<>'0') {
        $dataop=Operacion::obtenerTodolosDatosPoridOperacion($datot2->idoperacion);
        $direccionInstacion=$dataop->direccionInstacion;
    if ($dataop->idlocation == 0){ 
        $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
        $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial); 
        $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center" colspan='2'>
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "<b>".$dataop->numeroOrte."</b><br>".strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."  ".$dataop->nombreConcecionarioInstalicion."<br>".$nombreCliente."<br><b>".$datosVehiculo->placa."</b><br>".$datosVehiculo->chasis." ".$datosVehiculo->marca." ".$datosVehiculo->modelo."<br><b>".$datosVehiculo->plan."</b><br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."</b>" ?>"><?= $dataop->distritoi ?> <?= ($datot2->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
        <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>" style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot2->idtablaProgramacion ?>"  ></span>
    </td>           
        <?php
    }else{ 
        $clienteObj=Onix::obtenerDatosDesdeIds($dataop->idcliente,$dataop->idvehiculo,$dataop->idlocation);
        $apellidos=utf8_encode($clienteObj->apellidos);
        $nombreCliente=$apellidos." ".$clienteObj->nombre;
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center" colspan='2'>
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "<b>".$dataop->numeroOrte."</b><br>".strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."<br>".$nombreCliente."<br><b>".$clienteObj->placa."</b><br>".$clienteObj->chasis." ".$clienteObj->marca." ".$clienteObj->modelo."<br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."<b>" ?>"><?= $dataop->distritoi ?> <?= ($datot2->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
            <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot2->idtablaProgramacion ?>"  ></span>
    </td>       
        <?php
    }
        }else if($datot2->idcita<>'0'){
            $datoscita=Cita::obtenerCitaPorId($datot1->idcita);
        }else{
            ?>
            <td class="<?= (!$du)?'warning':'danger' ?> text-center ">
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "RESERVADO <br>".$datot2->observacion."</b>" ?>">RESERVADO</p>
            <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot2->idtablaProgramacion ?>"  ></span>
    </td>
            <?php
        }
    ?>
        <?php
}
}
        ?>
<!-- 14:16 -->
<?php 
if (!$n3) {
    if (!$n4) {
    if ($datot3) {

    if ($datot3->idtecnico!='0') {
        $du=Usuario::obtenerUsuario($datot3->idtecnico);
    }else{
        $du=false;
    }
    if ($datot3->idoperacion<>'0') {
    $dataop=Operacion::obtenerTodolosDatosPoridOperacion($datot3->idoperacion);
        $direccionInstacion=$dataop->direccionInstacion;
    if ($dataop->idlocation == 0){ 
        $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
        $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial); 
        $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "<b>".$dataop->numeroOrte."</b><br>".strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."  ".$dataop->nombreConcecionarioInstalicion."<br>".$nombreCliente."<br><b>".$datosVehiculo->placa."</b><br>".$datosVehiculo->chasis." ".$datosVehiculo->marca." ".$datosVehiculo->modelo."<br><b>".$datosVehiculo->plan."</b><br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."</b>" ?>"><?= $dataop->distritoi ?> <?= ($datot3->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
<span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>" style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot3->idtablaProgramacion ?>"  ></span>
    </td>           
        <?php
    }else{ 
        $clienteObj=Onix::obtenerDatosDesdeIds($dataop->idcliente,$dataop->idvehiculo,$dataop->idlocation);
        $apellidos=utf8_encode($clienteObj->apellidos);
        $nombreCliente=$apellidos." ".$clienteObj->nombre;
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "<b>".$dataop->numeroOrte."</b><br>".strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."<br>".$nombreCliente."<br><b>".$clienteObj->placa."</b><br>".$clienteObj->chasis." ".$clienteObj->marca." ".$clienteObj->modelo."<br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."<b>" ?>"><?= $dataop->distritoi ?> <?= ($datot3->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
<span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>" style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot3->idtablaProgramacion ?>"  ></span>
    </td>       
        <?php
    }
        }else if($datot3->idcita<>'0'){
            $datoscita=Cita::obtenerCitaPorId($datot3->idcita);
            ?>
<td class="primary text-center ">
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "CITA <br>".strtoupper($datoscita->nombreCliente)." ".strtoupper($datoscita->placa)." ".strtoupper($datoscita->plan)."<br><b>".$datoscita->observacion."</b>" ?>">SURQUILLO <?= ($datot3->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
<span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>" style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot3->idtablaProgramacion ?>"  ></span>
    </td>
            <?php
        }else{
            ?>
<td class="primary text-center ">
        <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "RESERVADO <br>".$datot3->observacion."</b>" ?>">RESERVADO</p>
            <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot3->idtablaProgramacion ?>"  ></span>
    </td>
            <?php
        }
    ?>
      
        <?php
    }else{
        ?>
<?php
    if (!$ver) {
       ?>
<td class="success text-center" >
     <span class="fa fa-calendar" style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="p,<?= $t3.",".$var->idturno ?>"  ></span>
</td>
       <?php 
    }else{
        ?>
<td class="success text-center">
    <?= ($hoy AND $hora > soloNumero($tt3)) ? 'Fuera de hora':'' ?>
</td>
        <?php
    }
?>
        <?php
    }
    }else{
    if ($datot3->idtecnico!='0') {
        $du=Usuario::obtenerUsuario($datot3->idtecnico);
    }else{
        $du=false;
    }
    if ($datot3->observacion!='') {
        $observacion=$datot3->observacion."";
    }else{
        $observacion='';
    }
        if ($datot3->idoperacion<>'0') {
        $dataop=Operacion::obtenerTodolosDatosPoridOperacion($datot3->idoperacion);
        $direccionInstacion=$dataop->direccionInstacion;
    if ($dataop->idChequeo == 0){ 
        $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
        $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial); 
        $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
        ?>
 <td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center" colspan='2'>
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "<b>".$dataop->numeroOrte."</b><br>".strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."  ".$dataop->nombreConcecionarioInstalicion."<br>".$nombreCliente."<br><b>".$datosVehiculo->placa."</b><br>".$datosVehiculo->chasis." ".$datosVehiculo->marca." ".$datosVehiculo->modelo."<br><b>".$datosVehiculo->plan."</b><br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."</b>" ?>"><?= $dataop->distritoi ?> <?= ($datot3->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
     <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot3->idtablaProgramacion ?>"  ></span>
    </td>           
        <?php
    }else{ 
        $clienteObj=Onix::obtenerDatosDesdeIds($dataop->idcliente,$dataop->idvehiculo,$dataop->idlocation);
        $apellidos=utf8_encode($clienteObj->apellidos);
        $nombreCliente=$apellidos." ".$clienteObj->nombre;
        ?>
 <td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center" colspan='2'>
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "<b>".$dataop->numeroOrte."</b><br>".strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."<br>".$nombreCliente."<br><b>".$clienteObj->placa."</b><br>".$clienteObj->chasis." ".$clienteObj->marca." ".$clienteObj->modelo."<br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."<b>" ?>"><?= $dataop->distritoi ?> <?= ($datot3->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
     <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot3->idtablaProgramacion ?>"  ></span>
    </td>       
        <?php
    }
        }else if($datot3->idcita<>'0'){
            $datoscita=Cita::obtenerCitaPorId($datot3->idcita);
        }else{
            ?>
<td class="primary text-center ">
    <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "RESERVADO <br>".$datot3->observacion."</b>" ?>">RESERVADO</p>
            <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot3->idtablaProgramacion ?>"  ></span>
    </td>
            <?php
        }
    ?>

        <?php
}
}
        ?>
<!-- 16:18 -->
<?php 
if (!$n4) {
if ($datot4) {
    if ($datot4->idtecnico!='0') {
        $du=Usuario::obtenerUsuario($datot4->idtecnico);
    }else{
        $du=false;
    }
    if ($datot4->observacion!='') {
        $observacion=$datot4->observacion."";
    }else{
        $observacion='';
    }

    if ($datot4->idoperacion<>'0') {
        $dataop=Operacion::obtenerTodolosDatosPoridOperacion($datot4->idoperacion);
        $direccionInstacion=$dataop->direccionInstacion;
    if ($dataop->idChequeo == 0){ 
        $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
        $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial); 
        $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "<b>".$dataop->numeroOrte."</b><br>".strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."  ".$dataop->nombreConcecionarioInstalicion."<br>".$nombreCliente."<br><b>".$datosVehiculo->placa."</b><br>".$datosVehiculo->chasis." ".$datosVehiculo->marca." ".$datosVehiculo->modelo."<br><b>".$datosVehiculo->plan."</b><br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."</b>" ?>"><?= $dataop->distritoi ?> <?= ($datot4->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
     <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot4->idtablaProgramacion ?>"  ></span>
    </td>
<?php
    }else{ 
        $clienteObj=Onix::obtenerDatosDesdeIds($dataop->idcliente,$dataop->idvehiculo,$dataop->idlocation);
        $apellidos=utf8_encode($clienteObj->apellidos);
        $nombreCliente=$apellidos." ".$clienteObj->nombre;
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "<b>".$dataop->numeroOrte."</b><br>".strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."<br>".$nombreCliente."<br><b>".$clienteObj->placa."</b><br>".$clienteObj->chasis." ".$clienteObj->marca." ".$clienteObj->modelo."<br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."<b>" ?>"><?= $dataop->distritoi ?><?= ($datot4->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
    <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot4->idtablaProgramacion ?>"  ></span>
    </td>       
        <?php
    }
        }else if($datot4->idcita<>'0'){
            $datoscita=Cita::obtenerCitaPorId($datot4->idcita);
            ?>
    <td class="primary text-center ">
            <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "CITA <br>".strtoupper($datoscita->nombreCliente)." ".strtoupper($datoscita->placa)." ".strtoupper($datoscita->plan)."<br><b>".$datoscita->observacion."</b>" ?>">SURQUILLO <?= ($datot4->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
            <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot1->idtablaProgramacion ?>"  ></span>
    </td>
            <?php
        }else{
            ?>
<td class="primary text-center ">
    <?php if ($du) { ?>
                <small class="text-center"><?= $du->usuarioNombre  ?></small>       
            <br>
            <?php } ?>
<p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "RESERVADO <br>".$datot4->observacion."</b>" ?>">RESERVADO</p>
            <span class="fa fa-<?= (!$du) ? 'edit':'eye' ?>"  style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="c,<?= $datot4->idtablaProgramacion ?>"  ></span>
    </td>
            <?php
        }
    ?>
      
        <?php
}else{
    if (!$ver) {
       ?>
<td class="success text-center">
     <span class="fa fa-calendar" style='cursor: pointer;' data-toggle="modal" data-target="#programacionModal"  data-id="p,<?= $t4.",".$var->idturno ?>"  ></span>
</td>
 </td>
       <?php 
    }else{
        ?>
<td class="success text-center">
    <?= ($hoy AND $hora > soloNumero($tt4)) ? 'Fuera de hora':'' ?>
</td>
        <?php
    }
    ?>
    <?php
}
        ?>
   </tr>
    	<?php
    	} 
?>


<?php
} 
?>
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>

  </tbody>
	</table>
</div>
</div>
</body>
</html>
       <?php 

?>
<?php endif ?>

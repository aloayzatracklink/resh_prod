
<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $fecha='';
  $estado=htmlspecialchars($_POST['convenio']);
  if (isset($_POST["fecha"])) {
    $fecha=strip_tags($_POST['fecha']);
  $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
    $desde=date("YmdHis",strtotime($desde));  
    $hasta=date("Ymd",strtotime($hasta)); 
    $hasta=$hasta."235959";
  }
  $operacionesObj=new Operacion();
  if ($estado=='76') {
    $cantidadO=$operacionesObj->datosParaInstalacionSinTecnicoCantidad();    
    $registrosPorPagina=10;
    $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
    $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
    $datos=$operacionesObj->datosParaInstalacionSinTecnico($posisionDePagina,$registrosPorPagina);    
 
  }else if($estado=='99'){
    $cantidadO=$operacionesObj->datosParaInstalacionPendeiten();    
    $registrosPorPagina=10;
    $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
    $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
    $datos=$operacionesObj->datosParaPendiente($posisionDePagina,$registrosPorPagina);     
  }else{
    $operacionesObj->estado=limpiar($estado);
    if (empty($fecha)){    
      $cantidadO=$operacionesObj->obtenerDatosParaTallerCantidadTotal();    
    }else{
      // $cantidadO=$operacionesObj->obtenerComisionesConveniosRANGOCantidad($desde,$hasta);
    }
    
    $registrosPorPagina=10;
    $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
    $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
    
    if (empty($fecha)) {
      $datos=$operacionesObj->obtenerDatosParaTallerP($posisionDePagina,$registrosPorPagina);    
    }else {
      // $datos=$operacionesObj->obtenerComisionesConveniosRANGO($posisionDePagina,$registrosPorPagina,$desde,$hasta);        
    } 

  } 

?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>    
    <?php if ($var->idChequeo==0): ?>
    <?php $usuarioOPE=Usuario::obtenerUsuario($var->idConfirmacionOPE)  ?>
    <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>
      <?php else: ?>
    <?php $usuarioOPE=Usuario::obtenerUsuario($var->idCreador)  ?>
      <?php $clienteObj=Onix::obtenerDatosDesdeIds($var->idcliente,$var->idvehiculo,$var->idlocation) ?>
    <?php endif ?>
     <!-- bg-warning success-->
    <tr  <?= obtenerClass($var->lugarInstalacion) ?>>
          <td  ><?= !empty($var->numeroOrte)?$var->numeroOrte:"Pendiente" ?></td>       
          <td><?= $var->lugarInstalacion ?></td>
          <td><?= $var->trabajo ?></td>
    <?php if ($var->idChequeo==0): ?>
         <td><?= ($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial) ; ?></td>
       <?php else: ?>
          <?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
        <td><?= $retVal = (!empty($clienteObj->nombre)) ? "$apellidos"." "."$clienteObj->nombre" : "$apellidos" ; ?></td>          
        <?php endif ?>
          <td><?= formatoHaceEnSinFecha($var->fechaInstalacion) ?></td>
          <td><?= $usuarioOPE->usuarioNombre." ".$usuarioOPE->usuarioApellidoPaterno ?></td>
          <td><?= obtenerEstado($var->estado) ?></td>  
          <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" data-id="<?php echo $var->idoperacion ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;</td>                    
    </tr>
<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="8">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="78" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>

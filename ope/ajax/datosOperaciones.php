<?php 
	require_once("../../includes/init.php");
	$n=1;
?>
<?php $objInstalar=Operacion::obtenerListosParaInstalacion() ?>
<?php if ($objInstalar): ?>
<?php foreach ($objInstalar as $var): ?>

        <?php $datosTrama = TramaIntegracionTY::obtenerTramaIntegracionPorIdOperacion($var->idoperacion);  ?>
        <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>
        <?php $datosF=DatosFacturacion::obtenerDatosFacturacionPorId($var->datosfacturacionid) ?>
        <?php 
			if(isset($datosF->idUsuarioCreado)){
				$usuarioV=Usuario::obtenerUsuario($datosF->idUsuarioCreado); 
			}else{
				$usuarioV = Usuario::obtenerUsuario(0);
			}
		?>
	<tr <?= obtenerClass($var->lugarInstalacion) ?>   style="<?= $datosVehiculo->campana=="Toyota"?"border: 2px solid red !important":"" ?>">
		<td><?= $n++ ?></td>
		<td><?= $var->trabajo ?></td>
		<td><?= formatoHaceEnSinFecha($var->fechaInstalacion) ?></td>
		<td><?=  !empty($var->lugarInstalacion)? $var->lugarInstalacion: 'SAC' ?></td>
		<?php if ($var->idlocation == 0): ?> 			
		<td><?php $clienteObj =Cliente::obtenerDatosDeclientePorId($var->idcliente);?> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial) ; ?></td>
		<td><?php $vehiculoObj = Vehiculo::obtenerVehiculo($var->idvehiculo); echo $vehiculoObj->placa ?></td>
		<td><?php 
		//taller-chasis-busc-mod-->
    		    $vehiculoObj = Vehiculo::obtenerVehiculo($var->idvehiculo); 
    		    if (!empty($vehiculoObj->chasis)) {
    		        echo $vehiculoObj->chasis;
    		    } else {
    		        echo ""; 
    		    }
    		?></td>

		<?php else: ?>
		<?php $clienteObj=Onix::obtenerDatosDesdeIds($var->idcliente,$var->idvehiculo,$var->idlocation) ?>
			<?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
		<td><?= $retVal = (!empty($clienteObj->nombre)) ? "$apellidos"." "."$clienteObj->nombre" : "$apellidos" ; ?></td>
		<td><?= $clienteObj->placa ?></td>
		<td><?php 
		//taller-chasis-busc-mod-->
    		    if (!empty($clienteObj->chasis)) {
    		        echo $clienteObj->chasis;
    		    } else {
    		        echo ""; 
    		    }
    		?></td>
		<?php endif ?>

        <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosVehiculo->campana</span>":$datosVehiculo->campana) ?></td>
        <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosTrama->tipo_transaccion</span>":'') ?></td>
		<?php $usuarioObj=Usuario::obtenerUsuario($var->idUsuarioCargo);  ?>
		<td style="background-color: <?php echo $retVal = ($usuarioObj->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>"><?php  echo $usuarioObj->usuarioNombre ?> <?php echo $usuarioObj->usuarioApellidoPaterno?></td>
        <td class="center"><?= $usuarioV->usuarioNombre;?>&nbsp;<?= $usuarioV->usuarioApellidoPaterno;  ?></td>
        <td class="center "><button class="btn btn-primary "   data-toggle="modal" data-target="#ordenes" data-id="<?php echo $var->idoperacion ?>">Elegir <i class="fa fa-cog"> </i></button></td>
		<td class="center"><strong class="font-bold">
			<?php $usuario=Usuario::obtenerUsuario($var->idCreador);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno; ?> <br><small style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px"><?php echo formato($var->fechaCreacion)  ?></small>
		</strong></td>
		</tr>
<?php endforeach ?>
	
	<?php endif ?>

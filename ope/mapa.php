<?php 
if (isset($_GET['coordenadas'])) {


 ?>
 <!DOCTYPE html>
 <html>
 <head>
  <meta charset="utf-8">
  <title>Ubicación</title>
  <style>
    html, body {
      height: 100%;
      margin: 0;
      padding: 0;
    }
    #map {
      height: 100%;
    }
  </style>
</head>
<body>
  <div id="map"></div>
  <script>
var marker;

function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    center: new google.maps.LatLng(<?php echo $_GET['coordenadas'] ?>)
  });

  marker = new google.maps.Marker({
    map: map,
    draggable: true,
    animation: google.maps.Animation.DROP,
    position: new google.maps.LatLng(<?php echo $_GET['coordenadas'] ?>)
  });
  marker.addListener('click', toggleBounce);
  var infowindow = new google.maps.InfoWindow({
    content:"<?php echo $_GET['lugar'] ?>"
  });

  infowindow.open(map,marker);

}

function toggleBounce() {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}
</script>
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDQ8AtI752JiFZ4o_Cc3mefql-a7I4eHM&callback=initMap"></script>
 <?php include_once('../includes/script.php') ?>
</body>
</body>
</html>
<?php }else{
  echo "verifique el link ";
} ?>
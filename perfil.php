<?php $nav="perfil" ;?>
<?php include("includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("ingresar");
} ?>

<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
               <?php include("includes/menuIzquierdo.php") ?>
            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <?php include("includes/navegacionArriba.php") ?>

        </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                 <div class="wrapper wrapper-content animated fadeInRight">


            <div class="row m-b-lg m-t-lg">
                <div class="col-md-6">

                    <div class="profile-image">
                            <img alt="public/perfil/<?php echo  $retVal = (isset($o->fotoPerfil) && $o->fotoPerfil != "") ? "$o->fotoPerfil" : "pefilpordefecto.png" ; ?>" class="img-circle" src="public/perfil/<?php echo  $retVal = (isset($o->fotoPerfil) && $o->fotoPerfil != "") ? "$o->fotoPerfil" : "pefilpordefecto.png" ; ?>" width="60" />
                    </div>
                    <div class="profile-info">
                        <div class="">
                            <div>
                                <h2 class="no-margins">
                                   <?php $usuario=Usuario::obtenerUsuario($session->idusuario);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno; ?> <?php echo $usuario->usuarioApellidoMaterno; ?>
                                </h2>
                                <h4><?php $usuarioR=Usuario::obtenerRoldeUsuario($usuario->idrol);  echo $usuarioR->cargo;?></h4>
                                <small>
                                  <?php echo $usuarioR->descripcion ?>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <table class="table small m-b-xs">
                        <tbody>
                        <tr>
                            <td>
                                <strong>142</strong> Baterias
                            </td>
                            <td>
                                <strong>22</strong> Chequeos
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <strong>61</strong> Robos
                            </td>
                            <td>
                                <strong>54</strong> Choques
                            </td>
                        </tr>
                      <!--   <tr>
                            <td>
                                <strong>154</strong> Choques
                            </td>
                            <td>
                                <strong>32</strong> Friends
                            </td>
                        </tr> -->
                        </tbody>
                    </table>
                </div>
                <div class="col-md-3">
                    <small>Ventas en las últimas 24h</small>
                    <h2 class="no-margins">206</h2>
                    <div id="sparkline1"></div>
                </div>


            </div>
            <div class="row">

                <div class="col-lg-12">

                    <div class="ibox">
                        <div class="ibox-content">
                                <h3>Acerca de <?php $usuario=Usuario::obtenerUsuario($session->idusuario);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno; ?></h3>

                            <p class="small">
                                <?php echo $usuario->sobreMi ?>
                            </p>

                            <p class="small font-bold">
                                <span><i class="fa fa-circle text-navy"></i> Estado online </span>
                                </p>

                        </div>
                    </div>

                    <div class="ibox">
                        <div class="ibox-content">
                            <h3>Compañeros de area</h3>
                            <p class="small">
                                Area de <?php echo $usuarioR->area ?>
                            </p>
                            <div class="user-friends">
                                <a href=""><img alt="image" class="img-circle" src="assets/img/a3.jpg"></a>
                                <a href=""><img alt="image" class="img-circle" src="assets/img/a1.jpg"></a>
                                <a href=""><img alt="image" class="img-circle" src="assets/img/a2.jpg"></a>
                                <a href=""><img alt="image" class="img-circle" src="assets/img/a4.jpg"></a>
                                <a href=""><img alt="image" class="img-circle" src="assets/img/a5.jpg"></a>
                                <a href=""><img alt="image" class="img-circle" src="assets/img/a6.jpg"></a>
                                <a href=""><img alt="image" class="img-circle" src="assets/img/a7.jpg"></a>
                                <a href=""><img alt="image" class="img-circle" src="assets/img/a8.jpg"></a>
                                <a href=""><img alt="image" class="img-circle" src="assets/img/a2.jpg"></a>
                                <a href=""><img alt="image" class="img-circle" src="assets/img/a1.jpg"></a>
                            </div>
                        </div>
                    </div>

                    <div class="ibox">
                        <div class="ibox-content">
                            <h3>Mensaje privado</h3>

                            <p class="small">                               
                                Enviale un mensaje privado a <?php $usuario=Usuario::obtenerUsuario($session->idusuario);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno; ?>
                            </p>

                            <div class="form-group">
                                <label>Asunto</label>
                                <input type="email" class="form-control" placeholder="Asunto del mensaje">
                            </div>
                            <div class="form-group">
                                <label>Mensaje</label>
                                <textarea class="form-control" placeholder="Tu mensaje" rows="3"></textarea>
                            </div>
                            <button class="btn btn-primary btn-block">Enviar</button>

                        </div>
                    </div>

                </div>

               
              <!--   <div class="col-lg-6 m-b-lg">
                    <div id="vertical-timeline" class="vertical-container light-timeline no-margins">
                        <div class="vertical-timeline-block">
                            <div class="vertical-timeline-icon navy-bg">
                                <i class="fa fa-briefcase"></i>
                            </div>

                            <div class="vertical-timeline-content">
                                <h2>Meeting</h2>
                                <p>Conference on the sales results for the previous year. Monica please examine sales trends in marketing and products. Below please find the current status of the sale.
                                </p>
                                <a href="#" class="btn btn-sm btn-primary"> More info</a>
                                    <span class="vertical-date">
                                        Today <br>
                                        <small>Dec 24</small>
                                    </span>
                            </div>
                        </div>

                        <div class="vertical-timeline-block">
                            <div class="vertical-timeline-icon blue-bg">
                                <i class="fa fa-file-text"></i>
                            </div>

                            <div class="vertical-timeline-content">
                                <h2>Send documents to Mike</h2>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since.</p>
                                <a href="#" class="btn btn-sm btn-success"> Download document </a>
                                    <span class="vertical-date">
                                        Today <br>
                                        <small>Dec 24</small>
                                    </span>
                            </div>
                        </div>

                        <div class="vertical-timeline-block">
                            <div class="vertical-timeline-icon lazur-bg">
                                <i class="fa fa-coffee"></i>
                            </div>

                            <div class="vertical-timeline-content">
                                <h2>Coffee Break</h2>
                                <p>Go to shop and find some products. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's. </p>
                                <a href="#" class="btn btn-sm btn-info">Read more</a>
                                <span class="vertical-date"> Yesterday <br><small>Dec 23</small></span>
                            </div>
                        </div>

                        <div class="vertical-timeline-block">
                            <div class="vertical-timeline-icon yellow-bg">
                                <i class="fa fa-phone"></i>
                            </div>

                            <div class="vertical-timeline-content">
                                <h2>Phone with Jeronimo</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>
                                <span class="vertical-date">Yesterday <br><small>Dec 23</small></span>
                            </div>
                        </div>

                        <div class="vertical-timeline-block">
                            <div class="vertical-timeline-icon navy-bg">
                                <i class="fa fa-comments"></i>
                            </div>

                            <div class="vertical-timeline-content">
                                <h2>Chat with Monica and Sandra</h2>
                                <p>Web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). </p>
                                <span class="vertical-date">Yesterday <br><small>Dec 23</small></span>
                            </div>
                        </div>
                    </div>

            </div> -->

        </div>
              <?php include("includes/footer.php") ?>
            </div>
        </div>

        </div>

    <script src="assets/js/jquery-2.1.1.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="assets/js/inspinia.js"></script>
    <script src="assets/js/plugins/pace/pace.min.js"></script>
    <script src="assets/js/plugins/footable/footable.all.min.js"></script>
 <?php include_once('includes/script.php') ?>
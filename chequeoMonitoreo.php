<?php $nav="chequeoMonitoreo" ;?>
<?php include("includes/head.php") ?>
<link rel="stylesheet" type="text/css" href="assets/calendario/dist/sweetalert.css">
<?php if (!$session->yaEstaIngresado()){ 
  irA("ingresar");
} ?>
<?php 
$obMenu=Usuario::obtenerMenu($session->idusuario) ?>

<?php if ($obMenu->chequeoMonitoreo=="si"): ?> 

<?php 
$errores=array();
$echo=array();
$chequeoObj=new Chequeos;
    if (isset($_POST['crearChequeoMonitoreo'])) {
        $chequeoObj->plan=$_POST['plan'];
        $chequeoObj->dniRuc=$_POST['dniORuc'];
        $chequeoObj->nombres=$_POST['nombreCliente'];
        $chequeoObj->placa=$_POST['placaVehiculo'];
        $chequeoObj->chasis=$_POST['chasis'];
        $chequeoObj->marcaModelo=$_POST['marca']." ".$_POST['modelo'];
        $chequeoObj->correo2=$_POST['correoCliente'];
        $chequeoObj->motivo=$_POST['motivo'];
        $chequeoObj->personaContacto=$_POST['personaContacto'];
        $chequeoObj->numeroContacto=$_POST['numeroContacto'];
        $chequeoObj->celular=$_POST['celular'];
        $chequeoObj->telefono=$_POST['telefono5'];
        $chequeoObj->correo=$_POST['correoCliente'];
        $chequeoObj->idcustomer=$_POST['idcustomer'];
        $chequeoObj->idvehicle=$_POST['idvehicle'];
        $chequeoObj->idlocation=$_POST['idlocation'];
        $chequeoObj->descripcionOrden=$_POST['ordenDesc'];
        if (isset($_POST['numeroOrden'])) {
            $chequeoObj->numeroOrden=$_POST['numeroOrden'];
        }
        $fecha= str_replace("/", "", $_POST['fecha']);
        $hora=str_replace(":", "", $_POST['hora']);
        $chequeoObj->fechaHoraChequeoLinea=$fecha.$hora;
        if (!isset($_POST['numeroOrden'])) {
            if ($_POST['chasis']=='') {
                $errores[]="no se registro chequeo por falta de numero de chasis";
            }
            $fecha=$_POST['fecha'].$_POST['hora'];
            $dataProgramacion=Chequeos::obtenerProgramacionChequeo($fecha);
            if (count($dataProgramacion)>='4') {
                $errores[]="Existen 5 chqueos programados para esa misma hora intenta programar con otra hora o fecha";
            }
             if($da=$chequeoObj->validarExistenciaPendienteChasis(limpiar(trim($_POST['chasis']))))
                {
                    $errores[]="El chasis ingresado ya esta en gestión";            
                }       
            if (empty($errores)) {                        
                if ($chequeoObj->registrarChequeoMonitoreo()){ 
                    $echo[]="Chequeo programado con exito";
                }
                else{
                    $errores[]="Se detecto errores en registrar datos de chequeo";
                }               
            } 

        }
        if (isset($_POST['numeroOrden'])) {
            if($_POST['numeroOrden']==''){
                $errores[]="no se registro chequeo por falta de numero de orden";
            }
            $fecha=$_POST['fecha'].$_POST['hora'];
            $dataProgramacion=Chequeos::obtenerProgramacionChequeo($fecha);
            if (count($dataProgramacion)>='4') {
                $errores[]="Existen 5 chqueos programados para esa misma hora intenta programar con otra hora o fecha";
            }
            if($da=$chequeoObj->validarExistenciaPendiente(limpiar(trim($_POST['numeroOrden']))))
            {  
                $errores[]="Orden ya esta en gestión";            
            }
             if (empty($errores)) {                        
                    if ($chequeoObj->registrarChequeoMonitoreo()){ 
                        $echo[]="Chequeo programado con exito";
                    }else{
                        $errores[]="Se detecto errores en registrar datos de chequeo";
                    }
            }       
        }
       
       
    }

if (isset($_POST['idchequeoActualizar'])) {
    $chequeoObj->motivo=limpiar($_POST['motivo']);
    $chequeoObj->personaContacto=limpiar($_POST['personaContacto']);
    $chequeoObj->numeroContacto=limpiar($_POST['numeroContacto']);
    $fecha= str_replace("/", "", $_POST['fecha']);
    $hora=str_replace(":", "", $_POST['hora']);
    $chequeoObj->fechaHoraChequeoLinea=$fecha.$hora;
    $chequeoObj->idchequeo=$_POST['idchequeoActualizar'];  
    if (!$chequeoObj->actualizarAlgunosDatos()) {
         $errores[]="Error en actualizar algunos datos";
    }else{
        $echo[]="Actualizado con exitos";
     }
}

?>
   <link href="assets/css/plugins/footable/footable.core.css" rel="stylesheet">
   <link href="assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
   <link href="assets/css/sweetalert2.css" rel="stylesheet">
   <style>
    .widget p {
        display: inline-block;        
    }

    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }

    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }

    .reloj .caja-segundos {
        display: inline-block;
    }

    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }</style>
       <style>
    .widget p {
        display: inline-block;        
    }

    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }

    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }

    .reloj .caja-segundos {
        display: inline-block;
    }

    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }</style>
    <body>

    <style>
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(assets/img/Preloader_6.gif) center no-repeat #fff;
}
</style>
<div class="se-pre-con"></div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

    <body>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <?php include("includes/menuIzquierdo.php") ?>

               </div>
           </nav>

           <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("includes/navegacionArriba.php") ?>

                </nav>
            </div>
            <div class="wrapper wrapper-content">

                <div class="row ibox float-e-margins ibox-content ">
                    <div class="col-md-12">
                        <div class="row">
                           <div class="col-lg-6">
                           <?php 

                       $usRol=Usuario::obtenerUsuario($session->idusuario);

                            ?>

                            <?php if ($usRol->idrol=='1'): ?>
                            <div class="row">
                                <div class="col-md-5">
                                    <input type="text" style="border:none;" class="col-md-6 form-control border-bottom" placeholder="PEGAR CHASIS" id="chasis" name="chasis"  autocomplete="off">
                                </div>
                                <div class="col-md-2">
                                    <input type="hidden" name="token" value="<?php echo Token::generar() ?>">
                                <div class="btn btn-primary" name="subir" data-toggle="modal" data-target="#bateriasOModal" >Velificar</div></div>
                            </div>
                            <?php else: ?>
                            <div class="row">
                                <div class="col-md-5">
                                    <input type="text" style="border:none;" class="col-md-6 form-control border-bottom" placeholder="PEGAR NÚMERO ORDEN" id="orden" name="orden"  autocomplete="off">
                                </div>
                                <div class="col-md-2">
                                    <input type="hidden" name="token" value="<?php echo Token::generar() ?>">
                                <div class="btn btn-primary" name="subir" data-toggle="modal" data-target="#bateriasOModal" >Velificar</div></div>
                            </div>
                            <?php endif ?> 
                      
                        </div>

                        <div class="col-md-2 ">
                         
                        </div>
                        <div class="col-md-1 ">
                            
                        </div>

                        <div class="col-md-3">
                           <div class="widget" style="line-height: 0px;       
                           padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px">
                           <div class="fecha">
                            <p id="diaSemana" class="diaSemana">Martes</p>
                            <p id="dia" class="dia">27</p>
                            <p>de </p>
                            <p id="mes" class="mes">Octubre</p>
                            <p>del </p>
                            <p id="year" class="year">2015</p>
                        </div>

                        <div class="reloj">
                            <p id="horas" class="horas">11</p>
                            <p>:</p>
                            <p id="minutos" class="minutos">48</p>
                            <p>:</p>
                            <div class="caja-segundos">
                                <p id="ampm" class="ampm">AM</p>
                                <p id="segundos" class="segundos">12</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="ibox float-e-margins col-lg-12">
        <div class="ibox-title">
            <h5>Solicitudes de chequeo a Monitoreo</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </ul>
            <a class="close-link">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
<style>.pagination1{margin:0;padding:0;}
.pagination1 li{
    display: inline;
    padding: 6px 10px 6px 10px;
    border: 1px solid #ddd;
    margin-right: -1px;
    font: 15px/20px Arial, Helvetica, sans-serif;
    background: #FFFFFF;
    box-shadow: inset 1px 1px 5px #F4F4F4;
}
.pagination1 li a{
    text-decoration:none;
    color: rgb(89, 141, 235);
}
.pagination1 li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination1 li.last {
    border-radius: 0px 5px 5px 0px;
}
.pagination1 li:hover{
    background: #CFF;
}
.pagination1 li.active{
    background: #F0F0F0;
    color: #333;
}
</style>
    <div class="ibox-content" id="resultados">
        <input type="text" class="form-control input-sm m-b-xs" id="filter"
        placeholder="Buscar">
        <table class=" table  table-stripped table-hover table-responsive" >
            <thead>
                <tr>
                    <th>#</th>                    
                    <th>Nombres</th>
                    <th>Placa</th>
                    <th>Fecha Creada</th>
                    <th>Fecha Programada</th>
                    <th>Estado</th>
                    <th>Acción</th>
                </tr>
            </thead>  
            <tbody  id="resultado">          
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Chequeos completados por monitoreo.</h5>&nbsp;&nbsp; <a href="chequeoMonitoreo"><i class="fa fa-rotate-right "></i></a>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <input type="text" class="form-control input-sm m-b-xs" id="filter1"
                placeholder="Fitrar vehículos o clientes en seguimiento">
                <table class="footable2 table table-stripped" data-page-size="8" data-filter=#filter1>
                    <thead>
                        <tr>
                            <tr>
                                <th>#</th>
                                <th>Placa</th>
                                <th>NOMBRES</th>
                                <th>ESTADO</th>
                                <th data-hide="all">CHASIS</th>
                                <th data-hide="all">Observaciones</th>
                                <th data-hide="all">Respuesta monitoreo</th>
                                <th data-hide="all">Fecha Registro</th>
                            </tr>
                        </tr>
                    </thead>
                    <?php $chObj=new Chequeos; ?>
                    <tbody id="">
                    <?php $m=1 ?>
                        <?php $datosChqueos=$chObj->obtenerChequeosRealizados() ?>
                        <?php if ($datosChqueos): ?>
                        <?php foreach ($datosChqueos as $var): ?>
                        <tr>
                            <td><?php echo $m++ ?></td>
                            <td><?php echo $var->placa ?></td>
                            <td><?php echo $var->nombres ?></td>
                            <td><?php echo obtenerEstado($var->estado) ?></td>
                            <td><?php echo $var->chasis ?></td>
                            <td><?php echo $var->observacion ?></td> 
                            <td><?php echo $var->conclusion ?></td> 
                            <td><?php echo hace($var->fechaHoraChequeoLinea) ?></td>
                        </tr>
                        <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5">
                                <ul class="pagination pull-right"></ul>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<?php include("includes/footer.php") ?>
</div>
</div>

</div>

<!-- LOS MODALES AQUI  -->

<div class="modal inmodal" id="bateriasOModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                <h4 class="modal-title">Solicitud de chequeo a Monitoreo</h4>
                <small class="font-bold"><div class="form-group ">
                </div></small>
            </div>
            <form  role="form" method="post" >

                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="submit" name="" class="btn btn-primary" id="btComfirmar">Confirmar</button><br>
                    <label for="" class="text-danger" id="mensajeCambioarFehca">Por Favor registrar en una hora o fecha disponible</label>
                </div>
            </form>   
        </div>
    </div>
</div>
<!-- LOS MODALES AQUI  -->

<!-- <button class='btn' data-clipboard-action='copy' data-clipboard-target='p'>Copy</button> -->
<script src="assets/js/jquery-2.1.1.js"></script>
<script>
          $('#mensajeCambioarFehca').hide();
 function capacidad(){
   $('#btComfirmar').prop('disabled', true);        
   $('#mensajeCambioarFehca').show();           
  }
 function limpiarCapacidad(){
    $('#btComfirmar').prop('disabled', false);        
    $('#mensajeCambioarFehca').hide();  
  }
</script>

<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="assets/js/inspinia.js"></script>
<script src="assets/js/plugins/pace/pace.min.js"></script>
<script src="assets/js/plugins/footable/footable.all.min.js"></script>

<script src="assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="assets/js/clipboard.min.js"></script>
<script src="assets/calendario/dist/sweetalert.min.js"></script>



<!-- Toastr -->
<script src="assets/js/plugins/toastr/toastr.min.js"></script>
 <?php include_once('includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
   
<script>

$(document).ready(function(){

    $("#resultado" ).load("che/paginacion"); 
    $('.footable2').footable();
    $("#resultado").on( "click", ".pagination1 a", function (e){
        e.preventDefault();
        $(".se-pre-con").show(); 
        var page = $(this).attr("data-page"); 
        $("#resultado").load("che/paginacion",{"page":page}, function(){ 
            $(".se-pre-con").hide(); 
        });
        
    });
    $("#filter").keyup(function()
    {   
        var datosS = $(this).val();
        if(datosS.length >= 1)
            {
        $.ajax({
                type : 'POST',
                url  : 'che/resultadosDeBusqueda',
                data :  'datosS='+ datosS,
                success : function(data)
                {
                    $("#resultado").html(data);
                }
            });
            return false;
        }else{
            $("#resultado" ).load("che/paginacion"); 
    
        }
    });
});

function limpiarChasis(){
    $("#chasis").val('');
    $("#orden").val('');
}

$('#bateriasOModal').on('show.bs.modal', function (e) {
var rowid = $("#orden").val();
var chasis = $("#chasis").val();
if (rowid=='' || chasis=='' ) {
    var chequeoid = $(e.relatedTarget).data('id');
    if (chequeoid!=null) {
        $.ajax({
        type : 'post',
                url : 'che/chdataModal', 
                data :  'chequeoid='+ chequeoid, 
                success : function(data){
                $('.modal-body').html(data);
            }
        });
    }else{
        $.ajax({
                    type : 'post',
                            url : 'che/chdataModal', 
                            data :  'formatoIncorrecto='+ rowid, 
                            success : function(data){
                            $('.modal-body').html(data);
                        }
                });
    }
}else{    
    var chasis = $("#chasis").val();
    // console.log("chasis "+chasis);
    // console.log("orden "+rowid);

    var rowid = $("#orden").val();

    if (chasis==null) {
          if (rowid.search('-')=='-1') {                   
                 $.ajax({
                    type : 'post',
                            url : 'che/chdataModal', 
                            data :  'formatoIncorrecto='+ rowid, 
                            success : function(data){
                            $('.modal-body').html(data);
                        }
                });

            }else{
                $.ajax({
                    type : 'post',
                            url : 'che/chdataModal', 
                            data :  'rowid='+ rowid, 
                            success : function(data){
                            $('.modal-body').html(data);
                        }
                    });        
            }
    }else if(rowid==null){
        if (chasis!='') {
             $.ajax({
                    type : 'post',
                            url : 'che/chdataModal', 
                            data :  'chasis='+ chasis, 
                            success : function(data){
                            $('.modal-body').html(data);
                        }
                    });  
        }else{

        }
    }
  


    
}
    });

//////////El modal de la lista //////////
///////////////////////



$(document).ready(function(){

    $('.footable2').footable();
    $('.footable').footable();
    $('.footable3').footable();

});


//BATERIAS 

(function(){
    var actualizarHora = function(){        
        var fecha = new Date(),
        horas = fecha.getHours(),
        ampm,
        minutos = fecha.getMinutes(),
        segundos = fecha.getSeconds(),
        diaSemana = fecha.getDay(),
        dia = fecha.getDate(),
        mes = fecha.getMonth(),
        year = fecha.getFullYear();

        // Accedemos a los elementos del DOM para agregar mas adelante sus correspondientes valores
        var pHoras = document.getElementById('horas'),
        pAMPM = document.getElementById('ampm'),
        pMinutos = document.getElementById('minutos'),
        pSegundos = document.getElementById('segundos'),
        pDiaSemana = document.getElementById('diaSemana'),
        pDia = document.getElementById('dia'),
        pMes = document.getElementById('mes'),
        pYear = document.getElementById('year');

        
        // Obtenemos el dia se la semana y lo mostramos
        var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        pDiaSemana.textContent = semana[diaSemana];

        // Obtenemos el dia del mes
        pDia.textContent = dia;

        // Obtenemos el Mes y año y lo mostramos
        var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        pMes.textContent = meses[mes];
        pYear.textContent = year;

        // Cambiamos las hora de 24 a 12 horas y establecemos si es AM o PM

        if (horas >= 12) {
            horas = horas - 12;
            ampm = 'PM';
        } else {
            ampm = 'AM';
        }

        // Detectamos cuando sean las 0 AM y transformamos a 12 AM
        if (horas == 0 ){
            horas = 12;
        }

        // Si queremos mostrar un cero antes de las horas ejecutamos este condicional
        // if (horas < 10){horas = '0' + horas;}
        pHoras.textContent = horas;
        pAMPM.textContent = ampm;

        // Minutos y Segundos
        if (minutos < 10){ minutos = "0" + minutos; }
        if (segundos < 10){ segundos = "0" + segundos; }

        pMinutos.textContent = minutos;
        pSegundos.textContent = segundos;
    };

    actualizarHora();
    var intervalo = setInterval(actualizarHora, 1000);
}())
</script>
  <script>
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
<?php else: ?> 
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?> 
<?php endif ?>
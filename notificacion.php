<?php include_once("includes/init.php"); ?>
<?php 
if ($session->ingresado) {	
$obMenu=Usuario::obtenerMenu($session->idusuario) ;
$fechaA8=date("YmdHis",time() + 8);		
$fechaDe8=date("YmdHis",time() - 8);	
$fechaSinSeg=date("YmdHis");
if ($obMenu->confirmarRegistroEnOnix=="si") {
	$DatosCita=Cita::obtenerUltimaCitaRegistradaa();	
	if ($DatosCita) {
		$fechaUltimoRegistro=$DatosCita->fechaRegistro;
		if ($fechaDe8 < $fechaUltimoRegistro && $fechaA8 > $fechaUltimoRegistro ) {
			for ($i=0; $i <1; $i++) { 
				$usuarioR=Usuario::obtenerUsuario($DatosCita->idcreador);  
		 	echo  Notificaciones::mostrarNotificacion("Cita nueva de $usuarioR->usuarioNombre ","Nueva cita registra da para vehículo de cliente $DatosCita->nombreCliente placa $DatosCita->placa  ","com/confirmarRegistroEnOnix");		
				}	
		}
	}
	$VehiculosNuevos=Vehiculo::obtenerVehiculosParaRegistrarEnOnixUltimoRegistroParaNotifi();
	if ($VehiculosNuevos) {		
	$VehiculosNuevos->fechaCreacion;	
	$fechaDe8;
		$fechaUltimoRegistro=$VehiculosNuevos->fechaCreacion;
		if ($fechaDe8 < $fechaUltimoRegistro && $fechaA8 > $fechaUltimoRegistro ) {
			for ($i=0; $i <1; $i++) { 
				$usuarioR=Usuario::obtenerUsuario($VehiculosNuevos->idcreador); 
				$clDatos=Cliente::obtenerDatosDeclientePorId($VehiculosNuevos->idcliente);
				$vhDatos=Vehiculo::obtenerVehiculo($VehiculosNuevos->idvehiculo);
		 	echo  Notificaciones::mostrarNotificacion("Nuevo vehículo de  $usuarioR->usuarioNombre ","Nuevo vehículo de cliente $clDatos->nombre placa $vhDatos->placa  ","com/confirmarRegistroEnOnix");		
				}	
		}				
	}
}
if ($obMenu->chequeos=="si") {
	$chequeoDataAll=Chequeos::obtenerChequeoTrackOtrasAreas();
	foreach ($chequeoDataAll as $var) {
		$fechaUltimoRegistro=$var->fechaHoraChequeoLinea."00";
		if ($fechaDe8 < $fechaUltimoRegistro && $fechaA8 > $fechaUltimoRegistro) {				
				echo Notificaciones::mostrarNotificacion("Realizar chequeo programado","Hay un chequeo programado ","chequeos");						
		}	
	}
}
if ($obMenu->registrarOperaciones=="si") {
	$datosSinOrden=Operacion::obtenerInstalacionesSinOrdenParaNotificacion();
	if ($datosSinOrden) {
		$fechaUltimoRegistro=$datosSinOrden->fechaConfirmacionOPE;
		if ($fechaDe8 < $fechaUltimoRegistro && $fechaA8 > $fechaUltimoRegistro ) {
			for ($i=0; $i <1; $i++) { 
				$usuarioR=Usuario::obtenerUsuario($datosSinOrden->idConfirmacionOPE); 
				$clDatos=Cliente::obtenerDatosDeclientePorId($datosSinOrden->idcliente);
				$vhDatos=Vehiculo::obtenerVehiculo($datosSinOrden->idvehiculo);
		 	echo  Notificaciones::mostrarNotificacion("Crear orden prepago de $usuarioR->usuarioNombre ","Nuevo vehículo de cliente $clDatos->nombre placa $vhDatos->placa  ","ope/asignarOrdenes");		
				}	
		}
	}
}
}
?>

<?php $nav="AprobacionCredito" ;?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
} ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->gestionContabilidad=="si"): ?>
<?php
if (isset($_POST['guardarCambios'])) {
    if (Token::verificar($_POST['guardarCambios'])) {
        $ObjCred = new SolicitudCredito();
        $ObjCred->idtipoper = $_POST['tipoper'];
        $ObjCred->idvendedor = $session->idusuario;
        $ObjCred->cliente = $_POST['nsolicitante'];
        $ObjCred->numerodoc = $_POST['nrucdni'];
        $ObjCred->monto = $_POST['tmonto'];
        $ObjCred->noperaciones = $_POST['noperacion'];
        $ObjCred->dcredito = $_POST['ndias'];
        $ObjCred->detalle = $_POST['ndetalle'];
        $ObjCred->respresantelegal = $_POST['nrepLegal'];
        $ObjCred->dni = $_POST['dniRP'];

        $ObjCred->jefecompras = $_POST['njefCompras'];
        $ObjCred->jf_email = $_POST['nemailC'];
        $ObjCred->jf_cel = $_POST['nCelTelfC'];

        $ObjCred->jefetesoreria = $_POST['njefTeso'];
        $ObjCred->jt_email = $_POST['nemailT'];
        $ObjCred->jt_cel = $_POST['nCelTelfT'];

        $rutaImg='../public/vouchers/';

        if (!empty($_FILES['voucher']['name'])) {
            $extencionesAbilitadas = array('jpg', 'jpeg', 'png', 'gif', 'PNG', 'JPG', 'JPEG', 'PDF', 'DOCX', 'GIF', 'docx', 'pdf', 'rtf','xlsx');
            $nombreArchivo = $_FILES['voucher']['name'];
            $nombreArchivo = str_replace(".rtf", ".doc", $nombreArchivo);
            $tmp = explode('.', $nombreArchivo);
            $extencionArchivo = end($tmp);
            $tamanioArchivo = $_FILES['voucher']['size'];
            $ubicacionTemporal = $_FILES['voucher']['tmp_name'];

            if (!in_array($extencionArchivo, $extencionesAbilitadas)) {
                $errores[] = 'La extencion de la imagen debe ser jpg jpeg png gif';
            }
            if ($tamanioArchivo > 15242880) {
                $errores[] = 'El tamaño del archivo no deve exeder los 5 megabytes';
            }
        }

        if (!empty($_FILES['voucher']['name'])) {
            $fecha = date("YmdHis");
            move_uploaded_file($ubicacionTemporal, $rutaImg . $fecha . limpiarImg($nombreArchivo));
            $ObjCred->img_dni = $fecha . limpiarImg($nombreArchivo);
        }


        if(!empty($_POST['voucher'])){
            $ObjCred->img_dni=$_POST['voucher'];
        }


        if (!empty($_FILES['voucherRUC']['name'])) {
            $extencionesAbilitadas = array('jpg', 'jpeg', 'png', 'gif', 'PNG', 'JPG', 'JPEG', 'PDF', 'DOCX', 'GIF', 'docx', 'pdf', 'rtf','xlsx');
            $nombreArchivo = $_FILES['voucherRUC']['name'];
            $nombreArchivo = str_replace(".rtf", ".doc", $nombreArchivo);
            $tmp = explode('.', $nombreArchivo);
            $extencionArchivo = end($tmp);
            $tamanioArchivo = $_FILES['voucherRUC']['size'];
            $ubicacionTemporal = $_FILES['voucherRUC']['tmp_name'];

            if (!in_array($extencionArchivo, $extencionesAbilitadas)) {
                $errores[] = 'La extencion de la imagen debe ser jpg jpeg png gif';
            }
            if ($tamanioArchivo > 15242880) {
                $errores[] = 'El tamaño del archivo no deve exeder los 5 megabytes';
            }
        }

        if (!empty($_FILES['voucherRUC']['name'])) {
            $fecha = date("YmdHis");
            move_uploaded_file($ubicacionTemporal, $rutaImg . $fecha . limpiarImg($nombreArchivo));
            $ObjCred->img_ficharuc = $fecha . limpiarImg($nombreArchivo);
        }

        if(!empty($_POST['voucherRUC'])){
            $ObjCred->img_ficharuc=$_POST['voucherRUC'];
        }



        if (!empty($_FILES['voucherPDT']['name'])) {
            $extencionesAbilitadas = array('jpg', 'jpeg', 'png', 'gif', 'PNG', 'JPG', 'JPEG', 'PDF', 'DOCX', 'GIF', 'docx', 'pdf', 'rtf','xlsx');
            $nombreArchivo = $_FILES['voucherPDT']['name'];
            $nombreArchivo = str_replace(".rtf", ".doc", $nombreArchivo);
            $tmp = explode('.', $nombreArchivo);
            $extencionArchivo = end($tmp);
            $tamanioArchivo = $_FILES['voucherPDT']['size'];
            $ubicacionTemporal = $_FILES['voucherPDT']['tmp_name'];

            if (!in_array($extencionArchivo, $extencionesAbilitadas)) {
                $errores[] = 'La extencion de la imagen debe ser jpg jpeg png gif';
            }
            if ($tamanioArchivo > 15242880) {
                $errores[] = 'El tamaño del archivo no deve exeder los 5 megabytes';
            }
        }

        if (!empty($_FILES['voucherPDT']['name'])) {
            $fecha = date("YmdHis");
            move_uploaded_file($ubicacionTemporal, $rutaImg . $fecha . limpiarImg($nombreArchivo));
            $ObjCred->img_pdt = $fecha . limpiarImg($nombreArchivo);
        }
        if(!empty($_POST['voucherPDT'])){
            $ObjCred->img_pdt=$_POST['voucherPDT'];
        }


        if (!empty($_FILES['voucherOrden']['name'])) {
            $extencionesAbilitadas = array('jpg', 'jpeg', 'png', 'gif', 'PNG', 'JPG', 'JPEG', 'PDF', 'DOCX', 'GIF', 'docx', 'pdf', 'rtf','xlsx');
            $nombreArchivo = $_FILES['voucherOrden']['name'];
            $nombreArchivo = str_replace(".rtf", ".doc", $nombreArchivo);
            $tmp = explode('.', $nombreArchivo);
            $extencionArchivo = end($tmp);
            $tamanioArchivo = $_FILES['voucherOrden']['size'];
            $ubicacionTemporal = $_FILES['voucherOrden']['tmp_name'];

            if (!in_array($extencionArchivo, $extencionesAbilitadas)) {
                $errores[] = 'La extencion de la imagen debe ser jpg jpeg png gif';
            }
            if ($tamanioArchivo > 15242880) {
                $errores[] = 'El tamaño del archivo no deve exeder los 5 megabytes';
            }
        }

        if (!empty($_FILES['voucherOrden']['name'])) {
            $fecha = date("YmdHis");
            move_uploaded_file($ubicacionTemporal, $rutaImg. $fecha . limpiarImg($nombreArchivo));
            $ObjCred->img_ordenservi = $fecha . limpiarImg($nombreArchivo);
        }
        if(!empty($_POST['voucherOrden'])){
            $ObjCred->img_ordenservi=$_POST['voucherOrden'];
        }

        // $ObjCred->img_dni=$_POST['voucher'];
        //$ObjCred->img_ficharuc=$_POST['voucherRUC'];
        // $ObjCred->img_pdt=$_POST['voucherPDT'];
        //  $ObjCred->img_ordenservi=$_POST['voucherOrden'];

        if ($_POST['tipoOpera'] == 'edit') {
            $ObjCred->idsolicre = $_POST['idSolicitud'];
            $ObjCred->updateSolicitudCredito();
        } else {
            $ObjCred->RegistrarSolicitarCredito();
        }
    }else{
        $errores[]="El formulario ya fue procesado, vericar.";
    }
}
?>
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/css2/style3.css">
<body>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <?php include("../includes/menuIzquierdo.php") ?>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../includes/navegacionArriba.php") ?>
            </nav>
        </div>
        <br>
        <div role="tablist">
            <input type="radio" title="Solicitudes " name="taprobacionsol" class='taprobacionsol' id="solicitudes" role="tab" aria-controls="solicitudes-panel" checked value="solicitudes" class="tsolicitud tsoli" />
            <input type="radio" title="Solicitudes Aprobadas" name="taprobacionsol"  class='taprobacionsol' id="aprobacion" role="tab" aria-controls="soliaprobadas-panel" value="aprobacion" class="tpoperacion intp" />
            <div class="tabs" aria-hidden="true">
                <label for="solicitudes" id="solicitudes-tab" class="tab"><h4>Solicitudes Pendientes</h4></label>
                <label for="aprobacion" id="aprobacion-tab" class="tab"><h4>Solicitudes Aprobadas</h4></label>
            </div>
            <div class="panels">
                <ul id="solicitudes-panel" class="panel active" role="tabpanel" aria-labelledby="solicitudes-tab" style="padding:0px">
                    <div class="wrapper wrapper-content">
                        <div class="row ibox float-e-margins ibox-content ">
                            <div class="col-md-12">
                                <div class="row">
                                    <p>Hola :)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </ul>
                <ul id="aprobacion-panel" class="panel active" role="tabpanel" aria-labelledby="aprobacion-tab" style="padding:0px">
                    <div class="wrapper wrapper-content">
                        <div class="row ibox float-e-margins ibox-content ">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="ibox-content">
                                        <div class="col-md-10"></div>
                                        <div class="col-md-2">
                                            <!--  <button class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editarAprobacionCredito" data-id="0" data-tipo="save">Registrar Solicitud <i class="fa fa-newspaper-o"></i></button>  </label>-->
                                        </div>
                                        <input type="text" class="form-control input-sm m-b-xs" id="filter"
                                               placeholder="Filtrar Solicitudes Credito">
                                        <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Tipo</th>
                                                <th>Cliente</th>
                                                <th>Monto</th>
                                                <th>N° Operaciones</th>
                                                <th>Dias Crédito</th>
                                                <th>Estado</th>
                                                <th>Acción</th>
                                                <!-- <th data-hide="all">Task</th> -->
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $obj=SolicitudCredito::aprobacionSolicitudCredito('');$n=1; ?>
                                            <?php if($obj){
                                                foreach ($obj as $var): ?>
                                                    <tr class="gradeX">
                                                        <td><?= $n++; ?></td>
                                                        <td><?= $var->desctipo ?> </td>
                                                        <td><?= $var->cliente ?> </td>
                                                        <td><?= $var->monto ?></td>
                                                        <td><?= $var->noperaciones ?></td>
                                                        <td><?= $var->dcredito ?></td>
                                                        <td><?= obtenerEstado($var->estado) ?></td>
                                                        <td>
                                                            <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#editarSolicitudCredito" data-id="<?php echo $var->idsolicre ?>" data-tipo="edit">Editar <i class="fa fa-edit"></i></button>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; }else{?>
                                                <tr>
                                                    <td colspan="8" class="text-center">No hay información Registrada</td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="7">
                                                    <ul class="pagination pull-right"></ul>
                                                </td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ul>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                    <div class="ibox float-e-margins col-lg-12">
                        <div class="ibox-title">
                            <h5>Aprobación de Créditos</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include("../includes/footer.php") ?>
        </div>
    </div>

    <div class="modal inmodal" id="editarSolicitudCredito" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceIn">
                <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title">Solicitud De Credito</h4>
                </div>
                <form  role="form" method="post"  enctype="multipart/form-data" name="editarSolicitudC" id="editarSolicitudC">
                    <div class="modal-body" style="background: #fff;padding-top: 0"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                        <button type="submit" name="guardarCambios" class="btn btn-primary btn-modal"  value="<?php echo Token::generar() ?>">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/plugins/clockpicker/clockpicker.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/select2/select2.full.min.js"></script>o

<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="../assets/js/jquery-confirm.min.js"></script>
<style>
    .popover {
        z-index: 999999;
    }
</style>


<script>
    <?php if (!empty($errores)): ?>
    <?php foreach ($errores as $error): ?>
    toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
    <?php endforeach ?>
    <?php endif ?>
    <?php if(!empty($echo)): ?>
    <?php foreach($echo as $ok): ?>
    toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
    <?php endforeach ?>
    <?php endif ?>
</script>
<script>
    $('#editarSolicitudCredito').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        var tipo = $(e.relatedTarget).data('tipo');
        $(".modal-title").html((tipo=='save'?'Registrar':'Actualizar')+" Solicitud De Credito");
        $(".btn-modal").html((tipo=='save'?'Guardar':'Actualizar'));
        $(".modal-body").html("");
        $.ajax({
            type : 'post',
            url : 'modal/SolicitudCreditoModal',
            data :  'rowid='+rowid+"&tipoOpera="+tipo,
            success : function(data){
                $('.modal-body').html(data);
            }
        });
    });

    $(document).ready(function(){
        $('.footable').footable();
        $('.footable2').footable();
    });

    function soloNumero(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) ) {
            status = "This field accepts numbers only.";
            return false
        }
        status = "";
        return true
    }

    function soloNumeroMonto(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode!=46 && (charCode < 48 || charCode > 57) ) {
            status = "This field accepts numbers only.";
            return false
        }
        status = "";
        return true
    }
</script>
<?php else: ?>
    <?php $session->salir();
    echo "NO NO NO NO ; presiona F5"
    ?>
<?php endif ?>

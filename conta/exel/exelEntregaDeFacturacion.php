
<?php
include("../../includes/init.php") ;
 if (isset($_POST['Descargar'])) {
	$idEstado=limpiar($_POST['corredor']);
	$fechaConsulta=$_POST['fecha'];	
    $desde=trim(current(explode('/', $fechaConsulta)));
    $tmp=explode('/', $fechaConsulta);
    $hasta=end($tmp);	
	$desde=date("YmdHis",strtotime($desde));	
	$hasta=date("Ymd",strtotime($hasta));	
	$hasta=$hasta."235959";

header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=$fechaConsulta.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false); 

 ?>
<html>
<head>
	
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.2.7.2 (Linux)"/>
	<meta name="author" content="Suzanne Torres Tumialan"/>
	<meta name="created" content="2017-07-07T18:13:55"/>
	<meta name="changed" content="2017-07-18T14:28:58.508084607"/>
	<meta name="AppVersion" content="14.0300"/>
	<meta name="Company" content="Hewlett-Packard Company"/>
	<meta name="DocSecurity" content="0"/>
	<meta name="HyperlinksChanged" content="false"/>
	<meta name="LinksUpToDate" content="false"/>
	<meta name="ScaleCrop" content="false"/>
	<meta name="ShareDoc" content="false"/>
	
	<style type="text/css">
		body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:x-small }
		a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } 
		a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } 
		comment { display:none;  } 
	</style>
	
</head>

<body>
<table>
	<thead><tr><th>Rango de fecha</th></tr></thead>
	<tbody><tr><td><?= $fechaConsulta ?></td></tr></tbody>
</table>
<table cellspacing="0" border="0">
	<colgroup span="19" width="98"></colgroup>
	<tr>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="20" align="left" valign=middle bgcolor="#8EB4E3"><font color="#000000"><br></font></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=middle bgcolor="#8EB4E3"><b><font color="#000000">Nº FACTURA</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#8EB4E3"><b><font color="#000000"><br></font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=middle bgcolor="#8EB4E3"><b><font color="#000000">RAZÓN SOCIAL</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle bgcolor="#8EB4E3"><b><font color="#000000">FECHA DE EMISIÓN</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle bgcolor="#8EB4E3" sdnum="1033;0;_-[$$-409]* #,##0.00_ ;_-[$$-409]* -#,##0.00 ;_-[$$-409]* &quot;-&quot;??_ ;_-@_ "><b><font color="#000000"> IMPORTE </font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=middle bgcolor="#8EB4E3"><b><font color="#000000">STATUS DE CANCELACIÓN</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000" align="left" valign=middle bgcolor="#8EB4E3"><b><font color="#000000"><br></font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#8EB4E3"><b><font color="#000000"><br></font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle bgcolor="#1F497D"><b><font color="#FFFFFF">DIRECCIÓN</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle bgcolor="#1F497D"><b><font color="#FFFFFF">DISTRITO</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle bgcolor="#1F497D"><b><font color="#FFFFFF">REFERENCIA DEL LUGAR</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle bgcolor="#1F497D"><b><font color="#FFFFFF">TELÉFONO</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle bgcolor="#1F497D"><b><font color="#FFFFFF">CONTACTO</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle bgcolor="#1F497D"><b><font color="#FFFFFF">HORARIO DE ATENCIÓN</font></b></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=4 align="center" valign=bottom bgcolor="#DCE6F2"><b><font color="#000000">ENTREGADO</font></b></td>
		</tr>
	<tr>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="45" align="left" valign=middle bgcolor="#8EB4E3"><b><font size=1 color="#000000">EJECUTIVO</font></b></td>
		<td style="border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#8EB4E3"><b><font color="#000000"><br></font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#8EB4E3"><b><font size=1 color="#000000">CANCELADO</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#8EB4E3"><b><font size=1 color="#000000">PENDIENTE DE COBRO</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#8EB4E3"><b><font size=1 color="#000000">CONDICIONES DE PAGO (Nº DE DIAS)</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#DCE6F2"><b><font size=1 color="#000000">SI</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#DCE6F2"><b><font size=1 color="#000000">FECHA</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#DCE6F2"><b><font size=1 color="#000000">NO</font></b></td>
		<td style="border-top: 1px solid #000000; border-left: 1px solid #000000" align="center" valign=middle bgcolor="#DCE6F2"><b><font size=1 color="#000000">MOTIVO</font></b></td>
	</tr>
<?php 

	if($datos=DatosFacturacion::obtenerDatosParElExel($idEstado,$desde,$hasta)){

// $usuario=Usuario::obtenerUsuario($idCreador);  
//     $nombreArchivo=$usuario->usuarioNombre." ".$usuario->usuarioApellidoPaterno." ".$fecha;
 ?>
 <?php foreach ($datos as $var): ?>

	<tr>
	<?php $usuario=Usuario::obtenerUsuario($var->idUsuarioCreado) ?>
	<?php $user=trim(current(explode('@', $usuario->usuario))); ?>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="18" align="left" valign=middle><font size=1><?= strtoupper($user) ?></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font size=1><?= $var->numeroFactura ?></font></td>
		<?php $tmp=explode('-', $var->numeroFactura);
    		$numero=end($tmp);	 ?>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font size=1><?= $numero ?></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font size=1><?= strtoupper($var->razonSocialFacturacion) ?></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdval="42885" ><font size=1><?= date("m/d/Y",strtotime($var->fechaVerificacion)) ?></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle sdval="160" ><font size=1> $ <?= $var->costo ?> </font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font size=1></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font size=1><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font size=1><br></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font size=1><?= $var->direccionEntrega ?></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font size=1><?= $var->distritoEntrega ?></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font size=1><?= $var->referenciaEntrega ?></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=middle ><font size=1><?= $var->telefonoFacturacion ?></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font size=1> <?= $var->recibidoPor ?></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle ><font size=1><?= $var->fechaHoraDeEntrega ?></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font size=1> <?= ($var->estadoEntrega=='68')?"SI":""; ?> </font></td>

		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font size=1><?= ($var->estadoEntrega=='68')? date("d/m/Y",strtotime($var->fechaHoraDeEntregaRegistro)):""; ?></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font size=1><?= ($var->estadoEntrega=='69')?"SI":"" ?></font></td>
		<td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000" align="left" valign=middle><font size=1><?= ($var->estadoEntrega=='70')?"Enviado ..... ":"" ?> <?= $var->observacionEntrega ?></font></td>
	</tr>

 	
 <?php endforeach ?>
	<?php 

	}else{

	 ?>
	<tr>
		<td>No se encotro registros</td>

	</tr>
	<?php 
	}
	 ?>
</table>
<!-- ************************************************************************** -->
</body>

</html>
<?php  }?>
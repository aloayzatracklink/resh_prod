<?php
include("../../includes/init.php") ;
 if (isset($_POST['Descargar'])) {

	$estadof=limpiar($_POST['convenio']);
	$fechaConsulta=$_POST['fecha'];	
    $desde=trim(current(explode('/', $fechaConsulta)));
    $tmp=explode('/', $fechaConsulta);
    $hasta=end($tmp);	
	$desde=date("YmdHis",strtotime($desde));	
	$hasta=date("Ymd",strtotime($hasta));	
	$hasta=$hasta."235959";
	$factuObj=new DatosFacturacion;
	$factuObj->estadoFacturacion=$estadof;
	header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
	header("Content-Disposition: attachment; filename=$fechaConsulta.xls");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false); 

}
 ?> 
<?php if ($datos=$factuObj->obtenerFacturaciones($desde,$hasta)){ ?>
	
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 plus MathML 2.0//EN" "http://www.w3.org/Math/DTD/mathml2/xhtml-math11-f.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://dublincore.org/documents/dcmi-terms/"><meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8"/><title xml:lang="en-US">- no title specified</title><meta name="DCTERMS.title" content="" xml:lang="en-US"/><meta name="DCTERMS.language" content="en-US" scheme="DCTERMS.RFC4646"/><meta name="DCTERMS.source" content="http://xml.openoffice.org/odf2xhtml"/><meta name="DCTERMS.issued" content="2017-09-07T11:31:53.870005624" scheme="DCTERMS.W3CDTF"/><meta name="DCTERMS.modified" content="2017-09-07T11:36:21.072483484" scheme="DCTERMS.W3CDTF"/><meta name="DCTERMS.provenance" content="" xml:lang="en-US"/><meta name="DCTERMS.subject" content="," xml:lang="en-US"/><link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" hreflang="en"/><link rel="schema.DCTERMS" href="http://purl.org/dc/terms/" hreflang="en"/><link rel="schema.DCTYPE" href="http://purl.org/dc/dcmitype/" hreflang="en"/><link rel="schema.DCAM" href="http://purl.org/dc/dcam/" hreflang="en"/><style type="text/css">
	@page {  }
	table { border-collapse:collapse; border-spacing:0; empty-cells:show }
	td, th { vertical-align:top; font-size:10pt;}
	h1, h2, h3, h4, h5, h6 { clear:both }
	ol, ul { margin:0; padding:0;}
	li { list-style: none; margin:0; padding:0;}
	<!-- "li span.odfLiEnd" - IE 7 issue-->
	li span. { clear: both; line-height:0; width:0; height:0; margin:0; padding:0; }
	span.footnodeNumber { padding-right:1em; }
	span.annotation_style_by_filter { font-size:95%; font-family:Arial; background-color:#fff000;  margin:0; border:0; padding:0;  }
	* { margin:0;}
	.ta1 { writing-mode:lr-tb; }
	.Default { font-family:Liberation Sans; }
	.ce1 { font-family:Calibri; color:#000000; font-size:8pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce10 { font-family:Arial; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; background-color:transparent; border-left-style:none; border-right-width:0.0261cm; border-right-style:solid; border-right-color:#000000; border-top-width:0.0261cm; border-top-style:solid; border-top-color:#000000; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; font-size:9pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce11 { font-family:Arial; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; background-color:transparent; border-left-width:0.0261cm; border-left-style:solid; border-left-color:#000000; border-right-width:0.0261cm; border-right-style:solid; border-right-color:#000000; border-top-style:none; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; font-size:9pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce12 { font-family:Calibri; color:#ffffff; font-size:11pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce13 { font-family:Arial; background-color:transparent; border-width:0.0261cm; border-style:solid; border-color:#000000; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; color:#ffffff; font-size:9pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce14 { font-family:Arial Narrow; background-color:#0070c0; border-width:0.0261cm; border-style:solid; border-color:#000000; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; color:#ffffff; font-size:9pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce15 { font-family:Arial Narrow; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; background-color:#0070c0; border-left-width:0.0261cm; border-left-style:solid; border-left-color:#000000; border-right-width:0.0261cm; border-right-style:solid; border-right-color:#000000; border-top-style:none; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; color:#ffffff; font-size:9pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce16 { font-family:Arial; background-color:transparent; border-width:0.0261cm; border-style:solid; border-color:#000000; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; font-size:9pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce17 { font-family:Arial Narrow; background-color:#0070c0; border-width:0.0261cm; border-style:solid; border-color:#000000; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; color:#ffffff; font-size:9pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce18 { font-family:Arial Narrow; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; background-color:#0070c0; border-left-width:0.0261cm; border-left-style:solid; border-left-color:#000000; border-right-width:0.0261cm; border-right-style:solid; border-right-color:#000000; border-top-style:none; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; color:#ffffff; font-size:9pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce19 { font-family:Arial; background-color:transparent; border-width:0.0261cm; border-style:solid; border-color:#000000; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; font-size:9pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce2 { font-family:Arial Narrow; background-color:#0070c0; border-width:0.0261cm; border-style:solid; border-color:#000000; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; color:#ffffff; font-size:8pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce20 { font-family:Calibri; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; border-left-style:none; border-right-style:none; border-top-style:none; vertical-align:bottom; text-align:left ! important; margin-left:0pt; writing-mode:page; color:#ff0000; font-size:11pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce21 { font-family:Arial Narrow; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; background-color:#0070c0; border-left-style:none; border-right-width:0.0261cm; border-right-style:solid; border-right-color:#000000; border-top-width:0.0261cm; border-top-style:solid; border-top-color:#000000; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; color:#ffffff; font-size:9pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce3 { font-family:Arial Narrow; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; background-color:#0070c0; border-left-width:0.0261cm; border-left-style:solid; border-left-color:#000000; border-right-width:0.0261cm; border-right-style:solid; border-right-color:#000000; border-top-style:none; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; color:#ffffff; font-size:8pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce4 { font-family:Arial; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; background-color:transparent; border-left-width:0.0261cm; border-left-style:solid; border-left-color:#000000; border-right-width:0.0261cm; border-right-style:solid; border-right-color:#000000; border-top-style:none; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; font-size:8pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce5 { font-family:Arial Narrow; background-color:#0070c0; border-width:0.0261cm; border-style:solid; border-color:#000000; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; color:#ffffff; font-size:9pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce6 { font-family:Arial Narrow; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; background-color:#0070c0; border-left-width:0.0261cm; border-left-style:solid; border-left-color:#000000; border-right-width:0.0261cm; border-right-style:solid; border-right-color:#000000; border-top-style:none; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; color:#ffffff; font-size:9pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce7 { font-family:Arial; background-color:transparent; border-width:0.0261cm; border-style:solid; border-color:#000000; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; font-size:9pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce8 { font-family:Arial; background-color:transparent; border-style:none; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; font-size:9pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:normal; }
	.ce9 { font-family:Arial Narrow; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#000000; background-color:#0070c0; border-left-style:none; border-right-width:0.0261cm; border-right-style:solid; border-right-color:#000000; border-top-style:none; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; color:#ffffff; font-size:9pt; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.co1 { width:83.34pt; }
	.co2 { width:98.79pt; }
	.co3 { width:64.01pt; }
	.co4 { width:85.66pt; }
	.co5 { width:111.91pt; }
	.co6 { width:94.96pt; }
	.co7 { width:196.84pt; }
	.ro1 { height:13.8pt; }
	.ro2 { height:12.81pt; }
	.ro3 { height:28.35pt; }
	</style>
	</head>
	<body dir="ltr">
	<table border="0" cellspacing="0" cellpadding="0" class="ta1">
		<colgroup><col width="128"/><col width="152"/><col width="99"/><col width="99"/><col width="132"/><col width="173"/><col width="146"/><col width="303"/><col width="99"/><col width="99"/><col width="99"/><col width="99"/><col width="99"/></colgroup>
		<tr class="ro1">
			<td style="text-align:left;width:83.34pt; " class="ce1"> </td>
			<td style="text-align:left;width:98.79pt; " class="Default"> </td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
			<td style="text-align:left;width:85.66pt; " class="Default"> </td>
			<td style="text-align:left;width:111.91pt; " class="ce12"> </td>
			<td style="text-align:left;width:94.96pt; " class="Default"><p>Rango de fecha:</p></td>
			<td style="text-align:left;width:196.84pt; " class="Default"><p>Lunes 2 de julio 2015 al martes 2 lulio 2015</p></td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
		</tr>
		<tr class="ro1">
			<td style="text-align:left;width:83.34pt; " class="ce1"> </td>
			<td style="text-align:left;width:98.79pt; " class="Default"> </td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
			<td style="text-align:left;width:85.66pt; " class="Default"> </td>
			<td style="text-align:left;width:111.91pt; " class="ce12"> </td>
			<td style="text-align:left;width:94.96pt; " class="Default"> </td>
			<td style="text-align:left;width:196.84pt; " class="Default"> </td>
			<td style="text-align:left;width:64.01pt; " class="ce20"> </td>
			<td style="text-align:left;width:64.01pt; " class="ce20"> </td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
			<td style="text-align:left;width:64.01pt; " class="ce20"> </td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
		</tr>
		<tr class="ro2">
			<td style="text-align:left;width:83.34pt; " class="ce2"><p>FECHA DEL CP</p></td>
			<td style="text-align:left;width:98.79pt; " class="ce5"><p>BOLETA</p></td>
			<td colspan="2" style="text-align:left;width:64.01pt; " class="ce5"><p>FACTURA</p></td>
			<td style="text-align:left;width:85.66pt; " class="ce5"><p>ESTADO DE PAGO</p></td>
			<td style="text-align:left;width:111.91pt; " class="ce5"><p>FECHA DE CANCELACIÓN</p></td>
			<td style="text-align:left;width:94.96pt; " class="ce14"><p>COSTO SIN IGV</p></td>
			<td style="text-align:left;width:196.84pt; " class="ce17"><p>CLIENTE</p></td>
			<td colspan="2" style="text-align:left;width:64.01pt; " class="ce5"><p>CANTIDAD</p></td>
			<td style="text-align:left;width:64.01pt; " class="ce5"><p>SISTEMA</p></td>
			<td style="text-align:left;width:64.01pt; " class="ce5"><p>CANAL</p></td>
			<td style="text-align:left;width:64.01pt; " class="ce5"><p>EJECUTIVO</p></td></tr>
		<tr class="ro2">
			<td style="text-align:left;width:83.34pt; " class="ce3"> </td>
			<td style="text-align:left;width:98.79pt; " class="ce6"> </td>
			<td colspan="2" style="text-align:left;width:64.01pt; " class="ce5"> </td>
			<td style="text-align:left;width:85.66pt; " class="ce6"> </td>
			<td style="text-align:left;width:111.91pt; " class="ce6"> </td>
			<td style="text-align:left;width:94.96pt; " class="ce15"> </td>
			<td style="text-align:left;width:196.84pt; " class="ce18"> </td>
			<td style="text-align:left;width:64.01pt; " class="ce5"><p>NUEVOS</p></td>
			<td style="text-align:left;width:64.01pt; " class="ce5"><p>*OTROS</p></td>
			<td style="text-align:left;width:64.01pt; " class="ce6"> </td>
			<td style="text-align:left;width:64.01pt; " class="ce6"> </td>
			<td style="text-align:left;width:64.01pt; " class="ce6"> </td>
		</tr>
		<?php 
			$total=0;
			$sumaSinIgv=0;
			$cantidad=1;

		 ?>
		<?php foreach ($datos as $var): ?>			
    <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculos) ?>    
    <?php $datosCanal=Canales::obtenerDatosCanalPorId($var->idcanal) ?>	
    <?php $usuario=Usuario::obtenerUsuario($var->idUsuarioCreado)  ?>	
		<tr class="ro3">
			<td style="text-align:right; width:83.34pt; " class="ce4"><p><?= formato($var->fechaVerificacion) ?></p></td>
			<td style="text-align:left;width:98.79pt; " class="ce7"><p> <?= $var->numeroBoleta ?></p></td>
			<td colspan="2" style="text-align:left;width:64.01pt; " class="ce7"> <?php $var->numeroFactura ?></td>
			<td style="text-align:left;width:85.66pt; " class="ce11"><p><?= obtenerEstado($var->estadoPago) ?> </p></td>
			<td style="text-align:left;width:111.91pt; " class="ce4"><p><?= $var->fechaPago ?></p></td>
			<td style="text-align:right; width:94.96pt; " class="ce7"><p> $ <?= $montoSin=round($var->costo/1.18,2) ?></p></td>
			<td style="text-align:left;width:196.84pt; " class="ce19"><p><?= ($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->nombre)." ".strtoupper($clienteObj->apellidosC) : strtoupper($clienteObj->razonSocial) ; ?></p></td>
			<td style="text-align:right; width:64.01pt; " class="ce11"><p><?= $cantidad ?></p></td>
			<td style="text-align:left;width:64.01pt; " class="ce11"> </td>
			<td style="text-align:left;width:64.01pt; " class="ce19"><p><?= $plan=limpiarPALABRAGPSSMART(strtoupper($datosVehiculo->plan)) ?> <?=  $var->tiempo ?> AÑO(S)</p></td>
			<td style="text-align:left;width:64.01pt; " class="ce19"><p><?= $datosCanal->nombreCanal ?></p></td>
			<td style="text-align:left;width:64.01pt; " class="ce19"><p><?= strtoupper($usuario->usuarioNombre)." ".strtoupper($usuario->usuarioApellidoPaterno) ?></p></td>
		</tr>
		<?php $sumaSinIgv+=$montoSin ?>
		<?php $total+=$cantidad ?>
		<?php endforeach ?>


		<tr class="ro1">
			<td style="text-align:left;width:83.34pt; " class="ce1"> </td>
			<td style="text-align:left;width:98.79pt; " class="Default"> </td>
			<td style="text-align:left;width:64.01pt; " class="ce8"> </td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
			<td style="text-align:left;width:85.66pt; " class="Default"> </td>
			<td style="text-align:left;width:111.91pt; " class="ce12"> </td>
			<td style="text-align:right; width:94.96pt; " class="ce16"><p>$ <?= $sumaSinIgv  ?></p></td>
			<td style="text-align:left;width:196.84pt; " class="ce19"> </td>
			<td style="text-align:right; width:64.01pt; " class="ce11"><p><?= $total ?></p></td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
			<td style="text-align:left;width:64.01pt; " class="Default"> </td>
		</tr>

		

</table>
</body>
</html>
<?php }else{
	?>
	<p>No se encontraron datos</p>
	<?php
	} ?>

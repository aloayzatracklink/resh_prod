<?php
require_once("../includes/init.php");
ini_set('memory_limit', '2048M');

$DatosF = new DatosFacturacion();
$Comisiones = new Comisiones();

$estadoPago = isset($_GET['estadoPago']) ? $_GET['estadoPago'] : null;
$fechaInicio = isset($_GET['fechaInicio']) ? $_GET['fechaInicio'] : null;
$fechaFin = isset($_GET['fechaFin']) ? $_GET['fechaFin'] : null;

$search = isset($_GET['search']) ? $_GET['search'] : null;
$cargaInit = isset($_GET['cargaInit']) ? $_GET['cargaInit'] : null;

$tipoFecha = isset($_GET['tipoFecha']) ? $_GET['tipoFecha'] : null;

switch ($estadoPago) {
    case '67':
    case '63':
    case '71':
    case '62':
    case '0':
        $response = $DatosF->paraTodosLosEstados($estadoPago, $fechaInicio, $fechaFin, $search, $tipoFecha);
        break;
    case 'todos':
        $response = $DatosF->datosFacturacionAllFecha($fechaInicio, $fechaFin, $search, $tipoFecha);
        break;
    default: //all
        $response = $DatosF->datosFacturacionAll($estadoPago, $fechaInicio, $fechaFin, $search, $cargaInit, $tipoFecha);
}

echo json_encode($response, JSON_INVALID_UTF8_IGNORE);
?>
<?php $nav="solicitudCredito" ?>
<?php require("../includes/head.php") ?>

    <link href="/assets/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="/assets/css/plugins/dropzone/dropzone.css" rel="stylesheet">

<?php 
    // if ($session->idusuario=2) 
    //     irA("rprEstadisticas");
?>
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
} ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->cotizar=="si" || $obMenu->operacionesComerciales=='si'): ?>    
<?php 
    require_once '../mailphp/PHPMailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;

    $creditObj=new Credito;    
    #$facturacion=new DatosFacturacion;
    $echo=array();
    $errores=array();
    if (isset($_POST['procesarddd'])) {
        Log::registrarLog('comisiones',$_POST['comisionid'],$_POST,'editadoDesdeComision');       
     $facturacion->tipoCanal=$_POST['tipoCanal'];
     $facturacion->nombreCanal=$_POST['nombreCanal'];
     $facturacion->ejecutivoCanal=$_POST['ejecutivoCanal'];
     $facturacion->datosfacturacionid=$_POST['idfacturacion'];
     if (!$facturacion->actualizarCanales()) {
         die("ERROR EN ACTUALIZAR CANAL");
     }
        $objetoComision->idcomision=$_POST['comisionid'];
        $objetoComision->comision=$_POST['comision'];        
        $objetoComision->idfacturacion=$_POST['idfacturacion'];        
        if($_POST['agregarId']=='0'){
            $objetoCanal->moneda=$_POST['monedaCanal'];
            if ($_POST['nombreBanco']=='') {
                $objetoCanal->banco=$_POST['bancoCanal'];
            }else{
                $objetoCanal->banco=$_POST['nombreBanco'];
            }
            $objetoCanal->numeroCuenta=$_POST['numeroCuenta'];
            $objetoCanal->cci=$_POST['cci'];
            $objetoCanal->ruc=$_POST['ruc'];
            $objetoCanal->titular=$_POST['titular'];
            if ($objetoCanal->registrarTitular()) {
                $objetoComision->idtitular=$objetoCanal->idtitular;
                $objetoComision->agregarIdTitular();
            }
        }else{
                $objetoComision->idtitular=$_POST['agregarId'];
                $objetoComision->agregarIdTitular();
        }

if (!empty($_FILES['recibo']['name'])) {
        $extencionesAbilitadas= array('jpg','jpeg','png','gif','pdf','JPG','JPEG','PNG','GIF','PDF','docx','DOCX');
        $nombreArchivo=$_FILES['recibo']['name'];        
        $tmp=explode('.', $nombreArchivo);
        $extencionArchivo=end($tmp);
        $tamanioArchivo=$_FILES['recibo']['size'];
        $ubicacionTemporal=$_FILES['recibo']['tmp_name'];
        if (!in_array($extencionArchivo,$extencionesAbilitadas)) {  
          $errores[]='La extencion de la imagen deve ser jpg jpeg png gif o pdf';
        }
        if ($tamanioArchivo>10485760) {
          $errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
        }
        $fecha=date("YmdHis");
        move_uploaded_file($ubicacionTemporal, '../public/vouchers/'.$fecha.$nombreArchivo); 
        $objetoComision->reciboHonorarios=$fecha.$nombreArchivo;

$clienteDatos=$_POST['datosCliente'];
$usuarioCrea=Usuario::obtenerUsuario('66');
$usuarioEje=Usuario::obtenerUsuario($session->idusuario);
$htmlContenido1="<body style=' margin: 0;padding: 0;font-family:Helvetica, Arial, sans-serif;    box-sizing: border-box;    font-size: 14px; background-color: #f6f6f6; '>
<table style=' background-color: #f6f6f6; width: 100%;'>
    <tr>
        <td></td>
        <td  width='600' style=' display: block !important;    max-width: 600px !important;    margin: 0 auto !important;    /* makes it centered */    clear: both !important;'>
            <div class='content' style='  max-width: 600px;    margin: 0 auto;    display: block;    padding: 20px;'>
                <table class='main' width='100%' cellpadding='0' cellspacing='0' style='background: #fff;    border: 1px solid #e9e9e9;    border-radius: 3px;'>
                    <tr>
                        <td style='font-size: 16px;    color: #fff;    font-weight: 500;    padding: 20px;    text-align: center;    border-radius: 3px 3px 0 0;  background: #1970b2;'>
                            COMPLETAR DATOS DE COMISION.
                        </td>
                    </tr>
                    <tr>
                        <td style='padding: 10px !important ;padding: 20px; vertical-align: top;'>
                            <table width='100%' cellpadding='0' cellspacing='0'>
                                
                                <tr>
                                    <td  style='padding: 0 0 20px;'>
                                        Estimada(o) $usuarioCrea->usuarioNombre $usuarioCrea->usuarioApellidoPaterno, el recibo de honorarios fue cargado para la comision del cliente:<strong>$clienteDatos </strong> del ejecutivo : $usuarioEje->usuarioNombre $usuarioEje->usuarioApellidoPaterno<br> <br> completa los datos del canal para el pago de la comision.  </td>
                                </tr>
                                <tr>
                                    <td style='padding: 0 0 20px;text-align: center; '>
                                        <a href='http://resh.tracklink.pe/com/comisionesR' style='text-decoration: none;    color: #FFF;    background-color: #1970b2;    border: solid #1970b2;    border-width: 5px 10px;    line-height: 2;
    font-weight: bold;    text-align: center;    cursor: pointer;
    display: inline-block;    border-radius: 5px;    text-transform: capitalize;'>Completar datos</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='padding: 0 0 20px;'>
                                        Gracias por usar Resh.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div style=' width: 100%;    clear: both;    color: #999;    padding: 20px;'>
                    <table width='100%'>
                        <tr>
                            <td style='font-size: 12px; text-align: center;padding: 0 0 20px;' ><a href='#' style='color: #999;'>Desuscriberse</a> de estas alertas.</td>                        </tr>
                    </table>
                </div></div>
        </td>
        <td></td>
    </tr>
</table>
</body>
</html>";
$mail->isSMTP();
$mail->SetLanguage( 'es', '../mailphp/PHPMailer/language/' );
$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->Host = gethostbyname('mail.tracklink.pe');
$mail->Port = 25;
$mail->SMTPAuth = false;
$mail->CharSet = 'UTF-8';
$mail->setFrom('no-responder@tracklink.pe','Resh');  
$mail->addReplyTo("pvasquez@tracklink.pe", 'Soporte resh'); 
$mail->addAddress("asistentecomercial@tracklink.pe"); 
$mail->addAddress("aacuna@tracklink.pe"); 
$mail->addAddress("raguedo@tracklink.pe"); 
$mail->addAddress("kmelendez@tracklink.pe"); 
// $mail->addAddress($usuarioEje->usuario,$usuarioEje->usuarioNombre." ".$usuarioEje->usuarioApellidoPaterno); 
$mail->addCC($usuarioEje->usuario,$usuarioEje->usuarioNombre." ".$usuarioEje->usuarioApellidoPaterno);
$mail->addBCC("pruebas-ti@tracklink.pe");
$mail->Subject='Completar datos de comisión del ejecutivo: '.$usuarioEje->usuarioNombre.' ';
$mail->msgHTML($htmlContenido1);
$mail->AltBody = 'Estimado(as) '.$usuarioCrea->usuarioNombre." ".$usuarioCrea->usuarioApellidoPaterno.' Completa los datos del canal'; 
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {     

}

}


    if (isset($_POST['reciboH'])) {
        $objetoComision->reciboHonorarios=$_POST['reciboH'];          
    }
    
    if (!empty($_POST['cci'])) {
        $objetoComision->cciC=$_POST['cci'];
        $objetoComision->registrarNumeroCCIC();
    }

    if ($_POST['estado']=='59') {
        $objetoComision->estado=$_POST['estado'];
    }else{
        $objetoComision->estado='91';
    }
        if ($objetoComision->actualizarComision()) {
                        $echo[]="Comision actutalizado con exito.";
        }else{
            die("fail");
        }
    }

 ?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<body>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="../assets/css/sweetalert2.css" rel="stylesheet">
<script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
<link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
   <style>
    .widget p {
        display: inline-block;        
    }
    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }
    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }
    .reloj .caja-segundos {
        display: inline-block;
    }
    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }</style>
    <body>
    <style>
.pagination1{margin:0;padding:0;}
.pagination1 li{
    display: inline;
    padding: 6px 10px 6px 10px;
    border: 1px solid #ddd;
    margin-right: -1px;
    font: 15px/20px Arial, Helvetica, sans-serif;
    background: #FFFFFF;
    box-shadow: inset 1px 1px 5px #F4F4F4;
}
.pagination1 li a{
    text-decoration:none;
    color: rgb(89, 141, 235);
}
.pagination1 li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination1 li.last {
    border-radius: 0px 5px 5px 0px;
}
.pagination1 li:hover{
    background: #CFF;
}
.pagination1 li.active{
    background: #F0F0F0;
    color: #333;
}
</style>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <?php include("../includes/menuIzquierdo.php") ?>
               </div>
           </nav>
           <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>
                </nav>
            </div>
            <div class="wrapper wrapper-content">

    <div class="ibox">      
    </div>
    <div class="ibox-content" id="resultados">
    <div class="row">
        <div class="col-md-8">
        <input type="text" class="form-control input-sm m-b-xs" id="filter"
        placeholder="Buscar" autofocus="">
        </div>
        <div class="col-md-4">
        <button class="btn bnt-primary" data-toggle="modal" data-target="#bateriasOModal" data-id="new" >Solicitar nuevo <i class="fa fa-plus"></i></button>

        </div>
    </div>
<div class='table-responsive'>
        <table class="table  table-stripped table-hover " >
             <thead>
                <tr>
                    <th>Cliente</th>
                    <th>Documento</th>
                    <th>Concepto</th>
                    <th>Importe</th>
                    <th>Estado</th>
                    <th>Acción</th>
                </tr>
            </thead> 
             <tbody  id="resultado">          
            </tbody>  
        </table>
    </div>
    </div>
</div>
<?php include("../includes/footer.php") ?>
</div>
</div>
</div>
<!-- LOS MODALES AQUI  -->
<div class="modal inmodal" id="bateriasOModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Creditos</h4>
                <small class="font-bold"><div class="form-group ">
                </div></small>
            </div>
            <form  role="form" method="post" enctype="multipart/form-data" >

                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="submit" name="procesar" class="btn btn-primary" value="<?php echo Token::generar() ?>">Guardar</button>
                </div>
            </form>   
        </div>
    </div>
</div>
<!-- <button class='btn' data-clipboard-action='copy' data-clipboard-target='p'>Copy</button> -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/jquery-confirm.min.js"></script>
<script src="../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>

<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/clipboard.min.js"></script>
<script src="../assets/calendario/dist/sweetalert.min.js"></script>

<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>

<?php include_once('../includes/script.php') ?>
<script>

$('#rango').daterangepicker({
    format: 'DD-MM-YYYY',
    endDate: moment(),
    showDropdowns: true,
    showWeekNumbers: true,
    timePicker: false,
    timePickerIncrement: 1,
    timePicker12Hour: false,
    ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
        'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Anterior Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    opens: 'right',
    drops: 'down',
    buttonClasses: ['btn', 'btn-sm'],
    applyClass: 'btn-primary',
    cancelClass: 'btn-default',
    separator: '/',
    locale: {
        applyLabel: 'Seleccionar',
        cancelLabel: 'Cancelar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Seleccionar Rango',
        daysOfWeek: ['Do', 'Lun', 'Mar', 'Mi', 'Ju', 'Vi','Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
    }
}, function(start, end, label) {               
     // $('#rango').focus();
});  

$( "#rango" ).change(function() {
    $('#rango').focus();
}); 
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
<?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
$(document).ready(function(){
//    $("#resultado" ).load( "pag/comisionesPag"); 
//    $('.footable2').footable();
//    $("#resultado").on( "click", ".pagination1 a", function (e){
//        e.preventDefault();
//        $(".se-pre-con").show(); 
//        var page = $(this).attr("data-page"); 
//        var ejecutivoComercial = $(".ejecutivoComercial").val();
//        var fecha=$("#rango").val();
//        var estado = $("#estado").val();
//        $("#resultado").load("pag/comisionesPag",{"page":page,"ejecutivoComercial":ejecutivoComercial,"fecha":fecha,"estado":estado}, function(){
//            $(".se-pre-con").hide(); 
//        });        
//    });
//    $("#filter").keyup(function()
//    {   
//        var datosS = $(this).val();
//        datosS = datosS.replace('<>/"', '');
//        var ejecutivoComercial = $(".ejecutivoComercial").val();
//        if(datosS.length >= 1)  
//            {
//        $.ajax({
//                type : 'POST',
//                url  : 'pag/resBusquedaComisiones',
//                data :  'datosS='+ datosS+'&ejecutivoComercial='+ejecutivoComercial,
//                success : function(data)
//                {
//                    $("#resultado").html(data);
//                }
//            });
//            return false;
//        }else{
//            $("#resultado" ).load("pag/comisionesPag");     
//        }
//    });
//$("#rango").bind("focus",function()
//{
//  var fecha=$(this).val();
//  var page = $("#resultado").attr("data-page"); 
//  var estado = $("#estado").val();
//  var ejecutivoComercial = $(".ejecutivoComercial").val();
//  $.ajax({
//      type : 'POST',
//      url  : 'pag/comisionesPag',
//      data : {page:page,fecha:fecha,ejecutivoComercial:ejecutivoComercial,estado:estado},
//      success : function(data)
//      {
//       $("#resultado").html(data);
//      }
//  });
//
//});

//$( "#estado" ).change(function() {
//    var fecha=$("#rango").val();
//    var page = $("#resultado").attr("data-page"); 
//    var estado = $(this).val();
//    var ejecutivoComercial = $(".ejecutivoComercial").val();
//    $("#resultado").load("pag/comisionesPag",{"page":page,"ejecutivoComercial":ejecutivoComercial,"fecha":fecha,"estado":estado}, function(){ 
//    });
//});
//
});

$('#bateriasOModal').on('show.bs.modal', function (e) {
    var rowid = $(e.relatedTarget).data('id');
    $.ajax({
        type : 'post',
                url : 'modal/creditoModal', 
                data :  'rowid='+ rowid, 
                success : function(data){
                $('.modal-body').html(data);
            }
        });
});

</script>
   <script>
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
<?php else: ?> 
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?> 
<?php endif ?>

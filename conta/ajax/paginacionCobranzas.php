<?php 
require_once("../../includes/init.php");

$fechaInicio = $_POST['fechaInicio'] ?? "";
$fechaFin = $_POST['fechaFin'] ?? "";

if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

    if(isset($_POST["page"])){
        $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
        if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');}
    }else{
        $numeroDePagina = 1;
    }

    $estadoPago = '';
    if (isset($_POST["estadoPago"]) && $_POST['estadoPago'] <> 'null') {
        $estadoPago = htmlspecialchars($_POST['estadoPago']);
    }
    //var_dump($estadoPago);
    $factObj = new DatosFacturacion();

    $cantidadO = $factObj->obtenerRegistrosSubidosCantidaEstadoPag($estadoPago, $fechaInicio, $fechaFin);

    // if (empty($estadoPago)) {
    //     $cantidadO = $factObj->obtenerRegistrosSubidosCantidaPago($fechaInicio, $fechaFin);    
    // } else {
    //     $cantidadO = $factObj->obtenerRegistrosSubidosCantidaEstadoPag($estadoPago, $fechaInicio, $fechaFin);        
    // }
    $comision = new Comisiones;

    $registrosPorPagina = 15;
    $paginasEnTotal = ceil($cantidadO->cantidad/ $registrosPorPagina); 
    $posisionDePagina = ($numeroDePagina - 1) * $registrosPorPagina;

    $n = 1;

    if (empty($estadoPago)) {
        $datos = $factObj->obtenerRegistrosSubidosPago($posisionDePagina, $registrosPorPagina, $fechaInicio, $fechaFin);    
    } elseif ($estadoPago != '71') {
        $datos = $factObj->obtenerRegistrosSubidosEPago($posisionDePagina, $registrosPorPagina, $estadoPago, $fechaInicio, $fechaFin);        
    } else {
        $datos = $factObj->obtenerRegistrosSubidosEPagoAutoespar($posisionDePagina, $registrosPorPagina, $estadoPago, $fechaInicio, $fechaFin);
    }
    

    if ($datos):              
        foreach ($datos as $var):    
            if($var->tipoDocumentoFacturacion=='Boleta'){
                $factObj->numeroBoleta = $var->numeroBoleta;
                $countF = $factObj->verificarExistenciaDobleDenumeroBoleta();
            } else if ($var->tipoDocumentoFacturacion=='Factura'){
                $factObj->numeroFactura = $var->numeroFactura;
                $countF = $factObj->verificarExistenciaDobleDenumeroFactura();
            }

            $comision->idfacturacion = $var->datosfacturacionid;
            $dtrab = $comision->verTrabajoComision();
            echo "<tr class='" . (($countF->cantidad > 1) ? "danger" : "") . "'>";
            echo "<td>" . $n++ . "</td>";
            echo "<td>" . $dtrab->trabajo . "</td>";
            echo "<td>" . $var->tipoDocumentoFacturacion . "</td>";
            $numero = ($var->tipoDocumentoFacturacion == 'Boleta') ? $var->numeroBoleta : $var->numeroFactura;
            echo "<td>" . (($numero == '') ? 'No facturado' : $numero) . "</td>";
            echo "<td>" . strtoupper($var->razonSocialFacturacion) . "</td>";
            echo "<td>" . haceMinimo($var->fechaCreada) . "</td>";
            echo "<td>" . obtenerEstado($var->estadoPago) . "</td>";
            if ($var->idcanal != 0) {
                $canalD = Canales::obtenerDatosCanalPorId($var->idcanal);
                echo "<td>" . $canalD->nombreCanal . " " . $canalD->ejecutivoCanal . "</td>";
            } else {
                echo "<td>Pendiente</td>";
            }
            echo "<td><button class='btn btn-primary' style='padding-top: 0; padding-bottom: 0' data-toggle='modal' data-target='#bateriasOModal' data-id='" . $var->datosfacturacionid . "'>Elegir <i class='fa fa-cog'></i></button>&nbsp;</td>";
            echo "</tr>";
        endforeach;

        echo "<tr>";
        echo "<td>Mostrando " . min(count($datos), $registrosPorPagina) . " de " . $cantidadO->cantidad . "</td>";
        echo "<td colspan='7'>" . paginate_function($registrosPorPagina, $numeroDePagina, $cantidadO->cantidad, $paginasEnTotal) . "</td>";
        echo "</tr>";         
    else:
        echo "<tr><td colspan='78' class='text-center'>No se encontró registro alguno</td></tr>";   
    endif;

} //

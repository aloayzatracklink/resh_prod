     <?php require_once("../../includes/init.php") ?>
  <?php if ($_POST): ?> 
<?php 

   $datos=limpiar(strip_tags($_POST['datosS'])); 
   $datos="%".$datos."%";
   $estadoPago='';
  if (isset($_POST["estadoPago"]) && $_POST['estadoPago'] <> 'null') {
    $estadoPago=htmlspecialchars($_POST['estadoPago']);
  }
 ?>    

<?php 
$facturacionObj= new DatosFacturacion;
if (empty($estadoPago)) {
  $datosA=$facturacionObj->buscarParaFiltroPago($datos);
} else {
  $datosA=$facturacionObj->buscarParaFiltroEPago($datos,$estadoPago);  
}

$comision = new Comisiones;

$n=1;
 ?>
  <?php if ($datosA): ?>                
  <?php foreach ($datosA as $var): ?>  
<?php 
if($var->tipoDocumentoFacturacion=='Boleta'){
$facturacionObj->numeroBoleta=$var->numeroBoleta;
    $countF=$facturacionObj->verificarExistenciaDobleDenumeroBoleta();
}else if ($var->tipoDocumentoFacturacion=='Factura'){
    $facturacionObj->numeroFactura=$var->numeroFactura;
    $countF=$facturacionObj->verificarExistenciaDobleDenumeroFactura();
}
 $comision->idfacturacion=$var->datosfacturacionid;
 $dtrab =$comision->verTrabajoComision();
 echo (is_object($dtrab) && isset($dtrab->trabajo)) ? $dtrab->trabajo : 'N/A';
?>
     <tr class='<?= ($countF->cantidad>1)?"danger":"" ?>'>
         <td><?php echo $n++ ?></td>
         <td><?= $dtrab->trabajo ?? 'N/A' ?></td>
         
          <td><?= $var->tipoDocumentoFacturacion ?></td>
          <?php $numero = ($var->tipoDocumentoFacturacion=='Boleta') ? $var->numeroBoleta : $var->numeroFactura ; ?>          
          <td><?= $retVal = ($numero=='') ? 'No facturado' : $numero ; ?></td>
         <td><?= strtoupper($var->razonSocialFacturacion) ?></td>
          <td><?= haceMinimo($var->fechaCreada) ?></td>
          <td><?= obtenerEstado($var->estadoPago) ?></td>
          <?php if ($var->idcanal!=0): ?>
          <td><?php $canalD=Canales::obtenerDatosCanalPorId($var->idcanal);
           echo $canalD->nombreCanal; echo "&nbsp;&nbsp;";echo $canalD->ejecutivoCanal ?></td>
          <?php else: ?>            
            <td>Pendiente</td>
          <?php endif ?>
          <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" data-id="<?php echo $var->datosfacturacionid ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;</td>
    </tr>
  <?php endforeach ?>
   <?php else: ?>
   <tr>
   <?php if (!empty($estadoPago)) {
     $estadoaa=obtenerEstado($estadoPago); 
   } ?>
        <td colspan="8" class="text-center">No se encontro ningun registro con <h2><?= $_POST['datosS'] ?>.</h2> <?= $retVal = (empty($estadoPago)) ? "" : $estadoaa; ?></td>
   </tr>
   <?php endif ?>   
 <?php endif ?> 

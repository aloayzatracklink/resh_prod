<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $estadoPago='';
  if (isset($_POST["estadoPago"]) && $_POST['estadoPago'] <> 'null') {
      $estadoPago=htmlspecialchars($_POST['estadoPago']);
  }

  $factObj=new DatosFacturacion();
  if (empty($estadoPago)) 
{    $cantidadO=$factObj->obtenerRegistrosSubidosParaEntrega();    
  } else {
    $cantidadO=$factObj->obtenerRegistrosSubidosCantidadParaEntregaConEstado($estadoPago);        
  }
  
  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  // echo $cantidadO->cantidad; 
  $n=1;
  if (empty($estadoPago)) {
    $datos=$factObj->obtenerRegistrosSubidosEntregaDeFacturas($posisionDePagina,$registrosPorPagina);    
  } else {
    $datos=$factObj->obtenerRegistrosSubidosEentregaDeFacturas($posisionDePagina,$registrosPorPagina,$estadoPago);        
  }
             ?>    
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>    
    <tr class="animated bounceIn">
         <td><?php echo $n++ ?></td>
         <?php $usuario=Usuario::obtenerUsuario($var->idUsuarioCreado)  ?>
          <td><?= $usuario->usuarioNombre." ".$usuario->usuarioApellidoPaterno ?></td>
          <?php $numero = ($var->tipoDocumentoFacturacion=='Boleta') ? $var->numeroBoleta : $var->numeroFactura ; ?>   
          <td><?= $numero  ?></td>       
          <td><?= $var->razonSocialFacturacion ?></td>
         <td><?= haceMinimo($var->fechaVerificacion) ?></td>
          <td><?= $var->costo ?></td>
          <td><?= $var->distritoEntrega ?></td>
          <td><?= obtenerEstado($var->estadoEntrega) ?></td>
          <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" data-id="<?php echo $var->datosfacturacionid ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;</td>
    </tr>
<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="7">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="78" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>
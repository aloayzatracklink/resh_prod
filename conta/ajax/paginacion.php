<?php require_once("../../includes/init.php") ?>  
<?php 
  if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $idEjecutivo='';
  $estado='';
  $fecha='';
  
  if (isset($_POST["ejecutivoComercial"]) && $_POST['ejecutivoComercial'] <> 'null') {
      $idEjecutivo=htmlspecialchars($_POST['ejecutivoComercial']);
  }
  if (isset($_POST['estado'])) {
    $estado=$_POST['estado'];
  }
  if (isset($_POST['fecha'])) {
    $fecha=$_POST['fecha'];  
    $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
     $desde=date("Ymd000000",strtotime($desde));    
    $hasta=date("Ymd",strtotime($hasta)); 
    $hasta=$hasta."23:59:59";  
  }

  $comObj=new Comisiones();
  if (empty($idEjecutivo)&&empty($estado) &&empty($fecha)) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantida();    
  } 

  if (!empty($idEjecutivo)) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaE($idEjecutivo);        
  }
  
  if (!empty($_POST['fecha'])) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaEFecha($desde,$hasta);        
  }
  
  if (!empty($_POST['estado'])) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaEestadoPago($estado);        
  }

  if (!empty($idEjecutivo)&&!empty($fecha)) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaEejecutviFecha($idEjecutivo,$desde,$hasta);        
  }
  if (!empty($estado)&&!empty($idEjecutivo)) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaEestaodIdejutivo($idEjecutivo,$estado);        
  }  
  if (!empty($estado)&&!empty($fecha)) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaEestadoFecha($estado,$desde,$hasta);        
  }
  // if (!empty($idEjecutivo)||!empty($estado)) {
  //   $cantidadO=$comObj->obtenerRegistrosSubidosCantidaE($idEjecutivo);        
  // }
  if (!empty($idEjecutivo)&&!empty($estado)&&!empty($fecha)) {
    $cantidadO=$comObj->obtenerRegistrosSubidosCantidaETodos($estado,$desde,$hasta,$idEjecutivo);        
  }




  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  // echo $cantidadO->cantidad; 
  $n=1;
  if (empty($idEjecutivo)&&empty($estado) &&empty($fecha)) {
    $datos=$comObj->obtenerRegistrosSubidos($posisionDePagina,$registrosPorPagina);    
  } 

  if (!empty($idEjecutivo)) {
    $datos=$comObj->obtenerRegistrosSubidosE($posisionDePagina,$registrosPorPagina,$idEjecutivo);        
  }
  if (!empty($fecha)) {
    $datos=$comObj->obtenerRegistrosSubidosEFecha($posisionDePagina,$registrosPorPagina,$desde,$hasta);        
  }
  if (!empty($estado)) {
    $datos=$comObj->obtenerRegistrosSubidosEEstado($posisionDePagina,$registrosPorPagina,$estado);        
  }
  if (!empty($idEjecutivo) && !empty($estado)  ) {
    $datos=$comObj->obtenerRegistrosSubidosEestadoIdejecu($posisionDePagina,$registrosPorPagina,$estado,$idEjecutivo);        
  }
  if (!empty($idEjecutivo) && !empty($fecha)  ) {
    $datos=$comObj->obtenerRegistrosSubidosEfechaIdejecutiov($posisionDePagina,$registrosPorPagina,$idEjecutivo,$desde,$hasta);        
  }
  if (!empty($estado) && !empty($fecha)  ) {
    $datos=$comObj->obtenerRegistrosSubidosEFechaEstado($posisionDePagina,$registrosPorPagina,$estado,$desde,$hasta);        
  }
  if (!empty($estado) && !empty($fecha) && !empty($idEjecutivo)  ) {
    $datos=$comObj->obtenerRegistrosSubidosETodos($posisionDePagina,$registrosPorPagina,$estado,$idEjecutivo,$desde,$hasta);        
  }

?>   
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>    
    <tr class="">
         <td><?php echo $n++ ?></td>
          <td><?php $clienDatos=Cliente::obtenerDatosDeclientePorId($var->idcliente); echo $retVal = ($clienDatos->tipoCliente=="Natural") ? $clienDatos->nombre." ".$clienDatos->apellidosC : $clienDatos->razonSocial ; ?></td>
          <td><?php $ff=DatosFacturacion::obtenerDatosFacturacionPorId($var->idfacturacion);
           echo $ff->nombreCanal; echo "&nbsp;&nbsp;";echo $ff->ejecutivoCanal ?></td>
          <td><?=  $retVal = (empty($var->numeroReciboHonorario)) ? "No completado" : $var->numeroReciboHonorario ;   ?> - <?= $retVal = (empty($var->numeroFacturacion)) ? "No completado" : $var->numeroFacturacion ; ?></td>
          <td><?= haceMinimo($var->fechaRegistro) ?></td>
          <td><?= obtenerEstado($var->estado) ?></td>
         
          <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" data-id="<?php echo $var->idcomision ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;</td>
    </tr>
<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="7">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="7" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>

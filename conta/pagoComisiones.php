<?php $nav="pagoComision" ?>
<?php require("../includes/head.php") ?>
<?php 
    // if ($session->idusuario=2) 
    //     irA("rprEstadisticas");
?>
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">

<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
} ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->pagoComision=="si"): ?>
<?php 
$errores=array();
$echo=array();
    //Registramos las alertas aquí ///
if (isset($_POST['procesar'])) {
    if (Token::verificar($_POST['procesar'])) { 
     Log::registrarLog('comisiones',$_POST['idComision'],$_POST,'pagoComision');            
      $idcomision=limpiar($_POST['idComision']);
      $comisionObj=new Comisiones;
      $comisionObj->observacion=$_POST['observacion'];
      $comisionObj->idcomision=$idcomision;
      $comisionObj->numeroCuentaPagado=$_POST['numeroDecuentaPago'];
      if (isset($_POST['pagado'])) 
          $comisionObj->estado=67;
      else
          $comisionObj->estado=62;

      if (!$comisionObj->marcarComoPagado()) {
          $errores[]="Editado con exito";
      }else{
          $echo[]="Guardado como pagado";
      }       
    }else{
    $errores[]=" El formulario ya fue procesado, the token :v";
    }
} 
    ?>
   <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
   <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
   <link href="../assets/css/sweetalert2.css" rel="stylesheet">

<script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
<link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
   <style>
    .widget p {
        display: inline-block;        
    }

    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }

    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }

    .reloj .caja-segundos {
        display: inline-block;
    }

    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }</style>
    <body>

    <style>
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
}
.pagination1{margin:0;padding:0;}
.pagination1 li{
    display: inline;
    padding: 6px 10px 6px 10px;
    border: 1px solid #ddd;
    margin-right: -1px;
    font: 15px/20px Arial, Helvetica, sans-serif;
    background: #FFFFFF;
    box-shadow: inset 1px 1px 5px #F4F4F4;
}
.pagination1 li a{
    text-decoration:none;
    color: rgb(89, 141, 235);
}
.pagination1 li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination1 li.last {
    border-radius: 0px 5px 5px 0px;
}
.pagination1 li:hover{
    background: #CFF;
}
.pagination1 li.active{
    background: #F0F0F0;
    color: #333;
}
</style>
<div class="se-pre-con"></div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <?php include("../includes/menuIzquierdo.php") ?>

               </div>
           </nav>
           <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>
                </nav>
            </div>
            <div class="wrapper wrapper-content">
                <div class="row ibox float-e-margins ibox-content ">
                    <div class="col-md-12">
                        <div class="row">
                        <div class="col-md-3">
                                       <label for="eje" class="">Seleccione Ejecutivo comercial *</label>
                        <select  style="border: none;" class="ejecutivoComercial corredor border-bottom form-control text-capitalize" required name="corredor">
                            <option value="" disabled selected>Seleccione ejecutivo *</option>
                            <?php $ejecutivosComercial=Usuario::obtenerEjecutivosComercialConComision() ?>
                            <?php foreach ($ejecutivosComercial as $var): ?>
                                <option value="<?= $var->idusuario ?>"><?= $var->usuarioNombre ?> <?= $var->usuarioApellidoPaterno ?></option>
                            <?php endforeach ?>
                            <option value="null">Todos los ejecutivos</option>
                          </select>
                        </div>   

                          <div class="col-lg-4">
                               <label for="rango" class="">Seleccione rango fecha *</label>
                                <input style="border: none;"  class="cd-select form-control border-bottom" required  type="text" placeholder="Seleccione Rango de fecha" class="form-control" value="" name="rango" id="rango" autocomplete="off">
                            </div>
                            <div class="col-md-2">
                               <label for="estado" class="">Estado </label>
                                 <select  style="border: none;" class="corredor border-bottom form-control text-capitalize" required name="estado" id="estado">
                                    <option value="" disabled selected>Seleccione *</option>
                                        <option value="59">Pendiente pago</option>
                                        <option value="67">pagado</option>
                                        <option value="62">no pagado</option>
                                        <option value="">Todos</option>
                                  </select> 
                            </div>

                        <div class=" col-md-3 ">
                           <div class="widget" style="line-height: 0px;       
                           padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px">
                           <div class="fecha">
                            <p id="diaSemana" class="diaSemana">Martes</p>
                            <p id="dia" class="dia">27</p>
                            <p>de </p>
                            <p id="mes" class="mes">Octubre</p>
                            <p>del </p>
                            <p id="year" class="year">2015</p>
                        </div>

                        <div class="reloj">
                            <p id="horas" class="horas">11</p>
                            <p>:</p>
                            <p id="minutos" class="minutos">48</p>
                            <p>:</p>
                            <div class="caja-segundos">
                                <p id="ampm" class="ampm">AM</p>
                                <p id="segundos" class="segundos">12</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox">
      
    </div>

    <div class="ibox-content" id="resultados">
        <input type="text" class="form-control input-sm m-b-xs" id="filter"
        placeholder="Buscar" autofocus="">

        <table class="table  table-stripped table-hover table-responsive" >
            <thead>
                <tr>
                    <th>#</th>                    
                    <th>Nombres</th>
                    <th>Canal</th>
                    <th>N° Recibo H. - N° Factura</th>
                    <th>Fecha Creada</th>
                    <th>Estado</th>
                    <th>Acción</th>
                </tr>
            </thead>  
            <tbody  id="resultado">          
            </tbody>
  
        </table>
    </div>
</div>

<?php include("../includes/footer.php") ?>
</div>
</div>

</div>
<!-- LOS MODALES AQUI  -->


<div class="modal inmodal" id="bateriasOModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                <h4 class="modal-title">Gestion de pago de comisiones</h4>
                <small class="font-bold"><div class="form-group ">
                </div></small>
            </div>
            <form  role="form" method="post" >

                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="submit" name="procesar" class="btn btn-primary" value="<?php echo Token::generar() ?>">Guardar</button>
                </div>
            </form>   
        </div>
    </div>
</div>


<!-- LOS MODALES AQUI  -->

<!-- <button class='btn' data-clipboard-action='copy' data-clipboard-target='p'>Copy</button> -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>

<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/clipboard.min.js"></script>
<script src="../assets/calendario/dist/sweetalert.min.js"></script>

    <!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>

 <?php include_once('../includes/script.php') ?>
<script>
$('#rango').daterangepicker({
    format: 'DD-MM-YYYY',
    endDate: moment(),
    showDropdowns: true,
    showWeekNumbers: true,
    timePicker: false,
    timePickerIncrement: 1,
    timePicker12Hour: false,
    ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
        'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Anterior Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    opens: 'right',
    drops: 'down',
    buttonClasses: ['btn', 'btn-sm'],
    applyClass: 'btn-primary',
    cancelClass: 'btn-default',
    separator: '/',
    locale: {
        applyLabel: 'Seleccionar',
        cancelLabel: 'Cancelar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Seleccionar Rango',
        daysOfWeek: ['Do', 'Lun', 'Mar', 'Mi', 'Ju', 'Vi','Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
    }
}, function(start, end, label) {               
     // $('#rango').focus();
});  
 $( "#rango" ).change(function() {
    $('#rango').focus();
}); 
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
<?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
$(document).ready(function(){
    $("#resultado" ).load( "ajax/paginacion"); 
    $('.footable2').footable();
    $("#resultado").on( "click", ".pagination1 a", function (e){
        e.preventDefault();
        $(".se-pre-con").show(); 
        var page = $(this).attr("data-page"); 
        var ejecutivoComercial = $(".ejecutivoComercial").val();
        var fecha=$("#rango").val();
        var estado = $("#estado").val();
        $("#resultado").load("ajax/paginacion",{"page":page,"ejecutivoComercial":ejecutivoComercial,"fecha":fecha,"estado":estado}, function(){ 
            $(".se-pre-con").hide(); 
        });        
    });
    $("#filter").keyup(function()
    {   
        var datosS = $(this).val();
        datosS = datosS.replace('<>/"', '');
        var ejecutivoComercial = $(".ejecutivoComercial").val();
        if(datosS.length >= 1)  
            {
        $.ajax({
                type : 'POST',
                url  : 'ajax/resultadosDeBusqueda',
                data :  'datosS='+ datosS+'&ejecutivoComercial='+ejecutivoComercial,
                success : function(data)
                {
                    $("#resultado").html(data);
                }
            });
            return false;
        }else{
            $("#resultado" ).load("ajax/paginacion");     
        }
    });
$("#rango").bind("focus",function()
{
  var fecha=$(this).val();
  var page = $("#resultado").attr("data-page"); 
  var estado = $("#estado").val();
  var ejecutivoComercial = $(".ejecutivoComercial").val();
  $.ajax({
      type : 'POST',
      url  : 'ajax/paginacion',
      data : {page:page,fecha:fecha,ejecutivoComercial:ejecutivoComercial,estado:estado},
      success : function(data)
      {
       $("#resultado").html(data);
      }
  });
 //  $.ajax({ /// de la descarga ....

 //    type : 'POST',
 //    url  : 'ajax/instalacionesR',          
 //    data : {fecha:fecha},
 //    success : function(data)
 //    {
 //     $("#resultado1").html(data);
 //   }
 // });
});

    $( ".ejecutivoComercial" ).change(function() {
        var fecha=$("#rango").val();
        var page = $("#resultado").attr("data-page"); 
        var estado = $("#estado").val();
        var ejecutivoComercial = $(this).val();
        $("#resultado").load("ajax/paginacion",{"page":page,"ejecutivoComercial":ejecutivoComercial,"estado":estado,"fecha":fecha}, function(){ 
        });
    });

    $( "#estado" ).change(function() {
          var fecha=$("#rango").val();
          var page = $("#resultado").attr("data-page"); 
          var estado = $(this).val();
          var ejecutivoComercial = $(".ejecutivoComercial").val();
        $("#resultado").load("ajax/paginacion",{"page":page,"ejecutivoComercial":ejecutivoComercial,"fecha":fecha,"estado":estado}, function(){ 
        });
    });

});
$('#bateriasOModal').on('show.bs.modal', function (e) {
    var rowid = $(e.relatedTarget).data('id');
    $.ajax({
        type : 'post',
                url : 'modal/pagoComision', 
                data :  'rowid='+ rowid, 
                success : function(data){
                $('.modal-body').html(data);
            }
        });
});

(function(){
    var actualizarHora = function(){
        var fecha = new Date(),
        horas = fecha.getHours(),
        ampm,
        minutos = fecha.getMinutes(),
        segundos = fecha.getSeconds(),
        diaSemana = fecha.getDay(),
        dia = fecha.getDate(),
        mes = fecha.getMonth(),
        year = fecha.getFullYear();
        var pHoras = document.getElementById('horas'),
        pAMPM = document.getElementById('ampm'),
        pMinutos = document.getElementById('minutos'),
        pSegundos = document.getElementById('segundos'),
        pDiaSemana = document.getElementById('diaSemana'),
        pDia = document.getElementById('dia'),
        pMes = document.getElementById('mes'),
        pYear = document.getElementById('year');                
        var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        pDiaSemana.textContent = semana[diaSemana];        
        pDia.textContent = dia;
        var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        pMes.textContent = meses[mes];
        pYear.textContent = year;
        if (horas >= 12) {
            horas = horas - 12;
            ampm = 'PM';
        } else {
            ampm = 'AM';
        }
        if (horas == 0 ){
            horas = 12;
        }
        pHoras.textContent = horas;
        pAMPM.textContent = ampm;
        if (minutos < 10){ minutos = "0" + minutos; }
        if (segundos < 10){ segundos = "0" + segundos; }

        pMinutos.textContent = minutos;
        pSegundos.textContent = segundos;
    };

    actualizarHora();
    var intervalo = setInterval(actualizarHora, 1000);
}())
</script>
   <script>
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
<?php else: ?> 
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?> 
<?php endif ?>

<?php $nav="contabilidad" ;?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar"); 
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->gestionContabilidad=="si"): ?>
<?php 
    $contabilidadO=new DatosFacturacion();
    $errores=array();
    $echo=array();
if (isset($_POST['contabilidad'])) { 
    if ($_POST['tipoDocumento']=='Boleta') {
        $contabilidadO->numeroBoleta=$_POST['boleta'];
        $contabilidadO->apunte=$_POST['observacion'];   
        if (isset($_POST['voucherNew'])) {
            $contabilidadO->confirmarVoucher($_POST['datosfacturacionid']);
        }
        $contabilidadO->verificadoDatosFacturacionBoleta($_POST['datosfacturacionid']);

if ($dataComision=Comisiones::verificarExistencia(limpiar($_POST['idcanal']), limpiar($_POST['datosfacturacionid']),limpiar($_POST['idvehiculo']))) {
        $objetoComision=new Comisiones;
        $objetoComision->idfacturacion=$_POST['datosfacturacionid'];
        $objetoComision->numeroFacturacion=$_POST['boleta'];
        $objetoComision->idcomision=$dataComision->idcomision;
        if ($objetoComision->registrarDatosDeFacturacion()){           

        }else{
                    die("fail 001");
        }
    }    
            $echo[]="El registro se realizo con exito ";
    }else
    if($_POST['tipoDocumento']=='Factura'){
        $contabilidadO->numeroFactura=$_POST['factura'];
        $contabilidadO->apunte=$_POST['observacion'];
        if (isset($_POST['voucherNew'])) {
           $contabilidadO->confirmarVoucher($_POST['datosfacturacionid']);            
        }
        $contabilidadO->verificadoDatosFacturacionFactura($_POST['datosfacturacionid']);
    if ($dataComision=Comisiones::verificarExistencia(limpiar($_POST['idcanal']), limpiar($_POST['datosfacturacionid']),limpiar($_POST['idvehiculo']))) {
        $objetoComision=new Comisiones;
        $objetoComision->idfacturacion=$_POST['datosfacturacionid'];
        $objetoComision->numeroFacturacion=$_POST['factura'];
         $objetoComision->idcomision=$dataComision->idcomision;
        if ($objetoComision->registrarDatosDeFacturacion()){           
            $echo[]="El registro se realizo con exito ";
        }else{
            die("fail 002");  
        } 
    }
    }
        
}


 ?>
 <link href="../assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<body>

    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <?php include("../includes/menuIzquierdo.php") ?>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
       <?php include("../includes/navegacionArriba.php") ?>           

        </nav>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Gestion de contabilidad.</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Orden</th>
                        <th>Tipo documento Facturacion</th>                    
                        <th>Vehículo</th>
                        <th>Cliente</th>
                        <th>Observaciones</th>
                        <th>Acción</th>
                        <th>Ejecutivo</th>
                        <th>Fecha creacion</th>
                        <th>Imagen de Voucher</th>
                    </tr>
                    </thead>
                   
                    <tbody>
                    <?php $n=1; ?>
                    <?php $contabilidadObj=Contabilidad::obtenerDatosParaFacturacion() ?>
                    <?php foreach ($contabilidadObj as $var): ?>                        
                    <tr class="gradeX">
                        <td><?php echo $n++ ?></td>      
                        <td><?php echo $retVal = (!empty($var->orden)) ? $var->orden : "Plataforma aun no asigna orden" ;  ?></td>      
                        <td><?php echo $var->tipoDocumentoFacturacion ?></td>
                    
                        <!-- the vehiculo here    -->
                        <?php $objVeh=Vehiculo::obtenerVehiculo($var->idvehiculos) ?>
                        <td class="center"> Placa: <?php echo $objVeh->placa ?> Chasis : <?php echo $objVeh->chasis ?></td>
                        <!-- the vehiculo here    -->
                        <!-- cliente here   -->
                        <?php $ClienteO=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
                        <td class="center"> <?php echo $retVal = ($ClienteO->tipoCliente=="Natural") ? "DNI: " : "RUC: " ; ?> <?php echo $retVal = ($ClienteO->tipoCliente=="Natural") ? $ClienteO->dni : $ClienteO->ruc ; ?><br><?php echo $retVal = ($ClienteO->tipoCliente=="Natural") ? "Nombres: " : "Razon social: " ; ?>  <?php echo $retVal = ($ClienteO->tipoCliente=="Natural") ? $ClienteO->nombre." ".$ClienteO->apellidosC : $ClienteO->razonSocial ; ?></td>
                        <!-- cliente here   -->
                        <td class="center"><?php echo $var->observaciones ?></td>
                        <td class="center "><button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#contabilidad" data-id="<?php echo $var->datosfacturacionid ?>">Elegir <i class="fa fa-cog"> </i></button></td>
                        <td class="center"><?php $usuario=Usuario::obtenerUsuario($var->idUsuarioCreado);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno;  ?></td><td class="center"><?php echo hace($var->fechaCreada) ?></td>
                        <td class="center">
                        <?php if (!empty($var->voucher)): ?>
                        <img src="../public/vouchers/<?php echo $var->voucher ?>" alt="<?php echo $var->datosfacturacionid ?>" width="100" >
                        <?php else: ?>
                            <h2 class="text-danger">Pagó en caja</h2>
                        <?php endif ?>
                        </td>
                    </tr>
                    <?php endforeach ?>
                    
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Orden</th>
                        <th>Tipo documento Facturacion</th>                    
                        <th>Vehículo</th>
                        <th>Cliente</th>
                        <th>Observaciones</th>
                        <th>Ejecutivo</th>
                        <th>Acción</th>
                        <th>Fecha creacion</th>
                        <th>Imagen de Voucher</th>
                    </tr>
                    </tfoot>
                    </table>
                </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
      <?php include("../includes/footer.php") ?>

        </div>
        </div>
     <div class="modal inmodal" id="contabilidad" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated bounceIn">
                    <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                        <h4 class="modal-title">Gestionar</h4>
                        <small class="font-bold">Contabilidad.<div class="form-group ">
                        </div></small>
                    </div>
                    <form  role="form" method="post" >

                        <div class="modal-body" style="background: #fff;padding-top: 0">
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                            <button type="submit" name="guardarComoPendiente" class="btn btn-info">Guardar como pendiente</button>
                            <button type="submit" name="contabilidad" class="btn btn-primary">Verificado</button>
                        </div>
                    </form>   
                </div>
            </div>
        </div>


    <!-- Mainly scripts -->
    <script src="../assets/js/jquery-2.1.1.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <script src="../assets/js/plugins/dataTables/datatables.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="../assets/js/inspinia.js"></script>
    <script src="../assets/js/plugins/pace/pace.min.js"></script>
<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
 <?php include_once('../includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
    <script>
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });
    
      $('#contabilidad').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            $.ajax({
                type : 'post',
            url : 'modal/facturacionDataModal', //Here you will fetch records 
            data :  'rowid='+ rowid, //Pass $id
                success : function(data){
                    $('.modal-body').html(data);//Show fetched data from database
                }
            });
        });

    </script>
</body>

</html>
<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>


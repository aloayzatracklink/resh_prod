     <?php require_once("../../includes/init.php") ?>
  <?php if ($_POST): ?> 
<?php 

   $datos=trim($_POST['datos']);
   $idfacturacion=$_POST['convenio'];
   $datos="%".$datos."%";
   $fecha='';
  if (isset($_POST["fecha"])) {
    $fecha=$_POST['fecha'];
    $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
    $desde=date("YmdHis",strtotime($desde));  
    $hasta=date("Ymd",strtotime($hasta)); 
    $hasta=$hasta."235959";
  }

 ?>  
<?php 
$datosFacturacion= new DatosFacturacion;
$datosFacturacion->estadoFacturacion=limpiar($idfacturacion);
if (empty($fecha)) {
  $datosA=$datosFacturacion->buscarParaFiltroDatosFacturacion($datos);
} else {
  $datosA=$datosFacturacion->buscarParaFiltroDatosFacturacionFecha($datos,$desde,$hasta);  
}

$n=1;
 ?>
  <?php if ($datosA): ?>                
  <?php foreach ($datosA as $var): ?>    
     <tr>
    <td><?= $n++ ?></td>      
    <td><?= $retVal = (!empty($var->orden)) ? $var->orden : "SAC aun no asigna orden" ;  ?></td>      
    <td><?= $var->tipoDocumentoFacturacion ?></td>    
    <?php $objVeh=Vehiculo::obtenerVehiculo($var->idvehiculos) ?>
    <td class="center"> Placa: <?= ($objVeh)!=null?$objVeh->placa:$var->razonSocialFacturacion ?> Chasis :<?= ($objVeh)!=null?$objVeh->chasis:"" ?></td>
    <?php $ClienteO=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php if($ClienteO!=null){ ?>
    <td class="center"> <?= $retVal = ($ClienteO->tipoCliente=="Natural") ? "DNI: " : "RUC: " ; ?> <?= $retVal = ($ClienteO->tipoCliente=="Natural") ? $ClienteO->dni : $ClienteO->ruc ; ?><br><?= $retVal = ($ClienteO->tipoCliente=="Natural") ? "Nombres: " : "Razon social: " ; ?>  <?= $retVal = ($ClienteO->tipoCliente=="Natural") ? $ClienteO->nombre." ".$ClienteO->apellidosC : $ClienteO->razonSocial ; ?></td>
    <?php }else{ ?>
    <td class="center"> <?= $var->razonSocialFacturacion  ?> </td>
    <?php } ?> 
    <td class="center"><?= $var->observaciones ?><?= (!empty($var->apunte))?' --'.$var->apunte:'' ?></td>
    <td class="center"><?php $usuario=Usuario::obtenerUsuario($var->idUsuarioCreado);  echo $usuario->usuarioNombre;?>&nbsp;<?= $usuario->usuarioApellidoPaterno;  ?></td>
    <td class="center"><?= haceMinimo($var->fechaCreada) ?></td>
  <td class="center ">
    <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#contabilidad" data-id="<?= $var->datosfacturacionid ?>">Elegir <i class="fa fa-cog"> </i></button>
  </td>
</tr>
<?php endforeach ?>
   <?php else: ?>
   <tr>   
        <td colspan="8" class="text-center">No se encontro ningun registro con <h2><?= $_POST['datos'] ?>.</h2> </td>
   </tr>
   <?php endif ?>   
 <?php endif ?> 

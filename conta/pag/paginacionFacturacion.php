
<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $fecha='';
  $estado=htmlspecialchars($_POST['convenio']);

  if (isset($_POST["fecha"])) {
    $fecha=strip_tags($_POST['fecha']);
  $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
  $desde=date("YmdHis",strtotime($desde));  
 $hasta=date("Ymd",strtotime($hasta)); 
  $hasta=$hasta."235959";
  }
  $datosFacturacion=new DatosFacturacion();
  $datosFacturacion->estadoFacturacion=limpiar($estado);
  if($estado==77777){
    $datos=$datosFacturacion->obtenerDatosSG();
    $cantidadO=20;
    $n=1;
  }else{

      if (empty($fecha)){    
        $cantidadO=$datosFacturacion->obtenerDatosParaFacturacionCantidad();    
      }else{
        $cantidadO=$datosFacturacion->obtenerDatosParaFacturacionCantidadRANGO($desde,$hasta);
      }
      
      $registrosPorPagina=10;
      $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
      $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
      // echo $cantidadO->cantidad; 
      $n=1;
      if (empty($fecha)) {
        $datos=$datosFacturacion->obtenerDatosParaFacturacion($posisionDePagina,$registrosPorPagina);    
      } else {
        $datos=$datosFacturacion->obtenerDatosParaFacturacionRango($posisionDePagina,$registrosPorPagina,$desde,$hasta);        
      }  

  }


  ?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>    
  <tr>
    <td><?= $n++ ?></td>      
    <td><?= $retVal = (!empty($var->orden)) ? $var->orden : "SAC aun no asigna orden" ;  ?></td>      
    <td><?= $var->tipoDocumentoFacturacion ?>
    <?php if (!empty($var->fechaVerificacion)): ?>
      <?= (!empty($var->numeroBoleta)) ? $var->numeroBoleta : $var->numeroFactura ?>
    <?php endif ?></td>    
    <?php $objVeh=Vehiculo::obtenerVehiculo($var->idvehiculos) ?>
    <td class="center"> Placa: <?= ($objVeh)!=null?$objVeh->placa:$var->razonSocialFacturacion ?> Chasis :<?= ($objVeh)!=null?$objVeh->chasis:"" ?></td>
    <?php $ClienteO=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php if($ClienteO!=null){ ?>
    <td class="center"> <?= $retVal = ($ClienteO->tipoCliente=="Natural") ? "DNI: " : "RUC: " ; ?> <?= $retVal = ($ClienteO->tipoCliente=="Natural") ? $ClienteO->dni : $ClienteO->ruc ; ?><br><?= $retVal = ($ClienteO->tipoCliente=="Natural") ? "Nombres: " : "Razon social: " ; ?>  <?= $retVal = ($ClienteO->tipoCliente=="Natural") ? $ClienteO->nombre." ".$ClienteO->apellidosC : $ClienteO->razonSocial ; ?></td>
    <?php }else{ ?>
    <td class="center"> <?= $var->razonSocialFacturacion  ?> </td>
    <?php } ?> 
    <td class="center"><?= $var->observaciones ?> <?= (!empty($var->apunte))?' --'.$var->apunte:'' ?></td>
    <td class="center"><?php $usuario=Usuario::obtenerUsuario($var->idUsuarioCreado);  echo $usuario->usuarioNombre;?>&nbsp;<?= $usuario->usuarioApellidoPaterno;  ?></td>
    <td class="center"><?= haceMinimo($var->fechaCreada) ?></td>
    <td class="center"><?= $var->idrenovacion=="0"?obtenerEstado($objVeh->estado):"" ?></td>
  <td class="center ">
    <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#contabilidad" data-id="<?= $var->datosfacturacionid ?>">Elegir <i class="fa fa-cog"> </i></button>
  </td>
</tr>
<?php endforeach ?>


  <?php if($estado!=77777): ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="7">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
  <?php endif; ?>
</tr>         
<?php else: ?>
<tr>
  <td colspan="14" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>

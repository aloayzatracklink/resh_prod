<?php 

/**
* 
*/
class Contabilidad extends Recursos
{
	public $datosfacturacionid;
	public $idcliente;
	public $idvehiculos;
	public $idcanal;
	public $tipoDocumentoFacturacion;
	public $rucFacturacion;
	public $dniFacturacion;
	public $razonSocialFacturacion;
	public $direccionFiscal;
	public $telefonoFacturacion;
	public $voucher;
	public $tiempo;
	public $costo;
	public $observaciones;
	public $numeroBoleta;
	public $numeroFactura;
	public $apunte;
	public $idVerificador;
	public $fechaVerificacion;
	public $idUsuarioCreado;
	public $estadoFacturacion;
	public $fechaPagoVerficacion;
	public $idusuarioPagoVerificacion;
	public $ultimaActualizacionVerificacion;
	public $observacionPagoVerificacion;
	public $estadoPago;
	public $plan;
	public $fechaCreada;
	public $direccionEntrega;
	public $referenciaEntrega;
	public $distritoEntrega;
	public $recibidoPor;
	public $entregaEnInstalacion;
	public $orden;
	public $idActualizador;
	public $fechaActualizacion;
	public $fechaHoraDeEntrega;
	public $cantidad;
	public $idSugerenciaDeFechaEntrega;
	public $fecha;

	function __construct()
	{
	}
	public function registrarDatosDeFacturacion(){
		global $db;
		global $session;
		$this->fechaCreada=date("YmdHis");
		$sql="INSERT INTO datosFacturacion (idcliente,idvehiculos, tipoDocumentoFacturacion, rucFacturacion,dniFacturacion,razonSocialFacturacion,direccionFiscal,telefonoFacturacion,direccionEntrega,entregaEnInstalacion,referenciaEntrega,distritoEntrega,fechaHoraDeEntrega,recibidoPor,voucher,plan,tiempo,costo,observaciones,fechaCreada,idUsuarioCreado) ";
		$sql.=" VALUES ('";
		$sql.=$db->escapeString($this->idcliente)."', '";
		$sql.=$db->escapeString($this->idvehiculos)."', '";
		$sql.=$db->escapeString($this->tipoDocumentoFacturacion)."', '";
		$sql.=$db->escapeString($this->rucFacturacion)."', '";
		$sql.=$db->escapeString($this->dniFacturacion)."', '";
		$sql.=$db->escapeString($this->razonSocialFacturacion)."', '";
		$sql.=$db->escapeString($this->direccionFiscal)."', '";
		$sql.=$db->escapeString($this->telefonoFacturacion)."', '";
		$sql.=$db->escapeString($this->direccionEntrega)."', '";
		$sql.=$db->escapeString($this->entregaEnInstalacion)."', '";
		$sql.=$db->escapeString($this->referenciaEntrega)."', '";
		$sql.=$db->escapeString($this->distritoEntrega)."', '";
		$sql.=$db->escapeString($this->fechaHoraDeEntrega)."', '";
		$sql.=$db->escapeString($this->recibidoPor)."', '";
		$sql.=$db->escapeString($this->voucher)."', '";
		$sql.=$db->escapeString($this->plan)."', '";
		$sql.=$db->escapeString($this->tiempo)."', '";
		$sql.=$db->escapeString($this->costo)."', '";
		$sql.=$db->escapeString($this->observaciones)."', '";
		$sql.=$db->escapeString($this->fechaCreada)."', '";
		$sql.=$db->escapeString($session->idusuario)."')";
		if ($db->query($sql)) {
			$this->datosfacturacionid=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}
	}

	public static function obtenerDatosParaFacturacion(){
		return $obtenerDatosFacturacion=self::sentencia("SELECT * FROM datosFacturacion WHERE estadoFacturacion=28 ");
	}

	public static function obtenerDatosFacturados(){
		return $obtenerDatosFacturacion=self::sentencia("SELECT * FROM datosFacturacion WHERE estadoFacturacion=34 ORDER BY datosfacturacionid DESC ");
	}
	public function agregarIdCanal(){
		global $db;
		global $session;
		$sql="UPDATE datosFacturacion SET ";
		$sql.=" idcanal='";
		$sql.=$db->escapeString($this->idcanal)."' ";
		$sql.=" WHERE  datosfacturacionid='";
		$sql.=$db->escapeString($this->datosfacturacionid)."'";
		return ($db->query($sql)) ? true :false;
	}
private static function obtenerSiYaEstaRegistrado($fecha){
		global $db;
		$fecha=limpiar($fecha);
		$fecha=$db->escapeString($fecha);
		$sql=self::sentencia("SELECT fecha FROM sugerenciaDeFechaEntrega WHERE fecha='$fecha' LIMIT 1");
		return !empty($sql)?true:false;
	}
	
public function registrarSugerenciaDeFechaEntrega(){
		global $db;
		$this->fecha=strtoupper($this->fecha);
		if ($this->fecha!='') {
			if (!self::obtenerSiYaEstaRegistrado($this->fecha)) {
				$sql="INSERT INTO sugerenciaDeFechaEntrega ";
				$sql.="(fecha) VALUES ('";
				$sql.=$db->escapeString($this->fecha)."')";
				return ($db->query($sql)) ? true :false;
			}else{
				return false;
			}			
		}else{
			return false;
		}
	}
	// public static function obtenerDatosFacturacionPorId($datosfacturacionid){
	// 	global $db;
	// 	$obtenerDatosFacturacionPorId=self::sentencia("SELECT * FROM datosFacturacion WHERE datosfacturacionid = '".$db->escapeString($datosfacturacionid)."'");
	// 	return !empty($obtenerDatosFacturacionPorId) ? array_shift($obtenerDatosFacturacionPorId):false;
	// }





}




 ?>
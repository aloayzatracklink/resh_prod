<?php

class DatosFacturacion extends Recursos
{
	public $datosfacturacionid;
	public $idcliente;
	public $idvehiculos;
	public $idcanal;
	public $tipoDocumentoFacturacion;
	public $rucFacturacion;
	public $dniFacturacion;
	public $razonSocialFacturacion;
	public $direccionFiscal;
	public $telefonoFacturacion;
	public $voucher;
	public $tiempo;
	public $costo;
	public $observaciones;
	public $numeroBoleta;
	public $numeroFactura;
	public $apunte;
	public $idVerificador;
	public $fechaVerificacion;
	public $idUsuarioCreado;
	public $estadoFacturacion;
	public $fechaPagoVerficacion;
	public $idusuarioPagoVerificacion;
	public $ultimaActualizacionVerificacion;
	public $observacionPagoVerificacion;
	public $estadoPago;
	public $plan;
	public $fechaCreada;
	public $direccionEntrega;
	public $referenciaEntrega;
	public $distritoEntrega;
	public $recibidoPor;
	public $entregaEnInstalacion;
	public $orden;
	public $idActualizador;
	public $fechaActualizacion;
	public $fechaHoraDeEntrega;
	public $cantidad;
	public $idSugerenciaDeFechaEntrega;
	public $fecha;
	public $estadoEntrega;
	public $identrega;
	public $observacionEntrega;
	public $fechaHoraDeEntregaRegistro;
	public $fechaPago;
	public $oficina;
	public $tipoCanal;
	public $canalOrigen;
	public $nombreCanal;
	public $ejecutivoCanal;
	public $idrenovacion;
	public $fechaAbono;
	public $numeroOperacion;
	public $bancoPago;
	public $seguimiento;
	public $contractStatus;
	public $formaPago;
	public $id;
	public $type_commission;
	public $trabajo;

	public $detalles;
	public $vehiculo;
	public $cliente;
	public $ejecutivo;
	public $fechacreada;
	public $estadoOperacion;

	public $fechaFin;
	public $fechaInicio;
	public $condicionFecha;
	public $fechaInicioFormatted;
	public $fechaFinFormatted;

	public $fechaFinPagoVer;
	public $fechaInicioPagoVer;
	public $condicionFechaPagoVer;

	public $fechaInicioPagoVerFormatted;
	public $fechaFinPagoVerFormatted;

	public $alerta;

	public $chasis;

	public $usuarioNombre; 
	public $usuarioApellidoPaterno; 
	public $usuarioApellidoMaterno;


	function __construct()
	{
	}

	public static function crearContrato($idfacturacion)
	{
		global $db;
		$datos = self::obtenerDatosFacturacionPorId($idfacturacion);
		if (is_null($datos->contractStatus)) {
			new SeguimientoFacturacion($idfacturacion, $datos);
			$aC = "UPDATE datosFacturacion SET contractStatus=1";
			$aC .= " where  datosfacturacionid='";
			$aC .= limpiar($idfacturacion) . "' ";
			return ($db->query($aC)) ? true : false;
		} else {
			return false;
		}
	}

	public function registrarDatosDeFacturacion()
	{
		global $db;
		$this->fechaCreada = date("YmdHis");
		$sql = "INSERT INTO datosFacturacion (idcliente,idvehiculos, idcanal,tipoDocumentoFacturacion, rucFacturacion,dniFacturacion,razonSocialFacturacion,direccionFiscal,telefonoFacturacion,voucher,direccionEntrega,referenciaEntrega,fechaHoraDeEntrega,entregaEnInstalacion,distritoEntrega,recibidoPor,plan,tiempo,costo,observaciones,oficina,tipoCanal,canalOrigen,nombreCanal,ejecutivoCanal,fechaCreada,idrenovacion,fechaAbono,numeroOperacion,bancoPago,orden,idUsuarioCreado,formaPago) ";
		$sql .= " VALUES ('";
		$sql .= $db->escapeString($this->idcliente) . "', '";
		$sql .= $db->escapeString($this->idvehiculos) . "', '";
		$sql .= $db->escapeString($this->idcanal) . "', '";
		$sql .= $db->escapeString($this->tipoDocumentoFacturacion) . "', '";
		$sql .= $db->escapeString($this->rucFacturacion) . "', '";
		$sql .= $db->escapeString($this->dniFacturacion) . "', '";
		$sql .= $db->escapeString(addslashes($this->razonSocialFacturacion)) . "', '";
		$sql .= $db->escapeString(addslashes($this->direccionFiscal)) . "', '";
		$sql .= $db->escapeString($this->telefonoFacturacion) . "', '";
		$sql .= $db->escapeString($this->voucher) . "', '";
		$sql .= $db->escapeString($this->direccionEntrega) . "', '";
		$sql .= $db->escapeString($this->referenciaEntrega) . "', '";
		$sql .= $db->escapeString($this->fechaHoraDeEntrega) . "', '";
		$sql .= $db->escapeString($this->entregaEnInstalacion) . "', '";
		$sql .= $db->escapeString($this->distritoEntrega) . "', '";
		$sql .= $db->escapeString($this->recibidoPor) . "', '";
		$sql .= $db->escapeString($this->plan) . "', '";
		$sql .= $db->escapeString($this->tiempo) . "', '";
		$sql .= $db->escapeString($this->costo) . "', '";
		$sql .= $db->escapeString(addslashes($this->observaciones)) . "', '";
		$sql .= $db->escapeString($this->oficina) . "', '";
		$sql .= $db->escapeString($this->tipoCanal) . "', '";
		$sql .= $db->escapeString($this->canalOrigen) . "', '";
		$sql .= $db->escapeString($this->nombreCanal) . "', '";
		$sql .= $db->escapeString($this->ejecutivoCanal) . "', '";
		$sql .= $db->escapeString($this->fechaCreada) . "', '";
		$sql .= $db->escapeString($this->idrenovacion) . "', '";
		$sql .= $db->escapeString($this->fechaAbono) . "', '";
		$sql .= $db->escapeString($this->numeroOperacion) . "', '";
		$sql .= $db->escapeString($this->bancoPago) . "', '";
		$sql .= $db->escapeString($this->orden) . "', '";
		$sql .= $db->escapeString($this->idUsuarioCreado) . "', '";
		$sql .= $db->escapeString($this->formaPago) . "')";
		if ($db->query($sql)) {
			$this->datosfacturacionid = $db->ultimoIdInsertado();
			new SeguimientoFacturacion($this->datosfacturacionid, $this);
			return $this->datosfacturacionid;
		} else {
			return false;
		}
	}


	public function actualizarCanales($ultimoid, $idopera)
	{
		global $db;

		if (!is_null($ultimoid) and !is_null($idopera)) {
			$datosOld = new SeguimientoConsulta();
			$dold = $datosOld->consultaSeg("datosFacturacion", $this->datosfacturacionid, "datosfacturacionid");
			$doldd = $dold;
		}
		new SeguimientoFacturacion($this->datosfacturacionid, $this);
		$aC = "UPDATE datosFacturacion SET tipoCanal = '";
		$aC .= limpiar($this->tipoCanal) . "', ";
		$aC .= " oficina='";
		$aC .= limpiar($this->oficina) . "', ";
		$aC .= " nombreCanal='";
		$aC .= limpiar($this->nombreCanal) . "', ";
		$aC .= " ejecutivoCanal='";
		$aC .= limpiar($this->ejecutivoCanal) . "' WHERE  ";
		$aC .= " datosfacturacionid='";
		$aC .= limpiar($this->datosfacturacionid) . "' ";
		if ($db->query($aC)) {
			if (!is_null($ultimoid) and !is_null($idopera)) {
				new SeguimientoTablas("datosFacturacion", " Modifico Admin", $this->datosfacturacionid, $this, "datosfacturacionid", $doldd, $ultimoid, $idopera);
			}
			return true;
		} else {
			return false;
		}
		// return ($db->query($aC))?true:false;

	}

	public function actualizarDatosFactSeguimiento()
	{
		global $db;

		$aC = " UPDATE datosFacturacion SET tipoDocumentoFacturacion = '";
		$aC .= limpiar($this->tipoDocumentoFacturacion) . "', ";
		$aC .= " razonSocialFacturacion='";
		$aC .= limpiar($this->razonSocialFacturacion) . "', ";
		$aC .= " direccionFiscal='";
		$aC .= limpiar($this->direccionFiscal) . "', ";
		$aC .= " telefonoFacturacion='";
		$aC .= limpiar($this->telefonoFacturacion) . "', ";
		$aC .= " dniFacturacion='";
		$aC .= limpiar($this->dniFacturacion) . "', ";
		$aC .= " rucFacturacion='";
		$aC .= limpiar($this->rucFacturacion) . "' WHERE ";
		$aC .= " datosfacturacionid='";
		$aC .= limpiar($this->datosfacturacionid) . "' ";
		if ($db->query($aC)) {
			return true;
		} else {
			return false;
		}
	}
	// INSERT INTO datosFacturacion (idcliente,idvehiculos, tipoDocumentoFacturacion, rucFacturacion,dniFacturacion,razonSocialFacturacion,direccionFiscal,telefonoFacturacion,voucher,direccionEntrega,plan,tiempo,costo,observaciones,fechaCreada,idUsuarioCreado) VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '')
	public function ingresarOrden($datosfacturacionid, $orden)
	{
		global $db;
		$sql = "UPDATE datosFacturacion SET orden = '" . $db->escapeString($orden) . "' ";
		$sql .= " WHERE datosfacturacionid = '$datosfacturacionid'";
		new SeguimientoFacturacion($datosfacturacionid, $this);
		if ($db->query($sql)) {
			return true;
		} else {
			return false;
		}
	}

	public function verificadoDatosFacturacionBoleta($datosfacturacionid)
	{
		global $db;
		global $session;
		$estado = 34;
		$fecha = date("YmdHis");
		$sql = "UPDATE datosFacturacion SET numeroBoleta = '";
		$sql .= $db->escapeString($this->numeroBoleta) . "', ";
		$sql .= " apunte = '";
		$sql .= $db->escapeString($this->apunte) . "', ";
		$sql .= " idVerificador = '";
		$sql .= $session->idusuario . "',";
		$sql .= " tipoDocumentoFacturacion = '";
		$sql .= $this->tipoDocumentoFacturacion . "',";
		$sql .= " estadoFacturacion = '";
		$sql .= $estado . "',";
		$sql .= " fechaVerificacion = '";
		$sql .= $fecha . "' WHERE datosfacturacionid = '";
		$sql .= $db->escapeString($datosfacturacionid) . "' ";
		new SeguimientoFacturacion($datosfacturacionid, $this);
		if (!$db->query($sql)) {
			return false;
		}
	}

	public function verificadoDatosFacturacionFactura($datosfacturacionid)
	{
		global $db;
		global $session;
		$estado = 34;
		$fecha = date("YmdHis");
		$sql = "UPDATE datosFacturacion SET numeroFactura = '";
		$sql .= $db->escapeString($this->numeroFactura) . "', ";
		$sql .= " apunte = '";
		$sql .= $db->escapeString($this->apunte) . "', ";
		$sql .= " idVerificador = '";
		$sql .= $session->idusuario . "',";
		$sql .= " tipoDocumentoFacturacion = '";
		$sql .= $this->tipoDocumentoFacturacion . "',";
		$sql .= " estadoFacturacion = '";
		$sql .= $estado . "',";
		$sql .= " fechaVerificacion = '";
		$sql .= $fecha . "' WHERE  datosfacturacionid = '";
		$sql .= $db->escapeString($datosfacturacionid) . "' ";
		new SeguimientoFacturacion($datosfacturacionid, $this);
		return ($db->query($sql)) ? true : false;
	}

	public static function obtenerDatosFacturacionPorId($datosfacturacionid)
	{
		global $db;
		$obtenerDatosFacturacionPorId = self::sentencia("SELECT * FROM datosFacturacion WHERE datosfacturacionid = '" . $db->escapeString($datosfacturacionid) . "'");
		return !empty($obtenerDatosFacturacionPorId) ? array_shift($obtenerDatosFacturacionPorId) : false;
	}

	public static function obtenerFiles($datosfacturacionid)
	{
		global $db;
		$obtenerDatosFacturacionPorId = self::sentencia("SELECT id,creation_at fecha , bucket voucher ,idorder orden, name apunte, type oficina, detail tipoCanal FROM invoice_document WHERE idinvoice = '" . $db->escapeString($datosfacturacionid) . "'");
		return !empty($obtenerDatosFacturacionPorId) ? $obtenerDatosFacturacionPorId : false;
	}


	public static function obtenerFilesCertificado($datosfacturacionid)
	{
		global $db;
		$obtenerDatosFacturacionPorId = self::sentencia("SELECT creation_at fecha , bucket voucher ,idorder orden, name apunte, type oficina, detail tipoCanal FROM invoice_document WHERE idinvoice = '" . $db->escapeString($datosfacturacionid) . "' and type='Certificado' ");
		return !empty($obtenerDatosFacturacionPorId) ? $obtenerDatosFacturacionPorId : false;
	}

	public static function obtenerDatosFacturacionPorIdRenovacion($idrenovacion)
	{
		global $db;
		$obtenerDatosFacturacionPorIdR = self::sentencia("SELECT * FROM datosFacturacion WHERE idrenovacion = '" . $db->escapeString($idrenovacion) . "'");
		return !empty($obtenerDatosFacturacionPorIdR) ? array_shift($obtenerDatosFacturacionPorIdR) : false;
	}

	public static function obtenerDatosFacturacionPorIdVehiculo($idvehiculo)
	{
		global $db;
		$obtenerDatosFacturacionPorId = self::sentencia("SELECT * FROM datosFacturacion WHERE idvehiculos = '" . $db->escapeString($idvehiculo) . "'");
		return !empty($obtenerDatosFacturacionPorId) ? array_shift($obtenerDatosFacturacionPorId) : false;
	}
	public function confirmarVoucher($datosfacturacionid)
	{
		global $db;
		$confirmarVoucherQ = ("UPDATE datosFacturacion SET voucher='q@g03n(@j@.png' WHERE datosfacturacionid=" . $db->escapeString($datosfacturacionid) . "");
		new SeguimientoFacturacion($datosfacturacionid, $this);
		if ($db->query($confirmarVoucherQ)) {
			return true;
		} else {
			return false;
		}
	}

	public static function verificarFacturacion($idfacturacion)
	{
		global $db;
		$verificarQ = self::sentencia("SELECT * FROM datosFacturacion WHERE estadoFacturacion=34 AND datosfacturacionid=" . $db->escapeString($idfacturacion) . "");
		return !empty($verificarQ) ? true : false;
	}

	public function actualizarDatosDeFacturacion()
	{

		new SeguimientoFacturacion($this->datosfacturacionid, $this);
		global $db;
		global $session;
		$sql = "UPDATE datosFacturacion SET ";
		$sql .= "tipoDocumentoFacturacion='";
		$sql .= $db->escapeString($this->tipoDocumentoFacturacion) . "', ";
		$sql .= "rucFacturacion='";
		$sql .= $db->escapeString($this->rucFacturacion) . "', ";
		$sql .= "razonSocialFacturacion='";
		$sql .= $db->escapeString($this->razonSocialFacturacion) . "', ";
		$sql .= "direccionFiscal='";
		$sql .= $db->escapeString($this->direccionFiscal) . "', ";
		$sql .= "telefonoFacturacion='";
		$sql .= $db->escapeString($this->telefonoFacturacion) . "', ";
		$sql .= "entregaEnInstalacion='";
		$sql .= $db->escapeString($this->entregaEnInstalacion) . "', ";
		$sql .= "direccionEntrega='";
		$sql .= $db->escapeString($this->direccionEntrega) . "', ";
		$sql .= "referenciaEntrega='";
		$sql .= $db->escapeString($this->referenciaEntrega) . "', ";
		$sql .= "distritoEntrega='";
		$sql .= $db->escapeString($this->distritoEntrega) . "', ";
		$sql .= "voucher='";
		$sql .= $db->escapeString($this->voucher) . "', ";
		$sql .= "fechaHoraDeEntrega='";
		$sql .= $db->escapeString($this->fechaHoraDeEntrega) . "', ";
		$sql .= "recibidoPor='";
		$sql .= $db->escapeString($this->recibidoPor) . "', ";
		$sql .= "dniFacturacion='";
		$sql .= $db->escapeString($this->dniFacturacion) . "' WHERE ";
		$sql .= " datosfacturacionid='";
		$sql .= $db->escapeString($this->datosfacturacionid) . "'";
		return ($db->query($sql)) ? true : false;

	}
	public static function obtenerVentasSinOrden()
	{
		$obtenerVentasSinOrden = self::sentencia("SELECT * FROM datosFacturacion WHERE orden =''");
		return !(empty($obtenerVentasSinOrden)) ? $obtenerVentasSinOrden : false;
	}

	public function agregarIdCanal()
	{

		new SeguimientoFacturacion($this->datosfacturacionid, $this);
		global $db;
		global $session;
		$sql = "UPDATE datosFacturacion SET ";
		$sql .= " idcanal='";
		$sql .= $db->escapeString($this->idcanal) . "' ";
		$sql .= " WHERE  datosfacturacionid='";
		$sql .= $db->escapeString($this->datosfacturacionid) . "'";
		return ($db->query($sql)) ? true : false;
	}

	// public function obtenerRegistrosSubidosCantidaPago()
	// {
	// 	$sql = self::sentencia("SELECT COUNT(*) as cantidad FROM datosFacturacion D WHERE estadoPago = '0' AND EXISTS(SELECT idfacturacion FROM comisiones C WHERE C.idfacturacion=D.datosfacturacionid) LIMIT 1");
	// 	return !empty($sql) ? array_shift($sql) : false;
	// }
	// public function obtenerRegistrosSubidosCantidaEstadoPag($estado)
	// {
	// 	$estado = limpiar($estado);
	// 	$sql = self::sentencia("SELECT COUNT(*) as cantidad FROM datosFacturacion D WHERE estadoPago = '$estado' AND EXISTS(SELECT idfacturacion FROM comisiones C WHERE C.idfacturacion=D.datosfacturacionid) LIMIT 1 ");
	// 	return !empty($sql) ? array_shift($sql) : false;
	// }

	public function obtenerRegistrosSubidosCantidaPago($fechaInicio = "", $fechaFin = "")
	{
		$condicionFecha = "";
		if (!empty($fechaInicio) && !empty($fechaFin)) {
			$fechaInicioFormatted = date('Ymd', strtotime($fechaInicio)) . "000000";
			$fechaFinFormatted = date('Ymd', strtotime($fechaFin)) . "235959";
			$condicionFecha = " AND fechaCreada BETWEEN '$fechaInicioFormatted' AND '$fechaFinFormatted' ";
		}

		$sql = self::sentencia("SELECT COUNT(*) as cantidad FROM datosFacturacion D WHERE estadoPago = '0' AND EXISTS(SELECT idfacturacion FROM comisiones C WHERE C.idfacturacion=D.datosfacturacionid) $condicionFecha LIMIT 1");
		return !empty($sql) ? array_shift($sql) : false;
	}

	public function obtenerRegistrosSubidosCantidaEstadoPag($estado = "", $fechaInicio = "", $fechaFin = "")
	{
		$estado = limpiar($estado);
		$condicionFecha = "";
		if (!empty($fechaInicio) && !empty($fechaFin)) {
			$fechaInicioFormatted = date('Ymd', strtotime($fechaInicio)) . "000000";
			$fechaFinFormatted = date('Ymd', strtotime($fechaFin)) . "235959";
			$condicionFecha = " AND fechaCreada BETWEEN '$fechaInicioFormatted' AND '$fechaFinFormatted' ";
		}

		$sql = self::sentencia("SELECT COUNT(*) as cantidad FROM datosFacturacion D WHERE estadoPago = '$estado' AND EXISTS(SELECT idfacturacion FROM comisiones C WHERE C.idfacturacion=D.datosfacturacionid) $condicionFecha LIMIT 1");
		return !empty($sql) ? array_shift($sql) : false;
	}



	// public function obtenerRegistrosSubidosPago($posisionDePagina,$registrosPorPagina){
	// 	$sql=self::sentencia("SELECT datosfacturacionid,D.idcliente,D.idvehiculos,D.idcanal,tipoDocumentoFacturacion, rucFacturacion,dniFacturacion, razonSocialFacturacion,numeroBoleta,numeroFactura,idUsuarioCreado,fechaCreada,estadoPago FROM datosFacturacion D WHERE (estadoPago = '0' OR estadoPago ='62')  AND EXISTS(SELECT idfacturacion FROM comisiones C WHERE C.idfacturacion=D.datosfacturacionid) LIMIT $posisionDePagina, $registrosPorPagina ");
	// 	return !empty($sql)? $sql:false;		
	// }
	// public function obtenerRegistrosSubidosEPago($posisionDePagina,$registrosPorPagina,$estadoPago){
	// 	$estadoPago=limpiar($estadoPago);
	// 	$sql=self::sentencia("SELECT datosfacturacionid,D.idcliente,D.idvehiculos,D.idcanal,tipoDocumentoFacturacion, rucFacturacion,dniFacturacion, razonSocialFacturacion,numeroBoleta,numeroFactura,idUsuarioCreado,fechaCreada,estadoPago  FROM datosFacturacion D WHERE estadoPago = '$estadoPago' AND EXISTS(SELECT idfacturacion FROM comisiones C WHERE C.idfacturacion=D.datosfacturacionid) LIMIT $posisionDePagina, $registrosPorPagina ");
	// 	return !empty($sql)? $sql:false;	
	// }
	// public function obtenerRegistrosSubidosPago($posisionDePagina, $registrosPorPagina, $fechaInicio = "", $fechaFin = "")
	// {
	// 	$condicionFecha = "";
	// 	if (!empty($fechaInicio) && !empty($fechaFin)) {
	// 		$condicionFecha = " AND fechaCreada BETWEEN '$fechaInicio' AND '$fechaFin' ";
	// 	}

	// 	$sql = self::sentencia("SELECT datosfacturacionid,D.idcliente,D.idvehiculos,D.idcanal,tipoDocumentoFacturacion, rucFacturacion,dniFacturacion, razonSocialFacturacion,numeroBoleta,numeroFactura,idUsuarioCreado,fechaCreada,estadoPago FROM datosFacturacion D WHERE (estadoPago = '0' OR estadoPago ='62') $condicionFecha AND EXISTS(SELECT idfacturacion FROM comisiones C WHERE C.idfacturacion=D.datosfacturacionid) LIMIT $posisionDePagina, $registrosPorPagina ");
	// 	return !empty($sql) ? $sql : false;
	// }
	// public function obtenerRegistrosSubidosEPago($posisionDePagina, $registrosPorPagina, $estadoPago, $fechaInicio = "", $fechaFin = "")
	// {
	// 	$condicionFecha = "";
	// 	if (!empty($fechaInicio) && !empty($fechaFin)) {
	// 		$condicionFecha = " AND fechaCreada BETWEEN '$fechaInicio' AND '$fechaFin' ";
	// 	}

	// 	$estadoPago = limpiar($estadoPago);
	// 	$sql = self::sentencia("SELECT datosfacturacionid,D.idcliente,D.idvehiculos,D.idcanal,tipoDocumentoFacturacion, rucFacturacion,dniFacturacion, razonSocialFacturacion,numeroBoleta,numeroFactura,idUsuarioCreado,fechaCreada,estadoPago  FROM datosFacturacion D WHERE estadoPago = '$estadoPago' $condicionFecha AND EXISTS(SELECT idfacturacion FROM comisiones C WHERE C.idfacturacion=D.datosfacturacionid) LIMIT $posisionDePagina, $registrosPorPagina ");
	// 	return !empty($sql) ? $sql : false;
	// }
	public function obtenerRegistrosSubidosPago($posisionDePagina, $registrosPorPagina, $fechaInicio, $fechaFin)
	{
		$condicionFecha = "";
		if (!empty($fechaInicio) && !empty($fechaFin)) {
			$fechaInicioFormatted = date('Ymd', strtotime($fechaInicio)) . "000000"; // Añadimos hora 00:00:00
			$fechaFinFormatted = date('Ymd', strtotime($fechaFin)) . "235959"; // Añadimos hora 23:59:59
			$condicionFecha = " AND fechaCreada BETWEEN '$fechaInicioFormatted' AND '$fechaFinFormatted' ";
		}

		$sql = self::sentencia("SELECT datosfacturacionid,D.idcliente,D.idvehiculos,D.idcanal,tipoDocumentoFacturacion, rucFacturacion,
		dniFacturacion, razonSocialFacturacion,numeroBoleta,numeroFactura,idUsuarioCreado,fechaCreada,estadoPago 
		FROM datosFacturacion D WHERE (estadoPago = '0' OR estadoPago ='62') $condicionFecha 
		AND EXISTS(SELECT idfacturacion FROM comisiones C WHERE C.idfacturacion=D.datosfacturacionid) 
		LIMIT $posisionDePagina, $registrosPorPagina ");
		//var_dump($sql);
		return !empty($sql) ? $sql : false;
	}

	public function obtenerRegistrosSubidosEPago($posisionDePagina, $registrosPorPagina, $estadoPago, $fechaInicio, $fechaFin)
	{
		$condicionFecha = "";
		if (!empty($fechaInicio) && !empty($fechaFin)) {
			$fechaInicioFormatted = date('Ymd', strtotime($fechaInicio)) . "000000"; // Añadimos hora 00:00:00
			$fechaFinFormatted = date('Ymd', strtotime($fechaFin)) . "235959"; // Añadimos hora 23:59:59
			$condicionFecha = " AND fechaCreada BETWEEN '$fechaInicioFormatted' AND '$fechaFinFormatted' ";
		}

		$estadoPago = limpiar($estadoPago);
		$sql = self::sentencia("SELECT datosfacturacionid,D.idcliente,D.idvehiculos,D.idcanal,tipoDocumentoFacturacion, rucFacturacion,dniFacturacion, razonSocialFacturacion,numeroBoleta,numeroFactura,idUsuarioCreado,fechaCreada,estadoPago  FROM datosFacturacion D WHERE estadoPago = '$estadoPago' $condicionFecha AND EXISTS(SELECT idfacturacion FROM comisiones C WHERE C.idfacturacion=D.datosfacturacionid) LIMIT $posisionDePagina, $registrosPorPagina ");
		//var_dump($sql);
		return !empty($sql) ? $sql : false;
	}

	public function obtenerRegistrosSubidosEPagoAutoespar($posisionDePagina, $registrosPorPagina, $estadoPago, $fechaInicio, $fechaFin)
	{
		$condicionFecha = "";
		if (!empty($fechaInicio) && !empty($fechaFin)) {
			$fechaInicioFormatted = date('Ymd', strtotime($fechaInicio)) . "000000"; // Añadimos hora 00:00:00
			$fechaFinFormatted = date('Ymd', strtotime($fechaFin)) . "235959"; // Añadimos hora 23:59:59
			$condicionFecha = " AND fechaCreada BETWEEN '$fechaInicioFormatted' AND '$fechaFinFormatted' ";
		}

		$estadoPago = limpiar($estadoPago);
		$sql = self::sentencia("SELECT datosfacturacionid,D.idcliente,D.idvehiculos,D.idcanal,tipoDocumentoFacturacion, rucFacturacion,dniFacturacion, razonSocialFacturacion,numeroBoleta,numeroFactura,idUsuarioCreado,fechaCreada,estadoPago  FROM datosFacturacion D WHERE estadoPago = '$estadoPago' $condicionFecha LIMIT $posisionDePagina, $registrosPorPagina ");
		//var_dump($sql);
		return !empty($sql) ? $sql : false;
	}

	public function obtenerRegistrosSubidosPagoExcel($posisionDePagina, $registrosPorPagina, $fechaInicio, $fechaFin)
	{
		$condicionFecha = "";
		if (!empty($fechaInicio) && !empty($fechaFin)) {
			$fechaInicioFormatted = date('Ymd', strtotime($fechaInicio)) . "000000"; // Añadimos hora 00:00:00
			$fechaFinFormatted = date('Ymd', strtotime($fechaFin)) . "235959"; // Añadimos hora 23:59:59
			$condicionFecha = " AND fechaCreada BETWEEN '$fechaInicioFormatted' AND '$fechaFinFormatted' ";
		}

		$sql = self::sentencia("SELECT datosfacturacionid,D.idcliente,D.idvehiculos,D.idcanal,tipoDocumentoFacturacion, rucFacturacion,dniFacturacion, razonSocialFacturacion,numeroBoleta,numeroFactura,idUsuarioCreado,fechaCreada,estadoPago FROM datosFacturacion D WHERE (estadoPago = '0' OR estadoPago ='62') $condicionFecha AND EXISTS(SELECT idfacturacion FROM comisiones C WHERE C.idfacturacion=D.datosfacturacionid)");
		//var_dump($sql);
		return !empty($sql) ? $sql : false;
	}

	public function obtenerRegistrosSubidosEPagoExcel($posisionDePagina, $registrosPorPagina, $estadoPago, $fechaInicio, $fechaFin)
	{
		$condicionFecha = "";
		if (!empty($fechaInicio) && !empty($fechaFin)) {
			$fechaInicioFormatted = date('Ymd', strtotime($fechaInicio)) . "000000"; // Añadimos hora 00:00:00
			$fechaFinFormatted = date('Ymd', strtotime($fechaFin)) . "235959"; // Añadimos hora 23:59:59
			$condicionFecha = " AND fechaCreada BETWEEN '$fechaInicioFormatted' AND '$fechaFinFormatted' ";
		}

		$estadoPago = limpiar($estadoPago);
		$sql = self::sentencia("SELECT datosfacturacionid,D.idcliente,D.idvehiculos,D.idcanal,tipoDocumentoFacturacion, rucFacturacion,dniFacturacion, razonSocialFacturacion,numeroBoleta,numeroFactura,idUsuarioCreado,fechaCreada,estadoPago  FROM datosFacturacion D WHERE estadoPago = '$estadoPago' $condicionFecha AND EXISTS(SELECT idfacturacion FROM comisiones C WHERE C.idfacturacion=D.datosfacturacionid)");
		//var_dump($sql);
		return !empty($sql) ? $sql : false;
	}

	public function obtenerRegistrosSubidosEPagoExcelAutoespar($posisionDePagina = '', $registrosPorPagina = '', $estadoPago, $fechaInicio, $fechaFin)
	{
		$condicionFecha = "";
		if (!empty($fechaInicio) && !empty($fechaFin)) {
			$fechaInicioFormatted = date('Ymd', strtotime($fechaInicio)) . "000000"; // Añadimos hora 00:00:00
			$fechaFinFormatted = date('Ymd', strtotime($fechaFin)) . "235959"; // Añadimos hora 23:59:59
			$condicionFecha = " AND fechaCreada BETWEEN '$fechaInicioFormatted' AND '$fechaFinFormatted' ";
		}

		$estadoPago = limpiar($estadoPago);
		$sql = self::sentencia("SELECT datosfacturacionid,D.idcliente,D.idvehiculos,D.idcanal,tipoDocumentoFacturacion, rucFacturacion,dniFacturacion, razonSocialFacturacion,numeroBoleta,numeroFactura,idUsuarioCreado,fechaCreada,estadoPago  FROM datosFacturacion D WHERE estadoPago = '$estadoPago' $condicionFecha");
		//var_dump($sql);
		return !empty($sql) ? $sql : false;
	}

	public function buscarParaFiltroPago($datos)
	{
		global $session;
		global $db;
		$datos = $db->escapeString($datos);
		$buscarParaFiltroPago = self::sentencia("SELECT datosfacturacionid,D.idcliente,D.idvehiculos,D.idcanal,
		tipoDocumentoFacturacion, rucFacturacion,dniFacturacion, razonSocialFacturacion,numeroBoleta,numeroFactura,
		idUsuarioCreado,D.fechaCreada,estadoPago  FROM datosFacturacion D 
		LEFT JOIN  clientes Cl ON Cl.idcliente=D.idcliente  
		WHERE  (D.razonSocialFacturacion LIKE '$datos' OR D.numeroBoleta LIKE '$datos' OR D.numeroFactura LIKE '$datos') OR (Cl.nombre LIKE '$datos' OR Cl.razonSocial LIKE '$datos' OR Cl.apellidosC LIKE '$datos' OR CONCAT_WS('',Cl.nombre,Cl.apellidosC) LIKE '$datos')");
		return !empty($buscarParaFiltroPago) ? $buscarParaFiltroPago : false;
	}
	public function buscarParaFiltroEPago($datos, $estadoPago)
	{
		global $session;
		global $db;
		$datos = $db->escapeString($datos);
		$estadoPago = $db->escapeString($estadoPago);
		$buscarParaFiltroPago = self::sentencia("SELECT datosfacturacionid,D.idcliente,D.idvehiculos,D.idcanal,tipoDocumentoFacturacion, rucFacturacion,dniFacturacion, razonSocialFacturacion,numeroBoleta,numeroFactura,idUsuarioCreado,D.fechaCreada,estadoPago  FROM datosFacturacion D LEFT JOIN  clientes Cl ON Cl.idcliente=D.idcliente  WHERE  ((D.razonSocialFacturacion LIKE '$datos' OR D.numeroBoleta LIKE '$datos' OR D.numeroFactura LIKE '$datos') OR (Cl.nombre LIKE '$datos' OR Cl.razonSocial LIKE '$datos' OR Cl.apellidosC LIKE '$datos' OR CONCAT_WS('',Cl.nombre,Cl.apellidosC) LIKE '$datos')) AND estadoPago='$estadoPago'");
		return !empty($buscarParaFiltroPago) ? $buscarParaFiltroPago : false;
	}
	public function actualizarEstadoPago()
	{

		global $session;
		global $db;
		$this->ultimaActualizacionVerificacion = date("YmdHis");
		$sql = "UPDATE datosFacturacion SET ";
		$sql .= " ultimaActualizacionVerificacion='";
		$sql .= $db->escapeString($this->ultimaActualizacionVerificacion) . "', ";
		$sql .= " idusuarioPagoVerificacion='";
		$sql .= $db->escapeString($session->idusuario) . "', ";
		$sql .= " estadoPago='";
		$sql .= $db->escapeString($this->estadoPago) . "',";
		$sql .= " observacionPagoVerificacion='";
		$sql .= $db->escapeString($this->observacionPagoVerificacion) . "',";
		$sql .= " fechaPagoVerficacion='";
		$sql .= $db->escapeString($this->fechaPagoVerficacion) . "' ";
		$sql .= " WHERE datosfacturacionid='";
		$sql .= $db->escapeString($this->datosfacturacionid) . "'";
		new SeguimientoFacturacion($this->datosfacturacionid, $this);
		return ($db->query($sql)) ? true : false;
	}
	public function actualizarDatosDeEntregaFactura()
	{

		global $db;
		$sql = "UPDATE datosFacturacion SET ";
		$sql .= " entregaEnInstalacion='";
		$sql .= $db->escapeString($this->entregaEnInstalacion) . "', ";
		$sql .= " telefonoFacturacion='";
		$sql .= $db->escapeString($this->telefonoFacturacion) . "', ";
		$sql .= " direccionEntrega='";
		$sql .= $db->escapeString($this->direccionEntrega) . "', ";
		$sql .= " referenciaEntrega='";
		$sql .= $db->escapeString($this->referenciaEntrega) . "', ";
		$sql .= " distritoEntrega='";
		$sql .= $db->escapeString($this->distritoEntrega) . "', ";
		$sql .= " recibidoPor='";
		$sql .= $db->escapeString($this->recibidoPor) . "', ";
		$sql .= " fechaHoraDeEntrega='";
		$sql .= $db->escapeString($this->fechaHoraDeEntrega) . "' ";
		$sql .= " WHERE = datosfacturacionid='";
		$sql .= $db->escapeString($this->datosfacturacionid) . "' ";
		return ($db->query($sql)) ? true : false;
	}
	public static function horariosSugeridosParaEntregaDeFactura()
	{
		$sqlEntrega = self::sentencia("SELECT DISTINCT(fecha) FROM sugerenciaDeFechaEntrega WHERE fecha<>'' OR fecha IS NOT NULL");
		return !empty($sqlEntrega) ? $sqlEntrega : false;
	}
	private static function obtenerSiYaEstaRegistrado($fecha)
	{
		global $db;
		$fecha = limpiar($fecha);
		$fecha = $db->escapeString($fecha);
		$sql = self::sentencia("SELECT fecha FROM sugerenciaDeFechaEntrega WHERE fecha='$fecha' LIMIT 1");
		return !empty($sql) ? true : false;
	}
	public function registrarSugerenciaDeFechaEntrega()
	{
		global $db;
		$this->fecha = strtoupper($this->fecha);
		if ($this->fecha != '') {
			if (!self::obtenerSiYaEstaRegistrado($this->fecha)) {
				$sql = "INSERT INTO sugerenciaDeFechaEntrega ";
				$sql .= "(fecha) VALUES ('";
				$sql .= $db->escapeString($this->fecha) . "')";
				return ($db->query($sql)) ? true : false;
			} else {
				return false;
			}
		} else {
			return false;
		}

	}
	public function obtenerRegistrosSubidosParaEntrega()
	{
		$sql = self::sentencia("SELECT COUNT(*) as cantidad FROM datosFacturacion WHERE  entregaEnInstalacion='' AND numeroFactura <> '' LIMIT 1");
		return !empty($sql) ? array_shift($sql) : false;
	}
	public function obtenerRegistrosSubidosCantidadParaEntregaConEstado($estado)
	{
		$estado = limpiar($estado);
		$sql = self::sentencia("SELECT COUNT(*) as cantidad FROM datosFacturacion  WHERE estadoPago = '$estado' AND entregaEnInstalacion='' AND numeroFactura <> '' LIMIT 1 ");
		return !empty($sql) ? array_shift($sql) : false;
	}

	public function obtenerRegistrosSubidosEntregaDeFacturas($posisionDePagina, $registrosPorPagina)
	{
		$sql = self::sentencia("SELECT datosfacturacionid,idUsuarioCreado, numeroFactura,razonSocialFacturacion, fechaVerificacion,costo,distritoEntrega, estadoEntrega FROM  datosFacturacion  WHERE   entregaEnInstalacion='' AND numeroFactura <> '' ORDER BY  datosfacturacionid DESC LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($sql) ? $sql : false;
	}

	public function obtenerRegistrosSubidosEentregaDeFacturas($posisionDePagina, $registrosPorPagina, $estadoPago)
	{
		$estadoPago = limpiar($estadoPago);
		$sql = self::sentencia("SELECT datosfacturacionid, idUsuarioCreado, numeroFactura,razonSocialFacturacion, fechaVerificacion,costo,distritoEntrega, estadoEntrega  FROM datosFacturacion  WHERE estadoEntrega = '$estadoPago' AND entregaEnInstalacion='' AND numeroFactura <> '' ORDER BY datosfacturacionid  DESC LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($sql) ? $sql : false;
	}


	public function buscarParaFiltroEntregaDeFacturas($datos)
	{
		global $session;
		global $db;
		$datos = $db->escapeString($datos);
		$buscarParaFiltroEntregaF = self::sentencia("SELECT datosfacturacionid,idUsuarioCreado, numeroFactura,razonSocialFacturacion, fechaVerificacion,costo,distritoEntrega, estadoEntrega  FROM datosFacturacion LEFT JOIN usuarios ON (datosFacturacion.idUsuarioCreado=usuarios.idusuario) WHERE  (razonSocialFacturacion LIKE '%$datos%' OR numeroFactura LIKE '$datos' OR numeroBoleta LIKE '$datos' OR distritoEntrega LIKE '$datos' OR usuarios.usuarioNombre LIKE '$datos'OR usuarios.usuarioApellidoPaterno LIKE '$datos')  AND (numeroFactura <> '' OR numeroBoleta <> '') LIMIT 15");
		return !empty($buscarParaFiltroEntregaF) ? $buscarParaFiltroEntregaF : false;

	}
	public function buscarParaFiltroEdeFacturas($datos, $estadoPago)
	{
		global $session;
		global $db;
		$datos = $db->escapeString($datos);
		$estadoPago = $db->escapeString($estadoPago);
		$buscarParaFiltroEFacts = self::sentencia("SELECT datosfacturacionid,idUsuarioCreado, numeroFactura,razonSocialFacturacion, fechaVerificacion,costo,distritoEntrega ,estadoEntrega  FROM datosFacturacion LEFT JOIN usuarios ON (datosFacturacion.idUsuarioCreado=usuarios.idusuario) WHERE  ((razonSocialFacturacion LIKE '$datos' OR numeroBoleta LIKE '$datos' OR numeroFactura LIKE '$datos' OR usuarios.usuarioNombre LIKE '$datos'OR usuarios.usuarioApellidoPaterno LIKE '$datos' OR distritoEntrega LIKE '$datos' )  AND estadoEntrega='$estadoPago' AND numeroFactura <> '' AND entregaEnInstalacion='')");
		return !empty($buscarParaFiltroEFacts) ? $buscarParaFiltroEFacts : false;

	}
	public function actualizarEntregaDeFacturas()
	{

		global $session;
		global $db;
		$sql = "UPDATE datosFacturacion SET ";
		$sql .= " fechaHoraDeEntregaRegistro='";
		$sql .= $db->escapeString($this->fechaHoraDeEntregaRegistro) . "', ";
		$sql .= " distritoEntrega='";
		$sql .= $db->escapeString($this->distritoEntrega) . "',";
		$sql .= " direccionEntrega='";
		$sql .= $db->escapeString($this->direccionEntrega) . "',";
		$sql .= " fechaHoraDeEntrega='";
		$sql .= $db->escapeString($this->fechaHoraDeEntrega) . "',";
		$sql .= " referenciaEntrega='";
		$sql .= $db->escapeString($this->referenciaEntrega) . "',";
		$sql .= " recibidoPor='";
		$sql .= $db->escapeString($this->recibidoPor) . "',";
		$sql .= " telefonoFacturacion='";
		$sql .= $db->escapeString($this->telefonoFacturacion) . "',";
		$sql .= " estadoEntrega='";
		$sql .= $db->escapeString($this->estadoEntrega) . "',";
		$sql .= " identrega='";
		$sql .= $session->idusuario . "', ";
		$sql .= " observacionEntrega='";
		$sql .= $db->escapeString($this->observacionEntrega) . "'";
		$sql .= " WHERE datosfacturacionid='";
		$sql .= $db->escapeString($this->datosfacturacionid) . "' ";
		new SeguimientoFacturacion($this->datosfacturacionid, $this);
		return ($db->query($sql)) ? true : false;
	}
	public static function obtenerDatosParElExel($idestadoEntrega, $desde, $hasta)
	{
		if (is_numeric($idestadoEntrega)) {
			$exdata = self::sentencia("SELECT * FROM datosFacturacion WHERE (estadoEntrega='$idestadoEntrega' AND fechaVerificacion BETWEEN '$desde' AND '$hasta')   AND numeroFactura <> '' AND entregaEnInstalacion = ''");
			return !empty($exdata) ? $exdata : false;
		} else {
			$exdata = self::sentencia("SELECT * FROM datosFacturacion WHERE (fechaVerificacion BETWEEN '$desde' AND '$hasta')   AND numeroFactura <> '' AND  (fechaHoraDeEntrega='' or distritoEntrega ='' OR recibidoPor='' ) ");
			return !empty($exdata) ? $exdata : false;
		}
	}


	public function obtenerDatosParaFacturacionCantidad()
	{
		$obtenerDatosFacturacion = self::sentencia("SELECT COUNT(*) as cantidad FROM datosFacturacion WHERE estadoFacturacion='$this->estadoFacturacion'  LIMIT 1");
		return !empty($obtenerDatosFacturacion) ? array_shift($obtenerDatosFacturacion) : false;
	}
	public function obtenerDatosParaFacturacionCantidadRANGO($desde, $hasta)
	{
		$obtenerDatosFacturacion = self::sentencia("SELECT COUNT(*) as cantidad FROM datosFacturacion WHERE  estadoFacturacion='$this->estadoFacturacion' AND fechaCreada BETWEEN '$desde' AND '$hasta' LIMIT 1");
		return !empty($obtenerDatosFacturacion) ? array_shift($obtenerDatosFacturacion) : false;
	}

	public function obtenerDatosParaFacturacion($posisionDePagina, $registrosPorPagina)
	{
		$obtenerDatosFacturacion = self::sentencia("SELECT * FROM datosFacturacion WHERE estadoFacturacion='$this->estadoFacturacion' ORDER BY fechaCreada DESC LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($obtenerDatosFacturacion) ? $obtenerDatosFacturacion : false;
	}

	public function obtenerDatosSG()
	{
		$obtenerDatosFacturacion = self::sentencia("SELECT * FROM datosFacturacion WHERE seguimiento=1 ORDER BY fechaCreada DESC ");
		return !empty($obtenerDatosFacturacion) ? $obtenerDatosFacturacion : false;
	}

	public function obtenerDatosParaFacturacionRango($posisionDePagina, $registrosPorPagina, $desde, $hasta)
	{
		$obtenerDatosFacturacion = self::sentencia("SELECT * FROM datosFacturacion WHERE estadoFacturacion='$this->estadoFacturacion' AND fechaCreada BETWEEN '$desde' AND '$hasta' ORDER BY fechaCreada DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($obtenerDatosFacturacion) ? $obtenerDatosFacturacion : false;
	}

	public function buscarParaFiltroDatosFacturacion($datos)
	{
		global $session;
		global $db;
		$datos = $db->escapeString($datos);
		$buscarParaFiltroEntregaF = self::sentencia("SELECT datosFacturacion.* FROM datosFacturacion left join renovaciones on (renovaciones.idrenovacion= datosFacturacion.idrenovacion) left join clientes on (clientes.idcliente=datosFacturacion.idcliente)  WHERE estadoFacturacion='$this->estadoFacturacion' AND (renovaciones.nombreCliente like '$datos' or orden LIKE '$datos' OR tipoDocumentoFacturacion LIKE '$datos' OR rucFacturacion LIKE '$datos' OR dniFacturacion LIKE '$datos' OR razonSocialFacturacion LIKE '$datos' OR apunte LIKE '$datos' OR numeroBoleta LIKE '$datos' OR numeroFactura LIKE '$datos' or clientes.apellidosC like '$datos' or clientes.razonSocial like '$datos' or clientes.dni like '$datos' or clientes.ruc like '$datos')   limit 20");
		return !empty($buscarParaFiltroEntregaF) ? $buscarParaFiltroEntregaF : false;
	}

	public function buscarParaFiltroDatosFacturacionFecha($datos, $desde, $hasta)
	{
		global $session;
		global $db;
		$datos = $db->escapeString($datos);
		$buscarParaFiltroEntregaF = self::sentencia("SELECT * FROM datosFacturacion WHERE estadoFacturacion='$this->estadoFacturacion' AND fechaCreada BETWEEN '$desde' AND '$hasta' AND (orden LIKE '$datos' OR tipoDocumentoFacturacion LIKE '$datos' OR rucFacturacion LIKE '$datos' OR dniFacturacion LIKE '$datos' OR razonSocialFacturacion LIKE '$datos' OR apunte LIKE '$datos' OR numeroBoleta LIKE '$datos' OR numeroFactura LIKE '$datos')");
		return !empty($buscarParaFiltroEntregaF) ? $buscarParaFiltroEntregaF : false;

	}

	public function obtenerFacturaciones($desde, $hasta)
	{
		$obteConvenios = self::sentencia("SELECT * FROM datosFacturacion WHERE estadoFacturacion='$this->estadoFacturacion' AND (fechaCreada BETWEEN '$desde' AND '$hasta')");
		return !empty($obteConvenios) ? $obteConvenios : false;

	}


	public function obtenerReportFactura($desde, $hasta)
	{
		$nsql = " SELECT case when df.orden='' then 'SAC aun no asigna' else df.orden end orden , 
			 concat(df.tipoDocumentoFacturacion,' ',case when df.fechaVerificacion then case when df.numeroBoleta!='' then df.numeroBoleta else df.numeroFactura end else ' ' end ) as detalles, 
			 CASE WHEN v.placa is not null and v.chasis is not null then concat('placa: ',v.placa,' chasis: ',v.chasis)  
			 when v.placa is null and v.chasis is not null  then concat('placa:',df.razonSocialFacturacion,' chasis:',v.chasis) 
			 when v.placa is null and v.chasis is null  then concat('placa:',df.razonSocialFacturacion,' chasis:') 
			 when v.placa is not null and v.chasis is null  then concat('placa:',v.placa,' chasis:') 
			 end vehiculo, 
			 ifnull(CASE WHEN cl.tipoCliente='Natural' then concat('DNI: ',cl.dni,' nombres: ',cl.nombre ,' ',cl.apellidosC)  
			 ELSE concat('RUC',cl.ruc,' Razon social: ',cl.razonSocial) 
			 end,df.razonSocialFacturacion) cliente,  concat(df.observaciones , '  -- ' ,df.apunte) as observaciones, 
			 concat(u.usuarioNombre,' ',u.usuarioApellidoPaterno) as ejecutivo, 
			 DATE_FORMAT(df.fechaCreada,'%d-%m-%Y %H:%i') as fechacreada ,
			 (SELECT e.estado FROM estados e where e.codigo=v.estado) estadoOperacion 
			 FROM datosFacturacion df 
			 left JOIN vehiculos v on v.idvehiculo=df.idvehiculos 
			 left JOIN clientes cl on cl.idcliente =df.idcliente  
			 LEFT JOIN usuarios u on u.idusuario =df.idUsuarioCreado 
			 WHERE df.estadoFacturacion='28' 
			 AND DATE_FORMAT(df.fechaCreada,'%Y%m%d') BETWEEN '$desde' AND '$hasta' ";
		//echo $nsql;
		$obteConvenios = self::sentencia($nsql);

		return !empty($obteConvenios) ? $obteConvenios : false;

	}


	public function guardarComoFacturarFindeMes()
	{
		new SeguimientoFacturacion($this->datosfacturacionid, $this);
		global $db;
		$sql = "UPDATE datosFacturacion SET apunte ='";
		$sql .= $this->apunte . "', ";
		$sql .= " estadoFacturacion='77' WHERE datosfacturacionid='";
		$sql .= $this->datosfacturacionid . "'";
		return ($db->query($sql)) ? true : false;
	}

	public function obtenerDatosParaCOMHCantidad()
	{
		global $session;
		$comH = self::sentencia("SELECT COUNT(*) as cantidad FROM datosFacturacion WHERE idUsuarioCreado='$session->idusuario'  LIMIT 1");
		return !empty($comH) ? array_shift($comH) : false;
	}

	public function obtenerDatosParaCOMH($posisionDePagina, $registrosPorPagina)
	{
		global $session;
		$comHDs = self::sentencia("SELECT * FROM datosFacturacion WHERE idUsuarioCreado='$session->idusuario' ORDER BY fechaCreada DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($comHDs) ? $comHDs : false;
	}
	public function obtenerDatosParaCOMHCantidadRANGO($desde, $hasta)
	{
		global $session;
		$comcomhr = self::sentencia("SELECT COUNT(*) as cantidad FROM datosFacturacion WHERE  idUsuarioCreado='$session->idusuario' AND (fechaCreada BETWEEN '$desde' AND '$hasta') LIMIT 1");
		return !empty($comcomhr) ? array_shift($comcomhr) : false;
	}

	public function obtenerDatosParaCOMHRango($posisionDePagina, $registrosPorPagina, $desde, $hasta)
	{
		global $session;
		$comOPERD = self::sentencia("SELECT * FROM datosFacturacion WHERE idUsuarioCreado='$session->idusuario' AND (fechaCreada BETWEEN '$desde' AND '$hasta') ORDER BY fechaCreada DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($comOPERD) ? $comOPERD : false;
	}

	public function buscarParaFiltroDatosOPEC($datos)
	{
		global $session;
		global $db;
		$datos = $db->escapeString($datos);
		$buscarParaFiltroEntregaOPC = self::sentencia("SELECT * FROM datosFacturacion DF INNER JOIN clientes CL ON (DF.idcliente=CL.idcliente) INNER JOIN vehiculos VH ON(DF.idvehiculos=VH.idvehiculo) WHERE idUsuarioCreado='$session->idusuario' AND (DF.razonSocialFacturacion LIKE '$datos' OR DF.plan LIKE '$datos' OR CL.nombre LIKE '$datos' OR CL.apellidosC LIKE '$datos' OR CL.razonSocial LIKE '$datos' OR VH.placa LIKE '$datos' OR VH.chasis LIKE '$datos')");
		return !empty($buscarParaFiltroEntregaOPC) ? $buscarParaFiltroEntregaOPC : false;
	}
	public static function obtenerVentasCanales($desde, $hasta)
	{
		$ovcanal = self::sentencia("SELECT * FROM datosFacturacion WHERE fechaCreada BETWEEN '$desde' AND '$hasta'");
		return !empty($ovcanal) ? $ovcanal : false;
	}
	public static function obtenerVentasPacifico($desde, $hasta)
	{
		$ovcanal = self::sentencia("SELECT DF.* FROM datosFacturacion DF INNER JOIN vehiculos V ON (DF.idvehiculos=V.idvehiculo AND V.companiaSeguro LIKE '%pacifico%') INNER JOIN operaciones O ON (O.datosfacturacionid = DF.datosfacturacionid AND DATE_FORMAT(O.fechaInstalacion,'%Y%m%d%H%i') BETWEEN '$desde' AND '$hasta') AND (O.estado='15' OR O.estado='99')");
		return !empty($ovcanal) ? $ovcanal : false;
	}
	public function marcarComoComisionado()
	{
		global $db;
		$datosFact = self::obtenerDatosFacturacionPorId($this->datosfacturacionid);
		if ($datosFact->voucher == 'q@g03n(@j@.png' or $datosFact->voucher == '') {
			$fPago = $datosFact->fechaCreada;
		} else {
			$fPago = substr($datosFact->voucher, 0, 14);
		}
		$da = Renovaciones::obtenerDatosDeRenovacionPorId($this->idrenovacion);

		if ($this->idrenovacion == 0) {
			$ideje = $datosFact->idUsuarioCreado;
		} else {

			$ideje = $da->idejecutivo;
		}


		$nFact = (!empty($datosFact->numeroBoleta)) ? $datosFact->numeroBoleta : $datosFact->numeroFactura;
		$sql = "INSERT INTO comisiones (idcliente,idvehiculo,idfacturacion,tiempo,montoTotal,fechapago,documentoFacturacion,numeroFacturacion,fechaRegistro,idCreador,estadoFacturacion,idFacturador,type_commission,trabajo,idrenovacion,fechaFacturador) VALUES ('";
		$sql .= $datosFact->idcliente . "','";
		$sql .= $datosFact->idvehiculos . "','";
		$sql .= $datosFact->datosfacturacionid . "','";
		$sql .= $datosFact->tiempo . "','";
		$sql .= $datosFact->costo . "','";
		$sql .= $fPago . "','";
		$sql .= $datosFact->tipoDocumentoFacturacion . "','";
		$sql .= $nFact . "','";
		$sql .= $datosFact->fechaCreada . "','";
		$sql .= $ideje . "','";
		$sql .= $datosFact->estadoFacturacion . "','";
		$sql .= $datosFact->idVerificador . "','";
		$sql .= $this->type_commission . "','";
		$sql .= $this->trabajo . "','";
		$sql .= $this->idrenovacion . "','";
		$sql .= $datosFact->fechaVerificacion . "')";
		new SeguimientoFacturacion($this->datosfacturacionid, $this);
		return ($db->query($sql)) ? true : false;
	}
	public function marcarComoAnulado($ultimoid, $idopera)
	{
		global $db;
		$datosOld = new SeguimientoConsulta();
		$dOld = $datosOld->consultaSeg("datosFacturacion", $this->datosfacturacionid, "datosfacturacionid");
		$dOldd = $dOld;
		$marcQ = "UPDATE datosFacturacion SET estadoFacturacion='18', estadoPago = '18' , estadoEntrega = '18', apunte='$this->apunte'  WHERE datosfacturacionid='$this->datosfacturacionid' ";
		new SeguimientoFacturacion($this->datosfacturacionid, $this);

		if ($db->query($marcQ)) {
			new SeguimientoTablas("datosFacturacion", " Marco Como Anulado", $this->datosfacturacionid, $this, "datosfacturacionid", $dOldd, $ultimoid, $idopera);
			return true;
		} else {
			return false;
		}

		//return ($db->query($marcQ))?true:false;
	}

	public function modificarDesdeAdmimcom($ultimoid, $idopera)
	{
		global $db;
		$datosOld = new SeguimientoConsulta();
		$dold = $datosOld->consultaSeg("datosFacturacion", $this->datosfacturacionid, "datosfacturacionid");
		$doldd = $dold;
		$sqqlAct = "UPDATE datosFacturacion SET costo='$this->costo', tiempo ='$this->tiempo' , plan ='$this->plan' , orden = '$this->orden' , idUsuarioCreado = '$this->idUsuarioCreado' WHERE datosfacturacionid='$this->datosfacturacionid' ";
		new SeguimientoFacturacion($this->datosfacturacionid, $this);
		if ($db->query($sqqlAct)) {
			new SeguimientoTablas("datosFacturacion", " Modifico datos desde admin", $this->datosfacturacionid, $this, "datosfacturacionid", $doldd, $ultimoid, $idopera);
			return true;
		} else {
			return false;
		}
		//  return ($db->query($sqqlAct))?true:false;
	}

	public function seguimientoM()
	{
		global $db;
		$sqqlAct = "UPDATE datosFacturacion SET seguimiento='$this->seguimiento' WHERE datosfacturacionid='$this->datosfacturacionid' ";
		new SeguimientoFacturacion($this->datosfacturacionid, $this);
		return ($db->query($sqqlAct)) ? true : false;
	}
	public function updateOrdenFact()
	{
		global $db;
		$sqqlAct = "UPDATE datosFacturacion SET orden='$this->orden' WHERE datosfacturacionid='$this->datosfacturacionid' ";
		//	new SeguimientoFacturacion($this->datosfacturacionid,$this);
		return ($db->query($sqqlAct)) ? true : false;
	}

	public static function obtenerDatosFacturacion($iddatosfact)
	{
		$obtenerDatosFactQ = self::sentencia("SELECT * FROM datosFacturacion  WHERE datosfacturacionid = '$iddatosfact' LIMIT 1");
		return !empty($obtenerDatosFactQ) ? array_shift($obtenerDatosFactQ) : false;
	}

	public static function obtenerSEguimiento($idobject)
	{
		global $db;
		$obtenerDatosFacturacionPorId = self::sentencia("SELECT register_at as fecha, idusuario as idrenovacion FROM facturacionSeguimiento  WHERE idobject = '" . $db->escapeString($idobject) . "' group by register_at  order by id desc limit 10");
		return !empty($obtenerDatosFacturacionPorId) ? $obtenerDatosFacturacionPorId : false;
	}

	public function modificarDesdeAdmimcomVoucher($ultimoid, $idopera)
	{
		global $db;

		$datosOld = new SeguimientoConsulta();
		$dOld = $datosOld->consultaSeg("datosFacturacion", $this->datosfacturacionid, "datosfacturacionid");
		$dOldd = $dOld;
		$sqqlAct = "UPDATE datosFacturacion SET voucher='$this->voucher' WHERE datosfacturacionid='$this->datosfacturacionid' ";
		new SeguimientoFacturacion($this->datosfacturacionid, $this);
		if ($db->query($sqqlAct)) {
			new SeguimientoTablas("datosFacturacion", " Modifico Voucher desde Admin ", $this->datosfacturacionid, $this, "datosfacturacioni", $dOldd, $ultimoid, $idopera);
			return true;
		} else {
			return false;
		}
		// return ($db->query($sqqlAct))?true:false;
	}

	public function verificarExistenciaDobleDenumeroFactura()
	{
		$ConsQv = self::sentencia("SELECT COUNT(*) as cantidad FROM datosFacturacion WHERE numeroFactura = '$this->numeroFactura'  LIMIT 1");
		return array_shift($ConsQv);
	}
	public function obtenerdobledenumerofactura()
	{
		$ConsQv = self::sentencia("SELECT * FROM datosFacturacion WHERE numeroFactura = '$this->numeroFactura' ");
		return (!empty($ConsQv)) ? $ConsQv : false;
	}
	public function verificarExistenciaDobleDenumeroBoleta()
	{
		$ConsQv = self::sentencia("SELECT COUNT(*) as cantidad FROM datosFacturacion WHERE numeroBoleta = '$this->numeroBoleta'  LIMIT 1");
		return array_shift($ConsQv);
	}
	public function obtenerDobleDenumeroBoleta()
	{
		$ConsQv = self::sentencia("SELECT * FROM datosFacturacion WHERE numeroBoleta  = '$this->numeroBoleta ' ");
		return (!empty($ConsQv)) ? $ConsQv : false;
	}
	public static function obtenerVOucheresEnRAngo($desde, $hasta)
	{
		$datos = self::sentencia("SELECT * FROM datosFacturacion WHERE SUBSTRING(voucher,1,14) BETWEEN '$desde' AND '$hasta' AND voucher<>'' AND voucher <> 'q@g03n(@j@.png' ");
		return !empty($datos) ? $datos : false;
	}

	//Para COBRANZAS------------------------------------------------------------------------------------------------------

	public function datosFacturacionAll($estadoPago, $fechaInicio, $fechaFin, $search, $cargaInit, $tipoFecha)
	{
		$searchTxt = "";
		if (!empty($search)) {
			$searchTxt .= "AND  (D.razonSocialFacturacion LIKE '%$search%' OR 
			D.numeroBoleta LIKE '%$search%' OR 
			D.numeroFactura LIKE '%$search%'OR 
			V.chasis LIKE '%$search%') OR (Cl.nombre LIKE '%$search%' 
			OR Cl.razonSocial LIKE '%$search%' 
			OR Cl.apellidosC LIKE '%$search%'
			OR CONCAT_WS(' ',Cl.nombre,Cl.apellidosC) LIKE '%$search%')";
		}
		$condicionFecha = "";
		if (isset($estadoPago) && $estadoPago == "all") {
			if (!empty($fechaInicio) && !empty($fechaFin)) {

				$tipoColFecha = $tipoFecha == 'fechaCreada' ? 'D.fechaCreada' : 'D.fechaPagoVerficacion';

				$fechaInicioFormatted = date('Ymd', strtotime($fechaInicio)) . "000000"; // Añadimos hora 00:00:00
				$fechaFinFormatted = date('Ymd', strtotime($fechaFin)) . "235959"; // Añadimos hora 23:59:59
				$condicionFecha .= "AND $tipoColFecha BETWEEN '$fechaInicioFormatted' AND '$fechaFinFormatted' ";
			}
		} else {
			$condicionFecha .= "";
		}

		$cargaTxt = "";
		if (!empty($cargaInit)) {
			$cargaTxt .= "LIMIT 0";
		}

		$sql = self::sentencia("SELECT V.chasis, D.datosfacturacionid,D.idcliente,D.idvehiculos,D.idcanal,D.tipoDocumentoFacturacion, D.rucFacturacion, D.dniFacturacion, 
		D.razonSocialFacturacion,D.numeroBoleta,D.numeroFactura,D.idUsuarioCreado,D.fechaCreada,D.fechaPagoVerficacion,D.estadoPago, C.nombreCanal, C.ejecutivoCanal, CO.trabajo,
		U.usuarioNombre, U.usuarioApellidoPaterno, U.usuarioApellidoMaterno,
		CASE
			WHEN tipoDocumentoFacturacion = 'Boleta' THEN (SELECT COUNT(*) FROM datosFacturacion DF WHERE DF.numeroBoleta=D.numeroBoleta AND DF.numeroBoleta!='')
			WHEN tipoDocumentoFacturacion = 'Factura' THEN (SELECT COUNT(*) FROM datosFacturacion DF WHERE DF.numeroFactura=D.numeroFactura AND DF.numeroFactura!='')
		END AS alerta
		FROM datosFacturacion D
		LEFT JOIN canales C ON D.idcanal = C.idcanal
		LEFT JOIN comisiones CO ON D.datosfacturacionid = CO.idfacturacion
		LEFT JOIN vehiculos V ON D.idvehiculos = V.idvehiculo
		LEFT JOIN usuarios U ON D.idusuarioPagoVerificacion = U.idusuario
		LEFT JOIN clientes Cl ON D.idcliente = Cl.idcliente
		WHERE 1=1 
		AND D.estadopago IN ('0','62','71','67','63') $condicionFecha $searchTxt $cargaTxt");
		return !empty($sql) ? $sql : false;
	}
	
	public function datosFacturacionAllFecha($fechaInicio, $fechaFin, $search, $tipoFecha)
	{

		$condicionFecha = "";
		$searchTxt = "";

		if (!empty($fechaInicio) && !empty($fechaFin)) {

			$tipoColFecha = $tipoFecha == 'fechaCreada' ? 'D.fechaCreada' : 'D.fechaPagoVerficacion';
			$fechaInicioFormatted = date('Ymd', strtotime($fechaInicio)) . "000000"; // Añadimos hora 00:00:00
			$fechaFinFormatted = date('Ymd', strtotime($fechaFin)) . "235959"; // Añadimos hora 23:59:59
			$condicionFecha .= "AND $tipoColFecha BETWEEN '$fechaInicioFormatted' AND '$fechaFinFormatted' ";
		}

		if (!empty($search)) {
			$searchTxt .= "AND  (D.razonSocialFacturacion LIKE '%$search%' OR 
			D.numeroBoleta LIKE '%$search%' OR 
			D.numeroFactura LIKE '%$search%'OR 
			V.chasis LIKE '%$search%') OR (Cl.nombre LIKE '%$search%' 
			OR Cl.razonSocial LIKE '%$search%' 
			OR Cl.apellidosC LIKE '%$search%'
			OR CONCAT_WS(' ',Cl.nombre,Cl.apellidosC) LIKE '%$search%')";
		}

		$sql = self::sentencia("SELECT DISTINCT V.chasis, D.datosfacturacionid,D.idcliente,D.idvehiculos,D.idcanal,D.tipoDocumentoFacturacion, D.rucFacturacion, D.dniFacturacion, 
		D.razonSocialFacturacion,D.numeroBoleta,D.numeroFactura,D.idUsuarioCreado,D.fechaCreada,D.fechaPagoVerficacion,D.estadoPago, C.nombreCanal, C.ejecutivoCanal, CO.trabajo, 
		U.usuarioNombre, U.usuarioApellidoPaterno, U.usuarioApellidoMaterno,
		CASE
			WHEN tipoDocumentoFacturacion = 'Boleta' THEN (SELECT COUNT(*) FROM datosFacturacion DF WHERE DF.numeroBoleta=D.numeroBoleta AND DF.numeroBoleta!='')
			WHEN tipoDocumentoFacturacion = 'Factura' THEN (SELECT COUNT(*) FROM datosFacturacion DF WHERE DF.numeroFactura=D.numeroFactura AND DF.numeroFactura!='')
		END AS alerta
		FROM datosFacturacion D
		LEFT JOIN canales C ON D.idcanal = C.idcanal
		INNER JOIN comisiones CO ON D.datosfacturacionid = CO.idfacturacion
		LEFT JOIN vehiculos V ON D.idvehiculos = V.idvehiculo
		LEFT JOIN usuarios U ON D.idusuarioPagoVerificacion = U.idusuario
		LEFT JOIN clientes Cl ON D.idcliente = Cl.idcliente
		WHERE 1=1 
		AND D.estadopago IN ('0','62') $condicionFecha $searchTxt");
		return !empty($sql) ? $sql : false;
	}
	public function paraTodosLosEstados($estadoPago, $fechaInicio, $fechaFin, $search, $tipoFecha)
	{
		$searchTxt = "";
		if (!empty($search)) {
			$searchTxt .= "AND  (D.razonSocialFacturacion LIKE '%$search%' OR 
			D.numeroBoleta LIKE '%$search%' OR 
			D.numeroFactura LIKE '%$search%' OR 
			V.chasis LIKE '%$search%') OR (Cl.nombre LIKE '%$search%' 
			OR Cl.razonSocial LIKE '%$search%' 
			OR Cl.apellidosC LIKE '%$search%'
			OR CONCAT_WS(' ',Cl.nombre,Cl.apellidosC) LIKE '%$search%')";
		}
		// FECHA CREADA
		$condicionFecha = "";
		if (!empty($fechaInicio) && !empty($fechaFin)) {

			$tipoColFecha = $tipoFecha == 'fechaCreada' ? 'D.fechaCreada' : 'D.fechaPagoVerficacion';

			$fechaInicioFormatted = date('Ymd', strtotime($fechaInicio)) . "000000"; // Añadimos hora 00:00:00
			$fechaFinFormatted = date('Ymd', strtotime($fechaFin)) . "235959"; // Añadimos hora 23:59:59
			$condicionFecha = " AND $tipoColFecha BETWEEN '$fechaInicioFormatted' AND '$fechaFinFormatted' ";
		}

		$sql = self::sentencia("SELECT DISTINCT V.chasis, D.datosfacturacionid,D.idcliente,D.idvehiculos,D.idcanal,D.tipoDocumentoFacturacion, D.rucFacturacion,D.dniFacturacion, D.razonSocialFacturacion,D.numeroBoleta,
		D.numeroFactura,D.idUsuarioCreado,D.fechaCreada, D.fechaPagoVerficacion,D.estadoPago,CO.trabajo, C.nombreCanal, C.ejecutivoCanal, U.usuarioNombre, U.usuarioApellidoPaterno, U.usuarioApellidoMaterno,
		CASE
			WHEN D.tipoDocumentoFacturacion = 'Boleta' THEN (SELECT COUNT(*) FROM datosFacturacion DF WHERE DF.numeroBoleta=D.numeroBoleta AND DF.numeroBoleta!='')
			WHEN D.tipoDocumentoFacturacion = 'Factura' THEN (SELECT COUNT(*) FROM datosFacturacion DF WHERE DF.numeroFactura=D.numeroFactura AND DF.numeroFactura!='')
		END AS alerta
		FROM datosFacturacion D
		LEFT JOIN canales C ON D.idcanal = C.idcanal
		INNER JOIN comisiones CO ON D.datosfacturacionid = CO.idfacturacion
		LEFT JOIN vehiculos V ON D.idvehiculos = V.idvehiculo
		LEFT JOIN usuarios U ON D.idusuarioPagoVerificacion = U.idusuario
		LEFT JOIN clientes Cl ON D.idcliente = Cl.idcliente
		WHERE 1=1 
	    AND D.estadoPago = $estadoPago $condicionFecha $searchTxt");
		//var_dump($sql);
		return !empty($sql) ? $sql : false;
	}
}
?>
<?php
class Credito extends Recursos {
    public $id;
    public $creditType;
    public $typeCustomer;
    public $name;
    public $doc;
    public $detailRequest;
    public $sales;
    public $mount;
    public $legal;
    public $docLegal;
    public $bossInvoice;
    public $mailbossInvoice;
    public $phoneBossInvoice;
    public $bossTreasury;
    public $mailBossTreasury;
    public $phoneBossTreasury;
    public $status;
    public $operations;
    public $dats;
    public $obs;

    public $idfact;
    public $ordloc;
    public $idorder;

    public $typedoc;
    public $doccust;
    public $namecust;
    public $vehicle;

    public $typedocF;
    public $doccustF;
    public $namecustF;

    public $invoicenumber;
    public $dateinvo;
    public $item;
    public $price;

    

}

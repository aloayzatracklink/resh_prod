<?php


class SolicitudCredito extends Recursos
{
   public $idsolicre;
   public $idtipoper;
   public $idvendedor;
   public $cliente;
   public $numerodoc;
   public $monto;
   public $noperaciones;
   public $dcredito;
   public $detalle;
   public $respresantelegal;
   public $dni;
   public $jefecompras;
   public $jf_email;
   public $jf_cel;
   public $jefetesoreria;
   public $jt_email;
   public $jt_cel;
   public $img_dni;
   public $img_ficharuc;
   public $img_pdt;
   public $img_ordenservi;
   public $estado;
   public $update_at;
   public $register_at;
   public $estadoEst;
   public $clase;

  // public $idtipoper;
  public $desctipo;


    public static function obtenerSolicitudCredito($idsolicre){
        global $session;

        if($idsolicre==''){
            $idsolicre='';
        }else{
            $idsolicre='  AND idsolicre='.$idsolicre.' ';
        }
        $sql=" SELECT idsolicre, tp.idtipoper,tp.desctipo, idvendedor, cliente, numerodoc, monto, noperaciones, dcredito, detalle, respresantelegal, dni,
                    jefecompras, jf_email, jf_cel, jefetesoreria, jt_email, jt_cel, img_dni, img_ficharuc, img_pdt, img_ordenservi,estado, update_at, register_at
                    FROM solicitud_credito  inner join tipoPersona tp on tp.idtipoper=solicitud_credito.idtipoper
                    where 1=1 and idvendedor=".$session->idusuario." ".$idsolicre." order by register_at asc ";
        $obtenerSolicitudCre=self::sentencia($sql);
        if($idsolicre==''){
            return $obtenerSolicitudCre;
        }else{
            return !empty($obtenerSolicitudCre)?array_shift($obtenerSolicitudCre):false;
        }
    }


    public static function aprobacionSolicitudCredito($idsolicre){
        global $session;

        if($idsolicre==''){
            $idsolicre='';
        }else{
            $idsolicre='  AND idsolicre='.$idsolicre.' ';
        }
        $sql=" SELECT idsolicre, tp.idtipoper,tp.desctipo, idvendedor, cliente, numerodoc, monto, noperaciones, dcredito, detalle, respresantelegal, dni,
                    jefecompras, jf_email, jf_cel, jefetesoreria, jt_email, jt_cel, img_dni, img_ficharuc, img_pdt, img_ordenservi,estado, update_at, register_at
                    FROM solicitud_credito  inner join tipoPersona tp on tp.idtipoper=solicitud_credito.idtipoper
                    where 1=1  ".$idsolicre." order by register_at asc ";
        $obtenerSolicitudCre=self::sentencia($sql);
        if($idsolicre==''){
            return $obtenerSolicitudCre;
        }else{
            return !empty($obtenerSolicitudCre)?array_shift($obtenerSolicitudCre):false;
        }
    }


    public static function obtenerTipoPersona(){
        $obtenertppersona=self::sentencia(" SELECT idtipoper,desctipo FROM tipoPersona ");
        return $obtenertppersona;
    }

    public static function obtenEstado($idestado){
        $obtenerEstdo=self::sentencia("select codigo,estado as estadoEst,clase,desc2 from estados  where codigo='$idestado'");
        return !empty($obtenerEstdo)?($obtenerEstdo):false;
    }
    /**
     * @return mixed
     */
    public function RegistrarSolicitarCredito()
    {
        $this->fechaCreada=date("YmdHis");
        global $db;
        global $session;
        $sql="INSERT INTO solicitud_credito(idtipoper, idvendedor, cliente, numerodoc, monto, noperaciones, dcredito, detalle, respresantelegal, dni, jefecompras,
jf_email, jf_cel, jefetesoreria, jt_email, jt_cel, img_dni, img_ficharuc, img_pdt, img_ordenservi,estado) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString($this->idtipoper)."' , '";
        $sql.= $db->escapeString($this->idvendedor)."' , '";
        $sql.= $db->escapeString(strtoupper($this->cliente))."' , '";
        $sql.= $db->escapeString($this->numerodoc)."' , '";
        $sql.= $db->escapeString($this->monto)."' , '";
        $sql.= $db->escapeString($this->noperaciones)."' , '";
        $sql.= $db->escapeString($this->dcredito)."' , '";
        $sql.= $db->escapeString($this->detalle)."' , '";
        $sql.= $db->escapeString($this->respresantelegal)."' , '";
        $sql.= $db->escapeString($this->dni)."' , '";
        $sql.= $db->escapeString($this->jefecompras)."' , '";
        $sql.= $db->escapeString($this->jf_email)."' , '";
        $sql.= $db->escapeString($this->jf_cel)."' , '";
        $sql.= $db->escapeString($this->jefetesoreria)."' , '";
        $sql.= $db->escapeString($this->jt_email)."' , '";
        $sql.= $db->escapeString($this->jt_cel)."' , '";
        $sql.= $db->escapeString($this->img_dni)."' , '";
        $sql.= $db->escapeString($this->img_ficharuc)."' , '";
        $sql.= $db->escapeString($this->img_pdt)."' , '";
        $sql.= $db->escapeString($this->img_ordenservi)."' , '";
        $sql.= "1')";
        if ($db->query($sql)) {
            return true;
        }else{
            return false;
        }
    }


    public function updateSolicitudCredito(){
        global $db;
        global $session;
        $sql=" UPDATE solicitud_credito SET ";
        $sql.=" idtipoper ='".$db->escapeString($this->idtipoper)."',";
        $sql.=" cliente ='".$db->escapeString($this->cliente)."',";
        $sql.=" numerodoc ='".$db->escapeString($this->numerodoc)."',";
        $sql.=" monto ='".$db->escapeString($this->monto)."',";
        $sql.=" noperaciones ='".$db->escapeString($this->noperaciones)."',";
        $sql.=" dcredito ='".$db->escapeString($this->dcredito)."',";
        $sql.=" detalle ='".$db->escapeString($this->detalle)."',";
        $sql.=" respresantelegal ='".$db->escapeString($this->respresantelegal)."',";
        $sql.=" dni ='".$db->escapeString($this->dni)."', ";
        $sql.=" jefecompras ='".$db->escapeString($this->jefecompras)."', ";
        $sql.=" jf_email ='".$db->escapeString($this->jf_email)."', ";
        $sql.=" jf_cel ='".$db->escapeString($this->jf_cel)."', ";
        $sql.=" jefetesoreria ='".$db->escapeString($this->jefetesoreria)."', ";
        $sql.=" jt_email ='".$db->escapeString($this->jt_email)."', ";
        $sql.=" jt_cel ='".$db->escapeString($this->jt_cel)."', ";
        $sql.=" img_dni ='".$db->escapeString($this->img_dni)."', ";
        $sql.=" img_ficharuc ='".$db->escapeString($this->img_ficharuc)."', ";
        $sql.=" img_pdt ='".$db->escapeString($this->img_pdt)."', ";
        $sql.=" img_ordenservi ='".$db->escapeString($this->img_ordenservi)."' ";
        $sql.=" WHERE idsolicre ='".$db->escapeString($this->idsolicre)."'";
        if ($db->query($sql)) {
            // $this->registrarLiberarTecProvincia();
            return true;
        }else{
            echo "the fail";
        }
    }



}
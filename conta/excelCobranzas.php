<?php
//require '../dompdf/lib/php-svg-lib/autoload';
require __DIR__ . "/../vendor/autoload.php";
//require 'vendor/autoload.php'; // Asegúrate de ajustar esta ruta al archivo autoload.php de Composer en tu proyecto
require_once("../includes/init.php");
function limpiarHTML($string)
{
    return strip_tags($string);
} //Pa' limpiar etiquetas del excel xd

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$fechaInicio = $_GET['fechaInicio'] ?? "";
$fechaFin = $_GET['fechaFin'] ?? "";
$estadoPago = $_GET['estadoPago'] ?? "";

$factObj = new DatosFacturacion();

// Establecer la posición y la cantidad de registros por página
// $numeroDePagina = $_GET['page'] ?? 1;
// $registrosPorPagina = 10; // Puedes ajustar este valor según tus necesidades
// $posisionDePagina = ($numeroDePagina - 1) * $registrosPorPagina;

// if (empty($estadoPago)) {
//     $datos = $factObj->obtenerRegistrosSubidosPagoExcel($posisionDePagina='', $registrosPorPagina='', $fechaInicio, $fechaFin);    
// } else {
//     $datos = $factObj->obtenerRegistrosSubidosEPagoExcel($posisionDePagina='', $registrosPorPagina='', $estadoPago, $fechaInicio, $fechaFin);        
// }

if (empty($estadoPago)) {
    $datos = $factObj->obtenerRegistrosSubidosPagoExcel($posisionDePagina = '', $registrosPorPagina = '', $fechaInicio, $fechaFin);
} elseif ($estadoPago != '71') {
    $datos = $factObj->obtenerRegistrosSubidosEPagoExcel($posisionDePagina = '', $registrosPorPagina = '', $estadoPago, $fechaInicio, $fechaFin);
} else {
    $datos = $factObj->obtenerRegistrosSubidosEPagoExcelAutoespar($posisionDePagina = '', $registrosPorPagina = '', $estadoPago, $fechaInicio, $fechaFin);
}

// Crear un nuevo archivo Excel
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

// Añadir encabezados
$sheet->setCellValue('A1', '#');
$sheet->setCellValue('B1', 'Trabajo');
$sheet->setCellValue('C1', 'Documento facturación');
$sheet->setCellValue('D1', 'N°');
$sheet->setCellValue('E1', 'Nombres');
$sheet->setCellValue('F1', 'Fecha Creada');
$sheet->setCellValue('G1', 'Estado');
$sheet->setCellValue('H1', 'Canal');


// Establecer el texto en negrita para los encabezados
$spreadsheet->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);

// Establecer un color de fondo para los encabezados
$spreadsheet->getActiveSheet()->getStyle('A1:H1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFD3D3D3'); // Color gris claro

// Centrar los encabezados
$spreadsheet->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

// Ajustar el ancho de las columnas según el contenido
foreach (range('A', 'G') as $columnID) {
    $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

$rowNumber = 2;
$contador = 1; // Iniciar contador para números en orden

foreach ($datos as $var) {
    // Añadir número en orden
    $sheet->setCellValue('A' . $rowNumber, $contador);

    // Obtener el trabajo
    $comision = new Comisiones;
    $comision->idfacturacion = $var->datosfacturacionid;
    $dtrab = $comision->verTrabajoComision();
    $trabajo = (is_object($dtrab) && isset($dtrab->trabajo)) ? $dtrab->trabajo : 'N/A';

    // Añadir datos al Excel
    $sheet->setCellValue('B' . $rowNumber, $trabajo);
    $sheet->setCellValue('C' . $rowNumber, $var->tipoDocumentoFacturacion);
    $numero = ($var->tipoDocumentoFacturacion == 'Boleta') ? $var->numeroBoleta : $var->numeroFactura;
    $sheet->setCellValue('D' . $rowNumber, $numero);
    $sheet->setCellValue('E' . $rowNumber, strtoupper($var->razonSocialFacturacion));

    // Limpiamos la fecha
    $fechaLimpia = limpiarHTML(haceMinimo($var->fechaCreada));
    $sheet->setCellValueExplicit('F' . $rowNumber, $fechaLimpia, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);

    // Limpiamos el estado
    $sheet->setCellValue('G' . $rowNumber, limpiarHTML(obtenerEstado($var->estadoPago)));

    if ($var->idcanal != 0) {
        $canalD = Canales::obtenerDatosCanalPorId($var->idcanal);
        $sheet->setCellValue('H' . $rowNumber, $canalD->nombreCanal . " " . $canalD->ejecutivoCanal);
    } else {
        $sheet->setCellValue('H' . $rowNumber, 'Pendiente');
    }

    $rowNumber++;
    $contador++; // Incrementar contador
}

$nombreArchivo = "CobranzasExcel_" . date("Y_m_d_H_i_s") . ".xlsx";

// Enviar el archivo Excel al navegador
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . $nombreArchivo . '"');
$writer = new Xlsx($spreadsheet);
$writer->save('php://output');
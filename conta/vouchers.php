<?php $nav="vouchers" ?>
<link href="../assets/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
<?php require("../includes/head.php") ?>
<link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
} ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->vouchers=="si"): ?>

<?php 
$errores=array();
$echo=array();
$comisionObj=new Comisiones;
    ?>
   <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
   <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
   <link href="../assets/css/sweetalert2.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/jquery-confirm.min.css">
   <style>
    .widget p {
        display: inline-block;        
    }
    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }

    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }

    .reloj .caja-segundos {
        display: inline-block;
    }

    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }</style>
    <body>

        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <?php include("../includes/menuIzquierdo.php") ?>

               </div>
           </nav>

           <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>

                </nav>
            </div>
            <div class="wrapper wrapper-content">
    <div class="row ibox float-e-margins ibox-content ">
                <div class="col-md-12">

                        <div class="row">
                        <div class="col-md-4">
                                <input style="border: none;"  class="cd-select form-control border-bottom" required  type="text" placeholder="Seleccione Rango de fecha" class="form-control" value="" name="fecha" id="rango">
                            </div>    
                        <div class="col-md-offset-4 col-md-4 ">
                           <div class="widget" style="line-height: 0px;       
                           padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px">
                           <div class="fecha">
                            <p id="diaSemana" class="diaSemana">Martes</p>
                            <p id="dia" class="dia">27</p>
                            <p>de </p>
                            <p id="mes" class="mes">Octubre</p>
                            <p>del </p>
                            <p id="year" class="year">2015</p>
                        </div>

                        <div class="reloj">
                            <p id="horas" class="horas">11</p>
                            <p>:</p>
                            <p id="minutos" class="minutos">48</p>
                            <p>:</p>
                            <div class="caja-segundos">
                                <p id="ampm" class="ampm">AM</p>
                                <p id="segundos" class="segundos">12</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox">
        <div class="ibox-title">
            <h5>Vouchers en el rango seleccionado</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </ul>
            <a class="close-link">
                <i class="fa fa-times"></i>
            </a>
        </div>
        </div>
    <div class="ibox-content">
        <div class="lightBoxGallery">
<div id = "resultadoooo"> </div>
    </div>
<div id="blueimp-gallery" class="blueimp-gallery">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
</div>
<?php include("../includes/footer.php") ?>
</div>
</div>
</div>
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
  <script src="../assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>


    <!-- blueimp gallery -->
<script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
<script src="../assets/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<?php include_once('../includes/script.php') ?> 
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
<script>
var fecha='<?= date("Ymd") ?>';          
          $.ajax({
            type : 'POST',
            url  : 'ajax/voucherResAJax',          
            data : {fecha:fecha},
            success : function(data)
            {
             $("#resultadoooo").html(data);
           }
         });
$("#rango").bind("focus",function()
      {
          var fecha=$(this).val();          
            $("#resultadoooo").html('<div class="row" > <div class="col-md-12 text-center"><i class="fa fa-spinner fa-spin fa-5x fa-fw text-center"></i> </div></div>');
          $.ajax({
            type : 'POST',
            url  : 'ajax/voucherResAJax',          
            data : {fecha:fecha},
            success : function(data)
            {
             $("#resultadoooo").html(data);
           }
         });
      });

$('#rango').daterangepicker({
    format: 'DD-MM-YYYY',
    endDate: moment(),
    showDropdowns: true,
    showWeekNumbers: true,
    timePicker: false,
    timePickerIncrement: 1,
    timePicker12Hour: false,
    ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
        'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Anterior Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    opens: 'right',
    drops: 'down',
    buttonClasses: ['btn', 'btn-sm'],
    applyClass: 'btn-primary',
    cancelClass: 'btn-default',
    separator: '/',
    locale: {
        applyLabel: 'Seleccionar',
        cancelLabel: 'Cancelar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Seleccionar Rango',
        daysOfWeek: ['Do', 'Lun', 'Mar', 'Mi', 'Ju', 'Vi','Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
    }
}, function(start, end, label) {               
     // $('#rango').focus();
});  
 $( "#rango" ).change(function() {
    $('#rango').focus();
}); 



    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
<?php else: ?> 
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?> 
<?php endif ?>

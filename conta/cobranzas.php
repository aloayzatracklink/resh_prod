<?php $nav = "cobranzas"; ?>
<?php require("../includes/head.php");
require_once("../includes/funciones.php"); ?>
<?php
// if ($session->idusuario=2) 
//     irA("rprEstadisticas");
?>
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">

<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
} ?>
<?php $obMenu = Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->cobranzas == "si"): ?>

    <?php

    $errores = array();
    $echo = array();
    $comisionObj = new Comisiones;
    //Registramos las alertas aquí ///
    if (isset($_POST['procesar'])) {
        if (Token::verificar($_POST['procesar'])) {
            $facturacionObj = new DatosFacturacion;
            $facturacionObj->estadoPago = limpiar($_POST['estadoPago']);
            $facturacionObj->observacionPagoVerificacion = limpiar($_POST['observaciones']);
            $facturacionObj->datosfacturacionid = $_POST['idFacturacion'];
            $facturacionObj->fechaPagoVerficacion = date("YmdHis");
            if ($facturacionObj->actualizarEstadoPago()) {
                if ($dataComision = Comisiones::verificarExistencia($facturacionObj->datosfacturacionid, $_POST['idvehiculo'])) {
                    $comisionObj->estadoPago = $_POST['estadoPago'];
                    $comisionObj->idcomision = $dataComision->idcomision;
                    if (!$comisionObj->registrarEstadoPago()) {
                        $errores = "Eroro en guardar datos de estado de comision comunicarse inmediatamente con pvasquez@tracklink.pe o el anex 913";
                    }
                }
                $echo[] = "Guardado";
            } else {
                $errores[] = " Error en guardar datos de pago";
            }
            if (!isset($_POST['pagadoOK'])) {
                // if (!$comisionObj->marcarComoPagado()) {
                //     $errores[]="Error en el guardar pago";
                // }else{
                //     $echo[]="Guardado como pagado";
                // }       
            }
        } else {
            $errores[] = " El formulario ya fue procesado, the token :v";
        }
    }
    ?>


    <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
    <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/sweetalert2.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/jquery-confirm.min.css">
    <link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

    <link rel="stylesheet" href="../assets/Datatable/CSS/jquery.dataTables.css">

    <!-- <link rel="stylesheet" href="../assets/Datatable/CSS/bootstrap.min.css"> -->
    <!-- <link rel="stylesheet" href="../assets/Datatable/CSS/dataTables.bootstrap.min.css"> -->
    <link rel="stylesheet" href="../assets/Datatable/CSS/buttons.dataTables.min.css">
    <!--PARA FECHA CALENDARIO-->
    <style>
        .widget p {
            display: inline-block;
        }

        .fecha {
            font-family: Helvetica, Arial;
            text-align: center;
            font-size: 14px;
        }

        .reloj {
            font-family: Helvetica, Arial;
            font-size: 23px;
            text-align: center;
            margin-top: 6px;
        }

        .reloj .caja-segundos {
            display: inline-block;
        }

        .reloj .segundos,
        .reloj .ampm {
            font-size: 10px;
            display: block;
        }
    </style>

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            transition: .4s;
            border-radius: 34px;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            transition: .4s;
            border-radius: 50%;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:checked+.slider:before {
            transform: translateX(26px);
        }

        /*Para ordenar las cosas de arriba xd*/
        .custom-margin {
            margin-bottom: 17px;
        }

        /* .btn-spacing {
                        margin-top: 20px;
                        margin-right: 20px;
                    } */

        #btnBuscar {
            /* margin-left: 20px !important; */
            /* margin-right: 10px !important; */
        }

        #btnLimpiar {
            /* margin-left: 30px !important; */
        }

        .btn-enhanced {
            font-size: 1em;
            /* padding: 10px 20px; */
        }

        .btn-group-custom {
            justify-content: flex-end;
            /* Alinea los botones hacia la derecha */
        }

        .rounded-shadow {
            border-radius: 5px;
            box-shadow: 0 7px 11px rgba(0, 0, 0, 0.1);
        }

        .wrapper-content {
            padding: 10px !important;
        }
    </style>

    <body>

        <style>
            .no-js #loader {
                display: none;
            }

            .js #loader {
                display: block;
                position: absolute;
                left: 100px;
                top: 0;
            }

            .se-pre-con {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
            }
        </style>

        <div class="se-pre-con"></div>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <?php include("../includes/menuIzquierdo.php") ?>

                </div>
            </nav>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <?php include("../includes/navegacionArriba.php") ?>

                    </nav>
                </div>

                <div class="wrapper wrapper-content">
                    <div class="row ibox float-e-margins ibox-content rounded-shadow">
                        <div class="col-md-12">
                            <div class="row custom-margin">
                                <!-- Filtro de Estado de Pago -->
                                <div class="col-md-4">
                                    <label for="estadoPago">ESTADO PAGO</label>
                                    <select id="estadoPago"
                                        class="estadoPago corredor form-control text-capitalize rounded-shadow" required
                                        name="corredor">
                                        <option value="all" selected>[Seleccione...]</option>
                                        <option value="67">Pagado</option>
                                        <option value="63">Pago Parcial</option>
                                        <option value="62">No pagado</option>
                                        <option value="0">Pendientes</option>
                                        <option value="71">Autoespar</option>
                                        <option value="todos">Todos</option>
                                    </select>
                                </div>

                                <!-- Barra de buscador -->
                                <div class="col-md-8">
                                    <label for="filter">Buscador histórico:</label>
                                    <input type="text" class="form-control input-sm m-b-xs rounded-shadow" id="filter"
                                        placeholder="Buscar..." autofocus="">
                                </div>
                            </div>

                            <div class="row custom-margin">
                                <div class="col-md-4">
                                    <label for="filtroFecha">Filtrar por:</label>
                                    <select id="filtroFecha" class="form-control rounded-shadow">
                                        <option value="fechaCreada" selected>Fecha Creada</option>
                                        <option value="fechaVerificacion">Fecha Pago Verificación</option>
                                    </select>
                                </div>

                                <!-- Filtro de fechas -->
                                <div class="col-md-4">
                                    <label for="fechaInicio">Fecha inicio:</label>
                                    <input type="date" id="fechaInicio" name="fechaInicio"
                                        class="form-control rounded-shadow">
                                </div>
                                <div class="col-md-4">
                                    <label for="fechaFin">Fecha fin:</label>
                                    <input type="date" id="fechaFin" name="fechaFin" class="form-control rounded-shadow">
                                </div>
                            </div>

                            <div class="row">
                                <!-- Botón de búsqueda y limpiar -->
                                <div class="col-md text-center btn-group-custom">
                                    <button type="button" id="btnBuscar"
                                        class="btn btn-primary btn-spacing btn-enhanced rounded-shadow">Buscar</button>
                                    <button type="button" id="btnLimpiar"
                                        class="btn btn-secondary btn-spacing btn-enhanced rounded-shadow"
                                        style="background-color: #FFC0CB;">Limpiar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Estado cobranzas comisiones</h5>

                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <style>
                        .pagination1 {
                            margin: 0;
                            padding: 0;
                        }

                        .pagination1 li {
                            display: inline;
                            padding: 6px 10px 6px 10px;
                            border: 1px solid #ddd;
                            margin-right: -1px;
                            font: 15px/20px Arial, Helvetica, sans-serif;
                            background: #FFFFFF;
                            box-shadow: inset 1px 1px 5px #F4F4F4;
                        }

                        .pagination1 li a {
                            text-decoration: none;
                            color: rgb(89, 141, 235);
                        }

                        .pagination1 li.first {
                            border-radius: 5px 0px 0px 5px;
                        }

                        .pagination1 li.last {
                            border-radius: 0px 5px 5px 0px;
                        }

                        .pagination1 li:hover {
                            background: #CFF;
                        }

                        .pagination1 li.active {
                            background: #F0F0F0;
                            color: #333;
                        }
                    </style>

                    <div class="ibox-content" id="resultados">
                        <!-- <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Buscar"
                            autofocus=""> -->
                        <table id="myTable" class="table  table-stripped table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Alerta</th>
                                    <th>Trabajo</th>
                                    <th>Doc. facturación</th>
                                    <th>N° comprobante</th>
                                    <th>Nombres o Razón social</th>
                                    <th>Chasis</th>
                                    <th>Fecha creada</th>
                                    <th>Fecha pago verificación</th>
                                    <th>Usuario verificación</th>
                                    <th>Estado</th>
                                    <th>Canal</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody id="resultado">
                            </tbody>
                        </table>
                    </div>
                </div>

                <?php include("../includes/footer.php") ?>
            </div>
        </div>

        </div>
        <!-- LOS MODALES AQUI  -->
        <div class="modal inmodal" id="bateriasOModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated bounceIn">
                    <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                        <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                        <h4 class="modal-title">Gestion de pago de comisiones</h4>
                        <small class="font-bold">
                            <div class="form-group ">
                            </div>
                        </small>
                    </div>
                    <form role="form" method="post">

                        <div class="modal-body" style="background: #fff;padding-top: 0">
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                            <button type="submit" name="procesar" class="btn btn-primary"
                                value="<?php echo Token::generar() ?>">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <!-- LOS MODALES AQUI  -->

        <!-- <button class='btn' data-clipboard-action='copy' data-clipboard-target='p'>Copy</button> -->
        <script src="../assets/js/jquery-2.1.1.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


        <script src="../assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="../assets/js/inspinia.js"></script>
        <script src="../assets/js/plugins/pace/pace.min.js"></script>
        <script src="../assets/js/plugins/footable/footable.all.min.js"></script>

        <script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
        <script src="../assets/js/clipboard.min.js"></script>
        <script src="../assets/calendario/dist/sweetalert.min.js"></script>

        <!-- Toastr -->
        <script src="../assets/js/plugins/toastr/toastr.min.js"></script>
        <script src="../assets/js/jquery-confirm.min.js"></script>
        <script src="../assets/js/plugins/fullcalendar/moment.min.js"></script> <!--MOMENT-->
        <script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script> <!-- FECHA CALENDARIOA-->

        <!-- Datatable -->
        <script src="../assets/Datatable/JS/jquery.dataTables.js"></script>

        <!-- <script src="../assets/Datatable/JS/dataTables.bootstrap.min.js"></script> -->

        <script src="../assets/Datatable/JS/dataTables.buttons.min.js"></script>
        <script src="../assets/Datatable/JS/ajax_libs_jszip_3.1.3_jszip.min.js"></script>
        <script src="../assets/Datatable/JS/buttons.html5.min.js"></script>
        <script src="../assets/Datatable/JS/buttons.print.min.js"></script>
        <script src="../assets/Datatable/JS/pdfmake.min.js"></script>
        <script src="../assets/Datatable/JS/vfs_fonts.js"></script>

        <?php include_once('../includes/script.php') ?>

        <script>
            <?php if (!empty($errores)): ?>
                <?php foreach ($errores as $error): ?>
                    toastr.error('<?php echo $error ?>', 'Error', { timeOut: 5000, progressBar: true, closeButton: true });
                <?php endforeach ?>
            <?php endif ?>
            <?php if (!empty($echo)): ?>
                <?php foreach ($echo as $ok): ?>
                    toastr.success("<?php echo $ok ?>", 'Exito', { timeOut: 5000, progressBar: true, closeButton: true });
                <?php endforeach ?>
            <?php endif ?> 
        </script>
        <script>
            $("#sparkline2").sparkline([5, 6, 7, 2, 0, 4, 2], {
                type: 'bar',
                barColor: '#1ab394',
                negBarColor: '#c6c6c6'
            });
            $("#sparkline3").sparkline([-5, -6, -2, -2, -0, -4, -2], {
                type: 'bar',
                barColor: '#1ab394',
                negBarColor: '#c6c6c6'
            });
        </script>

        <script>
            let tablaComisiones;

            $(document).ready(function () {

                tablaComisiones = $("#myTable").DataTable({
                    processing: true,
                    ajax: {
                        url: `cobranzaTablasAjax.php?cargaInit=inicial`,
                        dataSrc: "",
                    },
                    language: {
                        url: "../assets/Datatable/json/Spanish.json",
                    },
                    columns: [

                        { data: "datosfacturacionid", visible: false },
                        { data: "alerta", visible: false },
                        {
                            data: "trabajo",
                        },
                        { data: "tipoDocumentoFacturacion" }, //tipoDocumentoFacturacion
                        {
                            data: null,
                            render: function (data, type, row, meta) {
                                let numero = row.tipoDocumentoFacturacion == "Boleta" ? row.numeroBoleta : row.numeroFactura;

                                if (row.numero !== "") {
                                    return `${numero}`;
                                }
                                return `No facturado`;
                            },
                        },
                        { data: "razonSocialFacturacion" },

                        { data: "chasis" },
                        {
                            data: "fechaCreada",
                            render: function (data, type, row) {
                                let fecha = moment(data, "YYYYMMDDHHmmss");
                                return fecha.format("YYYY-MM-DD HH:mm:ss");
                            }
                        },

                        {
                            data: "fechaPagoVerficacion",
                            render: function (data, type, row) {
                                let fecha = moment(data, "YYYYMMDDHHmmss");
                                //return fecha.format("YYYY-MM-DD HH:mm:ss");

                                if (row.estadoPago == '71' || row.estadoPago == '67') {
                                    return fecha.format("YYYY-MM-DD HH:mm:ss");
                                } else {
                                    return "";
                                }
                            }
                        },

                        {
                            data: null,
                            render: function (data, type, row, meta) {
                                if (row.estadoPago == '67' || row.estadoPago == '71') {
                                    return `${row.usuarioNombre} ${row.usuarioApellidoPaterno} ${row.usuarioApellidoMaterno}`;
                                } else {
                                    return "";
                                }
                            }
                        },

                        {
                            data: "estadoPago",
                            render: function (data, type, row, meta) {
                                switch (row.estadoPago) {
                                    case '67':
                                        return '<span class="label label-primary">Pagado</span>';

                                    case '63':
                                        return '<span class="label label-warning">Pago parcial</span>';

                                    case '71':
                                        return '<span class="label label-danger">Autoespar</span>';

                                    case '62':
                                        return '<span class="label label-danger">No pagado</span>';

                                    case '0':
                                        return '<span class="label label-warning">Pendiente</span>';
                                    
                                    default:
                                        return '';

                                }
                            }

                        },
                        {
                            data: null,
                            render: function (data, type, row, meta) {
                                if (row.idcanal != 0) {
                                    return `${row.nombreCanal} ${row.ejecutivoCanal}`;
                                }
                                return `Pendiente`;
                            },
                        },
                        {
                            data: null,
                            render: function (data, type, row, meta) {
                                return `<button class='btn btn-primary' style='padding-top: 0; padding-bottom: 0' data-toggle='modal' data-target='#bateriasOModal' data-id='${row.datosfacturacionid}'>Elegir <i class='fa fa-cog'></i></button>`;
                            },
                        },
                    ],
                    createdRow: function (row, data, dataIndex) {
                        if (data.alerta > 1) {
                            $(row).addClass('danger');
                        }
                    },

                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "excel",
                            text: "Descargar Excel",
                            exportOptions: {
                                columns: [2, 3, 4, 5, 6, 7, 8, 9, 10],
                            },

                            filename: function () {
                                var d = new Date();
                                var year = d.getFullYear();
                                var month = ('0' + (d.getMonth() + 1)).slice(-2);
                                var day = ('0' + d.getDate()).slice(-2);
                                var hour = ('0' + d.getHours()).slice(-2);
                                var minute = ('0' + d.getMinutes()).slice(-2);
                                var second = ('0' + d.getSeconds()).slice(-2);
                                return 'Cobranzas-' + year + '-' + month + '-' + day + '-' + hour + '-' + minute + '-' + second;
                            }
                        },
                    ],
                    pageLength: 15,
                    order: [[0, "desc"]],
                });

                function reloadTable() {
                    let estadoPago = $(".estadoPago").val();
                    let fechaInicio = $("#fechaInicio").val();
                    let fechaFin = $("#fechaFin").val();

                    let tipoFecha = $("#filtroFecha").val();

                    let search = $("#filter").val();

                    if ((estadoPago !== "67" && estadoPago !== "71" && estadoPago !== "all") && tipoFecha === "fechaVerificacion" && fechaInicio && fechaFin) {
                        swal("¡Ops!", "No hay datos de fecha de verificación para este estado.", "warning");
                        return;
                    }

                    if (!search) {
                        if (estadoPago == "all" && !fechaInicio && !fechaFin) {
                            swal("¡Ops!", "Por favor, seleccione un rango de fechas.", "warning");
                            return;

                        } else {
                            if ((fechaInicio && !fechaFin) || (!fechaInicio && fechaFin)) {
                                swal("¡Ops!", "Seleccione ambos filtros de fecha.", "warning");
                                return;
                            }
                        }

                    }

                    let url = `cobranzaTablasAjax.php?estadoPago=${estadoPago}&fechaInicio=${fechaInicio}&fechaFin=${fechaFin}&tipoFecha=${tipoFecha}&search=${search}`;

                    tablaComisiones.ajax.url(url).load();
                }

                // Evento click para el botón "Buscar"
                $("#btnBuscar").click(function () {
                    reloadTable();
                });

                // Evento click para el botón "Limpiar"
                $('#btnLimpiar').click(function () {
                    // Limpiar los filtros y recargar la tabla
                    $('#fechaInicio').val('');
                    $('#fechaFin').val('');

                    $('#filtroFecha').val('fechaCreada');

                    $('.estadoPago').prop('selectedIndex', 0);
                    $('#filter').val(''); //LIMPIAR BUSCADOR HISTÓRICO search
                    tablaComisiones.search('').draw();

                    let url = `cobranzaTablasAjax.php?cargaInit=inicial`;

                    tablaComisiones.ajax.url(url).load();
                    //reloadTable();
                });

                $('#bateriasOModal').on('show.bs.modal', function (e) {
                    var rowid = $(e.relatedTarget).data('id');
                    $.ajax({
                        type: 'post',
                        url: 'modal/cobranzasModal',
                        data: 'rowid=' + rowid,
                        success: function (data) {
                            $('.modal-body').html(data);
                        }
                    });
                });


            });

        </script>

        <script>
            $(window).load(function () {
                $(".se-pre-con").fadeOut("slow");;
            });
        </script>
    <?php else: ?>
        <?php $session->salir();
        echo "NO NO NO NO ;"
            ?>
    <?php endif ?>
<?php 
if ($_POST['rowid']) {
	include_once("../../includes/head.php");
	$idFacturacion=htmlspecialchars(htmlentities($_POST['rowid']));
	$facturacionObj=DatosFacturacion::obtenerDatosFacturacionPorId($idFacturacion);
	// $comisionObj=Comisiones::obtenerDatosDeComision($idComision);
	$clienteObj=Cliente::obtenerDatosDeclientePorId($facturacionObj->idcliente);
	$canalObj=Canales::obtenerDatosCanalPorId($facturacionObj->idcanal);
	?>
	<link href="../../assets/css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">
	<div class="well" style="padding-top: 11px;
	padding-bottom: 10px;">
	<div class="row">
	<div class="row"><div class="col-md-12">
<?php if (empty($facturacionObj->voucher)): ?>
<h3 class="text-center text-danger">Pago pendiente en caja</h3>
<input type="hidden" name="voucherNew">
<?php else: ?>
  <img class="text-center" width="700" src="../../public/vouchers/<?php echo $facturacionObj->voucher ?>" alt="../../public/vouchers/<?php echo $facturacionObj->voucher ?>" >
   <div class="row">
    <a  class="btn btn-primary text-center  col-md-6 col-md-offset-3" href="../../public/vouchers/<?php echo $facturacionObj->voucher ?>" target="_blank" >Descargar/ver Voucher</a>
    
  </div>
  <h4 class="text-center text-primary">Pagó en caja</h4>
<?php endif ?>
</div>
</div>
		<div class="row">		
			<input type="hidden" name="idFacturacion" value="<?php echo $idFacturacion ?>">
				<input type="hidden" name="idcanal" value="<?= $facturacionObj->idcanal ?>">
			<input type="hidden" name="idvehiculo" value="<?= $facturacionObj->idvehiculos ?>">
			<div class="form-group col-md-12">
				<label for="nombreCliente" >Nombre de cliente</label>
				<input  style="border: none;" type="text" readonly id="nombreCliente" name="nombreCliente" class="border-bottom form-control" required value="<?php  echo $retVal = ($clienteObj->tipoCliente=="Natural") ? $clienteObj->nombre." ".$clienteObj->apellidosC : $clienteObj->razonSocial ; ?> ">
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<label for="canal">Documento Facuturacion</label><input id="canal" style="border: none;" type="text" class="border-bottom form-control" readonly="" value="<?=  $facturacionObj->tipoDocumentoFacturacion ?>">
			</div>
          <?php $numero = ($facturacionObj->tipoDocumentoFacturacion=='Boleta') ? $facturacionObj->numeroBoleta : $facturacionObj->numeroFactura ; ?>
			<div class="col-md-2">
				<label for="canal">Numero</label><input id="canal" style="border: none;" type="text" class="border-bottom form-control" readonly="" value="<?= $retVal = ($numero=='') ? 'No facturado' : $numero ; ?>">
			</div>
			<div class="col-md-4">
				<label for="canal">Monto</label><input id="canal" style="border: none;" type="text" class="border-bottom form-control" readonly="" name="numeroDecuentaPago" value="<?=  $facturacionObj->costo ?>" >
			</div>
			<div class="col-md-3">
				<label for="canal">obs</label><input id="canal" style="border: none;" type="text" class="border-bottom form-control" readonly="" value="<?= $facturacionObj->observaciones ?>">
			</div>
		</div>	
		<div class="row">
			<div class="col-md-6">
			<?php 
			$usuario=Usuario::obtenerUsuario($facturacionObj->idUsuarioCreado);
			 ?>
				<label for="ejecu">Ejecutivo de venta</label><input id="ejecu" style="border: none;" type="text" class="border-bottom form-control" readonly="" value="<?= $usuario->usuarioNombre." ".$usuario->usuarioApellidoPaterno ?>">
			</div>
			<div class="col-md-6">
				<label for="distritoEntrega">Distrito de entrega</label><input id="distritoEntrega" style="border: none;" type="text" name="distritoEntrega" class="border-bottom form-control"  value="<?= $facturacionObj->distritoEntrega ?>" >
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">				
				<label for="direccionEntrega">Direccion entrega</label><input id="direccionEntrega" style="border: none;" type="text" class="border-bottom form-control"  name="direccionEntrega" value="<?= $facturacionObj->direccionEntrega ?>">
			</div>
			<div class="col-md-6">				
				<label for="fechaHoraDeEntrega">Horario de entrega</label><input id="fechaHoraDeEntrega" style="border: none;" type="text" name="fechaHoraDeEntrega" class="border-bottom form-control"  value="<?= $facturacionObj->fechaHoraDeEntrega ?>">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">				
				<label for="referenciaEntrega">Referencia entrega</label><input id="referenciaEntrega" style="border: none;" type="text" class="border-bottom form-control"  name="referenciaEntrega" value="<?= $facturacionObj->referenciaEntrega ?>">
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6">				
				<label for="recibidoPor">Persona de contacto</label><input id="recibidoPor" style="border: none;" type="text" class="border-bottom form-control" name="recibidoPor" value="<?= $facturacionObj->recibidoPor ?>">
			</div>
				<div class="col-md-6">
				<label for="telefonoFacturacion">Número de contacto</label><input id="telefonoFacturacion" style="border: none;" type="text" name="telefonoFacturacion" class="border-bottom form-control"  value="<?= $facturacionObj->telefonoFacturacion ?>">
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12">
		<label for="observaciones" >Observaciones *</label>
		<input  style="border: none;" type="text" placeholder="Observaciones *" id="observaciones" name="observaciones" class="border-bottom form-control" value="<?= $facturacionObj->observacionEntrega  ?>"  >
	</div>
</div>
<div class="row">
<div class="col-md-2">
		<label for="pagado">Enviado</label>
		<div class="switch">
			<div class="onoffswitch">
				<input type="radio" class="onoffswitch-checkbox" id="pagado" value="70" name="estadoPago" <?= ($facturacionObj->estadoEntrega==70)?"checked":"" ?>>
				<label class="onoffswitch-label" for="pagado">
					<span class="onoffswitch-inner"></span>
					<span class="onoffswitch-switch"></span>
				</label>
			</div>
			<?php if ($facturacionObj->estadoEntrega==70): ?>
			<span class="help-block m-b-none">Fecha hora de verificacion. <?= hace($facturacionObj->fechaHoraDeEntregaRegistro) ?> por <strong><?php $UsuarioV=Usuario::obtenerUsuario($facturacionObj->identrega);  echo $UsuarioV->usuarioNombre." ".$UsuarioV->usuarioApellidoPaterno ?></strong></span>
			<?php endif ?>
		</div>
	</div>
	<div class="col-md-2">
		<label for="pagoParcial">Entregado</label>
		<div class="switch">
			<div class="onoffswitch">
				<input type="radio" class="onoffswitch-checkbox" id="pagoParcial" value="68" name="estadoPago" <?= ($facturacionObj->estadoEntrega==68)? "checked":"" ?>>
				<label class="onoffswitch-label" for="pagoParcial">
					<span class="onoffswitch-inner"></span>
					<span class="onoffswitch-switch"></span>
				</label>
			</div>
	<?php if ($facturacionObj->estadoEntrega==68): ?>
	<span class="help-block m-b-none">Última actualizacion. <?= hace($facturacionObj->fechaHoraDeEntregaRegistro) ?></span>
	<?php endif ?>
		</div>
	</div>
	<div class="col-md-2">
		<label for="noPagado">No entregado</label>
		<div class="switch">
			<div class="onoffswitch">
				<input type="radio" class="onoffswitch-checkbox" id="noPagado" value="69" name="estadoPago" <?= ($facturacionObj->estadoEntrega==69 || $facturacionObj->estadoPago==400 )? "checked":"" ?>>
				<label class="onoffswitch-label" for="noPagado">
					<span class="onoffswitch-inner"></span>
					<span class="onoffswitch-switch"></span>
				</label>
			</div>
	<?php if ($facturacionObj->estadoEntrega==69): ?>
	<span class="help-block m-b-none">Última actualizacion. <?= hace($facturacionObj->fechaHoraDeEntregaRegistro) ?></span>
	<?php endif ?>
		</div>
	</div>
</div> 
<?php
} 
?>

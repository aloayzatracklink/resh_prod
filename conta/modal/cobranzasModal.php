<?php
if ($_POST['rowid']) {
	include_once("../../includes/head.php");
	$idFacturacion = htmlspecialchars(htmlentities($_POST['rowid']));
	$facturacionObj = DatosFacturacion::obtenerDatosFacturacionPorId($idFacturacion);
	// $comisionObj=Comisiones::obtenerDatosDeComision($idComision);
	$clienteObj = Cliente::obtenerDatosDeclientePorId($facturacionObj->idcliente);
	$canalObj = Canales::obtenerDatosCanalPorId($facturacionObj->idcanal);
	?>
	<link href="../../assets/css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">
	<div class="well" style="padding-top: 11px;
	padding-bottom: 10px;">
		<div class="row">
			<div class="row">
				<div class="col-md-12">
					<?php if (empty($facturacionObj->voucher)): ?>
						<h3 class="text-center text-danger">Pago pendiente en caja</h3>
						<input type="hidden" name="voucherNew">
					<?php else: ?>
						<img class="text-center" width="700" src="../../public/vouchers/<?php echo $facturacionObj->voucher ?>"
							alt="../../public/vouchers/<?php echo $facturacionObj->voucher ?>">
						<div class="row">
							<a class="btn btn-primary text-center  col-md-6 col-md-offset-3"
								href="../../public/vouchers/<?php echo $facturacionObj->voucher ?>"
								target="_blank">Descargar/ver Voucher</a>

						</div>
					<?php endif ?>
				</div>
			</div>
			<div class="row">
				<input type="hidden" name="idFacturacion" value="<?php echo $idFacturacion ?>">
				<input type="hidden" name="idcanal" value="<?= $facturacionObj->idcanal ?>">
				<input type="hidden" name="idvehiculo" value="<?= $facturacionObj->idvehiculos ?>">
				<div class="form-group col-md-12">
					<label for="nombreCliente">Nombre de cliente</label>
					<input style="border: none;" type="text" readonly id="nombreCliente" name="nombreCliente"
						class="border-bottom form-control" required
						value="<?php echo $retVal = ($clienteObj->tipoCliente == "Natural") ? $clienteObj->nombre . " " . $clienteObj->apellidosC : $clienteObj->razonSocial; ?> ">
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<label for="canal">Documento Facuturacion</label><input id="canal" style="border: none;" type="text"
						class="border-bottom form-control" readonly=""
						value="<?= $facturacionObj->tipoDocumentoFacturacion ?>">
				</div>
				<?php $numero = ($facturacionObj->tipoDocumentoFacturacion == 'Boleta') ? $facturacionObj->numeroBoleta : $facturacionObj->numeroFactura; ?>
				<div class="col-md-2">
					<label for="canal">Numero</label><input id="canal" style="border: none;" type="text"
						class="border-bottom form-control" readonly=""
						value="<?= $retVal = ($numero == '') ? 'No facturado' : $numero; ?>">
				</div>
				<div class="col-md-4">
					<label for="canal">Monto</label><input id="canal" style="border: none;" type="text"
						class="border-bottom form-control" readonly="" name="numeroDecuentaPago"
						value="<?= $facturacionObj->costo ?>">
				</div>
				<div class="col-md-3">
					<label for="canal">obs</label><input id="canal" style="border: none;" type="text"
						class="border-bottom form-control" readonly="" value="<?= $facturacionObj->observaciones ?>">
				</div>
			</div>
			<div class="row">
				<div class="col-md-6"><label for="canal">Facturado a nombre:</label><input id="canal" style="border: none;"
						type="text" class="border-bottom form-control" readonly=""
						value="<?= $facturacionObj->razonSocialFacturacion ?>"></div>
				<div class="col-md-6"><label for="canal">Apuntes</label><input id="canal" style="border: none;" type="text"
						class="border-bottom form-control" readonly="" value="<?= $facturacionObj->apunte ?>"></div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<?php
					$usuario = Usuario::obtenerUsuario($facturacionObj->idUsuarioCreado);

					?>
					<label for="ejecu">Ejecutivo de venta</label><input id="ejecu" style="border: none;" type="text"
						class="border-bottom form-control" readonly=""
						value="<?= $usuario->usuarioNombre . " " . $usuario->usuarioApellidoPaterno ?>">
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<label for="observaciones">Observaciones *</label>
			<input style="border: none;" type="text" placeholder="Observaciones *" id="observaciones" name="observaciones"
				class="border-bottom form-control" value="<?= $facturacionObj->observacionPagoVerificacion ?>">
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<label for="pagado">Pagado</label>
			<div class="switch">
				<div class="onoffswitch">
					<input type="radio" class="onoffswitch-checkbox" id="pagado" value="67" name="estadoPago"
						<?= ($facturacionObj->estadoPago == 67) ? "checked" : "" ?>>
					<label class="onoffswitch-label" for="pagado">
						<span class="onoffswitch-inner"></span>
						<span class="onoffswitch-switch"></span>
					</label>
				</div>
				<?php if ($facturacionObj->estadoPago == 67): ?>
					<span class="help-block m-b-none">Fecha hora de verificacion.
						<?= hace($facturacionObj->fechaPagoVerficacion) ?> por <strong>
							<?php $UsuarioV = Usuario::obtenerUsuario($facturacionObj->idusuarioPagoVerificacion);
							echo $UsuarioV->usuarioNombre . " " . $UsuarioV->usuarioApellidoPaterno ?>
						</strong>
					</span>
				<?php endif ?>
			</div>
		</div>
		<div class="col-md-2">
			<label for="pagoParcial">Pago parcial</label>
			<div class="switch">
				<div class="onoffswitch">
					<input type="radio" class="onoffswitch-checkbox" id="pagoParcial" value="63" name="estadoPago"
						<?= ($facturacionObj->estadoPago == 63) ? "checked" : "" ?>>
					<label class="onoffswitch-label" for="pagoParcial">
						<span class="onoffswitch-inner"></span>
						<span class="onoffswitch-switch"></span>
					</label>
				</div>
				<?php if ($facturacionObj->estadoPago == 63): ?>
					<span class="help-block m-b-none">Última actualizacion.
						<?= hace($facturacionObj->ultimaActualizacionVerificacion) ?>
					</span>
				<?php endif ?>
			</div>
		</div>
		<div class="col-md-2">
			<label for="noPagado">No pagado</label>
			<div class="switch">
				<div class="onoffswitch">
					<input type="radio" class="onoffswitch-checkbox" id="noPagado" value="62" name="estadoPago"
						<?= ($facturacionObj->estadoPago == 62 || $facturacionObj->estadoPago == 0) ? "checked" : "" ?>>
					<label class="onoffswitch-label" for="noPagado">
						<span class="onoffswitch-inner"></span>
						<span class="onoffswitch-switch"></span>
					</label>
				</div>
				<?php if ($facturacionObj->estadoPago == 62): ?>
					<span class="help-block m-b-none">Última actualizacion.
						<?= hace($facturacionObj->ultimaActualizacionVerificacion) ?>
					</span>
				<?php endif ?>
			</div>
		</div>

		<div class="col-md-2">
			<?php if (strtoupper(trim($facturacionObj->razonSocialFacturacion)) == "AUTOESPAR S A" || strtoupper(trim($facturacionObj->razonSocialFacturacion)) == "AUTOESPAR S.A." || strtoupper(trim($facturacionObj->razonSocialFacturacion))=="AUTOESPAR S.A"): ?>
				<label for="autoespar">Autoespar</label>
				<div class="switch">
					<div class="onoffswitch">
						<input type="radio" class="onoffswitch-checkbox" id="autoespar" value="71" name="estadoPago"
							<?= ($facturacionObj->estadoPago == 71) ? "checked" : "" ?>>
						<label class="onoffswitch-label" for="autoespar">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
					</div>
					<?php if ($facturacionObj->estadoPago == 71): ?>
						<span class="help-block m-b-none">Fecha hora de verificacion.
							<?= hace($facturacionObj->fechaPagoVerficacion) ?> por <strong>
								<?php $UsuarioV = Usuario::obtenerUsuario($facturacionObj->idusuarioPagoVerificacion);
								echo $UsuarioV->usuarioNombre . " " . $UsuarioV->usuarioApellidoPaterno ?>
							</strong>
						</span>
					<?php endif ?>
				</div>
			<?php endif ?>
		</div>
	</div>
	<?php
}
?>
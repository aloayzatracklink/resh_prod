<?php 
if ($_POST['rowid']) {
  include_once("../../includes/head.php");
  	$datosfacturacionid=htmlspecialchars(htmlentities($_POST['rowid']));
  	$contabilidadObj=DatosFacturacion::obtenerDatosFacturacionPorId($datosfacturacionid);
  	$clienteObj = Cliente::obtenerDatosDeclientePorId($contabilidadObj->idcliente);
    $vehiculoObj=Vehiculo::obtenerVehiculo($contabilidadObj->idvehiculos);
    $renovacionObj=Renovaciones::obtenerDatosDeRenovacionPorId($contabilidadObj->idrenovacion);
?>
<input type="hidden" name="datosfacturacionid" value="<?php echo $contabilidadObj->datosfacturacionid ?>" >
<input type="hidden" name="idcanal" value="<?php echo $contabilidadObj->idcanal ?>" >
<input type="hidden" name="idvehiculo" value="<?php echo $contabilidadObj->idvehiculos ?>" >

<div class="well card border" >
    <div class="card-body">
    <?php if($clienteObj!=null){ ?>
        <div class="row">
        <div class="col-md-5 form-group">
                <label for=""># DOCUMENTO</label>
                <div class="input-group">
                    <input readonly class="form-control" type="text" value="<?= $clienteObj->tipoCliente=='Natural'?$clienteObj->dni:$clienteObj->ruc ?>" id="documento" >
                    <span class="input-group-btn"><a class="btn btn-primary " id="botonCopiarDni" >Copiar</a></span> 
                </div>
            </div>
            <div class="col-md-7 form-group">
                <label for="">CLIENTE</label>
                <input class="form-control" type="text" value="<?= $clienteObj->tipoCliente=="Natural"?strtoupper($clienteObj->apellidosC." ".$clienteObj->nombre):strtoupper($clienteObj->razonSocial) ?>" readonly>
            </div>
        </div>
    <?php }else{ ?>
        <div class="row">
        <div class="col-md-4 form-group">
                <label for=""># DOCUMENTO</label>
                <div class="input-group">
                    <input readonly class="form-control" type="text" value="" id="documento" >
                    <span class="input-group-btn"><a class="btn btn-primary " id="botonCopiarDni" >Copiar</a></span> 
                </div>
            </div>
            <div class="col-md-8 form-group">
                <label for="">CLIENTE</label>
                <input class="form-control" type="text" value="<?= $renovacionObj->nombreCliente ?>" readonly>
            </div>
        </div>
    <?php } ?>
        <div class="row">
            <div class="col-md-3 text-muted">
                <br>
                <label for="">fecha creacion: <?php echo haceMinimo($contabilidadObj->fechaCreada) ?></label>
            </div>
            <div class="col-md-5 form-group">
                <label for="">Ejecutivo</label>
                <input class="form-control" type="text" value="<?php $usuario=Usuario::obtenerUsuario($contabilidadObj->idUsuarioCreado);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno;  ?>" readonly>
            </div>
            <div class="col-md-4 form-group">
                <label for="">Tipo</label>
                <input class="form-control" type="text" value="<?= (!$renovacionObj)?' VENTA| '.strtoupper($contabilidadObj->tipoDocumentoFacturacion):'RENOVACIÓN' ?>" readonly>
            </div>
        </div>
    <hr>
    <h3 class="text-center text-info">
        Datos de facturación
      <?= obtenerEstado($contabilidadObj->estadoFacturacion) ?>
    </h3>
    <div class="row">
            <div class="col-md-3 form-group">
                <label for="">DOCUMENTO</label>
                <select  class="border-bottom form-control" required name="tipoDocumento">
                    <?php
                        if(($contabilidadObj->tipoDocumentoFacturacion=='Boleta') or ($contabilidadObj->tipoDocumentoFacturacion=='Factura')){
                        ?>
                   <option class='text-capitalize' value='<?= $contabilidadObj->tipoDocumentoFacturacion ?>' selected  ><?= $contabilidadObj->tipoDocumentoFacturacion ?></option>
                        <?php
                        }else{
                        ?>
                   <option class='text-capitalize' value='<?= ($contabilidadObj->tipoDocumentoFacturacion=='Banbif')?$contabilidadObj->tipoDocumentoFacturacion:"" ?>' selected  ><?= $contabilidadObj->tipoDocumentoFacturacion ?></option>
                   <option class='text-capitalize' value='Boleta' >Boleta</option>
                   <option class='text-capitalize' value='Factura' >Factura</option>
                        <?php
                        }
                    ?>
                </select>
            </div>
            <div class="col-md-3 form-group">
                <label for="">TIPO</label>
                <div class="input-group">
                    <input readonly class="form-control" type="text" value="<?= !empty($contabilidadObj->rucFacturacion)?$contabilidadObj->rucFacturacion:$contabilidadObj->dniFacturacion ?>" id="documentoRUC" >
                    <span class="input-group-btn"><a class="btn btn-primary " id="botonCopiarRUC" >Copiar</a></span> 
                </div>
            </div>
            <div class="col-md-6 form-group">
                <label for="">CLIENTE</label>
                <input class="form-control" type="text" value="<?= $contabilidadObj->razonSocialFacturacion ?>" readonly>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 form-group">
                <label for="">Forma Pago</label>
                <input class="form-control" type="text" value="<?= $contabilidadObj->formaPago ?>" readonly>
            </div>
           <div class="col-md-8 form-group">
                <label for="">DIRECCIÓN</label>
                <input class="form-control" type="text" value="<?= $contabilidadObj->direccionFiscal ?>" readonly>
            </div>
        </div>
    <hr>
  <?php if (!empty($contabilidadObj->observaciones)): ?>
        <div class="row">
           <div class="col-md-12 form-group has-warning">
                <label for="">OBSERVACIONES</label>
                <input class="form-control" type="text" value="<?= $contabilidadObj->observaciones ?> <?= !empty($contabilidadObj->apunte)?$contabilidadObj->apunte:'' ?>" readonly>
            </div>
        </div>
    <hr>
  <?php endif ?>
<!-- puede ser multiple -->
        <div class="row">
            <div class="col-md-7 form-group">
                <label for="">Plan</label>
                <input readonly class="form-control" type="text" value="<?= strtoupper($contabilidadObj->plan) ?><?=($vehiculoObj!=null)?" |".$vehiculoObj->placa." |".$vehiculoObj->chasis:''?>">
            </div>
            <?php $tmp=explode('-', $contabilidadObj->orden); $orden=end($tmp); ?>
            <div class="col-md-3 ">
                <label for="">Orden</label>
                <div class="input-group">
                    <input readonly class="form-control" type="text" value="<?= $orden ?>" id="orden" >
                    <span class="input-group-btn"><a class="btn btn-primary " id="botonCopiar" >Copiar</a></span> 
                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="">Precio</label>
                <input readonly  class="form-control" type="text" value="<?php echo $contabilidadObj->costo ?>">
            </div>
        </div>
<!-- puede ser multiple -->
    <hr>
    <div class="row">
            <div class="col-md-4 form-group">
                  <label for="seguimiento">Realizar seguimiento</label>
                  <div class="switch">
                    <div class="onoffswitch">
                      <input type="checkbox" class="onoffswitch-checkbox" id="seguimiento" value="si" <?= ($contabilidadObj->seguimiento)?'checked':'' ?> name="seguimiento">
                      <label class="onoffswitch-label" for="seguimiento">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                      </label>
                    </div>
                  </div>
                
            </div>
            <div class="col-md-3 form-group"></div>
            <div class="col-md-3 form-group">
                <label for="">Total items</label>
                <input readonly  class="form-control" type="text" value="1">
            </div>
            <div class="col-md-2 form-group">
                <label for="">Precio Total</label>
                <input readonly  class="form-control" type="text" value="<?php echo $contabilidadObj->costo ?>">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-8">
                <?php if (empty($contabilidadObj->voucher)): ?>
                <label class="text-center text-danger">Voucher pendiente</label>
                <input type="hidden" name="voucherNew">
                <?php else: ?>
                  <div class="row">
                      <img class="text-center" style="width:100%;
    max-width:800px;"  src="../../public/vouchers/<?= $contabilidadObj->voucher ?>" alt="../../public/vouchers/<?php echo $contabilidadObj->voucher ?>" >
                  </div>  
                  <div class="row">
                    <a  class="btn btn-primary text-center  col-md-6 col-md-offset-3" href="../../public/vouchers/<?= $contabilidadObj->voucher ?>" target="_blank" >Descargar/ver Voucher</a>    
                  </div>
                <?php endif ?>
            </div>
            <div class="col-md-4">
                  <div class="col-md-12">
                  <label for="Facturar">Facturar fin de mes</label>
                  <div class="switch">
                    <div class="onoffswitch">
                      <input type="checkbox" class="onoffswitch-checkbox" id="Facturar" value="si" name="facturarFin">
                      <label class="onoffswitch-label" for="Facturar">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
        </div>

<?php if ($contabilidadObj->tipoDocumentoFacturacion=='Banbif'){ ?>
<div class="row">
  <div class="form-group col-md-3">
    <label for="factura" class="sr-only">Numero de Factura Equipo</label>
    <input style="border: none;" type="text" placeholder="Numero de Factura Equipo" id="factura" name="factura" class="border-bottom form-control abc" required value="<?php echo $retVal = (isset($contabilidadObj->numeroFactura)) ? $contabilidadObj->numeroFactura : "" ; ?>">
  </div>
  <div class="form-group col-md-3">
    <label for="factura2" class="sr-only">Numero de Factura Servicio</label>
    <input style="border: none;" type="text" placeholder="Numero de Factura Servicio" id="factura2" name="factura2" class="border-bottom form-control abc" value="<?php echo $retVal = (isset($contabilidadObj->numeroFactura)) ? $contabilidadObj->numeroFactura : "" ; ?>">
  </div>
  <div class="form-group col-md-6">
    <label for="Observaciones1" class="">Observaciones</label>
    <input style="border: none;" type="text" placeholder="Observaciones" id="Observaciones1" name="observacion" class="border-bottom form-control abc"  value="<?php echo $retVal = (isset($contabilidadObj->apunte)) ? $contabilidadObj->apunte : "" ; ?>">
  </div>
</div>
<?php }else{
?>
<div class="row">
  <div class="form-group col-md-6">
    <label for="factura" class="">Numero</label>
    <input style="border: none;" type="text" placeholder="Numero" id="factura" name="numero" class="border-bottom form-control abc" required value="<?php echo $retVal = (!empty($contabilidadObj->numeroFactura)) ? $contabilidadObj->numeroFactura : $contabilidadObj->numeroBoleta; ?>">
  </div>
  <div class="form-group col-md-6">
    <label for="Observaciones1" class="">Observaciones</label>
    <input style="border: none;" type="text" placeholder="Observaciones" id="Observaciones1" name="observacion" class="border-bottom form-control abc"  value="<?php echo $retVal = (isset($contabilidadObj->apunte)) ? $contabilidadObj->apunte : "" ; ?>">
  </div>
</div>
<?php
}
?>
<div class="row">
<div id="esconderThis">
  <div class="form-group col-md-12">
    <label for="apunteapunte" class="sr-only">Apunte * </label>
    <input style="border: none;" type="text" placeholder="Apunte * " id="apunteapunte" name="apunte" class="border-bottom form-control"  required="" value="<?php echo $retVal = (isset($contabilidadObj->apunte)) ? $contabilidadObj->apunte : "" ; ?>">
  </div>
</div>
</div>


    </div>

   <div class="row">
        <div class="col-md-12">
        <?php
            $modfiaciones=DatosFacturacion::obtenerSEguimiento($contabilidadObj->datosfacturacionid);
            if($modfiaciones):
        ?>
<ul class="list-group list-group-flush">
  <li class="list-group-item"> <b>Ultimas modificaciones </b></li>
<?php foreach($modfiaciones as $var): ?>
    <?php $ooo=Usuario::obtenerUsuario($var->idrenovacion); ?>
  <li class="list-group-item"><?= $var->fecha ?>    ===> <?= $ooo->usuarioNombre." ".$ooo->usuarioApellidoPaterno ?> </li>
<?php endforeach; ?>
</ul>
<?php endif; ?>
        </div>

   </div>

</div>
<input type="hidden" name="datosfacturacionid" value="<?php echo $contabilidadObj->datosfacturacionid ?>" >


<?php
} 
?>

<script>
$("#esconderThis").hide();
$('#apunteapunte').prop('disabled', true);
$( "#Facturar" ).on( "click", function() {
var facturar=$( "input:checked" ).val();
if (facturar=='si') {
$("#esconderThis").show();    
$(".abc").hide();
$('#factura').prop('disabled', true);
$('#boleta').prop('disabled', true);
$('#apunteapunte').prop('disabled', false);
$('#obs1').prop('disabled', true);
$('#Observaciones1').prop('disabled', true);
}else{
$('#factura').prop('disabled', false);
$('#boleta').prop('disabled', false);
$('#apunteapunte').prop('disabled', true);
$("#esconderThis").hide();
$('#obs1').prop('disabled', false);
$('#Observaciones1').prop('disabled', false);
$(".abc").show();
}
});
document.getElementById("botonCopiar").addEventListener("click", function() {
    copyToClipboard(document.getElementById("orden"));
});

document.getElementById("botonCopiarDni").addEventListener("click", function() {
    copyToClipboard(document.getElementById("documento"));
});

document.getElementById("botonCopiarRUC").addEventListener("click", function() {
    copyToClipboard(document.getElementById("documentoRUC"));
});

function copyToClipboard(elem) {
// create hidden text element, if it doesn't already exist
var targetId = "_hiddenCopyText_";
var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
var origSelectionStart, origSelectionEnd;
if (isInput) {
// can just use the original source element for the selection and copy
target = elem;
origSelectionStart = elem.selectionStart;
origSelectionEnd = elem.selectionEnd;
} else {
// must use a temporary form element for the selection and copy
target = document.getElementById(targetId);
if (!target) {
var target = document.createElement("textarea");
target.style.position = "absolute";
target.style.left = "-9999px";
target.style.top = "0";
target.id = targetId;
document.body.appendChild(target);
}
target.textContent = elem.textContent;
}
// select the content
var currentFocus = document.activeElement;
target.focus();
target.setSelectionRange(0, target.value.length);

// copy the selection
var succeed;
try {
succeed = document.execCommand("copy");
} catch(e) {
succeed = false;
}
// restore original focus
if (currentFocus && typeof currentFocus.focus === "function") {
currentFocus.focus();
}

if (isInput) {
// restore prior selection
elem.setSelectionRange(origSelectionStart, origSelectionEnd);
} else {
     // clear temporary content
             target.textContent = "";
                 }
                     return succeed;
                     }

</script>

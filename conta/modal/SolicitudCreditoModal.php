<?php
include_once("../../includes/head.php");
$idsolicred=soloNumero($_POST['rowid']);
$tipoOpera=$_POST['tipoOpera'];
$Objtec=SolicitudCredito::obtenerSolicitudCredito($idsolicred);
$clienteObjModal=new Cliente;
if($tipoOpera=='edit'){?>
    <style>
        .tachado {
            text-decoration: line-through;
        }
    </style>
    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <div class="well" style="padding-top: 11px;padding-bottom: 10px;">
        <div class="row">
            <div class="col-md-4">
                <label for="tipoper" class="">Tipo Persona*</label>
                <select style="border: none;" class="border-bottom form-control text-capitalize tipoper" required
                        name="tipoper" id="tipoper">
                    <?php $objtp = SolicitudCredito::obtenerTipoPersona() ?>
                    <?php if ($objtp): ?>
                        <?php foreach ($objtp as $var): ?>
                            <option value="<?php echo $var->idtipoper ?>" <?= $Objtec->idtipoper==$var->idtipoper?'selected':'' ?>  ><?php echo $var->desctipo ?></option>
                        <?php endforeach ?>
                    <?php else: ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="form-group col-md-4 text-primary text-capitalize">
                <label for="nsolicitante"  class="nsolicitante">Razon Social / Nombre y Apellido</label>
                <input style="border: none;" type="text" placeholder="Ingresar Datos" id="nsolicitante"
                       name="nsolicitante" class="border-bottom form-control" required value="<?= $Objtec->cliente ?>">
                <input type="hidden" name="idSolicitud" value="<?= $Objtec->idsolicre ?>">
                <input type="hidden" name="tipoOpera" value="<?= $tipoOpera ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="nrucdni" class="rucdni">N° RUC / DNI</label>
                <input style="border: none;" type="text" placeholder="Ingresar Datos" id="nrucdni" disabled
                       name="nrucdni" class="border-bottom form-control" required  onKeyPress='return soloNumero(event)'
                       value="<?= $Objtec->numerodoc ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="tmonto">Monto Facturación</label>
                <input style="border: none;" type="text" placeholder="Ingresar Monto" id="tmonto"   onKeyPress='return soloNumeroMonto(event)'
                       name="tmonto" class="border-bottom form-control" required value="<?= $Objtec->monto ?>">
            </div>
            <div class="form-group col-md-4 text-primary text-capitalize">
                <label for="noperacion">N° Operaciones</label>
                <input style="border: none;" type="text" placeholder="Ingresar N° Operaciones" id="noperacion" onKeyPress='return soloNumero(event)'
                       name="noperacion" class="border-bottom form-control" required value="<?= $Objtec->noperaciones ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="ndias">Dias de Crédito</label>
                <input style="border: none;" type="text" placeholder="Ingresar Dias Crédito" id="ndias" onKeyPress='return soloNumero(event)'
                       name="ndias" class="border-bottom form-control" required
                       value="<?= $Objtec->dcredito ?>">
            </div>
        </div>
        <div class="row">
            <div class="form-group  col-md-12">
                <label for="ndetalle" class="">Detalle</label>
                <input style="border: none;" type="text" placeholder="Ingresar Detalle" id="ndetalle" name="ndetalle" class="form-control x2 border-bottom focusUrgenteta" value="<?= $Objtec->detalle ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="nrepLegal">Representante Legal</label>
                <input style="border: none;" type="text" placeholder="Ingresar Representante Legal" id="nrepLegal"
                       name="nrepLegal" class="border-bottom form-control"  value="<?= $Objtec->respresantelegal ?>">
            </div>
            <div class="form-group col-md-4 text-primary text-capitalize">
                <label for="dniRP">DNI</label>
                <input style="border: none;" type="text" placeholder="Ingresar Dni" id="dniRP"  onKeyPress='return soloNumero(event)'
                       name="dniRP" class="border-bottom form-control"  value="<?= $Objtec->dni ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="njefCompras">Jefe de Compras</label>
                <input style="border: none;" type="text" placeholder="Ingresar Jefe Compras" id="njefCompras"
                       name="njefCompras" class="border-bottom form-control" required value="<?= $Objtec->jefecompras ?>">
            </div>
            <div class="form-group col-md-4 text-primary text-capitalize">
                <label for="nemailC">Email</label>
                <input style="border: none;" type="email" placeholder="Ingresar Email" id="nemailC"
                       name="nemailC" class="border-bottom form-control" required value="<?= $Objtec->jf_email ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="nCelTelfC">Cel./Telf.</label>
                <input style="border: none;" type="text" placeholder="Ingresar Cel./Telf." id="nCelTelfC"   onKeyPress='return soloNumero(event)'
                       name="nCelTelfC" class="border-bottom form-control" required value="<?= $Objtec->jf_cel ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="njefTeso">Jefe de Tesorería</label>
                <input style="border: none;" type="text" placeholder="Ingresar Jefe Tesorería" id="njefTeso"
                       name="njefTeso" class="border-bottom form-control" required value="<?= $Objtec->jefetesoreria ?>">
            </div>
            <div class="form-group col-md-4 text-primary text-capitalize">
                <label for="nemailT">Email</label>
                <input style="border: none;" type="email" placeholder="Ingresar Email" id="nemailT"
                       name="nemailT" class="border-bottom form-control" required value="<?= $Objtec->jt_email ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="nCelTelfT">Cel./Telf.</label>
                <input style="border: none;" type="text" placeholder="Ingresar Cel./Telf." id="nCelTelfT"   onKeyPress='return soloNumero(event)'
                       name="nCelTelfT" class="border-bottom form-control" required value="<?= $Objtec->jt_cel ?>">
            </div>
        </div>
        <p class="font-bold  alert alert-success m-b-sm" style="">
            ANEXAR LOS DOCUMENTOS QUE SE SEÑALAN A CONTINUACIÓN.
        </p>

        <div class="row">
            <div class="col-md-12 form-group ">
                <label for="Forma*" >Copia DNI representate legal</label><br>
                <div class="col-md-7">
                    <p id="timgDNI" ><?= $Objtec->img_dni ?></p>
                    <input type="text" class="iDNI" name="voucher"  id="voucher" style="display: none" value="<?= $Objtec->img_dni ?>" />
                </div>
                <div class="col-md-1">
                    <div class="checkbox checkbox-success " style="margin:0">
                        <input id="editDNI" type="checkbox" name="editDNI" value="1">
                        <label for="editDNI">
                            <strong>Editar</strong>
                        </label>
                    </div>
                </div>
                <?php  if(trim($Objtec->img_dni)!=''){?>
                <div class="col-md-3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a  class="fa fa-eye child_v" style="cursor: pointer;color:#0c5977" href="../../public/vouchers/<?= $Objtec->img_dni ?>" target="_blank">Ver Docuemnto</a>
                </div>
                <?php } ?>
                <div class="col-md-12 fileinput input-group fileinput-new voucherImg" data-provides="fileinput" style="display:none;" >
                    <div class="form-control" data-trigger="fileinput">
                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                        <span class="fileinput-filename"></span>
                    </div>
                    <span class="input-group-addon btn btn-default btn-file">
					    <span class="fileinput-new"  >Seleccionar DNI Rep.Legal</span>
                        <span class="fileinput-exists">Cambiar</span>
                        <input type="file" name="voucher"  id="voucherImg"  class="iDNIf"/>
				    </span>
                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 form-group ">
                <label for="Forma*" >Copia Ficha RUC</label><br>
                <div class="col-md-7">
                    <p id="timgRUC" ><?= $Objtec->img_ficharuc ?></p>
                    <input type="text" class="iRUC" name="voucherRUC"  id="voucherRUC" style="display: none" value="<?= $Objtec->img_ficharuc ?>" />
                </div>
                <div class="col-md-1">
                    <div class="checkbox checkbox-success " style="margin:0">
                        <input id="editRUC" type="checkbox" name="editRUC" value="1">
                        <label for="editRUC">
                            <strong>Editar</strong>
                        </label>
                    </div>
                </div>
                <?php  if(trim($Objtec->img_ficharuc)!=''){?>
                <div class="col-md-3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a  class="fa fa-eye child_v" style="cursor: pointer;color:#0c5977" href="../../public/vouchers/<?= $Objtec->img_ficharuc ?>" target="_blank">Ver Documento</a>
                </div>
                <?php } ?>
                <div class="col-md-12 fileinput  input-group  voucherImgRUC fileinput-new" data-provides="fileinput" style="display:none;" >
                    <div class="form-control" data-trigger="fileinput">
                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                        <span class="fileinput-filename"></span>
                    </div>
                    <span class="input-group-addon btn btn-default btn-file">
			    		<span class="fileinput-new">Seleccionar Ficha RUC</span>
			    		<span class="fileinput-exists">Cambiar</span>
			    		<input type="file" name="voucherRUC"  id="voucherImgRUC"  class="iRUCf"/>
			    	</span>
                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 form-group ">
                <label for="Forma*" >PDT presentados a SUNAT(ultimos dos meses)</label><br>
                <div class="col-md-7">
                    <p id="timgPDT" ><?= $Objtec->img_pdt ?></p>
                    <input type="text" class="iPDT" name="voucherPDT"  id="voucherPDT" style="display: none" value="<?= $Objtec->img_pdt ?>" />
                </div>
                <div class="col-md-1">
                    <div class="checkbox checkbox-success " style="margin:0">
                        <input id="editPDT" type="checkbox" name="editPDT" value="1">
                        <label for="editPDT">
                            <strong>Editar</strong>
                        </label>
                    </div>
                </div>
                <?php  if(trim($Objtec->img_pdt)!=''){?>
                <div class="col-md-3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a  class="fa fa-eye child_v" style="cursor: pointer;color:#0c5977" href="../../public/vouchers/<?= $Objtec->img_pdt ?>" target="_blank">Ver Documento</a>
                </div>
                <?php } ?>
                <div class="col-md-12 fileinput  input-group fileinput-new voucherImgPDT" data-provides="fileinput" style="display:none;" >
                    <div class="form-control" data-trigger="fileinput">
                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                        <span class="fileinput-filename"></span>
                    </div>
                    <span class="input-group-addon btn btn-default btn-file">
			        		<span class="fileinput-new">Seleccionar PDT</span>
			        		<span class="fileinput-exists">Cambiar</span>
			        		<input type="file" name="voucherPDT"  id="voucherImgPDT" class="iPDTf"  />
			        	</span>
                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 form-group ">
                <label for="Forma*">Orden de Servicio</label><br>
                <div class="col-md-7">
                    <p id="timgOrden" ><?= $Objtec->img_ordenservi ?></p>
                    <input type="text" class="iOrden" name="voucherOrden"  id="voucherImgOrden" style="display: none" value="<?= $Objtec->img_ordenservi ?>" />
                </div>
                <div class="col-md-1">
                    <div class="checkbox checkbox-success " style="margin:0">
                        <input id="editOrden" type="checkbox" name="editOrden" value="1">
                        <label for="editOrden">
                            <strong>Editar</strong>
                        </label>
                    </div>
                </div>
                <?php  if(trim($Objtec->img_ordenservi)!=''){?>
                <div class="col-md-3">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a  class="fa fa-eye child_v" style="cursor: pointer;color:#0c5977" href="../../public/vouchers/<?= $Objtec->img_ordenservi ?>" target="_blank">Ver Documento</a>
                </div>
                <?php } ?>
                <div class="col-md-12 fileinput input-group fileinput-new voucherImgOrden" data-provides="fileinput" style="display:none;">
                    <div class="form-control" data-trigger="fileinput">
                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                        <span class="fileinput-filename"></span>
                    </div>
                    <span class="input-group-addon btn btn-default btn-file">
				        	<span class="fileinput-new">Seleccionar Orden Serv.</span>
		          			<span class="fileinput-exists">Cambiar</span>
         					<input type="file" name="voucherOrden"  id="voucherImgOrden" class="iOrdenf" required />
				    </span>
                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
                </div>
            </div>
        </div>
        <div class="row">
            <h3><strong>Nota:</strong> En caso ser cliente al contado, solo se adjunta Orden de Servicio.</h3>
        </div>
    </div>
    <script>
        function cambioTipoper() {
            $selectid=$("#tipoper option:selected").val();
            if($selectid==1){
                $("#nrucdni").prop("disabled",false);
                $("#nrucdni").attr('maxlength','8');
                $("#nrucdni").val("");
                $(".rucdni").text("N° DNI");
                $(".nsolicitante").text("Nombre y Apellido");
            }else if($selectid==2){
                $("#nrucdni").prop("disabled",false);
                $("#nrucdni").attr('maxlength','11');
                $("#nrucdni").val("");
                $(".rucdni").text("N° RUC");
                $(".nsolicitante").text("Razón Social");
            }else{
                $("#nrucdni").prop("disabled",true);
                $("#nrucdni").val("");
                $(".rucdni").text("N° RUC / DNI");
                $(".nsolicitante").text("Razon Social / Nombre y Apellido");
            }
            return false;
        }

        function cambioTipoper2() {

            $selectid=$("#tipoper option:selected").val();
            if($selectid==1){
                $("#nrucdni").prop("disabled",false);
                $("#nrucdni").attr('maxlength','8');
            //    $("#nrucdni").val("");
                $(".rucdni").text("N° DNI");
                $(".nsolicitante").text("Nombre y Apellido");
            }else if($selectid==2){
                $("#nrucdni").prop("disabled",false);
                $("#nrucdni").attr('maxlength','11');
              //  $("#nrucdni").val("");
                $(".rucdni").text("N° RUC");
                $(".nsolicitante").text("Razón Social");
            }
            return false;
        }

        function eOrden(){
            var p1=$('[name="editOrden"]:checked').val();
            if(p1==1){
                $(".voucherImgOrden").show();
                $('#voucherImgOrden').prop('disabled', false);
                $('.iOrden').prop('disabled',true);
                $('.iOrdenf').prop('disabled',false);
                $("#timgOrden").addClass("tachado");
            }else{
                $(".voucherImgOrden").hide();
                $('#voucherImgOrden').prop('disabled', true);
                $('.iOrden').prop('disabled',false);
                $('.iOrdenf').prop('disabled',true);
                $("#timgOrden").removeClass("tachado");
            }
        }

        function ePDT(){
            var p1=$('[name="editPDT"]:checked').val();
            if(p1==1){
                $(".voucherImgPDT").show();
                $('#voucherImgPDT').prop('disabled', false);
                $('.iPDT').prop('disabled',true);
                $('.iPDTf').prop('disabled',false);
                $("#timgPDT").addClass("tachado")
            }else{
                $(".voucherImgPDT").hide();
                $('#voucherImgPDT').prop('disabled', true);
                $('.iPDT').prop('disabled',true);
                $('.iPDTf').prop('disabled',false);
                $("#timgPDT").removeClass("tachado")
            }

        }

        function eRUC(){
            var p1=$('[name="editRUC"]:checked').val();
            if(p1==1){
                $(".voucherImgRUC").show();
                $('#voucherImgRUC').prop('disabled', false);
                $('.iRUC').prop('disabled',true);
                $('.iRUCf').prop('disabled',false);
                $("#timgRUC").addClass("tachado")
            }else{
                $(".voucherImgRUC").hide();
                $('#voucherImgRUC').prop('disabled', true);
                $('.iRUC').prop('disabled',true);
                $('.iRUCf').prop('disabled',false);
                $("#timgRUC").removeClass("tachado")
            }

        }

        function eDNI(){
            var p1=$('[name="editDNI"]:checked').val();
            if(p1==1){
                $(".voucherImg").show();
                $('#voucherImg').prop('disabled', false);
                $('.iDNI').prop('disabled',true);
                $('.iDNIf').prop('disabled',false);
                $("#timgDNI").addClass("tachado")
            }else{
                $(".voucherImg").hide();
                $('#voucherImgRUC').prop('disabled', true);
                $('.iDNI').prop('disabled',true);
                $('.iDNIf').prop('disabled',false);
                $("#timgDNI").removeClass("tachado")
            }

        }

        $(document).ready(function () {
            cambioTipoper2();
            eOrden();
            $("#tipoper").change(function () {
                cambioTipoper();
            });

            $("#editOrden").on("change",function (e) {
                eOrden();
            });

            $("#editPDT").on("change",function (e) {
                ePDT();
            });


            $("#editRUC").on("change",function (e) {
                eRUC();
            });

            $("#editDNI").on("change",function (e) {
                eDNI();
            });

        });
    </script>
    <?php
}
elseif($tipoOpera=='save') {
    ?>
    <div class="well" style="padding-top: 11px;padding-bottom: 10px;">
        <div class="row">
            <div class="col-md-4">
                <label for="tipoper" class="">Tipo Persona*</label>
                <select style="border: none;" class="border-bottom form-control text-capitalize tipoper" required
                        name="tipoper" id="tipoper">

                    <option value="">-- Seleccionar --</option>
                    <?php $objtp = SolicitudCredito::obtenerTipoPersona() ?>
                    <?php if ($objtp): ?>
                        <?php foreach ($objtp as $var): ?>
                            <option value="<?php echo $var->idtipoper ?>" ><?php echo $var->desctipo ?></option>
                        <?php endforeach ?>
                    <?php else: ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="form-group col-md-4 text-primary text-capitalize">
                <label for="nsolicitante" class="nsolicitante">Razon Social / Nombre y Apellido</label>
                <input style="border: none;" type="text" placeholder="Ingresar Datos" id="nsolicitante"
                       name="nsolicitante" class="border-bottom form-control" required value="">
                <input type="hidden" name="idSolicitud" value="">
                <input type="hidden" name="tipoOpera" value="">
            </div>
            <div class="form-group col-md-4">
                <label for="nrucdni" class="rucdni">N° RUC / DNI</label>
                <input style="border: none;" type="text" placeholder="Ingresar Datos" id="nrucdni" disabled
                       name="nrucdni" class="border-bottom form-control" required  onKeyPress='return soloNumero(event)'
                       value="">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="tmonto">Monto Facturación</label>
                <input style="border: none;" type="text" placeholder="Ingresar Monto" id="tmonto"   onKeyPress='return soloNumeroMonto(event)'
                       name="tmonto" class="border-bottom form-control" required value="">
            </div>
            <div class="form-group col-md-4 text-primary text-capitalize">
                <label for="noperacion">N° Operaciones</label>
                <input style="border: none;" type="text" placeholder="Ingresar N° Operaciones" id="noperacion" onKeyPress='return soloNumero(event)'
                       name="noperacion" class="border-bottom form-control" required value="">
            </div>
            <div class="form-group col-md-4">
                <label for="ndias">Dias de Crédito</label>
                <input style="border: none;" type="text" placeholder="Ingresar Dias Crédito" id="ndias" onKeyPress='return soloNumero(event)'
                       name="ndias" class="border-bottom form-control" required
                       value="">
            </div>
        </div>
        <div class="row">
            <div class="form-group  col-md-12">
                <label for="ndetalle" class="">Detalle</label>
                <input style="border: none;" type="text" placeholder="Ingresar Detalle" id="ndetalle" name="ndetalle" class="form-control x2 border-bottom focusUrgenteta">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="nrepLegal">Representante Legal</label>
                <input style="border: none;" type="text" placeholder="Ingresar Representante Legal" id="nrepLegal"
                       name="nrepLegal" class="border-bottom form-control"  value="">
            </div>
            <div class="form-group col-md-4 text-primary text-capitalize">
                <label for="dniRP">DNI</label>
                <input style="border: none;" type="text" placeholder="Ingresar Dni" id="dniRP"  onKeyPress='return soloNumero(event)'
                       name="dniRP" class="border-bottom form-control"  value="">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="njefCompras">Jefe de Compras</label>
                <input style="border: none;" type="text" placeholder="Ingresar Jefe Compras" id="njefCompras"
                       name="njefCompras" class="border-bottom form-control" required value="">
            </div>
            <div class="form-group col-md-4 text-primary text-capitalize">
                <label for="nemailC">Email</label>
                <input style="border: none;" type="email" placeholder="Ingresar Email" id="nemailC"
                       name="nemailC" class="border-bottom form-control" required value="">
            </div>
            <div class="form-group col-md-4">
                <label for="nCelTelfC">Cel./Telf.</label>
                <input style="border: none;" type="text" placeholder="Ingresar Cel./Telf." id="nCelTelfC"   onKeyPress='return soloNumero(event)'
                       name="nCelTelfC" class="border-bottom form-control" required value="">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="njefTeso">Jefe de Tesorería</label>
                <input style="border: none;" type="text" placeholder="Ingresar Jefe Tesorería" id="njefTeso"
                       name="njefTeso" class="border-bottom form-control" required value="">
            </div>
            <div class="form-group col-md-4 text-primary text-capitalize">
                <label for="nemailT">Email</label>
                <input style="border: none;" type="email" placeholder="Ingresar Email" id="nemailT"
                       name="nemailT" class="border-bottom form-control" required value="">
            </div>
            <div class="form-group col-md-4">
                <label for="nCelTelfT">Cel./Telf.</label>
                <input style="border: none;" type="text" placeholder="Ingresar Cel./Telf." id="nCelTelfT"   onKeyPress='return soloNumero(event)'
                       name="nCelTelfT" class="border-bottom form-control" required value="">
            </div>
        </div>
        <p class="font-bold  alert alert-success m-b-sm" style="">
            ANEXAR LOS DOCUMENTOS QUE SE SEÑALAN A CONTINUACIÓN.
        </p>

        <div class="row">
            <div class="col-md-12 form-group voucherImg">
                <label for="Forma*" >Copia DNI representate legal</label>
                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                    <div class="form-control" data-trigger="fileinput">
                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                        <span class="fileinput-filename"></span>
                    </div>
                    <span class="input-group-addon btn btn-default btn-file">
					    <span class="fileinput-new">Seleccionar Voucher de pago*</span>
    					<span class="fileinput-exists">Cambiar</span>
    					<input type="file" name="voucher"  id="voucherImg"  />
				    </span>
                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 form-group voucherImgRUC">
                <label for="Forma*" >Copia Ficha RUC</label>
                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                    <div class="form-control" data-trigger="fileinput">
                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                        <span class="fileinput-filename"></span>
                    </div>
                    <span class="input-group-addon btn btn-default btn-file">
			    		<span class="fileinput-new">Seleccionar Voucher de pago*</span>
			    		<span class="fileinput-exists">Cambiar</span>
			    		<input type="file" name="voucherRUC"  id="voucherImgRUC" />
			    	</span>
                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
                </div>
            </div>
        </div>
            <div class="row">
                <div class="col-md-12 form-group voucherImgPDT">
                    <label for="Forma*" >PDT presentados a SUNAT(ultimos dos meses)</label>
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
			        		<span class="fileinput-new">Seleccionar Voucher de pago*</span>
			        		<span class="fileinput-exists">Cambiar</span>
			        		<input type="file" name="voucherPDT"  id="voucherImgPDT"  />
			        	</span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group voucherImgOrden">
                    <label for="Forma*" >Orden de Servicio</label>
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
				        	<span class="fileinput-new">Seleccionar Voucher de pago*</span>
		          			<span class="fileinput-exists">Cambiar</span>
         					<input type="file" name="voucherOrden"  id="voucherImgOrden" required="" />
				        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <h3><strong>Nota:</strong> En caso ser cliente al contado, solo se adjunta Orden de Servicio.</h3>
            </div>
    </div>
    <script>
        $(document).ready(function () {
            $("#tipoper").change(function () {
                $selectid=$("#tipoper option:selected").val();
                if($selectid==1){
                    $("#nrucdni").prop("disabled",false);
                    $("#nrucdni").attr('maxlength','8');
                    $("#nrucdni").val("");
                    $(".rucdni").text("N° DNI");
                    $(".nsolicitante").text("Nombre y Apellido");
                }else if($selectid==2){
                    $("#nrucdni").prop("disabled",false);
                    $("#nrucdni").attr('maxlength','11');
                    $("#nrucdni").val("");
                    $(".rucdni").text("N° RUC");
                    $(".nsolicitante").text("Razon Social");
                }else{
                    $("#nrucdni").prop("disabled",true);
                    $("#nrucdni").val("");
                    $(".rucdni").text("N° RUC / DNI");
                    $(".nsolicitante").text("Razon Social / Nombre y Apellido");
                }
            });
        });
    </script>
    <?php
}
?>




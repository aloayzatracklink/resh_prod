<?php 
if ($_POST["rowid"] == "new") {
?>

<div class="row">
      <div class="col-md-12 text-ceter">
        <label for="tipocliente" >Tipo cliente</label>
        <select id="tipocliente" name="tipoCliente" class="form-control" required name="tipocliente">
            <option value="" disabled selected >Tipo cliente </option>
            <option value="NATURAL">Natural</option>
            <option value="JURIDICA">Juridico</option>
        </select>
      </div>
</div>

<!-- ejecutivo please -->

<section class="jr">
    <div class="row">
          <div class="col-md-8 text-ceter">
            <label for="razons" >Razon Social</label>
            <input  required type="text"  id="razons" name="razons" class="border-bottom form-control"  >
          </div>
            <div class="col-md-4 text-ceter">
            <label for="ruc" >RUC</label>
            <input  required type="text"  id="ruc" name="ruc" class="border-bottom form-control"  >
          </div>
    </div>
    
</section>

<section  class="ntr">
    <div class="row">
      <div class="col-md-8 text-ceter">
        <label for="name" >Nombres y Apellidos</label>
        <input  required type="text"  id="razons" name="name" class="border-bottom form-control"  >
      </div>
        <div class="col-md-4 text-ceter">
        <label for="dni" >DNI</label>
        <input  required type="text"  id="dni" name="dni" class="border-bottom form-control"  >
      </div>
    </div>
</section>

<div class="row">
      <div class="col-md-4 text-ceter">
        <label for="amount" >Monto de facturación</label>
        <input  required type="text"  id="amount" name="amount" class="border-bottom form-control"  >
      </div>
      <div class="col-md-4 text-ceter">
        <label for="dc" >Dias de credito</label>
        <select id="tipocliente" name="dc" class="form-control" required name="dc">
            <option value="" disabled selected >Dias de credito</option>
            <option value="7" >7</option>
            <option value="15" >15</option>
            <option value="30" >30</option>
            <option value="60" >60</option>
            <option value="CONTADO" >Contado</option>
        </select>
      </div>
    <div class="col-md-4 text-ceter">
        <label for="operations" >Numero operaciones</label>
        <input  required type="text"  id="operations" name="operations" class="border-bottom form-control"  >
      </div>
</div>
<div class="row">
      <div class="col-md-12 text-ceter">
        <label for="details" >Detalle de solicitud</label>
        <input  required type="text"  id="details" name="details" class="border-bottom form-control"  >
      </div>
</div>

<div class="row">
      <div class="col-md-8 text-ceter">
        <label for="rlegal" >Representante legal</label>
        <input  type="text"  id="rlegal" name="rlegal" class="border-bottom form-control"  >
      </div>
    <div class="col-md-4 text-ceter">
        <label for="rlegaldni" >DNI</label>
        <input  type="text"  id="rlegaldni" name="rlegaldni" class="border-bottom form-control"  >
      </div>
</div>

<div class="row">
      <div class="col-md-5 text-ceter">
        <label for="jcom" >Jefe de compras</label>
        <input required  type="text"  id="jcom" name="jcom" class="border-bottom form-control"  >
      </div>
    <div class="col-md-4 text-ceter">
        <label for="jcomemail" >Email</label>
        <input required  type="text"  id="jcomemail" name="jcomemail" class="border-bottom form-control"  >
      </div>
    <div class="col-md-3 text-ceter">
        <label for="jcomephone" >Cel./Tefl.</label>
        <input required  type="text"  id="jcomephone" name="jcomephone" class="border-bottom form-control"  >
      </div>
</div>

<div class="row">
      <div class="col-md-5 text-ceter">
        <label for="jtes" >Jefe de tesoreria</label>
        <input required  type="text"  id="jtes" name="jtes" class="border-bottom form-control"  >
      </div>
    <div class="col-md-4 text-ceter">
        <label for="jtesemail" >Email</label>
        <input required  type="text"  id="jtesemail" name="jtesemail" class="border-bottom form-control"  >
      </div>
    <div class="col-md-3 text-ceter">
        <label for="jtesephone" >Cel./Tefl.</label>
        <input required  type="text"  id="jtesephone" name="jtesephone" class="border-bottom form-control"  >
      </div>
</div>
<hr>
<div class="row">
      <div class="col-md-12 text-ceter">
      <label for="">Anexar los documentos que se señalan a continuación:</label>
        <ul>
            <li>Copia DNI representante legal</li>

                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <div class="form-control" data-trigger="fileinput">
                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                <span class="fileinput-filename"></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                <span class="fileinput-new">Seleccionar archivo</span>
                <span class="fileinput-exists">Cambiar</span>
                <input type="file" name="file1" multiple/>
                </span>
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                </div>
            <li>Copia ficha RUC</li>
                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <div class="form-control" data-trigger="fileinput">
                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                <span class="fileinput-filename"></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                <span class="fileinput-new">Seleccionar archivo</span>
                <span class="fileinput-exists">Cambiar</span>
                <input type="file" name="file2" multiple/>
                </span>
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                </div>
            <li>PDT  presentados a sunat (últimos dos meses) </li>
                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <div class="form-control" data-trigger="fileinput">
                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                <span class="fileinput-filename"></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                <span class="fileinput-new">Seleccionar archivo</span>
                <span class="fileinput-exists">Cambiar</span>
                <input type="file" name="file3" multiple/>
                </span>
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                </div>

            <li>Orden de servicio</li>
                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <div class="form-control" data-trigger="fileinput">
                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                <span class="fileinput-filename"></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                <span class="fileinput-new">Seleccionar archivo</span>
                <span class="fileinput-exists">Cambiar</span>
                <input type="file" name="file3" multiple/>
                </span>
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                </div>
        <label for="">
          <b>Nota:</b> en caso de ser al contado, solo adjuntar Orden de servicio</label>
        </ul>

          

            

            

      </div>
</div>

<script>
$(document).ready(function(){
    $(".jr").hide();
    $(".ntr").hide();
})

    $("#tipocliente").change(function(){
        if($(this).val()==="NATURAL"){
            $(".ntr").show();
            $(".jr").hide();
            $("#name").prop("disabled",false);
            $("#dni").prop("disabled",false);
            $("#ruc").prop("disabled",true);
            $("#razons").prop("disabled",true);
        }else{
            $(".jr").show();
            $(".ntr").hide();

            $("#name").prop("disabled",true);
            $("#dni").prop("disabled",true);
            $("#ruc").prop("disabled",false);
            $("#razons").prop("disabled",false);
        }
    });
</script>
<?php
}else{
    include_once("../../includes/init.php");
    $comisionid=htmlspecialchars(htmlentities($_POST["rowid"]));
    $comisionData=Comisiones::obtenerDatosDeComision($comisionid);
    //var_dump($comisionData);
    $clienDatos=Cliente::obtenerDatosDeclientePorId($comisionData->idcliente);
    $vhDatos=Vehiculo::obtenerVehiculo($comisionData->idvehiculo);
    if ($comisionData->idcanal==0) {
        $canalDatos=Canales::obtenerDatosTITULARPorId($comisionData->idtitular);
    }else{
        $canalDatos=Canales::obtenerDatosCanalPorId($comisionData->idcanal);
    }
    $obMenu=Usuario::obtenerMenu($session->idusuario);
    $o=Usuario::obtenerUsuario($comisionData->idCreador); 
    $objFact=DatosFacturacion::obtenerDatosFacturacionPorId($comisionData->idfacturacion);
    //var_dump($objFact);
  ?>
  <input type="hidden" name="comisionid" value="<?= $comisionid ?>" >
  <input type="hidden" name="idfacturacion" value="<?= $comisionData->idfacturacion ?>" >
  <input type="hidden" name="estado" value="<?= $comisionData->estado ?>" >
  <?php if ($comisionData): ?>
  <input type="hidden" name="canalid" value="<?= $comisionData->idcanal ?>" >    
  <?php endif ?>
  <div class="well" style="padding-top: 11px;padding-bottom: 10px;">
  <div class="row">
    <div class="row">
      <div class="form-group col-md-4">
        <label for="Observaciones" ><?= $retVal = ($clienDatos->tipoCliente=="Natural") ? "DNI" : "RUC" ?></label>
        <input name="" style="border: none;" type="text" placeholder="Web" id="Observaciones" class="border-bottom form-control" readonly  value="<?= $retVal = ($clienDatos->tipoCliente=="Natural") ? $clienDatos->dni : $clienDatos->ruc ?>">
      </div>
      <div class="form-group col-md-8">
        <label for="numeroTecnico" ><?=  ($clienDatos->tipoCliente=="Natural") ? "Nombres" : "Razon Social"  ?></label>
        <input  required readonly style="border: none;" type="text" id="numeroTecnico" name="" class="border-bottom form-control"  value="<?= $datosCliente = ($clienDatos->tipoCliente=="Natural") ? $clienDatos->nombre." ".$clienDatos->apellidosC : $clienDatos->razonSocial ?> ">
        <input type="hidden" value="<?= $datosCliente ?>" name="datosCliente">
      </div>
    </div>
    <hr>
     <div class="row">
      <div class="form-group col-md-3">
        <label for="placa" >Placa</label>
        <input name="" style="border: none;" type="text" placeholder="Web" id="placa" class="border-bottom form-control" readonly  value="<?= $vhDatos->placa ?>">
      </div>
      <div class="form-group col-md-3">
        <label for="marca" >Marca</label>
        <input  required readonly style="border: none;" type="text" id="marca" name="" class="border-bottom form-control"  value="<?= $vhDatos->marca ?> ">
      </div>
      <div class="form-group col-md-3">
        <label for="planN" >Plan</label>
        <input  required readonly style="border: none;" type="text" id="planN" name="" class="border-bottom form-control"  value="<?= $vhDatos->plan ?> ">
      </div>
      <div class="form-group col-md-3">
        <label for="chasis" >Chasis</label>
        <input  required readonly style="border: none;" type="text"  id="chasis" name="" class="border-bottom form-control"  value="<?= $vhDatos->chasis ?> ">
      </div>
    </div>
<div class="row">
<div class="col-md-12">
          <h3 class="text-center">Ejecutivo Tracklink: <?= $o->usuarioNombre." ".$o->usuarioApellidoPaterno;   ?></h3>
      </div>
  </div>
    <div class="row">
      <div class="col-md-4">
        <label for="tipoCanal" >Tipo canal</label>
        <input  required <?= (empty($objFact->tipoCanal))?'':'readonly' ?> style="border: none;" type="text"  id="tipoCanal" name="tipoCanal" class="border-bottom form-control"  value="<?= strtoupper($objFact->tipoCanal);   ?>">
      </div>
      <div class="col-md-4">

        <label for="nombreCanal" >Nombre Canal</label>
        <input  required <?= (empty($objFact->nombreCanal))?'':'readonly' ?> style="border: none;" type="text"  id="nombreCanal" name="nombreCanal" class="border-bottom form-control"  value="<?= strtoupper($objFact->nombreCanal) ?> ">
      </div>
<div class="col-md-4">
        <label for="ejecutivoCanal" >Ejecutivo canal</label>
        <input  required <?= (empty($objFact->ejecutivoCanal))?'':'readonly' ?> style="border: none;" type="text"  id="ejecutivoCanal" name="ejecutivoCanal" class="border-bottom form-control"  value="<?= strtoupper($objFact->ejecutivoCanal) ?> ">
      </div>
    </div>

</div>
<hr>
<?php if ($canalDatos){ ?>
<input type="hidden" value="<?= $canalDatos->idtitular ?>" name="agregarId">
<div class="row">
<div class="col-md-4">
    <label for="ruc" >Ruc</label>
    <input style="border: none;"  type="text" placeholder="Ruc *" id="ruc" name="ruc" class="border-bottom form-control" value="<?= strtoupper($canalDatos->ruc) ?>"  >
</div>
<div class="form-group col-md-8">
    <label for="titular" >Titular de la cuenta y del recibo por honorarios *</label>
    <input style="border: none;" type="text" placeholder="Titular *" id="titular" name="titular" class="border-bottom form-control" value="<?= strtoupper($canalDatos->titular) ?>"  >
  </div>
</div>
 <div class="row">
<div class="form-group col-md-4">
    <label for="moneda" >Banco</label>
    <select style="border: none;"  name="bancoCanal" class="cd-selec form-control border-bottom" readonly id="monedaCanal">
            <option value="<?php echo $canalDatos->banco ?>" selected><?php echo $canalDatos->banco ?></option>
    </select>
  </div>
<div class="form-group col-md-4">
    <label for="moneda" >Numero Cuenta</label>
        <input style="border: none;" readonly  type="text" placeholder="Numero Cuenta" id="numeroCuenta" name="numeroCuenta" class="border-bottom form-control" value="<?= strtoupper($canalDatos->numeroCuenta) ?>"  >
    </select>
  </div>
  <div class="form-group col-md-4">
    <label for="moneda" >Moneda *</label>
    <select style="border: none;"  name="moneda" class="cd-selec form-control border-bottom" readonly id="monedaCanal">
	      		<option value="<?php echo $canalDatos->moneda ?>" selected><?php echo $canalDatos->moneda ?></option>
    </select>
  </div>
</div> 
    
<div class="row">
    <div class="form-group col-md-4" id='cci1'>
    <label for="cci2" >CCI</label>
    <input style="border: none;" type="text" placeholder="CCI *" id="cci2" name="cci" class="border-bottom form-control" readonly value="<?= strtoupper($canalDatos->cci) ?>" >
  </div>
</div>   

<?php }else{ ?>
<div id="agregar">
  <div class="row">
    <div class="form-group col-md-12">
      <label for="canalIdcom" class="">Títular del RH y cuenta </label>
      <input style="border: none;" type="text" placeholder="Títular del RH y cuenta " id="canalIdnew" name="dni" class="canalId border-bottom form-control text-capitalize text-uppercase canalIdnew" autocomplete="off" required="" >
    </div>      
  </div>
    <div id="comisionHt">
    </div>
</div>
<div id="registrarNuevo">
    <button type="button" class="btn btn-white text-center" id="buscarExistente">Buscar registro existente</button>
  <div class="row data"></div>
</div>
<?php } ?>
<hr>
<div class="row">
  <div class="form-group col-md-3">
    <label for="totalM" >Monto total *</label>
    <input style="border: none;" type="text" placeholder="Monto total" id="totalM" name="montoTotal" class="border-bottom form-control" value="<?= $comisionData->montoTotal ?>" required >
  </div>  
  <div class="form-group col-md-3">
    <label for="comi" >Comision *</label>
    <input style="border: none;" type="text" placeholder="Comision" id="comi" name="comision" class="border-bottom form-control" value="<?= limpiarPendiente($comisionData->comision) ?>" required >
  </div>  
 
  <div class="form-group col-md-6">
    <label for="fe" >Fecha registro *</label>    
    <span class="help-block m-b-none"><?= hace($comisionData->fechaRegistro) ?></span>
  </div>  
</div>
<?php if (empty($comisionData->reciboHonorarios)): ?>  
<?php if ($comisionData->estadoFacturacion=='34'): ?>  
<div class="row">
    <div class="col-lg-12">
    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
      <div class="form-control" data-trigger="fileinput">
        <i class="glyphicon glyphicon-file fileinput-exists"></i>
        <span class="fileinput-filename"></span>
      </div>
      <span class="input-group-addon btn btn-default btn-file">
        <span class="fileinput-new">Adjuntar recibo por honorarios </span>
        <span class="fileinput-exists">Cambiar</span>
        <input type="file" name="recibo" />
      </span>
      <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
    </div> 
  </div>
</div>
<?php else: ?>
<div class="row">
    <div class="col-lg-12">
     <label class='text-danger' > Para adjuntar el RH la venta tiene que estar facturada </label>
  </div>
</div>
<?php endif ?>

<?php else: ?>
  <div class="row">
    <a  class="btn btn-primary text-center  col-md-6 col-md-offset-3" href="../../public/vouchers/<?= $comisionData->reciboHonorarios ?>" target="_blank" >Descargar/ver recibo por Honorarios</a>    
  </div>
  <input type="hidden" name="reciboH" value="<?= $comisionData->reciboHonorarios  ?>">
<?php endif ?>
<?php if ($obMenu->comisiones=="si"){ ?>  
<div class="row">
      <div class="form-group col-md-4">
        <label for="rhonorario" class="text-danger"> RECIBO HONORARIO *</label>
        <input style="border: none;" type="text" placeholder=" RECIBO HONORARIO* " id="rhonorario" name="rhonorario" required class="border-bottom form-control" value="<?= $comisionData->numeroReciboHonorario ?>">
      </div>
      
<?php if (!empty($comisionData->reciboHonorarios)){ ?>  
    <div class="form-group col-md-4">
        <br>
        <button type="button" class="btn btn-info text-center boorar" value="<?= $comisionData->idcomision ?>" >Eliminar RH</button>    
      </div>
    <?php } ?>  
</div>

<div class="row ">
    <div class="form-group col-md-4">
        <br>
        <button type="button" class="btn btn-warning text-center cambiaar" value="<?= $comisionData->idcomision ?>" >Cambiar títular</button>    
      </div>
<div class="col-md-4">
    <div class="form-group col-md-4">
        <br>
        <button type="button" class="btn btn-danger text-center eliminar" value="<?= $comisionData->idcomision ?>" >Eliminar de comisiones</button>    
      </div>
</div>

<div class="col-md-4">

</div>

</div>
<?php } ?>
<div class="row">
  <div class="form-group col-md-4">
    <label for="documento" >Documento *</label>
    <input required  style="border: none;" type="text" placeholder="Documento Factura" id="documento" name="Documento" readonly class="border-bottom form-control"  value="<?= $comisionData->documentoFacturacion ?>"  >
  </div>
  <div class="form-group col-md-3">
    <label for="numFactura">Numero de facturacion</label>
    <input  style="border: none;" type="text" placeholder="Numero de facturacion" id="numFactura" name="numeroFacturacion" class="border-bottom form-control" required readonly value="<?= $comisionData->numeroFacturacion ?>"  >
  </div>
  <div class="form-group col-md-5">
    <label for="rhnorario" >Recibo Honorario</label>
    <input  required  style="border: none;" type="text" placeholder="Recibo Honorario" id="rhnorario" class="border-bottom form-control"   value="<?= $comisionData->numeroReciboHonorario ?>" readonly>
  </div>
  
</div>
<?php if ($comisionData->estado==67): ?>
<div class="row">
<div class="col-md-12 form-group">&nbsp;&nbsp;<label for="">PAGADO EN LA FECHA <?= $comisionData->observacion ?></label></div>
  </div>
<?php endif ?>
<?php if ($comisionData->estado==62): ?>
  <div class="row">
    <div class="col-md-12 form-group">&nbsp;&nbsp;<label for="">Observación de pago: <?= $comisionData->observacion ?></label></div>
  </div>  
<?php endif ?>

<script src="../assets/js/jquery-confirm.min.js"></script>
<script>
$('.boorar').click(function(e) {
  var idcomision=$(this).val();
  $.confirm({
      theme: 'modern',
      title: '¿Cambiar de RH ?',
      content: 'Al confirmar este dialogo, el ejecutivo comercia debe subir el nuevo RH desde su perfil.  ',
      buttons: {
          Confirmar: {
            btnClass: 'btn-blue',
            action:function () {   
                var rh='si';
             $.ajax({
                type : 'POST',
                url  : 'controlador/borrarRH',
                data :  {id:idcomision,rh:rh},
                success : function(data)
                {
                    $("#filter").focus();                 
                    //$("#bateriasOModal.bs.modal").hide();
                    $.ajax({
                        type : 'POST',
                        url : 'modal/comisionDataModal', 
                        data : {rowid:idcomision},
                        success : function(data)
                        {
                            $('.modal-body').html(data);
                        }
                    });
                }
            });        
            }
          },          
          Cancelar: {
            btnClass: 'btn-warning',
            action:function () {  
              $("#filter").focus(); 
              $.alert('No se realizo nigun cambio!');
            }
          }
      }
  });
});

$('.cambiaar').click(function(e) {
  var idcomision=$(this).val();
  $.confirm({
      theme: 'modern',
      title: '¿Cambiar de Titular ?',
      content: 'cambiar te títular, número de cuenta banco, etc',
      buttons: {
          Confirmar: {
            btnClass: 'btn-blue',
            action:function () {   
                var titular='si';
             $.ajax({
                type : 'POST',
                url  : 'controlador/borrarRH',
                data :  {id:idcomision,titular:titular},
                success : function(data)
                {
                    $("#filter").focus();                 
                    //$("#bateriasOModal.bs.modal").hide();
                    $.ajax({
                        type : 'POST',
                        url : 'modal/comisionDataModal', 
                        data : {rowid:idcomision},
                        success : function(data)
                        {
                            $('.modal-body').html(data);
                        }
                    });
                }
            });        
            }
          },          
          Cancelar: {
            btnClass: 'btn-warning',
            action:function () {  
              $("#filter").focus(); 
              $.alert('No se realizo nigun cambio!');
            }
          }
      }
  });
  });

$('.eliminar').click(function(e) {
  var idcomision=$(this).val();
  $.confirm({
      theme: 'modern',
      title: '¿Desea eliminar esta comisión ?',
      content: 'eliminar de comisiones',
      buttons: {
          Confirmar: {
            btnClass: 'btn-blue',
            action:function () {   
                var eliminar='si';
             $.ajax({
                type : 'POST',
                url  : 'controlador/borrarRH',
                data :  {id:idcomision,eliminar:eliminar},
                success : function(data)
                {
                    $("#filter").focus();                 
                   $("#bateriasOModal").modal('toggle');
                 //   $.ajax({
                 //       type : 'POST',
                 //       url : 'modal/comisionDataModal', 
                 //       data : {rowid:idcomision},
                 //       success : function(data)
                 //       {
                 //           $('.modal-body').html(data);
                 //       }
                 //   });
                }
            });        
            }
          },          
          Cancelar: {
            btnClass: 'btn-warning',
            action:function () {  
              $("#filter").focus(); 
              $.alert('No se realizo nigun cambio!');
            }
          }
      }
  });
  });
   </script>
<script>
   $("#canalIdnew").bind("keyup focus",function()
           { 
           var canalId = $(this).val();
           if (canalId.length>=3){
           $.ajax({
                type : 'POST',
                url  : 'ajax/canalnew',
                data : {canalId:canalId},
                success : function(data)
                {
                $("#comisionHt").html(data);
                }
                });
           }else{
           $("#comisionHt").html("<p>Tienes que agregar el canal de la comisión</p>");
           $('#registrarNuevo').hide('slow');
           }
           return false;
           });

        $('#agregar').show();
        $('#registrarNuevo').hide();
        $('#buscarExistente').click(function(){
            $('#registrarNuevo').hide('slow');
            $('.data').html('');
            $('#agregar').show();
        });

            $('#otroBanco').hide();
            $('#bancoCanal').change(function(){
                if ($(this).val()=='OTRO BANCO') {
                    $('#otroBanco').show();
                    $('#numeroCuenta').hide('slow');
                    $('#numeroCuenta').prop('required',false);
                    $('#nombreBanco').prop('required',true);
                    $('#cci').prop('required',true);
                    $('#cci1').prop('disabled',true);
                    $('#cci1').hide('slow');
                    $('#cci2').prop('name','cciOLD');
                }else{
                    $('#numeroCuenta').show();
                    $('#numeroCuenta').prop('required',true);
                    $('#otroBanco').hide('slow');
                    $('#nombreBanco').prop('required',false);
                    $('#cci').prop('required',false);
                    $('#cci1').show();
                    $('#cci2').prop('name','cci');
                }
            });
</script>
<?php
} 
?>


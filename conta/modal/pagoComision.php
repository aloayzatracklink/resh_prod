<?php 
if ($_POST['rowid']) {
	include_once("../../includes/head.php");
	$idComision=htmlspecialchars(htmlentities($_POST['rowid']));
	$comisionObj=Comisiones::obtenerDatosDeComision($idComision);
	$clienteObj=Cliente::obtenerDatosDeclientePorId($comisionObj->idcliente);
    if ($comisionObj->idtitular<>'0') {
        $canalObj=Canales::obtenerDatosTITULARPorId($comisionObj->idtitular);
    }else{
        $canalObj=Canales::obtenerDatosCanalPorId($comisionObj->idcanal);
    }
$dd=DatosFacturacion::obtenerDatosFacturacionPorId($comisionObj->idfacturacion)
	?>
	<link href="../../assets/css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">
	<div class="well" style="padding-top: 11px;
	padding-bottom: 10px;">
	<div class="row">
		<div class="row">
		<h2 class="text-center">
			<input type="hidden" name="idComision" value="<?php echo $idComision ?>">
			<div class="form-group col-md-12">
				<label for="nombreCliente" >Nombre de cliente</label>
				<input id="nombreCliente"   name="nombreCliente" style="border: none;" type="text" class="border-bottom form-control" readonly="" value="<?php  echo $retVal = ($clienteObj->tipoCliente=="Natural") ? $clienteObj->nombre." ".$clienteObj->apellidosC : $clienteObj->razonSocial ; ?>">
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<label for="canal">Canal</label><input id="canal" style="border: none;" type="text" class="border-bottom form-control" readonly="" value="<?= $dd->tipoCanal ?>">
			</div>
			<div class="col-md-4">
				<label for="canal">Nombre canal</label><input id="canal" style="border: none;" type="text" class="border-bottom form-control" readonly="" value="<?=  $dd->nombreCanal ?>">
			</div>
			<div class="col-md-5">
				<label for="canal">Ejecutivo de canal</label><input id="canal" style="border: none;" type="text" class="border-bottom form-control" readonly="" value="<?=  $dd->ejecutivoCanal ?>">
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<label for="canal">Banco</label><input id="canal" style="border: none;" type="text" class="border-bottom form-control" readonly="" value="<?=  $canalObj->banco ?>">
			</div>
			<div class="col-md-2">
				<label for="canal">Moneda</label><input id="canal" style="border: none;" type="text" class="border-bottom form-control" readonly="" value="<?=  $canalObj->moneda ?>">
			</div>
			<div class="col-md-4">
				<label for="canal">Numero Cuenta</label><input id="canal" style="border: none;" type="text" class="border-bottom form-control" readonly="" name="numeroDecuentaPago" value="<?=  $canalObj->numeroCuenta ?>" >
			</div>
			<div class="col-md-3">
				<label for="canal">CCI</label><input id="canal" style="border: none;" type="text" class="border-bottom form-control" readonly="" value="<?= ($canalObj->cci=='')?'':$canalObj->cci ?>">
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<label for="canal">Titular</label><input id="canal" style="border: none;" type="text" class="border-bottom form-control" readonly="" value="<?= $canalObj->titular ?>">
			</div>
			<div class="col-md-3">
				<label for="canal">Ruc</label><input id="canal" style="border: none;" type="text" class="border-bottom form-control" readonly="" value="<?= $canalObj->ruc ?>">
			</div>
			<div class="col-md-3">
				<label for="canal">N° R.H.</label><input id="canal" style="border: none;" type="text" class="border-bottom form-control" readonly="" value="<?= $comisionObj->numeroReciboHonorario ?>">
			</div>			
		</div>
	</div>
</div>
<?php if (empty($comisionObj->reciboHonorarios)): ?>  
<div class="row">
    <div class="col-lg-12">
 	"No subio recibo por h."
  </div>
</div>
<?php else: ?>
         <div class="row">
    <img class="text-center col-md-6 col-md-offset-3" alt="Recibo" src="../../public/vouchers/<?= $comisionObj->reciboHonorarios ?>" >      
    </div>
  <div class="row">
    <a  class="btn btn-primary text-center  col-md-6 col-md-offset-3" href="../../public/vouchers/<?= $comisionObj->reciboHonorarios ?>" target="_blank" >Descargar/ver recibo por Honorarios</a>    
  </div>
  <input type="hidden" name="reciboH" value="<?= $comisionObj->reciboHonorarios  ?>">
<?php endif ?>

<hr>
<div class="row">
	<div class="col-md-3">
		<label for="documento" >Documento *</label>
		<input  style="border: none;" type="text" placeholder="Documento *" id="documento" name="documento" class="border-bottom form-control" required value="<?= $comisionObj->documentoFacturacion ?>" >
	</div>
	<div class="col-md-3">
		<label for="observaciones" >Numero *</label>
		<input  style="border: none;" type="text" placeholder="Numero *" id="observaciones" name="observaciones" class="border-bottom form-control" required value="<?= $comisionObj->numeroFacturacion ?>">
	</div>
	<div class="col-md-3">
		<label for="montoTotal" >Monto Total *</label>
		<input  style="border: none;" type="text" placeholder="Monto Total *" id="montoTotal" name="montoTotal" class="border-bottom form-control" required value="<?= $comisionObj->montoTotal ?>">
	</div>
	<div class="col-md-3">
		<label for="comision" >Comision *</label>
		<input  style="border: none;" type="text" placeholder="Comision *" id="comision" name="comision" class="border-bottom form-control" required value="<?= $comisionObj->comision ?>">
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12">
		<label for="">Ejecutivo comeercial Tracklink: <?php $usuEje=Usuario::obtenerUsuario($comisionObj->idCreador) ?> <?= $usuEje->usuarioNombre." ".$usuEje->usuarioApellidoPaterno; ?></label>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
	<?php $usuveri=Usuario::obtenerUsuario($comisionObj->idVerficadorPago) ?> 
		<label for="">Estado pago: <?= obtenerEstado($comisionObj->estadoPago) ?> Verificado por:<?= $usuveri->usuarioNombre." ".$usuveri->usuarioApellidoPaterno; ?></label>
	</div>
</div>

<hr>
<div class="row">
	<div class="col-md-2">
		<label for="pagado">¿Pagado?</label>
		<div class="switch">
			<div class="onoffswitch">
				<input type="checkbox" class="onoffswitch-checkbox" id="pagado" value="si" name="pagado" <?= ($comisionObj->estado==67)?"checked":"" ?>>
				<label class="onoffswitch-label" for="pagado">
					<span class="onoffswitch-inner"></span>
					<span class="onoffswitch-switch"></span>
				</label>
			</div>
	<?php if ($comisionObj->estado==67): ?>
	<span class="help-block m-b-none">Fecha hora del pago. <?= hace($comisionObj->fechaPagado) ?></span>
	<?php endif ?>
		</div>
	</div>
	<?php if ($comisionObj->estado==67): ?>
	<input type="hidden" name="pagadoOK" value="si">		
	<?php endif ?>
	<div class="col-md-10">
		<label for="Observaciones" >Observacion *</label>
		<input  style="border: none;" type="text" placeholder="Observaciones *" id="Observaciones" name="observacion" class="border-bottom form-control" required value="<?= $retVal = (!empty($comisionObj->observacion) ) ? $comisionObj->observacion : "" ; ?>">
	</div>
</div> 
<?php
} 
?>

<?php

class tablaProgramacion extends Recursos
{
   public $idtablaProgramacion; 
   public $fecha; 
   public $hora; 
   public $estado; 
   public $idoperacion; 
   public $idcita; 
   public $fechaRegistro; 
   public $idusuario; 
   public $rpara; 
   public $idtecnico; 
   public $idturno; 
   public $sede; 
   public $observacion; 
   public $fijo;
   public $horafin;

    public $tth_instalacion;
    public $tth_trasalado;
    public $iddistrito;
    public $ubicacion;
    public $tt;

   public function registrarTrabajoEntabla(){
       global $db;
       global $session;

       $bhf="ADDTIME('$this->hora',SUBTIME('".preg_replace('/\s+/', '', $this->tth_instalacion)."',(case when COALESCE('".preg_replace('/\s+/', '', $this->tth_instalacion)."','00:00')='00:00' then '00:01' ELSE '00:05' end)))";
       $bhff="ADDTIME('$this->hora',SUBTIME('".preg_replace('/\s+/', '', $this->tth_instalacion)."',(case when COALESCE('".preg_replace('/\s+/', '', $this->tth_instalacion)."','00:00')='00:00' then '00:00' ELSE '00:00' end)))";
       $bhi="SUBTIME('$this->hora',ADDTIME('$this->tth_trasalado',(case when COALESCE('$this->tth_trasalado','00:00')='00:00' then '00:01' ELSE '00:05' end)))";
       $hf="CASE WHEN ($bhf>='13:00' and $bhf<'14:00') THEN (ADDTIME($bhf,'01:00')) 
                ELSE
                CASE WHEN $bhi<'13:00' AND $bhf>='14:00' THEN 
                CASE WHEN hora>='14:00' THEN ($bhf) ELSE (ADDTIME($bhf,'01:00')) END  
                ELSE $bhf
                END
            END ";
       $hi=" CASE WHEN ($bhi>='13:00' and $bhi<'14:00') THEN (SUBTIME($bhi,'01:00')) 
                ELSE
                CASE WHEN $bhi<'13:00' AND $bhf>='14:00' THEN 
                 CASE WHEN hora>='14:00' THEN (SUBTIME($bhi,'01:00'))  ELSE ($bhi) END  
                ELSE $bhi
                END
            END  ";
      // echo $hi;
       $sqlrtet="INSERT INTO tablaProgramacion (fecha,hora,horafin,horatraslado,fechaRegistro,idusuario,idtecnico,tth_instalacion,iddistrito,ubicacion,tth_trasalado) VALUES ('";
       $sqlrtet.=$db->escapeString(cambiarPorguion($this->fecha))."','";
       $sqlrtet.=$db->escapeString($this->hora)."',";
       $sqlrtet.=$hf.",";
       $sqlrtet.="'00:00','";
       $sqlrtet.=$db->escapeString(date('YmdHis'))."','";
       $sqlrtet.=$db->escapeString($session->idusuario)."','";
       $sqlrtet.=$db->escapeString($this->idtecnico)."','";
       $sqlrtet.=$db->escapeString(preg_replace('/\s+/', '', $this->tth_instalacion))."',";
       $sqlrtet.=$db->escapeString($this->iddistrito).",'";
       $sqlrtet.=$db->escapeString($this->ubicacion)."','";
       $sqlrtet.=$db->escapeString($this->tth_trasalado)."')";
       if ($db->query($sqlrtet)){
           $this->idtablaProgramacion=$db->ultimoIdInsertado();
           $dat=$this->obteneDatosDeprogramacion();
           $this->deleteResPReg($this->hora,$dat->horafin,cambiarPorguion($this->fecha),$this->idtecnico,$this->idtablaProgramacion);
           return true;
       }else{
           return false;
       }
   }

   public function deleteResPReg($horaini,$horafin,$fecha,$idtec,$idtbprog){
       global $db;
       $sql="DELETE FROM tablaProgramacion where idtablaProgramacion in (((SELECT idtablaProgramacion FROM (select idtablaProgramacion from tablaProgramacion 
            where ((hora<'".$horaini."' and horafin>'".$horaini."') or (hora<('".$horafin."') and horafin>('".$horafin."')) or (hora>='".$horaini."' and horafin<=('".$horafin."'))) 
            and fecha='".$fecha."' and idtecnico='$idtec' and (idoperacion=0 and idcita=0) and idtablaProgramacion!='".$idtbprog."' )A))) ";
      // echo $sql." este es el sql";
            return ($db->query($sql))?true:false;
   }

    public function insertaridcita(){
       global $db;
       $sqlicid="UPDATE tablaProgramacion SET idcita='$this->idcita' WHERE idtablaProgramacion='$this->idtablaProgramacion'";
       return ($db->query($sqlicid))?true:false;
   }

   public function insertaridoperacion(){
       global $db;
       $sqlicid="UPDATE tablaProgramacion SET idoperacion='$this->idoperacion' WHERE idtablaProgramacion='$this->idtablaProgramacion'";
       return ($db->query($sqlicid))?true:false;
   }

   public function insertaridoperacionDoble(){
       global $db;
       $sqlicid="UPDATE tablaProgramacion SET idoperacion='$this->idoperacion' WHERE idtablaProgramacion='$this->idtablaProgramacion'";
       if($db->query($sqlicid)){
          /* $this->hora=obtenerSiguienteHora($this->fecha,$this->hora);
           if (!$this->verSiesFijo()) {
                if($this->registrarTrabajoEntabla()){
                        $sqlicid2="UPDATE tablaProgramacion SET idoperacion='$this->idoperacion' WHERE idtablaProgramacion='$this->idtablaProgramacion'";
                        return ($db->query($sqlicid2))?true:false;
                }else{
                    die('fail en registrar nuevamente');
                }
           }*/
       }else{
           return false;
       }
   }


   public function obtenerTrabajosParaLafecha() {
       $oTpla=self::sentencia("SELECT * FROM tablaProgramacion WHERE fecha='$this->fecha'");
       return !empty($oTpla)?$oTpla:false;
   }
   public function obtenerTrabajosParaLafechayHora() {
       $this->fecha=cambiarPorguion($this->fecha);
       $oTpla=self::sentencia("SELECT * FROM tablaProgramacion WHERE fecha='$this->fecha' and hora ='$this->hora'");
       return !empty($oTpla)?$oTpla:false;
   }

   public function obtenerTrabajosParaLafechayHoraDisponibleTaller() {
       $this->fecha=cambiarPorguion($this->fecha);
       //$oTpla=self::sentencia("SELECT ordenIdTaller as idtecnico FROM usuarios WHERE idrol=8 AND estado=37 AND sede LIKE 'taller' AND NOT EXISTS(SELECT idtecnico FROM tablaProgramacion WHERE idtecnico=ordenIdTaller AND fecha='$this->fecha' AND hora='$this->hora' ) ORDER BY ordenIdTaller ASC LIMIT 1 ");
       $oTpla=self::sentencia("SELECT idturno FROM turnos t WHERE sede like 'taller' AND NOT EXISTS(SELECT idturno FROM tablaProgramacion tb WHERE tb.idturno=t.idturno AND fecha='$this->fecha' AND hora='$this->hora')");
       if (!empty($oTpla)) {
           $datoss=array_shift($oTpla);
           return $datoss->idturno;
       }else{
           return false;
       }
   }

   public function obtenerTrabajosParaLafechayHoraYespacio() {
       $this->fecha=cambiarPorguion($this->fecha);
       $oTpla=self::sentencia("SELECT * FROM tablaProgramacion WHERE fecha='$this->fecha' and hora ='$this->hora' and idturno='$this->idturno' ORDER BY idtablaProgramacion DESC LIMIT 1");
       return !empty($oTpla)?array_shift($oTpla):false;
   }
   public function obteneDatosDeprogramacion(){
       $sqlodp=self::sentencia("SELECT * FROM tablaProgramacion WHERE idtablaProgramacion='$this->idtablaProgramacion' LIMIT 1 " );
       return ($sqlodp)?array_shift($sqlodp):false;
   }


   public function asignarTecnicoaProgramacionCita(){
       global $db;
       $sqlatp="UPDATE tablaProgramacion SET idtecnico='$this->idtecnico' WHERE idcita ='$this->idcita' ";
       return ($db->query($sqlatp))?true:false;
   }

   public function cambiarEstadoTipoTurno($idturno,$fijo){
       global $db;
       $sqlatp="UPDATE turnos SET fijo='$fijo' WHERE idturno ='$idturno' ";
       return ($db->query($sqlatp))?true:false;
   }

   public function asignarTecnicoaProgramacionOperacion(){
       global $db;
       $sqlatp="UPDATE tablaProgramacion SET idtecnico='$this->idtecnico' WHERE idoperacion ='$this->idoperacion' ";
       return ($db->query($sqlatp))?true:false;
   }

   public function desasociarTecnicoCita(){
       global $db;
       $sqlatp="UPDATE tablaProgramacion SET idtecnico='0' WHERE idcita ='$this->idcita' ";
       return ($db->query($sqlatp))?true:false;
   }

   public function cambiarReserva(){
       global $db;
       $sqlatp="UPDATE tablaProgramacion SET rpara='$this->rpara' WHERE idtablaProgramacion='$this->idtablaProgramacion' ";
       return ($db->query($sqlatp))?true:false;
   }
   public function desasociarTecnicoOperacion(){
       global $db;
       $sqlatp="UPDATE tablaProgramacion SET idtecnico='0' WHERE idoperacion ='$this->idoperacion' ";
       return ($db->query($sqlatp))?true:false;
   }
   public function obtenerPorIdoperacion(){
       $obPIO=self::sentencia("SELECT * FROM tablaProgramacion WHERE idoperacion='$this->idoperacion' LIMIT 1");
       return ($obPIO)?array_shift($obPIO):false;
   }
   public function reservarTurno(){
       global $db;
       global $session;
       $tth_install="CASE WHEN ('$this->hora'<='13:00' AND '$this->horafin'>='14:00') THEN  SUBTIME(SUBTIME('$this->horafin','$this->hora'),'01:00') ELSE SUBTIME('$this->horafin','$this->hora') END";
       $hf="SUBTIME('$this->horafin','00:05')";

       $resT="INSERT INTO tablaProgramacion(fecha,hora,idoperacion,idcita,fechaRegistro,idusuario,idtecnico,idturno,observacion,rpara,tth_instalacion,horafin,iddistrito,ubicacion) VALUES ('";
       $resT.=$db->escapeString($this->fecha)."','";
       $resT.=$db->escapeString($this->hora)."',0,0,'";
       $resT.=$db->escapeString(date('YmdHis'))."','";
       $resT.=$db->escapeString($session->idusuario)."','";
       $resT.=$db->escapeString($this->idtecnico)."',0,'";
       $resT.=$db->escapeString($this->observacion)."','";
       $resT.=$db->escapeString($this->rpara)."',";
       $resT.=$tth_install.",";
       $resT.=$hf.",0,'')";
       return ($db->query($resT))?true:false;
   }

    public static function verificarsiestarangoTbProg2d($horaini,$horafin,$fecha,$idtec){
        $obtenerHtbp=self::sentencia("select count(*) tt from tablaProgramacion 
            where ((hora<'".$horaini."' and horafin>'".$horaini."') or (hora<'".$horafin."' and horafin>'".$horafin."') or (hora>='".$horaini."' and horafin<='".$horafin."')) 
            and fecha='".$fecha."' and idtecnico='".$idtec."' and (idoperacion!=0 or idcita!=0) ");
        return !empty($obtenerHtbp)?array_shift($obtenerHtbp):false;
    }

    public static function verificarsiestarangoTbProg($horaini,$horafin,$fecha,$idtec){
        $obtenerHtbp=self::sentencia("select count(*) tt from tablaProgramacion 
            where ((hora<'".$horaini."' and horafin>'".$horaini."') or (hora<'".$horafin."' and horafin>'".$horafin."') or (hora>='".$horaini."' and horafin<='".$horafin."')) 
            and fecha='".$fecha."' and idtecnico='".$idtec."'  " );
        return !empty($obtenerHtbp)?array_shift($obtenerHtbp):false;
    }


    public function obtenerHoraFin(){
        global $db;
        global $session;

        $bhf="ADDTIME('$this->hora',SUBTIME('".preg_replace('/\s+/', '', $this->tth_instalacion)."',(case when COALESCE('".preg_replace('/\s+/', '', $this->tth_instalacion)."','00:00')='00:00' then '00:01' ELSE '00:05' end)))";
        $bhi="SUBTIME('$this->hora',ADDTIME('$this->tth_trasalado',(case when COALESCE('$this->tth_trasalado','00:00')='00:00' then '00:01' ELSE '00:05' end)))";
        $hf="CASE WHEN ($bhf>='13:00' and $bhf<'14:00') THEN (ADDTIME($bhf,'01:00')) 
                ELSE
                CASE WHEN $bhi<'13:00' AND $bhf>='14:00' THEN 
                CASE WHEN '$this->hora'>='14:00' THEN ($bhf) ELSE (ADDTIME($bhf,'01:00')) END  
                ELSE $bhf
                END
            END ";

        $obtenerHtbp=self::sentencia("select (".$hf.") tt " );
        return !empty($obtenerHtbp)?array_shift($obtenerHtbp):false;

    }


    public static function verificarsiestarangoTbProg2($horaini,$horafin,$fecha,$idtec,$idreserv){
        $hf="SUBTIME('$horafin','00:05')";
     //  echo  $horaini.'|'.$horafin.'|'.$fecha.'|'.$idtec.'|'.$idreserv;
        $obtenerHtbp=self::sentencia("select count(*) tt from tablaProgramacion 
            where ((hora<'".$horaini."' and horafin>'".$horaini."') or (hora<".$hf." and horafin>".$hf.") or (hora>='".$horaini."' and horafin<=".$hf.")) 
            and fecha='".$fecha."' and idtecnico='".$idtec."'   and idtablaProgramacion!='$idreserv'" );
        return !empty($obtenerHtbp)?array_shift($obtenerHtbp):false;
    }

   public function citaConfirmada(){
       global $db;
       $sqlcc="UPDATE tablaProgramacion SET idoperacion='$this->idoperacion',estado='0' WHERE idcita ='$this->idcita'";
       return ($db->query($sqlcc))?true:false;
   }

   public function borrarEspacioCita(){
       global $db;
       $this->idcita=limpiar($this->idcita);
       $bEc="UPDATE tablaProgramacion SET estado='321' WHERE idcita='$this->idcita'";
       return ($db->query($bEc))?true:false;
   }
   public function borrarEstacioOperacion(){
       global $db;
       $boPoR="UPDATE tablaProgramacion SET estado='321' WHERE idoperacion='$this->idoperacion'";
       return ($db->query($boPoR))?true:false;
   }
   public function liberarEspacioope(){
       global $db;
       $sqllEo="DELETE FROM tablaProgramacion WHERE idoperacion='$this->idoperacion' and estado='321'";
       return ($db->query($sqllEo))?true:false;
   }

   public function liberarEspaciocita(){
       global $db;
       $sqllEo="DELETE FROM tablaProgramacion WHERE idcita='$this->idcita' and estado='321'";
       return ($db->query($sqllEo))?true:false;
   }




public function obteneTurnosDisponibles(){
       $qot=self::sentencia("SELECT idturno,sede,fijo FROM turnos WHERE estado='37' ORDER BY orden, idturno asc");
       return ($qot)?$qot:false;
   }

public function obteneTurnosDisponiblesTotal(){
       $qot=self::sentencia("SELECT idturno,sede,fijo FROM turnos WHERE estado='37' or estado='92' order by orden, idturno  asc");
       return ($qot)?$qot:false;
   }

   public function obteneTurnosDisponiblesTaller(){
       $qot=self::sentencia("SELECT idturno,sede FROM turnos WHERE  sede LIKE 'Taller%' and estado=37 ORDER BY orden, idturno asc");
       return ($qot)?$qot:false;
   }

   public function obteneTurnos(){
       $qot=self::sentencia("SELECT idturno,sede,estado,fijo FROM turnos ORDER BY  orden, idturno asc ");
       return ($qot)?$qot:false;
   }
   public function obtenerDetallesDelTurno(){
       $oddt=self::sentencia("SELECT idturno,sede, estado,fijo FROM turnos WHERE idturno='$this->idturno' LIMIT 1");
       return ($oddt)?array_shift($oddt):false;
   }
   public function actualizarturno(){
       global $db;
       $sqlat="UPDATE turnos SET sede='$this->sede', estado='$this->estado' WHERE idturno='$this->idturno' ";
       return ($db->query($sqlat))?true:false;
   }
   public function verSiesFijo(){
    $vSf=self::sentencia("SELECT fijo FROM turnos WHERE idturno ='$this->idturno'");
    $da=array_shift($vSf);
    if ($da->fijo=='si') {
        return true;
    }else{
        return false;
    }
   }


   public function obtenerCantidadDETecnicosDisponiblesEnElHorario(){
        $obtenerTecnicosQ=self::sentencia("SELECT usuarioNombre as fecha , usuarioApellidoPaterno as hora , idusuario as idtecnico FROM usuarios u WHERE NOT EXISTS(SELECT idtecnico FROM tablaProgramacion WHERE idtecnico=u.idusuario AND fecha='$this->fecha' AND hora='$this->hora') AND idrol=8 AND estado=37 ORDER BY idusuario ASC ");		
        return ($obtenerTecnicosQ)?$obtenerTecnicosQ:false;
   }

   public function eliminarReserva(){
       global $db;
       if ($this->idtablaProgramacion==0 ||  $this->idtablaProgramacion=='') {
           return false;
       }
       $eR="DELETE FROM tablaProgramacion WHERE idtablaProgramacion='$this->idtablaProgramacion'";
       return ($db->query($eR))?true:false;
   }


    public function updateReserva(){
        global $db;
        $tth_install="CASE WHEN ('$this->hora'<='13:00' AND '$this->horafin'>='14:00') THEN  SUBTIME(SUBTIME('$this->horafin','$this->hora'),'01:00') ELSE SUBTIME('$this->horafin','$this->hora') END";
        $hinn = " CASE WHEN ('$this->hora'='13:00') THEN '14:00' ELSE '$this->hora' END";
        $hf="SUBTIME('$this->horafin','00:05')";
        $sqlatp="UPDATE tablaProgramacion SET observacion='$this->observacion', rpara='$this->rpara',hora=".$hinn.",horafin=".$hf.",idtecnico='$this->idtecnico',fecha='$this->fecha',";
        $sqlatp.=" tth_instalacion = ".$tth_install." WHERE idtablaProgramacion='$this->idtablaProgramacion' ";
        return ($db->query($sqlatp))?true:false;
    }

   
}

<?php 
	$path = $_SERVER['DOCUMENT_ROOT'];
	require_once("funciones.php");
	require_once("configuracion.php");
	require_once("Recursos.php");
	require_once("baseDatos.php");
	require_once("Usuario.php");
	require_once("Session.php");
	require_once("baseDatosOnix.php");
	require_once("baseDatosMSQLSrv.php");
	require_once("Onix.php");
    require_once("baseDatosWebTrack.php");
    require_once("WebTrack.php");
	require_once("Rol.php");
	require_once("Token.php");
	require_once("Estadistica.php");
	require_once("Faq.php");
	require_once("Calendario.php");
	require_once("Notificaciones.php");
	// require_once("Desinstalacion.php");
	require_once($path."/rpr/class/RepartePendientesWelcomeClass.php");
require_once("tablaProgramacion.php");
require_once("InputFormClass.php");
require_once($path.'/cem/class/SeguimientoRobo.php');
	require_once($path."/cem/class/Robo.php");
	require_once($path."/cem/class/BateriaTrack.php");
	require_once($path."/cem/class/Chat.php");
	require_once($path."/cem/class/Chequeos.php");
	require_once($path."/cem/class/noAtender.php");
	require_once($path."/cem/class/Maps.php");
	require_once($path."/cem/class/Choque.php");
	require_once($path."/cem/class/SeguimientoChoque.php");
	require_once($path."/cem/class/DatosInstalacion.php");
	require_once($path."/com/class/Vehiculo.php");
	require_once($path."/com/class/Cotizar.php");
	require_once($path."/com/class/Cliente.php");
	require_once($path."/com/class/Canales.php");
require_once($path."/com/class/Intermediarios.php");
require_once($path."/com/class/TramaIntegracion.php");
require_once($path."/com/class/TramaIntegracionPF.php");
require_once($path."/com/class/TramaIntegracionQL.php");
require_once($path."/com/class/TramaIntegracionTY.php");
require_once($path."/com/class/Comisiones.php");
require_once($path."/com/class/ReportesOnix.php");
require_once($path."/com/class/ReclamoClienteClass.php");
require_once($path."/com/class/ReclamoClienteReporteClass.php");
require_once($path."/com/class/DatosTablasClass.php");
	require_once($path."/com/class/CrearAcceso.php");
	require_once($path."/com/class/RelacionComercial.php");
	require_once($path."/com/class/Cita.php");
	require_once($path."/com/class/Motivos.php");
	require_once($path."/ope/class/Operacion.php");
	require_once($path."/ope/class/Tecnicos.php");
require_once($path."/cem/class/anomalia.php");
require_once($path."/cem/class/SeguimientoPK.php");
require_once($path."/cem/class/PostInstalacion.php");
require_once($path."/cem/class/acciones.php");
require_once($path."/ope/class/homologation_client.php");
require_once($path."/ope/class/homologation_documents.php");
require_once($path."/admin/class/Api.php");
require_once($path."/admin/class/APIToyota.php");
require_once($path."/ope/class/homologation_detail.php");
require_once($path."/conta/class/SolicitudCredito.php");
require_once($path."/conta/class/Contabilidad.php");
	require_once($path."/conta/class/Credito.php");
	require_once($path."/conta/class/SeguimientoFacturacion.php");
	require_once($path."/com/class/SeguimientoTablas.php");
	require_once($path."/com/class/SeguimientoConsulta.php");
	require_once($path."/com/class/CantidadRegistroOP.php");
	require_once($path."/com/class/ServiciosAdicionales.php");
	require_once($path."/com/class/OperacionServAdicionales.php");
	require_once($path."/conta/class/DatosFacturacion.php");
    require_once($path."/com/class/TiempoProgramServAdic.php");
    require_once($path."/com/class/AuditoriaComisiones.php");
	require_once($path."/rpr/class/Renovaciones.php");
	require_once($path."/admin/class/Log.php");
	require_once($path."/admin/class/AuditoriaSistemas.php");
	require_once($path."/cem/class/DataAnomalia.php");
	/////////////COMENTADO SOLO PARA SERVIDOR DE DESARROLLO //
	///OJO solucion $dpb; SeneteciaPublic()
	//require_once($path."/public/intDb.php");
	/////////////COMENTADO SOLO PARA SERVIDOR DE DESARROLLO //
	date_default_timezone_set('America/Lima');
if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}
<?php 
if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}
 ?>
<ul class="nav metismenu" id="side-menu">
    <li class="nav-header">
        <?php $o=Usuario::obtenerUsuario($session->idusuario); ?>
        <div class="dropdown profile-element"> <span>
                <?php 
        if (is_file('../public/perfil/'.$o->fotoPerfil.'')) {
          $imagenDir="../public/perfil/".$o->fotoPerfil."";      

        }elseif (is_file('public/perfil/'.$o->fotoPerfil.'')) {
          $imagenDir="public/perfil/".$o->fotoPerfil."";

        }elseif (is_file('../../public/perfil/'.$o->fotoPerfil.'')) {
          $imagenDir="../../public/perfil/".$o->fotoPerfil."";

        }else{
          if (is_file('../public/perfil/pefilpordefecto.png')) {
                $imagenDir="../public/perfil/pefilpordefecto.png";
          }elseif (is_file('public/perfil/pefilpordefecto.png')) {
                $imagenDir="public/perfil/pefilpordefecto.png";            
          }else{
                $imagenDir="../../public/perfil/pefilpordefecto.png";
          }
        }
     ?>
                <img alt="<?= $imagenDir ?>" class="img-circle" src="<?= $imagenDir ?>" width="60" />
            </span>
            <a data-toggle="dropdown" class="active" class="dropdown-toggle" href="#">
                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">
                            <?php  echo $o->usuarioNombre." ".$o->usuarioApellidoPaterno;  ?>&nbsp; <i
                                class="fa fa-toggle-down"></i>
                        </strong>
                    </span><small><?php echo $o->sobreMi ?> </small></span></a>
            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                <li><a href="../perfil.php">Ver Perfil</a></li>
                <li><a href="../editarPerfil">Editar Perfil</a></li>
                <!-- <li><a href="../mensajesPrivados">obtenerMenuMensajes privados</a></li> -->
                <li class="divider"></li>
                <li><a href="../salir">Salir</a></li>
            </ul>
        </div>
        <div class="logo-element">
            R+
        </div>
    </li>
    <!--           INICIO DE MENU -->
    <?php if ($obMenu->administrar=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="administrar") ? "active" : "" ; ?>">
        <a href="../admin/administrar"><i class="fa fa-slideshare"></i><span class="nav-label">Administrar </span></a>
    </li>
    <?php endif ?>
    <?php if ($obMenu->panel=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="index") ? "active" : "" ; ?>">
        <a href="../index"><i class="fa fa-dashboard"></i><span class="nav-label">Panel</span></a>
    </li>
    <?php endif ?>
    <?php if($obMenu->helpdesk == "si"):?>
    <li class="<?php echo $retVal = ($nav=="Helpdesk | Auditoria de Sistemas") ? "active" : "" ; ?>">
        <a href="../admin/helpdesk"><i class="fa fa-user"></i><span class="nav-label">Helpdesk</span></a>
    </li>
    <?php endif ?>
    <?php if($obMenu->subirAnomalias == "si"):?>
    <li class="<?php echo $retVal = ($nav=="CEM | Subir Anomalias") ? "active" : "" ; ?>">
        <a href="../cem/subirAnomalias"><i class="fa fa-upload"></i><span class="nav-label">Subir anomalias</span></a>
    </li>
    <?php endif ?>
    <li class="<?php echo $retVal = ($nav=="SeguimientoToyota") ? "active" : "" ; ?>">
        <a href="#"><i class="fa fa-exclamation-circle"></i> <span class="nav-label">Toyota</span> <span
                class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <?php if ($obMenu->instalaciones=="si" ){ ?>
            <li class="<?php echo $retVal = ($nav=="Activacion") ? "active" : "" ; ?>">
                <a href="../cem/Activacion"><i class="fa fa-car"></i><span class="nav-label">Activaciones</span></a>
            </li>
            <li class="<?php echo $retVal = ($nav=="SeguimientoToyotaCEM") ? "active" : "" ; ?>">
                <a href="../cem/SeguimientoToyotaCEM"><i class="fa fa-location-arrow"></i><span
                        class="nav-label">Seguimiento Toyota CEM</span></a>
            </li>
            <?php } ?>

            <?php if ($obMenu->registrarOperaciones=="si"){ ?>
            <li class="<?php echo $retVal = ($nav=="Vinculacion") ? "active" : "" ; ?>">
                <a href="../com/Vinculacion"><i class="fa fa-slideshare"></i><span
                        class="nav-label">Vinculaciones</span></a>
            </li>
            <li class="<?php echo $retVal = ($nav=="SeguimientoToyotaADV") ? "active" : "" ; ?>">
                <a href="../com/SeguimientoToyotaADV"><i class="fa fa-location-arrow"></i><span
                        class="nav-label">Seguimiento Toyota ADV</span></a>
            </li>
            <?php } ?>

        </ul>
    </li>
    <?php if ($obMenu->baterias=="si" && $o->idrol!=785): ?>
    <li class="<?php echo $retVal = ($nav=="bateriaTrack"||$nav=="bateriaOcto") ? "active" : "" ; ?>">
        <a href="#"><i class="fa fa-exclamation-circle"></i> <span class="nav-label">Baterías</span> <span
                class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li class="<?php echo $retVal = ($nav=="bateriaTrack") ? "active" : "" ; ?>"><a
                    href="http://resh2.tracklink.pe/baterias">Track</a></li>
            <li class="<?php echo $retVal = ($nav=="bateriaOcto") ? "active" : "" ; ?>"><a
                    href="http://resh2.tracklink.pe/bateriaOcto">Octo</a></li>
        </ul>
    </li>
    <?php endif ?>

    <?php if ($obMenu->reclamosClientes=="si"): ?>
        <li class="<?php echo $retVal = ($nav=="reclamosR") ? "active" : "" ; ?>">
            <a  href="../com/reclamosR"><i class="fa fa-book"></i></i><span class="nav-label">Reclamos clientes</span></a>
        </li>
    <?php endif ?>

    <?php if ($obMenu->enviarcredenciales=="si"): ?>
        <li class="<?php echo $retVal = ($nav=="enviarcredenciales") ? "active" : "" ; ?>">
            <a  href="../com/enviarcredenciales"><i class="fa fa-location-arrow"></i></i><span class="nav-label">Enviar credenciales</span></a>
        </li>
    <?php endif ?>

    <?php if ($obMenu->instalaciones=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="instalacion") ? "active" : "" ; ?>">
        <a href="../cem/instalacion"><i class="fa fa-check-square-o"></i><span class="nav-label">Registros
                <?php $cantidadPendientesIns=DatosInstalacion::verInstaladoNoprogramado()  ?></span><?php if ($cantidadPendientesIns) { ?><span
                class="label label-danger pull-right"><?=  
    $cantidadPendientesIns;
?></span><?php } ?></a>
    </li>

    <li class="<?php echo $retVal = ($nav=="gestionAnomalia") ? "active" : "" ; ?>">
        <a href="../cem/gestionAnomalias"><i class="fa fa-warning"></i><span class="nav-label">Gestión
                Anomalía</span></a>
    </li>

    <?php endif ?>
    <?php if ($obMenu->instalaciones=="si" or $obMenu->administrarComercial=="si"):  ?>

    <li class="<?php echo $retVal = ($nav=="seguimientoPK") ? "active" : "" ; ?>">
        <a href="../cem/seguimientoPK"><i class="fa fa-gear"></i><span class="nav-label">Seguimiento PK</span></a>
    </li>

    <?php if ($obMenu->seguimientoToyotaAnomalia=="si"): ?>
        <li class="<?php echo $retVal = ($nav=="seguimientoToyotaAnomalia") ? "active" : "" ; ?>">
            <a  href="../cem/seguimientoToyotaAnomalia"><i class="fa fa-gear"></i></i><span class="nav-label">Seguimiento Toyota</span></a>
        </li>
    <?php endif ?>

    <?php endif ?>
    <?php if ($obMenu->instalaciones=="si" || $session->idusuario=='3' || $session->idusuario=='80' || $session->idusuario=='148' || $session->idusuario=='111'): ?>
    <li class="<?php echo $retVal = ($nav=="procesosPostInstalacion") ? "active" : "" ; ?>">
        <a href="../cem/procesosPostInstalacion"><i class="fa fa-clipboard"></i><span class="nav-label">Procesos Post
                Instalacion</span></a>
    </li>
    <?php endif ?>
    <?php if ($obMenu->chequeos=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="chequeos") ? "active" : "" ; ?>">
        <a href="../cem/chequeos"><i class="fa fa-laptop"></i><span class="nav-label">Chequeos</span></a>
    </li>

    <li class="<?php echo $retVal = ($nav=="gestionAccesos") ? "active" : "" ; ?>">
        <a href="../cem/gestionAccesos.php"><i class="fa fa-adjust"></i><span class="nav-label">Gestión
                Accesos</span></a>
    </li>
    <?php endif ?>
    <?php if ($obMenu->choques=="si"): ?>
    <li
        class="<?php echo $retVal = ($nav=="choques"|| $nav=="seguimientoChoque"|| $nav=="detalleChoque") ? "active" : "" ; ?>">
        <a href="../cem/choques"><i class="fa fa-automobile"></i><span class="nav-label">Choques
                <?php echo $retVal = ($nav=="seguimientoChoque") ? " Seguimiento" : "" ; ?>
                <?php echo $retVal2 = ($nav=="detalleChoque") ? " Detalle" : "" ; ?>
                <?php $objComision=Choque::otenerChoquesEnSeguimientoCantiad(); if($objComision->cantidad<>0) { ?><span
                    class="label label-warning pull-right"><?php  
    echo $objComision->cantidad;
    ?></span><?php } ?></span></a>
    </li>
    <li
        class="<?php echo $retVal = ($nav=="asistencia"|| $nav=="seguimientoAsistencia"|| $nav=="detalleAsistencia") ? "active" : "" ; ?>">
        <a href="../cem/asistencia"><i class="fa fa-tasks"></i><span class="nav-label">Asistencias
                <?php echo $retVal = ($nav=="seguimientoAsistencia") ? " seguimiento" : "" ; ?>
                <?php echo $retVal2 = ($nav=="detalleAsistencia") ? " Detalle" : "" ; ?>
                <?php $objComision=Choque::otenerAsistenciaEnSeguimientoCantiad(); if($objComision->cantidad<>0) { ?><span
                    class="label label-warning pull-right"><?php  
    echo $objComision->cantidad;
    ?></span><?php } ?></span></a>
    </li>
        <?php if($o->idrol!=785) : ?>
            <li
                class="<?php echo $retVal = ($nav=="incautacion"|| $nav=="seguimientoIncautacion"|| $nav=="detalleIncautacion") ? "active" : "" ; ?>">
                <a href="../cem/incautacion"><i class="fa  fa-info-circle"></i><span
                        class="nav-label">Incautaciones<?php echo $retVal = ($nav=="seguimientoIncautacion") ? " seguimiento" : "" ; ?>
                        <?php echo $retVal2 = ($nav=="detalleIncautacion") ? " Detalle" : "" ; ?>
                        <?php $objComision=Choque::otenerIncautacionEnSeguimientoCantiad(); if($objComision->cantidad<>0) { ?><span
                            class="label label-warning pull-right"><?php  
            echo $objComision->cantidad;
            ?></span><?php } ?></span></a>
            </li>
        <?php endif ?>
    <?php endif ?>
    <?php if ($obMenu->robos=="si"): ?>
    <li
        class="<?php echo $retVal = ($nav=="robo"||$nav=="seguimientoRobo"||$nav=="detalleCliente") ? "active" : "" ; ?>">
        <a href="../cem/robo"><i class="fa fa-warning "></i><span
                class="nav-label">Robos<?php echo $retVal = ($nav=="seguimientoRobo") ? "-> Segimiento robo" : "" ; ?><?php echo $retVal = ($nav=="detalleCliente") ? "-> Detalle robo" : "" ; ?></span></a>
    </li>
    <?php endif ?>
    <?php if ($obMenu->maps=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="maps") ? "active" : "" ; ?>">
        <a href="../cem/maps"><i class="fa fa-map-marker"></i><span class="nav-label">Maps</span></a>
    </li>
    <?php endif ?>

    <?php if ($obMenu->verPendietes=="si"){ ?>
    <!--
 <li class="<?php echo $retVal = ($nav=="verPendietes") ? "active" : "" ; ?>">
  <a  href="../ope/verPendietes"><i class="fa fa-exclamation "></i><i class="fa fa-eye"></i><span class="nav-label">Pendientes comercial</span></a>
</li>
 <li class="<?php echo $retVal = ($nav=="adminOperacion") ? "active" : "" ; ?>">
  <a  href="../ope/adminOperacion"><i class="fa fa-calendar-o "></i><i class="fa fa-eye"></i><span class="nav-label">Admin Operaciones</span></a>
</li>
-->
    <?php } ?>
    <?php if ($obMenu->registrarOperaciones=="si"){ ?>

    <li class="<?php echo $retVal = ($nav=="verPendietes") ? "active" : "" ; ?>">
        <a href="../ope/verPendietes"><i class="fa fa-exclamation "></i><i class="fa fa-eye"></i><span
                class="nav-label">Pendientes comercial</span></a>
    </li>
    <li class="<?php echo $retVal = ($nav=="adminOperacion") ? "active" : "" ; ?>">
        <a href="../ope/adminOperacion"><i class="fa fa-calendar-o "></i><i class="fa fa-eye"></i><span
                class="nav-label">Admin Operaciones</span></a>
    </li>

    <li class="<?php echo $retVal = ($nav=="asignarOrdenes") ? "active" : "" ; ?>">
        <a href="../ope/asignarOrdenes"><i class="fa fa-keyboard-o"></i><span class="nav-label">Asignar órdenes
                <?php $opeObj=Operacion::pendientesDeAsignarOrden(); if($opeObj->cantidad<>0) { ?><span
                    class="label label-warning pull-right"><?php  
    echo $opeObj->cantidad;
    ?></span><?php } ?> </span></a>
    </li>
    <!-- obtenerListosParaInstalacionMenuLateral -->
    <li class="<?php echo $retVal = ($nav=="registrarOperaciones") ? "active" : "" ; ?>">
        <a href="../ope/registrarOperaciones"><i class="fa fa-gear"></i><span class="nav-label">Gestionar Operaciones
                <?php $opeObjmenLat=Operacion::obtenerListosParaInstalacionMenuLateral(); if($opeObjmenLat->cantidad<>0) { ?><span
                    class="label label-warning pull-right"><?php  
    echo $opeObjmenLat->cantidad;
    ?></span><?php } ?></span></a>
    </li>
    <li class="<?php echo $retVal = ($nav=="citasTallerSac") ? "active" : "" ; ?>">
        <a href="../ope/citasTallerSac"><i class="fa fa-sliders"></i><span class="nav-label">Citas Taller </span></a>
    </li>
    <li class="<?php echo $retVal = ($nav=="crearClientes") ? "active" : "" ; ?>">
        <a href="../ope/crearClientes"><i class="fa fa-plus-circle"></i><span class="nav-label">Crear de Onyx</span></a>
    </li>
    <li class="<?php echo $retVal = ($nav=="planKilometros") ? "active" : "" ; ?>">
        <a href="../com/planKilometros"><i class="fa fa-cloud"></i><span class="nav-label">Pacifico Plan
                Kilometros</span></a>
    </li>
    <?php } ?>
    <?php //if ($obMenu->cotizar=="si" and 2==1): ?>
    <!--<li class="<?php echo $retVal = ($nav=="crearAcceso") ? "active" : "" ; ?>">
    <a  href="../com/crearAcceso"><i class="fa fa-user"></i><span class="nav-label">Crear acceso</span><?php $cotCantidadObj=Cotizar::obtenerCotizacionCantidad(); if($cotCantidadObj->cantidad<>0){ ?><span class="label label-warning pull-right"><?php  
    echo $cotCantidadObj->cantidad;
    ?></span><?php } ?></a>
  </li>-->
    <?php //endif ?>
    <?php if ($obMenu->cotizar=="si"): ?>

    <li class="<?php echo $retVal = ($nav=="crearCita") ? "active" : "" ; ?>">
        <a href="../com/crearCita"><i class="fa fa-car"></i><span class="nav-label">Cita
                taller</span><?php $citaObj=Cita::reprogramarMenuIzquierdoCita(); if($citaObj->cantidad<>0){ ?><span
                class="label label-warning pull-right"><?php  
    echo $citaObj->cantidad;
    ?></span><?php } ?></a>
    </li>

    <!--
<li class="<?php echo $retVal = ($nav=="solicitudCredito") ? "active" : "" ; ?>">
  <a  href="../conta/solicitudCredito"><i class="fa fa-money"></i><i class="fa fa-usd"></i><span class="nav-label">Creditos</span></a>
</li>
-->

    <?php endif ?>

    <?php if ($obMenu->registrarCliente=="si"): ?>
		<li class="<?php echo $retVal = ($nav=="registrarCliente") ? "active" : "" ; ?>">
			<a href="../com/registrarCliente"><i class="fa fa-pencil-square-o"></i><span class="nav-label">Registrar
					clientes</span></a>
		</li>
		<li class="<?php echo $retVal = ($nav=="comisiones") ? "active" : "" ; ?>">
			<a href="../com/comisiones"><i class="fa fa-dollar"></i><span
					class="nav-label">Comisiones</span><?php $objComision=Comisiones::obtenerComisionesPendientes(); if($objComision->cantidad<>0) { ?><span
					class="label label-warning pull-right"><?php  
		echo $objComision->cantidad;
		?></span><?php } ?></a>
		</li>
    <?php endif ?>

    <?php if (Operacion::verificarSiRolUsuario(4)||$session->idusuario=='2'||$session->idusuario =='278'): ?>
		<li class="<?php echo $retVal = ($nav=="GestionDocumentos") ? "active" : "" ; ?>">
			<a href="../com/gestionDocumentos"><i class="fa fa-slideshare"></i><span class="nav-label">Gestion Documentos</span></a>
		</li>
    <?php endif ?>

	<?php if($obMenu->clienteMTC == "si"):?>
		<li class="<?php echo $retVal = ($nav=="clienteMTC") ? "active" : "" ; ?>">
			<a href="../cem/clienteMTC.php"><i class="fa fa-eye"></i><span class="nav-label">Cliente MTC</span></a>
		</li>
	<?php endif ?>

    <?php if ($obMenu->idusuario =="69"):?>
		<li class="<?php echo $retVal = ($nav=="AdminPostVenta") ? "active" : "" ; ?>">
			<a href="../rpr/adminPostVenta.php"><i class="fa fa-external-link-square"></i><span class="nav-label">Admin post venta</span></a>
		</li>
    <?php endif ?>
    <?php if ($obMenu->gestionPostVenta=="si" || $o->idrol!=785): ?>
        <li class="<?php echo $retVal = ($nav=="gestionBienvenidaPendientes") ? "active" : "" ; ?>">
            <a  href="../rpr/bienvenidaPendientes.php">
            <i class="fa fa-slideshare"></i>
            <span class="nav-label">Gest. Pend. Bienv.</span>
            <span class="label label-warning pull-right">
            <?php
            $repartePendientesWelcome = new RepartePendientesWelcomeClass();
            $repartePendientesWelcome->fk_usuario_gestiona=$session->idusuario;
            $cantPendientesAhora = $repartePendientesWelcome->cantPendientesAhora();
            echo $cantPendientesAhora[0]->cantidad;
            ?>
            </span>
            </a>
        </li>
        <li class="<?php echo $retVal = ($nav=="seguimientoBienvenida") ? "active" : "" ; ?>">
            <a href="../rpr/seguimientoWelcomeMail.php"><i class="fa fa-slideshare"></i><span class="nav-label">Seguimiento
                    Bienvenida</span></a>
        </li>
        <li class="<?php echo $retVal = ($nav=="AdminPostVenta") ? "active" : "" ; ?>">
            <a href="../rpr/adminPostVenta.php"><i class="fa fa-external-link-square"></i><span class="nav-label">Admin post
                    venta</span></a>
        </li>
        <li class="<?php echo $retVal = ($nav=="postVenta") ? "active" : "" ; ?>">
            <a href="../rpr/gestionPostVenta"><i class="fa fa-external-link-square"></i><span class="nav-label">Gestion post
                    venta</span></a>
        </li>

        <li class="<?php echo $retVal = ($nav=="verPendietes Temporales") ? "active" : "" ; ?>">
            <a href="../rpr/gestionPendientesTemporales"><i class="fa fa-pencil-square-o"></i><span
                    class="nav-label">Pendientes</span></a>
        </li>
    <?php endif ?>

    <?php if ($obMenu->gestionRenovaciones=="si"): ?>
    <li
        class="<?php echo $retVal = ($nav=="renovaciones") ? "active" : "" ; ?><?php echo $retVal = ($nav=="renovacionesE") ? "active" : "" ; ?>">
        <a href="../rpr/gestionRenovaciones"><i class="fa fa-history"></i><span class="nav-label">
                <?php echo $retVal = ($nav=="renovacionesE") ? "Reporte renovaciones" : "Gestion renovaciones" ; ?></span></a>
    </li>
    <?php endif ?>

    <?php if ($obMenu->verificarOperaciones=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="verificarOperaciones") ? "active" : "" ; ?>">
        <a href="../ope/verificarOperaciones"><i class="fa fa-line-chart"></i><span class="nav-label">Operaciones
            </span></a>
    </li>
    <?php endif ?>

<!--860t0hqxp-EZ-->
    <?php if ($obMenu->indicadoresTrabajo=="si"): ?>
        <li class="<?php echo $retVal = ($nav=="indicadoresTrabajo") ? "active" : "" ; ?>">
            <a  href="../ope/indicadoresTrabajo"><i class="fa fa-location-arrow"></i></i><span class="nav-label">Indicadores de trabajo</span></a>
        </li>
    <?php endif ?>


    <?php if ($obMenu->gestionTecnica=="si"): ?>
    <!-- asginarTecnicoMenuLateral -->
    <li class="<?php echo $retVal = ($nav=="gestionTecnica") ? "active" : "" ; ?>">
        <a href="../ope/gestionTecnica"><i class="fa fa-eye"></i><span class="nav-label">Gestion tecnica
            </span><?php $opeObjatmenLat=Operacion::asginarTecnicoMenuLateral(); if($opeObjatmenLat->cantidad<>0) { ?><span
                class="label label-warning pull-right"><?php  
    echo $opeObjatmenLat->cantidad;
    ?></span><?php } ?></a>
    </li>
    <?php endif ?>

    <?php if ($obMenu->editarTecnicos=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="editarTecnicos") ? "active" : "" ; ?>">
        <a href="../ope/editarTecnicos"><i class="fa fa-edit"></i><span class="nav-label">Editar Tecnicos </span></a>
    </li>
    <li class="<?php echo $retVal = ($nav=="Disponibilidad Técnica") ? "active" : "" ; ?>">
        <a href="../ope/diponibilidadTecnica"><i class="fa fa-calendar"></i><span class="nav-label">Disponibilidad
                Técnica</span></a>
    </li>
    <?php endif ?>

    <?php if ($obMenu->verificarComercial=="si" && $obMenu->idusuario=="148"): ?>
    <li class="<?php echo $retVal = ($nav=="gestionHomologacion") ? "active" : "" ; ?>">
        <a href="../ope/gestionHomologacion"><i class="fa fa-book"></i><span class="nav-label">Gestionar
                Homologación</span></a>
    </li>
    <?php endif ?>

    <?php if ($obMenu->verificarComercial=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="verificarComercial") ? "active" : "" ; ?>">
        <a href="../com/verificarComercial"><i class="fa fa-area-chart"></i><span class="nav-label">Ventas por
                Ejecutivo</span></a>
    </li>
    <?php endif ?>
    <?php if ($obMenu->verificarComercialP=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="verificarComercialP") ? "active" : "" ; ?>">
        <a href="../com/verificarComercialP"><i class="fa fa-line-chart"></i><span class="nav-label">Ventas por
                Productos</span></a>
    </li>
    <?php endif ?>
    <?php if ($obMenu->comercialPersonalizado=="si"): ?>
    <!-- <li class="<?php echo $retVal = ($nav=="comercialPersonalizado") ? "active" : "" ; ?>">
    <a  href="../com/comercialPersonalizado"><i class="fa fa-bar-chart-o"></i><span class="nav-label">Reporte personalizado</span></a>
  </li>-->
    <?php endif ?>
    <?php if ($obMenu->comisiones=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="comisionesR") ? "active" : "" ; ?>">
        <a href="../com/comisionesR"><i class="fa fa-book"></i></i><span class="nav-label">Reporte comisiones</span></a>
    </li>
    <?php endif ?>

    <?php if ($obMenu->reporteContratosOcto=="si"): ?>
        <li class="<?php echo $retVal = ($nav=="contratosoctoR") ? "active" : "" ; ?>">
            <a  href="../com/contratosoctoR"><i class="fa fa-book"></i></i><span class="nav-label">Reporte contratos octo</span></a>
        </li>
    <?php endif ?>


    <?php if ($obMenu->reciboHonorario=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="Recibo Honorarios") ? "active" : "" ; ?>">
        <a href="../com/reciboHonorario"><i class="fa fa-files-o"></i></i><span class="nav-label">Recibo por
                honorarios</span></a>
    </li>
    <?php endif ?>
    <?php if ($obMenu->pagoComision=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="pagoComision") ? "active" : "" ; ?>">
        <a href="../conta/pagoComisiones"><i class="fa fa-check-square"></i><span class="nav-label">Pago
                Comisiones</span></a>
    </li>
    <?php endif ?>
    <?php if ($obMenu->gestionContabilidad=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="contabilidad") ? "active" : "" ; ?>">
        <a href="../conta/gestionContabilidad"><i class="fa fa-credit-card"></i><span class="nav-label">Gestion
                facturación </span></a>
    </li>
    <?php endif ?>
    <?php if ($obMenu->historialContable=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="historialContable") ? "active" : "" ; ?>">
        <a href="../conta/historialContable"><i class="fa fa-sort-amount-desc"></i><span class="nav-label">Historial
                facturación </span></a>
    </li>
    <?php endif ?>

    <?php if ($obMenu->subirArchivos=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="subirArchivos") ? "active" : "" ; ?>">
        <a href="../admin/subirArchivos"><i class="fa fa-cloud-upload"></i><span class="nav-label">Subir
                archivos</span></a>
    </li>
    <?php endif ?>
    <?php if ($obMenu->chequeoMonitoreo=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="chequeoMonitoreo") ? "active" : "" ; ?>">
        <a href="../chequeoMonitoreo"><i class="fa fa-cogs"></i><span class="nav-label">Solicitud chequeo</span></a>
    </li>
    <?php endif ?>
    <?php if ($obMenu->reprogramacion=="si"): ?>
    <!--
  <li class="<?php echo $retVal = ($nav=="reprogramacion") ? "active" : "" ; ?>">
    <a href="../reprogramacion"><i class="fa fa-random"></i><span class="nav-label">Reprogramacion</span></a>
  </li>
  -->
    <?php endif ?>
    <?php if ($obMenu->desinstalacion=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="desinstalacion") ? "active" : "" ; ?>">
        <a href="../rpr/desinstalacion"><i class="fa fa-chain-broken"></i><span
                class="nav-label">Desinstalación</span></a>
    </li>
    <?php endif ?>
    <?php if ($obMenu->administrarComercial=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="administrarComercial") ? "active" : "" ; ?>">
        <a href="../com/administrarComercial"><i class="fa fa-newspaper-o"></i><span class="nav-label">Administrar
                Comercial</span></a>
    </li>
    <li class="<?= ($nav=="Seguimiento Comercial") ? "active" : "" ; ?>">
        <a href="../com/seguimientoComercial"><i class="fa fa-cloud"></i><span class="nav-label">Seguimiento
                Comercial</span></a>
    </li>
    <li class="<?= ($nav=="Seguimiento Renovaciones") ? "active" : "" ; ?>">
        <a href="../rpr/SeguimientoRenovaciones"><i class="fa fa-cloud"></i><span class="nav-label">Seguimiento
                Renovaciones</span></a>
    </li>
    <?php endif ?>
    <?php if ($obMenu->confirmarRegistroEnOnix=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="confirmarRegistroEnOnix") ? "active" : "" ; ?>">
        <a href="../com/confirmarRegistroEnOnix"><i class="fa fa-pencil"></i><span class="nav-label">Confirmar
                registros</span><?php $objvhc=Vehiculo::obtenerVehiculosParaRegistrarEnOnixCantidad(); if($objvhc->cantidad<>0) { ?><span
                class="label label-warning pull-right"><?php  
    echo $objvhc->cantidad;
    ?></span><?php } ?>
            <?php 
$citdaData=Cita::obtenerCitasParaConfirmarRegistroMenuLateral();
if($citdaData->cantidad<>0) { ?><span class="label label-primary pull-right"><?php  
    echo $citdaData->cantidad;
    ?></span><?php } ?>
        </a>
    </li>
    <?php if ($obMenu->programarInstalacion=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="programarInstalacion") ? "active" : "" ; ?>">
        <a href="../com/programarInstalacion"><i class="fa fa-calendar-o"></i><span class="nav-label">Pendientes de
                instalación</span><?php $obOpe=Operacion::obtenerInstaPendMenLat(); if($obOpe->cantidad<>0) { ?><span
                class="label label-warning pull-right"><?php  
    echo $obOpe->cantidad;
    ?></span><?php } ?></a>
    </li>
    <?php endif ?>
    <?php endif ?>
    <?php if ($obMenu->operacionesComerciales=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="operacionesComerciales") ? "active" : "" ; ?>">
        <a href="../com/operacionesComerciales"><i class="fa fa-puzzle-piece"></i><span class="nav-label">Operaciones
                comerciales</span></a>
    </li>

    <li class="<?php echo $retVal = ($nav=="crearCita") ? "active" : "" ; ?>">
        <a href="../com/crearCita"><i class="fa fa-car"></i><span class="nav-label">Cita
                taller</span><?php $citaObj=Cita::reprogramarMenuIzquierdoCita(); if($citaObj->cantidad<>0){ ?><span
                class="label label-warning pull-right"><?php  
    echo $citaObj->cantidad;
    ?></span><?php } ?></a>
    </li>

    <li class="<?php echo $retVal = ($nav=="planKilometros") ? "active" : "" ; ?>">
        <a href="../com/planKilometros"><i class="fa fa-cloud"></i><span class="nav-label">Pacifico Plan
                Kilometros</span></a>
    </li>


    <li class="<?php echo $retVal = ($nav=="planFull") ? "active" : "" ; ?>">
        <a href="../com/planFull"><i class="fa fa-bookmark"></i><span class="nav-label">Pacifico Plan Full</span></a>
    </li>


    <li class="<?php echo $retVal = ($nav=="gestionQualitas") ? "active" : "" ; ?>">
        <a href="../com/gestionQualitas"><i class="fa fa-location-arrow"></i><span class="nav-label">Qualitas</span></a>
    </li>

    <?php endif ?>

    <?php if ($obMenu->programacion=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="programacion") ? "active" : "" ; ?>">
        <a href="../programacionDisponibilidad"><i class="fa fa-calendar"></i><span class="nav-label">Ver
                disponibilidad</span></a>
        <!-- target="_blank"  -->
    </li>
    <?php endif ?>

    <?php if ($obMenu->programacion=="sioio"): ?>
    <li class="<?php echo $retVal = ($nav=="SolicitudCredito") ? "active" : "" ; ?>">
        <a href="../conta/soliciCredito"><i class="fa fa-send"></i><span class="nav-label">Solicitud Crédito</span></a>
        <!-- target="_blank"  -->
    </li>
    <?php endif ?>
    <?php if ($obMenu->gestionContabilidad=="siioi"): ?>
    <li class="<?php echo $retVal = ($nav=="AprobacionCredito") ? "active" : "" ; ?>">
        <a href="../conta/aprobacionCredito"><i class="fa fa-check"></i><span class="nav-label">Aprobacion
                Crédito</span></a>
    </li>
    <?php endif ?>
    <?php if ($obMenu->cobranzas=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="cobranzas") ? "active" : "" ; ?>">
        <a href="../conta/cobranzas"><i class="fa fa-archive"></i><span class="nav-label">Cobranzas</span></a>
        <!-- target="_blank"  -->
    </li>
    <?php endif ?>

    <?php if ($obMenu->vouchers=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="vouchers") ? "active" : "" ; ?>">
        <a href="../conta/vouchers"><i class="fa fa-file-image-o"></i><span class="nav-label">Vouchers</span></a>
        <!-- target="_blank"  -->
    </li>
    <?php endif ?>
    <?php if ($obMenu->entregaFactura=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="entregaFactura") ? "active" : "" ; ?>">
        <a href="../conta/entregaFacturas"><i class="fa fa-book"></i><span class="nav-label">Entrega factura</span></a>
    </li>
    <?php endif ?>
    <?php if (Comisiones::verificarSiExisteConvenioMenu()||$session->idusuario=='2'||$session->idusuario=='66'): ?>
    <li class="<?php echo $retVal = ($nav=="convenios") ? "active" : "" ; ?>">
        <a href="../com/convenios"><i class="fa fa-slideshare"></i><span class="nav-label">Datos de convenios</span></a>
    </li>
    <?php endif ?>



    <?php if ($obMenu->plataformaWebASP=="si"): ?>
    <li class="<?php echo $retVal = ($nav=="plataformaWebASP") ? "active" : "" ; ?>">
        <a href="../admin/plataformaWebASP"><i class="fa fa-location-arrow"></i><span class="nav-label">Plataforma
                Web</span></a>
    </li>
    <?php endif ?>

</ul>
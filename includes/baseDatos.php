<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
class BaseDatos 
{
	public $coneccion;

	function __construct(){
		$this->conectar();
	}

	public function conectar(){
		// $this->coneccion=mysqli_connect(HOST,USUARIO,CONTRASENIA,BASEDATOS);
		$this->coneccion=new mysqli(HOST,USUARIO,CONTRASENIA,BASEDATOS,PORT);		

		// $this->coneccion=new PDO('mysql:host='.HOST.';dbname='.BASEDATOS.'',USUARIO,CONTRASENIA);	
		if ($this->coneccion->connect_errno) {
			die("Ups algun error ocurrio por favor comunicate con kcabrera@tracklink.pe 3".$this->coneccion->connect_error);
		}

		/*if (!$this->coneccion->set_charset("utf8")) {
			die("Error mostrando el conjunto de caracteres utf8");
		}*/
	}

	public function queryProc($sql){
		$resultado=$this->coneccion->query($sql);
		$this->confirmar($resultado);
		mysqli_next_result($this->coneccion);
		//mysqli_free_result($this->coneccion);
		return $resultado;
	}
	public function query($sql){
		$resultado=$this->coneccion->query($sql);
		$this->confirmar($resultado, $sql);
		return $resultado;
	}

	public function confirmar($resultado, $sql=""){
		if (!$resultado) {
			echo($sql);
			die("Ups algun error ocurrio por favor comunicate con kcabrera@tracklink.pe 4".$this->coneccion->error);
		}
	}

	public function escapeString($string){
		$escape=$this->coneccion->real_escape_string($string);
		return $escape;	
	}

	public function ultimoIdInsertado(){
		return mysqli_insert_id($this->coneccion);
	}	



}

$db = new BaseDatos();
}
?>
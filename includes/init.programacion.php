<?php 
if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
	$path = $_SERVER['DOCUMENT_ROOT'];
	require_once("funciones.php");
	require_once("configuracion.php");
	require_once("Recursos.php");
	require_once("baseDatosOnix.php");
	require_once("Onix.php");
	require_once("baseDatos.php");
	require_once("Usuario.php");
	require_once("Session.php");
	require_once("tablaProgramacion.php");
	require_once("CTBProgramacion.php");
	require_once($path."/com/class/Vehiculo.php");
	require_once($path."/com/class/Cliente.php");
	require_once($path."/com/class/Cita.php");
	require_once($path."/com/class/RelacionComercial.php");
	require_once($path."/ope/class/Operacion.php");
	date_default_timezone_set('America/Lima');
}
 ?>

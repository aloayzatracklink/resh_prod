<?php 
	if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
class Calendario extends Recursos
{
	public $id;
	public $title;
	public $startdate;
	public $enddate;
	public $color;
	public $textColor;
	public $allDay;
	public $idoperacion;
	public $resourceId;
	public $idcita;
	public $cantidad;
	function __construct()
	{
		# code...
	}
	public function registrarFechaInstalacion(){
		global $db;
		$this->allDay='false';
		$sql="INSERT INTO calendar(`title`, `startdate`, `enddate`,`color`,`textColor`, `allDay`, `idoperacion`, `idcita`) VALUES ('";
		$sql.=$db->escapeString($this->title)."', '";
		$sql.=$db->escapeString($this->startdate)."', '";
		$sql.=$db->escapeString($this->enddate)."', '";
		$sql.=$db->escapeString($this->color)."', '";
		$sql.=$db->escapeString($this->textColor)."', '";
		$sql.=$db->escapeString($this->allDay)."', '";
		$sql.=$db->escapeString($this->idoperacion)."', '";
		$sql.=$db->escapeString($this->idcita)."')";
		if($db->query($sql)){
			return true;
		}else{
			die("error al ingresar los datos de instalacion al calendario ");
		}
	}
	public function confirmarFechaInstalacion($idcalendar){
		global $db;
		$sql="UPDATE calendar SET ";
		$sql.=" resourceId = '";
		$sql.=$db->escapeString($this->resourceId)."', ";
		$sql.=" title = '";
		$sql.=$db->escapeString($this->title)."', ";
		$sql.=" startdate = '";
		$sql.=$db->escapeString($this->startdate)."', ";
		$sql.=" color = '";
		$sql.=$db->escapeString($this->color)."' ";
		$sql.="WHERE idoperacion =".$db->escapeString($idcalendar);
		$db->query($sql);
		
	}
	public function asignarTecnico($idoperacion,$tecnicoID){
		global $db;
		$sql="UPDATE calendar SET resourceId = '$tecnicoID' WHERE idoperacion = '$idoperacion'";
		$db->query($sql);
	}
	public static function obtenerTituloInstalacion($id){
		$obtenerTituloInstalacionQ=self::sentencia("SELECT * FROM calendar WHERE idoperacion = $id LIMIT 1");
		return  !empty($obtenerTituloInstalacionQ) ? array_shift($obtenerTituloInstalacionQ) : false ;
	}

	public function confirmarFechaChequeo(){
		global $db;
		$sql="UPDATE calendar SET";
		$sql.=" startdate='";
		$sql.=$db->escapeString($this->startdate)."', ";
		$sql.=" resourceId='";
		$sql.=$db->escapeString($this->resourceId)."', ";
		$sql.=" title='";
		$sql.=$db->escapeString($this->title)."' WHERE ";
		$sql.=" idoperacion='";
		$sql.=$db->escapeString($this->idoperacion)."'";
		if($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
	public function confirmarChequeoEnMonitoreo(){
		global $db;
		$sql="UPDATE calendar SET";
		$sql.=" color = '";
		$sql.=$db->escapeString($this->color)."',";
		$sql.="title = '";
		$sql.=$db->escapeString($this->title)."' ";
		$sql.="WHERE idoperacion='".$db->escapeString($this->idoperacion)."'";
		return $var=($db->query($sql))?true:false;

	}
	public function guardarComoPendiente(){
		global $db;
		$sql="UPDATE calendar SET";
		$sql.=" color='";
		$sql.=$db->escapeString($this->color)."', ";
		$sql.=" title='";
		$sql.=$db->escapeString($this->title)."' WHERE idoperacion= '";
		$sql.=$db->escapeString($this->idoperacion)."'";
		return $var = ($db->query($sql)) ? true : false ;
	}
	public function borrarCita(){
		global $db;
		$sql="DELETE FROM calendar WHERE idcita='";
		$sql.=$db->escapeString($this->idcita)."'";
		return ($db->query($sql))?true:false;
	}
	public function borrarChequeoConIdOperacion(){
		global $db;
		$sql="DELETE FROM calendar WHERE idoperacion='";
		$sql.=$db->escapeString($this->idoperacion)."'";
		return ($db->query($sql))?true:false;
	}

	public function obtenerTareasProgramadas($desde,$hasta){
		$obtener=self::sentencia("SELECT * FROM calendar WHERE startdate BETWEEN '$desde' AND '$hasta' ");
		return !empty($obtener)?$obtener:false;

	}

	public function obtenerTareasProgramadasCantidad($desde,$hasta){
		$obtener=self::sentencia("SELECT COUNT(*) as cantidad FROM calendar WHERE startdate BETWEEN '$desde' AND '$hasta' ");
		return !empty($obtener)?array_shift($obtener):false;

	}
	public function obtenerTareasProgramadasTaller($desde,$hasta){
		$obtener=self::sentencia("SELECT COUNT(*) as cantidad FROM calendar WHERE startdate BETWEEN '$desde' AND '$hasta' AND (color = '#1ab394' OR color = '#c2c4d3' OR color='#48874a')");
		return !empty($obtener)?array_shift($obtener):false;

	}
	public function obtenerTareasProgramadasHora($desde){
		$obtener=self::sentencia("SELECT COUNT(*)  as cantidad FROM calendar WHERE startdate  LIKE '%$desde%' ");
		return !empty($obtener)?array_shift($obtener):false;

	}
	public function confirmarTecnicoYFecha(){
		global $db;
		$sql="UPDATE calendar SET resourceId='";
		$sql.=$this->resourceId."', ";
		$sql.="startdate='";
		$sql.=$this->startdate."' WHERE ";
		$sql.=" idoperacion='";
		$sql.=$this->idoperacion."' ";
		return ($db->query($sql))?true:false;
	}

	public static function obtenerPorId($id){
		$slqspor=self::sentencia("SELECT idoperacion,idcita FROM calendar WHERE id='$id' LIMIT 1");
		return !empty($slqspor)?array_shift($slqspor):false;

	}

	// public function obtenerTecnicoAsignadoEnFecha($desde,$hasta){
	// 	$obteTecnEnFecha=self::sentencia("SELECT * FROM operaciones WHERE idTecnicoAsignado='$this->idTecnicoAsignado' AND (fechaInstalacion BETWEEN '$desde' AND '$hasta' OR DATE_ADD(fechaInstalacion,INTERVAL -2 HOUR) BETWEEN '$desde' AND '$hasta'  ) ");
	// 	return !empty($obteTecnEnFecha)?true:false;
	// }
	public function obtenerTecnicoAsignadoEnFecha($desde,$hasta){
		$obteTecnEnFecha=self::sentencia("SELECT * FROM calendar WHERE resourceId='$this->resourceId' AND (DATE_FORMAT(startdate,'%Y/%m/%d %H:%i') BETWEEN '$desde' AND '$hasta') ");
		return !empty($obteTecnEnFecha)?true:false;
	}
    public function verificarsiExisteEliminar(){
        global $db;
        $obteTecnEnFecha=self::sentencia("SELECT * FROM calendar WHERE idoperacion='$this->idoperacion' ");
        if(!empty($obteTecnEnFecha)){
            $dlQ="DELETE FROM calendar WHERE idoperacion='$this->idoperacion' "; 
            return ($db->query($dlQ))?true:false;
        }else{
            return true; 
        }
    }
    public static function obtenerTrabajos($fecha){
        $fecha=formatoSinHoraAniOPrimerodia($fecha);
        $sqlll=self::sentencia("SELECT * FROM calendar WHERE startdate like '%$fecha%' ");
        return !empty($sqlll)?$sqlll:false;
    }
    
    public function cambiarFecha($fechaOld,$fechaN){
        global $db;
        $fechaOld=$db->escapeString($fechaOld);
        $fechaN=$db->escapeString($fechaN);
        $cFsql="UPDATE calendar SET startdate = REPLACE(startdate ,'$fechaOld','$fechaN') WHERE idoperacion = '$this->idoperacion' ";
        return ($db->query($cFsql))?true:false;
    }

}
}

 ?>

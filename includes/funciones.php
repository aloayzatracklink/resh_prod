<?php 
if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
	function autoloader($clase){
		$clase=ucfirst($clase);	
		$laRuta="includes/{$clase}.php";
		if (file_exists($laRuta)) {
			require_once($laRuta);
		}else{
			// die("the fail");
		}
	}
	spl_autoload_register('autoloader');

    function dd($data){
        return die("<pre>".var_dump($data)."</pre>");
    }
	
	function irA($ruta){
		header("Location:$ruta");
	}

	function dirigirApaginaEspecificaSegunRol($rolId){

	}
	function obtenerEstadoTRAMA($idestado){
		if ($idestado==1) {
			return '<span class="label label-info">Pendiente</span>';
		} else if($idestado==15) {
			return '<span class="label label-primary">Instalado</span>';
		}else if($idestado==16) {
			return '<span class="label label-success">Activo</span>';
		}else if($idestado==99) {
			return '<span class="label label-danger">Anulado</span>';
		}else{
			return 'error en obtener estado ';
		}		
	}


	function obtenerEstadoSeguimientoTRAMA($idestado){
		if ($idestado==0) {
			return '<span class="label label-info">No Contactado</span>';
		} else if($idestado==1) {
			return '<span class="label label-info">Contactado</span>';
		}else if($idestado==2) {
			return '<span class="label label-info">Agendado</span>';
		}else if($idestado==3) {
			return '<span class="label label-primary">Programado</span>';
		}else if($idestado==4) {
			return '<span class="label label-success">Instalado</span>';
		}else{
			return 'error en obtener estado ';
		}
	}

	function obtenerEstadoQuery($idestado){
		$objtp = SolicitudCredito::obtenEstado($idestado);
		if ($objtp):
			foreach ($objtp as $var):
				return "<span class='label label-".$var->clase."'>".$var->estadoEst."</span>";
			endforeach;
		else:
			echo '';
		endif;
	}

	function obtenerEstadoAnomalia($idestado){
		$anomali = new anomalia();
		//$objtp = anomalia::obtenerListEstadoAnomalia($idestado);
		$objtp = $anomali->obtenerListEstadoAnomalia($idestado);
		if ($objtp):
			foreach ($objtp as $var):
				return "<span class='label label-".$var->color."'>".$var->descripcion."</span>";
			endforeach;
		else:
			echo '';
		endif;
	}



	function obtenerEtiqueta($idestado){
		$objtp = SeguimientoPK::obtenerListEtiqueta($idestado);
		if ($objtp):
			foreach ($objtp as $var):
				return "<span class='label label-".$var->color."'>".$var->descripcion."</span>";
			endforeach;
		else:
			echo '';
		endif;
	}


	function obtenerEstadoSegPK($idestado){
		if ($idestado==0) {
			return '<span class="label label-info">Pendiente</span>';
		}  else if($idestado==8) {
			return '<span class="label label-primary">Seguimiento</span>';
		}else if($idestado==15) {
			return '<span class="label label-success">Realizado</span>';
		}
	}

function obtenerEstadoComision($idestado){
	if ($idestado==1) {
		return '<span class="label label-success">Tarjeta</span>';
	} else {
		return '<span class="label label-primary">Factura</span>';
	}
}
	function obtenerEstado($idestado){
		if ($idestado==1) {
			return '<span class="label label-info">Creado</span>';
		} else if($idestado==2) {
			return '<span class="label label-primary">Por instalar</span>';
		}else if($idestado==3) {
			return '<span class="label label-success">Instalado sin bienvenida</span>';
		}else if($idestado==4) {
			return '<span class="label label-success">Instalado</span>';
		}else if($idestado==5) {
			return '<span class="label label-warning">Por vencer</span>';
		}else if($idestado==6) {
			return '<span class="label label-warning">Contrato vencido</span>';
		}else if($idestado==7) {
			return '<span class="label label-danger">Desinstalado</span>';
		}else if($idestado==8) {
			return '<span class="label label-warning">Seguimiento</span>';
		}else if($idestado==9) {
			return '<span class="label label-success">Recuperado</span>';
		}else if($idestado==10) {
			return '<span class="label label-danger">No recuperado</span>';
		}else if($idestado==11) {
			return '<span class="label label-warning">Asistiendo</span>';
		}else if($idestado==12) {
			return '<span class="label label-primary">Asistido</span>';
		}else if($idestado==13) {
			return '<span class="label label-primary">Chequeo</span>';
		}else if($idestado==14) {
			return '<span class="label label-warning">En SAC</span>';
		}else if($idestado==15) {
			return '<span class="label label-success">Realizado</span>';
		}else if($idestado==16) {
			return '<span class="label label-warning">Dar de baja</span>';
		}else if($idestado==17) {
			return '<span class="label label-danger">Sin registrar</span>';
		}else if($idestado==18) {
			return '<span class="label label-danger">Anulado</span>';
		}else if($idestado==19) {
			return '<span class="label label-danger">No contactado</span>';
		}else if($idestado==20) {
			return '<span class="label label-primary">Registrado</span>';
		}else if($idestado==21) {
			return '<span class="label label-success">No verificado</span>';
		}else if($idestado==23) {
			return '<span class="label label-warning">Falta registrar Vehiculos</span>';
		}else if($idestado==24) {
			return '<span class="label label-success">Falta registrar Contactos autorizados</span>';
		}else if($idestado==25) {
			return '<span class="label label-success">Falta dar las gracias</span>';
		}else if($idestado==26) {
			return '<span class="label label-success">Verificado</span>';
		}else if($idestado==27) {
			return '<span class="label label-warning">No pagado</span>';
		}else if($idestado==28) {
			return '<span class="label label-success">No Facturado</span>';
		}else if($idestado==29) {
			return '<span class="label label-warning">No pagado</span>';
		}else if($idestado==30) {
			return '<span class="label label-success">Pagado sin verificar</span>';
		}else if($idestado==31) {
			return '<span class="label label-success">Accepto</span>';
		}else if($idestado==32) {
			return '<span class="label label-primary">Cancelado</span>';
		}else if($idestado==33) {
			return '<span class="label label-primary">Activo</span>';
		}else if($idestado==34) {
			return '<span class="label label-primary">Facturado</span>';
		}else if($idestado==35) {
			return '<span class="label label-primary">Observado por contabilidad</span>';
		}else if($idestado==36) {
			return '<span class="label label-primary">En Monitoreo</span>';
		}else if($idestado==0) {
			return '<span class="label label-warning">Pendiente</span>';
		}else if($idestado==37) {
			return '<span class="label label-success">Disponible</span>';
		}else if($idestado==38) {
			return '<span class="label label-primary">Instalando</span>';
		}else if($idestado==39) {
			return '<span class="label label-info">Viaje</span>';
		}else if($idestado==40) {
			return '<span class="label label-danger">No trabaja</span>';
		}else if($idestado==41) {
			return '<span class="label label-warning">Pagará en caja</span>';
		}else if($idestado==42) {
			return '<span class="label label-warning">Pagará en caja</span>';
		}else if($idestado==55) {
			return '<span class="label label-info">Falso Robo</span>';
		}else if($idestado==56) {
			return '<span class="label label-warning">No Renovado</span>';
		}else if($idestado==57) {
			return '<span class="label label-primary">Renovado</span>';
		}else if($idestado==58) {
			return '<span class="label label-warning">Pendiente datos de canal</span>';
		}else if($idestado==59) {
			return '<span class="label label-primary">Pendiente pago de comision</span>';
		}else if($idestado==60) {
			return '<span class="label label-primary">Todo bien</span>';
		}else if($idestado==61) {
			return '<span class="label label-danger">Falso Choque</span>';
		}else if($idestado==62) {
			return '<span class="label label-danger">No pagado</span>';
		}else if($idestado==67) {
			return '<span class="label label-primary">Pagado</span>';
		}else if($idestado==63) {
			return '<span class="label label-warning">Pago parcial</span>';
		}else if($idestado==64) {
			return '<span class="label label-primary">Chequeo en linea</span>';
		}else if($idestado==65) {
			return '<span class="label label-primary">Chequeo por monitoreo</span>';
		}else if($idestado==66) {
			return '<span class="label label-primary">Chequeo de rutina</span>';		
		}else if($idestado==68) {
			return '<span class="label label-primary">Entregado</span>';		
		}else if($idestado==69) {
			return '<span class="label label-danger">No entregado</span>';		
		}else if($idestado==70) {
			return '<span class="label label-primary">Enviado</span>';		
		}else if($idestado==777) {
			return '<span class="label label-primary">Realizado</span>';
		}else if($idestado==400) {
			return '<span class="label label-warning">Pendiente</span>';
		}else if($idestado==123) {
			return '<span class="label label-warning">Reprogramado</span>';
		}else if($idestado==321) {
			return '<span class="label label-danger">Reprogramar</span>';
		}else if($idestado==75) {
			return '<span class="label label-danger">Eliminado</span>';
		}else if($idestado==76) {
			return '<span class="label label-danger">Pendiente Tecnico</span>';
		}else if($idestado==77) {
			return '<span class="label label-warning">Facturar fin mes</span>';
		}else if($idestado==78) {
			return '<span class="label label-warning">Pendiente desinstalación</span>';
		}else if($idestado==79) {
			return '<span class="label label-info">Falso crash</span>';
		}else if($idestado==90) {
			return '<span class="label label-success">No reportado</span>';
		}else if($idestado==91) {
			return '<span class="label label-success">Enviado para autorización</span>';
		}else if($idestado==92) {
			return '<span class="label label-danger">No disponible</span>';
		}else if($idestado==93) {
			return '<span class="label label-success">Gestionado</span>';
		}else if($idestado==94) {
			return '<span class="label label-success">Incautado</span>';
		}else if($idestado==99) {
			return '<span class="label label-danger">No activado</span>';
		}else if($idestado==71) {
			return '<span class="label label-danger">Autoespar</span>';
		}else{
			return 'error en obtener estado ';
		}		
	}

function obtenerDireccionIP(){
    if (!empty($_SERVER ['HTTP_CLIENT_IP'] ))
      $ip=$_SERVER ['HTTP_CLIENT_IP'];
    elseif (!empty($_SERVER ['HTTP_X_FORWARDED_FOR'] ))
      $ip=$_SERVER ['HTTP_X_FORWARDED_FOR'];
    else
      $ip=$_SERVER ['REMOTE_ADDR'];
    return $ip;
}

function restringirIp($ip)
{
    $ipCliente = obtenerDireccionIP();

    if($ipCliente == $ip)
    {
        return true;
    }
    else
    {
        header('location: http://www.tracklink.pe');
        exit;
    }
}
function restringirRango()
{

    $ipCliente = obtenerDireccionIP();

    if(substr($ipCliente, 0, 8 ) == "150.214.")
    {
        return true;
    }
    else
    {
        header('location: http://www.tracklink.pe');
        exit;
    }
}
function restringirConjuntoIps($ips)
{
    $ipCliente = obtenerDireccionIP();
    if (in_array($ipCliente,$ips))
    {
        return true;
    }
    else
    {
        header('location: http://www.tracklink.pe');
        exit;
    }
}

function enviarMensaje($msj){
	if(!empty($msj)){
		$_SESSION['mensaje']=$msj;
	}else{
		$msj="";     
	}
}
function mostrarMensaje(){
	if (isset($_SESSION['mensaje'])) {
		echo $_SESSION['mensaje'];
		unset($_SESSION['mensaje']);
	}
}

function hace($fecha){

	$hoy=new DateTime();         
	$fecha=date("d-m-Y h:i a",strtotime($fecha));
	$vence=new DateTime($fecha);
	$intervalo=date_diff($hoy,$vence);
	// echo $intervalo->format('%R%a días');
	// if ($intervalo->format('%R')=="-") {
	//     return "Hace : ";
	// }
	if ($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" AND $intervalo->format('%h')=="0"){
	    return $fecha." <small>".$intervalo->format('Hace  %i minutos')."</small>"; 
	}else if ($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" ){
	    return $fecha." <small>".$intervalo->format('Hace %h horas con %i minutos')."</small>";

	}else if ($intervalo->format('%m')=="0") {
	    return $fecha." <small>".$intervalo->format('hace %d Días y %h horas con %i minutos')."</small>";
	}else{
		return $fecha." <small>".$intervalo->format('hace %m Meses %d Días y %h horas con %i minutos')."</small>"; 
	}
}

function iniciodiaSemanaSemanaLunes($fecha){       
	$dia=date("d",strtotime($fecha));
	$diaSemana=date("N",strtotime($fecha));
	$res=$dia-$diaSemana+1;
	return ($res>0)?$res:'1';
}
function findiaSemanaSemanaLunes($fecha){       
	$dia=date("d",strtotime($fecha));
	$diaSemana=date("N",strtotime($fecha));	   
	$ultimoDiaDelMes=date("t",strtotime($fecha));	   
	$res=$dia-$diaSemana+6;
	return ($res>$ultimoDiaDelMes)?$ultimoDiaDelMes:$res;
}

function obtenerprimerdiaDel($menosMes){
	return date('Ymd0000', strtotime("first day of -$menosMes month")); 
}
function obtenerultimodiaDel($menosMes){
	return date('Ymd2359', strtotime("last day of -$menosMes month")); 
}

function vence($fecha){
	 $hoy=new DateTime();         
    $fecha=date("d-m-Y",strtotime($fecha));
    $vence=new DateTime($fecha);
    $intervalo=date_diff($hoy,$vence);
    // echo $intervalo->format('%R%a días');
    $imprimir="";
    if ($intervalo->format('%R')=="-") {
        $imprimir.="<span class='text-danger'>Vencio hace:";
    }else{
        $imprimir.= "Vence en: ";
    }
    if ($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" AND $intervalo->format('%y')=="0") {
    		return $imprimir.=$intervalo->format('%h horas');
    }else if($intervalo->format('%m')=="0"  AND $intervalo->format('%y')=="0"){
    	  return $imprimir.=$intervalo->format(' %d Días</span> ');
    }else if($intervalo->format('%y')=="0"){
    	  return $imprimir.=$intervalo->format(' %m meses y %d Días</span> ');
    }else {
    	  return $imprimir.=$intervalo->format('%y Año y %m meses</span> ');
    }
    // return $imprimir.=$intervalo->format('%y año y %m Meses y %d Días');
}


function formatoHaceEn($fecha){
	 $hoy=new DateTime();         
    $fecha=date("d-m-Y H:i",strtotime($fecha));
    $dia=date("d",strtotime($fecha));

    $diaHoy=date("d");
    $ayer=date("d",strtotime("-1 days"));
    $manana=date("d",strtotime("tomorrow"));

    $vence=new DateTime($fecha);
    $intervalo=date_diff($hoy,$vence);
    // echo $intervalo->format('%R%a días');
    $imprimir="";
    if ($intervalo->format('%R')=="-") {
        $imprimir.="<span class='text-danger'>Hace:";
    }else if($intervalo->format('%R')=="+"){
        $imprimir.= "<span>En: ";
    }
    if($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" AND $intervalo->format('%y')=="0"AND $intervalo->format('%h')=="0"){
    	return	$fecha." hoy ".strtolower($imprimir).$intervalo->format('%i minutos');
    }
    else if ($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" AND $intervalo->format('%y')=="0") {
    	
    	if ($dia==$diaHoy) {
    		return	$fecha." Hoy ".strtolower($imprimir).$intervalo->format('%h horas');    		
    	}elseif ($dia==$ayer) {
    		return	$fecha." Ayer ".strtolower($imprimir).$intervalo->format('%h horas');    		
    	}elseif ($dia==$manana) {
    		return	$fecha." Mañana ".strtolower($imprimir).$intervalo->format('%h horas');    		
    	}
    	// return	$fecha." hoy ".strtolower($imprimir).$intervalo->format('%h horas');
    }else if($intervalo->format('%m')=="0"  AND $intervalo->format('%y')=="0"){
    	if ($dia==$manana) {
    	  return $fecha." Mañana ";
    	}elseif($dia==$ayer){
    	  return $fecha."<span class='text-danger'> Ayer ";
    		
    	}else{
    	  return $fecha." ".$imprimir.$intervalo->format(' %d días</span> ');
    	}
    }else if($intervalo->format('%y')=="0"){
    	  return $fecha." ".$imprimir.$intervalo->format(' %m meses y %d días</span> ');
    }else {
    	  return $fecha." ".$imprimir.$intervalo->format('%y año y %m meses</span> ');
    }
    // return $imprimir.=$intervalo->format('%y año y %m Meses y %d Días');
}
function formatoHaceEnSinFecha($fecha){
	 $hoy=new DateTime();         
    $fecha=date("d-m-Y H:i",strtotime($fecha));
    $dia=date("d",strtotime($fecha));
    $hora=date("H:i",strtotime($fecha));
    $diaHoy=date("d");
    $ayer=date("d",strtotime("-1 days"));
    $manana=date("d",strtotime("tomorrow"));

    $vence=new DateTime($fecha);
    $intervalo=date_diff($hoy,$vence);
    // echo $intervalo->format('%R%a días');
    $imprimir="";
    if ($intervalo->format('%R')=="-") {
        $imprimir.="<span class='text-danger'>Hace:";
    }else if($intervalo->format('%R')=="+"){
        $imprimir.= "<span>En: ";
    }
    if($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" AND $intervalo->format('%y')=="0"AND $intervalo->format('%h')=="0"){
    	return	$hora." hoy ".strtolower($imprimir).$intervalo->format('%i minutos');
    }
    else if ($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" AND $intervalo->format('%y')=="0") {
    	
    	if ($dia==$diaHoy) {
    		return	$hora." Hoy ".strtolower($imprimir).$intervalo->format('%h horas');    		
    	}elseif ($dia==$ayer) {
    		return	$hora." Ayer ".strtolower($imprimir).$intervalo->format('%h horas');    		
    	}elseif ($dia==$manana) {
    		return	$hora." Mañana ".strtolower($imprimir).$intervalo->format('%h horas');    		
    	}
    	// return	$hora." hoy ".strtolower($imprimir).$intervalo->format('%h horas');
    }else if($intervalo->format('%m')=="0"  AND $intervalo->format('%y')=="0"){
    	if ($dia==$manana) {
    	  return $hora." Mañana ";
    	}elseif($dia==$ayer){
    	  return $hora."<span class='text-danger'> Ayer ";
    		
    	}else{
    	  return $hora." ".$imprimir.$intervalo->format(' %d días</span> ');
    	}
    }else if($intervalo->format('%y')=="0"){
    	  return $hora." ".$imprimir.$intervalo->format(' %m meses y %d días</span> ');
    }else {
    	  return $hora." ".$imprimir.$intervalo->format('%y año y %m meses</span> ');
    }
    // return $imprimir.=$intervalo->format('%y año y %m Meses y %d Días');
}

function haceMinimo($fecha){
	$hoy=new DateTime();         
	$fecha=date("d-m-Y h:i a",strtotime($fecha));
	$dia=date("d",strtotime($fecha));
	$diaHoy=date("d");
	$ayer=date("d",strtotime("-1 days"));
	$vence=new DateTime($fecha);
	$intervalo=date_diff($hoy,$vence);
	// echo $intervalo->format('%R%a días');
	// if ($intervalo->format('%R')=="-") {
	//     return "Hace : ";
	// }
	if ($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" AND $intervalo->format('%h')=="0"){
	    return $fecha." <small>".$intervalo->format('Hace  %i minutos')."</small>"; 
	}else if ($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" ){
		if ($dia==$diaHoy) {
    		return	$fecha."<small> Hoy hace ".$intervalo->format('%h horas')."</small>";    		
    	}elseif ($dia==$ayer) {
    		return	$fecha."<small> Ayer hace ".$intervalo->format('%h horas')."</small>";    		
    	}else{
	    return $fecha." <small>".$intervalo->format('Hace %h horas')."</small>";   		
	    	
		}

	}else if ($intervalo->format('%m')=="0") {
		if($dia==$ayer){
    	  return $fecha."<small> Ayer </small>";    		
    	}else{
    	  return $fecha." <small>hace ".$intervalo->format(' %d días</small> ');
    	}
	}else{
		return $fecha." <small>".$intervalo->format('hace %m Meses ')."</small>"; 
	}
}
function haceMinimoDias($fecha){

	$hoy=new DateTime();         
	$fecha=date("Y-m-d",strtotime($fecha));
	$dia=date("d",strtotime($fecha));
	$diaHoy=date("d");
	$ayer=date("d",strtotime("-1 days"));
	$vence=new DateTime($fecha);
	$intervalo=date_diff($hoy,$vence);
	// echo $intervalo->format('%R%a días');
	// if ($intervalo->format('%R')=="-") {
	//     return "Hace : ";
	// }
 if ($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" ){
		if ($dia==$diaHoy) {
    		return	$fecha."<small> Hoy </small>";    		
    	}elseif ($dia==$ayer) {
    		return	$fecha."<small> Ayer </small>";    		
    	}

	}else if ($intervalo->format('%m')=="0") {
		if($dia==$ayer){
    	  return $fecha."<small> Ayer </small>";    		
    	}else{
    	  return $fecha." <small>hace ".$intervalo->format(' %d días</small> ');
    	}
	}else{
		return $fecha." <small>".$intervalo->format('hace %m Meses ')."</small>"; 
	}
}
function haceMinimoAnio($fecha){

	$hoy=new DateTime();         
	$fecha=date("d-m-Y h:i a",strtotime($fecha));
	$vence=new DateTime($fecha);
	$intervalo=date_diff($hoy,$vence);
	// echo $intervalo->format('%R%a días');
	// if ($intervalo->format('%R')=="-") {
	//     return "Hace : ";
	// }
	if ($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" AND $intervalo->format('%h')=="0" AND $intervalo->format('%y')=="0"){
	    return $fecha." <small>".$intervalo->format('Hace  %i minutos')."</small>"; 
	}else if ($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" AND $intervalo->format('%y')=="0"){
	    return $fecha." <small>".$intervalo->format('Hace %h horas')."</small>";

	}else if ($intervalo->format('%m')=="0" AND $intervalo->format('%y')=="0") {
	    return $fecha." <small>".$intervalo->format('hace %d Días ')."</small>";
	}else if($intervalo->format('%y')=="0"){
		return $fecha." <small>".$intervalo->format('hace %m Meses ')."</small>"; 
	}else{
		return $fecha." <small>".$intervalo->format('hace %y año y %m Meses ')."</small>"; 

	}
}

function hace1($fecha){

	$hoy=new DateTime();         
	$fecha=date("d-m-Y h:i a",strtotime($fecha));
	$vence=new DateTime($fecha);
	$intervalo=date_diff($hoy,$vence);
	// echo $intervalo->format('%R%a días');
	// if ($intervalo->format('%R')=="-") {
	//     return "Hace : ";
	// }
	if ($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" AND $intervalo->format('%h')=="0"){
	    return $intervalo->format('Hace  %i minutos'); 
	}else if ($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" ){
	    return $intervalo->format('Hace %h horas');

	}else if ($intervalo->format('%m')=="0") {
	    return $intervalo->format('hace %d Días y %h horas');
	}else{
		return $intervalo->format('hace %m Meses %d Días y %h horas con %i minutos'); 
	}
}

function hace2($fecha){

	$hoy=new DateTime();         
	$fecha=date("d-m-Y h:i a",strtotime($fecha));
	$fecha2=date("h:i a",strtotime($fecha));
	$vence=new DateTime($fecha);
	$intervalo=date_diff($hoy,$vence);
	// echo $intervalo->format('%R%a días');
	// if ($intervalo->format('%R')=="-") {
	//     return "Hace : ";
	// }
	if ($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" AND $intervalo->format('%h')=="0"){
	    return $fecha2." ".$intervalo->format('Hace  %i minutos'); 
	}else if ($intervalo->format('%m')=="0" AND $intervalo->format('%d')=="0" ){
	    return $intervalo->format('Hace %h horas con %i minutos');

	}else if ($intervalo->format('%m')=="0") {
	    return $intervalo->format('hace %d Días y %h horas con %i minutos');
	}else{
		return $intervalo->format('hace %m Meses %d Días y %h horas con %i minutos'); 
	}
}
 function formato($fecha){
 	return $fecha=date("d-m-Y H:i a",strtotime($fecha));
 }
 function formatoSinHora($fecha){
 	return $fecha=date("d-m-Y",strtotime($fecha));
 }
 function formatoSinHoraSINCERO($fecha){
 	return $fecha=date("j-m-Y",strtotime($fecha));
 }
 function formatoHora($fecha){
 	return $fecha=date("H:i",strtotime($fecha));
 }
 function formatoSinHoraMesPrimero($fecha){
 	return $fecha=date("m-d-Y",strtotime($fecha));
 }
 function formatoSinHoraAniOPrimero($fecha){
 	return $fecha=date("Y-m-d",strtotime($fecha));
 }
 function formatoAnio($fecha){
 	return $fecha=date("Y",strtotime($fecha));
 }
 function formatoMES($fecha){
 	return $fecha=date("m",strtotime($fecha));
 }
 function formatoSinHoraAniOPrimerodia($fecha){
 	return $fecha=date("Y/m/d",strtotime($fecha));
 }
 function formatodiaLimpio($fecha){
 	return $fecha=date("Ymd",strtotime($fecha));
 }
 function formatohoraLimpio($fecha){
 	return $fecha=date("Hi",strtotime($fecha));
 }
 function formatofechaLimipo($fecha){
 	return $fecha=date("YmdHi",strtotime($fecha));
 }
 function formatofechaLimipoADD($fecha,$hora){
 	$fecha=date("Ymd",strtotime($fecha));
 	return $fecha.$hora;

 }
 function obtenerMesNumero($fecha){
    return $fecha=date("m",strtotime($fecha));
 }
 function formataAniOPrimero24horas($fecha){
 	return $fecha=date("Y-m-d H:i",strtotime($fecha));
 }
 function formatoMesSinHora($fecha){
 	$fecha=date("d-m-Y",strtotime($fecha));
 	$dia=date("d",strtotime($fecha));
 	$anio=date("Y",strtotime($fecha));
 	$mes=date("m",strtotime($fecha));
 	$mes=mesDesdeNumero($mes);
 	return $dia." ".$mes." ".$anio;
 }
function fechaEstadistica($fecha){
	return formatoSinHora(substr($fecha, 0,-1));
} 
function limpiar($string){
	$string=str_replace("*", "", $string);
	return preg_replace('/[^A-Za-z0-9\@ñÑ.:& -]/', '', $string);
}


function limpiarImg($string){
	$string=str_replace("*", "", $string);
	return preg_replace('/[^A-Za-z0-9.]/', '', $string);
}

function limpiarPALABRAGPSSMART($string){
	return $string=trim(str_replace("GPS SMART", "", $string));
}  
function limpiarGuion($string){
	return $string=trim(str_replace("-", "", $string));
}  

function limpiarC($string){
	return $string=trim(str_replace("'", "", $string));
}  
function cambiarPorguion($string){
	return $string=trim(str_replace("/", "-", $string));
}  
function limpiarPALABRAINSTALACIONTRACK($string){
	return $string=trim(str_replace("INSTALACION TRACK ", "", $string));
}
function limpiarPALABRARACK($string){
	return $string=trim(str_replace("TRACK ", "", $string));
}
function limpiarPALABRAINSTALACION($string){
	return $string=trim(str_replace("INSTALACION", "", $string));
}
function limpiarPALABRAINSTALACIONTidel($string){
	return $string=trim(str_replace("INSTALACIÓN", "", $string));
}
function limpiarPALABRAactivacion($string){
	$string=trim(str_replace("ACTIVACION ", "", $string));
	return $new=limpiarPALABRARACK(limpiarPALABRAINSTALACION(limpiarPALABRAINSTALACIONTidel((utf8_encode($string)))));
}
function limpiarPlan($string){
	$string=trim(str_replace("RENOVACION ", "", $string));
	return $new=limpiarPALABRARACK(limpiarPALABRAINSTALACION(limpiarPALABRAINSTALACIONTidel((utf8_encode($string)))));
}
function limpiarPendiente($string){
	return $string=trim(str_replace("PENDIENTE", "", strtoupper($string)));	
}
function limpiarAnioS($string){
	return $string=trim(str_replace("S", "", strtoupper($string)));	
}
function limpiarAnio($string){
	$string=trim(str_replace("AñO", "", strtoupper($string)));	
	return limpiarAnioS($string);
}
function limpiarSeguro($string){
	return $string=trim(str_replace("SEGURO", "", strtoupper($string)));			
	}
function limpiarSeguros($string){
	$string=trim(str_replace("SEGUROS", "", strtoupper($string)));			
	return limpiarSeguro($string);
	}
function agregarTilde($string){
	$string=trim(str_replace("BASICO", "BÁSICO", strtoupper($string)));			
	return limpiarSeguro($string);
	}
function agregarAnio($tiempo){
	switch ($tiempo) {
		case $tiempo==1:
			return "AÑO";
			break;
		case $tiempo>1:
			return "AÑOS";
			break;		
		default:
			return "AÑOS";		
			break;
	}
}
function bancoPermitido($banco){
    switch ($banco){
        case $banco=='BANCO CONTINENTAL BBVA':
            return true;
			break;		
        case $banco=='BANCO DE CREDITO':
            return true;
			break;		
        case $banco=='BCP':
            return true;
			break;		
        case $banco=='BANCO FINANCIERO':
            return true;
			break;		
        case $banco=='':
            return false;
			break;		
        default:
            return false;
			break;		
    }
}

function soloNumero($string){
	return preg_replace('/[^0-9]/', '', $string);
}

function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
{
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ 
        $pagination .= '<ul class="pagination1  pull-right">';
        
        $right_links    = $current_page + 4; 
        $previous       = $current_page - 1; //previous link 
        $next           = $current_page + 1; //next link
        $first_link     = true; //boolean var to decide our first link
        
        if($current_page > 1){
      	$previous_link = ($previous==0)? 1: $previous;
            $pagination .= '<li class="first"><a href="#" data-page="1" title="Primero">&laquo;</a></li>'; //first link
            $pagination .= '<li><a href="#" data-page="'.$previous_link.'" title="Anterior">&lt;</a></li>'; //previous link
                for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
                    if($i > 0){
                        $pagination .= '<li><a href="#" data-page="'.$i.'" title="Pagina'.$i.'">'.$i.'</a></li>';
                    }
                }   
            $first_link = false; //set first link to false
        }        
        if($first_link){ //if current active page is first link
            $pagination .= '<li class="">'.$current_page.'</li>';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $pagination .= '<li class="">'.$current_page.'</li>';
        }else{ //regular current link
            $pagination .= '<li class="">'.$current_page.'</li>';
        }                
        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
                $pagination .= '<li><a href="#" data-page="'.$i.'" title="pagina '.$i.'">'.$i.'</a></li>';
            }
        }
        if($current_page < $total_pages){ 
        $next_link = ($i > $total_pages) ? $total_pages : $i;
                $pagination .= '<li><a href="#" data-page="'.$next.'" title="Siguiente">&gt;</a></li>'; //next link
                $pagination .= '<li class="last"><a href="#" data-page="'.$total_pages.'" title="Ultimo">&raquo;</a></li>'; //last link
        }
        
        $pagination .= '</ul>'; 
    }
    return $pagination; //return pagination links
}

function paginate_function2($item_per_page, $current_page, $total_records, $total_pages)
{
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ 
        $pagination .= '<ul class="pagination2  pull-right">';
        
        $right_links    = $current_page + 4; 
        $previous       = $current_page - 1; //previous link 
        $next           = $current_page + 1; //next link
        $first_link     = true; //boolean var to decide our first link
        
        if($current_page > 1){
      	$previous_link = ($previous==0)? 1: $previous;
            $pagination .= '<li class="first"><a href="#resultado2" data-page2="1" title="Primero">&laquo;</a></li>'; //first link
            $pagination .= '<li><a href="#resultado2" data-page2="'.$previous_link.'" title="Anterior">&lt;</a></li>'; //previous link
                for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
                    if($i > 0){
                        $pagination .= '<li><a href="#resultado2" data-page2="'.$i.'" title="Pagina'.$i.'">'.$i.'</a></li>';
                    }
                }   
            $first_link = false; //set first link to false
        }        
        if($first_link){ //if current active page is first link
            $pagination .= '<li class="">'.$current_page.'</li>';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $pagination .= '<li class="">'.$current_page.'</li>';
        }else{ //regular current link
            $pagination .= '<li class="">'.$current_page.'</li>';
        }                
        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
                $pagination .= '<li><a href="#resultado2" data-page2="'.$i.'" title="pagina '.$i.'">'.$i.'</a></li>';
            }
        }
        if($current_page < $total_pages){ 
        $next_link = ($i > $total_pages) ? $total_pages : $i;
                $pagination .= '<li><a href="#resultado2" data-page2="'.$next.'" title="Siguiente">&gt;</a></li>'; //next link
                $pagination .= '<li class="last"><a href="#resultado2" data-page2="'.$total_pages.'" title="Ultimo">&raquo;</a></li>'; //last link
        }
        
        $pagination .= '</ul>'; 
    }
    return $pagination; //return pagination links
}

	function quitarEspacio($string){
		return preg_replace('/\s+/', '', $string);
	}
	function dia(){
		$dia=date('l');
		switch ($dia) {
			case 'Monday':
				return "Lunes";
				break;

			case 'Tuesday':
				return "Martes";
				break;
			case 'Wednesday':
				return "Miercoles";
				break;
			case 'Thursday':
				return "Jueves";
				break;
			case 'Friday':
				return "Viernes";
				break;
			case 'Saturday':
				return "Sabado";
				break;
			case 'Sunday':
				return "Domingo";
				break;
			
			default:
				return "FAIL DETECTED";
				break;
		}
	}

	function diaPARAMETRO($dia){
		switch ($dia) {
			case 'Monday':
				return "Lunes";
				break;

			case 'Tuesday':
				return "Martes";
				break;
			case 'Wednesday':
				return "Miercoles";
				break;
			case 'Thursday':
				return "Jueves";
				break;
			case 'Friday':
				return "Viernes";
				break;
			case 'Saturday':
				return "Sabado";
				break;
			case 'Sunday':
				return "Domingo";
				break;
			
			default:
				return "FAIL DETECTED";
				break;
		}
	}
	function mes(){
		$mes=date('F');
		switch ($mes) {
			case 'January':
				return "Enero";
				break;
			case 'February':
				return "Febrero";
				break;
			case 'March':
				return "Marzo";
				break;
			case 'April':
				return "Abril";
				break;
			case 'May':
				return "Mayo";
				break;
			case 'June':
				return "Junio";
				break;
			case 'July':
				return "Julio";
				break;
			
			case 'August':
				return "Agosto";
				break;
			
			case 'September':
				return "Setiembre";
				break;
			
			case 'October':
				return "Octubre";
				break;
			
			case 'November':
				return "Noviembre";
				break;
			
			case 'December':
				return "Diciembre";
				break;
			
			default:
				return "FAIL DETECTED";
				break;
		}
	}
	function mesDesdeNumero($mes){
		switch ($mes) {
				case '01':
					return "ENERO";
					break;
				case '02':
					return "FEBRERO";
					break;
				case '03':
					return "MARZO";
					break;
				case '04':
					return "ABRIL";
					break;
				case '05':
					return "MAYO";
					break;
				case '06':
					return "JUNIO";
					break;
				case '07':
					return "JULIO";
					break;
				case '08':
					return "AGOSTO";
					break;
				case '09':
					return "SETIEMBRE";
					break;
				case '10':
					return "OCTUBRE";
					break;
				case '11':
					return "NOVIEMBRE";
					break;
				case '12':
					return "DICIEMBRE";
					break;
						
				default:
					return "Fail detected";
					break;
			}
	}

	function obtenerClassSinClass($lugar){
		switch ($lugar) {
			case 'Domicilio Cliente':
				return "warning";
				break;
			case 'Concesionario':
				return "info";
				break;
			
			default:
				return "";
				break;
		}
	}
	function obtenerClass($lugar){
		switch ($lugar) {
			case 'Domicilio Cliente':
				return "class='warning'";
				break;
			case 'Concesionario':
				return "class='info'";
				break;
			
			default:
				return "class=''";
				break;
		}
	}

	function calcularComision($comision,$precioComision,$precioVenta){
		return number_format(($comision*$precioVenta)/$precioComision, 2, '.', ',')  ;
	}

	function rojo($strng){
		return "<span style='color:red' >".$strng."</span>";
	}
	function dosDecimales($numero){
		return number_format((float)$numero, 2, '.', '');
	}

	function convertiArrayString($array){
		if (is_array($array)) {
			return json_encode($array);
		}else{
			return $array;
		}
	}

	function porcentajeDiferencia($Total,$cantidad){
	
			return dosDecimales( (1 - $cantidad / $Total) * 100);			
	}
	function porcentajeD($Total,$x){
		return dosDecimales(($x*100)/$Total);
	}

	function formatoDesdeHasta($desde,$hasta){
		$d1=date("d",strtotime($desde));
		$m1=date("m",strtotime($desde));
		$y1=date("Y",strtotime($desde));
		$d2=date("d",strtotime($hasta));
		$m2=date("m",strtotime($hasta));
		$y2=date("Y",strtotime($hasta));
		if($y1==$y2 and $m1==$m2){
			return 	" del  ".$d1." al ".$d2." de ".strtolower(mesDesdeNumero($m2))." ".$y2;
		}else if($y1==$y2){
			return 	" del  ".$d1." ".strtolower(mesDesdeNumero($m1))." al ".$d2." de ".strtolower(mesDesdeNumero($m2))." ".$y2;
		}else{
			return 	"del  ".$d1." de ".strtolower(mesDesdeNumero($m1))." ".$y1." al ".$d2."  ".strtolower(mesDesdeNumero($m2))." ".$y2;
			
		}
	}

	function in_array_field($needle, $needle_field, $haystack, $strict = false) { 
    if ($strict) { 
        foreach ($haystack as $item) 
            if (isset($item->$needle_field) && $item->$needle_field === $needle) 
                return true; 
    } 
    else { 
        foreach ($haystack as $item) 
            if (isset($item->$needle_field) && $item->$needle_field == $needle) 
                return true; 
    } 
    return false; 
}

function assc_array_count_values($array,$fecha) {
	$n=0;
     foreach($array as $row ) {
           if($row->fechaHoraChequeoLinea==$fecha){
           	$n++;
           }
     }
     return $n;
}
function assc_array_count_values_INSTALA($array,$fecha) {
	$n=0;
     foreach($array as $row ) {
           if($row->startdate==$fecha){
           	$n++;
           }
     }
     return $n;
}
function obtenerRangoHora($hora){
    $hora=formatohoraLimpio($hora);
    if($hora>=800 AND $hora < 1059){
        return "t1";
    }elseif($hora>=1100 AND  $hora < 1359){
        return "t2";
    }elseif($hora>=1400  AND  $hora < 1559){
        return "t3";
    }elseif($hora>=1600  AND  $hora < 1800){
        return "t4";
    }else{
        return false;
    }
}
// function arrayFechaREturn($array,$fecha) {
	
//      foreach($array as $row ) {
//            if($row->fechaHoraChequeoLinea==$fecha){
//            	return true;	
//            }else{
//            	return false;
//            }
//      }
// }
function comparacionYpause($hora,$sim){
   if ($sim==$sim2) {
        $horaU=new DateTime($hora);
        $horaD=new DateTime($hora2);
        $fdif= $horaU->diff($horaD);
        $diferencia=$fdif->format("%H%I%S");
   } 
}
function obtenerSiguienteHora($fecha,$hora){
            $esSabado=new DateTime($fecha);
            $dia=$esSabado->format('D');
            if ($dia=='Sat') {
                if ($hora=='09:00') {
                    return '11:00';
                }else
                if ($hora=='11:00') {
                    return '13:00';
                }else
                if ($hora=='13:00') {
                    return '15:00';
                }
            }else{
                if ($hora=='09:00') {
                    return '11:00';
                }else
                if ($hora=='11:00') {
                    return '14:00';
                }else
                if ($hora=='14:00') {
                    return '16:00';
                }
            }
}


	function obtenerColorDeActividad($trabajo){
		if ($trabajo=='Instalacion') {
			return 'danger';
		}else if($trabajo=='Desinstalacion'){
			return "info";
		}else{
			return "warning";
		}
	}

	function obtenerColorDeActividad2($trabajo,$ubicacion){
		$color='';
		if ($trabajo=='Instalacion') {
			if(trim($ubicacion)=='Taller'){
				$color="ctaller";
			}else if(trim($ubicacion)=='Domicilio'){
				$color='cdomicilio';
			}else if(trim($ubicacion)=='Concesionario'){
				$color='cconcesionario';
			}
			return $color;
		}else {
			if(trim($ubicacion)=='Taller'){
				$color="ctaller ccen";
			}else if(trim($ubicacion)=='Domicilio'){
				$color='cdomicilio ccen';
			}else if(trim($ubicacion)=='Concesionario'){
				$color='cconcesionario ccen';
			}
			return $color;
		}
	}

	/**
	 * Get url domain, protocol and port server web 
	 */
	function getBaseUrl() 
	{
		$currentPath = $_SERVER['PHP_SELF']; 
		$pathInfo = pathinfo($currentPath); 
		$hostName = $_SERVER['HTTP_HOST']; 
		$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
		return $protocol.'://'.$hostName."/";
	}

	function getBaseUrlImages(){
		$urlbase = getBaseUrl();
		return $urlbase."assets/img/";
	}

}

 ?>

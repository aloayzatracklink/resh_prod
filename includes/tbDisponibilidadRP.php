<?php if ($_POST): ?>
<?php
    require_once("../includes/init.programacion.php");
    $fecha=cambiarPorguion($_POST['fecha']);
    $objTablaProgramacion=new CTBProgramacion();
    $objTbProg=$objTablaProgramacion->obtenerDisponibilidad($fecha,1);
    $cabTbProg=$objTablaProgramacion->obtenerDisponibilidad($fecha,2);
    $objTTras=$objTablaProgramacion->ObtenertimeTrasladoDisponibilidad($fecha,0);

    $fechaMostrar=new DateTime($fecha);
    $sumcount=0;
    $O_igual=0;
    $C_igual=0;
    $CH_igual=0;
    $T_igual=0;
    $R_igual=0;
?>
<h4 class="text-center txt-info" class='margin-top' style='margin-top:0px;margin-bottom:0px'><?= diaPARAMETRO($fechaMostrar->format('l'))." ".$fechaMostrar->format('d')." de ".strtolower(mesDesdeNumero($fechaMostrar->format('m'))) ?> </h4>
<div id="table-scroll" class="table-scroll">
    <div class="table-wrap">
        <table class="main-table" id="tbprograma">
            <thead>
                <tr>
                    <th class="fixed-side table-cabecera-horario" scope="col" >#</th>
                    <th class="text-center fixed-side table-cabecera-horario" scope="col">Técnico</th>
                    <?php for($i=0; $i<count($cabTbProg); $i++){
                        if($cabTbProg[$i][1]==2){
                            $sumcount=1;
                        }else{
                            $sumcount=0;
                        }
                        ?>
                    <th colspan="2" class="text-center table-cabecera-horario" scope="col" ><?php echo $cabTbProg[$i][0] ?></th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
            <?php if($objTbProg){
            for($x=0; $x<count($objTbProg); $x++){
                $T_igual='';
                ?>
                <tr>
                    <td class="fixed-side text-center" ><?= $x+1;?></td>
                    <td class="fixed-side" ><?= str_replace('Pandero','(Pandero)', $objTbProg[$x][1]);?></td>
                    <?php for($xi=2; $xi<=(count($cabTbProg)*2)+$sumcount; $xi++){
                        $cadena =strtolower($objTbProg[$x][$xi]);
                        $array = explode("_", $cadena);
                        $color=($array[0]=='no')?'color-lock':'color-libre';
                        $icono=($array[0]=='no')?'fa fa-lock ':'fa fa-calendar ';

                        if($array[0]=='o'){
                            if($array[1]!=$O_igual) {
                            $dataop=Operacion::obtenerTodolosDatosPoridOperacion($array[1]);
                            //$dataop = new Operacion();
                            //$dataop->obtenerTodolosDatosPoridOperacion($array[1]);


                            // Crear una instancia de la clase CTBProgramacion ------------------------
                            $ctbProgramacion = new CTBProgramacion();
                            // Llamar al método ObtenertbProgramaxIdOpe() en la instancia creada
                            $dataTP = $ctbProgramacion->ObtenertbProgramaxIdOpe($array[1],$fecha,$array[2]);
                            //$dataTP=CTBProgramacion::ObtenertbProgramaxIdOpe($array[1],$fecha,$array[2]); -------------------

                            $colstti = explode(":", $dataTP->tth_instalacion);
                            $ncols=((!is_null($colstti[0]) ? $colstti[0] * 2 : 0) + ($colstti[1]!='00' ? 1 : 0));
                            $title='';$desc='';

                            if($dataop->idChequeo == 0){
                                $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
                                $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial);
                                $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
                                $title=strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion). "  " . $dataop->nombreConcecionarioInstalicion . "<br>" . $nombreCliente . "<br>" . $datosVehiculo->placa . "<br>" . $datosVehiculo->marca . " " . $datosVehiculo->modelo . "<br><b>" . $datosVehiculo->plan . "</b><br>" . $dataop->departamentoi . " " . $dataop->provinciai . "<br><b>" . $dataop->observacion . "</b>";
                                $desc=$dataop->distritoi." - ".strtoupper($datosVehiculo->plan)."<br/>(".$dataop->numeroOrte.")";
                            }else {
                                $clienteObj = Onix::obtenerDatosDesdeIds($dataop->idcliente, $dataop->idvehiculo, $dataop->idlocation);
                                $nombreCliente = utf8_encode($clienteObj->apellidos) . " " . $clienteObj->nombre;
                                $datosVehiculo = "";
                                $title = strtoupper($dataop->trabajo) . " " . strtoupper($dataop->lugarInstalacion) . "<br>" . $nombreCliente . "<br>" . $clienteObj->placa . "<br>" . $clienteObj->chasis . " " . $clienteObj->marca . " " . $clienteObj->modelo . "<br>" . $dataop->departamentoi . " " . $dataop->provinciai . "<br><b>" . $dataop->observacion . "<b>";
                                $desc = $dataop->distritoi."<br/>(".$dataop->numeroOrte.")";
                            }
                    ?>
                                <td class="<?= obtenerColorDeActividad2($dataop->trabajo,$dataTP->ubicacion) ?> text-center parent_r" style="padding:1.5px;font-weight:bold"
                                    colspan="<?= $ncols ?>">
                                    <p data-toggle="tooltip"  data-html="true" style='padding:0;margin:0;;cursor:<?= ($dataTP->estado=='321')?'default':'pointer' ?>;font-size: 10.5px; '  id="reser"
                                       title="<?= $title ?>"><?= $desc ?>
                                        <?= ($dataTP->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger crepro text-danger child_r" id="del_r" style="cursor: pointer" > </span>':'' ?>

                                        <span class="fa fa-eye child_v" style="cursor: pointer;color:#0c5977" data-toggle="modal" data-target="#programacionModal" data-id="o,<?= $dataTP->idtablaProgramacion ?>"></span>
                                    </p>
                                </td>
                    <?php
                    
                                $O_igual=$array[1];
                                $T_igual='';
                            }
                        }elseif($array[0]=='c'){
                            if($array[1]!=$C_igual) {
                            $datoscita=Cita::obtenerCitaPorId($array[1]);
                            $dataTP=CTBProgramacion::ObtenertbProgramaxIdCita($array[1],$fecha,$array[2]);
                            $colstti = explode(":", $dataTP->tth_instalacion);
                            $ncols=((!is_null($colstti[0]) ? $colstti[0] * 2 : 0) + ($colstti[1]!='00' ? 1 : 0));
                            $title="CITA <br>".strtoupper($datoscita->nombreCliente)."<br>".strtoupper($datoscita->placa)."<br>".strtoupper($datoscita->plan)."<br><b>".$datoscita->observacion."</b>";
                            $desc="SURQUILLO - ".strtoupper($datoscita->plan);
                                ?>
                                <td class="ctaller text-center parent_r" style="padding:1.5px;font-weight:bold;border:1px solid #118066 ;"
                                    colspan="<?= $ncols ?>">
                                    <p data-toggle="tooltip"  data-html="true" style='cursor:pointer;font-size: 11px; ' id="reser"
                                       title="<?= $title ?>"> <?= $desc; ?>
                                        <?= ($dataTP->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger crepro text-danger child_r" id="del_r" style="cursor: pointer" ></span>':'' ?>
                                    </p>
                                </td>
                                <?php $C_igual=$array[1];
                                $T_igual='';
                            }
                        }elseif($array[0]=='r'){
                            if($array[1]!=$R_igual) {

                                // Crear una instancia de la clase CTBProgramacion--------------------------
                                $ctbProgramacion = new CTBProgramacion();
                                // Llamar al método ObtenertbProgramaxIdTablaProgramacion() en la instancia creada
                                $dataTP = $ctbProgramacion->ObtenertbProgramaxIdTablaProgramacion($array[1],$fecha);
                                //$dataTP=CTBProgramacion::ObtenertbProgramaxIdTablaProgramacion($array[1],$fecha);--------------------


                                $colstti = explode(":", $dataTP->tth_instalacion);
                                $ncols=((!is_null($colstti[0]) ? $colstti[0] * 2 : 0) + ($colstti[1]!='00' ? 1 : 0));
                                $title="RESERVADO <br>".strtoupper($dataTP->observacion);
                                $desc=$ncols<=1?'R':'RESERVADO';
                                ?>
                                <td class=" text-center parent_r" style="padding:1.5px;font-weight:bold;border:solid 1px;border-color:#8fc3cc;"
                                    colspan="<?= $ncols ?>">
                                    <p data-toggle="tooltip"  data-html="true" style='cursor: pointer;font-size: 11px; ' id="reser"
                                       title="<?= $title ?>"><?= $desc; ?>
                                    </p>
                                </td>
                                <?php $R_igual=$array[1];
                                $T_igual='';
                            }
                        }elseif($array[0]!='no'){
                            $array2 = explode(",", $array[0]);
                            //$objTTras=$objTablaProgramacion->ObtenertimeTrasladoDisponibilidad($fecha,$array2[1]);
                            if($objTTras){
                                $tn=0;$cols=1;
                                for ($xt=0;$xt<count($objTTras);$xt++){
                                    if((intVal(str_replace(':','',$objTTras[$xt][3]))<=intVal(str_replace(':','',$array2[0].':00')) and
                                            intVal(str_replace(':','',$objTTras[$xt][2]))>=intVal(str_replace(':','',$array2[0].':00'))
                                        ) &&  $objTTras[$xt][0]==$array2[1]){
                                        $cols=1
                                        ?>
                                        <td colspan="<?= $cols ?>" class="text-center"
                                            style="padding:1.5px;background:#2f5099;color: #fff;vertical-align:inherit"><p><?=  $cols==1?'':($cols==2?'Tras.':'Traslado') ?>
                                                <i class="fa fa-automobile" ></i></p></td>
                                        <?php
                                        break;
                                    }
                                    $tn++;
                                }
                                if($tn==count($objTTras)){
                                    ?>
                                    <td class="success text-center <?= $color ?>" style="padding:1.5px; " >
                                        <i class="<?= $icono ?>"></i>
                                    </td>
                                    <?php
                                }
                            }else{
                                ?>
                                <td class="success text-center <?= $color ?>" style="padding:1.5px;" >
                                    <i class="<?= $icono ?>"></i>
                                </td>
                                <?php
                            }
                        }else{
                            ?>
                            <td class="success text-center <?= $color ?>" style="padding:1.5px;" >
                                <i class="<?= $icono ?>"></i>
                            </td>
                            <?php
                        }
                        if($cabTbProg[(($xi-2)/2)][1]==1){
                            echo '<td></td>';
                        }
                    }
                    ?>
                </tr>
            <?php } } ?>
            </tbody>
        </table>
    </div>
</div>
    <style>
        .table-scroll {
            position:relative;
            max-width:1600px;
            margin:auto;
            overflow:hidden;
            /*border:.5px solid #000;*/
            overflow-y: scroll;
            /* height: 500px;*/
        }
        .table-wrap {
            width:100%;
            overflow:auto;
        }
        .table-scroll table {
            width:100%;
            margin:auto;
            border-collapse:separate;
            border-spacing:0;
        }
        .table-scroll th, .table-scroll td {
            padding:5px 10px;
            border-bottom:1px solid;
            border-right:1px solid;
            border-color: #DDDDDD;;
            background:#fff;
            color:rgb(103, 106, 108) ;
            white-space:nowrap;
            vertical-align:top;
            font-size: 12px
        }
        .table-scroll thead, .table-scroll tfoot {
            background:#f9f9f9;
        }
        .clone {
            position:absolute;
            top:0;
            left:0;
            pointer-events:none;
        }
        .clone th, .clone td {
            visibility:hidden
        }
        .clone td, .clone th {
            border-color:transparent
        }
        .clone tbody th {
            visibility:visible;
            color:red;
        }
        .clone .fixed-side {
            border-bottom:1px solid;
            border-right:1px solid;
            border-color: #DDDDDD;;
            background:#ededf9;;
            color: #1C1C1C;
            visibility:visible;
        }
        .clone thead, .clone tfoot{
            background:transparent;
        }

        .table-cabecera-horario{
            background:#075598!important;
            color:white !important;
        }

        .ctaller{
            background: #69cab0 !important;
            color:#124426 !important;
            border-bottom:1px solid #1b4828 !important;
            border-right:1px solid #1b4828 !important;
        }

        .cdomicilio{
            background: #ecb5d9 !important;
            color:#90257a !important;
            border-bottom: 1px solid #143451 !important;
            border-right: 1px solid #143451 !important;
        }

        .cconcesionario{
            background: #7badd2 !important;
            color:#2f3748  !important;
            border-bottom:1px solid #143451 !important;
            border-right:1px solid #143451 !important;
        }

        .ccita{
            background: #f67e7d !important;
            color:#fff  !important;
            border-bottom:1px solid #683f3f  !important;
            border-right:1px solid #683f3f  !important;
        }
        .ccen{
            color:#fff  !important;
        }

        .info{
            background: #ebcccc !important;
            color:#683f3f !important;
            border-bottom:1px solid #683f3f !important;
            border-right:1px solid #683f3f !important;
        }
        .warning{
            background: #fff9de!important;
            color:#5e5c09 !important;
            border-bottom:1px solid #5e5c09 !important;
            border-right:1px solid #5e5c09 !important;
        }
        .danger{
            background: #ebcccc!important;
            color:#683f3f !important;
        }
        .info{
            background: #d6eefb !important;
            color:#3a5982 !important;
        }
        .warning{
            background: #fff9de!important;
            color:#5e5c09 !important;
        }
        .tooltip {
            position: fixed !important;
        }
        .crepro {
            color:#fd001b;
        }

    </style>
    <script>
        jQuery(document).ready(function() {
            jQuery(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');
            $('.child_r').hover(function() { $(this).parent().parent().css('border-color', '#ee5656'); },
                function() {
                   $(this).parent().parent().css('border-color', '#8fc3cc');
                }
            );
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

    </script>
<?php endif ?>


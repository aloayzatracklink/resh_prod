<?php if ($_POST): ?>
<?php
    require_once("../includes/init.programacion.php");
    $fecha=cambiarPorguion($_POST['fecha']);
    $ubicacion=$_POST['cita'];
    $tthinstala=$_POST['tthinstala'];
    $iddist=$_POST['iddist'];
    $lugart=$_POST['lugart'];
    $lugarVer='';$lTec='';
    if(!empty($_POST['clugar'])){
        $lugarVer=$_POST['clugar'];
        if($lugarVer=='ccita'){
            $lTec='1';
        }
    }


    $objTablaProgramacion=new CTBProgramacion();
    $objUbica=$objTablaProgramacion->ObtenertbUbicacion($ubicacion);
    $ubicacion=$objUbica->tt==0?'General':$ubicacion;
    //$distrito='';
    $objTbProg=$objTablaProgramacion->obtenerTablaProgramacion($fecha,$ubicacion,$iddist,1,$tthinstala,$lugart,$lTec);
    $cabTbProg=$objTablaProgramacion->obtenerTablaProgramacion($fecha,$ubicacion,$iddist,2,$tthinstala,$lugart,$lTec);
    $objTTras=$objTablaProgramacion->ObtenertimeTraslado($fecha,$iddist,0,$lugart);

    $fechaMostrar=new DateTime($fecha);
    $sumcount=0;
    $O_igual=0;
    $C_igual=0;
    $CH_igual=0;
    $T_igual=0;
    $R_igual=0;
?>
<h4 class="text-center txt-info" class='margin-top' style='margin-top:0px;margin-bottom:0px'><?= diaPARAMETRO($fechaMostrar->format('l'))." ".$fechaMostrar->format('d')." de ".strtolower(mesDesdeNumero($fechaMostrar->format('m'))) ?> </h4>
<div id="table-scroll" class="table-scroll">
    <div class="table-wrap">
        <table class="main-table" id="tbprograma">
            <thead>
                <tr>
                    <th class="fixed-side table-cabecera-horario" scope="col" >#</th>
                    <th class="text-center fixed-side table-cabecera-horario" scope="col">Técnico</th>
                    <?php for($i=0; $i<count($cabTbProg); $i++){
                        if($cabTbProg[$i][1]==2){
                            $sumcount=1;
                        }else{
                            $sumcount=0;
                        }
                        ?>
                    <th colspan="2" class="text-center table-cabecera-horario" scope="col" ><?php echo $cabTbProg[$i][0] ?></th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
            <?php if($objTbProg){
            for($x=0; $x<count($objTbProg); $x++){
                $T_igual='';
                ?>
                <tr>
                    <td class="fixed-side text-center" ><?= $x+1;?></td>
                    <td class="fixed-side" ><?= str_replace('Pandero','(Pandero)', $objTbProg[$x][1]);?></td>
                    <?php for($xi=2; $xi<=(count($cabTbProg)*2)+$sumcount; $xi++){
                        $cadena =strtolower($objTbProg[$x][$xi]);
                        $array = explode("_", $cadena);
                        if($array[0]=='o'){
                            if($array[1]!=$O_igual) {
                            $dataop=Operacion::obtenerTodolosDatosPoridOperacion($array[1]);
                            $dataTP=CTBProgramacion::ObtenertbProgramaxIdOpe($array[1],$fecha,$array[2]);
                            $colstti = explode(":", $dataTP->tth_instalacion);
                            if($dataop->idChequeo==0){
                                $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
                                $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial);
                                $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
                             ?> <td class="<?= obtenerColorDeActividad($dataop->trabajo) ?> text-center" style="padding:1.5px;font-weight:bold"
                                    colspan="<?= ((!is_null($colstti[0]) ? $colstti[0] * 2 : 0) + ($colstti[1]!='00' ? 1 : 0)) ?>">
                                    <p data-toggle="tooltip"  data-html="true" style='padding:0;margin:0;cursor:pointer;font-size: 10.5px; '
                                       title="<?= strtoupper($dataop->trabajo) . " " . strtoupper($dataop->lugarInstalacion) . "  " . $dataop->nombreConcecionarioInstalicion . "<br>" . $nombreCliente . "<br>" . $datosVehiculo->placa . "<br>" . $datosVehiculo->marca . " " . $datosVehiculo->modelo . "<br><b>" . $datosVehiculo->plan . "</b><br>" . $dataop->departamentoi . " " . $dataop->provinciai . "<br><b>" . $dataop->observacion . "</b>" ?>"><?= $dataop->distritoi." - ".strtoupper($datosVehiculo->plan)."<br/>(".($dataop->numeroOrte).")" ?>
                                        <?= ($dataTP->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?>
                                    </p>
                                </td>
                             <?php
                            }else{
                                $clienteObj=Onix::obtenerDatosDesdeIds($dataop->idcliente,$dataop->idvehiculo,$dataop->idlocation);
                                $apellidos=utf8_encode($clienteObj->apellidos);
                                $nombreCliente=$apellidos." ".$clienteObj->nombre;
                                $datosVehiculo="";
                                ?> <td class="<?= obtenerColorDeActividad($dataop->trabajo) ?> text-center" style="padding:1.5px;font-weight:bold"
                                       colspan="<?= ((!is_null($colstti[0]) ? $colstti[0] * 2 : 0) + ($colstti[1]!='00' ? 1 : 0)) ?>">
                                    <p data-toggle="tooltip"  data-html="true" style='cursor:pointer;font-size: 11px; '
                                       title="<?= strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."<br>".$nombreCliente."<br>".$clienteObj->placa."<br>".$clienteObj->chasis." ".$clienteObj->marca." ".$clienteObj->modelo."<br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."<b>" ?>"><?= $dataop->distritoi ?>
                                        <?= ($dataTP->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?>
                                    </p>
                                </td>
                                <?php

                            }
                                $O_igual=$array[1];
                            $T_igual='';
                            }

                        }elseif($array[0]=='c'){
                            if($array[1]!=$C_igual) {
                            //$dataop=Cita::obtenerTodolosDatosPoridOperacion($array[1]);
                            $datoscita=Cita::obtenerCitaPorId($array[1]);

                            $CTB= new CTBProgramacion();
                            //$dataTP=CTBProgramacion::ObtenertbProgramaxIdCita($array[1],$fecha,$array[2]); //taller-link-r
                            $dataTP=$CTB->ObtenertbProgramaxIdCita($array[1],$fecha,$array[2]);
                            
                            $colstti = explode(":", $dataTP->tth_instalacion);
                                ?>
                                <td class=" text-center parent_r" style="padding:1.5px;font-weight:bold;border:1px solid #118066 ;"
                                    colspan="<?= ((!is_null($colstti[0]) ? $colstti[0] * 2 : 0) + ($colstti[1]!='00' ? 1 : 0)) ?>">
                                    <p data-toggle="tooltip"  data-html="true" style='cursor:<?= ($dataTP->estado=='321')?'default':'pointer' ?>;font-size: 11px; '  id="reser"
                                       title="<?= "CITA <br>".strtoupper($datoscita->nombreCliente)." ".strtoupper($datoscita->placa)." ".strtoupper($datoscita->plan)."<br><b>".$datoscita->observacion."</b>" ?>">SURQUILLO - <?= strtoupper($datoscita->plan); ?>
                                        <?= ($dataTP->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger child_r" id="del_r" style="cursor: pointer" > <input type="text" name="idreserva" value='.$dataTP->idtablaProgramacion.' style="display: none"></span>':'' ?>
                                    </p>
                                </td>
                                <?php $C_igual=$array[1];
                                $T_igual='';
                            }
                        }elseif($array[0]=='r'){
                            $CTB= new CTBProgramacion();
                            //$dataTP = CTBProgramacion::ObtenertbProgramaxIdTablaProgramacion($array[1], $fecha); //taller-link-r 
                            $dataTP = $CTB->ObtenertbProgramaxIdTablaProgramacion($array[1], $fecha);

                            $colstti = explode(":", $dataTP->tth_instalacion);
                            if(count($objTbProg)==1){
                                $ncols = 1;
                               ?>
                                <td class=" text-center"
                                    style="padding:1.5px;font-weight:bold;border:solid 1px;border-color:#8fc3cc;"
                                    >
                                    <p data-toggle="tooltip" data-html="true"
                                       style='cursor: pointer;font-size: 11px; '
                                       title="RESERVADO <?= "<br>" . strtoupper($dataTP->observacion) ?>"><?= $ncols <= 1 ? 'Reserv.' : 'RESERVADO'; ?></p>
                                </td>
                    <?php
                            }else {
                                if ($array[1] != $R_igual) {
                                    $ncols = ((!is_null($colstti[0]) ? $colstti[0] * 2 : 0) + ($colstti[1] != '00' ? 1 : 0));
                                    ?>
                                    <td class=" text-center"
                                        style="padding:1.5px;font-weight:bold;border:solid 1px;border-color:#8fc3cc;"
                                        colspan="<?= $ncols ?>">
                                        <p data-toggle="tooltip" data-html="true"
                                           style='cursor: pointer;font-size: 11px; '
                                           title="RESERVADO <?= "<br>" . strtoupper($dataTP->observacion) ?>"><?= $ncols <= 1 ? 'R' : 'RESERVADO'; ?></p>
                                    </td>
                                    <?php $R_igual = $array[1];
                                    $T_igual = '';
                                }
                            }
                        }elseif($array[0]!='no'){
                            $array2 = explode(",", $array[0]);
                            //echo $array[0]." acccccccaaaaaaaa"  ;
                           // $dataop=Operacion::obtenerTodolosDatosPoridOperacion($array[1]);
                           // $dataTP=CTBProgramacion::ObtenertbProgramaxIdOpe($array[1]);
                           // $colsttt = explode(":", $dataTP->tth_trasalado);
                          //  $objTTras=$objTablaProgramacion->ObtenertimeTraslado($fecha,$iddist,$array2[1],$lugart);
                            if($objTTras){
                                $tn=0;$cols=1;
                                for ($xt=0;$xt<count($objTTras);$xt++){

                                    if((intVal(str_replace(':','',$objTTras[$xt][3]))<=intVal(str_replace(':','',$array2[0].':00')) and
                                        intVal(str_replace(':','',$objTTras[$xt][4]))>=intVal(str_replace(':','',$array2[0].':00'))
                                    )&&  $objTTras[$xt][0]==$array2[1]){
                                      // if(intVal(str_replace(':','',$array2[0]))<intVal(str_replace(':','',$objTTras[$xt][1]))){
                                       // if($objTTras[$xt][1]!=$T_igual) {
                                       //    $colsttt= explode(":", $objTTras[$xt][5]);
                                       //    $cols=((!is_null($colsttt[0]) ? $colsttt[0] * 2 : 0) + ($colsttt[1]!='00' ? 1 : 0));
                                        $cols=1
                                      // }
                                            //$cols=((!is_null($colsttt[0]) ? $colsttt[0] * 2 : 0) + ($colsttt[1]!='00' ? 1 : 0));
                                            ?>
                                            <td colspan="<?= $cols ?>" class="text-center"
                                                style="padding:1.5px;background:#2f5099;color: #fff;vertical-align:inherit"><p><?=  $cols==1?'':($cols==2?'Tras.':'Traslado') ?>
                                                    <i class="fa fa-automobile"></i></p></td>
                                            <?php
                                        //$T_igual = $objTTras[$xt][1];
                                     //   }
                                        break;
                                    }
                                    $tn++;
                                }

                                if($tn==count($objTTras)){
                                    ?>
                                    <td class="success" style="padding:1.5px" >
                                        <button class="btn btn-primary <?php if($array[0]=='no'){ echo 'fa fa-lock disabled';}else{ if($array[0]=='noo'){echo 'fa fa-check disabled'; }else{ echo 'fa fa-check  SeleccionarHorarioo';}} ?> btn-block" type='button' value='<?= $objTbProg[$x][$xi]?>'> </button>
                                    </td>
                                    <?php
                                }
                            }else{
                                ?>
                                <td class="success" style="padding:1.5px" >
                                    <button class="btn btn-primary <?php if($array[0]=='no'){ echo 'fa fa-lock disabled';}else{ if($array[0]=='noo'){echo 'fa fa-check disabled'; }else{ echo 'fa fa-check  SeleccionarHorarioo';}} ?> btn-block" type='button' value='<?= $objTbProg[$x][$xi]?>'> </button>
                                </td>
                                <?php
                            }
                        /*}elseif($array[0]=='r'){*/
                        }else{
                        ?>
                        <td class="success" style="padding:1.5px" >
                            <button class="btn btn-primary <?php if($array[0]=='no'){ echo 'fa fa-lock disabled';}else{ if($array[0]=='noo'){echo 'fa fa-check disabled'; }else{ echo 'fa fa-check  SeleccionarHorarioo';}} ?> btn-block" type='button' value='<?= $objTbProg[$x][$xi]?>'> </button>
                        </td>
                    <?php
                        }
                        if($cabTbProg[(($xi-2)/2)][1]==1){
                            echo '<td></td>';
                        }
                    }
                    ?>
                </tr>
            <?php } } ?>
            </tbody>
        </table>
    </div>
</div>
    <style>

        .table-scroll {
            position:relative;
            max-width:1600px;
            margin:auto;
            overflow:hidden;
            /*border:.5px solid #000;*/
        }
        .table-wrap {
            width:100%;
            overflow:auto;
        }
        .table-scroll table {
            width:100%;
            margin:auto;
            border-collapse:separate;
            border-spacing:0;
        }
        .table-scroll th, .table-scroll td {
            padding:5px 10px;
            border-bottom:1px solid;
            border-right:1px solid;
            border-color: #DDDDDD;;
            background:#fff;
            color:rgb(103, 106, 108) ;
            white-space:nowrap;
            vertical-align:top;
            font-size: 12px
        }
        .table-scroll thead, .table-scroll tfoot {
            background:#f9f9f9;
        }
        .clone {
            position:absolute;
            top:0;
            left:0;
            pointer-events:none;
        }
        .clone th, .clone td {
            visibility:hidden
        }
        .clone td, .clone th {
            border-color:transparent
        }
        .clone tbody th {
            visibility:visible;
            color:red;
        }
        .clone .fixed-side {
            border-bottom:1px solid;
            border-right:1px solid;
            border-color: #DDDDDD;;
            background:#ededf9;;
            color: #1C1C1C;
            visibility:visible;
        }
        .clone thead, .clone tfoot{
            background:transparent;
        }

        .table-cabecera-horario{
            background:#075598!important;
            color:white !important;
        }
        .danger{
            background: #ebcccc!important;
            color:#683f3f !important;
        }
        .info{
            background: #d6eefb !important;
            color:#3a5982 !important;
        }
        .warning{
            background: #fff9de!important;
            color:#5e5c09 !important;
        }
        .tooltip {
            position: fixed !important;
        }
    </style>
    <script>
        jQuery(document).ready(function() {
            jQuery(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');
        });


        function mostrarHora(string){
            var horaYcordenadas=string.split(",");
            return horaYcordenadas[0];
        }
        function posicion(string){
            var horaYcordenadas=string.split(",");
            return horaYcordenadas[1];
        }
        function deleteR(string){
            var horaYcordenadas=string.split(",");
            var n=horaYcordenadas[2]==undefined?'0':horaYcordenadas[2];
            return n;
        }
        $(document).ready(function(){
            $(".SeleccionarHorarioo").click(function() {
                var ubicacion=$(this).val();
                $('.hora').val(posicion(ubicacion));
                $('.delR').val(deleteR(ubicacion));
                var hora=mostrarHora(ubicacion);
                $('.horaVer').val(hora);
                $('.focusUrgenteta').focus();
                $('.horabtn').html(`Cambiar`);
                $('.horabtnm').html(`Cambiar`);
                $('#programacionHtml').html('');
                $('#programacionHtmlm').html('');
                //alert(ubicacion);
            });
            $('.horabtn').html(`Actualizar`);
            $('.horabtnm').html(`Actualizar`);

        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        $(".SeleccionarHorarioo").hover(function(){
            var canthorap=$("#canthoraprog").val()*2;
            canthorap=(canthorap==0?(($("#canthoraprogR").val().trim())*2):canthorap);
            var ntval="",concat="",hidecol="",ntval2="",concat2="";
                for (var i = 0; i < canthorap; i++) {
                    ntval+=eval("$(this).closest('td')"+concat+".find('button').val()")+" ";
                    concat+=".next('td')";
                }
               ntval=ntval.split(" ");
//                console.log(ntval);
                //var ntval=$(this).closest('td').next('td').find('button').val();
              //  console.log(ntval);
                if (ntval.includes("undefined") || ntval.includes("no")) {
                    $(this).attr('disabled','disabled');
                }else{
                   // console.log($(this));
                    $(this).parent().prop('colspan', canthorap);
                    for (var i = 0; i < (canthorap-1); i++) {
                        concat2+=".next('td')";
                        ntval2+=eval("$(this).closest('td')"+concat2+".hide()")+" ";
                    }
                  //  $(this).closest('td').next('td').hide()
                }
            },function(){
            var canthorap=$("#canthoraprog").val()*2;
            canthorap=(canthorap==0?(($("#canthoraprogR").val().trim())*2):canthorap);
                var ntval="",concat="",hidecol="",ntval2="",concat2="";
                for (var i = 0; i < canthorap; i++) {
                    ntval+=eval("$(this).closest('td')"+concat+".find('button').val()")+" ";
                    concat+=".next('td')";
                }
                ntval=ntval.split(" ");
                //var ntval=$(this).closest('td').next('td').find('button').val();
                if (ntval.includes("undefined") || ntval.includes("no")) {
                    $(this).attr('disabled','disabled');
                }else{
                    $(this).parent().prop('colspan','0');
                    for (var i = 0; i < canthorap-1; i++) {
                        concat2+=".next('td')";
                        ntval2+=eval("$(this).closest('td')"+concat2+".show()")+" ";
                    }
                    //$(this).closest('td').next('td').show();
                }
            }
        );

        $(document).ready(function(){
            $("#tbprograma tbody tr").each(function (index) {
                $(this).children("td").children("button").each(function (index2) {
                //console.log($(this));
                    var canthorap=$("#canthoraprog").val()*2;
                    canthorap=(canthorap==0?(($("#canthoraprogR").val().trim())*2):canthorap);
                    var ntval="",concat="",hidecol="",ntval2="",concat2="";
                    for (var i = 0; i < canthorap; i++) {
                        ntval+=eval("$(this).closest('td')"+concat+".find('button').val()")+" ";
                        concat+=".next('td')";
                    }

                    ntval=ntval.split(" ");
                    if (ntval.includes("undefined") || ntval.includes("no")) {
                        $(this).attr('disabled','disabled');
                    }
                });

                /*                $(this).children("td").children("p").each(function (index2) {
                   console.log($(this));
                });*/
            });
        });

    </script>
<?php endif ?>


<?php if ($_POST): ?>
<?php 
//var_dump($_POST);
    require_once("../includes/init.programacion.php");
    $obMenu=Usuario::obtenerMenu($session->idusuario);
    $cita=false;
    $domicilio=false;
    $idoperacion=0;
    if (isset($_POST['cita']) AND $_POST['cita']=='si') {
       $cita=true; 
    }
    if (isset($_POST['cita']) AND $_POST['cita']=='dom') {
        $domicilio=true;
    }
    if (isset($_POST['idoperacion'])) {
       $idoperacion=$_POST['idoperacion'];
    }
    $fecha=cambiarPorguion($_POST['fecha']);
    $programObj=new tablaProgramacion;
    $opeObjdetalle=new Operacion;
    $n=1;
    $soloUno=true;
    $hoy=false;
    $sabado=false;
    $ver=false;
    $limite=false;
    if (isset($_POST['ver'])) {
       $ver=true; 
    }
    if (isset($_POST['limite'])) {
        if ($_POST['limite']=='') {
           die('<h1 class="text-danger">Selecciona tipo de trabajo</h1>');
        }else{
            $trabajo=$_POST['limite'];
            if ($_POST['limite']=='Desinstalacion') {
                $limite=Operacion::obtenerLimite($fecha,$trabajo);
                if ($limite<=$opeObjdetalle->obtenerOperacionesTotalLimitesDes(soloNumero($fecha))) {
                    die('<h1 class="text-danger">No se puede programar mas de '.$limite.'  para el dia '.$fecha.' </h1>');
                }else{

                }
            }else if($_POST['limite']=='Cita'){
                $limite=Operacion::obtenerLimite($fecha,'Citas');
                if ($limite>$opeObjdetalle->obtenerCitasPordia(soloNumero($fecha))) {

                }else{
                    die('<h1 class="text-danger">No se puede programar mas de '.$limite.' citas para el dia '.$fecha.' </h1>');
                }
            }else{
                $limite=Operacion::obtenerLimite($fecha,"Otro");
                if ($limite>$opeObjdetalle->obtenerOperacionesTotalLimitesOTRO(soloNumero($fecha))) {

                }else{
                    die('<h1 class="text-danger">No se puede programar mas de '.$limite.' '.$trabajo.' para el dia '.$fecha.' </h1>');
                }
            }
        
        }
    }

    if (!$cita) {
        if ($ver) {
            //$datosT=$programObj->obteneTurnosDisponiblesTotal();
            $datosT=$programObj->obteneTurnosDisponibles();
        }else{
            $datosT=$programObj->obteneTurnosDisponibles();
        }
    }else{
        $datosT=$programObj->obteneTurnosDisponiblesTaller();
    }

    if ($fecha==date('Y-m-d')) {
        $hoy=true;
        $hora=new DateTime();
        $anticipacion=Operacion::obtenerLimite($fecha,"Hora");
        $hora->modify($anticipacion.' hour');
        $hora=$hora->format('Hi');
    }
    $horaParaManana=new DateTime();
    $horaParaManana->modify('+ 1 day');
    if ($horaParaManana->format('Y-m-d')==$fecha) {
        if (date('Hm')>='1700') {
            $hora='11:01';
            //$hora='08:00';
        }else{
            $hora='08:00';
        }
        $hoy=true;
    }

    $esSabado=new DateTime($fecha);
    $dia=$esSabado->format('D');
    if ($dia=='Sat') {
        $sabado=true;
        $t1='9';
        $t2='11';
        $t3='13';
        $t4='15';
        $tf='17';
        $tt1='09:00';
        $tt2='11:00';
        $ttp='13:00';
        $tt3='13:00';
        $tt4='15:00';
        $ttf='17:00';
    }else{
        $t1='9';
        $t2='11';
        $t3='14';
        $t4='16';
        $tf='18';
        $tt1='09:00';
        $tt2='11:00';
        $ttp='13:00';
        $tt3='14:00';
        $tt4='16:00';
        $ttf='18:00';
    }

if ($datosT) {
       ?>
<style>table.table-bordered{
    border:1px solid #bec8d8;
    margin-top:20px;
  }
table.table-bordered > thead > tr > th{
    border:1px solid #bec8d8;
}
table.table-bordered > tbody > tr > td{
    border:1px solid #bec8d8;
}</style>
<?php
    $fechaMostrar=new DateTime($fecha);
?>
<h4 class="text-center txt-info" class='margin-top' style='margin-top:0px;margin-bottom:0px'><?= diaPARAMETRO($fechaMostrar->format('l'))." ".$fechaMostrar->format('d')." de ".strtolower(mesDesdeNumero($fechaMostrar->format('m'))) ?> </h4>
  <table class="table table-bordered table-hover " style='margin-top:6px;' >
  <tbody>
  <?php if($ver){
        $limiteCita=Operacion::obtenerLimite($fecha,'Citas');
        $citas=$opeObjdetalle->obtenerCitasPordia(soloNumero($fecha));

        $limiteDes=Operacion::obtenerLimite($fecha,'Desinstalacion');
        $des=$opeObjdetalle->obtenerOperacionesTotalLimitesDes(soloNumero($fecha));
        if(empty($des)){
            $des=0;
        }

        $limite=Operacion::obtenerLimite($fecha,"Otro");
        $otros=$opeObjdetalle->obtenerOperacionesTotalLimitesOTRO(soloNumero($fecha));
        if($otros==''){
            $otros=0;
        }
  ?>
  <tr>
      <th colspan="2">Citas Taller (<?= $citas ?>/<?= $limiteCita ?>)</th>
      <th colspan="2" class='info'>Desinstalaciones (<?= $des ?>/<?= $limiteDes ?>)</th>  
      <th class='warning'>Otros (<?= $otros ?>/<?= $limite ?>)</th>  
    </tr>
    <tr>
      <th colspan="4"></th>
    </tr>
  <?php
  }
  ?>
    <tr>
      <th></th>
      <th><?= $tt1 ?> - <?= $tt2 ?></th>
      <th><?= $tt2 ?> - <?= $ttp ?></th>  
      <th><?= $tt3 ?> - <?= $tt4 ?></th>  
      <th><?= $tt4 ?> - <?= $ttf ?></th>  
    </tr>
<?php
foreach($datosT as $var){
    $programObj->fecha=$fecha;
    $programObj->hora=$tt1;
    $programObj->idturno=$var->idturno;
    $datot1=$programObj->obtenerTrabajosParaLafechayHoraYespacio();
    $programObj->fecha=$fecha;
    $programObj->hora=$tt2;
    $programObj->idturno=$var->idturno;
    $datot2=$programObj->obtenerTrabajosParaLafechayHoraYespacio();
    $programObj->fecha=$fecha;
    $programObj->hora=$tt3;
    $programObj->idturno=$var->idturno;
    $datot3=$programObj->obtenerTrabajosParaLafechayHoraYespacio();
    $programObj->fecha=$fecha;
    $programObj->hora=$tt4;
    $programObj->idturno=$var->idturno;
    $datot4=$programObj->obtenerTrabajosParaLafechayHoraYespacio();
    $n2=false;
    $n3=false;
    $n4=false;
    if ($datot1 and $datot2) {
        if ($datot1->idoperacion!='0' AND $datot2->idoperacion!='0') {
            if ($datot1->idoperacion==$datot2->idoperacion){
                $n2=true;
            }
        }
    }
    if ($datot2 and $datot3) {
        if ($datot2->idoperacion!='0' AND $datot3->idoperacion!='0') {
            if ($datot2->idoperacion==$datot3->idoperacion){
                $n3=true;
            }
        }
    }
    if ($datot3 and $datot4) {
        if ($datot3->idoperacion!='0' AND $datot4->idoperacion!='0') {
            if ($datot3->idoperacion==$datot4->idoperacion){
                $n4=true;
            }
        }
    }
?>
<tr>

    <td><?= $n++." ".$var->sede ?></td>
<!-- 09:00 -->
<?php 
if (!$n2) {
    if ($datot1) {
        if ($datot1->idoperacion<>'0') {
    $dataop=Operacion::obtenerTodolosDatosPoridOperacion($datot1->idoperacion);
    $lugarInstalacion=$dataop->lugarInstalacion;
    $dataop->nombreConcecionarioInstalicion;
    if ($dataop->idChequeo == 0){ 
        $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
        $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial); 
        $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."  ".$dataop->nombreConcecionarioInstalicion."<br>".$nombreCliente."<br>".$datosVehiculo->placa."<br>".$datosVehiculo->chasis." ".$datosVehiculo->marca." ".$datosVehiculo->modelo."<br><b>".$datosVehiculo->plan."</b><br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."</b>" ?>"><?= $dataop->distritoi ?><?= ($datot1->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
</td>           
        <?php
    }else{ 
        $clienteObj=Onix::obtenerDatosDesdeIds($dataop->idcliente,$dataop->idvehiculo,$dataop->idlocation);
        $apellidos=utf8_encode($clienteObj->apellidos);
        $nombreCliente=$apellidos." ".$clienteObj->nombre;
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."<br>".$nombreCliente."<br>".$clienteObj->placa."<br>".$clienteObj->chasis." ".$clienteObj->marca." ".$clienteObj->modelo."<br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."<b>" ?>"><?= $dataop->distritoi ?> <?= ($datot1->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
</td>           
        <?php
            }
        }else if($datot1->idcita<>'0'){
    $datoscita=Cita::obtenerCitaPorId($datot1->idcita);
        ?>
<td class="primary text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "CITA <br>".strtoupper($datoscita->nombreCliente)." ".strtoupper($datoscita->placa)." ".strtoupper($datoscita->plan)."<br><b>".$datoscita->observacion."</b>" ?>">SURQUILLO <?= ($datot1->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?> </p>
</td>      
    <?php
        }else{
            if ($session->idusuario==$datot1->rpara) {
                ?>
<td class="success ">
<button class="btn btn-primary <?= ($var->fijo=='si')?'x':'SeleccionarHorario' ?> SeleccionarHorarioo fa fa-check  btn-block" type='button' value='<?= $t1.",".$var->idturno ?>' > <?= $datot1->observacion ?></button>
</td>
                <?php
                
            }else{
                ?>
 <td class="primary  text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "RESERVADO <br>".strtoupper($datot1->observacion) ?>">RESERVADO</p>
</td>                   
                <?php
            }
        }
    ?>
        <?php
    }else{
        ?>
<?php
    if (!$ver) {
       ?>
<td class="success ">
    <button class="btn btn-primary <?= ($var->fijo=='si')?'x':'SeleccionarHorario' ?> SeleccionarHorarioo fa fa-check-circle  btn-block" type='button' value='<?= $t1.",".$var->idturno ?>' <?= ($hoy AND $hora > soloNumero($tt1)) ? 'disabled':'' ?>></button>
</td>
       <?php 
    }else{
        ?>
<td class="success text-center">
    <?= ($hoy AND $hora > soloNumero($tt1)) ? '<span class="fa fa-exclamation-circle fa-warning" style="color:orange"></span>':'' ?>
</td>
        <?php
    }
?>
        <?php
    }
}else{
        if ($datot1->idoperacion<>'0') {
        $dataop=Operacion::obtenerTodolosDatosPoridOperacion($datot1->idoperacion);
        $direccionInstacion=$dataop->direccionInstacion;
    if ($dataop->idChequeo == 0){ 
        $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
        $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial); 
        $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
        ?>
 <td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center" colspan='2'>
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."  ".$dataop->nombreConcecionarioInstalicion."<br>".$nombreCliente."<br>".$datosVehiculo->placa."<br>".$datosVehiculo->chasis." ".$datosVehiculo->marca." ".$datosVehiculo->modelo."<br><b>".$datosVehiculo->plan."</b><br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."</b>" ?>"><?= $dataop->distritoi ?> <?= ($datot1->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?> </p>
</td>           
        <?php
    }else{ 
        $clienteObj=Onix::obtenerDatosDesdeIds($dataop->idcliente,$dataop->idvehiculo,$dataop->idlocation);
        $apellidos=utf8_encode($clienteObj->apellidos);
        $nombreCliente=$apellidos." ".$clienteObj->nombre;
    ?>
 <td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center" colspan='2'>
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."<br>".$nombreCliente."<br>".$clienteObj->placa."<br>".$clienteObj->chasis." ".$clienteObj->marca." ".$clienteObj->modelo."<br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."<b>" ?>"><?= $dataop->distritoi ?>  <?= ($datot1->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?> </p>
</td>
        <?php
    }
    }else if($datot1->idcita<>'0'){
    }else{
        ?>
<td class="primary text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "RESERVADO <br>".strtoupper($datot1->observacion) ?>">RESERVADO</p>
</td>
        <?php
    }
    ?>
        <?php
}
?>
<!-- 11:13 -->
<?php 
if (!$n2) {
    if (!$n3) {
    if ($datot2) {
        if ($datot2->idoperacion<>'0') {
        $dataop=Operacion::obtenerTodolosDatosPoridOperacion($datot2->idoperacion);
        $direccionInstacion=$dataop->direccionInstacion;
    if ($dataop->idChequeo == 0){ 
        $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
        $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial); 
        $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."  ".$dataop->nombreConcecionarioInstalicion."<br>".$nombreCliente."<br>".$datosVehiculo->placa."<br>".$datosVehiculo->chasis." ".$datosVehiculo->marca." ".$datosVehiculo->modelo."<br><b>".$datosVehiculo->plan."</b><br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."</b>" ?>"><?= $dataop->distritoi ?> <?= ($datot2->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?> </p>
</td>
        <?php
    }else{ 
        $clienteObj=Onix::obtenerDatosDesdeIds($dataop->idcliente,$dataop->idvehiculo,$dataop->idlocation);
        $apellidos=utf8_encode($clienteObj->apellidos);
        $nombreCliente=$apellidos." ".$clienteObj->nombre;
?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."<br>".$nombreCliente."<br>".$clienteObj->placa."<br>".$clienteObj->chasis." ".$clienteObj->marca." ".$clienteObj->modelo."<br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."<b>" ?>"><?= $dataop->distritoi ?> <?= ($datot2->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
</td>
<?php
    }
        }else if($datot2->idcita<>'0'){
    $datoscita=Cita::obtenerCitaPorId($datot2->idcita);
            ?>
<td class="primary text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "CITA <br>".strtoupper($datoscita->nombreCliente)." ".strtoupper($datoscita->placa)." ".strtoupper($datoscita->plan)."<br><b>".$datoscita->observacion."</b>" ?>">SURQUILLO <?= ($datot2->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
</td>           
            <?php
        }else{
            if ($session->idusuario==$datot2->rpara) {
                ?>
<td class="success ">
<button class="btn btn-primary <?= ($var->fijo=='si')?'x':'SeleccionarHorario' ?> SeleccionarHorarioo fa fa-check  btn-block" type='button' value='<?= $t2.",".$var->idturno ?>' > <?= $datot2->observacion ?></button>
</td>
                <?php
                
            }else{
                ?>
 <td class="primary  text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "RESERVADO <br>".strtoupper($datot1->observacion) ?>">RESERVADO</p>
</td>                   
                <?php
            }
        }
    }else{
        ?>
<?php
    if (!$ver) {
       ?>
<td class="success text-center" ><button class="btn btn-primary <?= ($var->fijo=='si')?'x':'SeleccionarHorario' ?> SeleccionarHorarioo fa fa-check-circle  btn-block" type='button' value='<?= $t2.",".$var->idturno ?>' <?= ($hoy AND $hora > soloNumero($tt2)) ? 'disabled':'' ?>></button></td>
       <?php 
    }else{
        ?>
<td class="success text-center">
    <?= ($hoy AND $hora > soloNumero($tt2)) ? '<span class="fa fa-exclamation-circle fa-warning" style="color:orange"></span>':'' ?>
</td>
        <?php
    }
?>
        <?php
    }
    }else{
        if ($datot2->idoperacion<>'0') {
        $dataop=Operacion::obtenerTodolosDatosPoridOperacion($datot2->idoperacion);
        $direccionInstacion=$dataop->direccionInstacion;
    if ($dataop->idChequeo == 0){ 
        $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
        $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial); 
        $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center" colspan='2'>
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."  ".$dataop->nombreConcecionarioInstalicion."<br>".$nombreCliente."<br>".$datosVehiculo->placa."<br>".$datosVehiculo->chasis." ".$datosVehiculo->marca." ".$datosVehiculo->modelo."<br><b>".$datosVehiculo->plan."</b><br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."</b>" ?>"><?= $dataop->distritoi ?> <?= ($datot2->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?> </p>
</td>
        <?php
    }else{ 
        $clienteObj=Onix::obtenerDatosDesdeIds($dataop->idcliente,$dataop->idvehiculo,$dataop->idlocation);
        $apellidos=utf8_encode($clienteObj->apellidos);
        $nombreCliente=$apellidos." ".$clienteObj->nombre;
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center" colspan='2'>
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."<br>".$nombreCliente."<br>".$clienteObj->placa."<br>".$clienteObj->chasis." ".$clienteObj->marca." ".$clienteObj->modelo."<br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."<b>" ?>"><?= $dataop->distritoi ?>  <?= ($datot2->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
</td>
        <?php
    }
        }else if($datot2->idcita<>'0'){
        }else{
            ?>
<td class="primary text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "RESERVADO <br>".strtoupper($datot2->observacion) ?>">RESERVADO</p>
</td> 
            <?php
        }
    ?>
        <?php
}
}
        ?>
<!-- 14:16 -->
<?php 
if (!$n3) {
    if (!$n4) {
    if ($datot3) {
        if ($datot3->idoperacion<>'0') {
        $dataop=Operacion::obtenerTodolosDatosPoridOperacion($datot3->idoperacion);
       $direccionInstacion=$dataop->direccionInstacion;
    if ($dataop->idChequeo == 0){ 
        $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
        $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial); 
        $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."  ".$dataop->nombreConcecionarioInstalicion."<br>".$nombreCliente."<br>".$datosVehiculo->placa."<br>".$datosVehiculo->chasis." ".$datosVehiculo->marca." ".$datosVehiculo->modelo."<br><b>".$datosVehiculo->plan."</b><br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."</b>" ?>"><?= $dataop->distritoi ?> <?= ($datot3->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
</td>           
        <?php
    }else{ 
        $clienteObj=Onix::obtenerDatosDesdeIds($dataop->idcliente,$dataop->idvehiculo,$dataop->idlocation);
        $apellidos=utf8_encode($clienteObj->apellidos);
        $nombreCliente=$apellidos." ".$clienteObj->nombre;
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."<br>".$nombreCliente."<br>".$clienteObj->placa."<br>".$clienteObj->chasis." ".$clienteObj->marca." ".$clienteObj->modelo."<br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."<b>" ?>"><?= $dataop->distritoi ?> <?= ($datot3->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
</td>           
        <?php
    }
        }else if($datot3->idcita<>'0'){
            $datoscita=Cita::obtenerCitaPorId($datot3->idcita);
        ?>
<td class="primary text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "CITA <br>".strtoupper($datoscita->nombreCliente)." ".strtoupper($datoscita->placa)." ".strtoupper($datoscita->plan)."<br><b>".$datoscita->observacion."</b>" ?>">SURQUILLO <?= ($datot3->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
</td>      
    <?php
        }else{
            if ($session->idusuario==$datot3->rpara) {
                ?>
<td class="success ">
<button class="btn btn-primary <?= ($var->fijo=='si')?'x':'SeleccionarHorario' ?> SeleccionarHorarioo fa fa-check  btn-block" type='button' value='<?= $t3.",".$var->idturno ?>' > <?= $datot3->observacion ?></button>
</td>
                <?php
                
            }else{
                ?>
 <td class="primary  text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "RESERVADO <br>".strtoupper($datot1->observacion) ?>">RESERVADO</p>
</td>                   
                <?php
            }
        }
    }else{
        ?>
<?php
    if (!$ver) {
       ?>
<td class="success text-center" ><button class="btn btn-primary <?= ($var->fijo=='si')?'':'SeleccionarHorario' ?> SeleccionarHorarioo fa fa-check-circle  btn-block"  type='button' value='<?= $t3.",".$var->idturno ?>' <?= ($hoy AND $hora > soloNumero($tt3)) ? 'disabled':'' ?>></button></td>
       <?php 
    }else{
        ?>
<td class="success text-center">
    <?= ($hoy AND $hora > soloNumero($tt3)) ? '<span class="fa fa-exclamation-circle fa-warning" style="color:orange"></span>':'' ?>
</td>
        <?php
    }
?>
        <?php
    }
    }else{
        if ($datot3->idoperacion<>'0') {
        $dataop=Operacion::obtenerTodolosDatosPoridOperacion($datot3->idoperacion);
        $direccionInstacion=$dataop->direccionInstacion;
    if ($dataop->idChequeo == 0){ 
        $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
        $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial); 
        $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
        ?>
 <td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center" colspan='2'>
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."  ".$dataop->nombreConcecionarioInstalicion."<br>".$nombreCliente."<br>".$datosVehiculo->placa."<br>".$datosVehiculo->chasis." ".$datosVehiculo->marca." ".$datosVehiculo->modelo."<br><b>".$datosVehiculo->plan."</b><br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."</b>" ?>"><?= $dataop->distritoi ?>  <?= ($datot3->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
</td>           
        <?php
    }else{ 
        $clienteObj=Onix::obtenerDatosDesdeIds($dataop->idcliente,$dataop->idvehiculo,$dataop->idlocation);
        $apellidos=utf8_encode($clienteObj->apellidos);
        $nombreCliente=$apellidos." ".$clienteObj->nombre;
    ?>
 <td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center" colspan='2'>
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."<br>".$nombreCliente."<br>".$clienteObj->placa."<br>".$clienteObj->chasis." ".$clienteObj->marca." ".$clienteObj->modelo."<br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."<b>" ?>"><?= $dataop->distritoi ?> <?= ($datot3->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
</td>
        <?php
    }
        }else if($datot3->idcita<>'0'){
        }else{
            ?>
<td class="primary text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "RESERVADO <br>".strtoupper($datot3->observacion) ?>">RESERVADO</p>
</td>
            <?php
        }
}
}
        ?>
<!-- 16:18 -->
<?php 
if (!$n4) {
if ($datot4) {
        if ($datot4->idoperacion<>'0') {
        $dataop=Operacion::obtenerTodolosDatosPoridOperacion($datot4->idoperacion);
        $direccionInstacion=$dataop->direccionInstacion;
    if ($dataop->idChequeo == 0){ 
        $clienteObj=Cliente::obtenerDatosDeclientePorId($dataop->idcliente);
        $nombreCliente=($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial); 
        $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."  ".$dataop->nombreConcecionarioInstalicion."<br>".$nombreCliente."<br>".$datosVehiculo->placa."<br>".$datosVehiculo->chasis." ".$datosVehiculo->marca." ".$datosVehiculo->modelo."<br><b>".$datosVehiculo->plan."</b><br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."</b>" ?>"><?= $dataop->distritoi ?> <?= ($datot4->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
</td>           
        <?php
    }else{ 
        $clienteObj=Onix::obtenerDatosDesdeIds($dataop->idcliente,$dataop->idvehiculo,$dataop->idlocation);
        $apellidos=utf8_encode($clienteObj->apellidos);
        $nombreCliente=$apellidos." ".$clienteObj->nombre;
        ?>
<td class="<?= obtenerColorDeActividad($dataop->trabajo)  ?> text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= strtoupper($dataop->trabajo)." ".strtoupper($dataop->lugarInstalacion)."<br>".$nombreCliente."<br>".$clienteObj->placa."<br>".$clienteObj->chasis." ".$clienteObj->marca." ".$clienteObj->modelo."<br>".$dataop->departamentoi." ".$dataop->provinciai."<br><b>".$dataop->observacion."<b>" ?>"><?= $dataop->distritoi ?> <?= ($datot4->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
</td>           
        <?php
    }
        }else if($datot4->idcita<>'0'){
            $datoscita=Cita::obtenerCitaPorId($datot4->idcita);
        ?>
<td class="primary text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "CITA <br>".strtoupper($datoscita->nombreCliente)." ".strtoupper($datoscita->placa)." ".strtoupper($datoscita->plan)."<br><b>".$datoscita->observacion."</b>" ?>">SURQUILLO <?= ($datot4->estado=='321')?'<span class="fa fa-exclamation-circle fa-warning text-danger text-danger" ></span>':'' ?></p>
</td>      
    <?php
        }else{
            if ($session->idusuario==$datot4->rpara) {
                ?>
<td class="success ">
<button class="btn btn-primary <?= ($var->fijo=='si')?'x':'SeleccionarHorario' ?> SeleccionarHorarioo fa fa-check  btn-block" type='button' value='<?= $t4.",".$var->idturno ?>' > <?= $datot4->observacion ?></button>
</td>
                <?php
                
            }else{
                ?>
 <td class="primary  text-center">
    <p data-toggle="tooltip" data-html="true"  style='cursor: pointer;' title="<?= "RESERVADO <br>".strtoupper($datot1->observacion) ?>">RESERVADO</p>
</td>                   
                <?php
            }
        }
}else{
    if (!$ver) {
       ?>
<td class="success text-center last"><button class="btn btn-primary fa fa-check-circle SeleccionarHorarioo btn-block" <?= ($domicilio AND $var->fijo<>'si')?'disabled':''?> type='button' value='<?= $t4.",".$var->idturno ?>' <?= ($hoy AND $hora > soloNumero($tt4)) ? 'disabled':'' ?>></button></td>
       <?php 
    }else{
        ?>
<td class="success text-center">
    <?= ($hoy AND $hora > soloNumero($tt4)) ? '<span class="fa fa-exclamation-circle fa-warning" style="color:orange"></span>':'' ?>
</td>
        <?php
    }
    ?>
    <?php
}
        ?>
   </tr>
    	<?php
    	} 
?>

<?php
} 
}else{
    ?>
<h2 class="text-center text-danger">
    Tecnicos no hay tecnicos disponibles en taller, por favor cominicate con el área tecnica al anexo 802
</h2>
    <?php
}
?>

  </tbody>
	</table>
<script>
    function mostrarHora(string){
        var horaYcordenadas=string.split(",");
        if (horaYcordenadas[0]=='9') {
           return '09:00';
        }else{
            return horaYcordenadas[0]+':00'; 
        }
    }
    function posicion(string){
        var horaYcordenadas=string.split(",");
        return horaYcordenadas[1]; 
    }
$(document).ready(function(){
    $(".SeleccionarHorarioo").click(function() {
        var ubicacion=$(this).val();
        $('.hora').val(posicion(ubicacion));
        var hora=mostrarHora(ubicacion);
        $('.horaVer').val(hora);
        $('.focusUrgenteta').focus();
        $('.horabtn').html(`Cambiar`);
        $('#programacionHtml').html('');
        $('#programacionHtmlm').html('');
        //alert(ubicacion);
    });
        $('.horabtn').html(`Actualizar`);
<?php
    if ($domicilio) {
       ?>
    $(".SeleccionarHorario").hover(function(){
        $(this).parent().prop('colspan','2');
        var ntval=$(this).closest('td').next('td').find('button').val()
        if (ntval==null) {
            $(this).attr('disabled','disabled');
        }
        $(this).closest('td').next('td').hide()
    },function(){
        var ntval=$(this).closest('td').next('td').find('button').val()
        $(this).parent().prop('colspan','0');
        if (ntval==null) {
            $(this).attr('disabled','disabled');
        }
        $(this).closest('td').next('td').show()
        }
    );
       <?php 
    }
?>
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
       <?php 

?>
<?php endif ?>

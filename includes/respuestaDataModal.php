<?php 
if ($_POST['rowid']) {
  include_once("head.php");
  	$idfaqs=htmlspecialchars(htmlentities($_POST['rowid']));
  	$faqsO=Faq::obtenerPregunta($idfaqs);

  ?>
<h2><?php echo utf8_encode($faqsO->pregunta) ?></h2>
<div class="row"><div class="form-group col-md-12">
    <label for="boleta" class="sr-only">Numero de Boleta</label>
    <div class="ibox float-e-margins">
    	 <div class="ibox-content no-padding">
    <textarea  class="summernote col-md-12" name="respuesta" id="" cols="30" rows="10"><?php echo $faqsO->respuesta ?></textarea>

    </div>
    </div>
  </div>

</div>

<input type="hidden" name="id" value="<?php echo $faqsO->idfaqs ?>" >
  

  <script src="../js/jquery-2.1.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="../js/inspinia.js"></script>
    <script src="../js/plugins/pace/pace.min.js"></script>

    <!-- SUMMERNOTE -->
    <script src="../js/plugins/summernote/summernote.min.js"></script>

    <script>
        $(document).ready(function(){
            $('.summernote').summernote();
       });
    </script>

<?php
} 
?>


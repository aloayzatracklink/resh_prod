<?php 
	if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
	class Notificaciones extends Recursos
	{
		public $notificacionid;
		public $usuariood;
		public $tipoAlerta;
		public $asunto;
		public $detalles;
		public $link;
		public $fechaRegistro;
		public $estado;
		public $cantidad;
		function __construct()
		{
		}
        public static function registrarNotificacionRegularizarTemporal($detalles){
            global $db;
            global $session;
            $sqlrn="INSERT INTO notificaciones (usuarioid, tipoAlerta, asunto, detalles, link, fechaRegistro ) VALUES ('";
            $sqlrn.=$db->escapeString($session->idusuario)."','";
            $sqlrn.=$db->escapeString("fa fa-exclamation-triangle")."','";
            $sqlrn.=$db->escapeString("Regularizar ORTE para envio de certificado")."','";
            $sqlrn.=$db->escapeString($detalles)."','";
            $sqlrn.=$db->escapeString("#")."','";
            $sqlrn.=$db->escapeString(date('YmdHmi'))."')";
            return ($db->query($sqlrn))?true:false;
        }
        public static function obtenerRegularizacionesPARAOPEYSAC(){
            global $session;
            global $db;
            $sqlobpos=self::sentencia("SELECT * FROM notificaciones WHERE estado ='0' AND ('3'=(SELECT idrol FROM usuarios WHERE idusuario='".$db->escapeString($session->idusuario)."') OR '778'=(SELECT idrol FROM usuarios WHERE idusuario='".$db->escapeString($session->idusuario)."')) AND usuarioid<>'".$db->escapeString($session->idusuario)."' AND tipoAlerta='fa fa-exclamation-triangle' ");
            return !empty($sqlobpos)?$sqlobpos:false;
        }
        public static function cambiarDeEstado($orte){
            global $db;
            $sqlupec="UPDATE notificaciones SET";
            $sqlupec.=" estado=2";
            $sqlupec.=" WHERE detalles LIKE '%orte ".$db->escapeString(soloNumero(trim($orte)))." de%' ";
            return !($db->query($sqlupec))?true:false;
        }

        public static function obtenerRegularizacionesPARAOPEYSACCantidad(){
            global $session;
            global $db;
            $sqlobpos=self::sentencia("SELECT COUNT(*) as cantidad FROM notificaciones WHERE estado ='0' AND ('3'=(SELECT idrol FROM usuarios WHERE idusuario='".$db->escapeString($session->idusuario)."') OR '778'=(SELECT idrol FROM usuarios WHERE idusuario='".$db->escapeString($session->idusuario)."')) AND usuarioid<>'".$db->escapeString($session->idusuario)."' AND tipoAlerta='fa fa-exclamation-triangle' ");
            $data=array_shift($sqlobpos);
            if ($data->cantidad>=1) {
               return $data->cantidad; 
            }else{
                return false;
            }
        }
		public static function obtenerNotificaciones(){
			global $session;
			$sql=self::sentencia("SELECT * FROM notificaciones WHERE usuarioid= $session->idusuario AND estado = 0");
			return !empty($sql) ? $sql : false;
		}
		public static function obtenerCantidadNotificaciones(){
			global $session;
			$sql=self::sentencia("SELECT COUNT(tipoAlerta) AS cantidad FROM notificaciones WHERE usuarioid= $session->idusuario AND estado = 0 LIMIT 1");
			return !empty($sql) ? array_shift($sql) : false;
		}

		public static function mostrarNotificacion($mensaje,$cuerpo,$link){
			return '<script>Push.create("'.$mensaje.'", {
    				body: "'.$cuerpo.'",
   					icon: "../assets/img/logoR.png",
    				onClick: function () {
      					window.location="http://resh.tracklink.pe/'.$link.'";
      					this.close();
    				}
				});</script>';
		}
        public static function registrarnotificacion($cantidad,$estado) {
            global $db;
            $sqlrn="INSERT INTO notificaciones (tipoAlerta,asunto,fechaRegistro,estado) VALUES ('";
            $sqlrn.='BD'."','";
            $sqlrn.=$cantidad."','";
            $sqlrn.=date('YmdHis')."','";
            $sqlrn.=$estado."')";
            return ($db->query($sqlrn))?true:false;
        }
        public function actualizarCantidadAlertas($cantidad){
            global $db;
            $sqlacaa="UPDATE notificaciones SET asunto='$cantidad' WHERE tipoAlerta='BD' ";
            return ($db->query($sqlacaa))?true:false;
        }

        public function obtenerCantidadDeAlertas(){
            $sqlobca=self::sentencia("SELECT asunto FROM notificaciones WHERE tipoAlerta ='BD' ORDER BY notificacionid DESC LIMIT 1");
            $data=array_shift($sqlobca);
            return $data->asunto;
        }
	}
	}
	

 ?>

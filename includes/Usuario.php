<?php 
if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
class Usuario extends Recursos
{
	public $idusuario;
	public $usuarioNombre;
	public $usuarioApellidoPaterno;
	public $usuarioApellidoMaterno;
	public $anexo;
	public $usuario;
	public $contrasenia;
	public $contrasenia1;
	public $idrol;
	public $cargo;
	public $descripcion;
	public $area;
	public $fechaCreada;
	public $fotoPerfil;
	public $sobreMi;
	public $celular;
	public $celular1;
	public $ultimaModificacion;
	public $idultimamodificacion;
	public $ordenIdTaller;
	public $sede; 	
	public $comercialOperaciones;
	///LOS ACCESOS HERE //7
	public $idopcion;
	public $panel;
	public $baterias;
	public $instalaciones;
	public $chequeos;
	public $choques;
	public $robos;
	public $maps;
	public $registrarOperaciones;
	public $registrarCliente;
	public $cotizar;
	public $gestionPostVenta;
	public $gestionRenovaciones;
	public $verificarOperaciones;
	public $gestionTecnica;
	public $editarTecnicos;
	public $administrar;
	public $verificarComercial;
	public $verificarComercialP;
	public $comercialPersonalizado;
	public $gestionContabilidad;
	public $historialContable;
	public $pagoComision;
	public $programacion;
	public $subirArchivos;
	public $estado;
	public $fechaRegistro;
	public $registrarVehiculo;
	public $idRegistro;
	public $fechaModificacion;	
	public $idModificador;
	public $comisiones;
	public $chequeoMonitoreo;
	public $desinstalacion;
	public $reprogramacion;
	public $programarInstalacion;
	public $administrarComercial;
	public $confirmarRegistroEnOnix;
	public $operacionesComerciales;
	public $cobranzas;
	public $entregaFactura;
	public $cantidad;
	public $reciboHonorario;
	public $verPendietes;
	public $vouchers;
	public $plataformaWebASP;
	public $reporteContratosOcto;
	public $reclamosClientes;
	public $clienteMTC;
	public $seguimientoToyotaAnomalia;
	public $indicadoresTrabajo; //860t0hqxp-EZ
	
	public $enviarcredenciales;
	public $helpdesk;
	public $subirAnomalias;
	
	public $idcustomer;
	public $idvehicle;

	
	function __construct()
	{

	}
	public function registrarNuevoUsuario(){
		global $db;
		global $session;
		$fecha=date("YmdHis");
		$sql="INSERT INTO usuarios (usuarioNombre,usuarioApellidoPaterno,usuarioApellidoMaterno,anexo,celular,usuario,contrasenia,idrol,idCreador,fechaCreada) ";
		$sql.="VALUES ('";
		$sql.=$db->escapeString($this->usuarioNombre)."','";
		$sql.=$db->escapeString($this->usuarioApellidoPaterno)."','";
		$sql.=$db->escapeString($this->usuarioApellidoMaterno)."','";
		$sql.=$db->escapeString($this->anexo)."','";
		$sql.=$db->escapeString($this->celular)."','";
		$sql.=$db->escapeString($this->usuario)."','";
		$sql.=$db->escapeString($this->contrasenia)."','";
		$sql.=$db->escapeString($this->idrol)."','";
		$sql.=$db->escapeString($session->idusuario)."','";
		$sql.=$db->escapeString($fecha)."')";
		if ($db->query($sql)) {
			$this->idusuario=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}
	}

	public static function obtenerUsuarios(){
		return $obtenerUsuariosQ=self::sentencia("SELECT * FROM usuarios");
	}

	public static function obtenerUsuario($id){
		$obtenerUsuarioQueryArray=self::sentencia("SELECT * FROM usuarios WHERE idusuario ='$id' LIMIT 1");
		return  !empty($obtenerUsuarioQueryArray) ? array_shift($obtenerUsuarioQueryArray) : false ;
	}

	public function actualizarPerfilContrasenia($id){
		global $db;
		global $session;		
		$this->ultimaModificacion=date("YmdHis");
		$this->idultimamodificacion=$session->idusuario;
		$sql="UPDATE usuarios SET sobreMi ='";
		$sql.=$db->escapeString($this->sobreMi)."' ,";
		$sql.="contrasenia ='";
		$sql.=$db->escapeString($this->contrasenia)."' ,";
		$sql.="anexo ='";
		$sql.=$db->escapeString($this->anexo)."' ,";
		$sql.="fotoPerfil ='";
		$sql.=$db->escapeString($this->fotoPerfil)."' ,";
		$sql.="celular ='";
		$sql.=$db->escapeString($this->celular)."' ,";
		$sql.="celular1 ='";
		$sql.=$db->escapeString($this->celular1)."' ,";	
		$sql.="ultimaModificacion ='";
		$sql.=$db->escapeString($this->ultimaModificacion)."' ,";
		$sql.="idultimamodificacion ='";
		$sql.=$db->escapeString($this->idultimamodificacion)."' ";
		$sql.=" WHERE idusuario= '$id' ";
		if ($db->query($sql)) {
		 	return true;
		 } else{
		 	return false;
		 }
	}
	
	public function actualizarPerfil($id){
		global $db;
		global $session;		
		$this->ultimaModificacion=date("YmdHis");
		$this->idultimamodificacion=$session->idusuario;
		$sql="UPDATE usuarios SET sobreMi ='";
		$sql.=$db->escapeString($this->sobreMi)."' ,";
		$sql.="anexo ='";
		$sql.=$db->escapeString($this->anexo)."' ,";
		$sql.="fotoPerfil ='";
		$sql.=$db->escapeString($this->fotoPerfil)."' ,";
		$sql.="celular ='";
		$sql.=$db->escapeString($this->celular)."' ,";
		$sql.="celular1 ='";
		$sql.=$db->escapeString($this->celular1)."' ,";	
		$sql.="ultimaModificacion ='";
		$sql.=$db->escapeString($this->ultimaModificacion)."' ,";
		$sql.="idultimamodificacion ='";
		$sql.=$db->escapeString($this->idultimamodificacion)."' ";
		$sql.=" WHERE idusuario= '$id' ";
		if ($db->query($sql)) {
		 	return true;
		 } else{
		 	return false;
		 }


	}



	// --------------
	
	public static function obtenerRoldeUsuario($id){
		$obtenerRoldeUsuario=self::sentencia("SELECT cargo,idrol,area,descripcion FROM roles WHERE idrol = $id LIMIT 1");
		return  !empty($obtenerRoldeUsuario) ? array_shift($obtenerRoldeUsuario) : false ;
	
	}
	public function obtenerRoles(){
		$obtenerRolesQ=self::sentencia("SELECT * FROM roles ");
		foreach ($obtenerRolesQ as $var) :
			?>
			<option class="text-capitalize" value="<?php echo $var->idrol ?>"><?php echo $var->area ?> -> <?php echo $var->cargo ?></option>
			<?php
		endforeach;
	}

	// public static function obtenerCompañerosDeArea($area){
	// 	$sql=self::sentencia("SELECT fotoPerfil,idusuario FROM usuarios WHERE area ");
	// }

	public function obtenerTecnicos(){
		$obtenerTecnicosQ=self::sentencia("SELECT * FROM usuarios WHERE idrol=8 AND estado=37");
		foreach ($obtenerTecnicosQ as $var) :
			?>
			<option class="text-capitalize" value="<?php echo $var->idusuario ?>"><?php echo $var->usuarioNombre ?> <?php echo $var->usuarioApellidoPaterno ?></option>
			<?php
		endforeach;
	}

	public static function obtenerTecnicos2(){
		return $obtenerTecnicosQ=self::sentencia("SELECT * FROM usuarios WHERE idrol=8 AND (estado=37  OR  estado=38 OR estado=39) ORDER BY ordenIdTaller ");		
	}
	public static function obtenerTecnicos2COntar(){
		$obtenerTecnicosQ=self::sentencia("SELECT COUNT(*) as cantidad FROM usuarios WHERE idrol=8 AND (estado=37  OR  estado=38 OR estado=39) ORDER BY ordenIdTaller ");
		return array_shift($obtenerTecnicosQ);
	}
	public static function obtenerTecnicos3(){
		return $obtenerTecnicosQ=self::sentencia("SELECT * FROM usuarios WHERE idrol=8 AND (estado=37 OR estado=38 OR estado=39 OR estado =40) ");		
	}
	public static function cantidadTecnicosDisponibles(){
            $obtenerTecnicosQ=self::sentencia("SELECT * FROM usuarios WHERE idrol=8 AND estado=37 ORDER BY ordenIdTaller ASC ");		
            return ($obtenerTecnicosQ)?$obtenerTecnicosQ:false;
	}

	public static function cantidadTecnicosDisponiblesCitas(){
            $obtenerTecnicosQ=self::sentencia("SELECT * FROM usuarios WHERE idrol=8 AND estado=37 AND sede like 'taller' ORDER BY ordenIdTaller ASC ");		
            return ($obtenerTecnicosQ)?$obtenerTecnicosQ:false;
	}

	public function validarContrasenia($contrasenia){
		global $db;
		global $session;
		$contra=$db->escapeString($contrasenia);
		$sql=self::sentencia("SELECT contrasenia FROM usuarios WHERE contrasenia = '$contra' AND idusuario = ".$session->idusuario." ");
		return !empty($sql) ? TRUE: FALSE;

	}

	public function obtenerTecnicosEstadisticas(){
		$mes=date("m");
		$anio=date("Y");
		$obtenerUsuariosQ=self::sentencia("SELECT * FROM usuarios WHERE idrol=8 ");
		$dia1=$anio.$mes."01%";
		$dia2=$anio.$mes."02%";
		$dia3=$anio.$mes."03%";
		$dia4=$anio.$mes."04%";
		$dia5=$anio.$mes."05%";
		$dia6=$anio.$mes."06%";
		$dia7=$anio.$mes."07%";
		$dia8=$anio.$mes."08%";
		$dia9=$anio.$mes."09%";
		$dia10=$anio.$mes."10%";
		$dia11=$anio.$mes."11%";
		$dia12=$anio.$mes."12%";
		$dia13=$anio.$mes."13%";
		$dia14=$anio.$mes."14%";
		$dia15=$anio.$mes."15%";
		$dia16=$anio.$mes."16%";
		$dia17=$anio.$mes."17%";
		$dia18=$anio.$mes."18%";
		$dia19=$anio.$mes."19%";
		$dia20=$anio.$mes."20%";
		$dia21=$anio.$mes."21%";
		$dia22=$anio.$mes."22%";
		$dia23=$anio.$mes."23%";
		$dia24=$anio.$mes."23%";
		$dia25=$anio.$mes."25%";
		$dia25=$anio.$mes."25%";
		$dia26=$anio.$mes."26%";
		$dia27=$anio.$mes."27%";
		$dia28=$anio.$mes."28%";
		$dia29=$anio.$mes."29%";
		$dia30=$anio.$mes."30%";
		$dia31=$anio.$mes."31%";
		foreach ($obtenerUsuariosQ as $var) :
			?>						 
                ['<?php echo $var->usuarioNombre ?>',0,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia1);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia2);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia3);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia4);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia5);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia6);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia7);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia8);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia9);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia10);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia11);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia12);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia13);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia14);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia15);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia16);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia17);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia18);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia19);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia20);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia21);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia22);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia23);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia24);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia25);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia26);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia27);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia28);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia29);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia30);echo $objOp->instalaciones; ?>,<?php $objOp=Operacion::obtenerInstalacionesPorTecnico($var->idusuario,$dia31);echo $objOp->instalaciones; ?>,],
                       
			<?php
		endforeach;
	}
	public static function verificarUsuario($usuario,$contrasenia){
		global $db;
		$usuario=$db->escapeString($usuario);
		$contrasenia=$db->escapeString($contrasenia);
		$sql="SELECT * FROM usuarios WHERE ";
		$sql.="usuario='$usuario' AND ";
		$sql.="contrasenia='$contrasenia' AND estado='37' "; 
		$sql.="LIMIT 1 ";
		$verificarUsuarioQueryArray=self::sentencia($sql);
		return  !empty($verificarUsuarioQueryArray) ? array_shift($verificarUsuarioQueryArray) : false ;
		
	}
	public static function actualizarContrasenia($usuario,$contrasenia){
		global $db;
		$sql="UPDATE usuarios SET contrasenia='".$db->escapeString($contrasenia)."' WHERE usuario='".$db->escapeString($usuario)."'";
		if($db->query($sql))
			return true;
		else
			return false;
		
	}

	public function obtenerUsuarioExistencia($usuario){
		$obtenerRoldeUsuario=self::sentencia("SELECT usuario FROM usuarios WHERE usuario = '$usuario' LIMIT 1");
		return  !empty($obtenerRoldeUsuario) ? true : false ;
	}
	public function actualizarTecnico($idusuario){
		$this->ultimaModificacion=date("YmdHis");
		global $session;
		global $db;
		$this->idultimamodificacion=$session->idusuario;
		$sql="UPDATE usuarios SET ";
		$sql.="ordenIdTaller = '";
		$sql.=$db->escapeString($this->ordenIdTaller)."',";
		$sql.="sobreMi = '";
		$sql.=$db->escapeString($this->sobreMi)."', ";
		$sql.="sede = '";
		$sql.=$db->escapeString($this->sede)."', ";
		$sql.="estado = '";
		$sql.=$db->escapeString($this->estado)."', ";
		$sql.="idultimamodificacion = '";
		$sql.=$db->escapeString($this->idultimamodificacion)."', ";
		$sql.="ultimaModificacion = '";
		$sql.=$db->escapeString($this->ultimaModificacion)."' ";
		$sql.="WHERE idusuario = ".$db->escapeString($idusuario)."";
		if ($db->query($sql)) {
			return true;
		}else{
			die("Fail en acctualizar datos del tejnico ");
		}
	}
	public function registrarMenu(){
		global $db;
		global $session;
		$this->idRegistro=$session->idusuario;
		$this->fechaRegistro=date("YmdHis");
		$sql="INSERT INTO opciones (idusuario,panel,baterias,instalaciones,chequeos,choques,robos,maps,registrarOperaciones,registrarCliente,cotizar,gestionPostVenta,gestionRenovaciones,verificarOperaciones,gestionTecnica,editarTecnicos,administrar,verificarComercial,verificarComercialP,comercialPersonalizado,gestionContabilidad,historialContable,programacion,registrarVehiculo,fechaRegistro,confirmarRegistroEnOnix,programarInstalacion,administrarComercial,operacionesComerciales,chequeoMonitoreo,cobranzas,pagoComision,comisiones,desinstalacion,reprogramacion,idRegistro,reporteContratosOcto, clienteMTC,seguimientoToyotaAnomalia, reclamosClientes, enviarcredenciales,helpdesk, indicadoresTrabajo)";
		$sql.=" VALUES ('";
		$sql.=$db->escapeString($this->idusuario)."','";
		$sql.=$db->escapeString($this->panel)."','";
		$sql.=$db->escapeString($this->baterias)."','";
		$sql.=$db->escapeString($this->instalaciones)."','";
		$sql.=$db->escapeString($this->chequeos)."','";
		$sql.=$db->escapeString($this->choques)."','";
		$sql.=$db->escapeString($this->robos)."','";
		$sql.=$db->escapeString($this->maps)."','";
		$sql.=$db->escapeString($this->registrarOperaciones)."','";
		$sql.=$db->escapeString($this->registrarCliente)."','";
		$sql.=$db->escapeString($this->cotizar)."','";
		$sql.=$db->escapeString($this->gestionPostVenta)."','";
		$sql.=$db->escapeString($this->gestionRenovaciones)."','";
		$sql.=$db->escapeString($this->verificarOperaciones)."','";
		$sql.=$db->escapeString($this->gestionTecnica)."','";
		$sql.=$db->escapeString($this->editarTecnicos)."','";
		$sql.=$db->escapeString($this->administrar)."','";
		$sql.=$db->escapeString($this->verificarComercial)."','";
		$sql.=$db->escapeString($this->verificarComercialP)."','";
		$sql.=$db->escapeString($this->comercialPersonalizado)."','";
		$sql.=$db->escapeString($this->gestionContabilidad)."','";
		$sql.=$db->escapeString($this->historialContable)."','";
		$sql.=$db->escapeString($this->programacion)."','";
		$sql.=$db->escapeString($this->registrarVehiculo)."','";
		$sql.=$db->escapeString($this->fechaRegistro)."','";
		$sql.=$db->escapeString($this->confirmarRegistroEnOnix)."','";
		$sql.=$db->escapeString($this->programarInstalacion)."','";
		$sql.=$db->escapeString($this->administrarComercial)."','";
		$sql.=$db->escapeString($this->operacionesComerciales)."','";
		$sql.=$db->escapeString($this->chequeoMonitoreo)."','";
		$sql.=$db->escapeString($this->cobranzas)."','";
		$sql.=$db->escapeString($this->pagoComision)."','";
		$sql.=$db->escapeString($this->comisiones)."','";
		$sql.=$db->escapeString($this->desinstalacion)."','";
		$sql.=$db->escapeString($this->reprogramacion)."','";
		$sql.=$db->escapeString($this->idRegistro)."','";
		$sql.=$db->escapeString($this->reporteContratosOcto)."','";
		$sql.=$db->escapeString($this->clienteMTC)."','";
		$sql.=$db->escapeString($this->seguimientoToyotaAnomalia)."','";
		$sql.=$db->escapeString($this->reclamosClientes)."',";
		$sql.=$db->escapeString($this->enviarcredenciales)."',";
		$sql.=$db->escapeString($this->helpdesk)."',";
		$sql.=$db->escapeString($this->indicadoresTrabajo)."')";

		if ($db->query($sql)) {			
			$this->idopcion=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}

	}
	public function actualizarMenuPerfil($idusuario){
		global $session;
		global $db;
		$this->fechaModificacion=date("YmdHis");
		$this->ultimaModificacion=date("YmdHis");
		$this->idModificador=$session->idusuario;
		$this->idultimamodificacion=$session->idusuario;
		$sql="UPDATE usuarios SET ";
		$sql.="usuario = '";
		$sql.=$db->escapeString($this->usuario)."',";
		$sql.="contrasenia = '";
		$sql.=$db->escapeString($this->contrasenia)."', ";
		$sql.="usuarioNombre = '";
		$sql.=$db->escapeString($this->usuarioNombre)."', ";
		$sql.="usuarioApellidoPaterno = '";
		$sql.=$db->escapeString($this->usuarioApellidoPaterno)."', ";
		$sql.="usuarioApellidoMaterno = '";
		$sql.=$db->escapeString($this->usuarioApellidoMaterno)."', ";
		$sql.="idrol = '";
		$sql.=$db->escapeString($this->idrol)."', ";
		$sql.="idultimamodificacion = '";
		$sql.=$db->escapeString($this->idultimamodificacion)."', ";
		$sql.="ultimaModificacion = '";
		$sql.=$db->escapeString($this->ultimaModificacion)."' ";
		$sql.="WHERE idusuario = ".$db->escapeString($idusuario)."";
		if ($db->query($sql)) {
			// return true;
		}else{
			die("Fail en acctualizar datos usuario ");
		}
		
		$sql1="UPDATE opciones SET ";
		$sql1.="panel = '";
		$sql1.=$db->escapeString($this->panel)."', ";
		$sql1.="baterias = '";
		$sql1.=$db->escapeString($this->baterias)."', ";
		$sql1.="instalaciones = '";
		$sql1.=$db->escapeString($this->instalaciones)."', ";
		$sql1.="chequeos = '";
		$sql1.=$db->escapeString($this->chequeos)."', ";
		$sql1.="choques = '";
		$sql1.=$db->escapeString($this->choques)."', ";
		$sql1.="robos = '";
		$sql1.=$db->escapeString($this->robos)."', ";
		$sql1.="maps = '";
		$sql1.=$db->escapeString($this->maps)."', ";
		$sql1.="registrarOperaciones = '";
		$sql1.=$db->escapeString($this->registrarOperaciones)."', ";
		$sql1.="verificarOperaciones = '";
		$sql1.=$db->escapeString($this->verificarOperaciones)."', ";
		$sql1.="registrarCliente = '";
		$sql1.=$db->escapeString($this->registrarCliente)."', ";
		$sql1.="cotizar = '";
		$sql1.=$db->escapeString($this->cotizar)."', ";
		$sql1.="gestionPostVenta = '";
		$sql1.=$db->escapeString($this->gestionPostVenta)."', ";
		$sql1.="gestionRenovaciones = '";
		$sql1.=$db->escapeString($this->gestionRenovaciones)."', ";
		$sql1.="gestionTecnica = '";
		$sql1.=$db->escapeString($this->gestionTecnica)."', ";
		$sql1.="editarTecnicos = '";
		$sql1.=$db->escapeString($this->editarTecnicos)."', ";
		$sql1.="verificarComercial = '";
		$sql1.=$db->escapeString($this->verificarComercial)."', ";
		$sql1.="verificarComercialP = '";
		$sql1.=$db->escapeString($this->verificarComercialP)."', ";
		$sql1.="comercialPersonalizado = '";
		$sql1.=$db->escapeString($this->comercialPersonalizado)."', ";
		$sql1.="historialContable = '";
		$sql1.=$db->escapeString($this->historialContable)."', ";
		$sql1.="gestionContabilidad = '";
		$sql1.=$db->escapeString($this->gestionContabilidad)."', ";
		$sql1.="programacion = '";
		$sql1.=$db->escapeString($this->programacion)."', ";
		$sql1.="confirmarRegistroEnOnix = '";
		$sql1.=$db->escapeString($this->confirmarRegistroEnOnix)."', ";
		$sql1.="programarInstalacion = '";
		$sql1.=$db->escapeString($this->programarInstalacion)."', ";
		$sql1.="administrarComercial = '";
		$sql1.=$db->escapeString($this->administrarComercial)."', ";
		$sql1.="operacionesComerciales = '";
		$sql1.=$db->escapeString($this->operacionesComerciales)."', ";
		$sql1.="chequeoMonitoreo = '";
		$sql1.=$db->escapeString($this->chequeoMonitoreo)."', ";
		$sql1.="cobranzas = '";
		$sql1.=$db->escapeString($this->cobranzas)."', ";
		$sql1.="pagoComision = '";
		$sql1.=$db->escapeString($this->pagoComision)."', ";
		$sql1.="comisiones = '";
		$sql1.=$db->escapeString($this->comisiones)."', ";
		$sql1.="desinstalacion = '";
		$sql1.=$db->escapeString($this->desinstalacion)."', ";
		$sql1.="reprogramacion = '";
		$sql1.=$db->escapeString($this->reprogramacion)."', ";
		$sql1.="administrar = '";
		$sql1.=$db->escapeString($this->administrar)."', ";
		$sql1.="reporteContratosOcto = '";
		$sql1.=$db->escapeString($this->reporteContratosOcto)."', ";
		$sql1.="clienteMTC = '";
		$sql1.=$db->escapeString($this->clienteMTC)."' , ";
		$sql1.="reclamosClientes = '";
		$sql1.=$db->escapeString($this->reclamosClientes)."' , "; 
		$sql1.="enviarcredenciales = '";
		$sql1.=$db->escapeString($this->enviarcredenciales)."' , ";
		$sql1.="indicadoresTrabajo = '";
		$sql1.=$db->escapeString($this->indicadoresTrabajo)."' , ";
		$sql1.="seguimientoToyotaAnomalia = '";
		$sql1.=$db->escapeString($this->seguimientoToyotaAnomalia)."' ";
		$sql1.="helpdesk = '";
		$sql1.=$db->escapeString($this->helpdesk)."' ";
		$sql1.="WHERE idusuario = ".$db->escapeString($idusuario)."";
		if ($db->query($sql1)) {
			return true;
		}else{
			die("Fail en acctualizar opciones");
		}
	}

	public static function obtenerMenu($idusuario){
		$obtenerMenuQu=self::sentencia("SELECT * FROM opciones WHERE idusuario = $idusuario");
		return !empty($obtenerMenuQu) ? array_shift($obtenerMenuQu) : false ;
	}
	
	public static function obtenerEjecutivosComercialConComision(){
		$obtenerEjecutivos=self::sentencia("SELECT * FROM usuarios AS U WHERE  EXISTS(SELECT * FROM comisiones AS C WHERE  U.idusuario=C.idCreador)  AND estado=37");
		return !empty($obtenerEjecutivos)? $obtenerEjecutivos:false;
	}

	public static function obtenerPARARESERVA(){
		$obtenerEjecutivos=self::sentencia("SELECT * FROM usuarios  WHERE  estado=37 AND (idrol='4' || idrol='5' || idrol='1' || idrol='13' || idrol='778' || idrol='6' || idrol='3'  || idrol='783') ORDER BY usuarioNombre");
		return !empty($obtenerEjecutivos)? $obtenerEjecutivos:false;
	}
 	public static function obtenerEjecutivos(){
 		$obtenerEjecutivos=self::sentencia("SELECT * FROM usuarios WHERE (idrol='4'  || idrol='13' || idrol='778' || idrol='6' || idrol='3' || idusuario='93') AND estado ='37' ORDER BY usuarioNombre ASC ");
 		return !empty($obtenerEjecutivos)?$obtenerEjecutivos:false;
 	}

 	public static function obtenerParaVenta(){
 		$obtenerEjecutivos=self::sentencia("SELECT * FROM usuarios WHERE  estado ='37' ORDER BY usuarioNombre ASC ");
 		return !empty($obtenerEjecutivos)?$obtenerEjecutivos:false;
 	}

 	public static function obtenerEjecutivosAsignados($idusuario){
 		global $session;
 		$otenerEjecutivosAsignados=self::sentencia("SELECT * FROM usuarios WHERE comercialOperaciones=$idusuario");
 		return !empty($otenerEjecutivosAsignados)?$otenerEjecutivosAsignados:false;
 	}

 	public function obtenerEJecutiovsDeOperacionesPorDatos($datos){
 		global $db;
 		$datos=$db->escapeString($datos);
 			$obtenerEJecutiovsDeOperacionesPorDatos=self::sentencia("SELECT * FROM usuarios WHERE idrol=778 AND (usuarioNombre LIKE '%$datos%' OR usuarioApellidoPaterno LIKE '%$datos%' OR usuarioApellidoMaterno LIKE '%$datos%' OR CONCAT_WS(' ',usuarioNombre,usuarioApellidoPaterno) LIKE '%$datos%')");
 			return !empty($obtenerEJecutiovsDeOperacionesPorDatos)?$obtenerEJecutiovsDeOperacionesPorDatos:false;

 	}

 	public static function obtenerEJecutiovsDeOperaciones(){
 		$obOc=self::sentencia("SELECT * FROM usuarios WHERE idrol='778'");
 		return !empty($obOc)?$obOc:false;
 	}

    public static function obtenerUsuarioxId($idusuario){
        $obOc=self::sentencia("SELECT * FROM usuarios WHERE idusuario= '$idusuario' LIMIT 1");
        return !empty($obOc)? array_shift($obOc):false;
    }

    public static function obtenerUsuarioxUsu($usuario){
        $obOc=self::sentencia("SELECT * FROM usuarios WHERE usuario like '".$usuario."%' LIMIT 1");
        return !empty($obOc)? array_shift($obOc):false;
    }

	//query enviarcredenciales ----- para obtener correo de area responsable en base a permisos de acceso

	// public function correoDeAreaPorPermisoAlModulo($id){
	// 	$obOc=self::sentencia("SELECT o.idusuario, u.usuario AS correotrabajador, r.area AS arearesponsable, r.email AS correoarea  FROM opciones o
	// 	INNER JOIN usuarios u ON o.idusuario = u.idusuario
	// 	INNER JOIN roles r ON u.idrol = r.idrol
	// 	WHERE o.enviarcredenciales='si' and u.idusuario='$id'");
	// 	return !empty($obOc)? array_shift($obOc):false;
	// }
	//-------------------------------------------------------

 	public function asignarEjecutivoDeOperaciones(){
 		global $db;
 		global $session;
 		$this->ultimaModificacion=date("YmdHis");
 		$sql="UPDATE usuarios SET comercialOperaciones='";
 		$sql.=$db->escapeString($this->comercialOperaciones)."', ";
 		$sql.=" ultimaModificacion='";
 		$sql.=$db->escapeString($this->ultimaModificacion)."', ";
 		$sql.=" idultimamodificacion='";
 		$sql.=$session->idusuario."' WHERE idusuario='";
 		$sql.=$db->escapeString($this->idusuario)."'";
 		return ($db->query($sql)) ? true:false;
 	}

 	public function quitarEjecutivoDeOperaciones(){
 		global $db;
 		global $session;
 		$this->ultimaModificacion=date("YmdHis");
 		$sql="UPDATE usuarios SET comercialOperaciones='0', ";
 		$sql.=" ultimaModificacion='";
 		$sql.=$db->escapeString($this->ultimaModificacion)."', ";
 		$sql.=" idultimamodificacion='";
 		$sql.=$session->idusuario."' WHERE idusuario='";
 		$sql.=$db->escapeString($this->idusuario)."'";
 		return ($db->query($sql)) ? true:false;
 	}

	public static function listaNegraMtc(){
		$result=self::selectAll("SELECT DISTINCT idcustomer, idvehicle, motivoBL FROM black_list_mtc");
		return !empty($result) ? $result : []; 
	}

	//860ryp0nz-EZ
	public static function insertarEnListaNegraMtc(string $idcustomer, string $idvehicle, string $motivo) {
		//string
		global $db;
		// Realizar la inserción en la tabla black_list_mtc
		$query = "INSERT INTO black_list_mtc (idcustomer, idvehicle, motivoBL) VALUES ('$idcustomer', '$idvehicle', '$motivo')";
		//Values entre '' para que se guarde como tal
		// Ejecutar 👻
		$result = $db->query($query);
	
		return $result;
	}

	public static function eliminarDeListaNegraMtc(string $idcustomer, string $idvehicle) {
		//string
		global $db;
		// Realizar la eliminación en la tabla black_list_mtc
		$query = "DELETE FROM black_list_mtc WHERE idcustomer='$idcustomer' AND idvehicle='$idvehicle'";
		//Values entre '' para que se guarde como tal
		// Ejecutar 👻
		$result = $db->query($query);
	
		return $result;
	}
	
	
}
}
?>
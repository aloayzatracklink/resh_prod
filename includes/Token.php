<?php 
	if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
class Token
{
	public static function generar(){
		return $_SESSION['token']=base64_encode(openssl_random_pseudo_bytes(32));
	}
	public static function generarAlternativo(){
		return $_SESSION['tokenAlternativo']=limpiar(base64_encode(openssl_random_pseudo_bytes(32)));
	}

	public static function verificar($token){
		if (isset($_SESSION['token']) && $token===$_SESSION['token']) {
			unset($_SESSION['token']);
			return true;
		}
		return false;
	}
	public static function verificarTokenAlternativo($generarAlternativo){
		if (isset($_SESSION['tokenAlternativo']) && $generarAlternativo===$_SESSION['tokenAlternativo']) {
			unset($_SESSION['tokenAlternativo']);
			return true;
		}
		return false;
	}
	
}
}

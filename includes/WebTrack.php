<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) {
    header("location:../404.html");
} else {
    class WebTrack extends Recursos
    {
        public $codigo;
        public $tipo_comprobante;
        public $tipo_documento;
        public $numero_documento;
        public $nombre;
        public $apellido;
        public $direccion;
        public $telefono_contacto;
        public $nombre_contacto;
        public $email_contacto;
        public $email_comprobante_pago;
        public $departamento;
        public $url_poliza;
        public $url_tarjeta_propiedad_anverso;
        public $url_tarjeta_propiedad_reverso;
        public $url_comprobante_pago;
        public $fecha;
        public $cantidad;
        public $idsuariocargo;

        function __construct()
        {

        }

        public static function obtenerTramaIntegracionPag($posisionDePagina, $registrosPorPagina)
        {
            $obj = self::sentenciaWebTrack("SELECT * FROM solicitud_radar_pro where idoperacion is null order by fecha desc LIMIT $posisionDePagina, $registrosPorPagina");
            return !empty($obj) ? $obj : false;
        }

        public static function obtenerCantidadTramaIntegracionPag()
        {
            $obj = self::sentenciaWebTrack("SELECT COUNT(*) as cantidad FROM solicitud_radar_pro where idoperacion is null LIMIT 1 ");
            return !empty($obj) ? array_shift($obj) : false;
        }

        public static function obtenerSolicitudRadar($codigoSolicitud)
        {
            $obj = self::sentenciaWebTrack("SELECT * FROM solicitud_radar_pro where codigo='$codigoSolicitud'");
            return !empty($obj) ? array_shift($obj) : false;
        }

        public static function asignarSolicitud($codigoSolicitud)
        {
            global $session;
            global $dbWebTrack;

            $sql = "UPDATE solicitud_radar_pro SET idsuariocargo='" . $session->idusuario . "' WHERE codigo ='" . $codigoSolicitud . "' ";
            if ($dbWebTrack->queryWebTrack($sql)) {
                return true;
            } else {
                return false;
            }
        }

        public static function asignarOperacionId($idsolradar, $idoperacion)
        {
            global $dbWebTrack;

            $sql = "UPDATE solicitud_radar_pro SET idoperacion=" . $idoperacion . " WHERE codigo =" . $idsolradar . " ";
            if ($dbWebTrack->queryWebTrack($sql)) {
                return true;
            } else {
                return false;
            }
        }

    }
}
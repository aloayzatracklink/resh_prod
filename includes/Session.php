<?php 
	if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
	class Session
	{
		public $ingresado=false;
		public $idusuario;
		public $mensaje;
		public $rol;

		function __construct()
		{
			session_set_cookie_params(0);
			session_start();	
			$this->verificarIngreso();
			$this->verificarMensaje();
		}
		public function yaEstaIngresado(){
			return $this->ingresado;
		}
		public function ingresar($usuario){
			if ($usuario) {
				$this->idusuario=$_SESSION['idusuario']=$usuario->idusuario;
				$this->ingresado=true;
			}
		}

		public function salir(){
			unset($_SESSION['idusuario']);
			unset($this->idusuario);
			$this->ingresado=false;
		}

		private function verificarIngreso(){
			if(isset($_SESSION['idusuario'])){
				$this->idusuario=$_SESSION['idusuario'];
				$this->ingresado=true;
			}else{
				 unset($this->idusuario);
				 $this->ingresado=false;
			}
		}
		public function mensaje($msj=""){
			if (!empty($msj)) {
				$_SESSION['mensaje']=$msj;
			}else{
				return $this->mensaje="";
			}
		}
		public function verificarMensaje(){
			if (isset($_SESSION['mensaje'])) {
				$this->mensaje=$_SESSION['mensaje'];
				unset($_SESSION['mensaje']);
			}else{
				$this->mensaje="";
			}
		}
		
	}
	$session= new Session();
	}

 ?>
<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) {
    header("location:../404.html");
} else {
    class baseDatosWebTrack {
        public $coneccion2;

        function __construct() {
            $this->conectarWebTrackDb();
        }

        public function conectarWebTrackDb() {
            $this->coneccion2 = new mysqli(HOSTW, USUARIOW, CONTRASENIAW, BASEDATOSW);

            if ($this->coneccion2->connect_errno) {
                die("Error en conectarse a Web Tracklink " . $this->coneccion2->connect_error);
            }
        }


        public function queryWebTrack($sql) {
            $resultado = $this->coneccion2->query($sql);
            $this->confirmarWebTrack($resultado);
            return $resultado;
        }

        public function confirmarWebTrack($resultado) {
            if (!$resultado) {
                die("Error x6 the query " . $this->coneccion2->error);
            }
        }

        public function escapeStringWebTrack($string) {
            $escape = $this->coneccion2->real_escape_string($string);
            return $escape;
        }
    }

    $dbWebTrack = new baseDatosWebTrack();
}
?>

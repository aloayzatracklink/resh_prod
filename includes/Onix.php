<?php 
	if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
class Onix extends Recursos
{
	public $fechaOrden;
	public $nombre;
	public $apellidos;
	public $dni;
	public $marca;
	public $modelo;
	public $chasis;
	public $motor;
	public $anio;
	public $placa;
	public $color;
	public $articulo;
	public $celular;
	public $celular2;
	public $telefonoTrabajo;
	public $telefono;
	public $correo;
	public $correo2;
	public $desde;
	public $hasta;
	public $idcustomer;
	public $idvehicle;
	public $idlocation;
	public $comentarios;
	public $direccion;

	public $fechaInstacion;
	public $imei;
	public $cantidad;

	public $iduser;
	public $password;
	
	public $contactoApellido;
	public $contactoTelefono;
	public $contactoCelular;
	public $nombreContacto;
	public $idclienteContacto;
	public $correoContacto;
	public $parentesco;

	public $firstname;
	public $lastname;
	public $descuser;

	public $orden;
	public $idorder;
    public $serie;
    public $numeroFactura;
    public $orteO;
    public $precioO;
    public $tiempo;
    public $idtidentdoc;

	public $entper; //agregado 16/08/23
	public $identdoc; //agregado 16/08/23
	public $officework; //agregado 16/08/23
	public $fullname;

	public $idchasis;

	public $mail;

	public $correo_cliente;

	function __construct()
	{
		
	}

public static function ObtenerDatosDesdeChasis($chasis){
	global $dbOnix;
	$ObtenerDatosDesdeChasisQ=self::sentenciaOnix("SELECT V.idcustomer as idcustomer, V.idvehicle as idvehicle, V.idlocation as idlocation, V.idchasis as chasis,C.idtidentdoc, color, V.license as placa, V.yearmodel as anio, V.engine as motor, V.dateservfrom as desde, V.dateservto as hasta, V.descmodel as modelo, V.idbrand , M.descbrand as marca, C.identdoc as dni, C.lastname as apellidos, C.firstname as nombre , C.phonemain as celular, C.phonecell as celular2, C.mail as correo, C.phonework as telefonoTrabajo, C.comments as comentarios, C.mail2 as correo2 , C.faxwork as telefono, C.addressmain as  direccion FROM sid_vehicle V INNER JOIN sid_brand M ON V.idbrand=M.idbrand INNER JOIN sid_customer C ON (C.idcustomer=V.idcustomer AND C.idlocation=V.idlocation ) WHERE C.statcustomer='A' and   idchasis ='".$dbOnix->escapeStringOnix($chasis)."' ORDER BY V.dateservfrom DESC  LIMIT 1");
	return !empty($ObtenerDatosDesdeChasisQ)?array_shift($ObtenerDatosDesdeChasisQ):false;	
}

public static function obtenerDatosDesdeOrdenapitest($orden){
	global $dbOnix;
	$orden=trim(limpiar($orden));
    if ($orden) {
        if(strpos($orden,'-')!==false){
            $tmp=explode('-',$orden);
            $idloc=trim(array_values($tmp)[0]);	
            $idorden=trim(array_values($tmp)[1]);
            $obtenerDatosDesdeOrden=self::sentenciaOnix("SELECT sid_order.descorder orden,concat('| ',sid_vehicle.idchasis,' | ',sid_vehicle.license) apellidos,sid_vehicle.idchasis chasis from sid_orderveh inner join sid_order on (sid_order.idorder=sid_orderveh.idorder and sid_order.idlocation=sid_orderveh.idlocation) inner join sid_vehicle on (sid_orderveh.idcustomer=sid_vehicle.idcustomer and sid_orderveh.idloccust=sid_vehicle.idlocation and sid_orderveh.idvehicle=sid_vehicle.idvehicle) where sid_orderveh.idorder='".$idorden."' and sid_orderveh.idlocation='".$idloc."' limit 1");
            return array_shift($obtenerDatosDesdeOrden);
        }else{
            return null;
        }
    }else{
        return null;
    }
    
}

public static function obtenerDatosDesdeOrden($orden){
	global $dbOnix;
	$orden=trim(limpiar($orden));
    if ($orden) {
        $tmp=explode('-',$orden);
        $idloc=trim(array_values($tmp)[0]);	
        $idorden=trim(array_values($tmp)[1]);
        $obtenerDatosDesdeOrden=self::sentenciaOnix("SELECT O.descorder orden, OV.idcustomer as idcustomer, OV.idvehicle as idvehicle, OV.idloccust as idlocation, V.idchasis as chasis, color, V.license as placa, V.yearmodel as anio, V.engine as motor, V.dateservfrom as desde, V.dateservto as hasta, V.descmodel as modelo, V.idbrand , M.descbrand as marca, C.identdoc as dni, C.lastname as apellidos, C.firstname as nombre , C.phonemain as celular, C.phonecell as celular2, C.mail as correo, C.phonework as telefonoTrabajo, C.comments as comentarios, C.mail2 as correo2 , C.faxwork as telefono, C.addressmain as  direccion  FROM sid_orderveh OV INNER JOIN sid_customer C ON (C.idcustomer=OV.idcustomer AND C.idlocation=OV.idloccust) INNER JOIN sid_vehicle V ON (OV.idcustomer=V.idcustomer AND OV.idloccust=V.idlocation AND OV.idvehicle=V.idvehicle) INNER JOIN sid_brand M ON V.idbrand=M.idbrand INNER JOIN sid_order O ON (O.idorder=OV.idorder) WHERE OV.idorder='$idorden' AND OV.idlocation='$idloc'");
        return !empty($obtenerDatosDesdeOrden)?array_shift($obtenerDatosDesdeOrden):false;
    }else{
        return false;
    }
    
}

public static function obtenerORTE($orden){
	global $dbOnix;
	$orden=trim(limpiar($orden));
	$tmp=explode('-',$orden);
	$idloc=trim(array_values($tmp)[0]);	
	$idorden=trim(array_values($tmp)[1]);
	$obtenerDatosDesdeOrden=self::sentenciaOnix("SELECT orte as orteO FROM sid_order WHERE  idorder='$idorden' AND idlocation='$idloc'");
	return !empty($obtenerDatosDesdeOrden)?array_shift($obtenerDatosDesdeOrden):false;
}

public static function obtenerIdsdeOrte($orte){
	global $dbOnix;
	$orte=trim(limpiar($orte));
	$obtenerDatosDesdeOrden=self::sentenciaOnix("SELECT idorder as orden, idloccust as idlocation FROM sid_order WHERE  orte='$orte' LIMIT 1");
    if (!empty($obtenerDatosDesdeOrden)) {
       $datos=array_shift($obtenerDatosDesdeOrden);
       return $datos->idlocation."-".$datos->orden;
    }else{
        return false;
    }
}
public static function obtenerDatosFacturacion($orden){
	global $dbOnix;
	$orden=trim(limpiar($orden));
	$tmp=explode('-',$orden);
	$idloc=trim(array_values($tmp)[0]);	
	$idorden=trim(array_values($tmp)[1]);
	$obtenerDatosDesdeOrden=self::sentenciaOnix("SELECT SI.idtranspoint as serie, SI.nroinvoice as numeroFactura,SG.idorder ,   SGO.pricelist as precioO , SG.idgeninvoiceorder, SI.idinvoice FROM sid_invoice SI  INNER JOIN sid_genorder SG ON (SG.idgeninvoiceorder=SI.idgeninvoiceorder) INNER JOIN sid_detinvoice SGO ON (SI.idinvoice=SGO.idinvoice AND SGO.pricelist <>'' ) WHERE SG.idorder='$idorden' AND SG.idlocation='$idloc'");
	return !empty($obtenerDatosDesdeOrden)?array_shift($obtenerDatosDesdeOrden):false;
}

public static function obtenerDatosDesdeIds($idcustomer,$idvehicle,$idlocation){
	global $dbOnix;
	$idcustomer=$dbOnix->escapeStringOnix($idcustomer);
	$idvehicle=$dbOnix->escapeStringOnix($idvehicle);
	$idlocation=$dbOnix->escapeStringOnix($idlocation);
	$ObtenerDatosDesdeChasisQ=self::sentenciaOnix("SELECT V.idcustomer as idcustomer, V.idvehicle as idvehicle, V.idlocation as idlocation, V.idchasis as chasis, color, V.license as placa, V.yearmodel as anio, V.engine as motor, V.dateservfrom as desde, V.dateservto as hasta, V.descmodel as modelo, V.idbrand , M.descbrand as marca, C.identdoc as dni, C.lastname as apellidos, C.firstname as nombre , C.phonemain as celular, C.phonecell as celular2, C.mail as correo, C.phonework as telefonoTrabajo, C.comments as comentarios, C.mail2 as correo2 , C.faxwork as telefono, C.addressmain as  direccion FROM sid_vehicle V INNER JOIN sid_brand M ON V.idbrand=M.idbrand INNER JOIN sid_customer C ON (C.idcustomer=V.idcustomer AND C.idlocation=V.idlocation ) AND V.idvehicle= '$idvehicle' AND V.idlocation='$idlocation' AND V.idcustomer='$idcustomer' AND V.idlocation = '$idlocation'");
	return !empty($ObtenerDatosDesdeChasisQ)?array_shift($ObtenerDatosDesdeChasisQ):false;	
	// C.idcustomer=V.idcustomer AND C.idlocation=V.idlocation

	// SELECT V.idcustomer as idcustomer, V.idvehicle as idvehicle, V.idlocation as idlocation, V.idchasis as chasis, color, V.license as placa, V.yearmodel as anio, V.engine as motor, V.dateservfrom as desde, V.dateservto as hasta, V.descmodel as modelo, V.idbrand , M.descbrand as marca, C.identdoc as dni, C.lastname as apellidos, C.firstname as nombre , C.phonemain as celular, C.phonecell as celular2, C.mail as correo, C.phonework as telefonoTrabajo, C.comments as comentarios, C.mail2 as correo2 , C.faxwork as telefono FROM sid_vehicle as V, sid_customer as C, sid_brand as M WHERE V.idbrand=M.idbrand AND V.idvehicle= '$idvehicle' AND C.idcustomer='idcustomer' AND V.idlocation='$idlocation'

	// C.identdoc as dni, C.lastname as apellidos, C.firstname as nombre , C.phonemain as celular, C.phonecell as celular2, C.mail as correo, C.phonework as telefonoTrabajo, C.comments as comentarios, C.mail2 as correo2 , C.faxwork as telefono
}
public static function obtenerDatosCLIENTEDesdeIds($idcustomer,$idlocation){
	global $dbOnix;
	$idcustomer=$dbOnix->escapeStringOnix($idcustomer);
	$idlocation=$dbOnix->escapeStringOnix($idlocation);
	$ObtenerDatosDesdeChasisQ=self::sentenciaOnix("SELECT C.identdoc as dni, C.lastname as apellidos, C.firstname as nombre , C.phonemain as celular, C.phonecell as celular2, C.mail as correo, C.phonework as telefonoTrabajo, C.comments as comentarios, C.mail2 as correo2 , C.faxwork as telefono, C.addressmain as  direccion FROM sid_customer C WHERE  C.idlocation='$idlocation' AND C.idcustomer='$idcustomer'");
	return !empty($ObtenerDatosDesdeChasisQ)?array_shift($ObtenerDatosDesdeChasisQ):false;	
}

public static function obtenerCantididadVehiculos($idcustomer,$location){
	global $dbOnix;
	$obtenerCantididadVehiculos=self::sentenciaOnix("SELECT COUNT(*) as cantidad FROM sid_vehicle WHERE idcustomer ='".$dbOnix->escapeStringOnix($idcustomer)."' AND idlocation ='".$dbOnix->escapeStringOnix($location)."' LIMIT 1 ");
	return !empty($obtenerCantididadVehiculos)?array_shift($obtenerCantididadVehiculos):false;	
}

public static  function obtenerDatosDesdeId($idsim){
	global $dbOnix;
	//$obtenerDatosDesdeId=self::sentenciaOnix("SELECT S.dateinstall as fechaInstacion, S.idseries as imei, S.idcustomer as idcustomer, S.idvehicle as idvehicle, S.idloccust as idlocation, V.idchasis as chasis, color, V.license as placa, V.yearmodel as anio, V.engine as motor, V.dateservfrom as desde, V.dateservto as hasta, V.descmodel as modelo, V.idbrand , M.descbrand as marca, C.identdoc as dni, C.lastname as apellidos, C.firstname as nombre , C.phonemain as celular, C.phonecell as celular2, C.mail as correo, C.phonework as telefonoTrabajo, C.comments as comentarios, C.mail2 as correo2 , C.faxwork as telefono FROM sid_serie S, sid_customer C, sid_vehicle V INNER JOIN sid_brand M ON V.idbrand=M.idbrand WHERE C.idcustomer=S.idcustomer AND  S.idvehicle=V.idvehicle AND S.idcustomer=V.idcustomer AND S.idloccust=C.idlocation AND S.idloccust=V.idlocation  and S.idsim='".$dbOnix->escapeStringOnix($idsim)."' ORDER BY S.dateinstall DESC LIMIT 1");
	$obtenerDatosDesdeId=self::sentenciaOnix("SELECT S.dateinstall as fechaInstacion, S.idseries as imei, S.idcustomer as idcustomer, S.idvehicle as idvehicle, S.idloccust as idlocation, V.idchasis as chasis, color, V.license as placa, V.yearmodel as anio, V.engine as motor, V.dateservfrom as desde, V.dateservto as hasta, V.descmodel as modelo, V.idbrand , M.descbrand as marca, C.identdoc as dni, C.lastname as apellidos, C.firstname as nombre , C.phonemain as celular, C.phonecell as celular2, C.mail as correo, C.phonework as telefonoTrabajo, C.comments as comentarios, C.mail2 as correo2 , C.faxwork as telefono FROM sid_serie S LEFT JOIN sid_customer C ON (C.idcustomer=S.idcustomer AND S.idloccust=C.idlocation) INNER JOIN sid_vehicle V ON (V.idvehicle=S.idvehicle AND V.idcustomer=S.idcustomer AND V.idlocation=S.idloccust ) LEFT JOIN sid_brand M ON V.idbrand=M.idbrand WHERE  S.idsim='".$dbOnix->escapeStringOnix($idsim)."'  LIMIT 1");
	return !empty($obtenerDatosDesdeId)?array_shift($obtenerDatosDesdeId):false;
}
public static  function validarInstalacion($idsim){
	global $dbOnix;
	//$obtenerDatosDesdeId=self::sentenciaOnix("SELECT S.dateinstall as fechaInstacion, S.idseries as imei, S.idcustomer as idcustomer, S.idvehicle as idvehicle, S.idloccust as idlocation, V.idchasis as chasis, color, V.license as placa, V.yearmodel as anio, V.engine as motor, V.dateservfrom as desde, V.dateservto as hasta, V.descmodel as modelo, V.idbrand , M.descbrand as marca, C.identdoc as dni, C.lastname as apellidos, C.firstname as nombre , C.phonemain as celular, C.phonecell as celular2, C.mail as correo, C.phonework as telefonoTrabajo, C.comments as comentarios, C.mail2 as correo2 , C.faxwork as telefono FROM sid_serie S, sid_customer C, sid_vehicle V INNER JOIN sid_brand M ON V.idbrand=M.idbrand WHERE C.idcustomer=S.idcustomer AND  S.idvehicle=V.idvehicle AND S.idcustomer=V.idcustomer AND S.idloccust=C.idlocation AND S.idloccust=V.idlocation  and S.idsim='".$dbOnix->escapeStringOnix($idsim)."' ORDER BY S.dateinstall DESC LIMIT 1");
	$obtenerDatosDesdeId=self::sentenciaOnix("SELECT * FROM sid_serie  WHERE  idsim='".$dbOnix->escapeStringOnix($idsim)."' AND idcustomer IS NOT NULL AND idloccust IS NOT NULL AND idvehicle IS NOT NULL LIMIT 1");
	return !empty($obtenerDatosDesdeId)?true:false;
}

public static function obtenerFechaDeVencimiento($idcustomer,$idvehicle,$idlocation){
	global $dbOnix;
	$obtenerFechaDeVencimientoQ=self::sentenciaOnix("SELECT dateservto hasta , dateservfrom desde,TIMESTAMPDIFF(YEAR, dateservfrom, dateservto) as tiempo  FROM sid_contract WHERE idloccust='$idlocation'  AND idcustomer='$idcustomer' AND  idvehicle='$idvehicle' ORDER BY  idcontract DESC ");
	return !empty($obtenerFechaDeVencimientoQ)?array_shift($obtenerFechaDeVencimientoQ):false;
}

public static function obtenerOrdenesVehiculo($idcustomer, $idvehicle,$idlocation){
	global $dbOnix;
	$obtenerOrdenesVehiculoQ=self::sentenciaOnix("SELECT OV.idorder, OV.idlocation, OV.idvehicle, OV.idcustomer, O.descorder as articulo,O.idorder, O.datecreate as fechaOrden FROM sid_orderveh OV INNER JOIN sid_order O ON O.idorder=OV.idorder WHERE OV.idcustomer=".$dbOnix->escapeStringOnix($idcustomer)." AND OV.idvehicle=".$dbOnix->escapeStringOnix($idvehicle)." AND OV.idloccust ='".$dbOnix->escapeStringOnix($idlocation)."' ORDER BY OV.idorder DESC");
	return !empty($obtenerOrdenesVehiculoQ)?$obtenerOrdenesVehiculoQ:false;

}

public static function obtenerContactosAutorizadosDeCliente($idlocation,$idcustomer){
	global $dbOnix;
	$idlocation=$dbOnix->escapeStringOnix($idlocation);
	$idcustomer=$dbOnix->escapeStringOnix($idcustomer);
	$obtenerContactosAutorizadosDeClienteQ=self::sentenciaOnix("SELECT CO.lastnamecontact as contactoApellido, CO.phonecontact as contactoTelefono, CO.phonecellcontact as contactoCelular, CO.firstnamecontact as nombreContacto, CO.identdoccontact as idclienteContacto, CO.idtrelative as parentescoId,CO.emailcontact as correoContacto, TR.desctrelative as parentesco FROM sid_custcontact as CO, sid_trelative as TR WHERE idlocation='$idlocation' AND idcustomer='$idcustomer' AND TR.idtrelative=CO.idtrelative ");
	return !empty($obtenerContactosAutorizadosDeClienteQ)?$obtenerContactosAutorizadosDeClienteQ:false;
}

 public static function validarUsuarioYContrasenia($usuario, $contrasenia){
 	global $dbOnix;
 	$usuario=trim(htmlspecialchars($usuario));
 	$contrasenia=trim(htmlspecialchars($contrasenia));
 	$validarUsuarioYContrasenia=self::sentenciaOnix("SELECT iduser, password FROM sid_user WHERE iduser='".$dbOnix->escapeStringOnix($usuario)."' AND password ='".$dbOnix->escapeStringOnix($contrasenia)."' ");
 	return !empty($validarUsuarioYContrasenia)? true:false;

 }

public static function obtenerPlanDeServicio($idcustomer, $idvehicle,$idlocation){
	global $dbOnix;
	$obtenerPlanDeServicio=self::sentenciaOnix("SELECT  A.descarticle as articulo, A.idarticle, C.idarticle  FROM sid_contract C INNER JOIN sid_article A ON C.idarticle=A.idarticle WHERE C.idcustomer=".$dbOnix->escapeStringOnix($idcustomer)." AND C.idvehicle=".$dbOnix->escapeStringOnix($idvehicle)." AND C.idloccust ='".$dbOnix->escapeStringOnix($idlocation)."' ORDER BY C.idcontract DESC LIMIT 1");
	return !empty($obtenerPlanDeServicio)? array_shift($obtenerPlanDeServicio):false;

}
 ////usuarios->>
 public static function obtenerUsuarioContrasenia(){
 	global $dbOnix;
 	$obtenerUsuarioContrasenia=self::sentenciaOnix("SELECT * FROM sid_user");
 	return !empty($obtenerUsuarioContrasenia)?$obtenerUsuarioContrasenia:false;
 }

 public static function obtenerEmpleadosOnyx(){
 	global $dbOnix;
 	return $obtenerEmpleadosOnyxQ=self::sentenciaOnix("SELECT * FROM sid_employee");


 }
 // public static function obtenerDatosCompletos(){
 // 	"SELECT U.iduser as usuario "
 // }

 public static function obtenerVencidosEnfechaCorporativosEN($fecha){
 	global $dbOnix;
 	return $obcoEn=self::sentenciaOnix("SELECT CT.idcustomer ,  CT.idloccust as idlocation from sid_contract CT where DATE_FORMAT(CT.dateservto,'%Y%m%d%') < '$fecha' AND (SELECT COUNT(idchasis) FROM sid_vehicle VH WHERE VH.idcustomer=CT.idcustomer and VH.idlocation =CT.idloccust) > 2  GROUP BY CT.idcustomer");

 }


public static function obtenerUltimoVencido($idcustomer,$idloccust){
	global $dbOnix;
	$obtenerCantididadVehiculos=self::sentenciaOnix("SELECT DATE_FORMAT(dateservto,'%Y%m%d') as hasta FROM sid_contract WHERE idcustomer ='".$dbOnix->escapeStringOnix($idcustomer)."' AND idloccust='$idloccust' ORDER BY idcontract DESC LIMIT 1 ");
	return !empty($obtenerCantididadVehiculos)?array_shift($obtenerCantididadVehiculos):false;	
}

public static function obtenerTotoLosVehiculos(){
    return $obtodoLosVh=self::sentenciaOnix("SELECT idcustomer,idvehicle,idlocation, dateservfrom as desde FROM sid_vehicle ORDER BY idcustomer");
}

public static function obtenerActivos($fecha){
 	return $obcoEn=self::sentenciaOnix("SELECT CT.idcustomer ,  CT.idloccust as idlocation from sid_contract CT where DATE_FORMAT(CT.dateservto,'%Y%m%d%') < '$fecha' AND (SELECT COUNT(idchasis) FROM sid_vehicle VH WHERE VH.idcustomer=CT.idcustomer and VH.idlocation =CT.idloccust) > 2  GROUP BY CT.idcustomer");
//    ####QUERYS CRUDOS AUN ----- SOLO CONTRATOS------
//#### para ver los contratos si dias pasados contrato esta en negativo tiene contrato activo,
//SELECT dateactivate, dateservfrom, dateservto, DATEDIFF(dateservto, dateservfrom) as diasContrato, DATEDIFF('2017-09-01',dateservto) as diasPasadosContrato  FROM sid_contract;
//###para ver los contratos activos hasta la fecha
//SELECT dateservfrom, dateservto, DATEDIFF(dateservto, dateservfrom) as diasContrato, DATEDIFF('2017-09-01',dateservto) as diasPasadosContrato  FROM sid_contract WHERE DATEDIFF('2017-09-01',dateservto) < 0;
//SELECT COUNT(dateservfrom) FROM sid_contract WHERE DATEDIFF('2017-09-01',dateservto) < 0;
//###para ver los contratos activos hasta la fecha
//SELECT  dateservfrom, dateservto, DATEDIFF(dateservto, dateservfrom) as diasContrato, DATEDIFF('2017-09-01',dateservto) as diasPasadosContrato  FROM sid_contract WHERE DATEDIFF('2017-09-01',dateservto) > 0;
//SELECT  count(dateservfrom)  FROM sid_contract WHERE DATEDIFF('2017-09-01',dateservto) > 0;
//####QUERYS CRUDOS AUN ----- SOLO CONTRATOS------j(SELECT dateservto FROM sid_contract c WHERE v.idlocation=c.idloccust AND v.idcustomer=c.idcustomer AND c.idvehicle = v.idvehicle)
}

public function obtenerVehiculos($fecha){
 	//return $obcoEn=self::sentenciaOnix("SELECT v.idcustomer, v.idvehicle,  v.idlocation FROM sid_vehicle v WHERE (SELECT MAX(dateservto) FROM sid_contract c WHERE v.idlocation=c.idloccust AND v.idcustomer=c.idcustomer AND c.idvehicle = v.idvehicle) < '2018-01-31' LIMIT 5"); 
// 	return $obcoEn=self::sentenciaOnix("SELECT v.idcustomer, v.idvehicle,  v.idlocation FROM sid_vehicle v INNER JOIN sid_contract c ON v.idlocation=c.idloccust AND v.idcustomer=c.idcustomer AND c.idvehicle = v.idvehicle    LIMIT 10"); 
// 	return $obcoEn=self::sentenciaOnix("SELECT v.idcustomer, v.idvehicle,  v.idlocation FROM sid_contract c INNER JOIN sid_vehicle v ON v.idlocation=c.idloccust AND v.idcustomer=c.idcustomer AND c.idvehicle = v.idvehicle    LIMIT 10"); 
 	return $obcoEn=self::sentenciaOnix("SELECT MIN(c.dateservfrom) desde,MAX(c.dateservto) hasta,COUNT(*) 'cantidad contratos',idcustomer,idvehicle,idloccust idlocation FROM  sid_contract c GROUP BY idcustomer,idloccust,idvehicle HAVING hasta < '$fecha' "); 
}

public function verSiTienenBaja(){
    #echo $this->idlocation." , ".$this->idcustomer." , ".$this->idvehicle;
    $sqlvstb=self::sentenciaOnix("SELECT o.* FROM sid_orderveh ov INNER JOIN sid_order o ON (o.idorder=ov.idorder) where ov.idcustomer='$this->idcustomer' and ov.idvehicle='$this->idvehicle' and ov.idloccust='$this->idlocation'  AND (o.descorder like 'BAJA%') ");
    return !empty($sqlvstb)?true:false; 
}
public function verSiTienenDEsinstalacion(){
    #echo $this->idlocation." , ".$this->idcustomer." , ".$this->idvehicle;
    $sqlvstb=self::sentenciaOnix("SELECT o.* FROM sid_orderveh ov INNER JOIN sid_order o ON (o.idorder=ov.idorder) where ov.idcustomer='$this->idcustomer' and ov.idvehicle='$this->idvehicle' and ov.idloccust='$this->idlocation'  AND ( o.descorder like 'DESINSTAL%') ");
    return !empty($sqlvstb)?true:false; 
}
public function verFindecontrato(){
    $vfc=self::sentenciaOnix("SELECT MAX(dateservto) as hasta FROM sid_contract WHERE idcustomer = '$this->idcustomer' AND idloccust='$this->idlocation' AND idvehicle='$this->idvehicle'");
    if (!empty($vfc)) {
        $datos=array_shift($vfc);
        return $datos->hasta;
    }else{
        return false;
    }
}

public function verINICIOdecontrato(){
    $vfc=self::sentenciaOnix("SELECT MIN(dateservto) as desde FROM sid_contract WHERE idcustomer = '$this->idcustomer' AND idloccust='$this->idlocation' AND idvehicle='$this->idvehicle'");
    if (!empty($vfc)) {
        $datos=array_shift($vfc);
        return $datos->desde;
    }else{
        return false;
    }
    
}

public function obtenerDatosDesdeIdsns(){
	global $dbOnix;
	$ObtenerDatosDesdeChasisQ=self::sentenciaOnix("SELECT V.idcustomer as idcustomer, V.idvehicle as idvehicle, V.idlocation as idlocation, V.idchasis as chasis, color, V.license as placa, V.yearmodel as anio, V.engine as motor, V.dateservfrom as desde, V.dateservto as hasta, V.descmodel as modelo, V.idbrand , M.descbrand as marca, C.identdoc as dni, C.lastname as apellidos, C.firstname as nombre , C.phonemain as celular, C.phonecell as celular2, C.mail as correo, C.phonework as telefonoTrabajo, C.comments as comentarios, C.mail2 as correo2 , C.faxwork as telefono, C.addressmain as  direccion FROM sid_vehicle V INNER JOIN sid_brand M ON V.idbrand=M.idbrand INNER JOIN sid_customer C ON (C.idcustomer=V.idcustomer AND C.idlocation=V.idlocation ) AND V.idvehicle= '$this->idvehicle' AND V.idlocation='$this->idlocation' AND V.idcustomer='$this->idcustomer' AND V.idlocation = '$this->idlocation'");
	return !empty($ObtenerDatosDesdeChasisQ)?array_shift($ObtenerDatosDesdeChasisQ):false;	
}
public function obtenerSim(){
    $osDeids=self::sentenciaOnix("SELECT S.idsim as serie, SS.sim as dni , SS.operator as anio FROM sid_serie S LEFT JOIN sid_sim SS ON SS.idsim=S.idsim WHERE idloccust='$this->idlocation' AND idvehicle='$this->idvehicle' AND idcustomer='$this->idcustomer' ");
    if(!empty($osDeids)){
        return array_shift($osDeids);
    }else{
        return false;
    }
}
    
public function obtenerPlanDeServicions(){
	global $dbOnix;
	$obtenerPlanDeServicio=self::sentenciaOnix("SELECT  A.descarticle as articulo, A.idarticle, C.idarticle  FROM sid_contract C INNER JOIN sid_article A ON C.idarticle=A.idarticle WHERE C.idcustomer=".$dbOnix->escapeStringOnix($this->idcustomer)." AND C.idvehicle=".$dbOnix->escapeStringOnix($this->idvehicle)." AND C.idloccust ='".$dbOnix->escapeStringOnix($this->idlocation)."' ORDER BY C.idcontract DESC LIMIT 1");
    $plan=array_shift($obtenerPlanDeServicio);
    return $plan->articulo;

}

public function obtenerTodolosActivosEnLaWeb(){
    return $qdotaew=self::sentenciaOnix("SELECT S.idcustomer,S.idvehicle,S.idloccust idlocation, S.idsim as serie, Con_Vence.hasta FROM sid_serie S  JOIN (SELECT MAX(dateservto)  as hasta , idcustomer,idloccust,idvehicle  FROM sid_contract GROUP BY idcustomer,idloccust,idvehicle) Con_Vence ON   (Con_Vence.idcustomer=S.idcustomer AND Con_Vence.idloccust=S.idloccust AND Con_Vence.idvehicle=S.idvehicle) WHERE S.idsim is not null and S.idcustomer is not null and S.idvehicle is not null ");
}

public function obtenerVehiculosPOridDAta($idcustomer,$idlocation){
    return $qdotaew=self::sentenciaOnix("SELECT S.idcustomer,S.idvehicle,S.idloccust idlocation, S.idsim as serie  FROM sid_serie S   WHERE S.idsim is not null and S.idcustomer is not null and S.idvehicle is not null AND S.idcustomer='$idcustomer' AND S.idloccust='$idlocation' ");
}

public function obtenerTodolosVencidosDeLaWeb(){
    return $qdotaew=self::sentenciaOnix("SELECT S.idvehicle,S.idcustomer,S.idloccust idlocation, S.idsim FROM sid_serie S WHERE S.idsim is not null and S.idcustomer is not null and S.idvehicle is not null");
}

public static function obtenerDatosDecontrato($idcustomer,$idvehicle,$idlocation){
    //$qdodd=self::sentenciaOnix("SELECT MAX(c.dateservto) hasta FROM  sid_contract c WHERE idcustomer='$this->idcustomer' AND idloccust='$this->idlocation' AND idvehicle='$this->idvehicle' ");
    $qdodd=self::sentenciaOnix("SELECT MAX(c.dateservto) hasta FROM  sid_contract c WHERE idcustomer='$idcustomer' AND idloccust='$idlocation' AND idvehicle='$idvehicle'");
    return !(empty($qdodd))?array_shift($qdodd):false;
}

public function obtenerIdclientePorDatosDeVhCantidad($datos){
    $oicpdd=self::sentenciaOnix("SELECT COUNT(*) as cantidad FROM sid_vehicle WHERE idchasis like '%$datos' OR license like '%$datos%' OR lastnameuser like '%$datos%' OR firstnameuser like '%$datos%' LIMIT 1");
    $oicpdd=array_shift($oicpdd);
    return $oicpdd->cantidad;
}

public function obtenerIdclientePorDatosDeVh($datos, $posisionDePagina,$registrosPorPagina ){
    $oicpdd=self::sentenciaOnix("SELECT idcustomer, idlocation, idvehicle , lastnameuser as apellidos, firstnameuser as nombre , license placa , idchasis chasis  FROM sid_vehicle WHERE idchasis like '%$datos' OR license like '%$datos%' OR lastnameuser like '%$datos%' OR firstnameuser like '%$datos%' LIMIT $posisionDePagina ,  $registrosPorPagina");
    return !empty($oicpdd)?$oicpdd:false;
}

public function obtenerClientesConMTCAsignado($posisionDePagina, $registrosPorPagina){
	$onix = new baseDatosOnix();
	// $occma = $onix->queryOnix("SELECT cus.identdoc ,sa.descarticle, sv.idchasis, sv.license,sc.statcontract, DATE_FORMAT(sc.datemodif, '%d-%m-%Y') AS formatted_date 
	// 	FROM sid_contract sc
	// 	INNER JOIN sid_article sa ON sc.idarticle = sa.idarticle
	// 	INNER JOIN sid_vehicle sv ON sv.idcustomer = sc.idcustomer
	// 	INNER JOIN sid_customer cus ON sc.idcustomer = cus.idcustomer
	// 	WHERE sv.idarea IS NULL 
	// 	AND sa.descarticle LIKE '%MTC%'
	// 	AND sc.dateservto > NOW()
	// 	ORDER BY sc.datemodif DESC
	// 	LIMIT $posisionDePagina, $registrosPorPagina
	// ");
	$occma = $onix->queryOnix("SELECT * FROM vw_getClientsMTC LIMIT $posisionDePagina, $registrosPorPagina");
	$result = mysqli_fetch_all($occma);

	return !empty($result) ? $result : false;
}

public function obtenerCantidadClientesConMTC(){
	// $occcm = self::sentenciaOnix("SELECT COUNT(*) as cantidad FROM sid_order o 
	// 	INNER JOIN sid_customer c ON o.idcustomer = c.idcustomer INNER JOIN sid_vehicle v ON o.idcustomer = v.idcustomer WHERE o.descorder LIKE '%MTC%' AND o.statorder = 'P'
	// 	ORDER BY o.dateorder DESC");

	$occcm = self::sentenciaOnix("SELECT COUNT(*) as cantidad FROM sid_contract sc
		INNER JOIN sid_article sa ON sc.idarticle = sa.idarticle
		INNER JOIN sid_vehicle sv ON sv.idcustomer = sc.idcustomer
		INNER JOIN sid_customer cus ON sc.idcustomer = cus.idcustomer
		WHERE sv.idarea IS NULL
		AND sa.descarticle LIKE '%MTC%'
		AND sc.dateservto > NOW()
	ORDER BY sc.datemodif DESC");

	return !empty($occcm) ? array_shift($occcm) : false;	
}

// public function datosParaEnviarCredencailes($dni_ruc){
// 	$onix = new baseDatosOnix();
// 	$occma = $onix->queryOnix("SELECT CONCAT(sc.firstname, ' ', sc.lastname) AS cliente, sc.mail AS correo, sc.identdoc AS usuario, sc.customerpass AS contra
// 	FROM sid_customer sc
// 	WHERE sc.identdoc = '$dni_ruc'
// 	");
// 	$result = mysqli_fetch_all($occma);

// 	return !empty($result) ? $result : false;
// }

//AGREGADO para buscador RESH por chasis

public static function obtenerIdclientePorChasis($chasis){
	global $dbOnix;
	$chasis=$dbOnix->escapeStringOnix($chasis);
	$ObtenerDatosDesdeChasisQ=self::sentenciaOnix("SELECT V.idcustomer as idcustomer, V.idvehicle as idvehicle, V.idlocation as idlocation, V.idchasis as chasis, color, V.license as placa, V.yearmodel as anio, V.engine as motor, V.dateservfrom as desde, V.dateservto as hasta, V.descmodel as modelo, V.idbrand , M.descbrand as marca, C.identdoc as dni, C.lastname as apellidos, C.firstname as nombre , C.phonemain as celular, C.phonecell as celular2, C.mail as correo, C.phonework as telefonoTrabajo, C.comments as comentarios, C.mail2 as correo2 , C.faxwork as telefono, C.addressmain as  direccion FROM sid_vehicle V INNER JOIN sid_brand M ON V.idbrand=M.idbrand INNER JOIN sid_customer C ON (C.idcustomer=V.idcustomer AND C.idlocation=V.idlocation ) AND V.idchasis= '$chasis' AND V.idlocation='11' ORDER BY V.dateservfrom DESC LIMIT 1");
	return !empty($ObtenerDatosDesdeChasisQ)?array_shift($ObtenerDatosDesdeChasisQ):false;	

}
// public function obtenerBuscarClientesCantidadOnix($criterio){
// 	global $dbOnix;
// 	$result = self::sentenciaOnix("SELECT count(*) as cantidad 
// 								FROM  
// 								sid_customer 
// 								WHERE 
// 								identdoc LIKE '%".$dbOnix->escapeStringOnix($criterio)."%'
// 								OR firstname LIKE '%".$dbOnix->escapeStringOnix($criterio)."%' 
// 								OR lastname LIKE '%".$dbOnix->escapeStringOnix($criterio)."%'
// 								OR CONCAT(firstname, ' ', lastname) LIKE '%".$dbOnix->escapeStringOnix($criterio)."%'
// 								OR CONCAT(lastname, ' ', firstname) LIKE '%".$dbOnix->escapeStringOnix($criterio)."%'");
// 	return !empty($result) ? array_shift($result) : false;		
// }

public function obtenerBuscarClientesCantidadOnix($criterio)
{
    global $dbOnix;
    
    $UnCriterio = $dbOnix->escapeStringOnix($criterio);
    
    $query = "SELECT count(*) as cantidad 
              FROM sid_customer 
              WHERE CONCAT(firstname, ' ', lastname) LIKE '%$UnCriterio%'
              OR CONCAT(lastname, ' ', firstname) LIKE '%$UnCriterio%'  
              OR identdoc LIKE '%$UnCriterio%'";
    
    $result = self::sentenciaOnix($query);
    
    return array_shift($result);
}


// public function obtenerBuscarClientesPaginacionOnix($criterio, $posisionDePagina, $registrosPorPagina){
//     global $dbOnix;
//     $result = self::sentenciaOnix("SELECT * FROM sid_customer  
// 								WHERE CONCAT(firstname, ' ', lastname) LIKE '%".$dbOnix->escapeStringOnix($criterio)."%'
// 								OR CONCAT(lastname, ' ', firstname) LIKE '%".$dbOnix->escapeStringOnix($criterio)."%'  
// 								OR identdoc LIKE '%".$dbOnix->escapeStringOnix($criterio)."%'
// 								OR firstname LIKE '%".$dbOnix->escapeStringOnix($criterio)."%' 
// 								OR lastname LIKE '%".$dbOnix->escapeStringOnix($criterio)."%'
//                                 ORDER BY idcustomer DESC 
//                                 LIMIT $posisionDePagina, $registrosPorPagina");
//     return !empty($result) ? $result : false;
// }

public function obtenerBuscarClientesPaginacionOnix($criterio, $posisionDePagina, $registrosPorPagina){
    global $dbOnix;
    
    $UnCriterio = $dbOnix->escapeStringOnix($criterio);
    
    $query = "SELECT idcustomer, identdoc, firstname, lastname, mail 
              FROM sid_customer  
              WHERE CONCAT(firstname, ' ', lastname) LIKE '%$UnCriterio%'
              OR CONCAT(lastname, ' ', firstname) LIKE '%$UnCriterio%'  
              OR identdoc LIKE '%$UnCriterio%'
              ORDER BY idcustomer DESC 
              LIMIT $posisionDePagina, $registrosPorPagina";
    
    $result = self::sentenciaOnix($query);
    
    return !empty($result) ? $result : false;
}



	public function obtenerClientesConMTCAsignadoDescargarExcel(){
		$onix = new baseDatosOnix();
		$occma = $onix->queryOnix("SELECT cus.identdoc ,sa.descarticle, sv.idchasis, sv.license,sc.statcontract, DATE_FORMAT(sc.datemodif, '%d-%m-%Y') AS formatted_date 
			FROM sid_contract sc
			INNER JOIN sid_article sa ON sc.idarticle = sa.idarticle
			INNER JOIN sid_vehicle sv ON sv.idcustomer = sc.idcustomer
			INNER JOIN sid_customer cus ON sc.idcustomer = cus.idcustomer
			WHERE sv.idarea IS NULL 
			AND sa.descarticle LIKE '%MTC%'
			AND sc.dateservto > NOW()
			ORDER BY sc.datemodif DESC
		");
		$result = mysqli_fetch_all($occma);

		return !empty($result) ? $result : false;
	}

	//Para lo de enviar credenciales :D

	// public function datosParaEnviarCredencailes($dni_ruc){
	// 	$onix = new baseDatosOnix();
	// 	$occma = $onix->queryOnix("SELECT CONCAT(sc.firstname, ' ', sc.lastname) AS cliente, sc.mail AS correo, sc.identdoc AS usuario, sc.customerpass AS contra
	// 	FROM sid_customer sc
	// 	WHERE sc.identdoc = '$dni_ruc'");
	// 	$result = mysqli_fetch_all($occma);
	// }

	// public function datosParaEnviarCredenciales($dni_ruc){
	// 	$onix = new baseDatosOnix();

	// 	$occma = $onix->queryOnix("SELECT sa.descarticle,sc.idcustomer, CONCAT(sc.firstname, ' ', sc.lastname) AS cliente, 
	// 	sc.mail AS correo, sc.identdoc AS dniRuc  FROM sid_customer sc
	// 	INNER JOIN sid_contract scc ON sc.idcustomer = scc.idcustomer
	// 	INNER JOIN sid_article sa ON scc.idarticle = sa.idarticle
	// 	WHERE sc.identdoc = '$dni_ruc'
	// 	ORDER BY scc.datecreate DESC
	// 	LIMIT 1");

	// 	$result = mysqli_fetch_assoc($occma);
	
	// 	return $result ? $result : false;
	// }

	public function datosParaEnviarCredenciales($idcustomer, $idvehicle){
		$onix = new baseDatosOnix();
		
		$occma = $onix->queryOnix("SELECT 
		CONCAT(cus.firstname, ' ', cus.lastname) AS cliente, 
		cus.mail AS correo,
		sc.idcontract, 
		sc.idcustomer, 
		sc.idvehicle, 
		sa.idarticle, 
		sa.descarticle,
		CASE 
			WHEN LOWER(sa.descarticle) LIKE '%track%' OR LOWER(sa.descarticle) LIKE 'a track%' THEN 'TRACK'
			WHEN LOWER(sa.descarticle) LIKE '%e-call%' OR LOWER(sa.descarticle) LIKE 'a e-call%' THEN 'OCTO'
			ELSE NULL 
		END AS sistema,
		CASE 
			WHEN LOWER(sa.descarticle) LIKE '%interactivo%' OR LOWER(sa.descarticle) LIKE 'a interactivo%' THEN 'INTERACTIVO'
			WHEN LOWER(sa.descarticle) LIKE '%control%' OR LOWER(sa.descarticle) LIKE 'a control%' THEN 'CONTROL TOTAL'
			ELSE NULL 
		END AS tipoTrack
	FROM 
		sid_contract sc
	INNER JOIN 
		sid_customer cus ON sc.idcustomer = cus.idcustomer
	INNER JOIN 
		sid_article sa ON sc.idarticle = sa.idarticle
	WHERE 
		sc.idcustomer = '$idcustomer' AND sc.idvehicle = '$idvehicle'
	ORDER BY 
		sc.datecreate DESC 
	LIMIT 1;
	
	");

		$result = mysqli_fetch_assoc($occma);
	
		return $result ? $result : false;
	}

	// public function chasisPlacaPorDniRuc($dni_ruc){
	// 	$onix = new baseDatosOnix();

	// 	$occma = $onix->queryOnix("SELECT sv.idchasis, sv.license FROM sid_customer sc
	// 	INNER JOIN sid_vehicle sv ON sc.idcustomer = sv.idcustomer
	// 	WHERE sc.identdoc = '$dni_ruc'");

	// 	$result = mysqli_fetch_assoc($occma);
	
	// 	return $result ? $result : false;
	// 	//return !empty($result) ? $result : [];
	// }

	public function chasisPlacaPorDniRuc($dni_ruc) {
		$onix = new baseDatosOnix();
	
		$occma = $onix->queryOnix("SELECT sv.idchasis, sv.license,sc.idcustomer, sv.idvehicle FROM sid_customer sc
		INNER JOIN sid_vehicle sv ON sc.idcustomer = sv.idcustomer
		WHERE sc.identdoc = '$dni_ruc'");
	
		$result = array();
		while ($row = mysqli_fetch_assoc($occma)) {
			$result[] = $row;
		}
	
		return $result; // Devolverá un array vacío si no hay resultados
	}
	
	

	//PARA RECLAMOS
	public function chasisOnixPorIdCustomer($idcustomer){
		$result = self::sentenciaOnixV2("SELECT idchasis FROM sid_vehicle WHERE idcustomer = '$idcustomer' ORDER BY idcustomer ASC");
		return !empty($result) ? $result : [];		
	}

	public function chasislOnixPorIdChasis($idchasis){
		global $dbOnix;
		$result = self::sentenciaOnixV2("SELECT idchasis FROM sid_vehicle WHERE idchasis = '$idchasis'");
		return !empty($result) ? $result : [];		
	}
//860ryp0nz-EZ
	public function obtenerClientesConMTCAsignadoV2(){
		global $dbOnix;
		$result = self::selectAllOnix("SELECT sv.aliasvehicle, sv.descmodel, scs.idcustomer, sv.idvehicle,  sb.descbrand, sv.descmodel , scs.identdoc ,sa.descarticle, sv.idchasis, sv.license,sc.statcontract, sc.datecreate AS formatted_date 
		FROM sid_vehicle sv
		INNER JOIN sid_contract sc ON sv.idcustomer = sc.idcustomer AND sv.idvehicle = sc.idvehicle
		INNER JOIN sid_article sa ON sc.idarticle = sa.idarticle
		INNER JOIN sid_customer scs ON sc.idcustomer = scs.idcustomer
		INNER JOIN sid_brand sb ON sv.idbrand = sb.idbrand
		WHERE sv.idarea IS NULL AND sa.descarticle LIKE '%MTC%' AND sc.dateservto > NOW()");
		return !empty($result) ? $result : [];		
	}

	//860ryp0nz-EZ
	public function clientesMTCdeListaNegra(){
		global $dbOnix;
		// $list = '';
		// $contarList = count($lista);
		// foreach($lista as $key => $value){ 
		// 	if($contarList == $key + 1){
		// 		$list .="(scs.idcustomer='".$value['idcustomer']."' AND sv.idvehicle='".$value['idvehicle']."')";
		// 	} else{
		// 		$list .="(scs.idcustomer='".$value['idcustomer']."' AND sv.idvehicle='".$value['idvehicle']."') OR ";
		// 	}
		// }

		// $wherequery = '';
		// if($contarList > 0){
		// 	$wherequery = 'AND ('.$list.')';
		// } else{
		// 	$wherequery = 'LIMIT 0';
		// } $wherequery

		$result = self::selectAllOnix("SELECT scs.idcustomer, sv.idvehicle, scs.identdoc ,sa.descarticle, sv.idchasis, sv.license,sb.descbrand, sv.descmodel,sc.statcontract, sc.datecreate AS formatted_date 
		FROM sid_vehicle sv
		INNER JOIN sid_contract sc ON sv.idcustomer = sc.idcustomer AND sv.idvehicle = sc.idvehicle
		INNER JOIN sid_article sa ON sc.idarticle = sa.idarticle
		INNER JOIN sid_customer scs ON sc.idcustomer = scs.idcustomer
		INNER JOIN sid_brand sb ON sv.idbrand = sb.idbrand
		WHERE sv.idarea IS NULL AND sa.descarticle LIKE '%MTC%' AND sc.dateservto > NOW()
		
	");
		return !empty($result) ? $result : [];		
	}
// gestionDeAccesos
	public function obtenerCorreoPorChasis($chasis){
		
		$onix = new baseDatosOnix();
		
		$occma = $onix->queryOnix("SELECT CONCAT(sc.firstname, ' ', sc.lastname) AS cliente, sc.mail FROM sid_vehicle sv
		INNER JOIN sid_customer sc ON sv.idcustomer = sc.idcustomer
		WHERE sv.idchasis = '$chasis'");
		//return !empty($result) ? $result : [];

		$result = mysqli_fetch_assoc($occma); // Para una sola línea
	
		return $result ? $result : false;		
	}
	
}
}
?>


<?php 
if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}
 ?>
<div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="#">
                <div class="form-group">
                    <input type="text" placeholder="Buscar aquí "  class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">

            <li></li>

                <li>
                    <span class="m-r-sm text-muted welcome-message">Bienvenido a Resh.</span>
                </li>               
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#"><?php $notificacionOCan=Notificaciones::obtenerCantidadNotificaciones(); ?>
                        <i class="fa fa-bell"></i><span class="<?= ($notificacionOCan->cantidad>=1) ? 'label label-primary' : '' ; ?> <?= ($data=Notificaciones::obtenerRegularizacionesPARAOPEYSACCantidad())?'label label-primary':'' ?>"><?php  echo  $a = ($notificacionOCan->cantidad>=1) ? $notificacionOCan->cantidad : '' ?><?= ($data=Notificaciones::obtenerRegularizacionesPARAOPEYSACCantidad())?$data:'' ?></span>
                    </a>
                        <?php $notifiObj=Notificaciones::obtenerNotificaciones() ?>
                        <?php $notifiObj2=Notificaciones::obtenerRegularizacionesPARAOPEYSAC() ?>
                    <ul class="dropdown-menu dropdown-alerts">
                        <?php if ($notifiObj){ ?>
                        <?php foreach ($notifiObj as $var): ?>
                        <li>
                            <a href="<?php echo $var->link ?>">
                                <div>
                                    <i class="<?php echo $var->tipoAlerta ?> "> &nbsp;</i><b><?php echo $var->asunto ?></b><br><?= $var->detalles ?>
                                    <span class="pull-right text-muted small"> <?php echo haceMinimo ($var->fechaRegistro);
                                    ?></span>
                                </div>
                            </a>
                        </li><br>
                        <li class="divider"></li>
                        <?php endforeach ?>
                        <?php }else if($notifiObj2){ ?>
                        <?php foreach ($notifiObj2 as $var1): ?>
                        <li>
                            <a href="<?php echo $var1->link ?>">
                                <div>
                                    <i class="<?php echo $var1->tipoAlerta ?> "> &nbsp;</i><b><?php echo $var1->asunto ?></b><br><?= $var1->detalles ?>
                                    <span class="pull-right text-muted small"> <?php echo haceMinimo ($var1->fechaRegistro);
                                    ?></span>
                                </div>
                            </a>
                        </li><br>
                        <li class="divider"></li>
                        <?php endforeach ?>
                        <?php }else{ ?>
                        <li>
                            <div>
                                Sin notificaciones
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                      
                </li>


                <li>
                    <a href="../salir.php">
                        <i class="fa fa-sign-out"></i> Salir
                    </a>
                </li>
            </ul>

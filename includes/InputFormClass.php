<?php
class InputFormClass
{

    function __construct()
    {
    }

    /**
     * Control: input hidden
     */
    function getInputHidden($params)
    {
        $params = [
            'value' => isset($params['value']) ? $params['value'] : '',
            'name' => isset($params['name']) ? $params['name'] : '',
            'id' => isset($params['name']) ? $params['id'] : ''
        ];

        $template = function ($params) {
            extract($params);
            return "<input type=\"hidden\" id=\"$id\" name=\"$name\" value=\"$value\">";
        };

        return $template($params);
    }
    /**
     * Control: input text or input textarea
     */
    function getInputText($params)
    {
        $params = [
            'type' => isset($params['type']) ? $params['type'] : '',
            'name' => isset($params['name']) ? $params['name'] : '',
            'id' => isset($params['name']) ? $params['id'] : '',
            'label' => isset($params['label']) ? $params['label'] : '',
            'placeholder' => isset($params['label']) ? $params['placeholder'] : '',
            'value' => isset($params['value']) ? $params['value'] : '',
            'required' => isset($params['required']) ? (($params['required'] == True) ? 'required' : '') : '',
            'disabled' => isset($params['disabled']) ? (($params['disabled'] == True) ? 'disabled' : '') : '',
            'readonly' => isset($params['readonly']) ? (($params['readonly'] == True) ? 'readonly' : '') : ''
        ];

        $template = function ($params) {
            extract($params);
            if ($type == "text") {
                return "<label for=\"$id\">$label" . (($required) ? "<span style='color:red;'>(*)</span>" : "") . " :</label>
                        <input style=\"border: none;\" type=\"$type\"  $disabled $readonly placeholder=\"$placeholder\" id=\"$id\" name=\"$name\" class=\"border-bottom form-control x2 \" $required  value=\"$value\">";
            } elseif ($type == "textarea") {
                return "<label for=\"$id\">$label" . (($required) ? "<span style='color:red;'>(*)</span>" : "") . " :</label>
                        <textarea style=\"border: none;\" type=\"$type\" $disabled $readonly placeholder=\"$placeholder\" id=\"$id\" name=\"$name\" class=\"border-bottom form-control x2 \" $required >$value</textarea>";
            } else {
                return "";
            }
        };

        return $template($params);
    }

    /**
     * Control select
     */
    function getInputSelect($params)
    {
        $params = [
            'name' => isset($params['name']) ? $params['name'] : '',
            'id' => isset($params['name']) ? $params['id'] : '',
            'label' => isset($params['label']) ? $params['label'] : '',
            'placeholder' => isset($params['placeholder']) ? $params['placeholder'] : '',
            //label por placeholder :v
            'value' => isset($params['value']) ? $params['value'] : '',
            'data' => isset($params['data']) ? $params['data'] : '',
            'keyValue' => isset($params['keyValue']) ? $params['keyValue'] : '',
            'keyLabel' => isset($params['keyLabel']) ? $params['keyLabel'] : '',
            'required' => isset($params['required']) ? (($params['required'] == True) ? 'required' : '') : ''
        ];

        $template = function ($params) {
            extract($params);
            $placeholder = isset($placeholder) ? $placeholder : "[Seleccione]";
            $options = '<option value="" disabled selected>' . $placeholder . '</option>';
            foreach ($data as $row) {
                if ($value == $row->$keyValue) {
                    $options .= '<option selected value="' . $row->$keyValue . '">' . $row->$keyLabel . '</option>';
                } else {
                    $options .= '<option value="' . $row->$keyValue . '">' . $row->$keyLabel . '</option>';
                }
            }

            return "<label for=\"$id\">$label" . (($required) ? "<span style='color:red;'>(*)</span>" : "") . " :</label>
                <select $required style=\"border: none;\" name=\"$name\" id=\"$id\" class=\"border-bottom form-control text-capitalize valid\">
                " . $options . "
                </select>";
        };

        return $template($params);
    }




}
?>
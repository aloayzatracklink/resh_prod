<?php


class CTBProgramacion extends Recursos
{

    public $idtablaProgramacion;
    public $fecha;
    public $hora;
    public $estado;
    public $idoperacion;
    public $idcita;
    public $fechaRegistro;
    public $idusuario;
    public $rpara;
    public $idtecnico;
    public $idturno;
    public $sede;
    public $observacion;
    public $fijo;
    public $horafin;
    public $ubicacion;


    public $tth_instalacion;
    public $tth_trasalado;
    public $tt;

    public $tco;
    public $tcc;
    public $tdo;
    public $tdc;
    public $tto;
    public $ttc;

    public function obtenerTablaProgramacion($fecha,$ubicacion,$distrito,$operacion,$tthinstala,$lugart,$lTec){

        global $db;
        global $session;
        $resulObj=$db->queryProc("CALL sp_list_tbprogramacion2('$fecha','$ubicacion','$distrito','$operacion','$tthinstala','$session->idusuario','$lugart','$lTec') ");
        $objArray=array();
        while($row=mysqli_fetch_array($resulObj)){
            $objArray[]= $row;
        }

      //  $obj=self::sentencia("CALL sp_list_tbprogramacion2('$fecha','Domicilio',1,'$operacion') ");
        return ($objArray)?$objArray:false;
    }


    public function  ObtenertbProgramaxIdOpe($idOpera,$fecha,$hora){
        $objTP=self::sentencia("SELECT idtablaProgramacion, fecha, hora, estado, idoperacion, idcita, fechaRegistro, idusuario, idtecnico, idturno, observacion, rpara, tth_instalacion, tth_trasalado, ADDTIME(horafin,'00:05') as horafin, horaTraslado, iddistrito, ubicacion  FROM tablaProgramacion WHERE  idoperacion='$idOpera' and fecha='$fecha' and hora='$hora' ORDER BY idtablaProgramacion ");
        return !empty($objTP)?array_shift($objTP):false;
    }


    public function  ObtenertbProgramaxIdCita($idOpera,$fecha,$hora){
        $objTP=self::sentencia("SELECT idtablaProgramacion, fecha, hora, estado, idoperacion, idcita, fechaRegistro, idusuario, idtecnico, idturno, observacion, rpara, tth_instalacion, tth_trasalado, ADDTIME(horafin,'00:05') as horafin, horaTraslado, iddistrito, ubicacion  FROM tablaProgramacion WHERE  idcita='$idOpera' and fecha='$fecha' and hora='$hora' ORDER BY idtablaProgramacion ");
        return !empty($objTP)?array_shift($objTP):false;
    }

    public function  ObtenertbProgramaxIdTablaProgramacion($idTablaProgramacion,$fecha){
        $objTP=self::sentencia("SELECT idtablaProgramacion, fecha, hora, estado, idoperacion, idcita, fechaRegistro, idusuario, idtecnico, idturno, observacion, rpara, tth_instalacion, tth_trasalado, ADDTIME(horafin,'00:05') as horafin, horaTraslado, iddistrito, ubicacion FROM tablaProgramacion WHERE  idtablaProgramacion='$idTablaProgramacion'  and fecha='$fecha'   ORDER BY idtablaProgramacion ");
        return !empty($objTP)?array_shift($objTP):false;
    }

    public function  ObtenertimeTraslado($fecha,$iddist,$idtecnico,$ubicacion){
        global $db;
        $resulObj=$db->queryProc("CALL fn_calculartraslado('$fecha','$iddist','$idtecnico','$ubicacion') ");
        $objArray=array();
        while($row=mysqli_fetch_array($resulObj)){
            $objArray[]= $row;
        }

        //  $obj=self::sentencia("CALL sp_list_tbprogramacion2('$fecha','Domicilio',1,'$operacion') ");
        return ($objArray)?$objArray:false;
    }

    public function  ObtenertimeTrasladoDisponibilidad($fecha,$idtecnico){
        global $db;
        $resulObj=$db->queryProc("CALL fn_calculartrasladodisponibilidad('$fecha','$idtecnico') ");
        $objArray=array();
        while($row=mysqli_fetch_array($resulObj)){
            $objArray[]= $row;
        }

        //  $obj=self::sentencia("CALL sp_list_tbprogramacion2('$fecha','Domicilio',1,'$operacion') ");
        return ($objArray)?$objArray:false;
    }

    public function obtenerDisponibilidad($fecha,$operacion){

        global $db;
        $resulObj=$db->queryProc("CALL sp_list_disponibilidad('$fecha','$operacion')");
        $objArray=array();
        while($row=mysqli_fetch_array($resulObj)){
            $objArray[]= $row;
        }

        return ($objArray)?$objArray:false;
    }


    public function  ObtenertbUbicacion($ubica){
        $objTP=self::sentencia("SELECT count(*) tt FROM ubicacion where codigo='$ubica'");
        return !empty($objTP)?array_shift($objTP):false;
    }



    public function  ObtenerleyendaTbProg($fecha){
        $objTP=self::sentencia("
SELECT COALESCE((sum(((case when ubica='Concesionario' and trabajo='opera' then tt end)))),0)tco,
 COALESCE((sum(((case when ubica='Concesionario' and trabajo='central' then tt end)))),0)tcc,
 COALESCE((sum(((case when ubica='Domicilio' and trabajo='opera' then tt end)))),0)tdo,
 COALESCE((sum(((case when ubica='Domicilio' and trabajo='central' then tt end)))),0)tdc,
 COALESCE((sum(((case when ubica='Taller ' and trabajo='opera' then tt end)))),0)tto,
 COALESCE((sum(((case when ubica='Taller ' and trabajo='central' then tt end)))),0)ttc
FROM 
(
	SELECT DISTINCT CASE WHEN idcita<>0 THEN 'Taller' ELSE tp.ubicacion END ubica,
	CASE WHEN (idcita<>0) THEN 'opera' ELSE CASE WHEN o.trabajo<>'Instalacion' THEN 'central' ELSE 'opera' END END trabajo,
	count(*) tt
	FROM tablaProgramacion tp
	LEFT JOIN operaciones o ON tp.idoperacion=o.idoperacion 
	WHERE fecha='$fecha' AND (tp.ubicacion<>'' OR idcita<>0) and tp.ubicacion IS NOT NULL
	AND idtecnico<>0 and tp.estado<>'321' 
	GROUP BY CASE WHEN idcita<>0 THEN 'Taller' ELSE tp.ubicacion END ,
	CASE WHEN (idcita<>0) THEN 'opera' ELSE CASE WHEN o.trabajo<>'Instalacion' THEN 'central' ELSE 'opera' END END 
)core ;");
        return !empty($objTP)?array_shift($objTP):false;
    }

}
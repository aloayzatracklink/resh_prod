<?php 
  include_once("init.php");
  $fecha=$_POST['fecha'];
  $calObj=new Calendario;
  $date = new DateTime($fecha);
  $horasAntes = new DateTime($fecha);
  $unaHorasAntes= new DateTime($fecha);
  $mas4=$date->format("Y/m/d H:i ");
  $mas2=$date->format("Y/m/d H:i ");
  $unaHorasAntes->modify("+1 hour");
  $desde=$unaHorasAntes->format("Y/m/d H:i ");
  $horasAntes->modify("-90 minutes");
  $cantidad=$calObj->obtenerTareasProgramadasCantidad($horasAntes->format("Y/m/d H:i "),$mas4);  
  $cantidadtt=$calObj->obtenerTareasProgramadasTaller($horasAntes->format("Y/m/d H:i "),$mas4);  
  $calendaDaC=$calObj->obtenerTareasProgramadasHora($fecha);
  $tecnicoData=Usuario::obtenerTecnicos2COntar();
  if (isset($_POST['idtecnico'])) {
    if ($_POST['idtecnico']!=0) {
      $calObj->resourceId=$_POST['idtecnico']."<br>";
      $horasAntes->format("Y/m/d H:i ")." ".$desde;
      if($res=$calObj->obtenerTecnicoAsignadoEnFecha($horasAntes->format("Y/m/d H:i "),$desde)){
        $teAsignar=Usuario::obtenerUsuario($_POST['idtecnico']);
        ?>
    
         <div class="row">
            <div class="col-lg-12">              
              <h4  class="text-center text-danger">TECNICO <strong><?= strtoupper($teAsignar->usuarioNombre)." ".strtoupper($teAsignar->usuarioApellidoPaterno) ?></strong> OCUPADO EN EL HORARIO SELECCIONADO, SOLICITAR CAMBIO DE HORA O CAMBIAR DE TECNICO</h4>
            </div>
          </div>
          <script>capacidadTec()</script>          
        <?php
      }else{
        ?>
          <script>limcapacidadTec()</script>  
        <?php
        // echo "ok";        
      }      
    }
  }
 ?>
 <div class="row">
   <div class="col-md-12">
     <table class="table table-bordered table-striped">
  <tbody>
    <tr>
      <th>Fecha</th>
      <th>Tecnicos Disponible</th>
      <th>Actividades</th>
      <th>Limite de espacio en taller</th>
      <th>Acción</th>
    </tr>
    <tr>
      <td><?= $fecha ?></td>
      <td><?= $tecnicoData->cantidad ?></td>
      <td><?= $calendaDaC->cantidad ?>  de <?= $cantidad->cantidad ?></td>
      <td><?= $cantidadtt->cantidad ?> de 8</td>
      <script>
      var cantidad='<?= $cantidadtt->cantidad ?>'
        if (cantidad>=8) {
          capacidad(cantidad);
        }else{
          limpiarCapacidad();
        }
      </script>
      <td><div class="btn btn-primary esconderPro" onclick="esconder()">Esconder</div></td>
    </tr>
  </tbody>
  </table> 
   </div>
 </div>
  <?php 
      $usuario=Usuario::obtenerUsuario($session->idusuario);
  ?>
  <?php if ($usuario->idrol!=4){ ?>
    
<div class="row">
<!--     <div class="col-lg-1">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="fc-event-inner"></div>  
                <div id='external-events'>
                    <h4></h4>       
                    <p>
                        <img src="../assets/calendario/assets/img/trashcan.png" id="trash" alt="">
                    </p>
                    <p><small>Instalación taller <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background: #1ab394;"></span></small></p>
                    <p><small>Instalación Concecionario <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background: #23c6c8;"></span></small></p>
                    <hr>
                    <p><small>Instalación Domicilio <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background: #2ea1ea;"></span></small></p><hr>
                    <p><small>Cita Taller <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background: #c2c4d3;"></span></small></p>
                    <hr>
                    <p><small>Ok monitoreo <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background:#84c600;"></span></small></p>
                    <hr>
                    <p><small>Pendiente monitoreo <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background:#f48b97;"></span></small></p>
                    <hr>
                    <p><small>Chequeo domicilio <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background:#0239ff;"></span></small></p>
                    <hr>
                    <p><small>Chequeo Concecionario <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background:#8ef5ff;"></span></small></p>
                    <hr>
                    <p><small>Chequeo Taller <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background:#48874a;"></span></small></p>
                    <hr>
                </div>
            </div>
        </div>        
    </div> -->
    <div class="col-lg-12">
        <div class="ibox float-e-margins">           
            <div class="ibox-content">
                <div id="calendar"></div>
            </div>
        </div>
    </div>
</div>  
    <script src="../assets/calendario/moment.min.js"></script>
    <script src="../assets/calendario/fullcalendar.min.js"></script>
    <script src='../assets/calendario/scheduler.min.js'></script>
    <script src="../assets/calendario/dist/sweetalert.min.js"></script>
    <script>    
    $.ajax({
            url: '../assets/calendario/process',
            type: 'POST',
            data: 'type=fetch',
            async: false,
            success: function(s){
                json_events = s;
            }
    }); 
        $('#calendar').fullCalendar({
            events: JSON.parse(json_events),
            utc: true,
            aspectRatio: 5,
            schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
            height:500,
            firstDay:1,
            contentHeight:600,
            slotEventOverlap:false,
            slotLabelFormat:'H:mm', 
            defaultDate:'<?= $mas4 ?>',          
            // slotLabelFormat:'h(:mm)a',            
            minTime:'07:00:00',            
            maxTime:'20:00:00',            
            scrollTime:'09:00:00',  
            buttonText:{today:'Hoy',month:'Vista mensual', week: 'Vista Semanal',timelineDay:'Por tecnico'},
            nowIndicator: true,
            eventBackgroundColor: '#fff',
            monthNames:['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
            'Agosto', 'Setiembre', 'Octubre', 'Nobiembre', 'Diciembre'],
            monthNamesShort:['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun','Jul', 'Ago', 'Set', 'Oct', 'Nov', 'Dec'],
            dayNames:['Domingo', 'Lunes', 'Martes', 'Miercoles','Jueves', 'Viernes', 'Sabado'],
            dayNamesShort:['Dom', 'Lun', 'Mar', 'Mier', 'Jue', 'Vie', 'Sab'],

header: {
    left: 'prev,next today',
    center: 'title',
    right: 'agendaWeek, timelineDay'
},
<?php if($usuario->idrol==3 ): ?>
    editable: true,
    droppable: true, 
<?php else: ?>
    editable: false,
    droppable: false, 
<?php endif ?>
slotDuration: '01:00:00',
defaultView: 'timelineDay',
businessHours: {    
    dow: [ 1, 2, 3, 4, 5,6 ], 
    start: '8:00', 
    end: '18:00', 
}, 
resourceLabelText: 'Tecnicos',
resourceAreaWidth: '10%',
resources: [
{id:'0',title:'Sin asignar'},
            <?php $objT=Usuario::obtenerTecnicos2() ?>

            <?php foreach ($objT as $var): ?>
            { id: '<?php echo $var->idusuario ?>', title: '<?php echo $var->ordenIdTaller ?>  <?php  echo $var->usuarioNombre." ".$var->usuarioApellidoPaterno;  ?> - <?php echo $var->sede ?>' },                 
            <?php endforeach ?>
             ], 



eventClick: function(event, jsEvent, view) {
    console.log(event.id);
    var startT = event.start.format("h(:mm) a");
    var title = swal(startT+" "+event.title, "");
    if (title){
        event.title = title;
        console.log('type=changetitle&title='+title+'&eventid='+event.id);
        $.ajax({
            url: 'assets/calendario/process',
            data: 'type=changetitle&title='+title+'&eventid='+event.id,
            type: 'POST',
            dataType: 'json',
            success: function(response){    
                if(response.status == 'success')                            
                    $('#calendar').fullCalendar('updateEvent',event);
            },
            error: function(e){
                alert('Error processing your request: '+e.responseText);
            }
        });
    }
}
});

</script>
  <?php } ?>


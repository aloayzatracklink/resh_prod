<?php 
if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
class Recursos{
	
	function __construct()
	{
		
	}

	public static function sentencia($sql){
		global $db;
		$resultadoQ=$db->query($sql);
		$objetoArray=array();
		while($row=mysqli_fetch_array($resultadoQ)){
			$objetoArray[]= static::instanciador($row);
		}
		return $objetoArray;
	}

	public static function sentenciaV2($sql){
		global $db;
		$resultadoQ=$db->query($sql);
		$objetoArray=array();
		while($row=mysqli_fetch_array($resultadoQ)){
			$objetoArray[]= (Object)$row;
		}
		return $objetoArray;
	}

	public static function sentenciaOnix($sql){
		global $dbOnix;
		$resultadoQ=$dbOnix->queryOnix($sql);
		$objetoArray=array();
		while($row=mysqli_fetch_array($resultadoQ)){
			$objetoArray[]= static::instanciador($row);
		}
		return $objetoArray;
	}

	public static function sentenciaOnixV2($sql){
		global $dbOnix;
		$resultadoQ=$dbOnix->queryOnix($sql);
		$objetoArray=array();
		while($row=mysqli_fetch_array($resultadoQ)){
			$objetoArray[]= (Object)$row;
		}
		return $objetoArray;
	}

	public static function sentenciaWebTrack($sql){
		global $dbWebTrack;
		$resultadoQ=$dbWebTrack->queryWebTrack($sql);
		$objetoArray=array();
		while($row=mysqli_fetch_array($resultadoQ)){
			$objetoArray[]= static::instanciador($row);
		}
		return $objetoArray;
	}
     
	public static function sentenciaMSSQL($sql){
		global $dbMSSQL;
		$resultadoQ=$dbMSSQL ->querySSQL($sql);
		$objetoArray=array();
		while($row=sqlsrv_fetch_array($resultadoQ)){
			$objetoArray[]= static::instanciador($row);
		}
		return $objetoArray;
	}
	public static function sentenciaPublic($sql){
		global $dbp;
		$resultadoQ=$dbp->query($sql);
		$objetoArray=array();
		while($row=mysqli_fetch_array($resultadoQ)){
			$objetoArray[]= static::instanciador($row);
		}
		return $objetoArray;
	}
	
	public static function instanciador($elRegistro){
		$clasellamadora = get_called_class();
		$elObjeto= new $clasellamadora;
	    foreach ($elRegistro as $atributo => $value) {
	    	if ($elObjeto->tieneAtributo($atributo)) {
	    		$elObjeto->$atributo=$value; 
	    	}
	    }
	    return $elObjeto;
	}
	private function tieneAtributo($atributo){
		$propiedadesDelObjeto=get_object_vars($this);
		return array_key_exists($atributo, $propiedadesDelObjeto);
 	}
 	
	//Tilín insano 860ryp0nz-EZ

	public static function selectAll($sql){
		global $db;
		$resultadoQ=$db->query($sql);
		$row=$resultadoQ->fetch_all(MYSQLI_ASSOC);
		return $row;
}

	public static function selectAllOnix($sql){
		global $dbOnix;
		$resultadoQ=$dbOnix->queryOnix($sql);
		$row=$resultadoQ->fetch_all(MYSQLI_ASSOC);
		return $row;
	}
}
}
?>
<?php include("public/publicHead.php") ?>
<?php 
if (!empty($_SESSION['dni'])||!empty($_SESSION['ruc'])) {
	if (!empty($_SESSION['dni'])) {
		$clienteObj=Cliente::obtenerDatosDeclienteTemporalPorDni($_SESSION['dni']);
		$estadoDniObj=Cotizar::obtenerEstadoDni($_SESSION['dni']);
	}else 
	if(!empty($_SESSION['ruc'])){
		$estadoDniObj=Cotizar::obtenerEstadoRuc($_SESSION['ruc']);
		$clienteObj=Cliente::obtenerDatosDeclienteTemporalPorRuc($_SESSION['ruc']);
	}	

}else{
	irA("ingresarCliente");
}

if($estadoDniObj->estado==17){
	irA("registrarme");
}
if ($estadoDniObj->estado==23) {
	irA("registroVehiculos");
}
if ($estadoDniObj->estado==24) {
	irA("registroContactosAutorizados");
}
if ($estadoDniObj->estado==20) {
	irA("ingresarCliente");
}
?>

<link rel="stylesheet" href="assets/css2/style2.css">
<link href="assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<body class="gray-bg">
	<div class="wrapper wrapper-content ">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="ibox-content">
					<h2 class="font-bold text-center">Gracias sus datos an sido enviados con exito</h2>
					<hr>
					<div class="row">

						<div class="col-lg-12 text-primary text-center"><h2>Estimado: <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? $clienteObj->nombre." ".$clienteObj->apellidosC : $clienteObj->razonSocial ; ?> el registro se completo con exito, en breve nos comunicaremos para coordinar y confirmar el lugar y fecha de instalación. </h2></div> 
						
						

					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins ">



								<!-- fin de completar datos de cliente  -->


							</div>
						</div>





					</div>
				</div>
			</div>
		</div>
		<hr/>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				Copyright Wilman Pabel
			</div>
			<div class="col-md-8 col-md-offset-2 text-right">
				<small>&copy; <?php echo date("Y") ?></small>
			</div>
		</div>
	</div>
	<script src="assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
	 <?php include_once('includes/script.php') ?>
</body>

</html>
<?php 
$clienteDatos="Estimado Cliente";
if ($clienteObj->tipoCliente=="Natural") {
	$clienteDatos=$clienteObj->nombre." ".$clienteObj->apellidosC;
}else{
	$clienteDatos=$clienteObj->razonSocial;
}
$fecha=date("d/m/Y h:i A");
$correoCliente=$clienteObj->correo;
$desde="";
if (!empty($_SESSION['dni'])) {
	$cotizaObjDetalle=Cotizar::obtenerDatosPorDni($_SESSION['dni']);
}else if(!empty($_SESSION['ruc'])){
	$cotizaObjDetalle=Cotizar::obtenerDatosPorRuc($_SESSION['ruc']);
}
$usuario=Usuario::obtenerUsuario($cotizaObjDetalle->idCreador);

$correoEnviador=$usuario->usuario;
$nombreEnviador=$usuario->usuarioNombre." ".$usuario->usuarioApellidoPaterno;

?>
<?php 

// los correos here
/////PARA EL CLIENTE ////////////////////////////////////////////////////////////////////77777
///////////////////////////////////////////////////
require_once 'mailphp/PHPMailer/PHPMailerAutoload.php';
$Asunto =$nombreEnviador." de Tracklink, registro completado";
	// Get HTML contents from file
$htmlContenido = "<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
    <meta name='viewport' content='width=device-width' />
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
    <title>The alerta</title>
</head>
<body style=' margin: 0;padding: 0;font-family:Helvetica, Arial, sans-serif;    box-sizing: border-box;    font-size: 14px; background-color: #f6f6f6; width: 100%; '>
	<div style='margin-right: -15px;
	margin-left: -15px;'>	
		<div style='padding: 20px 10px 40px; padding: 0 50px; padding-top: 40px;padding-bottom: 40px;'>
			<div style=' padding: 40px;background-color: #ffffff;color: inherit;padding: 15px 40px 20px 40px;border-color: #e7eaec;border-image: none;border-style: solid solid none;border-width: 1px 0;border-radius: 30px;'>
				<div  style='margin-right: -15px;
				margin-left: -15px;'>
				<div style='position: relative;
				min-height: 1px;
				padding-right: 15px;
				padding-left: 15px;'>
				<img src='cid:tracklink' width='175' alt='' style=''> <img src='cid:ecall' width='175' style='float: right;'><br>
				<br>
				<div class='div'>
					<address>
						<span>De:</span>
						<br>
						<strong>Traklink , Peru.</strong><br>
						San Isidro <br>
						Calle Ricardo Angulo, 762<br>
						<span>".$nombreEnviador."</span><br>
						<span><small>".$correoEnviador."</small></span><br>
						<span>(01) 630 7575</span><br>

					</address>
				</div>
				<p>
					<span><strong>Fecha:</strong>$fecha</span><br/>                    
				</p>
				<h3>Felicitaciones !!</h3>
				<p>Hola ".$clienteDatos." el registro se completo con exito, bienvenido a la familia Tracklik. Queremos aprovechar esta oportunidad para decirle que estamos contentos de que haya elegido nuestro servicio. Es nuestro privilegio servirle y ofrecerle nuestra mejor atención. </p>
				<p>Si tiene alguna pregunta o duda sobre su servicio, por favor no dude en llamarnos para solicitar información durante las horas de trabajo o para atención de emergencia después de las horas de trabajo, llame el (01) 630-7575.</p>
                    <br>

				<div >Cinceramente <br><strong>Gracias </strong>
					
				</div>
				<br><br>
			</div>
		</div>
	</div>
</div>
</div>
</body> ";

/////Aqui se envia el correo ///

// $mail = new PHPMailer;
// $mail->isSMTP();
// $mail->SetLanguage( 'es', 'mailphp/PHPMailer/language/' );
// $mail->SMTPDebug = 0;
// $mail->Debugoutput = 'html';
// $mail->Host = "mail.tracklink.pe";
// $mail->Port = 25;
// $mail->SMTPAuth = false;
// $mail->CharSet = 'UTF-8';
// $mail->setFrom('no-responder@tracklink.pe', $nombreEnviador.' De Tracklink');  
// $mail->addReplyTo($correoEnviador, $nombreEnviador.' De Tracklink'); 
// $mail->addAddress($correoCliente, $clienteDatos); 
// $mail->addCC("pvasquez@tracklink.pe");
// // $mail->addBCC("bcc@example.com");
// $mail->Subject = 'Estimado: '.$clienteDatos.' El registro se completo con éxito'; //// ASUNTO //////
// $mail->AddEmbeddedImage('img/tracklink.png', 'tracklink');
// $mail->AddEmbeddedImage('img/ecall.png', 'ecall');
// $mail->msgHTML($htmlContenido);
// $mail->AltBody = 'Estimado(a) '.$clienteDatos.' el registro se completo con exito, bienvenido a la familia Tracklik.'; ////////ESTE SE EL CONTENIDO//// QUE SE BERA EN PRIERA INSTANACIA
// if (!$mail->send()) {
//     echo "Mailer Error: " . $mail->ErrorInfo;
// } else {
//     echo "Correo Enviado";
// }

/////Aqui se envia el correo ///

/////PARA EL CREADOR DE LA COTIZACION////////////////
$para1=$usuario->usuario;
$htmlContenido1="<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
    <meta name='viewport' content='width=device-width' />
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
    <title>The alerta</title>
</head>

<body style=' margin: 0;padding: 0;font-family:Helvetica, Arial, sans-serif;    box-sizing: border-box;    font-size: 14px; background-color: #f6f6f6; '>

<table style=' background-color: #f6f6f6; width: 100%;'>
    <tr>
        <td></td>
        <td  width='600' style=' display: block !important;    max-width: 600px !important;    margin: 0 auto !important;    /* makes it centered */    clear: both !important;'>
            <div class='content' style='  max-width: 600px;    margin: 0 auto;    display: block;    padding: 20px;'>
                <table class='main' width='100%' cellpadding='0' cellspacing='0' style='background: #fff;    border: 1px solid #e9e9e9;    border-radius: 3px;'>
                    <tr>
                        <td style='font-size: 16px;    color: #fff;    font-weight: 500;    padding: 20px;    text-align: center;    border-radius: 3px 3px 0 0;  background: #1ab394;'>
                            ALERTA REGISTRO COMPLETADO.
                        </td>
                    </tr>
                    <tr>
                        <td style='padding: 10px !important ;padding: 20px; vertical-align: top;'>
                            <table width='100%' cellpadding='0' cellspacing='0'>
                                
                                <tr>
                                    <td  style='padding: 0 0 20px;'>
                                        El cliente <strong>".$clienteDatos."</strong> <br> <br>Completo el registro de datos personales y del vehículo con éxito                      </td>
                                </tr>
                                <tr>
                                    <td style='padding: 0 0 20px;text-align: center; '>
                                        <a href='#' style='text-decoration: none;    color: #FFF;    background-color: #1ab394;    border: solid #1ab394;    border-width: 5px 10px;    line-height: 2;
    font-weight: bold;    text-align: center;    cursor: pointer;
    display: inline-block;    border-radius: 5px;    text-transform: capitalize;'>Verificar</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='padding: 0 0 20px;'>
                                        Gracias por usar Resh.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div style=' width: 100%;    clear: both;    color: #999;    padding: 20px;'>
                    <table width='100%'>
                        <tr>
                            <td style='font-size: 12px; text-align: center;padding: 0 0 20px;' ><a href='#' style='color: #999;'>Unsubscribe</a> de estas alertas.</td>                        </tr>
                    </table>
                </div></div>
        </td>
        <td></td>
    </tr>
</table>
</body>
</html>";



///Aqui se envia el correo ///

// $mail1 = new PHPMailer;
// $mail1->isSMTP();
// $mail1->SetLanguage( 'es', 'mailphp/PHPMailer/language/' );
// $mail1->SMTPDebug = 2;
// $mail1->Debugoutput = 'html';
// $mail1->Host = "mail.tracklink.pe";
// $mail1->Port = 25;
// $mail1->SMTPAuth = false;
// $mail1->CharSet = 'UTF-8';
// $mail1->setFrom('no-responder@tracklink.pe', 'Sistemas Tracklink');  
// $mail1->addReplyTo('pvasquez@tracklink.pe', 'Soporte De Tracklink'); 
// $mail1->addAddress($para1, $nombreEnviador); 
// $mail1->addCC("pvasquez@tracklink.pe");
// // $mail1->addBCC("bcc@example.com");
// $mail1->Subject = 'El cliente: '.$clienteDatos.' Completo su registro PRUEBA'; //// ASUNTO //////
// // $mail1->AddEmbeddedImage('img/tracklink.png', 'tracklink');
// $mail1->msgHTML($htmlContenido1);
// $mail1->AltBody = 'Estimado '.$clienteDatos.' agradecemos su preferencia'; ////////ESTE SE EL CONTENIDO//// QUE SE BERA EN PRIERA INSTANACIA
// if (!$mail1->send()) {
//     echo "Mailer Error: " . $mail1->ErrorInfo;
// } else {
//     echo "Correo Enviado";
// }

/////Aqui se envia el correo ///
/////Aqui se envia el correo ///




?>





<?php 

if (!empty($_SESSION['dni'])) {
	Cotizar::cambiarEstadoDni(20,$_SESSION['dni']);
	$objCotizar= new Cotizar();
	$objCotizar->cambiarEstadoCotizacion(31,$_SESSION['dni']);
	unset($_SESSION['dni']);

}
if (!empty($_SESSION['ruc'])) {
	Cotizar::cambiarEstadoRuc(20,$_SESSION['ruc']);
	$objCotizar= new Cotizar();
	$objCotizar->cambiarEstadoCotizacion(31,$_SESSION['ruc']);
	unset($_SESSION['ruc']);
						 	// session_destroy();
}


?> 

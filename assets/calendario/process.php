<?php
include('config.php');

$type = $_POST['type'];

if($type == 'new')
{
	$startdate = $_POST['startdate'].'+'.$_POST['zone'];
	$title = $_POST['title'];
	$insert = mysqli_query($con,"INSERT INTO calendar(`title`, `startdate`, `enddate`, `allDay`) VALUES('$title','$startdate','$startdate','false')");
	$lastid = mysqli_insert_id($con);
	echo json_encode(array('status'=>'success','eventid'=>$lastid));
}

if($type == 'changetitle')
{
	$eventid = $_POST['eventid'];
	$title = $_POST['title'];
	$update = mysqli_query($con,"UPDATE calendar SET title='$title' where id='$eventid'");
	if($update)
		echo json_encode(array('status'=>'success'));
	else
		echo json_encode(array('status'=>'failed'));
}

if($type == 'resetdate')
{
	$title = $_POST['title'];
	$startdate = $_POST['start'];
	$enddate = $_POST['end'];
	$eventid = $_POST['eventid'];
	$update = mysqli_query($con,"UPDATE calendar SET title='$title', startdate = '$startdate', enddate = '$enddate' where id='$eventid'");
	if($update)
		echo json_encode(array('status'=>'success'));
	else
		echo json_encode(array('status'=>'failed'));
}

if($type == 'obtenerDatosS')
{
	$eventid = $_POST['eventid'];
	require_once('../../includes/init.php');
	$dato=Calendario::obtenerPorId($eventid);
	// var_dump($dato);
	// $delete = mysqli_query($con,"DELETE FROM calendar where id='$eventid'");
	if ($dato->idoperacion!=0) {
	$opeDatos=Operacion::obtenerOperacionPorId($dato->idoperacion);
    if ($opeDatos->idConfirmacionOPE!=0) {
        $o=Usuario::obtenerUsuario($opeDatos->idConfirmacionOPE);
    }else{
        $o=Usuario::obtenerUsuario($opeDatos->idCreador);
    }
		echo json_encode(array('Ejecutivo'=>$o->usuarioNombre." ".$o->usuarioApellidoPaterno, 'contacto'=> $opeDatos->nombreContacto, 'numero'=>$opeDatos->numeroContacto, 'direccion'=>$opeDatos->direccionInstacion));
	}else{
	$opeDatos=Cita::obtenerCitaPorId($dato->idcita);
	$o=Usuario::obtenerUsuario($opeDatos->idOpe);
		echo json_encode(array('Ejecutivo'=>$o->usuarioNombre." ".$o->usuarioApellidoPaterno, 'contacto'=> $opeDatos->nombreCliente, 'numero'=>$opeDatos->celular, 'direccion'=>'Taller'));
	}

}

if($type == 'remove')
{
	$eventid = $_POST['eventid'];
	$delete = mysqli_query($con,"DELETE FROM calendar where id='$eventid'");
	if($delete)
		echo json_encode(array('status'=>'success'));
	else
		echo json_encode(array('status'=>'failed'));
}

if($type == 'fetch')
{
	$events = array();
	$query = mysqli_query($con, "SELECT * FROM calendar");
	while($fetch = mysqli_fetch_array($query,MYSQLI_ASSOC))
	{
	$e = array();
    $e['id'] = $fetch['id'];
    $e['resourceId'] = $fetch['resourceId'];
    $e['title'] = $fetch['title'];
    $e['start'] = $fetch['startdate'];
    $e['end'] = $fetch['enddate'];
    $e['color'] = $fetch['color'];
    $e['textColor'] = $fetch['textColor'];
    $e['borderColor'] = $fetch['borderColor'];

    $allday = ($fetch['allDay'] == "true") ? true : false;
    $e['allDay'] = $allday;

    array_push($events, $e);
	}
	echo json_encode($events);
}


?>

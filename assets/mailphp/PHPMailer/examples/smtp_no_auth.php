<?php
/**
 * This example shows making an SMTP connection without using authentication.
 */

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('America/Lima');

require_once '../PHPMailerAutoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
$mail->SetLanguage( 'es', '../language/' );
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = "mail.tracklink.pe";
//Set the SMTP port number - likely to be 25, 465 or 587
$mail->Port = 25;
//Whether to use SMTP authentication
$mail->SMTPAuth = false;
///the utf here ---
$mail->CharSet = 'UTF-8';
//Set who the message is to be sent from
$mail->setFrom('no-responder@tracklink.pe', 'Sistemas Resh');  // Desde donde se envia 
//Set an alternative reply-to address
$mail->addReplyTo('wilmanpabel@gmail.com', 'Wilman Pabel'); // donde deven responder
//Set who the message is to be sent to
$mail->addAddress('wilmanpabel@gmail.com', 'The programer'); // aquien se manda el mensaje
//CC and BCC
// $mail->addCC("cc@example.com");
// $mail->addBCC("bcc@example.com");
//Set the subject line
$mail->Subject = 'A test Mail correo de ejemplo'; //// ASUNTO //////
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->msgHTML(file_get_contents('contentsutf8.php'), dirname(__FILE__));
//Replace the plain text body with one created manually
$mail->AltBody = 'The is here beba ti ama pepa'; ////////ESTE SE EL CONTENIDO//// QUE SE BERA EN PRIERA INSTANACIA
//Attach an image file
	//aqui se adjunta los archivos que queremos enviar :D

// $mail->addAttachment('images/phpmailer_mini.png');

//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
}

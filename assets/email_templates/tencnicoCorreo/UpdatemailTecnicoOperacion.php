<?php
    $infoONix=Operacion::obtenerInfoOnyxMail($infoOpera->idcustomer,$infoOpera->idlocation,$infoOpera->idcompany,$infoOpera->idvehicle);
    $dataop=Operacion::obtenerTodolosDatosPoridOperacion($_POST['idoperacion']);
    $datosVehiculo=Vehiculo::obtenerVehiculo($dataop->idvehiculo);
    $datosProg=Operacion::ObtenertbPrograma($_POST['idoperacion']);

    $eje="";
    $ope="";
    $dplan=$datosVehiculo->plan;
    if ($dataop->idChequeo == 0) {
        $datosF=DatosFacturacion::obtenerDatosFacturacionPorId($dataop->datosfacturacionid);
        $usD=Usuario::obtenerUsuario($datosF->idUsuarioCreado);
        $usDP=Usuario::obtenerUsuario($usD->comercialOperaciones);
        $eje=$usD->usuarioNombre." ".$usD->usuarioApellidoPaterno;
        $ope=$usDP->usuarioNombre." ".$usDP->usuarioApellidoPaterno;
        $dplan=$datosVehiculo->plan;
    }else{
        $chequeoObj=Chequeos::obtenerChequeoTrackPorId($dataop->idChequeo);
        $dplan=$chequeoObj->plan;
    }
?>
<body style=' margin: 0;padding: 0;font-family:Helvetica, Arial, sans-serif;    box-sizing: border-box;    font-size: 14px; background-color: #f6f6f6; '>
<br>
<br><p style="margin-left: 15px">
<?php
if(isset($cambio)){
    echo "Estimado Técnico , hubo una cambio de información del cliente y/o orte en mención en el asunto del correo ,Por favor tener en cuenta la siguiente información brindada en este correo que es la Correcta . <br> <br>";
}
?></p>
<table style=' background-color: #f6f6f6; width: 100%;'>
    <tr>
        <td></td>
        <td  width='600' style=' display: block !important;    max-width: 600px !important;    margin: 0 auto !important;    /* makes it centered */    clear: both !important;'>
            <div class='content'>
                <table class='main' cellspacing="0" cellpadding="0" border="1" >
                    <tr>
                        <td style='font-size: 16px;    color: #fff;    font-weight: 500;    padding: 20px;    text-align: center;    border-radius: 3px 3px 0 0;  background: #1970b2;' colspan="10">
                            Datos de <?= ucwords(strtolower($desctrab)); ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 180px">Lugar :</td>
                        <td><?= $dataop->lugarInstalacion." ".$dataop->nombreConcecionarioInstalicion ?></td>
                    </tr>
                    <tr>
                        <td style="width: 180px">Orte :</td>
                        <td><?= $operacionObj->numeroOrte ?></td>
                    </tr>
                    <tr>
                        <td>Fecha :</td>
                        <td><?= $datosProg->fecha ?></td>
                    </tr>
                    <tr>
                        <td>Hora : </td>
                        <td><?= $datosProg->hora ?></td>
                    </tr>
                    <tr>
                        <td>Cliente :</td>
                        <td><?= $infoONix->cliente; ?></td>
                    </tr>
                    <tr>
                        <td>Plan :</td>
                        <td><?= $dplan ?></td>
                    </tr>
                    <tr>
                        <td>Placa :</td>
                        <td><?= $infoONix->license; ?></td>
                    </tr>
                    <?php if ($dataop->idChequeo == 0){ ?>
                    <tr>
                        <td>Servicios Adicionales :</td>
                        <td>
                            <?php $objSer = ServiciosAdicionales::obtenerServAdic($_POST['idoperacion']) ?>
                            <?php if ($objSer): ?>
                                <?php foreach ($objSer as $varad): ?>
                                    <?php if($varad->slect=='1'){
                                        echo $varad->descripcion.' ,';
                                    }?>
                                <?php endforeach ?>
                            <?php else: ?>
                            <?php endif ?>
                        </td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td>Chasis:</td>
                        <td><?= $infoONix->idchasis; ?></td>
                    </tr>
                    <tr>
                        <td>Motor :</td>
                        <td><?= $infoONix->engine; ?></td>
                    </tr>
                    <tr>
                        <td>Marca:</td>
                        <td><?= $infoONix->brand; ?></td>
                    </tr>
                    <tr>
                        <td>Modelo:</td>
                        <td><?= $infoONix->descmodel; ?></td>
                    </tr>
                    <tr>
                        <td>Año :</td>
                        <td><?= $infoONix->yearmodel; ?></td>
                    </tr>
                    <tr>
                        <td>Color :</td>
                        <td><?= $infoONix->color; ?></td>
                    </tr>
                 <?php if ($dataop->idChequeo != 0){ ?>
                    <tr>
                        <td>Ubicación de Sistema :</td>
                        <td><?=  $chequeoObj->ubicacionSistema; ?></td>
                    </tr>

                    <tr>
                        <td>Fecha Instalación:</td>
                        <td><?= $chequeoObj->fechaInstalacion; ?></td>
                    </tr>
                    <tr>
                        <td>Tecnico Instalador:</td>
                        <td><?=  $chequeoObj->instalador; ?></td>
                    </tr>

                     <tr>
                         <td>Motivo:</td>
                         <td><?=  $chequeoObj->motivo; ?></td>
                     </tr>

                     <tr>
                         <td>Seguimiento:</td>
                         <td><?=  $chequeoObj->seguimientoRealizado; ?></td>
                     </tr>

                    <?php }?>

                    <tr>
                        <td>Departamento :</td>
                        <td><?= $dataop->departamentoi?></td>
                    </tr>
                    <tr>
                        <td>Provincia :</td>
                        <td><?= $dataop->provinciai?></td>
                    </tr>
                    <tr>
                        <td>Distrito :</td>
                        <td><?= $dataop->distritoi?></td>
                    </tr>
                    <tr>
                        <td>Dirección :</td>
                        <td><?= $dataop->direccionInstacion?></td>
                    </tr>
                    <tr>
                        <td>Referencia :</td>
                        <td><?= $dataop->referenciaInstalacion?></td>
                    </tr>
                    <tr>
                        <td>Coordenadas :</td>
                        <td> </td>
                    </tr>
                    <tr>
                        <td>Nombre de Contacto :</td>
                        <td><?= $dataop->nombreContacto?></td>
                    </tr>
                    <tr>
                        <td>Telefono Contacto :</td>
                        <td><?= $dataop->numeroContacto?></td>
                    </tr>
                    <tr>
                        <td>ADV :</td>
                        <td><?= $ope ?></td>
                    </tr>
                    <tr>
                        <td>Ejecutivo Comercial :</td>
                        <td><?= $eje ?></td>
                    </tr>
                    <tr>
                        <td>Observaciones :</td>
                        <td><?= $dataop->observacion ?></td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
</body>
</html>


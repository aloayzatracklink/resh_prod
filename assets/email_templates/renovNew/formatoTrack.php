<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Renovación de servicio</title>
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
<style type="text/css" id="media-query">
body {
margin: 0;
padding: 0; }
table, tr, td {
vertical-align: top;
border-collapse: collapse; }

.ie-browser table, .mso-container table {
table-layout: fixed; }

* {
line-height: inherit; }

a[x-apple-data-detectors=true] {
color: inherit !important;
text-decoration: none !important; }

.img-container div,  .img-container button {
display: block !important; }

.fullwidth button {
width: 100% !important; }

.block-grid .col {
display: table-cell;
float: none !important;
vertical-align: top; }

.ie-browser .num12, .ie-browser .block-grid,  .num12,  .block-grid {
width: 622px !important; }

.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
line-height: 100%; }

.ie-browser .mixed-two-up .num4,  .mixed-two-up .num4 {
width: 204px !important; }

.ie-browser .mixed-two-up .num8,  .mixed-two-up .num8 {
width: 408px !important; }

.ie-browser .block-grid.two-up .col,  .block-grid.two-up .col {
width: 310px !important; }

.ie-browser .block-grid.three-up .col,  .block-grid.three-up .col {
width: 206px !important; }

.ie-browser .block-grid.four-up .col,  .block-grid.four-up .col {
width: 155px !important; }

.ie-browser .block-grid.five-up .col,  .block-grid.five-up .col {
width: 124px !important; }

.ie-browser .block-grid.six-up .col,  .block-grid.six-up .col {
width: 103px !important; }

.ie-browser .block-grid.seven-up .col,  .block-grid.seven-up .col {
width: 88px !important; }

.ie-browser .block-grid.eight-up .col,  .block-grid.eight-up .col {
width: 77px !important; }

.ie-browser .block-grid.nine-up .col,  .block-grid.nine-up .col {
width: 68px !important; }

.ie-browser .block-grid.ten-up .col,  .block-grid.ten-up .col {
width: 62px !important; }

.ie-browser .block-grid.eleven-up .col,  .block-grid.eleven-up .col {
width: 56px !important; }

.ie-browser .block-grid.twelve-up .col,  .block-grid.twelve-up .col {
width: 51px !important; }

@media only screen and (min-width: 640px) {
.block-grid {
width: 620px !important; }
.block-grid .col {
vertical-align: top; }
.block-grid .col.num12 {
width: 620px !important; }
.block-grid.mixed-two-up .col.num4 {
width: 204px !important; }
.block-grid.mixed-two-up .col.num8 {
width: 408px !important; }
.block-grid.two-up .col {
width: 310px !important; }
.block-grid.three-up .col {
width: 206px !important; }
.block-grid.four-up .col {
width: 155px !important; }
.block-grid.five-up .col {
width: 124px !important; }
.block-grid.six-up .col {
width: 103px !important; }
.block-grid.seven-up .col {
width: 88px !important; }
.block-grid.eight-up .col {
width: 77px !important; }
.block-grid.nine-up .col {
width: 68px !important; }
.block-grid.ten-up .col {
width: 62px !important; }
.block-grid.eleven-up .col {
width: 56px !important; }
.block-grid.twelve-up .col {
width: 51px !important; } }

@media (max-width: 640px) {
.block-grid, .col {
min-width: 320px !important;
max-width: 100% !important;
display: block !important; }
.block-grid {
width: calc(100% - 40px) !important; }
.col {
width: 100% !important; }
.col > div {
margin: 0 auto; }
img.fullwidth, img.fullwidthOnMobile {
max-width: 100% !important; }
.no-stack .col {
min-width: 0 !important;
display: table-cell !important; }
.no-stack.two-up .col {
width: 50% !important; }
.no-stack.mixed-two-up .col.num4 {
width: 33% !important; }
.no-stack.mixed-two-up .col.num8 {
width: 66% !important; }
.no-stack.three-up .col.num4 {
width: 33% !important; }
.no-stack.four-up .col.num3 {
width: 25% !important; } }


</style>
</head>
<body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #FFFFFF">
<style type="text/css" id="media-query-bodytag">
@media (max-width: 520px) {
.block-grid {
min-width: 320px!important;
max-width: 100%!important;
width: 100%!important;
display: block!important;
}
.col {
min-width: 320px!important;
max-width: 100%!important;
width: 100%!important;
display: block!important;
}
.col > div {
margin: 0 auto;
}
img.fullwidth {
max-width: 100%!important;
}
img.fullwidthOnMobile {
max-width: 100%!important;
}
.no-stack .col {
min-width: 0!important;
display: table-cell!important;
}
.no-stack.two-up .col {
width: 50%!important;
}
.no-stack.mixed-two-up .col.num4 {
width: 33%!important;
}
.no-stack.mixed-two-up .col.num8 {
width: 66%!important;
}
.no-stack.three-up .col.num4 {
width: 33%!important
}
.no-stack.four-up .col.num3 {
width: 25%!important
}
}
</style>
<style>
.lineaD{
    border-right: 1px solid #ccc;
}
    
.tabl{
border: 0px solid #ccc;
border-collapse: collapse;
margin: 0;
padding: 0;
width: 100%;
table-layout: fixed;
}
.capt{
font-size: 1.5em;
margin: .5em 0 .75em;
}

.trb {
background: #f5f5f5;
border: 1px solid #ddd;
padding: .35em;
}
.tr {
background: #fff;
border: 1px solid #ddd;
padding: .35em;
}
.thtd {
padding: .400em;
text-align: center;
}
.th {
font-size: .70em;
text-transform: capitalize;
}
@media screen and (max-width: 600px) {
#tabl {
border: 0;
}
.thead {
border: none;
clip: rect(0 0 0 0);
height: 1px;
margin: -1px;
overflow: hidden;
padding: 0;
position: absolute;
width: 1px;
}
.trm {
border-bottom: 3px solid #ddd;
display: block;
margin-bottom: .625em;
}
.tdm{
border-bottom: 1px solid #ddd;
display: block;
font-size: .8em;
text-align: center;
}
.tdb:before {
content: attr(data-label);
float: left;
font-weight: bold;
text-transform:capitalize;
}
.tdl:last-child {
border-bottom: 0;
}
.catpm {
font-size: 1.3em;
}

}
    </style>
<div class="ie-browser">
<table class="nl-container" style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #FFFFFF;width: 100%" cellpadding="0" cellspacing="0">
<tbody>
<tr style="vertical-align: top">
<td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<div class="col num12" style="min-width: 320px;max-width: 620px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
<div align="left" class="img-container center fixedwidth" style="padding-right: 0px;  padding-left: 0px;">
<img class="center fixedwidth" align="center" border="0" src="cid:head" alt="Tracklink Octo" title="Tracklik Octo" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 30%;max-width: 620px" width="620">
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 622px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<div class="col num12" style="min-width: 320px;max-width: 622px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
<div style="padding-right: 10px; padding-left: 10px; padding-top: 0px; padding-bottom: 0px;">
<div align="center"><div style="border-top: 2px solid #B5B5B5; width:100%; line-height:2px; height:2px; font-size:2px;">&#160;</div></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 622px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<div class="col num12" style="min-width: 320px;max-width: 622px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
<div style="font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:150%;color:#71777D; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 30px;">	
    <div style="font-size:12px;line-height:18px;color:#3e4144;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;"><div class="txtTinyMce-wrapper" style="font-size:12px; line-height:18px;">
    <p style="margin: 0;font-size: 14px;line-height: 21px"><span style="color: rgb(0, 0, 0); font-size: 14px; line-height: 21px;"><strong> Estimado: <?= ucwords(strtolower($nombres)) ?><span style="font-size: 14px; line-height: 21px;"> </span></strong></span></p>
        <p style="margin: 0;font-size: 14px;line-height: 21px">Reciba un cordial saludo de parte de la familia Tracklink Octo. Nos dirigimos a usted para agradecerle por la confianza que tiene depositada en nosotros para brindarle protección y seguridad en las vías peruanas a usted y a los que más ama.</p>
        <p style="margin: 0;font-size: 14px;line-height: 21px"><?= $mensajeCentral ?></p>
        <p style="margin: 0;font-size: 14px;line-height: 21px"><strong>Precio de renovacion</strong></p>
        <p style="margin: 0;font-size: 14px;line-height: 21px"><?= $monto ?> Dólares americanos incl. IGV por unidad.</p>
        <h3 style="margin: 0;font-size: 14px;line-height: 21px"><?=  $mensajePersonalizado ?></p>
<table class="tabl" style="border: 0px solid #ccc;border-collapse: collapse;margin: 0;padding: 0;width: 100%;table-layout: fixed;">
<thead class="thead">
	<tr class="tr trm">
		<th class="thtd th">Placa</th>
		<th class="thtd th">Marca/modelo</th>
		<th class="thtd th">Color</th>
		<th class="thtd th">Fecha vencimiento</th>
	</tr>
</thead>
<tbody>
<tr class="tr trm">
<td data-label="Placa" class="thtd th tdm tdb tdl"><?= $placaVehiculo ?></td>
<td data-label="Marca/modelo" class="thtd th tdm tdb tdl"><?= $marca." / ".$modelo ?></td>
<td data-label="Color" class="thtd th tdm tdb tdl"><?= $color ?></td>
<td data-label="Fecha vencimiento" class="thtd th tdm tdb tdl"><?= $vence ?></td>
</tr>
</tbody>
</table><br>
<?= $nlinkmail ?>
<h3 style="text-align:center" ><u>NÚMEROS DE CUENTA</u></h3>
        <p style="margin: 0;font-size: 14px;line-height: 21px"><strong>Transferencia: </strong>Si realiza el pago vía transferencia o depósito bancario, es importante que nos envié por este medio la confirmación del abono.</p>
<table class="tabl" style="border: 0px solid #ccc;border-collapse: collapse;margin: 0;padding: 0;width: 100%;table-layout: fixed;">
<thead class="thead">
	<tr class="tr">
		<th class="thtd th" colspan='4'>TITULAR DE LA CUENTA: MOTORLINK S.A.C. <br> RUC: 20525107915</th>
	</tr>
</thead>
<tbody>
<tr class="tr ">
    <td  class="thtd th lineaD" colspan='2' >N° Cuenta Corriente (Depósito)</td>
    <td  class="thtd th " colspan='2'>N° Cuenta Interbancario (vía internet, CCI)</td>
</tr>
<tr class="tr ">
    <td  class="thtd th lineaD" >Banco Financiero Dólares</td>
    <td  class="thtd th lineaD" >000-000447979159</td>
    <td  class="thtd th lineaD" >Banco Financiero Dólares</td>
    <td  class="thtd th " >035-000-000447979159-94</td>
</tr>
<tr class="trb " style="background: #f5f5f5;border: 1px solid #ddd;padding: .35em;">
    <td  class="thtd th lineaD" >Banco Financiero Soles</td>
    <td  class="thtd th lineaD" >000-000447978934</td>
    <td  class="thtd th lineaD" >Banco Financiero Soles</td>
    <td  class="thtd th " >035-000-000447978934-91</td>
</tr>
<tr class="tr ">
    <td  class="thtd th lineaD" >Banco de Crédito Dólares</td>
    <td  class="thtd th lineaD" >194-1967786-1-34</td>
    <td  class="thtd th lineaD" >Banco de Crédito Dólares</td>
    <td  class="thtd th " >002-194-001967786134-96</td>
</tr>
<tr class="trb " style="background: #f5f5f5;border: 1px solid #ddd;padding: .35em;">
    <td  class="thtd th lineaD" >Banco de Crédito Soles</td>
    <td  class="thtd th lineaD" >193-1853946-0-25</td>
    <td  class="thtd th lineaD" >Banco de Crédito Soles</td>
    <td  class="thtd th " >002-193-001853946025-15</td>
</tr><tr class="tr ">
    <td  class="thtd th lineaD" >Banco Continental BBVA Dólares</td>
    <td  class="thtd th lineaD" >0011-0426-0100005979</td>
    <td  class="thtd th lineaD" >Banco Continental BBVA Dólares</td>
    <td  class="thtd th " >011-426-000100005979-48</td>
</tr><tr class="trb " style="background: #f5f5f5;border: 1px solid #ddd;padding: .35em;">
    <td  class="thtd th lineaD" >Banco Continental BBVA Soles</td>
    <td  class="thtd th lineaD" >0011-0426-0100005960</td>
    <td  class="thtd th lineaD" >Banco Continental BBVA Soles</td>
    <td  class="thtd th " >011-426-000100005960-48</td>
</tr><tr class="tr ">
    <td  class="thtd th lineaD" >Banco BanBif Dólares</td>
    <td  class="thtd th lineaD" >7000364190</td>
    <td  class="thtd th lineaD" >Banco BanBif Dólares</td>
    <td  class="thtd th " >038-103-207000364190-80</td>
</tr>
<tr class="trb " style="background: #f5f5f5;border: 1px solid #ddd;padding: .35em;">
    <td  class="thtd th lineaD" >Banco BanBif Soles</td>
    <td  class="thtd th lineaD" >7000364203</td>
    <td  class="thtd th lineaD" >Banco BanBif Soles</td>
    <td  class="thtd th " >038-103-107000364203-82</td>
</tr><tr class="tr ">
    <td  class="thtd th lineaD" colspan='2' >Banco de la Nacion (Cuenta detracciones)</td>
    <td  class="thtd th lineaD" colspan='2' >00-046-021886</td>
</tr>
</tbody>
</table>
<p style="margin: 0;font-size: 14px;line-height: 21px">Si opta por realizar sus pagos vía transferencias o depósitos bancarios, requerimos nos envié por este medio la confirmación del abono, adjuntando el Voucher escaneado del depósito o la confirmación de transferencia.
<br><br>
<!--
<p style="margin: 0;font-size: 14px;line-height: 21px">Si opta por realizar sus pagos vía transferencias o depósitos bancarios, requerimos nos envié por este medio la confirmación del abono, adjuntando el Voucher escaneado del depósito o la confirmación de transferencia.
        De lo contrario, también puede realizar estos pagos en nuestras oficina de <strong>Calle Ricardo Angulo 762 San Isidro,</strong> de lunes a viernes de 09:00- 13:00, 14:00- 17:00(no se atenderá de 13:00 a 14:00) y los sábados de 9:00 a 12:00. <br><br>
-->
<strong>Si por algún motivo las unidades no fueran renovadas dentro de las fechas indicadas y sufrieran algún siniestro, Tracklink Octo no se responsabiliza por los daños o pérdidas ocasionados.</strong>
<br><br>
Cualquier consulta no dude en hacérnosla llegar que gustosos lo atenderemos.
</p>

        <br>


<div style="color: #000066; font-family: 'trebuchet ms', sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;"><span style="color: #17365d;"><span style="font-size: 13.3333px;"><span style="font-size: 12pt;">
Saludos</span>,</span></span></div>
<div style="color: #000066; font-family: 'trebuchet ms', sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;"><span style="color: #17365d;"><span style="font-size: 13.3333px;">&nbsp;</span></span></div>
<div style="color: #000066; font-family: 'trebuchet ms', sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;">
<div style="color: #000000; font-family: arial, helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; font-size: 13.3333px; background-color: #ffffff;"><span style="color: #003366;"><b>
<?= $nombreEnviador ?></b></span></div>
<div style="color: #000000; font-family: arial, helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; font-size: 13.3333px; background-color: #ffffff;">
<div style="color: #000066; font-family: 'trebuchet ms', sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;">
<p style="color: #000066; font-family: 'trebuchet ms', sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; margin: 0px;"><span style="font-size: 10pt; color: #003366; font-family: arial, helvetica, sans-serif;">
<?= $cargo ?><br /></span></p>
<p style="color: #000066; font-family: 'trebuchet ms', sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; margin: 0px;"><span style="color: #003366; font-size: 10pt; font-family: arial, helvetica, sans-serif;">
Tracklink -&nbsp;Octo Telematics Business Partner</span></p>
<p style="color: #000066; font-family: 'trebuchet ms', sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; margin: 0px;"><span style="color: #000000; font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="color: #003366;">
Oficina: +511 6307575, Anexo: <?= $anexo ?></span></span></p>
<p style="color: #000066; font-family: 'trebuchet ms', sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; margin: 0px;"><span style="color: #000000; font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="color: #003366;">
Celular: <?= $celular ?></span><br /></span></p>
<div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; color: #000000; line-height: normal; font-family: helvetica; font-size: 12px;">
<div><span style="color: #0000ff; font-size: 10pt;"><span style="font-family: arial, helvetica, sans-serif;"> <span style="color: #005a95; text-decoration: none; cursor: pointer;"> <span style="color: #005a95; text-decoration: none; cursor: pointer;"> <span style="color: #005a95; text-decoration: none; cursor: pointer;" gt="" a="" target="_blank" href="http://www.tracklink.pe/">www.tracklink.pe</span></span></span><span>&nbsp;</span>/<span>&nbsp;</span><span style="color: #005a95; text-decoration: none; cursor: pointer;"><span style="color: #005a95; text-decoration: none; cursor: pointer;"><span style="color: #005a95; text-decoration: none; cursor: pointer;"><a target="_blank" href="http://www.octotelematics.com/" style="color: #005a95; text-decoration: none; cursor: pointer;">www.octotelematics.com</a></span></span></span></span></span></div>
</div>
</div>
</div>
</div>
<div style="font-family: 'trebuchet ms', sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;">
<div class="" style="color: #000000; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; line-height: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; font-family: helvetica; background-color: #ffffff;"></div>
</div>




</div> </div> </div>
</div>
</div>
</div>
</div>

<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 622px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<div class="col num12" style="min-width: 320px;max-width: 620px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
<div style="padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 0px;">
<div align="center"><div style="border-top: 2px solid #B5B5B5; width:100%; line-height:2px; height:2px; font-size:2px;">&#160;</div></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid two-up ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<div class="col num6" style="max-width: 320px;min-width: 310px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
<div style="color:#969696;line-height:120%;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">	
<div style="font-size:12px;line-height:14px;color:#969696;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;"></div>	
</div>
</div>
</div>
</div>
<div class="col num6" style="max-width: 320px;min-width: 310px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
<div style="color:#969696;line-height:120%;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">	
<div style="font-size:12px;line-height:14px;color:#969696;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;"><p style="margin: 0;font-size: 14px;line-height: 17px;text-align: right"><strong>www.tracklink.pe</strong></p></div>	
</div>
</div>
</div>
</div>
</div>
</div>
</div> 
</td>
</tr>
</tbody>
</table>
</body>
</html>

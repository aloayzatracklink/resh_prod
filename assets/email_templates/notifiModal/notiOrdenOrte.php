<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Notificación Postventa</title>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
<style type="text/css" id="media-query">
    body {
        margin: 0;
        padding: 0; }
    table, tr, td {
        vertical-align: top;
        border-collapse: collapse; }
    .ie-browser table, .mso-container table {
        table-layout: fixed; }
    * {
        line-height: inherit; }
    a[x-apple-data-detectors=true] {
        color: inherit !important;
        text-decoration: none !important; }
    [owa] .img-container div, [owa] .img-container button {
        display: block !important; }
    [owa] .fullwidth button {
        width: 100% !important; }
    [owa] .block-grid .col {
        display: table-cell;
        float: none !important;
        vertical-align: top; }
    .ie-browser .num12, .ie-browser .block-grid, [owa] .num12, [owa] .block-grid {
        width: 600px !important; }
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
        line-height: 100%; }
    .ie-browser .mixed-two-up .num4, [owa] .mixed-two-up .num4 {
        width: 200px !important; }
    .ie-browser .mixed-two-up .num8, [owa] .mixed-two-up .num8 {
        width: 400px !important; }
    .ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {
        width: 300px !important; }
    .ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {
        width: 200px !important; }
    .ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {
        width: 150px !important; }
    .ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {
        width: 120px !important; }
    .ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {
        width: 100px !important; }
    .ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {
        width: 85px !important; }
    .ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {
        width: 75px !important; }
    .ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {
        width: 66px !important; }
    .ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {
        width: 60px !important; }
    .ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {
        width: 54px !important; }
    .ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {
        width: 50px !important; }
    @media only screen and (min-width: 620px) {
        .block-grid {
            width: 600px !important; }
        .block-grid .col {
            vertical-align: top; }
        .block-grid .col.num12 {
            width: 600px !important; }
        .block-grid.mixed-two-up .col.num4 {
            width: 200px !important; }
        .block-grid.mixed-two-up .col.num8 {
            width: 400px !important; }
        .block-grid.two-up .col {
            width: 300px !important; }
        .block-grid.three-up .col {
            width: 200px !important; }
        .block-grid.four-up .col {
            width: 150px !important; }
        .block-grid.five-up .col {
            width: 120px !important; }
        .block-grid.six-up .col {
            width: 100px !important; }
        .block-grid.seven-up .col {
            width: 85px !important; }
        .block-grid.eight-up .col {
            width: 75px !important; }
        .block-grid.nine-up .col {
            width: 66px !important; }
        .block-grid.ten-up .col {
            width: 60px !important; }
        .block-grid.eleven-up .col {
            width: 54px !important; }
        .block-grid.twelve-up .col {
            width: 50px !important; } }
    @media (max-width: 620px) {
        .block-grid, .col {
            min-width: 320px !important;
            max-width: 100% !important;
            display: block !important; }
        .block-grid {
            width: calc(100% - 40px) !important; }
        .col {
            width: 100% !important; }
        .col > div {
            margin: 0 auto; }
        img.fullwidth, img.fullwidthOnMobile {
            max-width: 100% !important; }
        .no-stack .col {
            min-width: 0 !important;
            display: table-cell !important; }
        .no-stack.two-up .col {
            width: 50% !important; }
        .no-stack.mixed-two-up .col.num4 {
            width: 33% !important; }
        .no-stack.mixed-two-up .col.num8 {
            width: 66% !important; }
        .no-stack.three-up .col.num4 {
            width: 33% !important; }
        .no-stack.four-up .col.num3 {
            width: 25% !important; } }
</style>
</head>
<body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #E3E8E5">
<style type="text/css" id="media-query-bodytag">
    @media (max-width: 520px) {
        .block-grid {
            min-width: 320px!important;
            max-width: 100%!important;
            width: 100%!important;
            display: block!important;
        }
        .col {
            min-width: 320px!important;
            max-width: 100%!important;
            width: 100%!important;
            display: block!important;
        }
        .col > div {
            margin: 0 auto;
        }
        img.fullwidth {
            max-width: 100%!important;
        }
        img.fullwidthOnMobile {
            max-width: 100%!important;
        }
        .no-stack .col {
            min-width: 0!important;
            display: table-cell!important;
        }
        .no-stack.two-up .col {
            width: 50%!important;
        }
        .no-stack.mixed-two-up .col.num4 {
            width: 33%!important;
        }
        .no-stack.mixed-two-up .col.num8 {
            width: 66%!important;
        }
        .no-stack.three-up .col.num4 {
            width: 33%!important
        }
        .no-stack.four-up .col.num3 {
            width: 25%!important
        }
    }
    div.centraTabla{
        text-align: center;
    }

    div.centraTabla table {
        margin: 0 auto;
        text-align: left;
    }
</style>
<table class="nl-container" style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #E3E8E5;width: 100%" cellpadding="0" cellspacing="0">
    <tbody>
    <tr style="vertical-align: top">
        <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
            <div style="background-color:transparent;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <div class="col num12" style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">

                                    <div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                        <div align="center"><div style="border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;">&#160;</div></div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>    <div style="background-color:transparent;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #296480;" class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:#296480;">
                        <div class="col num12" style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">

                                    <div style="padding-right: 0px; padding-left: 0px; padding-top: 0px; padding-bottom: 0px;">
                                        <div align="center"><div style="border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;">&#160;</div></div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>    <div style="background-color:#E3E8E5;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;" class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
                        <div class="col num12" style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">

                                    <div style="font-family:'Roboto', Tahoma, Verdana, Segoe, sans-serif;line-height:150%;color:#555555; padding-right: 30px; padding-left: 30px; padding-top: 30px; padding-bottom: 5px;">
                                        <div style="font-size:12px;line-height:18px;color:#555555;font-family:'Roboto', Tahoma, Verdana, Segoe, sans-serif;text-align:left;"><p style="margin: 0;font-size: 12px;line-height: 18px;text-align: center"><span style="font-size: 24px; line-height: 36px;"><strong><span style="line-height: 36px; font-size: 24px;"><span style="line-height: 36px; font-size: 24px;">Estimados <br></span></span></strong></span></p>
                                            <p style="margin: 0;font-size: 12px;line-height: 18px;text-align: center"><span style="font-size: 18px; line-height: 27px;">Se realizo el cambio de <?php echo ($dorte!=''?'Orte':'').$unn.($dnorden!=''?' Orden ':''); ?> del vehículo con placa <strong><?= $VhObj->placa ?>&#160;</strong></span><span style="font-size: 18px; line-height: 27px;">del cliente <strong><?= $nombreCl ?> <strong></span></p></div>
                                    </div>
                                    <div style="font-family:'Roboto', Tahoma, Verdana, Segoe, sans-serif;line-height:150%;color:#555555; padding-right: 30px; padding-left: 30px; padding-top: 30px; padding-bottom: 5px;">
                                        <div class="centraTabla" style="font-size:12px;line-height:18px;color:#555555;font-family:'Roboto', Tahoma, Verdana, Segoe, sans-serif;text-align:center;">
                                                <table style="border-color: grey" border="1">
                                                    <thead>
                                                        <tr>
                                                            <th width="80px"> </th>
                                                            <th width="100px">Antes</th>
                                                            <th width="100px">Ahora</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    if($dorte!=''){
                                                        echo '  <tr>
                                                            <td>Order</td>
                                                            <td>'.$opera->numeroOrte.'</td>
                                                            <td>'.$_POST['orte'].'</td>
                                                         </tr>';
                                                    }
                                                    if($dnorden!=''){
                                                        echo '  <tr>
                                                            <td>Orden</td>
                                                            <td>'.$DFObj->orden.'</td>
                                                            <td>'.$_POST['numeroOrdenF'].'</td>
                                                         </tr>';
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>    <div style="background-color:transparent;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;" class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
                        <div class="col num12" style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;">


                                    <div align="center" class="button-container center" style="padding-right: 10px; padding-left: 10px; padding-top:15px; padding-bottom:15px;">
                                        <div  href='http://resh.tracklink.pe/conta/gestionContabilidad' style="color: #ffffff; background-color: #296480; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 142px; width: 102px;width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent; padding-top: 5px; padding-right: 20px; padding-bottom: 5px; padding-left: 20px; font-family: 'Roboto', Tahoma, Verdana, Segoe, sans-serif; text-align: center; mso-border-alt: none;">
                                            <span style="font-size:12px;line-height:24px;"><span style="font-size: 20px; line-height: 40px;" data-mce-style="font-size: 20px;"><strong><a href='http://resh.tracklink.pe/conta/gestionContabilidad' style='color:#fff;text-decoration:none'>VERIFICAR</a></strong></span></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>    <div style="background-color:transparent;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #296480;" class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:#296480;">
                        <div class="col num12" style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">

                                    <div style="font-family:'Roboto', Tahoma, Verdana, Segoe, sans-serif;line-height:120%;color:#FFFFFF; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                        <?php $o=Usuario::obtenerUsuario($session->idusuario); ?>
                                        <div style="font-size:12px;line-height:14px;color:#FFFFFF;font-family:'Roboto', Tahoma, Verdana, Segoe, sans-serif;text-align:left;"><p style="margin: 0;font-size: 12px;line-height: 14px;text-align: center"><span style="font-size: 10px; line-height: 12px;"><span style="line-height: 12px; font-size: e0px;">Este correo se envió a contabilidadtrk@tracklink.pe modificación realizada por  <?php  echo $o->usuarioNombre." ".$o->usuarioApellidoPaterno;  ?></span></span></p></div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>    <div style="background-color:transparent;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <div class="col num12" style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">

                                    <div style="padding-right: 0px; padding-left: 0px; padding-top: 0px; padding-bottom: 0px;">
                                        <div align="center"><div style="border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;">&#160;</div></div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>    <div style="background-color:transparent;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <div class="col num12" style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:20px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;">

                                    <div style="padding-right: 0px; padding-left: 0px; padding-top: 0px; padding-bottom: 0px;">
                                        <div align="center"><div style="border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;">&#160;</div></div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </td>
    </tr>
    </tbody>
</table>
</body></html>

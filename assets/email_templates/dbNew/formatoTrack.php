<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Alerta de Desconexion de bateria o variación de voltage</title>
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
<style type="text/css" id="media-query">
body {
margin: 0;
padding: 0; }
table, tr, td {
vertical-align: top;
border-collapse: collapse; }

.ie-browser table, .mso-container table {
table-layout: fixed; }

* {
line-height: inherit; }

a[x-apple-data-detectors=true] {
color: inherit !important;
text-decoration: none !important; }

.img-container div,  .img-container button {
display: block !important; }

.fullwidth button {
width: 100% !important; }

.block-grid .col {
display: table-cell;
float: none !important;
vertical-align: top; }

.ie-browser .num12, .ie-browser .block-grid,  .num12,  .block-grid {
width: 620px !important; }

.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
line-height: 100%; }

.ie-browser .mixed-two-up .num4,  .mixed-two-up .num4 {
width: 204px !important; }

.ie-browser .mixed-two-up .num8,  .mixed-two-up .num8 {
width: 408px !important; }

.ie-browser .block-grid.two-up .col,  .block-grid.two-up .col {
width: 310px !important; }

.ie-browser .block-grid.three-up .col,  .block-grid.three-up .col {
width: 206px !important; }

.ie-browser .block-grid.four-up .col,  .block-grid.four-up .col {
width: 155px !important; }

.ie-browser .block-grid.five-up .col,  .block-grid.five-up .col {
width: 124px !important; }

.ie-browser .block-grid.six-up .col,  .block-grid.six-up .col {
width: 103px !important; }

.ie-browser .block-grid.seven-up .col,  .block-grid.seven-up .col {
width: 88px !important; }

.ie-browser .block-grid.eight-up .col,  .block-grid.eight-up .col {
width: 77px !important; }

.ie-browser .block-grid.nine-up .col,  .block-grid.nine-up .col {
width: 68px !important; }

.ie-browser .block-grid.ten-up .col,  .block-grid.ten-up .col {
width: 62px !important; }

.ie-browser .block-grid.eleven-up .col,  .block-grid.eleven-up .col {
width: 56px !important; }

.ie-browser .block-grid.twelve-up .col,  .block-grid.twelve-up .col {
width: 51px !important; }

@media only screen and (min-width: 640px) {
.block-grid {
width: 620px !important; }
.block-grid .col {
vertical-align: top; }
.block-grid .col.num12 {
width: 620px !important; }
.block-grid.mixed-two-up .col.num4 {
width: 204px !important; }
.block-grid.mixed-two-up .col.num8 {
width: 408px !important; }
.block-grid.two-up .col {
width: 310px !important; }
.block-grid.three-up .col {
width: 206px !important; }
.block-grid.four-up .col {
width: 155px !important; }
.block-grid.five-up .col {
width: 124px !important; }
.block-grid.six-up .col {
width: 103px !important; }
.block-grid.seven-up .col {
width: 88px !important; }
.block-grid.eight-up .col {
width: 77px !important; }
.block-grid.nine-up .col {
width: 68px !important; }
.block-grid.ten-up .col {
width: 62px !important; }
.block-grid.eleven-up .col {
width: 56px !important; }
.block-grid.twelve-up .col {
width: 51px !important; } }

@media (max-width: 640px) {
.block-grid, .col {
min-width: 320px !important;
max-width: 100% !important;
display: block !important; }
.block-grid {
width: calc(100% - 40px) !important; }
.col {
width: 100% !important; }
.col > div {
margin: 0 auto; }
img.fullwidth, img.fullwidthOnMobile {
max-width: 100% !important; }
.no-stack .col {
min-width: 0 !important;
display: table-cell !important; }
.no-stack.two-up .col {
width: 50% !important; }
.no-stack.mixed-two-up .col.num4 {
width: 33% !important; }
.no-stack.mixed-two-up .col.num8 {
width: 66% !important; }
.no-stack.three-up .col.num4 {
width: 33% !important; }
.no-stack.four-up .col.num3 {
width: 25% !important; } }


</style>
</head>
<body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #FFFFFF">
<style type="text/css" id="media-query-bodytag">
@media (max-width: 520px) {
.block-grid {
min-width: 320px!important;
max-width: 100%!important;
width: 100%!important;
display: block!important;
}
.col {
min-width: 320px!important;
max-width: 100%!important;
width: 100%!important;
display: block!important;
}
.col > div {
margin: 0 auto;
}
img.fullwidth {
max-width: 100%!important;
}
img.fullwidthOnMobile {
max-width: 100%!important;
}
.no-stack .col {
min-width: 0!important;
display: table-cell!important;
}
.no-stack.two-up .col {
width: 50%!important;
}
.no-stack.mixed-two-up .col.num4 {
width: 33%!important;
}
.no-stack.mixed-two-up .col.num8 {
width: 66%!important;
}
.no-stack.three-up .col.num4 {
width: 33%!important
}
.no-stack.four-up .col.num3 {
width: 25%!important
}
}
</style>
<style>
    
.tabl{
border: 0px solid #ccc;
border-collapse: collapse;
margin: 0;
padding: 0;
width: 100%;
table-layout: fixed;
}
.capt{
font-size: 1.5em;
margin: .5em 0 .75em;
}
.tr {
background: #fff;
border: 1px solid #ddd;
padding: .35em;
}
.thtd {
padding: .400em;
text-align: center;
}
.th {
font-size: .70em;
text-transform: capitalize;
}
@media screen and (max-width: 600px) {
#tabl {
border: 0;
}
.thead {
border: none;
clip: rect(0 0 0 0);
height: 1px;
margin: -1px;
overflow: hidden;
padding: 0;
position: absolute;
width: 1px;
}
.trm {
border-bottom: 3px solid #ddd;
display: block;
margin-bottom: .625em;
}
.tdm{
border-bottom: 1px solid #ddd;
display: block;
font-size: .8em;
text-align: center;
}
.tdb:before {
content: attr(data-label);
float: left;
font-weight: bold;
text-transform:capitalize;
}
.tdl:last-child {
border-bottom: 0;
}
.catpm {
font-size: 1.3em;
}

}
    </style>
<div class="ie-browser">
<table class="nl-container" style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #FFFFFF;width: 100%" cellpadding="0" cellspacing="0">
<tbody>
<tr style="vertical-align: top">
<td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<div class="col num12" style="min-width: 320px;max-width: 620px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
<div style="padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 0px;">
<div align="center"><div style="border-top: 2px solid #B5B5B5; width:100%; line-height:2px; height:2px; font-size:2px;">&#160;</div></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<div class="col num12" style="min-width: 320px;max-width: 620px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
<div style="font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:150%;color:#71777D; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 30px;">	
    <div style="font-size:12px;line-height:18px;color:#3e4144;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;"><div class="txtTinyMce-wrapper" style="font-size:12px; line-height:18px;">
    <p style="margin: 0;font-size: 14px;line-height: 21px"><span style="color: rgb(0, 0, 0); font-size: 14px; line-height: 21px;"><strong> Estimado: <?= ucwords(strtolower($nombres)) ?><span style="font-size: 14px; line-height: 21px;"> </span></strong></span></p>
        <p style="margin: 0;font-size: 14px;line-height: 21px">Reciba cordiales saludos de TRACKLINK OCTO. Usted actualmente cuenta con el servicio de control y recuperación vehicular.</p>
<table class="tabl">
<thead class="thead">
	<tr class="tr trm">
		<th class="thtd th">Marca/modelo</th>
		<th class="thtd th">Placa</th>
		<th class="thtd th">Chasis</th>
		<th class="thtd th">Hora</th>
	</tr>
</thead>
<tbody>

<tr class="tr trm">
<td data-label="Marca/modelo" class="thtd th tdm tdb tdl"><?= $marca." / ".$modelo ?></td>
<td data-label="Placa" class="thtd th tdm tdb tdl"><?= $placaVehiculo ?></td>
<td data-label="Chasis" class="thtd th tdm tdb tdl"><?= $chasis ?></td>
<td data-label="Hora" class="thtd th tdm tdb tdl"><?= $hora.":".$minuto." ".$amOpm ?></td>
</tr>
</tbody>
</table><br>
        <p style="margin: 0;font-size: 14px;line-height: 21px">Deseamos confirmar la alerta que hemos recibido por “desconexión de batería” o “variación de voltaje”, registrada en nuestro sistema. Agradeceremos nos brinde mayor detalle de lo sucedido comunicándose a la oficina central de TRACKLINK OCTO llamando al (01)630-7576. Le recordamos la importancia de realizar un chequeo cada 6 meses para garantizar el correcto funcionamiento de nuestro sistema, este chequeo se realiza en nuestro taller y es totalmente gratuito.</p>
        <br>
        <div style='color: #000066; font-family: sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'><span color='#17365d' style='color: #17365d;'><span style='font-size: 13.3333px;'>Un cordial saludo,</span></span></div>
        <div style='color: #000066; font-family: sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'></div>
        <div style='color: #000066; font-family: sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'><a href='http://www.tracklink.pe' title='Tracklink'><img src='cid:tracklink' /></a></div>
        <p style='color: #000066; font-family: sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; margin: 0px;'><span style='font-family: sans-serif; color: #17365d;'>&nbsp;</span></p>
        <div style='color: #000066; font-family: sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'>
        <p style='margin: 0px;'><span style='color: #003366;'><strong><?= $nombreEnviador ?><br/></strong></span></p>
        <p style='margin: 0px;'><span style='font-size: 11.5pt; color: #003366;'><?= $cargo ?>.<br /></span></p>
            <p style='margin: 0px;'><strong class='' style='color: #003366; font-family:  sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'><span class='' style='font-size: 11.5pt;'>Tracklink-</span></strong><strong class='' style='color: #003366; font-family:  sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'><span class='' style='font-size: 11.5pt;'>Octo Telematics</span></strong></p>
            <p style='margin: 0px;'><span style='color: #003366;'><span style='font-size: 11.5pt;'>Calle Ricardo Angulo 762 - Urb Corpac - San Isidro, Lima.<span style='color: #17365d;'><span class='Object' id='OBJ_PREFIX_DWT1132_com_zimbra_phone' style='color: #005a95; text-decoration: none; cursor: pointer;'><a href='callto:+51%20950.596.228' target='_blank' style='color: #005a95; text-decoration: none; cursor: pointer;'> </a></span></span></span><span style='font-size: 11.5pt;'><br /></span><span style='font-size: 11.5pt;'>Oficina principal:&nbsp;<span class='' style='color: #17365d; orphans: 2; widows: 2;'><span class='Object' id='OBJ_PREFIX_DWT1131_com_zimbra_phone' style='color: #005a95; text-decoration: none; cursor: pointer;'><a href='callto:+511%20630%207575' target='_blank' style='color: #005a95; text-decoration: none; cursor: pointer;'>+511 630 7576</a></span></span>. &nbsp;</span><br /></span></p>
            <div class='' style='color: #000000; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; line-height: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; font-family: helvetica; font-size: 12px; background-color: #ffffff;'><span class='' style='font-size: 11pt; color: #0096ff; font-family: calibri, sans-serif; orphans: 2; widows: 2;'><span class='Object' id='OBJ_PREFIX_DWT1133_com_zimbra_url' style='color: #005a95; text-decoration: none; cursor: pointer;'><span class='Object' id='OBJ_PREFIX_DWT1169_com_zimbra_url' style='color: #005a95; text-decoration: none; cursor: pointer;'><span class='Object' id='OBJ_PREFIX_DWT435_com_zimbra_url' style='color: #005a95; text-decoration: none; cursor: pointer;'><span class='Object' id='OBJ_PREFIX_DWT500_com_zimbra_url' style='color: #005a95; text-decoration: none; cursor: pointer;'><span class='Object' role='link' id='OBJ_PREFIX_DWT53_com_zimbra_url' style='color: #005a95; text-decoration: none; cursor: pointer;'><a href='http://www.tracklink.pe/' class='' target='_blank' style='color: #005a95; text-decoration: none; cursor: pointer;'>www.tracklink.pe</a></span></span></span></span></span>&nbsp;/<a href='https://www.octotelematics.com/' class='' target='_blank' style='color: #005a95; text-decoration: none; cursor: pointer;'>www.octotelematics.com</a></span></div>
            <div class='' style='color: #000000; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; line-height: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; font-family: helvetica; font-size: 12px; background-color: #ffffff;'><span class='' style='font-size: 11pt; color: #0096ff; font-family: calibri, sans-serif; orphans: 2; widows: 2;'><span class='Object' role='link' id='OBJ_PREFIX_DWT84_com_zimbra_url' style='color: #005a95; text-decoration: none; cursor: pointer; font-family:  sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'><a title='facebook' href='https://www.facebook.com/tracklinkperu/' target='_blank' style='color: #005a95; text-decoration: none; cursor: pointer;'><span class='Object' id='OBJ_PREFIX_DWT389_com_zimbra_url' style='color: #005a95; text-decoration: none; cursor: pointer;'><img src='cid:facebook' width='28' height='37' /></span></a></span><span style='font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: #000000; font-family: arial, helvetica, sans-serif; background-color: #ffffff; float: none; display: inline !important;'><span class='Object' role='link' id='OBJ_PREFIX_DWT85_com_zimbra_url' style='color: #005a95; text-decoration: none; cursor: pointer;'><a title='youtube' href='https://www.youtube.com/channel/UCylRCGZ4LBaNKk-XMJliURw' target='_blank' style='color: #005a95; text-decoration: none; cursor: pointer;'><img src='cid:youtube'  width='31' height='37' /></a></span><span class='Object' role='link' id='OBJ_PREFIX_DWT86_com_zimbra_url' style='color: #005a95; text-decoration: none; cursor: pointer;'><a title='twitter' href='https://twitter.com/TracklinkPE' target='_blank' style='color: #005a95; text-decoration: none; cursor: pointer;'><img src='cid:twitter'  width='33' height='39' /></a></span></span></span></div>
        </div>
</div> </div> </div>
</div>
</div>
</div>
</div>

<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<div class="col num12" style="min-width: 320px;max-width: 620px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
<div style="padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 0px;">
<div align="center"><div style="border-top: 2px solid #B5B5B5; width:100%; line-height:2px; height:2px; font-size:2px;">&#160;</div></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid two-up ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<div class="col num6" style="max-width: 320px;min-width: 310px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
<div style="color:#969696;line-height:120%;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">	
<div style="font-size:12px;line-height:14px;color:#969696;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;"></div>	
</div>
</div>
</div>
</div>
<div class="col num6" style="max-width: 320px;min-width: 310px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
<div style="color:#969696;line-height:120%;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">	
<div style="font-size:12px;line-height:14px;color:#969696;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;"><p style="margin: 0;font-size: 14px;line-height: 17px;text-align: right"><strong>www.tracklink.pe</strong></p></div>	
</div>
</div>
</div>
</div>
</div>
</div>
</div> 
</td>
</tr>
</tbody>
</table>
</body>
</html>

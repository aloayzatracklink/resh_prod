<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
<style type="text/css" id="media-query">
body {
margin: 0;
padding: 0; }

table, tr, td {
vertical-align: top;
border-collapse: collapse; }

.ie-browser table, .mso-container table {
table-layout: fixed; }

* {
line-height: inherit; }

a[x-apple-data-detectors=true] {
color: inherit !important;
text-decoration: none !important; }

.img-container div,  .img-container button {
display: block !important; }

.fullwidth button {
width: 100% !important; }

.block-grid .col {
display: table-cell;
float: none !important;
vertical-align: top; }

.ie-browser .num12, .ie-browser .block-grid,  .num12,  .block-grid {
width: 620px !important; }

.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
line-height: 100%; }

.ie-browser .mixed-two-up .num4,  .mixed-two-up .num4 {
width: 204px !important; }

.ie-browser .mixed-two-up .num8,  .mixed-two-up .num8 {
width: 408px !important; }

.ie-browser .block-grid.two-up .col,  .block-grid.two-up .col {
width: 310px !important; }

.ie-browser .block-grid.three-up .col,  .block-grid.three-up .col {
width: 206px !important; }

.ie-browser .block-grid.four-up .col,  .block-grid.four-up .col {
width: 155px !important; }

.ie-browser .block-grid.five-up .col,  .block-grid.five-up .col {
width: 124px !important; }

.ie-browser .block-grid.six-up .col,  .block-grid.six-up .col {
width: 103px !important; }

.ie-browser .block-grid.seven-up .col,  .block-grid.seven-up .col {
width: 88px !important; }

.ie-browser .block-grid.eight-up .col,  .block-grid.eight-up .col {
width: 77px !important; }

.ie-browser .block-grid.nine-up .col,  .block-grid.nine-up .col {
width: 68px !important; }

.ie-browser .block-grid.ten-up .col,  .block-grid.ten-up .col {
width: 62px !important; }

.ie-browser .block-grid.eleven-up .col,  .block-grid.eleven-up .col {
width: 56px !important; }

.ie-browser .block-grid.twelve-up .col,  .block-grid.twelve-up .col {
width: 51px !important; }

@media only screen and (min-width: 640px) {
.block-grid {
width: 620px !important; }
.block-grid .col {
vertical-align: top; }
.block-grid .col.num12 {
width: 620px !important; }
.block-grid.mixed-two-up .col.num4 {
width: 204px !important; }
.block-grid.mixed-two-up .col.num8 {
width: 408px !important; }
.block-grid.two-up .col {
width: 310px !important; }
.block-grid.three-up .col {
width: 206px !important; }
.block-grid.four-up .col {
width: 155px !important; }
.block-grid.five-up .col {
width: 124px !important; }
.block-grid.six-up .col {
width: 103px !important; }
.block-grid.seven-up .col {
width: 88px !important; }
.block-grid.eight-up .col {
width: 77px !important; }
.block-grid.nine-up .col {
width: 68px !important; }
.block-grid.ten-up .col {
width: 62px !important; }
.block-grid.eleven-up .col {
width: 56px !important; }
.block-grid.twelve-up .col {
width: 51px !important; } }

@media (max-width: 640px) {
.block-grid, .col {
min-width: 320px !important;
max-width: 100% !important;
display: block !important; }
.block-grid {
width: calc(100% - 40px) !important; }
.col {
width: 100% !important; }
.col > div {
margin: 0 auto; }
img.fullwidth, img.fullwidthOnMobile {
max-width: 100% !important; }
.no-stack .col {
min-width: 0 !important;
display: table-cell !important; }
.no-stack.two-up .col {
width: 50% !important; }
.no-stack.mixed-two-up .col.num4 {
width: 33% !important; }
.no-stack.mixed-two-up .col.num8 {
width: 66% !important; }
.no-stack.three-up .col.num4 {
width: 33% !important; }
.no-stack.four-up .col.num3 {
width: 25% !important; } }

</style>
</head>
<body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #FFFFFF">
<style type="text/css" id="media-query-bodytag">
@media (max-width: 520px) {
.block-grid {
min-width: 320px!important;
max-width: 100%!important;
width: 100%!important;
display: block!important;
}

.col {
min-width: 320px!important;
max-width: 100%!important;
width: 100%!important;
display: block!important;
}

.col > div {
margin: 0 auto;
}

img.fullwidth {
max-width: 100%!important;
}
img.fullwidthOnMobile {
max-width: 100%!important;
}
.no-stack .col {
min-width: 0!important;
display: table-cell!important;
}
.no-stack.two-up .col {
width: 50%!important;
}
.no-stack.mixed-two-up .col.num4 {
width: 33%!important;
}
.no-stack.mixed-two-up .col.num8 {
width: 66%!important;
}
.no-stack.three-up .col.num4 {
width: 33%!important
}
.no-stack.four-up .col.num3 {
width: 25%!important
}
}
</style>
<style>
.chart {
	display: table;
	table-layout: fixed;
	width: 100%;
	max-width: 700px;
	height: 200px;
	margin: 0 auto;
	background-image: linear-gradient(to top, rgba(0, 0, 0, 0.1) 2%, rgba(0, 0, 0, 0) 2%);
	background-size: 100% 50px;
	background-position: left top;
}
.chart li {
	position: relative;
	display: table-cell;
	vertical-align: bottom;
	height: 200px;
}
.chart span {
	margin: 0 1em;
	display: block;
	background: #acd3e2;;
	animation: draw 1s ease-in-out;
}
.chart span:before {
	position: absolute;
	left: 0;
	right: 0;
	top: 100%;
	padding: 5px 1em 0;
	display: block;
	text-align: center;
	content: attr(title);
	word-wrap: break-word;
}

.tabl{
border: 0px solid #ccc;
border-collapse: collapse;
margin: 0;
padding: 0;
width: 100%;
table-layout: fixed;
}
.capt{
font-size: 1.5em;
margin: .5em 0 .75em;
}
.tr {
background: #fff;
border: 1px solid #ddd;
padding: .35em;
}
.thtd {
padding: .400em;
text-align: center;
}
.th {
font-size: .70em;
text-transform: capitalize;
}
@media screen and (max-width: 600px) {
#tabl {
border: 0;
}
.thead {
border: none;
clip: rect(0 0 0 0);
height: 1px;
margin: -1px;
overflow: hidden;
padding: 0;
position: absolute;
width: 1px;
}
.trm {
border-bottom: 3px solid #ddd;
display: block;
margin-bottom: .625em;
}
.tdm{
border-bottom: 1px solid #ddd;
display: block;
font-size: .8em;
text-align: center;
}
.tdb:before {
content: attr(data-label);
float: left;
font-weight: bold;
text-transform:capitalize;
}
.tdl:last-child {
border-bottom: 0;
}
.catpm {
font-size: 1.3em;
}

}
</style>
<div class="ie-browser">
<table class="nl-container" style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #FFFFFF;width: 100%" cellpadding="0" cellspacing="0">
<tbody>
<tr style="vertical-align: top">
<td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<div class="col num12" style="min-width: 320px;max-width: 620px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
<div align="center" class="img-container center fixedwidth" style="padding-right: 0px;  padding-left: 0px;">
<img class="center fixedwidth" align="center" border="0" src="cid:head" alt="Tracklink Octo" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 620px" width="620">
</div>
</div>
</div>
</div>
</div>
</div>
</div>   
<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<div class="col num12" style="min-width: 320px;max-width: 620px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
<div style="padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 0px;">
    <div align="center">
        <div style="border-top: 2px solid #B5B5B5; width:100%; line-height:2px; height:2px; font-size:2px;">&#160;</div></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<div class="col num12" style="min-width: 320px;max-width: 620px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
<div style="font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:150%;color:#71777D; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 30px;">	
<div style="font-size:12px;line-height:18px;color:#71777D;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;">
<div class="txtTinyMce-wrapper" style="font-size:12px; line-height:18px;">

<p style="margin: 0;font-size: 14px;line-height: 21px;text-align: center; "><span style="color: rgb(0, 0, 0); font-size: 16px; line-height: 21px;"><strong>Reporte de operaciones</strong></span></p>


<p style="margin: 0;font-size: 14px;line-height: 21px">&#160;<br></p>
<p style="margin: 0;font-size: 14px;line-height: 21px;text-align: center;;"><strong>Por trabajo</strong></p>

<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
<div class="col num12" style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:55px; padding-right: 0px; padding-left: 0px;">
<div style="font-size: 16px;font-family:'Roboto', Tahoma, Verdana, Segoe, sans-serif; text-align: center;">
 <ul class="chart">
<?php
   $apD=$opeObj->obtenerActividadesProgramadas($desde,$hasta); 
   foreach($apD as $var){
       $opeObj->trabajo=$var->trabajo;
    $ct=$opeObj->obtenerTrabajoProgramado($desde,$hasta);
       $porcentaje=porcentajeD($total,$ct);
       if ($porcentaje<=20) {
           ?>
	<li>
    <p style="color: #5b5d60; padding-bottom: 0px;margin-bottom: 0px;text-aling:center"><?= $ct ?><br><?= $var->trabajo ?></p>
    <span style="height:<?= $porcentaje ?>%;margin: 3px" ></span>
	</li>
           <?php
       }else{
           ?>
	<li>
    <span style="height:<?= $porcentaje ?>%;margin: 3px" ><p style="color: #5b5d60;  "><?= $ct ?><br><?= $var->trabajo ?></p></span>
	</li>
           <?php
       }
       ?>
       <?php
   }
?>
<li>
<span style="height:100%;margin: 3px; background: #abe4e6"><p style="color: #5b5d60"><?= $total ?><br>Total</p></span>
</li>
</ul>          
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<p style="margin: 0;font-size: 14px;line-height: 21px;text-align: center;;"><strong>Por lugar</strong></p>

<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
<div class="col num12" style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:55px; padding-right: 0px; padding-left: 0px;">
<div style="font-size: 16px;font-family:'Roboto', Tahoma, Verdana, Segoe, sans-serif; text-align: center;">
<ul class="chart">
<?php
   $apD=$opeObj->obtenerLugaresDetrabajo($desde,$hasta); 
   foreach($apD as $var){
       $opeObj->lugarInstalacion=$var->lugarInstalacion;
    $ctl=$opeObj->obtenerTrabajoProgramadoLugar($desde,$hasta);
       $porcentaje=porcentajeD($total,$ctl);
       if ($porcentaje<=20) {
           ?>
	<li>
    <p style="color: #5b5d60; padding-bottom: 0px;margin-bottom: 0px;text-aling:center"><?= $ctl ?><br><?= $var->lugarInstalacion ?></p>
    <span style="height:<?= $porcentaje ?>%;margin: 3px" ></span>
	</li>
           <?php
       }else{
           ?>
	<li>
    <span style="height:<?= $porcentaje ?>%;margin: 3px" ><p style="color: #5b5d60;  "><?= $ctl ?><br><?= $var->lugarInstalacion  ?></p></span>
	</li>
           <?php
       }
       ?>
       <?php
   }
?>
<li>
<span style="height:100%;margin: 3px; background: #abe4e6"><p style="color: #5b5d60"><?= $total ?><br>Total</p></span>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>



<p style="margin: 0;font-size: 14px;line-height: 21px;text-align: center;;"><strong>Por tipo en instalaciones</strong></p>
<p style="margin: 0;font-size: 14px;line-height: 21px">&#160;<br></p>
<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
<div class="col num12" style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:55px; padding-right: 0px; padding-left: 0px;">
<div style="font-size: 16px;font-family:'Roboto', Tahoma, Verdana, Segoe, sans-serif; text-align: center;">
<ul class="chart">
<?php
   if($cita=$opeObj->obtenerCuantosPorCita($desde,$hasta)){
    $porcentajeCia=porcentajeD($total,$cita);
    $instala=$opeObj->obtenerInstalacionesN($desde,$hasta);
    $ventas=$instala-$cita;
    $totalV=$ventas+$cita;
    $porcentajeVentas=porcentajeD($total,$ventas);
    if ($porcentajeCia<=20) {
        ?>
<li>
    <p style="color: #5b5d60; padding-bottom: 0px;margin-bottom: 0px;text-aling:center"><?= $cita ?><br>Citas</p>
    <span style="height:<?= $porcentajeCia ?>%;margin: 3px" ></span>
	</li>
        <?php
    }else{
   ?>
       <li>
    <span style="height:<?= $porcentajeCia ?>%;margin: 3px" ><p style="color: #5b5d60;  "><?= $cita ?><br>Citas</p></span>
	</li>
   <?php 
    }
    if ($porcentajeVentas<=20) {
        ?>
<li>
    <p style="color: #5b5d60; padding-bottom: 0px;margin-bottom: 0px;text-aling:center"><?= $ventas ?><br>Ventas</p>
    <span style="height:<?= $porcentajeVentas ?>%;margin: 3px" ></span>
	</li>
        <?php
    }else{
   ?>
       <li>
    <span style="height:<?= $porcentajeVentas ?>%;margin: 3px" ><p style="color: #5b5d60;  "><?= $ventas ?><br>Ventas</p></span>
	</li>
   <?php 
    }

   }
?>
<?php
?>
<li>
<span style="height:100%;margin: 3px; background: #abe4e6"><p style="color: #5b5d60"><?= $totalV ?><br>Total</p></span>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<?php
    if($datosR=$opeObj->obtenerReprogramadosOperaciones($desde,$hasta)){
   ?>
 <p style="margin: 0;font-size: 14px;line-height: 21px;text-align: center;;"><strong>Reprogramaciones</strong></p>
<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
<div class="col num12" style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:55px; padding-right: 0px; padding-left: 0px;">
<div style="font-size: 16px;font-family:'Roboto', Tahoma, Verdana, Segoe, sans-serif; text-align: center;">
<table class="tabl">
<thead class="thead">
	<tr class="tr trm">
		<th class="thtd th">Tipo</th>
		<th class="thtd th">Lugar</th>
		<th class="thtd th">Distrito</th>
		<th class="thtd th">Observación</th>
		<th class="thtd th">Ejecutivo</th>
		<th class="thtd th">Orte</th>
	</tr>
</thead>
<tbody>
<?php
        $totalR=count($datosR);
    foreach($datosR as $var){
        if ($var->idConfirmacionOPE=='0') {
            $usD=Usuario::obtenerUsuario($var->idCreador);
        }else{
            $usD=Usuario::obtenerUsuario($var->idConfirmacionOPE);
        }
        ?>
<tr class="tr trm">
    <td data-label="Lugar" class="thtd th tdm tdb tdl"><?= $var->lugarInstalacion ?></td>
    <td data-label="Trabajo" class="thtd th tdm tdb tdl"><?= $var->trabajo ?></td>
    <td data-label="Distrito" class="thtd th tdm tdb tdl"><?= $var->distritoi ?></td>
    <td data-label="Observación" class="thtd th tdm tdb tdl"><?= $var->observacion ?></td>
    <td data-label="Ejecutivo" class="thtd th tdm tdb tdl"><?= $usD->usuarioNombre." ".$usD->usuarioApellidoPaterno ?></td>
    <td data-label="Orte" class="thtd th tdm tdb tdl"><?= !empty($var->numeroOrte)? $var->numeroOrte : 'Sin orte' ?></td>
</tr>
        <?php
    }
?>


<!-- total -->
<tr class="tr trm" style="background:#abe4e6;">
	<td  class="thtd th tdm tdb tdl"></td>
	<td  class="thtd th tdm tdb tdl" colspan='3'></td>
	<td  class="thtd th tdm tdb tdl" ><strong>Total</strong></td>
    <td class="thtd th tdm tdb tdl"><strong><?= $totalR ?></strong></td>
</tr>
<!-- total -->
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>      
   <?php 
    }
?>


<?php
    if ($dataC=$opeObj->obtenerReprogramadosCitas($desde,$hasta)) {
        ?>
            <p style="margin: 0;font-size: 14px;line-height: 21px;text-align: center;;"><strong>Citas no realizadas</strong></p>
<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
<div class="col num12" style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:55px; padding-right: 0px; padding-left: 0px;">
<div style="font-size: 16px;font-family:'Roboto', Tahoma, Verdana, Segoe, sans-serif; text-align: center;">
<table class="tabl">
<thead class="thead">
	<tr class="tr trm">
		<th class="thtd th">Ejecutivo</th>
		<th class="thtd th">Observación</th>
	</tr>
</thead>
<tbody>
<?php
$totalC=count($dataC);
foreach($dataC as $var){
        if ($var->idConfirmacionOPE=='0') {
            $usD=Usuario::obtenerUsuario($var->idCreador);
        }else{
            $usD=Usuario::obtenerUsuario($var->idConfirmacionOPE);
        }
    ?>
<tr class="tr trm">
    <td data-label="Ejecutivo" class="thtd th tdm tdb tdl"><?= $usD->usuarioNombre." ".$usD->usuarioApellidoPaterno ?></td>
    <td data-label="Observación" class="thtd th tdm tdb tdl"><?= $var->observacion ?></td>
</tr>
    <?php
}
?>
<!-- total -->
<tr class="tr trm" style="background:#abe4e6;">
	<td  class="thtd th tdm tdb tdl" ><strong>Total</strong></td>
    <td  class="thtd th tdm tdb tdl"><strong><?= $totalC ?></strong></td>
</tr>
<!-- total -->
</tbody>
</table>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
        <?php
        
    }
?>




</div>	
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid two-up ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<div class="col num6" style="max-width: 320px;min-width: 310px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
<div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
<div align="center"><div style="border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;">&#160;</div></div>
</div>
</div>
</div>
</div>
<div class="col num6" style="max-width: 320px;min-width: 310px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 10px; padding-left: 10px;">
<div align="right" class="img-container right  autowidth " style="padding-right: 0px;  padding-left: 0px;">
</div>
<div style="font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%;color:#555555; padding-right: 0px; padding-left: 0px; padding-top: 15px; padding-bottom: 0px;">	
    <div style="font-size:12px;line-height:14px;color:#555555;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;">
        <p style="margin: 0;font-size: 14px;line-height: 17px;text-align: right"><span style="color: rgb(0, 0, 0); font-size: 14px; line-height: 16px;"><strong>Resh</strong></span></p></div>	
</div>
<div style="font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%;color:#555555; padding-right: 0px; padding-left: 0px; padding-top: 0px; padding-bottom: 5px;">	
    <div style="font-size:12px;line-height:14px;color:#555555;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;"><div class="txtTinyMce-wrapper" style="font-size:12px; line-height:14px;">
            <p style="margin: 0;font-size: 14px;line-height: 16px;text-align: right"><span style="font-size: 12px; line-height: 14px;">Tracklink Octo</span></p></div></div>	
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<div class="col num12" style="min-width: 320px;max-width: 620px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
<div style="padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 0px;">
<div align="center"><div style="border-top: 2px solid #B5B5B5; width:100%; line-height:2px; height:2px; font-size:2px;">&#160;</div></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div style="background-color:transparent;">
<div style="Margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;" class="block-grid two-up ">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<div class="col num6" style="max-width: 320px;min-width: 310px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
<div style="color:#969696;line-height:120%;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">	
<div style="font-size:12px;line-height:14px;color:#969696;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;"><p style="margin: 0;font-size: 12px;line-height: 14px"><strong>OF. CA. Ricardo Angulo # 762 Urb. Corpac San isidro</strong></p><p style="margin: 0;font-size: 12px;line-height: 14px"><strong>Taller. Av. Paseo de la&#160; Republica # 4925 Surquillo</strong></p><p style="margin: 0;font-size: 12px;line-height: 14px"><strong>T (01) 630 7575</strong></p></div>	
</div>
</div>
</div>
</div>
<div class="col num6" style="max-width: 320px;min-width: 310px;display: table-cell;vertical-align: top;">
<div style="background-color: transparent; width: 100% !important;">
<div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
<div style="color:#969696;line-height:120%;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">	
<div style="font-size:12px;line-height:14px;color:#969696;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;"><p style="margin: 0;font-size: 14px;line-height: 17px;text-align: right"><strong>www.tracklink.pe</strong></p></div>	
</div>
</div>
</div>
</div>
</div>
</div>
</div> 

</td>
</tr>
</tbody>
</table>
</body>
</html>

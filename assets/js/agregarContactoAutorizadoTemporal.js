var maxField = 30; //Input fields increment limitation
var addButton = $('.add_button'); //Add button selector
var wrapper = $('.field_wrapper'); //Input field wrapper
var fieldHTML = "<div class='field_wrapper'></div><div class='row'><div class='hr-line-dashed'><div class='hr-line-dashed'><div class='form-group col-md-3'><label for='dniC' class='sr-only'>DNI No obligatorio</label><input style='border: none; color:#7b7e80;' type='text' placeholder='DNI No obligatorio' id='dniC' name='field_name[0][]' class='form-control border-bottom '  data-mask='99999999' id='dniI'></div><div class='form-group col-md-3'><label for='nombreC' class='sr-only'>Nombres *</label><input style='border: none;  color:#7b7e80;' type='text' placeholder='Nombres *' id='nombreC' name='field_name[1][]' class='form-control border-bottom text-capitalize'  required id='name'></div><div class='form-group col-md-3'><label for='apellidos' class='sr-only'>Apellidos *</label><input style='border: none;  color:#7b7e80;' type='text' placeholder='Apellidos *' id='apellidos' name='field_name[2][]' class='form-control x2 text-capitalize border-bottom text-capitalize' required ></div><div class='form-group col-md-3'><label for='dniC' class='sr-only'>Celular</label><input style='border: none;  color:#7b7e80;' type='text' placeholder='Celular *' id='celular' name='field_name[3][]' class='form-control border-bottom ' required  data-mask='999 999 999' id='celular'></div></div><div class='row'><div class='form-group col-md-4'><label for='telefono' class='sr-only'>Telefono</label><input style='border: none;  color:#7b7e80;' type='text' placeholder='telefono' id='telefono' name='field_name[4][]' class='form-control border-bottom text-capitalize' id='telefono'></div><div class='form-group col-md-4'><label for='correo' class='sr-only'>Correo</label><input style='border: none;  color:#7b7e80;' type='text' placeholder='Correo' id='correo' name='field_name[5][]' class='form-control x2 text-capitalize border-bottom text-capitalize' ></div><div class='form-group col-md-4'><label for='Parentesco' class='sr-only'>Parentesco</label><select  style='border: none; color:#7b7e80' class='tecnico border-bottom form-control' required name='field_name[6][]'><option class='' value='' disabled selected >Seleccione Parentesco</option><option value='Usuario'>Usuario(a)</option><option value='Padre'>Padre</option><option value='Madre'>Madre</option><option value='Hermano'>Hermano(a)</option><option value='Conyuge'>Conyuge</option><option value='Hijo'>Hijo(a)</option><option value='Nieto'>Nieto(a)</option><option value='Cuñado'>Cuñado(a)</option><option value='Suegro'>Suegro(a)</option></select></div></div><a href='javascript:void(0);' class='remove_button  col-md-3 col-md-offset-9 btn btn-warning' title='quitar contacto'><strong>Quitar </strong><i class='fa fa-minus-circle'></i></a></div>";    
var x = 1; //Initial field counter is 1
$(addButton).click(function(){ //Once add button is clicked
    if(x < maxField){ //Check maximum number of input fields
        x++; //Increment field counter
        $(wrapper).append(fieldHTML); // Add field html
    }
});
$(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
    e.preventDefault();
    $(this).parent('div').remove(); //Remove field html
    x--; //Decrement field counter
});


// #7b7e80
var chat = {}

chat.fetchMessages = function() {
    $.ajax({
        url: 'ajax/chat',
        type: 'post',
        data: {method: 'fetch'},
        success: function(data) {
            $('.small-chat-box .content').html(data);
        }
    }

    );  
}

chat.throwMessage = function(message) {
    if ($.trim(message.length != 0)) {
        $.ajax({
            url: 'ajax/chat',
            type: 'post',
            data: {method: 'throw', message: message},
            success: function(data) {
                chat.fetchMessages();
                chat.entry.val('');
            }
        }

        );
    }
}

chat.entry = $('.small-chat-box .form-chat .input-group .hablar');
chat.entry.bind('keydown', function(e) {
    if (e.keyCode === 13 && e.shiftKey === false) {
        chat.throwMessage($(this).val());
        e.preventDefault();
    }

});

chat.interval = setInterval(chat.fetchMessages, 1000);
chat.fetchMessages();
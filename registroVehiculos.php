<?php $nav="Registro de Vehículos" ?>
<?php include("public/publicHead.php") ?>
<?php

//traer estado de la cotizacion aqui ---- para saber si esta en el lugar correcto :V creo :V 
if (!empty($_SESSION['dni'])||!empty($_SESSION['ruc'])) {
   if (!empty($_SESSION['dni'])) {
      $estadoDniObj=Cotizar::obtenerEstadoDni($_SESSION['dni']);     
      $clienteObj=Cliente::obtenerDatosDeclienteTemporalPorDni($_SESSION['dni']);
      $cotizarObj=Cotizar::obtenerDatosPorDni($_SESSION['dni']);
   }else 
   if(!empty($_SESSION['ruc'])){
      $estadoDniObj=Cotizar::obtenerEstadoRuc($_SESSION['ruc']);
      $clienteObj=Cliente::obtenerDatosDeclienteTemporalPorRuc($_SESSION['ruc']);
      $cotizarObj=Cotizar::obtenerDatosPorRuc($_SESSION['ruc']);
   }   

}else{
    unset($_SESSION['dni']);
    unset($_SESSION['ruc']);
    irA("/");
}

if ($estadoDniObj->estado==17) {
    irA("registrarme");
}

if ($estadoDniObj->estado==24) {
    irA("registroContactosAutorizados");
}

if (isset($_POST['registrarVehiculos'])) {
    // $idcliente=$_SESSION['idcliente'];
    $vehiculoObj=new Vehiculo();
    $field_values_array=$_POST['field_name'];


    $n=0;
    foreach($field_values_array[0] as $val){
        $placa[$n]=$val;
        $n++;
    }

    $m=0;
    foreach($field_values_array[1] as $val) {
        $chasis[$m]=$val;
        $m++;
    }

    $x=0;
    foreach($field_values_array[2] as $val) {
        $motor[$x]=$val;
        $x++;
    }
    
    $y=0;
    foreach($field_values_array[3] as $val) {
        $marca[$y]=$val;
        $y++;
    }
    
    $z=0;
    foreach($field_values_array[4] as $val) {
        $modelo[$z]=$val;
        $z++;
    }

    $a=0;
    foreach($field_values_array[5] as $val) {
        $color[$a]=$val;
        $a++;
    }

    $b=0;
    foreach($field_values_array[6] as $val) {
        $anio[$b]=$val;
        $b++;
    }
    $c=0;
    foreach($field_values_array[7] as $val) {
        $seguro[$c]=$val;
        $c++;
    }
    $d=0;
    foreach($field_values_array[8] as $val) {
        $numeroTramiteSeguro[$d]=$val;
        $d++;
    }
    $e=0;
    foreach($field_values_array[9] as $val) {
        $numeroPoliza[$e]=$val;
        $e++;
    }
    $f=0;
    foreach($field_values_array[10] as $val) {
        $corredor[$f]=$val;
        $f++;
    }
    $g=0;
    foreach($field_values_array[11] as $val) {
        $concesionario[$g]=$val;
        $g++;
    }
    $datosVehiculo['placa']=$placa;
    $datosVehiculo['chasis']=$chasis;
    $datosVehiculo['motor']=$motor;
    $datosVehiculo['marca']=$marca;
    $datosVehiculo['modelo']=$modelo;
    $datosVehiculo['color']=$color;
    $datosVehiculo['anio']=$anio;
    $datosVehiculo['seguro']=$seguro;
    $datosVehiculo['numeroTramiteSeguro']=$numeroTramiteSeguro;
    $datosVehiculo['numeroPoliza']=$numeroPoliza;
    $datosVehiculo['corredor']=$corredor;
    $datosVehiculo['concesionario']=$concesionario;
    for ($i=0; $i < count($datosVehiculo['placa']) ; $i++) { 
        $datosVehiculo['placa_i']=$datosVehiculo['placa'][$i];
        $datosVehiculo['chasis_i']=$datosVehiculo['chasis'][$i];
        $datosVehiculo['motor_i']=$datosVehiculo['motor'][$i];
        $datosVehiculo['marca_i']=$datosVehiculo['marca'][$i];
        $datosVehiculo['modelo_i']=$datosVehiculo['modelo'][$i];
        $datosVehiculo['color_i']=$datosVehiculo['color'][$i];
        $datosVehiculo['anio_i']=$datosVehiculo['anio'][$i];
        $datosVehiculo['seguro_i']=$datosVehiculo['seguro'][$i];
        $datosVehiculo['numeroTramiteSeguro_i']=$datosVehiculo['numeroTramiteSeguro'][$i];
        $datosVehiculo['numeroPoliza_i']=$datosVehiculo['numeroPoliza'][$i];
        $datosVehiculo['corVehiredor_i']=$datosVehiculo['corredor'][$i];
        $datosVehiculo['concesionario_i']=$datosVehiculo['concesionario'][$i];
         $id = ($clienteObj) ? $clienteObj->idclientetemporal : "Fail" ;
        if($vehiculoObj->registrarVehiculosDesdeCliente($datosVehiculo,$id,$id)){
            irA("registroContactosAutorizados");
            if (isset($_SESSION['ruc'])) {
              Cotizar::cambiarEstadoRuc(24,$_SESSION['ruc']);              
            }else{
              Cotizar::cambiarEstadoDni(24,$_SESSION['dni']);              
            }
        }else{
            echo "the fail in datos d vehiculo registra o ejecutar la funcion";
        }
    }
}
if (isset($_POST['salir'])) {
unset($_SESSION['dni']);
unset($_SESSION['ruc']);
session_destroy();
irA("/");
}
?>
<link rel="stylesheet" href="assets/css2/style2.css">
<link href="assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<body class="gray-bg">
	<div class="wrapper wrapper-content animated bounceInUp">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="ibox-content">
<div class="row">
  <img src="assets/img/tracklogo.png" alt="" class="col-md-2 col-xs-4" width="200px">
  <div class="col-md-2 col-md-offset-8 col-xs-2 col-xs-offset-4"><form action="" method="POST"><button class="btn btn- col-lg-6" type="submit" name="salir">Salir <i class="fa fa-times-circle""></i></button></form></div>
</div>
					<!-- <h2 class="font-bold text-center">Regístrate aquí</h2> -->
            <?php $usuario=Usuario::obtenerUsuario($cotizarObj->idCreador);  ?>
          <div class="text-center"><small class="text-center">Si necesita ayuda puedes contactarte con <?php echo $usuario->usuarioNombre ?> <?php echo $usuario->usuarioApellidoPaterno ?> al (01) 630 7575 anexo <?php echo $usuario->anexo ?></small><br><small>Los campos que finalizan en * son obligatorios</small></div>
          <hr>
					<div class="row">
						<div class="col-lg-4 text-primary "><h2><?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? "DNI: " : "RUC: " ; ?> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? $clienteObj->dni : $clienteObj->ruc ; ?></h2></div>   
						<div class="col-lg-8 text-primary text-capitalize"><h2> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? "Nombres: " : "Razon social: " ; ?> <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? $clienteObj->nombre." ".$clienteObj->apellidosC : $clienteObj->razonSocial ; ?></h2></div> 
					</div>
					<div class="row">
						<div class="col-lg-12 ">
							<div class="ibox float-e-margins">
                    <form action="" method="post" id="formularioRegistroVehiculos">
                                    <hr>    
                                    <h2 class="font-bold text-center">Por favor ingresé datos de vehículo(s)</h2>
                                    <hr>
                                    <div class="row">
                                      <div class="col-lg-12 field_wrapper"> 
                                            <?php for ($i=0; $i < $estadoDniObj->cantidad ; $i++): ?>
                                                <hr>   
                                                <div class="row">
                                                 <div class="form-group col-md-2">
                                                  <label for="placa" class="sr-only">Placa *</label>
                                                  <input style="border: none;" type="text" placeholder="Placa *" id="placa" name="field_name[0][]" class="border-bottom form-control x2 text-uppercase" required >
                                              </div>
                                              <div class="form-group col-md-4">
                                                  <label for="chasis" class="sr-only">Chasis *</label>
                                                  <input style="border: none;" type="text" placeholder="Chasis *" id="chasis" name="field_name[1][]" class="border-bottom form-control text-uppercase" required >
                                              </div>
                                              <div class="form-group col-md-3">
                                                  <label for="motor" class="sr-only">Motor *</label>
                                                  <input style="border: none;" type="text" placeholder="Motor *" id="motor" name="field_name[2][]" class="border-bottom form-control text-uppercase" required >
                                              </div>
                                              <div class="form-group col-md-3">
                                                <label for="marca" class="sr-only">Marca *</label>
                                                <input style="border: none;" type="text" placeholder="Marca *" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off">
                                            </div>
                                        </div> 
                                        <div class="row">
                                           <div class="form-group col-md-3">
                                              <label for="modelo" class="sr-only">Modelo *</label>
                                              <input style="border: none;" type="text" placeholder="Modelo *" id="modelo" name="field_name[4][]" class="border-bottom form-control text-capitalize"  required >
                                          </div>
                                          <div class="form-group col-md-2">
                                              <label for="color" class="sr-only">Color *</label>
                                              <input style="border: none;" type="text" placeholder="Color *" id="color" name="field_name[5][]" class="border-bottom form-control x2 text-capitalize" required >
                                          </div>
                                          <div class="form-group col-md-2">
                                              <label for="anio" class="sr-only">Año *</label>
                                              <input style="border: none;" type="text" placeholder="Año fabricación *" id="anio" name="field_name[6][]" class="border-bottom form-control" required >
                                          </div>
                                          <div class="form-group col-md-3">
                                            <label for="seguro" class="sr-only">Seleccione Compañía de Seguros</label>
                                            <select  style="border: none;" class="border-bottom form-control text-capitalize" required name="field_name[7][]">
                                                <option class='text-capitalize' value='' disabled selected >Seleccione Compañía de Seguros</option>
                                                <option class='text-capitalize' value='No lo se, No recuerdo' >No lo se, No recuerdo</option>
                                                <option class='text-capitalize' value='No tengo' >No tengo</option>
                                                <option class='text-capitalize' value='La Positiva' >La Positiva</option>
                                                <option class='text-capitalize' value='Pacifico Seguros' >Pacifico Seguros</option>
                                                 <option class='text-capitalize' value='MHDI Seguros' >Rimac seguros</option> 
                                                <option class='text-capitalize' value='Mapfre Peru' >Mapfre Peru</option> 
                                                <option class='text-capitalize' value='MHDI Seguros' >HDI Seguros</option>
                                             </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                              <label for="Concesionario" class="sr-only">Concesionario</label>
                                              <input style="border: none;" type="text" placeholder="Concesionario" id="Concesionario" name="field_name[11][]" class="concesionario border-bottom form-control" required autocomplete="off">
                                          </div>
                                      </div>                                      
                                      <div class="row">
                                         <div class="form-group col-md-4">
                                          <label for="numeroTramiteSeguro" class="sr-only">Numero tramite de seguro</label>
                                          <input style="border: none;" type="text" placeholder="Numero tramite de seguro" id="numeroTramiteSeguro" name="field_name[8][]" class="numeroTramiteSeguro border-bottom form-control text-capitalize" >
                                      </div>
                                      <div class="form-group col-md-4">
                                          <label for="numeroPoliza" class="sr-only">N° de poliza</label>
                                          <input style="border: none;" type="text" placeholder="N° de poliza" id="numeroPoliza" name="field_name[9][]" class="border-bottom form-control text-capitalize" >
                                      </div>
                                      <div class="form-group col-md-4">
                                          <label for="corredor" class="sr-only">Corredor de seguros</label>
                                          <select  style="border: none;" class="corredor border-bottom form-control text-capitalize" required name="field_name[10][]">
                                           <option class='text-capitalize' value='' disabled selected >Seleccione Corredor de seguros</option>
                                           <option class='text-capitalize' value='No lo se, No recuerdo' >No lo se, No recuerdo</option>
                                           <?php Vehiculo::obtenerCorredores() ?>
                                       </select>
                                   </div>
                               </div>
                           <?php endfor; ?>
                           <div class="form-group col-md-3  col-md-offset-9">
                            <a class="add_button btn btn-success col-md-12 " title="Agregar contacto" name="agregarVehículo">Mas de <?= $estadoDniObj->cantidad ?> <i class="fa fa-plus-circle "></i></a> 
                        </div>
                    </div>  
                </div>  

                <div class="row">
                    <div class="col-lg-12 text-center"><button class="btn btn-success col-lg-4 text-center" type="submit" name="registrarVehiculos">Continuar con contactos<i class="fa fa-arrow-circle-o-down"></i></button></div>
                </div>
            </form> 


            <!-- datos del vehículo -->



            <hr>
            <h2 class="font-bold text-center">Contactos<i class="fa fa-arrow-circle-o-down"></i></h2>
            <hr>	

        </div>
    </div>





</div>
</div>
</div>
</div>
<hr/>
<div class="row">
 <div class="col-md-8 col-md-offset-2">
  <!-- Copyright Wilman Pabel -->
</div>
<div class="col-md-8 col-md-offset-2 text-right">
  <small>&copy; <?php echo date("Y") ?></small>
</div>
</div>
</div>
<script src="assets/js/jquery-2.1.1.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/plugins/footable/footable.all.min.js"></script>
<script src="assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="assets/js/agregarVehiculo.js"></script>
<!-- <script src="assets/js/plugins/validate/jquery.validate.min.js"></script> -->
 <?php include_once('includes/script.php') ?>
<script>
    $(document).ready(function(){
        // $('#formularioRegistroVehiculos').validate({});
        $('.footable').footable();
        $('.footable2').footable();
            // $(".corredor").select2({
            //     placeholder: "Seleccione un corredor de seguros",
            //     allowClear: true
            // });
            
         
            $('.concesionario').typeahead({
                source: [
                {"name": "No tengo"},
                {"name": "Alese perú"},
                {"name": "Gildemeister"},
                {"name": "Mitsui"},
                {"name": "Maquinaria Nacional S.A"},
                {"name": "Rosales Motors SA"},
                {"name": "Almacenes Santa Clara S.A."},
                {"name": "Auto Scorpio Motors S.A."},
                {"name": "EAFC MAQUISISTEMA"},
                {"name": "Braillard S.A."},
                {"name": "Braillard S.A. Arequipa"},
                {"name": "Fre Wagenmotors S.C.R.Ltda."},
                {"name": "Multimarkas"},
                {"name": "Scorpio Motors S.A."},
                {"name": "Alberto Torres S.A.C."},
                {"name": "Alejandrina S.A."},
                {"name": "Fys Rojas Motors S.A.C."},
                {"name": "Wong Hnos Automoviles S.A"},
                {"name": "Carrion Automotriz S.A."},
                {"name": "Eurosport"},
                {"name": "Resersur S.A.C."},
                {"name": "Ssangyong"},
                {"name": "Carrion Automotriz S.A."},
                {"name": "Pandero 4x4"},
                {"name": "Indumotora Del Perú"},
                {"name": "Autos Del Sur S.A.C."},
                {"name": "Ciro Arribasplata S.A."},
                {"name": "Amsa"},
                {"name": "Euroshop S.A"},
                {"name": "Flechelle"},
                {"name": "Hauswagen"},
                {"name": "Moliwagen"},
                {"name": "Peruwagen"},
                {"name": "Derco"},
                {"name": "Pandero VW"}
                ]
            });
            $('.marca').typeahead({
                source: [
                {"name": "Abarth"},
                {"name": "Alfa Romeo"},
                {"name": "Aro"},
                {"name": "Asia"},
                {"name": "Asia Motors"},
                {"name": "Aston Martin"},
                {"name": "Audi"},
                {"name": "Austin"},
                {"name": "Auverland"},
                {"name": "Bentley"},
                {"name": "Bertone"},
                {"name": "Bmw"},
                {"name": "Cadillac"},
                {"name": "Chevrolet"},
                {"name": "Chrysler"},
                {"name": "Citroen"},
                {"name": "Corvette"},
                {"name": "Dacia"},
                {"name": "Daewoo"},
                {"name": "Daf"},
                {"name": "Daihatsu"},
                {"name": "Daimler"},
                {"name": "Dodge"},
                {"name": "Ferrari"},
                {"name": "Fiat"},
                {"name": "Ford"},
                {"name": "Galloper"},
                {"name": "Gmc"},
                {"name": "Honda"},
                {"name": "Hummer"},
                {"name": "Hyundai"},
                {"name": "Infiniti"},
                {"name": "Isuzu"},
                {"name": "Iveco"},
                {"name": "Iveco pegaso"},
                {"name": "Jaguar"},
                {"name": "Jeep"},
                {"name": "Kia"},
                {"name": "Lada"},
                {"name": "Lamborghini"},
                {"name": "Lancia"},
                {"name": "Land-rover"},
                {"name": "Ldv"},
                {"name": "Lexus"},
                {"name": "Lotus"},
                {"name": "Mahindra"},
                {"name": "Maserati"},
                {"name": "Maybach"},
                {"name": "Mazda"},
                {"name": "Mercedes-benz"},
                {"name": "Mg"},
                {"name": "Mini"},
                {"name": "Mitsubishi"},
                {"name": "Morgan"},
                {"name": "Nissan"},
                {"name": "Opel"},
                {"name": "Peugeot"},
                {"name": "Pontiac"},
                {"name": "Porsche"},
                {"name": "Renault"},
                {"name": "Rolls-royce"},
                {"name": "Rover"},
                {"name": "Saab"},
                {"name": "Santana"},
                {"name": "Seat"},
                {"name": "Skoda"},
                {"name": "Ssangyong"},
                {"name": "Subaru"},
                {"name": "Suzuki"},
                {"name": "Talbot"},
                {"name": "Tata"},
                {"name": "Talbot"},
                {"name": "Toyota"},
                {"name": "Umm"},
                {"name": "Vaz"},
                {"name": "Volkswagen"},
                {"name": "Volvo"},
                {"name": "Wartburg"},                    
                {"name": "Austin"}
                ]
            });
        }); 
    </script>
</body>
</html>

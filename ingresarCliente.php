<?php include("public/publicHead.php") ?>
<?php 
unset($_SESSION['dni']);
unset($_SESSION['ruc']);
$mensaje=array();;
if(isset($_POST['ingresar'])&& $_POST['ingresar']=='verificar'){
    $data=trim(htmlspecialchars(htmlentities($_POST['dniORuc'])));    
    $cantidad=strlen($data);  
    if ($cantidad===8) {
        $cotizar=Cotizar::verificarPorDni($data);
        if($cotizar){           
            if (Token::verificar($_POST['token'])) {
                $estadoDniObj=Cotizar::obtenerEstadoDni($data);
                if ($estadoDniObj->estado==17) {
                    irA("registrarme");
                    $_SESSION['dni']=$data;
                }else
                if ($estadoDniObj->estado==23) {
                    irA("registroVehiculos");
                    $_SESSION['dni']=$data;

                }else 
                if ($estadoDniObj->estado==24) {
                    irA("registroContactosAutorizados");
                    $_SESSION['dni']=$data;

                }else
                if ($estadoDniObj->estado==25) {
                    irA("gracias");
                    $_SESSION['dni']=$data;

                }
           }
        }else if(!$cotizar){
            $mensaje[]="No existe Dni";
        }  
    }
    if($cantidad==11){ 
        $cotizar=Cotizar::verificarPorRuc($data);
        if($cotizar){          
            if (Token::verificar($_POST['token'])) {
                $estadoRucObj=Cotizar::obtenerEstadoRuc($data);
                if ($estadoRucObj->estado==17) {
                    irA("registrarme");
                    $_SESSION['ruc']=$data;
                }else
                if ($estadoRucObj->estado==23) {
                    irA("registroVehiculos");
                    $_SESSION['ruc']=$data;

                }else 
                if ($estadoRucObj->estado==24) {
                    irA("registroContactosAutorizados");
                    $_SESSION['ruc']=$data;

                }else
                if ($estadoRucObj->estado==25) {
                    irA("gracias");
                    $_SESSION['ruc']=$data;

                }
            }else{
            $mensaje[]="Verifique Ruc";            
        }
    }else{
        $mensaje[]="Verifique numero ingresado";            

    }
}
}
?>

<body class="gray-bg">

    <div class="passwordBox animated bounceIn">
        <div class="row">

            <div class="col-md-12">
                <div class="ibox-content">

                    <h2 class="font-bold text-center">Ingresar</h2>

                    <p class="text-center">
                        Por favor ingrese el número de su DNI o RUC.
                    </p>    
                    <div class="text-center">
                    <?php foreach ($mensaje as $var ): ?>
                        <p class="label label-warning "><?php echo $var ?></p>
                    <?php endforeach ?>
                 </div>
                 <div class="row">

                    <div class="col-lg-12">
                        <form  role="form" method="POST" id="ingresar">
                            <div class="form-group  border-bottom" >
                                <input type="number" style="border: none;" class="form-control  border-bottom" placeholder="DNI o RUC" required="" name="dniORuc" id="dniORuc" autofocus="">
                                <input type="hidden" name="token" value="<?php echo Token::generar() ?>">
                            </div>

                            <button type="submit" class="btn btn-primary block full-width m-b" name="ingresar" value="verificar">Ingresar</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            Copyright Wilman Pabel
        </div>
        <div class="col-md-6 text-right">
           <small>© 2016-<?php echo date('Y') ?>    </small>
       </div>
   </div>
</div>
<script src="assets/js/jquery-2.1.1.js"></script>
<script src="assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="assets/js/plugins/validate/jquery.validate.min.js"></script>
 <?php include_once('includes/script.php') ?>
<script>
 $(document).ready(function(){
    $("#ingresar").validate({
        rules: {
           dniORuc: {
              required:true,
              minlength:8,
              maxlength:11
          }
      },
      messages: {
        dniORuc: {
            required: "Es necesario ingresar este campo",
            minlength:"Faltan caracteres ",
            maxlength:"Exeden caracteres ",

        }

    }
});
});
</script>
</body>

</html>

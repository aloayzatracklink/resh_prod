<?php $nav="gestionBienvenidaPendientes" ?>
<?php require("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php //if ($obMenu->gestionPostVenta=="si"): ?>
<?php 

    if (isset($_POST['guardarCoorporativo'])) {
        if(Token::verificar($_POST['guardarCoorporativo'])) {
            $ttchk=$_POST['nfact'];
            for($i=0; $i < count($ttchk); $i++)
            {
                 $part= explode("_",$ttchk[$i]);
                $operacionObj->idlocation = $part[0];
                $operacionObj->idcustomer = $part[1];
                $operacionObj->idcompany = $part[2];
                $operacionObj->tipo = 'COORPORATIVO_TERMS';
                  $resul = $operacionObj->registerCoorporativo();
              }
            $echo[]="Se Actualizo con exito ";
   }else {
        $errores[] = "El formulario ya fue procesado, vericar.";
   }
}
?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">
<link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<link href="../assets/css/sweetalert2.css" rel="stylesheet">

    <style>
        .table-cabecera-horario{
            background:#075598!important;
            color:white !important;
            vertical-align: middle!important;
        }
        .table-cabecera-horario2{
            background:#164764 !important;
            color:white !important;
            vertical-align: middle!important;
        }
        .table-cabecera-horario3{
            background: #096452 !important;
            color:white !important;
        }
        .table-cabecera-horario4{
            background: #0e2e79 !important;
            color:white !important;
        }

        .table-scroll {
            position:relative;
            max-width:1600px;
            margin:auto;
            overflow:hidden;
            /*border:.5px solid #000;*/
        }

        .table-wrap {
            width:100%;
            overflow:auto;
        }

        .table-scroll table {
            width:100%;
            margin:auto;
            border-collapse:separate;
            border-spacing:0;
        }

        .table-scroll th, .table-scroll td {
            padding:5px 10px;
            border-bottom:1px solid;
            border-right:1px solid;
            border-color: #DDDDDD;;
            background:#fff;
            color:rgb(103, 106, 108) ;
            white-space:nowrap;
            vertical-align:top;
            font-size: 12px
        }

        .table-scroll thead, .table-scroll tfoot {
            background: #f9f9f9;
        }

        .bordertab{
            border-bottom: 1px #e1d0d0 solid !important;
            border-left: 1px #e1d0d0 solid !important;
        }
    </style>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <?php include("../includes/menuIzquierdo.php") ?>

            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                   <?php include("../includes/navegacionArriba.php") ?>
               </nav>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins col-lg-12">
                <div class="ibox-title">
                    <h5>Gestión de Bienvenidas Pendientes</h5>
                </div>
                <div class="row ibox float-e-margins ibox-content " style="margin: 0">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span style="font-size:30px; color:red; font-weight:bold;">
                                <?php
                                $repartePendientesWelcome = new RepartePendientesWelcomeClass();
                                $repartePendientesWelcome->fk_usuario_gestiona=$session->idusuario;
                                $cantPendientesAhora = $repartePendientesWelcome->cantPendientesAhora();
                                if($cantPendientesAhora):
                                    echo $cantPendientesAhora[0]->cantidad;
                                else:
                                    echo "0";
                                endif;
                                ?>
                                </span> <span style="font-weight:bold;">Pendientes</span>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class=" table   main-table" >
                        <thead>
                            <tr>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">#</th>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Id Cliente</th>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">DNI</th>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">RUC</th>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Nombre</th>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Campaña</th>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Teléfono</th>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Teléfono 2</th>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Teléfono 3</th>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Email</th>
                            </tr>
                        </thead>    
                        <tbody id="dataSeguiWelcome">
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
        <?php include("../includes/footer.php") ?>

    </div>
</div>

    
    


<!-- Mainly scripts -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>


    <script src="../assets/js/plugins/select2/select2.full.min.js"></script>
    <script src="../assets/bootbox/bootbox.all.js"></script>
<!-- Steps -->
<script src="../assets/js/plugins/staps/jquery.steps.min.js"></script>
<!-- Jquery Validate -->
<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>

    <script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
    <script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>

 <?php include_once('../includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
    <script>

        

      function loadData() {
           $.ajax({
              url: 'ajax/seguimientoWelcomeDataPendiente',
              type: 'POST',
              data: {},
              success: function (data) {
                $('#dataSeguiWelcome').html("");
                $('#dataSeguiWelcome').html(data);
              }
           });
      }
      $(document).ready(function(){
        loadData();


        $('.footable').footable();
        $('.footable2').footable();
    });

</script>
</body>

</html>
    <script>
        $(window).load(function() {
            $(".se-pre-con").fadeOut("slow");;
        });
    </script>

<?php //else: ?> 
    <?php 
        //$session->salir();
        //echo "NO NO NO NO ; presiona F5"
    ?> 
<?php //endif ?>
<script src="../assets/tools/loadingModal/jquery.loadingModal.min.js" ></script>
<script src="../assets/js/sweetalert2.min.js" ></script>
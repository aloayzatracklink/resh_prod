<?php 

if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
	class RepartePendientesWelcomeClass extends Recursos
	{

		public $idreparte_pendientes;
		public $vperiodo;
		public $vperiodo_ini;
		public $vperiodo_fin;
		public $fk_idarea_gestiona;
		public $fk_usuario_gestiona;
		public $icantidad;
		public $dtcrea;
		public $usercrea;
		public $dtelimina;
		public $userelimina;
		public $dtactualiza;
		public $useractualiza;
		public $vreclamo_cliente;

		function __construct()
		{
			global $session;
			$this->usercrea = $session->idusuario;
			$this->useractualiza = $session->idusuario;
			$this->vreclamo_cliente = "";
		}

		public function registrar($limit_ini, $limit_end){
			global $db;
			$sql="INSERT INTO sb_reparte_pendientes_welcome(
				vperiodo, 
				vperiodo_ini,
				vperiodo_fin,
				fk_idarea_gestiona,
				fk_usuario_gestiona,
				icantidad,
				dtcrea,
				usercrea) VALUES ('";
			$sql.=$db->escapeString($this->vperiodo)."' , ";
			$sql.="STR_TO_DATE('".$db->escapeString($this->vperiodo_ini)."','%Y-%m-%d'), ";
			$sql.="STR_TO_DATE('".$db->escapeString($this->vperiodo_fin)."','%Y-%m-%d'), '";
			$sql.=$db->escapeString($this->fk_idarea_gestiona)."' , '";
			$sql.=$db->escapeString($this->fk_usuario_gestiona)."' , '";
			$sql.=$db->escapeString($this->icantidad)."' , ";
			$sql.=" NOW() , '";
			$sql.=$db->escapeString($this->usercrea)."' )";
			if ($db->query($sql)){
				$this->idreparte_pendientes=$db->ultimoIdInsertado();
				$sql = $this->RegistrarListaClientesPendientes($limit_ini, $limit_end);
				return $sql;
			}else{
				return false;
			}

		}

		public function RegistrarListaClientesPendientes($limit_ini, $limit_end){
			global $db;
			$sql = <<<EOT
					INSERT INTO trackdb.sb_reparte_pendientes_welcome_lista
						(fk_idreparte_pendientes, fk_idcliente, fk_idterm)
					SELECT
						'$this->idreparte_pendientes' AS  fk_idreparte_pendientes,
						dat.cliente_id AS fk_idcliente,
						dat.term_id AS fk_idterm
					FROM
					(
					SELECT
					CASE WHEN v.plan like '%Pro%' THEN 'Otros'
							WHEN v.campana like '%metros%' THEN 'Otros'
							ELSE v.campana END campana,
					CASE WHEN v.campana LIKE '%metros%' THEN 'plan kilometros'
							WHEN IFNULL(t.financiadopor,'') LIKE '%CLAN%' THEN 'EL Clan'
							WHEN IFNULL(t.financiadopor,'') LIKE '%MOVILIZA%' THEN 'Moviliza'
							ELSE v.plan END plan,
						t.id as term_id,
						v.chasis,
						c.idcliente as cliente_id,
						IF(c.tipoCliente='Natural', CONCAT_WS(' ', c.apellidosC, c.nombre), c.razonSocial)  AS cliente_nombre,
						c.correo AS cliente_email,
						c.telefono AS cliente_telefono,
						c.telefono1 AS cliente_telefono1,
						c.telefono2 AS cliente_telefono2
					FROM trackdb.Terms t
					INNER JOIN trackdb.operaciones o on o.idoperacion = t.idOperation
					INNER JOIN trackdb.vehiculos v on v.idvehiculo = o.idvehiculo
					INNER JOIN trackdb.clientes c on c.idcliente = v.idcliente
					WHERE
						1=1
						AND IFNULL(statusAccept,0)=0
						AND DATE_FORMAT(t.dateSend,'%Y-%m-%d')>='{$db->escapeString($this->vperiodo_ini)}' 
						AND DATE_FORMAT(t.dateSend, '%Y-%m-%d') <='{$db->escapeString($this->vperiodo_fin)}'
						and CONCAT(t.idlocation ,t.idcustomer)
						not in (select CONCAT(nt.idlocation ,nt.idcustomer) from no_atenders_general_automatico nt)
						LIMIT {$limit_ini}, {$limit_end}
					)dat  order by dat.campana,plan asc;
					EOT;

					if ($db->query($sql)){
						return $sql;
					}else{
						return false;
					}
		}

		public function cantPendientesAhora(){
			$sql = <<<EOT
					SELECT
						COUNT(*) AS cantidad
					FROM
					trackdb.sb_reparte_pendientes_welcome w
					LEFT JOIN trackdb.sb_reparte_pendientes_welcome_lista wl ON w.idreparte_pendientes = wl.fk_idreparte_pendientes
					LEFT JOIN trackdb.Terms t ON t.id = wl.fk_idterm
					WHERE
						w.iactivo = 1
						AND IFNULL(t.statusAccept,0)=0
						AND w.fk_usuario_gestiona = {$this->fk_usuario_gestiona}
					EOT;
			$result=self::sentenciaV2($sql);
			return !empty($result)? $result:[];
		}

		public function listarPendientes(){
			$sql = <<<EOT
					SELECT
					dat.pendiente_periodo,
					dat.campana,
					dat.plan,
					dat.chasis,
					dat.cliente_id,
					dat.cliente_dni,
					dat.cliente_ruc,
					dat.cliente_nombre,
					dat.cliente_email,
					dat.cliente_telefono,
					dat.cliente_telefono1,
					dat.cliente_telefono2
					FROM
					(
					SELECT
					w.vperiodo    AS pendiente_periodo,
					CASE WHEN v.plan like '%Pro%' THEN 'Otros'
							WHEN v.campana like '%metros%' THEN 'Otros'
							ELSE v.campana END campana,
					CASE WHEN v.campana LIKE '%metros%' THEN 'plan kilometros'
							WHEN IFNULL(t.financiadopor,'') LIKE '%CLAN%' THEN 'EL Clan'
							WHEN IFNULL(t.financiadopor,'') LIKE '%MOVILIZA%' THEN 'Moviliza'
							ELSE v.plan END plan,
						v.chasis,
						c.idcliente as cliente_id,
						c.dni AS cliente_dni,
						c.ruc AS cliente_ruc,
						IF(c.tipoCliente='Natural', CONCAT_WS(' ', c.apellidosC, c.nombre), c.razonSocial)  AS cliente_nombre,
						c.correo AS cliente_email,
						c.telefono AS cliente_telefono,
						c.telefono1 AS cliente_telefono1,
						c.telefono2 AS cliente_telefono2
					FROM
						trackdb.sb_reparte_pendientes_welcome w
						LEFT JOIN trackdb.sb_reparte_pendientes_welcome_lista wl ON w.idreparte_pendientes = wl.fk_idreparte_pendientes
						LEFT JOIN trackdb.Terms t ON t.id = wl.fk_idterm
					INNER JOIN trackdb.operaciones o on o.idoperacion = t.idOperation
					INNER JOIN trackdb.vehiculos v on v.idvehiculo = o.idvehiculo
					INNER JOIN trackdb.clientes c on c.idcliente = v.idcliente
					WHERE
						1=1
						AND w.iactivo=1
						AND w.fk_usuario_gestiona={$this->usercrea}
						AND IFNULL(statusAccept,0)=0
						and CONCAT(t.idlocation ,t.idcustomer)
								not in (select CONCAT(nt.idlocation ,nt.idcustomer) from no_atenders_general_automatico nt)
					ORDER BY w.vperiodo DESC
					)dat  order by dat.campana,plan asc
					EOT;
			$result=self::sentenciaV2($sql);
			return !empty($result)? $result:[];
		}

		public function siExisteEnFechasReparticion(){
			global $db;
			$sql = <<<EOT
					SELECT COUNT(*) AS cantidad FROM trackdb.sb_reparte_pendientes_welcome w
					WHERE
					DATE_FORMAT(w.vperiodo_ini,'%Y-%m-%d')>='{$db->escapeString($this->vperiodo_ini)}'
					AND DATE_FORMAT(w.vperiodo_fin, '%Y-%m-%d') <='{$db->escapeString($this->vperiodo_fin)}'
					EOT;
			$result=self::sentenciaV2($sql);
			if(!empty($result)){
				return ($result[0]->cantidad>0)? true : false;
			}else{
				return false;
			}
		}

		


	}

}

 ?>

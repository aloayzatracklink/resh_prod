<?php 
/**
* 
*/
class Renovaciones extends Recursos
{
	public $idrenovacion;
	public $chasis;
	public $idusuario;
	public $fechaCreada;
	public $observacion;
	public $fechaModificada;
	public $ce;
	public $cv;
	public $nc;
	public $ok;
	public $bj;
	public $ot;
	public $estado;	
	public $monto;
	public $msjPersonalizado;
	public $nombreCliente;
	public $placa;
	public $vence;
	public $cantidad;
	public $estadoDes;
	public $idusuDes;
	public $fechaActualizacion;
	public $obsDesinstalacion;
	public $idoperacion;
	public $ndoc;
	public $marca;
	public $modelo;
	public $plan;
	public $tiempo;
	public $idejecutivo;
	public $preferente;
	public $status_preferente;
	public $paymentLink;
	public $paymentId;
	public $statusPay;
	public $norderPay;
	public $mail;
	public $nombre_cliente;
	public $apellido_cliente;

	function __construct()
	{
	}
	public function registrarChasis($chasis){
		global $session;
		global $db;		
		$fecha=date("YmdHis");
		$sql="INSERT INTO renovaciones (chasis,idusuario,fechaCreada,idejecutivo) VALUES ('".$db->escapeString($chasis)."','";
		$sql.=$session->idusuario."','";
		$sql.=$fecha."')";
		if ($db->query($sql)) {
			$this->idrenovacion=$db->ultimoIdInsertado();
			$oData=Onix::ObtenerDatosDesdeChasis($chasis);
			if ($oData) {
				$nombre=utf8_encode(addslashes($oData->nombre))." ".utf8_encode(addslashes($oData->apellidos));
				$vencimientoDatos=Onix::obtenerFechaDeVencimiento($oData->idcustomer,$oData->idvehicle,$oData->idlocation);
				$sql2="UPDATE renovaciones SET nombreCliente='$nombre',placa='$oData->placa',modelo='$oData->modelo',marca='$oData->marca',ndoc='$oData->dni', vence='$vencimientoDatos->hasta',tiempo='$vencimientoDatos->tiempo' WHERE chasis='$chasis'";
				$db->query($sql2);
			}
			return true;
		}else{
			return false;
		}
	}


	public function registrarChasis2($chasis,$idejecutivo){
		global $session;
		global $db;
		$fecha=date("YmdHis");
		$sql="INSERT INTO renovaciones (chasis,idusuario,idejecutivo,preferente,mail,status_preferente,fechaCreada) VALUES ('".$db->escapeString($chasis)."','";
		$sql.=$session->idusuario."','";
		$sql.=$db->escapeString($idejecutivo)."','";
		$sql.=$db->escapeString($this->preferente)."','";
		$sql.=$db->escapeString($this->mail)."',";
		$sql.="0,'";
		$sql.=$fecha."')";
		if ($db->query($sql)) {
			$this->idrenovacion=$db->ultimoIdInsertado();
			$oData=Onix::ObtenerDatosDesdeChasis($chasis);
			if ($oData) {
				$nombre=utf8_encode(addslashes($oData->nombre))." ".utf8_encode(addslashes($oData->apellidos));
				$nnom=utf8_encode(addslashes($oData->nombre));
				$nape=utf8_encode(addslashes($oData->apellidos));
				$this->nombre_cliente = $nnom;
				$this->apellido_cliente = $nape;
				$vencimientoDatos=Onix::obtenerFechaDeVencimiento($oData->idcustomer,$oData->idvehicle,$oData->idlocation);
				$sql2="UPDATE renovaciones SET nombreCliente='$nombre',nombre_cliente='$nnom',apellido_cliente='$nape', placa='$oData->placa',modelo='$oData->modelo',marca='$oData->marca',ndoc='$oData->dni', vence='$vencimientoDatos->hasta',tiempo='$vencimientoDatos->tiempo' WHERE chasis='$chasis'";
				$db->query($sql2);
			}
			return true;
		}else{
			return false;
		}
	}
	public function verificarExistencia($chasis){
		$verificarExistencia=self::sentencia("SELECT chasis FROM renovaciones WHERE estado=0 AND chasis='$chasis'");
		return empty($verificarExistencia)?true:false;


	}
	public function registrarSeguimiento($idrenovacion){
		global $session;
		global $db;
		$fecha=date("YmdHis");
		$sql="UPDATE renovaciones SET observacion='".$db->escapeString($this->observacion)."', ";
		$sql.="fechaModificada='".$fecha."',";
		$sql.="ce ='".$this->ce."',";
		$sql.=" ok ='".$this->ok."',";
		$sql.=" bj ='".$this->bj."',";
		$sql.=" mail ='".$this->mail."',";
		$sql.=" monto ='".$db->escapeString($this->monto)."', ";
		$sql.=" estado ='".$db->escapeString($this->estado)."', ";
		$sql.=" msjPersonalizado ='".$db->escapeString($this->msjPersonalizado)."' WHERE idrenovacion=".$db->escapeString($idrenovacion);
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}

	}


	public function obtenerRegistrosSubidos($posisionDePagina,$registrosPorPagina){
		global $session;
		$obtenerRegistroSubidoQ=self::sentencia("SELECT * FROM renovaciones WHERE idusuario = ".$session->idusuario." AND  estado=0 ORDER BY vence ASC LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}

	public function obtenerRegistrosSubidosOtherEstado($posisionDePagina,$registrosPorPagina){
		global $session;
		$obtenerRegistroSubidoQ=self::sentencia("SELECT * FROM renovaciones WHERE idusuario = ".$session->idusuario." AND  estado in (56,57) and idoperacion=0 ORDER BY fechaModificada DESC LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}

	public function obtenerRegistrosSubidosCantida(){
		global $session;
		$obtenerRegistroSubidoQ=self::sentencia("SELECT COUNT(*) as cantidad FROM renovaciones WHERE idusuario = ".$session->idusuario." AND estado=0 LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}

	public function obtenerRegistrosSubidosCantidaOtherEstado(){
		global $session;
		$obtenerRegistroSubidoQ=self::sentencia("SELECT COUNT(*) as cantidad FROM renovaciones WHERE idusuario = ".$session->idusuario." AND estado in (56,57) and idoperacion=0 LIMIT 1 ");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}

	public function obtenerRegistrosSubidosCantidaRenovadoHoy(){
		global $session;
		$hoy=date("Ymd");
		$hoy.="%";
		$obtenerRegistroSubidoQ=self::sentencia("SELECT COUNT(*) as cantidad FROM renovaciones WHERE idusuario = ".$session->idusuario." AND estado=57 AND fechaModificada LIKE '$hoy' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}

	public function obtenerRegistrosSubidosCantidaNoRenovadoHoy(){
		global $session;
		$hoy=date("Ymd");
		$hoy.="%";
		$obtenerRegistroSubidoQ=self::sentencia("SELECT COUNT(*) as cantidad FROM renovaciones WHERE idusuario = ".$session->idusuario." AND estado=56 AND fechaModificada LIKE '$hoy' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}

	public function buscarParaFiltro($datos){
		global $session;
		global $db;
		$datos=$db->escapeString($datos);
		$obtenerRegistroSubidoQ=self::sentencia("SELECT *  FROM renovaciones WHERE idusuario = ".$session->idusuario." AND estado=0 AND (nombreCliente LIKE '$datos' OR placa LIKE '$datos' OR chasis LIKE '$datos') LIMIT 10");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}


	public function buscarParaFiltroSeguimiento($datos,$desde,$hasta,$posisionDePagina,$registrosPorPagina){
		global $session;
		global $db;

		$fil = "";
		if($datos){
			$datos=$db->escapeString($datos);
			$fil = "  AND (trim(nombreCliente) LIKE '%$datos%' OR placa LIKE '$datos' OR chasis LIKE  '$datos') ";
		}
		if($desde){
			$fil .= "  AND fechaCreada  BETWEEN '$desde' AND '$hasta' ";
		}
		$obtenerRegistroSubidoQ=self::sentencia("SELECT *  FROM renovaciones WHERE 1=1 $fil  order by idrenovacion desc LIMIT $posisionDePagina, $registrosPorPagina   ");

		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}



	public function buscarParaFiltroSeguimientoTT($datos,$desde,$hasta){
		global $session;
		global $db;

		$fil = "";
		if($datos){
			$datos=$db->escapeString($datos);
			$fil = "  AND (trim(nombreCliente) LIKE '%$datos%' OR placa LIKE '$datos') ";
		}
		if($desde){
			$fil .= "  AND fechaCreada  BETWEEN '$desde' AND '$hasta' ";
		}
		$sql = " select count(*) as cantidad from (  SELECT *  FROM renovaciones WHERE 1=1 $fil )dat";
		$obtenerRegistroSubidoQ=self::sentencia($sql);
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}

	public function buscarParaFiltroOtherEstado($datos){
		global $session;
		global $db;
		$datos=$db->escapeString($datos);
		$obtenerRegistroSubidoQ=self::sentencia("SELECT *  FROM renovaciones WHERE idusuario = ".$session->idusuario." AND estado in (56,57) and idoperacion=0 AND (nombreCliente LIKE '$datos' OR placa LIKE '$datos') LIMIT 10");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}

	public static function obtenerDatosDeRenovacionPorId($id){
		$obtenerDatosDeRenovacionPorIdQ=self::sentencia("SELECT * FROM renovaciones WHERE idrenovacion='$id'");
		return !empty($obtenerDatosDeRenovacionPorIdQ)? array_shift($obtenerDatosDeRenovacionPorIdQ) : false ;
	}
	public function obtenerRenovadosYNoRenovados(){
		global $session;
		$obtenerRenovadosYNoRenovadosQ=self::sentencia("SELECT * FROM renovaciones WHERE (ok='si' OR bj='si') AND idusuario='$session->idusuario' ORDER BY fechaModificada DESC LIMIT 40 ");
		return !empty($obtenerRenovadosYNoRenovadosQ) ? $obtenerRenovadosYNoRenovadosQ : false;

	}
	public function actualizarTODOSLOSRegistrosSubidos(){
		global $db;
		$obtenerRegistroSubidoQ=self::sentencia("SELECT * FROM renovaciones ");
		foreach ($obtenerRegistroSubidoQ as $var) {

			$oData=Onix::ObtenerDatosDesdeChasis($var->chasis);
			$vencimientoDatos=Onix::obtenerFechaDeVencimiento($oData->idcustomer,$oData->idvehicle,$oData->idlocation);	
			$nombre=utf8_encode($oData->nombre)." ".utf8_encode($oData->apellidos);
			$nombre=limpiar($nombre);
				$sql="UPDATE renovaciones SET nombreCliente='$nombre', placa='$oData->placa', vence='$vencimientoDatos->hasta' WHERE chasis='$var->chasis'";
				$db->query($sql);
		}
		return true;
	}


	public function obtenerNoRenovadosCantida(){
		global $session;
		$obtenerRegistroSubidoQ=self::sentencia("SELECT COUNT(*) as cantidad FROM renovaciones WHERE bj='si' AND (estado ='56' OR estado ='14' OR estado='7') AND fechaModificada>'20180228230000' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false;
	}

	// public static function obtenerNoRenovados(){
	// 	$oteerN=self::sentencia("SELECT * FROM renovaciones WHERE bj='si' AND estado ='56'");
	// 	return !empty($oteerN)?$oteerN:false;
	// }
	public function obtenerNoRenovadosSubidos($posisionDePagina,$registrosPorPagina){
		global $session;
		$obtenerRegistroSubidoQ=self::sentencia("SELECT * FROM renovaciones WHERE bj='si' AND (estado ='56' OR estado ='14' OR estado='7') AND fechaModificada>'20180228230000' ORDER BY fechaModificada ASC LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}
	public function marcarComoDesinstalado(){
		global $db;
		global $session;
		$this->fechaActualizacion=date("YmdHis");
		$sql="UPDATE renovaciones SET estado='7', idusuDes='$session->idusuario', obsDesinstalacion='$this->obsDesinstalacion', fechaActualizacion='$this->fechaActualizacion' WHERE idrenovacion='$this->idrenovacion'";
		return ($db->query($sql))?true:false;
	}
	public function marcarComoProgramado(){
		global $db;
		global $session;
		$this->fechaActualizacion=date("YmdHis");
		$sql="UPDATE renovaciones SET estado='14', idusuDes='$session->idusuario', obsDesinstalacion='$this->obsDesinstalacion', fechaActualizacion='$this->fechaActualizacion', idoperacion='$this->idoperacion' WHERE idrenovacion='$this->idrenovacion'";
		return ($db->query($sql))?true:false;

	}
	public function obtenerBusquedaResultadoDes($datos){
		$dtp=self::sentencia("SELECT * FROM renovaciones  WHERE (nombreCliente LIKE '$datos' OR placa LIKE '$datos' OR chasis LIKE '$datos' OR obsDesinstalacion LIKE '$datos') LIMIT 10");
		return !empty($dtp)? $dtp:false;
	}

	public function verifyPAY($idreq){
	    $nsql = " SELECT pr.id,pr.date_confirm as dateconfirm,pr.first_name as firstname,pr.last_name as lastname,  bd.purchase_operation_number as norder,bd.mail ,bd.billing_type as typedoc,pr2.answer_message as message  FROM payment_request pr INNER JOIN billing_detail bd on bd.id = pr.billing_detail_id  INNER JOIN payment_response pr2 on pr2.reserved1 =pr.id and pr2.error_code ='00' and pr2.authorization_result ='00' " .
                " WHERE pr.id ='$idreq'";
		$dtp=self::sentencia($nsql);
		return !empty($dtp)? true:false;
	}

	/**
	 * @return mixed
	 */
	public function getRequestPay($param)
	{
		$billingDetail =array(
			"address" => "",
			"billingType" => "",
			"documentNumber" => "",
			"mail" =>  $param['mail'],
			"documentType" => "",
			"name" =>  $param['name'],
			"phone" =>  $param['phone']
		);
		$productPaymentRequest =array(
			"description" => "",
			"name" => "Servicio de GPS",
			"price" => $param['price']
		);
		$paymentRequest =array(
			"documentNumber" => "",
			"firstName" => $param['firstName'],
			"lastName" => $param['lastName'],
			"mail" =>  $param['mail'],
			"phone" =>  $param['phone'],
			"address" =>  $param['address'],
			"defaultMoney" => "DOLAR",
			"exchangeRate" => 3.8,
			"productPaymentRequests" => [$productPaymentRequest],
			"billingDetail" => $billingDetail,
			"project" => $param['project'],
			"projectId" =>  $param['projectId'],
			"created" => str_replace('@tracklink.pe', '', $param['created'])
		);
		$signature=array(
			"mail" => $param['created'],
		);


		$bodyDatta = array(
			"paymentRequest" =>$paymentRequest,
			"signature" => $signature,
			"idRequest" => $param['idRequest'],
		);
		return $bodyDatta;

	}
	public function updateParamPay($url,$idRequetPay,$idreno){
		global $db;
		$sql="UPDATE renovaciones SET paymentLink='".$url."', paymentId='$idRequetPay' WHERE idrenovacion='$idreno'";
		//echo $sql;
		return ($db->query($sql))?true:false;
	}


}

 ?>

<?php $nav="seguimientoBienvenida" ?>
<?php require("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->gestionPostVenta=="si"): ?>
<?php 
$vehiculoObj=new Vehiculo();
$operacionObj=new Operacion();

$form             = new InputFormClass();
$tablas           = new DatosTablasClass();
$areas            = $tablas->datosAreas();
$area_responsable = "";

    if (isset($_POST['guardarCoorporativo'])) {
        if(Token::verificar($_POST['guardarCoorporativo'])) {
            $ttchk=$_POST['nfact'];
            for($i=0; $i < count($ttchk); $i++)
            {
                 $part= explode("_",$ttchk[$i]);
                $operacionObj->idlocation = $part[0];
                $operacionObj->idcustomer = $part[1];
                $operacionObj->idcompany = $part[2];
                $operacionObj->tipo = 'COORPORATIVO_TERMS';
                  $resul = $operacionObj->registerCoorporativo();
              }
            $echo[]="Se Actualizo con exito ";
   }else {
        $errores[] = "El formulario ya fue procesado, vericar.";
   }
}
?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">
<link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<link href="../assets/css/sweetalert2.css" rel="stylesheet">

    <style>
        .table-cabecera-horario{
            background:#075598!important;
            color:white !important;
            vertical-align: middle!important;
        }
        .table-cabecera-horario2{
            background:#164764 !important;
            color:white !important;
            vertical-align: middle!important;
        }
        .table-cabecera-horario3{
            background: #096452 !important;
            color:white !important;
        }
        .table-cabecera-horario4{
            background: #0e2e79 !important;
            color:white !important;
        }

        .table-scroll {
            position:relative;
            max-width:1600px;
            margin:auto;
            overflow:hidden;
            /*border:.5px solid #000;*/
        }

        .table-wrap {
            width:100%;
            overflow:auto;
        }

        .table-scroll table {
            width:100%;
            margin:auto;
            border-collapse:separate;
            border-spacing:0;
        }

        .table-scroll th, .table-scroll td {
            padding:5px 10px;
            border-bottom:1px solid;
            border-right:1px solid;
            border-color: #DDDDDD;;
            background:#fff;
            color:rgb(103, 106, 108) ;
            white-space:nowrap;
            vertical-align:top;
            font-size: 12px
        }

        .table-scroll thead, .table-scroll tfoot {
            background: #f9f9f9;
        }

        .bordertab{
            border-bottom: 1px #e1d0d0 solid !important;
            border-left: 1px #e1d0d0 solid !important;
        }
    </style>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <?php include("../includes/menuIzquierdo.php") ?>

            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                   <?php include("../includes/navegacionArriba.php") ?>
               </nav>
           </div>
           <div class="wrapper wrapper-content">
            <div class="ibox float-e-margins col-lg-12">
                <div class="ibox-title">
                    <h5>Bienvenidas</h5>
                </div>
                <div class="row ibox float-e-margins ibox-content " style="margin: 0">
                    <div class="col-md-12">
                        <div class="row">
                            <form action="PHPExcel/seguimientoBienvenidasExcel" method="POST">
                                <div class="col-lg-3">
                                    <input style="border: none;" type="hidden" name="nest" value="'4'" >
                                    <label for="convenio" class="">Seleccione rango fecha Envío Bienvenida *</label>
                                    <input style="border: none;"  class="cd-select form-control border-bottom" required  type="text" placeholder="Seleccione Rango de fecha" class="form-control" value="" name="nfecha" id="rango" autocomplete="off">
                                </div>
                                <div class="col-md-1" id="resultado1">
                                    <button class="btn btn-success" name="Descargar" id="resultado1">Descargar</button>
                                </div>
                            </form>
                            <div class="col-md-2">
                                <!-- <button class="btn btn-primary" name="nbuscar" id="nbuscar">Buscar</button> -->
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#addcoorporativo" id="regCoorp"  data-tipo="save">sacar coorporativo <i class="fa fa-search"></i></button>
                            </div>
                            <div class="col-md-2">
                                <form action="PHPExcel/coorporativosRegistradosExcel" method="POST">
                                    <div class="col-md-1" id="resultado1">
                                        <button class="btn btn-success" name="DescargarC" id="resultadoC">Descargar Coorporativos</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="text-right">
                        <button type="button" id="btnRepartirPendientes" class="btn btn-success" data-toggle="modal" data-target="#modalRepartirPendientes" title="Repartir pendientes">Repartir Pendientes</button>
                    </div>
                    <div class="table-responsive">
                        <table class=" table   main-table" >
                        <thead>
                            <tr>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Campaña</th>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">descripción</th>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">enviados</th>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Aceptados</th>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Pendientes</th>
                                <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">%</th>
                            </tr>
                        </thead>    
                        <tbody id="dataSeguiWelcome">
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
        <?php include("../includes/footer.php") ?>

    </div>
</div>

    <div class="modal inmodal" id="addcoorporativo" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" style="overflow-y: scroll;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceIn">
                <div class="modal-header" style="padding-top: 0;padding-bottom:0">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title">Sacar Coorporativo</h4>
                </div>
                <form  role="form" method="post"  enctype="multipart/form-data" name="regEnvioCoorporativo" id="regEnvioCoorporativo">
                    <div class="modal-body" style="background: #fff;padding:0 15px"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                        <button type="submit" name="guardarCoorporativo" id="guardarCoorporativo" onclick="return validarSelect()" class="btn btn-primary btn-modal" value="<?php echo Token::generar() ?>" >Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="modalRepartirPendientes" tabindex="1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceIn">
                <div class="modal-header" style="padding-top: 5px;padding-bottom: 5px">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title text-left" style="font-size:20px;">Repartir <span id="spanCantPendientes" style="color:red;"></span> pendiente(s)</h4>
                </div>
                <div class="modal-body" style="background: #fff;padding-top: 0">
                    <div class="row" style="padding:5px;">
                        <div class="col-md-10">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <?php
                                    $params = [
                                    'label'       => 'Area responsable',
                                    'id'          => 'area_responsable',
                                    'name'        => 'area_responsable',
                                    'data'        => $areas,
                                    'keyValue'    => 'idrol',
                                    'keyLabel'    => 'area',
                                    'value'       => $area_responsable,
                                    'required'    => True
                                    ];
                                    echo $form->getInputSelect($params);
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <?php
                                    $params = [
                                    'label'       => 'Persona a cargo',
                                    'id'          => 'persona_cargo',
                                    'name'        => 'persona_cargo',
                                    'data'        =>  $personasACargo,
                                    'keyValue'    => 'idusuario',
                                    'keyLabel'    => 'usuario_nombre',
                                    'value'       => $persona_cargo,
                                    'required'    => True
                                    ];
                                    echo $form->getInputSelect($params);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 text-center">
                            <button type="button" onclick="agregarUsuarioRepartir()" style="margin-top:20px;" class="btn btn-success">Agregar</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table" id="tableRepartir">
                                <thead>
                                    <tr>
                                        <th>Encargado</th>
                                        <th class="text-right">Cantidad</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary" onclick="saveRepartirPendientes()">Guardar</button>
                </div>
            </div>
        </div>
    </div>


<!-- Mainly scripts -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>


    <script src="../assets/js/plugins/select2/select2.full.min.js"></script>
    <script src="../assets/bootbox/bootbox.all.js"></script>
<!-- Steps -->
<script src="../assets/js/plugins/staps/jquery.steps.min.js"></script>
<!-- Jquery Validate -->
<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>

    <script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
    <script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>

 <?php include_once('../includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
    <script>

        $('#addcoorporativo').on('show.bs.modal', function (e) {
            $(".modal-title").html("Sacar Coordinador");
            $(".btn-modal").html('Guardar');
            $(".modal-body").html("");

            var tipo = $(e.relatedTarget).data('tipo');

            $.ajax({
                type : 'post',
                url : 'modal/addCoorporativo',
                data :  "tipoOpera="+tipo,
                success : function(data){
                    $('.modal-body').html(data);
                }
            });
        });



        function validarSelect() {
            arr =  $('input[id^="nfact_"]:checked').map(function(){
                return this.value;
            }).get();

            var str = arr.join(',');
            if(str===''){
                str='0';
            }
            if(str==0) {
                bootbox.alert({
                    title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                    message: "<label>Seleccione un Coorporativo para realizar el registro *</label>",
                    size: 'small',
                    callback: function () {
                        /*  l.ladda( 'stop' );*/
                    }
                });
                return false;
            }
        } 
        
        $('#rango').daterangepicker({
            format: 'DD-MM-YYYY',
            // startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            // minDate: '01/01/2012',
            // maxDate: '12/31/2015',
            // dateLimit: { days: 60 },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: false,
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
                'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                'Anterior Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: '/',
            locale: {
                applyLabel: 'Seleccionar',
                cancelLabel: 'Cancelar',
                fromLabel: 'Desde',
                toLabel: 'Hasta',
                customRangeLabel: 'Seleccionar Rango',
                daysOfWeek: ['Do', 'Lun', 'Mar', 'Mi', 'Ju', 'Vi','Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                firstDay: 1
            }
        }, function(start, end, label) {
            console.log("label", label);
            loadData();
            localStorage.setItem("dataCalendar", JSON.stringify({
                "start":start.format('YYYY-MM-DD'),
                "end":end.format('YYYY-MM-DD'),
                "periodo":start.format('YYYYMM')
            }));

            console.log(localStorage.getItem("dataCalendar"));


            if(label=="Anterior Mes"){
                //$("#btnRepartirPendientes").show();
            }else{
                //$("#btnRepartirPendientes").hide();
            }
            $('#rango').focus();
        });

      function loadData() {
          var nfecha = $("#rango").val()
           $.ajax({
              url: 'ajax/seguimientoWelcomeData',
              type: 'POST',
              data: {fecha: nfecha},
              success: function (data) {
                $('#dataSeguiWelcome').html("");
                $('#dataSeguiWelcome').html(data);
              }
           });
      }
      $(document).ready(function(){
        loadData();

        // $("#nbuscar").click(function (){
        //     loadData();
        // })

        $('.footable').footable();
        $('.footable2').footable();
    });

    /**
     * Distribuir pendientes
     */
    $('#modalRepartirPendientes').on('show.bs.modal', function (e) {

        var cantPendientes = $("#cantPendientes").val();
        $("#spanCantPendientes").text(cantPendientes);
        

        if($.trim($('#rango').val())==""){
            SnackBar({message: "Seleccione intervalo de fechas", status: "danger"});
            $('#rango').focus();
            return false;
        }

        console.log("cantPendientes ", cantPendientes);
        if(parseInt(cantPendientes)==0 || cantPendientes==undefined){
            SnackBar({message: "No hay pendientes por distribuir, seleccione intervalo de fechas diferente.", status: "danger"});
            $('#rango').focus();
            return false;
        }

    

        
        //var rowid = $(e.relatedTarget).data('id') || null;
        // $.ajax({
        //     type : 'post',
        //     url : 'modal/BuscarClientesModal', //Here you will fetch records 
        //     data :  'rowid='+ rowid, //Pass $id
        //     success : function(data){
        //             $('#modalClientes .modal-body').html(data);//Show fetched data from database
        //         }
        //     });
    });

    function limpiarModalRepartirPendientes(){
        $("#spanCantPendientes").text(0);
        $("#tableRepartir tbody").html('');
        $('#area_responsable').prop('selectedIndex',0);
        cargaAreaPersonaACargo(999999999999999999, "");
        usuariosRepartir = [];
    }

    $('#modalRepartirPendientes').on('hide.bs.modal', function (e) {
        limpiarModalRepartirPendientes();
    });

    function cargaAreaPersonaACargo(idarea, idusuario){
        var idarea = idarea || null;
        var idusuario = idusuario || null;
        $.ajax({
            type : 'POST',
            url : '../com/ajax/DatosTablasAjax', //Here you will fetch records 
            data :  {
                "tabla":"area_usuarios",
                "idarea":idarea,
                "idusuario":idusuario
            },
            success : function(data){
                data = JSON.parse(data);
                //console.log(data);
                var textFirst = $("#persona_cargo option").eq(0).html();
                var strOptions = `<option value="">${textFirst}</option>`;
                data.forEach(function(row){
                    strOptions+=`<option value="${row.idusuario}">${row.usuario_nombre}</option>`;
                });
                $("#persona_cargo").html(strOptions);
                // console.log(strOptions);
            }
        });
    }

    $(document).ready(function(){
        $("#area_responsable").change(function(){
            cargaAreaPersonaACargo($(this).val(), null)
        });
    });

    var usuariosRepartir = [];
    function agregarUsuarioRepartir(){
        var cantPendientes = $("#cantPendientes").val();

        var area               = $("#area_responsable").val();
        var area_text          = $("#area_responsable option:selected").text();
        var persona_cargo      = $("#persona_cargo").val();
        var persona_cargo_text = $("#persona_cargo  option:selected").text();
        

        if($.trim(area)=="" || $.trim(persona_cargo)==""){
            SnackBar({message: "Seleccione usuario", status: "danger"});
            return;
        }

        // Validar si la persona a cargo ya fue agregado a la lista
        let personaExiste = usuariosRepartir.filter(p=>p.persona_cargo==persona_cargo);
        if(personaExiste.length>0){
            SnackBar({message: "Usuario ya agregado, seleccione otro diferente", status: "danger"});
            return;
        }

        // Agregar persona
        usuariosRepartir.push({
            "area"              : area,
            "area_text"         : area_text,
            "persona_cargo"     : persona_cargo,
            "persona_cargo_text": persona_cargo_text,
            "cantidad"          : 0
        });

        var tocaCadaUno = Math.floor(parseInt(cantPendientes)/usuariosRepartir.length);//Division entera
        var tocaCadaUnoResto = parseInt(cantPendientes)%usuariosRepartir.length;
        //console.log("tocaCadaUnoResto", tocaCadaUnoResto);

        // Repartir el entero
        usuariosRepartir.forEach(item => {
            item.cantidad = tocaCadaUno;
        });

        // Repartir el resto
        if(tocaCadaUnoResto>0){
            
            var BreakException = {};
            try {
                usuariosRepartir.forEach(function(item) {
                    item.cantidad += 1;
                    tocaCadaUnoResto -= 1;
                    if (tocaCadaUnoResto === 0) throw BreakException;
                });
            } catch (e) {
                if (e !== BreakException) throw e;
            }
        }

        //usuariosRepartir[usuariosRepartir.length-1].cantidad = tocaCadaUno;

        //console.log("usuariosRepartir", usuariosRepartir);
        $('#area_responsable').prop('selectedIndex',0);
        cargaAreaPersonaACargo(999999999999999999, "");
        cargarBodyTableRepartir();
    }

    function cargarBodyTableRepartir(){
        var tabla = '';
 
        usuariosRepartir.forEach(item=>{
            tabla+=`
                <tr>
                    <td>
                    <strong>${item.persona_cargo_text}</strong><br>
                    <span style="font-size:10px;color:green;">
                    ${item.area_text}
                    </span>
                    </td>
                    <td class="text-right" style="font-size:20px">
                    <strong>${item.cantidad}</strong>
                    </td>
                </tr>
            `;
        });
        $("#tableRepartir tbody").html(tabla);
    }

    function saveRepartirPendientes(){
        console.log(localStorage.getItem("dataCalendar"));
        var dataCalendar = JSON.parse(localStorage.getItem("dataCalendar"));

        $('body').loadingModal({text: 'Procesando registro...'});
        $.ajax({
            type: "POST",
            url: "ajax/saveRepartirPendientesAjax",
            data: {
                periodo         : dataCalendar.periodo,
                periodoIni      : dataCalendar.start,
                periodoFin      : dataCalendar.end,
                usuariosRepartir: usuariosRepartir
            },
            dataType: "json",
            encode: true,
        }).done(function (data) {
            console.log(data);
            if(data.error==false){
                limpiarModalRepartirPendientes();
                $('#modalRepartirPendientes').modal('hide');
                // SnackBar({message: data.msg,status: "success", timeout: false, fixed: true});
                swal({
                    title: "Ok!",
                    text: data.msg,//"Se guardó correctamente el registro.",
                    type: "success",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                function(){
                    window.location.reload();
                });
            }else{
                SnackBar({message: data.msg,status: "danger", timeout: false, fixed: true});
            }
            $('body').loadingModal('hide');
            // if(data.error==false){
            //     swal({
            //         title: "Ok!",
            //         text: data.msg,//"Se guardó correctamente el registro.",
            //         type: "success",
            //         //showCancelButton: true,
            //         //confirmButtonClass: "btn-danger",
            //         confirmButtonText: "Ok",
            //         closeOnConfirm: false
            //     },
            //     function(){
            //         //swal("Deleted!", "Your imaginary file has been deleted.", "success");
            //         window.location.reload();
            //     });

            //     if(data.respSendingNotif){
            //         if(data.respSendingNotif.send==1){
            //             SnackBar({message: data.respSendingNotif.msg,status: "success", timeout: false, fixed: true});
            //         }else{
            //             SnackBar({message: data.respSendingNotif.msg,status: "danger", timeout: false, fixed: true});
            //         }

            //         if(data.respSendingNotifClient.send==1){
            //             SnackBar({message: data.respSendingNotifClient.msg,status: "success", timeout: false, fixed: true});
            //         }else{
            //             SnackBar({message: data.respSendingNotifClient.msg,status: "danger", timeout: false, fixed: true});
            //         }

                    
            //     }

            // }else{
            //     alert("Hubo un error");
            // }
        });
    }
</script>
</body>

</html>
    <script>
        $(window).load(function() {
            $(".se-pre-con").fadeOut("slow");;
        });
    </script>

<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>
<script src="../assets/tools/loadingModal/jquery.loadingModal.min.js" ></script>
<script src="../assets/js/sweetalert2.min.js" ></script>
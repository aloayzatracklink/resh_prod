<?php $nav="renovacionesE" ?>
<?php require("../includes/head.php") ?>
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<?php 
    // if ($session->idusuario<>2) {
    //     irA("gestionRenovaciones");
    // }
 ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
} ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->gestionRenovaciones=="si"): ?>

<?php 
$errores=array();
$echo=array();
$renoObj=new Renovaciones();
    //Registramos las alertas aquí ///
    if (isset($_POST['subir'])) {
    
    if (Token::verificar($_POST['token'])) {        
        if (!empty($_POST['chasis'])) {
            $chasis=trim(htmlspecialchars($_POST['chasis']));
            if($renoObj->registrarChasis($chasis))
                $echo[]="Chasis registrado";
            else
                $errores[]="Error en el registro de chasis";
        }else{
            $errores[]="El campo chasis esta vacio";
        }
    

        }else{
        $errores[]=" El formulario ya fue procesado, the token :v";
        }
    }

    ?>
    <?php 
    require_once '../mailphp/PHPMailer/PHPMailerAutoload.php';
    $bateriaObj=new BateriaTrack();
    $contador=0;
    if (isset( $_POST['gestionarBateria'])) {
        $nombres=strtoupper($_POST['nombreCliente']);
        $correo=strtolower(trim($_POST['correoCliente']));
        $correo = preg_replace("/.(?R)*\[BS\]/","",$correo);
        $marca=strtoupper($_POST['marca']);
        $modelo=strtoupper( $_POST['modelo']);
        $color=strtoupper( $_POST['color']);
        $vence=strtoupper( $_POST['vence']);
        $anio=strtoupper( $_POST['anio']);
        $monto=strtoupper( $_POST['monto']);
        $mensajePersonalizado=$_POST['msjPersonalizado'];
        $renoObj->monto=$monto;
        $renoObj->msjPersonalizado=$mensajePersonalizado;
        $renoObj->observacion=$_POST['Observaciones'];
        $plan=strtoupper( $_POST['plan']);
        $monto=" $".$monto;
        $placaVehiculo=strtoupper($_POST['placaVehiculo']);
        $chasis=trim(strtoupper($_POST['chasis']));
        $chasisO=trim($_POST['chasisO']);
        $sistema="";
        $tiempoS=strtoupper($_POST['vencidoHace']);
        $mensajeCentral="Comprometidos en seguir brindándole la mejor calidad en los servicios, le recordamos que la fecha de vencimiento de su servicio y el precio de la renovación es el siguiente:";
        if (isset($_POST['plantilla'])){
            if ($_POST['plantilla']=="Track") {
            $mensajeCentral="Comprometidos en seguir brindándole la mejor calidad en los servicios, le recordamos que la fecha de vencimiento de su servicio y el precio de la renovación es el siguiente:";
            }else{
            $mensajeCentral="Comprometidos en seguir brindándole la mejor calidad en el servicio de auxilio especializado en caso de choque y recuperación vehicular por robo, le recordamos que la fecha de vencimiento de su servicio y el precio de la renovación es el siguiente:";
                
            }
        }  
     $correo= explode(",", $correo);
        // echo var_dump($correo);

        ////OBTENEMOS LOS DATOS DEL USUARIO
     $usuario=Usuario::obtenerUsuario($session->idusuario);
     $correoEnviador=$usuario->usuario;
     $nombreEnviador=$usuario->usuarioNombre." ".$usuario->usuarioApellidoPaterno." ".$usuario->usuarioApellidoMaterno;
     $nombreE=$usuario->usuarioNombre;
     $rolUsuario=Usuario::obtenerRoldeUsuario($usuario->idrol);
     $cargo=$rolUsuario->cargo;
     $anexo=$usuario->anexo;
     $celular=$usuario->celular;
        ///////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////ENVIAR CORREO AL CLIENTE ///////////////////////////////////////////////////////////
     if (isset($_POST['cee'])) {
        if ($_POST['correoCliente']<>"") {
   /////////////////

    $thehtml="<!DOCTYPE html>
<html lang='es'>
<head>
    <meta charset='UTF-8'>
    <title>Renovacion de servicio</title>
</head>
<body>
<div style='font-family: &quot;trebuchet ms&quot;, sans-serif; font-size: 12pt; color: rgb(0, 0, 102);'>
    <div>
    <img  src='cid:head'><br></div>
        <div><div style='font-family: arial, helvetica, sans-serif; font-size: 12pt; color: rgb(0, 0, 0);'><div style='font-family: &quot;trebuchet ms&quot;, sans-serif; font-size: 12pt; color: rgb(0, 0, 102);'><div style='font-family: &quot;trebuchet ms&quot;, sans-serif; font-size: 12pt; color: rgb(0, 0, 102);'><div style='font-family: &quot;trebuchet ms&quot;, sans-serif; font-size: 12pt; color: rgb(0, 0, 102);'>
            <div><p style='margin: 0px; text-align: justify; line-height: normal;'><span style='color: rgb(0, 51, 102); font-size: 10pt;'><br></span></p><p style='margin: 0px; text-align: justify; line-height: normal;'><span style='color: rgb(0, 51, 102); font-size: 11pt;'>Estimado $nombres,
            </span></span></p><p style='margin: 0px; text-align: justify; line-height: normal;'><span style='color: rgb(0, 51, 102); font-size: 11pt;'>&nbsp;</span></p><p style='margin: 0px; text-align: justify; line-height: normal;'><span style='color: rgb(0, 51, 102); font-size: 11pt;'>Reciba un cordial saludo de parte de la familia Tracklink Octo. Nos dirigimos a usted para agradecerle por la confianza que tiene depositada en nosotros para brindarle protección y seguridad en las vías peruanas a usted y a los que más ama.</span></p><p style='margin: 0px; text-align: justify; line-height: normal;'><span style='color: rgb(0, 51, 102); font-size: 11pt;'></span><br></p><p style='margin: 0px; text-align: justify; line-height: normal;'><span style='color: rgb(0, 51, 102); font-size: 11pt;'>$mensajeCentral</span></p><p style='margin: 0px; text-align: justify; line-height: normal;'><span style='color: rgb(0, 51, 102); font-size: 11pt;'></span><br></p><p style='margin: 0px; text-align: justify; line-height: normal;'><span style='color: rgb(0, 51, 102); font-size: 11pt;'><span style='text-decoration: underline;'><strong><span style='text-decoration: underline;'>PRECIO DE RENOVACIÓN:</span></strong></span></span><span style='font-family: symbol;'><span style='font-family: &quot;times new roman&quot;; font-style: normal; font-weight: normal; line-height: normal; font-stretch: normal; font-feature-settings: normal; font-kerning: auto; font-variant-ligatures: normal; font-variant-caps: normal;'></span></span></p><p style='margin: 0px; text-align: justify; line-height: normal;'><span style='color: rgb(0, 51, 102); font-size: 11pt;'><span style='text-decoration: underline;'><strong><span style='text-decoration: underline;'></span></strong></span></span><br></p><p style='margin: 0px; text-align: justify; line-height: normal;'><span style='color: rgb(0, 51, 102); font-size: 11pt;'>$monto Dólares americanos incl. IGV&nbsp; por unidad.</span></p><br><p style='color: rgb(0, 0, 102); font-family: &quot;trebuchet ms&quot;, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; margin: 0px; line-height: normal; background: none 0% 0% repeat scroll white;'><span style='color: rgb(0, 51, 102); font-size: 13pt;'><strong>$mensajePersonalizado</strong><br></span></p><p style='margin: 0px; text-align: justify; line-height: normal;'><span style='font-size: 11pt; color: rgb(0, 51, 102);'><span style='text-decoration: underline;'><strong><span style='text-decoration: underline;'></span></strong></span></span><br></p></div><div>
<div >

<table  style='border-collapse: collapse; width: 744px; table-layout: fixed; display: ' border='1px' cellpadding='3px'><tbody><tr><td style='text-align: center;'><span style='color: rgb(0, 51, 102); font-size: 9pt;'><strong>&nbsp;PLACA</strong></span></td><td style='text-align: center;'><span style='color: rgb(0, 51, 102); font-size: 9pt;'><strong>AÑO</strong></span></td><td style='text-align: center;'><span style='color: rgb(0, 51, 102); font-size: 9pt;'><strong>MARCA</strong></span></td><td style='text-align: center;'><span style='color: rgb(0, 51, 102); font-size: 9pt;'><strong>MODELO </strong></span></td><td style='text-align: center;'><span style='color: rgb(0, 51, 102); font-size: 9pt;'><strong>COLOR</strong></span></td><td style='text-align: center;'><span style='color: rgb(0, 51, 102); font-size: 9pt;'><strong>FECHA DE VENCIMIENTO</strong></span></td></tr><tr><td style='text-align: center;'><span style='color: rgb(0, 51, 102); font-size: 10pt;'>$placaVehiculo</span></td><td style='text-align: center;'><span style='color: rgb(0, 51, 102); font-size: 10pt;'>$anio</span></td><td style='text-align: center;'><span style='color: rgb(0, 51, 102); font-size: 10pt;'>$marca</span></td><td style='text-align: center;'><span style='color: rgb(0, 51, 102); font-size: 10pt;'>$modelo</span></td><td style='text-align: center;'><span style='color: rgb(0, 51, 102); font-size: 10pt;'>$color</span></td><td style='text-align: center;'><span style='color: rgb(0, 51, 102); font-size: 10pt;'>$vence</span></td></tr></tbody></table>
</div></div><div><span style='font-size: 11pt; color: rgb(0, 51, 102);'><strong></strong></span><br></div>
<div>
<span style='font-size: 11pt; color: rgb(0, 51, 102);'>Como parte de la renovación del servicio, su vehículo deberá pasar por un<em>&nbsp;'chequeo en linea gratuito',</em> que incluye la revisión y reprogramación de su dispositivo. Puede programar su cita* en cualquier momento, solicitándola como <em>Chequeo en Linea</em>, &nbsp;la cual se realizará entre las 5pm y 10pm de <span class='Object' id='OBJ_PREFIX_DWT1086_com_zimbra_date' style='text-decoration: none; cursor: pointer;'><span class='Object' role='link' id='OBJ_PREFIX_DWT122_com_zimbra_date'>lunes</span></span> a <span class='Object' id='OBJ_PREFIX_DWT1087_com_zimbra_date' style='text-decoration: none; cursor: pointer;'><span class='Object' role='link' id='OBJ_PREFIX_DWT123_com_zimbra_date'>viernes</span></span>.&nbsp;Posteriormente, &nbsp;nuestra Área de Monitoreo** se comunicará con usted vía telefónica y realizará las pruebas*** respectivas.</span></div><div><p style='color: rgb(0, 0, 102); font-family: &quot;trebuchet ms&quot;, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; margin: 0px; text-align: justify; line-height: normal; background: none 0% 0% repeat scroll white;'><span style='color: rgb(0, 51, 102); font-size: 11pt;'>&nbsp;</span></p><p style='color: rgb(0, 0, 102); font-family: &quot;trebuchet ms&quot;, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; margin: 0px; line-height: normal; background: none 0% 0% repeat scroll white;'><span style='color: rgb(0, 51, 102); font-size: 11pt;'>*Le recomendamos &nbsp;programar su cita con un día de anticipación.</span></p><p style='color: rgb(0, 0, 102); font-family: &quot;trebuchet ms&quot;, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; margin: 0px; line-height: normal; background: none 0% 0% repeat scroll white;'><span style='color: rgb(0, 51, 102); font-size: 11pt;'>**Los chequeos en línea tienen una duración de 10 a 15 minutos.</span></p><p style='color: rgb(0, 0, 102); font-family: &quot;trebuchet ms&quot;, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; margin: 0px; line-height: normal; background: none 0% 0% repeat scroll white;'><span style='color: rgb(0, 51, 102); font-size: 11pt;'>***Usted debe estar en su vehículo y no encontrarse en un sótano al momento del chequeo en línea.</span></p><p style='color: rgb(0, 0, 102); font-family: &quot;trebuchet ms&quot;, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; margin: 0px; line-height: normal; background: none 0% 0% repeat scroll white;'><span style='color: rgb(0, 51, 102); font-size: 13pt;'><br></span></p>
            <p style='color: rgb(0, 51, 102); font-family: &quot;trebuchet ms&quot;, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; margin: 0px; line-height: normal; background: none 0% 0% repeat scroll white;'><b><span style='font-family: calibri; background-color: transparent; font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: underline; vertical-align: baseline; white-space: pre-wrap;'>NÚMEROS DE CUENTA:</span></b><br></p></div><div><ul><li style='color: rgb(31, 73, 125); line-height: normal; vertical-align: baseline;'><span style='font-size: 11pt; color: rgb(0, 51, 102);'><strong>Transferencia: </strong>Si realiza el pago vía transferencia o depósito bancario, es importante que nos envió por este medio la confirmación del abono.</span></li></ul><div><table style='border-collapse: collapse;' width='888' border='1'><tbody><tr style='height: 0px;'><td colspan='4' style='border-width: 1px 1px 0px; border-style: solid; border-color: rgb(0, 0, 0); vertical-align: top; background-color: rgb(83, 141, 213); padding: 7px 5px;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><b><span style='font-size: 14.6667px; font-family: calibri; color: rgb(255, 255, 255); background-color: rgb(83, 141, 213); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Titular de la Cuenta: MOTORLINK S.A.C.</span></b></p></td></tr><tr style='height: 0px;'><td colspan='4' style='border-width: 0px 1px 1px; border-style: solid; border-color: rgb(0, 0, 0); vertical-align: top; background-color: rgb(83, 141, 213); padding: 7px 5px;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><b><span style='font-size: 14.6667px; font-family: calibri; color: rgb(255, 255, 255); background-color: rgb(83, 141, 213); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>RUC: 20525107915</span></b></p></td></tr><tr style='height: 0px;'>
            <td colspan='2' style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0);'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='color: rgb(0, 51, 102); font-size: 10pt;'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>N° Cuenta Corriente (Depósito)</span></span></p></td><td colspan='2' style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0);'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='color: rgb(0, 51, 102); font-size: 10pt;'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>N° Cuenta Interbancario (vía internet, CCI)</span></span></p></td></tr><tr style='height: 0px;'><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco Financiero Dólares</span></span></p></td><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1524_com_zimbra_phone'><a style='color: rgb(0, 51, 102);' target='_blank'>000-000447979159</a></span></span></span><br></p></td><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; vertical-align: baseline; white-space: pre-wrap;'>Banco Financiero Dólares</span></span></p></td><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1525_com_zimbra_phone'><a style='color: rgb(0, 51, 102);'  target='_blank'>035-000-000447979159-94</a></span></span></span><br></p></td></tr>

            <tr style='height: 0px;'><td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco Financiero Soles</span></span></p></td>
            <td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1526_com_zimbra_phone'><a style='color: rgb(0, 51, 102);'  target='_blank'>000-000447978934</a></span></span></span><br></p></td>
            <td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco Financiero Soles</span></span></p></td>
            <td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id=''><a style='color: rgb(0, 51, 102);'  target='_blank'>035-000-000447978934-91</a></span></span></span></p></td></tr>

            <tr style='height: 0px;'><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco de Crédito Dólares</span></span></p></td><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1528_com_zimbra_phone'><a style='color: rgb(0, 51, 102);'  target='_blank'>194-1967786-1-34</a></span></span></span><br></p></td><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco de Crédito Dólares</span></span></p></td><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1529_com_zimbra_phone'><a style='color: rgb(0, 51, 102);'  target='_blank'>002-194-001967786134-96</a></span></span></span></p></td></tr>


            <tr style='height: 0px;'>
            <td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco de Crédito Soles</span></span></p></td>

            <td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1530_com_zimbra_phone'><a style='color: rgb(0, 51, 102);'  target='_blank'>193-1853946-0-25</a></span></span></span><br></p></td>

            <td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco de Crédito Soles</span></span></p></td><td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1531_com_zimbra_phone'><a style='color: rgb(0, 51, 102);'  target='_blank'>002-193-001853946025-15</a></span></span></span></p></td></tr>


            <tr style='height: 0px;'><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco Continental BBVA Dólares</span></span></p></td><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1532_com_zimbra_phone'><a style='color: rgb(0, 51, 102);'  target='_blank'>0011-0426-0100005979-48</a></span></span></span></p></td><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco Continental BBVA Dólares</span></span></p></td><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1533_com_zimbra_phone'><a style='color: rgb(0, 51, 102);'  target='_blank'>011-426-000100005979-48</a></span></span></span></p></td></tr>



            <tr style='height: 29px;'><td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco Continental BBVA Soles</span></span></p></td>
            <td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1534_com_zimbra_phone'><a style='color: rgb(0, 51, 102);'  target='_blank'>0011-0426-0100005960</a></span>-48</span></span></p></td><td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco Continental BBVA Soles</span></span></p></td><td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1535_com_zimbra_phone'><a style='color: rgb(0, 51, 102);'  target='_blank'>011-426-000100005960</a></span>-48</span></span></p></td></tr>


<tr style='height: 0px;'><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco BanBif Dólares</span></span></p></td><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1532_com_zimbra_phone'><a style='color: rgb(0, 51, 102);'  target='_blank'>7000364190</a></span></span></span></p></td><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco BanBif Dólares</span></span></p></td><td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1533_com_zimbra_phone'><a style='color: rgb(0, 51, 102);'  target='_blank'>038-103-207000364190</a></span>-80</span></span></p></td></tr>



            <tr style='height: 29px;'><td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco BanBif Soles</span></span></p></td>
            <td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1534_com_zimbra_phone'><a style='color: rgb(0, 51, 102);'  target='_blank'>7000364203</a></span></span></span></p></td><td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco BanBif Soles</span></span></p></td><td style='vertical-align: top; background-color: rgb(255, 255, 255); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(255, 255, 255); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1535_com_zimbra_phone'><a style='color: rgb(0, 51, 102);'  target='_blank'>038-103-107000364203</a></span>-82</span></span></p></td></tr>


<tr style='height: 0px;'>
<td style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'>Banco de la Nacion (Cuenta detracciones)</span></span></p></td>
<td colspan='3' style='vertical-align: top; background-color: rgb(220, 230, 241); padding: 7px 5px; border: 1px solid rgb(0, 0, 0); text-align: left;'><p style='line-height: 1.38; text-align: center; margin: 0px;'><span style='font-size: 10pt; color: rgb(0, 51, 102);'><span style='font-family: calibri; background-color: rgb(220, 230, 241); font-variant-ligatures: normal; font-variant-caps: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;'><span class='Object' id='OBJ_PREFIX_DWT1532_com_zimbra_phone'><a style='color: rgb(0, 51, 102);'  target='_blank'>00-046-021886</a></span></span></span></p></td>


</tr>



            </tbody>
            </table></div></div><div><p style='margin: 0px;'><span style='font-size: 10pt; font-family: &quot;trebuchet ms&quot;, sans-serif; color: rgb(23, 54, 93);'>&nbsp;</span></p><div><br><div>
<div style='font-family: arial, helvetica, sans-serif; font-size: 12pt; color: rgb(0, 0, 0); font-style: normal; font-weight: normal; letter-spacing: normal; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255);'><span style='color: rgb(0, 55, 102); font-size: 11pt; font-style: normal; font-weight: normal; letter-spacing: normal; line-height: 21px; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; float: none; background-color: rgb(255, 255, 255); display: inline; font-family: &quot;trebuchet ms&quot;, sans-serif;'>
Si opta por realizar sus pagos vía transferencias o depósitos bancarios, requerimos nos envié por este medio la confirmación del abono, adjuntando el Voucher escaneado del depósito o la confirmación de transferencia.
De lo contrario, también puede realizar estos pagos en nuestras oficina de <strong>Paseo de la republica 4925 - Surquillo</strong> de lunes a viernes de 09:00 AM - 14:00 PM y 15:00 PM - 18:00 PM y los sábados de 9:00am a 12:00pm. <br><br>
<strong>Si por algún motivo las unidades no fueran renovadas dentro de las fechas indicadas y sufrieran algún siniestro, Tracklink no se responsabiliza por los daños o pérdidas ocasionados.</strong>
<br><br>
Cualquier consulta no dude en hacérnosla llegar que gustosos lo atenderemos.
</span></div>
<br>    
        <div><div><div><p style='margin: 0px;'><span style='font-size: 10pt; font-family: &quot;trebuchet ms&quot;, sans-serif; color: rgb(23, 54, 93);'>Saludos cordiales,</span></p><p style='margin: 0px;'><span style='font-size: 10pt; font-family: &quot;trebuchet ms&quot;, sans-serif; color: rgb(23, 54, 93);'></span><br></p><p style='margin: 0px;'><span style='font-family: &quot;trebuchet ms&quot;, sans-serif; color: rgb(23, 54, 93);'>&nbsp;<img src='cid:tracklink'></span></p><div><p style='margin: 0px;'><span style='color: rgb(0, 51, 102);'><strong>$nombreEnviador<br></strong></span></p><p style='margin: 0px;'><span style='font-size: 11.5pt; color: rgb(0, 51, 102);'>$cargo<br></span></p><p style='margin: 0px;'><span style='color: rgb(0, 51, 102);'><strong><span style='font-size: 11.5pt;'>Tracklink -&nbsp;</span></strong><strong><span style='font-size: 11.5pt;'>Octo Telematics<br></span></strong></span></p><p style='margin: 0px;'><span style='color: rgb(0, 51, 102);'><span style='font-size: 11.5pt;'>Calle Ricardo Angulo 762 - Urb Corpac - San Isidro, Lima.<span style='color: rgb(23, 54, 93);'><span class='Object' id='OBJ_PREFIX_DWT1132_com_zimbra_phone' style='color: rgb(0, 90, 149); text-decoration: none; cursor: pointer;'><a href='callto:+51%20950.596.228' target='_blank' style='color: rgb(0, 90, 149); text-decoration: none; cursor: pointer;'> </a></span></span></span><span style='font-size: 11.5pt;'><br></span><span style='font-size: 11.5pt;'>Oficina principal:&nbsp;<span class='' style='color: rgb(23, 54, 93);'><span class='Object' id='OBJ_PREFIX_DWT1131_com_zimbra_phone' style='color: rgb(0, 90, 149); text-decoration: none; cursor: pointer;'><a href='callto:+511%20630%207575' target='_blank' style='color: rgb(0, 90, 149); text-decoration: none; cursor: pointer;'>+511 630 7575</a></span></span> Anexo: $anexo.&nbsp;</span>Celular: <span style='color: rgb(23, 54, 93);'><span class='Object' id='' style='color: rgb(0, 90, 149); text-decoration: none; cursor: pointer;'><a href='callto:+51%20950.596.228' target='_blank' style='color: rgb(0, 90, 149); text-decoration: none; cursor: pointer;'> </a>$celular</span></span><br></span></p><p style='margin: 0px;'><span style='color: rgb(0, 51, 102);'>Oficina para pagos y taller : Av. Paseo de la republica 4925- Surquillo, Lima</span></p><div class='' style='color: rgb(0, 0, 0); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; line-height: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; font-family: helvetica; font-size: 12px; background-color: rgb(255, 255, 255);'><span class='' style='font-size: 11pt; color: rgb(0, 150, 255); font-family: calibri, sans-serif;'><span class='Object' id='OBJ_PREFIX_DWT1133_com_zimbra_url' style='color: rgb(0, 90, 149); text-decoration: none; cursor: pointer;'><span class='Object' id='OBJ_PREFIX_DWT1169_com_zimbra_url' style='color: rgb(0, 90, 149); text-decoration: none; cursor: pointer;'><span class='Object' id='OBJ_PREFIX_DWT435_com_zimbra_url' style='color: rgb(0, 90, 149); text-decoration: none; cursor: pointer;'><span class='Object' id=''><span class='Object' id=''><span class='Object' id=''><span class='Object' role='link' id=''><a target='_blank' style='color: rgb(0, 90, 149); text-decoration: none; cursor: pointer;' href='http://www.tracklink.pe'>www.tracklink.pe</a></span></span></span></span></span></span></span> / <span class='Object' role='link' id='OBJ_PREFIX_DWT111_com_zimbra_url'><a target='_blank' href='http://www.octotelematics.com' style='color: rgb(0, 90, 149); text-decoration: none; cursor: pointer;' >www.octotelematics.com</a></span></span></div><p style='margin: 0px;'><span style='color: rgb(0, 90, 149);'><span><span style='color: rgb(0, 0, 0); font-family: arial, helvetica, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); float: none; display: inline !important;'><span class='Object' id='OBJ_PREFIX_DWT3310_com_zimbra_url'><span class='Object' id='OBJ_PREFIX_DWT3318_com_zimbra_url'><span class='Object' role='link' id='OBJ_PREFIX_DWT112_com_zimbra_url'><a href='https://www.facebook.com/tracklinkperu/' target='_blank'><img  src='cid:facebook' width='33' height='39' ></a></span><span class='Object' role='link' id='OBJ_PREFIX_DWT113_com_zimbra_url'><a href='https://www.youtube.com/channel/UCylRCGZ4LBaNKk-XMJliURw' target='_blank'><img  src='cid:youtube' width='33' height='39'></a></span><span class='Object' role='link' id='OBJ_PREFIX_DWT114_com_zimbra_url'><a href='https://twitter.com/TracklinkPE' target='_blank'><img  src='cid:twitter' width='33' height='39'></a></span></span></span></span></span></span><br></p></div></div></div></div>
        </body>
</html>";


    // El correo para el cliente aqui /// <---->
    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SetLanguage( 'es', 'mailphp/PHPMailer/language/' );
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';
    $mail->Host = gethostbyname('mail.tracklink.pe');
    $mail->Port = 25;
    $mail->SMTPAuth = false;
    $mail->CharSet = 'UTF-8';
    $mail->setFrom('no-responder@tracklink.pe',$nombreE.' de Tracklink');  
    $mail->addReplyTo("$correoEnviador", $nombreE.' de Tracklink'); // donde deven responder
   

    foreach ($correo as $corr){
        $mail->addAddress("$corr"); // aquien se manda el mensaje            
    }
    //CC and BCC         
    $mail->addCC("$correoEnviador");
    $mail->addBCC("pruebas-ti@tracklink.pe");
    //Set the subject line
    $mail->Subject = 'Renovacion de servicio GPS '.$nombres.' '; //// ASUNTO //////
    //Read an HTML message body from an external file, convert referenced images to embedded,
    $mail->AddEmbeddedImage('../assets/img/logo_firma.png', 'tracklink');
    $mail->AddEmbeddedImage('../assets/img/media.png', 'twitter');
    $mail->AddEmbeddedImage('../assets/img/youtube.png', 'youtube');
    $mail->AddEmbeddedImage('../assets/img/fb.png', 'facebook');
    $mail->AddEmbeddedImage('../assets/img/head.png', 'head');
    //convert HTML into a basic plain-text alternative body
    $mail->msgHTML($thehtml);
    //Replace the plain text body with one created manually
    $mail->AltBody = 'Estimado(as) '.$nombres.'Deseamos confirmar la renovación de su servicio GPS'; ////////ESTE SE EL CONTENIDO//// QUE SE BERA EN PRIERA INSTANACIA
    //Attach an image file
      //aqui se adjunta los archivos que queremos enviar :D


    // $mail->addAttachment('images/phpmailer_mini.png');

   // ///////////// send the message, check for errors
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
      

   }

  //////
}
}


    if ($renoObj->registrarSeguimiento($_POST['idRenovacion'])) {
        
    }
    ////////////REGISTRAR EN LA BASE DE DATOSS HERE E ----////////////////////


   }





   ?>

   <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
   <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
   <link href="../assets/css/sweetalert2.css" rel="stylesheet">
   <style>
    .widget p {
        display: inline-block;        
    }

    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }

    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }

    .reloj .caja-segundos {
        display: inline-block;
    }

    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }</style>
    <body>

    <style>
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
}
</style>
<div class="se-pre-con"></div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <?php include("../includes/menuIzquierdo.php") ?>

               </div>
           </nav>

           <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>

                </nav>
            </div>
            <div class="wrapper wrapper-content">

             



<?php include("../includes/footer.php") ?>
</div>
</div>

</div>
<!-- LOS MODALES AQUI  -->


<div class="modal inmodal" id="bateriasOModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                <h4 class="modal-title">Renovación</h4>
                <small class="font-bold"><div class="form-group ">
                </div></small>
            </div>
            <form  role="form" method="post" >

                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="submit" name="gestionarBateria" class="btn btn-primary">Guardar</button>
                </div>
            </form>   
        </div>
    </div>
</div>


<!-- LOS MODALES AQUI  -->

<!-- <button class='btn' data-clipboard-action='copy' data-clipboard-target='p'>Copy</button> -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>

<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/clipboard.min.js"></script>
<script src="../assets/calendario/dist/sweetalert.min.js"></script>

<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
 <?php include_once('../includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
   
<script>

    <?php if (isset($registrado)): ?>
    swal({
      title: "Copiar y agendar en Onix!",
      text: "<?php echo $paraAgendar ?> ",
      html: true,
      type: "success"
  });    
<?php endif ?> 


var clipboard = new Clipboard('.btn1');

clipboard.on('success', function(e) {
    console.log(e);
});

clipboard.on('error', function(e) {
    console.log(e);
});
$(document).ready(function(){

    $('.footable2').footable();
    $('.footable').footable();
    $('.footable3').footable();

});

// var baterias={}
// baterias.fetchMessages = function() {
//     $.ajax({
//         url: 'ajax/bateria',
//         type: 'post',
//         data: {method: 'fetch'},
//         success: function(data) {
//             $('#bateriasOctoData').html(data);
//         }
//     }

//     );  
// }

// baterias.interval = setInterval(baterias.fetchMessages, 5000);
// baterias.fetchMessages();
//MODAL 
$('#bateriasOModal').on('show.bs.modal', function (e) {
    var rowid = $(e.relatedTarget).data('id');
    $.ajax({
        type : 'post',
                url : 'modal/renovacionesModal', //Here you will fetch records 
                data :  'rowid='+ rowid, //Pass $id
                success : function(data){
                $('.modal-body').html(data);//Show fetched data from database
            }
        });
});

//MODAL 
//BATERIAS 
var eventos={}
eventos.frejcar =function(){
    $.ajax({
        url: 'includes/bateriaOctoData',
                type: 'POST', // Send post data
                async: true,
                success: function(data){
                    $('.ibox-content .footable .tbody').html(data);
                }
            });
}
eventos.interval = setInterval(eventos.frejcar, 4000);
eventos.frejcar();

//BATERIAS 

(function(){
    var actualizarHora = function(){
        // Obtenemos la fecha actual, incluyendo las horas, minutos, segundos, dia de la semana, dia del mes, mes y año;
        var fecha = new Date(),
        horas = fecha.getHours(),
        ampm,
        minutos = fecha.getMinutes(),
        segundos = fecha.getSeconds(),
        diaSemana = fecha.getDay(),
        dia = fecha.getDate(),
        mes = fecha.getMonth(),
        year = fecha.getFullYear();

        // Accedemos a los elementos del DOM para agregar mas adelante sus correspondientes valores
        var pHoras = document.getElementById('horas'),
        pAMPM = document.getElementById('ampm'),
        pMinutos = document.getElementById('minutos'),
        pSegundos = document.getElementById('segundos'),
        pDiaSemana = document.getElementById('diaSemana'),
        pDia = document.getElementById('dia'),
        pMes = document.getElementById('mes'),
        pYear = document.getElementById('year');

        
        // Obtenemos el dia se la semana y lo mostramos
        var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        pDiaSemana.textContent = semana[diaSemana];

        // Obtenemos el dia del mes
        pDia.textContent = dia;

        // Obtenemos el Mes y año y lo mostramos
        var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        pMes.textContent = meses[mes];
        pYear.textContent = year;

        // Cambiamos las hora de 24 a 12 horas y establecemos si es AM o PM

        if (horas >= 12) {
            horas = horas - 12;
            ampm = 'PM';
        } else {
            ampm = 'AM';
        }

        // Detectamos cuando sean las 0 AM y transformamos a 12 AM
        if (horas == 0 ){
            horas = 12;
        }

        // Si queremos mostrar un cero antes de las horas ejecutamos este condicional
        // if (horas < 10){horas = '0' + horas;}
        pHoras.textContent = horas;
        pAMPM.textContent = ampm;

        // Minutos y Segundos
        if (minutos < 10){ minutos = "0" + minutos; }
        if (segundos < 10){ segundos = "0" + segundos; }

        pMinutos.textContent = minutos;
        pSegundos.textContent = segundos;
    };

    actualizarHora();
    var intervalo = setInterval(actualizarHora, 1000);
}())
</script>
   <script>
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
<?php else: ?> 
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?> 
<?php endif ?>
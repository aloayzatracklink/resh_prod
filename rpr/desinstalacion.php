<?php $nav="desinstalacion" ;?>
<?php include("../includes/head.php") ?>

<!-- <link href="../assets/css/bootstrap-datetimepicker.min.css"> -->
<link href="../assets/calendario/fullcalendar.min.css" rel="stylesheet">
<link href="../assets/calendario/fullcalendar.print.min.css" rel='stylesheet' media='print'>
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<link href='../assets/calendario/scheduler.min.css' rel='stylesheet' />
<link rel="stylesheet" type="text/css" href="../assets/css/jquery.datetimepicker.min.css"/>
<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
} ?>
<?php 
    require_once '../includes/tablaProgramacion.php';
    $echo=array();
    $errores=array();
    $pendiente='';
    $cheuqoObj=new Chequeos();
    $renovacionOb=new Renovaciones();
    $opeRacionObj=new Operacion();        
    $objtablaP=new tablaProgramacion();
    if (isset($_POST['inicio'])) {
        $chasis=limpiar(trim($_POST['chasis']));
        if(!$d=$cheuqoObj->validarExistenciaPendienteChasis($chasis)){
            $clienteObj=Onix::ObtenerDatosDesdeChasis($chasis);                 
        if ($clienteObj) {            
            $planobj=Onix::obtenerPlanDeServicio($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation);            
            $cheuqoObj->dniRuc=$clienteObj->dni;
            $apellidos=utf8_encode($clienteObj->apellidos);
            $cheuqoObj->nombres=$retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ;
            $cheuqoObj->placa=$clienteObj->placa;
            $cheuqoObj->marcaModelo=$clienteObj->marca." ".$clienteObj->modelo;
            $cheuqoObj->correo=$retVal = $clienteObj->correo2 .",".$clienteObj->correo2;
            $cheuqoObj->chasis=$chasis;            
            $cheuqoObj->celular=$clienteObj->celular;
            $cheuqoObj->telefono=$clienteObj->celular2." ".$clienteObj->telefono." ".$clienteObj->telefonoTrabajo;
            $cheuqoObj->plan=utf8_encode($planobj->articulo);
            $cheuqoObj->idcustomer=$clienteObj->idcustomer;
            $cheuqoObj->idvehicle=$clienteObj->idvehicle;
            $cheuqoObj->idlocation=$clienteObj->idlocation;
            if($cheuqoObj->registrarChequeoDesdeMonitoreo())
                $echo[]="Registrado con exito";
            else
                $errores[]="NO se pudo registrar";
        }else{
            $errores[]="No existe el chasis $chasis en la base de datos de Onyx";
        }
          }else{
            $errores[]="Chasis $chasis ya ha sido registrado";            
          }
    }

    if (isset($_POST['guardarCh'])) {
        if (isset($_POST['desinstalado'])) {
            $renovacionOb->obsDesinstalacion=$_POST['seguimientoDes'];
            $renovacionOb->idrenovacion=$_POST['idRenovacion'];
            if ($renovacionOb->marcarComoDesinstalado()) {
                $echo[]="Marcado como desinstalado";
            }else{
                $errores[]="Algun error al guardar como desinslaar";
            }
        }else{
if (Token::verificar($_POST['guardarCh'])) {            
    //die(var_dump($_POST));
        $objtablaP->fecha=$_POST['fecha'];
        $objtablaP->hora=$_POST['hora'];
        $objtablaP->idtecnico=$_POST['pos'];
         $objtablaP->tth_instalacion="0".$_POST['canthoraprog'].":00";
         $objtablaP->tth_trasalado='00:00';
    if ($_POST['lugar']=='Taller') {
        $objtablaP->iddistrito=$_POST['disT'];
        $objtablaP->ubicacion='Taller';
    }elseif($_POST['lugar']=='Domicilio Cliente'){
        $objtablaP->iddistrito=$_POST['distrito'];
        $objtablaP->ubicacion='Domicilio';
    }elseif($_POST['lugar']=='Concesionario'){
        $objtablaP->iddistrito=$_POST['distrito'];
        $objtablaP->ubicacion='Concesionario';
    }

    if (!$objtablaP->registrarTrabajoEntabla()) {
        $errores[]="no se creo en tabla programaciokn ";
        }
            if (isset($_POST['lugar'])) {
                if ($_POST['lugar']=='Taller') {
                      $cheuqoObj->departamentoc=$_POST['depT'];
                      $cheuqoObj->provinciac=$_POST['proT'];
                      $cheuqoObj->distritoc=$_POST['disT'];
                }else{
                    $cheuqoObj->departamentoc=$_POST['departamento'];
                    $cheuqoObj->provinciac=$_POST['provincia'];
                    $cheuqoObj->distritoc=$_POST['distrito'];                   
                }
            }
                $cheuqoObj->tipoChequeo="Desinstalacion";
                $cheuqoObj->estado=66;
                $cheuqoObj->seguimientoRealizado=htmlspecialchars($_POST['seguimientoDes']);
                $cheuqoObj->lugar=$_POST['lugar'];                    
                $cheuqoObj->plan=$_POST['plan'];                    
                $cheuqoObj->dniRuc=$_POST['dniRuc'];                    
                $cheuqoObj->nombres=$_POST['nombres'];                    
                $cheuqoObj->placa=$_POST['placa'];                    
                $cheuqoObj->marcaModelo=$_POST['marcaModelo'];                    
                $cheuqoObj->chasis=$_POST['chasis'];                    
                $cheuqoObj->direccion=htmlspecialchars($_POST['direccionInstalacion']); 
                if (isset($_POST['referenciaInstalacion'])) {
                    $cheuqoObj->referencia=htmlspecialchars($_POST['referenciaInstalacion']);
                }
                if (isset($_POST['coordenadasChequeoEnLineaLugar'])) {
                    $cheuqoObj->coordenadas=htmlspecialchars($_POST['coordenadasChequeoEnLineaLugar']);
                }
                $cheuqoObj->fechaInstalacion=$_POST['fechaInstalacion'];
                $cheuqoObj->instalador=$_POST['tecnicoInstalador'];
                $cheuqoObj->ubicacionSistema=$_POST['ubicacionDeSistema'];
                $cheuqoObj->personaContacto=limpiar($_POST['pcont']);
                $cheuqoObj->numeroContacto=limpiar($_POST['nrcontacto']);
                $cheuqoObj->fechaConfirmacionLinea=date('YmdHis');
                $cheuqoObj->fechaChequeoEnSac=limpiar($_POST['fecha'].$_POST['hora']);
                $cheuqoObj->motivo=htmlspecialchars("NO RENOVACION DE SERVICIO");
                $cheuqoObj->observacion=$_POST['seguimientoDes'];                    
                if ($cheuqoObj->guardarComoChequeoDesinstalacion()) {
                    $opeRacionObj->trabajo="Desinstalacion";                
                    $opeRacionObj->idChequeo=$cheuqoObj->idchequeo;
                    $opeRacionObj->idcliente=$_POST['idcustomer'];
                    $opeRacionObj->idvehiculo=$_POST['idvehicle'];
                    $opeRacionObj->idlocation=$_POST['idlocation'];   
                    $opeRacionObj->observacion=$_POST['seguimientoDes'];   
                    $opeRacionObj->nombreContacto=$_POST['pcont'];   
                    $opeRacionObj->numeroContacto=$_POST['nrcontacto'];   
                    if (isset($_POST['fecha']) AND isset($_POST['hora'])) {
                        $opeRacionObj->fechaInstalacion=$_POST['fecha']." ".$_POST['hora'];
                    }           
                    if (isset($_POST['lugar'])) {
                    if ($_POST['lugar']=='Taller') {
                              $opeRacionObj->departamentoi=$_POST['depT'];
                              $opeRacionObj->provinciai=$_POST['proT'];
                              $opeRacionObj->distritoi=$_POST['disT'];
                        }else{
                            $opeRacionObj->departamentoi=$_POST['departamento'];
                            $opeRacionObj->provinciai=$_POST['provincia'];
                            $opeRacionObj->distritoi=$_POST['distrito'];                   
                        }
                    }                  
                $opeRacionObj->estado='14';
                if (isset($_POST['lugar'])) {
                    $opeRacionObj->lugarInstalacion=$_POST['lugar'];
                }
            if ($opeRacionObj->registrarChequeo()) {
                $lugar=$_POST['lugar'];
                if ($lugar=="Taller") {
                    $objtablaP->idoperacion=$opeRacionObj->idoperacion;
                    $objtablaP->insertaridoperacion();       
                }
                if ($lugar=="Domicilio Cliente") {
                    $objtablaP->idoperacion=$opeRacionObj->idoperacion;
                    $objtablaP->insertaridoperacionDoble();       
                }
                if ($lugar=="Concesionario") {
                    $objtablaP->idoperacion=$opeRacionObj->idoperacion;
                    $objtablaP->insertaridoperacionDoble();       
                }
                $renovacionOb->obsDesinstalacion=$_POST['seguimientoDes'];
                $renovacionOb->idrenovacion=$_POST['idRenovacion'];
                $renovacionOb->idoperacion=$opeRacionObj->idoperacion;
                if ($renovacionOb->marcarComoProgramado()) {

                }                    
                $echo[]="Desinstalación programada.";
                }else{
                    $errores[]="Error x334243787";
                }               
           } else {
               $errores[]="Ocurrio algun error al registrar el chequeo error x220";
           } 
}else{
        $errores[]="El formulario ya fue procesado error Token..";
    }
}
}
// if (isset($_POST['guardarComoPendiente'])) {
// }
// 153
// 8888
 ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->desinstalacion=="si"): ?>
    <style>.pagination1{margin:0;padding:0;}
.pagination1 li{
    display: inline;
    padding: 6px 10px 6px 10px;
    border: 1px solid #ddd;
    margin-right: -1px;
    font: 15px/20px Arial, Helvetica, sans-serif;
    background: #FFFFFF;
    box-shadow: inset 1px 1px 5px #F4F4F4;
}
.pagination1 li a{
    text-decoration:none;
    color: rgb(89, 141, 235);
}
.pagination1 li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination1 li.last {
    border-radius: 0px 5px 5px 0px;
}
.pagination1 li:hover{
    background: #CFF;
}
.pagination1 li.active{
    background: #F0F0F0;
    color: #333;
}</style>
<body>
    <link href="../assets/css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">
    <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
        <link href="../assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
               <?php include("../includes/menuIzquierdo.php") ?>
            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <?php include("../includes/navegacionArriba.php") ?>

        </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                     <div class="ibox col-lg-12">
                <div class="ibox-title">
                <form action="" method="post">                    
                <div class="row">
                        <div class="col-md-6">&nbsp;&nbsp;&nbsp;<label for="">Lista de contatos vencidos</label> </div>
                        <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">                            
                            <label for="placa" class="sr-only">Chasis del vehículo</label>
                            <input type="text" placeholder="Chasis del vehículo" id="placa" name="chasis" class="form-control text-uppercase"  required >
                            </div>
                            <div class="col-md-6">
                                
                            <button class="btn btn-success" type="submit" name="inicio" disabled="">Registrar para desinstalación</button>
                            </div>
                            </div>
                        </div>
                    </div>
                </form>
                </div>
                <div class="ibox-content">
                    <input type="text" class="form-control input-sm m-b-xs" id="filter"
                    placeholder="Filtrar" autofocus="">
                    <table class="table table-stripped" data-page-size="8" data-filter=#filter>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Cliente(s)</th> 
                                <th>Placa</th>
                                <th>Estado</th>
                                <th>Fecha programada</th>  
                                <th>Fecha registro</th>
                                <th>Observaciones</th>
                                <th>Registrado por</th>
                                <th>Acción</th>
                            </tr>
                        </thead>    
                        <tbody id="chData">
                        </tbody>
                    </table>

                </div>
            </div>
            </div>
            </div>
              <?php include("../includes/footer.php") ?>
            </div>
        </div>

            
<div class="modal inmodal" id="modalDes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Gestionar desinstalaciones</h4>
            </div>
            <form  role="form" method="post" id='formDes' >
                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="submit" id="agregarVehiculoBttn" name="guardarCh" class="btn btn-primary" value="<?php echo Token::generar() ?>" >Guardar</button>
                    <div class="row">
                        <div class="col-lg-12">              
                          <label id="mostrar" class="text-center text-danger">Limite de taller alcanzado por favor intenta en otro horario</label>
                        </div>
                      </div>
                </div>
            </form>   
        </div>
    </div>
</div>
</div>

    </html>
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>    
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>    
<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="../assets/build/jquery.datetimepicker.full.js"></script>
<?php include_once('../includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
<script>
$('#formDes').validate();
    $(document).ready(function(){
        $('.footable').footable();
        $('.footable2').footable();
    });
    $("#chData" ).load( "pag/datosDesinstalacio"); //load initial records
    $("#chData").on( "click", ".pagination1 a", function (e){
        e.preventDefault();
        $(".se-pre-con").show(); //show loading element
        var page = $(this).attr("data-page"); //get page number from link
        $("#chData").load("pag/datosDesinstalacio",{"page":page}, function(){ //get content from PHP page
            $(".se-pre-con").hide(); //once done, hide loading element
        });        
    });
    $("#filter").bind("keyup focus",function()
    {   
        var datosS = $(this).val();        
        datosS = datosS.replace('<>/"', '');
        if(datosS.length >= 1)
            {
        $.ajax({
                type : 'POST',
                url  : 'pag/resultadoDes',
                data :  {datos:datosS},
                success : function(data)
                {
                    $("#chData").html(data);
                }
            });
            return false;
        }else{
            var page = $("#chData").attr("data-page");
            $.ajax({
            type : 'POST',
            url  : 'pag/datosDesinstalacio',
            data : {page:page},
            success : function(data)
            {
             $("#chData").html(data);
            }
            });
    
        }
    });

    $('#modalDes').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        $.ajax({
            type : 'post',
            url : 'modal/desinstalacionesModal', //Here you will fetch records 
            data :  'rowid='+ rowid, //Pass $id      
            success : function(data){
                    $('.modal-body').html(data);//Show fetched data from database
                }            });
    });
    $('#mostrar').hide();
 function capacidad(cantidad){
   $('#agregarVehiculoBttn').prop('disabled', true);        
   $('#mostrar').show();           
  }
 function limpiarCapacidad(){
    $('#agregarVehiculoBttn').prop('disabled', false);        
    $('#mostrar').hide();  
  }
</script>
<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>

  <?php require_once("../../includes/init.php") ?>
  
  <?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $renoObj=new Renovaciones();
  
  $cantidadO=$renoObj->obtenerRegistrosSubidosCantidaOtherEstado();
  $registrosPorPagina=7;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  // echo $cantidadO->cantidad; 
  $n=1;
  $datos=$renoObj->obtenerRegistrosSubidosOtherEstado($posisionDePagina,$registrosPorPagina);
             ?>    
<?php if ($datos): ?>
<?php foreach ($datos as $var): ?>    

    <?php 
    $datosF=DatosFacturacion::obtenerDatosFacturacionPorIdRenovacion($var->idrenovacion);

    ?>
    <tr class="animated bounceIn">
        <td><?= $retVal = (!empty($var->nombreCliente)) ? $var->nombreCliente : "No Esta en la base de datos" ; ?></td>
        <td><?= $var->placa ?></td>
        <td><?= obtenerEstado($var->estado)  ?></td>
        <td><?= haceMinimo($var->fechaModificada) ?></td>
        <td>
            <?= ($var->estado==57 && $datosF!=false)?$datosF->numeroBoleta.$datosF->numeroFactura:'-' ?>
        </td>
        <td>

        <?php if($datosF){ ?>
        <?php $filess=DatosFacturacion::obtenerFiles($datosF->datosfacturacionid) ?>
        <?php if($filess):  ?>
        <table class="table table-hover">
        <?php foreach($filess as $file): ?>
            <tr>
                <td><?= $file->fecha ?></td>
                <td><?= $file->oficina ?>&nbsp;&nbsp;<span class="fa fa-file-pdf-o"></span></td>
                <td><?= $file->tipoCanal ?></td>
                <td><?= $file->tipoCanal ?>&nbsp;
                    <a
                    href="/ope/downinvdoc?tnk=fef56f6a7c51ab802&b=<?= $file->voucher ?>&f=<?= $file->apunte ?>&fn=<?= strtoupper($file->oficina) ?> <?= trim($var->nombreCliente)." ".$var->placa ?>"
                    target="_blank"><span class="fa fa-download"></span></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </table>
        <?php else: ?>
        
            Generando documentos...
            <!--
            <a class="btn btn-primary" href="/ope/downinvdoc?tnk=eef56f6a7c51ab800&o=<?= $datosF->orden ?>&d=<?=$datosF->datosfacturacionid ?>" title="Generar">
                <span class="fa fa-file"><span><i class="fa fa-plus"></i></span>
            </a>
            -->
        <?php endif; ?>
        <?php } ?>
        </td>
        <td><?= $var->observacion ?></td>
        <td><?php if($var->paymentLink){
                echo $var->statusPay." -> ".$var->norderPay;
            }?>
        </td>
    </tr>
<?php endforeach ?>
    <tr>
        <td colspan="8">                        
        <?php echo paginate_function2($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
        </td>
    </tr>
 <?php endif ?>

<?php } ?>

<?php 
require_once("../../includes/init.php");
if(isset($_POST)){
    $desde ="";
    $hasta  ="";
    if(isset($_POST['fecha'])){
        if($_POST['fecha']!="") {
            $fechaConsulta = $_POST['fecha'];
            $desde = trim(current(explode('/', $fechaConsulta)));
            $tmp = explode('/', $fechaConsulta);
            $hasta = end($tmp);
            $desde = date("Y-m-d", strtotime($desde));
            $hasta = date("Y-m-d", strtotime($hasta));
        }
    }

    $objPSV= new Operacion;;
    $camp_igual ="";
    $ntotal = 0;
    $n = 0;
 ?>
       <?php $dsmart=$objPSV->dataWelcomeMail($desde,$hasta,"",1);?>
       <?php if ($dsmart): ?>
        <?php foreach ($dsmart as $var):?>
            <?php  if($camp_igual != $var->campana){
                $dsmart2=$objPSV->dataWelcomeMail($desde,$hasta,$var->campana,1);
                 $ntotal = count($dsmart2) ;
                ?>
            <tr>
                <td rowspan="<?= $ntotal + 1?>" style="text-align: center; vertical-align: middle;font-weight: bold"><?= $var->campana ?></td>
                <td style="font-weight: bold"><?= $var->plan ?></td>
                <td  style="text-align: center;"><?= $var->enviados ?></td>
                <td style="text-align: center;"><?= $var->aceptados ?></td>
                <td style="text-align: center; color:red;"><?= $var->enviados-$var->aceptados ?></td>
                <td style="text-align: center;"><?= $var->porc." %" ?></td>
            </tr>
            <?php $camp_igual = $var->campana;
                $n++;

                if($n == $ntotal){
                    $dsmart3=$objPSV->dataWelcomeMail($desde,$hasta,$var->campana,2);
                    ?>
                    <tr style="background-color: #4ca7e8;color: white;font-weight: bold;">
                        <td> Total</td>
                        <td style="text-align: center;"><?= $dsmart3->enviados ?></td>
                        <td style="text-align: center;"><?= $dsmart3->aceptados ?></td>
                        <td style="text-align: center; color:red;"><?= $dsmart3->enviados-$dsmart3->aceptados ?></td>
                        <td style="text-align: center;"><?= $dsmart3->porc." %" ?></td>
                    </tr>

                    <?php $n=0;}


                }else{
                  $n++;
                ?>
                <tr>
                    <td style="font-weight: bold"><?= $var->plan ?></td>
                    <td  style="text-align: center;"><?= $var->enviados ?></td>
                    <td style="text-align: center;"><?= $var->aceptados ?></td>
                    <td style="text-align: center; color:red;"><?= $var->enviados-$var->aceptados ?></td>
                    <td style="text-align: center;"><?= $var->porc." %" ?></td>
                </tr>
                <?php
                if($n == $ntotal){
                    $dsmart4=$objPSV->dataWelcomeMail($desde,$hasta,$var->campana,2);
                    ?>
                    <tr style="background-color: #4ca7e8;color: white;font-weight: bold;">
                        <td> Total</td>
                        <td style="text-align: center;"><?= $dsmart4->enviados ?></td>
                        <td style="text-align: center;"><?= $dsmart4->aceptados ?></td>
                        <td style="text-align: center; color:red;"><?= $dsmart4->enviados-$dsmart4->aceptados ?></td>
                        <td style="text-align: center;"><?= $dsmart4->porc." %" ?></td>
                    </tr>

                    <?php $n=0;} ?>
                <?php }?>
       <?php endforeach;
        $dsmart4=$objPSV->dataWelcomeMail($desde,$hasta,"",2);
        ?>
        <tr style="background-color: #24a978;color: white;font-weight: bold;">
            <td colspan="2" style="text-align:center"> Total</td>
            <td style="text-align: center;"><?= $dsmart4->enviados ?></td>
            <td style="text-align: center;"><?= $dsmart4->aceptados ?></td>
            <td style="text-align: center; ">
                <input type="hidden" name="cantPendientes" id="cantPendientes" value="<?=$dsmart4->enviados-$dsmart4->aceptados ?>">
                <a href="#" data-toggle="modal" data-target="#modalRepartirPendientes" style="color:red; text-decoration:underline;" data-toggle="tooltip" data-placement="bottom" title="Repartir pendientes"><?= $dsmart4->enviados-$dsmart4->aceptados ?></a>
            </td>
            <td style="text-align: center;"><?= $dsmart4->porc." %" ?></td>
        </tr>
       <?php endif ?>
<?php } ?>
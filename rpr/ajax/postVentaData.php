<?php 
require_once("../../includes/init.php");

if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    if(isset($_POST["page"])){
        $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
        if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
    }else{
        $numeroDePagina = 1;
    }


    $desde ="";
    $hasta  ="";
    if(isset($_POST['fecha'])){
        if($_POST['fecha']!="") {
            $fechaConsulta = $_POST['fecha'];
            $desde = trim(current(explode('/', $fechaConsulta)));
            $tmp = explode('/', $fechaConsulta);
            $hasta = end($tmp);
            $desde = date("Y-m-d", strtotime($desde));
            $hasta = date("Y-m-d", strtotime($hasta));
        }
    }

    $stMW= "";

    if(isset($_POST['estMW'])){
        $stMW = $_POST['estMW'];

    }

    $stEA = "";
    if(isset($_POST['estEA'])){
        $stEA =$_POST['estEA'] ;
    }

    $objPSV= new Operacion;
    $datos=limpiar($_POST['datos']);
    $cantidadO=$objPSV->obtenerCantidadvehiculosParaBienvenida($datos,"'3','99'",$desde,$hasta,$stMW,$stEA);
    $registrosPorPagina=15;
    $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
    $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
    $vehiculoObjOp=$objPSV->vehiculosParaBienvenidaPag($datos,$posisionDePagina,$registrosPorPagina,"'3','99'",$desde,$hasta,$stMW,$stEA);

    $n=$posisionDePagina+1;
	/*$vehiculoObjOp=Operacion::vehiculosParaBienvenida();	*/
 ?>
 <?php if ($vehiculoObjOp): ?>
 <?php foreach ($vehiculoObjOp as $var): ?>
 	<tr>
 		<td><?= $n++ ?></td>
 		<td><?= $var->trabajo ?></td>
 		<td><?= $retVal = ($var->tipoCliente=="Natural") ? strtoupper($var->apellidosC." ".$var->nombre) : strtoupper($var->razonSocial) ; ?></td>
 		<td><?= $var->placa ?></td>
 		<td><?= formatoSinHora($var->fechaInstalacion) ?></td>
 		<!-- <td><?= formatoSinHora($var->fechaActivacion) ?></td>  -->
		<td class="center"><?= $var->plan ?> <?= $var->campana ?></td>
        <td class="center"><?= $var->mailWelcome?'Enviado':'' ?><?= $var->sendMail == 1?' Programado':'' ?> </td>
        <td class="center"><?= $var->statusAccept?'Aceptado':'' ?></td>
        <td class="center"><?= $var->dateAccept ?></td>
        <td class="center"><?= $var->recordmail>1?'si':'no' ?></td>
        <td class="center"><?= $var->recordsms>0?'si':'no' ?></td>
		<?php $usuarioObj=Usuario::obtenerUsuario($var->idCargoB);  ?>
 		<td style="background-color: <?php echo $retVal = ($usuarioObj->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>"><?php  echo $usuarioObj->usuarioNombre ?> <?php echo $usuarioObj->usuarioApellidoPaterno?></td>
 		<!--<td class="center "><a href="/rpr/activatetmp?idv=<?=$var->idvehiculo?>" class="btn btn-success <?= $var->mailWelcome?'disabled':'' ?> " style="padding-top: 0;padding-bottom: 0" <?= $var->mailWelcome?'disabled':'' ?> > <?= $var->mailWelcome?'Enviado':'Enviar' ?> <i class="fa fa-send"></i></a></td>-->
 		<td class="center ">
            <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#ordenes" data-id="<?php echo $var->datosfacturacionid ?>">Elegir <i class="fa fa-cog"> </i></button>
            <?php if(strstr($var->campana,"no") or strstr($var->campana,"Smart") ){?>
                <form action="" method="post" name="FRMLINK" id="FRMLINK<?= $var->datosfacturacionid ?>">
                    <button class="btn btn-info ladda-button  ladda-button-dlink<?= $var->datosfacturacionid?> btnlink<?= $var->datosfacturacionid ?>" data-placa="<?= $var->placa ?>" data-style="zoom-in" style="padding-top: 0;padding-bottom: 0" onclick="return btnSendWelcome(this)" data-tipo="blink"  data-Hlink="<?= ($var->tipoCliente=="Natural") ? strtoupper($var->apellidosC." ".$var->nombre) : strtoupper($var->razonSocial) ?>" data-id="<?= $var->datosfacturacionid ?>"  id="btnlink<?= $var->datosfacturacionid ?>" name="btnlink">Enviar Bienvenida</button>
                    <input type="text" name="idlinkHomo" id="idlinkHomo<?= $var->datosfacturacionid ?>" disabled value="<?= $var->datosfacturacionid; ?>" style="display:none;">
                    <input type="text" name="idlOrden" id="idlOrden<?= $var->datosfacturacionid ?>" disabled value="<?= $var->ordenContrato; ?>" style="display:none;">
                    <input type="text" name="btnblink" id="btnblink<?=$var->datosfacturacionid ?>" disabled value="" style="display:none;">
                </form>
            <?php }?>
        </td>
 	</tr>
 <?php endforeach ?>

        <tr>
            <td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
            <td colspan="9">
                <?= paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
            </td>
        </tr>

    <?php else: ?>
        <tr>
            <td colspan="9" class="text-center">No se encontro registro alguno
            </td>
        </tr>
    <?php endif ?>
<?php } ?>
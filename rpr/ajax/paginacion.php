  <?php require_once("../../includes/init.php") ?>
  
  <?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $renoObj=new Renovaciones();
  
  $cantidadO=$renoObj->obtenerRegistrosSubidosCantida();
  $registrosPorPagina=7;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  // echo $cantidadO->cantidad; 
  $n=1;
  $datos=$renoObj->obtenerRegistrosSubidos($posisionDePagina,$registrosPorPagina);

             ?>    

             <?php if ($datos): ?>
              
<?php foreach ($datos as $var): ?>    
    <tr class="animated bounceIn">
        <td><?php echo $n++ ?></td>
        <td><?php  echo $retVal = (!empty($var->nombreCliente)) ? $var->nombreCliente : "No Esta en la base de datos" ; ?></td>
        <td><?php echo $var->placa ?></td>
        <td><?php echo $retVal = (!empty($var->vence)) ? formatoSinHora($var->vence) : "" ;  ?></td>
        <td><?php echo $retVal = (!empty($var->vence)) ? vence($var->vence) : "" ;  ?></td>
        <td><?php echo haceMinimo($var->fechaCreada) ?></td>
        <td><?php echo $var->observacion ?></td>
        <td><?php if($var->paymentLink){
             echo $var->statusPay." -> ".$var->norderPay;
          }?>
        </td>
        <td>
            <?php if($var->paymentLink){?>
            <a id="<?= $n ?>"><?php echo $var->paymentLink ?></a>
            <button class="btn btn-primary "  onclick="copyToClipboard('#<?=$n?>')" style="padding-top: 0;padding-bottom: 0" ><i class="fa fa-copy"></i></button>
            <?php }else{ echo "[--]";}?>
        </td>
        <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal"  data-target="#agregarRenovacionModal" data-id="<?php echo $var->idrenovacion ?>" >Elegir <i class="fa fa-cog"></i></button>&nbsp;

            <?php /*if($var->paymentLink){*/?><!--
            <form action="" method="post" name="FRMLINK" id="FRMLINK<?= $var->idrenovacion ?>">
            <input type="text" name="idlinkRen" id="idlinkRen<?= $var->idrenovacion ?>" disabled value="<?= $var->idrenovacion; ?>" style="display:none;">
            <input type="text" name="btnblinkRen" id="btnblinkRen<?=$var->idrenovacion ?>" disabled value="" style="display:none;">
            <button class="btn btn-success ladda-button  ladda-button-dlink<?= $var->idrenovacion?> btnlink<?= $var->idrenovacion ?>" data-style="zoom-in" style="padding-top: 0;padding-bottom: 0" onclick="return btnLinkR(this)" data-tipo="blink" data-price="<?= $var->monto ?>" data-Hlink="<?= (!empty($var->nombreCliente)) ? $var->nombreCliente : "No Esta en la base de datos" ?>" data-id="<?= $var->idrenovacion ?>"  id="btnlink<?= $var->idrenovacion ?>" name="btnlink">Enviar Link</button>
            </form>-->
                <?php/*}*/ ?>
        </td>
    </tr>
<?php endforeach ?>
 
              <tr>
                    <td colspan="8">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
                    </td>
                </tr>
            
 <?php endif ?>




<?php } ?>
  <script>
      function copyToClipboard(e) {
          var $temp = $("<input>");
          $("body").append($temp);
          $temp.val($(e).text()).select();
          document.execCommand("copy");
          $temp.remove();
          toastr.success('copiado', 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
      }
  </script>

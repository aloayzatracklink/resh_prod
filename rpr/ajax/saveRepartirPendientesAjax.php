<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST)){
    $response = ["error"=>false, "msg"=>""];

    $periodo          = $_POST["periodo"];
    $periodoIni       = $_POST["periodoIni"];
    $periodoFin       = $_POST["periodoFin"];
    $usuariosRepartir = $_POST["usuariosRepartir"];
    $limit_ini        = 0;

    $reparto               = new RepartePendientesWelcomeClass();
    $reparto->vperiodo_ini = $periodoIni;
    $reparto->vperiodo_fin = $periodoFin;
    $ifExiste              = $reparto->siExisteEnFechasReparticion();

    if($reparto->siExisteEnFechasReparticion()==true){
        $response["error"] = true;
        $response["msg"] = "Ya existe una distribución de pendientes registrado. Seleccione un intervalo de fechas diferente";
    }else{
        unset($reparto);
        foreach ($usuariosRepartir as &$row) {
            $reparto = new RepartePendientesWelcomeClass();
            $reparto->vperiodo            = $periodo;
            $reparto->vperiodo_ini        = $periodoIni;
            $reparto->vperiodo_fin        = $periodoFin;
            $reparto->fk_idarea_gestiona  = $row["area"];
            $reparto->fk_usuario_gestiona = $row["persona_cargo"];
            $reparto->icantidad           = $row["cantidad"];
            $sql = $reparto->registrar($limit_ini, $row["cantidad"]);
            $limit_ini+=$row["cantidad"];
            //$response['sql'] = $sql;
            unset($reparto);
        }
        
        $response['ifExiste'] = $ifExiste;
        $response["msg"] = "Se registró correctamente";
    }

   
    echo json_encode($response);
}
?>
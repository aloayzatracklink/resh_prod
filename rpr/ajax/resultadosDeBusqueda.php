     <?php require_once("../../includes/init.php") ?>
  <?php if ($_POST): ?> 
<?php 

   $datos=limpiar($_POST['datosS']); 
   $datos="%".$datos."%";
 
 ?>    

<?php 
$renoObj= new Renovaciones;
$datosA=$renoObj->buscarParaFiltro($datos);
$n=1;
 ?>
  <?php if ($datosA): ?>                
  <?php foreach ($datosA as $var): ?>      
      <tr class="animated ">
          <td><?php echo $n++ ?></td>
          <td><?php  echo $retVal = (!empty($var->nombreCliente)) ? $var->nombreCliente : "No Esta en la base de datos" ; ?></td>
          <td><?php echo $var->placa ?></td>
          <td><?php echo $retVal = (!empty($var->vence)) ? formatoSinHora($var->vence) : "" ;  ?></td>
          <td><?php echo $retVal = (!empty($var->vence)) ? vence($var->vence) : "" ;  ?></td>
          <td><?php echo haceMinimo($var->fechaCreada) ?></td>
          <td><?php echo $var->observacion ?></td>
          <td><?php if($var->paymentLink){
                  echo $var->statusPay." -> ".$var->norderPay;
              }?>
          </td>
          <td>
              <?php if($var->paymentLink){?>
                  <a id="<?= $n ?>"><?php echo $var->paymentLink ?></a>
                  <button class="btn btn-primary "  onclick="copyToClipboard('#<?=$n?>')" style="padding-top: 0;padding-bottom: 0" ><i class="fa fa-copy"></i></button>
              <?php }else{ echo "[--]";}?>
          </td>
          <td>
              <button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#agregarRenovacionModal" data-id="<?php echo $var->idrenovacion ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;
              <?php /*if($var->paymentLink){*/?><!--
                  <form action="" method="post" name="FRMLINK" id="FRMLINK<?= $var->idrenovacion ?>">
                      <input type="text" name="idlinkRen" id="idlinkRen<?= $var->idrenovacion ?>" disabled value="<?= $var->idrenovacion; ?>" style="display:none;">
                      <input type="text" name="btnblinkRen" id="btnblinkRen<?=$var->idrenovacion ?>" disabled value="" style="display:none;">
                      <button class="btn btn-success ladda-button  ladda-button-dlink<?= $var->idrenovacion?> btnlink<?= $var->idrenovacion ?>" data-style="zoom-in" style="padding-top: 0;padding-bottom: 0" onclick="return btnLinkR(this)" data-tipo="blink" data-price="<?= $var->monto ?>" data-Hlink="<?= (!empty($var->nombreCliente)) ? $var->nombreCliente : "No Esta en la base de datos" ?>" data-id="<?= $var->idrenovacion ?>"  id="btnlink<?= $var->idrenovacion ?>" name="btnlink">Enviar Link</button>
                  </form>-->

              <?php /*}*/ ?>
          </td>
      </tr>
  <?php endforeach ?>
   <?php else: ?>
   <tr>
        <td colspan="8" class="text-center">No se encontro ningun registro con <h2><?= $_POST['datosS'] ?>.</h2></td>
   </tr>
   <?php endif ?>   
 <?php endif ?>
     <script>
         function copyToClipboard(e) {
             var $temp = $("<input>");
             $("body").append($temp);
             $temp.val($(e).text()).select();
             document.execCommand("copy");
             $temp.remove();
             toastr.success('copiado', 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
         }
     </script>
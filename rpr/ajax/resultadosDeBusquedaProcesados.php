<?php require_once("../../includes/init.php") ?>
<?php if ($_POST): ?>
<?php 

   $datos=limpiar($_POST['datosS']); 
   $datos="%".$datos."%";
 
 ?>    

<?php 
$renoObj= new Renovaciones;
$datosA=$renoObj->buscarParaFiltroOtherEstado($datos);
$n=1;
 ?>
  <?php if ($datosA): ?>                
  <?php foreach ($datosA as $var): ?>      
    <?php 
    $datosF=DatosFacturacion::obtenerDatosFacturacionPorIdRenovacion($var->idrenovacion);
    ?>
    <tr class="animated bounceIn">
        <td><?= $retVal = (!empty($var->nombreCliente)) ? $var->nombreCliente : "No Esta en la base de datos" ; ?></td>
        <td><?= $var->placa ?></td>
        <td><?= obtenerEstado($var->estado)  ?></td>
        <td><?= haceMinimo($var->fechaModificada) ?></td>
        <td>
            <?= ($var->estado==57 && $datosF!=false)?$datosF->numeroBoleta.$datosF->numeroFactura:'-' ?>
        </td>
        <td>

        <?php if($datosF){ ?>
        <?php $filess=DatosFacturacion::obtenerFiles($datosF->datosfacturacionid) ?>
        <?php if($filess):  ?>
        <table class="table table-hover">
        <?php foreach($filess as $file): ?>
            <tr>
                <td><?= $file->fecha ?></td>
                <td><?= $file->oficina ?>&nbsp;&nbsp;<span class="fa fa-file-pdf-o"></span></td>
                <td><?= $file->tipoCanal ?></td>
                <td><?= $file->tipoCanal ?>&nbsp;
                    <a
                    href="/ope/downinvdoc?tnk=fef56f6a7c51ab802&b=<?= $file->voucher ?>&f=<?= $file->apunte ?>&fn=<?= strtoupper($file->oficina) ?> <?= trim($var->nombreCliente)." ".$var->placa ?>"
                    target="_blank"><span class="fa fa-download"></span></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </table>

            Generar nuevo contrato ->
            <a class="btn btn-primary" href="/ope/downinvdoc?tnk=eef56f6a7c51ab800&o=<?= $datosF->orden ?>&d=<?=$datosF->datosfacturacionid ?>" title="Generar">
                <span class="fa fa-file"><span><i class="fa fa-plus"></i></span>
            </a>
        <?php else: ?>
        
            Si no se genero ->
            <a class="btn btn-primary" href="/ope/downinvdoc?tnk=eef56f6a7c51ab800&o=<?= $datosF->orden ?>&d=<?=$datosF->datosfacturacionid ?>" title="Generar">
                <span class="fa fa-file"><span><i class="fa fa-plus"></i></span>
            </a>
        <?php endif; ?>
        <?php } ?>
        </td>
        <td><?= $var->observacion ?></td>
        <td><?php if($var->paymentLink){
                echo $var->statusPay." -> ".$var->norderPay;
            }?>
        </td>
    </tr>
  <?php endforeach ?>
   <?php else: ?>
   <tr>
        <td colspan="8" class="text-center">No se encontro ningun registro con <h2><?= $_POST['datosS'] ?>.</h2></td>
   </tr>
   <?php endif ?>   
 <?php endif ?> 

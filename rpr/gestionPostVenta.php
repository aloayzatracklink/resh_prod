<?php $nav="postVenta" ?>
<?php require("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
  irA("../ingresar");
}
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->gestionPostVenta=="si"): ?>
<?php 
$vehiculoObj=new Vehiculo();
$operacionObj=new Operacion();
if (isset($_POST['verificado'])) {
     if(Token::verificar($_POST['verificado'])) {
         $idvehiculo = $_POST['idv'];
//creamos las variables here :::VVVV
         $bienvenida;
         $usuarioContrasenia;
         $contrato;
         $estado;

         if (isset($_POST['estado'])) {
             $estado = $retVal = (isset($_POST['estado'])) ? $_POST['estado'] . " ubicado" : "";;
             $idvehiculo = $_POST['idv'];
             $vehiculoObj->accionBienvenida = $estado . " ---- " . $_POST['accionBienvenida'];;
             if ($vehiculoObj->pendienteBienvenida($idvehiculo)) {
             }

         } else {
             //recibimos los cheks :V
             $contrato = "";
             $bienvenida = "";
             $usuarioContrasenia = "";

             if (isset($_POST['bienvenida'])) {
                 $bienvenida .= "Se dio bienvenida";
             }
             if (isset($_POST['usuarioContrasenia'])) {
                 $usuarioContrasenia .= "Se brindo usuario y contraseña";
             }
             if (isset($_POST['contrato'])) {
                 $contrato .= "Se envio contrato";
             }
             //eso es todo amigos :V
             $vehiculoObj->accionBienvenida = $bienvenida . " -- " . $usuarioContrasenia . " -- " . $contrato . " -- " . $_POST['accionBienvenida'];;
             if ($vehiculoObj->bienvenida($idvehiculo)) {

             }
         }
         $echo[] = "Se gestiono Correctamente";
     }else {
         $errores[] = "El formulario ya fue procesado, verificar.";
     }
}else if (isset($_POST['btnblink'])) {
     if(Token::verificar($_POST['btnblink'])) {

         if (isset($_POST['idlinkHomo'])) {
                     ##GENERATE CONTRACT
                   if(isset($_POST['idlOrden'])) {
                       $ordenn=explode("-",$_POST['idlOrden']);
                       $ordenn[0];
                       $ordenn[1];

                       if($operacionObj->verficarOrden($ordenn[0],$ordenn[1])){
                           $command = escapeshellcmd('python3.7 ../../pdf/run_queue.py sendMailWelcome '.$_POST['idlinkHomo']);
                           $output = shell_exec($command);
                           $echo[] = "se envió Correo de Bienvenida";
                       }else{
                           $errores[] = " por favor verificar que la orden de contrato registrada es la correcta.";
                       }
                   }else{
                       $errores[] = " No se pudo generar por no tener Número de Contrato.";
                   }
         }

     }else {
         $errores[] = "El formulario ya fue procesado, verificar.";
     }
}else if (isset($_POST['guardarComoPendiente'])) {
    $idvehiculo=$_POST['idv'];        
    $vehiculoObj->accionBienvenida=$_POST['accionBienvenida'];;
    if($vehiculoObj->pendienteBienvenida($idvehiculo)){
    }   
}


?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="../assets/css/sweetalert2.css" rel="stylesheet">
<link href="../assets/css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">
<link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <?php include("../includes/menuIzquierdo.php") ?>

            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                   <?php include("../includes/navegacionArriba.php") ?>
               </nav>
           </div>
           <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins col-lg-12">
                <div class="ibox-title">
                    <h5>Pendientes para bienvenida</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
 <style>
     .no-js #loader { display: none;  }
     .js #loader { display: block; position: absolute; left: 100px; top: 0; }
     .se-pre-con {
         position: fixed;
         left: 0px;
         top: 0px;
         width: 100%;
         height: 100%;
         z-index: 9999;
         background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
     }
     .pagination1{margin:0;padding:0;}
     .pagination1 li{
         display: inline;
         padding: 6px 10px 6px 10px;
         border: 1px solid #ddd;
         margin-right: -1px;
         font: 15px/20px Arial, Helvetica, sans-serif;
         background: #FFFFFF;
         box-shadow: inset 1px 1px 5px #F4F4F4;
     }
     .pagination1 li a{
         text-decoration:none;
         color: rgb(89, 141, 235);
     }
     .pagination1 li.first {
         border-radius: 5px 0px 0px 5px;
     }
     .pagination1 li.last {
         border-radius: 0px 5px 5px 0px;
     }
     .pagination1 li:hover{
         background: #CFF;
     }
     .pagination1 li.active{
         background: #F0F0F0;
         color: #333;
     }

</style>
                <div class="row ibox float-e-margins ibox-content " style="margin: 0">
                    <div class="col-md-12">
                        <div class="row">
                            <form action="PHPExcel/GestionBienvenidasExcel" method="POST">
                                <div class="col-md-2">
                                    <label for="estadoMB" class="">Estado Mail</label>
                                    <select  style="border: none;" class="corredor border-bottom form-control text-capitalize" required name="estadoMB" id="estadoMB">
                                        <option value="" disabled selected>Seleccione </option>
                                        <option value="">Todos</option>
                                        <option value="0">No Enviado</option>
                                        <option value="1">Enviado</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for="estadoAc" class="">Estado Aceptacion </label>
                                    <select  style="border: none;" class="corredor border-bottom form-control text-capitalize" required name="estadoAc" id="estadoAc">
                                        <option value="" disabled selected>Seleccione </option>
                                        <option value="">Todos</option>
                                        <option value="0">No Aceptado</option>
                                        <option value="1">Aceptado</option>
                                    </select>
                                </div>

                                <div class="col-lg-3">
                                    <input style="border: none;" type="hidden" name="nest" value="'3','99'" >
                                    <label for="convenio" class="">Seleccione rango fecha Instalación *</label>
                                    <input style="border: none;"  class="cd-select form-control border-bottom" required  type="text" placeholder="Seleccione Rango de fecha" class="form-control" value="" name="nfecha" id="rango" autocomplete="off">
                                </div>
                                <div class="col-md-1" id="resultado1">
                                    <button class="btn btn-success" name="Descargar" id="resultado1">Descargar</button>
                                </div>
                            </form>
                            <div class="col-md-2">
                                <button class="btn btn-primary" name="nbuscar" id="nbuscar">Buscar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <input type="text" class="form-control input-sm m-b-xs" id="filter"
                    placeholder="Filtrar vehículos para bienvenida">
                    <div class="table-responsive">
                        <table class=" table  table-hover" >

                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Trabajo</th>
                                <th>Cliente(s)</th>
                                <th>Placa</th>
                                <th>Fecha</th>
                                <th>Plan</th>
                                <th>Mail Bienvenida</th>
                                <th>Estado Aceptacion</th>
                                <th>Fecha Aceptacion </th>
                                <th>Recordatorio Mail </th>
                                <th>Recordatorio SMS </th>
                                <th>Cargo</th>
                                <!--<th>Enviar Correo</th>-->
                                <th>Accion</th>
                            </tr>
                        </thead>    
                        <tbody id="dataPostVenta">
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Últimas bienvenidas realiadas</h5>

                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <input type="text" class="form-control input-sm m-b-xs filter122" id="filter1"
                            placeholder="Fitrar instalaciones">

                            <table class="footable2 table table-stripped" data-page-size="8" data-filter=#filter1>
                                <thead>
                                    <tr>
                                        <tr>
                                            <th>#</th>
                                            <th>Cliente</th>
                                            <th>Placa(s)</th>
                                            <th data-hide="all">Marca(s)</th> 
                                            <th data-hide="all">estado</th>
                                            <th>Plan</th>
                                            <th >Fecha Bienvenida</th>
                                            <th >Acciones</th>


                                            <!-- <th data-hide="all">Task</th> -->
                                        </tr>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $dataInstala=Vehiculo::obtenerInstalados(); 
                                        $n=1;
                                     ?>
                                     <?php if ($dataInstala): ?>
                                         <?php foreach ($dataInstala as $var): ?>   
                                         <tr>
                                             <td><?= $n++ ?></td>
                                             <?php $clienteObj =Cliente::obtenerDatosDeclientePorId($var->idcliente);?> 
                                             <td><?= ($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial) ; ?></td>
                                                <td><?php echo $var->placa ?></td>
                                                <td><?php echo $var->marca ?></td>
                                            <td class="center"><?php echo $var->modelo ?></td>
                                     

                                             <td><?php echo $var->plan; echo $var->campana  ?></td>
                                             <td class="center"><?php echo hace($var->fechaBienvenida) ?></td>                          
                                             <td><?php echo $var->accionBienvenida ?></td>
                                         </tr>                                          
                                         <?php endforeach ?>
                                     <?php endif ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="5">
                                            <ul class="pagination pull-right"></ul>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- modal aqui desarrollaremos   -->



            <div class="modal inmodal" id="ordenes" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content animated bounceIn">
                        <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                            <h4 class="modal-title">Post venta</h4>
                        </div>
                        <form  role="form" method="post" >

                            <div class="modal-body" style="background: #fff;padding-top: 0">
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                <button type="submit" name="guardarComoPendiente" class="btn btn-info">Guardar como pendiente</button>
                                <button type="submit" name="verificado" id="verificado" class="btn btn-primary" value="<?php echo Token::generar() ?>" >Guardar Cambios</button>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>

        </div>
        <?php include("../includes/footer.php") ?>

    </div>
</div>



<!-- Mainly scripts -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<!-- Steps -->
<script src="../assets/js/plugins/staps/jquery.steps.min.js"></script>
    <script src="../assets/bootbox/bootbox.all.js"></script>


<!-- Jquery Validate -->
<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
<!-- Toastr -->
    <script src="../assets/js/plugins/toastr/toastr.min.js"></script>
    <script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="../assets/js/jquery-confirm.min.js"></script>

    <script src="../assets/js/plugins/ladda/spin.min.js"></script>
    <script src="../assets/js/plugins/ladda/ladda.min.js"></script>
    <script src="../assets/js/plugins/ladda/ladda.jquery.min.js"></script>
    <script src="../assets/js/plugins/toastr/toastr.min.js"></script>
    <script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
    <script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
 <?php include_once('../includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>

    <script>
        $('#rango').daterangepicker({
            format: 'DD-MM-YYYY',
            // startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            // minDate: '01/01/2012',
            // maxDate: '12/31/2015',
            // dateLimit: { days: 60 },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: false,
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
                'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                'Anterior Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: '/',
            locale: {
                applyLabel: 'Seleccionar',
                cancelLabel: 'Cancelar',
                fromLabel: 'Desde',
                toLabel: 'Hasta',
                customRangeLabel: 'Seleccionar Rango',
                daysOfWeek: ['Do', 'Lun', 'Mar', 'Mi', 'Ju', 'Vi','Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                firstDay: 1
            }
        }, function(start, end, label) {
            $('#rango').focus();
        });
    </script>



    <script>
var eventos={}
function loadData() {
    var datosS =  $('#filter').val();
    var page = $('#resultado').attr("data-page");
    datosS = datosS.replace('<>/"', '');

        $.ajax({
            url: 'ajax/postVentaData',
            type: 'POST', // Send post data
            data: {page: page,datos:datosS},
            success: function (data) {
                $('#dataPostVenta').html("");
                $('#dataPostVenta').html(data);
            }
        });
}
/*
$("#filter").keyup(function() {
    var str=$("#filter").val();
    var n = str.length;
    if (n==0) {
        eventos.interval = setInterval(eventos.frejcar, 5000);
        eventos.frejcar();
    }else{
    clearInterval(eventos.interval);
    }

});*/

    $(document).ready(function(){
        loadData();

        $("#dataPostVenta").on( "click", ".pagination1 a", function (e){
            e.preventDefault();
            var datosS = $('#filter').val();
            var page = $(this).attr("data-page");
            datosS = datosS.replace('<>/"', '');
            $.ajax({
                url: 'ajax/postVentaData',
                type: 'POST', // Send post data
                data: {page: page,datos:datosS},
                success : function(data)
                {
                    $('#dataPostVenta').html(data);
                }
            });
        });

        $("#nbuscar").click(function (){
            var nfecha = $("#rango").val()
            var estMW = $("#estadoMB").val()
            var estEA = $("#estadoAc").val()

            var datosS =  $('#filter').val();
            var page = $('#dataPostVenta').attr("data-page");
            datosS = datosS.replace('<>/"', '');

            $.ajax({
                url: 'ajax/postVentaData',
                type: 'POST', // Send post data
                data: {fecha: nfecha,page: page,datos:datosS,estMW:estMW,estEA:estEA},
                success: function (data) {
                    $('#dataPostVenta').html("");
                    $('#dataPostVenta').html(data);
                }
            });
        })



        $("#filter").keypress(function(e)
        {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code==13){
                var nfecha = $("#rango").val()
                var estMW = $("#estadoMB").val()
                var estEA = $("#estadoAc").val()

                var datosS =  $('#filter').val();
                var page = $('#dataPostVenta').attr("data-page");
                datosS = datosS.replace('<>/"', '');

                $.ajax({
                    url: 'ajax/postVentaData',
                    type: 'POST', // Send post data
                    data: {fecha: nfecha,page: page,datos:datosS,estMW:estMW,estEA:estEA},
                    success: function (data) {
                        $('#dataPostVenta').html("");
                        $('#dataPostVenta').html(data);
                    }
                });
            }
        });

        $('.footable').footable();
        $('.footable2').footable();
    });
    $('#ordenes').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        $('.modal-body').html("");
        $.ajax({
            type : 'post',
            url : 'modal/postVentaDataModal', //Here you will fetch records
            // url : 'modal/operacionesDataModal', //Here you will fetch records
            data :  'rowid='+ rowid, //Pass $id
            success : function(data){
            $('.modal-body').html(data);//Show fetched data from database
        }
    });
    });


function btnSendWelcome(e){
    var rowid = $(e).data('id');
    var ndata = $(e).data('hlink');
    var nplca = $(e).data('placa');

    var l = $('.ladda-button-dlink'+rowid+'').ladda();
    l.ladda( 'start' );

    bootbox.confirm({
        title: "<h3><strong>Atención!</strong></h3>",
        message: "Esta Seguro que deseas enviar Correo de Bienvenida a:    "+ndata+"  Placa : "+nplca+ "?",
        size: 'small',
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar'
            }
        },
        callback: function (result) {
            if(result){
                $("#btnblink"+rowid).prop('disabled',false);
                $("#idlinkHomo"+rowid).prop('disabled',false);
                $("#idlOrden"+rowid).prop('disabled',false);
                $("#btnblink"+rowid).val($("#verificado").val());
                $("#FRMLINK"+rowid).submit();
                return true;
            }else{
                l.ladda('stop');
                //  $("#liberarTec").on("click",$(this)).find("#liberarTec");
            }
        }
    });
};
</script>
    </body>

    </html>
    <script>
        $(window).load(function() {
            $(".se-pre-con").fadeOut("slow");;
        });
    </script>

<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>

 <?php if ($_POST['rowid']): ?>  
 
 <?php 
 require "../../includes/init.php";

$df=DatosFacturacion::obtenerDatosFacturacionPorId(limpiar($_POST['rowid']));
$clienteObjO=Onix::obtenerDatosDesdeOrden($df->orden);

$operacionesObj=Operacion::obtenereOperacionesPorIdFacturacion(limpiar($_POST['rowid']));

 $vehiculoObj=Vehiculo::obtenerVehiculo($df->idvehiculos);
  $clienteObj=Cliente::obtenerClientePorVehiculo($df->idvehiculos,$vehiculoObj->idcliente);
 if($vehiculoObj->estado==3 OR $vehiculoObj->estado==99){
  ?>
   <?php  if (!$vehiculoObj->asignarCargoDeBienvenida()) {
      die("error en asginar cargo");
  } ?>

<div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->dni ?>">       
      <?php else: ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->ruc ?>">        
      <?php endif ?>  
    </div>
    <div class="col-md-9">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->razonSocial ?>">       
      <?php endif ?>
    </div>   
  </div>
<div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">Telefono</label>
        <input style="border: none;" type="text" placeholder="telefono" id="dniOruc"
        name="dni" class="border-bottom form-control x2 text-uppercase" required 
        value="<?php echo $clienteObjO->celular ?> | <?= $clienteObjO->telefono ?>">       
    </div>
    <div class="col-md-3 form-group"><label for="dniOruc">Telefono</label>
        <input style="border: none;" type="text" placeholder="telefono" id="dniOruc"
        name="dni" class="border-bottom form-control x2 text-uppercase" required
        value="<?php echo $clienteObjO->celular2 ?> | <?= $clienteObjO->telefonoTrabajo ?>">       
    </div>
    <div class="col-md-6 form-group"><label for="dniOruc">Correos</label>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc"
        name="dni" class="border-bottom form-control x2 text-uppercase" required
        value="<?= $clienteObjO->correo ?><?= ($clienteObjO->correo!='')?','.$clienteObjO->correo2:$clienteObjO->correo2 ?>">       
    </div>

       
  </div>
<hr>
<!-- <div class="row">
  <?php $ecomercal=Usuario::obtenerUsuario($vehiculoObj->idcreador) ?>
  <?php $ecomercalO=Usuario::obtenerUsuario($vehiculoObj->idRegistradorEnOnix) ?>
  <div class="form-group col-md-6">Ejecutivo comercial:<?=  $ecomercal->usuarioNombre." ".$ecomercal->usuarioApellidoPaterno; ?></div>
  <div class="form-group col-md-6">Ejecutivo de Operaciones Comerciales:<?=  $ecomercalO->usuarioNombre." ".$ecomercalO->usuarioApellidoPaterno; ?></div>

</div> -->
<div class="row">
  <div class="col-md-12"><label for="" class="text-center">Camapaña: <?= $vehiculoObj->campana ?></label></div>
</div>
<?php if (!empty($vehiculoObj->observaciones)): ?>
<div class="row">
  <div class="col-md-12"><h3 class="text-danger">Observaciones cem: <?= $vehiculoObj->observaciones ?></h3></div>
</div>
  
<?php endif ?>

  <div class="row">
    <div class="col-lg-12 field_wrapper"> 
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="field_name[0][]" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $vehiculoObj->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="">Chasis | Motor</label>
          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" class="border-bottom form-control text-uppercase" required
          value="<?php echo $vehiculoObj->chasis ?> | <?php echo $vehiculoObj->motor ?>">
        </div>
        <div class="form-group col-md-6">
          <label for="motor" class="">Marca | Modelo |Color | Año</label>
          <input style="border: none;" type="text" placeholder="Motor" id="motor"
           class="border-bottom form-control text-uppercase"
          required value="<?php echo $vehiculoObj->marca ?> | <?php echo $vehiculoObj->modelo ?> | <?php echo $vehiculoObj->color ?> | <?php echo $vehiculoObj->anio ?>" >
        </div>
      </div> 
    </div>
  </div> 
  <div class="row">
    <div class="form-group col-md-2">
      <label for="campana" class="">Campaña</label>
      <input style="border: none;" type="text" placeholder="Año" id="campana" name="campana" class="border-bottom form-control x2 " required value="<?php echo $vehiculoObj->campana ?>">
    </div>
      <div class="form-group col-md-2">
          <label for="campana" class="">N° Orden de Contrato</label>
          <input style="border: none;" type="text" placeholder="Año" id="nordcont" name="nordcont" class="border-bottom form-control x2 " readonly required value="<?php echo $operacionesObj->ordenContrato ?>">
      </div>
    <div class="col-md-08">
    <h3 class="text-center">Creado por <?php  $o=Usuario::obtenerUsuario($vehiculoObj->idcreador);  ?><?= $o->usuarioNombre." ".$o->usuarioApellidoPaterno;  ?>,  estado de Vehículo <?= obtenerEstado($vehiculoObj->estado) ?> </h3>
    </div>
  </div>
<div class="row">
<div class="form-group col-md-3">
   <label for="plan" class="">Plan </label>
   <select  style="border: none;" class="tecnico border-bottom form-control" required name="plan" id="plan">
      <option class="" value="<?php echo $vehiculoObj->plan ?>"  selected ><?php echo $vehiculoObj->plan ?></option>      
  </select> 
</div>
<div class="form-group col-md-1">
   <label for="plan" class="">Tiempo </label>
   <input style="border: none;" type="text" placeholder="Corredor de seguros." id="corredor" readonly="" class="border-bottom form-control text-capitalize"   value="<?= $df->tiempo[0] ?>">
</div>
<div class="form-group col-md-2">
   <label for="plan" class="">Precio </label>
   <input style="border: none;" type="text" placeholder="Corredor de seguros." id="corredor" readonly="" class="border-bottom form-control text-capitalize"   value="<?= $df->costo ?>">
</div>
<div class="form-group col-md-3">
   <label for="plan" class="">Numero orden facturación</label>
   <input style="border: none;" type="text" placeholder="Corredor de seguros." id="corredor" readonly="" class="border-bottom form-control text-capitalize"   value="<?= $df->orden ?>">
</div>
<div class="form-group col-md-3">
   <label for="plan" class="">Estado Facturación</label><br>
  <?= obtenerEstado($df->estadoFacturacion) ?>
</div>
</div>
<div class="row well">
        <div class="form-group col-md-4" >
            <label for="Observaciones"  > OBSERVACIONES: <?= $clienteObjO->nombre." ".$clienteObjO->apellidos ?> </label>
            <div style="border-right: solid;height: 90px;
overflow-y: auto;">
            <label class="border-" ><?php echo utf8_encode($clienteObjO->comentarios) ?></label>
            </div>
        </div>
        <div class="col-md-8 form-group"  >

        <label for="">ORDENES <?= $clienteObjO->placa ?> |<?= $clienteObjO->chasis ?></label>
    <div style="height: 100px; overflow-y: auto;">
            <?php $objOrdenes=Onix::obtenerOrdenesVehiculo($clienteObjO->idcustomer,$clienteObjO->idvehicle,$clienteObjO->idlocation) ?>
            <?php foreach ($objOrdenes as $var): ?>
                    <?= $orden=$var->idlocation."-".$var->idorder ?> <?= utf8_encode($var->articulo) ?><br>
                    <?php   $DataOrd=Onix::obtenerDatosFacturacion($orden) ?>
                    <?php   $DataOrt=Onix::obtenerORTE($orden) ?>
                    <?= ($DataOrd)?$DataOrd->serie.$DataOrd->numeroFactura."  $".round($DataOrd->precioO*1.18,2):""; ?>
                    <?= ($DataOrt->orteO!='')?"<b>ORTE ".$DataOrt->orteO."</b> ":'' ?> <small><?= haceMinimoAnio($var->fechaOrden) ?></small>
                    <hr>
            <?php endforeach ?>
            </div>
        </div>
    </div>
  <hr>
  <h2 class="text-center text-info">Fecha instalación <b><?= $operacionesObj->fechaInstalacion ?></b></h2>
  <h2 class="text-center text-info">Lugar <b><?= $operacionesObj->lugarInstalacion ?></b></h2>
  <input type="hidden" name="idv" value="<?php echo $vehiculoObj->idvehiculo ?>" >

  <div class="row">
    <div class="col-md-12">

        <?php $filess=DatosFacturacion::obtenerFiles($df->datosfacturacionid) ?>
        <?php if($filess):  ?>
        <table class="table table-hover">
        <?php foreach($filess as $file): ?>
            <tr>
                <td><?= $file->fecha ?></td>
                <td><?= $file->oficina ?>&nbsp;&nbsp;<span class="fa fa-file-pdf-o"></span></td>
                <td><?= $file->tipoCanal ?></td>
                <td><?= $file->tipoCanal ?>&nbsp;
                    <a
                    href="/ope/downinvdoc?tnk=fef56f6a7c51ab802&b=<?= $file->voucher ?>&f=<?= $file->apunte ?>&fn=<?= strtoupper($file->oficina) ?> <?= trim($clienteObjO->apellidos." ".$clienteObjO->nombre)." ".$clienteObjO->placa ?>"
                    target="_blank"><span class="fa fa-download"></span></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </table>

            <label for="" class="fa fa-danger">Generar otro contrato demora al menos de 10 a 20 segundos</label>
            <a class="btn btn-primary" href="/ope/downinvdoc?tnk=eef56f6a7c51ab800&o=<?= $operacionesObj->ordenContrato ?>&d=<?=$df->datosfacturacionid ?>" title="Generar">
                Generar otro contrato ->
                    <span class="fa fa-file"><span><i class="fa fa-plus"></i></span>
                </a>
        <?php else: ?>
            <?php if(!is_null($operacionesObj->ordenContrato)){ ?>
            <a class="btn btn-primary" href="/ope/downinvdoc?tnk=eef56f6a7c51ab800&o=<?= $operacionesObj->ordenContrato ?>&d=<?=$df->datosfacturacionid ?>" title="Generar">
                    <span class="fa fa-file"><span><i class="fa fa-plus"></i></span>
                </a>
            <?php }  ?>
        <?php endif; ?>
    </div>
    </div>


  <div class="row">
    <div class="col-md-12">
      <fieldset>
        <h3>
          Documentos enviados
        </h3>

        <div class="checkbox checkbox-primary">
          <input id="checkbox2" type="checkbox"  name="bienvenida" value="si">
          <label for="checkbox2">
            Bienvenida
          </label>
        </div>
        <div class="checkbox checkbox-success">
          <input id="checkbox3" type="checkbox" name="usuarioContrasenia" value="si">
          <label for="checkbox3">
            Usuario y Contraseña
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox4" type="checkbox" name="contrato" value="si">
          <label for="checkbox4">
            Contrato
          </label>
        </div>
        <div class="checkbox checkbox-warning">
          <input id="checkbox5" type="checkbox" checked name="estado" value="no">
          <label for="checkbox5">
            No ubicado
          </label>
        </div>
      </fieldset>
    </div>

  </div>
  <div class="row">          
    <div class="form-group col-md-12">
      <label for="accionBienvenida" class="">Observaciones de bienvenida</label>
      <input style="border: none;" type="text" placeholder="Acciones de bienvenida" id="accionBienvenida" name="accionBienvenida" class="border-bottom form-control" required value="<?php echo $vehiculoObj->accionBienvenida ?>">
    </div>         
  </div>
  <?php
}
?>
<?php endif ?>

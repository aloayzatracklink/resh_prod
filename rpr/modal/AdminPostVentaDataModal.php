 <?php if ($_POST['rowid']): ?>  
 
 <?php 
 require "../../includes/init.php";

$df=DatosFacturacion::obtenerDatosFacturacionPorId(limpiar($_POST['rowid']));
$clienteObjO=Onix::obtenerDatosDesdeOrden($df->orden);
$operacionesObj=Operacion::obtenereOperacionesPorIdFacturacion(limpiar($_POST['rowid']));

 $vehiculoObj=Vehiculo::obtenerVehiculo($df->idvehiculos);
  $clienteObj=Cliente::obtenerClientePorVehiculo($df->idvehiculos,$vehiculoObj->idcliente);
 if($vehiculoObj->estado==4){
  ?>
<div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <input style="border: none;" type="text" readonly placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->dni ?>">
      <?php else: ?>
        <input style="border: none;" type="text" readonly placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->ruc ?>">
      <?php endif ?>  
    </div>
    <div class="col-md-9">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text"  readonly placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" readonly placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" readonly placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->razonSocial ?>">
      <?php endif ?>
    </div>   
  </div>
<div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">Telefono</label>
        <input style="border: none;" type="text" readonly placeholder="telefono" id="dniOruc"
        name="dni" class="border-bottom form-control x2 text-uppercase" required 
        value="<?php echo $clienteObjO->celular ?> | <?= $clienteObjO->telefono ?>">       
    </div>
    <div class="col-md-3 form-group"><label for="dniOruc">Telefono</label>
        <input style="border: none;" type="text" readonly placeholder="telefono" id="dniOruc"
        name="dni" class="border-bottom form-control x2 text-uppercase" required
        value="<?php echo $clienteObjO->celular2 ?> | <?= $clienteObjO->telefonoTrabajo ?>">       
    </div>
    <div class="col-md-6 form-group"><label for="dniOruc">Correos</label>
        <input style="border: none;" type="text" readonly placeholder="Placa" id="dniOruc"
        name="dni" class="border-bottom form-control x2 text-uppercase" required
        value="<?= $clienteObjO->correo ?><?= ($clienteObjO->correo!='')?','.$clienteObjO->correo2:$clienteObjO->correo2 ?>">       
    </div>

       
  </div>
<hr>
<!-- <div class="row">
  <?php $ecomercal=Usuario::obtenerUsuario($vehiculoObj->idcreador) ?>
  <?php $ecomercalO=Usuario::obtenerUsuario($vehiculoObj->idRegistradorEnOnix) ?>
  <div class="form-group col-md-6">Ejecutivo comercial:<?=  $ecomercal->usuarioNombre." ".$ecomercal->usuarioApellidoPaterno; ?></div>
  <div class="form-group col-md-6">Ejecutivo de Operaciones Comerciales:<?=  $ecomercalO->usuarioNombre." ".$ecomercalO->usuarioApellidoPaterno; ?></div>

</div> -->
<div class="row">
  <div class="col-md-12"><label for="" class="text-center">Camapaña: <?= $vehiculoObj->campana ?></label></div>
</div>
<?php if (!empty($vehiculoObj->observaciones)): ?>
<div class="row">
  <div class="col-md-12"><h3 class="text-danger">Observaciones cem: <?= $vehiculoObj->observaciones ?></h3></div>
</div>
  
<?php endif ?>

  <div class="row">
    <div class="col-lg-12 field_wrapper"> 
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" readonly placeholder="Placa" id="placa" name="field_name[0][]" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $vehiculoObj->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="">Chasis | Motor</label>
          <input style="border: none;" type="text" readonly placeholder="Chasis" id="chasis" class="border-bottom form-control text-uppercase" required
          value="<?php echo $vehiculoObj->chasis ?> | <?php echo $vehiculoObj->motor ?>">
        </div>
        <div class="form-group col-md-6">
          <label for="motor" class="">Marca | Modelo |Color | Año</label>
          <input style="border: none;" type="text" readonly placeholder="Motor" id="motor"
           class="border-bottom form-control text-uppercase"
          required value="<?php echo $vehiculoObj->marca ?> | <?php echo $vehiculoObj->modelo ?> | <?php echo $vehiculoObj->color ?> | <?php echo $vehiculoObj->anio ?>" >
        </div>
      </div> 
    </div>
  </div> 
  <div class="row">
    <div class="form-group col-md-2">
      <label for="campana" class="">Campaña</label>
      <input style="border: none;" type="text" readonly placeholder="Año" id="campana" name="campana" class="border-bottom form-control x2 " required value="<?php echo $vehiculoObj->campana ?>">
    </div>

      <div class="form-group col-md-2">
          <label for="plan" class=""> Orden Contrato</label>
          <input style="border: none;" type="text" placeholder="Orden contrato" id="nordn"  name="nordn" class="border-bottom form-control text-capitalize"   value="<?= $operacionesObj->ordenContrato ?>">
      </div>

      <div class="form-group col-md-2">
          <label for="plan" class="">Estado</label>
          <select  style="border: none;"   class="tecnico border-bottom form-control" required name="estVh" id="estVh">
              <option class="" value="4" <?php echo $vehiculoObj->estado=="4"?'selected':'' ?> >Guardado</option>
              <option class="" value="3" <?php echo $vehiculoObj->plan=="3"?'selected':'' ?>" >Habilitar</option>
          </select>
      </div>
    <div class="col-md-8">
    <h3 class="text-center">Creado por <?php  $o=Usuario::obtenerUsuario($vehiculoObj->idcreador);  ?><?= $o->usuarioNombre." ".$o->usuarioApellidoPaterno;  ?>,  estado de Vehículo <?= obtenerEstado($vehiculoObj->estado) ?> </h3>
    </div>
  </div>
<div class="row">
<div class="form-group col-md-3">
   <label for="plan" class="">Plan </label>
   <select  style="border: none;" readonly=""  class="tecnico border-bottom form-control" required name="plan" id="plan">
      <option class="" value="<?php echo $vehiculoObj->plan ?>"  selected ><?php echo $vehiculoObj->plan ?></option>      
  </select> 
</div>
<div class="form-group col-md-1">
   <label for="plan" class="">Tiempo </label>
   <input style="border: none;"  type="text" placeholder="Corredor de seguros." id="corredor" readonly="" class="border-bottom form-control text-capitalize"   value="<?= $df->tiempo[0] ?>">
</div>
<div class="form-group col-md-2">
   <label for="plan" class="">Precio </label>
   <input style="border: none;" type="text" placeholder="Corredor de seguros." id="corredor" readonly="" class="border-bottom form-control text-capitalize"   value="<?= $df->costo ?>">
</div>
<div class="form-group col-md-3">
   <label for="plan" class="">Numero orden facturación</label>
   <input style="border: none;" type="text" placeholder="Corredor de seguros." id="corredor" readonly="" class="border-bottom form-control text-capitalize"   value="<?= $df->orden ?>">
</div>
<div class="form-group col-md-3">
   <label for="plan" class="">Estado Facturación</label><br>
  <?= obtenerEstado($df->estadoFacturacion) ?>
</div>
</div>
<div class="row well">
        <div class="form-group col-md-4" >
            <label for="Observaciones"  > OBSERVACIONES: <?= $clienteObjO->nombre." ".$clienteObjO->apellidos ?> </label>
            <div style="border-right: solid;height: 90px;
overflow-y: auto;">
            <label class="border-" ><?php echo utf8_encode($clienteObjO->comentarios) ?></label>
            </div>
        </div>
        <div class="col-md-8 form-group"  >

        <label for="">ORDENES <?= $clienteObjO->placa ?> |<?= $clienteObjO->chasis ?></label>
    <div style="height: 100px; overflow-y: auto;">
            <?php $objOrdenes=Onix::obtenerOrdenesVehiculo($clienteObjO->idcustomer,$clienteObjO->idvehicle,$clienteObjO->idlocation) ?>
            <?php foreach ($objOrdenes as $var): ?>
                    <?= $orden=$var->idlocation."-".$var->idorder ?> <?= utf8_encode($var->articulo) ?><br>
                    <?php   $DataOrd=Onix::obtenerDatosFacturacion($orden) ?>
                    <?php   $DataOrt=Onix::obtenerORTE($orden) ?>
                    <?= ($DataOrd)?$DataOrd->serie.$DataOrd->numeroFactura."  $".round($DataOrd->precioO*1.18,2):""; ?>
                    <?= ($DataOrt->orteO!='')?"<b>ORTE ".$DataOrt->orteO."</b> ":'' ?> <small><?= haceMinimoAnio($var->fechaOrden) ?></small>
                    <hr>
            <?php endforeach ?>
            </div>
        </div>
    </div>
  <hr>
  <h2 class="text-center text-info">Fecha instalación <b><?= $operacionesObj->fechaInstalacion ?></b></h2>
  <h2 class="text-center text-info">Lugar <b><?= $operacionesObj->lugarInstalacion ?></b></h2>
  <input type="hidden" name="idv" value="<?php echo $vehiculoObj->idvehiculo ?>" >
     <input type="hidden" name="idopera" value="<?php echo $operacionesObj->idoperacion ?>" >

  <div class="row">
    <div class="col-md-12">

        <?php $filess=DatosFacturacion::obtenerFiles($df->datosfacturacionid) ?>
        <?php if($filess):  ?>
        <table class="table table-hover">
        <?php foreach($filess as $file): ?>
            <tr>
                <td><?= $file->fecha ?></td>
                <td><?= $file->oficina ?>&nbsp;&nbsp;<span class="fa fa-file-pdf-o"></span></td>
                <td><?= $file->tipoCanal ?></td>
                <td><?= $file->tipoCanal ?>&nbsp;
                    <a
                    href="/ope/downinvdoc?tnk=fef56f6a7c51ab802&b=<?= $file->voucher ?>&f=<?= $file->apunte ?>&fn=<?= strtoupper($file->oficina) ?> <?= trim($clienteObjO->apellidos." ".$clienteObjO->nombre)." ".$clienteObjO->placa ?>"
                    target="_blank"><span class="fa fa-download"></span></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </table>


        <?php else: ?>

        <?php endif; ?>
    </div>
    </div>


  <?php
}
?>
<?php endif ?>


<link href="../../assets/css/plugins/summernote/summernote.css" rel="stylesheet">
<?php if (isset($_POST['chasis'])): ?>
<?php 
	include_once("../../includes/init.php");
	$chasis=htmlspecialchars(htmlentities(limpiar($_POST['chasis'])));		
	$clienteObj=Onix::ObtenerDatosDesdeChasis($chasis);
	$vencimientoDatos=Onix::obtenerFechaDeVencimiento($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation);	
	$tiempo="";
	if ($clienteObj) {
		$cantidadObjeto=Onix::obtenerCantididadVehiculos($clienteObj->idcustomer,$clienteObj->idlocation);
	}
	?>
	<div class="well" style="padding-top: 11px;
	padding-bottom: 10px;">
	<div class="row">
		<div class="row">
		<h2 class="text-center"><?php 
                        if($clienteObj){  
                        	echo vence($vencimientoDatos->hasta);
                        }else{
                            echo "No esta en la base de datos consulta este mensaje con pvasquez@tracklink.pe";
                        }
                        ?></h2>

                        <?php if ($clienteObj): ?>
                        	
                        <?php if ($cantidadObjeto): ?>
                        	<?php if ($cantidadObjeto->cantidad>1): ?>
                        	<h4 class="text-center text-warning">ATENCION ! Cliente tiene <?php echo $cantidadObjeto->cantidad ?> Vehiculos en Tracklink</h4>	
                        	<?php endif ?>
                        <?php endif ?>
	<input type="hidden" name="vencidoHace" value="<?php echo $tiempo ?>">
	<input type="hidden" name="registrarDesdeChasis" value="<?php echo $tiempo ?>">
			<div class="form-group col-md-3">
				<label for="Observaciones" > Usuario Web</label>
				<input name="usuarioWeb" style="border: none;" type="text" placeholder="Web" id="Observaciones" <?php echo $retVal = (!empty($clienteObj->dni)) ? "readonly" : "" ; ?> class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>">
			</div>
			<div class="form-group col-md-5">
				<label for="numeroTecnico" >Nombre de cliente*</label>
				<input  style="border: none;" type="text" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$clienteObj->apellidos" : "$clienteObj->apellidos" ; ?> ">
			</div>
			<div class="col-md-2"><label for="estado" >Celular</label>
				<input style="border: none;" type="text" <?php echo $retVal = (!empty($clienteObj->celular)) ? "readonly" : "" ; ?> id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">
			</div>
			<div class="col-md-2"><label for="estado" >Celular</label>
				<input style="border: none;" type="text" readonly id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular2)) ? "$clienteObj->celular2" : "" ; ?>">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-6">
				<label for="telefono" >Telefono </label>
				<input style="border: none;" type="text" readonly id="telefono"  class="border-bottom form-control" name="telefono" value="<?php echo $retVal = (!empty($clienteObj->telefonoTrabajo)) ? "$clienteObj->telefonoTrabajo" : "" ; ?>">
			</div>
			<div class="form-group col-md-6">
				<label for="telefono" >Telefono </label>
				<input style="border: none;" type="text" readonly id="telefono"  class="border-bottom form-control" name="telefono" value="<?php echo $retVal = (!empty($clienteObj->telefono)) ? "$clienteObj->telefono" : "" ; ?>">
			</div>
			
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label for="Observaciones"  >Correo cliente. </label>
				<input style="border: none;" type="text" placeholder="Correo" id="Observaciones"  class="border-bottom form-control" name="correoCliente" value="<?php echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo," : "$clienteObj->correo" ;echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo2" : "" ;  ?>">
			</div>
		</div>

		<div class="row">
			<div class="form-group col-md-12">
				<label for="Observaciones"  >Observaciones Onyx </label>
				<div style="border-right: solid;height: 50px;
    overflow-y: auto;">
				<label class="border-" ><?php echo utf8_encode($clienteObj->comentarios) ?></label>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>

<hr>
<div class="row">
	<div class="form-group col-md-12">
		<label for="Mensaje Personalizado">Mensaje personalizado.</label>
<textarea  style="border: none;" type="text" id="Mensaje Personalizado" name="msjPersonalizado" class="summernote border-bottom form-control "  >


</textarea>
	</div>
</div>
<hr>

<label for="">Vehículos</label>
<div class="row">
    <div class="col-md-3">
		<label for="idchasis">Chasis</label>
		<input  style="border: none;" type="text" placeholder="Chasis" id="chasis" class="border-bottom form-control" >
	</div>
	<div class="col-md-3">
		<label for="plate">Placa</label>
		<input  style="border: none;" id='plate' type="text" placeholder="Placa" class="border-bottom form-control" >
	</div>
    <div class="col-md-3">
		<label for="brand">Marca</label>
		<input  style="border: none;" id='brand' type="text" placeholder="Marca" class="border-bottom form-control" >
	</div>
    <div class="col-md-3">
		<label for="model">Modelo</label>
		<input  style="border: none;" id='model' type="text" placeholder="Modelo" class="border-bottom form-control" >
	</div>
    
</div>
<div class="row">
    <div class="col-md-3">
		<label for="article">Vence</label>
		<input  style="border: none;" id='vence' type="text" placeholder="Vence" class="border-bottom form-control" >
	</div>
	<div class="col-md-3">
		<label for="article" class="text-danger">Articulo</label>
		<input  style="border: none;" id='article' type="text" placeholder="Articulo" class="border-bottom form-control" >
	</div>
    <div class="col-md-3">
		<label for="price" class="text-danger">Precio</label>
		<input  style="border: none;" type="text" placeholder="Precio" id="price"  class="border-bottom form-control" >
	</div>
    <div class="col-md-3 text-rigth">
<br>
        <button class="btn btn-info btn-block" id='agregarVehiculo' type="button">Agregar</button>
	</div>
</div>
<h3>Descuentos</h3>
<div class="row">
    <div class="col-md-6">
		<label for="article">Descripción descuento</label>
		<input  style="border: none;" id='discountName' type="text" placeholder="Ejem: Prom especial 10%" class="border-bottom form-control" >
	</div>
	<div class="col-md-3">
		<label for="article">Monto</label>
		<input  style="border: none;" id='amountDiscount' type="text" placeholder="Monto" class="border-bottom form-control" >
	</div>
    
    <div class="col-md-3 text-rigth">
<br>
        <button class="btn btn-secondary btn-block" id="agregarDescuento" type="button">Agregar</button>
	</div>
</div>
<hr>
<table class="table">
<thead>
    <tr >
        <th>Articulo</th>
        <th>Precio</th>
    </tr>
</thead>
<tbody id="valuest">
    
</tbody>
<tfoot>
<tr id="discount">
</tr>
<tr  id="totalv">
</tr>
</tfoot>
</table>
<hr>
<h4>Datos de facturación</h4>
<div class="row siRenovado">
<div class="col-md-4 form-group">
	<select style="border: none;" id="selectNa" name="tipoDocumentoFacturacion" class="cd-selec form-control border-bottom" required >
		<option value="" disabled selected>Datos de facturación*</option>
		<option value="Boleta">Boleta</option>
		<option value="Factura">Factura</option>
	</select>   
</div>
<div class="col-md-8 form-group">
	<div class="content-pane eFactura">
		<div class="row"><br>
			<div class="form-group col-md-4">
				<label for="rucFacturacion" class="">Ruc *</label>
				<input style="border: none;" type="number" placeholder="Ruc *" id="rucFacturacion" name="rucFacturacion" class="form-control x2 border-bottom" maxlength="12" minlength="10" value="<?= $retVal = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>"  >
			</div>
			<?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
			<div class="form-group col-md-8">
				<label for="razonSocialFacturacion" class="">Razón Social *</label>
				<input style="border: none;" type="text" placeholder="Razón Social *" id="razonSocialFacturacion" name="razonSocialFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ; ?>" >
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-8">
				<label for="direccionFiscalFacturacion" class="sr-only">Dirección Fiscal *</label>
				<input style="border: none;" type="text" placeholder="Dirección Fiscal *" id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="form-control border-bottom text-capitalize" value="<?= utf8_encode($clienteObj->direccion) ?>">

			</div>
			<div class="form-group col-md-4">
				<label for="telefonoFacturacion" class="sr-only">Teléfono</label>
				<input style="border: none;" type="text" placeholder="Teléfono" id="telefonoFacturacion" name="telefonoFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">

			</div>
		</div>      
	</div> 
</div>  
</div>

<div class="row">
	<div class="col-md-12">
		<label for="Observaciones" >Observaciones *</label>
		<input  style="border: none;" type="text" placeholder="Observaciones *" id="Observaciones" name="Observaciones" class="border-bottom form-control" required >
	</div>
</div>



<?php if (isset($clienteObj->articulo)): ?>
	<input type="hidden" name="articulo" value="<?php echo $clienteObj->articulo ?>" >
<?php endif ?>
<?php else: ?>
<?php endif ?>
	
<?php endif ?>

<script src="../../assets/js/plugins/summernote/summernote.min.js"></script>
<script src="../../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script>

$('.summernote').summernote({
	placeholder:'Mensaje personalizado',
	tabsize:2,
	height:100
});


function getTable(){
    var myHtml = '';
    for (var i = 0; i < vehicles.length; i++) {
       myHtml = myHtml + "<tr onclick='myFunction("+i+")'><td>"+vehicles[i].name+"<br><small>"+vehicles[i].description+"</small></td>"+"<td>"+vehicles[i].price+"</td></tr>"
    }
    return myHtml;
}

var vehicles = []

function myFunction(i){

    vehicles.splice(i,1)

    if($("#discountName").val()!=='' && $("#amountDiscount").val()!==''){
        cotization.nameDiscount = $("#discountName").val()   
        cotization.discount = Number($("#amountDiscount").val())

       $("#valuest").html(getTable(vehicles));

        var total = 0;
        $.each(vehicles,function() {
            total += Number(this.price)
        });
        
       $("#discount").html("<th>"+cotization.nameDiscount+"</th><th> - "+cotization.discount+"</th>");

       $("#totalv").html("<th>Total USD $:</th><th>"+Number(total-cotization.discount)+"</th>");
   }else{
       $("#valuest").html(getTable(vehicles));

        var total = 0;
        $.each(vehicles,function() {
            total += Number(this.price)
        });
        if(total>0){
           $("#totalv").html("<th>Total USD $:</th><th>"+total+"</th>");
        }else{
           $("#totalv").html("");
        }
    }
}

$(document).ready(function(){

var cotization = {}

// cotization.billingDetail = {"billingType":"Boleta"}
// {
//     "billingDetail": {"billingType":"Boleta"},
//     "paymentRequest": {
//         "idCustomer":"000123", "idLocation":"11", "lastName":"Daniela",
//         "productPaymentRequests": [{"idArticle":"TRACKINT","name":"Revación Vence 2020/02/03"},{"idArticle":"OTHER"}]
//         
//     }
//     
// }

//cotization.paymentRequest.productPaymentRequests = []


$("#agregarVehiculo").click(function () {

    vehicles.push({"name":$("#article").val(),
        "price":$("#price").val(),
        "description": ($("#brand").val()===''?"":$("#brand").val())  + (($("#model").val()===''?'':" | " + $("#model").val() )) + ($("#plate").val()===''?'':' | '+$("#plate").val())
        }
    )

   $("#valuest").html(getTable(vehicles));
    var total = 0;
    $.each(vehicles,function() {
        total += Number(this.price)
    });

   $("#totalv").html("<th>Total USD $:</th><th>"+total+"</th>");


})

$("#agregarDescuento").click(function () {
    if($("#discountName").val()!=='' && $("#amountDiscount").val()!==''){
        cotization.nameDiscount = $("#discountName").val()   
        cotization.discount = Number($("#amountDiscount").val())

       $("#valuest").html(getTable(vehicles));

        var total = 0;
        $.each(vehicles,function() {
            total += Number(this.price)
        });
        
       $("#discount").html("<th>"+cotization.nameDiscount+"</th><th> - "+cotization.discount+"</th>");

       $("#totalv").html("<th>Total USD $:</th><th>"+Number(total-cotization.discount)+"</th>");
   }

});

$('#selectNa').on("change",function () {
if ($(this).val()=="Boleta") {
    $('.eBoleta').show(1000);
    $('.eFactura').hide(1000);
    $('#rucFacturacion').prop('required',false);
    $('#razonSocialFacturacion').prop('required',false);
    $('#direccionFiscalFacturacion').prop('required',false);
}
if($(this).val()=="Factura"){
    $('.eBoleta').hide(1000);
    $('.eFactura').show(1000);
    ruc=$('#rucFacturacion').val();
	if (ruc.length==11) {

	}else{
		alert("El ruc debe contener 11 caracteres actualmente tiene: "+ruc.length );
	}
    $('#rucFacturacion').prop('required',true);
    $('#razonSocialFacturacion').prop('required',true);
    $('#direccionFiscalFacturacion').prop('required',true);

}
  });
});

</script>

<link href="../../assets/css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="../../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
<?php
if (isset($_POST['rowid'])) {
	include_once("../../includes/init.php");
	$idRenovacion=htmlspecialchars(htmlentities($_POST['rowid']));	
	$objetoRenovacion=Renovaciones::obtenerDatosDeRenovacionPorId($idRenovacion);
	$clienteObj=Onix::ObtenerDatosDesdeChasis($objetoRenovacion->chasis);
	$vencimientoDatos=Onix::obtenerFechaDeVencimiento($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation);	
	$tiempo="";
	if ($clienteObj) {
		$cantidadObjeto=Onix::obtenerCantididadVehiculos($clienteObj->idcustomer, $clienteObj->idlocation);
	}
	?>
<div class="well" style="padding-top: 11px;
padding-bottom: 10px;">
<div class="row">
	<div class="row">
<h2 class="text-center">
<?php 
if($clienteObj){  
    #echo vence($vencimientoDatos->hasta);
}else{
    echo "No esta en la base de datos consulta este mensaje con pvasquez@tracklink.pe";
}
?></h2>
<?php if ($clienteObj): ?>
<?php if ($cantidadObjeto): ?>
    <?php if ($cantidadObjeto->cantidad>1): ?>
    <h4 class="text-center text-warning">ATENCION ! Cliente tiene <?php echo $cantidadObjeto->cantidad ?> Vehiculos en Tracklink</h4>	
    <?php endif ?>
<?php endif ?>

	<input type="hidden" name="idRenovacion" value="<?php echo $idRenovacion ?>">
	<input type="hidden" name="actualizarDatosDerenovacion" value="<?php echo $idRenovacion ?>">
	<input type="hidden" name="vencidoHace" value="<?php echo $tiempo ?>">

			<div class="form-group col-md-3">
				<label for="Observaciones" >DNI /RUC </label>
				<input name="usuarioWeb" style="border: none;" type="text" placeholder="Web" id="Observaciones" <?php echo $retVal = (!empty($clienteObj->dni)) ? "readonly" : "" ; ?> class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>">
			</div>
			<div class="form-group col-md-5">
				<label for="numeroTecnico" >CLIENTE</label>
				<input  style="border: none;" type="text" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->apellidos"." "."$clienteObj->nombre" : "$clienteObj->apellidos" ; ?> ">
			</div>
			<div class="col-md-4"><label for="estado" >Celular </label>
				<input style="border: none;" type="text" <?php echo $retVal = (!empty($clienteObj->celular)) ? "readonly" : "" ; ?> id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>|<?= $clienteObj->celular2 ?>">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-4">
				<label for="telefono" >Telefono </label>
				<input style="border: none;" type="text" readonly id="telefono"  class="border-bottom form-control" name="telefono" value="<?php echo $retVal = (!empty($clienteObj->telefonoTrabajo)) ? "$clienteObj->telefonoTrabajo" : "" ; ?> | <?= $clienteObj->telefono?>">
			</div>
			<div class="form-group col-md-8" >
				<label for="Observaciones">Observaciones Onyx </label>
				<div style="border-right: solid;height: 50px;
    overflow-y: auto;">
				<label class="border-" ><?php echo utf8_encode($clienteObj->comentarios) ?></label>
				</div>
			</div>
            
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label for="Observaciones"  >CORREO</label>
				<input style="border: none;" type="text" placeholder="Correo" id="Observaciones"  class="border-bottom form-control" name="correoCliente" value="<?php echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo," : "$clienteObj->correo" ;echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo2" : "" ;  ?>">
			</div>
		</div>
    <div class="row">
        <div class="form-group col-md-5">
            <div class="checkbox checkbox-success disabled <?php  if( $objetoRenovacion->preferente== 1){ echo 'checked';} ?>">
                <input id="cpreferente" type="checkbox" disabled <?php  if($objetoRenovacion->preferente== 1){ echo 'checked';} ?>  name="cpreferente" value="si">
                <label for="cpreferente">
                    <strong>Cliente Preferente</strong>
                </label>
            </div>
        </div>
    </div>

				<?php 
				if (!empty($vencimientoDatos->hasta)) {					
					$vencimientoDatos->hasta=date("d-m-Y",strtotime($vencimientoDatos->hasta));
				} 
				?>

				<input type="hidden" name="vence" value="<?php echo $vencimientoDatos->hasta ?>">
				<input type="hidden" name="anio" value="<?php echo $clienteObj->anio ?>">

	</div>
	</div>
</div>

<div class="row">
	<div class="form-group col-md-7">
		<label for="Mensaje Personalizado">Anterior mensaje :
<?php echo $objetoRenovacion->msjPersonalizado ?>
</label>

<textarea   style="border: none;" type="text" id="" name="msjPersonalizado" class="summernote border-bottom form-control "  >
</textarea>
	</div>

	<div class="col-md-3">
	
            <div class="toggle-button toggle-button--vesi">
                <input id="toggleButton5" type="checkbox" value="si" name="cee" >
                <label for="toggleButton5" data-on-text="Enviar" data-off-text="No enviar"></label>
                <div class="toggle-button__icon"></div>
            </div>   
      </div>  


	<div class="col-md-1">
		<label for="track">TRACK</label>
		<div class="switch">
			<div class="onoffswitch">
				<input type="radio" class="onoffswitch-checkbox" id="track" value="Track" name="plantilla" checked="">
				<label class="onoffswitch-label" for="track">
					<span class="onoffswitch-inner"></span>
					<span class="onoffswitch-switch"></span>
				</label>
			</div>
		</div>
	</div>
	<div class="col-md-1">
		<label for="octo">OCTO</label>
		<div class="switch">
			<div class="onoffswitch">
				<input type="radio" class="onoffswitch-checkbox" id="octo" value="Octo" name="plantilla" >
				<label class="onoffswitch-label" for="octo">
					<span class="onoffswitch-inner"></span>
					<span class="onoffswitch-switch"></span>
				</label>
			</div>
		</div>
	</div> 
</div>
        <table class="table">
            <tr>
                <td width="50%">
                    <label for="">Vehículo</label>
                    <input  style="border: none;" readonly type="text" class="border-bottom form-control"
                    required value="<?= $clienteObj->placa ?> | <?= $clienteObj->chasis ?> | <?= $clienteObj->marca." | ".$clienteObj->modelo ?>"  >
                </td>
                <td width="20%">
                    <label for="desdehasta" >Vence.</label>
                    <input style="border: none;" type="text" placeholder="Vence" id="desdehasta"  class="border-bottom form-control" name="desdehasta" value="<?php echo $retVal = (!empty($vencimientoDatos->hasta)) ? "$vencimientoDatos->hasta" : "" ; ?>">
                </td>
                <td width="20%">
                    <label for="Monto"  >Precio Renovacion</label>
                    <input style="border: none;" type="text" placeholder="Costo renovacion*" id="Monto"  class="border-bottom form-control" name="monto" required="" value="<?php echo $retVal = (!empty($objetoRenovacion->monto)) ? $objetoRenovacion->monto : "" ; ?>">
                </td>
                <td width="10%">

                <label for="baja">Renovado</label>
                <div class="switch">
                    <div class="onoffswitch">
                        <input type="checkbox" class="onoffswitch-checkbox" id="rnv" value="si" name="ok">
                        <label class="onoffswitch-label" for="rnv">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>

                </td>
            </tr>

            <tr class="rn">
                    <td colspan="4">
                        <div style="height: 90px; overflow-y: auto;">
                            <?php $objOrdenes=Onix::obtenerOrdenesVehiculo($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation) ?>
                            <?php foreach ($objOrdenes as $var): ?>
                            <b><?= $orden=$var->idlocation."-".$var->idorder ?></b>&nbsp;<?= utf8_encode($var->articulo) ?> &nbsp;
                            <?php   $DataOrd=Onix::obtenerDatosFacturacion($orden) ?>
                            <?php   $DataOrt=Onix::obtenerORTE($orden) ?>
                            <?= ($DataOrd)?$DataOrd->serie.$DataOrd->numeroFactura."  $".round($DataOrd->precioO*1.18,2):""; ?>
                            <?= ($DataOrt->orteO!='')?" ORTE ".$DataOrt->orteO." ":'' ?> <small><?= haceMinimoAnio($var->fechaOrden) ?></small>
                            <p class="btn btn-primary btn-sm"
                            onclick='completeData(<?= $var->idlocation ?>,<?= $var->idorder ?>, "<?= utf8_encode($var->articulo) ?>")'
                            ><span class="fa fa-check"></span></p>
                        <hr>
                        <?php endforeach ?>
                        </div>
                    </td>
            </tr>
            <tr class="rn">
                <td colspan="2">
                    <label for="">Articulo</label>
                    <input name="plan" id='articler' readonly class="form-control" required autocomplete='false' type="text" placeholder="">
                </td>
                <td colspan="2">
                    <label for="">Orden Facturación</label>
                    <input name="ordenRenova" id='ordenRenovar' readonly class="form-control" required autocomplete='false' type="text" placeholder="">
                </td>
            </tr>

        </table>

<hr>
<script>
function completeData(loc, order, articulo){
    //console.log(loc+" order:"+order+" orte:"+orte)
    $("#ordenRenovar").val(loc+"-"+order)
    $("#articler").val(articulo)
    $(".siRenovado").show();
}

$('#rnv').click(function(){
    if($(this).is(':checked')){
        $(".rn").show();
        //$(this).closest('tr').next().show();
        $("#ordenRenovar").prop('disabled',false)
        $("#articler").prop('disabled',false)
    }else{
        $(".rn").hide();
        $("#ordenRenovar").prop('disabled',true)
        $("#articler").prop('disabled',true)

        //$(this).closest('tr').next().hide();
    }
})
</script>

<input required readonly style="border: none;" type="hidden" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->marca)) ? "$clienteObj->marca" : "" ; ?>" >
<input required readonly style="border: none;" type="hidden" placeholder="Observaciones" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($clienteObj->modelo)) ? "$clienteObj->modelo" : "" ; ?>">
<input required readonly style="border: none;" type="hidden" placeholder="Observaciones" id="Observaciones" name="placaVehiculo" class="border-bottom form-control"   value="<?= $clienteObj->placa ?>">
<input  style="border: none;" readonly type="hidden" placeholder="Color" id="color" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->color)) ? "$clienteObj->color" : "" ; ?>">
<input required  readonly style="border: none;" type="hidden" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->chasis)) ? "$clienteObj->chasis" : "$chasis" ; ?>">
<input  style="border: none;" readonly type="hidden" placeholder="Observaciones" id="Observaciones" name="motor" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->motor)) ? "$clienteObj->motor" : "Actualizar" ; ?>">

<div class="row siRenovado">
	<div class="col-md-6">
        <div class="form-group">
            <label for="Forma*" >Forma</label>
            <select style="border: none;" id="formaPago" name="formaPago" class="form-control border-bottom" required >
                <option value="" disabled selected>Datos de pago*</option>
                <option value="Deposito">Deposito</option>
                <option value="OrdenDeCompra">Órden de compra</option>
                <option value="PagoEntaler" > Págo en taller</option>
                <!--<option value="Credito" >Credito</option>-->
            </select>
        </div>
	</div>
	<div class="col-md-6 form-group voucherImg">
		<label for="Forma*" >Voucher</label>
			<div class="fileinput fileinput-new input-group" data-provides="fileinput">
				<div class="form-control" data-trigger="fileinput">
					<i class="glyphicon glyphicon-file fileinput-exists"></i>
					<span class="fileinput-filename"></span>
				</div>
				<span class="input-group-addon btn btn-default btn-file">
					<span class="fileinput-new">Seleccionar Voucher de pago*</span>
					<span class="fileinput-exists">Cambiar</span>
					<input type="file" name="voucher"  id="voucherImg" required="" />
				</span>
				<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
			</div> 
		</div>
</div>
<div class="row siRenovado">
<div class="col-md-4 form-group">
	<select style="border: none;" id="selectNa" name="tipoDocumentoFacturacion" class="cd-selec form-control border-bottom" required >
		<option value="" disabled selected>Datos de facturación*</option>
		<option value="Boleta">Boleta</option>
		<option value="Factura">Factura</option>
	</select>   
</div>
<div class="col-md-8 form-group">
	<div class="content-pane e1">
		<h3>Seleccione pago</h3>
	</div>
	<div class="content-pane eBoleta">
		<h3>Continuar</h3>
	</div>
	<div class="content-pane eFactura">
		<div class="row"><br>
			<div class="form-group col-md-4">
				<label for="rucFacturacion" class="">Ruc *</label>
				<input style="border: none;" type="number" placeholder="Ruc *" id="rucFacturacion" name="rucFacturacion" class="form-control x2 border-bottom" maxlength="12" minlength="10" value="<?= $retVal = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>"  >
			</div>
			<?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
			<div class="form-group col-md-8">
				<label for="razonSocialFacturacion" class="">Razón Social *</label>
				<input style="border: none;" type="text" placeholder="Razón Social *" id="razonSocialFacturacion" name="razonSocialFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ; ?>" >
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-8">
				<label for="direccionFiscalFacturacion" class="sr-only">Dirección Fiscal *</label>
				<input style="border: none;" type="text" placeholder="Dirección Fiscal *" id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="form-control border-bottom text-capitalize" value="<?= utf8_encode($clienteObj->direccion) ?>">

			</div>
			<div class="form-group col-md-4">
				<label for="telefonoFacturacion" class="sr-only">Teléfono</label>
				<input style="border: none;" type="text" placeholder="Teléfono" id="telefonoFacturacion" name="telefonoFacturacion" class="form-control border-bottom text-capitalize" value="<?= $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">

			</div>
		</div>      
	</div> 
</div>  
</div>

<div class="row siRenovado">
    <div class="form-group col-md-12">
        <label for="objsf" class="">Observacion para facturación</label>
        <input style="border: none;" type="text" placeholder="Observacion para facturación" id="objsf" name="contaObs" class="form-control x2 border-bottom"  >
    </div>
</div>




<div class="row">
	<div class="col-md-12">
		<label for="Observaciones" >Observaciones *</label>
		<input  style="border: none;" type="text" placeholder="Observaciones *" id="Observaciones" name="Observaciones" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($objetoRenovacion->observacion) ) ? $objetoRenovacion->observacion : "" ; ?>">
	</div>
</div>
<div class="row">
<div class="col-md-12">
    <label for="baja">Dar de baja no facturados</label>
    <div class="switch">
        <div class="onoffswitch">
            <input type="checkbox" class="onoffswitch-checkbox" id="baja" value="si" name="bj">
            <label class="onoffswitch-label" for="baja">
                <span class="onoffswitch-inner"></span>
                <span class="onoffswitch-switch"></span>
            </label>
        </div>
    </div>
</div>
<hr>
</div>
<input type="hidden" name="chasisO" value="<?php echo $chasis ?>" >
<?php if (isset($clienteObj->articulo)): ?>
	<input type="hidden" name="articulo" value="<?php echo $clienteObj->articulo ?>" >

<?php endif ?>
<?php else: ?>

<?php endif ?>
<script>
$(document).ready(function(){

$('.siRenovado').hide();
$(".rn").hide();
$("#ordenRenovar").prop('disabled',true)
$("#articler").prop('disabled',true)

$('#clientes').click(function(){
    $('.siRenovado').show();
    if($(this).is(':checked')){
        $('.siRenovado').show();
        $("#baja").prop("checked",false);
        $('#ordenRenova').prop('disabled', false);
        $('#formaPago').prop('disabled', false);
        $('#voucherImg').prop('disabled', false);

        //$('#fechaAbono').prop('disabled', false);
        //$('#numeroOperacion').prop('disabled', false);
        //$('#bancoPago').prop('disabled', false);
        $('#selectNa').prop('disabled', false);
    }else{
        $("#baja").prop("checked",false);
        $('.siRenovado').hide();
    }

});

$('#baja').click(function(){
    if($(this).is(':checked')){
            $('.siRenovado').hide();
            $("#clientes").prop("checked",false);
            $('#ordenRenova').prop('disabled', true);
            $('#formaPago').prop('disabled', true);
            $('#voucherImg').prop('disabled', true);
            //$('#fechaAbono').prop('disabled', true);
           // $('#numeroOperacion').prop('disabled', true);
           // $('#bancoPago').prop('disabled', true);
            $('#selectNa').prop('disabled', true);
    }else{
            $("#clientes").prop("checked",false);
            $('#ordenRenova').prop('disabled', false);
            $('#formaPago').prop('disabled', false);
            $('#voucherImg').prop('disabled', false);
           // $('#fechaAbono').prop('disabled', false);
           // $('#numeroOperacion').prop('disabled', false);
           // $('#bancoPago').prop('disabled', false);
            $('#selectNa').prop('disabled', false);
    }
});

$('#toggleButton5').click(function(){
    if($(this).is(':checked')){
            $('.siRenovado').hide();
            $("#clientes").prop("checked",false);
            $('#ordenRenova').prop('disabled', true);
            $('#formaPago').prop('disabled', true);
            $('#voucherImg').prop('disabled', true);
            //$('#fechaAbono').prop('disabled', true);
           // $('#numeroOperacion').prop('disabled', true);
           // $('#bancoPago').prop('disabled', true);
            $('#selectNa').prop('disabled', true);
    }else{
            $("#clientes").prop("checked",false);
            $('#ordenRenova').prop('disabled', false);
            $('#formaPago').prop('disabled', false);
            $('#voucherImg').prop('disabled', false);
           // $('#fechaAbono').prop('disabled', false);
           // $('#numeroOperacion').prop('disabled', false);
           // $('#bancoPago').prop('disabled', false);
            $('#selectNa').prop('disabled', false);
    }
});
//toggleButton5

$('.eBoleta').hide();
$('.eFactura').hide();
$('#selectNa').on("change",function () {
if ($(this).val()=="Boleta") {
    $('.eBoleta').show(1000);
    $('.eFactura').hide(1000);
    $('#rucFacturacion').prop('required',false);
    $('#razonSocialFacturacion').prop('required',false);
    $('#direccionFiscalFacturacion').prop('required',false);
}
if($(this).val()=="Factura"){
    $('.eBoleta').hide(1000);
    $('.eFactura').show(1000);
    ruc=$('#rucFacturacion').val();
	if (ruc.length==11) {

	}else{
		alert("El ruc debe contener 11 caracteres actualmente tiene: "+ruc.length );
	}
    $('#rucFacturacion').prop('required',true);
    $('#razonSocialFacturacion').prop('required',true);
    $('#direccionFiscalFacturacion').prop('required',true);

}
  });

$('.voucherImg').hide();
$('#formaPago').on("change",function(){
        //if ($(this).val()=='Deposito'||$(this).val()=='OrdenDeCompra') {
    console.log("hola");
            $('#voucherImg').prop('disabled', false);

            //$('#fechaAbono').prop('disabled', false);
            //$('#numeroOperacion').prop('disabled', false);
            //$('#bancoPago').prop('disabled', false);
            $('.voucherImg').show('slow');
           // }else{
           // $('#voucherImg').prop('disabled', true);
           // $('#fechaAbono').prop('disabled', true);
           // $('#numeroOperacion').prop('disabled', true);
           // $('#bancoPago').prop('disabled', true);
           // $('.voucherImg').hide('slow');
        //}
});


function obtenerVal(orden){
    $.ajax({
       type : 'POST',
       url  : 'api/onyxDataOrden',
       data:{orden:orden},
       success:function(data){
            var data = JSON.parse(data);
            $("#plandeO").val(data.orden+" "+data.apellidos);
            $("#tiempoO").val(data.hasta);
       },
       error:function(e){
        console.log(e);
       }
    });
}
})

</script>

<?php
} 
?>

<?php 
/////////////////////////%#$%#$%#$%$#%$B tes t////////#########################///###################################//&/%&/%&/%&/%&/%&/&/&
 ?>

<?php if (isset($_POST['chasis'])): ?>
<?php 
	include_once("../../includes/init.php");
	$chasis=htmlspecialchars(htmlentities(limpiar($_POST['chasis'])));		
	$clienteObj=Onix::ObtenerDatosDesdeChasis($chasis);
	$vencimientoDatos=Onix::obtenerFechaDeVencimiento($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation);	
	$tiempo="";

    $obMenu=Usuario::obtenerMenu($session->idusuario);
	if ($clienteObj) {
		$cantidadObjeto=Onix::obtenerCantididadVehiculos($clienteObj->idcustomer,$clienteObj->idlocation);
	}
	?>
	<div class="well" style="padding-top: 11px;
	padding-bottom: 10px;">
	<div class="row">
		<div class="row">
		<h2 class="text-center"><?php 
                        if($clienteObj){  
                        	echo vence($vencimientoDatos->hasta);
                        }else{
                            echo "No esta en la base de datos consulta este mensaje con pvasquez@tracklink.pe";
                        }
                        ?></h2>

                        <?php if ($clienteObj): ?>
                        	
                        <?php if ($cantidadObjeto): ?>
                        	<?php if ($cantidadObjeto->cantidad>1): ?>
                        	<h4 class="text-center text-warning">ATENCION ! Cliente tiene <?php echo $cantidadObjeto->cantidad ?> Vehiculos en Tracklink</h4>	
                        	<?php endif ?>
                        <?php endif ?>
	<input type="hidden" name="vencidoHace" value="<?php echo $tiempo ?>">
	<input type="hidden" name="registrarDesdeChasis" value="<?php echo $tiempo ?>">
			<div class="form-group col-md-3">
				<label for="Observaciones" > Usuario Web</label>
				<input name="usuarioWeb" style="border: none;" type="text" placeholder="Web" id="Observaciones" <?php echo $retVal = (!empty($clienteObj->dni)) ? "readonly" : "" ; ?> class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>">
			</div>
			<div class="form-group col-md-5">
				<label for="numeroTecnico" >Nombre de cliente*</label>
				<input  style="border: none;" type="text" readonly id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$clienteObj->apellidos" : "$clienteObj->apellidos" ; ?> ">
			</div>
			<div class="col-md-2"><label for="estado" >Celular</label>
				<input style="border: none;" type="text" <?php echo $retVal = (!empty($clienteObj->celular)) ? "readonly" : "" ; ?> id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular)) ? "$clienteObj->celular" : "" ; ?>">
			</div>
			<div class="col-md-2"><label for="estado" >Celular</label>
				<input style="border: none;" type="text" readonly id="estado"  class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->celular2)) ? "$clienteObj->celular2" : "" ; ?>">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-4">
				<label for="telefono" >Telefono </label>
				<input style="border: none;" type="text" readonly id="telefono"  class="border-bottom form-control" name="telefono" value="<?php echo $retVal = (!empty($clienteObj->telefonoTrabajo)) ? "$clienteObj->telefonoTrabajo" : "" ; ?>">
			</div>
			<div class="form-group col-md-4">
				<label for="telefono" >Telefono </label>
				<input style="border: none;" type="text" readonly id="telefono"  class="border-bottom form-control" name="telefono" value="<?php echo $retVal = (!empty($clienteObj->telefono)) ? "$clienteObj->telefono" : "" ; ?>">
			</div>
			<div class="form-group col-md-4">
				<label for="desdehasta" >Vence.</label>
				<?php 
				if (!empty($vencimientoDatos->hasta)) {					
					$vencimientoDatos->hasta=date("d-m-Y",strtotime($vencimientoDatos->hasta));
				} 
				?>
				<input style="border: none;" type="text" placeholder="Vence" id="desdehasta"  class="border-bottom form-control" name="desdehasta" value="<?php echo $retVal = (!empty($vencimientoDatos->hasta)) ? "$vencimientoDatos->hasta" : "" ; ?>">

				<input type="hidden" name="vence" value="<?php echo $vencimientoDatos->hasta ?>">
				<input type="hidden" name="anio" value="<?php echo $clienteObj->anio ?>">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-8">
				<label for="Observaciones"  >Correo cliente. </label>
				<input style="border: none;" type="text" placeholder="Correo" id="Observaciones"  class="border-bottom form-control" name="correoCliente" value="<?php echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo," : "$clienteObj->correo" ;echo $retVal = (!empty($clienteObj->correo2)) ? "$clienteObj->correo2" : "" ;  ?>">
			</div>
			<div class="form-group col-md-4 ">
				<label for="Monto"  >Costo renovacion*</label>
				<input style="border: none;" type="text" placeholder="Costo renovacion*" id="Monto"  class="border-bottom form-control" name="monto" required="" >
			</div>
		</div>

		<div class="row">
			<div class="form-group col-md-6" ">
				<label for="Observaciones"  >Observaciones Onyx </label>
				<div style="border-right: solid;height: 50px;
    overflow-y: auto;">
				<label class="border-" ><?php echo utf8_encode($clienteObj->comentarios) ?></label>
				</div>
			</div>
			<div class="col-md-6 form-group" " >
			<label for="">Ordenes de Vehículo</label>
		<div style="height: 50px;
    overflow-y: auto;">
				<?php $objOrdenes=Onix::obtenerOrdenesVehiculo($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation) ?>
				<?php foreach ($objOrdenes as $var): ?>
					<?php echo utf8_encode($var->articulo) ?><br>
					<?php echo haceMinimoAnio($var->fechaOrden) ?><hr>
				<?php endforeach ?>
				</div>
			</div>
		</div>
	</div>

	</div>
</div>
<hr>

<div class="row">
	<div class="form-group col-md-3">
		<label for="numeroTecnico" >Placa Vehiculo*</label>
		<input  style="border: none;" readonly type="text" placeholder="Placa" id="numeroTecnico" name="placaVehiculo" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->placa)) ? "$clienteObj->placa" : "" ; ?>"  >
	</div>
	<div class="form-group col-md-3">
		<label for="Observaciones"  >Marca *</label>
		<input required readonly style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->marca)) ? "$clienteObj->marca" : "" ; ?>" >
	</div>
	<div class="form-group col-md-3">
		<label for="Observaciones" >Modelo*</label>
		<input required readonly style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($clienteObj->modelo)) ? "$clienteObj->modelo" : "" ; ?>">
	</div>
	<div class="form-group col-md-3">
		<label for="color"  >Color</label>
		<input  style="border: none;" readonly type="text" placeholder="Color" id="color" name="color" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->color)) ? "$clienteObj->color" : "" ; ?>">
	</div>
	
</div>
    <div class="row">
        <div class="form-group col-md-4">
            <label for="idEjecutvio" class="">Seleccione Ejecutivo</label>
            <?php
          //  if ($obMenu->administrarComercial=="si") {
                ?>
                <select  style="border: none;" class="border-bottom form-control text-capitalize" required name="idEjecutvio" id="idEjecutvio">
                    <option class='text-capitalize' value=''  disabled selected >Seleccione Ejecutivo</option>
                    <?php if ($datosUsu=Usuario::obtenerEjecutivos()): ?>
                        <?php foreach ($datosUsu as $var): ?>
                            <option class='text-capitalize' value='<?= $var->idusuario ?>' > <?= $var->usuarioNombre." ".$var->usuarioApellidoPaterno;  ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
                <?php
            /*}else{
                ?>
                <select  style="border: none;" class="border-bottom form-control text-capitalize" required name="idEjecutvio" id="idEjecutvio">
                    <option class='text-capitalize' value='' disabled selected >Seleccione Ejecutivo</option>
                    <?php if ($datosUsu=Usuario::obtenerEjecutivosAsignados($session->idusuario)): ?>
                        <option class="text-capitalize" value="153"> Motorlink </option>
                        <?php foreach ($datosUsu as $var): ?>
                            <option class='text-capitalize' value='<?= $var->idusuario ?>' > <?= $var->usuarioNombre." ".$var->usuarioApellidoPaterno;  ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
                <?php
            }*/
            ?>
        </div>
        <div class="form-group col-md-4">
            <div class="form-group col-md-5">

                <div class="checkbox checkbox-success  <?php  if( $clienteObj->idtidentdoc== "R" || strlen($clienteObj->dni) == 11){ echo 'disabled';} ?>">
                    <input id="cpreferente" type="checkbox"  <?php  if($clienteObj->idtidentdoc== "R" || strlen($clienteObj->dni) == 11 ){ echo 'disabled';} ?>  name="cpreferente" value="si">
                    <label for="cpreferente">
                        <strong>Cliente Preferente</strong>
                    </label>
                </div>
            </div>
        </div>
    </div>
        <div class="row">
	<div class="form-group col-md-6">
		<label for="Observaciones" >Chasis*</label>
		<input required  readonly style="border: none;" type="text" placeholder="Chasis" id="Observaciones" name="chasis" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->chasis)) ? "$clienteObj->chasis" : "$chasis" ; ?>">
	</div>
	<div class="form-group col-md-6">
		<label for="Observaciones" >Motor</label>
		<input  style="border: none;" readonly type="text" placeholder="Observaciones" id="Observaciones" name="motor" class="border-bottom form-control" value="<?php echo $retVal = (!empty($clienteObj->motor)) ? "$clienteObj->motor" : "Actualizar" ; ?>">
	</div>
</div>
<hr>
<div class="row">
	<div class="form-group col-md-12">
		<label for="Mensaje Personalizado">Mensaje personalizado.</label>
<textarea  style="border: none;" type="text" id="Mensaje Personalizado" name="msjPersonalizado" class="summernote border-bottom form-control "  >


</textarea>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
	
            <div class="toggle-button toggle-button--vesi">
                <input id="toggleButton5" type="checkbox" value="si" name="cee" >
                <label for="toggleButton5" data-on-text="Enviar correo" data-off-text="No enviar"></label>
                <div class="toggle-button__icon"></div>
            </div>   
            </div>   

	
	<div class="col-md-2">
		<label for="trackAbc">TRACK</label>
		<div class="switch">
			<div class="onoffswitch">
				<input type="radio" class="onoffswitch-checkbox" id="trackAbc" value="Track" name="plantilla" checked="">
				<label class="onoffswitch-label" for="trackAbc">
					<span class="onoffswitch-inner"></span>
					<span class="onoffswitch-switch"></span>
				</label>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<label for="octoAbv">OCTO</label>
		<div class="switch">
			<div class="onoffswitch">
				<input type="radio" class="onoffswitch-checkbox" id="octoAbv" value="Octo" name="plantilla" >
				<label class="onoffswitch-label" for="octoAbv">
					<span class="onoffswitch-inner"></span>
					<span class="onoffswitch-switch"></span>
				</label>
			</div>
		</div>
	</div>
</div>
	<hr>
<div class="row">
	<div class="col-md-12">
		<label for="Observaciones" >Observaciones *</label>
		<input  style="border: none;" type="text" placeholder="Observaciones *" id="Observaciones" name="Observaciones" class="border-bottom form-control" required >
	</div>
</div>
<input type="hidden" name="chasisO" value="<?php echo $chasis ?>" >
<?php if (isset($clienteObj->articulo)): ?>
	<input type="hidden" name="articulo" value="<?php echo $clienteObj->articulo ?>" >
<?php endif ?>
<?php else: ?>
<?php endif ?>
	
<?php endif ?>

<script src="../../assets/js/plugins/summernote/summernote.min.js"></script>
<script>
$('.summernote').summernote({
	placeholder:'Mensaje personalizado',
	tabsize:2,
	height:100
});
</script>



<?php 
if ($_POST['rowid']) {
  include_once("../../includes/head.php");
  $idOperacion=htmlspecialchars(htmlentities($_POST['rowid']));
  $ObjetoOperacion=Operacion::obtenerOperacionPorId($idOperacion);
  $vehiculoObj=Vehiculo::obtenerVehiculo($ObjetoOperacion->idvehiculo);
  $clienteObj=Cliente::obtenerClientePorVehiculo($ObjetoOperacion->idvehiculo,$ObjetoOperacion->idcliente);
  Operacion::asignarCagoOperaciones($_POST['rowid']);
  ?>
  <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
  <h2 class="text-center"><?php echo $ObjetoOperacion->trabajo ?></h2>
  <?php 
    $objFacturacion=DatosFacturacion::obtenerDatosFacturacionPorId($ObjetoOperacion->datosfacturacionid);
   ?>
  <?php if (empty($objFacturacion->voucher)): ?> 
  <h3 class="text-center text-danger">Atención pago en caja</h3>
  <input type="hidden" name="pagoEnCaja" value="ok">
  <?php endif ?> 
  <!-- INSTALACIONES  ORDEN  -->
  <?php if ($vehiculoObj->estado<>1): ?>

    <div class="well" style="padding-top: 11px;
    padding-bottom: 10px;">
    <div class="row">
      <div class="form-group col-md-4 text-primary text-capitalize">
        <?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? "Nombres: " : "Razon social: " ; ?><?php echo $retVal = ($clienteObj->tipoCliente=="Natural") ? $clienteObj->nombre." ".$clienteObj->apellidosC : $clienteObj->razonSocial ; ?>

      </div>
      <div class="form-group col-md-4">
        Placa: <?php echo $vehiculoObj->placa ?>
      </div>  
      <div class="form-group col-md-4">
       Estado: <?php echo obtenerEstado($vehiculoObj->estado) ?>
     </div>
   </div> 
   <div class="row">
    <div class="form-group col-md-4 text-primary text-capitalize">
      Marca <?php echo $vehiculoObj->marca; ?>

    </div>
    <div class="form-group col-md-4">
      Modelo: <?php echo $vehiculoObj->modelo ?>
    </div>  
    <div class="form-group col-md-4">
     Color: <?php echo $vehiculoObj->color ?>
   </div>
 </div>
 <div class="row">
  <div class="form-group col-md-4 text-primary text-capitalize">
    Chasis: <?php echo $vehiculoObj->chasis; ?>

  </div>
  <div class="form-group col-md-4">
    Motor: <?php echo $vehiculoObj->motor ?>
  </div>  
  <div class="form-group col-md-4">
   Año: <?php echo $vehiculoObj->anio ?>
 </div>
</div>


<?php if (!empty($vehiculoObj->serviciosAdicionales)): ?>
 <div class="row">
   <div class="form-group col-md-12">
     Servicios adicionales: <?php echo $vehiculoObj->serviciosAdicionales ?>
   </div>
 </div>
<?php endif ?>
<?php if ($vehiculoObj->estado==3): ?>
  <?php  if (!$vehiculoObj->asignarCargoDeBienvenida()) {
      die("error en asginar cargo");
  } ?>
 <div class="row">
  <div class="form-group col-md-4 text-primary text-capitalize">
    Telefonos: <?php echo $clienteObj->telefono; ?>, <?php echo $clienteObj->telefono1; ?>, <?php echo $retVal = (!is_null($clienteObj->telefono2)) ? ", ".$clienteObj->telefono2 : "" ?> <?php echo $retVal = (!is_null($clienteObj->telefono3)) ? ", ".$clienteObj->telefono3 : ""?>
  </div>
  <div class="form-group col-md-4">
    <div class="row">
      <div class="col-md-12">
        Correos: <?php echo $clienteObj->correo ?>

      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <?php echo $retVal = (!is_null($clienteObj->correo1)) ? ",".$clienteObj->correo1 : "" ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <?php echo $retVal = (!is_null($clienteObj->correo2)) ? ",".$clienteObj->correo2 : "" ?>
      </div>
    </div>
  </div>  
  <div class="form-group col-md-4">
   Plan: <?php echo $vehiculoObj->plan ?>
 </div>

</div> 
<?php endif ?>
</div> 
<?php endif ?>
<!-- ORDENES INSTALACIONES FIN END END END END  -->




<?php 
if ($vehiculoObj->estado==1) {
  ?>
  <br>  
  <div class="row">
    <div class="col-md-3 form-group"><label for="dniOruc">DNI o Ruc: </label>
      <input type="hidden" name="tipoCliente" value="<?php echo $clienteObj->tipoCliente ?>">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>

        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="dni" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->dni ?>">       
      <?php else: ?>
        <input style="border: none;" type="text" placeholder="Placa" id="dniOruc" name="ruc" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->ruc ?>">        
      <?php endif ?>  
    </div>

    <div class="col-md-9">
      <?php if ($clienteObj->tipoCliente=="Natural"): ?>
        <div class="row">
          <div class="col-md-4 form-group">
            <label for="nombrecl">Nombre: </label>
            <input style="border: none;" type="text" placeholder="Nombre" id="nombrecl" name="nombre" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->nombre ?>">
          </div>
          <div class="col-md-8 ">
            <label for="apellidosC">Apellidos</label>
            <input style="border: none;" type="text" placeholder="Apellidos" id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->apellidosC ?>">
          </div>
        </div>
      <?php else: ?>
        <label for="razonSocial"> Razon Social: </label> 
        <input style="border: none;" type="text" placeholder="Razon social" id="razonSocial" name="razonSocial" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->razonSocial ?>">       
      <?php endif ?>
    </div>
    

  </div>    
  <?php if ($clienteObj->tipoCliente=="Natural"): ?>
    <div class="row">
      <div class="col-md-4"><label for="departamento">Departamento:</label>
        <input style="border: none;" type="text" placeholder="Departamento" id="departamento" name="departamento" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->departamento ?>">                
      </div>
      <div class="col-md-4">
        <label for="provincia">Provincia:</label> 
        <input style="border: none;" type="text" placeholder="Departamento" id="provincia" name="provincia" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->provincia ?>">       
      </div>
      <div class="col-md-4">
        <label for="distrito">Distrito: </label>
        <input style="border: none;" type="text" placeholder="Distrito" id="distrito" name="distrito" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->distrito ?>">        
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <label for="direccion">Direccion: </label>
        <input style="border: none;" type="text" placeholder="Direccion" id="direccion" name="direccion" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->direccion ?>">       
      </div>
      <div class="col-md-6">
        <label for="referencia">Referencia: </label>
        <input style="border: none;" type="text" placeholder="Referencia:" id="referencia" name="referencia" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->referencia ?>">     
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <label for="fechaNacimiento">Fecha de nacimiento: </label> 
        <input style="border: none;" type="text" placeholder="Fecha de nacimiento:" id="fechaNacimiento" name="fechaNacimiento" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->fechaNacimiento ?>"> 
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3">
      <label for="telefono">Celular: </label>
      <input style="border: none;" type="text" placeholder="Celular:" id="telefono" name="telefono" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->telefono ?>">        
    </div>
    <div class="col-md-3">
      <label for="telefono1">Telefono : </label>
      <input style="border: none;" type="text" placeholder="Telefono :" id="telefono1" name="telefono1" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->telefono1 ?>">     
    </div>
    <div class="col-md-3">
      <label for="correo">Correo: </label>
      <input style="border: none;" type="text" placeholder="Correo:" id="correo" name="correo" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->correo ?>">       
    </div>
    <div class="col-md-3">
      <label for="correo1">Correo alternativo: </label>
      <input style="border: none;" type="text" placeholder="Correo alternativo:" id="correo1" name="correo1" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->correo1 ?>">        
    </div>
  </div>
<?php else: ?>

  <div class="row">
    <div class="col-md-4">
      <label for="departamentoL">Departamento: </label>
      <input style="border: none;" type="text" placeholder="Departamento: " id="departamentoL" name="departamentoL" class="border-bottom form-control x2 text-uppercase" required  value="<?php echo $clienteObj->departamentoL ?>">      
    </div>
    <div class="col-md-4">
      <label for="provinciaL">Provincia: </label>
      <input style="border: none;" type="text" placeholder="Provincia: " id="provinciaL" name="provinciaL" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $clienteObj->provinciaL ?>">
    </div>
    <div class="col-md-4">
      <label for="distritoL">Distrito: </label>
      <input style="border: none;" type="text" placeholder="Distrito: " id="distritoL" name="distritoL" class="border-bottom form-control x2 text-uppercase"   required value="<?php echo $clienteObj->distritoL ?>">      
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <label for="direccionL">Direccion: </label>
      <input style="border: none;" type="text" placeholder="Direccion: " id="direccionL" name="direccionL" class="border-bottom form-control x2 text-uppercase" required  value="<?php echo $clienteObj->direccionL ?>" >
    </div>

  </div>
  <div class="row">
    <div class="col-md-3">
      <label for="telefono">Celular: </label>
      <input style="border: none;" type="text" placeholder="Celular: " id="telefono" name="telefono" class="border-bottom form-control x2 text-uppercase" required=""  value="<?php echo $clienteObj->telefono ?>">
    </div>
    <div class="col-md-3">
      <label for="telefono1">Telefono : </label>
      <input style="border: none;" type="text" placeholder="Telefono: " id="telefono1" name="telefono1" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->telefono1 ?>">
    </div>
    <div class="col-md-3">
      <label for="correo">Correo: </label>
      <input style="border: none;" type="text" placeholder="Correo: " id="correo" name="correo" class="border-bottom form-control x2 text-uppercase" required="" value="<?php echo $clienteObj->correo ?>">
    </div>
    <div class="col-md-3">
      <label for="correo1">Correo alternativo: </label>
      <input style="border: none;" type="text" placeholder="Correo alternativo: " id="correo1" name="correo1" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->correo1 ?>">
    </div>
  </div>
<?php endif ?>
<?php if ($clienteObj->tipoCliente!="Natural"): ?>
  <hr>
  <div class="row">
    <div class="row">
      <div class="col-md-12">
        <h4>Representante legal </h4>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <label for="dni">DNI: </label>
        <input style="border: none;" type="text" placeholder="DNI: " id="dni" name="dni" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->dni ?>">
      </div>
      <div class="col-md-4">
        <label for="nombre">Nombres: </label>
        <input style="border: none;" type="text" placeholder="Nombres: " id="nombre" name="nombre" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->nombre ?>">
      </div>
      <div class="col-md-4">
        <label for="apellidosC">Apellidos </label>
        <input style="border: none;" type="text" placeholder="Nombres: " id="apellidosC" name="apellidosC" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->apellidosC ?>">
      </div>
    </div><div class="row">
    <div class="col-md-12">
      <label for="fechaNacimiento">Fecha de nacimiento: </label> 
      <input style="border: none;" type="text" placeholder="Fecha de nacimiento: " id="fechaNacimiento" name="fechaNacimiento" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->fechaNacimiento ?>">
      <input type="hidden" name="fechaNacimiento" value="<?php echo $clienteObj->fechaNacimiento ?>"> 
    </div>
  </div> 
</div>
<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-4">
        <label for="departamento">Departamento: </label>
        <input style="border: none;" type="text" placeholder="Departamento:" id="departamento" name="departamento" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->departamento ?>">
      </div>
      <div class="col-md-4">
        <label for="provincia">Provincia: </label>
        <input style="border: none;" type="text" placeholder="Provincia:" id="provincia" name="provincia" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->provincia ?>">
      </div>
      <div class="col-md-4">
        <label for="distrito">Distrito: </label>
        <input style="border: none;" type="text" placeholder="Distrito:" id="distrito" name="distrito" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->distrito ?>">
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <label for="direccion">Direccion: </label>
        <input style="border: none;" type="text" placeholder="Direccion:" id="direccion" name="direccion" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->direccion ?>">
      </div>
      <div class="col-md-6">
        <label for="referencia">Referencia: </label>
        <input style="border: none;" type="text" placeholder="Referencia:" id="referencia" name="referencia" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->referencia ?>">
      </div> 
    </div>
    <div class="row">
      <div class="col-md-3">
        <label for="telefono2">Celular: </label>
        <input style="border: none;" type="text" placeholder="Celular :" id="telefono2" name="telefono2" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->telefono2 ?>">
      </div>
      <div class="col-md-3">
        <label for="telefono3">Telefono : </label>
        <input style="border: none;" type="text" placeholder="Telefono:" id="telefono3" name="telefono3" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->telefono3 ?>">
      </div>
      <div class="col-md-3">
        <label for="correo2">Correo: </label>
        <input style="border: none;" type="text" placeholder="Correo:" id="correo2" name="correo2" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->correo2 ?>">
      </div>
      <div class="col-md-3">
        <label for="correo3">Correo alternativo: </label>
        <input style="border: none;" type="text" placeholder="Correo alternativo:" id="correo3" name="correo3" class="border-bottom form-control x2 text-uppercase"  value="<?php echo $clienteObj->correo3 ?>">
      </div>
    </div>
  </div>    
</div>
<?php endif ?>
<hr>
<h3>Datos del vehiculo</h3>
<?php 
$vehiculoObj1=Vehiculo::obtenerClientePorIdCliente($clienteObj->idcliente,$vehiculoObj->idvehiculo);
// $n=1;

?>


<?php foreach ($vehiculoObj1 as $var): ?> 
  <div class="row"><hr>
    <!-- <h3 class="text-center"># <?php echo $n++ ?></h3> -->

    <div class="col-lg-12 field_wrapper"> 
      <div class="row">
        <div class="form-group col-md-2">
          <label for="placa" class="">Placa</label>
          <input style="border: none;" type="text" placeholder="Placa" id="placa" name="field_name[0][]" class="border-bottom form-control x2 text-uppercase" required value="<?php echo $var->placa ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="chasis" class="">Chasis</label>
          <input style="border: none;" type="text" placeholder="Chasis" id="chasis" name="field_name[1][]" class="border-bottom form-control text-uppercase" required value="<?php echo $var->chasis ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="motor" class="">Motor</label>
          <input style="border: none;" type="text" placeholder="Motor" id="motor" name="field_name[2][]" class="border-bottom form-control text-uppercase" required value="<?php echo $var->motor ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="marca" class="">Marca</label>
          <input style="border: none;" type="text" placeholder="Marca" id="marca" name="field_name[3][]" class="marca border-bottom form-control text-capitalize"  required autocomplete="off" value="<?php echo $var->marca ?>" >
        </div>
      </div> 
      <div class="row">
        <div class="form-group col-md-3">
          <label for="modelo" class="">Modelo</label>
          <input style="border: none;" type="text" placeholder="Modelo" id="modelo" name="field_name[4][]" class="border-bottom form-control text-capitalize"  required  value="<?php echo $var->modelo ?>">
        </div>
        <div class="form-group col-md-2">
          <label for="color" class="">Color</label>
          <input style="border: none;" type="text" placeholder="Color" id="color" name="field_name[5][]" class="border-bottom form-control x2 text-capitalize" required value="<?php echo $var->color ?>" >
        </div>
        <div class="form-group col-md-2">
          <label for="anio" class="">Año</label>
          <input style="border: none;" type="text" placeholder="Año" id="anio" name="field_name[6][]" class="border-bottom form-control" required value="<?php echo $var->anio ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="seguro" class="">Compañia de seguros</label>
          <input style="border: none;" type="text" placeholder="Compañia de seguros" id="seguro" name="field_name[7][]" class="seguro border-bottom form-control" required autocomplete="off" value="<?php echo $var->companiaSeguro ?>">
        </div>
        <div class="form-group col-md-2">
          <label for="concecionario" class="">Concesionario</label>
          <input style="border: none;" type="text" placeholder="Concesionario" id="concecionario" name="field_name[11][]" class="seguro border-bottom form-control" required autocomplete="off" value="<?php echo $var->concesionario ?>">
        </div>

      </div>                                      


      <div class="row">
        <div class="form-group col-md-4">
          <label for="numeroTramiteSeguro" class="">Numero tramite de seguro</label>
          <input style="border: none;" type="text" placeholder="Numero tramite de seguro" id="numeroTramiteSeguro" name="field_name[8][]" class="numeroTramiteSeguro border-bottom form-control text-capitalize"  value="<?php echo $var->numeroTramiteSeguro ?>"  >
        </div>
        <div class="form-group col-md-4">
          <label for="numeroPoliza" class="">N° de poliza</label>
          <input style="border: none;" type="text" placeholder="N° de poliza" id="numeroPoliza" name="field_name[9][]" class="border-bottom form-control text-capitalize"   value="<?php echo $var->numeroPoliza ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="corredor" class="">Corredor de seguros</label>
          <select  style="border: none;" class="corredor border-bottom form-control text-capitalize" required name="field_name[10][]">
            <option class='text-capitalize' value='"<?php echo $var->idcorredordeseguro ?>"'  selected ><?php 
              echo $var->idcorredordeseguro
              ?></option>
              <?php Vehiculo::obtenerCorredores() ?>
            </select>
          </div>
        <!-- <div class="form-group col-md-3">
          <a class="add_button btn btn-info col-md-12" title="Agregar contacto" name="agregarVehículo">Agregar <i class="fa fa-plus-circle "></i></a> 
        </div> -->
      </div> 
    </div>
  </div> 

<?php endforeach ?> 

<hr><div class="hr-line-dashed"></div>

<?php if (($contactosAutorizadosTObj=Cliente::obtenerContactosAutorizados($clienteObj->idcliente))<>false): ?>

  <?php 
  $m=1;
  $contactosAutorizadosTObj=Cliente::obtenerContactosAutorizados($clienteObj->idcliente);
  ?>
  <h3>Contactos autorizados: </h3>
  <?php foreach ($contactosAutorizadosTObj as $var): ?>
    <h3 class="text-center">#<?php echo $m++ ?></h3>
    <div class="row">
      <div class="form-group col-md-3">
        <label for="dniC" class="">DNI: </label> <?php echo $var->dni ?>
        <input type="hidden" name="contactos[0][]" value="<?php echo $var->dni ?>" >
      </div>
      <div class="form-group col-md-3">
        <label for="nombreC" class="">Nombres: </label> <?php echo $var->nombre ?>
        <input type="hidden" name="contactos[1][]" value="<?php echo $var->nombre ?>" >
      </div>
      <div class="form-group col-md-3">
        <label for="apellidos" class="">Apellidos : </label> <?php echo $var->apellidos ?>
        <input type="hidden" name="contactos[2][]" value="<?php echo $var->apellidos ?>" >

      </div>
      <div class="form-group col-md-3">
        <label for="dniC" class="">Celular: </label> <?php echo $var->telefono ?>
        <input type="hidden" name="contactos[3][]" value="<?php echo $var->telefono ?>" >
      </div>

    </div>

    <div class="row">
      <div class="form-group col-md-3">
        <label for="telefono" class="">Telefono: </label> <?php echo $var->telefono1 ?>
        <input type="hidden" name="contactos[4][]" value="<?php echo $var->telefono1 ?>" >
      </div>
      <div class="form-group col-md-5">
        <label for="correo" class="">Correo: </label> <?php echo $var->correo ?>
        <input type="hidden" name="contactos[5][]" value="<?php echo $var->correo ?>" >
      </div>
      <div class="form-group col-md-4">
        <label for="Parentesco" class="">Parentesco: </label> <?php echo $var->parentesco ?>
        <input type="hidden" name="contactos[6][]" value="<?php echo $var->parentesco ?>" >
      </div>

      <div class="hr-line-dashed"></div>

    </div>

  <?php endforeach ?>
<?php else: ?>
  <h2 class="text-center">Sin contactos Autorizados</h2>
  <hr>  
<?php endif ?>
<h2 class="text-center">Datos de instalacíon</h2>
<div class="row">
  <div class="form-group col-md-4">
    <label for="trabajo" class="sr-only">Trabajo</label>
    <input style="border: none;" <?php  echo $var=($ObjetoOperacion->estado==14) ? "readonly":"" ?> type="text" placeholder="Trabajo" id="trabajo" name="trabajo" class="trabajo border-bottom form-control" required value="<?php  echo $var=($ObjetoOperacion->estado==14) ? "Instalacion":"" ?>">
  </div>

  <div class="form-group col-md-4">
    <label for="tecnico" class="sr-only">Tecnicos</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="tecnico">
      <option class="text-capitalize" value="" disabled selected >Seleccione técnico</option>
      <?php Usuario::obtenerTecnicos() ?>            
    </select>
  </div>
  <div class="form-group col-md-4">
    <label for="orte" class="sr-only">Numero de Orte</label>
    <input style="border: none;" type="text" placeholder="Numero de Orte" id="orte" name="orte" class="border-bottom form-control x2 " required >
  </div>

</div> 
<div class="row">
  <div class="col-md-3">
    <label for="orte" class="sr-only">Numero de Orden</label>
    <input style="border: none;" type="text" placeholder="Numero de Orden" id="orden" name="orden" class="border-bottom form-control x2 " required >
  </div>
  <?php if ($ObjetoOperacion->lugarInstalacion=="Domicilio"): ?>
   <div class="col-md-3"><h3>Coordenadas de instalación --></h3></div>
   <div class="col-md-6">
    <input style="border: none;" type="text" placeholder="Coordenadas insalaicon" id="orden" name="orden" class="border-bottom form-control x2 " value="<?php echo $ObjetoOperacion->coordenadasInstalacion ?>" >
  </div>
<?php endif ?>

</div>
<hr>
<h4>Lugar de instalación <?php echo $ObjetoOperacion->lugarInstalacion ?> <?php echo $retVal = (!empty($ObjetoOperacion->nombreConcecionarioInstalicion)) ? $ObjetoOperacion->nombreConcecionarioInstalicion : "" ; ?></h4>
<hr>

<input type="hidden" name="idoperacion" value="<?php echo $ObjetoOperacion->idoperacion ?>">
<input type="hidden" name="idvh" value="<?php echo $vehiculoObj->idvehiculo ?>">
<input type="hidden" name="datosfacturacionid" value="<?php echo $ObjetoOperacion->datosfacturacionid ?>">
<div class="row">          
  <div class="form-group col-md-3">
    <label for="lugar" class="sr-only">Lugar</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="lugar">
      <option class="text-capitalize" value="<?php echo $ObjetoOperacion->lugarInstalacion ?>"  selected ><?php echo $ObjetoOperacion->lugarInstalacion ?></option>
      <option class="text-capitalize" value="Taller" >Taller</option>
      <option class="text-capitalize" value="Domicilio Cliente" >Domicilio Cliente</option>
      <option class="text-capitalize" value="Concesionario" >Concesionario</option>

    </select>
  </div>
  <div class="form-group col-md-9">
    <label for="direccionInstalacion" class="sr-only">Direccion Instalcion </label>
    <input style="border: none;" type="text" placeholder="Direccion Instalcion" id="direccionInstalacion" name="direccionInstalacion" class="border-bottom form-control" required value="<?php echo $ObjetoOperacion->direccionInstacion ?>">
  </div>

</div>
<?php if (isset($ObjetoOperacion->coordenadasInstalacion)): ?>
  <div class="row"><div class="col-lg-12"><label for="cordenadas">Coordenadas de instalacion: </label><label > <?php echo $ObjetoOperacion->coordenadasInstalacion ?></label><br><h2 class="text-success"><a  target="_blank" href="mapa.php?coordenadas=<?php echo $ObjetoOperacion->coordenadasInstalacion ?>&lugar=<?php echo $ObjetoOperacion->direccionInstacion ?>">ver mapa</a></h2></div></div>    
<?php endif ?>

<div class="row">
  <div class="form-group col-md-12" id="data_1">
    <label  class="sr-only">Fecha</label>
    <input type="text" id="datetimepicker3" name="fecha" required/>      
  </div> 
</div>       



<?php
}else if($vehiculoObj->estado==3){

  ?><hr>
  <div class="row">
    <div class="form-group col-md-4">
      <label for="placa" class="sr-only">Placa</label>
      <input style="border: none;"  type="text" placeholder="Placa" id="placa" name="placa" class="placa border-bottom form-control" required value="<?php echo $vehiculoObj->placa ?>">
    </div>

    <div class="form-group col-md-4">
      <label for="placa" class="sr-only">Chasis</label>
      <input style="border: none;"  type="text" placeholder="Chasis" id="placa" name="placa" class="placa border-bottom form-control" required value="<?php echo $vehiculoObj->chasis ?>">
    </div>
    <div class="form-group col-md-4">
      <label for="color" class="sr-only">Color</label>
      <input style="border: none;" type="text" placeholder="Color" id="color" name="color" class="border-bottom form-control x2 " required value="<?php echo $vehiculoObj->color ?>">
    </div>

  </div> 
  <div class="row">
    <div class="form-group col-md-4">
      <label for="marca" class="sr-only">Marca</label>
      <input style="border: none;"  type="text" placeholder="Marca" id="marca" name="marca" class="marca border-bottom form-control" required value="<?php echo $vehiculoObj->marca ?>">
    </div>

    <div class="form-group col-md-4">
      <label for="modelo" class="sr-only">Modelo</label>
      <input style="border: none;"  type="text" placeholder="Modelo" id="modelo" name="modelo" class="modelo border-bottom form-control" required value="<?php echo $vehiculoObj->modelo ?>">
    </div>
    <div class="form-group col-md-4">
      <label for="anio" class="sr-only">Ano</label>
      <input style="border: none;" type="text" placeholder="Año" id="anio" name="anio" class="border-bottom form-control x2 " required value="<?php echo $vehiculoObj->anio ?>">
    </div>

  </div>
  <hr>
  <input type="hidden" name="idv" value="<?php echo $vehiculoObj->idvehiculo ?>" >
  <div class="row">
    <div class="col-md-12">
      <fieldset>
        <h3>
          Documentos enviados
        </h3>

        <div class="checkbox checkbox-primary">
          <input id="checkbox2" type="checkbox"  name="bienvenida" value="si">
          <label for="checkbox2">
            Bienvenida
          </label>
        </div>
        <div class="checkbox checkbox-success">
          <input id="checkbox3" type="checkbox" name="usuarioContrasenia" value="si">
          <label for="checkbox3">
            Usuario y Contraseña
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox4" type="checkbox" name="contrato" value="si">
          <label for="checkbox4">
            Contrato
          </label>
        </div>
        <div class="checkbox checkbox-warning">
          <input id="checkbox5" type="checkbox" checked name="estado" value="no">
          <label for="checkbox5">
            No ubicado
          </label>
        </div>
      </fieldset>
    </div>

  </div>
  <div class="row">          
    <div class="form-group col-md-12">
      <label for="accionBienvenida" class="sr-only">Observaciones de bienvenida</label>
      <input style="border: none;" type="text" placeholder="Acciones de bienvenida" id="accionBienvenida" name="accionBienvenida" class="border-bottom form-control" required value="<?php echo $vehiculoObj->accionBienvenida ?>">
    </div>         
  </div>
  <?php

}else if($vehiculoObj->estado==4){

  ?>
  <div class="form-group col-md-4">
    <label for="trabajo" class="sr-only">Trabajo</label>
    <input style="border: none;" type="text" placeholder="Trabajo" id="trabajo" name="trabajo" class="trabajo border-bottom form-control" required value="Instalacion">
  </div>
  <?php


}

?>


<?php
} 
?> 
<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/js/bootstrap.min.js"></script>
<script src="../../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../../assets/js/inspinia.js"></script>
<script src="../../assets/js/plugins/pace/pace.min.js"></script>
<script src="../../assets/js/plugins/footable/footable.all.min.js"></script>
<!-- Steps -->
<script src="../../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../../assets/js/plugins/validate/jquery.validate.min.js"></script>

<script src="../../assets/js/plugins/select2/select2.full.min.js"></script>

<script src="../../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>




<script src="../../assets/build/jquery.datetimepicker.full.js"></script>
<script src="../../assets/js/plugins/iCheck/icheck.min.js"></script>
<script>  

  jQuery.datetimepicker.setLocale('es');
  $('#datetimepicker3').datetimepicker({
    inline:true,
    allowTimes:['9:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00']

  });
  $('#datetimepicker3').datetimepicker({value:'<?php echo $ObjetoOperacion->fechaInstalacion ?>'})
</script>


  
  <?php require_once("../../includes/init.php") ?>
  
  <?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $renoObj=new Renovaciones();
  
  $cantidadO=$renoObj->obtenerNoRenovadosCantida();
  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  // echo $cantidadO->cantidad; 
  $n=1;
  $datos=$renoObj->obtenerNoRenovadosSubidos($posisionDePagina,$registrosPorPagina);
             ?>    
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>    
       <?php $usuario=Usuario::obtenerUsuario($var->idusuario)  ?>   
       <?php if ($var->idoperacion!=0){ ?>
       <?php $opeBj=Operacion::obtenerOperacionPorId($var->idoperacion)  ?>                                      
        <?php }else{
            $opeBj=false;
          } ?>             
     <tr class="">
        <td><?php echo $n++ ?></td>
        <td><?php  echo $retVal = (!empty($var->nombreCliente)) ? $var->nombreCliente : "No Esta en la base de datos" ; ?></td>
        <td><?php echo $var->placa ?></td>
        <td><?= obtenerEstado($var->estado)  ?> <?= ($opeBj)?obtenerEstado($opeBj->estado):''  ?></td>
        <td><?= ($opeBj && !empty($opeBj->fechaInstalacion))?$opeBj->fechaInstalacion."  en ".$opeBj->lugarInstalacion:''  ?></td>
        <td><?php echo haceMinimo($var->fechaModificada) ?></td>
        <td><?php echo $var->obsDesinstalacion ?> </td>
        <td><?= strtoupper($usuario->usuarioNombre)." ".strtoupper($usuario->usuarioApellidoPaterno) ?></td>
        <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal"  data-target="#modalDes" data-id="<?php echo $var->idrenovacion ?>" >Elegir <i class="fa fa-cog"></i></button>&nbsp;</td>
    </tr> 
<?php endforeach ?>
 
              <tr>
              <td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
                    <td colspan="8">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
                    </td>
                </tr>
            <?php else: ?>
              <tr>
  <td colspan="9" class="text-center">No se encontro registro alguno    
  </td>
</tr> 
 <?php endif ?>




<?php } ?>


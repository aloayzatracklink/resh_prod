<?php require_once("../../includes/init.php") ?>
<?php if ($_POST): ?> 
<?php 
   $datos=limpiar($_POST['datos']); 
   $datosa="%".$datos."%";
 ?>  
<?php 
  $citaObj=false;
  $opeObj=false;
  if ($_POST['tipo']=='venta') {
        $opeObj=new Operacion();
      $datosA=$opeObj->obtenerpendientesComFiltro($datosa);
  } else {
     $citaObj=new Cita();
      $datosA=$citaObj->pendienteComCitaFiltro($datosa);
    // $datos=$datosFacturacion->obtenerDatosParaFacturacionRango($posisionDePagina,$registrosPorPagina,$desde,$hasta);        
  } 
$n=1;
 ?>
  <?php if ($datosA): ?>                
  <?php foreach ($datosA as $var): ?>    
  <?php if ($citaObj){ ?>
     <?php $eje=Usuario::obtenerUsuario($var->idcreador)  ?>
       <tr>
         <td><?= $n++ ?></td>
         <td><?= $var->nombreCliente ?></td>
         <td><?= $var->placa ?></td>
         <td><?= $eje->usuarioNombre." ".$eje->usuarioApellidoPaterno ?></td>
         <td><?= haceMinimo($var->fechaRegistro) ?></td>
         <td>
          <button class="btn btn-primary formCita"  style="padding-top: 0;padding-bottom: 0"  type="submit" value="<?php echo $var->idcita ?>"  >listo Sac <i class="fa fa-check-square-o"></i></button>&nbsp;          
         </td> 
       </tr>
  <?php }else{ ?>
        <?php $plataformaU=Usuario::obtenerUsuario($var->idCreador)  ?>
    <?php if ($var->idChequeo==0): ?>
    <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>
      <?php else: ?>
      <?php $clienteObj=Onix::obtenerDatosDesdeIds($var->idcliente,$var->idvehiculo,$var->idlocation) ?>
    <?php endif ?>
    <tr >
    <td><?= $n++ ?></td>  
    <?php if ($var->idChequeo==0): ?>
       <td><?= ($clienteObj->tipoCliente=="Natural") ? $clienteObj->apellidosC." ".$clienteObj->nombre : $clienteObj->razonSocial ; ?></td>
       <td><?= $datosVehiculo->placa ?></td>
     <?php else: ?>
        <?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
     <td><?= $retVal = (!empty($clienteObj->nombre)) ? "$apellidos"." "."$clienteObj->nombre" : "$apellidos" ; ?></td>       
       <td><?= $clienteObj->placa ?></td>
      <?php endif ?>
        <td><?= $plataformaU->usuarioNombre." ".$plataformaU->usuarioApellidoPaterno ?></td>
        <td><?= haceMinimo($var->fechaCreacion) ?></td>        
        <td>
            <button class="btn btn-primary formmm "  onclick="return" style="padding-top: 0;padding-bottom: 0" type="submit" value="<?php echo $var->idoperacion ?>">listo Sac <i class="fa fa-check-square-o"></i></button>&nbsp;
            
         </td>  
    </tr>

<?php } ?>
<?php endforeach ?>
// <script>
$('.formmm').click(function(e) {      
  var idoperacion=$(this).val();
  $.confirm({
      theme: 'modern',
      title: '¿Marcar como lísto para  Sac?',
      content: 'Al marcar como sac esta información sera visualizada por Sac inmediatamente',
      buttons: {
          Confirmar: {
            btnClass: 'btn-blue',
            action:function () {   
             $.ajax({
                type : 'POST',
                url  : 'reporteE/lSac',
                data :  {idoperacion:idoperacion},
                success : function(data)
                {
              $("#filter").focus();                 
              $.alert('Se realizo el cambio');                    
                }
            });        
            }
          },          
          Cancelar: {
            btnClass: 'btn-warning',
            action:function () {  
              $.alert('No se realizo nigun cambio!');
              $("#filter").focus();             
            }
          }
      }
  });
});

$('.formCita').click(function(e) {      
  var idcita=$(this).val();
  $.confirm({
      theme: 'modern',
      title: '¿Marcar como lísto para  Sac?',
      content: 'Al marcar como sac esta información sera visualizada por Sac inmediatamente',
      buttons: {
          Confirmar: {
            btnClass: 'btn-blue',
            action:function () {   
             $.ajax({
                type : 'POST',
                url  : 'reporteE/lSac',
                data :  {idcita:idcita},
                success : function(data)
                {
              $.alert('Se realizo el cambio');                    
              $("#filter").focus();                  
                }
            });        
            }
          },          
          Cancelar: {
            btnClass: 'btn-warning',
            action:function () {  
              $.alert('No se realizo nigun cambio!');
              $("#filter").focus(); 
            
            }
          }
      }
  });
});

  
// });
</script>
   <?php else: ?>
   <tr>   
        <td colspan="8" class="text-center">No se encontro ningun registro con <h2><?= $_POST['datos'] ?>.</h2> </td>
   </tr>
   <?php endif ?>   
 <?php endif ?> 

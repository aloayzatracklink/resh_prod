<?php require_once("../../includes/init.php");

if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    if (isset($_POST["page"])) {
        $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
        if (!is_numeric($numeroDePagina)) {
            die('Numero de pagina incorrecta !');
        } //incase of invalid page number
    } else {
        $numeroDePagina = 1; //if there's no page number, set it to 1
    }
    $desde ="";
    $hasta ="";

    if (isset($_POST["fecha"])) {
        if($_POST["fecha"]){
        $fecha=strip_tags($_POST['fecha']);
        $desde=trim(current(explode('/', $fecha)));
        $tmp=explode('/', $fecha);
        $hasta=end($tmp);
        $desde=date("YmdHis",strtotime($desde));
        $hasta=date("Ymd",strtotime($hasta));
        $hasta=$hasta."235959";
        }
    }
    $datos="";
    if (isset($_POST["datos"])) {
        if($_POST["datos"])
           $datos = limpiar($_POST['datos']);
    }
    $objAn = new Renovaciones();

    $cantidadO = $objAn->buscarParaFiltroSeguimientoTT($datos,$desde,$hasta);

    $registrosPorPagina = 20;
    $paginasEnTotal = ceil($cantidadO->cantidad / $registrosPorPagina);
    $posisionDePagina = (($numeroDePagina - 1) * $registrosPorPagina);

    $obj=Renovaciones::buscarParaFiltroSeguimiento($datos,$desde,$hasta,$posisionDePagina,$registrosPorPagina);$n=0; ?>
    <?php if($obj){
        foreach ($obj as $var):
            $datosF = DatosFacturacion::obtenerDatosFacturacionPorIdRenovacion($var->idrenovacion);
          $n++; ?>
            <tr class="gradeX">
                <td class="text-left "><?= $n; ?></td>
                <td class="text-left">
                    <?php  echo $retVal = (!empty($var->nombreCliente)) ? $var->nombreCliente : "No Esta en la base de datos" ; ?>
                    <input type="hidden" name="idanoma" value="<?= $var->idrenovacion?>" >
                </td>
                <td class="text-left"><?= $var->chasis." | ".$var->placa?> </td>
                <td><?php echo $retVal = (!empty($var->vence)) ? formatoSinHora($var->vence) : "" ;  ?></td>
                <td><?php echo haceMinimo($var->fechaCreada) ?></td>
                <td><?= haceMinimo($var->fechaModificada) ?></td>
                <td><?php echo $var->observacion ?></td>
                <td> <?= obtenerEstado($datosF->estadoFacturacion) ?> </td>
                <td class="text-center"><?= obtenerEstado($var->estado)?> </td>
                <?php $usuarioEje=Usuario::obtenerUsuario($var->idejecutivo);  ?>
                <td style="background-color: #8befda"><?php  echo $usuarioEje->usuarioNombre ?> <?php echo $usuarioEje->usuarioApellidoPaterno?></td>

                <?php $usuarioObj=Usuario::obtenerUsuario($var->idusuario);  ?>
                <td style="background-color: #86d7fd"><?php  echo $usuarioObj->usuarioNombre ?> <?php echo $usuarioObj->usuarioApellidoPaterno?></td>

                <td>
                    <?php if($var->estado!=0){?>
                    <button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#verRenovacion" data-id="<?php echo $var->idrenovacion ?>">Elegir <i class="fa fa-cog"></i></button>
                   <?php }?>
            </td>
            </tr>
        <?php endforeach;?>
        <tr>
            <td colspan="11"></td>
            <td colspan="2" class="text-right">Mostrando <?= $retVal = ($n<$registrosPorPagina) ? $n : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
        </tr>
    <?php } else{?>
        <tr>
            <td colspan="16" class="text-center">No hay información Registrada</td>
        </tr>
    <?php }?>
<?php } ?>


<?php 
require_once("../../includes/init.php") ;
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }  

  $opeObj=new DatosInstalacion();

  if ($_POST['tipo']=='temporales'){    
      $cantidadO=$opeObj->listaTemporalesInstalacionesCantidad();  
      $registrosPorPagina=10;
      $paginasEnTotal=ceil($cantidadO/$registrosPorPagina);
      $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
      $datos=$opeObj->listaTemporalesInstalaciones($posisionDePagina,$registrosPorPagina);

  }else{    
      $cantidadO=$opeObj->listaTemporalesInstalacionesCantidadFLUJO();  
      $registrosPorPagina=10;
      $paginasEnTotal=ceil($cantidadO/$registrosPorPagina);
      $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
      $datos=$opeObj->listaTemporalesInstalacionesFLUJO($posisionDePagina,$registrosPorPagina);
  }  


?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?> 
  <?php if ($_POST['tipo']!='temporales'){ ?>
    <tr>
        <td><?= $var->orte."|".formatoSinHora($var->fecha)."|".$var->estado  ?></td>
        <td><?= $var->cliente  ?></td>
        <td><?= $var->placa."|".$var->chasis  ?></td>
        <td><?= "-"  ?></td>
        <td><?= $var->operador  ?></td>
        <td><?= haceMinimo($var->fechaRegistro)  ?></td>
        <td><a class="btn btn-primary"  onclick="return confirm('¿Estas seguro de resolver el operacion?')"
        href="/rpr/restmp?idope=<?= $var->iddatosInstalacion ?>"><span class="fa fa-check"></span></a></td>
    </tr>
  <?php }else{ ?>
    <tr>
        <td><?= $var->orte."|".formatoSinHora($var->fecha)  ?></td>
        <td><?= $var->cliente  ?></td>
        <td><?= $var->placa."|".$var->chasis  ?></td>
        <td><?= $var->operador  ?></td>
        <td><?= "-"  ?></td>
        <td><?= haceMinimo($var->fechaRegistro)  ?></td>
        <td><a class="btn btn-primary" onclick="return confirm('¿Estas seguro de resolver el temporal?')"
        href="/rpr/restmp?idins=<?= $var->iddatosInstalacion ?>"><span class="fa fa-check"></span></a></td>
    </tr>

<?php } ?>
<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO<$registrosPorPagina) ? $cantidadO : $registrosPorPagina ; ?> de <?= $cantidadO  ?></td>
  <td colspan="10">                        
    <?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="10" class="text-center">No se encontro registro alguno</td>
</tr>   
<?php endif ?>
<?php } ?>

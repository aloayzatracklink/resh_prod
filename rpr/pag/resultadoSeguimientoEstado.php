<?php require_once("../../includes/init.php") ?>
<?php 
   $datos=limpiar($_POST['datos']); 
   $tipo=$_POST['tipoV'];
   $datos="%".$datos."%";
   $fecha='';
  if (isset($_POST["fecha"])) {
    $fecha=$_POST['fecha'];
    $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
    $desde=date("YmdHis",strtotime($desde));  
    $hasta=date("Ymd",strtotime($hasta)); 
    $hasta=$hasta."235959";
  }
   if($tipo=='ventas')
   {
 ?>  
<?php 
  $opOBJ=new Operacion;
  if (empty($fecha)) {
    $datosA=$opOBJ->buscarParaFiltroDatosOPECSeguimiento($datos);
  } else {
    $datosA=$opOBJ->buscarParaFiltroOPECFechaSeguimientoCom($datos,$desde,$hasta);  
  }
  $n=1;
 ?>
  <?php if ($datosA): ?>                
  <?php foreach ($datosA as $var): ?>    
        <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
        <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>
        <?php $datosF=DatosFacturacion::obtenerDatosFacturacionPorId($var->datosfacturacionid) ?>
        <?php $usuarioV=Usuario::obtenerUsuario($datosF->idUsuarioCreado)  ?>
        <?php $vhOC=Vehiculo::verificarSiEstaDuplicado($datosVehiculo->chasis) ?>
       <?php $datosOpera=Operacion::obtenerOperacionesPorVehiculoIDClienteId($var->idvehiculo,$var->idcliente); ?>
        <tr class='<?= obtenerClassSinClass($var->lugarInstalacion) ?> <?= ($vhOC->cantidad>1)?"danger":"" ?>' >
        <td ><span class="label label-primary"><?= $datosOpera->trabajo ?></span></td>
        <td><?= ($clienteObj->tipoCliente=="Natural") ? $clienteObj->apellidosC." ".$clienteObj->nombre : $clienteObj->razonSocial ; ?></td>
        <td class="center"> Placa: <?= $datosVehiculo->placa ?> <br><?= $datosVehiculo->chasis ?></td>
        <td class="center"><?= haceMinimo($datosVehiculo->fechaCreacion) ?></td>
        <td class="center"><?= !empty($var->fechaInstalacion)?formatoHaceEn($var->fechaInstalacion):"Pendiente" ?></td>
        <td class="center"><?= obtenerEstado($var->estado) ?></td>
        <td class="center"><?= $usuarioV->usuarioNombre;?>&nbsp;<?= $usuarioV->usuarioApellidoPaterno;  ?></td>
        <td class="center"><?= strtoupper($datosF->nombreCanal);  ?></td>
            <td class="center"><strong class="font-bold">
                    <?php $usuario=Usuario::obtenerUsuario($var->idCreador);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno; ?> <br><small style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px"><?php echo formato($var->fechaCreacion)  ?></small>
                </strong></td>
    </tr>
<?php endforeach ?>
   <?php else: ?>
   <tr>   
        <td colspan="8" class="text-center">No se encontro ningun registro con <h2><?= $_POST['datos'] ?>.</h2> </td>
   </tr>
   <?php endif ?>   
<?php
   }else if($tipo=='citas'){
 ?>  
<?php 
  $citaObj=new Cita;
  if (empty($fecha)) {
    $datosA=$citaObj->seguimientoComCitaFiltro($datos);
  } else {
    $datosA=$citaObj->seguimientoComCitaFiltroFecha($datos,$desde,$hasta);  
  }
  $n=1;
 ?>
  <?php if ($datosA): ?>                
  <?php foreach ($datosA as $var): ?>    
        <?php $usuarioV=Usuario::obtenerUsuario($var->idcreador)  ?>
        <?php $datosCanal=Canales::obtenerDatosCanalPorId($var->idcanal) ?>

    <tr >
    <td><?= $var->nombreCliente ?></td>
        <td class="center"> Placa: <?= $var->placa ?></td>
        <td class="center"><?= haceMinimo($var->fechaRegistro) ?></td>
        <td class="center"><?= !empty($var->fecha)?formatoHaceEn($var->fecha):"Pendiente" ?></td>    
        <td class="center"><?= obtenerEstado($var->idestado) ?></td>    
        <td class="center"><?= $usuarioV->usuarioNombre;?>&nbsp;<?= $usuarioV->usuarioApellidoPaterno;  ?></td>   
        <td class="center"><?= ($datosCanal)?$datosCanal->nombreCanal." -  ".$datosCanal->ejecutivoCanal:'Pendiente';  ?></td>
    </tr>
<?php endforeach ?>
   <?php else: ?>
   <tr>   
        <td colspan="8" class="text-center">No se encontro ningun registro con <h2><?= $_POST['datos'] ?>.</h2> </td>
   </tr>
   <?php endif ?>   
<?php
   }  
?>

<?php $nav="editarPerfil" ;?>
<?php include("includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("ingresar");
} ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php 
$o=Usuario::obtenerUsuario($session->idusuario);
$objetoUsuario=new Usuario();
$errores=array();
$exito=array();
if (isset($_POST['guardar'])) {
    $contrasenia=trim($_POST['contrasenia']);
    $contrasenia=hash('sha512', $contrasenia);
    // if($objetoUsuario->validarContrasenia($contrasenia)){
    if (!empty($_FILES['foto']['name'])) {
        $extencionesAbilitadas= array('jpg','jpeg','png','gif','');
        $nombreArchivo=$_FILES['foto']['name'];
        $extencionArchivo=explode('.', $nombreArchivo);
        $extencionArchivo=strtolower(end($extencionArchivo));
        $tamanioArchivo=$_FILES['foto']['size'];
        $ubicacionTemporal=$_FILES['foto']['tmp_name'];

        if (!in_array($extencionArchivo,$extencionesAbilitadas)) {
            $errores[]='La extencion de la imagen deve ser jpg jpeg png gif';
        }
        if ($tamanioArchivo>5242880) {
            $errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
        }

    }
     /// ahora validamos la contrasenia...
        $contrasenia1=$_POST['contrasenia1'];
        $contrasenia2=$_POST['contrasenia2'];
        if (!empty($_POST['contrasenia1'])) {              

            if ($contrasenia1<>$contrasenia2) {
                $errores[]="Las nuevas contraseñas no coinciden";
            }else
            if(empty($contrasenia1)|| empty($contrasenia2)){
                $errores[]="Completa las cotraseñas correctamente";
            }else{
                $contrasenia1=hash('sha512', $contrasenia1);
                $objetoUsuario->sobreMi=$_POST['sobreMi'];
                $objetoUsuario->contrasenia=$contrasenia1;
                if (!empty($_FILES['foto']['name'])) {
                    $objetoUsuario->fotoPerfil=$nombreArchivo;
                    move_uploaded_file($ubicacionTemporal, 'public/perfil/'.$nombreArchivo);                    
                }else{
                    $objetoUsuario->fotoPerfil=$o->fotoPerfil;    
                }
                $objetoUsuario->celular=$_POST['celular'];
                $objetoUsuario->celular1=$_POST['celular1'];
                if($objetoUsuario->actualizarPerfilContrasenia($o->idusuario)){
                    $exito[]="los datos y la contraseña fueron modificados  correctamente";
                    
                }

            }
        }
        else{
            $objetoUsuario->sobreMi=$_POST['sobreMi'];
            if (!empty($_FILES['foto']['name'])) {
                $objetoUsuario->fotoPerfil=$nombreArchivo;
                move_uploaded_file($ubicacionTemporal, 'public/perfil/'.$nombreArchivo);
            }else{
                $objetoUsuario->fotoPerfil=$o->fotoPerfil;   
            }
            $objetoUsuario->anexo=$_POST['anexo'];
            $objetoUsuario->celular=$_POST['celular'];
            $objetoUsuario->celular1=$_POST['celular1'];
            if($objetoUsuario->actualizarPerfil($o->idusuario)){
            $exito[]=" Datos modificados ";
            }
        }
    // }else{
    //      $errores[]="Ingresarcontraseña correctamente";
    // }
}

?>
<link href="assets/css/plugins/dropzone/basic.css" rel="stylesheet">
<link href="assets/css/plugins/dropzone/dropzone.css" rel="stylesheet">
<link href="assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="assets/css/plugins/codemirror/codemirror.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
               <?php include("includes/menuIzquierdo.php") ?>
           </div>
       </nav>

       <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("includes/navegacionArriba.php") ?>

            </nav>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h2 class="text-center">Modificar Contraseña </h2>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form method="post" class="form-horizontal" enctype="multipart/form-data">
                         

                      
                            <div class="form-group"><label class="col-sm-2 control-label">Contraseña</label>

                                <div class="col-sm-10"><input type="password" class="form-control" name="contrasenia" readonly required><span class="help-block m-b-none" >Por defecto es el nombre de usuario</span></div>
                            </div>
                            <div class="form-group"><label class="col-lg-2 control-label">Nombres Completos</label>

                                <div class="col-lg-10"><input  type="text" disabled="" placeholder="Nombres Completos" class="form-control" value="<?php $usuario=Usuario::obtenerUsuario($session->idusuario);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno; echo "&nbsp;"; echo $usuario->usuarioApellidoMaterno; ?>&nbsp;" ></div>
                            </div>
                            <div class="form-group"><label class="col-lg-2 control-label">Usuario</label>

                                <div class="col-lg-10"><input type="text" disabled="" placeholder="Usuario" class="form-control" value="<?php $usuario=Usuario::obtenerUsuario($session->idusuario);?>&nbsp;<?php echo $usuario->usuario; ?>&nbsp;"></div>
                            </div>
<!-- 

                        <div class="form-group"><label class="col-sm-2 control-label">Normal</label>

                            <div class="col-sm-10"><input type="text" class="form-control"></div>
                        </div> -->
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Sobre mi:</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="sobreMi" value="<?php echo  $retVal = (isset($o->sobreMi)) ? "$o->sobreMi" : "" ; ?>"> <span class="help-block m-b-none" >Describir una brebe descripcion ....</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>

                        <div class="hr-line-dashed"></div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label text-danger">Nueva Contraseña</label>

                            <div class="col-sm-10"><input type="password" class="form-control" name="contrasenia1" readonly=""></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label text-danger">Confirmar Nueva Contraseña</label>

                            <div class="col-sm-10"><input type="password" class="form-control" name="contrasenia2" readonly=""></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Foto de perfil</label>

                            <div class="col-sm-8"><div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput">
                                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                    <span class="fileinput-new">Seleccionar foto de perfil</span>
                                    <span class="fileinput-exists">Cambiar</span>
                                    <input type="file" name="foto"/>
                                </span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
                            </div> </div><div class="col-sm-2"><span>
                            <img alt="public/perfil/<?php echo  $retVal = (isset($o->fotoPerfil) && $o->fotoPerfil != "") ? "$o->fotoPerfil" : "pefilpordefecto.png" ; ?>" class="img-circle" src="public/perfil/<?php echo  $retVal = (isset($o->fotoPerfil) && $o->fotoPerfil != "") ? "$o->fotoPerfil" : "pefilpordefecto.png" ; ?>" width="60" />
                        </span></div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-lg-2 control-label">Area:</label>

                        <div class="col-lg-4"><p class="form-control-static"><?php $usuario=Usuario::obtenerRoldeUsuario($usuario->idrol); echo $usuario->area ;echo "&nbsp;-";echo "&nbsp;" ;echo $usuario->cargo;?></p></div><div class="col-lg-2"><label class="col-lg-2 control-label">Anexo: </label></div><div class="col-lg-4"><input type="text" class="form-control" name="anexo" value="<?php echo  $retVal = (isset($o->anexo)) ? "$o->anexo" : "" ; ?>"></div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group"><label class="col-lg-2 control-label">Celular:</label>

                        <div class="col-lg-4"><input type="text" class="form-control" name="celular" value="<?php echo  $retVal = (isset($o->celular)) ? "$o->celular" : "" ; ?>"> <span class="help-block m-b-none"></div><div class="col-lg-2"><label class="col-lg-2 control-label">Celular: </label></div><div class="col-lg-4"><input type="text" class="form-control" name="celular1" value="<?php echo  $retVal = (isset($o->celular1)) ? "$o->celular1" : "" ; ?>"></div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="submit" name="cancelar">Cancelar .. </button>
                            <button class="btn btn-primary" type="submit" name="guardar">Guardar cambios</button>
                        </div>
                    </div>
                    <br>
                </form>



    <div class="row">
        <div class="col-lg-12">
            <br>
            <?php include("includes/footer.php") ?>
        </div>
    </div>

</div>

<script src="assets/js/jquery-2.1.1.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="assets/js/inspinia.js"></script>
<script src="assets/js/plugins/pace/pace.min.js"></script>
<script src="assets/js/plugins/footable/footable.all.min.js"></script>
<script src="assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="assets/js/plugins/toastr/toastr.min.js"></script>
    <!-- toastr.success('We do have the Kapua suite available.', 'Turtle Bay Resort', {timeOut: 5000,progressBar:true}) -->
<script>
<?php foreach ($errores as $error): ?>
    toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true}) 
  <?php endforeach ?> 
<?php foreach ($exito as $ex): ?>
    toastr.success('<?php echo $ex ?>', 'Exito', {timeOut: 5000,progressBar:true,closeButton:true}) 
<?php endforeach ?>  

</script>
    
<?php include_once('includes/script.php') ?>
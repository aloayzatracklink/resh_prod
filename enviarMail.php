<?php 

include("./includes/head.php");
require_once './mailphp/PHPMailer/PHPMailerAutoload.php';

$poliza = "FHSKA123129MSM";

$thehtml="<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<meta name='viewport' content='width=device-width'>
<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<title></title><style type='text/css' id='media-query'>
body {
margin: 0;
padding: 0; }
table, tr, td {
vertical-align: top;
border-collapse: collapse; }
.ie-browser table, .mso-container table {
table-layout: fixed; }
* {
line-height: inherit; }
a[x-apple-data-detectors=true] {
color: inherit !important;
text-decoration: none !important; }
[owa] .img-container div, [owa] .img-container button {
display: block !important; }
[owa] .fullwidth button {
width: 100% !important; }
.ie-browser .col, [owa] .block-grid .col {
display: table-cell;
float: none !important;
vertical-align: top; }
.ie-browser .num12, .ie-browser .block-grid, [owa] .num12, [owa] .block-grid {
width: 500px !important; }
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
line-height: 100%; }
.ie-browser .mixed-two-up .num4, [owa] .mixed-two-up .num4 {
width: 164px !important; }
.ie-browser .mixed-two-up .num8, [owa] .mixed-two-up .num8 {
width: 328px !important; }
.ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {
width: 250px !important; }
.ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {
width: 166px !important; }
.ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {
width: 125px !important; }
.ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {
width: 100px !important; }
.ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {
width: 83px !important; }
.ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {
width: 71px !important; }
.ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {
width: 62px !important; }
.ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {
width: 55px !important; }
.ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {
width: 50px !important; }
.ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {
width: 45px !important; }
.ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {
width: 41px !important; }
@media only screen and (min-width: 520px) {
.block-grid {
width: 500px !important; }
.block-grid .col {
display: table-cell;
Float: none !important;
vertical-align: top; }
.block-grid .col.num12 {
width: 500px !important; }
.block-grid.mixed-two-up .col.num4 {
width: 164px !important; }
.block-grid.mixed-two-up .col.num8 {
width: 328px !important; }
.block-grid.two-up .col {
width: 250px !important; }
.block-grid.three-up .col {
width: 166px !important; }
.block-grid.four-up .col {
width: 125px !important; }
.block-grid.five-up .col {
width: 100px !important; }
.block-grid.six-up .col {
width: 83px !important; }
.block-grid.seven-up .col {
width: 71px !important; }
.block-grid.eight-up .col {
width: 62px !important; }
.block-grid.nine-up .col {
width: 55px !important; }
.block-grid.ten-up .col {
width: 50px !important; }
.block-grid.eleven-up .col {
width: 45px !important; }
.block-grid.twelve-up .col {
width: 41px !important; } }
@media (max-width: 520px) {
.block-grid, .col {
min-width: 320px !important;
max-width: 100% !important; }
.block-grid {
width: calc(100% - 40px) !important; }
.col {
width: 100% !important; }
.col > div {
margin: 0 auto; }
img.fullwidth {
max-width: 100% !important; } }
</style>
</head>
<body class='clean-body' style='margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #FFFFFF'>
<div class='nl-container' style='min-width: 320px;Margin: 0 auto;background-color: #FFFFFF'>
<div style='background-color:#003399;'>
  <div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
    <div style='border-collapse: collapse;display: table;width: 100%;'>
      <div class='col num12' style='min-width: 320px;max-width: 500px;width: 500px;width: calc(18000% - 89500px);background-color: transparent;'>
        <div style='background-color: transparent; width: 100% !important;'>
          <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 5px; padding-left: 5px;'>
            <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
              <div align='center'><div style='border-top: 10px solid transparent; width:100%; line-height:0px;'>&nbsp;</div></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>    
<div style='background-color:#e7e7e7;'>
<div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid two-up'>
  <div style='border-collapse: collapse;display: table;width: 100%;'>
    <div class='col num6' style='Float: left;max-width: 320px;min-width: 250px;width: 250px;width: calc(35250px - 7000%);background-color: transparent;'>
      <div style='background-color: transparent; width: 100% !important;'>
        <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 5px; padding-left: 5px;'>
          <div align='center' class='img-container center' style='padding-right: 0px;  padding-left: 0px;'>
            <a href='http://www.tracklink.pe/TracklinkPeru/' target='_blank'>
              <img class='center' align='center' border='0' src='cid:logo' alt='Image' title='Tracklink' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;width: 100%;max-width: 217px' width='217'>
            </a>
          </div>
        </div>
      </div>
    </div>
<div class='col num6' style='Float: left;max-width: 320px;min-width: 250px;width: 250px;width: calc(35250px - 7000%);background-color: transparent;'>
  <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:15px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;'>
      <div align='center' class='img-container center' style='padding-right: 0px;  padding-left: 0px;'>
        <a href='http://www.tracklink.pe/TracklinkPeru/' target='_blank'>
          <img class='center' align='center' border='0' src='cid:eCall' alt='Image' title='Octo' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;width: 100%;max-width: 100px' width='100'>
        </a>
      </div>
    </div>
  </div>
</div>
  </div>
</div>
</div>    
<div style='background-color:#EDEDED;'>
<div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
  <div style='border-collapse: collapse;display: table;width: 100%;'>

    <div class='col num12' style='min-width: 320px;max-width: 500px;width: 500px;width: calc(18000% - 89500px);background-color: transparent;'>
      <div style='background-color: transparent; width: 100% !important;'>
        <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'>
          <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
           <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px'><strong>Buen dia</strong><br>Estimados se les comunica que se ha detectado una nueva poliza de pacifico.</div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div> 

<div style='background-color:transparent;'>
  <div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid mixed-two-up'>
    <div style='border-collapse: collapse;display: table;width: 100%;'>
      <div class='col num4' style='Float: left;max-width: 320px;min-width: 164px;width: 167px;width: calc(78164px - 15600%);background-color: transparent;'>
        <div style='background-color: transparent; width: 100% !important;'>
          <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 5px; padding-left: 5px;'>
            <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
             <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px'><strong><span style='font-size: 14px; line-height: 16px;'>Poliza:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$poliza</span></strong></p></div>     
         </div>
       </div>
     </div>

 </div>
</div>
</div>    

</div> 

<div style='background-color:#e7e7e7;'>
    <br>
    <br>
    <div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
        <p align='center'><strong>Atte. RESH</strong></p>
    </div>
    <div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
        <div style='border-collapse: collapse;display: table;width: 100%;'>

        <div class='col num12' style='min-width: 320px;max-width: 500px;width: 500px;width: calc(18000% - 89500px);background-color: transparent;'>
            <div style='background-color: transparent; width: 100% !important;'>
            <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'>
                <div align='center' style='padding-right: 10px; padding-left: 10px; padding-bottom: 10px;'>
                <div style='line-height:10px;font-size:1px'>&nbsp;</div>
                <div style='display: table; max-width:151;'>
                    <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px'>
                    <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
                        <a href='https://www.facebook.com/tracklinkperu/' title='Facebook' target='_blank'>
                        <img src='cid:facebook' alt='Facebook' title='Facebook' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
                        </a>
                        <div style='line-height:5px;font-size:1px'>&nbsp;</div>
                    </td></tr>
                    </tbody></table>
                    <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px'>
                    <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
                        <a href='http://twitter.com/TracklinkPE/' title='Twitter' target='_blank'>
                        <img src='cid:twitter' alt='Twitter' title='Twitter' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
                        </a>
                        <div style='line-height:5px;font-size:1px'>&nbsp;</div>
                    </td></tr>
                    </tbody></table>
                    <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 0'>
                    <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
                        <a href='https://www.youtube.com/channel/UCylRCGZ4LBaNKk-XMJliURw' title='YouTube' target='_blank'>
                        <img src='cid:youtube@2x' alt='YouTube' title='YouTube' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
                        </a>
                        <div style='line-height:5px;font-size:1px'>&nbsp;</div>
                    </td></tr>
                    </tbody></table>
                </div>
                </div>             
                <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
                <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 17px;text-align: center'>TRACKLINK OCTO</p><p style='margin: 0;font-size: 14px;line-height: 16px;text-align: center'> Calle Ricardo Angulo 762 Urb Corpac - San Isidro, Lima. <br><a style='color:#0068A5;text-decoration: underline;' title='Tracklink' href='http://www.tracklink.pe' target='_blank' rel='noopener noreferrer'>www.tracklink.pe</a>&nbsp;/ <a style='color:#0068A5;text-decoration: underline;' title='Octo' href='http://www.octotelematics.com' target='_blank' rel='noopener noreferrer'>www.octotelematics.com</a></p></div>
            </div>           
            </div>
        </div>
        </div>          
        </div>
    </div>
</div>   
</div>
</body>
</html>";


//El correo para el cliente aqui /// <---->
$mail = new PHPMailer;
$mail->isSMTP();
$mail->SetLanguage( 'es', 'mailphp/PHPMailer/language/');
$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->Host = gethostbyname('mail.tracklink.pe');
$mail->Port = 25;
$mail->SMTPAuth = false;
$mail->CharSet = 'UTF-8';
$mail->setFrom('no-responder@tracklink.pe', 'Resh de Tracklink');  

$mail->addAddress("aloayza@tracklink.pe","Alex"); // aquien se manda el mensaje
// $mail->addAddress("drodriguez@tracklink.pe","Daniel Rodriguez"); // aquien se manda el mensaje
// $mail->addAddress("rmiranda@tracklink.pe","Renzo Miranda Quiñones"); // aquien se manda el mensaje
// $mail->addAddress("aquichiz@tracklink.pe","Andres Quichiz"); // aquien se manda el mensaje
//CC and BCC
// $mail->addCC("pruebas-ti@tracklink.pe");
// $mail->addBCC("pruebas-ti@tracklink.pe");
// $mail->addBCC("lgiordanelli@tracklink.pe");
//Set the subject line
$mail->Subject = 'Nueva Poliza detectada en el Sistema - RESH'; //// ASUNTO //////
//Read an HTML message body from an external file, convert referenced images to embedded,
$mail->AddEmbeddedImage('./assets/img/face.png', 'facebook');
$mail->AddEmbeddedImage('./assets/img/twitter.png', 'twitter');
$mail->AddEmbeddedImage('./assets/img/youtube@2x.png', 'youtube@2x');
$mail->AddEmbeddedImage('./assets/img/track_new.png', 'logo');    
$mail->AddEmbeddedImage('./assets/img/ecall_new.png', 'eCall');
// $mail->AddEmbeddedImage('./assets/img/crash2.png', 'crash');
// $mail->AddEmbeddedImage('img/tracklink.png', 'tracklink');
//convert HTML into a basic plain-text alternative body
$mail->msgHTML($thehtml);
//Replace the plain text body with one created manually

$nombreCliente = "Alex Loayza";
$placa = "BBB-000";

$mail->AltBody = "Estimado(as) hemos detectado una nueva poliza en el sistema"; ////////ESTE SE EL CONTENIDO//// QUE SE BERA EN PRIERA INSTANACIA
//Attach an image file
  //aqui se adjunta los archivos que queremos enviar :D

// $mail->addAttachment('images/phpmailer_mini.png');

//send the message, check for errors
if (!$mail->send()) {
  echo "Mailer Error: " . $mail->ErrorInfo;
 } else {
    // echo "Correo Enviado";
    $url="int/seguimientoChoque.php?id=$choqueObj->idchoque";
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>'; exit;
  //   header("Location:detalleRobo.php?id=$robo->idrobo");
  // echo "enviado";
 }
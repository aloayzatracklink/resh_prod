<?php
  if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    require_once("../../includes/init.php"); 
  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1;
  }
    require_once("../../includes/init.php"); 
   // echo "<pre>";
   // var_dump($_POST);
        $datos=false;
    if(6<=strlen($_POST['datosS'])){
        $datos=trim(limpiar($_POST['datosS']));
        $onyxObj=new Onix;
        $cantidadO=$onyxObj->obtenerIdclientePorDatosDeVhCantidad($datos);    
        $registrosPorPagina='10';
        $paginasEnTotal=ceil($cantidadO/$registrosPorPagina);
        $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
        $datos=$onyxObj->obtenerIdclientePorDatosDeVh($datos,$posisionDePagina,$registrosPorPagina);
    }else{
$datos=false;
    }
    if ($datos) {
        foreach($datos as $var):
       ?>
 <tr class=''> 
    <td><?= $var->apellidos." ".$var->nombre ?></td>
    <td><?= $var->placa ?></td>
    <td><?= $var->chasis ?></td>
    <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" data-id="<?= $var->idcustomer.",".$var->idvehicle.",".$var->idlocation ?>">Elegir<i class="fa fa-cog"></i></button>&nbsp;</td>                    
</tr>         
       <?php 
        endforeach;
        ?>
<tr>
    <td>Mostrando <?= ($cantidadO<$registrosPorPagina) ? $cantidadO : $registrosPorPagina ; ?> de <?= $cantidadO  ?></td>
    <td colspan="8">                        
        <?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO,$paginasEnTotal) ?>
    </td>
</tr>
        <?php
    }
?>
<?php
  }
?>

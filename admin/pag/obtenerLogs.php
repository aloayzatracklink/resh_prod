<?php include("../../includes/init.php") ?>
<?php
if(isset($_POST)){

    $registrosPorPagina=10;

    if(isset($_POST['page'])){
        $numeroDePagina = $_POST['page'];
        
    }else{
        $numeroDePagina = 1;
    }

    $cantidadO= AuditoriaSistemas::obtenerCantidadTotal();
    $paginasEnTotal=ceil($cantidadO[0]->cantidad/$registrosPorPagina);
    $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);

    // if($numeroDePagina != 1 && $numeroDePagina > $paginasEnTotal){
    //     header('HTTP/1.1 500 Internal Server Booboo');
    //     header('Content-Type: application/json; charset=UTF-8');
    //     die(json_encode(array('message' => 'ERROR', 'code' => 1337)));
    // }

    //* Filter By process Name
    if(isset($_POST['processName'])){
        $filter = $_POST['processName'];
        $logs = AuditoriaSistemas::obtenerLogsPorProceso($filter, $posisionDePagina, $registrosPorPagina);
    }elseif(isset($_POST['searchText']) && $_POST['searchText'] != ""){
        $searchText = $_POST['searchText'];
        $logs = AuditoriaSistemas::obtenerLogsPorBuscador($searchText);
    }else{
        $logs = AuditoriaSistemas::obtenerLogsPorPaginaGeneral($posisionDePagina, $registrosPorPagina);
    }

    if(count($logs) == 0){
        header('HTTP/1.1 500 Internal Server Booboo');
        header('Content-Type: application/json; charset=UTF-8');
        die(json_encode(array('message' => 'ERROR', 'code' => 1337)));
    }
?>
<?php
if($logs){
    $counter = 1;
    foreach($logs as $log){ ?>
        <tr>
            <td><?= $counter++ ?></td>
            <td><?= utf8_encode($log->message) ?></td>
            <td class="text-center"><span class="badge badge-warning text-xl">JSON</span></td>
            <td class="text-center">
                <?php if($log->statusCode >= 200 && $log->statusCode < 400): ?>
                    <span class="badge badge-primary text-xl"><?= $log->statusCode ?></span>
                <?php endif ?>
                <?php if($log->statusCode >= 400 && $log->statusCode < 500): ?>
                    <span class="badge badge-danger text-xl"><?= $log->statusCode ?></span>
                <?php endif ?>
                <?php if($log->statusCode >= 500 && $log->statusCode < 600): ?>
                    <span class="badge badge-warning text-xl"><?= $log->statusCode ?></span>
                <?php endif ?>
            </td>
            <td class="text-center"><span class="badge badge-warning text-xl">JSON</span></td>
            <td class="text-center"><span class="text-xl"><?= $log->scriptName ?></span></td>
            <td class="text-center"><span class="badge badge-success text-xl"><?= $log->jobType ?></span></td>
            <td class="text-center"><span class="badge badge-name text-xl"><?= $log->scriptReference ?></span></td>
            <td><button class="btn btn-primary" data-toggle="modal" data-target="#verLog" data-id="<?= $log->id ?>"><i class="fa fa-eye"></i></button></td>
        </tr>
    <?php } ?>
<?php } ?>
<?php }else{ ?>
    <tr><td colspan="78" class="text-center">No se encontro registro alguno</td></tr>
<?php } ?>
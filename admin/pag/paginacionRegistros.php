<?php 
  if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    require_once("../../includes/init.php"); 
  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
        if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
        }else{
            $numeroDePagina = 1;
        }
      $fechaCheckB='';
      if (isset($_POST['fechaCheckB'])) {
        $fechaCheckB=limpiar($_POST['fechaCheckB']);
      }
      $datos=false;
      $tipo=limpiar($_POST['tipo']);
      $registrosPorPagina='10';
      $mes=limpiar($_POST['mes']);
      $anio=limpiar($_POST['anio']);
      $datosInstObj=new DatosInstalacion;
      if (!empty($fechaCheckB)) {
          if ($fechaCheckB=='mes') {
            $cantidadO=$datosInstObj->obtenerTodosRegistrosMes();    
            $registrosPorPagina=10;
            $paginasEnTotal=ceil($cantidadO/$registrosPorPagina);
            $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
            $datos=$datosInstObj->obtenerTodosRegistrosMesData($posisionDePagina,$registrosPorPagina);
          }
          if ($fechaCheckB=='hoy') {
            $cantidadO=$datosInstObj->obtenerTodosRegistrosHoy();    
            $registrosPorPagina=10;
            $paginasEnTotal=ceil($cantidadO/$registrosPorPagina);
            $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
            $datos=$datosInstObj->obtenerTodosRegistrosHoyData($posisionDePagina,$registrosPorPagina);
          }
          if ($fechaCheckB=='todos') {
            $cantidadO=$datosInstObj->obtenerTodosRegistrosTodos();    
            $registrosPorPagina=10;
            $paginasEnTotal=ceil($cantidadO/$registrosPorPagina);
            $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
            $datos=$datosInstObj->obtenerTodosRegistrosTodosDatos($posisionDePagina,$registrosPorPagina);
          }
      }else{
        $datosInstObj->servicio=$tipo."%";
          if ($anio=='0') {
            $datosInstObj->fecha=$mes;
            $cantidadO=$datosInstObj->obtenerTodosRegistrosMesSna();    
            $registrosPorPagina=10;
            $paginasEnTotal=ceil($cantidadO/$registrosPorPagina);
            $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
            $datos=$datosInstObj->obtenerTodosRegistrosMesDataSna($posisionDePagina,$registrosPorPagina);
          }else if($mes!='0'){
            $datosInstObj->fecha=$anio.$mes;
            $cantidadO=$datosInstObj->obtenerTodosRegistrosMesCna();    
            $registrosPorPagina=10;
            $paginasEnTotal=ceil($cantidadO/$registrosPorPagina);
            $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
            $datos=$datosInstObj->obtenerTodosRegistrosMesDataCna($posisionDePagina,$registrosPorPagina);
          }else{
            $datosInstObj->fecha=$anio;
            $cantidadO=$datosInstObj->obtenerTodosRegistrosMesCna();    
            $registrosPorPagina=10;
            $paginasEnTotal=ceil($cantidadO/$registrosPorPagina);
            $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
            $datos=$datosInstObj->obtenerTodosRegistrosMesDataCna($posisionDePagina,$registrosPorPagina);
          } 
      }
    if ($datos) {
        foreach($datos as $var){
            ?>
            <tr class='<?= ($var->tmp=="")?"":"danger"?>'> 
                <td><?= formatoSinHora($var->fecha) ?><br><b><?= $var->orte ?><b></td>
                <td><?= $var->cliente ?></td>
                <td><?= $var->placa ?></td>
                <td><?= $var->chasis ?></td>
                <td><?= $var->servicio ?><br><?= limpiarPlan($var->sistema) ?> </td>
                <td><?= $var->operador ?></td>
                <td><?= obtenerEstado($var->estado) ?></td>
                <td><?= (!empty($var->horaActivacion))?formatoHora($var->horaActivacion):'' ?></td>
                <td><?= $var->observaciones ?></td>
                <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" data-id="<?php echo $var->iddatosInstalacion ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;</td>                    
            </tr> 
        <?php
        }
        ?>
<tr>
<td>Mostrando <?= ($cantidadO<$registrosPorPagina) ? $cantidadO : $registrosPorPagina ; ?> de <?= $cantidadO  ?></td>
<td colspan="8">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO,$paginasEnTotal) ?>
</td>
</tr>
    <?php
        }else{
     ?>
    <tr>
  <td colspan="78" class="text-center">No se encontro registro alguno    
  </td>
</tr>
<?php
    }
  }
?>

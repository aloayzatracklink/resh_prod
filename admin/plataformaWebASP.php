<?php $nav="plataformaWebASP" ?>
<?php require("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
} ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->plataformaWebASP=="si"): ?>
<?php 
$operacionObj=new Operacion();
$calObj=new Calendario();
$datos="";
$errores=array();
$echo=array();
if (isset($_POST['cambiarEstado'])) {
    if (isset($_POST['sim']) && isset($_POST['estado'])) {
    require_once("../includes/init.track.php");
    $onObj=new Operacion;
    $heObj=new Healt;
    $onObj->idcliente=limpiar($_POST['idcustomer']);
    $onObj->idvehiculo=limpiar($_POST['idvehicle']);
    $onObj->idlocation=limpiar($_POST['idlocation']);
    $onObj->sim=limpiar($_POST['sim']);
    $onObj->idcita=limpiar($_POST['estadoAnt']);
    $onObj->ordenContrato=limpiar($_POST['estado']);
    if ($onObj->registrarCambiosDesdeResh()) {
           if ($_POST['estado']=='0') {
               $heObj->esconderVehiculoDePlataformaWeb($onObj->serie);
               $echo[]='Se escondio el vehículo del cliente '.$_POST['nombreCl'].' de su vehículo con placa '.$_POST['placa'];
           }else if ($_POST['estado']=='1'){
               $heObj->mostrarVehiculoDePlataformaWeb($onObj->serie);
               $echo[]='Se mostro el vehículo del cliente '.$_POST['nombreCl'].' de su vehículo con placa '.$_POST['placa'];
           }else{
               $errores[]='No se cambio de estado ya que no se reconoce el estado '.$_POST['nombreCl'].' de su vehículo con placa '.$_POST['placa'];
           }
        
    }else{
               $errores[]='No se cambio de estado error en registrar en log cambios '.$_POST['nombreCl'].' de su vehículo con placa '.$_POST['placa'];
    }
    }else{
               $errores[]='No se cambio de estado error en registrar en estado o sim '.$_POST['nombreCl'].' de su vehículo con placa '.$_POST['placa'];
    }
}
?>
   <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
   <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
   <link href="../assets/css/sweetalert2.css" rel="stylesheet">
   <link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
   <style>
    .widget p {
        display: inline-block;        
    }
    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }

    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }
    .reloj .caja-segundos {
        display: inline-block;
    }
    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
}
.pagination1{margin:0;padding:0;}
.pagination1 li{
    display: inline;
    padding: 6px 10px 6px 10px;
    border: 1px solid #ddd;
    margin-right: -1px;
    font: 15px/20px Arial, Helvetica, sans-serif;
    background: #FFFFFF;
    box-shadow: inset 1px 1px 5px #F4F4F4;
}
.pagination1 li a{
    text-decoration:none;
    color: rgb(89, 141, 235);
}
.pagination1 li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination1 li.last {
    border-radius: 0px 5px 5px 0px;
}
.pagination1 li:hover{
    background: #CFF;
}
.pagination1 li.active{
    background: #F0F0F0;
    color: #333;
}
</style>
 <body>
<div class="se-pre-con"></div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <?php include("../includes/menuIzquierdo.php") ?>
               </div>
           </nav>
           <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>
                </nav>
            </div>
            <div class="wrapper wrapper-content">
  <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Vehículos en la plataforma de cliente</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                            <div class="col-md-6">
                                    <input id="filter" type="text" placeholder="Buscar aquí" class="input-sm form-control" autofocus=""> 
                            </div>
                                <!-- <div class="col-md-2 m-b-xs">
                                    <div data-toggle="buttons" class="btn-group">
                                        <label class="btn btn-sm btn-white"> <input type="radio"  class='por' name="por" value='cliente'> Cliente </label>
                                        <label class="btn btn-sm btn-white active"> <input type="radio" class='por' name="por" checked="" value='vehiculo'> Vehículo </label>
                                    </div>
                                </div>-->
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Placa</th>
                                        <th>Chasis</th>
                                        <th>Acción</th>
                                    </tr>
                                    </thead>
                                    <tbody id="resultado">
                                    </tbody>
                                </table>
                            </div>
            
                        </div>
                    </div>
                </div>
            </div>

</div>
<?php include("../includes/footer.php") ?>
</div>
</div>
<div class="modal inmodal" id="bateriasOModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Verificar estado en Web</h4> 
                <small class="font-bold"><div class="form-group ">
                </div></small>
            </div>
            <form  role="form" method="post" >
                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="submit" name="cambiarEstado" class="btn btn-primary">Gestionado</button>
                </div>
            </form>   
        </div>
    </div>
</div>
</body>
<!-- LOS MODALES AQUI  -->
<!-- <button class='btn' data-clipboard-action='copy' data-clipboard-target='p'>Copy</button> -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/clipboard.min.js"></script>
<script src="../assets/calendario/dist/sweetalert.min.js"></script>
    <!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<?php // include_once('../includes/script.php') ?> 
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
$(document).ready(function(){
    //var por=$(".por:checked").val();
    //$("#resultado").load("pag/paginacionRegistros",{por:por}, function(){ 
        //});
    $("#resultado").on( "click", ".pagination1 a", function (e){
        e.preventDefault();
        var datosS = $("#filter").val();
        var por=$(".por:checked").val();
        $("#resultado").load("pag/resultadoRegistrosG",{page:page,por:por,datosS:datosS}, function(){ 
        });
    });
    $("#filter").keyup(function()
    {   
        var datosS = $(this).val();
        datosS = datosS.replace('<>/"', '');
        var por=$(".por:checked").val();
        $.ajax({
                type : 'POST',
                url  : 'pag/resultadoRegistrosG',
                data :  {datosS:datosS,por:por},
                success : function(data)
                {
                    $("#resultado").html(data);
                }
            });
            return false;
    });

    $(".por").change(function() {
        var por = $(this).val();
        var datosS = $("#filter").val();
        $("#resultado").load("pag/resultadoRegistrosG",{por:por, datosS:datosS}, function(){ 
        });
    });


});
$('#bateriasOModal').on('show.bs.modal', function (e) {
    var rowid = $(e.relatedTarget).data('id');
    $.ajax({
        type : 'post',
        url : 'modal/datosInstalacionGModal', //Here you will fetch records 
        data :  'rowid='+ rowid, //Pass $id
        success : function(data){
                $('.modal-body').html(data);//Show fetched data from database
        }
    }); 
});

</script>
   <script>
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
<?php else: ?> 
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?> 
<?php endif ?>

<?php 
/**
* 
*/
class Log extends Recursos
{
	public $idlog;
	public $tabla;
	public $id;
	public $idsession;
	public $arrayAntes;
	public $arrayDespues;
	public $fecha;
	function __construct()
	{
		
	}
	public static function registrarLog($tabla,$id,$arrayAntes,$arrayDespues){
		global $db;
		global $session;
		$ipActual=exec("ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'");
		$idsession=(isset($session->idusuario) )?$session->idusuario:$_SERVER['REDIRECT_UNIQUE_ID'];
		$arrayAntes=(is_array($arrayAntes) || is_object($arrayAntes))?convertiArrayString($arrayAntes):$arrayAntes;
		$arrayDespues=(is_array($arrayDespues)|| is_object($arrayDespues))?convertiArrayString($arrayDespues):$arrayDespues;				
		$fechaCreada=date("YmdHis");
		$sql="INSERT INTO log (tabla,id,idsession,arrayAntes,arrayDespues,HTTP_HOST,HTTP_CONNECTION , HTTP_USER_AGENT, SERVER_ADDR, SERVER_PORT, REMOTE_ADDR,SCRIPT_FILENAME,REDIRECT_URL, REQUEST_URI ,fecha) VALUES ('";
		$sql.=$db->escapeString($tabla)."','";
		$sql.=$db->escapeString($id)."','";
		$sql.=$db->escapeString($idsession)."','";
		$sql.=$db->escapeString($arrayAntes)."','";
		$sql.=$db->escapeString($arrayDespues)."','";		
		$sql.=$db->escapeString($_SERVER['HTTP_HOST'])."','";		
		$sql.=$db->escapeString($_SERVER['HTTP_CONNECTION'])."','";		
		$sql.=$db->escapeString($_SERVER['HTTP_USER_AGENT'])."','";		
		$sql.=$db->escapeString($ipActual)."','";		
		$sql.=$db->escapeString($_SERVER['SERVER_PORT'])."','";		
		$sql.=$db->escapeString($_SERVER['REMOTE_ADDR'])."','";		
		$sql.=$db->escapeString($_SERVER['SCRIPT_FILENAME'])."','";		
		$sql.=$db->escapeString($_SERVER['REDIRECT_URL'])."','";		
		$sql.=$db->escapeString($_SERVER['REQUEST_URI'])."','";		
		$sql.=$db->escapeString($fechaCreada)."')";
		if ($db->query($sql)) {		
			return true;	
		}else{
			return false;
		}
	}
	public function actualizarDatosCanal(){
		global $db;
		$sql="UPDATE canales SET banco ='";
		$sql.=$db->escapeString($this->banco)."', ";
		$sql.="numeroCuenta ='";
		$sql.=$db->escapeString($this->numeroCuenta)."', ";
		$sql.="cci ='";
		$sql.=$db->escapeString($this->cci)."', ";
		$sql.="nombreCanal ='";
		$sql.=$db->escapeString($this->nombreCanal)."', ";
		// $sql.="titular ='";
		// $sql.=$db->escapeString($this->titular)."', ";
		$sql.="ejecutivoCanal ='";
		$sql.=$db->escapeString($this->ejecutivoCanal)."', ";
		$sql.="canalId ='";
		$sql.=$db->escapeString(soloNumero($this->canalId))."', ";
		$sql.="moneda ='";
		$sql.=$db->escapeString($this->moneda)."' ";
		$sql.=" WHERE idcanal =";
		$sql.=$db->escapeString($this->idcanal)." ";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
	public function actualizarDatosCanalcom(){
		global $db;
		$sql="UPDATE canales SET banco ='";
		$sql.=$db->escapeString($this->banco)."', ";
		$sql.="numeroCuenta ='";
		$sql.=$db->escapeString($this->numeroCuenta)."', ";
		$sql.="cci ='";
		$sql.=$db->escapeString($this->cci)."', ";
		$sql.="nombreCanal ='";
		$sql.=$db->escapeString($this->nombreCanal)."', ";
		$sql.="titular ='";
		$sql.=$db->escapeString($this->titular)."', ";
		$sql.="ejecutivoCanal ='";
		$sql.=$db->escapeString($this->ejecutivoCanal)."', ";
		$sql.="canalId ='";
		$sql.=$db->escapeString(soloNumero($this->canalId))."', ";
		$sql.="moneda ='";
		$sql.=$db->escapeString($this->moneda)."' ";
		$sql.=" WHERE idcanal =";
		$sql.=$db->escapeString($this->idcanal)." ";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
	public function otenerDatosCanal($canalId){
		global $db;
		$sql=self::sentencia("SELECT * FROM canales WHERE canalId='".$db->escapeString($canalId)."' AND canalId<>'' ORDER BY idcanal DESC LIMIT 1");
		return !empty($sql) ? array_shift($sql) : false;		
	}

	public static function verNombreCanal(){
		$sql=self::sentencia("SELECT DISTINCT(nombreCanal) FROM canales WHERE cobraComision='' AND tipoCanal<>'CONVENIO' ");
		return !empty($sql) ? $sql : false;
	}

	public static function verEjecutiVodeCanal(){
		$sql=self::sentencia("SELECT DISTINCT(ejecutivoCanal) FROM canales WHERE cobraComision=''");
		return !empty($sql) ? $sql : false;
	}
	public static function verTiendadeCanal(){
		$sql=self::sentencia("SELECT DISTINCT(tiendaCanal) FROM canales WHERE cobraComision='' AND tipoCanal='CONVENIO' AND nombreCanal='1'");
		return !empty($sql) ? $sql : false;
	}

	public static function verDniCanal(){		
		$sql=self::sentencia("SELECT canalId,ejecutivoCanal FROM canales WHERE canalId <> ''"); 
		return !empty($sql)?$sql:false;
	}

	public static function obtenerDatosCanalPorId($idcanal){
		global $db;
		$obtenerDatosCanalPorIdQ=self::sentencia("SELECT * FROM canales WHERE idcanal =".$db->escapeString($idcanal)." LIMIT 1");
		return !empty($obtenerDatosCanalPorIdQ) ? array_shift($obtenerDatosCanalPorIdQ) : false;
	}
	public static function verificarEstadoDeCanal($idcanal){
		global $db;
		$verificarQ=self::sentencia("SELECT * FROM canales WHERE idcanal=".$db->escapeString($idcanal)." AND (banco='PENDIENTE' OR numeroCuenta='PENDIENTE' OR moneda='PENDIENTE' OR canalId='PENDIENTE' OR ejecutivoCanal='PENDIENTE')  LIMIT 1");
		return !empty($verificarQ) ? true:false; 
	}
	public function registrarNumeroRuc(){
		global $db;
		$registrarNumeroRucQ="UPDATE canales SET ruc ='".$db->escapeString($this->ruc)."' WHERE idcanal='".$db->escapeString($this->idcanal)."' ";
		if ($db->query($registrarNumeroRucQ)) {
			return true;
		}else{
			return false;
		}
	}
	
	public static function obtenerCanalDesdeNombresYEjecutivo($nombreCanal,$ejecutivoCanal){
		$nombreCanal=htmlspecialchars($nombreCanal);
		$ejecutivoCanal=htmlspecialchars($ejecutivoCanal);
		$obtenerCanalDesdeNombresYEjecutivo=self::sentencia("SELECT * FROM canales WHERE nombreCanal='$nombreCanal' AND ejecutivoCanal='$ejecutivoCanal' LIMIT 1");
		return !(empty($obtenerCanalDesdeNombresYEjecutivo))?array_shift($obtenerCanalDesdeNombresYEjecutivo):false;
	}

	public static function obtenerConvenios(){
		$obc=self::sentencia("SELECT nombreConvenio,idconvenio FROM convenios WHERE estado='0'");
		return !empty($obc)?$obc:false;
	}
	public static function obtenerConveniosDeEjecutivo(){
		global $session;
		$obc=self::sentencia("SELECT nombreConvenio,idconvenio FROM convenios WHERE estado='0' AND idEjecutivoAsignado='$session->idusuario' ");
		return !empty($obc)?$obc:false;
	}
	public static function obtenerConveniosTodos(){
		global $session;
		$obc=self::sentencia("SELECT nombreConvenio,idconvenio FROM convenios WHERE estado='0' ");
		return !empty($obc)?$obc:false;
	}
	public static function obtenerDatosDeConvenioPorId($id){
		$id=limpiar($id);
		$covD=self::sentencia("SELECT nombreConvenio FROM convenios WHERE idconvenio='$id' LIMIT 1 ");
		return !empty($covD)?array_shift($covD):false;

	}
 
}

 ?>
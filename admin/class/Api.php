<?php
/**
 * Clase para consumir API Rest
 * Las operaciones soportadas son:
 *
 * 	- POST		: Agregar
 * 	- GET		: Consultar
 * 	- DELETE	: Eliminar
 * 	- PUT		: Actualizar
 * 	- PATCH		: Actualizar por parte
 *
 */

class API{


    static function Authentication($URL,$usuario,$password,$ARRAY){
        $datapost = '';
        $headers 	= array('Content-Type: application/x-www-form-urlencoded');
        foreach($ARRAY as $key=>$value) {
            $datapost .= $key . "=" . $value . "&";
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$usuario:$password");
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$datapost);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * Enviar parámetros a un servidor a través del protocolo HTTP (POST).
     * Se utiliza para agregar datos en una API REST
     *
     * @param string $URL URL recurso, ejemplo: http://website.com/recurso
     * @param string $TOKEN token de autenticación
     * @param array $ARRAY parámetros a envíar
     * @return JSON
     */
    static function POST($URL,$TOKEN,$ARRAY){
        $datapost = '';
        /*foreach($ARRAY as $key=>$value) {
            $datapost .= $key . "=" . $value . "&";
        }
        var_dump($ARRAY);
        var_dump(json_encode($ARRAY,JSON_FORCE_OBJECT));*/

        $headers 	= array('Authorization: Bearer ' . $TOKEN,'Content-Type:application/json');
        $ch 		= curl_init();
        curl_setopt($ch,CURLOPT_URL,$URL);
        curl_setopt($ch,CURLOPT_POST, 1);
/*        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));*/
        curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($ARRAY));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        curl_close ($ch);
        return $response;
    }


    static function POSFD($URL,$headers,$ARRAY){
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$URL);
        curl_setopt($ch,CURLOPT_POSTFIELDS, build_post_fields($ARRAY));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        curl_setopt($ch,CURLOPT_UPLOAD, TRUE);
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        curl_close ($ch);
        return $response;
    }

    static function POSTB($URL,$headers,$ARRAY){
       /* echo $URL;
        echo "--------<br/>";
        echo var_dump($headers);
        echo "----------<br/>";*/
        $ch 		= curl_init();
        curl_setopt($ch,CURLOPT_URL,$URL);
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($ARRAY));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        curl_close ($ch);
        return $response;
    }
    /**
     * Consultar a un servidor a través del protocolo HTTP (GET).
     * Se utiliza para consultar recursos en una API REST
     *
     * @param string $URL URL recurso, ejemplo: http://website.com/recurso/(id) no obligatorio
     * @param string $TOKEN token de autenticación
     * @return JSON
     */
    static function GET($URL,$TOKEN){
        $headers 	= array('Authorization: Bearer ' . $TOKEN);
        $ch 		= curl_init();
        curl_setopt($ch,CURLOPT_URL,$URL);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        curl_close ($ch);
        return $response;
    }

    /**
     * Consultar a un servidor a través del protocolo HTTP (DELETE).
     * Se utiliza para eliminar recursos en una API REST
     *
     * @param string $URL URL recurso, ejemplo: http://website.com/recurso/id
     * @param string $TOKEN token de autenticación
     * @return JSON
     */
    static function DELETE($URL,$TOKEN){
        $headers 	= array('Authorization: Bearer ' . $TOKEN);
        $ch 		= curl_init();

        curl_setopt($ch,CURLOPT_URL,$URL);
        curl_setopt($ch,CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        curl_close ($ch);
        return $response;
    }

    /**
     * Enviar parámetros a un servidor a través del protocolo HTTP (PUT).
     * Se utiliza para editar un recurso en una API REST
     *
     * @param string $URL URL recurso, ejemplo: http://website.com/recurso/id
     * @param string $TOKEN token de autenticación
     * @param array $ARRAY parámetros a envíar
     * @return JSON
     */
    static function PUT($URL,$TOKEN,$ARRAY){
        $datapost = '';
        foreach($ARRAY as $key=>$value) {
            $datapost .= $key . "=" . $value . "&";
        }

        $headers 	= array('Authorization: Bearer ' . $TOKEN);
        $ch 		= curl_init();
        curl_setopt($ch,CURLOPT_URL,$URL);
        curl_setopt($ch,CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$datapost);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        curl_close ($ch);
        return $response;
    }

    /**
     * Enviar parámetros a un servidor a través del protocolo HTTP (PATCH).
     * Se utiliza para editar un atributo específico (recurso) en una API REST
     *
     * @param string $URL URL recurso, ejemplo: http://website.com/recurso/id
     * @param string $TOKEN token de autenticación
     * @param array $ARRAY parametros parámetros a envíar
     * @return JSON
     */
    static function PATCH($URL,$TOKEN,$ARRAY){
        $datapost = '';
        foreach($ARRAY as $key=>$value) {
            $datapost .= $key . "=" . $value . "&";
        }

        $headers 	= array('Authorization: Bearer ' . $TOKEN);
        $ch 		= curl_init();
        curl_setopt($ch,CURLOPT_URL,$URL);
        curl_setopt($ch,CURLOPT_CUSTOMREQUEST, "PATCH");
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$datapost);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        curl_close ($ch);
        return $response;
    }

    /**
     * Convertir JSON a un ARRAY
     *
     * @param json $json Formato JSON
     * @return ARRAY
     */
    static function JSON_TO_ARRAY($json){
        return json_decode($json,true);
    }
}

function build_post_fields( $data,$existingKeys='',&$returnArray=[]){
    if(($data instanceof CURLFile) or !(is_array($data) or is_object($data))){
        $returnArray[$existingKeys]=$data;
        return $returnArray;
    }
    else{
        foreach ($data as $key => $item) {
            build_post_fields($item,$existingKeys?$existingKeys."[$key]":$key,$returnArray);
        }
        return $returnArray;
    }
}
<?php

class AuditoriaSistemas extends Recursos{
    public $id;
    public $message;
    public $body;
    public $statusCode;
    public $responseBody;
    public $scriptName;
    public $scriptServer;
    public $scriptReference;
    public $scriptDescription;
    public $jobType;
    public $dateCreate;
    public $cantidad;
    
    function __construct(){}

    public function obtenerLogs(){
        global $db;
		global $session;
        $sql = "SELECT * FROM auditoria_sistemas";
        return self::sentencia($sql);
    }

    public function obtenerCantidadTotal(){
        global $db;
		global $session;
        $sql = "SELECT COUNT(*) as cantidad FROM auditoria_sistemas";
        return self::sentencia($sql);
    }

    public function verLogPorId($id){
        global $db;
        global $session;
        $sql = "SELECT * FROM auditoria_sistemas WHERE id = $id";
        $result = self::sentencia($sql);
		return !empty($result) ? $result : false;
    }

    public function obtenerLogsPorProceso($filter, $prevPageRows, $currentPageRows){
        global $db;
		global $session;
        $sql = "SELECT * FROM auditoria_sistemas WHERE scriptReference LIKE '%$filter%' LIMIT $prevPageRows, $currentPageRows";
        $result = self::sentencia($sql);
        return !empty($result) ? $result : [];
    }

    public function obtenerLogsPorBuscador($searchText){
        global $db;
		global $session;
        $sql = "SELECT * FROM auditoria_sistemas WHERE
                    body LIKE '%$searchText%' OR
                    statusCode LIKE '%$searchText%' OR
                    message LIKE '%$searchText%' OR
                    responseBody LIKE '%$searchText%' OR
                    scriptServer LIKE '%$searchText%' OR
                    jobType LIKE '%$searchText%'";
        $result = self::sentencia($sql);
        return !empty($result) ? $result : [];
    }

    public function obtenerLogsPorPaginaGeneral($prevPageRows, $currentPageRows){
        global $db;
		global $session;
        $sql = "SELECT * FROM auditoria_sistemas LIMIT $prevPageRows, $currentPageRows";
        $result = self::sentencia($sql);
        return !empty($result) ? $result : [];
    }

    public function prettyJson($data){
        $json = utf8_encode($data);
        $len = strlen($json);
        $split = str_split($json);
        for($i = 0; $i < $len; $i++){
            if($split[$i] == "'"){
                $split[$i] = '"';
            }
            if($split[$i] == "{"){
                $split[$i] .= "<br>";
            }

            $current = $i;
            if(($split[$i] == ":" || $split[$i] == " ") && $split[++$current] == "["){
                $split[$i] .= "<br>";
            }

            if($split[$i] == ","){
                $split[$i] .= "<br>";
            }

            $current = $i;
            if($split[++$current] == "}"){
                $split[$i] .= "<br>";
            }
        }
        return implode("", $split);
    }
}
?>
<?php $nav='subirArchivos'?>
<?php include("../includes/head.php") ?>
<link rel="stylesheet" href="../assets/css/jquery-confirm.min.css">
<?php if (!$session->yaEstaIngresado()) {
	irA("../ingresar");
} ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->subirArchivos=="si"): ?> 
	<?php 
//subimos los archivos ... 
	$contador=0;
	if (isset($_POST['subirArchivo'])) {    
		$fnombre=$_FILES['sel_file']['name'];
		$ver_extencion = explode(".", $fnombre);
		if(strtolower(end($ver_extencion))=="csv"){
			$filename = $_FILES['sel_file']['tmp_name'];
			$handle = fopen($filename, "r");
			global $db;
         	$idu=$session->idusuario;
         	$fe=date("YmdHis");
			while (($datos = fgetcsv($handle, 20000, ",")) !== FALSE)
			{
				$contador++;
				if (isset($_POST['subirArchivo'])) {
					if(isset($datos[20])) {	
					    $one = $datos[20];
					}else{
						$one='';
					}
					if(isset($datos[18])) {	
					    $one = $datos[18];
					}else{
						$one8='';
					}
					if(isset($datos[19])) {	
					    $one = $datos[19];
					}else{
						$one9='';
					}
					if ($datos[1]!='') {						
                        if ($datos[0]=='') {
                           $dia='25'; 
                        }else{
                            $dia=substr($datos[0],0,2);
                        }
                        $fecha=$_POST['anio'].$_POST['mes'].$dia;
						$sql="INSERT INTO datosInstalacion (fecha,fechaSubida,nombreArchivo,orte,cliente,placa, chasis,imei , id, odo,ubicacionSistema,lugarInstalacion,tecnicoInstalador,sistema, servicio, operador,estado,	horaActivacion,	cordenadas,	observaciones,observaciones2,observaciones3,observaciones4,fechaRegistro,idusuario) VALUES ('".soloNumero($fecha)."','".addslashes($datos[0])."','".addslashes($fnombre)."','".soloNumero($datos[1])."','".addslashes($datos[2])."','$datos[3]','$datos[4]','$datos[5]','$datos[6]','$datos[7]','$datos[8]','$datos[9]','".addslashes($datos[10])."','".addslashes($datos[11])."','".addslashes($datos[12])."','".addslashes($datos[13])."','$datos[14]','$datos[15]','$datos[16]','".addslashes($datos[17])."','".addslashes($one8)."','".addslashes($one9)."','".$one."','$fe','$idu')";
						$db->query($sql);
					}
				}
			}
			fclose($handle);
		}else{
			echo "abrir el archivo correcto tio";
		}
	}
	?>
	<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
	<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
	<body>
		<div id="wrapper">
			<nav class="navbar-default navbar-static-side" role="navigation">
				<div class="sidebar-collapse">
					<?php include("../includes/menuIzquierdo.php") ?>
					
				</div>
			</nav>

			<div id="page-wrapper" class="gray-bg dashbard-1">
				<div class="row border-bottom">
					<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
						<?php include("../includes/navegacionArriba.php") ?>

					</nav>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="wrapper wrapper-content">
							<div class="ibox float-e-margins ibox-content ">
							<div class="row ">
								<div class="col-md-12">
									<div class="row">
										<form  method='post' enctype="multipart/form-data"> 
									<div class="col-md-3">
											<div class="fileinput fileinput-new input-group" data-provides="fileinput">
												<div class="form-control" data-trigger="fileinput">
													<i class="glyphicon glyphicon-file fileinput-exists"></i>
													<span class="fileinput-filename"></span>
												</div>
												<span class="input-group-addon btn btn-default btn-file">
													<span class="fileinput-new">Seleccionar archivo * </span>
													<span class="fileinput-exists">Cambiar</span>
													<input type="file" name="sel_file" required/>
												</span>
												<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
											</div> 
											</div>
                                            <div class="col-md-3">
                        <select id="tipo" class="input-sm form-control input-s-sm inline" name="mes" required>
                                            <option value="" selected disabled>Mes</option>
                                        <option value="01">Enero</option>
                                        <option value="02">Febrero</option>
                                        <option value="03">Marzo</option>
                                        <option value="04">Abril</option>
                                        <option value="05">Mayo</option>
                                        <option value="06">Junio</option>
                                        <option value="07">Julio</option>
                                        <option value="08">Agosto</option>
                                        <option value="09">Setiembre</option>
                                        <option value="10">Octubre</option>
                                        <option value="11">Novienbre</option>
                                        <option value="12">Diciembre</option>
                                </select>
                            </div>
                                <div class="col-md-3">

                                <select id="tipo" class="input-sm form-control input-s-sm inline" required name='anio'>
                                    <option value="" selected disabled >Año</option>
                                    <option value="2015">2015</option>
                                    <option value="2016">2016</option>
                                    <option value="2017">2017</option>
                                </select>
                        </div>
                        <div class="col-md-3"><button class="btn btn-success col-md-6" type="submit" name="subirArchivo">	Subir!</button></div>
                                                    </form>
                                            
                                        </div>
								</div>
							</div>
							
						
<div class="row">
<div class="col-md-12">
<table class="table table-hover">
<br>
    <thead>
            <tr>
                <th>SISTEMA Y FECHA</th>
                <th>NOMBRE DE ARCHIVO</th>
                <th>ORTE</th>
                <th>CLIENTE</th>
                <th>FECHA SUBIDA</th>
                <th>USUARIO</th>
                <th>CANTIDAD</th>
                <th>Acción</th>
            </tr>
    </thead>
<tbody>
<?php $datosUlt=DatosInstalacion::obtenerLosUltimosRegistros() ?>
<?php if ($datosUlt){ ?>
	<?php foreach ($datosUlt as $var): ?>	
	<?php $u=Usuario::obtenerUsuario($var->idusuario) ?>	
    <tr>
<?php
    if (1969==formatoAnio($var->fecha)) {
       ?>
        <td>Error en formato de<br> fecha es año mes y dia</td>
       <?php 
    }else{
        ?>
        <td><?= strtolower($var->sistema)." -  ".mesDesdeNumero(formatoMES($var->fecha))."  ".formatoAnio($var->fecha) ?></td>
        <?php
    }
?>
        <td><?= $var->nombreArchivo ?></td>
        <td><?= $var->orte ?></td>
        <td><?= $var->cliente ?></td>
        <td><?= formataAniOPrimero24horas($var->fechaRegistro) ?></td>
        <td><?= $u->usuarioNombre." ".$u->usuarioApellidoPaterno ?></td>
        <td><?= DatosInstalacion::obterCuantosRegistros($var->fechaRegistro) ?>
        <td>
            <button class="btn btn-danger formmm eliminarUltimoGrupoBTN"   style="padding-top: 0;padding-bottom: 0" type="submit" value="<?php echo $var->fechaRegistro ?>">Eliminar <i class="fa fa-check-square-o"></i></button>&nbsp;
        </td> </tr>
	<?php endforeach ?>
<?php } ?>
</tbody>
</table>
</div>
</div>


										
							</div>
							
						</div>
					</div>


					<?php include("../includes/footer.php") ?>
				</div>
			</div>

		</div>

		<script src="../assets/js/jquery-2.1.1.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
		<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

		<!-- Custom and plugin javascript -->
		<script src="../assets/js/inspinia.js"></script>
		<script src="../assets/js/plugins/pace/pace.min.js"></script>
		<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
		<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

 <script src="../assets/js/jquery-confirm.min.js"></script>
		<?php include_once('../includes/script.php') ?>
		<script> $(document).ready(function(){
			$('.footable').footable();
			$('.footable2').footable();

$(".eliminarUltimoGrupoBTN").click(function(e) {
var borrarGrupo=$(this).val();
 $.confirm({
      theme: 'modern',
      title: '¿Elimar bloque subido?',
      content: 'Se eliminara todo los registros subidos anteriormente.',
      buttons: {
          Confirmar: {
            btnClass: 'btn-blue',
            action:function () {   
             $.ajax({
                type : 'POST',
                url  : 'proc/proBorrarBloqu',
                data :  {borrarGrupo:borrarGrupo},
                success : function(data)
                {
              $.alert('Se realizo el cambio');                    
   					window.location.href = "subirArchivos";      
                }
            });        
            }
          },          
          Cancelar: {
            btnClass: 'btn-warning',
            action:function () {  
              $.alert('No se realizo nigun cambio!');
              $("#filter").focus(); 
            
            }
          }
      }
  });

});
		});


		</script>
	<?php else: ?> 
		<?php $session->salir();
		echo "NO NO NO NO ;"
		?> 
	<?php endif ?>

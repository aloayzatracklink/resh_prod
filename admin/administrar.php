<?php $nav="administrar" ;?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
	irA("ingresar");

}
?> 
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->administrar=="si"): ?>
	<?php
	$errores=array();
	$mensaje="";
	$usuarioObj=new Usuario();
//8 son tecnicos
	if (isset($_POST['registrarNuevoUsuario'])) {
		$usuarioObj->usuario = filter_var(strtolower($_POST['usuario']), FILTER_SANITIZE_STRING);
		$usuarioObj->anexo = filter_var(strtolower($_POST['anexo']), FILTER_SANITIZE_STRING);
		$usuarioObj->celular = filter_var(strtolower($_POST['celular']), FILTER_SANITIZE_STRING);
		$usuarioObj->contrasenia = $_POST['contrasenia'];
		$usuarioObj->contrasenia1 = $_POST['contrasenia1'];
		$usuarioObj->usuarioNombre = ucwords(strtolower($_POST['nombre']));
		$usuarioObj->usuarioApellidoPaterno = ucwords(strtolower($_POST['apellidoPaterno']));
		$usuarioObj->usuarioApellidoMaterno = ucwords(strtolower($_POST['apellidoMaterno']));
		$usuarioObj->idrol = $_POST['rolid'];
		if (empty($usuarioObj->contrasenia) or empty($usuarioObj->contrasenia1)or empty($usuarioObj->usuarioNombre) or empty($usuarioObj->usuarioApellidoPaterno) or empty($usuarioObj->usuarioApellidoMaterno) or empty($usuarioObj->idrol)) {
			$errores[]= 'Por favor rellena todos los datos correctamente';
		}else{

		//comprovar s exste en la base de datos compar por usuario ----

			if ($usuarioObj->obtenerUsuarioExistencia($usuarioObj->usuario)) {
				$errores[]= 'El usuario ingresado ya existe en la base de datos';
			}
		// encriptar datos here -----
			$usuarioObj->contrasenia = hash('sha512', $usuarioObj->contrasenia);
			$usuarioObj->contrasenia1 = hash('sha512', $usuarioObj->contrasenia1);
		//fin de ecn...
		// comparamos si la data es igal
			if ($usuarioObj->contrasenia!=$usuarioObj->contrasenia1) {
				$errores[] = 'Las contraseñas no son iguales';
			}
		///  
			 //recibimos los cheks :V
			if (isset($_POST['panel'])) {
				$usuarioObj->panel=$_POST['panel'];
			}
			if (isset($_POST['baterias'])) {
				$usuarioObj->baterias=$_POST['baterias'];
			}
			if (isset($_POST['instalaciones'])) {
				$usuarioObj->instalaciones=$_POST['instalaciones'];
			}
			if (isset($_POST['chequeos'])) {
				$usuarioObj->chequeos=$_POST['chequeos'];
			}
			if (isset($_POST['choques'])) {
				$usuarioObj->choques=$_POST['choques'];
			}
			if (isset($_POST['robo'])) {
				$usuarioObj->robos=$_POST['robo'];
			}
			if (isset($_POST['maps'])) {
				$usuarioObj->maps=$_POST['maps'];
			}
			if (isset($_POST['operaciones'])) {
				$usuarioObj->registrarOperaciones=$_POST['operaciones'];
			}
			if (isset($_POST['registrarClientes'])) {
				$usuarioObj->registrarCliente=$_POST['registrarClientes'];
			}
			if (isset($_POST['cotizarCliente'])) {
				$usuarioObj->cotizar=$_POST['cotizarCliente'];
			}
			if (isset($_POST['gestionPostVenta'])) {
				$usuarioObj->gestionPostVenta=$_POST['gestionPostVenta'];
			}
			if (isset($_POST['gestionRenovacion'])) {
				$usuarioObj->gestionRenovaciones=$_POST['gestionRenovacion'];
			}
			if (isset($_POST['Veroperaciones'])) {
				$usuarioObj->verificarOperaciones=$_POST['Veroperaciones'];
			}
			if (isset($_POST['gestionTecnica'])) {
				$usuarioObj->gestionTecnica=$_POST['gestionTecnica'];
			}
			if (isset($_POST['editarTecnicos'])) {
				$usuarioObj->editarTecnicos=$_POST['editarTecnicos'];
			}
			if (isset($_POST['ventasPorEjecutivos'])) {
				$usuarioObj->verificarComercial=$_POST['ventasPorEjecutivos'];
			}
			if (isset($_POST['ventasPorProducto'])) {
				$usuarioObj->verificarComercialP=$_POST['ventasPorProducto'];
			}
			if (isset($_POST['reportePersonalizado'])) {
				$usuarioObj->comercialPersonalizado=$_POST['reportePersonalizado'];
			}
			if (isset($_POST['contabilidad'])) {
				$usuarioObj->gestionContabilidad=$_POST['contabilidad'];
				$usuarioObj->historialContable=$_POST['contabilidad'];
			}
			if (isset($_POST['programacion'])) {
				$usuarioObj->programacion=$_POST['programacion'];
			}
			///////////////////
			////////////////
			if (isset($_POST['confirmarRegistro'])) {
				$usuarioObj->confirmarRegistroEnOnix=$_POST['confirmarRegistro'];
			}

			if (isset($_POST['programarInstalacion'])) {
				$usuarioObj->programarInstalacion=$_POST['programarInstalacion'];
			}

			if (isset($_POST['adminComercial'])) {
				$usuarioObj->administrarComercial=$_POST['adminComercial'];
			}

			if (isset($_POST['operacionesComerciales'])) {
				$usuarioObj->operacionesComerciales=$_POST['operacionesComerciales'];
			}

			if (isset($_POST['chequeoAmonitoreo'])) {
				$usuarioObj->chequeoMonitoreo=$_POST['chequeoAmonitoreo'];
			}

			if (isset($_POST['cobranzas'])) {
				$usuarioObj->cobranzas=$_POST['cobranzas'];
			}
			if (isset($_POST['pagoComisiones'])) {
				$usuarioObj->pagoComision=$_POST['pagoComisiones'];
			}
			
			if (isset($_POST['reporteComisiones'])) {
				$usuarioObj->comisiones=$_POST['reporteComisiones'];
			}

			if (isset($_POST['reporteContratosOcto'])) {
				$usuarioObj->reporteContratosOcto=$_POST['reporteContratosOcto'];
			}

			if (isset($_POST['reclamosClientes'])) {
				$usuarioObj->reclamosClientes=$_POST['reclamosClientes'];
			}
			
			if (isset($_POST['desinstalacion'])) {
				$usuarioObj->desinstalacion=$_POST['desinstalacion'];
			}

			if (isset($_POST['reprogramacion'])) {
				$usuarioObj->reprogramacion=$_POST['reprogramacion'];
			}

        //eso es todo amigos :V

			if (empty($errores)) {
				if($usuarioObj->registrarNuevoUsuario()){
					$usuarioObj->registrarMenu();
					$mensaje="Registro exitoso";
				}
			}
		}



	}

	//////FIN DEL REGISTRO DE NUEVOS USUARIOS ///////////7

	if (isset($_POST['guardarCambios'])) {
		$usuarioObj->usuario = filter_var(strtolower($_POST['usuario']), FILTER_SANITIZE_STRING);
		if ($_POST['contrasenia']=='') {
			$usuarioObj->contrasenia=$_POST['cont'];
		}else{
			$usuarioObj->contrasenia = $_POST['contrasenia'];
			$usuarioObj->contrasenia = hash('sha512', $usuarioObj->contrasenia);			
		}

		$usuarioObj->usuarioNombre = ucwords(strtolower($_POST['nombre']));
		$usuarioObj->usuarioApellidoPaterno = ucwords(strtolower($_POST['apellidoPaterno']));
		$usuarioObj->usuarioApellidoMaterno = ucwords(strtolower($_POST['apellidoMaterno']));
		$usuarioObj->idrol = $_POST['idrol'];
		//recibimos los cheks :V
			if (isset($_POST['panel'])) {
				$usuarioObj->panel=$_POST['panel'];
			}
			if (isset($_POST['baterias'])) {
				$usuarioObj->baterias=$_POST['baterias'];
			}
			if (isset($_POST['instalaciones'])) {
				$usuarioObj->instalaciones=$_POST['instalaciones'];
			}
			if (isset($_POST['chequeos'])) {
				$usuarioObj->chequeos=$_POST['chequeos'];
			}
			if (isset($_POST['choques'])) {
				$usuarioObj->choques=$_POST['choques'];
			}
			if (isset($_POST['robo'])) {
				$usuarioObj->robos=$_POST['robo'];
			}
			if (isset($_POST['maps'])) {
				$usuarioObj->maps=$_POST['maps'];
			}
			if (isset($_POST['operaciones'])) {
				$usuarioObj->registrarOperaciones=$_POST['operaciones'];
			}
			if (isset($_POST['registrarClientes'])) {
				$usuarioObj->registrarCliente=$_POST['registrarClientes'];
			}
			if (isset($_POST['cotizarCliente'])) {
				$usuarioObj->cotizar=$_POST['cotizarCliente'];
			}
			if (isset($_POST['gestionPostVenta'])) {
				$usuarioObj->gestionPostVenta=$_POST['gestionPostVenta'];
			}
			if (isset($_POST['gestionRenovacion'])) {
				$usuarioObj->gestionRenovaciones=$_POST['gestionRenovacion'];
			}
			if (isset($_POST['Veroperaciones'])) {
				$usuarioObj->verificarOperaciones=$_POST['Veroperaciones'];
			}
			if (isset($_POST['gestionTecnica'])) {
				$usuarioObj->gestionTecnica=$_POST['gestionTecnica'];
			}
			if (isset($_POST['editarTecnicos'])) {
				$usuarioObj->editarTecnicos=$_POST['editarTecnicos'];
			}
			if (isset($_POST['ventasPorEjecutivos'])) {
				$usuarioObj->verificarComercial=$_POST['ventasPorEjecutivos'];
			}
			if (isset($_POST['ventasPorProducto'])) {
				$usuarioObj->verificarComercialP=$_POST['ventasPorProducto'];
			}
			if (isset($_POST['reportePersonalizado'])) {
				$usuarioObj->comercialPersonalizado=$_POST['reportePersonalizado'];
			}
			if (isset($_POST['contabilidad'])) {
				$usuarioObj->gestionContabilidad=$_POST['contabilidad'];
				$usuarioObj->historialContable=$_POST['contabilidad'];
			}
			if (isset($_POST['programacion'])) {
				$usuarioObj->programacion=$_POST['programacion'];
			}
			if (isset($_POST['admin'])) {
				$usuarioObj->administrar=$_POST['admin'];
			}
			
			if (isset($_POST['confirmarRegistro'])) {
				$usuarioObj->confirmarRegistroEnOnix=$_POST['confirmarRegistro'];
			}

			if (isset($_POST['programarInstalacion'])) {
				$usuarioObj->programarInstalacion=$_POST['programarInstalacion'];
			}

			if (isset($_POST['adminComercial'])) {
				$usuarioObj->administrarComercial=$_POST['adminComercial'];
			}

			if (isset($_POST['operacionesComerciales'])) {
				$usuarioObj->operacionesComerciales=$_POST['operacionesComerciales'];
			}

			if (isset($_POST['chequeoAmonitoreo'])) {
				$usuarioObj->chequeoMonitoreo=$_POST['chequeoAmonitoreo'];
			}

			if (isset($_POST['cobranzas'])) {
				$usuarioObj->cobranzas=$_POST['cobranzas'];
			}
			if (isset($_POST['pagoComisiones'])) {
				$usuarioObj->pagoComision=$_POST['pagoComisiones'];
			}
			
			if (isset($_POST['reporteComisiones'])) {
				$usuarioObj->comisiones=$_POST['reporteComisiones'];
			}

			if (isset($_POST['reporteContratosOcto'])) {
				$usuarioObj->reporteContratosOcto=$_POST['reporteContratosOcto'];
			}

			if (isset($_POST['reclamosClientes'])) {
				$usuarioObj->reclamosClientes=$_POST['reclamosClientes'];
			}
			
			if (isset($_POST['desinstalacion'])) {
				$usuarioObj->desinstalacion=$_POST['desinstalacion'];
			}
			
			if (isset($_POST['reprogramacion'])) {
				$usuarioObj->reprogramacion=$_POST['reprogramacion'];
			}
        //eso es todo amigos :V
			if ($usuarioObj->actualizarMenuPerfil($_POST['idusuario'])) {
				$mensaje="Actualizado";
			}

	}


	if (isset($_POST['verificar'])) {

	}
	?>



	<body>

		<link href="../assets/css/plugins/iCheck/custom.css" rel="stylesheet">
		<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">	

		<div id="wrapper">

			<nav class="navbar-default navbar-static-side" role="navigation">
				<div class="sidebar-collapse">
					<?php include("../includes/menuIzquierdo.php") ?>
				</div>
			</nav>

			<div id="page-wrapper" class="gray-bg">
				<div class="row border-bottom">
					<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
						<?php include("../includes/navegacionArriba.php") ?>

					</nav>
				</div>
				<div class="wrapper wrapper-content animated fadeInRight">
		<!-- 		<?php
				 echo $session=session_id() ;
				 echo "<br>";
				 echo $time=time();
				 $tiempoEsperaEnSegundos=60;
				 $tiempoSalida=$time-$tiempoEsperaEnSegundos;
				 $query= "SELECT * FROM usuariosEnLinea WHERE session='$session'";
				 // $db->query($query);
				?> 
			<h5>asdf</h5>
 -->
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<?php foreach ($errores as $error): ?>
									<span class="label label-warning"><?php echo $error ?></span><br>
								<?php endforeach ?>
								<span class="label label-success"><?php echo $mensaje ?></span><br>
								<div class="ibox-title">
									<h5>Registrar usuario</h5>
									<div class="ibox-tools">
										<a class="collapse-link">
											<i class="fa fa-chevron-up"></i>
										</a>
										<a class="close-link">
											<i class="fa fa-times"></i>
										</a>
									</div>
								</div>
								<div class="ibox-content">
									<div class="ibox float-e-margins">
										<form action="" method="post">
											<div class="row">
												<div class="form-group col-md-3">
													<label for="nombre" class="sr-only">Nombre</label>
													<input style="border: none;" type="text" placeholder="Nombres" id="nombre" name="nombre" class="form-control border-bottom text-capitalize"  required id="nombre">
												</div>
												<div class="form-group col-md-3">
													<label for="apellidoPaterno" class="sr-only">Apellido Paterno</label>
													<input style="border: none;" type="text" placeholder="Apellido Paterno" id="apellidoPaterno" name="apellidoPaterno" class="form-control x2 text-capitalize border-bottom text-capitalize" required >
												</div>
												<div class="form-group col-md-3">
													<label for="apellidoMaterno" class="sr-only">Apellido Materno</label>
													<input style="border: none;" type="text" placeholder="Apellido Materno" id="apellidoMaterno" name="apellidoMaterno" class="form-control border-bottom text-capitalize" required >
												</div>
												<div class="form-group col-md-3">
													<label for="usuario" class="sr-only">Usuario</label>
													<input style="border: none;" type="text" placeholder="Usuario" id="usuario" name="usuario" class="form-control border-bottom"   id="name">
												</div>
											</div>

											<div class="row">
												<div class="col-md-2">
													<label for="anexo" class="sr-only">Anexo</label>
													<input style="border: none;" type="text" placeholder="Anexo" id="anexo" name="anexo" class="form-control border-bottom"  >
												</div>
												<div class="col-md-2">
													<label for="celular" class="sr-only">Celular</label>
													<input style="border: none;" type="number" placeholder="Celular" id="celular" name="celular" class="form-control border-bottom"  >
												</div>
												<div class="form-group col-md-2">
													<label for="rolid" class="sr-only">Roles</label>
													<select  style="border: none;" class="rolid border-bottom form-control text-capitalize" required name="rolid">
														<option class="text-capitalize" value="" disabled selected >Seleccione Cargo</option>
														<?php $usuarioObj->obtenerRoles() ?>            
													</select>
												</div>
												<div class="form-group col-md-3">
													<label for="contrasenia" class="sr-only">Contraseña</label>
													<input style="border: none;" type="password" placeholder="Contraseña" id="contrasenia" name="contrasenia" class="form-control border-bottom" required >
												</div>
												<div class="form-group col-md-3">
													<label for="contrasenia1" class="sr-only">Confirmar Contraseña</label>
													<input style="border: none;" type="password" placeholder="Confirmar Contraseña" id="contrasenia1" name="contrasenia1" class="form-control border-bottom" required >
												</div>
											</div>
											<div class="row">
												<div class="col-md-1">
													<label for="example1">Panel</label>
													<div class="switch">
														<div class="onoffswitch">
															<input type="checkbox"  class="onoffswitch-checkbox" id="example1" value="si" name="panel">
															<label class="onoffswitch-label" for="example1">
																<span class="onoffswitch-inner"></span>
																<span class="onoffswitch-switch"></span>
															</label>
														</div>
													</div>
												</div>
												<div class="col-md-1">
													<label for="example2">Baterías</label>
													<div class="switch">
														<div class="onoffswitch">
															<input type="checkbox" class="onoffswitch-checkbox" id="example2" value="si" name="baterias">
															<label class="onoffswitch-label" for="example2">
																<span class="onoffswitch-inner"></span>
																<span class="onoffswitch-switch"></span>
															</label>
														</div>
													</div>
												</div>	<div class="col-md-1">
												<label for="example3">Instalaciones</label>
												<div class="switch">
													<div class="onoffswitch">
														<input type="checkbox" class="onoffswitch-checkbox" id="example3" value="si" name="instalaciones">
														<label class="onoffswitch-label" for="example3">
															<span class="onoffswitch-inner"></span>
															<span class="onoffswitch-switch"></span>
														</label>
													</div>
												</div>
											</div>	<div class="col-md-1">
											<label for="example4">Chequeos</label>
											<div class="switch">
												<div class="onoffswitch">
													<input type="checkbox" class="onoffswitch-checkbox" id="example4" value="si" name="chequeos">
													<label class="onoffswitch-label" for="example4">
														<span class="onoffswitch-inner"></span>
														<span class="onoffswitch-switch"></span>
													</label>
												</div>
											</div>
										</div>	<div class="col-md-1">
										<label for="choques">Choques</label>
										<div class="switch">
											<div class="onoffswitch">
												<input type="checkbox" class="onoffswitch-checkbox" id="choques" value="si" name="choques">
												<label class="onoffswitch-label" for="choques">
													<span class="onoffswitch-inner"></span>
													<span class="onoffswitch-switch"></span>
												</label>
											</div>
										</div>
									</div>	<div class="col-md-1">
									<label for="robo">Robos</label>
									<div class="switch">
										<div class="onoffswitch">
											<input type="checkbox" class="onoffswitch-checkbox" id="robo" value="si" name="robo">
											<label class="onoffswitch-label" for="robo">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
											</label>
										</div>
									</div>
								</div>	<div class="col-md-2">
								<label for="maps">Maps</label>
								<div class="switch">
									<div class="onoffswitch">
										<input type="checkbox" class="onoffswitch-checkbox" id="maps" value="si" name="maps">
										<label class="onoffswitch-label" for="maps">
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>	<div class="col-md-2">
							<label for="operaciones">Asignar Operaciones</label>
							<div class="switch">
								<div class="onoffswitch">
									<input type="checkbox" class="onoffswitch-checkbox" id="operaciones" value="si" name="operaciones">
									<label class="onoffswitch-label" for="operaciones">
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>	<div class="col-md-2">
						<label for="clientes">Registrar Clientes</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="clientes" value="si" name="registrarClientes">
								<label class="onoffswitch-label" for="clientes">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>	
				</div>
				<div class="row">
					<div class="col-md-2">
						<label for="cotizar">Crear Acceso</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="cotizar" value="si" name="cotizarCliente">
								<label class="onoffswitch-label" for="cotizar">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>	
					<div class="col-md-2">
						<label for="post">Gestión Post Venta</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="post" value="si" name="gestionPostVenta">
								<label class="onoffswitch-label" for="post">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>	
					<div class="col-md-2">
						<label for="renovaciones">Gestión Renovaciones</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="renovaciones" value="si" name="gestionRenovacion">
								<label class="onoffswitch-label" for="renovaciones">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>	
					<div class="col-md-2">
						<label for="Veroperaciones">Ver Operaciones</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="Veroperaciones" value="si" name="Veroperaciones">
								<label class="onoffswitch-label" for="Veroperaciones">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>	
					<div class="col-md-2">
						<label for="gestionTecnica">Gestión Tecnica</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="gestionTecnica" value="si" name="gestionTecnica">
								<label class="onoffswitch-label" for="gestionTecnica">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<label for="editarTecnicos">Editar Tecnicos</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="editarTecnicos" value="si" name="editarTecnicos">
								<label class="onoffswitch-label" for="editarTecnicos">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>	
				</div>
				<div class="row">
					<div class="col-md-2">
						<label for="ventasPorEjecutivos">Ventas por ejecutivo</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="ventasPorEjecutivos" value="si" name="ventasPorEjecutivos">
								<label class="onoffswitch-label" for="ventasPorEjecutivos">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>	
					<div class="col-md-2">
						<label for="ventasPorProducto">Ventas por producto</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="ventasPorProducto" value="si" name="ventasPorProducto">
								<label class="onoffswitch-label" for="ventasPorProducto">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>	
					<div class="col-md-3">
						<label for="reportePersonalizado">Reporte personalizado</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="reportePersonalizado" value="si" name="reportePersonalizado">
								<label class="onoffswitch-label" for="reportePersonalizado">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>					
					<div class="col-md-2">
						<label for="programacion">Programacion</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="programacion" value="si" name="programacion" >
								<label class="onoffswitch-label" for="programacion">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>	
				</div>
				<hr>
				<div class="row">
					<div class="col-md-2">
						<label for="confirmarRegistro">Confirmar Registros</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="confirmarRegistro" value="si" name="confirmarRegistro">
								<label class="onoffswitch-label" for="confirmarRegistro">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>

					<div class="col-md-2">
						<label for="programarInstalacion">Programar instalacion</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="programarInstalacion" value="si" name="programarInstalacion">
								<label class="onoffswitch-label" for="programarInstalacion">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
				

					<div class="col-md-2">
						<label for="adminComercial">Adminsitrar Comercial</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="adminComercial" value="si" name="adminComercial">
								<label class="onoffswitch-label" for="adminComercial">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<label for="operacionesComerciales">Operaciones comerciales</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="operacionesComerciales" value="si" name="operacionesComerciales">
								<label class="onoffswitch-label" for="operacionesComerciales">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>						
					<div class="col-md-2">
						<label for="chequeoAmonitoreo">Chequeo a monitoreo</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="chequeoAmonitoreo" value="si" name="chequeoAmonitoreo">
								<label class="onoffswitch-label" for="chequeoAmonitoreo">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>

				</div>

				<hr>
				<div class="row">
						
					<div class="col-md-2">
						<label for="contabilidad">Contabilidad</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="contabilidad" value="si" name="contabilidad">
								<label class="onoffswitch-label" for="contabilidad">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
						
					<div class="col-md-2">
						<label for="cobranzas">Cobranzas</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="cobranzas" value="si" name="cobranzas">
								<label class="onoffswitch-label" for="cobranzas">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>

						
					<div class="col-md-2">
						<label for="pagoComisiones">Pago Comisiones</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="pagoComisiones" value="si" name="pagoComisiones">
								<label class="onoffswitch-label" for="pagoComisiones">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
						
					<div class="col-md-2">
						<label for="reporteComisiones">Reporte Comisiones</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="reporteComisiones" value="si" name="reporteComisiones">
								<label class="onoffswitch-label" for="reporteComisiones">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<label for="reporteContratosOcto">Reporte Contratos Octo</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="reporteContratosOcto" value="si" name="reporteContratosOcto">
								<label class="onoffswitch-label" for="reporteContratosOcto">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>

				</div>

				<hr>
				<div class="row">
					<div class="col-md-2">
						<label for="desinstalacion">Desinstalación</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="desinstalacion" value="si" name="desinstalacion">
								<label class="onoffswitch-label" for="desinstalacion">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<label for="reprogramacion">Reprogramación</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="reprogramacion" value="si" name="reprogramacion">
								<label class="onoffswitch-label" for="reprogramacion">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-2">
						<label for="reclamosClientes">Reclamos clientes</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="reclamosClientes" value="si" name="reclamosClientes">
								<label class="onoffswitch-label" for="reclamosClientes">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
				</div>					
				<hr>
				<div class="row">
					<div class="col-md-2">
						<label for="admin">Adminsitrar</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="admin" value="si" name="admin">
								<label class="onoffswitch-label" for="admin">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
					<!-- Toyota Anomalias CEM -->
					<div class="col-md-2">
						<label for="seguimientoToyotaAnomalia">Toyota Anomalias CEM</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="seguimientoToyotaAnomalia" value="si" name="seguimientoToyotaAnomalia">
								<label class="onoffswitch-label" for="seguimientoToyotaAnomalia">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
					<!-- Clientes MTC -->
					<div class="col-md-2">
						<label for="clienteMTC">Cliente MTC</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="clienteMTC" value="si" name="clienteMTC">
								<label class="onoffswitch-label" for="clienteMTC">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
				</div>

					<div class="row">
						<div class="col-lg-6"><button class="btn btn-success col-lg-7" type="submit" name="registrarNuevoUsuario">Continuar</button></div>

					</div>



				</div> 
			</form>
			<hr>
			<input type="text" class="form-control input-sm m-b-xs" id="filter"
			placeholder="Filtrar usuarios">

			<table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
				<thead>
					<tr>
						<th>#</th>
						<th>Nombre</th>
						<th>Apellido Paterno</th>
						<th>Apellido Materno</th>
						<th>Usuario</th>
						<th data-hide="phone,tablet">Area</th>
						<th data-hide="phone,tablet">Cargo</th>
		        			<th>Acciones</th><!-- 
		        			<th data-hide="all">Plan</th>
		        			<th data-hide="all">Chasis</th>
		        			<th data-hide="all">Color</th> -->

		        			<!-- <th data-hide="phone,tablet">Creado por</th> -->

		        			<!-- <th data-hide="all">Task</th> -->
		        		</tr>
		        	</thead>    
		        	<tbody>
		        		<?php $usuarioData=Usuario::obtenerUsuarios() ?>
		        		<?php $n=1; ?>
		        		<?php foreach ($usuarioData as $var): ?>
		        			<tr class="gradeX">
		        				<td><?php echo $n++ ?></td>
		        				<td><?php echo $var->usuarioNombre ?></td>
		        				<td><?php echo $var->usuarioApellidoPaterno ?></td>
		        				<!-- <td><?php echo obtenerEstado($var->estado)  ?></td> -->
		        				<td><?php echo $var->usuarioApellidoMaterno ?></td>
		        				<td><?php echo $var->usuario ?></td>
		        				<td><?php $rolObj=Usuario::obtenerRoldeUsuario($var->idrol); echo $rolObj->area ?></td>
		        				<td><?php echo $rolObj->cargo ?></td>
		        				<td><button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#administrar" data-id="<?php echo $var->idusuario ?>">Editar <i class="fa fa-cog"> </i></button></td>
		        			</tr>
		        		<?php endforeach ?>

		        	</tbody>
		        	<tfoot>
		        		<tr>
		        			<td colspan="5">
		        				<ul class="pagination pull-right"></ul>
		        			</td>
		        		</tr>
		        	</tfoot>
		        </table>
		    </div>




		    <?php include("../includes/footer.php") ?>


<!-- modal aqui desarrollaremos   -->


            <div class="modal inmodal" id="administrar" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content animated bounceIn">
                        <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                            <h4 class="modal-title">Administrar</h4>
                            <small class="font-bold">admisnitrator here sudo :V.   <div class="form-group ">
                            </div></small>
                        </div>
                        <form  role="form" method="post" >

                            <div class="modal-body" style="background: #fff;padding-top: 0">
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>                                
                                <button type="submit" name="guardarCambios" class="btn btn-primary">Guardar Cambios</button>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>



            <!-- modal aqui fin  desarrollaremos   -->

		    <!-- Mainly scripts -->
		    <script src="../assets/js/jquery-2.1.1.js"></script>
		    <script src="../assets/js/bootstrap.min.js"></script>
		    <script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
		    <script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

		    <!-- Custom and plugin javascript -->
		    <script src="../assets/js/inspinia.js"></script>
		    <script src="../assets/js/plugins/pace/pace.min.js"></script>
		    <script src="../assets/js/plugins/footable/footable.all.min.js"></script>
		    <!-- Steps -->
		    <script src="../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
		    <script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
		    <script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>

		    <script src="../assets/js/plugins/iCheck/icheck.min.js"></script>
		    <?php include_once('../includes/script.php') ?>
		    <script>
		  
		    	$(document).ready(function(){
		    		$('.footable').footable();
		    		$('#administrar').on('show.bs.modal', function (e) {
		    			var rowid = $(e.relatedTarget).data('id');
		    			$.ajax({
		    			type : 'post',
            			url : 'modal/administradorDatamodal', //Here you will fetch records 
            			data :  'rowid='+ rowid, //Pass $id
 			           success : function(data){
                    $('.modal-body').html(data);//Show fetched data from database
                }
            });
		    		});
		    	});

		    </script>

		</body>

		</html>
	<?php else: ?> 
		<?php $session->salir();
		echo "NO NO NO NO ; presiona F5"
		?> 
	<?php endif ?>
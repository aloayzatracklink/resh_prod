<?php $nav="Helpdesk | Auditoria de Sistemas" ?>
<?php include("../includes/head.php") ?>
<?php include("../includes/init.php") ?>
<?php
    if (!$session->yaEstaIngresado()) {
        irA("ingresar");
    }
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<body>
    <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!-- Custom Style -->
    <style>
        .p-4{
            padding: 4rem;
        }
        .p-2{
            padding: 2rem;
        }
        .px-4{
            padding-left: 4rem;
            padding-right: 4rem;
        }
        .px-2{
            padding-left: 2rem;
            padding-right: 2rem;
        }
        .m-4{
            margin: 4rem;
        }
        .m-2{
            margin: 2rem;
        }
        .mx-4{
            margin-left: 4rem;
            margin-right: 4rem;
        }
        .mx-2{
            margin-left: 2rem;
            margin-right: 2rem;
        }
        .bg-white{
            background-color: #ffffff;
        }
        .rounded{
            border-radius: 16px;
        }
        .card-shadow{
            /* box-shadow: rgba(17, 12, 46, 0.15) 0px 48px 100px 0px; */
            /* box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px; */
            box-shadow: rgba(255, 255, 255, 0.1) 0px 1px 1px 0px inset, rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;
        }
        .thead:hover{
            background-color: rgba(0, 0, 0, 0.1);
        }
        .TableContainer{
            /* width: 80%; */
            margin: 0 auto;
            /* min-width: 500px; */
            overflow-x: auto;
            /* background-color: ; */
            box-shadow: rgba(255, 255, 255, 0.1) 0px 1px 1px 0px inset, rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;
        }
        .TableHolder{
            margin: 0 auto;
            width: 100%;
            text-align: left;
            color: #000;
            cursor: default;
            border-spacing: 0;
            border-collapse: collapse;
        }
        .TableHolder tr:first-child th{
            border-bottom: 1px solid rgba(0, 0, 0, 0.3);
            padding: 15px 15px;
            background-color: #ffffff44;
            font-size: 1.1em;
        }
        .TableHolder tr td, .TableHolder tr:not(:first-child) th{
            padding: 12px 20px;
            position: relative;
            font-weight: normal;
        }
        .TableHolder tr:not(:first-child) th{
            transition: all 1s ease-out;
        }

        .TableHolder tr td::after, .TableHolder tr:not(first-child) th::after{
            width: 0;
            height: 2px;
            background-color: #ffffff22;
            content: " ";
            position: absolute;
            bottom: 0;
            left: 0;
            transition: width 0.1s ease-out 0.2s, height 0.2s ease 0.4s;
        }

        .TDActiv::after{
            width: calc(100% - 20px) !important;
            height: 100% !important;
        }
        .text-xl{
            font-size: 1.5rem;
        }
        .button-custom{
            background-color: transparent !important;
            border: none;
        }
        .button-custom:hover h4{
            /* background-color: rgba(0, 0, 0, 0.5) !important; */
            color:#1c84c6;

        }
        .text-primary{
            color: #1ab394;
        }
        .badge-name{
            background-color: #363062;
            color: #ffffff;
        }
        .bg-code{
            background-color: #20262e !important;
        }
    </style>
    <link href="../assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">

    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <?php include("../includes/menuIzquierdo.php") ?>
            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>
                </nav>
            </div>
            <!-- Main content -->
            <div class="row bg-white p-4 m-4 rounded">
                <div class="col-12">
                    <h2><strong>Auditoria de Sistemas y Procesos</strong></h2>
                    <p>Módulo para gestión de procesos y sistemas dentro de la empresa. Sean servicios (linux), API's, Procesos automáticos, entre otros.
                    De manera externa como interna.
                    </p>
                </div>
                <div class="col-12" style="margin-top: 2rem;">
                    <div class="bg-white p-4 rounded card-shadow">
                        <h4>Buscador</h4>
                        <div style="display: flex;">
                            <input type="text" name="searchLog" id="searchLog" class="form-control" placeholder="Ingresa un parámetro">
                            <button class="btn btn-primary" style="margin-left: 1rem;" id="searchBtn"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 6rem;min-height: 30vh;">
                    <div class="col-md-2 card-shadow p-2 rounded">
                        <h3 class="text-primary">Lista de Procesos</h3>
                        <button class="button-custom" data-id="sftp"><h4>Sftp-service</h4></button>
                        <button class="button-custom" data-id="web"><h4>Web concesionarios</h4></button>
                        <button class="button-custom" data-id="resh"><h4>Mail Bienvenida</h4></button>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-9 TableContainer">
                        <table class="TableHolder">
                            <tr>
                                <th>#</th>
                                <th>Mensaje</th>
                                <th>Cuerpo de envio</th>
                                <th>Codigo de respuesta</th>
                                <th>Cuerpo de respuesta</th>
                                <th>Nombre del proceso</th>
                                <th>Tipo de tarea</th>
                                <th>Nombre de Proceso</th>
                                <th></th>
                            </tr>
                            <tbody id="resultado"></tbody>
                        </table>
                        <p class="text-xl" style="margin-right: 1rem;text-align:end;"><strong id="page"></strong></p>
                        <div style="display: flex;padding:1rem;">
                            <button class="btn btn-success" id="back"style="width: 50%;"><strong><</strong></button>
                            <button class="btn btn-success" id="next"style="width: 50%;margin-left:1rem;"><strong>></strong></button>
                        </div>
                    </div>
                </div>
            </div>
            <?php include("../includes/footer.php") ?>
        </div>
        <div class="modal inmodal" id="verLog" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated bounceIn">
                    <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                        <h4 class="modal-title">Ver Log</h4>
                    </div>
                    <div class="modal-body" style="background: #fff;padding-top: 0">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Regresar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../assets/js/jquery-2.1.1.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../assets/js/inspinia.js"></script>
    <script src="../assets/js/plugins/pace/pace.min.js"></script>
    <script src="../assets/js/plugins/footable/footable.all.min.js"></script>
    <script src="../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
    <script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="../assets/js/plugins/iCheck/icheck.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
    <script>
        var page = 1

        //* Fetch initial data
        $(document).ready(function(e){
            $.ajax({
                type: 'post',
                url: 'pag/obtenerLogs',
                success: function (data) {
                    $('#resultado').html(data);
                    $('#page').text(`Página ${page}`);
                },
            });
        });

        //* Display Modal
        $('#verLog').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            $.ajax({
                type : 'post',
                url : 'modal/verLogDataModal',
                data :  'rowid='+ rowid,
                success : function(data){
                    $('.modal-body').html(data);
                }
            });
        });

        //* Fetch data with filters
        $('.button-custom').on('click', function (e){
            let filter = $(this).data('id');
            $.ajax({
                type: 'post',
                data: 'processName='+filter,
                url: 'pag/obtenerLogs',
                success: function(data){
                    $('#resultado').html(data);
                },
                error: function(){
                    Swal.fire({
                        icon: 'info',
                        title: '¿Que pasó?',
                        text: `Parece que la página ${page} no existe!`
                    });
                }
            });
        });

        //* Search
        $('#searchBtn').on('click', function(e){
            let searchText = $('#searchLog').val();
            $.ajax({
                type: 'post',
                url: 'pag/obtenerLogs',
                data: 'searchText='+searchText,
                success: function(data){
                    $('#resultado').html(data);
                },
                error: function(){
                    Swal.fire({
                        icon: 'info',
                        title: '¿Que pasó?',
                        text: `Parece que la página ${page} no existe!`
                    });
                }
            });
        });

        //* Pagination
        $('#back').on('click', function(e){
            if(page == 1){
                Swal.fire({
                    icon: 'info',
                    title: '¿Que pasó?',
                    text: 'Parece que estás en la pagína 1 intenta avanzar!'
                });
                return;
            }
            $.ajax({
                type: 'post',
                url: 'pag/obtenerLogs',
                data: 'page='+page,
                success: function (data){
                    $('#resultado').html(data);
                    page--;
                    $('#page').text(`Página ${page}`);
                }
            });
        })
        $('#next').on('click', function(e){
            postPage = page;
            $.ajax({
                type: 'post',
                url: 'pag/obtenerLogs',
                data: 'page='+page,
                success: function (data){
                    $('#resultado').html(data);
                    page++;
                    $('#page').text(`Página ${page}`);
                },
                error: function(){
                    Swal.fire({
                        icon: 'info',
                        title: '¿Que pasó?',
                        text: `Parece que la página ${++postPage} no existe!`
                    });
                }
            });
        })
    </script>
</body>

<?php include("../../includes/init.php") ?>
<?php
if (isset($_POST['rowid'])){

    $log = AuditoriaSistemas::verLogPorId($_POST['rowid']); 

    if($log[0]){
    $logData = $log[0]
?>
    <style>
        .bg-code{
            background-color: #20262e !important;
        }
        .p-4{
            padding: 4rem;
        }
        .p-2{
            padding: 2rem;
        }
        .px-4{
            padding-left: 4rem;
            padding-right: 4rem;
        }
        .px-2{
            padding-left: 2rem;
            padding-right: 2rem;
        }
        .m-4{
            margin: 4rem;
        }
        .m-2{
            margin: 2rem;
        }
        .mx-4{
            margin-left: 4rem;
            margin-right: 4rem;
        }
        .mx-2{
            margin-left: 2rem;
            margin-right: 2rem;
        }
        .bg-white{
            background-color: #ffffff;
        }
        .rounded{
            border-radius: 16px;
        }
        .bg-code p{
            color: #cd9076;
        }
    </style>
    <div class="row p-2">
        <div class="col-12 col-md-6">
            <label for="message">Mensaje</label>
            <input type="text" class="form-control" name="message" value="<?= utf8_encode($logData->message) ?>" disabled>
        </div>
        <div class="col-12 col-md-6">
            <label for="statusCode">Código de respuesta</label>
            <input type="text" class="form-control" name="statusCode" value="<?= $logData->statusCode ?>" disabled>
        </div>
        <div class="col-md-12" style="margin-top: 1rem;">
            <label for="sBody">Cuerpo de envío</label>
            <div class="bg-code" style="max-height: 500px; overflow-y:scroll;">
                <p class="rounded p-4 text-white"><?= AuditoriaSistemas::prettyJson($logData->body) ?></p>
            </div>
            <div class="glow-container">
                <div class="augs" data-augmented-ui></div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top: 1rem;">
            <label for="resBody">Cuerpo de respuesta</label>
            <div class="bg-code" style="max-height: 500px; overflow-y:scroll;">
                <p class="rounded p-4 text-white"><?= AuditoriaSistemas::prettyJson($logData->responseBody) ?></p>
            </div>
            <div class="glow-container">
                <div class="augs" data-augmented-ui></div>
            </div>
        </div>
        <div class="col-12 col-md-6" style="margin-top: 1rem;">
            <label for="message">Nombre de proceso</label>
            <input type="text" class="form-control" name="message" value="<?= utf8_encode($logData->scriptName) ?>" disabled>
        </div>
        <div class="col-12 col-md-6" style="margin-top: 1rem;">
            <label for="server">Servidor</label>
            <input type="text" class="form-control" name="server" value="<?= utf8_encode($logData->scriptServer) ?>" disabled>
        </div>
        <div class="col-md-12" style="margin-top: 1rem !important;margin-left:0.2rem; margin-right: 0.2rem;">
            <label for="dProcess" style="margin-top: 1rem !important;">Descripción del proceso</label>
            <textarea class="form-control" name="dProcess" rows="3" disabled style="resize:none"><?= utf8_encode($logData->scriptDescription) ?></textarea>
        </div>
        <div class="col-12 col-md-6" style="margin-top: 1rem;">
            <label for="jobType">Tipo de Tarea</label>
            <input type="text" class="form-control" name="jobType" value="<?= utf8_encode($logData->jobType) ?>" disabled>
        </div>
        <div class="col-12 col-md-6" style="margin-top: 1rem;">
            <label for="dCreate">Fecha de creación</label>
            <input type="text" class="form-control" name="dCreate" value="<?= date('d/m/Y', strtotime($logData->dateCreate)) ?>" disabled>
        </div>
    </div>
<?php
    }else{
        echo "No se pudo obtener los datos del log";
    }

}else{
    echo "No se ha encontrado el registro";
}
?>
<?php 
if (isset($_POST['rowid'])) {
	include_once("../../includes/init.track.php");
    $onObj=new Onix;
    $idsclievh=$_POST['rowid'];	
    $idsArr=explode(",",$idsclievh);
    $idcustomer=soloNumero($idsArr[0]);
    $idvehicle=soloNumero($idsArr[1]);
    $idlocation=soloNumero($idsArr[2]);
    $onObj->idvehicle=$idvehicle;
    $onObj->idcustomer=$idcustomer;
    $onObj->idlocation=$idlocation;
    if ($simD=$onObj->obtenerSim()) {
       $sim=$simD->serie; 
       $hObj=new Healt;
       $estado=$hObj->mostrarEstadoVehiculoDePlataformaWeb($sim);
       if ($estado==0 OR $estado==1) {
           
       }else{
           $sim=false;
       }
       

    }else{
        $sim=false;
    }
    $datosCliente=Onix::obtenerDatosDesdeIds($idcustomer,$idvehicle,$idlocation);
    $dPlan=Onix::obtenerPlanDeServicio($idcustomer,$idvehicle,$idlocation);
    $Vence=Onix::obtenerDatosDecontrato($idcustomer,$idvehicle,$idlocation);

?>
<link href="../../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
<style>
section {
  display: block;
  position: relative;
  margin-top: 2em;
}

.labell {
  width: 100%;
  border-radius: 3px;
  border: 1px solid #d1d3d4;
  color: #d4d4d4;
  background: #ffffff;
  line-height: 2.5em;
  text-indent: 3.25em;
}

input.radio:empty {
  margin-left: -9999px;
}

input.radio:empty ~ label {
  display: block;
  position: relative;
  bottom: 0;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

input.radio:empty ~ label:before {
  position: absolute;
  display: block;
  top: 0;
  bottom: 0;
  left: 0;
  content: '';
  width: 2.5em;
  background: #D1D3D4;
  border-radius: 3px 0 0 3px;
}

input.radio:hover:not(:checked) ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #C2C2C2;
}

input.radio:hover:not(:checked) ~ label {
  color: #888;
  -webkit-transition: 0.25s ease-in-out;
  transition: 0.25s ease-in-out;
}

input.radio:checked ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #FFFFFF;
  background: #4DCB6D;
  -webkit-transition: 0.25s ease-in-out;
  transition: 0.25s ease-in-out;
}

input.radio:checked ~ label {
  color: #777;
  -webkit-box-shadow: 3px 3px 10px 0px rgba(69, 90, 100, 0.5);
          box-shadow: 3px 3px 10px 0px rgba(69, 90, 100, 0.5);
  -webkit-transition: 0.25s ease-in-out;
  transition: 0.25s ease-in-out;
}

input.radio:focus ~ label:before {
  -webkit-transition: 0.25s ease-in-out;
  transition: 0.25s ease-in-out;
  color: white;
}
</style>
<div class="" style="padding-top: 11px; padding-bottom: 10px;">
<div class="row">
<div class="row">
    <div class="form-group col-md-12">
        <label for="numeroTecnico" >Nombre de cliente*</label>
        <input  style="border: none;" readonly value='<?= $datosCliente->apellidos." ".$datosCliente->nombre ?>' type="text"  class="border-bottom form-control" required placeholder='Nombre Cliente'>
    </div>
<input type="hidden" name="nombreCl" value='<?= $datosCliente->apellidos." ".$datosCliente->nombre ?>' >
</div>
<div class="row">
    <div class="form-group col-md-3">
        <label for="numeroTecnico" >Placa Vehiculo*</label>
        <input  style="border: none;" readonly value='<?= $datosCliente->placa ?>' type="text" placeholder="Placa" id="numeroTecnico" name="placa" class="border-bottom form-control" required  >
<input type="hidden" name="placaCl" value="<?= $datosCliente->placa ?>" >
    </div>
    <div class="form-group col-md-4">
        <label for="Observaciones" >Chasis*</label>
        <input required   style="border: none;" readonly value='<?= $datosCliente->chasis ?>' type="text" placeholder="Chasis" id="Observaciones" class="border-bottom form-control" name="chasis" >
    </div>
<div class="form-group col-md-5">
        <label for="Observaciones" >PLan*</label>
        <input required   style="border: none;" readonly value='<?= $dPlan->articulo ?>' type="text" placeholder="Chasis" id="Observaciones" class="border-bottom form-control" name="chasis" >
    </div>
</div>
<div class="row">
    <div class="form-group col-md-12">
<h3 class='text-center'> <?=  vence($Vence->hasta)."<br> ".formatoSinHoraAniOPrimero($Vence->hasta) ?>
</div>
</div>
<?php
   if ($sim) {
      ?>
<input type="hidden" name="estadoAnt" value="<?= $estado ?>" >
<div class="row">
    <div class="form-group col-md-12">
 <section>
 <input type="radio" name="estado" value='1' id="radio1" class="radio" <?= ($estado==1) ? 'checked':'' ?> />
    <label for="radio1" class='labell'>Activado</label>
  </section>
 <section>
    <input type="radio" value='0' name="estado" id="radio2" class="radio" class='bg-danger' <?= ($estado==0) ? 'checked':'' ?>/>
    <label for="radio2" class='labell' >No activado</label>
  </section>
</div>
</div>

      <?php 
   } 
?>
</div>
</div>         
<input type="hidden" name="idvehicle" value="<?=  $idvehicle ?>" >
<input type="hidden" name="idcustomer" value="<?=  $idcustomer ?>" >
<input type="hidden" name="idlocation" value="<?=  $idlocation ?>" >
<input type="hidden" name="sim" value="<?=  $sim ?>" >

<?php } ?>


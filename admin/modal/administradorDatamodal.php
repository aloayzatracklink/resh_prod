

<?php 
if ($_POST['rowid']) {
  include_once("../../includes/head.php");
  	$idusuario=htmlspecialchars(htmlentities($_POST['rowid']));
  	$usuarioObj=Usuario::obtenerUsuario($idusuario);
	   $menuObj=Usuario::obtenerMenu($idusuario);

  ?><br>
    <input type="hidden" name="idusuario" value="<?php echo $usuarioObj->idusuario ?>" >
    <input type="hidden" name="cont" value="<?php echo $usuarioObj->contrasenia ?>" >
  <div class="row">          
  <div class="form-group col-md-4">
      <label for="nombre1" class="sr-only">Nombre</label>
      <input style="border: none;" type="text" placeholder="Nombre" id="nombre1" name="nombre" class="border-bottom form-control" required value="<?php echo $usuarioObj->usuarioNombre  ?>">
    </div>
    <div class="form-group col-md-4">
      <label for="apellidos1" class="sr-only">Apellido Paterno</label>
      <input style="border: none;" type="text" placeholder="Apellido Paterno" id="apellidos1" name="apellidoPaterno" class="border-bottom form-control " required value="<?php echo $usuarioObj->usuarioApellidoPaterno ?>">
    </div>
    <div class="form-group col-md-4">
      <label for="apellidoMaterno1" class="sr-only">Apellido Materno</label>
      <input style="border: none;" type="text" placeholder="Apellido Materno" id="apellidoMaterno1" name="apellidoMaterno" class="border-bottom form-control " required value="<?php echo $usuarioObj->usuarioApellidoMaterno ?>">
    </div>     
  </div>
  <div class="row">          
  <div class="form-group col-md-4">
      <label for="usuarioa1" class="sr-only">Usuario</label>
      <input style="border: none;" type="text" placeholder="Usuario" id="usuarioa1" name="usuario" class="border-bottom form-control" required value="<?php echo $usuarioObj->usuario ?>">
    </div>
    <div class="form-group col-md-4">
      <label for="lugarInstalacion" class="sr-only">Area Cargo</label>
      <select  style="border: none;" class="rolid border-bottom form-control text-capitalize" required name="idrol"><?php $objRol=Usuario::obtenerRoldeUsuario($usuarioObj->idrol) ?>
                            <option class="text-capitalize" value="<?php echo $usuarioObj->idrol ?>"  
                            selected > <?php echo $objRol->area ?> - <?php  echo $objRol->cargo ?></option>
                            <?php  ?>
                            <?php $usuarioObj->obtenerRoles() ?>            
                          </select>
    </div>
           
  <div class="form-group col-md-4">
      <label for="contrasenia" class="sr-only">Contrasenia</label>
      <input style="border: none;" type="text" placeholder="Contrasenia" id="contrasenia" name="contrasenia" class="border-bottom form-control" >
    </div>
  </div>
  <div class="row">  
  <?php if ($menuObj): ?>
            
          
<!-- 

     <div class="form-group col-md-12">
      <label for="observacion" class="sr-only">Observaciones</label>
      <input style="border: none;" type="text" placeholder="Observaciones" id="observacion" name="observacion" class="border-bottom form-control" value="<?php echo $retVal = (isset($operacionObj->observacion)) ? $operacionObj->observacion : "" ; ?>" >
    </div> -->
    <div class="row">
                        <div class="col-md-1">
                          <label for="example11">Panel</label>
                          <div class="switch">
                            <div class="onoffswitch">
                              <input type="checkbox" <?php echo $retVal = ($menuObj->panel=="si") ? "checked" : "" ; ?> class="onoffswitch-checkbox" id="example11" value="si" name="panel">
                              <label class="onoffswitch-label" for="example11">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-1">
                          <label for="example22">Baterias</label>
                          <div class="switch">
                            <div class="onoffswitch">
                              <input type="checkbox" <?php echo $retVal = ($menuObj->baterias=="si") ? "checked" : "" ; ?>  class="onoffswitch-checkbox" id="example22" value="si" name="baterias">
                              <label class="onoffswitch-label" for="example22">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                            </div>
                          </div>
                        </div>  <div class="col-md-1">
                        <label for="example33">Instalaciones</label>
                        <div class="switch">
                          <div class="onoffswitch">
                            <input type="checkbox" <?php echo $retVal = ($menuObj->instalaciones=="si") ? "checked" : "" ; ?>  class="onoffswitch-checkbox" id="example33" value="si" name="instalaciones">
                            <label class="onoffswitch-label" for="example33">
                              <span class="onoffswitch-inner"></span>
                              <span class="onoffswitch-switch"></span>
                            </label>
                          </div>
                        </div>
                      </div>  <div class="col-md-1">
                      <label for="example44">Chequeos</label>
                      <div class="switch">
                        <div class="onoffswitch">
                          <input type="checkbox" class="onoffswitch-checkbox" id="example44" value="si" name="chequeos" <?php echo $retVal = ($menuObj->chequeos=="si") ? "checked" : "" ; ?>>
                          <label class="onoffswitch-label" for="example44">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                          </label>
                        </div>
                      </div>
                    </div>  <div class="col-md-1">
                    <label for="choques1">Choques</label>
                    <div class="switch">
                      <div class="onoffswitch">
                        <input type="checkbox" class="onoffswitch-checkbox" id="choques1" value="si" name="choques" <?php echo $retVal = ($menuObj->choques=="si") ? "checked" : "" ; ?>>
                        <label class="onoffswitch-label" for="choques1">
                          <span class="onoffswitch-inner"></span>
                          <span class="onoffswitch-switch"></span>
                        </label>
                      </div>
                    </div>
                  </div>  <div class="col-md-1">
                  <label for="robo1">Robos</label>
                  <div class="switch">
                    <div class="onoffswitch">
                      <input type="checkbox" class="onoffswitch-checkbox" id="robo1" value="si" name="robo" <?php echo $retVal = ($menuObj->robos=="si") ? "checked" : "" ; ?>>
                      <label class="onoffswitch-label" for="robo1">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                      </label>
                    </div>
                  </div>
                </div>  <div class="col-md-2">
                <label for="maps1">Maps</label>
                <div class="switch">
                  <div class="onoffswitch">
                    <input type="checkbox" class="onoffswitch-checkbox" id="maps1" value="si" name="maps" <?php echo $retVal = ($menuObj->maps=="si") ? "checked" : "" ; ?>>
                    <label class="onoffswitch-label" for="maps1">
                      <span class="onoffswitch-inner"></span>
                      <span class="onoffswitch-switch"></span>
                    </label>
                  </div>
                </div>
              </div>  <div class="col-md-2">
              <label for="operaciones1">Asignar Operaciones</label>
              <div class="switch">
                <div class="onoffswitch">
                  <input type="checkbox" class="onoffswitch-checkbox" id="operaciones1" value="si" name="operaciones" <?php echo $retVal = ($menuObj->registrarOperaciones=="si") ? "checked" : "" ; ?>>
                  <label class="onoffswitch-label" for="operaciones1">
                    <span class="onoffswitch-inner"></span>
                    <span class="onoffswitch-switch"></span>
                  </label>
                </div>
              </div>
            </div>  <div class="col-md-2">
            <label for="clientes1">Registrar Clientes</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="clientes1" value="si" name="registrarClientes" <?php echo $retVal = ($menuObj->registrarCliente=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="clientes1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>  
        </div>
        <div class="row">
          <div class="col-md-2">
            <label for="cotizar1">Cotizar</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="cotizar1" value="si" name="cotizarCliente" <?php echo $retVal = ($menuObj->cotizar=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="cotizar1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>  
          <div class="col-md-2">
            <label for="post1">Gestion Post Venta</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="post1" value="si" name="gestionPostVenta" <?php echo $retVal = ($menuObj->gestionPostVenta=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="post1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>  
          <div class="col-md-2">
            <label for="renovaciones1">Gestion Renovaciones</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="renovaciones1" value="si" name="gestionRenovacion" <?php echo $retVal = ($menuObj->gestionRenovaciones=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="renovaciones1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>  
          <div class="col-md-2">
            <label for="Veroperaciones1">Ver Operaciones</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="Veroperaciones1" value="si" name="Veroperaciones" <?php echo $retVal = ($menuObj->verificarOperaciones=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="Veroperaciones1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>  
          <div class="col-md-2">
            <label for="gestionTecnica1">Gestion Tecnica</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="gestionTecnica1" value="si" name="gestionTecnica" <?php echo $retVal = ($menuObj->gestionTecnica=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="gestionTecnica1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <label for="editarTecnicos1">Editar Tecnicos</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="editarTecnicos1" value="si" name="editarTecnicos" <?php echo $retVal = ($menuObj->editarTecnicos=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="editarTecnicos1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>  
        </div>
        <div class="row">
          <div class="col-md-2">
            <label for="ventasPorEjecutivos1">Ventas por ejecutivo</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="ventasPorEjecutivos1" value="si" name="ventasPorEjecutivos" <?php echo $retVal = ($menuObj->verificarComercial=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="ventasPorEjecutivos1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>  
          <div class="col-md-2">
            <label for="ventasPorProducto1">Ventas por producto</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="ventasPorProducto1" value="si" name="ventasPorProducto" <?php echo $retVal = ($menuObj->verificarComercialP=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="ventasPorProducto1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>  
          <div class="col-md-3">
            <label for="reportePersonalizado1">Reporte personalizado</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="reportePersonalizado1" value="si" name="reportePersonalizado" <?php echo $retVal = ($menuObj->comercialPersonalizado=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="reportePersonalizado1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>  
          <div class="col-md-3">
            <label for="contabilidad1">Contabilidad</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="contabilidad1" value="si" name="contabilidad" <?php echo $retVal = ($menuObj->gestionContabilidad=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="contabilidad1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>        
          <div class="col-md-2">
            <label for="programacion1">Programacion</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="programacion1" value="si" name="programacion" <?= ($menuObj->programacion=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="programacion1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>  
        </div>
  <hr>
        <div class="row">
          <div class="col-md-2">
            <label for="confirmarRegistro1">Confirmar Registros</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="confirmarRegistro1" value="si" name="confirmarRegistro"  <?= ($menuObj->confirmarRegistroEnOnix=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="confirmarRegistro1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>

          <div class="col-md-2">
            <label for="programarInstalacion1">Programar Instalación</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="programarInstalacion1" value="si" name="programarInstalacion" <?php echo $retVal = ($menuObj->programarInstalacion=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="programarInstalacion1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>
        

          <div class="col-md-2">
            <label for="adminComercial1">Adminsitrar Comercial</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="adminComercial1" value="si" name="adminComercial" <?php echo $retVal = ($menuObj->administrarComercial=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="adminComercial1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <label for="operacionesComerciales1">Operaciones comerciales</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="operacionesComerciales1" value="si" name="operacionesComerciales" <?php echo $retVal = ($menuObj->operacionesComerciales=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="operacionesComerciales1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>            
          <div class="col-md-2">
            <label for="chequeoAmonitoreo1">Chequeo a monitoreo</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="chequeoAmonitoreo1" value="si" name="chequeoAmonitoreo" <?php echo $retVal = ($menuObj->chequeoMonitoreo=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="chequeoAmonitoreo1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>

        </div>

        <hr>
        <div class="row">
          <div class="col-md-2">
            <label for="cobranzas1">Cobranzas</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="cobranzas1" value="si" name="cobranzas" <?php echo $retVal = ($menuObj->cobranzas=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="cobranzas1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>

            
          <div class="col-md-2">
            <label for="pagoComisiones1">Pago Comisiones</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="pagoComisiones1" value="si" name="pagoComisiones" <?php echo $retVal = ($menuObj->pagoComision=="si") ? "checked" : "" ; ?> >
                <label class="onoffswitch-label" for="pagoComisiones1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>
            
          <div class="col-md-2">
            <label for="reporteComisiones1">Reporte Comisiones</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="reporteComisiones1" value="si" name="reporteComisiones" <?php echo $retVal = ($menuObj->comisiones=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="reporteComisiones1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>

          <div class="col-md-2">
						<label for="reporteContratosOcto2">Reporte Contratos Octo</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="reporteContratosOcto2" value="si" name="reporteContratosOcto" <?php echo $retVal = ($menuObj->reporteContratosOcto=="si") ? "checked" : "" ; ?>>
								<label class="onoffswitch-label" for="reporteContratosOcto2">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>

        </div>

        <hr>
        <div class="row">
          <div class="col-md-2">
            <label for="desinstalacion1">Desinstalación</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="desinstalacion1" value="si" name="desinstalacion" <?php echo $retVal = ($menuObj->desinstalacion=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="desinstalacion1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <label for="reprogramacion1">Reprogramación</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="reprogramacion1" value="si" name="reprogramacion"  <?php echo $retVal = ($menuObj->reprogramacion=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="reprogramacion1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>
        </div>
        <hr>
				<div class="row">
          <!-- Reclamo clientes -->
					<div class="col-md-2">
						<label for="reclamosClientes2">Reclamos clientes</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="reclamosClientes2" value="si" name="reclamosClientes" <?php echo $retVal = ($menuObj->reclamosClientes=="si") ? "checked" : "" ; ?>>
								<label class="onoffswitch-label" for="reclamosClientes2">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
          <!-- Toyota Anomalias CEM -->
          <div class="col-md-2">
						<label for="seguimientoToyotaAnomalia">Toyota Anomalias CEM</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="seguimientoToyotaAnomalia" value="si" name="seguimientoToyotaAnomalia" <?php echo $retVal = ($menuObj->seguimientoToyotaAnomalia=="si") ? "checked" : "" ; ?>>
								<label class="onoffswitch-label" for="seguimientoToyotaAnomalia">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
          <!-- Clientes MTC -->
          <div class="col-md-2">
						<label for="clienteMTC">Cliente MTC</label>
						<div class="switch">
							<div class="onoffswitch">
								<input type="checkbox" class="onoffswitch-checkbox" id="clienteMTC" value="si" name="clienteMTC" <?php echo $retVal = ($menuObj->clienteMTC=="si") ? "checked" : "" ; ?>>
								<label class="onoffswitch-label" for="clienteMTC">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
				</div>
        <hr>
        <div class="row">
          <div class="col-md-2">
            <label for="admin1">Adminsitrar</label>
            <div class="switch">
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="admin1" value="si" name="admin" <?php echo $retVal = ($menuObj->administrar=="si") ? "checked" : "" ; ?>>
                <label class="onoffswitch-label" for="admin1">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>
          </div>

  </div> 

<!-- fasd -->
<?php endif ?>        
  		</div> <!-- name="pruebas" -->


<?php
}else{
  header('Location:../../ingresar.php');
} 
?>

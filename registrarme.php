<?php include("public/publicHead.php") ?>
<?php 

if (!empty($_SESSION['dni'])||!empty($_SESSION['ruc'])){
if (!empty($_SESSION['dni'])) {
$cotizarObj=Cotizar::obtenerDatosPorDni($_SESSION['dni']);
if ($cotizarObj->estado==23){
irA("registroVehiculos");	
}else
if ($cotizarObj->estado==24){
irA("registroContactosAutorizados");	
}
}
if (!empty($_SESSION['ruc'])) {
$cotizarObj=Cotizar::obtenerDatosPorRuc($_SESSION['ruc']);
if ($cotizarObj->estado==23){
irA("registroVehiculos");	
}else
if ($cotizarObj->estado==24){
irA("registroContactosAutorizados");	
}
}
}else{		
irA("ingresarCliente");
unset($_SESSION['dni']);
}
$errores=array();
$clienteObj=new Cliente();

if (isset($_POST['registrarNatural'])) {
if ($clienteObj) {

if (!empty($_FILES['voucher']['name'])){
# code...
// cargar aqui la imagen  --voucher
$extencionesAbilitadas= array('jpg','jpeg','png','gif','');
$nombreArchivo=$_FILES['voucher']['name'];
$extencionArchivo=strtolower(end(explode('.', $nombreArchivo)));
$tamanioArchivo=$_FILES['voucher']['size'];
$ubicacionTemporal=$_FILES['voucher']['tmp_name'];

if (!in_array($extencionArchivo,$extencionesAbilitadas)) {
$errores[]='La extencion de la imagen deve ser jpg jpeg png gif';
}
if ($tamanioArchivo>5242880) {
$errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
}
}

$clienteObj->cotizacionid=$cotizarObj->cotizacionid;
$clienteObj->tipoCliente=$_POST['tipoCliente'];
$clienteObj->dni=$_POST['dni'];
$clienteObj->nombre=$_POST['nombre'];
$clienteObj->apellidosC=$_POST['apellidos'];
$clienteObj->departamento=$_POST['departamento'];
$clienteObj->provincia=$_POST['provincia'];
$clienteObj->distrito=$_POST['distrito'];
$clienteObj->direccion=$_POST['direccion'];
$clienteObj->referencia=$_POST['referencia'];
$clienteObj->telefono=$_POST['telefono'];
$clienteObj->telefono1=$_POST['telefono1'];
$clienteObj->correo=$_POST['correo'];
$clienteObj->correo1=$_POST['correo1'];
$clienteObj->fechaNacimiento=$_POST['fechaNacimiento'];
$clienteObj->tipoDocumentoFacturacion=$_POST['tipoDocumentoFacturacion'];
if (isset($_POST['tipoDocumentoFacturacion']) && $_POST['tipoDocumentoFacturacion']=="Factura") {

if (empty($_POST['rucFacturacion'])) {
$errores[]='Completar el RUC para la facturacion';
}else 
if (empty($_POST['razonSocialFacturacion'])) {
$errores[]='Completar el RUC para la facturacion';
}else
if (empty($_POST['direccionFiscalFacturacion'])) {
$errores[]='Completar el RUC para la facturacion';
}		
}
$clienteObj->rucFacturacion=$_POST['rucFacturacion'];
$clienteObj->razonSocialFacturacion=$_POST['razonSocialFacturacion'];
$clienteObj->direccionFiscalFacturacion	=$_POST['direccionFiscalFacturacion'];
$clienteObj->telefonoFacturacion=$_POST['telefonoFacturacion'];
$clienteObj->direccionEntrega=$_POST['direccionEntrega'];

$clienteObj->idCreador=$_SESSION['dni'];
if (empty($errores)) {			
if ($clienteObj->registrarClienteNaturalTemporal()) {
Cotizar::cambiarEstadoDni(23,$clienteObj->dni);
// $_SESSION['idcliente']=$clienteObj->idclienteTemporal;
$cotizarObjDetalle=new Cotizar();
if (!$objCotizarA=Cotizar::obtenerEstadoPagoDni($_SESSION['dni'])){
$cotizarObjDetalle->fecha=date("YmdHis");
move_uploaded_file($_FILES['voucher']['tmp_name'], 'public/vouchers/'.$cotizarObjDetalle->fecha.$_FILES['voucher']['name']); 
$cotizarObjDetalle->voucher=$cotizarObjDetalle->fecha.$nombreArchivo;
$cotizarObjDetalle->actualizarEstadoPagoDni($_SESSION['dni']);
}
irA("registroVehiculos");

}
}else{

}



}
}




// registrar empresa here
if (isset($_POST['registrarEmpresa'])) {
if (!empty($_FILES['voucher']['name'])){
// cargar aqui la imagen  --voucher
$extencionesAbilitadas= array('jpg','jpeg','png','gif','');
$nombreArchivo=$_FILES['voucher']['name'];
$extencionArchivo=strtolower(end(explode('.', $nombreArchivo)));
$tamanioArchivo=$_FILES['voucher']['size'];
$ubicacionTemporal=$_FILES['voucher']['tmp_name'];

if (!in_array($extencionArchivo,$extencionesAbilitadas)) {
$errores[]='La extencion de la imagen deve ser jpg jpeg png gif';
}
if ($tamanioArchivo>5242880) {
$errores[]='El tamaño del archivo no deve exeder los 5 megabytes';
}
}
// datos de la empresa aqui
$clienteObj->cotizacionid=$cotizarObj->cotizacionid;
$clienteObj->tipoCliente=$_POST['tipoCliente'];
$clienteObj->ruc=$_POST['ruc'];
$clienteObj->razonSocial=$_POST['razonSocial'];	
$clienteObj->telefono=$_POST['telefono'];
$clienteObj->provinciaL=$_POST['provinciaL'];
$clienteObj->departamentoL=$_POST['departamentoL'];
$clienteObj->distritoL=$_POST['distritoL'];
$clienteObj->direccionL=$_POST['direccionL'];
$clienteObj->telefono1=$_POST['telefono1'];
$clienteObj->correo=$_POST['correo'];
$clienteObj->correo1=$_POST['correo1'];
$clienteObj->sectorDesempenia=$_POST['sectorDesempenia'];
//Datos del representante legal aqui
$clienteObj->dni=$_POST['dni'];
$clienteObj->nombre=$_POST['nombre'];
$clienteObj->apellidosC=$_POST['apellidoS'];
$clienteObj->departamento=$_POST['departamento'];
$clienteObj->provincia=$_POST['provincia'];
$clienteObj->distrito=$_POST['distrito'];
$clienteObj->direccion=$_POST['direccion'];
$clienteObj->telefono2=$_POST['telefono2'];
$clienteObj->telefono3=$_POST['telefono3'];
$clienteObj->correo2=$_POST['correo2'];
$clienteObj->correo3=$_POST['correo3'];
$clienteObj->idCreador=$_SESSION['ruc'];
// Datos de facturacion
$clienteObj->tipoDocumentoFacturacion=$_POST['tipoDocumentoFacturacion'];
if (isset($_POST['tipoDocumentoFacturacion']) && $_POST['tipoDocumentoFacturacion']=="Factura") {

if (empty($_POST['rucFacturacion'])) {
$errores[]='Completar el RUC para la facturacion';
}else 
if (empty($_POST['razonSocialFacturacion'])) {
$errores[]='Completar el RUC para la facturacion';
}else
if (empty($_POST['direccionFiscalFacturacion'])) {
$errores[]='Completar el RUC para la facturacion';
}		
}
$clienteObj->rucFacturacion=$_POST['rucFacturacion'];
$clienteObj->razonSocialFacturacion=$_POST['razonSocialFacturacion'];
$clienteObj->direccionFiscalFacturacion	=$_POST['direccionFiscalFacturacion'];
$clienteObj->telefonoFacturacion=$_POST['telefonoFacturacion'];
$clienteObj->direccionEntrega=$_POST['direccionEntrega'];	$clienteObj->idCreador=$_SESSION['ruc'];
if (empty($errores)) {		
if ($clienteObj->registrarClienteEmpresaTemporal()) {
Cotizar::cambiarEstadoRuc(23,$clienteObj->ruc);
// $_SESSION['idcliente']=$clienteObj->idclienteTemporal;
$cotizarObjDetalle=new Cotizar();
if (!$objCotizarA=Cotizar::obtenerEstadoPagoRuc($_SESSION['ruc'])){
$cotizarObjDetalle->fecha=date("YmdHis");
move_uploaded_file($_FILES['voucher']['tmp_name'], 'public/vouchers/'.$cotizarObjDetalle->fecha.$_FILES['voucher']['name']); 
$cotizarObjDetalle->voucher=$cotizarObjDetalle->fecha.$nombreArchivo;
$cotizarObjDetalle->actualizarEstadoPagoRuc($_SESSION['ruc']);
}
irA("registroVehiculos");
}else{
// irA("robo");
}
}
}
if (isset($_POST['salir'])) {
unset($_SESSION['dni']);
unset($_SESSION['ruc']);
session_destroy();
irA("ingresarCliente");
}
?>
<link rel="stylesheet" href="assets/css2/style2.css">
<link href="assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<body class="gray-bg">
<div class="wrapper wrapper-content animated pulse" style="padding-top: 99px">
<div class="row">
<div class="col-md-8 col-md-offset-2">
<div class="ibox-content">
<?php foreach ($errores as $error): ?>
<span class="label label-danger"><?php echo $error ?></span>
<?php endforeach ?>
<div class="row"><div class="col-md-2 col-md-offset-11 col-xs-2 col-xs-offset-9"><form action="" method="POST"><button class="btn btn-warning col-lg-6" type="submit" name="salir">Salir <i class="fa fa-times-circle""></i></button></form></div></div>
<h2 class="font-bold text-center">Registrate aqui</h2>
<?php $usuario=Usuario::obtenerUsuario($cotizarObj->idCreador);  ?>
<div class="text-center"><small class="text-center">Si nececita ayuda puedes contactarte con <?php echo $usuario->usuarioNombre ?> <?php echo $usuario->usuarioApellidoPaterno ?> al 630 7575 anexo <?php echo $usuario->anexo ?></small><br><small>Los campos que finalizan en * son obligatorios</small></div>
<hr>
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div role="tablist">
<div class="tabs" aria-hidden="true">
<?php if (!empty($_SESSION['dni'])): ?>
<label for="starks" id="starks-tab" class=" text-center"><h4>Persona natural</h4></label>
<form role="form" method="POST" id="personaNatural" enctype="multipart/form-data">
<input type="radio" title="Starks" name="tipoCliente" id="starks" role="tab" aria-controls="starks-panel" checked value="Natural" />

<div class="row">


<div class="form-group col-md-3">
	<label for="dniI" class="sr-only">DNI</label>
	<input readonly style="border: none;" type="text" placeholder="DNI" id="numero" name="dni" class="form-control border-bottom " required  data-mask="99999999" id="dniI" value="<?php echo $cotizarObj->dni ?>">
</div>
<div class="form-group col-md-4">
	<label for="nombre" class="sr-only">Nombre</label>
	<input style="border: none;" type="text" placeholder="Nombres" id="nombre" name="nombre" class="form-control border-bottom text-capitalize"  value="<?php echo $cotizarObj->nombre ?>" id="name">
</div>
<div class="form-group col-md-5">
	<label for="apellidoPaterno" class="sr-only">Apellidos </label>
	<input style="border: none;" type="text" placeholder="Apellidos" id="apellidos" name="apellidos" class="form-control x2 text-capitalize border-bottom text-capitalize" required value="<?php echo $cotizarObj->apellidos ?>" >
</div>


</div> 
<div class="row">
<div class="form-group col-lg-2">
<label for="departamento1" class="sr-only">Departamento</label>
<select  style="border: none;" class="departamento border-bottom form-control text-capitalize" required name="departamento" id="departamento1">
<option class="text-capitalize" value="" disabled selected >Seleccione Departamento</option>
<?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
<?php if ($objDepartamento): ?>
<?php foreach ($objDepartamento as $var): ?>
	<option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
<?php endforeach ?>
<?php else: ?>
<?php endif ?>
</select>
</div>
<div class="form-group col-lg-2" >
<label for="corredor" class="sr-only">Provincia</label>
<select  style="border: none;" class="provincia1 border-bottom form-control text-capitalize" required name="provincia"  >
</select>
</div>
<div class="form-group col-lg-2" >
<label for="distrito" class="sr-only">Distrito</label>
<select  style="border: none;" class="distritos1 border-bottom form-control text-capitalize" required name="distrito"  >
</select>
</div>
<div class="form-group col-lg-6">
	<label for="direccion" class="sr-only">Direccion *</label>
	<input style="border: none;" type="text" placeholder="Direccion *" id="direccion" name="direccion" class="form-control border-bottom text-capitalize" required autocomplete="off">
</div>		                    
</div> 
<div class="row">
<div class="form-group col-md-6">
	<label for="referencia" class="sr-only">Referencia *</label>
	<input style="border: none;" type="text" placeholder="Referencia *" id="referencia" name="referencia" class="form-control border-bottom" required >
</div>
<div class="form-group col-md-3">
	<label for="telefono" class="sr-only">Celular</label>
	<input style="border: none;" type="text" placeholder="Celular" id="telefono" name="telefono" class="form-control border-bottom" required data-mask="999 999 999" value="<?php echo $cotizarObj->contacto ?>">
</div>
<div class="form-group col-md-3">
	<label for="telefono1" class="sr-only">Telefono</label>
	<input style="border: none;" type="text" placeholder="Telefono" id="telefono1" name="telefono1" class="form-control border-bottom" >
</div>


</div>
<div class="row">
<div class="form-group col-md-4">
	<label for="correo" class="sr-only">Correo</label>
	<input style="border: none;" type="email" placeholder="Correo" id="correo" name="correo" class="form-control x2 border-bottom" required value="<?php echo $cotizarObj->correo ?>">
</div>
<div class="form-group col-md-4">
	<label for="correo1" class="sr-only">Correo Alternativo</label>
	<input style="border: none;" type="text" placeholder="Correo Alternativo" id="correo1" name="correo1" class="form-control border-bottom"  >
</div>
<div class="form-group col-md-4">
	<label for="fechaNacimiento" class="sr-only">Fecha de Nacimiento *</label>
	<input style="border: none;" type="text" placeholder="Dia/Mes/Año Fecha Nacimiento *" id="fechaNacimiento" name="fechaNacimiento" class="form-control border-bottom" required data-mask="99/99/9999">

</div>

</div> 

<div class="row">
<div class="col-md-4">
	<select style="border: none;" id="selectNa" name="tipoDocumentoFacturacion" class="cd-selec form-control border-bottom" required >
		<option value="" disabled selected>Seleccione datos de facturación *</option>
		<option value="Boleta">Boleta</option>
		<option value="Factura">Factura</option>
	</select>   
</div>
<div class="col-md-8">
	<div class="content-pane e1">
		<h3>Seleccione pago</h3>
	</div>
	<div class="content-pane eBoleta">
		<h3>Continuar</h3>
	</div>
	<div class="content-pane eFactura">
		<div class="row"><br>
			<div class="form-group col-md-4">
				<label for="rucFacturacion" class="sr-only">Ruc *</label>
				<input style="border: none;" type="number" placeholder="Ruc *" id="rucFacturacion" name="rucFacturacion" class="form-control x2 border-bottom" >
			</div>
			<div class="form-group col-md-8">
				<label for="razonSocialFacturacion" class="sr-only">Razon Social *</label>
				<input style="border: none;" type="text" placeholder="Razon Social *" id="razonSocialFacturacion" name="razonSocialFacturacion" class="form-control border-bottom text-capitalize"  >
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-8">
				<label for="direccionFiscalFacturacion" class="sr-only">Direccion Fiscal *</label>
				<input style="border: none;" type="text" placeholder="Direccion Fiscal *" id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="form-control border-bottom text-capitalize" ">

			</div>
			<div class="form-group col-md-4">
				<label for="telefonoFacturacion" class="sr-only">Telefono </label>
				<input style="border: none;" type="text" placeholder="Telefono" id="telefonoFacturacion" name="telefonoFacturacion" class="form-control border-bottom text-capitalize" ">

			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label for="direccionEntrega" class="sr-only">Direccion de engrega </label>
					<input style="border: none;" type="text" placeholder="Direccion de engrega" id="direccionEntrega" name="direccionEntrega" class="form-control border-bottom text-capitalize" ">
				</div>
			</div>
		</div>
	</div> 
</div>  
</div>  <br>

<div class="row">
<?php if (!$objCotizarA=Cotizar::obtenerEstadoPagoDni($_SESSION['dni'])): ?>


	<div class="col-lg-6">
		<div class="fileinput fileinput-new input-group" data-provides="fileinput">
			<div class="form-control" data-trigger="fileinput">
				<i class="glyphicon glyphicon-file fileinput-exists"></i>
				<span class="fileinput-filename"></span>
			</div>
			<span class="input-group-addon btn btn-default btn-file">
				<span class="fileinput-new">Seleccionar Voucher de pago * </span>
				<span class="fileinput-exists">Cambiar</span>
				<input type="file" name="voucher" required/>
			</span>
			<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
		</div> 
	</div>

<?php endif ?>

<div class="col-lg-6">
	<button class="btn btn-success col-lg-12" type="submit" name="registrarNatural">Continuar con los datos del vehículo <i class="fa fa-arrow-circle-o-down"></i></button>
</div>
</form>

<?php endif; ?>


<!-- ===================si es empresa inicia desde aqui ==================  -->

<?php if (!empty($_SESSION['ruc'])): ?>
<label for="lannisters" id="lannisters-tab" class=" text-center"><h4>Empresa</h4></label>
<form action="" method="post" id="empresa" enctype="multipart/form-data">

<input type="radio" title="Lanisters" name="tipoCliente" id="lannisters" role="tab" aria-controls="lannisters-panel" value="Empresa" checked />

<div class="row">
	<div class="form-group col-lg-3">
		<label for="ruc" class="sr-only">Ruc* </label>
		<input style="border: none;" type="text" placeholder="Ruc*" id="ruc" name="ruc" class="form-control  border-bottom" required  data-mask="99999999999" value="<?php echo $cotizarObj->ruc ?>">
	</div>
	<div class="form-group col-lg-6">
		<label for="razonSocial" class="sr-only">Razon Social* </label>
		<input style="border: none;"  type="text" placeholder="Razon Social *" id="razonSocial" name="razonSocial" class="form-control border-bottom text-capitalize"  required value="<?php echo $cotizarObj->razonSocial ?>" >
	</div>
	<div class="form-group col-lg-3">
		<label for="telefono" class="sr-only">Celular* </label>
		<input style="border: none;" type="text" placeholder="Celular" id="telefono" name="telefono" class="form-control border-bottom text-capitalize" required data-mask="999 999 999" value="<?php echo $cotizarObj->contacto ?>">
	</div>			                     
</div>

<div class="row">
	<div class="form-group col-lg-2">
<label for="departamento2" class="sr-only">Departamento</label>
<select  style="border: none;" class="departamento2 border-bottom form-control text-capitalize" required name="departamentoL" id="departamento2">
<option class="text-capitalize" value="" disabled selected >Seleccione Departamento</option>
<?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
<?php if ($objDepartamento): ?>
<?php foreach ($objDepartamento as $var): ?>
	<option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
<?php endforeach ?>
<?php else: ?>
<?php endif ?>
</select>
</div>
<div class="form-group col-lg-2" >
<label for="corredor" class="sr-only">Provincia</label>
<select  style="border: none;" class="provincia2 border-bottom form-control text-capitalize" required name="provinciaL"  >
</select>
</div>
<div class="form-group col-lg-2" >
<label for="distrito" class="sr-only">Distrito</label>
<select  style="border: none;" class="distritos2 border-bottom form-control text-capitalize" required name="distritoL"  >
</select>
</div>
	<div class="form-group col-lg-6">
		<label for="direccionL" class="sr-only">Direccion legal*</label>
		<input style="border: none;" type="text" placeholder="Direccion Legal*" id="direccion legal" name="direccionL" class="form-control border-bottom text-capitalize" required >
	</div>		                    
</div> 


<div class="row">
	<div class="form-group col-md-4">
		<label for="telefono1" class="sr-only">Telefono</label>
		<input style="border: none;" type="text" placeholder="Telefono" id="telefono1" name="telefono1" class="form-control border-bottom" value="<?php $cotizarObj->contacto1 ?>" >
	</div>
	<div class="form-group col-md-4">
		<label for="correo" class="sr-only">Correo *</label>
		<input style="border: none;" type="email" placeholder="Correo*" id="correo" name="correo" class="form-control x2 text-capitalize border-bottom" required  value="<?php echo $cotizarObj->correo ?>">
	</div>
	<div class="form-group col-md-4">
		<label for="sectorDesempenia" class="sr-only">Sector que desempeña</label>
		<input style="border: none;"	 type="text" placeholder="Sector que desempeña" id="sectorDesempenia" name="sectorDesempenia" class="form-control border-bottom"  >
	</div>
</div>
<div class="hr-line-dashed"></div>
<h3>Representante legal</h3>

<div class="row">

	<div class="form-group col-md-2">
		<label for="dniI" class="sr-only">DNI*</label>
		<input style="border: none;" type="text" placeholder="DNI*" id="numero" name="dni" class="form-control border-bottom " required  data-mask="99999999" id="dniI">
	</div>
	<div class="form-group col-md-4">
		<label for="nombre" class="sr-only">Nombres*</label>
		<input style="border: none;" type="text" placeholder="Nombres*" id="nombre" name="nombre" class="form-control border-bottom text-capitalize"  required id="name">
	</div>
	<div class="form-group col-md-6">
		<label for="apellidoPaterno" class="sr-only">Apellidos*</label>
		<input style="border: none;" type="text" placeholder="Apellidos*" id="apellidoPaterno" name="apellidoS" class="form-control x2 text-capitalize border-bottom text-capitalize" required >
	</div>
<!-- 	
	<div class="form-group col-md-3">
		<label for="apellidoMaterno" class="sr-only">Apellido Materno*</label>
		<input style="border: none;" type="text" placeholder="Apellido Materno*" id="apellidoMaterno" name="apellidoMaterno" class="form-control border-bottom text-capitalize" required >
	</div>
 -->
</div> 

<div class="row">
	<div class="form-group col-lg-2">
<label for="departamento1" class="sr-only">Departamento</label>
<select  style="border: none;" class="departamento3 border-bottom form-control text-capitalize" required name="departamento" id="departamento1">
<option class="text-capitalize" value="" disabled selected >Seleccione Departamento</option>
<?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
<?php if ($objDepartamento): ?>
<?php foreach ($objDepartamento as $var): ?>
	<option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
<?php endforeach ?>
<?php else: ?>
<?php endif ?>
</select>
</div>
<div class="form-group col-lg-2" >
<label for="corredor" class="sr-only">Provincia</label>
<select  style="border: none;" class="provincia3 border-bottom form-control text-capitalize" required name="provincia"  >
</select>
</div>
<div class="form-group col-lg-2" >
<label for="distrito" class="sr-only">Distrito</label>
<select  style="border: none;" class="distritos3 border-bottom form-control text-capitalize" required name="distrito"  >
</select>
</div>
	<div class="form-group col-lg-6">
		<label for="direccion" class="sr-only">Direccion *</label>
		<input style="border: none;" type="text" placeholder="Direccion*" id="direccion" name="direccion" class="form-control border-bottom text-capitalize" required autocomplete="off">
	</div>		                    
</div> 
<div class="row">
	<div class="form-group col-md-3">
		<label for="telefono2" class="sr-only">Celular* </label>
		<input style="border: none;" type="text" placeholder="Celular*" id="telefono2" name="telefono2" class="form-control border-bottom" required data-mask="999 999 999">
	</div>
	<div class="form-group col-md-3">
		<label for="telefono3" class="sr-only">Telefono</label>
		<input style="border: none;" type="text" placeholder="Telefono" id="telefono3" name="telefono3" class="form-control border-bottom" >
	</div>
	<div class="form-group col-md-3">
		<label for="correo1" class="sr-only">Correo*</label>
		<input style="border: none;" type="email" placeholder="Correo*" id="correo1" name="correo1" class="form-control x2 border-bottom" required >
	</div>
	<div class="form-group col-md-3">
		<label for="correo2" class="sr-only">Correo Alternativo</label>
		<input style="border: none;" type="text" placeholder="Correo Alternativo" id="correo2" name="correo2" class="form-control border-bottom"  >
	</div>
</div> 
<div class="row">
	<div class="col-md-4">
		<select style="border: none;" id="selectNa" name="tipoDocumentoFacturacion" class="cd-selec form-control border-bottom" required >
			<option value="" disabled selected>Seleccione datos de facturación *</option>
			<option value="Boleta">Boleta</option>
			<option value="Factura">Factura</option>
		</select>   
	</div>
	<div class="col-md-8">
		<div class="content-pane e1">
			<h3>Seleccione pago</h3>
		</div>
		<div class="content-pane eBoleta">
			<h3>Continuar</h3>
		</div>
		<div class="content-pane eFactura">
			<div class="row"><br>
				<div class="form-group col-md-4">
					<label for="rucFacturacion" class="sr-only">Ruc *</label>
					<input style="border: none;" type="number" placeholder="Ruc *" id="rucFacturacion" name="rucFacturacion" class="form-control x2 border-bottom" >
				</div>
				<div class="form-group col-md-8">
					<label for="razonSocialFacturacion" class="sr-only">Razon Social *</label>
					<input style="border: none;" type="text" placeholder="Razon Social *" id="razonSocialFacturacion" name="razonSocialFacturacion" class="form-control border-bottom text-capitalize"  >
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-8">
					<label for="direccionFiscalFacturacion" class="sr-only">Direccion Fiscal *</label>
					<input style="border: none;" type="text" placeholder="Direccion Fiscal *" id="direccionFiscalFacturacion" name="direccionFiscalFacturacion" class="form-control border-bottom text-capitalize" ">

				</div>
				<div class="form-group col-md-4">
					<label for="telefonoFacturacion" class="sr-only">Telefono </label>
					<input style="border: none;" type="text" placeholder="Telefono" id="telefonoFacturacion" name="telefonoFacturacion" class="form-control border-bottom text-capitalize" ">

				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label for="direccionEntrega" class="sr-only">Direccion de engrega </label>
					<input style="border: none;" type="text" placeholder="Direccion de engrega" id="direccionEntrega" name="direccionEntrega" class="form-control border-bottom text-capitalize" ">
				</div>
			</div>
		</div> 
	</div>  
</div>  <br>
<div class="row">
	<?php if (!$objCotizarA=Cotizar::obtenerEstadoPagoRuc($_SESSION['ruc'])): ?>


		<div class="col-lg-6">
			<div class="fileinput fileinput-new input-group" data-provides="fileinput">
				<div class="form-control" data-trigger="fileinput">
					<i class="glyphicon glyphicon-file fileinput-exists"></i>
					<span class="fileinput-filename"></span>
				</div>
				<span class="input-group-addon btn btn-default btn-file">
					<span class="fileinput-new">Seleccionar Voucher de pago * </span>
					<span class="fileinput-exists">Cambiar</span>
					<input type="file" name="voucher" required/>
				</span>
				<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Borrar</a>
			</div> 
		</div>

	<?php endif ?>
	<div class="col-lg-6">
		<button class="btn btn-success col-lg-12" type="submit" name="registrarEmpresa">Continuar con los datos del vehículo <i class="fa fa-arrow-circle-o-down"></i></button>

	</div>
</div>
</form>
<?php endif ?>
</div>


</div>

</div>

<!-- fin de completar datos de cliente  -->

<hr>	
<h2 class="font-bold text-center">Datos del vehículo <i class="fa fa-arrow-circle-o-down"></i></h2>
<hr>		

</div>
</div>





</div>
</div>
</div>
</div>
<hr/>
<div class="row">
<div class="col-md-8 col-md-offset-2">
Copyright Wilman Pabel
</div>
<div class="col-md-8 col-md-offset-2 text-right">
<small>&copy; <?php echo date("Y") ?></small>
</div>
</div>
</div>
<script src="assets/js/jquery-2.1.1.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="assets/js/registrarme.js"></script>
<?php include_once('includes/script.php') ?> 
<script>
$('#selectNa').on("change",function () {
  
var tipoDePagoSeleccionado=document.getElementById('selectNa');
value = tipoDePagoSeleccionado.options[tipoDePagoSeleccionado.selectedIndex].value;
if (value=="Boleta") {

  document.getElementById('rucFacturacion').removeAttribute("required");
  document.getElementById('razonSocialFacturacion').removeAttribute("required");
  document.getElementById('direccionFiscalFacturacion').removeAttribute("required");
  document.getElementById('direccionEntrega').removeAttribute("required");
}
if(value=="Factura"){

  var rucFacturacion=document.getElementById('rucFacturacion');
  var razonSocialFacturacion=document.getElementById('razonSocialFacturacion');
  var direccionFiscalFacturacion=document.getElementById('direccionFiscalFacturacion');
  var direccionEntrega=document.getElementById('direccionEntrega');
  var att = document.createAttribute("required");
  var att1 = document.createAttribute("required");
  var att2 = document.createAttribute("required");
  var att3 = document.createAttribute("required");
  att.value = "true";  
  rucFacturacion.setAttributeNode(att);
  razonSocialFacturacion.setAttributeNode(att1);
  direccionFiscalFacturacion.setAttributeNode(att2);
  direccionEntrega.setAttributeNode(att3);
}

  });</script>
<script>
$(".departamento").change(function(){provincias();});
$(".departamento2").change(function(){provincias2();});
$(".departamento3").change(function(){provincias3();});
$(".provincia1").change(function(){distritos();});
$(".provincia2").change(function(){distritos2();});
$(".provincia3").change(function(){distritos3();});
function provincias(){
var iddepartamento = $(".departamento").val();
$.ajax({				
type : 'POST',
url  : 'com/ajax/provincia',
data :  'iddepartamento='+ iddepartamento,
success : function(data)
{
$(".provincia1").html(data);
// $("#departamento").attr("disabled",false);

}
});
return false;
}
function provincias2(){
var iddepartamento = $(".departamento2").val();
$.ajax({				
type : 'POST',
url  : 'com/ajax/provincia',
data :  'iddepartamento='+ iddepartamento,
success : function(data)
{
$(".provincia2").html(data);
// $("#departamento").attr("disabled",false);

}
});
return false;
}
function provincias3(){
var iddepartamento = $(".departamento3").val();
$.ajax({				
type : 'POST',
url  : 'com/ajax/provincia',
data :  'iddepartamento='+ iddepartamento,
success : function(data)
{
$(".provincia3").html(data);
// $("#departamento").attr("disabled",false);

}
});
return false;
}
function distritos(){
var idprovincia = $(".provincia1").val();
$.ajax({				
type : 'POST',
url  : 'com/ajax/distrito',
data :  'idprovincia='+ idprovincia,
success : function(data)
{
$(".distritos1").html(data)
}
});
return false;
}
function distritos2(){
var idprovincia = $(".provincia2").val();
$.ajax({				
type : 'POST',
url  : 'com/ajax/distrito',
data :  'idprovincia='+ idprovincia,
success : function(data)
{
$(".distritos2").html(data)
}
});
return false;
}
function distritos3(){
var idprovincia = $(".provincia3").val();
$.ajax({				
type : 'POST',
url  : 'com/ajax/distrito',
data :  'idprovincia='+ idprovincia,
success : function(data)
{
$(".distritos3").html(data)
}
});
return false;
}

</script>
<script>

$(document).ready(function(){
// $('.footable').footable();
//    $('.footable2').footable();





$("#personaNatural").validate({
rules: {
nombre: {
required:true
},
dni:{
required:true
},
apellidos:{
required:true
},
fechaNacimiento:{
required:true
},
cotizar:{
required:true
},
rucFacturacion:{

minlength:11,	
maxlength:11,	
},
razonSocialFacturacion:{

minlength:3,	

},
direccionFiscalFacturacion:{

minlength:3,	

},
telefono:{
required:true,
minlength:9	
}

},
messages: {

nombre: {
required: "Es necesario ingresar nombres",

},
telefono: {
required: "Es necesario ingresar un numero de contacto",
minlength: jQuery.validator.format("Se nececita {0} caracteres!")
},apellidos: {
required: "Es necesario ingresar los apellidos",

},departamento: {
required: "Seleccione departamento",

},provincia: {
required: "Seleccione provincia",

},distrito: {
required: "Seleccione distrito",

},rucFacturacion: {
minlength: jQuery.validator.format("Se nececita al menos {0} caracteres!")      			

},direccionFiscalFacturacion: {
minlength: jQuery.validator.format("Se nececita  al menos {0} caracteres!")         			

},razonSocialFacturacion: {
minlength: jQuery.validator.format("Se nececita al menos {0} caracteres!"),
maxlength: jQuery.validator.format("Se nececita {0} caracteres!")

},direccion: {
required: "Por favor ingrese dirección",

},referencia: {
required: "Por favor ingrese referencia",

},correo: {
required: "es necesario un correo ",
email:"Ingrese un correo valido",

},cotizar: {
required: "es necesario enviar el voucher de pago o deposito ",         			

},fechaNacimiento: {
required: "Ingrese la fecha de nacmient Dia Mes y año ",
date:"Ingrese un correo valido",

},
dni: {
required: "Numero de dni necesario"
}

}
});$("#empresa").validate({
rules: {
nombre: {
required:true
},
ruc:{
required:true
},
apellidoPaterno:{
required:true
},
apellidoMaterno:{
required:true
},
rucFacturacion:{
required:false,
minlength:11,	
maxlength:11,	
},
razonSocialFacturacion:{
required:false,
minlength:3,	

},
direccionFiscalFacturacion:{
required:false,
minlength:3,	

},
telefono:{
required:true,
minlength:9	
}

},
messages: {

nombre: {
required: "Es necesario ingresar la Razon Social",

},
telefono: {
required: "Es necesario ingresar un numero de contacto",
minlength: jQuery.validator.format("Se nececita {0} caracteres!")
},apellidoPaterno: {
required: "Es necesario ingresar el apellido Paterno",

},apellidoMaterno: {
required: "Es necesario ingresar el apellido Materno",

},departamento: {
required: "Seleccione departamento",

},provincia: {
required: "Seleccione provincia",

},distrito: {
required: "Seleccione distrito",

},direccion: {
required: "Ingrese dirección",
},rucFacturacion: {
minlength: jQuery.validator.format("Se nececita al menos {0} caracteres!")        			

},direccionFiscalFacturacion: {
minlength: jQuery.validator.format("Se nececita  al menos {0} caracteres!")         			

},razonSocialFacturacion: {
minlength: jQuery.validator.format("Se nececita al menos {0} caracteres!"),
maxlength: jQuery.validator.format("Se nececita {0} caracteres!")

},correo: {
required: "Es necesario un correo ",
email:"Ingrese un correo valido",

},
ruc: {
required: "Numero de RUC necesario"
}

}
});



});

</script>
</body>

</html>

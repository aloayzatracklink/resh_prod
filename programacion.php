<?php $nav="programacion" ;?>
<?php include("includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("ingresar");
} ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->programacion=="si"): ?>  
<link href="assets/css/bootstrap.min.css" rel="stylesheet">

<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="assets/css/plugins/iCheck/custom.css" rel="stylesheet">

<link href="assets/calendario/fullcalendar.min.css" rel="stylesheet">
<link href="assets/calendario/fullcalendar.print.min.css" rel='stylesheet' media='print'>

<link href="assets/css/animate.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/calendario/dist/sweetalert.css">
<link href='assets/calendario/scheduler.min.css' rel='stylesheet' />
<!-- <style>.fc-content td:hover{
    background: #f7f7f7; 
    opacity: 0.4;
}</style> -->
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
             <?php include("includes/menuIzquierdo.php") ?>
         </div>
     </nav>

     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("includes/navegacionArriba.php") ?>

            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                    <div class="row animated fadeInDown">
                        <div class="col-lg-1">
                            <div class="ibox float-e-margins">

                                <div class="ibox-content">
                                    <div class="fc-event-inner"></div>  
                                    <div id='external-events'>
                                        <h4></h4>       
                                        <p>
                                            <img src="assets/calendario/assets/img/trashcan.png" id="trash" alt="">
                                           
                                        </p>
                                        <p><small>Instalación taller <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background: #1ab394;"></span></small></p>
                                        <p><small>Instalación Concecionario <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background: #23c6c8;"></span></small></p>
                                        <hr>
                                        <p><small>Instalación Domicilio <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background: #2ea1ea;"></span></small></p><hr>
                                        <p><small>Cita Taller <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background: #c2c4d3;"></span></small></p>
                                        <hr>
                                        <p><small>Ok monitoreo <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background:#84c600;"></span></small></p>
                                        <hr>
                                        <p><small>Pendiente monitoreo <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background:#f48b97;"></span></small></p>
                                        <hr>
                                        <p><small>Chequeo domicilio <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background:#0239ff;"></span></small></p>
                                        <hr>
                                        <p><small>Chequeo Concecionario <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background:#8ef5ff;"></span></small></p>
                                        <hr>
                                        <p><small>Chequeo Taller <span style="float: left; width: 20px; height: 20px;  margin: 5px;  border: 1px solid rgba(0, 0, 0, .2);background:#48874a;"></span></small></p>
                                        <hr>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-lg-11">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                    <div class="row"> 
                        <div class="col-md-10">
                    <h3>LISTA DE PROGRAMACION DE EVENTOS </h3>
                        </div>
                        <div class="col-md-2">
                    <a class="btn btn-primary"  href='programacionDisponibilidad' > Ver por diponibilidad </a>
                        </div>
                    </div>
                                     
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <i class="fa fa-wrench"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a href="#">Config option 1</a>
                                            </li>
                                            <li><a href="#">Config option 2</a>
                                            </li>
                                        </ul>
                                        <a class="close-link">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div id="calendar"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include("includes/footer.php") ?>
            </div>
        </div>

    </div>
    <!-- Mainly scripts -->
    <script src="assets/calendario/moment.min.js"></script>
    <script src="assets/js/jquery-2.1.1.js"></script>

    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="assets/js/inspinia.js"></script>
    <script src="assets/js/plugins/pace/pace.min.js"></script>
    <!-- jQuery UI custom -->
    <script src="assets/js/jquery-ui.custom.min.js"></script>

    <!-- iCheck -->
    <script src="assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Full Calendar -->
    <script src="assets/calendario/fullcalendar.min.js"></script>
    <script src='assets/calendario/scheduler.min.js'></script>
    <script src="assets/calendario/dist/sweetalert.min.js"></script>
     <?php include_once('includes/script.php') ?>
    <?php 
    $usuario=Usuario::obtenerUsuario($session->idusuario);
    ?>
    <!-- <script src='calendario/lang-all.js'></script> -->
    <script>
        $(document).ready(function() {

        var zone = "09:00";  //Change this to your timezone

        $.ajax({
            url: 'assets/calendario/process',
            type: 'POST', // Send post data
            data: 'type=fetch',
            async: false,
            success: function(s){
                json_events = s;
            }
        });
        var currentMousePos = {
            x: -1,
            y: -1
        };
        jQuery(document).on("mousemove", function (event) {
            currentMousePos.x = event.pageX;
            currentMousePos.y = event.pageY;
        });

        /* initialize the external events
        -----------------------------------------------------------------*/

        $('#external-events .fc-event').each(function() {

            // store data so the calendar k nows to render an event upon drop
            $(this).data('event', {
                title: $.trim($(this).text()), // use the element's text as the event title
                stick: true // maintain when user navigates (see docs on the renderEvent method)
            });

            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 1111999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });

        });


        /* initialize the calendar
        -----------------------------------------------------------------*/

        $('#calendar').fullCalendar({
            events: JSON.parse(json_events),
            //events: [{"id":"14","title":"New Event","start":"2017-02-11T12:00:00+04:00","allDay":false}],
            utc: true,
            aspectRatio: 5,
            schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
            height:700,
            firstDay:1,
            contentHeight:700,
            slotEventOverlap:false,
            slotLabelFormat:'H:mm',            
            // slotLabelFormat:'h(:mm)a',            
            minTime:'07:00:00',            
            maxTime:'20:00:00',            
            scrollTime:'09:00:00',  
            buttonText:{today:'Hoy',month:'Vista mensual', week: 'Vista Semanal',timelineDay:'Por tecnico'},
            nowIndicator: true,
            eventBackgroundColor: '#fff',
            monthNames:['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
            'Agosto', 'Setiembre', 'Octubre', 'Nobiembre', 'Diciembre'],
            monthNamesShort:['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun','Jul', 'Ago', 'Set', 'Oct', 'Nov', 'Dec'],
            dayNames:['Domingo', 'Lunes', 'Martes', 'Miercoles','Jueves', 'Viernes', 'Sabado'],
            dayNamesShort:['Dom', 'Lun', 'Mar', 'Mier', 'Jue', 'Vie', 'Sab'],
//       eventMouseover: function(calEvent, jsEvent) {
//     var tooltip = '<div class="tooltipevent" style="width:100px;background:#fff;position:absolute;z-index:10001;">' + calEvent.title + '</div>';
//     $("body").append(tooltip);
//     $(this).mouseover(function(e) {
//         $(this).css('z-index', 10000);
//         $('.tooltipevent').fadeIn('500');
//         $('.tooltipevent').fadeTo('10', 1.9);
//     }).mousemove(function(e) {
//         $('.tooltipevent').css('top', e.pageY + 10);
//         $('.tooltipevent').css('left', e.pageX + 20);
//     });
// },
// eventMouseout: function(calEvent, jsEvent) {
//      $(this).css('z-index', 8);
//      $('.tooltipevent').remove();
// },
header: {
    left: 'prev,next today',
    center: 'title',
    right: 'month,agendaWeek,timelineDay'
},
<?php if($usuario->idrol==3 ): ?>
    editable: true,
    droppable: true, 
<?php else: ?>
    editable: false,
    droppable: false, 
<?php endif ?>
slotDuration: '00:30:00',
defaultView: 'timelineDay',
businessHours: {    
    dow: [ 1, 2, 3, 4, 5,6 ], 
    start: '9:00', 
                end: '18:00', // an end time (6pm in this example)
            }, 
            resourceLabelText: 'Tecnicos',
            resourceAreaWidth: '20%',
            resources: [
            {id:'0',title:'Sin asignar'},
            <?php $objT=Usuario::obtenerTecnicos2() ?>

    <?php if($usuario->idrol==4): ?>
            <?php foreach ($objT as $var): ?>
            { id: '<?php echo $var->idusuario ?>', title: '<?php echo $var->ordenIdTaller ?>   - <?php echo $var->sede ?>' },                 
            <?php endforeach ?>
             ], 
    <?php else: ?>
            <?php foreach ($objT as $var): ?>
             { id: '<?php echo $var->idusuario ?>', title: '<?php echo $var->ordenIdTaller ?> - <?php echo $var->usuarioNombre ?> <?php echo $var->usuarioApellidoPaterno ?> - <?php echo $var->sede ?>' },                 
            <?php endforeach ?>
            ], 
    <?php endif ?>

eventReceive: function(event){
    var title = event.title;
    var start = event.start.format("YYYY-MM-DD[T]HH:mm:SS");
    $.ajax({
        url: 'assets/calendario/process',
        data: 'type=new&title='+title+'&startdate='+start+'&zone='+zone,
        type: 'POST',
        dataType: 'json',
        success: function(response){
            event.id = response.eventid;
            $('#calendar').fullCalendar('updateEvent',event);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
    $('#calendar').fullCalendar('updateEvent',event);
    console.log(event);
},
eventDrop: function(event, delta, revertFunc) {
    var title = event.title;
    var start = event.start.format();
    var end = (event.end == null) ? start : event.end.format();
    $.ajax({
        url: 'assets/calendario/process',
        data: 'type=resetdate&title='+title+'&start='+start+'&end='+end+'&eventid='+event.id,
        type: 'POST',
        dataType: 'json',
        success: function(response){
            if(response.status != 'success')                            
                revertFunc();
        },
        error: function(e){                     
            revertFunc();
            alert('Error processing your request: '+e.responseText);
        }
    });
},
eventClick: function(event, jsEvent, view) {
    // console.log(event.id);
    var startT = event.start.format("h(:mm) a");
      $.ajax({
        url: 'assets/calendario/process',
        data: 'type=obtenerDatosS&eventid='+event.id,
        type: 'POST',
        dataType: 'json',
        success: function(response){
            // if(response.status != 'ok'){
                swal({html:true,title:startT+" "+event.title,text:"Ejecutivo ope:"+response.Ejecutivo+" <br> Contacto:"+response.contacto+" <br> Numero:"+response.numero+"<br><br> Dirección:"+response.direccion});
            // }                           
                
        },
        error: function(e){                     
            // revertFunc();
            alert('Error processing your request: '+e.responseText);
        }
    });
    // swal(startT+" "+event.title, "Ejecutivo:");
    // if (title){}
},
eventResize: function(event, delta, revertFunc) {
    console.log(event);
    var title = event.title;
    var end = event.end.format();
    var start = event.start.format();
    $.ajax({
        url: 'assets/calendario/process',
        data: 'type=resetdate&title='+title+'&start='+start+'&end='+end+'&eventid='+event.id,
        type: 'POST',
        dataType: 'json',
        success: function(response){
            if(response.status != 'success')                            
                revertFunc();
        },
        error: function(e){                     
            revertFunc();
            alert('Error processing your request: '+e.responseText);
        }
    });
},
eventDragStop: function (event, jsEvent, ui, view) {
    if (isElemOverDiv()) {
        var con = confirm('deseas eliminarlo?');
        if(con == true) {
            $.ajax({
                url: 'assets/calendario/process',
                data: 'type=remove&eventid='+event.id,
                type: 'POST',
                dataType: 'json',
                success: function(response){
                    console.log(response);
                    if(response.status == 'success'){
                        $('#calendar').fullCalendar('removeEvents');
                        getFreshEvents();
                    }
                },
                error: function(e){ 
                    alert('Error processing your request: '+e.responseText);
                }
            });
        }   
    }
},
});

function getFreshEvents(){
    $.ajax({
        url: 'assets/calendario/process',
            type: 'POST', // Send post data
            data: 'type=fetch',
            async: false,
            success: function(s){
                freshevents = s;
            }
        });
    $('#calendar').fullCalendar('addEventSource', JSON.parse(freshevents));
}

var eventos={}
eventos.frejcar =function(){
    $.ajax({
        url: 'assets/calendario/process',
            type: 'POST', // Send post data
            data: 'type=fetch',
            async: false,
            success: function(s){
                freshevents = s;
            }
        });
    $('#calendar').fullCalendar( 'removeEvents');
    $('#calendar').fullCalendar( 'addEventSource', JSON.parse(freshevents) );
    $('#calendar').fullCalendar( 'rerenderEvents');
}



function isElemOverDiv() {
    var trashEl = jQuery('#trash');

    var ofs = trashEl.offset();

    var x1 = ofs.left;
    var x2 = ofs.left + trashEl.outerWidth(true);
    var y1 = ofs.top;
    var y2 = ofs.top + trashEl.outerHeight(true);

    if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&
        currentMousePos.y >= y1 && currentMousePos.y <= y2) {
        return true;
}
return false;
}

eventos.interval = setInterval(eventos.frejcar, 50000);
eventos.frejcar();
// getFreshEvents();
});
</script>
<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>


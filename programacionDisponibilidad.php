<?php $nav="programacion" ;?>
<?php include("includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("ingresar");
} ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->programacion=="si"): ?>  
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="assets/css/plugins/iCheck/custom.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="jquery.datetimepicker.css"/>
<link href="assets/css/animate.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
             <?php include("includes/menuIzquierdo.php") ?>
         </div>
     </nav>
     <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("includes/navegacionArriba.php") ?>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                    <div class="row animated fadeInDown">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title" style="margin: 0;padding: 10px;height: 85px"> <!--Cuadrado blanco superior que contiene a los buscadores-->
                                <div class="ibox-tools" style="">
                                    <div class="col-md-8">
                                        <table class="" style="font-size:smaller;">
                                            <thead>
                                            <tr>
                                                <!--                                                    <th></th>-->
                                                <th width="70px" colspan="2">Taller <span id="ttt" style="color: #ee5b5b;">(0)</span> </th>
                                                <th width="80px" colspan="2">Domicilio <span id="ttd" style="color: #ee5b5b;">(0)</span> </th>
                                                <th width="100px" colspan="2">Concesionario <span id="tttc" style="color: #ee5b5b;">(0)</span> </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <!--                                                        <td>Operaciones</td>-->
                                                <td class="text-center" width="25px">
                                                    <div style="border:1px solid #000000;padding:0;width: 12.5px;display: inline-block;">
                                                        <div style="width:4px;height:0;border:5px solid #1ab394;overflow:hidden"></div>
                                                    </div>
                                                </td >
                                                <td class="text-left">  <p style="display: inline-block;margin:0" id="tto">0</p></td>
                                                <td class="text-center" >
                                                    <div style="border:1px solid #000000;padding:0;width: 12.5px;display: inline-block;">
                                                        <div style="width:4px;height:0;border:5px solid #ecb5d9;overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td class="text-left">
                                                    <p style="display: inline-block;margin:0" id="tdo">0</p>
                                                </td>
                                                <td class="text-center" >
                                                    <div style="border:1px solid #000000;padding:0;width: 12.5px;display: inline-block;">
                                                        <div style="width:4px;height:0;border:5px solid #7badd2;overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td  class="text-left">
                                                    <p style="display: inline-block;margin: 0" id="tco">0</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <!--                                                        <td>Central</td>-->
                                                <td class="text-center">
                                                    <div style="border:1px solid #000000;padding:0;width: 12px;display: inline-block;">
                                                        <div style="width:4px;height:0;border-left: 10px solid #fff; border-top: 10px solid #1ab394;overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td class="text-left">
                                                    <p style="display: inline-block;margin: 0" id="ttc">0</p>
                                                </td>
                                                <td class="text-center">
                                                    <div style="border:1px solid #000000;padding:0;width: 12.5px;display: inline-block;">
                                                        <div style="width:4px;height:0;border-left: 10px solid #fff; border-top: 10px solid #ecb5d9;overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td  class="text-left">
                                                    <p style="display: inline-block;margin: 0" id="tdc">0</p>
                                                </td>
                                                <td class="text-center">
                                                    <div style="border:1px solid #000000;padding:0;width: 12.5px;display: inline-block;">
                                                        <div style="width:4px;height:0;border-left: 10px solid #fff; border-top: 10px solid #7badd2;overflow:hidden"></div>
                                                    </div>
                                                </td>
                                                <td  class="text-left">
                                                    <p style="display: inline-block;margin: 0" id="tcc">0</p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>


<!------EDWARD----BUSCADOR POR CHASIS-------------------------------------------------------------------------------------------------->
                                    <div>
                                    <input type="text" id="chasisInput" placeholder="INGRESAR CHASIS" style="position: absolute; 
                                        top: 10px; left: 400px; width: 300px; padding: 10px; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);">
                                    <button id="buscarBtn" style="position: absolute; 
                                        top: 10px; left: 720px; width: 120px; padding: 10px; border-radius: 5px; background-color: #008B8B; color: white;"><strong>BUSCAR</strong></button>
                                    </div>
                                    <link rel="stylesheet" href="modalChasisBusq.css">
                                    <div id="modal2" class="modal">
                                    <div class="modal-content">
                                        <span class="close">&times;</span>
                                        <h3 style="font-weight: bold; font-family: 'Courier New', monospace;">INFORMACIÓN RELACIONADA AL CHASIS</h3>
                                        <div id="modal2-content"></div>
                                    </div>
                                    </div>

                                    <script>
                                    // Captura el click del botón buscar
                                    document.getElementById("buscarBtn").addEventListener("click", function() {
                                        // Obtener el número de chasis ingresado por el usuario
                                        var chasis = document.getElementById("chasisInput").value;

                                        // Realiza una petición AJAX para buscar los datos en la base de datos
                                        var xhr = new XMLHttpRequest();
                                        xhr.onreadystatechange = function() {
                                        if (xhr.readyState === 4 && xhr.status === 200) {
                                            // Cuando la respuesta esté lista, muestra los resultados en el modal 2
                                            document.getElementById("modal2-content").innerHTML = xhr.responseText;
                                            document.getElementById("modal2").style.display = "block";
                                        }
                                        };
                                        xhr.open("GET", "search.php?chasis=" + chasis, true);
                                        xhr.send();
                                    });

                                    // Botón de cerrar (X)
                                    document.querySelector("#modal2 .close").addEventListener("click", function() {
                                        document.getElementById("modal2").style.display = "none";
                                    });
                                    </script>


<!---------EDWARD--------------------------------------------------------------------------------------------------->

                                    
                                    <div class="col-md-4">
                                        <div class="form-group col-md-8">
                                            <div class="input-group date">
                                                <span class="input-group-addon clickD"><i class="fa fa-calendar clickD"></i></span><input type="text" class="form-control fechaa"  id="fechaCh" name="fecha" autocomplete="off" required >
                                            </div>
                                        </div>
                                        <div class="col-md-3"> </div>
                                        <div class="col-md-3 spinn" style="display: none">
                                            <div class=" sk-spinner sk-spinner-fading-circle">
                                                <div class="sk-circle1 sk-circle"></div>
                                                <div class="sk-circle2 sk-circle"></div>
                                                <div class="sk-circle3 sk-circle"></div>
                                                <div class="sk-circle4 sk-circle"></div>
                                                <div class="sk-circle5 sk-circle"></div>
                                                <div class="sk-circle6 sk-circle"></div>
                                                <div class="sk-circle7 sk-circle"></div>
                                                <div class="sk-circle8 sk-circle"></div>
                                                <div class="sk-circle9 sk-circle"></div>
                                                <div class="sk-circle10 sk-circle"></div>
                                                <div class="sk-circle11 sk-circle"></div>
                                                <div class="sk-circle12 sk-circle"></div>
                                            </div>
                                        </div>
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>

                                </div>
                                </div>
                                <div class="ibox-content" style="padding: 0;margin-top:15px">
                                <div class="row">
                                </div>
                                    <!-- <div class="ibox" id="ibox1">-->
                                    <div class="ibox-content" style="padding: 0">
                                        <div class="sk-spinner sk-spinner-double-bounce">
                                            <div class="sk-double-bounce1"></div>
                                            <div class="sk-double-bounce2"></div>
                                        </div>
                                        <div class="table-responsive"  id="programacionHtml">
                                        </div>
                                    </div>
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include("includes/footer.php") ?>
            </div>
        </div>
    </div>


        <!--MODAL-->
        <div class="modal inmodal" id="programacionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated bounceIn">
                    <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    </div>
                    <form  role="form" method="post" >
                        <div class="modal-body" style="background: #fff;padding-top: 0">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <script src="assets/js/jquery-2.1.1.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="assets/js/inspinia.js"></script>
    <script src="assets/js/plugins/pace/pace.min.js"></script>
    <!-- jQuery UI custom -->
    <!-- iCheck -->
    <script src="assets/js/plugins/iCheck/icheck.min.js"></script>
    <script src="assets/build/jquery.datetimepicker.full.js"></script>
    <!-- Full Calendar -->
        <script src="assets/js/plugins/ladda/spin.min.js"></script>
        <script src="assets/js/plugins/ladda/ladda.min.js"></script>
        <script src="assets/js/plugins/ladda/ladda.jquery.min.js"></script>
        <script src="assets/js/plugins/toastr/toastr.min.js"></script>


     <?php include_once('includes/script.php') ?>

        <style>
            .ibox-content > .sk-spinner {
                display: none;
            }

            .ibox-content.sk-loading {
                position: relative;
            }
            .ibox-content.sk-loading:after {
                content: '';
                background-color: rgba(255, 255, 255, 0.7);
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
            }
            .ibox-content.sk-loading > .sk-spinner {
                display: block;
                position: absolute;
                top: 40%;
                left: 0;
                right: 0;
                z-index: 2000;
            }
        </style>
<script>
var fechah = "<?= date('Y/m/d') ?>";
var ver = "ok";
//var ver = "ok";
//$.ajax({
//type : 'POST',
//url  : '../includes/programacionAjaxNew',
//data : {fecha:fecha,ver:ver},
//success : function(data)
//{
//    $("#programacionHtml").html(data);
//}
//});

$(".clickD").click(function(){
   $(".fechaa").focus(); 
});
$(".fechaa").focus(function(){
   $(this).blur(); 
});


function listarLeyendaTBProg(){
    var fecha = $('#fechaCh').val();
    if(fecha==''){
        fecha=fechah;
    }
    $.ajax({
        type : 'POST',
        url  : '../../com/ajax/ttleyendatbprog',
        data : {fecha:fecha},
        success : function(data)
        {
            //  data=JSON.stringify(data.trim());
            var obj = jQuery.parseJSON(data.trim());
            $("#ttt").text("("+(parseInt(obj.tto) + parseInt(obj.ttc))+")");
            $("#ttd").text("("+(parseInt(obj.tdo) + parseInt(obj.tdc))+")");
            $("#tttc").text("("+(parseInt(obj.tco) + parseInt(obj.tcc))+")");

            $("#tto").text(obj.tto);
            $("#tdo").text(obj.tdo);
            $("#tco").text(obj.tco);

            $("#ttc").text(obj.ttc);
            $("#tdc").text(obj.tdc);
            $("#tcc").text(obj.tcc);
        }
    });
}
function listardisponilidadTec(){
    var fecha = $('#fechaCh').val();
    if(fecha==''){
        fecha=fechah;
    }
    $(".spinn").show();
    $("#buscarr").off( "click" );
    $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
    $.ajax({
        type : 'POST',
        url  : '../includes/tbDisponibilidadRP',
        data : {fecha:fecha},
        success : function(data)
        {
            $("#programacionHtml").html(data);
            $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
            $(".spinn").hide();
        }
    });
}
var fechOld='';
$(".fechaa").change(function(){
    var fecha = $('#fechaCh').val();
    if(fecha!=fechOld){
        listardisponilidadTec();
        listarLeyendaTBProg();
        fechOld=fecha;
    }
});

jQuery.datetimepicker.setLocale('es');
$('#fechaCh').datetimepicker({
//  minDate:fechah,
  format:'Y/m/d',
  timepicker:false,
  scrollMonth : false,
  scrollInput: false
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
$(document).ready(function() {

var evento={}
evento.actualizar=function (){
        listardisponilidadTec();
        listarLeyendaTBProg();
};
evento.interval = setInterval(evento.actualizar, 15000);
evento.actualizar();

    });


$('#programacionModal').on('show.bs.modal', function (e) {
    var rowid = $(e.relatedTarget).data('id');
    var fechaP = $('.fechaa').val();
    if (fechaP=='') {
        var fechaP = "2019-12-06";
    }
    $.ajax({
        type : 'post',
        url : 'ope/modal/programarT',
        data :  {rowid:rowid,dia:fechaP},
        success : function(data){
            $('.modal-body').html(data);
        }
    });
});
</script>
        <?php else: ?>
            <?php $session->salir();
            echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>




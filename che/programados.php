<?php if ($_POST): ?>
	<?php 
	require_once("../includes/init.php");
		$dataProgramacion=Chequeos::obtenerProgramacionChequeoDIA($_POST['fecha']);
		$fechaL=formatofechaLimipo($_POST['fecha']);
		$horaL=formatohoraLimpio($_POST['fecha']);
		// var_dump($dataProgramacion);
if ($horaL>=1700 && $horaL<=2200) {		
	?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
</head>
<body>
<div class="table-responsive">
  <table class="table table-bordered" >
  <tbody>
    <tr>
    <th ></th>
      <th>17:00 <?= ($horaL==1700)?' <i class="fa fa-plus-circle fa-spin fa-fw text-info fa-2x"></i>':'' ?></th>
      <th>17:30 <?= ($horaL==1730)?' <i class="fa fa-plus-circle fa-spin fa-fw text-info fa-2x"></i>':'' ?></th>  
      <th>18:00 <?= ($horaL==1800)?' <i class="fa fa-plus-circle fa-spin fa-fw text-info fa-2x"></i>':'' ?></th>
      <th>18:30 <?= ($horaL==1830)?' <i class="fa fa-plus-circle fa-spin fa-fw text-info fa-2x"></i>':'' ?></th>
      <th>19:00 <?= ($horaL==1900)?' <i class="fa fa-plus-circle fa-spin fa-fw text-info fa-2x"></i>':'' ?></th>
      <th>19:30 <?= ($horaL==1930)?' <i class="fa fa-plus-circle fa-spin fa-fw text-info fa-2x"></i>':'' ?></th>
	  <th>20:00 <?= ($horaL==2000)?' <i class="fa fa-plus-circle fa-spin fa-fw text-info fa-2x"></i>':'' ?></th>
	  <th>20:30 <?= ($horaL==2030)?' <i class="fa fa-plus-circle fa-spin fa-fw text-info fa-2x"></i>':'' ?></th>
	  <th>21:00 <?= ($horaL==2100)?' <i class="fa fa-plus-circle fa-spin fa-fw text-info fa-2x"></i>':'' ?></th>
	  <th>21:30 <?= ($horaL==2130)?' <i class="fa fa-plus-circle fa-spin fa-fw text-info fa-2x"></i>':'' ?></th>
	  <th>22:00 <?= ($horaL==2200)?' <i class="fa fa-plus-circle fa-spin fa-fw text-info fa-2x"></i>':'' ?></th>
    </tr>
    <tr>
    <?php if ($dataProgramacion){ ?>
    <tr>
      <td>CEM1</td>

      <?php $h1700=assc_array_count_values($dataProgramacion,formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'1700')) ?>      
      <?php $h1730=assc_array_count_values($dataProgramacion,formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'1730')) ?>      
      <?php $h1800=assc_array_count_values($dataProgramacion,formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'1800')) ?>      
      <?php $h1830=assc_array_count_values($dataProgramacion,formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'1830')) ?>      
      <?php $h1900=assc_array_count_values($dataProgramacion,formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'1900')) ?>      
      <?php $h1930=assc_array_count_values($dataProgramacion,formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'1930')) ?>      
      <?php $h2000=assc_array_count_values($dataProgramacion,formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'2000')) ?>      
      <?php $h2030=assc_array_count_values($dataProgramacion,formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'2030')) ?>      
      <?php $h2100=assc_array_count_values($dataProgramacion,formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'2100')) ?>      
      <?php $h2130=assc_array_count_values($dataProgramacion,formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'2130')) ?>      
      <?php $h2200=assc_array_count_values($dataProgramacion,formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'2200')) ?>   
       <script>
      var cantidad='<?= $h=assc_array_count_values($dataProgramacion,formatofechaLimipo($_POST['fecha']))  ?>'
        if (cantidad>=2) {
          capacidad();
        }else{
          limpiarCapacidad();
        }
      </script>
      <td class="<?= (in_array_field(formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'1700'), 'fechaHoraChequeoLinea',$dataProgramacion))?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= (in_array_field(formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'1730'), 'fechaHoraChequeoLinea',$dataProgramacion))?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= (in_array_field(formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'1800'), 'fechaHoraChequeoLinea',$dataProgramacion))?'bg-warning':'bg-success' ?>"></td>      
      <td class="<?= (in_array_field(formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'1830'), 'fechaHoraChequeoLinea',$dataProgramacion))?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= (in_array_field(formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'1900'), 'fechaHoraChequeoLinea',$dataProgramacion))?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= (in_array_field(formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'1930'), 'fechaHoraChequeoLinea',$dataProgramacion))?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= (in_array_field(formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'2000'), 'fechaHoraChequeoLinea',$dataProgramacion))?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= (in_array_field(formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'2030'), 'fechaHoraChequeoLinea',$dataProgramacion))?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= (in_array_field(formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'2100'), 'fechaHoraChequeoLinea',$dataProgramacion))?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= (in_array_field(formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'2130'), 'fechaHoraChequeoLinea',$dataProgramacion))?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= (in_array_field(formatofechaLimipoADD(formatodiaLimpio($_POST['fecha']),'2200'), 'fechaHoraChequeoLinea',$dataProgramacion))?'bg-warning':'bg-success' ?>"></td>
    </tr>
    <tr>
      <td>CEM2</td>
      <td class="<?= ($h1700>1)?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= ($h1730>1)?'bg-warning':'bg-success' ?>"></td>      
      <td class="<?= ($h1800>1)?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= ($h1830>1)?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= ($h1900>1)?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= ($h1930>1)?'bg-warning':'bg-success' ?>"></td>      
      <td class="<?= ($h2000>1)?'bg-warning':'bg-success' ?>"></td>      	
      <td class="<?= ($h2030>1)?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= ($h2100>1)?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= ($h2130>1)?'bg-warning':'bg-success' ?>"></td>
      <td class="<?= ($h2200>1)?'bg-warning':'bg-success' ?>"></td>
    </tr>
    <?php 
		}else{
	?>
	<script>limpiarCapacidad();</script>
   <tr>
      <td>CEM1</td>
      <td  class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
    </tr>
      <td>CEM2</td>
      <td class="bg-success "></td>
      <td class="bg-success"></td>      
      <td class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
      <td class="bg-success"></td>
    </tr>
    	<?php
    	} 
    	?> 
  </tbody>
	</table>
</div>
</body>
</html>
		<?php
}else{

	?>
<script>  limpiarCapacidad();</script>
	<?php
}



	 ?>
<?php endif ?>
<?php $nav="bateriaOcto" ;?>
<?php include("../../includes/head.php") ?>

<link rel="stylesheet" type="text/css" href="../../assets/calendario/dist/sweetalert.css">
<?php if (!$session->yaEstaIngresado()){ 
  irA("../../ingresar");
} ?>
<?php 
$obMenu=Usuario::obtenerMenu($session->idusuario) ?>

<?php if ($obMenu->baterias=="si"): ?> 
    <?php 


 require_once '../../mailphp/PHPMailer/PHPMailerAutoload.php';
   $paraAgendar="";
if (isset( $_POST['agregar'])) {

        $nombre= trim(strtoupper($_POST['nombreCliente']));
        $placaVehiculo=trim(strtoupper($_POST['placaVehiculo']));
        $chasis=trim(strtoupper($_POST['chasis']));
        $tipo=strtoupper($_POST['tipo']);
        $motivo=strtoupper($_POST['motivo']);
        ////OBTENEMOS LOS DATOS DEL USUARIO
$usuario=Usuario::obtenerUsuario($session->idusuario);
$correoEnviador=$usuario->usuario;
$nombreEnviador=$usuario->usuarioNombre." ".$usuario->usuarioApellidoPaterno." ".$usuario->usuarioApellidoMaterno;
$nombreE=$usuario->usuarioNombre;
$rolUsuario=Usuario::obtenerRoldeUsuario($usuario->idrol);
$cargo=$rolUsuario->cargo;

      $fecha=date("YmdHis");
      $paraAgendar.=" Se agrego a la lista de no llamar a: <br> Placa: ".$placaVehiculo." <br> Chasis: ".$chasis."<br> Cliente: ".$nombre."<br>   Motivo :" .$motivo." <br> Chasis: ".$chasis."<br> Fecha: ".$fecha."";

////////////////////////////////////////////////CORREO POR DEFECTO /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////ALA CENTRAL //////////////////////////////////////////////////////////

$thehtml="<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
    <meta name='viewport' content='width=device-width' />
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
    <title>The alerta</title>
</head>

<body style=' margin: 0;padding: 0;font-family:Helvetica, Arial, sans-serif;    box-sizing: border-box;    font-size: 14px; background-color: #f6f6f6; '>

    <table style=' background-color: #f6f6f6; width: 100%;'>
        <tr>
            <td></td>
            <td  width='680' style=' display: block !important;    max-width: 680px !important;    margin: 0 auto !important;    /* makes it centered */    clear: both !important;'>
                <div class='content' style='  max-width: 680px;    margin: 0 auto;    display: block;    padding: 20px;'>
                    <table class='main' width='100%' cellpadding='0' cellspacing='0' style='background: #fff;    border: 1px solid #e9e9e9;    border-radius: 3px;'>
                        <tr>
                            <td style='font-size: 16px;    color: #fff;    font-weight: 500;    padding: 20px;    text-align: center;    border-radius: 3px 3px 0 0;  background: #003399;'>
                                Se agrego a la lista no llamar.
                            </td>
                        </tr>
                        <tr>
                            <td style='padding: 10px !important ;padding: 20px; vertical-align: top;'>
                                <table width='100%' cellpadding='0' cellspacing='0'>
                                    <tr>
                                        <td  style='padding: 0 0 20px;'>
                                            Estimados <strong> Central de emergencia y Monitoreo.</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  style='padding: 0 0 20px;'>
                                            SE AGREGO A LA LISTA DE NO LLAMAR A: $nombre
                                        </td>
                                    </tr>                                       
                                    <td  style='padding: 0 0 20px;'>                                      

                                    <table width='100%' cellpadding='0' cellspacing='0'>

                                        <td  style='padding: 0 0 20px;'>
                                            <table style='border-collapse: separate;
                                            background: #fff;
                                            -moz-border-radius: 5px;
                                            -webkit-border-radius: 5px;
                                            border-radius: 5px;
                                            margin: 10px auto;
                                            -moz-box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
                                            -webkit-box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
                                            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);'>
                                            <thead style=' -moz-border-radius: 5px;
                                            -webkit-border-radius: 5px;
                                            border-radius: 5px;'>
                                            <tr>
                                                <th style='font-family: helvetica;
                                                font-size: 16px;
                                                font-weight: 400;
                                                color: #fff;                                            
                                                text-align: left;
                                                padding: 20px;
                                                background-size: 100%;
                                                background-color:#003399;
                                                border-top: 1px solid #858d99;'>Chasis</th>

                                                <th style='font-family: helvetica;
                                                font-size: 16px;
                                                font-weight: 400;
                                                color: #fff;                                            
                                                text-align: left;
                                                padding: 20px;
                                                background-size: 100%;
                                                background-color:#003399;
                                                border-top: 1px solid #858d99;'> PLACA: </th>

                                                <th style='font-family: helvetica;
                                                font-size: 16px;
                                                font-weight: 400;
                                                color: #fff;                                            
                                                text-align: left;
                                                padding: 20px;
                                                background-size: 100%;
                                                background-color:#003399;
                                                border-top: 1px solid #858d99;'>NOMBRE CLIENTE:  </th>

                                                <th style='font-family: helvetica;
                                                font-size: 16px;
                                                font-weight: 400;
                                                color: #fff;                                            
                                                text-align: left;
                                                padding: 20px;
                                                background-size: 100%;
                                                background-color:#003399;
                                                border-top: 1px solid #858d99;'> MOTIVO</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style=' font-family:Helvetica;
                                                font-weight: 400;
                                                color: #303031;
                                                font-size: 15px;
                                                padding: 20px 20px 20px 20px;
                                                border-bottom: 1px solid #e0e0e0;border-right: 1px solid #d8d2d2;'>$chasis
                                                </td>
                                                <td style=' font-family:Helvetica;
                                                font-weight: 400;
                                                color: #303031;
                                                font-size: 15px;
                                                padding: 20px 20px 20px 20px;
                                                border-bottom: 1px solid #e0e0e0;border-right: 1px solid #d8d2d2;'> $placaVehiculo</td>
                                                <td style=' font-family:Helvetica;
                                                font-weight: 400;
                                                color: #303031;
                                                font-size: 15px;
                                                padding: 20px 20px 20px 20px;
                                                border-bottom: 1px solid #e0e0e0;border-right: 1px solid #d8d2d2;'> $nombre</td>
                                                <td style=' font-family:Helvetica;
                                                font-weight: 400;
                                                color: #303031;
                                                font-size: 15px;
                                                padding: 20px 20px 20px 20px;
                                                border-bottom: 1px solid #e0e0e0;border-right: 1px solid #d8d2d2;'>$motivo</td>
                                            </tbody>
                                        </table>
                                        <tr>
                                            <td  style='padding: 0 0 20px;'>
                                             <h3>Se tipifico en Onix como sigue:</h3>
                                             <p>$paraAgendar</p>                
                                         </td>
                                     </tr>
                                    
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div style=' width: 100%;    clear: both;    color: #999;    padding: 20px;'>
                        <table width='100%'>
                            
                            </table>
                        </div></div>
                    </td>
                    <td></td>
                </tr>
            </table>
        </body>
        </html>";
        $registrado="fsdaf";

    // // El correo para el cliente aqui /// <---->
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->SetLanguage( 'es', 'mailphp/PHPMailer/language/' );
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host=gethostbyname('mail.tracklink.pe');
        $mail->Port = 25;
        $mail->SMTPAuth = false;
        $mail->CharSet = 'UTF-8';
        $mail->setFrom('no-responder@tracklink.pe',$nombreE.'');  
    $mail->addReplyTo($correoEnviador, $nombreE.' de Tracklink'); // donde deven responder
    //Set who the message is to be sent to
    $mail->addAddress("centralemergencia@tracklink.pe","Central de Emergencia"); // aquien se manda el mensaje            
    //CC and BCC
    $mail->addCC("pruebas-ti@tracklink.pe","TEST");
    // $mail->addBCC("bcc@example.com");
    //Set the subject line
    $mail->Subject = 'AGREGADO A LISTA NO LLAMAR A CLIENTE : '.$nombre.' TEST'; //// ASUNTO //////
    //Read an HTML message body from an external file, convert referenced images to embedded,
    // $mail->AddEmbeddedImage('img/logo_firma.png', 'tracklink');
    // $mail->AddEmbeddedImage('img/media.png', 'twitter');
    // $mail->AddEmbeddedImage('img/youtube.png', 'youtube');
    // $mail->AddEmbeddedImage('img/fb.png', 'facebook');
    //convert HTML into a basic plain-text alternative body
    $mail->msgHTML($thehtml);
    //Replace the plain text body with one created manually
    $mail->AltBody = 'Estimado(as) Central de emergencia se añadiao a lista de no llamar'; ////////ESTE SE EL CONTENIDO//// QUE SE BERA EN PRIERA INSTANACIA
    // Attach an image file
    //   aqui se adjunta los archivos que queremos enviar :D


    $mail->addAttachment('images/phpmailer_mini.png');

   /////////////////// send the message, check for errors
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        echo "Correo Enviado";

    }
////////////////////////////////////////////////CORREO POR DEFECTO /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////ALA CENTRAL //////////////////////////////////////////////////////////

///////////////////////REGISTRAR EN LA BASE DE DATOSS HERE E ----////////////////////
   $objetoBateriaTrack=new BateriaTrack();
        $objetoBateriaTrack->chasis=$chasis;
        $objetoBateriaTrack->placaVehiculo =$placaVehiculo;
        $objetoBateriaTrack->nombre=$nombre;
        $objetoBateriaTrack->tipo=$tipo;
        $objetoBateriaTrack->motivo=$motivo;
        if ($objetoBateriaTrack->agregarNoLlamarOcto()) {
           
       }
        ///////////////////////REGISTRAR EN LA BASE DE DATOSS HERE E ----////////////////////
    }

if (isset($_POST['eliminar'])) {
    $chasis=$_POST['chasis'];
    if (BateriaTrack::eliminarDeFalsasAlertasOcto($chasis)) {
        echo "Eliminado";
    }
}
   ?>
   <link href="../../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
   <link href="../../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<style>
    
.contenedor {
    width: 90%;
    max-width: 1000px;
    margin: auto; 
}

h1 {
    margin: 20px 0;
    text-align: center;
    font-family:  helvetica, sans-serif;
    color: #ce7f08;
    font-weight: normal;
}

.contenedor .articulos {
    margin-bottom: 20px;
}

.contenedor .articulos ul {
    list-style: none;
}

.contenedor .articulos ul li {
    padding:15px 10px;
    border-bottom: 1px solid #c8c8c8;
}

.paginacion ul {
    list-style: none;
}

.paginacion ul li {
    display: inline-block;
    margin-right: 10px; 
}

.paginacion ul .active a {
    background: #ce7f08;
}

.paginacion ul .disabled {
    background: #c8c8c8;
    display: inline-block;
    font-family: helvetica, sans-serif;
    padding: 15px 25px;
    cursor:not-allowed;
    color:#fff;
    text-decoration: none;
}

.paginacion ul li a {
    display: inline-block;
    font-family: helvetica, sans-serif;
    padding: 15px 25px;
    background: #424242;
    color:#fff;
    text-decoration: none;
}

.paginacion ul li a:hover {
    text-decoration: none;
    background: #3299bb;
}
</style>
<?php 
    
try {
    $conexion = new PDO('mysql:host=192.168.2.21;dbname=trackdb', 'resh', 'resh2018');
} catch (PDOException $e) {
    echo "ERROR: " . $e->getMessage();
    die();
}

$pagina = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1 ;
$postPorPagina = 20;

$inicio = ($pagina > 1) ? ($pagina * $postPorPagina - $postPorPagina) : 0;

$articulos = $conexion->prepare("
    SELECT SQL_CALC_FOUND_ROWS * FROM noLlamarOcto 
    ORDER BY idNoLlamarOcto DESC LIMIT $inicio, $postPorPagina 
");

$articulos->execute();
$articulos = $articulos->fetchAll();

$totalArticulos = $conexion->query('SELECT FOUND_ROWS() as total');
$totalArticulos = $totalArticulos->fetch()['total'];

$numeroPaginas = ceil($totalArticulos / $postPorPagina);


 ?>
   <body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
               <?php include("../../includes/menuIzquierdo.php") ?>

           </div>
       </nav>

       <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../../includes/navegacionArriba.php") ?>

            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">

                    <div class="row ibox float-e-margins ibox-content">
                        <div class="row">
                            <div class="col-md-12"><a href="../bateriaOcto"  class="btn btn-success"><i class="fa fa-arrow-left"></i> Regresar a Gestion de baterias</a></div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="row">
                            <form action="" method="POST">
                                <div class="form-group col-md-2">
                                    <label for="Observaciones" >CHASIS*</label>
                                    <input name="chasis" style="border: none;" type="text" placeholder="chasis*" id="Observaciones" class="border-bottom form-control" required="" >
                                </div>
                                 <div class="col-md-1"><label for="estado">Placa*</label>
                                    <input style="border: none;" type="text" id="estado"  class="border-bottom form-control" placeholder="Placa*" required="" name="placaVehiculo">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="numeroTecnico">Nombre de cliente*</label>
                                    <input  style="border: none;" type="text" id="numeroTecnico" name="nombreCliente" class="border-bottom form-control" required  placeholder="Nombre de cliente*">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="motivo">tipo*</label>
                                    <input  style="border: none;" type="text" id="tipo" name="tipo" class="border-bottom form-control" required  placeholder="tipo*">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="motivo">Motivo*</label>
                                    <input  style="border: none;" type="text" id="motivo" name="motivo" class="border-bottom form-control" required  placeholder="Motivo*">
                                </div>
                                <div class="form-group col-md-1">
                                <button class="btn btn-primary" type="submit" name="agregar">Agregar</button>
                                </div>
                               </form>
                            </div><hr><hr>
                            <form action="" method="post">
                                <div class="row"> 
                                    <div class="col-md-4">
                                        <input style="border: none;" type="text" class="form-control border-bottom" id="filter"
                                        placeholder="Filtrar vehículo de lista por CHASIS" name="chasis" required="">
                                    </div>
                                    <div class="col-md-4">
                                    <button class="btn btn-primary col-md-6" name="buscar">Buscar</button>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="noLlamarOcto" class="btn btn-info">Actualizar</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Lista de no Llamar</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                            <section class="articulos">
            <ul>
          <!--   <div class="row">
                                <div class="col-sm-5 m-b-xs"><select class="input-sm form-control input-s-sm inline">
                                    <option value="0">Option 1</option>
                                    <option value="1">Option 2</option>
                                    <option value="2">Option 3</option>
                                    <option value="3">Option 4</option>
                                </select>
                                </div>
                                <div class="col-sm-4 m-b-xs">
                                    <div data-toggle="buttons" class="btn-group">
                                        <label class="btn btn-sm btn-white"> <input type="radio" id="option1" name="options"> Day </label>
                                        <label class="btn btn-sm btn-white active"> <input type="radio" id="option2" name="options"> Week </label>
                                        <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options"> Month </label>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group"><input type="text" placeholder="Search" class="input-sm form-control"> <span class="input-group-btn">
                                        <button type="button" class="btn btn-sm btn-primary"> Go!</button> </span></div>
                                </div>
                            </div> -->

                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr> 
                                            <th>#</th>
                                            <th>CHASIS</th>
                                            <th>placa</th>
                                            <th>Cliente</th>
                                            <th>Tipo</th>
                                            <th>Motivo</th>
                                            <th>fecha Registro</th>
                                            <th>Agregado por</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        if (isset($_POST['buscar'])) {
                                            $chasis=htmlspecialchars((htmlentities($_POST['chasis'])));
                                            $objBateriaa=BateriaTrack::obtenerDatosDeNoLlamarOcto($chasis);
                                            if ($objBateriaa) {
                                                
                                            
                                                ?>
                                               
                                                <tr>
                                                    <td>1</td>
                                                    <td><?php echo $objBateriaa->chasis ?></td>
                                                    <td><?php echo $objBateriaa->placa ?></td>
                                                    <td><?php echo $objBateriaa->nombre ?></td>
                                                    <td><?php echo $objBateriaa->tipo ?></td>
                                                    <td><?php echo $objBateriaa->motivo ?></td>
                                                    <td><?php echo $objBateriaa->fechaRegistro ?></td>
                                                    <td><?php $usuario=Usuario::obtenerUsuario($objBateriaa->idusuario);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno;  ?></td>
                                                    <td><form action="" method="post" class="deletethis"> <input type="hidden" name="chasis" value="<?php echo $objBateriaa->chasis ?>"><button class="aaa btn btn-warning" name="eliminar">Eliminar</button></form>
                                                    </td>
                                                </tr>
                                            
                                                <?php
                                            }else{
                                                ?>
                                                <tr><td colspan="7"> No hay datos del id ingresado</td></tr>
                                                <?php
                                            }
                                        }else{

                                        
                                     ?>
                                        <!-- <td><input type="checkbox" checked="" class="i-checks" name="input[]"></td> -->
                                        <?php $n=1 ?>
                                        <?php foreach ($articulos as $articulo): ?>
                                            <tr>
                                                <td><?php echo $n++ ?></td>
                                                <td><?php echo $articulo['chasis'] ?></td>   
                                                <td><?php echo $articulo['placa'] ?></td>
                                                <td><?php echo $articulo['nombre'] ?></td>
                                                <td><?php echo $articulo['tipo'] ?></td>
                                                <td><?php echo $articulo['motivo'] ?></td>
                                                <td><?php echo $articulo['fechaRegistro'] ?></td>
                                                <td><?php $usuario=Usuario::obtenerUsuario($articulo['idusuario']);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno;  ?></td>
                                                <td><form action="" method="post" class="deletethis"> <input type="hidden" name="chasis" value="<?php echo $articulo['chasis'] ?>"><button class="aaa btn btn-warning" name="eliminar">Eliminar</button></form>
                                                </td>

                                            </tr>


                                        <?php endforeach; ?>   

                                    </tbody>
                                </table>
                            </div>


        <section class="paginacion">
            <ul>
                <!-- Establecemos cuando el boton de "Anterior" estara desabilitado -->
                <?php if ($pagina == 1): ?>
                    <li class="disabled">&laquo;</li>
                <?php else: ?>
                    <li><a href="?pagina=<?php echo $pagina - 1 ?>">&laquo;</a></li>
                <?php endif; ?>

                <!-- Ejecutamos un ciclo para mostrar las paginas -->
                <?php 
                for ($i=1; $i <= $numeroPaginas ; $i++) { 
                    if ($pagina == $i) {
                        echo "<li class='active'><a href='?pagina=$i'>$i</a></li>";
                    } else {
                        echo "<li><a href='?pagina=$i'>$i</a></li>";
                    }
                }
                ?>

                <!-- Establecemos cuando el boton de "Siguiente" estara desabilitado -->
                <?php if ($pagina == $numeroPaginas): ?>
                    <li class="disabled">&raquo;</li>
                <?php else: ?>
                    <li><a href="?pagina=<?php echo $pagina + 1 ?>">&raquo;</a></li>
                <?php endif; ?>
            </ul>
        </section>
   <?php 
                                        }

                                         ?> 

                              
                        </div>
                    </div>
                </div>

                <?php include("../../includes/footer.php") ?>
            </div>
        </div>

    </div>
    <!-- LOS MODALES AQUI  -->


    <div class="modal inmodal" id="bateriasG" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceIn">
                <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                    <h4 class="modal-title">Gestionar Desconexion  de bateria</h4>
                    <small class="font-bold"><div class="form-group ">
                    </div></small>
                </div>
                <form  role="form" method="post" >

                    <div class="modal-body" style="background: #fff;padding-top: 0">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                        <button type="submit" name="gestionarBateria" class="btn btn-primary">Gestionado</button>
                    </div>
                </form>   
            </div>
        </div>
    </div>


    <!-- LOS MODALES AQUI  -->

    <script src="../../assets/js/jquery-2.1.1.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="../../assets/js/inspinia.js"></script>
    <script src="../../assets/js/plugins/pace/pace.min.js"></script>
    <script src="../../assets/js/plugins/footable/footable.all.min.js"></script>

    <script src="../../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <script src="../../assets/calendario/dist/sweetalert.min.js"></script>
    <?php include_once('../../includes/script.php') ?> 
    <!-- Queda hacer lo de Ajax -->

    <!-- Si o Si -->

    <!-- Queda hacer lo de Ajax -->

    <script>
$(".aaa").click(function(){
//     var c;
    return c=confirm("Realmente desea eliminarlo") ;
//    swal({
//         title: "Estas seguro?",
//         text: "Una vez borrado no se podra recuperar el registro!",
//         type: "warning",
//         showCancelButton: true,
//         confirmButtonColor: "#DD6B55",
//         confirmButtonText: "Si, Borrar!",
//         cancelButtonText: "No, cancelar plx!",
//         closeOnConfirm: false,
//         closeOnCancel: false },
//      function (isConfirm) {
//         if (isConfirm) {
//            return c=swal("Eliminado!", "El registro fue eliminado.", "success");
//         } else {
//             swal("Canleado", "El  registro sige registrado :)", "error");
//         }
    });


// });
/*$('.aaa').on('click',function(e){
    e.preventDefault();
    var form = $('.deletethis');
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function(isConfirm){
        if (isConfirm) form.submit();
    });
});*/
        <?php if (isset($registrado)): ?>
        swal({
          title: "Copiar y agendar en Onix!",
          text: "<?php echo $paraAgendar ?>",
          html: true,
          type: "success"
      });
    <?php endif ?> 

    $(document).ready(function(){
        $('.footable2').footable();
    });

</script>

<?php else: ?> 
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?> 
<?php endif ?>

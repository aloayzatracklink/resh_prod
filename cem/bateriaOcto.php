<?php $nav="bateriaOcto" ;?>
<?php include("../includes/head.php") ?>
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<?php if (!$session->yaEstaIngresado()){ 
  irA("../ingresar");
} ?>
<?php 
$obMenu=Usuario::obtenerMenu($session->idusuario) ?>

<?php if ($obMenu->baterias=="si"): ?> 

<?php 
$errores=array();
$echo=array();
    //Registramos las alertas aquí ///
    if (isset($_POST['subir'])) {
    
    if (Token::verificar($_POST['token'])) {
    $bateriaObj1=new BateriaTrack();
        if (isset($_POST['theft'])) {
                 $bateriaObj1->tipoAlerta=$_POST['theft'];
            }
        if (!empty($_POST['chasis'])) {
            $bateriaObj1->chasis=limpiar(trim(htmlspecialchars($_POST['chasis'])));
            $bateriaObj1->registrarChasis();            
            $echo[]="Chasis registrado";
        }
    

        }else{
        $errores[]=" El formulario ya fue procesado, the token :v";
        }
    }

    ?>
    <?php 
    require_once '../mailphp/PHPMailer/PHPMailerAutoload.php';
    $bateriaObj=new BateriaTrack();
    $contador=0;
    $paraAgendar="";
    $cordenadasAgenda="";
    if (isset( $_POST['gestionarBateria'])) {
    ob_start();
        $nombres=strtoupper($_POST['nombreCliente']);
        $correo=strtolower(trim($_POST['correoCliente']));
        $correo = preg_replace("/.(?R)*\[BS\]/","",$correo);
        $marca=strtoupper($_POST['marca']);
        $modelo=strtoupper( $_POST['modelo']);
        $placaVehiculo=strtoupper($_POST['placaVehiculo']);
        $chasis=trim(strtoupper($_POST['chasis']));
        $chasisO=trim($_POST['chasisO']);
        $sistema="";
        /////////////////////////////////////
        if (isset($_POST['articulo'])) {
            $sistema=strtoupper($_POST['articulo']);            
        }
        ///////////////////////////////////
        $seguimiento=strtoupper($_POST['seguimiento']);
        $coordenadas=$_POST['coordenadas'];

        if ($coordenadas<>"") {
          $cordenadasAgenda=" Coordenadas: ".$coordenadas." <br>";  
      }

        ///PARA AGENDAR HERE ////
      $hora=date('h');
      $minuto=date('i');
  // $minuto=$minuto-3;
      $amOpm=date('A');
      $dia=date("d-m-Y");
      $paraAgendar.="desconexión de bateria de cliente: ".$placaVehiculo."<br> Chasis: ".$chasis."<br>Sistema : " .$sistema." <br>".$cordenadasAgenda." Horas: ".$hora.":".$minuto." ".$amOpm." del  dia: ".$dia."<br>Acciónes: <br> ";


      if (isset($_POST['ok'])) {
         $paraAgendar.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* Todo en orden <br>";
     }

     if (isset($_POST['cv'])) {
         $paraAgendar.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* Mensaje en casilla de voz <br>";
     }
    if (isset($_POST['ta'])) {
        $paraAgendar.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* Vehiculo en tallaer <br>";
    }
     if (isset($_POST['ad'])) {
         $paraAgendar.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* Actualizar datos<br>";
     }

     if (isset($_POST['desS'])) {
         $paraAgendar.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* Contrato vencido sin orden de desinstalacion<br>";
     }
     if (isset($_POST['des'])) {
         $paraAgendar.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* Desinstalado por orden de baja<br>";
     }
     if (isset($_POST['fa'])) {
         $paraAgendar.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* Falsa alerta <br>";    
     }
     $correo= explode(",", $correo);
     $usuario=Usuario::obtenerUsuario($session->idusuario);
     $correoEnviador=$usuario->usuario;
     $nombreEnviador=$usuario->usuarioNombre." ".$usuario->usuarioApellidoPaterno." ".$usuario->usuarioApellidoMaterno;
     $nombreE=$usuario->usuarioNombre;
     $rolUsuario=Usuario::obtenerRoldeUsuario($usuario->idrol);
     $cargo=$rolUsuario->cargo;
     if (isset($_POST['cee'])) {
        if ($_POST['correoCliente']<>"") {
    include("../assets/email_templates/dbNew/formatoTrack.php");
    // El correo para el cliente aqui /// <---->
    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SetLanguage( 'es', 'mailphp/PHPMailer/language/' );
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';
    $mail->Host = gethostbyname('mail.tracklink.pe');
    $mail->Port = 25;
    $mail->SMTPAuth = false;
    $mail->CharSet = 'UTF-8';
    $mail->setFrom('centralemergencia@tracklink.pe','Central Emergencia');  

    $paraAgendar.=" **correo enviado a: <br>";
    foreach ($correo as $corr){
        ////PARA AGENDAR ///
        $paraAgendar.="-> ".$corr." <br>";
        $mail->addAddress("$corr"); // aquien se manda el mensaje            
    }
    //$mail->addAddress("pvasquez@tracklink.pe"); // aquien se manda el mensaje            

    $mail->addBCC("centralemergencia@tracklink.pe");
    $mail->addBCC("pruebas-ti@tracklink.pe");
    //Set the subject line
    $mail->Subject = 'Desconexión de bateria del vehículo '.ucwords(strtolower($marca)).' placa '.$placaVehiculo.' '; //// ASUNTO //////
    //Read an HTML message body from an external file, convert referenced images to embedded,
    $mail->AddEmbeddedImage('../assets/img/track_new_firma.png', 'tracklink');
    $mail->AddEmbeddedImage('../assets/img/media.png', 'twitter');
    $mail->AddEmbeddedImage('../assets/img/youtube.png', 'youtube');
    $mail->AddEmbeddedImage('../assets/img/fb.png', 'facebook');
    //convert HTML into a basic plain-text alternative body
    $content = ob_get_clean();
    $mail->msgHTML($content);
    //Replace the plain text body with one created manually
    $mail->AltBody = 'Estimado(as) '.$nombres.'Deseamos confirmar la alerta “Desconexión de Batería” o “Variación de Voltaje” registrada en nuestro sistema'; ////////ESTE SE EL CONTENIDO//// QUE SE BERA EN PRIERA INSTANACIA
    //Attach an image file
      //aqui se adjunta los archivos que queremos enviar :D


    // $mail->addAttachment('images/phpmailer_mini.png');
// ///////////// send the message, check for errors
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
       $paraAgendar.=" ** Correo enviado<br>";

   }

  //////
}
}

$paraAgendar.="Detalles:  ".$seguimiento." ";
$registrado="ok";

///////////////////////REGISTRAR EN LA BASE DE DATOSS HERE E ----////////////////////
    $paraAgendar="<p id='paraAgendar' >".$paraAgendar."</p>";
    $dates=date("YmdHis");
    $objetoBateriaTrack=new BateriaTrack();
    $chasisO=trim($chasisO);
    $objetoBateriaTrack->chasis=trim($chasisO);
    $objetoBateriaTrack->agenda=$paraAgendar;
    $objetoBateriaTrack->tallerDias=1;
    if (isset($_POST['cee'])) {
        $objetoBateriaTrack->cee=$_POST['cee'];
    }
    if (isset($_POST['ok'])) {
        $objetoBateriaTrack->ok=$_POST['ok'];            
    }
    if (isset($_POST['cv'])) {
        $objetoBateriaTrack->cv=$_POST['cv'];

    }
    if (isset($_POST['ad'])) {
        $objetoBateriaTrack->ad=$_POST['ad'];
            # code...
    }
    if (isset($_POST['desS'])) {
        $objetoBateriaTrack->desS=$_POST['desS'];
            # code...
    }
    if (isset($_POST['des'])) {
            $objetoBateriaTrack->des=$_POST['des'];            # code...
        }
         if (isset($_POST['ta'])) {
        $objetoBateriaTrack->ta=$_POST['ta'];
            # code...
    }
    if (isset($_POST['fa'])) {
        $objetoBateriaTrack->fa=$_POST['fa'];
            # code...
    }
        $objetoBateriaTrack->accionesSegimiento=$seguimiento;
        $objetoBateriaTrack->fechaAlerta=$_POST['fechaAlertaO'];
        $objetoBateriaTrack->fechaRegistro=$dates;
        $objetoBateriaTrack->nombreCliente=$nombres;
        // $nombres
        if ($objetoBateriaTrack->registrarAlertaOcto()) {
            ///ACTUALIZAMOS LA DATA HER //
       }
   }





   ?>
   <?php 
   if (isset($_GET['idBorrar'])) {
        $idbateriaO=htmlspecialchars($_GET['idBorrar']);
        BateriaTrack::eliminaralertaaOBateria($idbateriaO);
        unset($_GET['idBorrar']);
        irA('bateriaOcto');
   }
    ?>
   <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
   <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
   <link href="../assets/css/sweetalert2.css" rel="stylesheet">
   <style>
    .widget p {
        display: inline-block;        
    }

    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }

    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }

    .reloj .caja-segundos {
        display: inline-block;
    }

    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }</style>
    <body>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <?php include("../includes/menuIzquierdo.php") ?>
               </div>
           </nav>

           <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>

                </nav>
            </div>
            <div class="wrapper wrapper-content">

                <div class="row ibox float-e-margins ibox-content ">
                    <div class="col-md-12">
                        <div class="row">
                           <div class="col-lg-4">
                        <form action="" method="post">
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" style="border:none;" class="col-md-6 form-control border-bottom" placeholder="CHASIS" id="chasis" name="chasis">
                                </div>
                                <div class="col-md-3">
                                    <label for="theft">¿ Es Theft ?</label>
                                    <div class="switch">
                                        <div class="onoffswitch">
                                            <input type="checkbox" class="onoffswitch-checkbox" id="theft" value="THEFT" name="theft">
                                            <label class="onoffswitch-label" for="theft">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <input type="hidden" name="token" value="<?php echo Token::generar() ?>">
                                <button class="btn btn-primary" name="subir">subir</button></div>
                            </div>
                        </form>
                            
                        </div>


                        <div class="col-md-2 ">
                            <a href="int/noLlamarOcto" class="btn btn-success">Ver lista de no llamar <i class="fa fa-arrow-circle-o-right"></i></a>
                        </div>
                        <div class="col-md-2 ">
                            <a href="int/registrosOctoB" class="btn btn-warning">Buscar en registros</a>
                        </div>
                        <div class="col-md-1 ">
                            <a href="int/bateriaEstadisticaOcto" class="btn btn-info">Estadísticas <i class="fa fa fa-area-chart"></i></a>
                        </div>


                        <div class="col-md-3">
                           <div class="widget" style="line-height: 0px;       
                           padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px">
                           <div class="fecha">
                            <p id="diaSemana" class="diaSemana">Martes</p>
                            <p id="dia" class="dia">27</p>
                            <p>de </p>
                            <p id="mes" class="mes">Octubre</p>
                            <p>del </p>
                            <p id="year" class="year">2015</p>
                        </div>

                        <div class="reloj">
                            <p id="horas" class="horas">11</p>
                            <p>:</p>
                            <p id="minutos" class="minutos">48</p>
                            <p>:</p>
                            <div class="caja-segundos">
                                <p id="ampm" class="ampm">AM</p>
                                <p id="segundos" class="segundos">12</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <hr>
            ULTIMO CHASIS SUBIDO
            <table class="table table-hover no-margins">
                <thead>
                    <tr>
                        <th>TIPO</th>
                        <th>CHASIS</th>
                        <th>subido Por</th>
                        <th>Hora Subida</th>                        
                    </tr>
                </thead>

                <tbody id="bateriasOctoData">              
                </tbody>
            </table>
        </div>



    </div>



    <div class="ibox float-e-margins col-lg-12">
        <div class="ibox-title">
            <h5>Alertas de desconexión de bateria</h5>

            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </ul>
            <a class="close-link">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>

    <div class="ibox-content">
        <input type="text" class="form-control input-sm m-b-xs" id="filter"
        placeholder="Filtrar vehículos con desconexión de bateria">

        <table class="footable table  table-stripped" data-page-size="8" data-filter=#filter>
            <thead>
                <tr>
                    <th>Tipo Alerta</th>                                
                    <th>Chasis</th>
                    <th>Fecha</th>
                    <th>Hora</th>
                    <th>A cargo de </th>
                    <th>Agregado Por</th>
                    <th>Acción</th>
                </tr>
            </thead>  
            <tbody  class="tbody">
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5">
                        <ul class="pagination pull-right"></ul>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Última gestion de Baterías Octo </h5>&nbsp;&nbsp; <a href="bateriaOcto"><i class="fa fa-rotate-right "></i></a>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <input type="text" class="form-control input-sm m-b-xs" id="filter1" placeholder="Fitrar Baterías realizadass">

                <table class="footable2 table table-stripped" data-page-size="8" data-filter="#filter1">
                    <thead>
                        <tr>
                            <tr>
                                <th>Cliente</th>
                                <th>Chasis</th>
                                <th>Acciones Realizadas</th>
                                <th data-hide="all">Observaciones</th>
                                <th data-hide="all">Hora Atendida</th>
                                <th>Atendido Por</th>
                                <th data-hide="all">Guardado Onix asi </th>
                            </tr>
                        </tr>
                    </thead>
                    <tbody>
                         <?php $datosba=BateriaTrack::obtenerAlertasAtendidasHoyOcto()  ?> 
                         <?php
   if ($datosba) {
                             foreach($datosba as $var):
                         ?>
			 	<tr id="conenidoTabla">                                
			 		<td><?php echo $var->nombreCliente ?></td> 
			 		<td><?php echo $var->chasis ?></td> 
			 		<td><?php echo $retVal = ($var->cv=="si") ? " -Mensaje en la casilla de voz " : "" ; echo $retVal = ($var->cee=="si") ? " -Se envió correo" : " " ;echo $retVal = ($var->ok=="si") ? " -Todo bien" : " " ;echo $retVal = ($var->ad=="si") ? " -Actualizar Datos" : " " ;echo $retVal = ($var->des=="si") ? " -Desinstalado" : " " ; echo $retVal = ($var->desS=="si") ? " -Contrato Vencido sin Orden de Desinstacion" : " " ; echo $retVal = ($var->ta=="si") ? " -Vehiculo en Taller" : " " ;echo $retVal = ($var->fa=="si") ? " -Falsa alerta" : " " ; ?></td>
			 		<td><?php echo $var->accionesSegimiento ?> </td>
			 		<td><?php echo hace2($var->fechaRegistro) ?></td>
			 		<td><?php $usuarioOb=Usuario::obtenerUsuario($var->idusuario); echo $usuarioOb->usuarioNombre ?> <?php echo $usuarioOb->usuarioApellidoPaterno ?></td>
			 		<td><?php echo $var->agenda ?></td>
			 		
			 	</tr>
<?php
    endforeach;
   }
?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5">
                                <ul class="pagination pull-right"></ul>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<?php include("../includes/footer.php") ?>
</div>
</div>

</div>
<!-- LOS MODALES AQUI  -->


<div class="modal inmodal" id="bateriasOModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                <h4 class="modal-title">Gestionar desconexión  de bateria OCTO</h4>
                <small class="font-bold"><div class="form-group ">
                </div></small>
            </div>
            <form  role="form" method="post" >

                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="submit" name="gestionarBateria" class="btn btn-primary">Gestionado</button>
                </div>
            </form>   
        </div>
    </div>
</div>


<!-- LOS MODALES AQUI  -->

<!-- <button class='btn' data-clipboard-action='copy' data-clipboard-target='p'>Copy</button> -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>

<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/clipboard.min.js"></script>
<script src="../assets/calendario/dist/sweetalert.min.js"></script>

<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
 <?php include_once('../includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
   
<script>

    <?php if (isset($registrado)): ?>
    swal({
      title: "Copiar y agendar en Onix!",
      text: "<?php echo $paraAgendar ?> ",
      html: true,
      type: "success"
  });    
<?php endif ?> 


var clipboard = new Clipboard('.btn1');

clipboard.on('success', function(e) {
    console.log(e);
});

clipboard.on('error', function(e) {
    console.log(e);
});
$(document).ready(function(){

    $('.footable2').footable();
    $('.footable').footable();
    $('.footable3').footable();

});

var baterias={}
baterias.fetchMessages = function() {
    $.ajax({
        url: 'ajax/bateria',
        type: 'post',
        data: {method: 'fetch'},
        success: function(data) {
            $('#bateriasOctoData').html(data);
        }
    }

    );  
}

baterias.interval = setInterval(baterias.fetchMessages, 5000);
baterias.fetchMessages();
//MODAL 
$('#bateriasOModal').on('show.bs.modal', function (e) {
    var rowid = $(e.relatedTarget).data('id');
    $.ajax({
        type : 'post',
                url : 'modal/bateriaDataOctoModal', //Here you will fetch records 
                data :  'rowid='+ rowid, //Pass $id
                success : function(data){
                $('.modal-body').html(data);//Show fetched data from database
            }
        });
});

//MODAL 
//BATERIAS 
var eventos={}
eventos.frejcar =function(){
    $.ajax({
        url: 'ajax/bateriaOctoData',
                type: 'POST', // Send post data
                async: true,
                success: function(data){
                    $('.ibox-content .footable .tbody').html(data);
                }
            });
}
eventos.interval = setInterval(eventos.frejcar, 4000);
eventos.frejcar();

//BATERIAS 

(function(){
    var actualizarHora = function(){
        // Obtenemos la fecha actual, incluyendo las horas, minutos, segundos, dia de la semana, dia del mes, mes y año;
        var fecha = new Date(),
        horas = fecha.getHours(),
        ampm,
        minutos = fecha.getMinutes(),
        segundos = fecha.getSeconds(),
        diaSemana = fecha.getDay(),
        dia = fecha.getDate(),
        mes = fecha.getMonth(),
        year = fecha.getFullYear();

        // Accedemos a los elementos del DOM para agregar mas adelante sus correspondientes valores
        var pHoras = document.getElementById('horas'),
        pAMPM = document.getElementById('ampm'),
        pMinutos = document.getElementById('minutos'),
        pSegundos = document.getElementById('segundos'),
        pDiaSemana = document.getElementById('diaSemana'),
        pDia = document.getElementById('dia'),
        pMes = document.getElementById('mes'),
        pYear = document.getElementById('year');

        
        // Obtenemos el dia se la semana y lo mostramos
        var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        pDiaSemana.textContent = semana[diaSemana];

        // Obtenemos el dia del mes
        pDia.textContent = dia;

        // Obtenemos el Mes y año y lo mostramos
        var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        pMes.textContent = meses[mes];
        pYear.textContent = year;

        // Cambiamos las hora de 24 a 12 horas y establecemos si es AM o PM

        if (horas >= 12) {
            horas = horas - 12;
            ampm = 'PM';
        } else {
            ampm = 'AM';
        }

        // Detectamos cuando sean las 0 AM y transformamos a 12 AM
        if (horas == 0 ){
            horas = 12;
        }

        // Si queremos mostrar un cero antes de las horas ejecutamos este condicional
        // if (horas < 10){horas = '0' + horas;}
        pHoras.textContent = horas;
        pAMPM.textContent = ampm;

        // Minutos y Segundos
        if (minutos < 10){ minutos = "0" + minutos; }
        if (segundos < 10){ segundos = "0" + segundos; }

        pMinutos.textContent = minutos;
        pSegundos.textContent = segundos;
    };

    actualizarHora();
    var intervalo = setInterval(actualizarHora, 1000);
}())
</script>

<?php else: ?> 
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?> 
<?php endif ?>

<?php 
require __DIR__ . "/../vendor/autoload.php";
require("../includes/configuracion.php");
require_once("../includes/Recursos.php");
require_once("../includes/baseDatos.php");
require("../includes/baseDatosOnix.php");
require_once("../includes/Onix.php");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\IOFactory;

if (isset($_POST['DescargarMTC'])) {
    if(isset($_POST) ){
        $onixobj=new Onix();
        $datos=$onixobj->obtenerClientesConMTCAsignadoDescargarExcel();
        #echo count($datos); 

        //$helper->log('Create new Spreadsheet object');
        $fileName = "clientesMTC".date("YmdHis").".xlsx";
        $spreadsheet = new Spreadsheet();


        $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1', '#')
        ->setCellValue('B1', 'Documento de cliente')
        ->setCellValue('C1', 'Plan')
        ->setCellValue('D1', 'Chasis')
        ->setCellvalue('E1','Placa')
        ->setCellvalue('F1','Estado')
        ->setCellvalue('G1','Fecha de última actualización');

        $rows = 2;
        $contador = 1;
        foreach ($datos as $item) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A' . $rows, $contador) //#
                ->setCellValue('B' . $rows, $item[0]) // Documento de cliente
                ->setCellValue('C' . $rows, $item[1]) // Plan
                ->setCellValue('D' . $rows, $item[2]) // Chasis
                ->setCellValue('E' . $rows, $item[3]) // Placa
                ->setCellValue('F' . $rows, $item[4]== "P" ? "Esta pendiente" : "No se encontró su estado") // Estado
                ->setCellValue('G' . $rows, $item[5]); // Fecha de última actualización
            $rows++;
            $contador++;
        }
        
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Items Selected');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
        
        // $writer = new Xlsx($spreadsheet);
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        // $writer->save('php://output');


    }
}

?>
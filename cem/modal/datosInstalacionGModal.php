<?php 
if (isset($_POST['rowid'])) {
	include_once("../../includes/init.php");
    //$operacionObj=new DatosInstalacion;
	$idDatosInstalacion=limpiar($_POST['rowid']);	
    $datosInstala=DatosInstalacion::obtenerdatosDeidoperacion($idDatosInstalacion);
    $idoperacion=$datosInstala->idoperacion;
    if ($idoperacion!=0) {
        $operacionObj=Operacion::obtenerOperacionPorId($idoperacion);
    }else{
        $operacionObj=false;
    }
?>
<link href="../../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
<div class="" style="padding-top: 11px; padding-bottom: 10px;">
<div class="row">
<div class="row">
    <div class="form-group col-md-12">
        <label for="numeroTecnico" >Nombre de cliente*</label>
        <input  style="border: none;" value='<?= $datosInstala->cliente ?>' type="text" name="nombreCliente"  id="numeroTecnico" class="border-bottom form-control" required placeholder='Nombre Cliente'>
    </div>
</div>
<div class="row">
<div class="form-group col-md-3">
        <label for="numeroTecnico" >ORTE</label>
        <input  style="border: none;"  value='<?= $datosInstala->orte ?>'   type="text" placeholder="ORTE" id="orte" name="orte" class="border-bottom form-control" required  >
    </div>
    <div class="form-group col-md-3">
        <label for="numeroTecnico" >Placa Vehiculo*</label>
        <input  style="border: none;" value='<?= $datosInstala->placa ?>' type="text" placeholder="Placa" id="numeroTecnico" name="placa" class="border-bottom form-control" required  >
    </div>
    <div class="form-group col-md-6">
        <label for="Observaciones" >Chasis*</label>
        <input required   style="border: none;" value='<?= $datosInstala->chasis ?>' type="text" placeholder="Chasis" id="Observaciones" class="border-bottom form-control" name="chasis" >
    </div>
</div>
</div>
</div>         
<div class="row ">
<div class='col-md-12 text-warning text-center'>
<?php
    if ($operacionObj) {
       $usuCem=Usuario::obtenerUsuario($operacionObj->idusuarioActivador); 
    }
?>
    <label class="text-warning text-center"> Registrado en CEM por <?= ($datosInstala) ? $datosInstala->operador:$usuCem->usuarioNombre." ".$usuCem->usuarioApellidoPaterno ?> </label>
</div>
</div>
<?php } ?>
  <div class="row">
    <div class="form-group col-md-4">
      <label for="trabajo" class="">Trabajo</label>
      <input style="border: none;"  type="text" placeholder="Trabajo" id="trabajo" name="trabajo" class="trabajo border-bottom form-control" required value="<?= ($operacionObj)?$operacionObj->trabajo:$datosInstala->servicio ?>">
    </div>
<?php
    if ($operacionObj) {
       ?>
    <input type="hidden" name="idop" value="<?php echo $operacionObj->idoperacion ?>" >
    <input type="hidden" name="idTecnicoAsignado" value="<?php echo $operacionObj->idTecnicoAsignado ?>" >
       <?php 
    } 
?>

    <div class="form-group col-md-4">
      <label for="tecnicoInstalador" class="">Tecnico <?= ($datosInstala) ? "(<small> ".$datosInstala->tecnicoInstalador."</small>)" : "" ?></label>
    </div>
<div class="form-group col-md-4">
      <label for="trabajo" class="">OPERADOR CEM</label>
      <input style="border: none;"  type="text" placeholder="Trabajo" id="trabajo" name="trabajo" class="trabajo border-bottom form-control" required value="<?= ($datosInstala)?$datosInstala->operador:'' ?>">
    </div>
  </div> 
  <hr>
  <h4>Datos de <?= ($operacionObj)?strtolower($operacionObj->trabajo):$datosInstala->servicio ?></h4>
<?php
    if (!$operacionObj) {
       ?>
   <div class="row">
    <div class="form-group col-md-4">
      <label for="imei" class="">IMEI</label>
      <input style="border: none;" type="text" placeholder="IMEI" id="imei" name="imei" class="border-bottom form-control" required autocomplete='off' value="<?= ($datosInstala) ? $datosInstala->imei : '' ?>">
    </div>
    <div class="form-group col-md-4">
      <label for="id" class="">ID</label>
      <input style="border: none;" type="text" placeholder="ID" id="id" name="id" class="border-bottom form-control " required autocomplete='off' value="<?= ($datosInstala) ? $datosInstala->id : '' ; ?>">
    </div>
    <div class="form-group col-md-4">
      <label for="odometro" class="">Odometro</label>
      <input style="border: none;" type="text" placeholder="Odometro" id="odometro" name="odometro" class="border-bottom form-control " required value="<?= ($datosInstala) ? $datosInstala->odo: '' ?>">
    </div>
  </div>        
<div class="row">          
    <div class="form-group col-md-3">
      <label for="ubicacionSistema" class="">Ubicación de sistema</label>
      <input style="border: none;" type="text" placeholder="Ubicación de sistema" id="ubicacionSistema" name="ubicacionSistema" class="border-bottom form-control" required value="<?= ($datosInstala) ? $datosInstala->ubicacionSistema : '' ?>">
    </div>
    <div class="form-group col-md-3">
      <label for="lugarInstalacion" class="">Lugar</label>
      <input style="border: none;" type="text" placeholder="Lugar instalación/Chequeo" id="lugarInstalacion" name="lugarInstalacion" class="border-bottom form-control" value="">
    </div>
     <div class="form-group col-md-3">
     <label for="fechaInstalacion" class="">Fecha <?= strtolower($datosInstala->servicio) ?>.</label>
      <input style="border: none;" type="text" placeholder="Fecha instalación" id="fechaInstalacion" name="fechaInstalacion" class="border-bottom form-control" value="<?= formatoSinHoraAniOPrimerodia($datosInstala->fecha)  ?>" >
    </div>      
     <div class="form-group col-md-3">
     <label for="horalInstalacion" class="">Hora <?= strtolower($datosInstala->servicio) ?>.</label>
      <input style="border: none;" type="text" readonly  placeholder="Fecha instalación" id="horalInstalacion" name="horalInstalacion" class="border-bottom form-control" value="<?= substr($datosInstala->horaActivacion, 11,18) ?>" >
    </div>
  </div>
        <h3>
         Estado
        </h3>
        <div class="checkbox-primary">
          <input id="checkbox2" type="radio" required  name="estado" value="activado" <?= ($datosInstala->estado==15 or $datosInstala->estado==4) ? 'checked':'' ?>>
          <label for="checkbox2">
          Activado <?php
    if ($datosInstala) {
    if ($datosInstala->horaActivacion!='') {
        echo formato($datosInstala->horaActivacion);
       ?>
<input type="hidden" name="horaActivacion" value="<?= $datosInstala->horaActivacion ?>" >
       <?php 
    }
    }
?>
          </label>
        </div>
        <div class="checkbox-warning">
        <input id="checkbox1" type="radio" required  name="estado" value="pendiente" <?= ($datosInstala->estado==99) ? 'checked':'' ?>>
          <label for="checkbox1">
           Pendiente
          </label>
        </div>
<?php
    if (!empty($datosInstala->observaciones)) {
       ?>
<div class='row'>
    <label class='text-warning'>Observaciones: <?= $datosInstala->observaciones ?> </label>
</div>          
       <?php 
    }
?>
  <div class="row">
<div class="form-group col-md-8">
      <label for="observacion" class="">Observaciones CEM</label>
      <input style="border: none;" type="text" placeholder="Observaciones" id="observacion" name="observacion" class="border-bottom form-control" >
    </div>
<div class="form-group col-md-4">
      <label for="coordenadas" class="">Coordenadas</label>
      <input style="border: none;" type="text" placeholder="Coordenadas" id="coordenadas" name="coordenadas" class="border-bottom form-control" required value="<?= ($datosInstala) ? $datosInstala->cordenadas : '' ?>">
    </div>
  </div> 
<?php
    if($dataLog=DatosInstalacion::obnerActualizacionesDeLOGidDatosInstalacion($datosInstala->iddatosInstalacion)){
   ?>
<div class="row">
<h2>Ultimas Modificaciones</h2>
<table class="table" >
<thead>
<tr>
<th>Usuario</th>
<th>Comentarios</th>
<th>Fecha</th>
</tr>
</thead>
<tbody>
<?php
 foreach($dataLog as $var) :
$o=Usuario::obtenerUsuario($var->idusuario)
?>
<tr>
<td><?=  $o->usuarioNombre." ".$o->usuarioApellidoPaterno ?></td>
<td><?= $var->observaciones ?></td>
<td><?= formato($var->fechaRegistro) ?></td>
</tr>
<?php
    endforeach;
?>
</tbody>
</table>
  </div> 
   <?php 
    }
?>
       <fieldset>
        <h3>
         Pruebas realizadas
        </h3>
        <div class="checkbox checkbox-info">
          <input id="checkbox4" type="checkbox" name="bloqueoDesbloqueo" value="si">
          <label for="checkbox4">
            Bloqueo / Desbloqueo
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox9" type="checkbox" name="panico" value="si">
          <label for="checkbox9">
            Boton de panico
          </label>
        </div>      
        <div class="checkbox checkbox-info">
          <input id="checkbox5" type="checkbox" name="pestillos" value="si">
          <label for="checkbox5">
           Apertura de pestillos
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox6" type="checkbox" name="sensorCombustible" value="si">
          <label for="checkbox6">
            Sensor de combustible
          </label>
        </div>
         <div class="checkbox checkbox-info">
          <input id="checkbox7" type="checkbox" name="kit" value="si">
          <label for="checkbox7">
            Kit habla escucha
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox8" type="checkbox" name="alertaSonora" value="si">
          <label for="checkbox8">
           Alerta sonora por exeso de velocidad
          </label>
        </div>
      </fieldset>
       <?php 
    }else{
        ?>
<?php if ($operacionObj->estado=='36' OR $operacionObj->estado=='99' OR $operacionObj->estado=='15') { ?>
<?php if ($operacionObj->trabajo!='Desinstalación'){ ?>  
  <div class="row">
    <div class="form-group col-md-4">
      <label for="imei" class="">IMEI</label>
      <input style="border: none;" type="text" placeholder="IMEI" id="imei" name="imei" class="border-bottom form-control" required autocomplete='off' value="<?= ($datosInstala) ? $datosInstala->imei : $operacionObj->imei ?>">
    </div>
    <div class="form-group col-md-4">
      <label for="id" class="">ID</label>
      <input style="border: none;" type="text" placeholder="ID" id="id" name="id" class="border-bottom form-control " required autocomplete='off' value="<?= ($datosInstala) ? $datosInstala->id : $operacionObj->id ; ?>">
    </div>
    <div class="form-group col-md-4">
      <label for="odometro" class="">Odometro</label>
      <input style="border: none;" type="text" placeholder="Odometro" id="odometro" name="odometro" class="border-bottom form-control " required value="<?= ($datosInstala) ? $datosInstala->odo: $operacionObj->odometro ?>">
    </div>
  </div>
  <div class="row">          
    <div class="form-group col-md-3">
      <label for="ubicacionSistema" class="">Ubicación de sistema</label>
      <input style="border: none;" type="text" placeholder="Ubicación de sistema" id="ubicacionSistema" name="ubicacionSistema" class="border-bottom form-control" required value="<?= ($datosInstala) ? $datosInstala->ubicacionSistema : $operacionObj->ubicacionSistema ?>">
    </div>
    <div class="form-group col-md-3">
      <label for="lugarInstalacion" class="">Lugar</label>
      <input style="border: none;" type="text" placeholder="Lugar instalación/Chequeo" id="lugarInstalacion" name="lugarInstalacion" class="border-bottom form-control" value="<?= ($operacionObj->lugarInstalacion) ? strtoupper($operacionObj->lugarInstalacion) : '' ; ?> <?= (($operacionObj->nombreConcecionarioInstalicion)) ? ''.$operacionObj->nombreConcecionarioInstalicion : ''; ?>">
    </div>
<?php
    if ($datosInstala) {
       ?>
     <div class="form-group col-md-3">
     <label for="fechaInstalacion" class="">Fecha <?= strtolower($operacionObj->trabajo) ?>.</label>
      <input style="border: none;" type="text" placeholder="Fecha instalación" id="fechaInstalacion" name="fechaInstalacion" class="border-bottom form-control" value="<?= formatoSinHoraAniOPrimerodia($datosInstala->fecha)  ?>" >
    </div>      
       <?php 
    }else{
        ?>
    <div class="form-group col-md-3">
     <label for="fechaInstalacion" class="">Fecha <?= strtolower($operacionObj->trabajo) ?>.</label>
      <input style="border: none;" type="text" placeholder="Fecha instalación" id="fechaInstalacion" name="fechaInstalacion" class="border-bottom form-control" value="<?= substr($operacionObj->fechaInstalacion, 0,10)  ?>" >
    </div>        
        <?php
    }
?>
    
     <div class="form-group col-md-3">
     <label for="horalInstalacion" class="">Hora <?= strtolower($operacionObj->trabajo) ?>.</label>
      <input style="border: none;" type="text" readonly  placeholder="Fecha instalación" id="horalInstalacion" name="horalInstalacion" class="border-bottom form-control" value="<?= substr($operacionObj->fechaInstalacion, 11,18) ?>" >
    </div>
  </div>
   
        <h3>
         Estado
        </h3>
        <div class="checkbox-primary">
          <input id="checkbox2" type="radio" required  name="estado" value="activado" <?= ($operacionObj->estado==15) ? 'checked':'' ?>>
          <label for="checkbox2">
          Activado <?php
    if ($datosInstala) {
    if ($datosInstala->horaActivacion!='') {
        echo formato($datosInstala->horaActivacion);
       ?>
<input type="hidden" name="horaActivacion" value="<?= $datosInstala->horaActivacion ?>" >
       <?php 
    }
    }
?>
          </label>
        </div>
        <div class="checkbox-warning">
        <input id="checkbox1" type="radio" required  name="estado" value="pendiente" <?= ($operacionObj->estado==99) ? 'checked':'' ?>>
          <label for="checkbox1">
           Pendiente
          </label>
        </div>
<?php
    if (!empty($operacionObj->observacion)) {
       ?>
<div class='row'>
    <label class='text-warning'>Observaciones: <?= $operacionObj->observacion ?> </label>
</div>          
       <?php 
    }
?>
<?php
    if (!empty($datosInstala->observaciones)) {
       ?>
<div class='row'>
    <label class='text-warning'>Observaciones: <?= $datosInstala->observaciones ?> </label>
</div>          
       <?php 
    }
?>
  <div class="row">
<div class="form-group col-md-8">
      <label for="observacion" class="">Observaciones CEM</label>
      <input style="border: none;" type="text" placeholder="Observaciones" id="observacion" name="observacion" class="border-bottom form-control" >
    </div>
<div class="form-group col-md-4">
      <label for="coordenadas" class="">Coordenadas</label>
      <input style="border: none;" type="text" placeholder="Coordenadas" id="coordenadas" name="coordenadas" class="border-bottom form-control" required value="<?= ($datosInstala) ? $datosInstala->cordenadas : $operacionObj->coordenadas ?>">
    </div>
  </div> 
<?php
    if($dataLog=DatosInstalacion::obnerActualizacionesDeLOG($idoperacion)){
   ?>
<div class="row">
<h2>Ultimas Modificaciones</h2>
<table class="table" >
<thead>
<tr>
<th>Usuario</th>
<th>Comentarios</th>
<th>Fecha</th>
</tr>
</thead>
<tbody>
<?php
 foreach($dataLog as $var) :
$o=Usuario::obtenerUsuario($var->idusuario)
?>
<tr>
<td><?=  $o->usuarioNombre." ".$o->usuarioApellidoPaterno ?></td>
<td><?= $var->observaciones ?></td>
<td><?= formato($var->fechaRegistro) ?></td>
</tr>
<?php
    endforeach;
?>
</tbody>
</table>
  </div> 
   <?php 
    }
?>
       <fieldset>
        <h3>
         Pruebas realizadas
        </h3>
        <div class="checkbox checkbox-info">
          <input id="checkbox4" type="checkbox" name="bloqueoDesbloqueo" value="si">
          <label for="checkbox4">
            Bloqueo / Desbloqueo
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox9" type="checkbox" name="panico" value="si">
          <label for="checkbox9">
            Boton de panico
          </label>
        </div>      
        <div class="checkbox checkbox-info">
          <input id="checkbox5" type="checkbox" name="pestillos" value="si">
          <label for="checkbox5">
           Apertura de pestillos
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox6" type="checkbox" name="sensorCombustible" value="si">
          <label for="checkbox6">
            Sensor de combustible
          </label>
        </div>
         <div class="checkbox checkbox-info">
          <input id="checkbox7" type="checkbox" name="kit" value="si">
          <label for="checkbox7">
            Kit habla escucha
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox8" type="checkbox" name="alertaSonora" value="si">
          <label for="checkbox8">
           Alerta sonora por exeso de velocidad
          </label>
        </div>
      </fieldset>
<?php }else{
  ?>
<input type="hidden" name="estado" value="desinstalacion" >
  <div class="row">          
    <div class="form-group col-md-12">
      <label for="ubicacionSistema" class="">Ubicación de sistema</label>
      <input style="border: none;" type="text" placeholder="Ubicacion de sistema" id="ubicacionSistema" name="ubicacionSistema" class="border-bottom form-control" required value="<?php echo $retVal = (isset($operacionObj->ubicacionSistema)) ? $operacionObj->ubicacionSistema : "" ; ?>">
      </div>
    </div>
<h2>
          Desinstalado<?php
if ($datosInstala) {
    if ($datosInstala->horaActivacion!='') {
        echo formato($datosInstala->horaActivacion);
       ?>
<input type="hidden" name="horaActivacion" value="<?= $datosInstala->horaActivacion ?>" >
       <?php 
    }
}
?>
</h2>
<div class="row">
<div class="form-group col-md-4">
      <label for="lugarInstalacion" class="">Lugar</label>
      <input style="border: none;" type="text" placeholder="Lugar instalación/Chequeo" id="lugarInstalacion" name="lugarInstalacion" class="border-bottom form-control" value="<?= ($datosInstala) ? strtoupper($datosInstala->lugarInstalacion) : '' ; ?>">
    </div>
    </div>
  <div class="row">
     <div class="form-group col-md-3">
     <label for="fechaInstalacion" class="">Fecha <?= strtolower($datosInstala->servicio) ?>.</label>
      <input style="border: none;" type="text" placeholder="Fecha instalación" id="fechaInstalacion" name="fechaInstalacion" class="border-bottom form-control" value="<?= formatoSinHoraAniOPrimerodia($datosInstala->fecha)  ?>" >
    </div>      
     <div class="form-group col-md-3">
     <label for="horalInstalacion" class="">Hora <?= strtolower($datosInstala->servicio) ?>.</label>
      <input style="border: none;" type="text" placeholder="Fecha instalación" id="horalInstalacion" name="horalInstalacion" class="border-bottom form-control" value="<?= substr($operacionObj->fechaInstalacion, 11,18) ?>" >
    </div>
    <div class="form-group col-md-6">
      <label for="observacion" class="">Observaciones CEM</label>
      <input required style="border: none;" type="text" placeholder="Observaciones" id="observacion" name="observacion" class="border-bottom form-control" >
    </div>
  </div>
<?php
    if($dataLog=DatosInstalacion::obnerActualizacionesDeLOGidDatosInstalacion($datosInstala->iddatosInstalacion)){
   ?>
<div class="row">
<h2>Ultimas Modificaciones</h2>
<table class="table" >
<thead>
<tr>
<th>Usuario</th>
<th>Comentarios</th>
<th>Fecha</th>
</tr>
</thead>
<tbody>
<?php
 foreach($dataLog as $var) :
$o=Usuario::obtenerUsuario($var->idusuario)
?>
<tr>
<td><?=  $o->usuarioNombre." ".$o->usuarioApellidoPaterno ?></td>
<td><?= $var->observaciones ?></td>
<td><?= formato($var->fechaRegistro) ?></td>
</tr>
<?php
    endforeach;
?>
</tbody>
</table>
  </div> 
   <?php 
    }
?>
  <?php
  } ?>
  <?php
  }
?>
        <?php
    }
?>
<input type="hidden" name="idop" value="<?php echo $operacionObj->idoperacion ?>" >
<input type="hidden" name="idTecnicoAsignado" value="<?php echo $operacionObj->idTecnicoAsignado ?>">

<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/build/jquery.datetimepicker.full.js"></script>
<script src="../../assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="../../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
 <script>
    $("#instalacionForm").validate({

    }); 
<?php
    if (!empty($operacionObj->fechaInstalacion)) {
       ?>
  $('#imei').typeahead({
    source: [
    {"name": "861020036"},
    {"name": "8687890218"}
    ]
  });
  $('#id').typeahead({
    source: [
    {"name": "11D1228000020"},
    ]
  });
jQuery.datetimepicker.setLocale('es');
$('#fechaInstalacion').datetimepicker({
  format:'Y/m/d',
  timepicker:false,
  scrollMonth : false,
  scrollInput: false
});
$('#horalInstalacion').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','11:00','14:00','16:00']
});
       <?php 
?>
</script>          
<?php
}else if($_POST['rowid'] =='Add'){
	include_once("../../includes/init.php");
    ?>
<link href="../../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
<div class="row">
    <div class="form-group col-md-12">
        <label for="numeroOrte" >Número ORTE</label>
        <input   style="border: none;" type="text" name="numeroOrte"  id="numeroOrte" class="border-bottom form-control" required autofocus='on'>
    </div>
</div>
<div class="" style="padding-top: 11px; padding-bottom: 10px;">
<div id='resultadoConsultaOnyx' ></div>
</div>       
<div id="mostrar">
<div class="row">
    <div class="form-group col-md-4">
    <label for="fechaInstalacion" class="">Fecha.</label>
    <input style="border: none;" type="text" placeholder="Fecha instalación" id="fechaInstalacion" name="fechaInstalacion" class="border-bottom form-control" value="<?= date('Y/m/d') ?>" >
</div>      
<div class="form-group col-md-4">
<label for="trabajo" class="">Trabajo</label>
<select  style="border: none;" class="trabajo border-bottom form-control text-capitalize" name="trabajo" required="">
    <option value="" disabled="" selected >Seleccione tecnico</option>
    <option value="Instalacion" >Instalacion</option>
    <option value="Desinstalacion" >Desinstalacion</option>
    <option value="Chequeo por monitoreo" >Chequeo por monitoreo</option>
    <option value="Chequeo de rutina" >Chequeo de rutina</option>
    </select>
</div>
<input type="hidden" name="agregarTEMP" value="tmp" >
<div class="form-group col-md-4">
    <label for="tecnicoInstalador" class="">Tecnico</label>
    <select  style="border: none;" class="tecnico border-bottom form-control text-capitalize" required name="tecnicoInstalador" required="">
    <option value="" disabled="" selected="">Seleccione tecnico</option>
    <?php Usuario::obtenerTecnicos() ?> 
    </select>
</div>
</div>
<div class="row">
<div class="form-group col-md-4">
    <label for="imei" class="">IMEI</label>
    <input style="border: none;" type="text" placeholder="IMEI" id="imei" name="imei" class="border-bottom form-control" required autocomplete='off' >
</div>
<div class="form-group col-md-4">
    <label for="id" class="">ID</label>
    <input style="border: none;" type="text" placeholder="ID" id="id" name="id" class="border-bottom form-control " required autocomplete='off' >
</div>
<div class="form-group col-md-4">
    <label for="odometro" class="">Odometro</label>
    <input style="border: none;" type="text" placeholder="Odometro" id="odometro" name="odometro" class="border-bottom form-control " required >
</div>
</div>
<div class="row">          
<div class="form-group col-md-4">
    <label for="ubicacionSistema" class="">Ubicación de sistema</label>
    <input style="border: none;" type="text" placeholder="Ubicación de sistema" id="ubicacionSistema" name="ubicacionSistema" class="border-bottom form-control" required >
</div>
<div class="form-group col-md-4">
    <label for="lugarInstalacion" class="">Lugar</label>
    <input style="border: none;" type="text" placeholder="Lugar instalación/Chequeo" id="lugarInstalacion" name="lugarInstalacion" class="border-bottom form-control" required >
</div>

<div class="form-group col-md-4">
      <label for="coordenadas" class="">Coordenadas</label>
      <input style="border: none;" type="text" placeholder="Coordenadas" id="coordenadas" name="coordenadas" class="border-bottom form-control" required value="<?php echo $retVal = (isset($operacionObj->coordenadas)) ? $operacionObj->coordenadas : "" ; ?>">
    </div>
</div>
<h3>
    Estado
</h3>
<div class="checkbox-primary">
        <input id="checkbox2" type="radio" required  name="estado" value="activado">
        <label for="checkbox2">
        Activado</label>
    </div>
<div class="checkbox-warning">
<input id="checkbox1" type="radio" required  name="estado" value="pendiente">
    <label for="checkbox1">
    Pendiente
    </label>
</div>
<div class="row">
<div class="form-group col-md-12">
      <label for="observacion" class="">Observaciones CEM</label>
      <input style="border: none;" type="text" placeholder="Observaciones" id="observacion" name="observacion" class="border-bottom form-control" >
    </div>
  </div>
<fieldset>
        <h3>
         Pruebas realizadas
        </h3>
        <div class="checkbox checkbox-info">
          <input id="checkbox4" type="checkbox" name="bloqueoDesbloqueo" value="si">
          <label for="checkbox4">
            Bloqueo / Desbloqueo
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox9" type="checkbox" name="panico" value="si">
          <label for="checkbox9">
            Boton de panico
          </label>
        </div>      
        <div class="checkbox checkbox-info">
          <input id="checkbox5" type="checkbox" name="pestillos" value="si">
          <label for="checkbox5">
           Apertura de pestillos
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox6" type="checkbox" name="sensorCombustible" value="si">
          <label for="checkbox6">
            Sensor de combustible
          </label>
        </div>
         <div class="checkbox checkbox-info">
          <input id="checkbox7" type="checkbox" name="kit" value="si">
          <label for="checkbox7">
            Kit habla escucha
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox8" type="checkbox" name="alertaSonora" value="si">
          <label for="checkbox8">
           Alerta sonora por exeso de velocidad
          </label>
        </div>
      </fieldset>
</div>
<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/build/jquery.datetimepicker.full.js"></script>
<script src="../../assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="../../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script>
    $("#instalacionForm").validate({

    }); 
  $('#imei').typeahead({
    source: [
    {"name": "861020036"},
    {"name": "8687890218"}
    ]
  });
  $('#id').typeahead({
    source: [
    {"name": "11D1228000020"},
    ]
  });
jQuery.datetimepicker.setLocale('es');
$('#fechaInstalacion').datetimepicker({
  format:'Y/m/d',
  timepicker:false,
  scrollMonth : false,
  scrollInput: false
});
$('#mostrar').hide();
    $("#numeroOrte").on("keyup change",function(){
        var dni = $(this).val();
        if(dni.length >= 4)
        {		
            $("#resultadoConsultaOnyx").html('<div class="row" > <div class="col-md-12 text-center"><i class="fa fa-spinner fa-spin fa-5x fa-fw text-center"></i> </div></div>');
            $.ajax({
                type : 'POST',
                url  : '../ajax/dataDeOrte',
                data : $(this).serialize(),
                success : function(data)
                {
                    $("#resultadoConsultaOnyx").html(data);
                }
            });
            $('#mostrar').show();
            return false;
        }
        else
        {
            $('#mostrar').hide();
            $("#resultadoConsultaOnyx").html('');
        }			
    });
</script>
    <?php
}else if($_POST['rowid'] =='rerTemporales'){
	include_once("../../includes/init.php");
    $datosTMP=DatosInstalacion::listadeTemporales();
    if($datosTMP){
        ?>
<div class="table-responsive">
<table class="table table-condensed  table-hover" >
            <thead>
                <tr>                   
                    <th>Orte</th>
                    <th>Lugar</th>
                    <th>Trabajo</th>
                    <th>Cliente</th>
                    <th>Fecha Programada</th>
                    <th>Ejecutivo CEM</th>
                    <th>Fecha Registro</th>
                    <th>Estado</th>
                </tr>
            </thead>  
            <tbody>          
        <?php
        foreach($datosTMP as $var ):
       ?>
    <tr>           
        <td><?= $var->orte ?> </td>
        <td><?= $var->lugarInstalacion ?> </td>
        <td><?= $var->servicio ?> </td>
        <td><?= $var->cliente ?> </td>
        <td><?= formatoSinHora($var->fecha) ?> </td>
        <td><?= $var->operador ?> </td>
        <td><?= haceMinimo($var->fechaRegistro) ?> </td>
        <td><?= obtenerEstado($var->estado) ?></td>
        <!-- <td><button name="eliminarTMP241" class='btn btn-sm btn-danger' id="eliminar" value="<?= $var->iddatosInstalacion ?>"> Eliminar Temporal</td> -->
    </tr>
       <?php 
        endforeach;
        ?>
            </tbody>  
        </table>
   </div> 
        <?php
        }else{
        ?>
           No se encontraron temporales, si tienes alguna duda contactate con pvasquez@tracklink.pe 
        <?php
    }
    ?>
    <?php
}
?>

<?php
    include_once("../../includes/init.php");
?>
    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>
    <div class="showOPERA" style="padding: 30px">
        <div class="showTipo">
            <div class="row">
                <die class="form-group col-md-12" style="margin-top:20px">
                    <label for="seguimientoTrama" class=""> --- Agregar Estado ---</label>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Agregar Estado" id="ndesestado" name="ndesestado">
                <span class="input-group-btn">
                    <button type="button" id="registrarEstado" class="btn btn-primary">Registrar Estado </button>
                </span>
            </div>
                </die>
                <table class="table main-table" >
                    <thead>
                    <tr>
                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"></th>
                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Estado</th>
                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">accion</th>
                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">color</th>
                    </tr>
                    </thead>
                    <tbody id="result">

                    </tbody>
                </table>
            </div>
            <hr>
        </div>
    </div>

        <script>

            function listEstado(){
                $('#result').html("<tr class='gradeX'> <td colspan='4' class='text-center'> Buscando ...</td>    </tr>")
                $.ajax({
                    type : 'POST',
                    url  : '../ajax/resultadolstEstado',
                    data : {},
                    success : function(data)
                    {
                        $("#result").html(data);
                    }
                });
            }

            listEstado();

            $("#registrarEstado").click(function(){
                var body=$('#ndesestado').val()
                if(body.length>0){
                    $.ajax({
                        type : 'POST',
                        url  : '../ajax/registrarEst',
                        data : {body:body},
                        success : function(data)
                        {
                            $('#ndesestado').val('')
                            listEstado()
                        }
                    });
                }
                return false;
            });

        </script>


<?php
    include_once("../../includes/init.php");
?>
    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>
    <div class="showOPERA" style="padding: 30px">
        <div class="showTipo">
            <div class="row">
                <die class="form-group col-md-12" style="margin-top:20px">
                    <label for="seguimientoTrama" class=""> --- Agregar Etiqueta ---</label>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Agregar Etiqueta" id="ndetiqueta" name="ndetiqueta">
                <span class="input-group-btn">
                    <button type="button" id="registrarEtiqueta" class="btn btn-primary">Registrar Etiqueta </button>
                </span>
            </div>
                </die>
                <table class="table main-table" >
                    <thead>
                    <tr>
                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"></th>
                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Etiqueta</th>
                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">color</th>
                    </tr>
                    </thead>
                    <tbody id="result">

                    </tbody>
                </table>
            </div>
            <hr>
        </div>
    </div>

        <script>

            function listEtiqueta(){
                $('#result').html("<tr class='gradeX'> <td colspan='4' class='text-center'> Buscando ...</td>    </tr>")
                $.ajax({
                    type : 'POST',
                    url  : '../ajax/resultadolstEtiquetaSegPk',
                    data : {},
                    success : function(data)
                    {
                        $("#result").html(data);
                    }
                });
            }

            listEtiqueta();

            $("#registrarEtiqueta").click(function(){
                var body=$('#ndetiqueta').val()
                if(body.length>0){
                    $.ajax({
                        type : 'POST',
                        url  : '../ajax/registrarEtiqueta',
                        data : {body:body},
                        success : function(data)
                        {
                            $('#ndetiqueta').val('')
                            listEtiqueta()
                        }
                    });
                }
                return false;
            });

        </script>


<?php
    if (isset($_POST['idanom'])) {
    include_once("../../includes/init.php");
    $idAnom=limpiar(trim($_POST['idanom']));

    $varTI=anomalia::finanomaliid($idAnom);
    $varTI->asignarCargo();
    ?>
    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>
    <div class="showOPERA">
        <div class="well" style="padding-top: 11px;padding-bottom: 10px; ">
                    <?php if ($varTI): ?>
                    <?php if (!is_null($varTI->iduseruso)) {
                        $usuarioObj = Usuario::obtenerUsuario($varTI->iduseruso);
                    } ?>

                        <div class="col-md-12 text-center"><label  class='text-success' for="">A Cargo de : <?php if(!is_null($varTI->iduseruso)){echo $usuarioObj->usuarioNombre.' '.$usuarioObj->usuarioApellidoPaterno;}else{ echo 'Libre';}?></label></divas>
                        </div>
                        <div class="row">
                        <div class="form-group col-md-3">
                            <label for="siste" >Sistema</label>
                            <input name="siste" style="border: none;" type="text" placeholder="sistema" id="siste" readonly class="border-bottom form-control"  value="<?= $varTI->sistema ?>">
                        </div>
                        <div class="form-group col-md-5">
                            <?php $clit=utf8_encode($varTI->cliente) ?>
                            <input type="hidden" id='idanom' name="idanom" value="<?= $idAnom ?>">
                            <input type="hidden" id='nidcust' name="nidcust" value="<?= $varTI->idcustomer  ?>">
                            <input type="hidden" id='nidloc' name="nidloc" value="<?= $varTI->idlocation ?>">
                            <input type="hidden" id='nidveh' name="nidveh" value="<?= $varTI->idvehicle ?>">
                            <label for="nclient" >Cliente :</label>
                            <input  style="border: none;" type="text" readonly id="nclient" name="nclient" class="border-bottom form-control" required value="<?= $clit ?> ">
                        </div>
                        <div class="col-md-3"><label for="nestado" >Estado</label>
                            <select style="border: none;margin-bottom: 15px" class="border-bottom form-control text-capitalize" required=""  id="nestado" name="nestado" aria-required="true">
                                <?php
                            $obtest = anomalia::obtenerListEstadoAnomalia("");
                            foreach ($obtest as $var) :
                                ?>
                                <option class="text-capitalize badge-<?=$var->color?>" value="<?php echo $var->id ?>" <?php echo ($var->id==$varTI->sendMail)?"selected":"";?> ><?= $var->descripcion?></option>
                            <?php
                            endforeach;
                                ?></select>
                        </div>
                        </div>
                        <div class="row">
                        <div class="form-group col-md-3">
                            <label for="nchasis" >chasis :</label>
                            <input  style="border: none;" type="text" readonly id="nchasis" name="nchasis" class="border-bottom form-control" required value="<?= $varTI->chasis ?> ">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nplaca" >Placa :</label>
                            <input  style="border: none;" type="text" readonly id="nplaca" name="nplaca" class="border-bottom form-control" required value="<?= $varTI->placa ?> ">
                        </div>
                        <div class="col-md-2"><label for="ncantnoti" >Cant.Notify</label>
                            <input style="border: none;" type="text" readonly name="ncantnoti" id="ncantnoti"  class="border-bottom form-control"  value="<?= $varTI->cantNotify ?>" >
                        </div>
                        <div class="col-md-2"><label for="ncantdias" >Cant. Dias</label>
                            <input style="border: none;" type="text" readonly name="ncantdias" id="ncantdias"  class="border-bottom form-control"  value="<?= $varTI->cantdias ?>">
                        </div>
                        </div>
        </div>
        <div class="showTipo">
            <div class="row">
                <die class="form-group col-md-12">
                    <label for="seguimientoTrama" class="">Seguimiento</label>
<div class="input-group">
                <input type="text" class="form-control" placeholder="Seguimiento" name="nseguimiento" id="nseguimiento" >
                <span class="input-group-btn">
                    <button type="button" id="registrarSeguiminto" class="btn btn-primary">Registrar Seguimiento </button>
                </span>
            </div>
                </die>
            </div>
            <ul id='segimientohtml'>
                <?php foreach(anomalia::getSEg($idAnom) as $var ): ?>
                    <li><?= $var->fecha ?> <b><?= $var->body ?></b></li>
                <?php endforeach ?>
            </ul>
            <hr>
        </div>
        <div class="modal-footer">
            <button type="button" id='cancelaranomalia' class="btn btn-white" data-dismiss="modal">Cancelar</button>
            <button type="submit" name="guardaranomalia" id="guardaranomalia" class="btn btn-primary" value="<?php echo Token::generar() ?>">Guardar</button>
        </div>
    </div>


        <script>
            $("#registrarSeguiminto").click(function(){
                // alert(servAd) ;seguimientoTrama
                var nidcust=$('#nidcust').val()
                var nidloc=$('#nidloc').val()
                var nidveh=$('#nidveh').val()
                var nplac=$('#nplaca').val()
                var body=$('#nseguimiento').val()
                var subj ="GESTIÓN DE ANOMALÍA - "+nplac
                var idnom = $("#idanom").val()
                var today = new Date();
                var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()+' '+today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                if(body.length>0){
                    $.ajax({
                        type : 'POST',
                        url  : 'ajax/registrarSegAnom',
                        data : {body:body,idanom:idnom,idcustomer:nidcust,idlocation:nidloc,idvehicle:nidveh,subject:subj},
                        success : function(data)
                        {
                            $('#nseguimiento').val('')
                            $("#segimientohtml").append('<li> '+date+' <b>'+body+'</b></li>');
                        }
                    });
                }
                return false;
            });

        </script>
<?php endif ?>

<?php
}
?>

<?php 
if ($_POST['rowid']) {
  include_once("../../includes/init.php");

  $lista = Operacion::obtenerFrecuentes($session->idusuario);
  $idchequeo=htmlspecialchars(htmlentities($_POST['rowid']));
  $chequeoObj=Chequeos::obtenerChequeoTrackPorId($idchequeo);
//  var_dump($chequeoObj);
  if ($chequeoObj->idReprogramado!=0) {
    $reprogramadoDatos=Chequeos::obtenerChequeoTrackPorId($chequeoObj->idReprogramado);
    $usuarioRe=Usuario::obtenerUsuario($chequeoObj->idCreador); $usuarioRep=$usuarioRe->usuarioNombre." ".$usuarioRe->usuarioApellidoPaterno;
  }

  $chObj=new Chequeos;
  $clienteObjModal=new Cliente;
  $chObj->idchequeo=limpiar($idchequeo);
  $errores=array();
  if ($chObj->verificarDisponible()) {
    if ($chObj->asignarChequeo()) {
      
    } else {
      $errores[]= "Error en asignar:";
    }    
  }else{
    if ($chequeoObj->idCargo<>$session->idusuario) {      
      $usuario=Usuario::obtenerUsuario($chequeoObj->idCargo);  $usuario=$usuario->usuarioNombre." ".$usuario->usuarioApellidoPaterno;
      $errores[]="Este chequeo esta a cargo de  $usuario";    
    }
  }
  ?>
  <input type="hidden" name="idchequeo" value="<?php echo $idchequeo ?>">
  <input type="hidden" name="idcustomer" value="<?php echo $chequeoObj->idcustomer ?>">
  <input type="hidden" name="idvehicle" value="<?php echo $chequeoObj->idvehicle ?>">
  <input type="hidden" name="idlocation" value="<?php echo $chequeoObj->idlocation ?>">

  <input type="hidden" name="direccionTaller" value="<?= $lista->direccionInstacion  ?>">
  <input type="hidden" name="disT" id="distritoTallerid" value="<?= $lista->distritoi  ?>">
  <input type="hidden" name="proT" value="<?= $lista->provinciai  ?>">
  <input type="hidden" name="depT" value="<?= $lista->departamentoi  ?>">

  <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>

    <style>
        .ibox-content > .sk-spinner {
            display: none;
        }

        .ibox-content.sk-loading {
            position: relative;
        }
        .ibox-content.sk-loading:after {
            content: '';
            background-color: rgba(255, 255, 255, 0.7);
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        .ibox-content.sk-loading > .sk-spinner {
            display: block;
            position: absolute;
            top: 40%;
            left: 0;
            right: 0;
            z-index: 2000;
        }
    </style>
    <div class="" style="padding-top: 11px;
  padding-bottom: 10px;">
    <div class="row">
        <div class="form-group col-md-4 text-primary text-capitalize">
    <label for="placa"><p>Nombres:</p></label><br>
      <?php echo  $chequeoObj->nombres ; ?>
  <input type="hidden" name="nombres" value="<?php echo $chObj->nombres ?>">

    </div>
    <div class="form-group col-md-4">
    <label for="placa"><p>Placa:</p></label>
      <input style="border: none;" type="text" placeholder="Placa" id="placa" name="placa" class="border-bottom form-control x2 " required  value="<?php echo $chequeoObj->placa ?>"> 
    </div>  
    <div class="form-group col-md-4">
    <label for="placa"><p>Estado:</p></label><br>
     <?php echo obtenerEstado($chequeoObj->estado) ?>
   </div>
 </div> 
 <div class="row">
  <div class="form-group col-md-4 text-primary text-capitalize">
    <label for="placa"><p>Marca modelo:</p></label><br>
 <?php echo $chequeoObj->marcaModelo; ?>
      <input style="border: none;" type="hidden"   name="marcaModelo"   value="<?php echo $chequeoObj->marcaModelo ?>">
 </div> 
<div class="form-group col-md-4">
   <label for="">Plan:</label> <?php echo $chequeoObj->plan ?>
      <input style="border: none;" type="hidden"  id="plan" name="plan"   value="<?= $chequeoObj->plan ?>">
 </div> 
 <div class="form-group col-md-4">
  <label for="">Chasis: </label><br>
    <?php echo $chequeoObj->chasis; ?>
 </div>
</div>
<hr>
<div class="row">
  <div class=" form-group col-md-6">
  <label for="personaContacto"><p>Persona contacto:</p></label>
      <input style="border: none;" type="text" placeholder="Persona contacto" id="personaContacto" name="pcont" class="border-bottom form-control x2 " required  value="<?= $retVal = (empty($chequeoObj->personaContacto)) ? "" : $chequeoObj->personaContacto ;?>">
  </div>
<!--   <input type="hidden" value="<?= $chequeoObj->personaContacto ?>" name="pcont"><input type="hidden" value="<?= $chequeoObj->numeroContacto ?>" name="nrcontacto"> -->  
  <div class=" form-group col-md-6">
  <label for="numeroContacto"><p>Número contacto:</p></label>
      <input style="border: none;" type="text" placeholder="Número contacto" id="numeroContacto" name="nrcontacto" class="border-bottom form-control x2 " required  value="<?= $retVal = (empty($chequeoObj->numeroContacto)) ? "" : $chequeoObj->numeroContacto ;?>">
  </div>  
</div>
<div class="row">
  <div class=" form-group col-md-8">
     <label for="motivo"><p>Motivo:</p></label>
      <input style="border: none;" type="text" placeholder="Motivo" id="motivo" name="motivo" class="border-bottom form-control x2 " required  value="<?= $retVal = (empty($chequeoObj->motivo)) ? "" : $chequeoObj->motivo ;?>">
  </div>
  <div class=" form-group col-md-4">
     <label for="placa"><p>Correo:</p></label>
      <input style="border: none;" type="text" placeholder="Correo" id="placa" name="correo" class="border-bottom form-control x2 "   value="<?= $retVal = (empty($chequeoObj->correo)) ? "" : $chequeoObj->correo ;?>">
  </div>
</div>
<?php if ($chequeoObj->idReprogramado!=0): ?>
<div class="row">
  <div class=" form-group col-md-12">
  <h3 class="text-center">Seguimiento realizado aterior: <span class=" text-danger"><?= $reprogramadoDatos->seguimientoRealizado ?> <small class="text-center">Por <?= $usuarioRep ?></small></span></h3>  
  </div>  
</div>  
<?php endif ?>
<div class="row">
  <div class=" form-group col-md-12">
    <h3 class="text-center">Hora sugerida <?= formatoHaceEn($chequeoObj->fechaHoraChequeoLinea) ?></h3>
    <?php if (!empty($chequeoObj->numeroOrden)): ?>
      <h4 class="text-center">Orden: <?= $chequeoObj->numeroOrden ?> descripción de orden: <?= $chequeoObj->descripcionOrden ?></h4>          
<input type="hidden" name="chequeonumeroOrden" value="<?= $chequeoObj->numeroOrden ?>">
      <?php endif ?>    
</div>
</div>
<?php if (!empty($chequeoObj->observacion)): ?>
  <div class="row">
  <div class=" form-group col-md-12">
    <h3 class="text-center">Observacion SAC: <?= $chequeoObj->observacion ?></h3>   
</div>
</div>
<?php endif ?>
<?php if (!empty($errores)): ?>
  <?php foreach ($errores as $var): ?>
    <h1 class="text-danger text-center" ><?= $var ?></h1>
  <?php endforeach ?>
<?php endif ?>
<?php 
if ($chequeoObj->estado==0) {
  ?>
  <h2 class="text-center">Guardar cómo.</h2>
  <div class="row">
    <div class="form-group col-md-3">
      <label for="trabajo" class="">Trabajo</label>
        <select  style="border: none;" class="tecnico border-bottom form-control" required name="tipoChequeo" id="tipoChequeo">
        <?php if (!empty($chequeoObj->tipoChequeo)): ?>
            <option value="<?= $chequeoObj->tipoChequeo ?>"  selected >Pendiente </option>
          <?php else: ?>
            <option value="" disabled selected >Seleccione *</option>           
        <?php endif ?>
            <option value="Chequeo en linea">Chequeo en linea</option>
            <option value="Chequeo por monitoreo">Chequeo por monitoreo</option>
            <option value="Chequeo de rutina">Chequeo de rutina</option>
            <option value="Upgrade">Upgrade</option>
            <option value="Reinstalacion">Reinstalacion</option>
            <option value="Migración">Migración</option>
            <option value="Desinstalacion">Desinstalación</option>
            <option value="Reprogramar">Reprogramar</option>
            <option value="Cancelado">Cancelar</option> 
        </select>
    </div>    
    <div class="form-group col-md-6">
      <label for="seguimientoRealizado" class="">Seguimiento realizado</label>
      <input style="border: none;" type="text" placeholder="Seguimiento realizado" id="seguimientoRealizado" name="seguimientoRealizado" class="border-bottom form-control x2 " required value="<?= (empty($chequeoObj->seguimientoRealizado)) ? "" : $chequeoObj->seguimientoRealizado ;?>">
    </div>
    <div class="col-md-3 form-group">
        <label for="coordenadasChequeoEnLinea" class="">Coordenadas </label>
      <input style="border: none;" type="text" placeholder="Coordenadas de chequeo en linea" id="coordenadasChequeoEnLinea" name="coordenadasChequeoEnLinea" class="border-bottom form-control x2 chequeo " required=""  value="<?= (empty($chequeoObj->coordenadas)) ? "" : $chequeoObj->coordenadas ;?>" >
    </div>
  </div>
<div class="row esconder">
    <div class="form-group col-md-6">
      <label for="fechaRepro" class="">Fecha de nuevo chequeo en linea</label>
      <input style="border: none;" type="text" placeholder="Fecha" id="fechaRepro" name="fechaRepro" class="border-bottom form-control x2 " required >
    </div>
    <div class="col-md-6 form-group">
        <label for="horaRepro" class="">Hora</label>
      <input style="border: none;" type="text" placeholder="hora" id="horaRepro" name="horaRepro" class="border-bottom form-control x2 " required="" >
    </div>
  </div>
</div>
  <div class="row">
    <div class="form-group col-md-4">
      <label for="fechaInstalacion" class="sr-only">Fecha de Instalación</label>
      <input style="border: none;" type="text" placeholder="Fecha de instalacion" id="fechaInstalacion" name="fechaInstalacion" class="trabajo border-bottom form-control" required  >
    </div>    
    <div class="form-group col-md-8">
      <label for="instalador" class="sr-only">Instalador</label>
      <input style="border: none;" type="text" placeholder="Instalador" id="instalador" name="instalador" class="trabajo border-bottom form-control x2 " required>
    </div>

  </div> 
  <div class="row">
    <div class="form-group col-md-4">
      <label for="ubicacionSistema" class="sr-only">Ubicacion de sistema</label>
      <input style="border: none;" type="text" placeholder="Ubicacion de sistema" id="ubicacionSistema" name="ubicacionSistema" class="trabajo ubicacionSistema border-bottom form-control" required >
    </div> 
  </div> 

  <hr>
  <h3 class="text-center" >Lugar de Chequeo</h3>
  <hr>
  <div class="row">          
    <div class="form-group col-md-3">
      <label for="lugar" class="sr-only">Lugar</label>
      <select  style="border: none;" class="trabajo tecnico border-bottom form-control text-capitalize"  name="lugar" id="lugar" required>
        <option class="text-capitalize" value=""  selected  disabled="">Seleccione lugar de chequeo</option>
        <option class="text-capitalize" value="Taller" >Taller</option>
        <option class="text-capitalize" value="Domicilio Cliente" >Domicilio Cliente</option>
        <option class="text-capitalize" value="Concesionario" >Concesionario</option>
      </select>
        <input type="hidden" id="canthoraprog" name="canthoraprog" value="2">
    </div>
      <div class="form-group col-md-9">
      <label for="coordenadasChequeoEnLineaLugar" class="sr-only">Coordenadas de lugar</label>
      <input style="border: none;" type="text" placeholder="Coordenadas de lugar" id="coordenadasChequeoEnLineaLugar" name="coordenadasChequeoEnLineaLugar" class="trabajo border-bottom form-control" required >
    </div>
  </div>
  <div id="esconderHtml">
  <div class="row">
  <div class="form-group  col-md-4">
    <label for="departamentoInstalacionD" class="">Departamento*</label>
    <select  style="border: none;" class="trabajo departamentoInstalacionD border-bottom form-control text-capitalize" required name="departamento" id="departamentoInstalacionD">
      <option class="text-capitalize" value="" disabled selected >Seleccione departamento</option>
      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
      <?php if ($objDepartamento): ?>
        <?php foreach ($objDepartamento as $var): ?>
          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
        <?php endforeach ?>
      <?php else: ?>
      <?php endif ?>
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="provinciaInstalacionD" class="">Provincia*</label>
    <select  style="border: none;" class="trabajo provinciaInstalacionD border-bottom form-control text-capitalize" required name="provincia" id="provinciaInstalacionD" >
      
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="distritoInstalacionD" class="">Distrito*</label>
    <select  style="border: none;" class="trabajo distritoInstalacionD border-bottom form-control text-capitalize" required name="distrito"  id="distritoInstalacionD" >
      
    </select>           
  </div>
</div>

</div>
  <div class="row">
     <div class="form-group col-md-6">
      <label for="direccionInstalacion" class="sr-only">Direccion</label>
      <input style="border: none;" type="text" placeholder="Direccion" id="direccionInstalacion" name="direccionInstalacion" class="trabajo border-bottom form-control" required >
    </div>
    <div class="form-group col-md-6">
      <label for="referenciaInstalacion" class="sr-only">Referencia</label>
      <input style="border: none;" type="text" placeholder="Referencia" id="referenciaInstalacion" name="referenciaInstalacion" class="trabajo border-bottom form-control"  required>
    </div>
  </div>

  <h3 class="text-center">Fecha</h3>
<div class="row" >
        <div class="form-group col-md-4">   
                      <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" class="trabajo form-control tal tala fecha"  id="fechaIT" name="fechaIT" autocomplete="off" required>
          </div>
        </div>
        <div class="form-group col-md-4">
            <label for="turno" class="">Turno</label>
                <button  type="button" placeholder="Hora *" id="" value='' required  class="horabtn form-control border-bottom btn btn-info" >Seleccionar hora</button>
                <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posT" class=" trabajo hora form-control border-bottom"  required="" readonly>
        </div>
        <div class="form-group col-md-4">
                <label for="horaS" class="">Hora seleccionada</label>
                <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaT" class="trabajo horaVer  form-control border-bottom"  required="" readonly>
        </div>
    </div>
    <div class="ibox" id="ibox1">
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-double-bounce">
                <div class="sk-double-bounce1"></div>
                <div class="sk-double-bounce2"></div>
            </div>
            <div id="programacionHtml">
            </div>
        </div>
    </div>
    <?php
}
    ?>
    <?php
    if ($chequeoObj->estado==321) {
        ?>
        <h2 class="text-center">Guardar cómo.</h2>
  <div class="row">
    <div class="form-group col-md-3">
    <label for="trabajo" class="">Trabajo <?= !empty($chequeoObj->tipoChequeo) ? "<label class='text-danger' >".$chequeoObj->tipoChequeo."</label>" : '' ?></label>
        <select  style="border: none;" class="tecnico border-bottom form-control" required name="tipoChequeo" id="tipoChequeo">       
            <option value="" disabled selected >Seleccione *</option>           
            <option value="Chequeo en linea">Chequeo en linea</option>
            <option value="Chequeo por monitoreo">Chequeo por monitoreo</option>
            <option value="Chequeo de rutina">Chequeo de rutina</option>
            <option value="Upgrade">Upgrade</option>
            <option value="Reinstalacion">Reinstalacion</option>
            <option value="Migración">Migración</option>
            <option value="Desinstalacion">Desinstalación</option>
            <option value="Reprogramar">Reprogramar</option>
            <option value="Cancelado">Cancelar</option> 
        </select>
    </div>    
    <div class="form-group col-md-6">
      <label for="seguimientoRealizado" class="">Seguimiento realizado</label>
      <input style="border: none;" type="text" placeholder="Seguimiento realizado" id="seguimientoRealizado" name="seguimientoRealizado" class="border-bottom form-control x2 " required value="<?= $chequeoObj->seguimientoRealizado ?>">
    </div>
    <div class="col-md-3 form-group">
        <label for="coordenadasChequeoEnLinea" class="">Coordenadas </label>
      <input style="border: none;" type="text" placeholder="Coordenadas de chequeo en linea" id="coordenadasChequeoEnLinea" name="coordenadasChequeoEnLinea" class="border-bottom form-control x2 chequeo " required=""  value="<?= (empty($chequeoObj->coordenadas)) ? "" : $chequeoObj->coordenadas ;?>" >
    </div>

  </div>
<div class="row esconder">
    <div class="form-group col-md-6">
      <label for="fechaRepro" class="">Fecha de nuevo chequeo en linea</label>
      <input style="border: none;" type="text" placeholder="Fecha" id="fechaRepro" name="fechaRepro" class="border-bottom form-control x2 " required >
    </div>
    <div class="col-md-6 form-group">
        <label for="horaRepro" class="">Hora</label>
      <input style="border: none;" type="text" placeholder="hora" id="horaRepro" name="horaRepro" class="border-bottom form-control x2 " required="" >
    </div>
  </div>
  <div class="row">
    <div class="form-group col-md-4">
      <label for="fechaInstalacion" class="">Fecha de Instalación</label>
      <input style="border: none;" type="text" placeholder="Fecha de instalacion" id="fechaInstalacion" name="fechaInstalacion" class="trabajo border-bottom form-control trabajo" required value="<?= $chequeoObj->fechaInstalacion ?>" >
    </div>    
    <div class="form-group col-md-8">
      <label for="instalador" class="">Instalador</label>
      <input style="border: none;" type="text" placeholder="Instalador" id="instalador" name="instalador" class="trabajo border-bottom form-control x2 " required value="<?= $chequeoObj->instalador ?>">
    </div>
  </div> 
  <div class="row">
    <div class="form-group col-md-4">
      <label for="ubicacionSistema" class="">Ubicacion de sistema</label>
      <input style="border: none;" type="text" placeholder="Ubicacion de sistema" id="ubicacionSistema" name="ubicacionSistema" class="trabajo ubicacionSistema border-bottom form-control" required value="<?= $chequeoObj->ubicacionSistema ?>">
    </div> 
  </div> 

  <hr>
  <h3 class="text-center" >Lugar de Chequeo </h3>
  <hr>
  <div class="row">          
    <div class="form-group col-md-3">
      <label for="lugar" class="">Lugar <?= !empty($chequeoObj->lugar) ? "<label class='text-danger' >".$chequeoObj->lugar."</label>" : '' ?></label>
      <select  style="border: none;" class="tecnico trabajo border-bottom form-control text-capitalize"  name="lugar" id="lugar" required>
        <option class="text-capitalize" value=""  selected disabled="" >Seleccione lugar</option>
        <option class="text-capitalize" value="Taller" >Taller</option>
        <option class="text-capitalize" value="Domicilio Cliente" >Domicilio Cliente</option>
        <option class="text-capitalize" value="Concesionario" >Concesionario</option>
      </select>
        <input type="hidden" id="canthoraprog" name="canthoraprog" value="2">
    </div>
    <div class="form-group col-md-9">
      <label for="coordenadasChequeoEnLineaLugar" class="">Coordenadas de lugar</label>
      <input style="border: none;" type="text" placeholder="Coordenadas de lugar" id="coordenadasChequeoEnLineaLugar" name="coordenadasChequeoEnLineaLugar" class="trabajo border-bottom form-control" required value="<?= $chequeoObj->coordenadas ?>" >
    </div>
  </div>
<div id="esconderHtml">
<div class="row">
  <div class="form-group  col-md-4">
    <label for="departamentoInstalacionD" class="">Departamento*</label>
    <select  style="border: none;" class="trabajo departamentoInstalacionD border-bottom form-control text-capitalize" required name="departamento" id="departamentoInstalacionD">
      <option class="text-capitalize" value="<?php echo $chequeoObj->departamentoc ?>"  selected ><?php $departamento=$clienteObjModal->obtenerDepartamentoPorId($chequeoObj->departamentoc) ?><?= $departamento->departamento ?></option>
      <?php $objDepartamento=Cliente::obtenerDepartamentos() ?>
      <?php if ($objDepartamento): ?>
        <?php foreach ($objDepartamento as $var): ?>
          <option value="<?php echo $var->iddepartamento ?>"><?php echo $var->departamento ?></option>
        <?php endforeach ?>
      <?php else: ?>
      <?php endif ?>
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="provinciaInstalacionD" class="">Provincia*</label>
    <select  style="border: none;" class="trabajo provinciaInstalacionD border-bottom form-control text-capitalize" required name="provincia" id="provinciaInstalacionD" >
      <option class="text-capitalize" value="<?php echo $chequeoObj->provinciac ?>"  selected ><?php $provincia=$clienteObjModal->otenerProvinciaPorId($chequeoObj->provinciac) ?><?php echo $provincia->provincia ?></option>
    </select>
  </div>
  <div class="form-group  col-md-4">
    <label for="distritoInstalacionD" class="">Distrito*</label>
    <select  style="border: none;" class="trabajo distritoInstalacionD border-bottom form-control text-capitalize" required name="distrito"  id="distritoInstalacionD" >
      <option class="text-capitalize" value="<?php echo $chequeoObj->distritoc ?>"  selected ><?php $distrito=$clienteObjModal->obtenerDistritoPorId($chequeoObj->distritoc) ?><?= $distrito->distrito ?></option>
    </select>           
  </div>
</div>
</div>
  <div class="row">
     <div class="form-group col-md-6">
      <label for="direccionInstalacion" class="">Direccion</label>
      <input style="border: none;" type="text" placeholder="Direccion" id="" name="direccionInstalacion" class="trabajo border-bottom form-control" required value="<?= $chequeoObj->direccion ?>">
      <input type="hidden" id="direccionInstalacion">
    </div>
    <div class="form-group col-md-6">
      <label for="referenciaInstalacion" class="">Referencia</label>
      <input style="border: none;" type="text" placeholder="Referencia" id="referenciaInstalacion" name="referenciaInstalacion" class="trabajo border-bottom form-control" value="<?= $chequeoObj->referencia ?>"  required>
    </div>
  </div>
  <h3 class="text-center">Fecha</h3>
<div class="row" >
        <div class="form-group col-md-4">   
                      <label  class="">Fecha</label>
          <div class="input-group date">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" class="trabajo form-control tal tala fecha"  id="fechaIT" name="fechaIT" autocomplete="off" required>
          </div>
        </div>
        <div class="form-group col-md-4">
            <label for="turno" class="">Turno</label>
                <button  type="button" placeholder="Hora *" id="" value='' required  class="trabajo horabtn form-control border-bottom btn btn-info" >Seleccionar hora</button>
                <input style="border: none;" type="hidden" placeholder="Hora *" id="" name="posT" class="hora form-control border-bottom"  required="" readonly>
        </div>
        <div class="form-group col-md-4">
                <label for="horaS" class="">Hora seleccionada</label>
                <input style="border: none;" type="text" placeholder="Hora *" id="" name="horaT" class="horaVer tala tal form-control border-bottom trabajo "  required="" readonly>
        </div>
    </div>
    <div class="ibox" id="ibox1">
        <div class="ibox-content">
            <div class="sk-spinner sk-spinner-double-bounce">
                <div class="sk-double-bounce1"></div>
                <div class="sk-double-bounce2"></div>
            </div>
            <div id="programacionHtml">
            </div>
        </div>
    </div>

    <?php
}
    ?>

    <?php
}
?>
<?php
$fechaCreacion=date("Y-m-d h:i A");
?>
<script src="../../assets/bootbox/bootbox.all.js"></script>
<script>
    $(".esconder").hide();
$("#horaRepro").prop('disabled',true);
$("#fechaRepro").prop('disabled',true);


    function lstTProg(){
        var lugarI=$("#lugar").val();
        var TCheq=$("#tipoChequeo").val();
        var distrito="0";
        var tthinstala=$('#canthoraprog').val();
        var cita = 'General';
        var lugarT='';
        if (lugarI=='Taller') {
            var fecha = $('#fechaIT').val();
            distrito='1291'
            lugarT='Taller';
        }else if (lugarI=='Domicilio Cliente') {
            var fecha = $('#fechaIT').val();
            distrito=$('#distritoInstalacionD').val();
            lugarT='Domicilio';
        }else if (lugarI=='Concesionario') {
            var fecha = $('#fechaIT').val();
            distrito=$('#distritoInstalacionD').val();
            lugarT='Concesionario';
        }
      //  console.log("lugar="+lugarI+" tcheq="+TCheq);
        if (lugarI!=null && TCheq!=null && fecha!='' && (distrito==0 || distrito!=null)) {
            $(".horabtn").off( "click" );
            $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
            $.ajax({
                type : 'POST',
                url  : '../includes/tbProgramacion',
                data : {fecha:fecha,cita:cita,tthinstala:tthinstala,iddist:distrito,lugart:lugarT},
                success : function(data)
                {
                    $("#programacionHtml").html(data);
                    $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
                    $(".horabtn").on("click",lstTProg).find(".horabtn");
                }
            });
        }else{
            $("#programacionHtml").html('<h3 class="text-center text-info">Debe Seleccionar Tipo Chequeo, Lugar, fecha y/o Distrito.</h3>');
        }
        return false;
    }

    $(".horabtn").click(function(){
        lstTProg();
    });


    function validaDP() {
        // var dp=1;
        var fecha = $('#fechaIT').val();
        var hora = $(".horaVer").val();
        var canthoraprog ='2';
        var idtec = $('input[name="posT"]').val();
        // console.log("fecha: "+fecha+" idtec:"+idtec+" canhora:"+canthoraprog.trim());
        $.ajax({
            type: 'POST',
            url: '../com/ajax/validarRegistroTB',
            data:'tipo=save&fecha='+fecha+'&idtec='+idtec+"&canthoraprog=" + canthoraprog.trim() + "&hora=" + hora,
            // data :  'tipo=save&fecha=2019-11-27&idtec=2&canthoraprog=2&hora=12:00',
            success: function (data) {
                if (data.trim() == 'dp') {
                    //dp='1';
                    $("#agregarVehiculoBttn").prop("disabled",true);
                    //      clearInterval(eventos.interval);
                    $("#programacionHtml").html('');
                    $(".horabtn").html('Seleccionar hora');
                    $(".horaVer").val('');
                    // fn(dp);
                    bootbox.alert({
                        title: "<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                        message: "<label>El horario Seleccionado("+hora+") ya fue Registrado,Por Favor Seleccione otro*</label>",
                        size: 'small',
                        callback: function () {
                            /*  l.ladda( 'stop' );*/
                        }
                    });
                }else if(data.trim()=='Ok'){
                    $("#agregarVehiculoBttn").prop("disabled",false)
                }
            }
        });
    }

    var eventos={};
    eventos.frejcar =function(){
        validaDP();
    };
    eventos.interval = setInterval(eventos.frejcar, 3000);

    $('.fecha').change(function () {
        $(".horaVer").val('');
        $(".horabtn").val('Seleccionar hora');
        $("#programacionHtml").html('');
    });
    $('#tipoChequeo').change(function () {

        $(".horaVer").val('');
    $(".horabtn").val('Seleccionar hora');
    $("#programacionHtml").html('');
    var tipoChequeo=$(this).val();
    if (tipoChequeo=='Chequeo en linea') {
       $(".chequeo").prop('disabled',false);
       $(".trabajo").prop('disabled',true);
       $(".esconder").hide('slow');
    }else{
       $(".trabajo").prop('disabled',false);
       $(".chequeo").prop('disabled',true);
       $(".esconder").hide('slow');
    }
    if (tipoChequeo=='Cancelado' || tipoChequeo=='Reprogramar') {
       $(".chequeo").prop('disabled',true);
       $(".trabajo").prop('disabled',true);
       if (tipoChequeo=='Reprogramar') {
            $("#horaRepro").prop('disabled',false);
            $("#fechaRepro").prop('disabled',false);
       $(".esconder").show('slow');
       }else{
            $("#horaRepro").prop('disabled',true);
            $("#fechaRepro").prop('disabled',true);
        $(".esconder").hide('slow');
       }
       //$(".fecha").prop('disabled',false);
    }
});

$('#lugar').on("change",function () {  
    $(".horaVer").val('');
    $(".horabtn").val('Seleccionar hora');
    $("#programacionHtml").html('');
  var lugarChequeo=document.getElementById('lugar');
  value = lugarChequeo.options[lugarChequeo.selectedIndex].value;
  if (value=="Taller") { 
    $('#coordenadasChequeoEnLineaLugar').prop('disabled', true);
    $('#referenciaInstalacion').prop('disabled', true);
     document.getElementById('direccionInstalacion').value='<?= $lista->direccionInstacion ?>' ;  
    $("#esconderHtml").hide();
    $('.departamentoInstalacionD').prop('disabled', true);
    $('.provinciaInstalacionD').prop('disabled', true);
    $('.distritoInstalacionD').prop('disabled', true);
       
}else{
      $('#coordenadasChequeoEnLineaLugar').prop('disabled', false); 
      $('#referenciaInstalacion').prop('disabled', false); 
      $('#direccionInstalacion').prop('disabled', false);       
      document.getElementById('direccionInstalacion').value='' ;
      $("#esconderHtml").show();
       $('.departamentoInstalacionD').prop('disabled', false);
       $('.provinciaInstalacionD').prop('disabled', false);
       $('.distritoInstalacionD').prop('disabled', false);
    }

});
<?php $fechaA=date('Y-m-d') ?>
jQuery.datetimepicker.setLocale('es');
$('.fecha').datetimepicker({
  minDate:'<?= $fechaA ?>',
  format:'Y-m-d',
  timepicker:false,
  scrollMonth : false,
  scrollInput: false
});
$('#horaRepro').datetimepicker({
  datepicker:false,
      format:'H:i',
    });
$('#fechaRepro').datetimepicker({
  minDate:'<?= $fechaA ?>',
  format:'Y-m-d',
  timepicker:false,
  scrollMonth : false,
  scrollInput: false
});

 ///////////
$("#esconderHtml").hide();
 $('.departamentoInstalacionD').prop('disabled', true);
 $('.provinciaInstalacionD').prop('disabled', true);
 $('.distritoInstalacionD').prop('disabled', true); 

 $(".departamentoInstalacionD").change(function(){provincias4();});
 $(".provinciaInstalacionD").change(function(){distritos4();});
 
function provincias4(){
  var iddepartamento = $(".departamentoInstalacionD").val();
  $.ajax({        
    type : 'POST',
    url  : '../com/ajax/provincia',
    data :  'iddepartamento='+ iddepartamento,
    success : function(data)
    {
      $(".provinciaInstalacionD").html(data);
    }
  });
  return false;
}
  function distritos4(){
      var idprovincia = $(".provinciaInstalacionD").val();
      $.ajax({        
        type : 'POST',
        url  : '../com/ajax/distrito',
        data :  'idprovincia='+ idprovincia,
        success : function(data)
        {
          $(".distritoInstalacionD").html(data)
        }
      });
      return false;
    }


    $('#departamentoInstalacionD,#provinciaInstalacionD,#distritoInstalacionD').change(function(){
        $("#programacionHtml").html('');
        $(".horabtn").html('Seleccionar hora');
        $(".horaVer").val('');
    });
    ///////////////////////////////////////////////////
</script>

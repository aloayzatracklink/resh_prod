<?php
include_once("../../includes/head.php");
$idAcc = soloNumero($_POST['rowid']);
$tipoOpera = $_POST['tipoOpera'];
$Objtec = acciones::obtenerAccesos($idAcc);
if ($tipoOpera == 'edit') {
    ?>
    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <div class="well" style="padding-top: 11px;padding-bottom: 10px;">
        <div class="row">
            <div class="form-group col-md-4 text-primary text-capitalize">
                <label for="nfinanciera">Financiera</label>
                <select style="border: none;" class="tecnico border-bottom form-control" required name="nfinanciera">
                    <option value="MOVILIZA" <?php if ($Objtec->financiera == "MOVILIZA") {
                        echo "selected";
                    } ?>>MOVILIZA
                    </option>
                    <option value="EL CLAN" <?php if ($Objtec->financiera == "EL CLAN") {
                        echo "selected";
                    } ?>>EL CLAN</option>
                    <option value="SANTANDER" <?php if ($Objtec->financiera == "SANTANDER") {
                        echo "selected";
                    } ?>>SANTANDER
                    </option>
                </select>
            </div>
            <div class="form-group col-md-5">
                <label for="ncliente">Cliente</label>
                <input style="border: none;" type="text" placeholder="Cliente" id="ncliente" name="ncliente"
                    class="border-bottom form-control" required value="<?= $Objtec->cliente ?>">
                <input type="hidden" id='tipoOpera' name="tipoOpera" value="<?= $tipoOpera ?>">
            </div>
            <div class="col-md-3">
                <label for="nchasis">Chasis</label>
                <input style="border: none;" type="text" placeholder="Chasis" id="nchasis" name="nchasis"
                    class="border-bottom form-control" required value="<?= $Objtec->chasis ?>">
                <input type="hidden" name="nidacceso" value="<?= $Objtec->idaccfi ?>">
            </div>
        </div>
        <div class="row">
            <div class="form-group  col-md-4">
                <label for="nplaca">Placa</label>
                <input style="border: none;" type="text" placeholder="Placa" id="nplaca" name="nplaca"
                    class="border-bottom form-control" required value="<?= $Objtec->placa ?>">
            </div>
            <div class="form-group  col-md-6">
                <label for="ndireccion">Dirección</label>
                <input style="border: none;" type="text" placeholder="Dirección" id="ndireccion" name="ndireccion"
                    class="border-bottom form-control" required value="<?= $Objtec->direccion ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="nusuario">Usuario</label>
                <input style="border: none;" type="text" placeholder="Usuario" id="nusuario" name="nusuario"
                    class="border-bottom form-control" required value="<?= $Objtec->usuario ?>">
            </div>

            <div class="col-md-4">
                <label for="ncontrasenia">Clave</label>
                <input style="border: none;" type="text" placeholder="Clave" id="ncontrasenia" name="ncontrasenia"
                    class="border-bottom form-control" required value="<?= $Objtec->contrasenia ?>">
            </div>
        </div>
    </div>
    <!-- Nuevo botón "Enviar credenciales" gestionDeAccesos -->
    <button type="button" class="btn btn-primary" id="enviarCredenciales" onclick=envCred()> ENVIAR CREDENCIALES</button>
    <style>
        .popover {
            z-index: 999999;
        }
    </style>

    <?php
} elseif ($tipoOpera == 'save') {
    ?>

    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <div class="well" style="padding-top: 11px;padding-bottom: 10px;">
        <div class="row">
            <div class="form-group col-md-4 text-primary text-capitalize">
                <label for="nfinanciera">Financiera</label>
                <select style="border: none;" class="tecnico border-bottom form-control" required name="nfinanciera">
                    <option value="MOVILIZA">MOVILIZA</option>
                    <option value="EL CLAN">EL CLAN</option>
                    <option value="SANTANDER">SANTANDER</option>
                </select>
            </div>
            <div class="form-group col-md-5">
                <label for="ncliente">Cliente</label>
                <input style="border: none;" type="text" placeholder="Cliente" id="ncliente" name="ncliente"
                    class="border-bottom form-control" required value="">
            </div>
            <div class="col-md-3">
                <label for="nchasis">Chasis</label>
                <input style="border: none;" type="text" placeholder="Chasis" id="nchasis" name="nchasis"
                    class="border-bottom form-control" required value="">
                <input type="hidden" name="nidacceso" value="">
                <input type="hidden" id='tipoOpera' name="tipoOpera" value="">
            </div>
        </div>
        <div class="row">
            <div class="form-group  col-md-4">
                <label for="nplaca">Placa</label>
                <input style="border: none;" type="text" placeholder="Placa" id="nplaca" name="nplaca"
                    class="border-bottom form-control" required value="">
            </div>
            <div class="form-group  col-md-6">
                <label for="ndireccion">Dirección</label>
                <input style="border: none;" type="text" placeholder="Dirección" id="ndireccion" name="ndireccion"
                    class="border-bottom form-control" required value="">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="nchasis">Usuario</label>
                <input style="border: none;" type="text" placeholder="Usuario" id="nusuario" name="nusuario"
                    class="border-bottom form-control" required value="">
            </div>

            <div class="col-md-4">
                <label for="nchasis">Clave</label>
                <input style="border: none;" type="text" placeholder="Clave" id="ncontrasenia" name="ncontrasenia"
                    class="border-bottom form-control" required value="">
            </div>
        </div>
    </div>
    <!-- Nuevo botón "Enviar credenciales" -->
    <!-- <button type="button" class="btn btn-primary" id="enviarCredenciales" onclick=envCred()>Enviarcredenciales</button> -->
    <style>
        .popover {
            z-index: 999999;
        }
    </style>

    <?php
}
?>
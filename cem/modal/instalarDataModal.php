
<link rel="stylesheet" href="../../assets/css/plugins/select2/select2.min.css">
<link rel="stylesheet" href="../../assets/css/style2.css">
<?php
if (isset($_POST['rowid']) AND $_POST['rowid'] !='Add' AND $_POST['rowid'] !='rerTemporales') {
	include_once("../../includes/init.php");
	$idoperacion=limpiar($_POST['rowid']);	
	$operacionObj=Operacion::obtenerOperacionPorId($idoperacion);
    $VhObj=Vehiculo::obtenerVehiculo($operacionObj->idvehiculo);
    $cliObjR=Cliente::obtenerDatosDeclientePorId($operacionObj->idcliente);
    $datosFac=DatosFacturacion::obtenerDatosFacturacionPorId($operacionObj->datosfacturacionid);
    $datosInstala=DatosInstalacion::obtenerSiexiste($idoperacion);

    $verificaPeticionActivacion = TramaIntegracionTY::verificarActivacion($idoperacion);

    if ($VhObj) {
      $clienteObj=Onix::obtenerDatosDesdeOrden($operacionObj->numeroOrden);
    }
    $cheObj=false;
    if ($operacionObj->idChequeo!=0) {
        $cheObj=Chequeos::obtenerChequeoTrackPorId($operacionObj->idChequeo);
        $clienteObj=Onix::obtenerDatosDesdeIds($operacionObj->idcliente,$operacionObj->idvehiculo,$operacionObj->idlocation);
    }

    if(!$clienteObj){
        $clienteObj=Onix::ObtenerDatosDesdeChasis($VhObj->chasis);
    }

    $noAtender=noAtender::obtenerSiexiste($clienteObj->idcustomer,$clienteObj->idlocation,$clienteObj->idvehicle);

	if ($clienteObj) {
		$cantidadObjeto=Onix::obtenerCantididadVehiculos($clienteObj->idcustomer,$clienteObj->idlocation);
        $planobj=Onix::obtenerPlanDeServicio($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation);
        $vencimientoDatos=Onix::obtenerFechaDeVencimiento($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation);	
        $tiempo="";
        //var_dump($clienteObj);
  ?>


<div style="padding-top: 11px; padding-bottom: 10px;">
<div class="row">
    <div class="row">
    <?php if ($cantidadObjeto){ ?>
        <?php if ($cantidadObjeto->cantidad>1){ ?>
        <h4 class="text-center text-warning">ATENCION ! Cliente tiene <?php echo $cantidadObjeto->cantidad ?> Vehiculos en Tracklink</h4>	
        <?php } ?>
    <?php } ?>
    <div class="form-group col-md-3">
        <label for="Observaciones" >DNI/RUC</label>
        <input style="border: none;" type="text" placeholder="Web" id="Observaciones" <?php echo $retVal = (!empty($clienteObj->dni)) ? "" : "" ; ?> class="border-bottom form-control"  value="<?php echo $dniRuc = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>">
        <input type="hidden" value="<?php echo $dniRuc ?>">
    </div>
    <div class="form-group col-md-9">
        <label for="numeroTecnico"  >Nombre de cliente*  <span class="text-danger"><?= ($operacionObj->trabajo=='Instalacion')?$cliObjR->razonSocial." ".$cliObjR->apellidosC:"" ?></span></label>
        <input  style="border: none;" type="text" name="nombreCliente"  id="numeroTecnico" class="border-bottom form-control" required value="<?php echo $nombres = (!empty($clienteObj->nombre)) ? "$clienteObj->apellidos"." "."$clienteObj->nombre" : "$clienteObj->apellidos" ; ?> ">
        <input type="hidden" value="<?php echo $nombres ?>">
    </div>
    </div>
<div class="row">
    <div class="form-group col-md-3">
        <label for="numeroTecnico" >Placa Vehiculo*</label>
        <input  style="border: none;"  type="text" placeholder="Placa" id="numeroTecnico" name="placa" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->placa)) ? "$clienteObj->placa" : "" ; ?>"  >
    </div>
<div class="form-group col-md-3">
    <label for="Observaciones"  >Marca *</label>
    <input required  style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->marca)) ? "$clienteObj->marca" : "" ; ?>" >
</div>
<div class="form-group col-md-3">
    <label for="Observaciones" >Modelo*</label>
    <input required  style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($clienteObj->modelo)) ? "$clienteObj->modelo" : "" ; ?>">
</div>
<div class="form-group col-md-3">
    <label for="color"  >Color</label>
    <input  style="border: none;"  type="text" placeholder="Color" id="color" class="border-bottom form-control" name="modelo"  value="<?php echo $retVal = (!empty($clienteObj->color)) ? "$clienteObj->color" : "" ; ?>">
</div>	
</div>
<div class="row">
<div class="form-group col-md-6">
    <label for="Observaciones" >Chasis*</label>
    <input required   style="border: none;" type="text" placeholder="Chasis" id="Observaciones" class="border-bottom form-control" name="chasis" value="<?php echo $retVal = (!empty($clienteObj->chasis)) ? "$clienteObj->chasis" : "$chasis" ; ?>">
</div>
<div class="form-group col-md-6">
    <label for="Observaciones" >Motor</label>
    <input  style="border: none;"  type="text" placeholder="Observaciones" id="Observaciones" class="border-bottom form-control" name="motor" value="<?php echo $retVal = (!empty($clienteObj->motor)) ? "$clienteObj->motor" : "Actualizar" ; ?>">
</div>
</div>
    <div class="row well">
        <div class="form-group col-md-6" >
            <label for="Observaciones"  >Observaciones Onyx </label>
            <div style="border-right: solid;height: 90px;
overflow-y: auto;">
            <label class="border-" ><?php echo utf8_encode($clienteObj->comentarios) ?></label>
            </div>
        </div>
        <div class="col-md-6 form-group"  >
        <label for="">Ordenes de Vehículo</label>
    <div style="height: 90px;
overflow-y: auto;">
            <?php $objOrdenes=Onix::obtenerOrdenesVehiculo($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation) ?>
            <?php foreach ($objOrdenes as $var): ?>
                <?= $orden=$var->idlocation."-".$var->idorder ?> <?= utf8_encode($var->articulo) ?><br>
                    <?php   $DataOrd=Onix::obtenerDatosFacturacion($orden) ?>
                    <?php   $DataOrt=Onix::obtenerORTE($orden) ?>
                    <?= ($DataOrd)?$DataOrd->serie.$DataOrd->numeroFactura."  $".round($DataOrd->precioO*1.18,2):""; ?>
                    <?= ($DataOrt->orteO!='')?" ORTE ".$DataOrt->orteO." ":'' ?> <small><?= haceMinimoAnio($var->fechaOrden) ?></small>
                    <hr>
            <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
</div>
<?php }else{ ?>
<div class="" style="padding-top: 11px; padding-bottom: 10px;">
<div class="row">
    <div class="row">
        <div class="form-group col-md-12">
            <label for="numeroTecnico" >Nombre de cliente*</label>
            <input  style="border: none;" type="text" name="nombreCliente"  id="numeroTecnico" class="border-bottom form-control" required>
        </div>
    </div>
<div class="row">
    <div class="form-group col-md-3">
        <label for="numeroTecnico" >Placa Vehiculo*</label>
        <input  style="border: none;"  type="text" placeholder="Placa" id="numeroTecnico" name="placa" class="border-bottom form-control" required  >
    </div>
    <div class="form-group col-md-3">
        <label for="Observaciones"  >Marca *</label>
        <input required  style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"   >
    </div>
    <div class="form-group col-md-3">
        <label for="Observaciones" >Modelo*</label>
        <input required  style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="modelo" class="border-bottom form-control"   >
    </div>
    <div class="form-group col-md-3">
        <label for="color"  >Color</label>
        <input  style="border: none;"  type="text" placeholder="Color" id="color" class="border-bottom form-control" name="modelo"  >
    </div>	
</div>
<div class="row">
    <div class="form-group col-md-6">
        <label for="Observaciones" >Chasis*</label>
        <input required   style="border: none;" type="text"  <?= ($VhObj->campana=="Toyota")?"readonly":""?>  placeholder="Chasis" id="Observaciones" class="border-bottom form-control" name="chasis" >
    </div>
    <div class="form-group col-md-6">
        <label for="Observaciones" >Motor</label>
        <input  style="border: none;"  type="text" placeholder="Observaciones" id="Observaciones" class="border-bottom form-control" name="motor" >
    </div>
</div>
</div>
</div>         
<?php } ?>
</head>
<link href="../../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
<div class="row ">
<div class='col-md-12  text-center'>
<?php
    if (!$datosInstala) {
       $usuCem=Usuario::obtenerUsuario($operacionObj->idusuarioActivador); 
    }
?>
    <label class="text-warning text-center"> Registrado en CEM por <?= ($datosInstala) ? $datosInstala->operador:$usuCem->usuarioNombre." ".$usuCem->usuarioApellidoPaterno ?> </label>
    <br>

    <?php
    if (!$cheObj) {
    ?>
    <label class="text-success text-center">PLAN <?= $VhObj->plan ?> -  Canpaña   <?= $VhObj->campana ?></label>
    <?php
    }else{
    ?>
    <label class="text-success  text-center">PLAN <?= $cheObj->plan ?> </label>
    <?php
        }
    ?>
</div>
</div>
  <div class="row">
    <div class="form-group col-md-4">
      <label for="trabajo" class="">Trabajo</label>
      <input style="border: none;" readonly  type="text" placeholder="Trabajo" id="trabajo" name="trabajo" class="trabajo border-bottom form-control" required value="<?= $operacionObj->trabajo ?>">
    </div>
    <input type="hidden" name="idop" value="<?php echo $operacionObj->idoperacion ?>" >

<input type="hidden" name="dniOruc" value="<?= $clienteObj->dni ?>" >
<input type="hidden" name="idlocation" value="<?= $clienteObj->idlocation ?>" >
<input type="hidden" name="idcustomer" value="<?= $clienteObj->idcustomer ?>" >
<input type="hidden" name="idvehicle" value="<?= $clienteObj->idvehicle ?>" >
<input type="hidden" name="lastname" value="<?= utf8_encode($clienteObj->apellidos) ?>" >

    <input type="hidden" name="fechaInstalacionOLD" value="<?= substr($operacionObj->fechaInstalacion, 0,10)  ?>" >
<?php
    if ($VhObj) {
       ?>
    <input type="hidden" name="idve" value="<?php echo $VhObj->idvehiculo ?>" >
       <?php 
    }
?>
    <input type="hidden" name="idTecnicoAsignado" value="<?php echo $operacionObj->idTecnicoAsignado ?>" >
    <div class="form-group col-md-4">
      <label for="tecnicoInstalador" class="">Tecnico <?= ($datosInstala) ? "(<small> ".$datosInstala->tecnicoInstalador."</small>)" : "" ?></label>
      <select  style="border: none;" class="mi-selector tecnico border-bottom form-control text-capitalize" required name="tecnicoInstalador" id="tecnicoInstalador" required="">
      <?php if ($operacionObj->idTecnicoAsignado==0 AND $operacionObj->idTecnicoInstalador=='0'){ ?>
        <option value="" disabled="" selected="">Seleccione tecnico</option>
             <?php Usuario::obtenerTecnicos() ?> 
        <?php }else if($operacionObj->idTecnicoAsignado!=0 AND $operacionObj->idTecnicoInstalador=='0'){ ?>
        <option class=" text-capitalize form-control" value="<?php echo $operacionObj->idTecnicoAsignado ?>" selected ><?php $usuarioTecnico=Usuario::obtenerUsuario($operacionObj->idTecnicoAsignado);  echo $usuarioTecnico->usuarioNombre;echo "&nbsp;"; echo $usuarioTecnico->usuarioApellidoPaterno;?></option>
          <?php Usuario::obtenerTecnicos() ?>   
        <?php }else if($operacionObj->idTecnicoAsignado==0 AND $operacionObj->idTecnicoInstalador!='0'){ ?>
        <option class=" text-capitalize form-control" value="<?php echo $operacionObj->idTecnicoInstalador ?>" selected ><?php $usuarioTecnico=Usuario::obtenerUsuario($operacionObj->idTecnicoInstalador);  echo $usuarioTecnico->usuarioNombre;echo "&nbsp;"; echo $usuarioTecnico->usuarioApellidoPaterno;?></option>
          <?php Usuario::obtenerTecnicos() ?>   
        <?php }else{ ?>
        <option class=" text-capitalize form-control" value="<?php echo $operacionObj->idTecnicoAsignado ?>" selected ><?php $usuarioTecnico=Usuario::obtenerUsuario($operacionObj->idTecnicoAsignado);  echo $usuarioTecnico->usuarioNombre;echo "&nbsp;"; echo $usuarioTecnico->usuarioApellidoPaterno;?></option>
          <?php Usuario::obtenerTecnicos() ?>   
      <?php } ?>
      </select>
    </div>
    <div class="form-group col-md-4">
      <label for="orte" class="">Orte</label>
      <input style="border: none;" type="text" placeholder="Numero de Orte " id="orte" name="orte" class="border-bottom form-control x2 "  value="<?php echo $operacionObj->numeroOrte ?>">
    </div>
  </div> 
  <hr>
  <h4>Datos de <?= strtolower($operacionObj->trabajo) ?></h4>
<?php if ($operacionObj->estado=='36' OR $operacionObj->estado=='99' OR $operacionObj->estado=='15') { ?>
<?php if ($operacionObj->trabajo!=='Desinstalación'){ ?>  
  <div class="row">
    <div class="form-group col-md-4">
      <label for="imei" class="">IMEI</label>
      <input style="border: none;" type="text" placeholder="IMEI" id="imei" name="imei" class="border-bottom form-control" required autocomplete='off' value="<?= ($datosInstala) ? $datosInstala->imei : $operacionObj->imei ?>">
    </div>
    <div class="form-group col-md-4">
      <label for="id" class="">ID (unitID)</label>
      <input style="border: none;" type="text" placeholder="ID" id="id" name="id" class="border-bottom form-control " required autocomplete='off' value="<?= ($datosInstala) ? $datosInstala->id : $operacionObj->id ; ?>">
    </div>
    <div class="form-group col-md-4">
      <label for="odometro" class="">Odometro</label>
      <input style="border: none;" type="text" placeholder="Odometro" id="odometro" name="odometro" class="border-bottom form-control " required value="<?= ($datosInstala) ? $datosInstala->odo: $operacionObj->odometro ?>">
    </div>
  </div>
  <div class="row">          
    <div class="form-group col-md-3">
      <label for="ubicacionSistema" class="">Ubicación de sistema</label>
      <input style="border: none;" type="text" placeholder="Ubicación de sistema" id="ubicacionSistema" name="ubicacionSistema" class="border-bottom form-control" required value="<?= ($datosInstala) ? $datosInstala->ubicacionSistema : $operacionObj->ubicacionSistema ?>">
    </div>
    <div class="form-group col-md-3">
      <label for="lugarInstalacion" class="">Lugar</label>
      <input style="border: none;" type="text" placeholder="Lugar instalación/Chequeo" id="lugarInstalacion" name="lugarInstalacion" class="border-bottom form-control" value="<?= ($operacionObj->lugarInstalacion) ? strtoupper($operacionObj->lugarInstalacion) : '' ; ?> <?= (($operacionObj->nombreConcecionarioInstalicion)) ? ''.$operacionObj->nombreConcecionarioInstalicion : ''; ?>">
    </div>
<?php
    if ($datosInstala) {
       ?>
     <div class="form-group col-md-3">
     <label for="fechaInstalacion" class="">Fecha <?= strtolower($operacionObj->trabajo) ?>.</label>
      <input style="border: none;" type="text" placeholder="Fecha instalación" id="fechaInstalacion" name="fechaInstalacion" class="border-bottom form-control" value="<?= formatoSinHoraAniOPrimerodia($datosInstala->fecha)  ?>" >
    </div>      
       <?php 
    }else{
        ?>
    <div class="form-group col-md-3">
     <label for="fechaInstalacion" class="">Fecha <?= strtolower($operacionObj->trabajo) ?>.</label>
      <input style="border: none;" type="text" placeholder="Fecha instalación" id="fechaInstalacion" name="fechaInstalacion" class="border-bottom form-control" value="<?= substr($operacionObj->fechaInstalacion, 0,10)  ?>" >
    </div>        
        <?php
    }
?>
    
     <div class="form-group col-md-3">
     <label for="horalInstalacion" class="">Hora <?= strtolower($operacionObj->trabajo) ?>.</label>
      <input style="border: none;" type="text" readonly  placeholder="Fecha instalación" id="horalInstalacion" name="horalInstalacion" class="border-bottom form-control" value="<?= substr($operacionObj->fechaInstalacion, 11,18) ?>" >
    </div>
  </div>
            <div class="row">
                <div class="form-group col-md-5">
        <h3>
         Estado
        </h3>
                    <?php  $estV=0;
                    if($verificaPeticionActivacion){
                       $estV =$verificaPeticionActivacion->estadoVinculacion;
                    }
                    ?>
        <div class="checkbox-primary">
          <input id="checkbox2" type="radio" required <?= ($VhObj->campana=="Toyota")?"disabled":""?>  name="estado" value="activado" <?= ($operacionObj->estado==15) ? 'checked':'' ?>>
          <label for="checkbox2">
          Activado

              <?php
    if ($datosInstala) {
    if ($datosInstala->horaActivacion!='') {
        echo formato($datosInstala->horaActivacion);
       ?>
<input type="hidden" name="horaActivacion" value="<?= $datosInstala->horaActivacion ?>" >
       <?php 
    }
    }
?>
          </label>
        </div>

        <div class="checkbox-warning">
        <input id="checkbox1" type="radio"  <?= ($VhObj->campana=="Toyota" && $operacionObj->estado==15 )?"disabled":""?>  required  name="estado" value="pendiente" <?= ($operacionObj->estado==99 || ($VhObj->campana=="Toyota" && $operacionObj->estado!=15 )) ? 'checked':'' ?> >
          <label for="checkbox1">
              <?= ($VhObj->campana=="Toyota")?"Instalado":"pendiente"?>
          </label>
        </div>
                </div>

    <?php
    if ($VhObj) {
    ?>
                <div class="form-group col-md-5">
                    <div class="checkbox checkbox-success <?php  if($noAtender || strpos($VhObj->campana, 'metros') !== false){ echo 'checked';} ?>">
                        <input id="modo2" type="checkbox" <?php  if($noAtender || strpos($VhObj->campana, 'metros') !== false){ echo 'checked';} ?>  name="modo2" value="si">
                        <label for="modo2">
                           <strong>MODO 2</strong>
                        </label>
                    </div>
                </div>

      <?php 
      }else{
      ?>
                <div class="form-group col-md-5">
                    <div class="checkbox checkbox-success <?php  if($noAtender){ echo 'checked';} ?>">
                        <input id="modo2" type="checkbox" <?php  if($noAtender){ echo 'checked';} ?>  name="modo2" value="si">
                        <label for="modo2">
                           <strong>MODO 2</strong>
                        </label>
                    </div>
                </div>
      <?php
          }
      ?>

            </div>

<?php
    if (!empty($operacionObj->observacion)) {
       ?>
<div class='row'>
    <label class='text-warning'>Observaciones: <?= $operacionObj->observacion ?> </label>
</div>          
       <?php 
    }
?>
<?php
    if (!empty($datosInstala->observaciones)) {
       ?>
<div class='row'>
    <label class='text-warning'>Observaciones: <?= $datosInstala->observaciones ?> </label>
</div>          
       <?php 
    }
?>

  <div class="row">
<div class="form-group col-md-9">
      <label for="observacion" class="">Observaciones CEM</label>
      <input style="border: none;" type="text" placeholder="Observaciones" id="observacion" name="observacion" class="border-bottom form-control" >
    </div>
<div class="form-group col-md-3">
      <label for="coordenadas" class="">Coordenadas</label>
      <input style="border: none;" type="text" placeholder="Coordenadas" id="coordenadas" name="coordenadas" class="border-bottom form-control" required value="<?= ($datosInstala) ? $datosInstala->cordenadas : $operacionObj->coordenadas ?>">
    </div>
  </div>

  <?php if ((strpos($operacionObj->trabajo, 'Che') !== false) ): ?>
  <!--
<div class="row" >
    <div class="form-group col-md-12">
         <label class="font-normal" for='motivos'>Motivos indicado por el técnico: </label>
                <?php 
                    $motivoArr = array();
                    foreach(Motivos::getMotivosForClass("Chequeos",$idoperacion) as $mt){
                         array_push($motivoArr,$mt->motivo_id);
                    }
                ?>
            <div>
                <select data-placeholder="Elige los motivos del chequeo" class="chosen-select form-control" required id="motivos" name="motivos[]"  multiple style="width:350px;" tabindex="4">
                <?php foreach (Motivos::getMotivosByType("CHEQUEO") as $var): ?>
                    <option value="<?= $var->id ?>" <?= in_array($var->id,$motivoArr)?'selected':'' ?>  ><?=  $var->motivo ?></option>
                <?php endforeach ; ?>
                </select>
                </div>
                </div>
                <small class="tex-danger">Debe completar este campo</small>
  </div>
  -->
  <?php endif ?>

<?php
    if($dataLog=DatosInstalacion::obnerActualizacionesDeLOG($idoperacion)){
   ?>
<div class="row">
<h2>Ultimas Modificaciones</h2>
<table class="table" >
<thead>
<tr>
<th>Usuario</th>
<th>Comentarios</th>
<th>Fecha</th>
</tr>
</thead>
<tbody>
<?php
 foreach($dataLog as $var) :
$o=Usuario::obtenerUsuario($var->idusuario)
?>
<tr>
<td><?=  $o->usuarioNombre." ".$o->usuarioApellidoPaterno ?></td>
<td><?= $var->observaciones ?></td>
<td><?= formato($var->fechaRegistro) ?></td>
</tr>
<?php
    endforeach;
?>
</tbody>
</table>
  </div> 
   <?php 
    }
?>
       <fieldset>
        <h3>
         Pruebas realizadas
        </h3>
        <div class="checkbox checkbox-info">
          <input id="checkbox4" type="checkbox" name="bloqueoDesbloqueo" value="si">
          <label for="checkbox4">
            Bloqueo / Desbloqueo
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox9" type="checkbox" name="panico" value="si">
          <label for="checkbox9">
            Boton de panico
          </label>
        </div>      
        <div class="checkbox checkbox-info">
          <input id="checkbox5" type="checkbox" name="pestillos" value="si">
          <label for="checkbox5">
           Apertura de pestillos
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox6" type="checkbox" name="sensorCombustible" value="si">
          <label for="checkbox6">
            Sensor de combustible
          </label>
        </div>
         <div class="checkbox checkbox-info">
          <input id="checkbox7" type="checkbox" name="kit" value="si">
          <label for="checkbox7">
            Kit habla escucha
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox8" type="checkbox" name="alertaSonora" value="si">
          <label for="checkbox8">
           Alerta sonora por exeso de velocidad
          </label>
        </div>
      </fieldset>
<?php }else{
  ?>
<input type="hidden" name="estado" value="desinstalacion" >
  <div class="row">          
    <div class="form-group col-md-12">
      <label for="ubicacionSistema" class="">Ubicación de sistema</label>
      <input style="border: none;" type="text" placeholder="Ubicacion de sistema" id="ubicacionSistema" name="ubicacionSistema" class="border-bottom form-control" required value="<?php echo $retVal = (isset($operacionObj->ubicacionSistema)) ? $operacionObj->ubicacionSistema : "" ; ?>">
      </div>
    </div>
<?php
    if (!empty($operacionObj->observacion)) {
       ?>
<div class='row'>
    <label class='text-warning'>Observaciones: <?= $operacionObj->observacion ?> </label>
</div>  
       <?php 
    }
?>
<h2>
          Desinstalado<?php
if ($datosInstala) {
    if ($datosInstala->horaActivacion!='') {
        echo formato($datosInstala->horaActivacion);
       ?>
<input type="hidden" name="horaActivacion" value="<?= $datosInstala->horaActivacion ?>" >
       <?php 
    }
}
?>
</h2>
<div class="row">
<div class="form-group col-md-4">
      <label for="lugarInstalacion" class="">Lugar</label>
      <input style="border: none;" type="text" placeholder="Lugar instalación/Chequeo" id="lugarInstalacion" name="lugarInstalacion" class="border-bottom form-control" value="<?= (isset($operacionObj->lugarInstalacion)) ? strtoupper($operacionObj->lugarInstalacion) : '' ; ?> <?= (isset($operacionObj->nombreConcecionarioInstalicion)) ? ''.$operacionObj->nombreConcecionarioInstalicion : ''; ?>">
    </div>
    </div>
  <div class="row">
<?php
    if ($datosInstala) {
       ?>
     <div class="form-group col-md-3">
     <label for="fechaInstalacion" class="">Fecha <?= strtolower($operacionObj->trabajo) ?>.</label>
      <input style="border: none;" type="text" placeholder="Fecha instalación" id="fechaInstalacion" name="fechaInstalacion" class="border-bottom form-control" value="<?= formatoSinHoraAniOPrimerodia($datosInstala->fecha)  ?>" >
    </div>      
       <?php 
    }else{
        ?>
    <div class="form-group col-md-3">
     <label for="fechaInstalacion" class="">Fecha <?= strtolower($operacionObj->trabajo) ?>.</label>
      <input style="border: none;" type="text" placeholder="Fecha instalación" id="fechaInstalacion" name="fechaInstalacion" class="border-bottom form-control" value="<?= substr($operacionObj->fechaInstalacion, 0,10)  ?>" >
    </div>        
        <?php
    }
?>
     <div class="form-group col-md-3">
     <label for="horalInstalacion" class="">Hora <?= strtolower($operacionObj->trabajo) ?>.</label>
      <input style="border: none;" type="text" placeholder="Fecha instalación" id="horalInstalacion" name="horalInstalacion" class="border-bottom form-control" value="<?= substr($operacionObj->fechaInstalacion, 11,18) ?>" >
    </div>
    <div class="form-group col-md-6">
      <label for="observacion" class="">Observaciones CEM</label>
      <input required style="border: none;" type="text" placeholder="Observaciones" id="observacion" name="observacion" class="border-bottom form-control" >
    </div>
  </div>

  
<?php
    if($dataLog=DatosInstalacion::obnerActualizacionesDeLOG($idoperacion)){
   ?>
<div class="row">
<h2>Ultimas Modificaciones</h2>
<table class="table" >
<thead>
<tr>
<th>Usuario</th>
<th>Comentarios</th>
<th>Fecha</th>
</tr>
</thead>
<tbody>
<?php
 foreach($dataLog as $var) :
$o=Usuario::obtenerUsuario($var->idusuario)
?>
<tr>
<td><?=  $o->usuarioNombre." ".$o->usuarioApellidoPaterno ?></td>
<td><?= $var->observaciones ?></td>
<td><?= formato($var->fechaRegistro) ?></td>
</tr>
<?php
    endforeach;
?>
</tbody>
</table>
  </div> 
   <?php 
    }
?>
  <?php
  } ?>
  <?php
  }
?>
<input type="hidden" name="idop" value="<?php echo $operacionObj->idoperacion ?>" >
<input type="hidden" name="datosFacturacionId" value="<?php echo $operacionObj->datosfacturacionid ?>" >
<input type="hidden" name="ordenContrato" value="<?php echo $operacionObj->ordenContrato ?>" >
<input type="hidden" name="idTecnicoAsignado" value="<?php echo $operacionObj->idTecnicoAsignado ?>">
<?php
    if ($cheObj) {
       ?>
<input type="hidden" name="plan" value="<?= $cheObj->plan ?>" >
       <?php 
    }else{
   ?>
<input type="hidden" name="plan" value="<?= $VhObj->plan ?>" >
   <?php 
    }
?>
<script src="../../assets/js/jquery-2.1.1.js"></script>
    <script src="../../assets/js/bootstrap.js"></script>
<script src="../../assets/build/jquery.datetimepicker.full.js"></script>
<script src="../../assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="../../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<script src="../../assets/js/plugins/chosen/chosen.jquery.js"></script>
    <script src="../../assets/js/plugins/select2/select2.full.min.js"></script>

 <script>

    $(document).ready(function() {
        $('.mi-selector').select2({
            dropdownParent: $('#bateriasOModal')
        });
    })
        $('.chosen-select').chosen( {
        width: "100%",
        no_results_text: "Oops, no se encontro motivo!"
        });

$('.chosen-select').change(function(){
    console.log($(this).val())
});
$.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" })
    $("#instalacionForm").validate({
    }); 
<?php
    if (!empty($operacionObj->fechaInstalacion)) {
       ?>
  $('#imei').typeahead({
    source: [
    {"name": "861020036"},
    {"name": "8687890218"}
    ]
  });
  $('#id').typeahead({
    source: [
    {"name": "11D1228000020"},
    ]
  });
jQuery.datetimepicker.setLocale('es');
$('#fechaInstalacion').datetimepicker({
  format:'Y/m/d',
  timepicker:false,
  scrollMonth : false,
  scrollInput: false
});
$('#horalInstalacion').datetimepicker({
    datepicker:false,
    format:'H:i',
    allowTimes:['9:00','11:00','14:00','16:00']
});
       <?php 
    }
?>
</script>          
<?php
}
else if($_POST['rowid'] =='Add'){
	include_once("../../includes/init.php");
    ?>
<link href="../../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="../../assets/css/style2.css"/>

<div class="row">
    <div class="form-group col-md-12">
        <label for="numeroOrte" >Número ORTE</label>
        <input   style="border: none;" type="text" name="numeroOrte"  id="numeroOrte" class="border-bottom form-control" required autofocus='on'>
    </div>
</div>
<div class="" style="padding-top: 11px; padding-bottom: 10px;">
<div id='resultadoConsultaOnyx' ></div>
</div>       
<div id="mostrar">
<div class="row">
    <div class="form-group col-md-4">
    <label for="fechaInstalacion" class="">Fecha.</label>
    <input style="border: none;" type="text" placeholder="Fecha instalación" id="fechaInstalacion" name="fechaInstalacion" class="border-bottom form-control" value="<?= date('Y/m/d') ?>" >
</div>      
<div class="form-group col-md-4">
<label for="trabajo" class="">Trabajo</label>
<select  style="border: none;" class="trabajo border-bottom form-control text-capitalize" name="trabajo" required="">
    <option value="" disabled="" selected >Seleccione tecnico</option>
    <option value="Instalacion" >Instalacion</option>
    <option value="Desinstalacion" >Desinstalacion</option>
    <option value="Chequeo por monitoreo" >Chequeo por monitoreo</option>
    <option value="Chequeo de rutina" >Chequeo de rutina</option>
    <option value="Reinstalacion" >Reinstalación</option>
    </select>
</div>
<input type="hidden" name="agregarTEMP" value="tmp" >
<div class="form-group col-md-4">
    <label for="tecnicoInstalador" class="">Tecnico</label>
    <select  style="border: none;" class="mi-selector tecnico border-bottom form-control text-capitalize" required name="tecnicoInstalador" required="">
    <option  value="" disabled="" selected="">Seleccione tecnico</option>
    <?php Usuario::obtenerTecnicos() ?> 
    </select>
</div>
</div>
<div class="row">
<div class="form-group col-md-4">
    <label for="imei" class="">IMEI</label>
    <input style="border: none;" type="text" placeholder="IMEI" id="imei" name="imei" class="border-bottom form-control" required autocomplete='off' >
</div>
<div class="form-group col-md-4">
    <label for="id" class="">ID</label>
    <input style="border: none;" type="text" placeholder="ID" id="id" name="id" class="border-bottom form-control " required autocomplete='off' >
</div>
<div class="form-group col-md-4">
    <label for="odometro" class="">Odometro</label>
    <input style="border: none;" type="text" placeholder="Odometro" id="odometro" name="odometro" class="border-bottom form-control " required >
</div>
</div>
<div class="row">          
<div class="form-group col-md-4">
    <label for="ubicacionSistema" class="">Ubicación de sistema</label>
    <input style="border: none;" type="text" placeholder="Ubicación de sistema" id="ubicacionSistema" name="ubicacionSistema" class="border-bottom form-control" required >
</div>
<div class="form-group col-md-4">
    <label for="lugarInstalacion" class="">Lugar</label>
    <input style="border: none;" type="text" placeholder="Lugar instalación/Chequeo" id="lugarInstalacion" name="lugarInstalacion" class="border-bottom form-control" required >
</div>

<div class="form-group col-md-4">
      <label for="coordenadas" class="">Coordenadas</label>
      <input style="border: none;" type="text" placeholder="Coordenadas" id="coordenadas" name="coordenadas" class="border-bottom form-control" required value="<?php echo $retVal = (isset($operacionObj->coordenadas)) ? $operacionObj->coordenadas : "" ; ?>">
    </div>
</div>
<h3>
    Estado
</h3>
<div class="checkbox-primary">
        <input id="checkbox2" type="radio" required  name="estado" value="activado">
        <label for="checkbox2">
        Activado</label>
    </div>
<div class="checkbox-warning">
<input id="checkbox1" type="radio" required  name="estado" value="pendiente">
    <label for="checkbox1">
    Pendiente
    </label>
</div>
<div class="row">
<div class="form-group col-md-12">
      <label for="observacion" class="">Observaciones CEM</label>
      <input style="border: none;" type="text" placeholder="Observaciones" id="observacion" name="observacion" class="border-bottom form-control" >
    </div>
  </div>
<fieldset>
        <h3>
         Pruebas realizadas
        </h3>
        <div class="checkbox checkbox-info">
          <input id="checkbox4" type="checkbox" name="bloqueoDesbloqueo" value="si">
          <label for="checkbox4">
            Bloqueo / Desbloqueo
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox9" type="checkbox" name="panico" value="si">
          <label for="checkbox9">
            Boton de panico
          </label>
        </div>      
        <div class="checkbox checkbox-info">
          <input id="checkbox5" type="checkbox" name="pestillos" value="si">
          <label for="checkbox5">
           Apertura de pestillos
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox6" type="checkbox" name="sensorCombustible" value="si">
          <label for="checkbox6">
            Sensor de combustible
          </label>
        </div>
         <div class="checkbox checkbox-info">
          <input id="checkbox7" type="checkbox" name="kit" value="si">
          <label for="checkbox7">
            Kit habla escucha
          </label>
        </div>
        <div class="checkbox checkbox-info">
          <input id="checkbox8" type="checkbox" name="alertaSonora" value="si">
          <label for="checkbox8">
           Alerta sonora por exeso de velocidad
          </label>
        </div>
      </fieldset>
</div>
<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/build/jquery.datetimepicker.full.js"></script>
<script src="../../assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="../../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>

    <script src="../../assets/js/plugins/select2/select2.full.min.js"></script>
<script>
    $(document).ready(function() {
        $('.mi-selector').select2({
            dropdownParent: $('#bateriasOModal')
        });
    })
    $("#instalacionForm").validate({

    }); 
  $('#imei').typeahead({
    source: [
    {"name": "861020036"},
    {"name": "8687890218"}
    ]
  });
  $('#id').typeahead({
    source: [
    {"name": "11D1228000020"},
    ]
  });
jQuery.datetimepicker.setLocale('es');
$('#fechaInstalacion').datetimepicker({
  format:'Y/m/d',
  timepicker:false,
  scrollMonth : false,
  scrollInput: false
});
$('#mostrar').hide();
    $("#numeroOrte").on("keyup change",function(){
        var dni = $(this).val();
        if(dni.length >= 4)
        {		
            $("#resultadoConsultaOnyx").html('<div class="row" > <div class="col-md-12 text-center"><i class="fa fa-spinner fa-spin fa-5x fa-fw text-center"></i> </div></div>');
            $.ajax({
                type : 'POST',
                url  : '../ajax/dataDeOrte',
                data : $(this).serialize(),
                success : function(data)
                {
                    $("#resultadoConsultaOnyx").html(data);
                }
            });
            $('#mostrar').show();
            return false;
        }
        else
        {
            $('#mostrar').hide();
            $("#resultadoConsultaOnyx").html('');
        }			
    });
</script>
    <?php
}
else if($_POST['rowid'] =='rerTemporales'){
	include_once("../../includes/init.php");
    $datosTMP=DatosInstalacion::listadeTemporales();
    if($datosTMP){
        ?>
<div class="table-responsive">
<table class="table table-condensed  table-hover" >
            <thead>
                <tr>                   
                    <th>Orte</th>
                    <th>Lugar</th>
                    <th>Trabajo</th>
                    <th>Cliente</th>
                    <th>Fecha Programada</th>
                    <th>IMEI</th>
                    <th>SIM</th>
                    <th>Ejecutivo CEM</th>
                    <th>Fecha Registro</th>
                    <th>Estado</th>
                </tr>
            </thead>  
            <tbody>          
        <?php
        foreach($datosTMP as $var ):
       ?>
    <tr>           
        <td><?= $var->orte ?> </td>
        <td><?= $var->lugarInstalacion ?> </td>
        <td><?= $var->servicio ?> </td>
        <td><?= $var->cliente ?> </td>
        <td><?= formatoSinHora($var->fecha) ?> </td>
        <td><?= $var->imei ?> </td>
        <td><?= $var->id ?> </td>
        <td><?= $var->operador ?> </td>
        <td><?= haceMinimo($var->fechaRegistro) ?> </td>
        <td><?= obtenerEstado($var->estado) ?></td>
        <!-- <td><button name="eliminarTMP241" class='btn btn-sm btn-danger' id="eliminar" value="<?= $var->iddatosInstalacion ?>"> Eliminar Temporal</td> -->
    </tr>
       <?php 
        endforeach;
        ?>
            </tbody>  
        </table>
   </div> 
        <?php
        }else{
        ?>
           No se encontraron temporales, si tienes alguna duda contactate con pvasquez@tracklink.pe 
        <?php
    }
    ?>
    <?php
}
?>

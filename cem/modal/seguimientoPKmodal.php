<?php
    if (isset($_POST['idsegpk'])) {
    include_once("../../includes/init.php");
    $idSegPk=limpiar(trim($_POST['idsegpk']));

    $objSgPK=new SeguimientoPK();

    $varTI = $objSgPK->findSeguimientoId($idSegPk);
    if(!$varTI->idsuariocargo)
        $varTI->asignarCargo();
    ?>
    <link href="../assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../jquery.datetimepicker.css"/>
    <div class="showOPERA">
        <div class="well" style="padding-top: 11px;padding-bottom: 10px; ">
                    <?php if ($varTI): ?>
                    <?php if (!is_null($varTI->idsuariocargo)) {
                        $usuarioObj = Usuario::obtenerUsuario($varTI->idsuariocargo);
                    } ?>

                        <div class="col-md-12 text-center"><label  class='text-success' for="">A Cargo de : <?php if(!is_null($varTI->idsuariocargo)){echo $usuarioObj->usuarioNombre.' '.$usuarioObj->usuarioApellidoPaterno;}else{ echo 'Libre';}?></label></divas>
                        </div>
            <br><br>
                        <div class="row">
                            <div class="form-group col-md-3">
                              <label for="ntipo" >Tipo</label>
                              <input name="ntipo" style="border: none;" type="text" placeholder="Tipo Gestión" id="ntipo" readonly class="border-bottom form-control"  value="<?= $varTI->type ?>">
                            </div>
                            <div class="form-group col-md-2">
                              <label for="npoliza" >Póliza</label>
                              <input name="npoliza" style="border: none;" type="text" placeholder="Tipo Gestión" id="npoliza" readonly class="border-bottom form-control"  value="<?= $varTI->policy ?>">
                            </div>
                            <div class="form-group col-md-5">
                              <?php $clit=utf8_encode($varTI->cliente) ?>
                              <input type="hidden" id='idgestion' name="idgestion" value="<?= $idSegPk ?>">
                              <input type="hidden" id='nidtrama' name="nidtrama" value="<?= $varTI->idtrama ?>">
                              <input type="hidden" id='oldetiqueta' name="oldetiqueta" value="<?= $varTI->etiqueta ?>">
                              <label for="nclient" >Cliente :</label>
                              <input  style="border: none;" type="text" readonly id="nclient" name="nclient" class="border-bottom form-control" required value="<?= $clit ?>">
                            </div>
                            <div class="col-md-2"><label for="ncantnoti" >Fecha Instalación</label>
                              <input style="border: none;" type="text" readonly name="nfechaI" id="nfechaI"  class="border-bottom form-control"  value="<?= $varTI->fechaI ?>" >
                            </div>
                        </div>

                        <div class="row">
                        <div class="form-group col-md-3">
                            <label for="nchasis" >chasis :</label>
                            <input  style="border: none;" type="text" id="nchasis" name="nchasis" class="border-bottom form-control" required value="<?= $varTI->chasis ?>">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="nplaca" >Placa :</label>
                            <input  style="border: none;" type="text"  id="nplaca" name="nplaca" class="border-bottom form-control" required value="<?= $varTI->placa ?>">
                        </div>
                        <div class="col-md-7"><label for="nObservacion" >Observación</label>
                            <textarea style="border: none;"  readonly name="nObservacion" id="nObservacion"  class="border-bottom form-control"> <?= $varTI->observacion ?> </textarea>
                        </div>
                        </div>
                         <div class="row">
                             <div class="col-md-3"><label for="netiqueta" >Etiqueta</label>

                                 <select style="border: none;margin-bottom: 15px" class="border-bottom form-control text-capitalize" required id="netiqueta" name="netiqueta" aria-required="true">
                                     <?php if ($varTI->etiqueta !=2 or $varTI->etiqueta!=4){
                                     $obtest = SeguimientoPK::obtenerListEtiqueta("");
                                     if($obtest):
                                       foreach ($obtest as $var) :
                                         ?>
                                         <option class="text-capitalize badge-<?=$var->color?>" value="<?php echo $var->id ?>" <?php echo ($var->id==$varTI->etiqueta)?"selected":"";?> ><?= $var->descripcion?></option>
                                     <?php
                                       endforeach;
                                     else:
                                        echo '<option value="" >-- Seleccionar --</option>';
                                     endif;
                                     }else{
                                         if ($varTI->etiqueta==2)
                                           echo '<option value="2">Póliza Anulada</option>';
                                         elseif ($varTI->etiqueta==4)
                                           echo '<option value="4" >Sincronizado</option>';
                                     }

                                     ?></select>
                             </div>
                             <div class="col-md-3"><label for="nestado" >Estado</label>
                                 <select style="border: none;margin-bottom: 15px" class="border-bottom form-control text-capitalize" required=""  id="nestado" name="nestado" aria-required="true">
                                     <option value="0" <?= $varTI->estado==0?"selected":""; ?> >Pendiente</option>
                                     <option value="8"  <?= $varTI->estado==8?"selected":""; ?>>Seguimiento</option>
                                     <option value="15" <?= $varTI->estado==15?"selected":""; ?>>Realizado</option>
                                 </select>
                             </div>
                             <div class="col-md-3">
                                 <div class="checkbox checkbox-success  <?php  if( $varTI->visita_presencial== 1){ echo 'checked';} ?>">
                                     <input id="npresencial" type="checkbox"  <?php  if($varTI->visita_presencial== 1){ echo 'checked';} ?>  name="npresencial" value="si">
                                     <label for="npresencial">
                                         <strong>Visita Presencial</strong>
                                     </label>
                                 </div>
                             </div>
                         </div>
        </div>
        <div class="showTipo">
            <div class="row">
                <die class="form-group col-md-12">
                    <label for="seguimientoTrama" class="">Seguimiento</label>
<div class="input-group">
                <input type="text" class="form-control" placeholder="Seguimiento" name="nseguimiento" id="nseguimiento" >
                <span class="input-group-btn">
                    <button type="button" id="registrarSeguiminto" class="btn btn-primary">Registrar Seguimiento </button>
                </span>
            </div>
                </die>
            </div>
            <ul id='segimientohtml'>
                <?php foreach(SeguimientoPK::getSEg($idSegPk) as $var ): ?>
                    <li><?= $var->fecha ?> <b><?= $var->body ?></b></li>
                <?php endforeach ?>
            </ul>
            <hr>

            <div class="">
                <span>Seguimiento General</span>
            </div>
            <ul id='segimientohtml'>
                <?php foreach(SeguimientoPK::findSeguimientoByPolicy($varTI->policy) as $var ): ?>
                    <li><?= $var->fecha ?> <b><?= $var->body ?></b></li>
                <?php endforeach ?>
            </ul>

        </div>
        <div class="modal-footer">
            <button type="button" id='cancelarSeguimiento' class="btn btn-white" data-dismiss="modal">Cancelar</button>
            <button type="submit" name="guardarSeguimientoPK" id="guardarSeguimientoPK" class="btn btn-primary" value="<?php echo Token::generar() ?>">Guardar</button>
        </div>
    </div>
        <script src="../../assets/bootbox/bootbox.all.js"></script>

        <script>
            $("#registrarSeguiminto").click(function(){
                var body=$('#nseguimiento').val()
                var idsegpk = $("#idgestion").val()
                var today = new Date();
                var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()+' '+today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                if(body.length>0){
                    $.ajax({
                        type : 'POST',
                        url  : 'ajax/registrarResSegPk',
                        data : {body:body,idsegpk:idsegpk},
                        success : function(data)
                        {
                            $('#nseguimiento').val('')
                            $("#segimientohtml").append('<li> '+date+' <b>'+body+'</b></li>');
                        }
                    });
                }
                return false;
            });

            $(document).ready(function () {
                $('#guardarSeguimientoPK').click(function() {
                    var check =$("#netiqueta option:selected").val();
                    console.log(check)
                    if(check==0) {
                        bootbox.alert({
                            title:"<h3 class='text-danger'><strong>Alerta!</strong></h3>",
                            message: "<label>Debe Seleccionar una Etiqueta</label>",
                            size: 'small',
                            callback: function () {
                                /*  l.ladda( 'stop' );*/
                            }
                        });
                        console.log("Seleccionar Etiqueta*");
                        return false;
                    }
                })
            })
        </script>
<?php endif ?>

<?php
}
?>

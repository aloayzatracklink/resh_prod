<?php $nav="seguimientoToyotaAnomalia" ;?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
	irA("ingresar");
}
?>

<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>

<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="../assets/css/sweetalert2.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/css2/style2.css">
<link rel="stylesheet" href="../assets/css2/style3.css">

<style>
    .table-cabecera-horario{
        background:#075598!important;
        color:white !important;
        vertical-align: middle!important;
    }
    .table-cabecera-horario2{
        background:#164764 !important;
        color:white !important;
        vertical-align: middle!important;
    }
    .table-cabecera-horario3{
        background: #096452 !important;
        color:white !important;
    }
    .table-cabecera-horario4{
        background: #0e2e79 !important;
        color:white !important;
    }

    .table-scroll {
        position:relative;
        max-width:1600px;
        margin:auto;
        overflow:hidden;
        /*border:.5px solid #000;*/
    }

    .table-wrap {
        width:100%;
        overflow:auto;
    }

    .table-scroll table {
        width:100%;
        margin:auto;
        border-collapse:separate;
        border-spacing:0;
    }

    .table-scroll th, .table-scroll td {
        padding:5px 10px;
        border-bottom:1px solid;
        border-right:1px solid;
        border-color: #DDDDDD;;
        background:#fff;
        color:rgb(103, 106, 108) ;
        white-space:nowrap;
        vertical-align:top;
        font-size: 12px
    }

    .table-scroll thead, .table-scroll tfoot {
        background: #f9f9f9;
    }

    .bordertab{
        border-bottom: 1px #e1d0d0 solid !important;
        border-left: 1px #e1d0d0 solid !important;
    }
</style>

<body>
    <link href="../assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
    <!-- Custom CSS -->
    <script src="../assets/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../assets/css/utilities.css">


    <div id="wrapper">
        <!-- Menu -->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <?php include("../includes/menuIzquierdo.php"); ?>
            </div>
        </nav>

        <!-- Main content -->
        <div id="page-wrapper" class="gray-bg">
            <!-- Menu responsive -->
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>
                </nav>
            </div>

            <section class="row p-2">
                <div class="col-md-12 white-bg p-2">
                    <h3>Seguimiento Toyota Anomalia</h3>
                </div>
            </section>
            <section class="row" style="margin-bottom: 2rem;">
                <div class="col-md-12 table bg-white">
                        <table class="table main-table" >
                                <thead>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"></th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Cliente</th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Placa</th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Chasis</th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Dias con anomalia</th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Servicio</th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Estado</th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Cant. Notify</th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Cargo</th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Acción</th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Seguimiento</th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Fecha Registro</th>
                                    <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Ultima Actualización</th>
                                </thead>
                        <tbody id="resultado">
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">
                                <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <p id="buscando" class="text-center"><strong>Buscando resultados ...</strong></p>
                </div>
            </section>
            <div class="row">
                <?php include("../includes/footer.php") ?>
            </div>
        </div>
    </div>

    <!-- Modal de Anomalias -->
    <div class="modal inmodal" id="anomaliaOModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceIn">
                <div class="modal-header" style="padding-top: 5px;padding-bottom: 2px">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                        <h4 class="modal-title">Gestión Toyota Anomalía</h4>
                        <small class="font-bold"><div class="form-group ">
                            </div></small>
                    </div>
                    <form  role="form" method="post" enctype="multipart/form-data" id="formOpe" name="formOpe">
                        <div class="modal-body" style="background: #fff;padding-top: 0">
                        </div>
                    </form>
                </div>
            </div>
    </div>
    
    <?php //include("../includes/footer.php") ?>
    <!-- Mainly scripts -->
    <script src="../assets/js/jquery-2.1.1.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../assets/js/plugins/clockpicker/clockpicker.js"></script>
    <script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <!-- Custom and plugin javascript -->
    <script src="../assets/js/inspinia.js"></script>
    <script src="../assets/js/plugins/pace/pace.min.js"></script>
    <script src="../assets/js/plugins/footable/footable.all.min.js"></script>
    <script src="../assets/js/plugins/select2/select2.full.min.js"></script>
    <script src="../assets/bootbox/bootbox.all.js"></script>

    <!-- Toastr -->
    <script src="../assets/js/plugins/toastr/toastr.min.js"></script>
    <script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="../assets/js/jquery-confirm.min.js"></script>

    <script src="../assets/js/plugins/ladda/spin.min.js"></script>
    <script src="../assets/js/plugins/ladda/ladda.min.js"></script>
    <script src="../assets/js/plugins/ladda/ladda.jquery.min.js"></script>
    <script src="../assets/js/plugins/toastr/toastr.min.js"></script>
    <?php include_once('../includes/script.php') ?>
    <script>
    $(document).ready(function() {
      

        $('#anomaliaOModal').on('show.bs.modal', function (e) {
            $('.modal-body').html("");

            var rowid = $(e.relatedTarget).data('idint');
            $.ajax({
                    type : 'post',
                    url : 'modal/seguimientoToyotaModalAnomalia',
                    data :  'idanom='+ rowid,
                    success : function(data){
                        $('.modal-body').html(data);
                    }
                });
        });

        $(document).ready(function(){
            $('.footable').footable();
            $('.footable2').footable();
        });


        $('body').on('hidden.bs.modal', function () {
            if($('.modal.in').length > 0)
            {
                $('body').addClass('modal-open');
            }
        });


       
        $.ajax({
            type : 'POST',
            url  : 'pag/resultadoBusquedaToyotaGestionAnomalia',
            data :  {datos:'',estd:'',sist:'',page:1},
            success : function(data)
            {
                $("#resultado").html(data);
                $("#buscando").hide();
            }
        });
    });
    </script>
</body>

</html>
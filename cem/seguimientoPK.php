<?php
    $nav="seguimientoPK";
    include("../includes/head.php");
    if (!$session->yaEstaIngresado()) {
        irA("../ingresar");
    }
    $obMenu=Usuario::obtenerMenu($session->idusuario);

if ($obMenu->instalaciones=="si" or $obMenu->administrarComercial=="si"):
        if (isset($_POST['guardarSeguimientoPK'])) {
            if(Token::verificar($_POST['guardarSeguimientoPK'])) {
                $nest = $_POST['nestado'];
                $idan = $_POST['idgestion'];
                $idEtiqueta="";
                $OldEtiqueta="";

                if (isset($_POST['netiqueta'])) {
                    $idEtiqueta=$_POST['netiqueta'];
                }
                $npresencial=0;
                if (isset($_POST['npresencial'])) {
                    if($_POST['npresencial']=="si")
                       $npresencial=1;
                }

                $objA = new SeguimientoPK();
                $objA->estado = $nest;
                $objA->idgesppk = $idan;
                $objA->etiqueta = $idEtiqueta;
                $objA->visita_presencial = $npresencial;

                $anm = $objA->updateSeguimientoEstado();


                if (isset($_POST['oldetiqueta'])) {
                    $OldEtiqueta=$_POST['oldetiqueta'];
                }

                if($OldEtiqueta != $idEtiqueta){
                    $ndes = $objA->obtenerListEtiqueta($idEtiqueta);
                    $ndat ="";
                    foreach ($ndes as $xx):
                      $ndat = array(
                         "idsegpk" => $idan,
                         "body" => "Se selecciono la etiqueta: ".$xx->descripcion
                      );
                    endforeach;
                    if($ndat!="")
                       $objA->saveSeg($ndat);
                }


                $objTrama = new TramaIntegracion();

                if (isset($_POST['nchasis'])) {
                    $dChasis=$_POST['nchasis'];
                }


                if (isset($_POST['nplaca'])) {
                    $dplaca=$_POST['nplaca'];
                }

                if (isset($_POST['nidtrama'])) {
                    $idtram=$_POST['nidtrama'];
                }



                $objTrama->idtrama = $idtram;
                $objTrama->placa = $dplaca;
                $objTrama->chasis = $dChasis;
                $objTrama->updateTrama2();

                if($anm)
                    $echo[]="Se Actualizó con exito ";
            } else {
                $errores[]="El formulario ya fue procesado, verificar.";
            }
        }
        else {
            if (isset($_POST['addEtiqueta'])) {
                if (Token::verificar($_POST['addEtiqueta'])) {
                    $tchk=0;
                    if (isset($_POST['idst'])) {
                        $tchk=$_POST['idst'];
                    }
                    for($ii=1; $ii <= count($tchk); $ii++) {

                        $nid=$tchk[($ii-1)];
                        $ncolo=$_POST['ncolor_'.$ii];

                        $objd = new SeguimientoPK();
                        $objd->id =$nid;
                        $objd->color = $ncolo;
                        $objd->updateEtiqueta();
                    }

                    $echo[] = "Etiqueta actualizado con exito";
                } else {
                    $errores[] = "El formulario ya fue procesado, vericar.";
                }
            }
        }
?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="../assets/css/sweetalert2.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/css2/style2.css">
<link rel="stylesheet" href="../assets/css2/style3.css">

<style>
    .table-cabecera-horario{
        background:#075598!important;
        color:white !important;
        vertical-align: middle!important;
    }
    .table-cabecera-horario2{
        background:#164764 !important;
        color:white !important;
        vertical-align: middle!important;
    }
    .table-cabecera-horario3{
        background: #096452 !important;
        color:white !important;
    }
    .table-cabecera-horario4{
        background: #0e2e79 !important;
        color:white !important;
    }

    .table-scroll {
        position:relative;
        max-width:1600px;
        margin:auto;
        overflow:hidden;
        /*border:.5px solid #000;*/
    }

    .table-wrap {
        width:100%;
        overflow:auto;
    }

    .table-scroll table {
        width:100%;
        margin:auto;
        border-collapse:separate;
        border-spacing:0;
    }

    .table-scroll th, .table-scroll td {
        padding:5px 10px;
        border-bottom:1px solid;
        border-right:1px solid;
        border-color: #DDDDDD;;
        #background:#fff;
        color:rgb(103, 106, 108) ;
        white-space:nowrap;
        vertical-align:top;
        font-size: 12px
    }

    .table-scroll thead, .table-scroll tfoot {
        background: #f9f9f9;
    }

    .bordertab{
        border-bottom: 1px #e1d0d0 solid !important;
        border-left: 1px #e1d0d0 solid !important;
    }
</style>
<body>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <?php include("../includes/menuIzquierdo.php") ?>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../includes/navegacionArriba.php") ?>
            </nav>
        </div>
        <div class="wrapper wrapper-content">
            <div class="row ibox float-e-margins ibox-content">
                <div class="col-md-12" style="padding: 0">
                    <div id="table-scroll" class="table-scroll">
                        <div class="table-wrap">
                            <div class="col-md-6"></div>
                            <div class="col-md-2">
                                <label for="cboTipo">tipo</label>
                                <select style="border: none;margin-bottom: 15px" class="border-bottom form-control text-capitalize" required="" id="cboTipo" name="cboTipo" aria-required="true">
                                    <option value="">Todos</option>
                                    <option value="KM CERO">KM CERO</option>
                                    <option value="NO ACTIVADO">NO ACTIVADO</option>
                                    <option value="KM NO VARIA">KM NO VARIA</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="cboEstado">Estado</label>
                                <select style="border: none;margin-bottom: 15px" class="border-bottom form-control text-capitalize" required=""  id="cboEstado" name="cboEstado" aria-required="true">
                                    <option value="">Todos</option>
                                    <option value="0">Pendiente</option>
                                    <option value="8">Seguimiento</option>
                                    <option value="15">Realizado</option>
                                </select>
                            </div>
                            <?php if($session->idusuario==2 or $session->idusuario==47){?>
                            <div class="col-md-2">
                                <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#addEtiquetaSegPk">Agregar Etiqueta <i class="fa fa-save"></i></button>
                            </div>
                            <?php }?>
                            <form action="PHPExcel/SeguimientoPKExcel" method="POST">
                                <div class="col-md-2" id="resultado1">
                                    <button class="btn btn-primary" name="Descargar" id="resultado1">Descargar en Excel</button>
                                </div>
                            </form>

                            <input type="text" class="form-control input-sm m-b-xs" id="filterrr"
                                   placeholder="Buscar" style="margin-bottom: 15px">
                            <table class="table main-table" >
                                <thead>
                                    <tr>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"></th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Tipo de Gestión</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Póliza</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Cliente</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Placa</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Chasis</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Fecha Instalación</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Km App</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Estado</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Etiqueta</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Cargo</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Acción</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Observación</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Seguimiento</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Fecha Registro</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Ultima Actualización</th>
                                    </tr>
                                </thead>
                                <tbody id="resultado">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("../includes/footer.php") ?>
    </div>
</div>


<div class="modal inmodal" id="seguimientoOModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 5px;padding-bottom: 2px">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Seguimiento PK</h4>
                <small class="font-bold"><div class="form-group">
                    </div></small>
            </div>
            <form  role="form" method="post" enctype="multipart/form-data" id="formOpe" name="formOpe">
                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>
            </form>
        </div>
    </div>
</div>



<div class="modal inmodal" id="addEtiquetaSegPk" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom:0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Agregar Etiqueta</h4>
            </div>
            <form  role="form" method="post"  enctype="multipart/form-data" name="regEst" id="regEst">
                <input  type="text" name="btnAddEtiqueta" id="btnAddEtiqueta" disabled value="" style="display: none">
                <div class="modal-body2" style="background: #fff;padding:0 15px"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                    <button type="submit" name="addEtiqueta" id="addEtiqueta"  class="btn btn-primary btn-modal" value="<?php echo Token::generar() ?>" >Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/plugins/clockpicker/clockpicker.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/select2/select2.full.min.js"></script>
<script src="../assets/bootbox/bootbox.all.js"></script>

<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="../assets/js/jquery-confirm.min.js"></script>

<script src="../assets/js/plugins/ladda/spin.min.js"></script>
<script src="../assets/js/plugins/ladda/ladda.min.js"></script>
<script src="../assets/js/plugins/ladda/ladda.jquery.min.js"></script>
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>

<script src="../assets/bootbox/bootbox.all.js"></script>
<style>
    .popover {
        z-index: 999999;
    }
</style>
<?php include_once('../includes/script.php') ?>
<script>

    <?php if (!empty($errores)): ?>
    <?php foreach ($errores as $error): ?>
    toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
    <?php endforeach ?>
    <?php endif ?>
    <?php if(!empty($echo)): ?>
    <?php foreach($echo as $ok): ?>
    toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
    <?php endforeach ?>
    <?php endif ?>
</script>
<script>


    $('#addEtiquetaSegPk').on('show.bs.modal', function (e) {
        $(".modal-body2").html("");
        $.ajax({
            type : 'post',
            url : 'modal/addEtiquetaSegPk',
            data :  "",
            success : function(data){
                $('.modal-body2').html(data);
            }
        });
    });

    $('#seguimientoOModal').on('show.bs.modal', function (e) {
        $('.modal-body').html("");
        var rowid = $(e.relatedTarget).data('idint');
        $.ajax({
            type : 'post',
            url : 'modal/seguimientoPKmodal',
            data :  'idsegpk='+ rowid,
            success : function(data){
                $('.modal-body').html(data);
            }
        });
    });

    $(document).ready(function(){
        $('.footable').footable();
        $('.footable2').footable();
    });


    $('body').on('hidden.bs.modal', function () {
        if($('.modal.in').length > 0)
        {
            $('body').addClass('modal-open');
        }
    });


    $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
    $.ajax({
        type : 'POST',
        url  : 'pag/resultadoBusquedaSeguimientoPK',
        data :  {datos:'',estd:'',tipo:'',page:1},
        success : function(data)
        {
            $("#resultado").html(data);
        }
    });


    $("#cboTipo").change(function() {
        $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
        var datosS = $("#filterrr").val();
        datosS = datosS.replace('<>/"', '');
        var estd=$("#cboEstado option:selected").val()
        var tipo=$("#cboTipo option:selected").val()
        $.ajax({
            type : 'POST',
            url  : 'pag/resultadoBusquedaSeguimientoPK',
            data :  {datos:datosS,estd:estd,tipo:tipo},
            success : function(data)
            {
                $("#resultado").html(data);
            }
        });
        return false;

    })

    $("#cboEstado").change(function() {
        $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
        var datosS = $("#filterrr").val();
        datosS = datosS.replace('<>/"', '');
        var estd=$("#cboEstado option:selected").val()
        var tipo=$("#cboTipo option:selected").val()
        $.ajax({
            type : 'POST',
            url  : 'pag/resultadoBusquedaSeguimientoPK',
            data :  {datos:datosS,estd:estd,tipo:tipo},
            success : function(data)
            {
                $("#resultado").html(data);
            }
        });
        return false;
    })


    $("#filterrr").keypress(function(e)
    {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){
            $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
            var datosS = $(this).val();
            datosS = datosS.replace('<>/"', '');
            var estd=$("#cboEstado option:selected").val()
            var tipo=$("#cboTipo option:selected").val()
            $.ajax({
                type : 'POST',
                url  : 'pag/resultadoBusquedaSeguimientoPK',
                data :  {datos:datosS,estd:estd,tipo:tipo},
                success : function(data)
                {
                    $("#resultado").html(data);
                }
            });
            return false;
        }
    });



</script>
<?php else: ?>
    <?php $session->salir();
    echo "NO NO NO NO ; presiona F5"
    ?>
<?php endif ?>

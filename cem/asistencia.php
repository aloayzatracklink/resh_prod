<?php $nav="asistencia"; ?>
<?php include("../includes/head.php") ?>
<?php 
$errores=[];
if (!$session->yaEstaIngresado()) { irA("../ingresar"); } 
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->choques=="si"): ?>
<?php
$choqueObj=new Choque();
if (isset($_POST['inicio'])) {
  require_once '../mailphp/PHPMailer/PHPMailerAutoload.php';
  $errores=array();
  if (empty($_POST['chasis']) OR !isset($_POST['chasis'])) {
    echo $errores[]="Error registre correctamente el chasis";
  }
    $chasis=$_POST['chasis'];
    if ($choqueObj) {
      $clienteObj=Onix::ObtenerDatosDesdeChasis($chasis);
      // var_dump($clienteObj);
      if ($clienteObj) {
        $choqueObj->placaChoque=$clienteObj->placa;
        $choqueObj->marca=$clienteObj->marca;
        $choqueObj->modelo=$clienteObj->modelo;
        $apellidos=utf8_encode($clienteObj->apellidos);
        $propietario = (!empty($clienteObj->nombre)) ? utf8_encode($clienteObj->nombre)." ".$apellidos : $apellidos ;
        $choqueObj->propietario=$propietario;
        $choqueObj->idcliente=$clienteObj->dni;
        $correosCliente=$clienteObj->correo;
        $choqueObj->vence=$clienteObj->hasta;
        if (!empty($clienteObj->correo2))
          $correosCliente.=",".$clienteObj->correo2;
      }else{
        $errores[]="Chasis no existe en la base de datos de Onyx, verifica e intente nuevamente.";
      }
//para el correo here //        
//
if (empty($errores)) {
$placa=$choqueObj->placaChoque;
$nombreCliente=$choqueObj->propietario;
$marca=$choqueObj->marca;
$modelo=$choqueObj->modelo;
$choqueObj->chasis=$chasis;
if ($choqueObj->registrarAsistencia()){  
  $usuario=Usuario::obtenerUsuario($session->idusuario);  $usuario->usuarioNombre; $usuario->usuarioApellidoPaterno;  
      /////Aqui se envia el correo ///
  $thehtml="<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<meta name='viewport' content='width=device-width'>
<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<title></title><style type='text/css' id='media-query'>
body {
margin: 0;
padding: 0; }
table, tr, td {
vertical-align: top;
border-collapse: collapse; }
.ie-browser table, .mso-container table {
table-layout: fixed; }
* {
line-height: inherit; }
a[x-apple-data-detectors=true] {
color: inherit !important;
text-decoration: none !important; }
[owa] .img-container div, [owa] .img-container button {
display: block !important; }
[owa] .fullwidth button {
width: 100% !important; }
.ie-browser .col, [owa] .block-grid .col {
display: table-cell;
float: none !important;
vertical-align: top; }
.ie-browser .num12, .ie-browser .block-grid, [owa] .num12, [owa] .block-grid {
width: 500px !important; }
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
line-height: 100%; }
.ie-browser .mixed-two-up .num4, [owa] .mixed-two-up .num4 {
width: 164px !important; }
.ie-browser .mixed-two-up .num8, [owa] .mixed-two-up .num8 {
width: 328px !important; }
.ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {
width: 250px !important; }
.ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {
width: 166px !important; }
.ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {
width: 125px !important; }
.ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {
width: 100px !important; }
.ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {
width: 83px !important; }
.ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {
width: 71px !important; }
.ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {
width: 62px !important; }
.ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {
width: 55px !important; }
.ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {
width: 50px !important; }
.ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {
width: 45px !important; }
.ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {
width: 41px !important; }
@media only screen and (min-width: 520px) {
.block-grid {
width: 500px !important; }
.block-grid .col {
display: table-cell;
Float: none !important;
vertical-align: top; }
.block-grid .col.num12 {
width: 500px !important; }
.block-grid.mixed-two-up .col.num4 {
width: 164px !important; }
.block-grid.mixed-two-up .col.num8 {
width: 328px !important; }
.block-grid.two-up .col {
width: 250px !important; }
.block-grid.three-up .col {
width: 166px !important; }
.block-grid.four-up .col {
width: 125px !important; }
.block-grid.five-up .col {
width: 100px !important; }
.block-grid.six-up .col {
width: 83px !important; }
.block-grid.seven-up .col {
width: 71px !important; }
.block-grid.eight-up .col {
width: 62px !important; }
.block-grid.nine-up .col {
width: 55px !important; }
.block-grid.ten-up .col {
width: 50px !important; }
.block-grid.eleven-up .col {
width: 45px !important; }
.block-grid.twelve-up .col {
width: 41px !important; } }
@media (max-width: 520px) {
.block-grid, .col {
min-width: 320px !important;
max-width: 100% !important; }
.block-grid {
width: calc(100% - 40px) !important; }
.col {
width: 100% !important; }
.col > div {
margin: 0 auto; }
img.fullwidth {
max-width: 100% !important; } }
</style>
</head>
<body class='clean-body' style='margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #FFFFFF'>
<div class='nl-container' style='min-width: 320px;Margin: 0 auto;background-color: #FFFFFF'>
<div style='background-color:#003399;'>
  <div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
    <div style='border-collapse: collapse;display: table;width: 100%;'>
      <div class='col num12' style='min-width: 320px;max-width: 500px;width: 500px;width: calc(18000% - 89500px);background-color: transparent;'>
        <div style='background-color: transparent; width: 100% !important;'>
          <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 5px; padding-left: 5px;'>
            <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
              <div align='center'><div style='border-top: 10px solid transparent; width:100%; line-height:0px;'>&nbsp;</div></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>    
<div style='background-color:#e7e7e7;'>
<div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid two-up'>
  <div style='border-collapse: collapse;display: table;width: 100%;'>
    <div class='col num6' style='Float: left;max-width: 320px;min-width: 250px;width: 250px;width: calc(35250px - 7000%);background-color: transparent;'>
      <div style='background-color: transparent; width: 100% !important;'>
        <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 5px; padding-left: 5px;'>
          <div align='center' class='img-container center' style='padding-right: 0px;  padding-left: 0px;'>
            <a href='http://www.tracklink.pe/TracklinkPeru/' target='_blank'>
              <img class='center' align='center' border='0' src='cid:logo' alt='Image' title='Tracklink' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;width: 100%;max-width: 217px' width='217'>
            </a>
          </div>
        </div>
      </div>
    </div>
<div class='col num6' style='Float: left;max-width: 320px;min-width: 250px;width: 250px;width: calc(35250px - 7000%);background-color: transparent;'>
  <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:15px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;'>
      <div align='center' class='img-container center' style='padding-right: 0px;  padding-left: 0px;'>
      </div>
    </div>
  </div>
</div>
  </div>
</div>
</div>    
<div style='background-color:#EDEDED;'>
<div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
  <div style='border-collapse: collapse;display: table;width: 100%;'>

    <div class='col num12' style='min-width: 320px;max-width: 500px;width: 500px;width: calc(18000% - 89500px);background-color: transparent;'>
      <div style='background-color: transparent; width: 100% !important;'>
        <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'>
          <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
           <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px'>Estimados Central de emergencia se registró asistencia. <br><br>Cliente:&nbsp;<strong>$nombreCliente.</strong></p></div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div> 

<div style='background-color:transparent;'>
  <div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid mixed-two-up'>
    <div style='border-collapse: collapse;display: table;width: 100%;'>

      <div class='col num4' style='Float: left;max-width: 320px;min-width: 164px;width: 167px;width: calc(78164px - 15600%);background-color: transparent;'>
        <div style='background-color: transparent; width: 100% !important;'>
          <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 5px; padding-left: 5px;'>
            <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
             <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px'><strong><span style='font-size: 14px; line-height: 16px;'>Placa:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$placa</span></strong></p></div>
           </div>             
           <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
             <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px'><strong><span style='font-size: 14px; line-height: 16px;'>Marca: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$marca</span></strong></p></div>
           </div>            
           <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
             <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px'><strong><span style='font-size: 14px; line-height: 16px;'>Modelo:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$modelo</span></strong></p></div>
           </div>             
           <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
             <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px'><strong><span style='font-size: 14px; line-height: 16px;'>Chasis:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$chasis</span></strong></p></div>
           </div>       
         </div>
       </div>
     </div>

 </div>
</div>
</div>    

</div> 

<div style='background-color:#e7e7e7;'>
<div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
<div style='border-collapse: collapse;display: table;width: 100%;'>
</div>
</div>
</div>   
</div>
</body>
</html>";
//El correo para el cliente aqui /// <---->
$mail = new PHPMailer;
$mail->isSMTP();
$mail->SetLanguage( 'es', 'mailphp/PHPMailer/language/' );
$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->Host = gethostbyname('mail.tracklink.pe');
$mail->Port = 25;
$mail->SMTPAuth = false;
$mail->CharSet = 'UTF-8';
$mail->setFrom('no-responder@tracklink.pe', 'Resh de Tracklink');  
// $mail->addAddress("ldelgado@tracklink.pe","Luis"); // aquien se manda el mensaje
// $mail->addAddress("eaponte@tracklink.pe","Enrique"); // aquien se manda el mensaje
//$mail->addAddress("pvasquez@tracklink.pe","Pabel"); // aquien se manda el mensaje
$mail->addAddress("centralemergencia@tracklink.pe","CEM"); // aquien se manda el mensaje
// $mail->addAddress("drodriguez@tracklink.pe","Daniel Rodriguez"); // aquien se manda el mensaje
// $mail->addAddress("rmiranda@tracklink.pe","Renzo Miranda Quiñones"); // aquien se manda el mensaje
// $mail->addAddress("aquichiz@tracklink.pe","Andres Quichiz"); // aquien se manda el mensaje
// $mail->addCC("pruebas-ti@tracklink.pe");
$mail->addBCC("pruebas-ti@tracklink.pe");
//Set the subject line
$mail->Subject = 'Asistencia al cliente:'.ucwords(strtolower($nombreCliente)).' placa '.$placa ." "; //// ASUNTO //////
//Read an HTML message body from an external file, convert referenced images to embedded,
$mail->AddEmbeddedImage('../assets/img/track_new.png', 'logo');    
// $mail->AddEmbeddedImage('img/tracklink.png', 'tracklink');
//convert HTML into a basic plain-text alternative body
$mail->msgHTML($thehtml);
//Replace the plain text body with one created manually
$mail->AltBody = 'Estimado(as) '.$nombreCliente.' hemos registrado una asistencia '.$placa." ".' seguimiento por '.$usuario->usuarioNombre." "; ////////ESTE SE EL CONTENIDO//// QUE SE BERA EN PRIERA INSTANACIA
//Attach an image file
  //aqui se adjunta los archivos que queremos enviar :D

// $mail->addAttachment('images/phpmailer_mini.png');

//send the message, check for errors
if (!$mail->send()) {
  echo "Mailer Error: " . $mail->ErrorInfo;
 } else {
    // echo "Correo Enviado";
    $url="int/seguimientoAsistencia.php?id=$choqueObj->idasistencia";
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>'; exit;
  //   header("Location:detalleRobo.php?id=$robo->idrobo");
  // echo "enviado";
 }

///Aqui se envia el correo ///


}else{
$errores[]='Error en registrar choque';
}
}
}
}
?>
<body>

  <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
      <div class="sidebar-collapse">
        <?php include("../includes/menuIzquierdo.php") ?>
      </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
          <?php include("../includes/navegacionArriba.php") ?>

        </nav>
      </div>
      <div class="wrapper wrapper-content animated fadeInRight">

       <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>Nueva asistencia</h5>
              <div class="ibox-tools">
                <a class="collapse-link">
                  <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <i class="fa fa-wrench"></i>
                </a>
                <a class="close-link">
                  <i class="fa fa-times"></i>
                </a>
              </div>
            </div>
            <div class="ibox-content">
              <form role="form" class="" method="post" >
                <div class="row">
                  <div class="form-group col-md-4">
                    <label for="chasis" class="sr-only">Chasis del vehículo</label>
                    <input type="text" placeholder="Chasis del vehículo" id="chasis" name="chasis" class="form-control text-uppercase" autofocus required >
                  </div> 
                  <div class="col-md-2">
                    <button class="btn btn-success" type="submit" name="inicio">Iniciar asistencia</button>
                  </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Ultimas asistenciass</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="fa fa-wrench"></i>
            </a>
            <a class="close-link">
              <i class="fa fa-times"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content">
          <input type="text" class="form-control input-sm m-b-xs" id="filter"
          placeholder="Buscar">

          <table class="table table-hover footable " data-page-size="8" data-filter=#filter>
            <thead>
              <tr>
                <th>#</th>
                <th>Placa</th>
                <th>Propietario</th>
                <th>Operador</th>
                <th>Estado</th>
                <th>Fecha</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody>
              <?php $choqueData=$choqueObj->obtenerAsistencias() ?>
              <?php $n=1 ?>
              <?php if ($choqueData): ?>
                <?php foreach ($choqueData as $var): ?>
                    <tr>
                      <td><?php echo $n++ ?></td>
                      <td><?php echo  $var->placaAsistencia ?></td>
                      <td><?php echo $var->propietario ?></td>
                        <td><?php $usuario=Usuario::obtenerUsuario($var->idusuario);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno;  ?>&nbsp;</td>
                      </td>
                      <td><?php echo obtenerEstado($var->estado) ?></td>
                      <td><?php echo haceMinimo($var->fechaRegistro) ?></td>
                      <td>
                  <td class="center"><a href="int/detalleAsistencia.php?id=<?php echo $var->idasistencia ?>" class="btn btn-success btn-xs">Ver</a>&nbsp;<a href="int/seguimientoAsistencia.php?id=<?php echo $var->idasistencia ?>" class="btn btn-info btn-xs">Seguir</a></td>
                    </tr>
                <?php endforeach ?>
              <?php endif ?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="5">
                  <ul class="pagination pull-right"></ul>
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>







</div>
<?php include("../includes/footer.php") ?>

</div>
</div>

<div class="small-chat-box fadeInRight animated">

  <div class="heading" draggable="true">
    <small class="chat-date pull-right">
      <?php echo $horaActual=date("h:i"); ?>
    </small>
    chat
  </div>

  <div class="content">

  </div>

  <div class="form-chat">
    <div class="input-group input-group-sm">
      <textarea class="hablar form-control" placeholder="Shift+Enter para saltar espacio"></textarea>
      <span class="input-group-btn"><button
        class="btn btn-primary">Enviar
      </button> </span>
    </div>
  </div>
</form>
</div>
<div id="small-chat">
  <span class="badge badge-warning pull-right">5</span>
  <a class="open-small-chat">
    <i class="fa fa-comments"></i>
  </a>    
</div>

<!-- Mainly scripts -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<!-- chat -->
<script src="../assets/js/chat.js"></script>
<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
 <?php include_once('../includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
</script>
<script>
  $(document).ready(function(){       
    $('.footable').footable();
    // $('.footable2').footable();

  });
</script>

</body>

</html>
<?php else: ?> 
  <?php $session->salir();
  echo "NO NO NO NO ; presiona F5"
  ?> 
<?php endif ?>

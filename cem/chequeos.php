<?php $nav="chequeos" ;?>
<?php include("../includes/head.php") ?>
<link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">
<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
} ?>
<?php 
require_once '../includes/tablaProgramacion.php';
        $echo=array();
        $errores=array();
        $pendiente='';
        $cheuqoObj=new Chequeos;
        $opeRacionObj=new Operacion;        
        $objtablaP=new tablaProgramacion();
    if (isset($_POST['inicio'])) {
        $chasis=limpiar(trim($_POST['chasis']));
        if(!$d=$cheuqoObj->validarExistenciaPendienteChasis($chasis)){
            $clienteObj=Onix::ObtenerDatosDesdeChasis($chasis);           
      
        if ($clienteObj) {            
            $planobj=Onix::obtenerPlanDeServicio($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation);
            
            $cheuqoObj->dniRuc=$clienteObj->dni;
            $apellidos=utf8_encode($clienteObj->apellidos);
            $cheuqoObj->nombres=$retVal = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ;
            $cheuqoObj->placa=$clienteObj->placa;
            $cheuqoObj->marcaModelo=$clienteObj->marca." ".$clienteObj->modelo;
            $cheuqoObj->correo=$retVal = $clienteObj->correo2 .",".$clienteObj->correo2;
            $cheuqoObj->chasis=$chasis;            
            $cheuqoObj->celular=$clienteObj->celular;
            $cheuqoObj->telefono=$clienteObj->celular2." ".$clienteObj->telefono." ".$clienteObj->telefonoTrabajo;
            $cheuqoObj->plan=utf8_encode($planobj->articulo);
            $cheuqoObj->idcustomer=$clienteObj->idcustomer;
            $cheuqoObj->idvehicle=$clienteObj->idvehicle;
            $cheuqoObj->idlocation=$clienteObj->idlocation;
            if($cheuqoObj->registrarChequeoDesdeMonitoreo())
                $echo[]="Registrado con exito";
            else
                $errores[]="NO se pudo registrar";
        }else{
            $errores[]="No existe el chasis $chasis en la base de datos de Onyx o el cliente no esta activo";
        }
          }else{
            $errores[]="Chasis $chasis ya ha sido registrado";            
          }
    }

    if (isset($_POST['guardarCh'])) {
       // echo "<pre>";
       // die(var_dump($_POST));
        if (Token::verificar($_POST['guardarCh'])) {
            //var_dump($_POST);
           $opeRacionObj->trabajo=$_POST['tipoChequeo'];                
           $opeRacionObj->idChequeo=$_POST['idchequeo'];
           $opeRacionObj->idcliente=$_POST['idcustomer'];
           $opeRacionObj->idvehiculo=$_POST['idvehicle'];
           $opeRacionObj->idlocation=$_POST['idlocation'];
           $opeRacionObj->nombre=limpiar($_POST['nombres']);
           $opeRacionObj->placa= limpiar($_POST['placa']);    
           $opeRacionObj->observacion=htmlspecialchars($_POST['seguimientoRealizado']);
           if (isset($_POST['lugar'])) {
                  if ($_POST['lugar']=='Taller') {
                      $opeRacionObj->departamentoi=$_POST['depT'];
                      $opeRacionObj->provinciai=$_POST['proT'];
                      $opeRacionObj->distritoi=$_POST['disT'];
                  }else{
                      $opeRacionObj->departamentoi=$_POST['departamento'];
                      $opeRacionObj->provinciai=$_POST['provincia'];
                      $opeRacionObj->distritoi=$_POST['distrito'];                   
                  }
            }               
           if (isset($_POST['fechaIT']) AND isset($_POST['horaT'])) {
                $opeRacionObj->fechaInstalacion=$_POST['fechaIT']." ".$_POST['horaT'];
           }
           if ($_POST['tipoChequeo']=='Chequeo en linea') {
                if (isset($_POST['chequeonumeroOrden'])) {
                    $opeRacionObj->estado='777';
                    $opeRacionObj->numeroOrden= limpiar($_POST['chequeonumeroOrden']);
                }else{
                    $opeRacionObj->estado='14';
                    $opeRacionObj->lugarInstalacion="Crear Orden";
                    $opeRacionObj->fechaInstalacion=date('Y/m/d H:i');
                }
                    $cheuqoObj->tipoChequeo=$_POST['tipoChequeo'];
                    $cheuqoObj->idchequeo=$opeRacionObj->idChequeo;
                    $cheuqoObj->conclusion=limpiar($_POST['seguimientoRealizado']);
                    $cheuqoObj->coordenadas=limpiar($_POST['coordenadasChequeoEnLinea']);
                    if ($cheuqoObj->guardarComoChequeoEnLinea()) { }
                    $opeRacionObj->registrarChequeo();
           }else
            if($_POST['tipoChequeo']=='Reprogramar'){
                $cheuqoObj->estado='321';
                $idChequeo=limpiar($_POST['idchequeo']);
                $cheuqoObj->personaContacto=limpiar($_POST['pcont']);
                $cheuqoObj->numeroContacto=limpiar($_POST['nrcontacto']);
                $cheuqoObj->motivo=limpiar($_POST['motivo']);
                $cheuqoObj->tipoChequeo="Reprogramacion chequeo";
                $cheuqoObj->seguimientoRealizado=limpiar($_POST['seguimientoRealizado']);
                $fecha= str_replace("-", "", $_POST['fechaRepro']);
                $hora=str_replace(":", "", $_POST['horaRepro']);
                $cheuqoObj->fechaHoraChequeoLinea=$fecha.$hora;
                $cheuqoObj->idchequeo=$idChequeo;    
                if ($cheuqoObj->reprogramarChequeoEnLinea()) {
                    $echo[]="Se reprograma el chequeo para otro día";
                }else{
                    $errores[]="Algo malo ocurrio";
                }
           }else
            if($_POST['tipoChequeo']=='Cancelado'){
                $idChequeo=limpiar($_POST['idchequeo']);
                $idChequeo=limpiar($_POST['idchequeo']);
                $cheuqoObj->personaContacto=limpiar($_POST['pcont']);
                $cheuqoObj->numeroContacto=limpiar($_POST['nrcontacto']);
                $cheuqoObj->motivo=limpiar($_POST['motivo']);
                $cheuqoObj->correo=limpiar($_POST['correo']);
                $cheuqoObj->tipoChequeo="Cancelado chequeo";
                $cheuqoObj->seguimientoRealizado=limpiar($_POST['seguimientoRealizado']);       
                $cheuqoObj->idchequeo=$idChequeo;    
                if ($cheuqoObj->cancelarChequeoEnLinea()) {
                    $echo[]="Cancelado con exito";
                }else{
                    $errores[]="Algo malo ocurrio";
                }
           }else{
                    $cheuqoObj->tipoChequeo=$_POST['tipoChequeo'];
                    $cheuqoObj->ubicacionSistema=limpiar($_POST['ubicacionSistema']);
                    $cheuqoObj->seguimientoRealizado=htmlspecialchars($_POST['seguimientoRealizado']);
                    $cheuqoObj->instalador=htmlspecialchars($_POST['instalador']);
                    $cheuqoObj->direccion= htmlspecialchars($_POST['direccionInstalacion']);
                    if (isset($_POST['referenciaInstalacion'])) {
                        $cheuqoObj->referencia=htmlspecialchars($_POST['referenciaInstalacion']);
                    }
                    if (isset($_POST['coordenadasChequeoEnLineaLugar'])) {
                        $cheuqoObj->coordenadas=htmlspecialchars($_POST['coordenadasChequeoEnLineaLugar']);
                    }
                    $cheuqoObj->lugar=$_POST['lugar'];
                    $cheuqoObj->fechaInstalacion=limpiar($_POST['fechaInstalacion']);
                    $cheuqoObj->personaContacto=limpiar($_POST['pcont']);
                    $cheuqoObj->numeroContacto=limpiar($_POST['nrcontacto']);
                    $cheuqoObj->fechaChequeoEnSac=limpiar($_POST['fechaIT'].$_POST['horaT']);
                    $cheuqoObj->motivo=htmlspecialchars($_POST['motivo']);
                    $cheuqoObj->observacion=$_POST['tipoChequeo'];
                    $cheuqoObj->idchequeo=$opeRacionObj->idChequeo;
                    if ($cheuqoObj->guardarComoChequeoPorMonitoreo()) {
                        $echo[]=$_POST['tipoChequeo']." programado exitosamente .";
                    }else{
                        $errores[]="Error x3342432";
                    }
                    $opeRacionObj->estado='14';
                    $opeRacionObj->nombreContacto=$_POST['pcont'];
                    $opeRacionObj->numeroContacto=$_POST['nrcontacto'];
                    $opeRacionObj->numeroOrden='';
                    $objtablaP->fecha=$_POST['fechaIT'];
                    $objtablaP->hora=$_POST['horaT'];
                    $objtablaP->idtecnico=$_POST['posT'];
                    if (isset($_POST['lugar'])) {
                        $opeRacionObj->lugarInstalacion=$_POST['lugar'];
                        if ($_POST['lugar']=='Taller') {
                       // if (!$dataa=$objtablaP->obtenerTrabajosParaLafechayHoraYespacio()) {
                           // if (!$objtablaP->registrarTrabajoEntabla()) {
                          //      $errores[]="no se creo en tabla programaciokn ";
                            //}else{}
                      //  }else{

                            $objtablaP->iddistrito=$_POST['disT'];
                            $objtablaP->tth_instalacion="0".$_POST['canthoraprog'].":00";
                            $objtablaP->tth_trasalado='00:00';
                            $objtablaP->ubicacion='Taller';

                                //    $objtablaP->idturno=$turnoNew;
                                    if (!$objtablaP->registrarTrabajoEntabla()) {
                                        $errores[]="no se creo en tabla programaciokn ";
                                    }else{}

                               // if($turnoNew=$objtablaP->obtenerTrabajosParaLafechayHoraDisponibleTaller()){
                               //     $objtablaP->idturno=$turnoNew;
                               //     if (!$objtablaP->registrarTrabajoEntabla()) {
                               //         $errores[]="no se creo en tabla programaciokn ";
                               //     }else{}
                               // }else{
                               //     $errores[]="No se registro ninguna cita el horario que selecionaste ya esta ocupado";
                               // }

                          //  }
                            $cheuqoObj->departamentoc=$_POST['depT'];
                            $cheuqoObj->provinciac=$_POST['proT'];
                            $cheuqoObj->distritoc=$_POST['disT'];

                        }elseif($_POST['lugar']=='Domicilio Cliente'){
                            $cheuqoObj->departamentoc=$_POST['departamento'];
                            $cheuqoObj->provinciac=$_POST['provincia'];
                            $cheuqoObj->distritoc=$_POST['distrito'];

                            $objtablaP->iddistrito=$_POST['distrito'];
                            $objtablaP->tth_instalacion="0".$_POST['canthoraprog'].":00";
                            $objtablaP->tth_trasalado='00:00';
                            $objtablaP->ubicacion='Domicilio';
                            if (!$objtablaP->registrarTrabajoEntabla()) {
                                $errores[]="no se creo en tabla programaciokn ";
                            }else{}
                        }elseif($_POST['lugar']=='Concesionario'){

                            $objtablaP->iddistrito=$_POST['distrito'];
                            $objtablaP->tth_instalacion="0".$_POST['canthoraprog'].":00";
                            $objtablaP->tth_trasalado='00:00';
                            $objtablaP->ubicacion='Concesionario';
                            if (!$objtablaP->registrarTrabajoEntabla()) {
                                $errores[]="no se creo en tabla programaciokn ";
                            }else{}
                        }
                    }
                    $opeRacionObj->registrarChequeo();
                    if ($_POST['lugar']=='Taller') {
                        $objtablaP->idoperacion=$opeRacionObj->idoperacion;
                        $objtablaP->insertaridoperacion();       
                    }else{
                        $objtablaP->idoperacion=$opeRacionObj->idoperacion;
                        $objtablaP->insertaridoperacionDoble();       
                    }   
            }
    }else{
        $errores[]="El formulario ya fue procesado error Token..";
    }
    }

// if (isset($_POST['guardarComoPendiente'])) {
// }
// 153
// 8888

 ?>

<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->chequeos=="si"): ?>
<body>
    <link href="assets/css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">
    <link href="assets/css/plugins/footable/footable.core.css" rel="stylesheet">
        <link href="assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
               <?php include("../includes/menuIzquierdo.php") ?>
            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <?php include("../includes/navegacionArriba.php") ?>
        </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                     <div class="ibox float-e-margins col-lg-12">
                <div class="ibox-title">
                <div class="row">
                        <div class="col-md-6">&nbsp;&nbsp;&nbsp;<label for="">Programar Chequeos</label>   <a class="btn btn-primary" href="int/historialChequeos.php" style="padding: 0">Ver historial</a> </div>  
                        <div class="col-md-6">
                        <div class="row">
                <form action="" method="post">                    
                            <div class="col-md-6">                            
                            <label for="placa" class="sr-only">Chasis del vehículo</label>
                            <input type="text" placeholder="Chasis del vehículo" id="placa" name="chasis" class="form-control text-uppercase"  required >
                            </div>
                            <div class="col-md-6">
                                
                            <button class="btn btn-success" type="submit" name="inicio">Registrar chequeo</button>
                            </div>
                            </div>
                </form>
                        </div>
                    </div>
                </div>
                </div>
                <div class="ibox-content">
                    <input type="text" class="form-control input-sm m-b-xs" id="filter"
                    placeholder="Filtrar vehículos para chequeo" autofocus="">

                    <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                        <thead>
                            <tr>
                                <!-- <th>#</th> -->
                                <th>Placa</th>
                                <th>Cliente(s)</th> 
                                <th>Fecha hora chequeo</th>  
                                <th>Estado</th>
                                <th>Cargo</th>
                                <th>Enviado Por</th>
                                <th>Acción</th>
                            </tr>
                        </thead>    
                        <tbody id="chData">

                        </tbody>
                    </table>
                </div>
            </div>
            </div>
              <?php include("../includes/footer.php") ?>
            </div>
        </div>

            <!-- modal aqui desarrollaremos   -->
            <div class="modal inmodal" id="chequeo" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content animated bounceIn">
                        <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                            <h4 class="modal-title">Gestionar</h4>
                        </div>
                        <form  role="form" method="post" id='chequeosForm' >
                            <div class="modal-body" style="background: #fff;padding-top: 0">
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                <button type="submit" id="agregarVehiculoBttn" name="guardarCh" class="btn btn-primary" value="<?php echo Token::generar() ?>" >Realizado</button>
                                <div class="row">
                                    <div class="col-lg-12">              
                                      <label id="mostrar" class="text-center text-danger">Limite de taller alcanzado por favor intenta en otro horario</label>
                                    </div>
                                  </div>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>
        </div>
        </html>

    <script src="../assets/js/jquery-2.1.1.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../assets/build/jquery.datetimepicker.full.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="../assets/js/inspinia.js"></script>
    <script src="../assets/js/plugins/pace/pace.min.js"></script>
    <script src="../assets/js/plugins/footable/footable.all.min.js"></script>
    <script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
    
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
 <?php include_once('../includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
<script>
$("#chequeosForm").validate();
    $(document).ready(function(){
        $('.footable').footable();
        $('.footable2').footable();
    });
    $('#chequeo').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        $.ajax({
            type : 'post',
            url : 'modal/chequeoDataModal', //Here you will fetch records 
            data :  'rowid='+ rowid, //Pass $id
            success : function(data){
                    $('.modal-body').html(data);//Show fetched data from database
                }
            });
    });
    var eventos={}
    eventos.frejcar =function(){
        $.ajax({
            url: 'ajax/chData',
                    type: 'POST', // Send post data
                    async: true,
                    success: function(data){
                        $('#chData').html(data);
                    }
                });
    }
    eventos.interval = setInterval(eventos.frejcar, 8000);
    eventos.frejcar();

$("#filter").keyup(function() {
    var str=$("#filter").val();
    var n = str.length;
    if (n==0) {
        eventos.interval = setInterval(eventos.frejcar, 8000);
        eventos.frejcar();
    }else{
     clearInterval(eventos.interval);        
    }

});
      $('#mostrar').hide();
 function capacidad(cantidad){
   $('#agregarVehiculoBttn').prop('disabled', true);        
   $('#mostrar').show();           
  }
 function limpiarCapacidad(){
    $('#agregarVehiculoBttn').prop('disabled', false);        
    $('#mostrar').hide();  
  }
</script>
<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>

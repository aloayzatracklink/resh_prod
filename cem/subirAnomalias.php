<?php $nav="CEM | Subir Anomalias" ?>
<?php include("../includes/head.php") ?>
<?php include("../includes/init.php") ?>
<?php
    if (!$session->yaEstaIngresado()) {
        irA("ingresar");
    }
?>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    var isCharge = false
</script>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php
    ini_set('memory_limit', '508M');
    require __DIR__ . "/../vendor/autoload.php";
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    use PhpOffice\PhpSpreadsheet\Reader\IReader;
    use PhpOffice\PhpSpreadsheet\IOFactory;

    if (isset($_POST['procesar'])){
        // if(count(str_split($_FILES['vouchers']['tmp_name'])) >= 1){
            $destination_path = getcwd().DIRECTORY_SEPARATOR;
            $msgError = "";
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($_FILES['vouchers']['tmp_name']);
            $spreadsheet = $spreadsheet->getActiveSheet();
            $dataArray =  $spreadsheet->toArray();
            $dataAnomalia = new DataAnomalia();

            if(count($dataArray)>1){
                $dataAnomalia->truncateTable();
                for($i=1; $i<count($dataArray); $i++){
                    if(($dataArray[$i][0] == "" || $dataArray[$i][1] == "") || ($dataArray[$i][0] == null || $dataArray[$i][1] == null)){
                        break;
                    }
                    $data = [];
                    $data['fecha_voucher'] = $dataArray[$i][0];
                    $data['id_formulario'] = $dataArray[$i][1];
                    $data['tipo_contrato'] = $dataArray[$i][2];
                    $data['estado_contrato'] = $dataArray[$i][3];
                    $data['cliente'] = utf8_encode($dataArray[$i][4]);
                    $data['agente'] = $dataArray[$i][5];
                    $data['instalador'] = $dataArray[$i][6];
                    $data['estado_terminal'] = $dataArray[$i][7];
                    $data['matricula'] = $dataArray[$i][8];
                    $data['vehiculo'] = $dataArray[$i][9];
                    $data['cod_agente'] = $dataArray[$i][10];
                    $data['direccion_cliente'] = $dataArray[$i][11];
                    $data['poblacion_cliente'] = $dataArray[$i][12];
                    $data['cap_cliente'] = $dataArray[$i][13];
                    $data['prov_cliente'] = $dataArray[$i][14];
                    $data['pais_cliente'] = $dataArray[$i][15];
                    $data['contacto'] = $dataArray[$i][16];
                    $data['cel_contacto'] = $dataArray[$i][17];
                    $data['cod_install'] = $dataArray[$i][18];
                    $data['cel_install'] = $dataArray[$i][19];
                    $data['fax_install'] = $dataArray[$i][20];
                    $data['cel_agente'] = $dataArray[$i][21];
                    $data['fecha_asoc'] = $dataArray[$i][22];
                    $data['fecha_actv'] = $dataArray[$i][23];
                    $data['imei'] = $dataArray[$i][24];
                    $data['cel_terminal'] = $dataArray[$i][25];
                    $data['fecha_cierre'] = $dataArray[$i][26];
                    $data['motivo_chi'] = $dataArray[$i][27];
                    $data['sw_clex'] = $dataArray[$i][28];
                    $data['sw_meta'] = $dataArray[$i][29];
                    $data['nro_poliza'] = $dataArray[$i][30];
                    $data['chasis'] = $dataArray[$i][31];
                    $data['email'] = $dataArray[$i][32];
                    $data['dni'] = $dataArray[$i][33];
                    $dataAnomalia->inserDataToTable($data);
                    ?>
                    <script>
                        isCharge = true
                    </script>
                    <?php
                }
            }
        // }
    }
?>
<body>
    <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!-- Custom Style -->
    <style>
        .p-4{
            padding: 4rem;
        }
        .p-2{
            padding: 2rem;
        }
        .px-4{
            padding-left: 4rem;
            padding-right: 4rem;
        }
        .px-2{
            padding-left: 2rem;
            padding-right: 2rem;
        }
        .m-4{
            margin: 4rem;
        }
        .m-2{
            margin: 2rem;
        }
        .mx-4{
            margin-left: 4rem;
            margin-right: 4rem;
        }
        .mx-2{
            margin-left: 2rem;
            margin-right: 2rem;
        }
        .bg-white{
            background-color: #ffffff;
        }
        .rounded{
            border-radius: 16px;
        }
        .card-shadow{
            /* box-shadow: rgba(17, 12, 46, 0.15) 0px 48px 100px 0px; */
            /* box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px; */
            box-shadow: rgba(255, 255, 255, 0.1) 0px 1px 1px 0px inset, rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;
        }
        .thead:hover{
            background-color: rgba(0, 0, 0, 0.1);
        }
        .TableContainer{
            /* width: 80%; */
            margin: 0 auto;
            /* min-width: 500px; */
            overflow-x: auto;
            /* background-color: ; */
            box-shadow: rgba(255, 255, 255, 0.1) 0px 1px 1px 0px inset, rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;
        }
        .TableHolder{
            margin: 0 auto;
            width: 100%;
            text-align: left;
            color: #000;
            cursor: default;
            border-spacing: 0;
            border-collapse: collapse;
        }
        .TableHolder tr:first-child th{
            border-bottom: 1px solid rgba(0, 0, 0, 0.3);
            padding: 15px 15px;
            background-color: #ffffff44;
            font-size: 1.1em;
        }
        .TableHolder tr td, .TableHolder tr:not(:first-child) th{
            padding: 12px 20px;
            position: relative;
            font-weight: normal;
        }
        .TableHolder tr:not(:first-child) th{
            transition: all 1s ease-out;
        }

        .TableHolder tr td::after, .TableHolder tr:not(first-child) th::after{
            width: 0;
            height: 2px;
            background-color: #ffffff22;
            content: " ";
            position: absolute;
            bottom: 0;
            left: 0;
            transition: width 0.1s ease-out 0.2s, height 0.2s ease 0.4s;
        }

        .TDActiv::after{
            width: calc(100% - 20px) !important;
            height: 100% !important;
        }
        .text-xl{
            font-size: 1.5rem;
        }
        .button-custom{
            background-color: transparent !important;
            border: none;
        }
        .button-custom:hover h4{
            /* background-color: rgba(0, 0, 0, 0.5) !important; */
            color:#1c84c6;

        }
        .text-primary{
            color: #1ab394;
        }
        .badge-name{
            background-color: #363062;
            color: #ffffff;
        }
        .bg-code{
            background-color: #20262e !important;
        }
    </style>
    <link href="../assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/calendario/dist/sweetalert.css">

    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <?php include("../includes/menuIzquierdo.php") ?>
            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>
                </nav>
            </div>
            <!-- Main content -->
            <div class="row bg-white p-4 m-4 rounded">
                <div class="col-12">
                    <h2><strong>Subir Anomalias</strong></h2>
                    <p>Proceso temporal para subir las anomalias de Octo</p>
                </div>
                <form method="POST" enctype="multipart/form-data" action="">
                    <input type="file" class="mt-2" name="vouchers" id="vouchers" />
                    <input type="submit" name="procesar" id="procesar" class="btn btn-primary" style="margin-top: 2rem;" value="Subir Anomalia">
                </form>
                <!-- <div class="col-12" style="margin-top: 2rem;">
                    <div class="bg-white p-4 rounded card-shadow">
                        <h4>Buscador</h4>
                        <div style="display: flex;">
                            <input type="text" name="searchLog" id="searchLog" class="form-control" placeholder="Ingresa un parámetro">
                            <button class="btn btn-primary" style="margin-left: 1rem;" id="searchBtn"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 6rem;min-height: 30vh;">
                    <div class="col-md-2 card-shadow p-2 rounded">
                        <h3 class="text-primary">Lista de Procesos</h3>
                        <button class="button-custom" data-id="sftp"><h4>Sftp-service</h4></button>
                        <button class="button-custom" data-id="web"><h4>Web concesionarios</h4></button>
                        <button class="button-custom" data-id="resh"><h4>Mail Bienvenida</h4></button>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-9 TableContainer">
                        <table class="TableHolder">
                            <tr>
                                <th>#</th>
                                <th>Mensaje</th>
                                <th>Cuerpo de envio</th>
                                <th>Codigo de respuesta</th>
                                <th>Cuerpo de respuesta</th>
                                <th>Nombre del proceso</th>
                                <th>Tipo de tarea</th>
                                <th>Nombre de Proceso</th>
                                <th></th>
                            </tr>
                            <tbody id="resultado"></tbody>
                        </table>
                        <p class="text-xl" style="margin-right: 1rem;text-align:end;"><strong id="page"></strong></p>
                        <div style="display: flex;padding:1rem;">
                            <button class="btn btn-success" id="back"style="width: 50%;"><strong><</strong></button>
                            <button class="btn btn-success" id="next"style="width: 50%;margin-left:1rem;"><strong>></strong></button>
                        </div>
                    </div>
                </div> -->
            </div>
            <?php include("../includes/footer.php") ?>
        </div>
        <div class="modal inmodal" id="verLog" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated bounceIn">
                    <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                        <h4 class="modal-title">Ver Log</h4>
                    </div>
                    <div class="modal-body" style="background: #fff;padding-top: 0">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Regresar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../assets/js/jquery-2.1.1.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../assets/js/inspinia.js"></script>
    <script src="../assets/js/plugins/pace/pace.min.js"></script>
    <script src="../assets/js/plugins/footable/footable.all.min.js"></script>
    <script src="../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
    <script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="../assets/js/plugins/iCheck/icheck.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
    <script>
        var page = 1

        //* Fetch initial data
        // $(document).ready(function(e){
        //     $.ajax({
        //         type: 'post',
        //         url: 'pag/obtenerLogs',
        //         success: function (data) {
        //             $('#resultado').html(data);
        //             $('#page').text(`Página ${page}`);
        //         },
        //     });
        // });

        //* Display Modal
        // $('#vouchers').on('change', function (e) {
        //     // var rowid = $(e.relatedTarget).data('id');
        //     // $.ajax({
        //     //     type : 'post',
        //     //     url : 'modal/verLogDataModal',
        //     //     data :  'rowid='+ rowid,
        //     //     success : function(data){
        //     //         $('.modal-body').html(data);
        //     //     }
        //     // });
        //     console.log($('#vouchers').val())
        // });

        // $('')
        if(isCharge){
            Swal.fire({
                icon: 'success',
                title: 'Correcto',
                text: `La carga ha sido exitósa!`
            });
        }
    </script>
</body>
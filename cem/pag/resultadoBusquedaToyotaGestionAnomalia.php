<?php require_once("../../includes/init.php");

if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    if (isset($_POST["page"])) {
        $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
        if (!is_numeric($numeroDePagina)) {
            die('Numero de pagina incorrecta !');
        } //incase of invalid page number
    } else {
        $numeroDePagina = 1; //if there's no page number, set it to 1
    }
    $datos = limpiar($_POST['datos']);
    $est = limpiar($_POST['estd']);
    $sist = limpiar($_POST['sist']);

    $objAn = new anomalia();

    $cantidadO = $objAn->ttObtenerListaAnomaliaToyota();
    $registrosPorPagina = 20;
    $paginasEnTotal = ceil($cantidadO->cantidad / $registrosPorPagina);
    $posisionDePagina = (($numeroDePagina - 1) * $registrosPorPagina);

    $obj=anomalia::obtenerListaAnomaliaToyota($datos,$est,$sist,$posisionDePagina,$registrosPorPagina);$n=0; ?>
    <?php if($obj){
        foreach ($obj as $var):
          $n++; ?>
            <tr class="gradeX">
                <td class="text-left "><?= $n; ?></td>
                <td class="text-left">
                    <?= $var->cliente ?>
                    <input type="hidden" name="idanoma" value="<?= $var->id?>" >
                </td>
                <td class="text-left"><?= $var->placa ?> </td>
                <td class="text-left"><?= $var->chasis?> </td>
                <td class="text-center"><?= $var->cantdias?> </td>
                <td class="text-center"><?= $var->sistema ?> </td>
                <td class="text-center"><?= obtenerEstadoAnomalia($var->sendMail)?> </td>
                <td class="text-center"><?= $var->cantNotify ?> </td>
                <?php $usuarioObj=Usuario::obtenerUsuario($var->iduseruso);  ?>
                <td style="background-color: <?php echo $retVal = ($usuarioObj->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>"><?php  echo $usuarioObj->usuarioNombre ?> <?php echo $usuarioObj->usuarioApellidoPaterno?></td>
                <td class="text-center">
                    <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#anomaliaOModal"  data-idInt="<?= $var->id ?>">Elegir <i class="fa fa-cog"></i></button>
                </td>
                <td class="text-left"> <ul style="padding: 0">
                        <?php foreach(anomalia::getSEg($var->id) as $var2 ): ?>
                            <li><?= $var2->fecha ?> <b><?= $var2->body ?></b></li>
                        <?php endforeach ?></ul>
                </td>
                <td class="text-left"><?= $var->create_date ?> </td>
                <td class="text-left"><?= $var->update_at_auto ?> </td>
            </tr>
        <?php endforeach;?>
        <tr>
            <td colspan="11"></td>
            <td colspan="2" class="text-right">Mostrando <?= $retVal = ($n<$registrosPorPagina) ? $n : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>

        </tr>
    <?php } else{?>
        <tr>
            <td colspan="16" class="text-center">No hay informaciÃ³n Registrada</td>
        </tr>
    <?php }?>
<?php } ?>

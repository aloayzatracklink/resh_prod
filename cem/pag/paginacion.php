<?php require_once("../../includes/init.php") ?>  
<?php 

  if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $tipo='';
  $mes='';
  $anio='';
  if (isset($_POST["tipo"]) && $_POST['tipo'] <> '0') {
      $tipo=htmlspecialchars($_POST['tipo']);
  }
  if (isset($_POST['mes'])&& $_POST['mes'] <>'0') {
    $mes=$_POST['mes'];
  }
  if (isset($_POST['anio'])&& $_POST['anio'] <>'0') {
    $anio=$_POST['anio'];  
  }
  $datosInstObj=new DatosInstalacion();
  if (empty($tipo)&&empty($mes) &&empty($anio)) {
    $cantidadO=$datosInstObj->obtenerRegistrosSubidosCantida();    
  } 

  if (!empty($idEjecutivo)) {
    $cantidadO=$datosInstObj->obtenerRegistrosSubidosCantidaE($idEjecutivo);        
  }
  
  if (!empty($_POST['fecha'])) {
    $cantidadO=$datosInstObj->obtenerRegistrosSubidosCantidaEFecha($desde,$hasta);        
  }
  
  if (!empty($_POST['estado'])) {
    $cantidadO=$datosInstObj->obtenerRegistrosSubidosCantidaEestadoPago($estado);        
  }

  if (!empty($idEjecutivo)&&!empty($fecha)) {
    $cantidadO=$datosInstObj->obtenerRegistrosSubidosCantidaEejecutviFecha($idEjecutivo,$desde,$hasta);        
  }
  if (!empty($estado)&&!empty($idEjecutivo)) {
    $cantidadO=$datosInstObj->obtenerRegistrosSubidosCantidaEestaodIdejutivo($idEjecutivo,$estado);        
  }  
  if (!empty($estado)&&!empty($fecha)) {
    $cantidadO=$datosInstObj->obtenerRegistrosSubidosCantidaEestadoFecha($estado,$desde,$hasta);        
  }
  // if (!empty($idEjecutivo)||!empty($estado)) {
  //   $cantidadO=$datosInstObj->obtenerRegistrosSubidosCantidaE($idEjecutivo);        
  // }
  if (!empty($idEjecutivo)&&!empty($estado)&&!empty($fecha)) {
    $cantidadO=$datosInstObj->obtenerRegistrosSubidosCantidaETodos($estado,$desde,$hasta,$idEjecutivo);        
  }




  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  // echo $cantidadO->cantidad; 
  $n=1;
  if (empty($idEjecutivo)&&empty($estado) &&empty($fecha)) {
    $datos=$datosInstObj->obtenerRegistrosSubidos($posisionDePagina,$registrosPorPagina);    
  } 

  if (!empty($idEjecutivo)) {
    $datos=$datosInstObj->obtenerRegistrosSubidosE($posisionDePagina,$registrosPorPagina,$idEjecutivo);        
  }
  if (!empty($fecha)) {
    $datos=$datosInstObj->obtenerRegistrosSubidosEFecha($posisionDePagina,$registrosPorPagina,$desde,$hasta);        
  }
  if (!empty($estado)) {
    $datos=$datosInstObj->obtenerRegistrosSubidosEEstado($posisionDePagina,$registrosPorPagina,$estado);        
  }
  if (!empty($idEjecutivo) && !empty($estado)  ) {
    $datos=$datosInstObj->obtenerRegistrosSubidosEestadoIdejecu($posisionDePagina,$registrosPorPagina,$estado,$idEjecutivo);        
  }
  if (!empty($idEjecutivo) && !empty($fecha)  ) {
    $datos=$datosInstObj->obtenerRegistrosSubidosEfechaIdejecutiov($posisionDePagina,$registrosPorPagina,$idEjecutivo,$desde,$hasta);        
  }
  if (!empty($estado) && !empty($fecha)  ) {
    $datos=$datosInstObj->obtenerRegistrosSubidosEFechaEstado($posisionDePagina,$registrosPorPagina,$estado,$desde,$hasta);        
  }
  if (!empty($estado) && !empty($fecha) && !empty($idEjecutivo)  ) {
    $datos=$datosInstObj->obtenerRegistrosSubidosETodos($posisionDePagina,$registrosPorPagina,$estado,$idEjecutivo,$desde,$hasta);        
  }

?>   
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>    
<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="7">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="7" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>

<?php require_once("../../includes/init.php") ?>  
<?php

if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) ){
    if(isset($_POST["page"])){
        $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
        if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta!');}
    }else{
        $numeroDePagina = 1;
    }

    $onix = new Onix();
    $counter = 0;
    $registrosPorPagina=15;
    $cantidad = $onix->obtenerCantidadClientesConMTC();
    $paginasEnTotal=ceil($cantidad->cantidad/$registrosPorPagina);
    $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
    $clientes = $onix->obtenerClientesConMTCAsignado($posisionDePagina,$registrosPorPagina);
?>

    <?php foreach ($clientes as $var): ?>   
    <tr>  
        <td><?= ++$counter ?></td>
        <td><?= $var[0] ?></td>
        <td><?= utf8_encode($var[1]) ?></td>
        <td><?= $var[2] ?></td>
        <td><?= $var[3] ?></td>
        <td><span class='badge badge-warning'>Esta pendiente</span></td>
        <td><?= $var[5] ?></td>
    </tr>
    <?php endforeach ?>
    <tr>
        <td>Mostrando <?= $retVal = ($cantidad->cantidad<$registrosPorPagina) ? $cantidad->cantidad : $registrosPorPagina ; ?> de <?= $cantidad->cantidad  ?></td>
        <td colspan="7">                        
            <?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidad->cantidad,$paginasEnTotal) ?>
        </td>
    </tr>   
<?php } ?>
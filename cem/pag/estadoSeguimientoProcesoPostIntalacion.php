<?php
require_once("../../includes/init.php");

if (!$session->yaEstaIngresado()) {
    irA("../../ingresar");
} else {
    if (isset($_POST['idoperealizada'])) {
        if (PostInstalacion::updateEstadoSeguimiento(limpiar($_POST['idoperealizada']))) {
            $var = 'OK';
        } else {
            die("FAIL HERE");
        }
    }
}

?>
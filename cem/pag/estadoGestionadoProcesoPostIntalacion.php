<?php
require_once("../../includes/init.php");

if (!$session->yaEstaIngresado()) {
    irA("../../ingresar");
} else {
    if (isset($_POST['idoperealizada'])) {
        if (PostInstalacion::updatePostInstalacionEstado(limpiar($_POST['idoperealizada']))) {
            $var = 'OK';
        } else {
            die("FAIL HERE");
        }
    }
}

?>
<?php require_once("../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
    $operacionesObj=new Operacion();
    $registrosPorPagina=15;
    if ($_POST['fechas']=='pasados') {
        $cantidad=$operacionesObj->obtenerTotalPasado();
        $paginasEnTotal=ceil($cantidad/$registrosPorPagina);
        $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
        $datos=$operacionesObj->obtenerTotalPasadoDATA($posisionDePagina,$registrosPorPagina);    
    }else if($_POST['fechas']=='hoy') {
        $cantidad=$operacionesObj->obtenerTotalHoy();
        $paginasEnTotal=ceil($cantidad/$registrosPorPagina);
        $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
        $datos=$operacionesObj->obtenerTotalHoyDATA($posisionDePagina,$registrosPorPagina);    
    }else if($_POST['fechas']=='siguientes') {
        $cantidad=$operacionesObj->obtenerTotalFuturo();
        $paginasEnTotal=ceil($cantidad/$registrosPorPagina);
        $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
        $datos=$operacionesObj->obtenerTotalFuturoDATA($posisionDePagina,$registrosPorPagina);    
    }else if($_POST['fechas']=='pendiente') {
        $cantidad=$operacionesObj->obtenerTotalPENDIETEN();
        $paginasEnTotal=ceil($cantidad/$registrosPorPagina);
        $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
        $datos=$operacionesObj->obtenerTotalPENDIETENDATA($posisionDePagina,$registrosPorPagina);    
    }else if($_POST['fechas']=='listo') {
        $cantidad=$operacionesObj->obtenerTotalLISTO();
        $paginasEnTotal=ceil($cantidad/$registrosPorPagina);
        $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
        $datos=$operacionesObj->obtenerTotalLISTONDATA($posisionDePagina,$registrosPorPagina);    
    }
?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>    
    <?php if ($var->idChequeo==0): ?>
                <?php $datosTrama = TramaIntegracionTY::obtenerTramaIntegracionPorIdOperacion($var->idoperacion);  ?>
    <?php $usuarioOPE=Usuario::obtenerUsuario($var->idConfirmacionOPE)  ?>
    <?php $clienteObj=Cliente::obtenerDatosDeclientePorId($var->idcliente) ?>
    <?php $datosVehiculo=Vehiculo::obtenerVehiculo($var->idvehiculo) ?>
      <?php else: ?>
    <?php $usuarioOPE=Usuario::obtenerUsuario($var->idCreador)  ?>
      <?php $clienteObj=Onix::obtenerDatosDesdeIds($var->idcliente,$var->idvehiculo,$var->idlocation) ?>
    <?php endif ?>
     <!-- bg-warning success-->
    <tr  <?= obtenerClass($var->lugarInstalacion) ?>  style="<?= $datosVehiculo->campana=="Toyota"?"border: 2px solid red !important":"" ?>">
          <td  ><?= !empty($var->numeroOrte)?$var->numeroOrte:"Pendiente" ?></td>       
          <td><?= $var->lugarInstalacion ?></td>
          <td><?= $var->trabajo ?></td>
    <?php if ($var->idChequeo==0): ?>
         <td><?= ($clienteObj->tipoCliente=="Natural") ? strtoupper($clienteObj->apellidosC)." ".strtoupper($clienteObj->nombre) : strtoupper($clienteObj->razonSocial) ; ?></td>
       <?php else: ?>
          <?php $apellidos=utf8_encode($clienteObj->apellidos) ?>
        <td><?= $retVal = (!empty($clienteObj->nombre)) ? "$apellidos"." "."$clienteObj->nombre" : "$apellidos" ; ?></td>          
        <?php endif ?>
          <td><?= formatoHaceEnSinFecha($var->fechaInstalacion) ?></td>
          <td><?= $usuarioOPE->usuarioNombre." ".$usuarioOPE->usuarioApellidoPaterno ?></td>
          <td><?= obtenerEstado($var->estado) ?></td>
        <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosVehiculo->campana</span>":$datosVehiculo->campana) ?></td>
        <td class="center"><?= empty($datosVehiculo->campana)?'':($datosVehiculo->campana=='Toyota'?"<span class='label label-danger'>$datosTrama->tipo_transaccion</span>":'') ?></td>
          <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" data-id="<?php echo $var->idoperacion ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;</td>
    </tr>
<?php endforeach ?>
<tr>
<td>Mostrando <?= ($cantidad<$registrosPorPagina) ? $cantidad : $registrosPorPagina ; ?> de <?= $cantidad  ?></td>
  <td colspan="8">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="78" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>

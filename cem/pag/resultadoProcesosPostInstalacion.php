<?php require_once("../../includes/init.php");

if (isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    if (isset($_POST["page"])) {
        $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
        if (!is_numeric($numeroDePagina)) {
            die('Numero de pagina incorrecta !');
        } //incase of invalid page number
    } else {
        $numeroDePagina = 1; //if there's no page number, set it to 1
    }
    $datos = limpiar($_POST['datos']);
    $est = limpiar($_POST['estd']);
    $sist = limpiar($_POST['sist']);

    $objPostInstall = new PostInstalacion();

    $cantidadO = $objPostInstall->ttobtenerlstpostinstalacion($datos, $est, $sist);
    $registrosPorPagina = 20;
    $paginasEnTotal = ceil($cantidadO->cantidad / $registrosPorPagina);
    $posisionDePagina = (($numeroDePagina - 1) * $registrosPorPagina);

    $obj = PostInstalacion::obtenerlstpostinstalacion($datos, $est, $sist, $posisionDePagina, $registrosPorPagina);
    $n = 0; ?>
    <?php if ($obj) {
        foreach ($obj as $var):
            $n++; ?>
            <tr class="gradeX">
                <td class="text-left "><?= $n; ?></td>
                <td class="text-left"><?= $var->idoperealizadas ?> </td>
                <td class="text-left"><?= (isset($var->cuenta) && $var->cuenta != null) ? $var->cuenta : "TRACKLINK" ?> </td>
                <td class="text-left"><?= $var->fecha_format ?> </td>
                <td class="text-left"><?= $var->numero_orte ?> </td>
                <td class="text-left">
                    <?= $var->cliente_nombres ?>
                    <input type="hidden" name="idpostinstalacion" value="<?= $var->idoperealizadas ?>">
                </td>
                <td class="text-left"><?= $var->placa ?> </td>
                <td class="text-left"><?= $var->chasis ?> </td>
                <td class="text-left"><?= $var->device_imei ?> </td>
                <td class="text-left"><?= $var->device_id ?> </td>
                <td class="text-left"><?= $var->odometro ?> </td>
                <td class="text-left"><?= $var->ubicacion_sistema ?> </td>
                <td class="text-left"><?= $var->lugar_instalacion ?> </td>
                <td class="text-left"><?= $var->tecnico_instalador ?> </td>
                <td class="text-center"><?= $var->sistema ?> </td>
                <td class="text-center"><?= $var->servicio ?> </td>
                <td class="text-center"><?= $var->plan ?> </td>
                <td class="text-center"><?= $var->estado ?> </td>
                <td class="text-center"><?= $var->fecha_gestionado ?> </td>
                <td class="text-center"><?= $var->gestionado_por_nombres ?> </td>
                <td>
                    <?php if ($var->estado != 'GESTIONADO') { ?>
                        <button class="btn btn-primary formProcesoPostInstall"  style="padding-top: 0;padding-bottom: 0"  type="submit" value="<?php echo $var->idoperealizadas ?>"> Gestionado <i class="fa fa-check-square-o"></i></button>&nbsp;
                    <?php } ?>

                    <?php if ($var->estado != 'GESTIONADO' && $var->estado !="SEGUIMIENTO") { ?>
                        <button class="btn btn-success formSegProcesoPostInstall"  style="padding-top: 0;padding-bottom: 0"  type="submit" value="<?php echo $var->idoperealizadas ?>"> Seguir <i class="fa fa-adjust"></i></button>&nbsp;
                    <?php } ?>
                </td>
                <?php $usuarioCa=Usuario::obtenerUsuario($var->seguimiento_por); ?>
                <td style="background-color: <?= $retVal = ($usuarioCa->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>" ><?= $usuarioCa->usuarioNombre." ".$usuarioCa->usuarioApellidoPaterno ?></td>
                <td class="text-center"><?= $var->observacion . ($var->ubicacion_boton_panico == "" ? "" : " / UBICACION BOTON PANICO: " . $var->ubicacion_boton_panico) . ($var->medida_combustible == "" ? "" : " / MEDIDA COMBUSTIBLE: " . $var->medida_combustible) ?> </td>
                <td class="text-center"><?= $var->tipo_instalacion ?> </td>
                <td class="text-center"><?= $var->cantdias ?> </td>
                <td class="text-center"><?= $var->test_ubicacion ?> </td>
                <td class="text-center"><?= $var->test_boton_panico ?> </td>
                <td class="text-center"><?= $var->test_bateria ?> </td>
                <td class="text-center"><?= $var->test_encender_motor ?> </td>
                <td class="text-center"><?= $var->test_apagar_motor ?> </td>
                <td class="text-center"><?= $var->test_bloquear_motor ?> </td>
                <td class="text-center"><?= $var->test_desbloquear_motor ?> </td>
                <td class="text-center"><?= $var->test_abrir_pestillos ?> </td>
                <td class="text-center"><?= $var->config_odometro ?> </td>
                <td class="text-center"><?= $var->test_reiniciar_gps ?> </td>
                <!--<td class="text-center">
                    <button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0" data-toggle="modal"
                            data-target="#anomaliaOModal" data-idInt="<?/*= $var->id */?>">Cambiar Estado <i class="fa fa-cog"></i>
                    </button>
                </td>-->
            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="15"></td>
            <td colspan="2" class="text-right">
                Mostrando <?= $retVal = ($n < $registrosPorPagina) ? $n : $registrosPorPagina; ?>
                de <?= $cantidadO->cantidad ?></td>

        </tr>
    <?php } else { ?>
        <tr>
            <td colspan="16" class="text-center">No hay información Registrada</td>
        </tr>
    <?php } ?>
<?php } ?>

<script>

    $('.formProcesoPostInstall').click(function(e) {
        console.log("Realizado");
        var idoperealizada = $(this).val();
        $.confirm({
            theme: 'modern',
            title: '¿Cambiar estado como Gestionado?',
            content: 'Al cambiar a gestionado el registro desaparecera de la lista, para volver a visualizar cambiar el filtro a "Todos"',
            buttons: {
                Confirmar: {
                    btnClass: 'btn-blue',
                    action:function () {
                        $.ajax({
                            type : 'POST',
                            url  : 'pag/estadoGestionadoProcesoPostIntalacion',
                            data :  {idoperealizada:idoperealizada},
                            success : function(data) {
                                $("#filter").focus();
                                $.alert('Se realizo el cambio');
                                document.location.reload(true);
                            }
                        });
                    }
                },
                Cancelar: {
                    btnClass: 'btn-warning',
                    action:function () {
                        $("#filter").focus();
                        $.alert('No se realizo nigun cambio!');
                    }
                }
            }
        });
    });

    $('.formSegProcesoPostInstall').click(function(e) {
        console.log("Realizado");
        var idoperealizada = $(this).val();
        $.confirm({
            theme: 'modern',
            title: '¿Seguimiento?',
            content: 'Dar seguimiento al proceso de instalación ',
            buttons: {
                Confirmar: {
                    btnClass: 'btn-red',
                    action:function () {
                        $.ajax({
                            type : 'POST',
                            url  : 'pag/estadoSeguimientoProcesoPostIntalacion',
                            data :  {idoperealizada:idoperealizada},
                            success : function(data) {
                                $("#filter").focus();
                                $.alert('Se realizo el cambio');
                                document.location.reload(true);
                            }
                        });
                    }
                },
                Cancelar: {
                    btnClass: 'btn-warning',
                    action:function () {
                        $("#filter").focus();
                        $.alert('No se realizo nigun cambio!');
                    }
                }
            }
        });
    });
</script>
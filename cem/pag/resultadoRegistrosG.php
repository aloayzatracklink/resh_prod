<?php
  if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    require_once("../../includes/init.php"); 
  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1;
  }
    require_once("../../includes/init.php"); 
    $datosInstObj=new DatosInstalacion;
    $datos='%'.$_POST['datosS'].'%';
    $datos=$datosInstObj->obtenerDatosDeInstalacionF($datos);
    if ($datos) {
        foreach($datos as $var):
       ?>
 <tr class='<?= ($var->tmp=="")?"":"danger"?>'> 
                <td><?= formatoSinHora($var->fecha) ?><br><b><?= $var->orte ?><b></td>
                <td><?= $var->cliente ?></td>
                <td><?= $var->placa ?></td>
                <td><?= $var->chasis ?></td>
                <td><?= $var->servicio ?><br><?= limpiarPlan($var->sistema) ?> </td>
                <td><?= $var->operador ?></td>
                <td><?= (is_numeric($var->estado))?obtenerEstado($var->estado):$var->estado ?></td>
                <td><?= (!empty($var->horaActivacion))?formatoHora($var->horaActivacion):'' ?></td>
                <td><?= $var->observaciones ?></td>
                <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" data-id="<?php echo $var->iddatosInstalacion ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;</td>                    
            </tr>         
       <?php 
        endforeach;
    }
?>
<?php
  }
?>

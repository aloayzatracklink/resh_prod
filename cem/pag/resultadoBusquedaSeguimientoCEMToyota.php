<?php require_once("../../includes/init.php");

if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    if (isset($_POST["page"])) {
        $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
        if (!is_numeric($numeroDePagina)) {
            die('Numero de pagina incorrecta !');
        } //incase of invalid page number
    } else {
        $numeroDePagina = 1; //if there's no page number, set it to 1
    }
    $datos = limpiar($_POST['datos']);

    $estdAc= limpiar($_POST['estdAc']);
    $tr = ($_POST['tr']);

    $fecha='';$desde="";$hasta="";
    if (isset($_POST["fecha"])) {
        if($_POST["fecha"]!="") {
            $fecha = strip_tags($_POST['fecha']);
            $desde = trim(current(explode('/', $fecha)));
            $tmp = explode('/', $fecha);
            $hasta = end($tmp);
            $desde = date("Y-m-d", strtotime($desde));
            $hasta = date("Y-m-d", strtotime($hasta));
        }

    }

    $objTY = new TramaIntegracionTY();

    $cantidadO = $objTY->getCantidadInfoSegTyCEM($datos,$tr,$estdAc,$desde,$hasta);
    $registrosPorPagina = 20;
    $paginasEnTotal = ceil($cantidadO->cantidad / $registrosPorPagina);
    $posisionDePagina = (($numeroDePagina - 1) * $registrosPorPagina);

    $obj=TramaIntegracionTY::getInfoSegTyCEM($datos,$tr,$estdAc,$desde,$hasta,$posisionDePagina,$registrosPorPagina);$n=0; ?>
    <?php if($obj){
        foreach ($obj as $var):
            $n++; ?>
            <tr >
                <td><?=  $n ?></td>
                <td><?=  $var->tipo_transaccion ?></td>
                <td><?=  $var->fechaRecibido ?></td>
                <td><?=  $var->fecha ?></td>
                <td><?=  $var->orte ?></td>
                <td><?=  $var->cliente ?></td>
                <td><?=  $var->placa ?></td>
                <td><?=  $var->chasis ?></td>
                <td><?=  $var->imei ?></td>
                <td><?=  $var->id ?></td>
                <td><?=  $var->odo ?></td>
                <td><?=  $var->ubicacionSistema ?></td>
                <td><?=  $var->lugarInstalacion ?></td>
                <td><?=  $var->tecnicoInstalador ?></td>
                <td><?=  $var->plan ?></td>
                <td><?=  $var->operador ?></td>
                <td><?=  $var->estadoOpera ?></td>
                <td><?=  $var->fechaInstalacion ?></td>
                <td><?=  $var->estadoActivacion ?></td>
                <td><?=  $var->fecha_activacion ?></td>
                <?php $usuarioObj=Usuario::obtenerUsuario($var->usuario_activacion);  ?>
                <td style="background-color: <?php echo $retVal = ($usuarioObj->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>"><?php  echo $usuarioObj->usuarioNombre ?> <?php echo $usuarioObj->usuarioApellidoPaterno?></td>
                <td><?=  $var->estadoTrama ?></td>
                <td><?=  $var->motivoAnulacion ?></td>
                <?php $usuarioObj=Usuario::obtenerUsuario($var->usuario_anulacion);  ?>
                <td style="background-color: <?php echo $retVal = ($usuarioObj->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>"><?php  echo $usuarioObj->usuarioNombre ?> <?php echo $usuarioObj->usuarioApellidoPaterno?></td>
                <td><?=  $var->fecha_anulacion?></td>
            </tr>
        <?php endforeach;?>
        <tr>
            <td colspan="2" class="text-right">Mostrando <?= $retVal = ($n<$registrosPorPagina) ? $n : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>

        </tr>
    <?php } else{?>
        <tr>
            <td colspan="16" class="text-center">No hay información Registrada</td>
        </tr>
    <?php }?>
<?php } ?>

<?php
    $nav="procesosPostInstalacion";
    include("../includes/head.php");
    if (!$session->yaEstaIngresado()) {
        irA("../ingresar");
    }
    $obMenu=Usuario::obtenerMenu($session->idusuario);

if ($obMenu->instalaciones=="si" || $session->idusuario=='3' || $session->idusuario=='80' || $session->idusuario=='148' || $session->idusuario=='111'):
        if (isset($_POST['guardaranomalia'])) {
            if(Token::verificar($_POST['guardaranomalia'])) {
                $nest = $_POST['nestado'];
                $idan = $_POST['idanom'];
                $objA = new anomalia();
                $objA->sendMail = $nest;
                $objA->id = $idan;
                $anm = $objA->updateAnomaliaEstado();
                if(isset($_POST['nseguimiento']) && trim($_POST['nseguimiento'])!="") {
                    $seg = array(
                        "body" => $_POST['nseguimiento'],
                        "idanom" => $idan,
                        "idcustomer" => $_POST["nidcust"],
                        "idlocation" => $_POST["nidloc"],
                        "idvehicle" => $_POST["nidveh"],
                        "subject" => ("GESTIÓN DE ANOMALÍA - ".$_POST['nplaca'])
                    );
                    $objA->saveSeg($seg);
                }
                if($anm)
                    $echo[]="Se Registro con exito ";
            } else {
                $errores[]="El formulario ya fue procesado, verificar.";
            }
        }
    else {
        if (isset($_POST['addEstado'])) {
            if (Token::verificar($_POST['addEstado'])) {
                $tchk=0;
                if (isset($_POST['idst'])) {
                    $tchk=$_POST['idst'];
                }
                for($ii=1; $ii <= count($tchk); $ii++) {
                    $rchk=0;
                    if(isset($_POST['nacc_'.$ii])){
                        $rchk=1;
                    }
                    $nid=$tchk[($ii-1)];
                    $nest=$rchk;
                    $ncolo=$_POST['ncolor_'.$ii];

                    $objd = new anomalia();
                    $objd->id =$nid;
                    $objd->accion = $nest;
                    $objd->color = $ncolo;
                    $objd->updateEstado();
                }

                $echo[] = "Estado actualizado con exito";
            } else {
                $errores[] = "El formulario ya fue procesado, vericar.";
            }
        }
    }
?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="../assets/css/sweetalert2.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/css2/style2.css">
<link rel="stylesheet" href="../assets/css2/style3.css">
<link rel="stylesheet" href="../assets/css/jquery-confirm.min.css">

<style>
    .table-cabecera-horario{
        background:#075598!important;
        color:white !important;
        vertical-align: middle!important;
    }
    .table-cabecera-horario2{
        background:#164764 !important;
        color:white !important;
        vertical-align: middle!important;
    }
    .table-cabecera-horario3{
        background: #096452 !important;
        color:white !important;
    }
    .table-cabecera-horario4{
        background: #0e2e79 !important;
        color:white !important;
    }

    .table-scroll {
        position:relative;
        max-width:1600px;
        margin:auto;
        overflow:hidden;
        /*border:.5px solid #000;*/
    }

    .table-wrap {
        width:100%;
        overflow:auto;
    }

    .table-scroll table {
        width:100%;
        margin:auto;
        border-collapse:separate;
        border-spacing:0;
    }

    .table-scroll th, .table-scroll td {
        padding:5px 10px;
        border-bottom:1px solid;
        border-right:1px solid;
        border-color: #DDDDDD;;
        background:#fff;
        color:rgb(103, 106, 108) ;
        white-space:nowrap;
        vertical-align:top;
        font-size: 12px
    }

    .table-scroll thead, .table-scroll tfoot {
        background: #f9f9f9;
    }

    .bordertab{
        border-bottom: 1px #e1d0d0 solid !important;
        border-left: 1px #e1d0d0 solid !important;
    }
</style>
<body>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <?php include("../includes/menuIzquierdo.php") ?>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../includes/navegacionArriba.php") ?>
            </nav>
        </div>
        <div class="wrapper wrapper-content">
            <div class="row ibox float-e-margins ibox-content">
                <div class="col-md-12" style="padding: 0">
                    <div id="table-scroll" class="table-scroll">
                        <div class="table-wrap">
                            <div class="col-md-6"></div>
                            <div class="col-md-2">
                                <label for="cboSistema">Sistema</label>
                                <select style="border: none;margin-bottom: 15px" class="border-bottom form-control text-capitalize" required="" id="cboSistema" name="cboSistema" aria-required="true">
                                    <option value="">Todos</option>
                                    <option value="OCTO">Octo</option>
                                    <option value="TRACK">Track</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="cboEstado">Estado</label>
                                <select style="border: none;margin-bottom: 15px" class="border-bottom form-control text-capitalize" required=""  id="cboEstado" name="cboEstado" aria-required="true">
                                    <option value="">Todos</option>
                                    <option value="PENDIENTE" selected>PENDIENTE</option>
                                    <option value="GESTIONADO">GESTIONADO</option>

                                </select>
                            </div>
                            <div class="col-md-2" style="padding-top: 18px;">
                                <!--<button class="btn btn-sm btn-info" data-toggle="modal" data-target="#addEstadoAnomalia">Agregar Estado <i class="fa fa-save"></i></button>-->
                                <form action="PHPExcel/PostInstalacionExcel" method="POST">
                                    <div class="col-md-2" id="resultado1">
                                        <button class="btn btn-primary" name="Descargar" id="resultado1">Descargar en Excel</button>
                                    </div>
                                </form>
                            </div>

                            <input type="text" class="form-control input-sm m-b-xs" id="filterrr"
                                   placeholder="Buscar" style="margin-bottom: 15px">
                            <table class="table main-table" >
                                <thead>
                                    <tr>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"></th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">UID</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Cuenta</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Fecha</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Orte</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Cliente</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Placa</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Chasis</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">IMEI</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">ID</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Odoo</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Ubicacion Sistema</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Lugar</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Tecnico Instalador</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Sistema</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Servicio</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Plan</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Estado</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Fecha Gestionado</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Gestionado Por</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Accion</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Cargo</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Observacion</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Modo Instalacion</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Cant. Dias</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Prueba Ubicacion</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">A. Panico</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">A. Bateria</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Motor ON</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Motor OFF</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Bloquear Motor</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Desbloquear Motor</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Abrir Pestillos</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Config. Odometro</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Reset Disp. GPS</th>
<!--                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"></th>-->
                                    </tr>
                                </thead>
                                <tbody id="resultado">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("../includes/footer.php") ?>
    </div>
</div>


<div class="modal inmodal" id="addEstadoAnomalia" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom:0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Agregar Estado</h4>
            </div>
            <form  role="form" method="post"  enctype="multipart/form-data" name="regEst" id="regEst">
                <input  type="text" name="btnAddEstado" id="btnAddEstado" disabled value="" style="display: none">
                <div class="modal-body2" style="background: #fff;padding:0 15px"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                    <button type="submit" name="addEstado" id="addEstado"  class="btn btn-primary btn-modal" value="<?php echo Token::generar() ?>" >Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal inmodal" id="anomaliaOModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 5px;padding-bottom: 2px">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                <h4 class="modal-title">Gestión Anomalía</h4>
                <small class="font-bold"><div class="form-group ">
                    </div></small>
            </div>
            <form  role="form" method="post" enctype="multipart/form-data" id="formOpe" name="formOpe">
                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>
            </form>
        </div>
    </div>
</div>

<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/plugins/clockpicker/clockpicker.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/select2/select2.full.min.js"></script>
<script src="../assets/bootbox/bootbox.all.js"></script>

<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="../assets/js/jquery-confirm.min.js"></script>

<script src="../assets/js/plugins/ladda/spin.min.js"></script>
<script src="../assets/js/plugins/ladda/ladda.min.js"></script>
<script src="../assets/js/plugins/ladda/ladda.jquery.min.js"></script>
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<style>
    .popover {
        z-index: 999999;
    }
</style>
<?php include_once('../includes/script.php') ?>
<script>

    <?php if (!empty($errores)): ?>
    <?php foreach ($errores as $error): ?>
    toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
    <?php endforeach ?>
    <?php endif ?>
    <?php if(!empty($echo)): ?>
    <?php foreach($echo as $ok): ?>
    toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
    <?php endforeach ?>
    <?php endif ?>
</script>
<script>
    $('#addEstadoAnomalia').on('show.bs.modal', function (e) {
        $(".modal-body2").html("");
        $.ajax({
            type : 'post',
            url : 'modal/addEstado',
            data :  "",
            success : function(data){
                $('.modal-body2').html(data);
            }
        });
    });

    $('#anomaliaOModal').on('show.bs.modal', function (e) {
        $('.modal-body').html("");
        var rowid = $(e.relatedTarget).data('idint');
        $.ajax({
            type : 'post',
            url : 'modal/anomaliasmodal',
            data :  'idanom='+ rowid,
            success : function(data){
                $('.modal-body').html(data);
            }
        });
    });

    $(document).ready(function(){
        $('.footable').footable();
        $('.footable2').footable();
    });


    $('body').on('hidden.bs.modal', function () {
        if($('.modal.in').length > 0)
        {
            $('body').addClass('modal-open');
        }
    });


    $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
    $.ajax({
        type : 'POST',
        url  : 'pag/resultadoProcesosPostInstalacion',
        data :  {datos:'',estd:'PENDIENTE',sist:'',page:1},
        success : function(data)
        {
            $("#resultado").html(data);
        }
    });


    $("#cboSistema").change(function() {
        $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
        var datosS = $("#filterrr").val();
        datosS = datosS.replace('<>/"', '');
        var estd=$("#cboEstado option:selected").val()
        var sist=$("#cboSistema option:selected").val()
        $.ajax({
            type : 'POST',
            url  : 'pag/resultadoProcesosPostInstalacion',
            data :  {datos:datosS,estd:estd,sist:sist},
            success : function(data)
            {
                $("#resultado").html(data);
            }
        });
        return false;

    })

    $("#cboEstado").change(function() {
        $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
        var datosS = $("#filterrr").val();
        datosS = datosS.replace('<>/"', '');
        var estd=$("#cboEstado option:selected").val()
        var sist=$("#cboSistema option:selected").val()
        $.ajax({
            type : 'POST',
            url  : 'pag/resultadoProcesosPostInstalacion',
            data :  {datos:datosS,estd:estd,sist:sist},
            success : function(data)
            {
                $("#resultado").html(data);
            }
        });
        return false;

    })



    $("#filterrr").keypress(function(e)
    {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){
            $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
            var datosS = $(this).val();
            datosS = datosS.replace('<>/"', '');
            var estd=$("#cboEstado option:selected").val()
            var sist=$("#cboSistema option:selected").val()
            $.ajax({
                type : 'POST',
                url  : 'pag/resultadoProcesosPostInstalacion',
                data :  {datos:datosS,estd:estd,sist:sist},
                success : function(data)
                {
                    $("#resultado").html(data);
                }
            });
            return false;
        }
    });



</script>
<?php else: ?>
    <?php $session->salir();
    echo "NO NO NO NO ; presiona F5"
    ?>
<?php endif ?>

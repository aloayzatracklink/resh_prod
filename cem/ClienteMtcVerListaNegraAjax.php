<?php
require_once("../includes/init.php");
//860ryp0nz-EZ
$onix = new Onix();
$resh = new Usuario();

// Obtiene los datos de listaNegraMtc
$listaNegra = $resh->listaNegraMtc();

// Obtiene los datos adicionales de clientesMTCdeListaNegra
$extraData = $onix->clientesMTCdeListaNegra();

foreach ($listaNegra as $key => $value) {
    foreach ($extraData as $keys => $values) {
        if ($value['idcustomer'] == $values['idcustomer'] && $value['idvehicle'] == $values['idvehicle']) {
            $listaNegra[$key]['identdoc'] = $values['identdoc'];
            $listaNegra[$key]['descarticle'] = $values['descarticle'];
            $listaNegra[$key]['idchasis'] = $values['idchasis'];
            $listaNegra[$key]['license'] = $values['license'];
            $listaNegra[$key]['descbrand'] = $values['descbrand'];
            $listaNegra[$key]['descmodel'] = $values['descmodel'];
            $listaNegra[$key]['formatted_date'] = $values['formatted_date'];
            $listaNegra[$key]['motivoBL'] = $value['motivoBL'];
        }
    }
}
foreach ($listaNegra as $key => $value) {
    if (!isset($value['identdoc'])) {
        unset($listaNegra[$key]);
    }
}

$listaNegra = array_values($listaNegra);
echo json_encode($listaNegra, JSON_INVALID_UTF8_IGNORE);
?>
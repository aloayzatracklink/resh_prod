<?php $nav="robo"; ?>
<?php include("../includes/head.php") ?>
<?php 
if (!$session->yaEstaIngresado()) { irA("../ingresar"); } 
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->robos=="si"): ?>
<?php
require_once '../mailphp/PHPMailer/PHPMailerAutoload.php';
$robo=new Robo();
if (isset($_POST['inicio'])) {
    if ($robo) {        
 $chasis=trim($_POST['placaR']);
  $objRobo=Onix::ObtenerDatosDesdeChasis($chasis);
  if ($objRobo) {
    $robo->placaRobo=$objRobo->placa;
    $robo->chasis=$objRobo->chasis;
    $robo->marca=$objRobo->marca;
    $robo->modelo=$objRobo->modelo;
    $robo->color=$objRobo->color;
    $robo->anio=$objRobo->anio;
    $cliente = (!empty($objRobo->apellidos)) ? $objRobo->nombre." ".$objRobo->apellidos : $objRobo->apellidos ; 
    $robo->propietario=$cliente;
    $robo->dniOrucPropietario=$objRobo->dni;
    $robo->contrato=$objRobo->articulo;
    $robo->vence=$objRobo->hasta;
    $robo->nombreInformante=strtoupper($_POST['nombreI']);
    $robo->numeroContacto=$_POST['numeroC'];
    $robo->idusuario=$session->idusuario;
        if ($robo->crearRobo()) {
        $usuario=Usuario::obtenerUsuario($session->idusuario);  $usuario->usuarioNombre; $usuario->usuarioApellidoPaterno;  
            /////Aqui se envia el correo ///
$thehtml="<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
    <meta name='viewport' content='width=device-width' />
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
    <title>The alerta</title>
</head>

<body style=' margin: 0;padding: 0;font-family:Helvetica, Arial, sans-serif;    box-sizing: border-box;    font-size: 14px; background-color: #f6f6f6; '>

<table style=' background-color: #f6f6f6; width: 100%;'>
    <tr>
        <td></td>
        <td  width='600' style=' display: block !important;    max-width: 600px !important;    margin: 0 auto !important;    /* makes it centered */    clear: both !important;'>
            <div class='content' style='  max-width: 600px;    margin: 0 auto;    display: block;    padding: 20px;'>
                <table class='main' width='100%' cellpadding='0' cellspacing='0' style='background: #fff;    border: 1px solid #e9e9e9;    border-radius: 3px;'>
                    <tr>
                        <td style='font-size: 16px;    color: #fff;    font-weight: 500;    padding: 20px;    text-align: center;    border-radius: 3px 3px 0 0;  background: #ed5565;'>
        Llamada recibida por alerta de Robo.
                        </td>
                    </tr>
                    <tr>
                        <td style='padding: 10px !important ;padding: 20px; vertical-align: top;'>
                            <table width='100%' cellpadding='0' cellspacing='0'>
                                <tr>
                                    <td  style='padding: 0 0 20px;'>
                                        A cargo de <strong> $usuario->usuarioNombre $usuario->usuarioApellidoPaterno .</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td  style='padding: 0 0 20px;'>
                                         $robo->nombreInformante reporto el robo de Vehículo de placa $robo->placaRobo <br> Chasis $robo->chasis. <br> Marca: $robo->marca<br> Modelo: $robo->modelo</td>
                                </tr>
                                <tr>
                                    <td style='padding: 0 0 20px;text-align: center; '>
                                        <a href='http://resh.tracklink.pe/robo' style='text-decoration: none;    color: #FFF;    background-color: #ed5565;    border: solid #ed5565;    border-width: 5px 10px;    line-height: 2;    font-weight: bold;    text-align: center;    cursor: pointer;    display: inline-block;    border-radius: 5px;    text-transform: capitalize;'>Visuallizar estado</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='padding: 0 0 20px;'>
                                        Gracias por utilizar Resh Sistemas.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div style=' width: 100%;    clear: both;    color: #999;    padding: 20px;'>
                    <table width='100%'>
                        <tr>
                            <td style='font-size: 12px; text-align: center;padding: 0 0 20px;color: #999;' ><a href='#' style='color: #999;'>Desuscribirse </a> de este tipo de alertas.</td>                        </tr>
                    </table>
                </div></div>
        </td>
        <td></td>
    </tr>
</table>
</body>
</html>";
//El correo para el cliente aqui /// <---->
$mail = new PHPMailer;
$mail->isSMTP();
$mail->SetLanguage( 'es', 'mailphp/PHPMailer/language/' );
$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->Host = gethostbyname('mail.tracklink.pe');
$mail->Port = 25;
$mail->SMTPAuth = false;
$mail->CharSet = 'UTF-8';
$mail->setFrom('centralemergencia@tracklink.pe', 'Central De Emergencia Tracklink');  
// $mail->addReplyTo("centralemergencia@tracklink.pe", 'Central De Emergencia'); 
$mail->addAddress("centralemergencia@tracklink.pe","Central De Emergencia"); // aquien se manda el mensaje
//$mail->addAddress("pvasquez@tracklink.pe","Pabel"); // aquien se manda el mensaje
//CC and BCC
//$mail->addCC("pruebas-ti@tracklink.pe");
$mail->addBCC("pruebas-ti@tracklink.pe");
// $mail->addBCC("bcc@example.com");
$mail->Subject = 'Robo repotado: '.$robo->placaRobo.' por '.$robo->nombreInformante.''; //// ASUNTO 
$mail->msgHTML($thehtml);
//Replace the plain text body with one created manually
$mail->AltBody = 'Estimado(as) '.$robo->placaRobo.' fue reporta do como robado vea el seguimiento'; ////////ESTE SE EL CONTENIDO//// QUE SE BERA EN PRIERA INSTANACIA
//Attach an image file
  //aqui se adjunta los archivos que queremos enviar :D

// $mail->addAttachment('images/phpmailer_mini.png');

//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    // echo "Correo Enviado";
    $url="int/detalleRobo.php?id=$robo->idrobo";
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>'; exit;
    // header("Location:detalleRobo.php?id=$robo->idrobo");
}

///Aqui se envia el correo ///
        

        }else{
            irA("robo");
        }

  }else{
    echo "No existe en la base de datos";
  }
   
    }

}


?>
<body>
    <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <?php include("../includes/menuIzquierdo.php") ?>
        </div>
    </nav>
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <?php include("../includes/navegacionArriba.php") ?>

        </nav>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
               <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Nuevo Robo</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <form role="form" class="" method="post" >
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="placa" class="sr-only">Chasis</label>
                                    <input type="text" placeholder="Chasis" id="placa" name="placaR" class="form-control text-uppercase" autofocus required >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="nombreI" class="sr-only">Nombre del informante</label>
                                    <input type="text" placeholder="Nombre completo del informante" id="nombreI" name="nombreI" class="form-control x2 text-capitalize" required >
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="numeroC" class="sr-only">Número de contacto</label>
                                    <input type="number" maxlength="10" minlength="9" placeholder="Número de contacto" id="numero" name="numeroC" class="form-control" required >
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-success" type="submit" name="inicio">Iniciar seguimiento</button>
                                </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        </div>     
                <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Ultimos robos</h5>

                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <input type="text" class="form-control input-sm m-b-xs" id="filter"
                                   placeholder="Buscar">

                            <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Placa</th>
                                    <th>Propietario</th>
                                    <th>Operador</th>
                                    <th>Estado</th>
                                    <th>Fecha</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php Robo::obtenerRobos() ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
            </div>
        <?php include("../includes/footer.php") ?>
        </div>
        </div>
<div class="small-chat-box fadeInRight animated">
            <div class="heading" draggable="true">
                <small class="chat-date pull-right">
                    <?php echo $horaActual=date("h:i"); ?>
                </small>
                chat
            </div>

            <div class="content">

            </div>
                
            <div class="form-chat">
                <div class="input-group input-group-sm">
                    <textarea class="hablar form-control" placeholder="Shift+Enter para saltar espacio"></textarea>
                    <span class="input-group-btn"><button
                        class="btn btn-primary">Enviar
                </button> </span>
                </div>
            </div>
                </form>

        </div>
        <div id="small-chat">

            <span class="badge badge-warning pull-right">5</span>
            <a class="open-small-chat">
                <i class="fa fa-comments"></i>

            </a>    
        </div>




    <!-- Mainly scripts -->
    <script src="../assets/js/jquery-2.1.1.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="../assets/js/inspinia.js"></script>
    <script src="../assets/js/plugins/pace/pace.min.js"></script>
    <script src="../assets/js/plugins/footable/footable.all.min.js"></script>
    <!-- chat -->
    <script src="../assets/js/chat.js"></script>
    <?php include_once('../includes/script.php') ?> 

    <script>
        $(document).ready(function(){       
        $('.footable').footable();
            $('.footable2').footable();

        });
    </script>

</body>

</html>
<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>

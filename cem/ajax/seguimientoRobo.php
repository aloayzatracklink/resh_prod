<?php include("../../includes/init.php") ?>
<?php 
$idRobo = $_POST['idRobo'];
$roboDetalleObj=Robo::obtenerRobo($idRobo);
?>
<hr/>
<strong>Acciones de seguimiento</strong>
<div id="vertical-timeline" class="vertical-container dark-timeline"><?php SeguimientoRobo::obtenerSeguimientoRoboParaMostrar($roboDetalleObj->idrobo) ?>
	<div class="vertical-timeline-block">
		<div class="vertical-timeline-icon red-bg">
			<i class="fa fa-exclamation-circle"></i>
		</div>
		<div class="vertical-timeline-content">
			<p>Central de emergencia recibe llamada de <strong><?php echo $roboDetalleObj->nombreInformante ?></strong>  con DNI Numero <?php echo $roboDetalleObj->dniInformante ?> quien reporta el robo de vehículo con placa: <h2> <?php echo $roboDetalleObj->placaRobo ?></h2> pide que nos contactemos al número <strong><?php echo $roboDetalleObj->numeroContacto ?></strong>
			</p><br>
			<span class="vertical-date small text-muted"> <?php echo hace ($roboDetalleObj->fechaRegistro) ?></span>
			<small> <br>
				<strong>Operador a cargo</strong> -- <cite title="" data-original-title=""><?php $usuarioAsesor= Usuario::obtenerUsuario($roboDetalleObj->idusuario); echo $usuarioAsesor->usuarioNombre;echo "&nbsp;"; echo $usuarioAsesor->usuarioApellidoPaterno  ?></cite></small>             
			</div>
		</div>        
	</div>

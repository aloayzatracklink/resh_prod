<?php
require_once("../../includes/init.php");
if(isset($_POST)) {
 ?>

 <?php $obje = SeguimientoPK::obtenerListEtiquetaSegPK(""); $n=0;
 if($obje){
  foreach ($obje as $var):
   $n++; ?>
   <tr class="gradeX">
    <td class="text-left "><?= $n; ?></td>
    <td class="text-left">
        <input  type="hidden" name="idst[]"  value="<?= $var->id ?>"  >
     <?= $var->descripcion ?>
    </td>
    <td class="text-center" style="padding: 0">
     <select style="border: none;margin-bottom: 15px;margin:0" class="border-bottom form-control text-capitalize" required=""  id="ncolor_<?= $n ?>" name="ncolor_<?= $n ?>" aria-required="true">
      <option class="text-capitalize badge-primary" value="primary" <?php echo ($var->color=="primary")?"selected":"";?> >Primary</option>
      <option class="text-capitalize badge-info" value="info" <?php echo ($var->color=="info")?"selected":"";?> >Info</option>
      <option class="text-capitalize badge-success" value="success" <?php echo ($var->color=="success")?"selected":"";?> >Success</option>
      <option class="text-capitalize badge-warning" value="warning" <?php echo ($var->color=="warning")?"selected":"";?> >Warning</option>
      <option class="text-capitalize badge-danger" value="danger" <?php echo ($var->color=="danger")?"selected":"";?> >Danger</option>
     </select>
    </td>
   </tr>
  <?php endforeach;?>
 <?php } else{?>
  <tr>
   <td colspan="16" class="text-center">No hay información Registrada</td>
  </tr>
 <?php }?>
 <?php
}
?>

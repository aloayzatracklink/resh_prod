<?php require_once("../../includes/init.php");
$bateriaObj=new BateriaTrack;
$datos=$bateriaObj->obtenerAlertasHoy();
if ($datos) {
	$n=1;
	foreach ($datos as $var) {
        $nuevaFechaF=date("d-m-Y",strtotime($var->fecha));
        $nuevaHoraF=date("H:i",strtotime($var->hora));
	?>
		<tr>
			<td><?= $var->unitId ?></td>
			<td><?= $nuevaFechaF ?></td>
			<td><?php $var->hora; echo $nuevaHoraF ?></td>
			<?php $usuarioObj=Usuario::obtenerUsuario($var->idUsuarioCargo);  ?>
			<td style="background-color: <?= ($usuarioObj->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>"><?php  echo $usuarioObj->usuarioNombre ?> <?= $usuarioObj->usuarioApellidoPaterno?></td>
			<td><?= hace1($var->fechaSubida) ?></td>
			<td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasG" data-id="<?= $var->alertaSubidaId ?>">Elegir <i class="fa fa-cog"></i></button></td>
		</tr>	
	<?php
	}
}
?>

<?php include("../../includes/init.php") ?>
<?php if (isset($_POST['idChoque'])): ?>

<?php 

$idChoque = $_POST['idChoque'];
$choqueObj=new Choque;
$dadosChoque=$choqueObj->obtenerDatosDeChoquePorId($idChoque);
?>
<hr/>
<strong>Acciones de seguimiento</strong>
<div id="vertical-timeline" class="vertical-container dark-timeline"><?php $dataChoque=SeguimientoChoque::obtenerDatosChoquesSeguimiento($idChoque); $n=1; ?>
<?php if ($dataChoque): ?>
<?php foreach ($dataChoque as $var): ?>
<div class="vertical-timeline-block">
                    <div class="vertical-timeline-icon gray-bg">
                        <i class="fa <?php if ($var->tipoContacto==1): ?>
                        	fa-phone-square
                        <?php elseif($var->tipoContacto==2): ?>
                        	fa fa-phone
                        <?php elseif ($var->tipoContacto==3): ?>fa-envelope-o                  	
                        <?php else: ?>
        					 fa-file-text-o	               	
                        <?php endif ?>-circle"></i>
                    </div>
                    <div class="vertical-timeline-content">
                        <p><?php if ($var->tipoContacto==1): ?>
                        	Llamada realizada a
                        <?php elseif($var->tipoContacto==2): ?>
                        	Llamada recibida de
                        <?php elseif ($var->tipoContacto==3): ?> 
                        	Correo enviado a           	
                        <?php else: ?>
                        <?php endif ?><strong> <?php echo $var->nombre ?></strong></p>
                        <p><?php echo $var->detalles ?>
                        <?php echo $var->notas ?>
                        </p>
                        <span class="vertical-date small text-muted"> <?php echo hace($var->fechaRegistro) ?> </span>
                        <small> <br>
                         -- <cite title="" data-original-title=""><?php $usuarioAsesor= Usuario::obtenerUsuario($var->idusuario); echo $usuarioAsesor->usuarioNombre;echo "&nbsp;"; echo $usuarioAsesor->usuarioApellidoPaterno  ?></cite></small>             
                    </div>
                </div>	
<?php endforeach ?>
<?php endif ?>
	<div class="vertical-timeline-block">
		<div class="vertical-timeline-icon red-bg">
			<i class="fa fa-exclamation-circle"></i>
		</div>
		<div class="vertical-timeline-content">
			<p>Se registra evento Crash en la plataforma <?php echo $dadosChoque->plataforma ?> correspondiente al cliente <strong><?php echo $dadosChoque->propietario ?></strong>  con Identificacion Numero <?php echo $dadosChoque->idcliente ?>, el crash se registró con una intensidad de <?php echo $retVal = (empty($dadosChoque->impacto)) ? "<span class='text-danger'>No se registro</span>" : $dadosChoque->impacto ; ?> en la siguiente dirección: <?php echo $retVal = (empty($dadosChoque->lugar)) ? "<span class='text-danger'>No se registro</span>" : $dadosChoque->lugar ; ?>
			cliente cuenta con Plan <?php echo $retVal = (empty($dadosChoque->plan)) ? "<span class='text-danger'>No se registro</span>" : $dadosChoque->plan ; ?>. Contrato <?php echo vence($dadosChoque->vence) ?> <?php echo formatoSinHora($dadosChoque->vence) ?>
			</p><br>
			<span class="vertical-date small text-muted"> <?php echo hace ($dadosChoque->fechaRegistro) ?></span>
			<small> <br>
				<strong>Operador a cargo</strong> -- <cite title="" data-original-title=""><?php $usuarioAsesor= Usuario::obtenerUsuario($dadosChoque->idusuario); echo $usuarioAsesor->usuarioNombre;echo "&nbsp;"; echo $usuarioAsesor->usuarioApellidoPaterno  ?></cite></small>             
			</div>
		</div>        
	</div>
    
<?php endif ?>

<?php

  if (isset($_POST['message']) === true) {
        $message = trim($_POST['message']);
        if(empty($message) === false) {
            $seguimientoChoqueObj=new SeguimientoChoque;
            $seguimientoChoqueObj->notas=$message;
            $seguimientoChoqueObj->idchoque=$_POST['idChoque'];
            if ($seguimientoChoqueObj->registrarNota()) {

            }else{
                echo "Fail detected parte 2";
            }
          /* $chatObj=new Chat();
           $chatObj->registraChat($message);*/
        }
    }

     ?>
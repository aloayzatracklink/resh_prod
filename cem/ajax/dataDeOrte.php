<?php
if (isset($_POST['numeroOrte'])) {
	include_once("../../includes/init.php");
    $orte=$_POST['numeroOrte'];
    $orden=Onix::obtenerIdsdeOrte($orte);
    $clienteObj=Onix::obtenerDatosDesdeOrden($orden);
	if ($clienteObj) {
        $plan=Onix::obtenerPlanDeServicio($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation);
		$cantidadObjeto=Onix::obtenerCantididadVehiculos($clienteObj->idcustomer,$clienteObj->idlocation);
        $planobj=Onix::obtenerPlanDeServicio($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation);
        $vencimientoDatos=Onix::obtenerFechaDeVencimiento($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation);	
        $tiempo="";
  ?>
<input type="hidden" name='plan'  value="<?= $plan->articulo ?>">
<div style="padding-top: 11px; padding-bottom: 10px;">
    <div class="row">
    <div class="row">
    <?php if ($cantidadObjeto){ ?>
        <?php if ($cantidadObjeto->cantidad>1){ ?>
        <h4 class="text-center text-warning">ATENCION ! Cliente tiene <?php echo $cantidadObjeto->cantidad ?> Vehiculos en Tracklink</h4>	
        <?php } ?>
    <?php } ?>
    <div class="form-group col-md-3">
        <label for="Observaciones" >DNI/RUC</label>
        <input style="border: none;" type="text" placeholder="Web" id="Observaciones" <?php echo $retVal = (!empty($clienteObj->dni)) ? "" : "" ; ?> class="border-bottom form-control"  value="<?php echo $dniRuc = (!empty($clienteObj->dni)) ? $clienteObj->dni : "" ; ?>">
        <input type="hidden" value="<?php echo $dniRuc ?>">
    </div>
    <div class="form-group col-md-9">
        <label for="numeroTecnico" >Nombre de cliente*</label>
        <input  style="border: none;" type="text" name="nombreCliente"  id="numeroTecnico" class="border-bottom form-control" required value="<?php echo $nombres = (!empty($clienteObj->nombre)) ? "$clienteObj->apellidos"." "."$clienteObj->nombre" : "$clienteObj->apellidos" ; ?> ">
        <input type="hidden" value="<?php echo $nombres ?>">
    </div>
    </div>
<div class="row">
    <div class="form-group col-md-3">
        <label for="numeroTecnico" >Placa Vehiculo*</label>
        <input  style="border: none;"  type="text" placeholder="Placa" id="numeroTecnico" name="placa" class="border-bottom form-control" required value="<?php echo $retVal = (!empty($clienteObj->placa)) ? "$clienteObj->placa" : "" ; ?>"  >
    </div>
<div class="form-group col-md-3">
    <label for="Observaciones"  >Marca *</label>
    <input required  style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"  value="<?php echo $retVal = (!empty($clienteObj->marca)) ? "$clienteObj->marca" : "" ; ?>" >
</div>
<div class="form-group col-md-3">
    <label for="Observaciones" >Modelo*</label>
    <input required  style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="modelo" class="border-bottom form-control"   value="<?php echo $retVal = (!empty($clienteObj->modelo)) ? "$clienteObj->modelo" : "" ; ?>">
</div>
<div class="form-group col-md-3">
    <label for="color"  >Color</label>
    <input  style="border: none;"  type="text" placeholder="Color" id="color" class="border-bottom form-control" name="modelo"  value="<?php echo $retVal = (!empty($clienteObj->color)) ? "$clienteObj->color" : "" ; ?>">
</div>	
</div>
<div class="row">
<div class="form-group col-md-6">
    <label for="Observaciones" >Chasis*</label>
    <input required   style="border: none;" type="text" placeholder="Chasis" id="Observaciones" class="border-bottom form-control" name="chasis" value="<?php echo $retVal = (!empty($clienteObj->chasis)) ? "$clienteObj->chasis" : "$chasis" ; ?>">
</div>
<div class="form-group col-md-6">
    <label for="Observaciones" >Motor</label>
    <input  style="border: none;"  type="text" placeholder="Observaciones" id="Observaciones" class="border-bottom form-control" name="motor" value="<?php echo $retVal = (!empty($clienteObj->motor)) ? "$clienteObj->motor" : "Actualizar" ; ?>">
</div>
</div>
    <div class="row well">
        <div class="form-group col-md-6" >
            <label for="Observaciones"  >Observaciones Onyx </label>
            <div style="border-right: solid;height: 90px;
overflow-y: auto;">
            <label class="border-" ><?php echo utf8_encode($clienteObj->comentarios) ?></label>
            </div>
        </div>
        <div class="col-md-6 form-group"  >
        <label for="">Ordenes de Vehículo</label>
    <div style="height: 90px;
overflow-y: auto;">
            <?php $objOrdenes=Onix::obtenerOrdenesVehiculo($clienteObj->idcustomer,$clienteObj->idvehicle,$clienteObj->idlocation) ?>
            <?php foreach ($objOrdenes as $var): ?>
                <?= $orden=$var->idlocation."-".$var->idorder ?> <?= utf8_encode($var->articulo) ?><br>
                    <?php   $DataOrd=Onix::obtenerDatosFacturacion($orden) ?>
                    <?php   $DataOrt=Onix::obtenerORTE($orden) ?>
                    <?= ($DataOrd)?$DataOrd->serie.$DataOrd->numeroFactura."  $".round($DataOrd->precioO*1.18,2):""; ?>
                    <?= ($DataOrt->orteO!='')?" ORTE ".$DataOrt->orteO." ":'' ?> <small><?= haceMinimoAnio($var->fechaOrden) ?></small>
                    <hr>
            <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
</div>
<?php }else{ ?>
<div class="" style="padding-top: 11px; padding-bottom: 10px;">
<div class="row">
    <div class="row">
        <div class="form-group col-md-12">
            <label for="numeroTecnico" >Nombre de cliente*</label>
            <input  style="border: none;" type="text" name="nombreCliente"  id="numeroTecnico" class="border-bottom form-control" required>
        </div>
    </div>
<div class="row">
    <div class="form-group col-md-3">
        <label for="numeroTecnico" >Placa Vehiculo*</label>
        <input  style="border: none;"  type="text" placeholder="Placa" id="numeroTecnico" name="placa" class="border-bottom form-control" required  >
    </div>
    <div class="form-group col-md-3">
        <label for="Observaciones"  >Marca *</label>
        <input required  style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="marca" class="border-bottom form-control"   >
    </div>
    <div class="form-group col-md-3">
        <label for="Observaciones" >Modelo*</label>
        <input required  style="border: none;" type="text" placeholder="Observaciones" id="Observaciones" name="modelo" class="border-bottom form-control"   >
    </div>
    <div class="form-group col-md-3">
        <label for="color"  >Color</label>
        <input  style="border: none;"  type="text" placeholder="Color" id="color" class="border-bottom form-control" name="modelo"  >
    </div>	
</div>
<div class="row">
    <div class="form-group col-md-6">
        <label for="Observaciones" >Chasis*</label>
        <input required   style="border: none;" type="text" placeholder="Chasis" id="Observaciones" class="border-bottom form-control" name="chasis" >
    </div>
    <div class="form-group col-md-6">
        <label for="Observaciones" >Motor</label>
        <input  style="border: none;"  type="text" placeholder="Observaciones" id="Observaciones" class="border-bottom form-control" name="motor" >
    </div>
</div>
</div>
</div>         
<?php
                     }
 } ?>


	<?php 
	require_once("../../includes/init.php");
	 ?>
	<?php $chequeosObj=Chequeos::obtenerChequeoTrackYo() ?>
    <?php $chequeoDataAll=Chequeos::obtenerChequeoTrackOtrasAreas() ?>
  
    <?php if ($chequeoDataAll): ?>        
    <?php foreach ($chequeoDataAll as $var1): ?>
     <tr>
        <td><?= $var1->placa ?></td>
        <td><?= $var1->nombres ?><br> <small><?= $var1->plan ?></small></td>
        <td><?= formatoHaceEn($var1->fechaHoraChequeoLinea) ?></td>
        <td><?= obtenerEstado($var1->estado) ?></td>
        <?php $usuarioCa=Usuario::obtenerUsuario($var1->idCargo); ?>
        <?php $usuarioCr=Usuario::obtenerUsuario($var1->idCreador); ?>
        <td style="background-color: <?= $retVal = ($usuarioCa->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>" ><?= $usuarioCa->usuarioNombre." ".$usuarioCa->usuarioApellidoPaterno ?></td>
        <td><?= $usuarioCr->usuarioNombre." ".$usuarioCr->usuarioApellidoPaterno ?></td>
        <td><button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#chequeo" data-id="<?= $var1->idchequeo ?>">Gestionar <i class="fa fa-cog"> </i></button></td>
    </tr>                            
    <?php endforeach ?>      
        <tr>
            <td colspan="6" class="text-center"> -------- * -------- </td>
        </tr>
    <?php endif ?>
      <?php if ($chequeosObj): ?>
     <?php foreach ($chequeosObj as $var): ?>                    
         <tr>
            <td><?= $var->placa ?></td>
            <td><?= $var->nombres ?> <br><small><?= $var->plan ?></small></td>
            <td><?= formatoHaceEn($var->fechaHoraChequeoLinea) ?></td>
            <td><?= obtenerEstado($var->estado) ?></td>
            <?php $usuarioCa=Usuario::obtenerUsuario($var->idCargo); ?>
            <?php $usuarioCr=Usuario::obtenerUsuario($var->idCreador); ?>
            <td style="background-color: <?= $retVal = ($usuarioCa->usuarioNombre=="Libre") ? "#8befda" : "#86d7fd" ; ?>" ><?= $usuarioCa->usuarioNombre." ".$usuarioCa->usuarioApellidoPaterno ?></td>
            <td><?= $usuarioCr->usuarioNombre." ".$usuarioCr->usuarioApellidoPaterno ?></td>
            <td><button class="btn btn-primary " style="padding-top: 0;padding-bottom: 0"  data-toggle="modal" data-target="#chequeo" data-id="<?= $var->idchequeo ?>">Gestionar <i class="fa fa-cog"> </i></button></td>
        </tr>                                   
    <?php endforeach ?>  
    <?php endif ?>


  



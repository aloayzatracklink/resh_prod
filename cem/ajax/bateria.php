<?php require_once("../../includes/init.php"); ?>
<?php 
if (!$session->yaEstaIngresado()) {
    irA("ingresar");
} 
if (isset($_POST['method']) === true && empty($_POST['method']) === false) {
    $method = trim($_POST['method']);
    if ($method === 'fetch') {
        
        if (empty(BateriaTrack::mostrarUltimoRegistroSubido())===true) {  
                              
        } else {
            BateriaTrack::mostrarUltimoRegistroSubido();
        }
    } else if ($method === 'throw' && isset($_POST['message']) === true) {
        $message = trim($_POST['message']);
        if(empty($message) === false) {
           $bateriaObj=new BateriaTrack();
           $bateriaObj->registrarChasis($message);
        }
    }
}
?>  
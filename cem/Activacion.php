<?php
    $nav="Activacion";
    include("../includes/head.php");
    if (!$session->yaEstaIngresado()) {
        irA("../ingresar");
    }
    $obMenu=Usuario::obtenerMenu($session->idusuario);


if ($obMenu->instalaciones=="si"):
     if (isset($_POST['btnblink'])) {
         if (Token::verificar($_POST['btnblink'])) {
             if(isset($_POST['idlinkHomo'])) {
                 $trama = TramaIntegracionTY::getActivationIdTrama($_POST['idlinkHomo']);
                 // notificar instalacion toyota ---  inicio
                 $path =  "/sendStatusVehicle";
                 $param = array(
                     "chasis" => $trama->chasis,
                     "estado" => "activated"
                 );
                 $rptaService = APIToyota::PostService($path,$param);

                 $echo[] = "Se  Activo con exito el chasis ".$trama->chasis;
                 // notificar instalacion toyota ---  fin
                 //echo $rptaService;
                 if($rptaService) {
                     TramaIntegracionTY::updateActivacion($_POST['idlinkHomo']);
                     $operacionObj = new Operacion();
                     $vehiObj = new Vehiculo();
                     $datosIObj = new DatosInstalacion;

                     $vehiObj->idvehiculo = $trama->idvehiculo;
                     $vehiObj->cambiarEstadoInstaladosinbienvenida();
                     $datosIObj->idoperacion= $trama->idoperacion;
                     $datosIObj->actualizarDatosActivacionDI();
                     $operacionObj->idoperacion = $trama->idoperacion;
                     $operacionObj->actualizarDatosActivacionOperacion();
                 }

             }

         }   else {
                    $errores[] = "El formulario ya fue procesado, vericar.";
                }
     }


?>
<link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="../assets/css/sweetalert2.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/css2/style2.css">
<link rel="stylesheet" href="../assets/css2/style3.css">

<style>
    .table-cabecera-horario{
        background:#075598!important;
        color:white !important;
        vertical-align: middle!important;
    }
    .table-cabecera-horario2{
        background:#164764 !important;
        color:white !important;
        vertical-align: middle!important;
    }
    .table-cabecera-horario3{
        background: #096452 !important;
        color:white !important;
    }
    .table-cabecera-horario4{
        background: #0e2e79 !important;
        color:white !important;
    }

    .table-scroll {
        position:relative;
        max-width:1600px;
        margin:auto;
        overflow:hidden;
        /*border:.5px solid #000;*/
    }

    .table-wrap {
        width:100%;
        overflow:auto;
    }

    .table-scroll table {
        width:100%;
        margin:auto;
        border-collapse:separate;
        border-spacing:0;
    }

    .table-scroll th, .table-scroll td {
        padding:5px 10px;
        border-bottom:1px solid;
        border-right:1px solid;
        border-color: #DDDDDD;;
        #background:#fff;
        color:rgb(103, 106, 108) ;
        white-space:nowrap;
        vertical-align:top;
        font-size: 12px
    }

    .table-scroll thead, .table-scroll tfoot {
        background: #f9f9f9;
    }

    .bordertab{
        border-bottom: 1px #e1d0d0 solid !important;
        border-left: 1px #e1d0d0 solid !important;
    }
</style>
<body>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <?php include("../includes/menuIzquierdo.php") ?>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../includes/navegacionArriba.php") ?>
            </nav>
        </div>
        <div class="wrapper wrapper-content">
            <div class="row ibox float-e-margins ibox-content">
                <div class="col-md-12" style="padding: 0">
                    <div id="table-scroll" class="table-scroll">
                        <div class="table-wrap">
                            <div class="col-md-2">
                                <label for="cboEstado">Estado</label>
                                <select style="border: none;margin-bottom: 15px" class="border-bottom form-control text-capitalize" required=""  id="cboEstado" name="cboEstado" aria-required="true">
                                    <option value="">Todos</option>
                                    <option value="1">Activados</option>
                                    <option value="0" selected>No Activado</option>
                                </select>
                            </div>


                            <input type="text" class="form-control input-sm m-b-xs" id="filterrr"
                                   placeholder="Buscar" style="margin-bottom: 15px">
                            <table class="table main-table" >
                                <thead>
                                    <tr>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"></th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Tipo Registro</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Asegurado</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Chasis</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Placa</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Kms</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Estado</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Fecha Solicitud</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Fecha Activacion</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1">Cargo</th>
                                        <th class="text-center fixed-side table-cabecera-horario" colspan="1" rowspan="1"> Acción</th>
                                    </tr>
                                </thead>
                                <tbody id="resultado">


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("../includes/footer.php") ?>
    </div>
</div>

<button type="hidden" name="guardaactivacion" id="guardaactivacion" class="btn btn-primary" style="display: none" value="<?php echo Token::generar() ?>" ></button>
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/plugins/clockpicker/clockpicker.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/select2/select2.full.min.js"></script>
<script src="../assets/bootbox/bootbox.all.js"></script>

<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="../assets/js/jquery-confirm.min.js"></script>

<script src="../assets/js/plugins/ladda/spin.min.js"></script>
<script src="../assets/js/plugins/ladda/ladda.min.js"></script>
<script src="../assets/js/plugins/ladda/ladda.jquery.min.js"></script>
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>

<script src="../assets/bootbox/bootbox.all.js"></script>
<style>
    .popover {
        z-index: 999999;
    }
</style>
<?php include_once('../includes/script.php') ?>
<script>

    <?php if (!empty($errores)): ?>
    <?php foreach ($errores as $error): ?>
    toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
    <?php endforeach ?>
    <?php endif ?>
    <?php if(!empty($echo)): ?>
    <?php foreach($echo as $ok): ?>
    toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
    <?php endforeach ?>
    <?php endif ?>
</script>
<script>



    $(document).ready(function(){
        $('.footable').footable();
        $('.footable2').footable();
    });


    $('body').on('hidden.bs.modal', function () {
        if($('.modal.in').length > 0)
        {
            $('body').addClass('modal-open');
        }
    });

    $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
    $.ajax({
        type : 'POST',
        url  : 'pag/resultadoBusquedaActivacionToyota',
        data :  {datos:'',estd:'0',page:1},
        success : function(data)
        {
            $("#resultado").html(data);
        }
    });

    $("#cboEstado").change(function() {
        $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
        var datosS = $("#filterrr").val();
        datosS = datosS.replace('<>/"', '');
        var estd=$("#cboEstado option:selected").val()
        $.ajax({
            type : 'POST',
            url  : 'pag/resultadoBusquedaActivacionToyota',
            data :  {datos:datosS,estd:estd},
            success : function(data)
            {
                $("#resultado").html(data);
            }
        });
        return false;

    })



    $("#filterrr").keypress(function(e)
    {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){
            $("#resultado").html(" <tr>\n            <td colspan=\"16\" class=\"text-center\"> Buscando ...</td>\n       </tr>");
            var datosS = $(this).val();
            datosS = datosS.replace('<>/"', '');
            var estd=$("#cboEstado option:selected").val()
            var sist=$("#cboSistema option:selected").val()
            $.ajax({
                type : 'POST',
                url  : 'pag/resultadoBusquedaActivacionToyota',
                data :  {datos:datosS,estd:estd},
                success : function(data)
                {
                    $("#resultado").html(data);
                }
            });
            return false;
        }
    });



    function btnLink(e){
        var rowid = $(e).data('id');
        var ndata = $(e).data('hlink');

        var l = $('.ladda-button-dlink'+rowid+'').ladda();
        l.ladda( 'start' );

        bootbox.confirm({
            title: "<h3><strong>Atención!</strong></h3>",
            message: "Esta Seguro que deseas Activar  el Chasis    "+ndata+" ?",
            size: 'small',
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancelar'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirmar'
                }
            },
            callback: function (result) {
                if(result){
                    $("#btnblink"+rowid).prop('disabled',false);
                    $("#idlinkHomo"+rowid).prop('disabled',false);
                    $("#btnblink"+rowid).val($("#guardaactivacion").val());
                    $("#FRMLINK"+rowid).submit();
                    return true;
                }else{
                    l.ladda('stop');
                    //  $("#liberarTec").on("click",$(this)).find("#liberarTec");
                }
            }
        });
        return false;
    }
</script>
<?php else: ?>
    <?php $session->salir();
    echo "NO NO NO NO ; presiona F5"
    ?>
<?php endif ?>

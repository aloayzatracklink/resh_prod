<?php
class anomalia extends Recursos
{
      public $tipo;
      public $sistema;
      public $idcustomer;
      public $idlocation;
      public $idvehicle;
      public $cliente;
      public $chasis;
      public $placa;
      public $asunto;
      public $cantNotify;
      public $sendMail;
      public $observaciones;
      public $create_date;
      public $update_at;
      public $iduseruso;
      public $cantdias;

      public $color;
      public $id;
      public $descripcion;
      public $accion;

      public $cantidad;

      public $body;
      public $class_id;
      public $idusuario;
      public $fecha;
      public $created;
      public $creation_at;
      public $last_modify;
      public $updated_at;
      public $update_at_auto;


      public $estado;
      public $seguimiento;
      public $created_at;


    public function obtenerListEstadoAnomalia($idest) {
          $fil="" ;
          if($idest!=""){
               $fil=" and  ea.id='$idest'";
           }
            $sql="select ea.id,ea.descripcion,ea.accion,ea.color from estados_anomalias ea where 1=1 $fil";
            $rslt = self::sentencia($sql);
            return $rslt;
      }


    public function ttobtenerlstanomalia($datos,$est,$sist) {
        $filt="";
        if($est!=""){
            $filt.=" and naa.sendMail='$est' ";
        }
        if($sist!=""){
            $filt.=" and naa.sistema='$sist' ";
        }

        $sql="  select count(*) as cantidad from (
               select naa.id,naa.tipo,sistema ,naa.idcustomer ,naa.idlocation ,naa.idvehicle ,naa.cliente ,naa.chasis ,naa.placa,naa.asunto ,naa.cantNotify ,naa.sendMail,naa.observaciones,naa.update_at_auto,
              DATE_FORMAT(naa.created_at,'%Y-%m-%d') as create_date,naa.update_at ,iduseruso,CONCAT('Hace ',DATEDIFF(DATE_FORMAT(NOW(),'%Y-%m-%d'),DATE_FORMAT(naa.created_at,'%Y-%m-%d')), ' dias') cantdias
              from notificacion_auto_anomalia naa where 1=1 and (naa.cliente like '%$datos%' or naa.placa like '%$datos%' or naa.chasis like '%$datos%') $filt
              order by id desc )dat
            ";

        $rslt = self::sentencia($sql);
        return !empty($rslt)? array_shift($rslt):false;
    }

    public function obtenerlstanomalia($datos,$est,$sist,$posisionDePagina,$registrosPorPagina) {
        $filt="";
        if($est!=""){
            $filt.=" and naa.sendMail='$est' ";
        }
        if($sist!=""){
            $filt.=" and naa.sistema='$sist' ";
        }
        $sql="select naa.id, naa.tipo,sistema ,naa.idcustomer ,naa.idlocation ,naa.idvehicle ,naa.cliente ,naa.chasis ,naa.placa,naa.asunto ,naa.cantNotify ,naa.sendMail,naa.observaciones,naa.update_at_auto,
              DATE_FORMAT(naa.created_at,'%Y-%m-%d') as create_date,naa.update_at ,iduseruso,CONCAT('Hace ',DATEDIFF(DATE_FORMAT(NOW(),'%Y-%m-%d'),DATE_FORMAT(naa.ultimasenalvalidafecha,'%Y-%m-%d')),' dias') cantdias
              from notificacion_auto_anomalia naa where 1=1 and (naa.cliente like '%$datos%' or naa.placa like '%$datos%' or naa.chasis like '%$datos%') $filt
              order by id desc LIMIT $posisionDePagina, $registrosPorPagina";
        $rslt = self::sentencia($sql);
        return $rslt;
    }

    public function finanomaliid($id) {

        $sql=" 
               select naa.id,naa.tipo,sistema ,naa.idcustomer ,naa.idlocation ,naa.idvehicle ,naa.cliente ,naa.chasis ,naa.placa,naa.asunto ,naa.cantNotify ,naa.sendMail,naa.observaciones,naa.iduseruso,
              DATE_FORMAT(naa.created_at,'%Y-%m-%d') as create_date,naa.update_at ,iduseruso,CONCAT('Hace ',DATEDIFF(DATE_FORMAT(NOW(),'%Y-%m-%d'),DATE_FORMAT(naa.created_at,'%Y-%m-%d')), ' dias') cantdias
              from notificacion_auto_anomalia naa where 1=1 and naa.id='$id'
              order by id desc 
            ";

        $rslt = self::sentencia($sql);
        return !empty($rslt)? array_shift($rslt):false;
    }


    public function asignarCargo(){
        global $db;
        global $session;
        $sql="UPDATE  notificacion_auto_anomalia SET ";
        $sql.="iduseruso='";
        $sql.=$db->escapeString($session->idusuario)."' ";
        $sql.=" WHERE id=";
        $sql.=$db->escapeString($this->id);
        return ($db->query($sql)) ? true:false;

    }


    public function updateAnomaliaEstado(){
        global $db;
        global $session;
        $this->fechaActivacion=date("Y-m-d H:i:s");
        $sql="UPDATE  notificacion_auto_anomalia SET ";
        $sql.="sendMail='";
        $sql.=$db->escapeString($this->sendMail)."',update_at='";
        $sql.=$db->escapeString($this->fechaActivacion)."' ";
        $sql.=" WHERE id=";
        $sql.=$db->escapeString($this->id);
        return ($db->query($sql)) ? true:false;
    }

    public function saveSeg($seg){
        global $session;
        global $db;
        $sql="INSERT INTO seguimientos_detalle (body,class,class_id,idusuario) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString($seg['body'])."' , '";
        $sql.= $db->escapeString('ANOMALIA')."' , '";
        $sql.= $db->escapeString($seg['idanom'])."' , '";
        $sql.= $db->escapeString($session->idusuario)."' )";
        if ($db->query($sql)) {
            $this->AgendarSeguimiento($seg);
            return True;
        }
        else{
            return False;
        }
    }



    public function saveEstado($seg){
        global $session;
        global $db;

        $this->crea_at = date("Y-m-d H:i:s");

        $sql="INSERT INTO estados_anomalias (descripcion,accion,color,created_at,idusuario) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString($seg['body'])."' , '";
        $sql.= $db->escapeString('1')."' , '";
        $sql.= $db->escapeString('primary')."' , '";
        $sql.= $this->crea_at."' , '";
        $sql.= $db->escapeString($session->idusuario)."' )";
        if ($db->query($sql)) {
            return True;
        }
        else{
            return False;
        }
    }

    public function AgendarSeguimiento($seg){
        global $db;

        $username = 'onyx';
        $password = 'bwkvwvyfhijqmuaxye';
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Basic '. base64_encode("$username:$password")
        );

        $bodyDatta = array(
            "idcustomer" => $seg["idcustomer"],
            "idlocation" => $seg["idlocation"],
            "idvehicle" => $seg["idvehicle"],
            "subject" => $seg['subject'],
            "message" => $seg['body'],
        );

        API::POSTB("http://192.168.2.26:30090/onyx/saveAgendaNotification", $headers, $bodyDatta);

        return true ;
    }


    public static function getSEg($id){
        $obj=self::sentencia("select id,body,class_id,idusuario,fecha,created ,creation_at ,last_modify ,updated_at from seguimientos_detalle where class_id='$id' and class = 'ANOMALIA'");
        return !empty($obj)?$obj:[];
    }



    public function updateEstado()
    {
        global $db;
        global $session;
        $this->update_at = date("Y-m-d H:i:s");
        $sql = " UPDATE estados_anomalias et SET ";
        $sql .= " et.accion ='" . $db->escapeString($this->accion) . "',";
        $sql .= " et.color ='". $db->escapeString($this->color) ."',";
        $sql .= " et.update_at ='". $this->update_at ."' ,";
        $sql .= " et.update_user ='" . $db->escapeString($session->idusuario) . "' ";
        $sql .= " WHERE et.id ='". $db->escapeString($this->id)."'";

        if ($db->query($sql)) {
            return true;
        } else {
            echo "the fail";
        }
    }



    public function obtenerAnomaliaExcel(){
        $sql=" SELECT  naa.sistema ,naa.cliente ,naa.placa ,naa.chasis ,CONCAT('Hace ',DATEDIFF(DATE_FORMAT(NOW(),'%Y-%m-%d'),DATE_FORMAT(naa.created_at,'%Y-%m-%d')),' dias') cantdias,
              (SELECT ea.descripcion FROM estados_anomalias ea where ea.id=naa.sendMail) as estado,naa.cantNotify,
              (SELECT GROUP_concat(body) FROM seguimientos_detalle sd where sd.class_id =naa.id and sd.class='ANOMALIA') seguimiento,naa.created_at,naa.update_at_auto
              FROM notificacion_auto_anomalia naa
              WHERE 1=1 order by created_at desc; ";
        $dtp=self::sentencia($sql);
        return !empty($dtp)? $dtp:false;
    }

    public function obtenerListaAnomaliaToyota($datos,$est,$sist,$posisionDePagina,$registrosPorPagina) {
        $filt="";
        if($est!=""){
            $filt.=" and naa.sendMail='$est' ";
        }
        if($sist!=""){
            $filt.=" and naa.sistema='$sist' ";
        }
        $sql="SELECT naa.id, naa.tipo,sistema ,naa.idcustomer ,naa.idlocation ,naa.idvehicle ,naa.cliente ,naa.chasis ,naa.placa,naa.asunto ,naa.cantNotify ,naa.sendMail,naa.observaciones,naa.update_at_auto,
            DATE_FORMAT(naa.created_at,'%Y-%m-%d') AS create_date,naa.update_at ,iduseruso,CONCAT('Hace ',DATEDIFF(DATE_FORMAT(NOW(),'%Y-%m-%d'),DATE_FORMAT(naa.created_at,'%Y-%m-%d')),' dias') cantdias
            FROM notificacion_auto_anomalia naa 
            LEFT JOIN toyotaintegracion ty ON naa.chasis =  ty.chasis
            WHERE 1=1
            AND ty.estado = 16 AND (naa.cliente LIKE '%$datos%' OR naa.placa LIKE '%$datos%' OR naa.chasis LIKE '%$datos%')
            $filt
            ORDER BY id DESC LIMIT $posisionDePagina, $registrosPorPagina            ";
        $rslt = self::sentencia($sql);
        return $rslt;
    }

    public function ttObtenerListaAnomaliaToyota(){
        $sql="SELECT COUNT(*) as cantidad
            FROM notificacion_auto_anomalia naa 
            LEFT JOIN toyotaintegracion ty ON naa.chasis =  ty.chasis
            WHERE 1=1
            AND ty.estado = 16
            ORDER BY id DESC";
        $rslt = self::sentencia($sql);
        return !empty($rslt)? array_shift($rslt):false;
    }

}
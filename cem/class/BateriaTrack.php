<?php 

if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
class BateriaTrack extends Recursos
{
	////subir	
	public $alertaSubidaId;
	public $idBateriaOctoSubida;
	public $unitId;
	public $placaVehiculo;
	public $tipoAlerta;
	public $fecha;
	public $hora;
	public $idusuario;
	public $idUsuarioCargo;
	public $fechaSubida;
	public $longitude;
	public $latitude;
	////////////////////////

	/////////////////ATENDIDOS////////////
	public $idAlertaBateriaTrack;
	public $agenda;
	public $tallerDias;
	public $cee;
	public $ok;
	public $cv;
	public $ad;
	public $desS;
	public $des;
	public $ta;
	public $fa;
	public $accionesSegimiento;
	public $fechaAlerta;
	public $fechaRegistro;
	public $horaSubida;
	////////////////////////

	///////LISTA ROJA ///////
	public $idListaRoja;
	public $sim;
	public $placa;
	public $cliente;
	public $descripcion;
	public $motivo;
	public $tipoListaRoja;
	public $idUsuario;
	public $tipo;

	/////////////////////////
	
	///////DATA BASIC //////
	public $nombres;
	public $nombre;
	public $estado;
	public $usuarioWeb;
	public $sistema;
	public $marca;
	public $modelo;
	public $color;
	public $anio;
	public $chasis;
	public $motor;
	public $operadora;
	public $ultimaActualizacion;
	public $idActualizador;
	//////////////////777777

////////////////////DATA COMPLETE HERs777777777777777777777777
	public $fechaOrden;
	public $dni;
	public $articulo;
	public $creador;
	public $precio;
	public $numContrato;
	public $tipoSistema;
	public $concesionario;
	public $financiera;
	public $aseguradora;
	public $telefono1;
	public $telefono2;
	public $celular;
	public $correo;
	public $desde;
	public $hasta;
	public $cantidad;
////////////////////DATA COMPLETE HERs777777777777777777777777
	//////N
	public $nombreCliente;
	///
	public $unitIdNoLlamar;
	///////
	public $fechaAA; 
public	function __construct()
	{
		# code...
	}

	public function subirArchivo(){
		global $db;
		$this->fechaSubida=date("YmdHis");
		$sql="INSERT INTO alertaSubida (unitId,placaVehiculo,tipoAlerta,fecha,hora,idusuario,estado,fechaSubida)";
		$sql.="VALUES ('";
		$sql.= $db->escapeString($this->unitId)."' , '";
		$sql.= $db->escapeString($this->placaVehiculo)."' , '";
		$sql.= $db->escapeString($this->tipoAlerta)."' , '";
		$sql.= $db->escapeString($this->fecha)."' , '";
		$sql.= $db->escapeString($this->hora)."' , '";
		$sql.= $db->escapeString($this->idusuario)."' , '";
		$sql.= $db->escapeString($this->obtenerUltimoSubidoAT())."' , '";
		$sql.= $db->escapeString($this->fechaSubida)."')";
		if ($db->query($sql)) {
			$this->alertaSubidaId=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}
	}

	public function subirArchivoAutoResh(){
		global $db;
		$this->fechaSubida=date("YmdHis");
		$sql="INSERT INTO alertaSubida (unitId,tipoAlerta,fecha,hora,latitude,longitude,estado,fechaSubida)";
		$sql.="VALUES ('";
		$sql.= $db->escapeString(trim($this->unitId))."' , '";
		$sql.= $db->escapeString($this->tipoAlerta)."' , '";
		$sql.= $db->escapeString($this->fecha)."' , '";
		$sql.= $db->escapeString($this->hora)."' , '";
		$sql.= $db->escapeString($this->latitude)."' , '";
		$sql.= $db->escapeString($this->longitude)."' , '";
		$sql.= $db->escapeString($this->obtenerUltimoSubidoAT())."' , '";
		$sql.= $db->escapeString($this->fechaSubida)."')";
		if ($db->query($sql)) {
			$this->alertaSubidaId=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}
	}

	public function subirArchivoAutoReshNoInstalados(){
		global $db;
		$this->fechaSubida=date("YmdHis");
		$sql="INSERT INTO alertaSubida (unitId,tipoAlerta,fecha,hora,latitude,longitude,estado,fechaSubida)";
		$sql.="VALUES ('";
		$sql.= $db->escapeString($this->unitId)."' , '";
		$sql.= $db->escapeString($this->tipoAlerta)."' , '";
		$sql.= $db->escapeString($this->fecha)."' , '";
		$sql.= $db->escapeString($this->hora)."' , '";
		$sql.= $db->escapeString($this->latitude)."' , '";
		$sql.= $db->escapeString($this->longitude)."' , '";
		$sql.="3"."' , '";
		$sql.= $db->escapeString($this->fechaSubida)."')";
		if ($db->query($sql)) {
			$this->alertaSubidaId=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}
	}
/*
	public static function obtenerUsuario($id){
		$obtenerUsuarioQueryArray=self::sentencia("SELECT * FROM usuarios WHERE idusuario = $id LIMIT 1");
		return  !empty($obtenerUsuarioQueryArray) ? array_shift($obtenerUsuarioQueryArray) : false ;
	}

*/	
public function ultimoAlertaTrack(){
	$ultimoAlertaTrack=self::sentencia("SELECT CONCAT(fecha,hora) AS fechaAA FROM alertaSubida  ORDER BY alertaSubidaId DESC LIMIT 1");
	return !empty($ultimoAlertaTrack)? array_shift($ultimoAlertaTrack):false;
	//SELECT count(placaVehiculo)AS cnt,unitId,placaVehiculo FROM `alertaSubida` GROUP BY placaVehiculo HAVING (cnt > 1) ORDER BY `cnt` DESC
}

public function ultimoAlertaTrackTosubir(){
	$ultimoAlertaTrack=self::sentencia("SELECT CONCAT(fecha,hora) AS fechaAA FROM alertaSubida  ORDER BY alertaSubidaId DESC LIMIT 1");
    if (!empty($ultimoAlertaTrack)) {
        $dd=array_shift($ultimoAlertaTrack);
        return $dd->fechaAA; 
    }else{
        return false;
    }
}

// public function obtenerAlertasREalizadasHoy(){
// 	$fechaHoy=date("Ymd");
// 	$obtenerAtendidosHoy=self::sentencia("SELECT * FROM alertaBateriaTrack WHERE unitId = '$this->unitId' AND fechaAlerta LIKE '$fechaHoy'");
// 	return true;SELECT count(placaVehiculo)AS cnt,unitId,placaVehiculo FROM `alertaSubida` GROUP BY placaVehiculo HAVING (cnt > 1)
// 	// :false;	
// }
// public function verificarEnNollamar(){
// 	$this->unitId=trim($this->unitId);
// 	$obtenerNoLlamar=self::sentencia("SELECT * FROM listaRoja WHERE sim = '$this->unitId' AND tipoListaRoja ='No llamar' AND sim<>'' " );
// 	return true;
// 	// return !empty($obtenerNoLlamar)?true:false;
// }

public function obtenerAlertasHoy(){
	$fechaHoy=date("YmdHis");
    $desdeQueHora = new DateTime();
    $desdeQueHora->modify("-400 minutes");
    $hora=$desdeQueHora->format("YmdHms");
    //AND NOT EXISTS(SELECT sim FROM listaRoja lr WHERE TRIM(als.unitId)=TRIM(lr.sim))
    //TIMEDIFF(now(),DATE_FORMAT(CONCAT(als.fecha, als.hora),'%Y-%m-%d %k:%i:%s') ) as tipoAlerta
    $obtenerAlertasHoy=self::sentencia("SELECT als.* FROM alertaSubida als WHERE '$hora' <= CONCAT(als.fecha,als.hora) AND als.fecha<>'BD' AND als.hora <>'BD' AND NOT EXISTS(SELECT sim FROM listaRoja lr WHERE TRIM(als.unitId)=TRIM(lr.sim)) AND NOT EXISTS(SELECT fechaAlerta FROM alertaBateriaTrack at WHERE at.fechaAlerta=CONCAT(als.fecha,als.hora)) AND estado=0");
    return !empty($obtenerAlertasHoy)?$obtenerAlertasHoy:false;
}

public function obtenerAlertasHoyCANTIDAD(){
	$fechaHoy=date("YmdHis");
    $desdeQueHora = new DateTime();
    $desdeQueHora->modify("-600 minutes");
    $hora=$desdeQueHora->format("YmdHms");
    //AND NOT EXISTS(SELECT sim FROM listaRoja lr WHERE TRIM(als.unitId)=TRIM(lr.sim))
    //TIMEDIFF(now(),DATE_FORMAT(CONCAT(als.fecha, als.hora),'%Y-%m-%d %k:%i:%s') ) as tipoAlerta
    $obtenerAlertasHoy=self::sentencia("SELECT COUNT(*) as cantidad FROM alertaSubida als WHERE '$hora' <= CONCAT(als.fecha,als.hora) AND als.fecha<>'BD' AND als.hora <>'BD' AND NOT EXISTS(SELECT sim FROM listaRoja lr WHERE TRIM(als.unitId)=TRIM(lr.sim)) AND NOT EXISTS(SELECT fechaAlerta FROM alertaBateriaTrack at WHERE at.fechaAlerta=CONCAT(als.fecha,als.hora)) AND estado=0");
    $datos=array_shift($obtenerAlertasHoy);
    return $datos->cantidad;
}

public function obtenerUltimoSubidoAT(){
    $sqlobusat=self::sentencia("SELECT CONCAT(fecha,hora) as fechaAlerta FROM alertaSubida WHERE  unitId='$this->unitId' ORDER BY alertaSubidaId DESC LIMIT 1");
    if (!empty($sqlobusat)) {
        $datos=array_shift($sqlobusat);
        $horaU=new DateTime($datos->fechaAlerta);
        $hora=new DateTime($this->fecha.$this->hora);
        $fdif= $hora->diff($horaU);
        $hora=$fdif->format("%H%I%S");
        $dia=$fdif->format("%Y%M%D");
        if($dia<=0){
            if ($hora>='030000') {
                return '0'; 
            }else{
                return '2';
            }
        }else{
            return '0';
        }
    }else{
        return '0';
    }
}

public static  function obtenerUltimoatendido($sim){
    $sqlobusat=self::sentencia("SELECT fechaAlerta FROM alertaBateriaTrack WHERE unitId LIKE '%$sim%' ORDER BY idAlertaBateriaTrack DESC LIMIT 1");
    if (!empty($sqlobusat)) {
        $datos=array_shift($sqlobusat);
        return $datos->fechaAlerta;
    }else{
        return false;
    }
}

public static function asignarCago($unitId){
	global $session;
	global $db;
	$sql="UPDATE alertaSubida SET idUsuarioCargo=".$session->idusuario." WHERE alertaSubidaId ='".$unitId."'";
	if ($db->query($sql)) {
	}
}
	
	public static function obtenerDatosCliente($sim){
		$obtenerDatosClienteQ=self::sentencia("SELECT * FROM dataBasic WHERE unitId = '$sim' LIMIT 1");
		return  !empty($obtenerDatosClienteQ) ? array_shift($obtenerDatosClienteQ) : false ;
	}

	
	public static function obtenerDatosClienteDeChasis($chasis){
		$obtenerDatosClienteDeChasisQ=self::sentencia("SELECT * FROM dataComplete WHERE chasis='$chasis' AND precio<>'0' AND numContrato<>'0' ORDER BY idDataComplete DESC LIMIT 1");
		return  !empty($obtenerDatosClienteDeChasisQ) ? array_shift($obtenerDatosClienteDeChasisQ) : false ;
	}

	public static function obtenerHoraDeUltimaSubida($alertaSubidaId){
		$fechaHoy=date("Ymd");
		$obtenerQ=self::sentencia("SELECT fechaSubida FROM alertaSubida WHERE alertaSubidaId='$alertaSubidaId'  ");
		return !empty($obtenerQ) ? array_shift($obtenerQ): false;
	}

	public static function obtenerAlertasAtendidasHoy(){
    //$fechaHoy=date("Ymd");
    $desdeCuando = new DateTime();
    $desdeCuando->modify("-1 days");
     $fechaHoy=$desdeCuando->format("Ymd");
		// $obtenerAlertasHoy=self::sentencia("SELECT DISTINCT unitId,fechaSubida FROM alertaSubida WHERE fecha BETWEEN $fechaHoy AND $fechaHoy ");
		$n=0;
		// foreach ($obtenerAlertasHoy as $hoy) {
		 $obtenerAtendidosHoy=self::sentencia("SELECT * FROM alertaBateriaTrack WHERE  fechaAlerta >= '$fechaHoy'  ORDER BY idAlertaBateriaTrack DESC LIMIT 50");
		 return !empty($obtenerAtendidosHoy)?$obtenerAtendidosHoy:false;

		// }
	}


	public function registrarAlertaTrack(){
		global $db;
		global $session;
		$this->idusuario=$session->idusuario;
		$sql="INSERT INTO alertaBateriaTrack (unitId,agenda,tallerDias,cee,ta,ok,cv,ad,desS,des,accionesSegimiento,idusuario,fechaAlerta,fechaRegistro)";
		$sql.="VALUES (' ";
		$sql.= $db->escapeString($this->unitId)."','";
		$sql.= $db->escapeString($this->agenda)."','";
		$sql.= $db->escapeString($this->tallerDias)."','";
		$sql.= $db->escapeString($this->cee)."','";
		$sql.= $db->escapeString($this->ta)."','";
		$sql.= $db->escapeString($this->ok)."','";
		$sql.= $db->escapeString($this->cv)."','";
		$sql.= $db->escapeString($this->ad)."','";
		$sql.= $db->escapeString($this->desS)."','";
		$sql.= $db->escapeString($this->des)."','";
		$sql.= $db->escapeString($this->accionesSegimiento)."','";
		$sql.= $db->escapeString($this->idusuario)."','";
		$sql.= $db->escapeString($this->fechaAlerta)."','";
		$sql.= $db->escapeString($this->fechaRegistro)."' )";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
	public static function obtenerNoLlamarLista(){
		return $obtenerNoLlamarListaQ=self::sentencia("SELECT * FROM listaRoja ORDER BY idListaRoja DESC");
	}

	public static function obtenerDatosDeNoLlamar($sim){
		global $db;
		$sim=limpiar($sim);
		$obtenerDatosDeNoLlamar=self::sentencia("SELECT * FROM listaRoja WHERE sim ='".$db->escapeString($sim)."' LIMIT 1");
		return !empty($obtenerDatosDeNoLlamar)?array_shift($obtenerDatosDeNoLlamar):false;
	}

	public function agregarNoLlamar(){
		global $db;
		global $session;
		$this->fechaRegistro=date("YmdHis");
		$this->idUsuario=$session->idusuario;
		$sql="INSERT INTO listaRoja (sim, placa, cliente, motivo,fechaRegistro,idUsuario )";
		$sql.=" VALUES ('";
		$sql.=$db->escapeString($this->unitId)."','";
		$sql.=$db->escapeString($this->placaVehiculo)."','";
		$sql.=$db->escapeString($this->cliente)."','";
		$sql.=$db->escapeString($this->motivo)."','";
		$sql.=$db->escapeString($this->fechaRegistro)."','";
		$sql.=$db->escapeString($this->idUsuario)."')";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
	public static function eliminarDeFalsasAlertas($sim){
		global $db;
		$sql="DELETE FROM listaRoja WHERE sim='".$db->escapeString($sim)."'";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}

	}
	public static function obtener5ultimasAlertasSubidas(){
		 return $obtener5ultimasAlertasSubidas=self::sentencia("SELECT * FROM alertaSubida ORDER BY alertaSubidaId DESC LIMIT 1");

	}
	public static function obtenerAlertaTrackPorId($id){
		$obTid=self::sentencia("SELECT * FROM alertaSubida WHERE alertaSubidaId = '$id' LIMIT 1");
		return !empty($obTid)?array_shift($obTid):false;
	}


	///////////////////////////////ALERTA OCTO ///////////////////////////////////////////////
	public function registrarChasis(){
		global $db;
		global $session;
		$fechaSubida=date("Ymd");
		$horaSubida=date("h:i A");
		$fechaRegistro=date("YmdHis");;
		$idusuario=$session->idusuario;
		$sql="INSERT INTO bateriaOctoSubida (chasis,tipoAlerta, fechaSubida,fechaRegistro,horaSubida,estado,idusuario) VALUES ('";
		$sql.=$db->escapeString($this->chasis)."','";
		$sql.=$db->escapeString($this->tipoAlerta)."','";
		$sql.=$db->escapeString($fechaSubida)."','";
		$sql.=$db->escapeString($fechaRegistro)."','";
		$sql.=$db->escapeString($horaSubida)."','";
		$sql.=$db->escapeString($this->obtenerUltimoSubidoATOCTO())."','";
		$sql.=$db->escapeString($idusuario)."')";
		if ($db->query($sql)) {
			
		}else{
			echo "fail en ingresar Chasis";
		}
	}
    
public function obtenerUltimoSubidoATOCTO(){
    $sqlobusat=self::sentencia("SELECT fechaRegistro FROM bateriaOctoSubida WHERE idBateriaOctoSubida>'13999' AND  chasis LIKE '%$this->chasis%' ORDER BY idBateriaOctoSubida DESC LIMIT 1");
    if (!empty($sqlobusat)) {
        $datos=array_shift($sqlobusat);
        $horaU=new DateTime($datos->fechaRegistro);
        $hora=new DateTime();
        $fdif= $hora->diff($horaU);
        $hora=$fdif->format("%H%I%S");
        $dia=$fdif->format("%Y%M%D");
        if($dia<=0){
            if ($hora>=030000) {
                return 0; 
            }else{
                return 2;
            }
        }else{
            return 0;
        }
    }else{
        return 0;
    }
}
	public static function mostrarUltimoRegistroSubido(){
		$registrosubido=self::sentencia("SELECT * FROM bateriaOctoSubida ORDER BY idBateriaOctoSubida DESC LIMIT 1");
		foreach ($registrosubido as $var) {
			?>
            <td><?php echo $retVal = ($var->tipoAlerta=='THEFT') ? $var->tipoAlerta : 'NO BATERY' ;  ?></td>
            <td><?php echo $var->chasis ?></td>
            <td><i class="fa fa-user"></i> <?php $usuarioOb=Usuario::obtenerUsuario($var->idusuario); echo $usuarioOb->usuarioNombre ?> <?php echo $usuarioOb->usuarioApellidoPaterno ?></td>
            <td><?php echo $var->horaSubida ?></td>
			<?php
		}

	}



	public function agregarNoLlamarOcto(){
		global $db;
		global $session;
		$this->fechaRegistro=date("YmdHis");
		$this->idUsuario=$session->idusuario;
		$sql="INSERT INTO noLlamarOcto (chasis, nombre, placa, tipo, motivo,fechaRegistro,idUsuario )";
		$sql.=" VALUES ('";
		$sql.=$db->escapeString($this->chasis)."','";
		$sql.=$db->escapeString($this->nombre)."','";
		$sql.=$db->escapeString($this->placaVehiculo)."','";
		$sql.=$db->escapeString($this->tipo)."','";
		$sql.=$db->escapeString($this->motivo)."','";
		$sql.=$db->escapeString($this->fechaRegistro)."','";
		$sql.=$db->escapeString($this->idUsuario)."')";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
	public static function eliminarDeFalsasAlertasOcto($chasis){
		global $db;
		$sql="DELETE FROM noLlamarOcto WHERE chasis='".$db->escapeString($chasis)."'";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}

	}



	public static function obtenerDatosDeNoLlamarOcto($chasis){
		global $db;
		$obtenerDatosDeNoLlamar=self::sentencia("SELECT * FROM noLlamarOcto WHERE chasis ='".$db->escapeString($chasis)."' LIMIT 1");
		return !empty($obtenerDatosDeNoLlamar)?array_shift($obtenerDatosDeNoLlamar):false;
	}



	public function obtenerAlertasHoyOcto(){
        $desdeQueHora = new DateTime();
        $desdeQueHora->modify("-600 minutes");
        $hora=$desdeQueHora->format("YmdHms");
        $obtenerAlertasHoy=self::sentencia("SELECT als.* FROM bateriaOctoSubida  als WHERE idBateriaOctoSubida>'13999' AND '$hora' <= fechaRegistro  AND NOT EXISTS(SELECT chasis FROM noLlamarOcto lr WHERE TRIM(als.chasis)=TRIM(lr.chasis)) AND NOT EXISTS(SELECT fechaAlerta FROM alertaBateriaOcto at WHERE at.fechaAlerta=als.fechaRegistro) AND estado=0");
        return !empty($obtenerAlertasHoy)?$obtenerAlertasHoy:false;
	}

public function obtenerBateriaDesdeId(){
    $sqlobdi=self::sentencia("SELECT * FROM bateriaOctoSubida WHERE idBateriaOctoSubida='$this->idBateriaOctoSubida' LIMIT 1");
    return !empty($sqlobdi)?array_shift($sqlobdi):false;
}

	public function asignarCagoOcto(){
		global $session;
		global $db;
		$sql="UPDATE bateriaOctoSubida SET idUsuarioCargo=".$session->idusuario." WHERE idBateriaOctoSubida ='".$this->idBateriaOctoSubida."' ";
		if ($db->query($sql)) {
		}
	}

	public function registrarAlertaOcto(){
		global $db;
		global $session;
		$this->idusuario=$session->idusuario;
        if($this->chasis==''){
            return false;
        }
		$sql="INSERT INTO alertaBateriaOcto (chasis,agenda,tallerDias,cee,ta,ok,cv,ad,desS,des,fa,accionesSegimiento,idusuario,fechaAlerta,fechaRegistro,nombreCliente)";
		$sql.="VALUES ('";
		$sql.= $db->escapeString($this->chasis)."','";
		$sql.= $db->escapeString($this->agenda)."','";
		$sql.= $db->escapeString($this->tallerDias)."','";
		$sql.= $db->escapeString($this->cee)."','";
		$sql.= $db->escapeString($this->ta)."','";
		$sql.= $db->escapeString($this->ok)."','";
		$sql.= $db->escapeString($this->cv)."','";
		$sql.= $db->escapeString($this->ad)."','";
		$sql.= $db->escapeString($this->desS)."','";
		$sql.= $db->escapeString($this->des)."','";
		$sql.= $db->escapeString($this->fa)."','";
		$sql.= $db->escapeString($this->accionesSegimiento)."','";
		$sql.= $db->escapeString($this->idusuario)."','";
		$sql.= $db->escapeString($this->fechaAlerta)."','";
		$sql.= $db->escapeString($this->fechaRegistro)."','";
		$sql.= $db->escapeString($this->nombreCliente)."' )";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}

	public static function obtenerAlertasAtendidasHoyOcto(){
        $fecha=new DateTime('- 600 minutes');
        $hora=$fecha->format('YmdHms');
        $obtenerAtendidosHoy=self::sentencia("SELECT * FROM alertaBateriaOcto WHERE idBateriaOcto>'11387' AND  fechaRegistro >= '$hora' ORDER BY idBateriaOcto DESC LIMIT 70");
        return !empty($obtenerAtendidosHoy)?$obtenerAtendidosHoy:false;
	}
	public function actualizarDataComplete(){
		global $db;
		global $session;
		$this->fechaSubida=date("Y-m-d h:i A");
		$this->idusuario=$session->idusuario;
		$sql="INSERT INTO dataComplete (chasis, nombre,placa, marca,modelo,color,fechaSubida,idusuario) VALUES ('";
		$sql.=$db->escapeString($this->chasis)."','";		
		$sql.=$db->escapeString($this->nombre)."','";		
		$sql.=$db->escapeString($this->placa)."','";	
		$sql.=$db->escapeString($this->marca)."','";	
		$sql.=$db->escapeString($this->modelo)."','";	
		$sql.=$db->escapeString($this->color)."','";	
		$sql.=$db->escapeString($this->fechaSubida)."','";	
		$sql.=$db->escapeString($this->idusuario)."') ";	
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}

	}

	public static function eliminaralertaaOBateria($idBateriaOctoSubida  ){
		global $db;
		$sql="DELETE FROM bateriaOctoSubida WHERE idBateriaOctoSubida='".$db->escapeString($idBateriaOctoSubida)."'";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
	public static function obtenerCantidadVehiculosDataComplete($cliente){
		global $db;
		$sql=self::sentencia('SELECT COUNT(DISTINCT chasis) as cantidad FROM `dataComplete` WHERE nombre="'.$db->escapeString($cliente).'"');
		return !empty($sql) ? array_shift($sql) : false; 
	}

	///////////////////////////////ALERTA OCTO ////////////////////$///////////////////////////

	public static function obtenerCantidadDeBateriasTota($mes){
		$anio=date('Y');
		$fecha=$anio.$mes."%";
		$batetotal=self::sentencia("SELECT COUNT(chasis) as cantidad FROM bateriaOctoSubida WHERE  fechaRegistro LIKE '$fecha' ");
		return !empty($batetotal)? array_shift($batetotal):false;
	}
	public static function obtenerCantidadDeBateriasAtendidas($mes){
		$anio=date('Y');
		$fecha=$anio.$mes."%";
		$batetotal=self::sentencia("SELECT COUNT(chasis) as cantidad FROM alertaBateriaOcto WHERE  fechaRegistro LIKE '$fecha' ");
		return !empty($batetotal)? array_shift($batetotal):false;
	}

	public static function obtenerCantidadDeBateriasNobatery($mes){
		$anio=date('Y');
		$fecha=$anio.$mes."%";
		$batetotal=self::sentencia("SELECT COUNT(chasis) as cantidad FROM bateriaOctoSubida WHERE  fechaRegistro LIKE '$fecha' AND tipoAlerta='' ");
		return !empty($batetotal)? array_shift($batetotal):false;
	}
	public static function obtenerCantidadDeBateriaTHEFT($mes){
		$anio=date('Y');
		$fecha=$anio.$mes."%";
		$batetotal=self::sentencia("SELECT COUNT(chasis) as cantidad FROM bateriaOctoSubida WHERE  fechaRegistro LIKE '$fecha' AND tipoAlerta='THEFT' ");
		return !empty($batetotal)? array_shift($batetotal):false;
	}

	public static function obtenerBateriasAtendidasDatos($mes){
		$anio=date('Y');
		$fecha=$anio.$mes."%";
		$batetotal=self::sentencia("SELECT * FROM alertaBateriaOcto WHERE  fechaRegistro LIKE '$fecha' ");
		return !empty($batetotal)? $batetotal:false;
			
	}

	public function obtenerGestionadosestadoCantidad($estado){
		if ($estado=='des') {
			$sql=self::sentencia("SELECT COUNT(*) as cantidad FROM alertaBateriaTrack WHERE des='si' LIMIT 1 ");
			return !empty($sql)?array_shift($sql):false;
		}else
		if ($estado=='desO') {
			$sql=self::sentencia("SELECT COUNT(*) as cantidad  FROM alertaBateriaOcto WHERE des='si' LIMIT 1 ");
			return !empty($sql)?array_shift($sql):false;
		}else
		if ($estado=='desS') {
			$sql=self::sentencia("SELECT COUNT(*) as cantidad  FROM alertaBateriaTrack WHERE desS='si' LIMIT 1 ");
			return !empty($sql)?array_shift($sql):false;
		}else
		if ($estado=='desSO') {
			$sql=self::sentencia("SELECT COUNT(*) as cantidad  FROM alertaBateriaOcto WHERE  desS='si' LIMIT 1 ");
			return !empty($sql)?array_shift($sql):false;
		}
	}
	public function obtenerGestionadosestadoaa($posisionDePagina,$registrosPorPagina,$estado){
		if ($estado=='des') {
		$xxd=self::sentencia("SELECT * FROM alertaBateriaTrack WHERE des='si' ORDER BY idAlertaBateriaTrack DESC LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($xxd)?$xxd:false;
		}else
		if ($estado=='desO') {
			$sql=self::sentencia("SELECT * FROM alertaBateriaOcto WHERE des='si' ORDER BY idBateriaOcto DESC LIMIT $posisionDePagina, $registrosPorPagina ");
			return !empty($sql)?$sql:false;
		}else
		if ($estado=='desS') {
			$sql=self::sentencia("SELECT *  FROM alertaBateriaTrack WHERE desS='si' ORDER BY idAlertaBateriaTrack DESC LIMIT $posisionDePagina, $registrosPorPagina ");
			return !empty($sql)?$sql:false;
		}else
		if ($estado=='desSO') {
			$sql=self::sentencia("SELECT *  FROM alertaBateriaOcto WHERE desS='si' ORDER BY idBateriaOcto DESC LIMIT $posisionDePagina, $registrosPorPagina " );
			return !empty($sql)?$sql:false;
		}else if ($estado=''){
			die("EStado please");
		}
	}


	public function obtenerGestionadosestadoCantidadF($estado,$desde,$hasta){		
		if ($estado=='des') {
			$sql=self::sentencia("SELECT COUNT(*) as cantidad FROM alertaBateriaTrack WHERE des='si' AND fechaRegistro  BETWEEN '$desde' AND '$hasta'LIMIT 1 ");
			return !empty($sql)?array_shift($sql):false;
		}else
		if ($estado=='desO') {
			$sql=self::sentencia("SELECT COUNT(*) as cantidad  FROM alertaBateriaOcto WHERE des='si' AND fechaRegistro  BETWEEN '$desde' AND '$hasta' LIMIT 1 ");
			return !empty($sql)?array_shift($sql):false;
		}else
		if ($estado=='desS') {
			$sql=self::sentencia("SELECT COUNT(*) as cantidad  FROM alertaBateriaTrack WHERE desS='si'  AND fechaRegistro  BETWEEN '$desde' AND '$hasta' LIMIT 1 ");
			return !empty($sql)?array_shift($sql):false;
		}else
		if ($estado=='desSO') {
			$sql=self::sentencia("SELECT COUNT(*) as cantidad  FROM alertaBateriaOcto WHERE  desS='si' AND fechaRegistro  BETWEEN '$desde' AND '$hasta' LIMIT 1 ");
			return !empty($sql)?array_shift($sql):false;
		}
	}

	public function obtenerGestionadosestadoaaF($posisionDePagina,$registrosPorPagina,$estado,$desde,$hasta){
		
		if ($estado=='des') {
		$xxd=self::sentencia("SELECT * FROM alertaBateriaTrack WHERE des='si' AND fechaRegistro  BETWEEN '$desde' AND '$hasta' ORDER BY idAlertaBateriaTrack DESC LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($xxd)?$xxd:false;
		}else
		if ($estado=='desO') {
			$sql=self::sentencia("SELECT * FROM alertaBateriaOcto WHERE des='si' AND fechaRegistro  BETWEEN '$desde' AND '$hasta'  ORDER BY idBateriaOcto DESC LIMIT $posisionDePagina, $registrosPorPagina");
			return !empty($sql)?$sql:false;
		}else
		if ($estado=='desS') {
			$sql=self::sentencia("SELECT *  FROM alertaBateriaTrack WHERE desS='si' AND fechaRegistro  BETWEEN '$desde' AND '$hasta' ORDER BY idAlertaBateriaTrack DESC LIMIT $posisionDePagina, $registrosPorPagina ");
			return !empty($sql)?$sql:false;
		}else
		if ($estado=='desSO') {
			$sql=self::sentencia("SELECT *  FROM alertaBateriaOcto WHERE desS='si' AND fechaRegistro  BETWEEN '$desde' AND '$hasta' ORDER BY idBateriaOcto DESC LIMIT $posisionDePagina, $registrosPorPagina ");
			return !empty($sql)?$sql:false;
		}else if ($estado=''){
			die("EStado please");
		}

	}

    public static function obtener5UltimasAtenciones($unitId){
        $slqu5aten=self::sentencia("SELECT * FROM alertaBateriaTrack WHERE unitId LIKE '%$unitId%' ORDER BY idAlertaBateriaTrack DESC  LIMIT 5 ");
        return !empty($slqu5aten)?$slqu5aten:false;
    }

    public static function obtener5UltimasAtencionesOCTO($chasis){
        $slqu5aten=self::sentencia("SELECT * FROM alertaBateriaOcto WHERE chasis LIKE '%$chasis%' ORDER BY idBateriaOcto DESC  LIMIT 5 ");
        return !empty($slqu5aten)?$slqu5aten:false;
    }
}
}


 ?>

<?php 
	if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
	class SeguimientoChoque extends Recursos	
	{
		public $idseguimientoChoque;
		public $idchoque;
		public $tipoContacto;
		public $nombre;
		public $detalles;
		public $notas;
		public $fechaRegistro;
		public $idusuario;
		public $audio;
		function __construct()
		{
			# code...
		}

		public function registrarSeguimientoChoque(){
			global $db;
			global $session;
			$this->fechaRegistro=date("YmdHis");
			$sql="INSERT INTO seguimientoChoque (idchoque,tipoContacto,nombre,detalles,fechaRegistro,idusuario) VALUES ('";
			$sql.=$db->escapeString($this->idchoque)."','";
			$sql.=$db->escapeString($this->tipoContacto)."','";
			$sql.=$db->escapeString($this->nombre)."','";
			$sql.=$db->escapeString($this->detalles)."','";
			$sql.=$db->escapeString($this->fechaRegistro)."','";
			$sql.=$db->escapeString($session->idusuario)."')";
			if ($db->query($sql)) {
				return true;
			}else{
				return false;
			}

		}

		public function registrarNota(){
			global $db;
			global $session;
			$this->fechaRegistro=date("YmdHis");
			$sql="INSERT INTO seguimientoChoque (idchoque,notas,fechaRegistro,idusuario) VALUES ('";
			$sql.=$db->escapeString($this->idchoque)."','";
			$sql.=$db->escapeString($this->notas)."','";
			$sql.=$db->escapeString($this->fechaRegistro)."','";
			$sql.=$db->escapeString($session->idusuario)."')";
			if ($db->query($sql)) {
				return true;
			} else {
				return false;
			}
		}
		public function registrarNotaAsistencia(){
			global $db;
			global $session;
			$this->fechaRegistro=date("YmdHis");
			$sql="INSERT INTO seguimientoAsistencia (idasistencia,notas,fechaRegistro,idusuario) VALUES ('";
			$sql.=$db->escapeString($this->idasistencia)."','";
			$sql.=$db->escapeString($this->notas)."','";
			$sql.=$db->escapeString($this->fechaRegistro)."','";
			$sql.=$db->escapeString($session->idusuario)."')";
			if ($db->query($sql)) {
				return true;
			} else {
				return false;
			}
		}

		public function registrarNotaIncautacion(){
			global $db;
			global $session;
			$this->fechaRegistro=date("YmdHis");
			$sql="INSERT INTO seguimientoIncautacion (idincautacion,notas,fechaRegistro,idusuario) VALUES ('";
			$sql.=$db->escapeString($this->idasistencia)."','";
			$sql.=$db->escapeString($this->notas)."','";
			$sql.=$db->escapeString($this->fechaRegistro)."','";
			$sql.=$db->escapeString($session->idusuario)."')";
			if ($db->query($sql)) {
				return true;
			} else {
				return false;
			}
		}

		public static function obtenerDatosChoquesSeguimiento($idchoque){
			global $db;
			$idchoque=$db->escapeString($idchoque);		
			$obtenerDatosChoquesSeguimientoQ=self::sentencia("SELECT * FROM seguimientoChoque WHERE idchoque = $idchoque ORDER BY fechaRegistro DESC ");
			return !empty($obtenerDatosChoquesSeguimientoQ)?$obtenerDatosChoquesSeguimientoQ:false;

		}

		public static function obtenerDatosAsistenciaSeguimiento($idchoque){
			global $db;
			$idchoque=$db->escapeString($idchoque);		
			$obtenerDatosChoquesSeguimientoQ=self::sentencia("SELECT * FROM seguimientoAsistencia WHERE idasistencia = $idchoque ORDER BY fechaRegistro DESC ");
			return !empty($obtenerDatosChoquesSeguimientoQ)?$obtenerDatosChoquesSeguimientoQ:false;
		}

		public static function obtenerDatosIncautacionSeguimiento($idchoque){
			global $db;
			$idchoque=$db->escapeString($idchoque);		
			$obtenerDatosChoquesSeguimientoQ=self::sentencia("SELECT * FROM seguimientoIncautacion WHERE idincautacion = '$idchoque' ORDER BY fechaRegistro DESC ");
			return !empty($obtenerDatosChoquesSeguimientoQ)?$obtenerDatosChoquesSeguimientoQ:false;
		}
		public static function obtenerDatosChoquesSeguimientoASC($idchoque){
			global $db;
			$idchoque=$db->escapeString($idchoque);		
			$obtenerDatosChoquesSeguimientoQ=self::sentencia("SELECT * FROM seguimientoChoque WHERE idchoque = $idchoque ORDER BY fechaRegistro ASC ");
			return !empty($obtenerDatosChoquesSeguimientoQ)?$obtenerDatosChoquesSeguimientoQ:false;

		}

		public static function obtenerDatosAsistenciaSeguimientoASC($idchoque){
			global $db;
			$idchoque=$db->escapeString($idchoque);		
			$obtenerDatosChoquesSeguimientoQ=self::sentencia("SELECT * FROM seguimientoAsistencia WHERE idasistencia = $idchoque ORDER BY fechaRegistro ASC ");
			return !empty($obtenerDatosChoquesSeguimientoQ)?$obtenerDatosChoquesSeguimientoQ:false;

		}

		public static function obtenerDatosIncautacionSeguimientoASC($idchoque){
			global $db;
			$idchoque=$db->escapeString($idchoque);		
			$obtenerDatosChoquesSeguimientoQ=self::sentencia("SELECT * FROM seguimientoIncautacion WHERE idincautacion = '$idchoque' ORDER BY fechaRegistro ASC ");
			return !empty($obtenerDatosChoquesSeguimientoQ)?$obtenerDatosChoquesSeguimientoQ:false;

		}

		public function registrarAudio(){
			global $db;
			$sql="UPDATE seguimientoChoque SET";
			$sql.=" audio='";
			$sql.=$db->escapeString($this->audio)."' WHERE idseguimientoChoque='";
			$sql.=$db->escapeString($this->idseguimientoChoque)."'";
			return ($db->query($sql)) ? true:false;
		}


	}
	}


 ?>

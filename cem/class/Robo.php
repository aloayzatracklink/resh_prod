<?php 
if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
class Robo extends Recursos
{
	public $idrobo;
	public $placaRobo;
	public $chasis;
	public $nombreInformante;
	public $numeroContacto;
	public $dniInformante;
	public $marca;
	public $modelo;
	public $color;
	public $anio;
	public $propietario;
	public $dniOrucPropietario;
	public $contrato;
	public $vence;
	public $seguro;
	public $poliza;
	public $siniestro;
	public $horaRobo;
	public $lugarRobo;
	public $modalidad;
	public $horaRecupero;
	public $lugarRecupero;
	public $fechaRegistro;
	public $horaRegistro;
	public $estado;
	public $idusuario;
//////7777 DATA COMPLETE /////////////
	public $placa;
	public $nombre;
	public $dni;
	public $tipoSistema;
	public $hasta;

	// public $dniOrucPropietario;

	function __construct()
	{
		
	}
	public function crearRobo(){
		$this->horaRegistro=date("H:i");
		$this->fechaRegistro=date("YmdHis");
		global $db;
		$sql="INSERT INTO robos (placaRobo,nombreInformante,numeroContacto,anio,chasis,marca,modelo,color,propietario,dniOrucPropietario,contrato,seguro,poliza,vence,fechaRegistro,horaRegistro,idusuario) ";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString(strtoupper($this->placaRobo))."' , '";
		$sql.= $db->escapeString(ucfirst($this->nombreInformante))."' , '";
		$sql.= $db->escapeString($this->numeroContacto)."' , '";
		$sql.= $db->escapeString($this->anio)."' , '";
		$sql.= $db->escapeString($this->chasis)."' , '";
		$sql.= $db->escapeString($this->marca)."' , '";
		$sql.= $db->escapeString($this->modelo)."' , '";
		$sql.= $db->escapeString($this->color)."' , '";
		$sql.= $db->escapeString($this->propietario)."' , '";
		$sql.= $db->escapeString($this->dniOrucPropietario)."' , '";
		$sql.= $db->escapeString($this->contrato)."' , '";
		$sql.= $db->escapeString($this->seguro)."' , '";
		$sql.= $db->escapeString($this->poliza)."' , '";
		$sql.= $db->escapeString($this->vence)."' , '";
		$sql.= $db->escapeString($this->fechaRegistro)."' , '";
		$sql.= $db->escapeString($this->horaRegistro)."' , '";
		$sql.= $db->escapeString($this->idusuario)."')";
		if ($db->query($sql)) {
			$this->idrobo=$db->ultimoIdInsertado();
			return true;
			}else{
				return false;
			}

		
	}	
	public static function actualizarDesdeBaseDatosComplete($chasis){
		global $db;
		$actualizarDesdeBaseDatosCompleteQ=self::sentencia("SELECT * FROM dataComplete WHERE chasis='".$db->escapeString($chasis)."' AND precio<>'0' AND numContrato<>'0' ORDER BY idDataComplete DESC LIMIT 1");
		return  !empty($actualizarDesdeBaseDatosCompleteQ) ? array_shift($actualizarDesdeBaseDatosCompleteQ) : false ;
	}

	public function actualizarDatosDeRobo(){
		global $db;
		$sql="UPDATE robos SET ";
		$sql.="placaRobo = '".$db->escapeString(strtoupper($this->placaRobo))."' , ";
		$sql.="anio = '".$db->escapeString(strtoupper($this->anio))."', ";
		$sql.="chasis = '".$db->escapeString(strtoupper($this->chasis))."', ";
		$sql.="marca = '".$db->escapeString(strtoupper($this->marca))."', ";
		$sql.="modelo = '".$db->escapeString(strtoupper($this->modelo))."', ";
		$sql.="color = '".$db->escapeString(strtoupper($this->color))."', ";
		$sql.="propietario = '".$db->escapeString(strtoupper($this->propietario))."', ";
		$sql.="dniOrucPropietario = '".$db->escapeString(strtoupper($this->dniOrucPropietario))."', ";
		$sql.="contrato = '".$db->escapeString(strtoupper($this->contrato))."', ";
		$sql.="vence = '".$db->escapeString(strtoupper($this->vence))."', ";
		$sql.="seguro = '".$db->escapeString(strtoupper($this->seguro))."', ";
		$sql.="poliza = '".$db->escapeString(strtoupper($this->poliza))."', ";
		$sql.="siniestro = '".$db->escapeString(strtoupper($this->siniestro))."', ";
		$sql.="horaRobo = '".$db->escapeString(strtoupper($this->horaRobo))."', ";
		$sql.="lugarRobo = '".$db->escapeString(strtoupper($this->lugarRobo))."', ";
		$sql.="modalidad = '".$db->escapeString(strtoupper($this->modalidad))."', ";
		$sql.="horaRecupero = '".$db->escapeString(strtoupper($this->horaRecupero))."', ";
		$sql.="lugarRecupero = '".$db->escapeString(strtoupper($this->lugarRecupero))."', ";
		$sql.="estado = '".$db->escapeString(strtoupper($this->estado))."' ";
		$sql.= "WHERE idrobo= '".$db->escapeString($this->idrobo)."' ";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
	public  function cambiarEstado($num){
		global $db;
		$sql="UPDATE robos SET estado='".$db->escapeString(ucfirst($num))."'";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
	public static function obtenerRobos(){
		global $db;
		$n=0;
		$robosObj=self::sentencia("SELECT * FROM robos ORDER BY idrobo DESC LIMIT 50");
		foreach ($robosObj as $var) :
		$n++;
			?>
				<tr class="gradeX">
	                <td><?php echo $n ?></td>
	                <td><?php echo $var->placaRobo ?></td>
	                <td class="center"><?php echo $var->propietario ?></td>
	                <td><?php $usuarioAsesor= Usuario::obtenerUsuario($var->idusuario); echo $usuarioAsesor->usuarioNombre;echo "&nbsp;"; echo $usuarioAsesor->usuarioApellidoPaterno  ?></td>
	                <td ><?= obtenerEstado($var->estado) ?></td>
	                <td> <?php echo haceMinimo($var->fechaRegistro) ?></td>
	                <td class="center"><a href="int/clienteDetalle.php?id=<?php echo $var->idrobo ?>" class="btn btn-success btn-xs">Ver</a>&nbsp;<a href="int/detalleRobo.php?id=<?php echo $var->idrobo ?>" class="btn btn-info btn-xs">Seguir</a></td>
	            </tr>
			<?php
			endforeach;

	}
	public static function obtenerRobo($id){
		$obtenerRoboQueryArray=self::sentencia("SELECT * FROM robos WHERE idrobo = $id LIMIT 1");
		return  !empty($obtenerRoboQueryArray) ? array_shift($obtenerRoboQueryArray) : false ;
	}
 	public static function sentencia($sql){
		global $db;
		$resultadoQ=$db->query($sql);
		$objetoArray=array();
		while($row=mysqli_fetch_array($resultadoQ)){
			$objetoArray[]= self::instanciador($row);
		}
		return $objetoArray;
	}
 	
}
}
 ?>
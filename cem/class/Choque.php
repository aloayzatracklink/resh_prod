<?php 
if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
class Choque extends Recursos
{
	public $idchoque;
	public $idasistencia;
	public $idincautacion;
	public $placaChoque;
	public $placaAsistencia;
	public $plataforma;
	public $chasis;
	public $marca;
	public $modelo;
	public $propietario;
	public $idcliente;
	public $plan;
	public $aseguradora;
	public $numeroPoliza;
	public $numeroSiniestroAseguradora;
	public $fechaChoque;
	public $fechaAsistencia;
	public $lugar;
	public $modalidad;
	public $fechaReporte; 
	public $velocidad;
	public $impacto;
	public $idusuario;
	public $fechaRegistro;
	public $idActualizador;
	public $fechaActualizacion;
	public $estado;
	public $vence;
	public $cantidad;
	public $tipo;
    public $operador;
    public $fecha;
	function __construct()
	{		
	}
	public function registrarChoque(){
		global $db;
		global $session;
		$fecha=date("YmdHis");
		$sql="INSERT INTO choques (placaChoque,plataforma,chasis,marca,modelo,propietario,idcliente,fechaReporte,vence,idusuario,fechaRegistro) VALUES ('";
		$sql.=$db->escapeString($this->placaChoque)."' , '";
		$sql.=$db->escapeString($this->plataforma)."' , '";
		$sql.=$db->escapeString($this->chasis)."' , '";
		$sql.=$db->escapeString($this->marca)."' , '";
		$sql.=$db->escapeString($this->modelo)."' , '";
		$sql.=$db->escapeString($this->propietario)."' , '";
		$sql.=$db->escapeString($this->idcliente)."' , '";
		$sql.=$db->escapeString($fecha)."' , '";
		$sql.=$db->escapeString($this->vence)."' , '";
		$sql.=$db->escapeString($session->idusuario)."' , '";
		$sql.=$db->escapeString($fecha)."')";
		if ($db->query($sql)) {
			$this->idchoque=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}
}

	public function registrarAsistencia(){
		global $db;
		global $session;
		$fecha=date("YmdHis");
		$sql="INSERT INTO asistencia (placaAsistencia,chasis,marca,modelo,propietario,idcliente,fechaReporte,vence,idusuario,fechaRegistro) VALUES ('";
		$sql.=$db->escapeString($this->placaChoque)."' , '";
		$sql.=$db->escapeString($this->chasis)."' , '";
		$sql.=$db->escapeString($this->marca)."' , '";
		$sql.=$db->escapeString($this->modelo)."' , '";
		$sql.=$db->escapeString($this->propietario)."' , '";
		$sql.=$db->escapeString($this->idcliente)."' , '";
		$sql.=$db->escapeString($fecha)."' , '";
		$sql.=$db->escapeString($this->vence)."' , '";
		$sql.=$db->escapeString($session->idusuario)."' , '";
		$sql.=$db->escapeString($fecha)."')";
		if ($db->query($sql)) {
			$this->idasistencia=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}
}

	public function registrarIncautacion(){
		global $db;
		global $session;
		$fecha=date("YmdHis");
		$sql="INSERT INTO incautacion (placaAsistencia,chasis,marca,modelo,propietario,idcliente,fechaReporte,vence,idusuario,fechaRegistro) VALUES ('";
		$sql.=$db->escapeString($this->placaChoque)."' , '";
		$sql.=$db->escapeString($this->chasis)."' , '";
		$sql.=$db->escapeString($this->marca)."' , '";
		$sql.=$db->escapeString($this->modelo)."' , '";
		$sql.=$db->escapeString($this->propietario)."' , '";
		$sql.=$db->escapeString($this->idcliente)."' , '";
		$sql.=$db->escapeString($fecha)."' , '";
		$sql.=$db->escapeString($this->vence)."' , '";
		$sql.=$db->escapeString($session->idusuario)."' , '";
		$sql.=$db->escapeString($fecha)."')";
		if ($db->query($sql)) {
			$this->idincautacion=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}
}
	public function obtenerDatosDeChoquePorId($idchoque){
		global $db;
		$idchoque=$db->escapeString($idchoque);
		$obtenerDatosDeRoboPorId=self::sentencia("SELECT * FROM choques WHERE idchoque='$idchoque'");
		return !empty($obtenerDatosDeRoboPorId)?array_shift($obtenerDatosDeRoboPorId):false;

	}

	public function obtenerDatosDeAsistenciaPorId($idasistencia){
		global $db;
		$idasistencia=$db->escapeString($idasistencia);
		$obtenerDatosDeRoboPorId=self::sentencia("SELECT * FROM asistencia WHERE idasistencia='$idasistencia'");
		return !empty($obtenerDatosDeRoboPorId)?array_shift($obtenerDatosDeRoboPorId):false;

	}

	public function obtenerDatosDeIncautacionPorId($diincautacion){
		global $db;
		$ii=$db->escapeString($diincautacion);
		$obtenerDatosDeRoboPorId=self::sentencia("SELECT * FROM incautacion WHERE idincautacion='$ii'");
		return !empty($obtenerDatosDeRoboPorId)?array_shift($obtenerDatosDeRoboPorId):false;
	}

	public function obtenerChoques(){
		$obtenerChoquesQ=self::sentencia("SELECT * FROM choques ORDER BY idchoque DESC LIMIT 200");
		return !empty($obtenerChoquesQ)?$obtenerChoquesQ:false;
	}

	public function obtenerAsistencias(){
		$obtenerChoquesQ=self::sentencia("SELECT * FROM asistencia ORDER BY idasistencia DESC LIMIT 200");
		return !empty($obtenerChoquesQ)?$obtenerChoquesQ:false;
	}

	public function obtenerIncautaciones(){
		$obtenerChoquesQ=self::sentencia("SELECT * FROM incautacion ORDER BY idincautacion  DESC LIMIT 200");
		return !empty($obtenerChoquesQ)?$obtenerChoquesQ:false;
	}
	public function actualizarDatos(){
		global $db; global $session; $this->fechaActualizacion=date("YmdHis");
		$sql="UPDATE choques SET plan ='";
		$sql.=$db->escapeString($this->plan)."' ,";
		$sql.=" aseguradora ='";
		$sql.=$db->escapeString($this->aseguradora)."' ,";
		$sql.=" numeroPoliza ='";
		$sql.=$db->escapeString($this->numeroPoliza)."' ,";
		$sql.=" numeroSiniestroAseguradora ='";
		$sql.=$db->escapeString($this->numeroSiniestroAseguradora)."' ,";
		$sql.=" fechaChoque ='";
		$sql.=$db->escapeString($this->fechaChoque)."' ,";
		$sql.=" lugar ='";
		$sql.=$db->escapeString($this->lugar)."' ,";
		$sql.=" modalidad ='";
		$sql.=$db->escapeString($this->modalidad)."' ,"; 
		$sql.=" velocidad ='";
		$sql.=$db->escapeString($this->velocidad)."' ,"; 
		$sql.=" impacto ='";
		$sql.=$db->escapeString($this->impacto)."' ,"; 
		$sql.=" impacto ='";
		$sql.=$db->escapeString($this->impacto)."' ,";
		$sql.=" fechaActualizacion ='";
		$sql.=$db->escapeString($this->fechaActualizacion)."' ,";
		$sql.=" idActualizador ='";
		$sql.=$db->escapeString($session->idusuario)."' ,";
		$sql.=" estado ='";
		$sql.=$db->escapeString($this->estado)."' ";
		$sql.=" WHERE idchoque ='".$this->idchoque."'";
		if ($db->query($sql)) {
			return true;
		} else {
			return false;
		}
	}

	public function actualizarDatosAsistencia(){
		global $db; global $session; $this->fechaActualizacion=date("YmdHis");
		$sql="UPDATE asistencia SET plan ='";
		$sql.=$db->escapeString($this->plan)."' ,";
		$sql.=" aseguradora ='";
		$sql.=$db->escapeString($this->aseguradora)."' ,";
		$sql.=" numeroPoliza ='";
		$sql.=$db->escapeString($this->numeroPoliza)."' ,";
		$sql.=" numeroSiniestroAseguradora ='";
		$sql.=$db->escapeString($this->numeroSiniestroAseguradora)."' ,";
		$sql.=" fechaAsistencia ='";
		$sql.=$db->escapeString($this->fechaChoque)."' ,";
		$sql.=" lugar ='";
		$sql.=$db->escapeString($this->lugar)."' ,";
		$sql.=" tipo ='";
		$sql.=$db->escapeString($this->modalidad)."' ,"; 
		$sql.=" fechaActualizacion ='";
		$sql.=$db->escapeString($this->fechaActualizacion)."' ,";
		$sql.=" idActualizador ='";
		$sql.=$db->escapeString($session->idusuario)."' ,";
		$sql.=" estado ='";
		$sql.=$db->escapeString($this->estado)."' ";
		$sql.=" WHERE idasistencia ='".$this->idasistencia."'";
		if ($db->query($sql)) {
			return true;
		} else {
			return false;
		}
	}

	public function actualizarDatosIncautacion(){
		global $db; global $session; $this->fechaActualizacion=date("YmdHis");
		$sql="UPDATE incautacion SET plan ='";
		$sql.=$db->escapeString($this->plan)."' ,";
		$sql.=" aseguradora ='";
		$sql.=$db->escapeString($this->aseguradora)."' ,";
		$sql.=" numeroPoliza ='";
		$sql.=$db->escapeString($this->numeroPoliza)."' ,";
		$sql.=" numeroSiniestroAseguradora ='";
		$sql.=$db->escapeString($this->numeroSiniestroAseguradora)."' ,";
		$sql.=" fechaAsistencia ='";
		$sql.=$db->escapeString($this->fechaChoque)."' ,";
		$sql.=" lugar ='";
		$sql.=$db->escapeString($this->lugar)."' ,";
		$sql.=" tipo ='";
		$sql.=$db->escapeString($this->modalidad)."' ,"; 
		$sql.=" fechaActualizacion ='";
		$sql.=$db->escapeString($this->fechaActualizacion)."' ,";
		$sql.=" idActualizador ='";
		$sql.=$db->escapeString($session->idusuario)."' ,";
		$sql.=" estado ='";
		$sql.=$db->escapeString($this->estado)."' ";
		$sql.=" WHERE idincautacion ='".$this->idasistencia."'";
		if ($db->query($sql)) {
			return true;
		} else {
			return false;
		}
	}

	public function actualizarfechaIcautacion(){
		global $db; 
		$sql="UPDATE incautacion SET fechaAsistencia ='";
		$sql.=$db->escapeString($this->fechaChoque)."' ";
		$sql.=" WHERE idincautacion ='".$this->idasistencia."'";
		if ($db->query($sql)) {
			return true;
		} else {
			return false;
		}
	}

	public function actualizarDatosCliente(){
		global $db;
		$sql="UPDATE choques SET placaChoque ='";
		$sql.=$db->escapeString($this->placaChoque)."', ";
		$sql.=" chasis='";
		$sql.=$db->escapeString($this->chasis)."','";
		$sql.=" marca='";
		$sql.=$db->escapeString($this->marca)."','";
		$sql.=" modelo='";
		$sql.=$db->escapeString($this->modelo)."','";
		$sql.=" propietario='";
		$sql.=$db->escapeString($this->propietario)."','";
		$sql.=" idcliente='";
		$sql.=$db->escapeString($this->idcliente)."' ";
		$sql.=" WHERE idchoque='";
		$sql.=$db->escapeString($this->idchoque)."'";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}

	}

	public function verificarExistenciaenChile($chasis){
		$verificarech=self::sentencia("SELECT idformulario as cantidad FROM cliente_chiles WHERE idformulario='$chasis' LIMIT 1");
        $chile=array_shift($verificarech);
        //dd($verificarech);
        return empty($verificarech)?$chile->cantidad:false;
	}

	public static function otenerChoquesEnSeguimientoCantiad(){
		$otenerChoquesEnSeguimientoCantiadQ=self::sentencia("SELECT COUNT(*) as cantidad FROM choques WHERE estado='11' LIMIT 1");
		return !empty($otenerChoquesEnSeguimientoCantiadQ)?array_shift($otenerChoquesEnSeguimientoCantiadQ):false;
	}

	public static function otenerAsistenciaEnSeguimientoCantiad(){
		$otenerChoquesEnSeguimientoCantiadQ=self::sentencia("SELECT COUNT(*) as cantidad FROM asistencia WHERE estado='11' LIMIT 1");
		return !empty($otenerChoquesEnSeguimientoCantiadQ)?array_shift($otenerChoquesEnSeguimientoCantiadQ):false;
	}
	public static function otenerIncautacionEnSeguimientoCantiad(){
		$otenerChoquesEnSeguimientoCantiadQ=self::sentencia("SELECT COUNT(*) as cantidad FROM incautacion WHERE estado='11' LIMIT 1");
		return !empty($otenerChoquesEnSeguimientoCantiadQ)?array_shift($otenerChoquesEnSeguimientoCantiadQ):false;
	}

	public function obtenerDatChoques($fechaInicio,$fechaFin){
		$sql="        
        SELECT  idchoque, placaChoque,chasis ,propietario,
        (SELECT CONCAT(u.usuarioNombre,' ',u.usuarioApellidoMaterno) FROM usuarios u WHERE u.idusuario =c.idusuario) operador,
		(SELECT e.estado FROM estados e WHERE e.codigo =c.estado)estado,
		DATE_FORMAT(fechaRegistro,'%Y-%m-%d %H:%i:%s') fecha  
		FROM choques c 
		WHERE DATE_FORMAT(c.fechaRegistro,'%Y-%m-%d')>='$fechaInicio' and DATE_FORMAT(c.fechaRegistro,'%Y-%m-%d')<='$fechaFin'
		order by idchoque desc
        ";
		$dtp=self::sentencia($sql);
		return !empty($dtp)? $dtp:false;
	}
}
}?>
<?php 
if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
class DatosInstalacion extends Recursos
{
	public $iddatosInstalacion;	
	public $orte;		
	public $cliente;		
    public $nombreArchivo;
	public $placa;		
	public $chasis;
	public $imei;
	public $id;	
	public $odo;	
	public $ubicacionSistema;		
	public $lugarInstalacion;		
	public $tecnicoInstalador;
	public $sistema;		
	public $servicio;		
	public $operador;		
	public $estado;			
	public $fecha;		
	public $horaActivacion;		
	public $cordenadas;	
	public $observaciones;	
	public $observaciones2;	
	public $observaciones3;	
	public $observaciones4;	
	public $idusuario;	
	public $fechaRegistro;	
    public $cantidad;
    public $idoperacion;
    public $tmp;
	function __construct()
	{
	}
	
	public function registrarDatosDeInstalacion(){
		global $db;
		global $session;
		$this->fechaRegistro=date("YmdHis");
		$sql="INSERT INTO datosInstalacion (fecha,orte,cliente,placa,chasis,imei,id,odo,ubicacionSistema,lugarInstalacion,tecnicoInstalador,sistema,servicio,operador,horaActivacion,cordenadas,observaciones, fechaRegistro,estado,idusuario,idoperacion) VALUES ('";
		$sql.=$db->escapeString($this->fecha)."' , '";
		$sql.=$db->escapeString($this->orte)."' , '";
		$sql.=$db->escapeString($this->cliente)."' , '";
		$sql.=$db->escapeString($this->placa)."' , '";
		$sql.=$db->escapeString($this->chasis)."' , '";
		$sql.=$db->escapeString($this->imei)."' , '";
		$sql.=$db->escapeString($this->id)."' , '";
		$sql.=$db->escapeString($this->odo)."' , '";
		$sql.=$db->escapeString($this->ubicacionSistema)."' , '";
		$sql.=$db->escapeString($this->lugarInstalacion)."' , '";
		$sql.=$db->escapeString($this->tecnicoInstalador)."' , '";
		$sql.=$db->escapeString($this->sistema)."' , '";
		$sql.=$db->escapeString($this->servicio)."' , '";
		$sql.=$db->escapeString($this->operador)."' , '";
		$sql.=$db->escapeString($this->horaActivacion)."' , '";
		$sql.=$db->escapeString($this->cordenadas)."' , '";
		$sql.=$db->escapeString($this->observaciones)."' , '";
		$sql.=$db->escapeString($this->fechaRegistro)."' , '";
		$sql.=$db->escapeString($this->estado)."' , '";
		$sql.=$db->escapeString($session->idusuario)."' , '";
		$sql.=$db->escapeString($this->idoperacion)."' )";
		if ($db->query($sql)){
			return true;
		}else{
			return false;
		}
	}
	public function registrarDatosDeInstalacionTMP(){
		global $db;
		global $session;
		$this->fechaRegistro=date("YmdHis");
		$sql="INSERT INTO datosInstalacion (fecha,orte,cliente,placa,chasis,imei,id,odo,ubicacionSistema,lugarInstalacion,tecnicoInstalador,sistema,servicio,operador,estado,horaActivacion,cordenadas,observaciones, fechaRegistro,idusuario,tmp) VALUES ('";
		$sql.=$db->escapeString($this->fecha)."' , '";
		$sql.=$db->escapeString($this->orte)."' , '";
		$sql.=$db->escapeString($this->cliente)."' , '";
		$sql.=$db->escapeString($this->placa)."' , '";
		$sql.=$db->escapeString($this->chasis)."' , '";
		$sql.=$db->escapeString($this->imei)."' , '";
		$sql.=$db->escapeString($this->id)."' , '";
		$sql.=$db->escapeString($this->odo)."' , '";
		$sql.=$db->escapeString($this->ubicacionSistema)."' , '";
		$sql.=$db->escapeString($this->lugarInstalacion)."' , '";
		$sql.=$db->escapeString($this->tecnicoInstalador)."' , '";
		$sql.=$db->escapeString($this->sistema)."' , '";
		$sql.=$db->escapeString($this->servicio)."' , '";
		$sql.=$db->escapeString($this->operador)."' , '";
		$sql.=$db->escapeString($this->estado)."' , '";
		$sql.=$db->escapeString($this->horaActivacion)."' , '";
		$sql.=$db->escapeString($this->cordenadas)."' , '";
		$sql.=$db->escapeString($this->observaciones)."' , '";
		$sql.=$db->escapeString($this->fechaRegistro)."' , '";
		$sql.=$db->escapeString($session->idusuario)."' , '";
		$sql.=$db->escapeString('SI')."' )";
		if ($db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	public function actualizarDatosDeInstalacion(){
		global $db;
		global $session;
		$this->fechaRegistro=date("YmdHis");
		$sql="UPDATE datosInstalacion SET ";
		$sql.="fecha = '".$db->escapeString($this->fecha)."',";
		$sql.="imei = '".$db->escapeString($this->imei)."',";
		$sql.="id = '".$db->escapeString($this->id)."',";
		$sql.="odo = '".$db->escapeString($this->odo)."',";
		$sql.="ubicacionSistema = '".$db->escapeString($this->ubicacionSistema)."',";
		$sql.="tecnicoInstalador = '".$db->escapeString($this->tecnicoInstalador)."',";
		$sql.="operador = '".$db->escapeString($this->operador)."',";
		$sql.="horaActivacion = '".$db->escapeString($this->horaActivacion)."',";
		$sql.="cordenadas = '".$db->escapeString($this->cordenadas)."',";
		$sql.="observaciones = '".$db->escapeString($this->observaciones)."',";
		$sql.="idusuario = '".$db->escapeString($session->idusuario)."',";
		$sql.="fechaRegistro = '".$db->escapeString($this->fechaRegistro)."',";
		$sql.="estado = '".$db->escapeString($this->estado)."' ";
		$sql.=" WHERE idoperacion = '$this->idoperacion'";
		if ($db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

    public static function obnerActualizacionesDeLOG($idoperacion){
        $obtener=self::sentencia("SELECT * FROM datosInstalacionLog WHERE idoperacion='".limpiar($idoperacion)."'");
        return  !empty($obtener)?$obtener:false;
    }

    public static function obnerActualizacionesDeLOGidDatosInstalacion($iddatosInstalacion){
        $obtener=self::sentencia("SELECT * FROM datosInstalacionLog WHERE iddatosInstalacion='".limpiar($iddatosInstalacion)."'");
        return  !empty($obtener)?$obtener:false;
    }

    public function verificarExistenciaOperacion(){
        $oeo=self::sentencia("SELECT * FROM datosInstalacion WHERE idoperacion='$this->idoperacion'");
        return !empty($oeo)?true:false;
    }
	public static function obtenerDatosInstalacion(){
		$sql=self::sentencia("SELECT * FROM datosInstalacion LIMIT 10");
		return (!empty($sql))?$sql:false;
	}
    public static function obtenerSiexiste($idoperacion){
        $oeo=self::sentencia("SELECT * FROM datosInstalacion WHERE idoperacion='".limpiar($idoperacion)."' LIMIT 1");
        return !empty($oeo)?array_shift($oeo):false;
    }

    public  function obtenerSiexisteF($idoperacion){
        $oeo=self::sentencia("SELECT * FROM datosInstalacion WHERE idoperacion='".limpiar($idoperacion)."' LIMIT 1");
        return !empty($oeo)?array_shift($oeo):false;
    }

    public function obtenerSiexisteORTE($orte){
        $oeo=self::sentencia("SELECT * FROM datosInstalacion WHERE orte='".limpiar($orte)."' LIMIT 1");
        return !empty($oeo)?array_shift($oeo):false;
    }

    public function obtenerSiexisteORTEALL($orte){
        $oeo=self::sentencia("SELECT * FROM datosInstalacion WHERE orte='".limpiar($orte)."' ");
        return !empty($oeo)?$oeo:false;
    }


    public static function obtenerdatosDeidoperacion($iddatosInstalacion){
        $oeo=self::sentencia("SELECT * FROM datosInstalacion WHERE iddatosInstalacion='".limpiar($iddatosInstalacion)."'");
        return !empty($oeo)?array_shift($oeo):false;
    }

    public function obtenerdatosDeinstalacion($iddatosInstalacion){
        $oeo=self::sentencia("SELECT * FROM datosInstalacion WHERE iddatosInstalacion='".limpiar($iddatosInstalacion)."' ");
        return !empty($oeo)?array_shift($oeo):false;
    }
    
	public static function obtenerLosUltimosRegistros(){
		$sqlultmo=self::sentencia("SELECT * FROM datosInstalacion WHERE idoperacion='0' AND tmp='' GROUP BY fechaRegistro ORDER BY iddatosInstalacion DESC LIMIT 10");
		return !empty($sqlultmo)?$sqlultmo:false;
	}
    public static function obterCuantosRegistros($fechaRegistro){
        $cuanto=self::sentencia("SELECT COUNT(*) as cantidad FROM datosInstalacion WHERE fechaRegistro='$fechaRegistro' LIMIT 1");
        $datos=array_shift($cuanto);
        return $datos->cantidad;
    }
	public static function otenerUltimoGrupoFecha(){
		$sqlultmoFecha=self::sentencia("SELECT fechaRegistro FROM datosInstalacion ORDER BY iddatosInstalacion DESC LIMIT 1");
		return !empty($sqlultmoFecha)?array_shift($sqlultmoFecha):false;
	}
	public static function eliminarUltimoGrupo($fecha){
		global $db;
		$sql="DELETE FROM datosInstalacion WHERE fechaRegistro = '$fecha'";
		return ($db->query($sql))?true:false;
	}


	public static function obtenerChequeoTrackOtrasAreas(){
		return $obtenerChequeoTrack=self::sentencia("SELECT * FROM chequeos WHERE (estado=0 OR estado = 321) AND idMonitoreo=0 ORDER BY fechaHoraChequeoLinea");
		// C.placa, C.nombres, C.fechaHoraChequeoLinea,C.estado,C.idCargo, 1<>'SELECT idrol FROM usuarios WHERE idusuario =C.idCreador'
		// SELECT C.placa, C.nombres, C.fechaHoraChequeoLinea,C.estado,C.idCargo,C.idCreador,C.idchequeo FROM chequeos C JOIN usuarios U ON(C.idCreador = U.idusuario AND U.idrol<>1) ORDER BY fechaHoraChequeoLinea 
	}
	public static function obtenerChequeosGestionados(){
		$obtenerChequeosGestionados=self::sentencia("SELECT * FROM chequeos WHERE estado <> 0 ");
		return !empty($obtenerChequeosGestionados)?$obtenerChequeosGestionados:false;
	}


	public static function obtenerChequeoTrackPorId($id){
		$obtenerChequeoTrackPorIdQ=self::sentencia("SELECT * FROM chequeos WHERE idchequeo ='$id' LIMIT 1");
		return  !empty($obtenerChequeoTrackPorIdQ) ? array_shift($obtenerChequeoTrackPorIdQ) : false ;
	}

	public function validarExistenciaPendiente($numeroOrden){
		global $db;
		$validarExistenciaPendienteQ=self::sentencia("SELECT numeroOrden FROM chequeos WHERE numeroOrden='".$db->escapeString($numeroOrden)."' AND estado = 0");
		return !empty($validarExistenciaPendienteQ) ? true :false;
	}

	public function validarExistenciaPendienteChasis($chasis){
		global $db;
		$validarExistenciaPendienteQ=self::sentencia("SELECT chasis FROM chequeos WHERE chasis='".$db->escapeString($chasis)."' AND estado = 0");
		return !empty($validarExistenciaPendienteQ) ? true :false;
	}

	public static function obtenerProgramacionChequeo($fecha){
		global $db;
        $fecha=str_replace(":", "", $fecha);
		$fecha=limpiar($fecha);
		$obtenerProgramacionChequeoQ=self::sentencia("SELECT * FROM chequeos WHERE fechaHoraChequeoLinea LIKE '".$db->escapeString($fecha)."' AND estado='0' ");
		return !empty($obtenerProgramacionChequeoQ)?$obtenerProgramacionChequeoQ:false;
	}	


	public function obtenerChequeosRealizados(){
		global $session;
		$obchR=self::sentencia("SELECT * FROM chequeos WHERE (estado ='64' OR estado ='65' OR estado = '66')  AND  idCreador ='".$session->idusuario."' LIMIT 70 ");
		return !empty($obchR) ? $obchR :false;
	}

	public function asignarChequeo(){
		global $db;
		global $session;
		$this->fechaAperturaChequeoLinea=date("YmdHis");
		$asignarChequeo="UPDATE chequeos SET fechaAperturaChequeoLinea=";
		$asignarChequeo.=$db->escapeString($this->fechaAperturaChequeoLinea).", ";
		$asignarChequeo.=" idCargo='".$session->idusuario."' WHERE idchequeo='".$db->escapeString($this->idchequeo)."'";
		if ($db->query($asignarChequeo)) {
			return true;
		} else {
			return false;
		}
	}
	public function verificarDisponible(){
		$verificarDisponibleQ=self::sentencia("SELECT * FROM chequeos WHERE idCargo = '0' AND  idchequeo ='".$this->idchequeo."'");
		return !empty($verificarDisponibleQ)?true:false;
	}

	public function guardarComoChequeoEnLinea(){
		global $db;
		global $session;
		$this->fechaConfirmacionLinea=date("YmdHis");
		$sql="UPDATE chequeos SET conclusion='";
		$sql.=$db->escapeString($this->conclusion)."', coordenadas ='";		
		$sql.=$db->escapeString($this->coordenadas)."', fechaConfirmacionLinea ='";
		$sql.=$db->escapeString($this->fechaConfirmacionLinea)."', tipoChequeo ='";
		$sql.=$db->escapeString($this->tipoChequeo)."', estado ='64'";
		$sql.=", idConfimadorChequeoLinea ='$session->idusuario' WHERE idchequeo='".$db->escapeString($this->idchequeo)."'";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
	public function guardarComoChequeoPorMonitoreo(){
		global $db;
		global $session;
		$this->fechaConfirmacionLinea=date("YmdHis");
		$sql="UPDATE chequeos SET ubicacionSistema='";
		$sql.=$db->escapeString($this->ubicacionSistema)."', ";
		$sql.="seguimientoRealizado ='";
		$sql.=$db->escapeString($this->seguimientoRealizado)."', ";
		$sql.="instalador ='";
		$sql.=$db->escapeString($this->instalador)."', ";
		$sql.="tipoChequeo ='";
		$sql.=$db->escapeString($this->tipoChequeo)."', ";
		$sql.="direccion ='";
		$sql.=$db->escapeString($this->direccion)."', ";
		$sql.="departamentoc ='";
		$sql.=$db->escapeString($this->departamentoc)."', ";
		$sql.="provinciac ='";
		$sql.=$db->escapeString($this->provinciac)."', ";
		$sql.="distritoc ='";
		$sql.=$db->escapeString($this->distritoc)."', ";
		$sql.="lugar ='";
		$sql.=$db->escapeString($this->lugar)."', ";
		$sql.="referencia ='";
		$sql.=$db->escapeString($this->referencia)."', ";
		$sql.="coordenadas ='";
		$sql.=$db->escapeString($this->coordenadas)."', ";
		$sql.="fechaInstalacion ='";
		$sql.=$db->escapeString($this->fechaInstalacion)."', ";
		$sql.="personaContacto ='";
		$sql.=$db->escapeString($this->personaContacto)."', ";
		$sql.="numeroContacto ='";
		$sql.=$db->escapeString($this->numeroContacto)."', ";
		$sql.="fechaConfirmacionLinea ='";
		$sql.=$db->escapeString($this->fechaConfirmacionLinea)."', ";
		$sql.="idConfimadorChequeoLinea ='";
		$sql.=$db->escapeString($session->idusuario)."', ";
		$sql.="fechaChequeoEnSac ='";
		$sql.=$db->escapeString($this->fechaChequeoEnSac)."', ";
		$sql.="motivo ='";
		$sql.=$db->escapeString($this->motivo)."', ";
		$sql.="estado ='";
		$sql.=$db->escapeString($this->estado)."', ";
		$sql.="observacion ='";
		$sql.=$db->escapeString($this->observacion)."' WHERE ";
		$sql.=" idchequeo = '".$db->escapeString($this->idchequeo)."'";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}

	}

/////////////////////////paginacion /////////////77
	public function obtenerRegistrosSubidosCantida(){
		global $session;
		$obtenerRegistroSubidoQ=self::sentencia("SELECT COUNT(*) as cantidad FROM datosInstalacion  LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false; 
	}

	public function obtenerRegistrosSubidos($posisionDePagina,$registrosPorPagina){
		global $session;
		$obtenerRegistroSubidoQ=self::sentencia("SELECT * FROM chequeos WHERE idCreador = ".$session->idusuario." AND estado='0' LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}
	////////////resutado de busqueda//
	public function buscarParaFiltro($datos){
		global $session;
		global $db;	
		$datos=$db->escapeString($datos);		
		$obtenerRegistroSubidoQ=self::sentencia("SELECT *  FROM chequeos WHERE idCreador = ".$session->idusuario."  AND (nombres LIKE '$datos' OR placa LIKE '$datos')");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}
	public static function obtenerPlan($idchequeo){
		 $oplan=self::sentencia("SELECT plan,fechaChequeoEnSac FROM chequeos WHERE idchequeo = '$idchequeo' LIMIT 1");
		 return !empty($oplan)? array_shift($oplan):false;
	}

	public function actualizarAlgunosDatos(){
		global $db;
		$sql="UPDATE chequeos SET ";
		$sql.=" motivo='";
		$sql.=$db->escapeString($this->motivo)."', ";
		$sql.=" personaContacto='";
		$sql.=$db->escapeString($this->personaContacto)."', ";
		$sql.=" numeroContacto='";
		$sql.=$db->escapeString($this->numeroContacto)."', ";
		$sql.=" fechaHoraChequeoLinea='";
		$sql.=$db->escapeString($this->fechaHoraChequeoLinea)."' ";
		$sql.=" WHERE idchequeo='";
		$sql.=$db->escapeString($this->idchequeo)."'";
		return ($db->query($sql))?true:false;
	}
	public function guardarSeguimientoRealizado(){
		global $db;
		$this->fechaConfirmacionLinea=date("YmdHis");
		$sql="UPDATE chequeos SET ";
		$sql.=" tipoChequeo='";
		$sql.=$db->escapeString($this->tipoChequeo)."', ";
		$sql.=" fechaConfirmacionLinea='";
		$sql.=$db->escapeString($this->fechaConfirmacionLinea)."', ";
		$sql.=" estado='";
		$sql.=$db->escapeString($this->estado)."', ";
		$sql.=" seguimientoRealizado='";
		$sql.=$db->escapeString($this->seguimientoRealizado)."' ";
		$sql.=" WHERE idchequeo='";
		$sql.=$db->escapeString($this->idchequeo)."'";
		$db->query($sql);
	}
	
	public function reprogramarChequeoEnLinea(){
		global $db;
		$this->estado='123';
		$this->guardarSeguimientoRealizado();
		$chData=self::obtenerChequeoTrackPorId($this->idchequeo);
		$this->plan=$chData->plan;
		$this->dniRuc=$chData->dniRuc;
		$this->nombres=$chData->nombres;
		$this->placa=$chData->placa;
		$this->chasis=$chData->chasis;
		$this->marcaModelo=$chData->marcaModelo;
		$this->observacion=$chData->observacion;
		$this->idcustomer=$chData->idcustomer;
		$this->idvehicle=$chData->idvehicle;
		$this->idlocation=$chData->idlocation;
		$this->descripcionOrden=$chData->descripcionOrden;
		$this->numeroOrden=$chData->numeroOrden;
		$this->idReprogramado=$this->idchequeo;
		$this->tipoChequeo='';
		if($this->registrarChequeoMonitoreo()){
			return true;
		}else{
			return false;
		}
	}
	public function cancelarChequeoEnLinea(){
		global $db;
		global $session;
		$this->estado='32';
		$this->guardarSeguimientoRealizado();
		$sql="UPDATE chequeos SET estado='";
		$sql.=$this->estado."', ";
		$sql.=" personaContacto='";
		$sql.=$this->personaContacto."',";
		$sql.=" numeroContacto='";
		$sql.=$this->numeroContacto."',";
		$sql.=" motivo='";
		$sql.=$this->motivo."',";
		$sql.=" idConfimadorChequeoLinea='";
		$sql.=$session->idusuario."', ";
		$sql.=" correo='";
		$sql.=$this->correo."' ";
		$sql.=" WHERE idchequeo='";
		$sql.=$this->idchequeo."' ";
		if($db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	public function reprogramarChequeo(){
		global $db;
		global $session;		
		$this->fechaHoraChequeoLinea=date("YmdHi");
		$this->fechaRegistro=date("YmdHis");
		$datosChequeo=self::obtenerChequeoTrackPorId($this->idchequeo);
		$sql="INSERT INTO chequeos (plan, dniRuc, nombres,placa,chasis,marcaModelo,observacion, motivo, celular, telefono,correo,personaContacto,numeroContacto,ubicacionSistema,lugar, departamentoc,provinciac,distritoc,direccion,referencia,coordenadas,fechaInstalacion, idcustomer, instalador ,idvehicle, idlocation, idReprogramado,descripcionOrden,fechaHoraChequeoLinea,fechaRegistro,estado,idCreador) VALUES ('";
		$sql.=$db->escapeString($datosChequeo->plan)."','";
		$sql.=$db->escapeString($datosChequeo->dniRuc)."','";
		$sql.=$db->escapeString($datosChequeo->nombres)."','";
		$sql.=$db->escapeString($datosChequeo->placa)."','";
		$sql.=$db->escapeString($datosChequeo->chasis)."','";
		$sql.=$db->escapeString($datosChequeo->marcaModelo)."','";
		$sql.=$db->escapeString($this->observacion)."','";
		$sql.=$db->escapeString($datosChequeo->motivo)."','";
		$sql.=$db->escapeString($datosChequeo->celular)."','";
		$sql.=$db->escapeString($datosChequeo->telefono)."','";
		$sql.=$db->escapeString($datosChequeo->correo)."','";
		$sql.=$db->escapeString($datosChequeo->personaContacto)."','";
		$sql.=$db->escapeString($datosChequeo->numeroContacto)."','";
		$sql.=$db->escapeString($datosChequeo->ubicacionSistema)."','";
		$sql.=$db->escapeString($datosChequeo->lugar)."','";
		$sql.=$db->escapeString($datosChequeo->departamentoc)."','";
		$sql.=$db->escapeString($datosChequeo->provinciac)."','";
		$sql.=$db->escapeString($datosChequeo->distritoc)."','";
		$sql.=$db->escapeString($datosChequeo->direccion)."','";
		$sql.=$db->escapeString($datosChequeo->referencia)."','";
		$sql.=$db->escapeString($datosChequeo->coordenadas)."','";
		$sql.=$db->escapeString($datosChequeo->fechaInstalacion)."','";
		$sql.=$db->escapeString($datosChequeo->idcustomer)."','";
		$sql.=$db->escapeString($datosChequeo->instalador)."','";
		$sql.=$db->escapeString($datosChequeo->idvehicle)."','";
		$sql.=$db->escapeString($datosChequeo->idlocation)."','";
		$sql.=$db->escapeString($this->idchequeo)."','";
		$sql.=$db->escapeString("reprogramadoSac")."','";
		$sql.=$db->escapeString($this->fechaHoraChequeoLinea)."','";
		$sql.=$db->escapeString($this->fechaRegistro)."','";
		$sql.=$db->escapeString('321')."','";
		$sql.=$db->escapeString($session->idusuario)."')";
		return ($db->query($sql))?true:false;
	}

    public static function verInstaladoNoprogramado(){
        $sqlinstanpro=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE estado='99' LIMIT 1 ");
        $cant=array_shift($sqlinstanpro);
        if($cant->cantidad>=1){
            return $cant->cantidad;
        }else{
            return false;
        }
    }

    public static function cantidadTempo(){
        $sqlinstanpro=self::sentencia("SELECT COUNT(*) as cantidad FROM datosInstalacion WHERE tmp='SI' AND idoperacion='0' AND servicio NOT LIKE 'Reinstalacion' LIMIT 1 ");
        $cant=array_shift($sqlinstanpro);
        if($cant->cantidad>=1){
            return $cant->cantidad;
        }else{
            return false;
        }
    }

    public  function listaTemporalesInstalaciones($pos,$reg){
        $sqlinstanpro=self::sentencia("select * from datosInstalacion where tmp='SI' and servicio='Instalacion' and fecha > 20181230 order by fecha desc LIMIT $pos, $reg");
        return !empty($sqlinstanpro)?$sqlinstanpro:false;
    }

    public function listaTemporalesInstalacionesCantidad(){
        $sqlinstanpro=self::sentencia("select count(*) as cantidad from datosInstalacion where tmp='SI' and servicio='Instalacion' and fecha > 20181230 order by fecha desc");
        $cant=array_shift($sqlinstanpro);
        if($cant->cantidad>=1){
            return $cant->cantidad;
        }else{
            return false;
        }
    }

    public  function listaTemporalesInstalacionesFLUJO($pos,$reg){
        $sqlinstanpro=self::sentencia("SELECT operaciones.idoperacion iddatosInstalacion,operaciones.numeroOrte orte, SUBSTRING(replace(fechaInstalacion,'/','-'),1,10) fecha, case when operaciones.estado=15 then 'Realizado' when operaciones.estado=321 then 'Reprogramar' when operaciones.estado=36 then 'Listo monitoreo' when operaciones.estado=99 then 'Instalado no activado' when operaciones.estado=14 then 'Pendiente sac' when operaciones.estado=18 then 'Anulado' else operaciones.estado end as estado, CONCAT(clientes.apellidosC,' ', clientes.nombre,' | ', clientes.razonSocial) cliente, concat(vehiculos.placa ,'|', vehiculos.chasis) placa, concat(usuarios.usuarioApellidoPaterno, ' ', usuarios.usuarioNombre) operador, operaciones.fechaCreacion fechaRegistro from operaciones inner join clientes on (clientes.idcliente = operaciones.idcliente) inner join vehiculos on (vehiculos.idvehiculo=operaciones.idvehiculo) inner join datosFacturacion on (operaciones.idvehiculo=datosFacturacion.idvehiculos) inner join usuarios on (datosFacturacion.idUsuarioCreado=usuarios.idusuario) where trabajo ='Instalacion' and operaciones.estado in (14,36,321) and replace(fechaInstalacion,'/','-') > '2018-12-32' order by replace(fechaInstalacion,'/','-') LIMIT $pos, $reg");
        return !empty($sqlinstanpro)?$sqlinstanpro:false;
    }

    public function listaTemporalesInstalacionesCantidadFLUJO(){
        $sqlinstanpro=self::sentencia("SELECT count(*) cantidad from operaciones  where trabajo ='Instalacion' and operaciones.estado in (14,36,321) and replace(fechaInstalacion,'/','-') > '2018-12-32' order by replace(fechaInstalacion,'/','-') ");
        $cant=array_shift($sqlinstanpro);
        if($cant->cantidad>=1){
            return $cant->cantidad;
        }else{
            return false;
        }
    }

    public static function listadeTemporales(){
        $sqlinstanpro=self::sentencia("SELECT * FROM datosInstalacion WHERE tmp='SI' AND idoperacion='0' ");
        return !empty($sqlinstanpro)?$sqlinstanpro:false;
    }

    public function eliminarTmpPorId(){
        global $db;
        $sqlll="DELETE FROM datosInstalacion WHERE iddatosInstalacion='";
        $sqlll.=$db->escapeString(limpiar($this->iddatosInstalacion))."'";
        return ($db->query($sqlll))?true:false;
    }
    public function verificarSiOrteEstaenTMP(){
        global $db;
        if($this->orte==''){
            return false;
        }
        $sqlinstanpro=self::sentencia("SELECT * FROM datosInstalacion WHERE tmp='SI' AND idoperacion='0' AND orte='".$db->escapeString($this->orte)."' LIMIT 1");
        return !empty($sqlinstanpro)? array_shift($sqlinstanpro):false;
    }

    public static function solduplicado($iddatosInstalacion){
        global $db;
        $sql="UPDATE datosInstalacion SET tmp='' ";
        $sql.=" WHERE iddatosInstalacion= '"; 
        $sql.=$db->escapeString($iddatosInstalacion)."'";
        return ($db->query($sql))?true:false;
    }

    public function agregaridOperacion(){
        global $db;
        $sql="UPDATE datosInstalacion SET tmp='',";
        $sql.="idoperacion='";
        $sql.=$db->escapeString($this->idoperacion)."' ";
        $sql.=" WHERE iddatosInstalacion= '"; 
        $sql.=$db->escapeString($this->iddatosInstalacion)."'";
        return ($db->query($sql))?true:false;
    }

	public function obtenerTodosRegistrosMes(){
        $mes=date('Ym');
		$obtenerRegistroSubidoQ=self::sentencia("SELECT COUNT(*) as cantidad FROM datosInstalacion WHERE fecha LIKE '$mes%'  LIMIT 1");
        $data=array_shift($obtenerRegistroSubidoQ);
		return $data->cantidad;
	}

	public function obtenerTodosRegistrosMesData($posisionDePagina, $registrosPorPagina){
        $mes=date('Ym');
		$obtenerRegistroSubidoQ=self::sentencia("SELECT * FROM datosInstalacion WHERE fecha LIKE '$mes%' ORDER BY iddatosInstalacion DESC LIMIT $posisionDePagina ,  $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ)?$obtenerRegistroSubidoQ:false;
	}
	public function obtenerTodosRegistrosHoy(){
        $mes=date('Ymd');
		$obtenerRegistroSubidoQ=self::sentencia("SELECT COUNT(*) as cantidad FROM datosInstalacion WHERE fecha LIKE '$mes%'  LIMIT 1");
        $data=array_shift($obtenerRegistroSubidoQ);
		return $data->cantidad;
	}
	public function obtenerTodosRegistrosHoyData($posisionDePagina, $registrosPorPagina){
        $mes=date('Ymd');
		$obtenerRegistroSubidoQ=self::sentencia("SELECT * FROM datosInstalacion WHERE fecha LIKE '$mes%' ORDER BY iddatosInstalacion DESC LIMIT $posisionDePagina ,  $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ)?$obtenerRegistroSubidoQ:false;
	}
	public function obtenerTodosRegistrosTodos(){
		$obtenerRegistroSubidoQ=self::sentencia("SELECT COUNT(*) as cantidad FROM datosInstalacion LIMIT 1");
        $data=array_shift($obtenerRegistroSubidoQ);
		return $data->cantidad;
	}

	public function obtenerTodosRegistrosTodosDatos($posisionDePagina, $registrosPorPagina){
		$obtenerRegistroSubidoQ=self::sentencia("SELECT * FROM datosInstalacion ORDER BY iddatosInstalacion DESC LIMIT $posisionDePagina ,  $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ)?$obtenerRegistroSubidoQ:false;
	}

	public function obtenerTodosRegistrosMesSna(){
        $anio=date('Y');
        $anio.=$this->fecha;
		$obtenerRegistroSubidoQ=self::sentencia("SELECT COUNT(*) as cantidad FROM datosInstalacion WHERE fecha LIKE '$anio%' AND servicio LIKE '$this->servicio'  LIMIT 1");
        $data=array_shift($obtenerRegistroSubidoQ);
		return $data->cantidad;
	}

	public function obtenerTodosRegistrosMesDataSna($posisionDePagina, $registrosPorPagina){
        $anio=date('Y');
        $anio.=$this->fecha;
		$obtenerRegistroSubidoQ=self::sentencia("SELECT * FROM datosInstalacion WHERE fecha LIKE '$anio%' AND servicio LIKE '$this->servicio' ORDER BY iddatosInstalacion DESC LIMIT $posisionDePagina ,  $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ)?$obtenerRegistroSubidoQ:false;
	}

	public function obtenerTodosRegistrosMesCna(){
        $anio=$this->fecha;
		$obtenerRegistroSubidoQ=self::sentencia("SELECT COUNT(*) as cantidad FROM datosInstalacion WHERE fecha LIKE '$anio%' AND servicio LIKE '$this->servicio'  LIMIT 1");
        $data=array_shift($obtenerRegistroSubidoQ);
		return $data->cantidad;
	}

	public function obtenerTodosRegistrosMesDataCna($posisionDePagina, $registrosPorPagina){
        $anio=$this->fecha;
		$obtenerRegistroSubidoQ=self::sentencia("SELECT * FROM datosInstalacion WHERE fecha LIKE '$anio%' AND servicio LIKE '$this->servicio' ORDER BY iddatosInstalacion DESC LIMIT $posisionDePagina ,  $registrosPorPagina");
		return !empty($obtenerRegistroSubidoQ)?$obtenerRegistroSubidoQ:false;
	}
    public function obtenerDatosDeInstalacionF($datos){
        $sqlobdif=self::sentencia("SELECT * FROM datosInstalacion WHERE orte LIKE '$datos' OR cliente LIKE '$datos' OR placa LIKE '$datos' OR chasis LIKE '$datos' OR imei LIKE '$datos' OR id LIKE '$datos' LIMIT 15");
        return !empty($sqlobdif)?$sqlobdif:false;
    }


		public function actualizarDatosVinculacion(){
			global $db;
			global $session;
			$sql="UPDATE datosInstalacion SET ";
			$sql.="cliente = '".$db->escapeString($this->cliente)."' ";
			$sql.=" WHERE idoperacion = '$this->idoperacion'";
			if ($db->query($sql)){
				return true;
			}else{
				return false;
			}
		}


		public function actualizarDatosActivacionDI(){
			global $db;
			global $session;
			$sql="UPDATE datosInstalacion SET ";
			$sql.=" estado= '4' ";
			$sql.=" WHERE idoperacion = '$this->idoperacion'";
			if ($db->query($sql)){
				return true;
			}else{
				return false;
			}
		}

	}
}
?>

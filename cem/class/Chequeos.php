<?php 

if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
class Chequeos extends Recursos
{
	public $idchequeo;	
	public $tipoChequeo;	
	public $plan;	
	public $dniRuc;	
	public $nombres;	
	public $placa;		
	public $chasis;		
	public $marcaModelo;		
	public $correo2;		
	public $motivo;
	public $celular;
	public $telefono;	
	public $imei;	
	public $id;		
	public $correo;
	public $fechaChequeoEnSac;		
	public $personaContacto;		
	public $numeroContacto;			
	public $ubicacionSistema;		
	public $lugar;	
	public $departamentoc;	
	public $provinciac;	
	public $distritoc;	
	public $direccion;	
	public $referencia;	
	public $coordenadas;	
	public $fechaInstalacion;
	public $observacion;
	public $pruebasRealizadas;
	public $conclusion;
	public $fechaHoraChequeoLinea;
	public $fechaAperturaChequeoLinea;
	public $fechaConfirmacionLinea;
	public $idConfimadorChequeoLinea;
	public $idCargo;		
	public $idCreador;
	public $fechaRegistro;	
	public $estado;
	public $fechaConfirmacionTecnico;
	public $idTecnicoConfirmador;
	public $observacionTecnica;
	public $instalador;
	public $idMonitoreo;
	public $cantidad;
	public $seguimientoRealizado;
	public $idcustomer;
	public $idvehicle;
	public $idlocation;
	public $odometro;
	public $descripcionOrden;
	public $numeroOrden;
	public $idReprogramado;
	function __construct()
	{
	}
	public function registrarChequeoMonitoreo(){
		global $db;
		global $session;
		$this->fechaRegistro=date("YmdHis");
		$sql="INSERT INTO chequeos(plan,dniRuc,nombres,placa,chasis,marcaModelo,observacion,motivo,personaContacto,numeroContacto,celular,telefono,correo,lugar,departamentoc,provinciac,distritoc, direccion,referencia,fechaHoraChequeoLinea,idCreador,idcustomer,idvehicle,idlocation,descripcionOrden,numeroOrden,idReprogramado,fechaRegistro) VALUES ('";
		$sql.=$db->escapeString($this->plan)."' , '";
		$sql.=$db->escapeString($this->dniRuc)."' , '";
		$sql.=$db->escapeString($this->nombres)."' , '";
		$sql.=$db->escapeString($this->placa)."' , '";
		$sql.=$db->escapeString($this->chasis)."' , '";
		$sql.=$db->escapeString($this->marcaModelo)."' , '";
		$sql.=$db->escapeString($this->observacion)."' , '";
		$sql.=$db->escapeString($this->motivo)."' , '";
		$sql.=$db->escapeString($this->personaContacto)."' , '";
		$sql.=$db->escapeString($this->numeroContacto)."' , '";
		$sql.=$db->escapeString($this->celular)."' , '";
		$sql.=$db->escapeString($this->telefono)."' , '";
		$sql.=$db->escapeString($this->correo)."' , '";
		$sql.=$db->escapeString($this->lugar)."' , '";
		$sql.=$db->escapeString($this->departamentoc)."' , '";
		$sql.=$db->escapeString($this->provinciac)."' , '";
		$sql.=$db->escapeString($this->distritoc)."' , '";
		$sql.=$db->escapeString($this->direccion)."' , '";
		$sql.=$db->escapeString($this->referencia)."' , '";
		$sql.=$db->escapeString($this->fechaHoraChequeoLinea)."' , '";
		$sql.=$db->escapeString($session->idusuario)."' , '";
		$sql.=$db->escapeString($this->idcustomer)."' , '";
		$sql.=$db->escapeString($this->idvehicle)."' , '";
		$sql.=$db->escapeString($this->idlocation)."' , '";
		$sql.=$db->escapeString($this->descripcionOrden)."' , '";
		$sql.=$db->escapeString($this->numeroOrden)."' , '";
		$sql.=$db->escapeString($this->idReprogramado)."' , '";
		$sql.=$db->escapeString($this->fechaRegistro)."' )";
		if ($db->query($sql)){
			$this->idchequeo=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}

	}

	public function registrarChequeoDesdeMonitoreo(){
		global $db;
		global $session;
		$this->fechaRegistro=date("YmdHis");
		$sql="INSERT INTO chequeos(plan,dniRuc,nombres,placa,chasis,marcaModelo,observacion,motivo,celular,telefono,correo,lugar,direccion,referencia,fechaHoraChequeoLinea,idCreador,idMonitoreo,idcustomer,idvehicle,idlocation,fechaRegistro) VALUES ('";
		$sql.=$db->escapeString($this->plan)."' , '";
		$sql.=$db->escapeString($this->dniRuc)."' , '";
		$sql.=$db->escapeString($this->nombres)."' , '";
		$sql.=$db->escapeString($this->placa)."' , '";
		$sql.=$db->escapeString($this->chasis)."' , '";
		$sql.=$db->escapeString($this->marcaModelo)."' , '";
		$sql.=$db->escapeString($this->observacion)."' , '";
		$sql.=$db->escapeString($this->motivo)."' , '";
		$sql.=$db->escapeString($this->celular)."' , '";
		$sql.=$db->escapeString($this->telefono)."' , '";
		$sql.=$db->escapeString($this->correo)."' , '";
		$sql.=$db->escapeString($this->lugar)."' , '";
		$sql.=$db->escapeString($this->direccion)."' , '";
		$sql.=$db->escapeString($this->referencia)."' , '";
		$sql.=$db->escapeString($this->fechaRegistro)."' , '";
		$sql.=$db->escapeString($session->idusuario)."' , '";
		$sql.=$db->escapeString($session->idusuario)."' , '";
		$sql.=$db->escapeString($this->idcustomer)."' , '";
		$sql.=$db->escapeString($this->idvehicle)."' , '";
		$sql.=$db->escapeString($this->idlocation)."' , '";
		$sql.=$db->escapeString($this->fechaRegistro)."' )";
		if ($db->query($sql)){ 
			$this->idchequeo=$db->ultimoIdInsertado();
			return true;
		}
		else{
			return false;
		}
		

	}

	public static function obtenerChequeoTrackYO(){
		global $session;
		return $obtenerChequeoTrack=self::sentencia("SELECT * FROM chequeos WHERE idCreador=$session->idusuario AND idMonitoreo=$session->idusuario AND estado =0 ORDER BY fechaHoraChequeoLinea ASC");
	}

	public static function obtenerChequeoTrackOtrasAreas(){
		return $obtenerChequeoTrack=self::sentencia("SELECT * FROM chequeos WHERE (estado=0 OR estado = 321) and idCreador<>'0' ORDER BY fechaHoraChequeoLinea");
		// C.placa, C.nombres, C.fechaHoraChequeoLinea,C.estado,C.idCargo, 1<>'SELECT idrol FROM usuarios WHERE idusuario =C.idCreador'
		// SELECT C.placa, C.nombres, C.fechaHoraChequeoLinea,C.estado,C.idCargo,C.idCreador,C.idchequeo FROM chequeos C JOIN usuarios U ON(C.idCreador = U.idusuario AND U.idrol<>1) ORDER BY fechaHoraChequeoLinea 
	}
	public static function obtenerChequeosGestionados(){
		$obtenerChequeosGestionados=self::sentencia("SELECT * FROM chequeos WHERE estado <> 0 ");
		return !empty($obtenerChequeosGestionados)?$obtenerChequeosGestionados:false;
	}


	public static function obtenerChequeoTrackPorId($id){
		$obtenerChequeoTrackPorIdQ=self::sentencia("SELECT * FROM chequeos WHERE idchequeo ='$id' LIMIT 1");
		return  !empty($obtenerChequeoTrackPorIdQ) ? array_shift($obtenerChequeoTrackPorIdQ) : false ;
	}

	public function validarExistenciaPendiente($numeroOrden){
		global $db;
		$validarExistenciaPendienteQ=self::sentencia("SELECT numeroOrden FROM chequeos WHERE numeroOrden='".$db->escapeString($numeroOrden)."' AND estado = 0");
		return !empty($validarExistenciaPendienteQ) ? true :false;
	}

	public function validarExistenciaPendienteChasis($chasis){
		global $db;
		$validarExistenciaPendienteQ=self::sentencia("SELECT chasis FROM chequeos WHERE chasis='".$db->escapeString($chasis)."' AND estado = 0");
		return !empty($validarExistenciaPendienteQ) ? true :false;
	}

	public static function obtenerProgramacionChequeo($fecha){
		global $db;
        $fecha=str_replace(":", "", $fecha);
		$fecha=limpiar($fecha);
		$obtenerProgramacionChequeoQ=self::sentencia("SELECT * FROM chequeos WHERE fechaHoraChequeoLinea LIKE '".$db->escapeString($fecha)."' AND estado='0' ");
		return !empty($obtenerProgramacionChequeoQ)?$obtenerProgramacionChequeoQ:false;
	}	


	public static function obtenerProgramacionChequeoDIA($fecha){
		global $db;
		$fecha=formatodiaLimpio($fecha);
		$obtenerProgramacionChequeoQ=self::sentencia("SELECT * FROM chequeos WHERE  fechaHoraChequeoLinea LIKE '$fecha%' AND estado='0' ");
		return !empty($obtenerProgramacionChequeoQ)?$obtenerProgramacionChequeoQ:false;
	}	


	public function obtenerChequeosRealizados(){
		global $session;
		$obchR=self::sentencia("SELECT * FROM chequeos WHERE (estado ='64' OR estado ='65' OR estado = '66')  AND  idCreador ='".$session->idusuario."' LIMIT 70 ");
		return !empty($obchR) ? $obchR :false;
	}

	public function asignarChequeo(){
		global $db;
		global $session;
		$this->fechaAperturaChequeoLinea=date("YmdHis");
		$asignarChequeo="UPDATE chequeos SET fechaAperturaChequeoLinea=";
		$asignarChequeo.=$db->escapeString($this->fechaAperturaChequeoLinea).", ";
		$asignarChequeo.=" idCargo='".$session->idusuario."' WHERE idchequeo='".$db->escapeString($this->idchequeo)."'";
		if ($db->query($asignarChequeo)) {
			return true;
		} else {
			return false;
		}
	}
	public function verificarDisponible(){
		$verificarDisponibleQ=self::sentencia("SELECT * FROM chequeos WHERE idCargo = '0' AND  idchequeo ='".$this->idchequeo."'");
		return !empty($verificarDisponibleQ)?true:false;
	}

	public function guardarComoChequeoEnLinea(){
		global $db;
		global $session;
		$this->fechaConfirmacionLinea=date("YmdHis");
		$sql="UPDATE chequeos SET conclusion='";
		$sql.=$db->escapeString($this->conclusion)."', coordenadas ='";		
		$sql.=$db->escapeString($this->coordenadas)."', fechaConfirmacionLinea ='";
		$sql.=$db->escapeString($this->fechaConfirmacionLinea)."', tipoChequeo ='";
		$sql.=$db->escapeString($this->tipoChequeo)."', estado ='64'";
		$sql.=", idConfimadorChequeoLinea ='$session->idusuario' WHERE idchequeo='".$db->escapeString($this->idchequeo)."'";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}
	public function guardarComoChequeoPorMonitoreo(){
		global $db;
		global $session;
		$this->fechaConfirmacionLinea=date("YmdHis");
		$sql="UPDATE chequeos SET ubicacionSistema='";
		$sql.=$db->escapeString($this->ubicacionSistema)."', ";
		$sql.="seguimientoRealizado ='";
		$sql.=$db->escapeString($this->seguimientoRealizado)."', ";
		$sql.="instalador ='";
		$sql.=$db->escapeString($this->instalador)."', ";
		$sql.="tipoChequeo ='";
		$sql.=$db->escapeString($this->tipoChequeo)."', ";
		$sql.="direccion ='";
		$sql.=$db->escapeString($this->direccion)."', ";
		$sql.="departamentoc ='";
		$sql.=$db->escapeString($this->departamentoc)."', ";
		$sql.="provinciac ='";
		$sql.=$db->escapeString($this->provinciac)."', ";
		$sql.="distritoc ='";
		$sql.=$db->escapeString($this->distritoc)."', ";
		$sql.="lugar ='";
		$sql.=$db->escapeString($this->lugar)."', ";
		$sql.="referencia ='";
		$sql.=$db->escapeString($this->referencia)."', ";
		$sql.="coordenadas ='";
		$sql.=$db->escapeString($this->coordenadas)."', ";
		$sql.="fechaInstalacion ='";
		$sql.=$db->escapeString($this->fechaInstalacion)."', ";
		$sql.="personaContacto ='";
		$sql.=$db->escapeString($this->personaContacto)."', ";
		$sql.="numeroContacto ='";
		$sql.=$db->escapeString($this->numeroContacto)."', ";
		$sql.="fechaConfirmacionLinea ='";
		$sql.=$db->escapeString($this->fechaConfirmacionLinea)."', ";
		$sql.="idConfimadorChequeoLinea ='";
		$sql.=$db->escapeString($session->idusuario)."', ";
		$sql.="fechaChequeoEnSac ='";
		$sql.=$db->escapeString($this->fechaChequeoEnSac)."', ";
		$sql.="motivo ='";
		$sql.=$db->escapeString($this->motivo)."', ";
		$sql.="estado ='93', ";
		$sql.="observacion ='";
		$sql.=$db->escapeString($this->observacion)."' WHERE ";
		$sql.=" idchequeo = '".$db->escapeString($this->idchequeo)."'";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}

	}

	public function guardarComoChequeoDesinstalacion(){
		global $db;
		global $session;
		$this->fechaConfirmacionLinea=date("YmdHis");
		$sql="INSERT INTO chequeos (tipoChequeo,plan,dniRuc,nombres,placa, chasis,marcaModelo,direccion,departamentoc,provinciac,distritoc,lugar,referencia,coordenadas,fechaInstalacion,personaContacto,numeroContacto,fechaConfirmacionLinea,idConfimadorChequeoLinea,fechaChequeoEnSac,motivo,estado,seguimientoRealizado,idCreador ,ubicacionSistema, instalador ,observacion) VALUES ('";
		$sql.=$db->escapeString($this->tipoChequeo)."', '";
		$sql.=$db->escapeString($this->plan)."', '";
		$sql.=$db->escapeString($this->dniRuc)."', '";
		$sql.=$db->escapeString($this->nombres)."', '";
		$sql.=$db->escapeString($this->placa)."', '";
		$sql.=$db->escapeString($this->chasis)."', '";
		$sql.=$db->escapeString($this->marcaModelo)."', '";
		$sql.=$db->escapeString($this->direccion)."', '";
		$sql.=$db->escapeString($this->departamentoc)."', '";
		$sql.=$db->escapeString($this->provinciac)."', '";
		$sql.=$db->escapeString($this->distritoc)."', '";
		$sql.=$db->escapeString($this->lugar)."', '";
		$sql.=$db->escapeString($this->referencia)."', '";
		$sql.=$db->escapeString($this->coordenadas)."', '";
		$sql.=$db->escapeString($this->fechaInstalacion)."', '";
		$sql.=$db->escapeString($this->personaContacto)."', '";
		$sql.=$db->escapeString($this->numeroContacto)."', '";
		$sql.=$db->escapeString($this->fechaConfirmacionLinea)."', '";
		$sql.=$db->escapeString($session->idusuario)."', '";
		$sql.=$db->escapeString($this->fechaChequeoEnSac)."', '";
		$sql.=$db->escapeString($this->motivo)."', '";
		$sql.=$db->escapeString($this->estado)."', '";
		$sql.=$db->escapeString($this->seguimientoRealizado)."', '";
		$sql.=$db->escapeString($this->idCreador)."', '";
		$sql.=$db->escapeString($this->ubicacionSistema)."', '";
		$sql.=$db->escapeString($this->instalador)."', '";
		$sql.=$db->escapeString($this->observacion)."')";
		if ($db->query($sql)) {
			$this->idchequeo=$db->ultimoIdInsertado();
			return true;
		}else{
			return false;
		}
	}

/////////////////////////paginacion /////////////77
	public function obtenerRegistrosSubidosCantida(){
		global $session;
		$obtenerRegistroSubidoQ=self::sentencia("SELECT COUNT(*) as cantidad FROM chequeos WHERE idCreador = ".$session->idusuario." AND estado='0' LIMIT 1");
		return !empty($obtenerRegistroSubidoQ) ? array_shift($obtenerRegistroSubidoQ) : false; 
	}

	public function obtenerRegistrosSubidos($posisionDePagina,$registrosPorPagina){
		global $session;
		$obtenerRegistroSubidoQ=self::sentencia("SELECT * FROM chequeos WHERE idCreador = ".$session->idusuario." AND estado='0' LIMIT $posisionDePagina, $registrosPorPagina ");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}
	////////////resutado de busqueda//
	public function buscarParaFiltro($datos){
		global $session;
		global $db;	
		$datos=$db->escapeString($datos);		
		$obtenerRegistroSubidoQ=self::sentencia("SELECT *  FROM chequeos WHERE idCreador = ".$session->idusuario."  AND (nombres LIKE '$datos' OR placa LIKE '$datos')");
		return !empty($obtenerRegistroSubidoQ) ? $obtenerRegistroSubidoQ : false;
	}
	public static function obtenerPlan($idchequeo){
		 $oplan=self::sentencia("SELECT plan,fechaChequeoEnSac FROM chequeos WHERE idchequeo = '$idchequeo' LIMIT 1");
		 return !empty($oplan)? array_shift($oplan):false;
	}

	public function actualizarAlgunosDatos(){
		global $db;
		$sql="UPDATE chequeos SET ";
		$sql.=" motivo='";
		$sql.=$db->escapeString($this->motivo)."', ";
		$sql.=" personaContacto='";
		$sql.=$db->escapeString($this->personaContacto)."', ";
		$sql.=" numeroContacto='";
		$sql.=$db->escapeString($this->numeroContacto)."', ";
		$sql.=" fechaHoraChequeoLinea='";
		$sql.=$db->escapeString($this->fechaHoraChequeoLinea)."' ";
		$sql.=" WHERE idchequeo='";
		$sql.=$db->escapeString($this->idchequeo)."'";
		return ($db->query($sql))?true:false;
	}
	
	public function guardarSeguimientoRealizado(){
		global $db;
		$this->fechaConfirmacionLinea=date("YmdHis");
		$sql="UPDATE chequeos SET ";
		$sql.=" tipoChequeo='";
		$sql.=$db->escapeString($this->tipoChequeo)."', ";
		$sql.=" fechaConfirmacionLinea='";
		$sql.=$db->escapeString($this->fechaConfirmacionLinea)."', ";
		$sql.=" estado='";
		$sql.=$db->escapeString($this->estado)."', ";
		$sql.=" seguimientoRealizado='";
		$sql.=$db->escapeString($this->seguimientoRealizado)."' ";
		$sql.=" WHERE idchequeo='";
		$sql.=$db->escapeString($this->idchequeo)."'";
		$db->query($sql);
	}
	
	public function reprogramarChequeoEnLinea(){
		global $session;
		global $db;
        $rsql="UPDATE chequeos SET estado='$this->estado',";
        $rsql.="seguimientoRealizado='";
        $rsql.=$this->seguimientoRealizado."', ";
        $rsql.="fechaHoraChequeoLinea='";
        $rsql.=$this->fechaHoraChequeoLinea."', ";
        $rsql.="personaContacto='";
        $rsql.=$this->personaContacto."', ";
        $rsql.="estado='0', ";
        $rsql.="numeroContacto='";
        $rsql.=$this->numeroContacto."', ";
        $rsql.="idCargo='0', ";
        $rsql.="idreprogramado='";
        $rsql.=$session->idusuario."' ";
        $rsql.=" WHERE idchequeo='";
        $rsql.=$this->idchequeo."' ";
        return ($db->query($rsql))?true:false;
	}

	public function cancelarChequeoEnLinea(){
		global $db;
		global $session;
		$this->estado='32';
		$this->guardarSeguimientoRealizado();
		$sql="UPDATE chequeos SET estado='";
		$sql.=$this->estado."', ";
		$sql.=" personaContacto='";
		$sql.=$this->personaContacto."',";
		$sql.=" numeroContacto='";
		$sql.=$this->numeroContacto."',";
		$sql.=" motivo='";
		$sql.=$this->motivo."',";
		$sql.=" idConfimadorChequeoLinea='";
		$sql.=$session->idusuario."', ";
		$sql.=" correo='";
		$sql.=$this->correo."' ";
		$sql.=" WHERE idchequeo='";
		$sql.=$this->idchequeo."' ";
		if($db->query($sql)){
			return true;
		}else{
			return false;
		}
	}

	public function reprogramarChequeo(){
		global $db;
		global $session;		
		$this->fechaHoraChequeoLinea=date("YmdHi");
		$this->fechaRegistro=date("YmdHis");
		$datosChequeo=self::obtenerChequeoTrackPorId($this->idchequeo);
		$sql="INSERT INTO chequeos (plan, dniRuc, nombres,placa,chasis,marcaModelo,observacion, motivo, celular, telefono,correo,personaContacto,numeroContacto,ubicacionSistema,lugar, departamentoc,provinciac,distritoc,direccion,referencia,coordenadas,fechaInstalacion, idcustomer, instalador ,idvehicle, idlocation, idReprogramado,descripcionOrden,fechaHoraChequeoLinea,fechaRegistro,estado,idCreador) VALUES ('";
		$sql.=$db->escapeString($datosChequeo->plan)."','";
		$sql.=$db->escapeString($datosChequeo->dniRuc)."','";
		$sql.=$db->escapeString($datosChequeo->nombres)."','";
		$sql.=$db->escapeString($datosChequeo->placa)."','";
		$sql.=$db->escapeString($datosChequeo->chasis)."','";
		$sql.=$db->escapeString($datosChequeo->marcaModelo)."','";
		$sql.=$db->escapeString($this->observacion)."','";
		$sql.=$db->escapeString($datosChequeo->motivo)."','";
		$sql.=$db->escapeString($datosChequeo->celular)."','";
		$sql.=$db->escapeString($datosChequeo->telefono)."','";
		$sql.=$db->escapeString($datosChequeo->correo)."','";
		$sql.=$db->escapeString($datosChequeo->personaContacto)."','";
		$sql.=$db->escapeString($datosChequeo->numeroContacto)."','";
		$sql.=$db->escapeString($datosChequeo->ubicacionSistema)."','";
		$sql.=$db->escapeString($datosChequeo->lugar)."','";
		$sql.=$db->escapeString($datosChequeo->departamentoc)."','";
		$sql.=$db->escapeString($datosChequeo->provinciac)."','";
		$sql.=$db->escapeString($datosChequeo->distritoc)."','";
		$sql.=$db->escapeString($datosChequeo->direccion)."','";
		$sql.=$db->escapeString($datosChequeo->referencia)."','";
		$sql.=$db->escapeString($datosChequeo->coordenadas)."','";
		$sql.=$db->escapeString($datosChequeo->fechaInstalacion)."','";
		$sql.=$db->escapeString($datosChequeo->idcustomer)."','";
		$sql.=$db->escapeString($datosChequeo->instalador)."','";
		$sql.=$db->escapeString($datosChequeo->idvehicle)."','";
		$sql.=$db->escapeString($datosChequeo->idlocation)."','";
		$sql.=$db->escapeString($this->idchequeo)."','";
		$sql.=$db->escapeString("reprogramadoSac")."','";
		$sql.=$db->escapeString($this->fechaHoraChequeoLinea)."','";
		$sql.=$db->escapeString($this->fechaRegistro)."','";
		$sql.=$db->escapeString('321')."','";
		$sql.=$db->escapeString($session->idusuario)."')";
		return ($db->query($sql))?true:false;
	}
    public function marcarParaReprogramado(){
        global $db;
        $this->estado='321';
		$sql="UPDATE chequeos SET estado='";
		$sql.=$this->estado."', ";
		$sql.=" observacion='";
		$sql.=$this->observacion."' ";
		$sql.=" WHERE idchequeo='";
		$sql.=$this->idchequeo."' ";
        return ($db->query($sql))?true:false;
    }

	public function historialCemCantidadTotal(){
		$historialSacCantidadTotal=self::sentencia("SELECT COUNT(*) as cantidad FROM chequeos LIMIT 1");
		return !empty($historialSacCantidadTotal)? array_shift($historialSacCantidadTotal) :false;
	}
		public function obtenerHistorialCem($posisionDePagina,$registrosPorPagina){
		$dtp=self::sentencia("SELECT * FROM chequeos ORDER BY fechaConfirmacionLinea DESC LIMIT $posisionDePagina, $registrosPorPagina");
		return !empty($dtp)? $dtp:false;		
	}

	public function obtenerHistorialCEMBusqueda($datos){
		$dtp=self::sentencia("SELECT * FROM chequeos  WHERE nombres LIKE '$datos' OR placa LIKE '$datos' OR chasis LIKE '$datos'OR tipoChequeo LIKE '$datos'  ORDER BY fechaConfirmacionLinea");
		return !empty($dtp)? $dtp:false;
	}
		public function obtenerHistorialCEMBusquedaExcel(){
			$dtp=self::sentencia("SELECT * FROM chequeos  WHERE 1=1  ORDER BY fechaConfirmacionLinea desc");
			return !empty($dtp)? $dtp:false;
		}

    public function verificarAnteriorExistencia(){
        $ver=self::sentencia("SELECT COUNT(*) as cantidad FROM operaciones WHERE idChequeo = '$this->idchequeo' LIMIT 1");
        $obCan=array_shift($ver);
        if ($obCan->cantidad>1) {
            return $ver=self::sentencia("SELECT numeroOrte as placa, numeroOrden as chasis FROM operaciones WHERE idChequeo = '$this->idchequeo'");
        }else{
            return false; 
        }
    }

}

}

 ?>

<?php


class acciones extends Recursos
{
     public $idaccfi;
     public $financiera;
     public $chasis;
     public $placa;
     public $cliente;
     public $direccion;
     public $usuario;
     public $contrasenia;
     public $observacion;
     public $status;
     public $idusuario;



    public static function obtenerAccesos($idacc){
        if($idacc==''){
            $idtAcc='';
        }else{
            $idtAcc=' AND idaccfi='.$idacc.'';
        }

        $sqlOa = "SELECT idaccfi, financiera, chasis, placa, cliente, direccion, usuario, contrasenia, observacion, status
                 FROM trackdb.access_financiera   WHERE 1=1 ".$idtAcc." order by  idaccfi asc  ";

        $obtenerAccesos=self::sentencia($sqlOa);
        if($idtAcc==''){
            return $obtenerAccesos;
        }else{
            return !empty($obtenerAccesos)?array_shift($obtenerAccesos):false;
        }
    }

    public function registrarAcceso(){
        $this->fechaCreada=date("YmdHis");
        global $db;
        global $session;
        $sql=" INSERT INTO access_financiera(financiera, chasis, placa, cliente, direccion, usuario, contrasenia, observacion, status,idusuario) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString(strtoupper($this->financiera))."' , '";
        $sql.= $db->escapeString(strtoupper($this->chasis))."' , '";
        $sql.= $db->escapeString(strtoupper($this->placa))."' , '";
        $sql.= $db->escapeString(strtoupper($this->cliente))."' , '";
        $sql.= $db->escapeString(strtoupper($this->direccion))."' , '";
        $sql.= $db->escapeString($this->usuario)."' , '";
        $sql.= $db->escapeString($this->contrasenia)."' , '";
        $sql.= $db->escapeString($this->observacion)."' , '";
        $sql.= $db->escapeString($this->status)."' , '";
        $sql.= $db->escapeString($session->idusuario)."')";
        if ($db->query($sql)) {
            return true;
        }else{
            return false;
        }
    }

    public function updateAcceso(){
        global $db;
        global $session;
        $sql=" UPDATE access_financiera SET ";
        $sql.=" financiera ='".$db->escapeString($this->financiera)."', ";
        $sql.=" chasis ='".$db->escapeString($this->chasis)."', ";
        $sql.=" placa ='".$db->escapeString($this->placa)."', ";
        $sql.=" cliente ='".$db->escapeString($this->cliente)."', ";
        $sql.=" direccion ='".$db->escapeString($this->direccion)."' ,";
        $sql.=" usuario ='".$db->escapeString($this->usuario)."', ";
        $sql.=" contrasenia ='".$db->escapeString($this->contrasenia)."', ";
        $sql.=" observacion ='".$db->escapeString($this->observacion)."', ";
        $sql.=" status ='".$db->escapeString($this->status)."' ";
        $sql.=" WHERE idaccfi ='".$db->escapeString($this->idaccfi)."'";
        if ($db->query($sql)) {
            // $this->registrarLiberarTecProvincia();
            return true;
        }else{
            echo "the fail";
        }
    }


}
<?php

class DataAnomalia extends Recursos{

    public $tipo;
    public $sistema;
    public $cliente;
    public $placa;
    public $chasis;
    public $update_at;
    public $created_at;
    public $ultimoreporte;

    public function __construct(){}

    public function truncateTable(){
        global $db;
        $sql = "TRUNCATE TABLE data_anomalia";
        return self::sentencia($sql);
    }

    public function inserDataToTable($data){
        global $db;
		global $session;
        $timestamp = time();
        $currentDate = gmdate('Y-m-d H:i:s', $timestamp);

        $data['tipo'] = ($data['tipo'] == "" || $data['tipo'] == null) ? "ANOMALIA" : $data['tipo'];
        $data['sistema'] = ($data['sistema'] == "" || $data['sistema'] == null) ? "OCTO" : $data['sistema'];
        $data['cliente'] = ($data['cliente'] == "" || $data['cliente'] == null) ? "-" : $data['cliente'];
        $data['matricula'] = ($data['matricula'] == "" || $data['matricula'] == null) ? "-" : $data['matricula'];
        $data['chasis'] = ($data['chasis'] == "" || $data['chasis'] == null) ? "-" : $data['chasis'];
        $data['update_at'] = strval($currentDate);
        $data['created_at'] = strval($currentDate);
        $data['ultimoreporte'] = strval(date('d-m-Y',strtotime("-3 days")));

        $sql = "INSERT INTO data_anomalia (tipo, sistema, cliente, placa, chasis, update_at, created_at, ultimoreporte)".
            "VALUES(".
            "'".$db->escapeString($data['tipo'])."',".
            "'".$db->escapeString($data['sistema'])."',".
            "'".$db->escapeString($data['cliente'])."',".
            "'".$db->escapeString($data['matricula'])."',".
            "'".$db->escapeString($data['chasis'])."',".
            "'".$db->escapeString($data['update_at'])."',".
            "'".$db->escapeString($data['created_at'])."',".
            "'".$db->escapeString($data['ultimoreporte'])."'".
            ")";
        return self::sentencia($sql);
    }

}
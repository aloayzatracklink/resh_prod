<?php

class PostInstalacion extends Recursos
{
    public $idoperealizadas;
    public $numero_orte;
    public $cliente_nombres;
    public $servicio;
    public $fecha;
    public $fecha_format;
    public $fecha_instalacion;
    public $placa;
    public $chasis;
    public $plan;
    public $sistema;
    public $device_imei;
    public $device_id;
    public $odometro;
    public $ubicacion_sistema;
    public $lugar_instalacion;
    public $tecnico_instalador;
    public $servicios_adicionales;
    public $ubicacion_boton_panico;
    public $medida_combustible;
    public $observacion;
    public $tipo_instalacion;
    public $test_ubicacion;
    public $test_bloquear_motor;
    public $test_desbloquear_motor;
    public $test_abrir_pestillos;
    public $test_reiniciar_gps;
    public $test_boton_panico;
    public $habitos_manejo;
    public $test_bateria;
    public $test_encender_motor;
    public $test_apagar_motor;
    public $config_odometro;
    public $estado;
    public $creado_por;
    public $actualizado_por;
    public $fecha_creacion;
    public $fecha_gestionado;
    public $fecha_actualizacion;
    public $cantdias;
    public $cantidad;
    public $gestionado_por;
    public $gestionado_por_nombres;
    public $seguimiento_por;
    public $cuenta;

    public function obtenerListEstadoAnomalia($idest)
    {
        $fil = "";
        if ($idest != "") {
            $fil = " and  ea.id='$idest'";
        }
        $sql = "select ea.id,ea.descripcion,ea.accion,ea.color from estados_anomalias ea where 1=1 $fil";
        $rslt = self::sentencia($sql);
        return $rslt;
    }


    public function ttobtenerlstpostinstalacion($datos, $est, $sist)
    {
        $filt = "";
        if ($est != "") {
            if($est=="PENDIENTE") {
                $est = "PENDIENTE','SEGUIMIENTO";
            }
            $filt .= " and or2.estado in ('$est') ";
        }
        if ($sist != "") {
            $filt .= " and or2.sistema='$sist' ";
        }

        $sql = "select count(*) as cantidad 
            from (
                select DATE_FORMAT(or2.fecha , '%Y-%m-%d') fecha_format, or2.*, CONCAT('Hace ', DATEDIFF(DATE_FORMAT(NOW(), '%Y-%m-%d'), DATE_FORMAT(or2.fecha_creacion, '%Y-%m-%d')), ' dias') cantdias 
                from operaciones_realizadas or2
                where (or2.cliente_nombres like '%$datos%' or or2.placa like '%$datos%' or or2.chasis like '%$datos%') $filt
                order by or2.idoperealizadas desc
            ) dat";

        $rslt = self::sentencia($sql);
        return !empty($rslt) ? array_shift($rslt) : false;
    }

    public function obtenerlstpostinstalacion($datos, $est, $sist, $posisionDePagina, $registrosPorPagina)
    {
        $filt = "";
        if ($est != "") {
            if($est=="PENDIENTE") {
                $est = "PENDIENTE','SEGUIMIENTO";
            }
            $filt .= " and or2.estado in ('$est') ";
        }
        if ($sist != "") {
            $filt .= " and or2.sistema='$sist' ";
        }
        $sql = "select DATE_FORMAT(or2.fecha_creacion , '%Y-%m-%d %H:%i') fecha_format, DATE_FORMAT(or2.fecha_gestionado , '%Y-%m-%d %H:%i') fecha_gestionado, or2.*, CONCAT('Hace ', DATEDIFF(DATE_FORMAT(NOW(), '%Y-%m-%d'), DATE_FORMAT(or2.fecha_creacion, '%Y-%m-%d')), ' dias') cantdias,
                CONCAT(u.usuarioNombre, ' ', u.usuarioApellidoPaterno) gestionado_por_nombres
                from operaciones_realizadas or2
                left join usuarios u on u.idusuario = or2.gestionado_por
                where (or2.cliente_nombres like '%$datos%' or or2.placa like '%$datos%' or or2.chasis like '%$datos%' or or2.numero_orte like '%$datos%' or or2.device_imei like '%$datos%' or or2.device_id like '%$datos%') $filt
                order by or2.idoperealizadas desc LIMIT $posisionDePagina, $registrosPorPagina            ";
        echo $sql;
        $rslt = self::sentencia($sql);
        return $rslt;
    }

    public function finanomaliid($id)
    {

        $sql = " 
               select naa.id,naa.tipo,sistema ,naa.idcustomer ,naa.idlocation ,naa.idvehicle ,naa.cliente ,naa.chasis ,naa.placa,naa.asunto ,naa.cantNotify ,naa.sendMail,naa.observaciones,naa.iduseruso,
              DATE_FORMAT(naa.created_at,'%Y-%m-%d') as create_date,naa.update_at ,iduseruso,CONCAT('Hace ',DATEDIFF(DATE_FORMAT(NOW(),'%Y-%m-%d'),DATE_FORMAT(naa.created_at,'%Y-%m-%d')), ' dias') cantdias
              from notificacion_auto_anomalia naa where 1=1 and naa.id='$id'
              order by id desc 
            ";

        $rslt = self::sentencia($sql);
        return !empty($rslt) ? array_shift($rslt) : false;
    }


    public function asignarCargo()
    {
        global $db;
        global $session;
        $sql = "UPDATE  notificacion_auto_anomalia SET ";
        $sql .= "iduseruso='";
        $sql .= $db->escapeString($session->idusuario) . "' ";
        $sql .= " WHERE id=";
        $sql .= $db->escapeString($this->id);
        return ($db->query($sql)) ? true : false;

    }


    public function updatePostInstalacionEstado($idoperealizada)
    {
        global $db;
        global $session;

        $idusuario = intval($session->idusuario);
        $sql = "UPDATE operaciones_realizadas SET estado = 'GESTIONADO', gestionado_por = $idusuario, fecha_gestionado = NOW() WHERE idoperealizadas = $idoperealizada";
        return ($db->query($sql)) ? true : false;
    }


    public function updateEstadoSeguimiento($idoperealizada)
    {
        global $db;
        global $session;

        $idusuario = intval($session->idusuario);
        $sql = "UPDATE operaciones_realizadas SET estado = 'SEGUIMIENTO', seguimiento_por = $idusuario, fecha_seguimiento = NOW() WHERE idoperealizadas = $idoperealizada";
        return ($db->query($sql)) ? true : false;
    }

    public function saveSeg($seg)
    {
        global $session;
        global $db;
        $sql = "INSERT INTO seguimientos_detalle (body,class,class_id,idusuario) ";
        $sql .= " VALUES ('";
        $sql .= $db->escapeString($seg['body']) . "' , '";
        $sql .= $db->escapeString('ANOMALIA') . "' , '";
        $sql .= $db->escapeString($seg['idanom']) . "' , '";
        $sql .= $db->escapeString($session->idusuario) . "' )";
        if ($db->query($sql)) {
            $this->AgendarSeguimiento($seg);
            return True;
        } else {
            return False;
        }
    }


    public function saveEstado($seg)
    {
        global $session;
        global $db;

        $this->crea_at = date("Y-m-d H:i:s");

        $sql = "INSERT INTO estados_anomalias (descripcion,accion,color,created_at,idusuario) ";
        $sql .= " VALUES ('";
        $sql .= $db->escapeString($seg['body']) . "' , '";
        $sql .= $db->escapeString('1') . "' , '";
        $sql .= $db->escapeString('primary') . "' , '";
        $sql .= $this->crea_at . "' , '";
        $sql .= $db->escapeString($session->idusuario) . "' )";
        if ($db->query($sql)) {
            return True;
        } else {
            return False;
        }
    }

    public function AgendarSeguimiento($seg)
    {
        global $db;

        $username = 'onyx';
        $password = 'bwkvwvyfhijqmuaxye';
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Basic ' . base64_encode("$username:$password")
        );

        $bodyDatta = array(
            "idcustomer" => $seg["idcustomer"],
            "idlocation" => $seg["idlocation"],
            "idvehicle" => $seg["idvehicle"],
            "subject" => $seg['subject'],
            "message" => $seg['body'],
        );

        API::POSTB("http://192.168.2.26:30090/onyx/saveAgendaNotification", $headers, $bodyDatta);

        return true;
    }


    public static function getSEg($id)
    {
        $obj = self::sentencia("select id,body,class_id,idusuario,fecha,created ,creation_at ,last_modify ,updated_at from seguimientos_detalle where class_id='$id' and class = 'ANOMALIA'");
        return !empty($obj) ? $obj : [];
    }


    public function updateEstado()
    {
        global $db;
        global $session;
        $this->update_at = date("Y-m-d H:i:s");
        $sql = " UPDATE estados_anomalias et SET ";
        $sql .= " et.accion ='" . $db->escapeString($this->accion) . "',";
        $sql .= " et.color ='" . $db->escapeString($this->color) . "',";
        $sql .= " et.update_at ='" . $this->update_at . "' ,";
        $sql .= " et.update_user ='" . $db->escapeString($session->idusuario) . "' ";
        $sql .= " WHERE et.id ='" . $db->escapeString($this->id) . "'";

        if ($db->query($sql)) {
            return true;
        } else {
            echo "the fail";
        }
    }


    public function obtenerPostInstalacionExcel()
    {
        $sql = "select DATE_FORMAT(or2.fecha_creacion , '%Y-%m-%d %H:%i') fecha_format, DATE_FORMAT(or2.fecha_gestionado , '%Y-%m-%d %H:%i') fecha_gestionado, or2.*, CONCAT('Hace ', DATEDIFF(DATE_FORMAT(NOW(), '%Y-%m-%d'), DATE_FORMAT(or2.fecha_creacion, '%Y-%m-%d')), ' dias') cantdias 
                from operaciones_realizadas or2
                order by or2.idoperealizadas desc";
        $dtp = self::sentencia($sql);
        return !empty($dtp) ? $dtp : false;
    }

}
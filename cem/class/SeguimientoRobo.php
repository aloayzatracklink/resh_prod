<?php 

if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
     header("location:../404.html");
}else{
class SeguimientoRobo extends Recursos
{
	public $idseguimiento;
	public $idrobo;
	public $tipoContacto;
	public $nombre;
	public $detalles;
	public $notas;
	public $horaRegistro;
	public $idusuario;
	function __construct()
	{
		
	}
	public function crearSeguimientoRobo(){
		global $db;
		$this->horaRegistro=date("YmdHis");
		$sql="INSERT INTO seguimientoRobo (idrobo,tipoContacto,nombre,detalles,horaRegistro,idusuario) ";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString(strtoupper($this->idrobo))."' , '";
		$sql.= $db->escapeString(ucfirst($this->tipoContacto))."' , '";
		$sql.= $db->escapeString(ucfirst($this->nombre))."' , '";
		$sql.= $db->escapeString(ucfirst($this->detalles))."' , '";
		$sql.= $db->escapeString(ucfirst($this->horaRegistro))."' , '";
		$sql.= $db->escapeString($this->idusuario)."') ";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}

		
	}	
	
	public function crearNota(){
		global $db;
		$this->horaRegistro=date("YmdHis");
		$sql="INSERT INTO seguimientoRobo (idrobo,notas,horaRegistro,idusuario) ";
		$sql.=" VALUES ('";
		$sql.= $db->escapeString(strtoupper($this->idrobo))."' , '";
		$sql.= $db->escapeString(ucfirst($this->notas))."' , '";
		$sql.= $db->escapeString(ucfirst($this->horaRegistro))."' , '";
		$sql.= $db->escapeString($this->idusuario)."') ";
		if ($db->query($sql)) {
			return true;
		}else{
			return false;
		}
	}

	public static function obtenerSeguimientoRobo($idrobo){
		global $db;
		$n=0;
		$seguimientoObj=self::sentencia("SELECT * FROM seguimientoRobo WHERE idrobo = $idrobo ORDER BY idseguimientoRobo ASC");
		foreach ($seguimientoObj as $var) :
		$n++;
			?>
				<div class="vertical-timeline-block">
                    <div class="vertical-timeline-icon gray-bg">
                        <i class="fa <?php if ($var->tipoContacto==1): ?>
                        	fa-phone-square
                        <?php elseif($var->tipoContacto==2): ?>
                        	fa fa-phone
                        <?php elseif ($var->tipoContacto==3): ?>fa-envelope-o                  	
                        <?php else: ?>
        					 fa-file-text-o	               	
                        <?php endif ?>-circle"></i>
                    </div>
                    <div class="vertical-timeline-content">
                        <p><?php if ($var->tipoContacto==1): ?>
                        	Llamada realizada a
                        <?php elseif($var->tipoContacto==2): ?>
                        	Llamada recibida de
                        <?php elseif ($var->tipoContacto==3): ?> 
                        	Correo enviado a           	
                        <?php else: ?>
                        <?php endif ?><strong> <?php echo $var->nombre ?></strong></p>
                        <p><?php echo $var->detalles ?>
                        <?php echo $var->notas ?>
                        </p>
                        <span class="vertical-date small text-muted"> <?php echo formato($var->horaRegistro) ?> </span>
                        <small> <br>
                         -- <cite title="" data-original-title=""><?php $usuarioAsesor= Usuario::obtenerUsuario($var->idusuario); echo $usuarioAsesor->usuarioNombre;echo "&nbsp;"; echo $usuarioAsesor->usuarioApellidoPaterno  ?></cite></small>             
                    </div>
                </div>

			<?php
			endforeach;

	}

	public static function obtenerSeguimientoRoboParaMostrar($idrobo){
		global $db;
		$n=0;
		$seguimientoObj=self::sentencia("SELECT * FROM seguimientoRobo WHERE idrobo = $idrobo ORDER BY idseguimientoRobo DESC");
		foreach ($seguimientoObj as $var) :
		$n++;
			?>
				<div class="vertical-timeline-block">
                    <div class="vertical-timeline-icon gray-bg">
                        <i class="fa <?php if ($var->tipoContacto==1): ?>
                        	fa-phone-square
                        <?php elseif($var->tipoContacto==2): ?>
                        	fa fa-phone
                        <?php elseif ($var->tipoContacto==3): ?>fa-envelope-o                  	
                        <?php else: ?>
        					 fa-file-text-o	               	
                        <?php endif ?>-circle"></i>
                    </div>
                    <div class="vertical-timeline-content">
                        <p><?php if ($var->tipoContacto==1): ?>
                        	Llamada realizada a
                        <?php elseif($var->tipoContacto==2): ?>
                        	Llamada recibida de
                        <?php elseif ($var->tipoContacto==3): ?> 
                        	Correo enviado a           	
                        <?php else: ?>
                        <?php endif ?><strong> <?php echo $var->nombre ?></strong></p>
                        <p><?php echo $var->detalles ?>
                        <?php echo $var->notas ?>
                        </p>
                        <span class="vertical-date small text-muted"> <?php echo hace($var->horaRegistro) ?> </span>
                        <small> <br>
                         -- <cite title="" data-original-title=""><?php $usuarioAsesor= Usuario::obtenerUsuario($var->idusuario); echo $usuarioAsesor->usuarioNombre;echo "&nbsp;"; echo $usuarioAsesor->usuarioApellidoPaterno  ?></cite></small>             
                    </div>
                </div>

			<?php
			endforeach;

	}
	public static function obtenerSeguimientoRoboParaWord($idrobo){
		global $db;
		$n=0;
		$seguimientoObj=self::sentencia("SELECT * FROM seguimientoRobo WHERE idrobo = $idrobo ORDER BY idseguimientoRobo ASC");
		foreach ($seguimientoObj as $var) :
		$n++;
			?>
			<UL>
				<LI><P STYLE='margin-right: 0.36in; margin-bottom: 0in'>
					<FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><FONT COLOR='#1f3864'><FONT FACE='Calibri, serif'><FONT SIZE=3><B><?php echo formato($var->horaRegistro) ?>.</B></FONT></FONT></FONT><FONT COLOR='#1f3864'><FONT FACE='Calibri, serif'></FONT></FONT><FONT COLOR='#1f3864'><FONT FACE='Calibri, serif'><FONT SIZE=3>
					<?php if ($var->tipoContacto==4): ?>
						<?php echo $var->notas ?>
					<?php endif ?>
					<?php if ($var->tipoContacto==1): ?>
						Llamada realizada a <?php echo $var->nombre ?> <?php echo $var->detalles ?>
					<?php endif ?>
					<?php if ($var->tipoContacto==2): ?>
						Llamada decibida de <?php echo $var->nombre ?> <?php echo $var->detalles ?><?php endif ?>
						<small> <br>
							-- <cite title="" data-original-title=""><?php $usuarioAsesor= Usuario::obtenerUsuario($var->idusuario); echo $usuarioAsesor->usuarioNombre;echo "&nbsp;"; echo $usuarioAsesor->usuarioApellidoPaterno  ?></cite></small> 
						</FONT></FONT></FONT></FONT></FONT></P>
					</LI>
				</UL>
<?php
			endforeach;

	}
 	
}
}
 ?>
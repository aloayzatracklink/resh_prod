<?php
if (__FILE__ == $_SERVER['SCRIPT_FILENAME'])
{
    header("location:../404.html");
}else {

    class noAtender extends Recursos
    {
        public $id;
        public $idcustomer;
        public $idlocation;
        public $idvehicle;
        public $created_at;
        public $updated_at;

        function __construct()
        {
        }

        public function actualizarDatosDeInstalacion(){
            global $db;
            global $session;
            $this->created_at=date("YmdHis");
            $sql=" INSERT INTO no_atenders(idcustomer,idlocation,idvehicle,created_at) VALUES ( '";
            $sql.=$db->escapeString($this->idcustomer)."' , '";
            $sql.=$db->escapeString($this->idlocation)."' , '";
            $sql.=$db->escapeString($this->idvehicle)."' , '";
            $sql.=$db->escapeString($this->created_at)."'  )";

            if ($db->query($sql)){
                return true;
            }else{
                return false;
            }
        }

        public static function obtenerSiexiste($idcustomer,$idlocation,$idvehicle){

            $oeo=self::sentencia("SELECT * FROM no_atenders WHERE idcustomer='".$idcustomer."' and idlocation='".$idlocation."'
            and idvehicle='".$idvehicle."' LIMIT 1");

            return !empty($oeo)?array_shift($oeo):false;
        }

    }
}
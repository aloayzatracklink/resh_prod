<?php


class SeguimientoPK extends Recursos
{
 public $idgesppk;
 public $type;
 public $policy;
 public $estado;
 public $observacion;
 public $idsuariocargo;
 public $creation_at;
 public $dateUpdate;

 public $cantidad;
 public $cliente;
 public $placa;
 public $chasis;
 public $fechaI;
 public $km;
public $encargado;

    public $body;
    public $class_id;
    public $idusuario;
    public $fecha;
    public $created;
    public $last_modify;
    public $updated_at;
    public $update_at_auto;
    public $dateCargo;

    public $seguimiento;
    public $created_at;
    public $color;
    public $idtrama;
    public $etiqueta;

    public $id;
    public $descripcion;
    public $visita_presencial;



    public function ttobtenerlstSeguimiento($datos,$est,$tipo) {
        $filt="";
        if($est!=""){
            $filt.=" and g.estado='$est' ";
        }
        if($tipo!=""){
            $filt.=" and g.type='$tipo' ";
        }

        $sql=" select count(*) as cantidad from ( SELECT g.idgesppk ,g.`type`,g.policy,concat(ti.apellido_asegurado,' ',ti.nombre_asegurado) as cliente,ti.placa ,ti.chasis,
          DATE_FORMAT(o.fechaInstalacion,'%Y-%m-%d')fechaI,g.km km,g.estado , g.idsuariocargo ,g.creation_at ,g.dateUpdate,g.observacion
          FROM gestionppk g 
          INNER JOIN tramaIntegrancion ti on ti.policy = g.policy 
          LEFT JOIN operaciones o on o.idoperacion =ti.idoperacion 
          WHERE 1=1 and (g.policy like '%$datos%' or concat(ti.apellido_asegurado,' ',ti.nombre_asegurado) like '%$datos%'
          or ti.chasis like '%$datos%' or ti.placa like '%$datos%' )  $filt  )dat ";

        $rslt = self::sentencia($sql);
        return !empty($rslt)? array_shift($rslt):false;
    }


    public function obtenerlstSeguimiento($datos,$est,$tipo,$posisionDePagina,$registrosPorPagina) {

        $filt="";
        if($est!=""){
            $filt.=" and g.estado='$est' ";
        }
        if($tipo!=""){
            $filt.=" and g.type='$tipo' ";
        }

        $sql=" SELECT g.idgesppk ,g.`type`,g.policy,concat(ti.apellido_asegurado,' ',ti.nombre_asegurado) as cliente,ti.placa ,ti.chasis,IFNULL(g.etiqueta,0) as etiqueta,
          DATE_FORMAT(o.fechaInstalacion,'%Y-%m-%d')fechaI,g.km km,g.estado , g.idsuariocargo ,g.creation_at ,g.dateUpdate,g.observacion,
           case when  (SELECT count(*) from gestionppk g2 where g2.policy=g.policy and g2.`type` = g.`type`) =2 THEN 'fff9cd'
             when (SELECT count(*) from gestionppk g2 where g2.policy=g.policy and g2.`type` = g.`type`) >2 THEN 'f9c6c6'
             else 'ffffff' end color
          FROM gestionppk g 
          INNER JOIN tramaIntegrancion ti on ti.policy = g.policy 
          LEFT JOIN operaciones o on o.idoperacion =ti.idoperacion 
          WHERE 1=1 and (g.policy like '%$datos%' or concat(ti.apellido_asegurado,' ',ti.nombre_asegurado) like '%$datos%'
          or ti.chasis like '%$datos%' or ti.placa like '%$datos%' ) 
          and g.estado!=15 $filt   order by  DATE_FORMAT(o.fechaInstalacion,'%Y-%m-%d') asc LIMIT $posisionDePagina, $registrosPorPagina  ";


        $rslt = self::sentencia($sql);
        return $rslt;
    }



    public static function getSEg($id){
        $obj=self::sentencia("select id,body,class_id,idusuario,fecha,created ,creation_at ,last_modify ,updated_at from seguimientos_detalle where class_id='$id' and class = 'GESTIONPPK'");
        return !empty($obj)?$obj:[];
    }


    public function obtenerSeguimientoPKExcel(){

        $sql=" SELECT  g.idgesppk ,g.`type`,g.policy,concat(ti.apellido_asegurado,' ',ti.nombre_asegurado) as cliente,ti.placa ,ti.chasis,
          DATE_FORMAT(o.fechaInstalacion,'%Y-%m-%d')fechaI,g.km km, g.idsuariocargo ,g.creation_at ,g.dateUpdate,g.observacion,
              (SELECT e.estado FROM estados e where e.codigo=g.estado) as estado,dateCargo,
              (SELECT egp.descripcion FROM etiquetas_gestionpk egp WHERE egp.id=  g.etiqueta) as etiqueta,
              (SELECT GROUP_concat('(',DATE_FORMAT(fecha,'%d/%m/%Y'),')',body)  FROM seguimientos_detalle sd where sd.class_id =g.idgesppk and sd.class='GESTIONPPK') seguimiento,
              (SELECT concat(usuarioNombre,usuarioApellidoPaterno) FROM usuarios us WHERE us.idusuario=IFNULL(g.idsuariocargo,0)) encargado
             FROM gestionppk g 
          INNER JOIN tramaIntegrancion ti on ti.policy = g.policy 
          LEFT JOIN operaciones o on o.idoperacion =ti.idoperacion 
              WHERE 1=1 order by g.creation_at desc ";


        $dtp=self::sentencia($sql);
        return !empty($dtp)? $dtp:false;
    }




    public function findSeguimientoId($id) {

        $sql=" 
           SELECT g.idgesppk ,g.`type`,g.policy,concat(ti.apellido_asegurado,' ',ti.nombre_asegurado) as cliente,ti.placa ,ti.chasis,
          DATE_FORMAT(o.fechaInstalacion,'%Y-%m-%d')fechaI,g.km km,g.estado , g.idsuariocargo ,g.creation_at ,g.dateUpdate,g.observacion,ti.idtrama,IFNULL(g.etiqueta,0) as etiqueta,IFNULL(g.visita_presencial,0) as visita_presencial
          FROM gestionppk g 
          INNER JOIN tramaIntegrancion ti on ti.policy = g.policy 
          LEFT JOIN operaciones o on o.idoperacion =ti.idoperacion 
          WHERE 1=1 and g.idgesppk='$id'
              order by id desc 
            ";

        $rslt = self::sentencia($sql);
        return !empty($rslt)? array_shift($rslt):false;
    }



    public function findSeguimientoByPolicy($id) {

        $sql=" SELECT id,body,class_id,idusuario,fecha,created ,creation_at ,last_modify ,updated_at from seguimientos_detalle 
WHERE class_id in (SELECT g.idgesppk FROM gestionppk g where g.policy ='$id')
AND class = 'GESTIONPPK' order by id asc
            ";

        $obj = self::sentencia($sql);
        return !empty($obj)?$obj:[];
    }

    public function asignarCargo(){
        global $db;
        global $session;
        $sql="UPDATE gestionppk SET ";
        $sql.=" idsuariocargo='";
        $sql.=$db->escapeString($session->idusuario)."', ";
        $sql.=" dateCargo='".date("Y-m-d H:i:s")."'";;
        $sql.=" WHERE idgesppk=";
        $sql.=$db->escapeString($this->idgesppk);
        return ($db->query($sql)) ? true:false;

    }


    public function updateSeguimientoEstado(){
        global $db;
        global $session;
        $this->fechaActivacion=date("Y-m-d H:i:s");
        $sql="UPDATE gestionppk SET ";
        $sql.=" estado='";
        $sql.=$db->escapeString($this->estado)."' , ";
        $sql.=" visita_presencial='";
        $sql.=$db->escapeString($this->visita_presencial)."', ";
        $sql.=" etiqueta='";
        $sql.=$db->escapeString($this->etiqueta)."' ";
        $sql.=" WHERE idgesppk=";
        $sql.=$db->escapeString($this->idgesppk);
        if ($db->query($sql)) {

            return True;
        }
        else{
            return False;
        }
    }

    public function saveSeg($seg){
        global $session;
        global $db;
        $sql="INSERT INTO seguimientos_detalle (body,class,class_id,idusuario) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString($seg['body'])."' , '";
        $sql.= $db->escapeString('GESTIONPPK')."' , '";
        $sql.= $db->escapeString($seg['idsegpk'])."' , '";
        $sql.= $db->escapeString($session->idusuario)."' )";
        if ($db->query($sql)) {
            return True;
        }
        else{
            return False;
        }
    }


    public function obtenerListEtiqueta($idest) {
        $fil="" ;
        if($idest!=""){
            $fil=" and  dat.id='$idest'";
        }
        $sql="SELECT * FROM (SELECT 0 as id, '-- Seleccionar --' as descripcion, '' as color 
               UNION ALL
               select et.id,et.descripcion,et.color from etiquetas_gestionpk et)dat where 1=1 $fil";
        $rslt = self::sentencia($sql);
        return $rslt;
    }



    public function obtenerListEtiquetaSegPK($idest) {
        $fil="" ;
        if($idest!=""){
            $fil=" and  ea.id='$idest'";
        }
        $sql="select ea.id,ea.descripcion,ea.color from etiquetas_gestionpk ea where 1=1 $fil";
        $rslt = self::sentencia($sql);
        return $rslt;
    }



    public function saveEtiqueta($seg){
        global $session;
        global $db;

        $this->crea_at = date("Y-m-d H:i:s");

        $sql="INSERT INTO etiquetas_gestionpk (descripcion,color,created_at,idusuario) ";
        $sql.=" VALUES ('";
        $sql.= $db->escapeString($seg['body'])."' , '";
        $sql.= $db->escapeString('primary')."' , '";
        $sql.= $this->crea_at."' , '";
        $sql.= $db->escapeString($session->idusuario)."' )";
        if ($db->query($sql)) {
            return True;
        }
        else{
            return False;
        }
    }


    public function updateEtiqueta()
    {
        global $db;
        global $session;
        $this->update_at = date("Y-m-d H:i:s");
        $sql = " UPDATE etiquetas_gestionpk et SET ";
        $sql .= " et.color ='". $db->escapeString($this->color) ."',";
        $sql .= " et.update_at ='". $this->update_at ."' ,";
        $sql .= " et.update_user ='" . $db->escapeString($session->idusuario) . "' ";
        $sql .= " WHERE et.id ='". $db->escapeString($this->id)."'";

        if ($db->query($sql)) {
            return true;
        } else {
            echo "the fail";
        }
    }

}
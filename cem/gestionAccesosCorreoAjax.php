<?php
//gestionDeAccesos
require_once("../includes/init.php");
require __DIR__ . "/../vendor/autoload.php";
require_once("../com/templateMail.php");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
// 860ryp0nz-EZ
$on = new Onix();

$chasis = $_POST['txtchasis'];
$usu = $_POST['txtusu'];
$contra = $_POST['txtcontra'];

$correoCliente = $on->obtenerCorreoPorChasis($chasis);

if (!$correoCliente) {
    $arrResponse = array('status' => false, 'response' => 'EL CHASIS INGRESADO NO SE ENCUENTRA REGISTRADO O NO ES EL CORRECTO');
    echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
    die();
}

$correousu = $correoCliente["mail"];

if (empty($correousu)) {
    $arrResponse = array('status' => false, 'response' => 'El cliente no cuenta con correo');
    echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
    die();
}

$nombreCliente = $correoCliente["cliente"];

$datotabla = new DatosTablasClass();
$infoAreas = $datotabla->correoDeAreaPorPermisoAlModulo($session->idusuario);
//var_dump($infoAreas[0]->arearesponsable);
$corredearea = $infoAreas[0]->correoarea;
$arearesponsable = $infoAreas[0]->arearesponsable;
$nombredelcargo = $infoAreas[0]->nombredelcargo;

if ($chasis == "" || $contra == "" || $usu == "") {

    $arrResponse = array('status' => false, 'response' => 'Debe completar todos los campos obligatorios');
    echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
    die();
}

$asunto = "";
$bodyEmail = '';

$mail = new PHPMailer(true);
try {
    $mail->CharSet = 'UTF-8';
    $mail->isSMTP();
    $mail->Host = MAIL_HOST;
    $mail->SMTPAuth = true;
    $mail->Username = MAIL_USERNAME;
    $mail->Password = MAIL_PASSWORD;
    $mail->SMTPSecure = MAIL_SECURE;
    $mail->Port = MAIL_PORT;

    $mail->addEmbeddedImage('../com/logotrackfinal.png', 'logotrackfinal');
    $mail->addEmbeddedImage('../com/logoapple.png', 'logoapple');
    $mail->addEmbeddedImage('../com/logoplaystore.png', 'logoplaystore');
    $mail->addEmbeddedImage('../com/logoyoutube.png', 'logoyoutube');

    //$mail->setFrom(MAIL_USERNAME, 'CENTRAL DE EMERGENCIAS - TRACKLINK');

    if($nombredelcargo){
        $mail->setFrom($corredearea, $nombredelcargo);
    }
    $mail->addAddress($correousu, $nombreCliente);
    //$mail->addAddress('ezarate@tracklink.pe');

    // if($correoCopiaAdicional=''){
    //     $mail->addCC('');
    // } else{
    //     $mail->addCC($correoCopiaAdicional);
    // }

    // if($correoCopiaAdicional){
    //     //$mail->addCC($correoCopiaAdicional);
    //     $mail->addBCC($correoCopiaAdicional);
    // }

    //Copia invisible

    $mail->addBCC('kvega@tracklink.pe');
    $mail->addBCC('postventa@tracklink.pe');
    $mail->addBCC('centralemergencia@tracklink.pe');
    //$mail->addBCC($corredearea);

    // if($correoInvisible){
    //     $mail->addBCC($correoInvisible);  //860rynf48-EZ
    // }
    //$mail->addCC('kevinwillianvm@gmail.com');
    //$mail->addCC('echahua@tracklink.pe');
    //$mail->addBCC('edwardzte98@gmail.com');
    $mail->isHTML(true);

    $asunto = 'Credenciales de Acceso';
    $bodyEmail = gestionAccesosCredenciales($nombreCliente, $usu, $contra);
    //$mail->addAttachment('MANUAL_INTERACTIVO (17).pdf', 'MANUAL INTERACTIVO.pdf');
    $mail->Subject = $asunto;
    $mail->Body = '' . $bodyEmail;

    $mail->send();

    $arrResponse = array('status' => true, 'response' => 'Se envió el correo al cliente.');
} catch (Exception $e) {
    $arrResponse = array('status' => false, 'response' => 'Hubo un error:' . $mail->ErrorInfo);
}

echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
die();
//echo json_encode($correoCliente, JSON_INVALID_UTF8_IGNORE);
?>
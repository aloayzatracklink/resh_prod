<?php $nav="incautacion"; ?>
<?php include("../includes/head.php") ?>
<?php 
$errores=[];
if (!$session->yaEstaIngresado()) { irA("../ingresar"); } 
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->choques=="si"): ?>
<?php
$choqueObj=new Choque();
if (isset($_POST['inicio'])) {
  $errores=array();
  if (empty($_POST['chasis']) OR !isset($_POST['chasis'])) {
    echo $errores[]="Error registre correctamente el chasis";
  }
    $chasis=$_POST['chasis'];
    if ($choqueObj) {
      $clienteObj=Onix::ObtenerDatosDesdeChasis($chasis);
      if ($clienteObj) {
        $choqueObj->placaChoque=$clienteObj->placa;
        $choqueObj->marca=$clienteObj->marca;
        $choqueObj->modelo=$clienteObj->modelo;
        $apellidos=utf8_encode($clienteObj->apellidos);
        $propietario = (!empty($clienteObj->nombre)) ? utf8_encode($clienteObj->nombre)." ".$apellidos : $apellidos ;
        $choqueObj->propietario=$propietario;
        $choqueObj->idcliente=$clienteObj->dni;
        $correosCliente=$clienteObj->correo;
        $choqueObj->vence=$clienteObj->hasta;
	$choqueObj->chasis=$chasis;
if ($choqueObj->registrarIncautacion()){  
    $url="int/seguimientoIncautacion.php?id=$choqueObj->idincautacion";
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>'; exit;
}
      }else{
        $errores[]="Chasis no existe en la base de datos de Onyx, verifica e intente nuevamente.";
      }

}
}

if (isset($_POST['registrarEiniciarSeguimiento'])) {
  $errores=array();
    if ($choqueObj) {
        $choqueObj->placaChoque=$_POST['placa'];
        $choqueObj->marca=$_POST['marca'];
        $choqueObj->modelo=$_POST['modelo'];
        $propietario = $_POST['propietario'];
        $choqueObj->propietario=$propietario;
        $choqueObj->idcliente=$_POST['dni'];
if ($choqueObj->registrarIncautacion()){  
    $url="int/seguimientoIncautacion.php?id=$choqueObj->idincautacion";
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>'; exit;
}
}
}
?>
<body>

  <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
      <div class="sidebar-collapse">
        <?php include("../includes/menuIzquierdo.php") ?>
      </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
      <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
          <?php include("../includes/navegacionArriba.php") ?>
        </nav>
      </div>
      <div class="wrapper wrapper-content animated fadeInRight">
       <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>Nueva incautacion</h5>
              <div class="ibox-tools">
                <a class="collapse-link">
                  <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <i class="fa fa-wrench"></i>
                </a>
                <a class="close-link">
                  <i class="fa fa-times"></i>
                </a>
              </div>
            </div>
            <div class="ibox-content">
              <form role="form" class="" method="post" >
                <div class="row">
                  <div class="form-group col-md-4">
                    <label for="chasis" class="sr-only">Chasis del vehículo</label>
                    <input type="text" placeholder="Chasis del vehículo" id="chasis" name="chasis" class="form-control text-uppercase" autofocus required >
                  </div> 
                  <div class="col-md-2">
                    <button class="btn btn-success" type="submit" name="inicio">Iniciar seguimiento</button>
                  </div>
		<div class="col-md-4">
                    <button class="btn btn-info" data-toggle="modal" data-target="#bateriasG" type="button" name="inicio">Agregar e iniciar seguimiento</button>
                  </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Ultimas incautaciones</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="fa fa-wrench"></i>
            </a>
            <a class="close-link">
              <i class="fa fa-times"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content">
          <input type="text" class="form-control input-sm m-b-xs" id="filter"
          placeholder="Buscar">

          <table class="table table-hover footable " data-page-size="8" data-filter=#filter>
            <thead>
              <tr>
                <th>#</th>
                <th>Placa</th>
                <th>Propietario</th>
                <th>Operador</th>
                <th>Estado</th>
                <th>Fecha</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody>
              <?php $choqueData=$choqueObj->obtenerIncautaciones() ?>
              <?php $n=1 ?>
              <?php if ($choqueData): ?>
                <?php foreach ($choqueData as $var): ?>
                    <tr>
                      <td><?php echo $n++ ?></td>
                      <td><?php echo  $var->placaAsistencia ?></td>
                      <td><?php echo $var->propietario ?></td>
                        <td><?php $usuario=Usuario::obtenerUsuario($var->idusuario);  echo $usuario->usuarioNombre;?>&nbsp;<?php echo $usuario->usuarioApellidoPaterno;  ?>&nbsp;</td>
                      </td>
                      <td><?php echo obtenerEstado($var->estado) ?></td>
                      <td><?php echo haceMinimo($var->fechaRegistro) ?></td>
                      <td>
                  <td class="center"><a href="int/detalleIncautacion.php?id=<?php echo $var->idincautacion  ?>" class="btn btn-success btn-xs">Ver</a>&nbsp;<a href="int/seguimientoIncautacion.php?id=<?php echo $var->idincautacion   ?>" class="btn btn-info btn-xs">Seguir</a></td>
                    </tr>
                <?php endforeach ?>
              <?php endif ?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="5">
                  <ul class="pagination pull-right"></ul>
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>







</div>
<?php include("../includes/footer.php") ?>

</div>
</div>

<div class="small-chat-box fadeInRight animated">

  <div class="heading" draggable="true">
    <small class="chat-date pull-right">
      <?php echo $horaActual=date("h:i"); ?>
    </small>
    chat
  </div>

  <div class="content">

  </div>

  <div class="form-chat">
    <div class="input-group input-group-sm">
      <textarea class="hablar form-control" placeholder="Shift+Enter para saltar espacio"></textarea>
      <span class="input-group-btn"><button
        class="btn btn-primary">Enviar
      </button> </span>
    </div>
  </div>
</form>
</div>
<div id="small-chat">
  <span class="badge badge-warning pull-right">5</span>
  <a class="open-small-chat">
    <i class="fa fa-comments"></i>
  </a>    
</div>

<div class="modal inmodal" id="bateriasG" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Agregar datos del vehículo</h4>
		<small class="font-bold">
		<div class="form-group ">
		</div>
		</small>
            </div>
            <form  role="form" method="post" >
                <div class="modal-body" style="background: #fff;padding-top: 0">
			<div class="row">
				<div class="col-md-4">
					<label>Placa </label>
					<input class="form-control" name="placa" required>
				</div>
				<div class="col-md-4">
					<label>Marca</label>
					<input class="form-control" name="marca" required>
				</div>
				<div class="col-md-4">
					<label>Modelo</label>
					<input class="form-control" name="modelo" required>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<label>Dni</label>
					<input class="form-control" name="dni" >
				</div>
				<div class="col-md-8">
					<label>Propietario</label>
					<input class="form-control" name="propietario" required>
				</div>
			</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="submit" name="registrarEiniciarSeguimiento" class="btn btn-primary">Iniciar Seguimiento</button>
                </div>
            </form>   
        </div>
    </div>
</div>


<!-- Mainly scripts -->
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<!-- chat -->
<script src="../assets/js/chat.js"></script>
<!-- Toastr -->
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
 <?php include_once('../includes/script.php') ?>
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
</script>
<script>
  $(document).ready(function(){       
    $('.footable').footable();
    // $('.footable2').footable();

  });
</script>

</body>

</html>
<?php else: ?> 
  <?php $session->salir();
  echo "NO NO NO NO ; presiona F5"
  ?> 
<?php endif ?>

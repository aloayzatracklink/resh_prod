<?php $nav="detalleCliente" ?>
<?php include("../../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("../../ingresar");
} ?>
<?php 
$id=$_GET['id'];
$roboDetalleObj=Robo::obtenerRobo($id);
 ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?> 
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <?php include("../../includes/menuIzquierdo.php") ?> 
            </div>
        </nav>
  <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../../includes/navegacionArriba.php") ?>

        </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2><?php echo $roboDetalleObj->placaRobo ?> de cliente  <?php echo $roboDetalleObj->propietario ?></h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="../robo.php">Robos</a>
                    </li>
                    
                    <li class="active">
                        <strong>Detalle robo</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>
        <div class="wrapper wrapper-content">
            <div class="row animated fadeInRight">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="text-center float-e-margins p-md">
                    
                    <a href="#" class="btn btn-xs btn-primary" id="lightVersion">Version Ligera</a>
                    <a href="#" class="btn btn-xs btn-primary" id="darkVersion">Version Clara</a>
                    <a href="#" class="btn btn-xs btn-primary" id="leftVersion">Todo a la Izquierda</a>
                    <a href="../phpWord/testword?id=<?php echo $id ?>" class="btn btn-xs btn-info">Obtener Reporte en word</a>
                    </div>
                    <div class="ibox-content" id="ibox-content">

                        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
                             <div class="vertical-timeline-block">
                                <div class="vertical-timeline-icon red-bg">
                                    <i class="fa fa-exclamation-circle"></i>
                                </div>
                                <div class="vertical-timeline-content">
                                    <p>Central de emergencia recibe llamada de <strong><?php echo $roboDetalleObj->nombreInformante ?></strong>  con DNI Numero <?php echo $roboDetalleObj->dniInformante ?> quien reporta el robo de vehículo con placa: <h2> <?php echo $roboDetalleObj->placaRobo ?></h2> pide que nos contactemos al número <strong><?php echo $roboDetalleObj->numeroContacto ?></strong>
                                    </p><br>
                                    <span class="vertical-date small text-muted"><?php echo formato($roboDetalleObj->fechaRegistro) ?> </span>
                                    <small> <br>
                                    <strong>Operador a cargo</strong> -- <cite title="" data-original-title=""><?php $usuarioAsesor= Usuario::obtenerUsuario($roboDetalleObj->idusuario); echo $usuarioAsesor->usuarioNombre;echo "&nbsp;"; echo $usuarioAsesor->usuarioApellidoPaterno  ?></cite></small>             
                                </div>
                            </div>
                           <?php SeguimientoRobo::obtenerSeguimientoRobo($roboDetalleObj->idrobo) ?>                           
                       
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
      <?php include("../../includes/footer.php") ?>

        </div>
        </div>
    <!-- Mainly scripts -->
    <script src="../../assets/js/jquery-2.1.1.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Peity -->
    <script src="../../assets/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="../../assets/js/inspinia.js"></script>
    <script src="../../assets/js/plugins/pace/pace.min.js"></script>

    <!-- Peity -->
    <script src="../../assets/js/demo/peity-demo.js"></script>

<?php include_once('../../includes/script.php') ?>
    <script>
        $(document).ready(function(){

            // Local script for demo purpose only
            $('#lightVersion').click(function(event) {
                event.preventDefault()
                $('#ibox-content').removeClass('ibox-content');
                $('#vertical-timeline').removeClass('dark-timeline');
                $('#vertical-timeline').addClass('light-timeline');
            });

            $('#darkVersion').click(function(event) {
                event.preventDefault()
                $('#ibox-content').addClass('ibox-content');
                $('#vertical-timeline').removeClass('light-timeline');
                $('#vertical-timeline').addClass('dark-timeline');
            });

            $('#leftVersion').click(function(event) {
                event.preventDefault()
                $('#vertical-timeline').toggleClass('center-orientation');
            });


        });
    </script>

</body>

</html>

<?php $nav="Contratos vencidos" ;?>
<?php include("../../includes/head.php") ?>
<link rel="stylesheet" type="text/css" href="../../assets/calendario/dist/sweetalert.css">
<?php if (!$session->yaEstaIngresado()){ 
  irA("../ingresar");
} ?>
<?php 
$obMenu=Usuario::obtenerMenu($session->idusuario) ?>

<?php if ($obMenu->baterias=="si"): ?> 

<?php 
if (isset($_POST['marcarComoGestionado'])) {}
// if (isset($_POST['guardarComoPendiente'])) {}
    ?>
   <link href="../../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
   <link href="../../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
   <link href="../../assets/css/sweetalert2.css" rel="stylesheet">
   <link href="../../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
   <style>
    .widget p {
        display: inline-block;        
    }

    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }

    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }
    .reloj .caja-segundos {
        display: inline-block;
    }
    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../../assets/img/Preloader_6.gif) center no-repeat #fff;
}
.pagination1{margin:0;padding:0;}
.pagination1 li{
    display: inline;
    padding: 6px 10px 6px 10px;
    border: 1px solid #ddd;
    margin-right: -1px;
    font: 15px/20px Arial, Helvetica, sans-serif;
    background: #FFFFFF;
    box-shadow: inset 1px 1px 5px #F4F4F4;
}
.pagination1 li a{
    text-decoration:none;
    color: rgb(89, 141, 235);
}
.pagination1 li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination1 li.last {
    border-radius: 0px 5px 5px 0px;
}
.pagination1 li:hover{
    background: #CFF;
}
.pagination1 li.active{
    background: #F0F0F0;
    color: #333;
}
</style>
 <body>
<div class="se-pre-con"></div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <?php include("../../includes/menuIzquierdo.php") ?>
               </div>
           </nav>
           <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../../includes/navegacionArriba.php") ?>
                </nav>
            </div>
            <div class="wrapper wrapper-content">
                <div class="row ibox float-e-margins ibox-content ">
                    <div class="col-md-12">
                        <div class="row">
                        <form action="PHPExel/convenioExel" method="POST">
                        <div class="col-md-2">
                           <label for="convenio" class="">Seleccione estado *</label>
                             <select style="border: none;"  name="convenio" class="cd-selec form-control border-bottom " required id="convenio">                            
                            <option value="des">Desinstalado con orden de baja TRACK</option>
                            <option value="desO">Desinstalado con orden de baja OCTO</option>
                            <option value="desS" selected>Contrato vencido sin orden TRACK</option>
                            <option value="desSO">Contrato vencido sin orden OCTO</option>
                            <!-- <option value="15">Instalado monitoreo</option> -->
                            </select> 
                        </div> 
                        <div class="col-lg-3">
                           <label for="rango" class="">Seleccione rango fecha *</label>
                            <input style="border: none;"  class="cd-select form-control border-bottom" required  type="text" placeholder="Seleccione Rango de fecha" class="form-control" value="" name="fecha" id="rango" autocomplete="off">
                        </div>
                        <div class="col-md-2" id="resultado1">                        
                           <a href="../bateriaTrack" class="btn btn-info">Regresar a Baterías Track</a>
                        </div>    
                        </form>
                        <div class="col-md-offset-1 col-md-4 ">
                           <div class="widget" style="line-height: 0px;       
                           padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px">
                           <div class="fecha">
                            <p id="diaSemana" class="diaSemana">Martes</p>
                            <p id="dia" class="dia">27</p>
                            <p>de </p>
                            <p id="mes" class="mes">Octubre</p>
                            <p>del </p>
                            <p id="year" class="year">2015</p>
                        </div>
                        <div class="reloj">
                            <p id="horas" class="horas">11</p>
                            <p>:</p>
                            <p id="minutos" class="minutos">48</p>
                            <p>:</p>
                            <div class="caja-segundos">
                                <p id="ampm" class="ampm">AM</p>
                                <p id="segundos" class="segundos">12</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox-content" id="resultados">
        <input type="text" class="form-control input-sm m-b-xs" id="filter"
        placeholder="Buscar" autofocus="">
        <div class="table-responsive">
        <table class="table table-condensed  table-hover" >
            <thead>
                <tr>   
                    <th>Agenda</th>
                    <th>Fecha registro</th>
                    <th>Registrardor</th>

                    <!-- <th>Ejecutivo Sac</th>
                    <th>Fecha registro</th>
                    <th>Acción</th> -->
                </tr>
            </thead>  
            <tbody  id="resultado">          
            </tbody>  
        </table>
        </div>
    </div>
</div>
<?php include("../../includes/footer.php") ?>
</div>
</div>
<div class="modal inmodal" id="bateriasOModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Gestión instalación</h4> 
                <small class="font-bold"><div class="form-group ">
                </div></small>
            </div>
            <form  role="form" method="post" >
                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal" onclick="reprogramar()">Reprogramar</button>
                    <button type="submit" name="guardarComoPendiente" class="btn btn-info">Guardar como pendiente</button>
                    <button type="submit" name="instalar" class="btn btn-primary">Instalado</button>
                </div>
            </form>   
        </div>
    </div>
</div>
</body>
<!-- LOS MODALES AQUI  -->
<!-- <button class='btn' data-clipboard-action='copy' data-clipboard-target='p'>Copy</button> -->
<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/js/bootstrap.min.js"></script>
<script src="../../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../../assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../../assets/js/inspinia.js"></script>
<script src="../../assets/js/plugins/pace/pace.min.js"></script>
<script src="../../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../../assets/js/clipboard.min.js"></script>
<script src="../../assets/calendario/dist/sweetalert.min.js"></script>

    <!-- Toastr -->
<script src="../../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../../assets/js/plugins/fullcalendar/moment.min.js"></script>
<script src="../../assets/js/plugins/daterangepicker/daterangepicker.js"></script>

<?php include_once('../../includes/script.php') ?> 
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
</script>
<script>
        
        $('#rango').daterangepicker({
                format: 'DD-MM-YYYY',
                // startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                // minDate: '01/01/2012',
                // maxDate: '12/31/2015',
                // dateLimit: { days: 60 },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: false,
                ranges: {
                    'Hoy': [moment(), moment()],
                    'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
                    'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                    'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                    'Anterior Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: '/',
                locale: {
                    applyLabel: 'Seleccionar',
                    cancelLabel: 'Cancelar',
                    fromLabel: 'Desde',
                    toLabel: 'Hasta',
                    customRangeLabel: 'Seleccionar Rango',
                    daysOfWeek: ['Do', 'Lun', 'Mar', 'Mi', 'Ju', 'Vi','Sa'],
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    firstDay: 1
                }
            }, function(start, end, label) {               
                 $('#rango').focus();
            });
</script>
   
<script>

$(document).ready(function(){
    var convenio=$("#convenio").val();
    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionReporte',
        data : {convenio:convenio},
        success : function(data)
        {
         $("#resultado").html(data);
        }
      });

    $( "#convenio" ).change(function() { 
        var convenio = $(this).val(); 
        var fecha=$("#rango").val();      
        var page = $('#resultado').attr("data-page"); 
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionReporte',
        data : {convenio:convenio,page:page,fecha:fecha},
        success : function(data)
        {
         $("#resultado").html(data);
        }
      });
    });
     $("#rango").bind("keyup focus",function()
    {
        var fecha=$(this).val();
        var page = $("#resultado").attr("data-page"); 
        var convenio=$("#convenio").val();        
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionReporte',
        data : {convenio:convenio,page:page,fecha:fecha},
        success : function(data)
        {
         $("#resultado").html(data);
        }
        });
    });     
    $("#resultado").on( "click", ".pagination1 a", function (e){
        e.preventDefault();
        var fecha=$("#rango").val();
        var convenio=$("#convenio").val();        
        var page = $(this).attr("data-page"); 
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionReporte',
        data : {convenio:convenio,page:page,fecha:fecha},
        success : function(data)
        {
         $("#resultado").html(data);
        }
        });

    });

$("#filter").bind("keyup focus",function()
    {   
        var datosS = $(this).val();
        var convenio=$("#convenio").val();        
        var fecha=$("#rango").val();
        datosS = datosS.replace('<>/"', '');
        if(datosS.length >= 1)
        {
            $.ajax({
                type : 'POST',
                url  : 'pag/resultadoBusquedaHistorial',
                data :  {convenio:convenio,fecha:fecha,datos:datosS},
                success : function(data)
                {
                    $("#resultado").html(data);
                }
            });
            return false;
        }else{
            var convenio=$("#convenio").val();        
            var fecha=$("#rango").val();
            var page = $("#resultado").attr("data-page");
            $.ajax({
            type : 'POST',
            url  : 'pag/paginacionReporte',
            data : {convenio:convenio,page:page,fecha:fecha},
            success : function(data)
            {
             $("#resultado").html(data);
            }
            });
    
        }
    });
});
// $('#bateriasOModal').on('show.bs.modal', function (e) {
//     var rowid = $(e.relatedTarget).data('id');
//     $.ajax({
//         type : 'post',
//         url : 'modal/instalarDataModal', //Here you will fetch records 
//         data :  'rowid='+ rowid, //Pass $id
//         success : function(data){
//                 $('.modal-body').html(data);//Show fetched data from database
//         }
//     }); 
// });
 // function reprogramar(){
 //        var a=confirm()
 //    }

// $("#rango").bind("keyup focus",function()
//     {
//         var convenio = $("#convenio").val();
//         var fecha = $(this).val();  
//         $.ajax({
//           type : 'POST',
//           url  : 'ajax/convenioReporte',          
//           data : {fecha:fecha,convenio:convenio},
//           success : function(data)
//           {
//            $("#resultado1").html(data);
//          }
//        });
//         return false;
// });
// $("#convenio").change(function()
//     {
//         var convenio = $(this).val();  
//         var fecha = $("#rango").val();
//         $.ajax({
//           type : 'POST',
//           url  : 'ajax/convenioReporte',          
//           data : {fecha:fecha,convenio:convenio},
//           success : function(data)
//           {
//            $("#resultado1").html(data);
//          }
//        });
//         return false;
// });

//MODAL 
//BATERIAS 

//BATERIAS 

(function(){
    var actualizarHora = function(){
        // Obtenemos la fecha actual, incluyendo las horas, minutos, segundos, dia de la semana, dia del mes, mes y año;
        var fecha = new Date(),
        horas = fecha.getHours(),
        ampm,
        minutos = fecha.getMinutes(),
        segundos = fecha.getSeconds(),
        diaSemana = fecha.getDay(),
        dia = fecha.getDate(),
        mes = fecha.getMonth(),
        year = fecha.getFullYear();

        // Accedemos a los elementos del DOM para agregar mas adelante sus correspondientes valores
        var pHoras = document.getElementById('horas'),
        pAMPM = document.getElementById('ampm'),
        pMinutos = document.getElementById('minutos'),
        pSegundos = document.getElementById('segundos'),
        pDiaSemana = document.getElementById('diaSemana'),
        pDia = document.getElementById('dia'),
        pMes = document.getElementById('mes'),
        pYear = document.getElementById('year');

        
        // Obtenemos el dia se la semana y lo mostramos
        var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        pDiaSemana.textContent = semana[diaSemana];

        // Obtenemos el dia del mes
        pDia.textContent = dia;

        // Obtenemos el Mes y año y lo mostramos
        var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        pMes.textContent = meses[mes];
        pYear.textContent = year;

        // Cambiamos las hora de 24 a 12 horas y establecemos si es AM o PM

        if (horas >= 12) {
            horas = horas - 12;
            ampm = 'PM';
        } else {
            ampm = 'AM';
        }

        // Detectamos cuando sean las 0 AM y transformamos a 12 AM
        if (horas == 0 ){
            horas = 12;
        }

        // Si queremos mostrar un cero antes de las horas ejecutamos este condicional
        // if (horas < 10){horas = '0' + horas;}
        pHoras.textContent = horas;
        pAMPM.textContent = ampm;

        // Minutos y Segundos
        if (minutos < 10){ minutos = "0" + minutos; }
        if (segundos < 10){ segundos = "0" + segundos; }

        pMinutos.textContent = minutos;
        pSegundos.textContent = segundos;
    };

    actualizarHora();
    var intervalo = setInterval(actualizarHora, 1000);
}())
</script>
   <script>
    $(window).load(function() {
        $(".se-pre-con").fadeOut("slow");;
    });
</script>
<?php else: ?> 
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?> 
<?php endif ?>


<?php require_once("../../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  $fecha='';
  echo $estado=limpiar($_POST['convenio']);
  if (isset($_POST["fecha"])) {
    $fecha=strip_tags($_POST['fecha']);
    $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
    $desde=date("YmdHis",strtotime($desde));  
    $hasta=date("Ymd",strtotime($hasta)); 
    $hasta=$hasta."235959";
  }
if ($fecha=='') {  
  $bateriaObj=new BateriaTrack;
  $cantidadO=$bateriaObj->obtenerGestionadosestadoCantidad($estado);   
  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  $datos=$bateriaObj->obtenerGestionadosestadoaa($posisionDePagina,$registrosPorPagina,$estado);    

}else{
   $bateriaObj=new BateriaTrack;
  $cantidadO=$bateriaObj->obtenerGestionadosestadoCantidadF($estado,$desde,$hasta);   
  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);
  $datos=$bateriaObj->obtenerGestionadosestadoaaF($posisionDePagina,$registrosPorPagina,$estado,$desde,$hasta);    

}
?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>  
  <tr>
    <td><?= $var->agenda ?></td>
    <?php $o=Usuario::obtenerUsuario($var->idusuario) ?>
    <td><?= $o->usuarioNombre." ".$o->usuarioApellidoPaterno; ?></td>
    <td><?= haceMinimo($var->fechaRegistro) ?></td>
  </tr>


<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="8">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="78" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>
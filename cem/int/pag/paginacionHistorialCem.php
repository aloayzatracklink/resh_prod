
<?php require_once("../../../includes/init.php") ?>  
<?php 
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

  if(isset($_POST["page"])){
    $numeroDePagina = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($numeroDePagina)){die('Numero de pagina incorrecta !');} //incase of invalid page number
  }else{
    $numeroDePagina = 1; //if there's no page number, set it to 1
  }
  if (isset($_POST["fecha"])) {
    $fecha=strip_tags($_POST['fecha']);
  $desde=trim(current(explode('/', $fecha)));
    $tmp=explode('/', $fecha);
    $hasta=end($tmp);
     $desde=date("Y/m/d 00:00:00",strtotime($desde));    
    $hasta=date("Y/m/d ",strtotime($hasta)); 
    $hasta=$hasta."23:59:59";
  }
  $operacionesObj=new Chequeos();
  if (empty($fecha)){    
    $cantidadO=$operacionesObj->historialCemCantidadTotal();  
  }else{    
    $cantidadO=$operacionesObj->historialSacCantidadTotalRangoFecha($desde,$hasta);    
  }  
  $registrosPorPagina=10;
  $paginasEnTotal=ceil($cantidadO->cantidad/$registrosPorPagina);
  $posisionDePagina=(($numeroDePagina-1)*$registrosPorPagina);

  if (empty($fecha)) {
      $datos=$operacionesObj->obtenerHistorialCem($posisionDePagina,$registrosPorPagina);
  } else {
      $datos=$operacionesObj->obtenerHistorialSacRango($posisionDePagina,$registrosPorPagina,$desde,$hasta);
    // $datos=$datosFacturacion->obtenerDatosParaFacturacionRango($posisionDePagina,$registrosPorPagina,$desde,$hasta);        
  }  

  $n=1;
             ?>      
<?php if ($datos): ?>              
<?php foreach ($datos as $var): ?>    
    <?php $creardor=Usuario::obtenerUsuario($var->idCreador)  ?> 
    <?php $cemusu=Usuario::obtenerUsuario($var->idConfimadorChequeoLinea)  ?> 
    
    <tr>
          <td><?= $n++ ?></td>
          <td  ><?= $var->tipoChequeo ?></td>       
          <td><?= $var->plan ?></td>
          <td><?= $var->nombres ?></td>
          <td><?= $var->placa ?></td>
          <td><?= $var->chasis ?></td>
          <td><?= $creardor->usuarioNombre." ".$creardor->usuarioApellidoPaterno ?></td>
          <td> <?= (empty($var->fechaConfirmacionLinea) ? '': hace($var->fechaConfirmacionLinea) )?></td>
          <td><?= $cemusu->usuarioNombre." ".$cemusu->usuarioApellidoPaterno ?></td>

   <!--  <td><button class="btn btn-primary "  style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#bateriasOModal" data-id="<?php echo $var->idoperacion ?>">Elegir <i class="fa fa-cog"></i></button>&nbsp;</td>  -->
    </tr>
<?php endforeach ?>
<tr>
<td>Mostrando <?= $retVal = ($cantidadO->cantidad<$registrosPorPagina) ? $cantidadO->cantidad : $registrosPorPagina ; ?> de <?= $cantidadO->cantidad  ?></td>
  <td colspan="10">                        
<?php echo paginate_function($registrosPorPagina,$numeroDePagina,$cantidadO->cantidad,$paginasEnTotal) ?>
  </td>
</tr>         
<?php else: ?>
<tr>
  <td colspan="10" class="text-center">No se encontro registro alguno    
  </td>
</tr>   
<?php endif ?>
<?php } ?>
<?php $nav="bateriaOcto" ;?>
<?php include("../../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
	irA("../../ingresar");
} 
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->baterias=="si"): ?>
		<?php 
		$mesEstadistica=date("m");
		if (isset($_POST['mes1'])) {
			$mesEstadistica='01';
		}
		if (isset($_POST['mes2'])) {
			$mesEstadistica='02';
			
		}
		if (isset($_POST['mes3'])) {
			$mesEstadistica='03';
			
		}
		if (isset($_POST['mes4'])) {
			$mesEstadistica='04';
			
		}
		if (isset($_POST['mes5'])) {
			$mesEstadistica='05';
			
		}
		if (isset($_POST['mes6'])) {
			$mesEstadistica='06';
			
		}
		if (isset($_POST['mes7'])) {
			$mesEstadistica='07';
			
		}
		if (isset($_POST['mes8'])) {
			$mesEstadistica='08';
			
		}
		if (isset($_POST['mes9'])) {
			$mesEstadistica='09';
			
		}
		if (isset($_POST['mes10'])) {
			$mesEstadistica='10';
			
		}
		if (isset($_POST['mes11'])) {
			$mesEstadistica='11';
			
		}
		if (isset($_POST['mes12'])) {
			$mesEstadistica='12';
			
		}

	 ?>

<body>
	<div id="wrapper">
		<nav class="navbar-default navbar-static-side" role="navigation">
			<div class="sidebar-collapse">
				<?php include("../../includes/menuIzquierdo.php") ?>
			</div>
		</nav>
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div class="row border-bottom">
				<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
					<?php include("../../includes/navegacionArriba.php") ?>

				</nav>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="wrapper wrapper-content">


						<div class="row">
							<div class="col-lg-12">
								<div class="ibox float-e-margins">
									<div class="ibox-title">
										<label>Baterias del mes - <?= mesDesdeNumero($mesEstadistica) ?>  del <?php echo $anio=date("Y") ?></label>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<a href="../bateriaOcto" class="btn btn-info">Regresar a baterias Octo</a>&nbsp;
									<?php if ($session->idusuario==2 || $session->idusuario==36 || $session->idusuario==92 || $session->idusuario==6  ): ?>
										
									<a href="exelReports/obtenerReporteBateriaOctoExel?m=<?= $mesEstadistica  ?>" class="btn btn-success">Reporte en Exel</a>
									<?php endif ?>
									<form method="post">
										<button class="btn btn-default <?php echo $retVal = ($mesEstadistica==1) ? "active" : "" ; ?>" name="mes1" value="1">Enero</button>
										<button class="btn btn-default <?php echo $retVal = ($mesEstadistica==2) ? "active" : "" ; ?>" name="mes2" value="2">Febrero</button>
										<button class="btn btn-default <?php echo $retVal = ($mesEstadistica==3) ? "active" : "" ; ?>" name="mes3" value="3">Marzo</button>
										<button class="btn btn-default <?php echo $retVal = ($mesEstadistica==4) ? "active" : "" ; ?>" name="mes4" value="4">Abril</button>
										<button class="btn btn-default <?php echo $retVal = ($mesEstadistica==5) ? "active" : "" ; ?>" name="mes5" value="5">Mayo</button>
										<button class="btn btn-default <?php echo $retVal = ($mesEstadistica==6) ? "active" : "" ; ?>" name="mes6" value="6">Junio</button>
										<button class="btn btn-default <?php echo $retVal = ($mesEstadistica==7) ? "active" : "" ; ?>" name="mes7" value="7">Julio</button>
										<button class="btn btn-default <?php echo $retVal = ($mesEstadistica==8) ? "active" : "" ; ?>" name="mes8" value="8">Agosto</button>
										<button class="btn btn-default <?php echo $retVal = ($mesEstadistica==9) ? "active" : "" ; ?>" name="mes9" value="9">Setiembre</button>
										<button class="btn btn-default <?php echo $retVal = ($mesEstadistica==10) ? "active" : "" ; ?>" name="mes10" value="10">Octubre</button>
										<button class="btn btn-default <?php echo $retVal = ($mesEstadistica==11) ? "active" : "" ; ?>" name="mes11" value="11">Nobiembre</button>
										<button class="btn btn-default <?php echo $retVal = ($mesEstadistica==12) ? "active" : "" ; ?>" name="mes12" value="12">Diciembre</button>
										</form>
									</div>
									<div class="ibox-content">
										<div>
											<div id="slineChart" ></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">

							<div class="col-lg-3">
								<div class="ibox float-e-margins">
									<div class="ibox-title">
										<h5>Alertas total diaria de hoy <?php $fecha=date("l") ?>
										<?php if ($fecha=="Monday"): ?>
											Lunes
										<?php endif ?>
										<?php if ($fecha=="Tuesday"): ?>
											Martes
										<?php endif ?>										
										<?php if ($fecha=="Wednesday"): ?>
											Miercoles
										<?php endif ?>
										<?php if ($fecha=="Thursday"): ?>
											Jueves
										<?php endif ?>
										<?php if ($fecha=="Friday"): ?>
											Viernes
										<?php endif ?>
										<?php if ($fecha=="Saturday"): ?>
											Sabado
										<?php endif ?>
										<?php if ($fecha=="Sunday"): ?>
											Domingo
										<?php endif ?>
										</h5>
									</div>
									<div class="ibox-content">
										<div>
											<div id="pie"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-9">
								<div class="ibox float-e-margins">
									<div class="ibox-title">
										<h5>Vista Anual del <?php echo date("Y") ?></h5>

									</div>
									<div class="ibox-content">
										<div>
											<div id="stocked"></div>
										</div>
									</div>
								</div>
							</div>
						</div>


					</div>




				</div>



			</div>

			<?php include("../../includes/footer.php") ?>
		</div>
	</div>

</div>


<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/js/bootstrap.min.js"></script>
<script src="../../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- FooTable -->
<script src="../../assets/js/plugins/footable/footable.all.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../../assets/js/inspinia.js"></script>
<script src="../../assets/js/plugins/pace/pace.min.js"></script>
<script src="../../assets/js/plugins/d3/d3.min.js"></script>
<script src="../../assets/js/plugins/c3/c3.min.js"></script>
 <?php include_once('../../includes/script.php') ?> 
	
<?php 


$estadiditaObj=new Estadistica();
?>

<script>
	$(document).ready(function(){
		c3.generate({
			bindto: '#slineChart',
			data:{
				x: 'x',
				columns: [				  
					<?php $estadiditaObj->obtenerAlertasVistaMensualOcto($mesEstadistica) ?>
				],
				
				type: 'spline'
			},
			 axis: {
        x: {
            type: 'category',
            tick: {
                rotate: 75,
                multiline: false
            },
            height: 130
        }
    }
		});
		c3.generate({
			bindto: '#pie',
			data:{
				columns: [
				<?php $estadiditaObj->obtenerBateriasDiariasOcto() ?>	
				],
				// colors:{
				// 	Ejecutivo1: '#1ab394',
				// 	Ejecutivo2: '#BABABA',
				// 	Ejecutivo3: '#adebBA'
				// },
				type : 'pie'
			}
		});
		c3.generate({
			bindto: '#stocked',
			data:{
				x: 'x',
				columns: [
				['x', 'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Junio','Septiembre','Octubre','Noviembre','Diciembre'],
				['Total Alertas', <?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalOcto('01');echo $ob->total; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalOcto('02');echo $ob->total; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalOcto('03');echo $ob->total; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalOcto('04');echo $ob->total; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalOcto('05');echo $ob->total; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalOcto('06');echo $ob->total; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalOcto('07');echo $ob->total; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalOcto('08');echo $ob->total; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalOcto('09');echo $ob->total; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalOcto('10');echo $ob->total; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalOcto('11');echo $ob->total; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalOcto('12');echo $ob->total; ?>],
				['Atendidas', <?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalAtendidasOcto('01');echo $ob->atendidos; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalAtendidasOcto('02');echo $ob->atendidos; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalAtendidasOcto('03');echo $ob->atendidos; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalAtendidasOcto('04');echo $ob->atendidos; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalAtendidasOcto('05');echo $ob->atendidos; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalAtendidasOcto('06');echo $ob->atendidos; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalAtendidasOcto('07');echo $ob->atendidos; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalAtendidasOcto('08');echo $ob->atendidos; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalAtendidasOcto('09');echo $ob->atendidos; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalAtendidasOcto('10');echo $ob->atendidos; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalAtendidasOcto('11');echo $ob->atendidos; ?>,<?php  $ob=$estadiditaObj->obtenerVistaAnualAlertasTotalAtendidasOcto('12');echo $ob->atendidos; ?>],
				
				
				],
				colors:{
					Vendidos: '#71c173',
					Cancelados: '#e40e0e'
				},
				type: 'bar',
				groups: [
				['Vendidos', 'Cancelados']
				]
			},
			 axis: {
        x: {
            type: 'category',
            tick: {
                rotate: 75,
                multiline: false
            },
            height: 130
        }
    }
		});

	});

</script>
<?php else: ?> 
    <?php $session->salir();
        echo "NO NO NO NO ; presiona F5"
    ?> 
<?php endif ?>
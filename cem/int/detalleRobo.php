<?php $nav="seguimientoRobo" ?>
<?php include("../../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("../../ingresar");
} ?>
<link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
<link rel="stylesheet" href="../../assets/css2/style.css">
<?php if (!$session->yaEstaIngresado()) { irA("ingresar");   }

require_once '../../mailphp/PHPMailer/PHPMailerAutoload.php';

$id=$_GET['id'];
$roboDetalleObj=Robo::obtenerRobo($id);
$clienteDatos=Onix::ObtenerDatosDesdeChasis($roboDetalleObj->chasis);
if (!$clienteDatos) {
    $clienteDatos=Onix::ObtenerDatosDesdeChasis($roboDetalleObj->placaRobo);
}
if (isset($_POST['actualizar'])) {
    $roboDetalleObj->color=$clienteDatos->color;
    $roboDetalleObj->placaRobo=$clienteDatos->placa;
    $roboDetalleObj->anio=$clienteDatos->anio;
        $cliente = (!empty($clienteDatos->apellidos)) ? $clienteDatos->nombre." ".$clienteDatos->apellidos : $clienteDatos->apellidos ; 
    $roboDetalleObj->propietario=$cliente;
    $roboDetalleObj->marca=$clienteDatos->marca;
    $roboDetalleObj->modelo=$clienteDatos->modelo;
    $roboDetalleObj->chasis=$clienteDatos->chasis;
    $roboDetalleObj->contrato=$_POST['contrato'];
    $roboDetalleObj->vence=$_POST['vence'];
    $roboDetalleObj->seguro=$_POST['seguro'];
    $roboDetalleObj->horaRobo=$_POST['horaRobo'];
    $roboDetalleObj->poliza=$_POST['poliza'];
    $roboDetalleObj->siniestro=$_POST['siniestro'];
    $roboDetalleObj->lugarRobo=$_POST['lugarRobo'];
    $roboDetalleObj->modalidad=$_POST['modalidad'];
    $roboDetalleObj->modalidad=$_POST['modalidad'];
    $roboDetalleObj->horaRecupero=$_POST['horaRecupero'];
    $roboDetalleObj->lugarRecupero=$_POST['lugarRecupero'];
    $roboDetalleObj->estado=$_POST['estado'];







    if($roboDetalleObj->actualizarDatosDeRobo()){
        $mensaje="Los datos fueron actualizados correctamente";

        if($_POST['estado']==9 or $_POST['estado']=="9"){

            $thehtml="<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
    <meta name='viewport' content='width=device-width' />
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
    <title>The alerta</title>
</head>

<body style=' margin: 0;padding: 0;font-family:Helvetica, Arial, sans-serif;    box-sizing: border-box;    font-size: 14px; background-color: #f6f6f6; '>

<table style=' background-color: #f6f6f6; width: 100%;'>
    <tr>
        <td></td>
        <td  width='600' style=' display: block !important;    max-width: 600px !important;    margin: 0 auto !important;    /* makes it centered */    clear: both !important;'>
            <div class='content' style='  max-width: 600px;    margin: 0 auto;    display: block;    padding: 20px;'>
                <table class='main' width='100%' cellpadding='0' cellspacing='0' style='background: #fff;    border: 1px solid #e9e9e9;    border-radius: 3px;'>
                    <tr>
                        <td style='font-size: 16px;    color: #fff;    font-weight: 500;    padding: 20px;    text-align: center;    border-radius: 3px 3px 0 0;  background: rgb(80 177 102);'>
         Unidad Recuperada.
                        </td>
                    </tr>
                    <tr>
                        <td style='padding: 10px !important ;padding: 20px; vertical-align: top;'>
                            <table width='100%' cellpadding='0' cellspacing='0'>
                                <tr>
                                    <td  style='padding: 0 0 20px;'>
                                         Placa $clienteDatos->placa <br> Chasis $clienteDatos->chasis. <br> Marca: $clienteDatos->marca<br> Modelo: $clienteDatos->modelo</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
             </div>
        </td>
        <td></td>
    </tr>
</table>
</body>
</html>";
//El correo para el cliente aqui /// <---->
            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->SetLanguage( 'es', 'mailphp/PHPMailer/language/' );
            $mail->SMTPDebug = 0;
            $mail->Debugoutput = 'html';
            $mail->Host = gethostbyname('mail.tracklink.pe');
            $mail->Port = 25;
            $mail->SMTPAuth = false;
            $mail->CharSet = 'UTF-8';
            $mail->setFrom('centralemergencia@tracklink.pe', 'Central De Emergencia Tracklink');
            $mail->addAddress("aquichiz@tracklink.pe"); // aquien se manda el mensaje
            $mail->addCC("ctamayo@tracklink.pe"); // con copia
            $mail->addCC("echahua@tracklink.pe"); // con copia
            $mail->addBCC("pruebas-ti@tracklink.pe");
            $mail->Subject = 'Unidad Recuperada: '.$clienteDatos->placa.' / '.$clienteDatos->chasis." - ".$cliente ; //// ASUNTO
            $mail->msgHTML($thehtml);
//Replace the plain text body with one created manually
            $mail->AltBody = 'Estimado(as) '.$clienteDatos->placa.' fue reportado  como recuperado vea el seguimiento'; ////////ESTE SE EL CONTENIDO//// QUE SE BERA EN PRIERA INSTANACIA

            if (!$mail->send()) {
                echo "Mailer Error: " . $mail->ErrorInfo;
            } else {
            }
        }


    }else{
        $mensaje="";
    }
}
$seguimientoRoboObj=new SeguimientoRobo();
if (isset($_POST['registrarSeguimiento'])) {
    $seguimientoRoboObj->tipoContacto=$_POST['tipoContacto'];
    $seguimientoRoboObj->nombre=$_POST['nombre'];
    $seguimientoRoboObj->detalles=$_POST['detalles'];
    $seguimientoRoboObj->idusuario=$session->idusuario;
    $seguimientoRoboObj->idrobo=$roboDetalleObj->idrobo;
    if ($seguimientoRoboObj->crearSeguimientoRobo()) {
        // echo "fasdfsdf";
    }else{
        echo "Fail detected";
    }
}
if (isset($_POST['registrarApunte'])) {
    $seguimientoRoboObj->notas=$_POST['apuntes'];
    $seguimientoRoboObj->idusuario=$session->idusuario;
    $seguimientoRoboObj->idrobo=$roboDetalleObj->idrobo;
    if ($seguimientoRoboObj->crearNota()) {

    }else{
        echo "Fail detected parte 2";
    }
}
if (isset($_POST['estado'])) {
            // $num=2;
            // $roboObjEstado=new Robo();
            // if( $roboObjEstado->cambiarEstado($num)){

            // }else{
            //     echo "No se realizo nada";
            // }

}

if (isset($_POST['actualizarDatosVehiculoYcliente'])) {
  $chasis=trim(htmlspecialchars($_POST['chasisActulizar']));
  $objRobo=Onix::ObtenerDatosDesdeChasis($chasis);
  if ($objRobo) {
    $roboDetalleObj->placaRobo=$objRobo->placa;
    $roboDetalleObj->chasis=$objRobo->chasis;
    $roboDetalleObj->marca=$objRobo->marca;
    $roboDetalleObj->modelo=$objRobo->modelo;
    $roboDetalleObj->color=$objRobo->color;
    $roboDetalleObj->anio=$objRobo->anio;
    $cliente = (!empty($objRobo->apellidos)) ? $objRobo->nombre." ".$objRobo->apellidos : $objRobo->apellidos ; 
    $roboDetalleObj->propietario=$cliente;
    $roboDetalleObj->dniOrucPropietario=$objRobo->dni;
    $roboDetalleObj->contrato=$objRobo->articulo;
    $roboDetalleObj->vence=$objRobo->hasta;
    if($roboDetalleObj->actualizarDatosDeRobo()){
        $mensaje="Los datos fueron actualizados correctamente";
    }else{
        $mensaje="";
    }
  }else{
    echo "No existe en la base de datos";
  }

  
}
 
?>

<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<body>

    <div id="wrapper">

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
             <?php include("../../includes/menuIzquierdo.php") ?>
         </div>
     </nav>


     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../../includes/navegacionArriba.php") ?>
            </nav>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-sm-6">  
<?php if ($roboDetalleObj->chasis==''): ?>  
<div class="ibox">
<div class="ibox-content">
<div class="row">
<div class="col-md-12">  <form action="" method="post">
<div class="form-group">
<h2 class="col-sm-2 control-label text-danger"><strong>Chasis:</strong></h2>
<div class="col-sm-10"><strong><div class="input-group"><input type="text" class="form-control" name="chasisActulizar"> <span class="input-group-btn"> <button type="submit" class="btn btn-primary" name="actualizarDatosVehiculoYcliente">Registrar
</button> </span></div><span class="help-block m-b-none text-danger">Chasis.</span></strong>
</div>
</div>  </form>
</div>
</div>
</div>
</div>
<?php endif ?>
<div class="ibox">
  <div class="ibox">
          <div class="ibox-content">              
<?php if ($clienteDatos){ ?>
              <?php $apellidos=utf8_encode($clienteDatos->apellidos) ?>
            <h3><strong><?php echo $retVal = (!empty($clienteDatos->nombre)) ? "$clienteDatos->nombre"." "."$apellidos" : "$apellidos" ; ?></strong> <small>Contrato <?php echo vence($clienteDatos->hasta) ?></small></h3>
        
           
        <section class="row">
              <div class="col-md-3"><i class="fa fa-phone-square"></i> <?php echo $clienteDatos->celular ?></div>
              <div class="col-md-3"><i class="fa fa-mobile"></i> <?php echo $clienteDatos->celular2 ?></i></div>
              <div class="col-md-3"><i class="fa fa-phone"></i> <?php echo $clienteDatos->telefonoTrabajo ?></div>
              <div class="col-md-3"><i class="fa fa-phone-square"></i> <?php echo $clienteDatos->telefono ?></div>
        </section>
<br>
<?php } ?>
        <form action="" method="post">
            <div class="row">
               <div class="form-group"><label class="col-sm-2 control-label">Apuntes:</label>
                <div class="col-sm-10"><input autofocus="" type="text" class="form-control input-lg" name="apuntes" required > <span class="help-block m-b-none">Apuntes.</span>
                </div>
            </div> 
            <div class="form-group">
                <div class="col-sm-9"></div>
                <div class="col-sm-2"><button name="registrarApunte" type="submit" class="brn btn-info"> Registrar </button></div>
            </div>
        </div>

    </form>

<?php if ($clienteDatos){ ?>


<div class="row">
  
<h3 class="col-md-6">Datos importantes </h3>
</div>

<div class="row">
<div class="form-group col-md-12" ">
        <label for="Observaciones">Contactos Autorizados   </label>
        <div style="height: 80px;
    overflow-y: auto;">
  <?php 
  $objContacto=Onix::obtenerContactosAutorizadosDeCliente($clienteDatos->idlocation,$clienteDatos->idcustomer);
if($objContacto){
  $n=1;
  ?>
  <table class="table table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>Parentesco</th>
        <th>Nombres</th>
        <th>Celular</th>
        <th>Telefono</th>
        <th>Correo</th>
    </tr>
    </thead>
    <tbody>
  <?php
  foreach ($objContacto as $var4) {
  ?>  
  <tr>
      <td><?php echo $n++ ?> </td>
      <td><?php echo $var4->parentesco ?></td>
      <td><?php  echo $var4->nombreContacto." ".$var4->contactoApellido; ?></td>
      <td > <?php echo $var4->contactoCelular ?> </td>
      <td > <?php echo $var4->contactoTelefono ?> </td>
      <td > <?php echo $var4->correoContacto ?> </td>
  </tr>
  <?php

  }
  ?>
     </tbody>
 </table>
  <?php
  
}else{
  echo "sin contactos Autoriados ";
}
   ?>

        </div>
<br><hr>
      </div>
      </div>
 <div class="row">     
<div class="form-group col-md-6" ">
        <label for="Observaciones">Observaciones Onyx </label>
        <div style="border-right: solid;height: 60px;
    overflow-y: auto;">
        <label class="border"><?php echo utf8_encode($clienteDatos->comentarios) ?></label>
        </div>
      </div>
<div class="col-md-6 form-group" " >
      <label for="">Ordenes de Vehículo</label>
    <div style="height: 60px;
    overflow-y: auto;">
        <?php $objOrdenes=Onix::obtenerOrdenesVehiculo($clienteDatos->idcustomer,$clienteDatos->idvehicle,$clienteDatos->idlocation) ?>
        <?php foreach ($objOrdenes as $var): ?>
          <?php echo utf8_encode($var->articulo) ?><br>
          <?php echo haceMinimoAnio($var->fechaOrden) ?><hr>
        <?php endforeach ?>
        </div>
      </div>

  
</div>
<?php } ?>

</div>
</div>
<hr><hr><br>
</div>
</div>
<div class="col-sm-6">
    <div class="ibox ">

        <div class="ibox-content">
            <div class="tab-content">
                <div id="contact-1" class="tab-pane active">
                    <div class="row m-b-lg">
                        <div class="col-lg-4  text-center">
                            <h2><?php echo $roboDetalleObj->placaRobo; ?></h2>
                            <h2><?php echo $roboDetalleObj->marca ?></h2>
                            <h2><?php echo $roboDetalleObj->modelo ?></h2> 
                                   
                                             </div>
                                             <div class="col-lg-8">                                         
                                                <ul class="list-group clear-list">

                                                    <li class="list-group-item fist-item">
                                                        <span class="pull-right"><?= obtenerEstado($roboDetalleObj->estado) ?></span>
                                                      Estado
                                                  </li>

                                                  <li class="list-group-item">
                                                    <span class="pull-right"><?php echo $roboDetalleObj->color ?></span>
                                                    Color
                                                </li>

                                                <li class="list-group-item">
                                                    <span class="pull-right"> <?php echo $roboDetalleObj->anio ?> </span>
                                                    Año
                                                </li>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal2">
                                                            Actualizar datos <?php $recuperado="1" ?>
                                                        </button>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <form method="post">
                                                            <button type="submit" class="btn btn-<?php if ($roboDetalleObj->estado==0) {
                                                                echo "primary";
                                                            }elseif($roboDetalleObj->estado==1){
                                                                echo "success";
                                                            }else{
                                                                echo "danger";
                                                            } ?>" name="estado" ><?php if ($roboDetalleObj->estado==0) {
                                                                echo "Actualizar";
                                                            }elseif($roboDetalleObj->estado==1){
                                                                echo "Recuperado";
                                                            }else{
                                                                echo "No recuperado";
                                                            }
                                                            ?></button>
                                                        </form>
                                                    </div>
                                                </div>
<!-- ////////////Modal Here -->

<div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
<div class="modal-content">
  <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
      <h4 class="modal-title">Actualizar datos de Robo</h4>
        </div>

  <form action="" method="post">
      <div class="modal-body">
          <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <div class="row">
                          <div class="col-sm-12">
                          <label for="">numero de poliza</label>
                          <input type="text" class="form-control" name="poliza" placeholder="Número de póliza" value="<?php echo $roboDetalleObj->poliza ?>">
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="row">
                          <div class="col-sm-12">
                            <label for="" class="help-block m-b-none text-danger">propietario de vehículo</label>
                          <input type="text" class="form-control" name="propietario" placeholder="Propietario" value="<?php echo $roboDetalleObj->propietario ?>"> 
                          </div>
                      </div>
                  </div>

              <div class="form-group">
                      <div class="row">
                          <div class="col-sm-12">
                            <label for="" class="help-block m-b-none text-danger">Contrato / Sistema</label>
                            <input type="text" class="form-control" name="contrato" placeholder="Contrato" value="<?php echo $roboDetalleObj->contrato ?>"> 
                          </div>
                      </div>
                  </div>

              </div>

              <div class="col-md-6">
              
                
                  <div class="form-group">
                      <div class="row">
                      <label for=""> <span class="help-block m-b-none">Vence contrato</span></label>
                          <div class="col-sm-12"><input id="venceH" type="text" class="form-control" name="vence" placeholder="Vence" value="<?php echo $roboDetalleObj->vence ?>">
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="row">
                          <div class="col-sm-12">
                          <label for=""><span class="help-block m-b-none text-danger">Compañia de seguros</span></label>
                          <input type="text" class="form-control" name="seguro" placeholder="Compañia de seguros" value="<?php echo $roboDetalleObj->seguro ?>" > <strong></strong>
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="row">
                      <label for=""><strong><span class="help-block m-b-none text-danger">Hora y fecha del robo</span></strong></label>
                          <div class="col-sm-12"><input id="horaRoboH" type="text" class="form-control" name="horaRobo" placeholder="Hora y fecha del robo" value="<?php echo $roboDetalleObj->horaRobo ?>"> 
                          </div>
                      </div>
                  </div>
             
              </div>
          </div>
          <div class="row">
          <div class="col-md-12">
            <div class="form-group">
                  <div class="row">
                          <div class="col-sm-12">
                          <label for=""><span class="help-block m-b-none text-danger">Lugar del Robo</span></label>
                          <input id="lugarRobo" type="text" class="form-control" name="lugarRobo" placeholder="Lugar del Robo" value="<?php echo $roboDetalleObj->lugarRobo ?>"> 
                          </div>
                      </div>
                  </div>
          </div>  
          </div>
          <div class="row">
          <div class="col-md-12">
            <div class="form-group">
                      <div class="row">
                          <div class="col-sm-12">
                            <span class="help-block m-b-none text-danger">Moldalidad</span>
                          <input id="modalidad" type="text" class="form-control" name="modalidad" placeholder="Modalidad" value="<?php echo $roboDetalleObj->modalidad ?>"> <strong></strong>
                          </div>
                      </div>
                  </div>
          </div>  
          </div>
          <div class="row">
          <div class="col-md-12">
               <div class="form-group">
                      <div class="row">
                          <div class="col-sm-12">
                          <label for=""><span class="text-danger">Número de siniestro</span></label>
                          <input id="horaRoboH" type="text" class="form-control" name="siniestro" placeholder="Numero de siniestro" value="<?php echo $roboDetalleObj->siniestro ?>"> <strong></strong>
                          </div>
                      </div>
                  </div>
          </div>  
          </div>
          <div class="row">
          <div class="col-md-12">
            <div class="form-group">
                      <div class="row">
                          <div class="col-sm-12">
                          <label for=""> <strong><span class="help-block m-b-none text-danger">Hora de recupero</span></strong></label>
                          <input id="horaRecuperoH"  type="text" class="form-control" name="horaRecupero" placeholder="Hora de recupero" value="<?php echo $retVal = (!empty($roboDetalleObj->horaRecupero)) ? $roboDetalleObj->horaRecupero : "" ; ?>" > 
             
          </div>
                      </div>
                  </div>
          </div>  
          </div>
          <div class="row">
          <div class="col-md-12">
            <div class="form-group">
                      <div class="row">
                          <div class="col-sm-12">
                          <label for=""><strong><span class="help-block m-b-none text-danger">Lugar de recupero</span></strong></label><input id="horaRecuperoH"  type="text" class="form-control" name="lugarRecupero" placeholder="Lugar de recupero" value="<?php echo $retVal = (!empty($roboDetalleObj->lugarRecupero)) ? $roboDetalleObj->lugarRecupero : "" ; ?>" > 
              
          </div>
                      </div>
                  </div>
          </div>  
          </div>

             <div class="row">
          <div class="col-md-12">
          <h2 class="text-center">Estado</h2>

            <div class="col-md-3">
                          <label for="asistiendo">Asistiendo</label>
                          <div class="switch">
                            <div class="onoffswitch">
                              <input type="radio" class="onoffswitch-checkbox" id="asistiendo" value="11" name="estado"  <?php echo $retVal = ($roboDetalleObj->estado==11) ? "checked" : "" ; ?>  >
                              <label class="onoffswitch-label" for="asistiendo">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                            </div>
                          </div>
                        </div>

                  <div class="col-md-3">
                          <label for="asistido">Recuperado</label>
                          <div class="switch">
                            <div class="onoffswitch">
                              <input type="radio" class="onoffswitch-checkbox" id="asistido" value="9" name="estado" <?php echo $retVal = ($roboDetalleObj->estado==9) ? "checked" : "" ; ?> >
                              <label class="onoffswitch-label" for="asistido">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                            </div>
                          </div>
                        </div>

                  <div class="col-md-3">
                          <label for="noReportado">No recuperado </label>
                          <div class="switch">
                            <div class="onoffswitch">
                              <input type="radio" class="onoffswitch-checkbox" id="noReportado" value="10" name="estado" <?php echo $retVal = ($roboDetalleObj->estado==10) ? "checked" : "" ; ?> >
                              <label class="onoffswitch-label" for="noReportado">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                            </div>
                          </div>
                        </div>

                  <div class="col-md-3">
                          <label for="falsoCrash">Falso Robo</label>
                          <div class="switch">
                            <div class="onoffswitch">
                              <input type="radio" class="onoffswitch-checkbox" id="falsoCrash" value="55" name="estado" <?php echo $retVal = ($roboDetalleObj->estado==55) ? "checked" : "" ; ?> >
                              <label class="onoffswitch-label" for="falsoCrash">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                            </div>
                          </div>
                        </div>

            
          </div>  
          </div>
          <br>
          <div class="row">
                <div class="col-md-3 ">
                  <a href="../phpWord/testword?id=<?php echo $id ?>" class="btn btn-primary">Obtener reporte en Word</a>
                </div> 
                <div class="col-md-3">
                  <a href="clienteDetalle?id=<?php echo $id  ?>" class="btn btn-info">Visualizar</a> 
                </div> 
          </div>


      </div>
      <div class="modal-footer">
         <div class="row">
          <div class="col-sm-6">
          <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
          <button type="submit" name="actualizar" class="btn btn-primary">Guardar cambios</button>
      </form>

  </div>
                                                        </div>

    </div>
                                                </div>
                                            </div>
                                        </div>



                                    </ul>
                                </div>
                            </div>
                            <div class="client-detail">
                            <div class="panel panel-default">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class=""><div class="panel-heading">
        <h5 class="panel-title">
            Mas datos
        </h5>
    </div></a>
    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="true">
        <div class="panel-body"><ul class="list-group clear-list">
            <li class="list-group-item fist-item">
                <span class="pull-right"><?php echo $roboDetalleObj->propietario ?> </span>
                Propietario
            </li>
            <li class="list-group-item ">
                <span class="pull-right"> <?php echo $roboDetalleObj->contrato ?></span>
                Contrato
            </li>
            <li class="list-group-item">
                <span class="pull-right"><?php echo formato($roboDetalleObj->vence) ?></span>
                Vence
            </li>
            <li class="list-group-item">
                <span class="pull-right"><?php echo $roboDetalleObj->seguro ?></span>
                Seguros
            </li>
            <li class="list-group-item">
                <span class="pull-right"><?php echo $roboDetalleObj->horaRobo ?> </span>
                Hora de robo
            </li>
            <li class="list-group-item">
                <span class="pull-right"><?php echo $roboDetalleObj->horaRegistro ?> </span>
                Hora del reporte
            </li>
        </ul>
    </div>
</div>
</div>
                               <div class="full-height-scroll">   

                               </div>
                           </div>
                       </div>



                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
<?php include("../../includes/footer.php") ?>
</div>
</div>



<!-- Mainly scripts -->
<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/js/bootstrap.min.js"></script>
<script src="../../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../../assets/js/inspinia.js"></script>
<script src="../../assets/js/plugins/pace/pace.min.js"></script>

<script src="../../assets/build/jquery.datetimepicker.full.js"></script>

<?php include_once('../../includes/script.php') ?> 
<script>
<?php $fechaA=date('Y-m-d H:00:00') ?>
     var eventos={}
    eventos.frejcar =function(){
        $.ajax({
            url: '../ajax/seguimientoRobo',
                type: 'POST', // Send post data
                data: 'idRobo=<?php echo $id ?>',
                async: true,
                success: function(data){
                    $('.client-detail .full-height-scroll').html(data);
                }
                // error: function(e){                     
                //     // revertFunc();
                //     alert('Error processing your request: '+e.responseText);
                // }  
            });
    }
    eventos.interval = setInterval(eventos.frejcar, 4000);
    eventos.frejcar();
      $(document).ready(function(){
            jQuery.datetimepicker.setLocale('es');
            $('#horaRecuperoH').datetimepicker({
                inline:false,
                allowTimes:['00:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:30','11:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','19:00','20:00','21:00','22:00','23:00','24:00'],
                format:'d-m-Y H:i'

                    });
            $('#horaRoboH').datetimepicker({
                inline:false,
                allowTimes:['00:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:30','11:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','19:00','20:00','21:00','22:00','23:00','24:00'],
                format:'d-m-Y H:i'

                    });
            $('#venceH').datetimepicker({
                inline:false,
                timepicker:false,

                    });
  
      });
</script>
</body>
</html>

<?php if (isset($_GET['m'])): ?>
	<?php 
	require_once("../../../includes/init.php");
$nombreArchivo="desconexionDBOCto".date('d-m-Y');
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=$nombreArchivo.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false); 
	 

	 ?>
<html xmlns="http://www.w3.org/1999/xhtml"><!--This file was converted to xhtml by LibreOffice - see http://cgit.freedesktop.org/libreoffice/core/tree/filter/source/xslt for the code.--><head profile="http://dublincore.org/documents/dcmi-terms/"><meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8"/><title xml:lang="en-US">- no title specified</title><meta name="DCTERMS.title" content="" xml:lang="en-US"/><meta name="DCTERMS.language" content="en-US" scheme="DCTERMS.RFC4646"/><meta name="DCTERMS.source" content="http://xml.openoffice.org/odf2xhtml"/><meta name="DCTERMS.creator" content="Daniel Rodríguez"/><meta name="DCTERMS.issued" content="2017-04-04T16:34:52" scheme="DCTERMS.W3CDTF"/><meta name="DCTERMS.modified" content="2017-07-12T17:55:37.267946871" scheme="DCTERMS.W3CDTF"/><meta name="DCTERMS.provenance" content="" xml:lang="en-US"/><meta name="DCTERMS.subject" content="," xml:lang="en-US"/><link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" hreflang="en"/><link rel="schema.DCTERMS" href="http://purl.org/dc/terms/" hreflang="en"/><link rel="schema.DCTYPE" href="http://purl.org/dc/dcmitype/" hreflang="en"/><link rel="schema.DCAM" href="http://purl.org/dc/dcam/" hreflang="en"/>
<style type="text/css">
	@page {  }
	table { border-collapse:collapse; border-spacing:0; empty-cells:show }
	td, th { vertical-align:top; font-size:10pt;}
	h1, h2, h3, h4, h5, h6 { clear:both }
	ol, ul { margin:0; padding:0;}
	li { list-style: none; margin:0; padding:0;}
	<!-- "li span.odfLiEnd" - IE 7 issue-->
	li span. { clear: both; line-height:0; width:0; height:0; margin:0; padding:0; }
	span.footnodeNumber { padding-right:1em; }
	span.annotation_style_by_filter { font-size:95%; font-family:Arial; background-color:#fff000;  margin:0; border:0; padding:0;  }
	* { margin:0;}
	.ta1 { writing-mode:lr-tb; }
	.Default { font-family:Calibri; vertical-align:bottom; color:#000000; font-size:11pt; }
	.ce1 { color:#000000; font-size:11pt; font-family:Calibri; vertical-align:bottom; text-align:left ! important; margin-left:0pt; writing-mode:page; font-style:italic; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce10 { color:#000000; font-size:9pt; font-family:Arial Narrow; vertical-align:middle; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#1f497d; border-left-style:none; border-right-style:none; border-top-width:0.0261cm; border-top-style:solid; border-top-color:#1f497d; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce11 { color:#000000; font-size:5pt; font-family:Calibri; vertical-align:middle; text-align:left ! important; margin-left:0pt; }
	.ce2 { color:#000000; font-size:11pt; font-family:Calibri; vertical-align:middle; }
	.ce3 { color:#000000; font-size:11pt; font-family:Calibri; vertical-align:middle; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:italic; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce4 { color:#000000; font-size:12pt; font-family:Calibri; vertical-align:middle; border-style:none; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce5 { color:#ffffff; font-size:8pt; font-family:Arial Narrow; vertical-align:middle; background-color:#17375e; border-style:none; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce6 { color:#000000; font-size:9pt; font-family:Arial Narrow; vertical-align:middle; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#1f497d; border-left-style:none; border-right-style:none; border-top-width:0.0261cm; border-top-style:solid; border-top-color:#1f497d; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce7 { color:#000000; font-size:8pt; font-family:Calibri; vertical-align:middle; text-align:left ! important; margin-left:0pt; }
	.ce8 { color:#000000; font-size:12pt; font-family:Calibri; vertical-align:bottom; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.ce9 { color:#000000; font-size:10pt; font-family:Arial Narrow; vertical-align:middle; border-bottom-width:0.0261cm; border-bottom-style:solid; border-bottom-color:#1f497d; border-left-style:none; border-right-style:none; border-top-width:0.0261cm; border-top-style:solid; border-top-color:#1f497d; text-align:center ! important; margin-left:0pt; writing-mode:page; font-style:normal; text-shadow:none; text-decoration:none ! important; font-weight:bold; }
	.co1 { width:54pt; }
	.co2 { width:98.79pt; }
	.co3 { width:61.74pt; }
	.co4 { width:213.05pt; }
	.co5 { width:107.55pt; }
	.co6 { width:94.65pt; }
	.co7 { width:73.76pt; }
	.ro1 { height:13.8pt; }
	.ro2 { height:23.24pt; }
	.ro3 { height:8.25pt; }
	.ro4 { height:17.94pt; }
	.ro5 { height:9.95pt; }
	.ro6 { height:19.96pt; }
	.ro7 { height:33.45pt; }
	.ro8 { height:43.94pt; }
	<!-- ODF styles with no properties representable as CSS -->
	 { }
	</style></head><body dir="ltr" style="margin-top:54pt; margin-bottom:54pt; margin-left:50.4pt; margin-right:50.4pt; "><table border="0" cellspacing="0" cellpadding="0" class="ta1"><colgroup><col width="83"/><col width="152"/><col width="95"/><col width="328"/><col width="166"/><col width="146"/><col width="114"/></colgroup><tr class="ro1"><td style="text-align:left;width:54pt; " class="ce1"> </td><td colspan="3" style="text-align:left;width:98.79pt; " class="ce3"><p>
	<?= dia() ?>, <?= date('d') ?> de <?=  mes() ?> de <?= date('Y') ?> – MES <?php 
		if ($_GET['m']) {
			switch ($_GET['m']) {
				case '01':
					echo "ENERO";
					break;
				case '02':
					echo "FEBRERO";
					break;
				case '03':
					echo "MARZO";
					break;
				case '04':
					echo "ABRIL";
					break;
				case '05':
					echo "MAYO";
					break;
				case '06':
					echo "JUNIO";
					break;
				case '07':
					echo "JULIO";
					break;
				case '08':
					echo "AGOSTO";
					break;
				case '09':
					echo "SETIEMBRE";
					break;
				case '10':
					echo "OCTUBRE";
					break;
				case '11':
					echo "NOVIEMBRE";
					break;
				case '12':
					echo "DICIEMBRE";
					break;
						
				default:
					echo "Fail detected";
					break;
			}
		}

	 ?>
	</p></td><td style="text-align:left;width:107.55pt; " class="Default"> </td><td style="text-align:left;width:94.65pt; " class="Default"> </td><td style="text-align:left;width:73.76pt; " class="Default"> </td></tr><tr class="ro2"><td style="text-align:left;width:54pt; " class="Default"> </td><td colspan="5" style="text-align:left;width:98.79pt; " class="ce4"><p>REPORTE DE CENTRAL DE DESCONEXION DE BATERIA OCTO</p></td><td style="text-align:left;width:73.76pt; " class="Default"> </td></tr><tr class="ro3"><td style="text-align:left;width:54pt; " class="Default"> </td><td style="text-align:left;width:98.79pt; " class="Default"> </td><td style="text-align:left;width:61.74pt; " class="Default"> </td><td style="text-align:left;width:213.05pt; " class="Default"> </td><td style="text-align:left;width:107.55pt; " class="Default"> </td><td style="text-align:left;width:94.65pt; " class="Default"> </td><td style="text-align:left;width:73.76pt; " class="Default"> </td></tr><tr class="ro4"><td style="text-align:left;width:54pt; " class="Default"> </td><td colspan="3" style="text-align:left;width:98.79pt; " class="ce5"><p>ACTIVIDADES</p></td><td colspan="2" style="text-align:left;width:107.55pt; " class="ce5"><p>TOTALES</p></td><td style="text-align:left;width:73.76pt; " class="Default"> </td></tr><tr class="ro1"><td style="text-align:left;width:54pt; " class="Default"> </td><td colspan="3" style="text-align:left;width:98.79pt; " class="ce6"><p>DESCONEXION DE BATERIA OCTO TOTAL</p></td><td colspan="2" style="text-align:right; width:107.55pt; " class="ce9"><p><?php $objBateria=BateriaTrack::obtenerCantidadDeBateriasTota(limpiar($_GET['m'])) ?><?= $objBateria->cantidad ?></p></td><td style="text-align:left;width:73.76pt; " class="Default"> </td></tr><tr class="ro1"><td style="text-align:left;width:54pt; " class="Default"> </td><td colspan="3" style="text-align:left;width:98.79pt; " class="ce6"><p>DESCONEXION DE BATERIA ATENDIDAS</p></td><td colspan="2" style="text-align:right; width:107.55pt; " class="ce9"><p><?php $objBateria=BateriaTrack::obtenerCantidadDeBateriasAtendidas(limpiar($_GET['m'])) ?><?= $objBateria->cantidad ?></p></td><td style="text-align:left;width:73.76pt; " class="Default"> </td></tr><tr class="ro1"><td style="text-align:left;width:54pt; " class="Default"> </td><td colspan="3" style="text-align:left;width:98.79pt; " class="ce6"><p>ALERTAS NO BATERY</p></td><td colspan="2" style="text-align:right; width:107.55pt; " class="ce9"><p><?php $objBateria=BateriaTrack::obtenerCantidadDeBateriasNobatery(limpiar($_GET['m'])) ?><?= $objBateria->cantidad ?></p></td><td style="text-align:left;width:73.76pt; " class="Default"> </td></tr><tr class="ro1"><td style="text-align:left;width:54pt; " class="Default"> </td><td colspan="3" style="text-align:left;width:98.79pt; " class="ce6"><p>ALERTAS THEFT</p></td><td colspan="2" style="text-align:right; width:107.55pt; " class="ce9"><p><?php $objBateria=BateriaTrack::obtenerCantidadDeBateriaTHEFT(limpiar($_GET['m'])) ?><?= $objBateria->cantidad ?>  </p></td><td style="text-align:left;width:73.76pt; " class="Default"> </td></tr><tr class="ro1"><td style="text-align:left;width:54pt; " class="Default"> </td><td style="text-align:left;width:98.79pt; " class="Default"> </td><td style="text-align:left;width:61.74pt; " class="Default"> </td><td style="text-align:left;width:213.05pt; " class="Default"> </td><td style="text-align:left;width:107.55pt; " class="Default"> </td><td style="text-align:left;width:94.65pt; " class="Default"> </td><td style="text-align:left;width:73.76pt; " class="Default"> </td></tr><tr class="ro5"><td style="text-align:left;width:54pt; " class="Default"> </td><td style="text-align:left;width:98.79pt; " class="Default"> </td><td style="text-align:left;width:61.74pt; " class="Default"> </td><td style="text-align:left;width:213.05pt; " class="Default"> </td><td style="text-align:left;width:107.55pt; " class="Default"> </td><td style="text-align:left;width:94.65pt; " class="Default"> </td><td style="text-align:left;width:73.76pt; " class="Default"> </td></tr><tr class="ro6"><td style="text-align:left;width:54pt; " class="Default"> </td><td style="text-align:left;width:98.79pt; " class="ce5"><p>CHASIS</p></td><td style="text-align:left;width:61.74pt; " class="ce5"><p>ACCIONES EN RESH</p></td><td style="text-align:left;width:213.05pt; " class="ce5"><p>ACCIONES DE SEGUIMIENTO</p></td><td style="text-align:left;width:107.55pt; " class="ce5"><p>SEGUIMIENTO POR</p></td><td style="text-align:left;width:94.65pt; " class="ce5"><p>FECHA DE REGISTRO Y HORA</p></td><td style="text-align:left;width:73.76pt; " class="Default"> </td></tr>
	<?php $datosBAteria=BateriaTrack::obtenerBateriasAtendidasDatos(limpiar($_GET['m'])) ?>
	<?php $n=1 ?>
	<?php if ($datosBAteria): ?>
	<?php foreach ($datosBAteria as $var): ?>		
	<tr class="ro7"><td style="text-align:left;width:54pt; " class="ce2"> </td><td style="text-align:left;width:98.79pt; " class="ce7"><p><?= $var->chasis ?></p></td><td style="text-align:left;width:61.74pt; " class="ce11"><p><?= "Correo enviado ".$var->cee." - En taller=".$var->ta."- Casilla de voz=".$var->cv."- Actualizar datos".$var->ad."-  contrato vencido=".$var->des."- Desinstalado".$var->desS."- Falsa alerta =".$var->fa; ?> </p></td><td style="text-align:left;width:213.05pt; " class="ce7"><p><?= $var->accionesSegimiento ?></p>
	</td>
	<?php $usuario=Usuario::obtenerUsuario($var->idusuario) ?>
	<td style="text-align:left;width:107.55pt; " class="ce7"><p><?= $usuario->usuarioNombre." ".$usuario->usuarioApellidoPaterno; ?></p></td><td style="text-align:left;width:94.65pt; " class="ce7"><p><?= formato($var->fechaRegistro) ?></p></td><td style="text-align:left;width:73.76pt; " class="ce2"> </td></tr>
	<?php endforeach ?>
<?php else: ?>
	<tr class="ro1"><td style="text-align:left;width:54pt; " class="Default"> </td><td style="text-align:left;width:98.79pt; " class="Default"> </td><td style="text-align:left;width:61.74pt; " class="Default" colspan="4"> No se encotrn registros</td><td style="text-align:left;width:213.05pt; " class="Default"> </td><td style="text-align:left;width:107.55pt; " class="Default"> </td><td style="text-align:left;width:94.65pt; " class="Default"> </td><td style="text-align:left;width:73.76pt; " class="Default"> </td></tr>
<?php endif ?>
	<tr class="ro1"><td style="text-align:left;width:54pt; " class="Default"> </td><td style="text-align:left;width:98.79pt; " class="Default"> </td><td style="text-align:left;width:61.74pt; " class="Default"> </td><td style="text-align:left;width:213.05pt; " class="Default"> </td><td style="text-align:left;width:107.55pt; " class="Default"> </td><td style="text-align:left;width:94.65pt; " class="Default"> </td><td style="text-align:left;width:73.76pt; " class="Default"> </td></tr>

	<tr class="ro1"><td style="text-align:left;width:54pt; " class="Default"> </td><td style="text-align:left;width:98.79pt; " class="Default"> </td><td style="text-align:left;width:61.74pt; " class="Default"> </td><td style="text-align:left;width:213.05pt; " class="Default"> </td><td style="text-align:left;width:107.55pt; " class="Default"> </td><td style="text-align:left;width:94.65pt; " class="Default"> </td><td style="text-align:left;width:73.76pt; " class="Default"> </td></tr></table></body></html>

<?php else: ?>
	No se seleccion mes correctamente.
<?php endif ?>

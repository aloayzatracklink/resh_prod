-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 15-06-2017 a las 22:07:39
-- Versión del servidor: 10.1.22-MariaDB
-- Versión de PHP: 7.0.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `trackdb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operaciones`
--

CREATE TABLE `operaciones` (
  `idoperacion` int(11) NOT NULL,
  `idcliente` varchar(11) NOT NULL,
  `idvehiculo` int(11) NOT NULL,
  `idTecnicoAsignado` int(11) NOT NULL,
  `idChequeo` int(11) NOT NULL,
  `idRenovacion` int(11) NOT NULL,
  `idlocation` int(20) NOT NULL,
  `numeroOrte` varchar(40) NOT NULL,
  `fechaModficacionOrte` varchar(70) NOT NULL,
  `numeroOrte2` varchar(23) NOT NULL,
  `fechaModificacionOrte2` varchar(80) NOT NULL,
  `numeroOrte3` varchar(20) NOT NULL,
  `fechaModificacionOrte3` varchar(80) NOT NULL,
  `datosfacturacionid` varchar(255) NOT NULL,
  `lugarInstalacion` varchar(255) NOT NULL,
  `nombreConcecionarioInstalicion` varchar(255) NOT NULL,
  `direccionInstacion` varchar(400) NOT NULL,
  `coordenadasInstalacion` varchar(255) NOT NULL,
  `fechaInstalacion` varchar(60) NOT NULL,
  `nombreContacto` varchar(255) NOT NULL,
  `numeroContacto` varchar(255) NOT NULL,
  `idTecnicoInstalador` int(11) NOT NULL,
  `idCreador` int(11) NOT NULL,
  `observacion` varchar(255) NOT NULL,
  `imei` varchar(50) NOT NULL,
  `id` varchar(50) NOT NULL,
  `pruebas` varchar(255) NOT NULL,
  `odometro` varchar(60) NOT NULL,
  `ubicacionSistema` varchar(255) NOT NULL,
  `lugarActivacion` varchar(255) NOT NULL,
  `coordenadas` varchar(100) NOT NULL,
  `opciones` varchar(20) NOT NULL,
  `fechaActivacion` varchar(20) NOT NULL,
  `tiempoServicio` varchar(244) NOT NULL,
  `fechaCreacion` varchar(30) NOT NULL,
  `trabajo` varchar(50) NOT NULL DEFAULT 'Instalacion',
  `idusuarioActivador` int(11) NOT NULL,
  `idUsuarioPendiente` int(11) NOT NULL,
  `idUsuarioPlataforma` varchar(255) NOT NULL,
  `idUsuarioCargo` int(11) NOT NULL,
  `fechaPlataformaV` varchar(255) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '14'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `operaciones`
--

INSERT INTO `operaciones` (`idoperacion`, `idcliente`, `idvehiculo`, `idTecnicoAsignado`, `idChequeo`, `idRenovacion`, `idlocation`, `numeroOrte`, `fechaModficacionOrte`, `numeroOrte2`, `fechaModificacionOrte2`, `numeroOrte3`, `fechaModificacionOrte3`, `datosfacturacionid`, `lugarInstalacion`, `nombreConcecionarioInstalicion`, `direccionInstacion`, `coordenadasInstalacion`, `fechaInstalacion`, `nombreContacto`, `numeroContacto`, `idTecnicoInstalador`, `idCreador`, `observacion`, `imei`, `id`, `pruebas`, `odometro`, `ubicacionSistema`, `lugarActivacion`, `coordenadas`, `opciones`, `fechaActivacion`, `tiempoServicio`, `fechaCreacion`, `trabajo`, `idusuarioActivador`, `idUsuarioPendiente`, `idUsuarioPlataforma`, `idUsuarioCargo`, `fechaPlataformaV`, `estado`) VALUES
(1, '1', 1, 10, 0, 0, 0, '234324', '', '', '', '', '', '1', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-06 11:00:00', 'PABEL', '321312312412', 10, 50, 'obs', '324423234', '243242352353', ' Ubicacion ok  Pruebas de bloqueo y desbloqueo OK ', 'odo312312', '-32,231', 'TallerÂ ', '-12321124,1231231', '', '20170505111105', '', '20170505103719', 'Instalacion', 2, 0, '16', 5, '20170505105855', 15),
(2, '1', 2, 11, 0, 0, 0, '43242', '', '', '', '', '', '2', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-06 09:00:00', 'pabel', '123123123123', 11, 2, '234324', '4324234234', '43242343', ' Ubicacion ok ', '23423432', '234324324', 'TallerÂ ', '2342342423', '', '20170511115851', '', '20170505115024', 'Instalacion', 2, 0, '17', 2, '20170505120141', 15),
(3, '2', 3, 10, 0, 0, 0, '342324', '', '', '', '', '', '3', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-06 11:00:00', 'pabel', '31231231212', 0, 3, '', '', '', '', '', '', '', '', '', '', '', '20170505115359', 'Instalacion', 0, 0, '2', 2, '20170510142256', 36),
(4, '4', 4, 27, 0, 0, 0, '343234', '', '', '', '', '', '4', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/05 09:00', 'pabel', '23324324', 0, 3, '', '', '', '', '', '', '', '', '', '', '', '20170505124125', 'Instalacion', 0, 0, '2', 2, '20170510142315', 36),
(5, '4', 5, 33, 0, 0, 0, '32432', '', '', '', '', '', '5', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/05 09:00', 'pabel', '23324324', 0, 3, '', '', '', '', '', '', '', '', '', '', '', '20170505124126', 'Instalacion', 0, 0, '2', 2, '20170510142332', 36),
(6, '5', 6, 9, 0, 0, 0, '22013', '', '', '', '', '', '6', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-10 11:00:00', 'daniela', '312312413213', 0, 50, '', '', '', '', '', '', '', '', '', '', '', '20170508103532', 'Instalacion', 0, 0, '2', 2, '20170524172627', 36),
(7, '6', 7, 9, 0, 0, 0, '32132', '', '', '', '', '', '7', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-10 13:00:00', 'ha ha ahaaaaaaaaa', '3232432132131', 9, 58, '', '324234234', '34234324', ' Ubicacion ok  Pruebas de bloqueo y desbloqueo OK ', '232', '-3214', 'TallerÂ ', '-312412412,12321412', '', '20170608151527', '', '20170508105004', 'Instalacion', 2, 0, '2', 2, '20170524174232', 15),
(8, '8', 8, 9, 0, 0, 0, '3123213', '', '', '', '', '', '8', 'Domicilio Cliente', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/09 13:00', 'sadfsadf', '3224432', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170509111803', 'Instalacion', 0, 0, '2', 2, '20170524174256', 36),
(9, '9', 9, 22, 0, 0, 0, '432432432', '', '', '', '', '', '9', 'Concesionario', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/09 12:00', 'fsadf', '32424', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170509121553', 'Instalacion', 0, 0, '2', 2, '20170524174357', 36),
(10, '10', 10, 12, 0, 0, 0, '432423', '', '', '', '', '', '10', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/10 13:00', 'fadfasdf', '323244234', 12, 2, '123123123', '312313', '12312312', '', '3123213', '3123123', 'TallerÂ 123123', '123123123', '', '', '', '20170509150802', 'Instalacion', 0, 2, '2', 2, '20170524174438', 36),
(11, '10', 11, 13, 0, 0, 0, '13221412', '', '', '', '', '', '11', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/10 13:00', 'fadfasdf', '323244234', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170509150803', 'Instalacion', 0, 0, '2', 2, '20170524174454', 36),
(12, '11', 12, 13, 0, 0, 0, '4123', '', '', '', '', '', '12', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/10 11:00', 'pabel', '32432423523', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170509153423', 'Instalacion', 0, 0, '2', 2, '20170606180302', 36),
(13, '11', 13, 0, 0, 0, 0, '', '', '', '', '', '', '13', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/10 11:00', 'pabel', '32432423523', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170509153424', 'Instalacion', 0, 0, '', 2, '', 14),
(14, '12', 14, 0, 0, 0, 0, '', '', '', '', '', '', '14', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/10 15:00', 'pabel', '23231212', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170509162655', 'Instalacion', 0, 0, '', 2, '', 14),
(15, '13', 15, 0, 0, 0, 0, '', '', '', '', '', '', '15', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/11 13:00', 'pabel', '123124', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170509163754', 'Instalacion', 0, 0, '', 2, '', 14),
(16, '14', 16, 0, 0, 0, 0, '', '', '', '', '', '', '16', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/10 13:00', 'pabel', '132124', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170510102048', 'Instalacion', 0, 0, '', 2, '', 14),
(17, '15', 17, 0, 0, 0, 0, '', '', '', '', '', '', '17', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/10 10:00', 'fasdfasdf', '213123124124', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170510102745', 'Instalacion', 0, 0, '', 2, '', 14),
(18, '15', 18, 0, 0, 0, 0, '', '', '', '', '', '', '18', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/10 10:00', 'fasdfasdf', '213123124124', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170510102746', 'Instalacion', 0, 0, '', 2, '', 14),
(19, '1', 19, 19, 0, 0, 0, '3123', '', '', '', '', '', '19', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-10 15:00:00', 'asdfafas', '324234', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170510114350', 'Instalacion', 0, 0, '2', 2, '20170607101714', 36),
(20, '16', 20, 20, 0, 0, 0, '4324324', '', '', '', '', '', '20', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/11 12:00', 'mosadf', '211213121', 20, 2, '4234234324', '234234', '23423', ' Ubicacion ok  Pruebas de bloqueo y desbloqueo OK ', '23434324', '234234', 'TallerÂ ', '2423423', '', '20170511115927', '', '20170510122541', 'Instalacion', 2, 0, '2', 2, '20170511115646', 15),
(21, '16', 21, 0, 0, 0, 0, '', '', '', '', '', '', '21', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/11 12:00', 'mosadf', '211213121', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170510122542', 'Instalacion', 0, 0, '', 2, '', 14),
(22, '1', 22, 0, 0, 0, 0, '', '', '', '', '', '', '22', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-19 13:00:00', 'fasdfasdf', '1233124', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170510163318', 'Instalacion', 0, 0, '', 0, '', 14),
(23, '17', 23, 0, 0, 0, 0, '', '', '', '', '', '', '23', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/11 11:00', 'dafasdfasd', '21313123', 0, 50, '', '', '', '', '', '', '', '', '', '', '', '20170511091054', 'Instalacion', 0, 0, '', 0, '', 14),
(24, '17', 24, 0, 0, 0, 0, '', '', '', '', '', '', '24', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/11 11:00', 'dafasdfasd', '21313123', 0, 50, '', '', '', '', '', '', '', '', '', '', '', '20170511091055', 'Instalacion', 0, 0, '', 2, '', 14),
(25, '20', 25, 0, 0, 0, 0, '', '', '', '', '', '', '25', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-13 13:00:00', 'fsdafasdf', '4324324', 0, 50, '', '', '', '', '', '', '', '', '', '', '', '20170511093253', 'Instalacion', 0, 0, '', 0, '', 14),
(26, '18', 26, 0, 0, 0, 0, '', '', '', '', '', '', '26', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-12 13:00:00', 'sfadfasdf12312', '233123123', 0, 50, '', '', '', '', '', '', '', '', '', '', '', '20170511095449', 'Instalacion', 0, 0, '', 0, '', 14),
(27, '21', 27, 13, 0, 0, 0, '312312', '', '', '', '', '', '27', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/11 13:00', 'pabel', '1231232421', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170511141722', 'Instalacion', 0, 0, '2', 2, '20170607090956', 36),
(28, '1', 28, 0, 0, 0, 0, '', '', '', '', '', '', '28', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-28 13:00:00', 'sfsfs', '12123113', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170527105442', 'Instalacion', 0, 0, '', 0, '', 14),
(29, '1', 29, 0, 0, 0, 0, '', '', '', '', '', '', '29', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-29 10:55:00', 'fsafasdf', '13123124', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170527105554', 'Instalacion', 0, 0, '', 44, '', 14),
(30, '1', 36, 0, 0, 0, 0, '', '', '', '', '', '', '36', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-30 15:55:00', 'asdfdasfsd', '312312', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170529155550', 'Instalacion', 0, 0, '', 0, '', 14),
(31, '1', 37, 0, 0, 0, 0, '', '', '', '', '', '', '37', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-30 15:56:00', 'asfasdf', '3123123123', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170529155627', 'Instalacion', 0, 0, '', 0, '', 14),
(32, '1', 38, 0, 0, 0, 0, '', '', '', '', '', '', '38', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-30 11:00:00', '234324', '231312', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170529171012', 'Instalacion', 0, 0, '', 0, '', 14),
(33, '1', 39, 0, 0, 0, 0, '', '', '', '', '', '', '39', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-30 17:14:00', 'fsdfasdf', '321312414', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170529171442', 'Instalacion', 0, 0, '', 0, '', 14),
(34, '1', 40, 0, 0, 0, 0, '', '', '', '', '', '', '40', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-30 17:16:00', '2324234', '34324324324', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170529171637', 'Instalacion', 0, 0, '', 0, '', 14),
(35, '1', 41, 0, 0, 0, 0, '', '', '', '', '', '', '41', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-06-01 09:00:00', 'fsadfsdf', '3324324324', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170529174632', 'Instalacion', 0, 0, '', 0, '', 14),
(36, '1', 42, 0, 0, 0, 0, '', '', '', '', '', '', '42', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-06-02 09:00:00', 'fsafasfsdf', '2313213', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170529174806', 'Instalacion', 0, 0, '', 0, '', 14),
(37, '1', 43, 27, 0, 0, 0, '234-2234', '', '', '', '', '', '43', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-05-27 13:00:00', 'fsadfsdfs', '3213123', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170529175448', 'Instalacion', 0, 0, '2', 2, '20170530165525', 36),
(38, '23', 44, 0, 0, 0, 0, '', '', '', '', '', '', '44', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/30 08:00', 'afdsfs', '312313', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170530090203', 'Instalacion', 0, 0, '', 0, '', 14),
(39, '24', 45, 0, 0, 0, 0, '', '', '', '', '', '', '45', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/30 11:00', 'fsadfsdf', '213123', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170530090841', 'Instalacion', 0, 0, '', 0, '', 14),
(40, '25', 46, 0, 0, 0, 0, '', '', '', '', '', '', '46', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/31 11:00', 'dfasfd', '21323123', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170530091709', 'Instalacion', 0, 0, '', 0, '', 14),
(41, '25', 47, 0, 0, 0, 0, '', '', '', '', '', '', '47', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/31 11:00', 'dfasfd', '21323123', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170530091709', 'Instalacion', 0, 0, '', 0, '', 14),
(42, '26', 48, 22, 0, 0, 0, '231223', '', '', '', '', '', '48', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/30 11:00', 'tewtwert', '234234', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170530093711', 'Instalacion', 0, 0, '2', 2, '20170531095838', 36),
(43, '26', 49, 0, 0, 0, 0, '', '', '', '', '', '', '49', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/30 11:00', 'tewtwert', '234234', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170530093712', 'Instalacion', 0, 0, '', 0, '', 14),
(44, '27', 50, 9, 0, 0, 0, '3243', '', '', '', '', '', '50', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/30 09:00', 'fasdfas', '234234', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170530095016', 'Instalacion', 0, 0, '2', 2, '20170530171143', 36),
(45, '28', 51, 0, 0, 0, 0, '', '', '', '', '', '', '51', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/31 13:00', 'pabel', '2313123', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170531114532', 'Instalacion', 0, 0, '', 0, '', 14),
(46, '28', 52, 0, 0, 0, 0, '', '', '', '', '', '', '52', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/31 13:00', 'pabel', '2313123', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170531114533', 'Instalacion', 0, 0, '', 0, '', 14),
(47, '29', 53, 0, 0, 0, 0, '', '', '', '', '', '', '53', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/31 13:00', 'pabel vasquez', '940598303', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170531135925', 'Instalacion', 0, 0, '', 0, '', 14),
(48, '29', 54, 0, 0, 0, 0, '', '', '', '', '', '', '54', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/31 13:00', 'pabel vasquez', '940598303', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170531135926', 'Instalacion', 0, 0, '', 0, '', 14),
(49, '30', 55, 0, 0, 0, 0, '', '', '', '', '', '', '55', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/31 15:00', 'DFASDFADS', '23123213', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170531140550', 'Instalacion', 0, 0, '', 0, '', 14),
(50, '30', 56, 0, 0, 0, 0, '', '', '', '', '', '', '56', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/05/31 15:00', 'DFASDFADS', '23123213', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170531140551', 'Instalacion', 0, 0, '', 0, '', 14),
(51, '31', 57, 0, 0, 0, 0, '', '', '', '', '', '', '57', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/06/02 13:00', 'aaffasdfsd', '123123', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170531142208', 'Instalacion', 0, 0, '', 2, '', 14),
(52, '31', 58, 0, 0, 0, 0, '', '', '', '', '', '', '58', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/06/02 13:00', 'aaffasdfsd', '123123', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170531142209', 'Instalacion', 0, 0, '', 2, '', 14),
(53, '1', 59, 29, 0, 0, 0, '3123123', '', '', '', '', '', '59', 'Domicilio Cliente', '', '7 10 32 jr huascaran 751 Referencia ..altura del hospital de la solidaridad', '-123312,-123123', '2017-06-08 10:00:00', 'PABEL', '3123124124', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170607104910', 'Instalacion', 0, 0, '2', 2, '20170607105113', 36),
(54, '32', 60, 0, 0, 0, 0, '', '', '', '', '', '', '60', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/06/13 09:00', 'Wilman Pabel Vasquez Garcia', '3123123141', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170612180257', 'Instalacion', 0, 0, '', 0, '', 14),
(55, '33', 61, 0, 0, 0, 0, '', '', '', '', '', '', '61', 'Taller', '', 'Av. Paseo de la RepÃºblica 4925 con Angamos Este - Surquillo, Lima, Lima', '', '2017/06/14 11:00', 'hermano', '312312421', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170613092736', 'Instalacion', 0, 0, '', 0, '', 14),
(62, '00002644', 18, 0, 11, 0, 11, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '20170615101918', 'Chequeo', 0, 0, '', 0, '', 14),
(63, '00002644', 18, 0, 11, 0, 11, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', '', '', '', '20170615102637', 'Chequeo', 0, 0, '', 0, '', 14),
(64, '1', 62, 0, 0, 0, 0, '', '', '', '', '', '', '62', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '', '', '', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170615143151', 'Instalacion', 0, 0, '', 0, '', 0),
(65, '1', 63, 0, 0, 0, 0, '', '', '', '', '', '', '63', 'Taller', '', 'Paseo de la Republica 4925,Surquillo, Lima, Lima', '', '2017-06-15 11:00:00', 'pabel', '3123123123', 0, 2, '', '', '', '', '', '', '', '', '', '', '', '20170615143453', 'Instalacion', 0, 0, '', 0, '', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `operaciones`
--
ALTER TABLE `operaciones`
  ADD PRIMARY KEY (`idoperacion`),
  ADD KEY `idoperacion` (`idoperacion`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `operaciones`
--
ALTER TABLE `operaciones`
  MODIFY `idoperacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

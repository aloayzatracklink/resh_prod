<?php $nav="detalleChoque" ?>
<?php include("../../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("../../ingresar");
} ?>
<?php 
$id=$_GET['id'];
$choqueObj=new Choque;
$dataChoque=$choqueObj->obtenerDatosDeChoquePorId($id);
$seguimientoChObj=new SeguimientoChoque;
$echo=array();
$errores=array();
if (isset($_POST['subirAudio'])) {
    $fechaRegistro=date("YmdHis");
      if (!empty($_FILES['audioLlamada']['name'])) {
        $extencionesAbilitadas=array('wav',"WAV");
        $nombreArchivo=$_FILES['audioLlamada']['name'];
        $tmp=explode('.', $nombreArchivo);
        $extencionArchivo=end($tmp);
        $tamanioArchivo=$_FILES['audioLlamada']['size'];
        $ubicacionTemporal=$_FILES['audioLlamada']['tmp_name'];
        if (!in_array($extencionArchivo,$extencionesAbilitadas)) {
          $errores[]='La extencion del audio  deve ser Wav';
        }
        if ($tamanioArchivo>10242880) {
          $errores[]='El tamaño del archivo no deve exeder los 10 megabytes';
        }
      }
    if (empty($errores)) {
        move_uploaded_file($ubicacionTemporal, 'audio/'.$fechaRegistro.$nombreArchivo);
        $seguimientoChObj->idseguimientoChoque=limpiar($_POST['subirAudio']);
        $seguimientoChObj->audio=$fechaRegistro.$nombreArchivo;
        if($seguimientoChObj->registrarAudio()){
            $echo[]="Archivo subido con exito";
        }else{
            $errores[]="Algun error aucurrio pfasfasdf3f2323";
        }
        
    }

    
}



 ?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?> 

  <link href="../../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <?php include("../../includes/menuIzquierdo.php") ?> 
            </div>
        </nav>
  <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../../includes/navegacionArriba.php") ?>
        </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2><?php echo $dataChoque->placaChoque ?> de cliente  <?php echo $dataChoque->propietario ?></h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="../choques.php">Choques</a>
                    </li>
                    
                    <li class="active">
                        <strong>Detalle choque</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>
        <div class="wrapper wrapper-content">
            <div class="row animated fadeInRight">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="text-center float-e-margins p-md">
                    
                    <a href="#" class="btn btn-xs btn-primary" id="lightVersion">Version Ligera</a>
                    <a href="#" class="btn btn-xs btn-primary" id="darkVersion">Version Clara</a>
                    <a href="#" class="btn btn-xs btn-primary" id="leftVersion">Todo a la Izquierda</a>
                    <a href="../phpWord/choqueWord?id=<?php echo $id ?>" class="btn btn-xs btn-info">Obtener Reporte en word</a>
                    </div>
                    <div class="ibox-content" id="ibox-content">

                        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
                             <div class="vertical-timeline-block">
                                <div class="vertical-timeline-icon red-bg">
                                    <i class="fa fa-exclamation-circle"></i>
                                </div>
                                <div class="vertical-timeline-content">
                                   <p>Se registra evento Crash en la plataforma <?php echo $dataChoque->plataforma ?> correspondiente al cliente <strong><?php echo $dataChoque->propietario ?></strong>  con Identificacion Numero <?php echo $dataChoque->idcliente ?>, el crash se registró con una intensidad de <?php echo $retVal = (empty($dataChoque->impacto)) ? "<span class='text-danger'>No se registro</span>" : $dataChoque->impacto ; ?> en la siguiente dirección: <?php echo $retVal = (empty($dataChoque->lugar)) ? "<span class='text-danger'>No se registro</span>" : $dataChoque->lugar ; ?>
            cliente cuenta con Plan <?php echo $retVal = (empty($dataChoque->plan)) ? "<span class='text-danger'>No se registro</span>" : $dataChoque->plan ; ?>. Contrato <?php echo vence($dataChoque->vence) ?> <?php echo formatoSinHora($dataChoque->vence) ?>
            </p><br>
                                    <span class="vertical-date small text-muted"><?php echo formato($dataChoque->fechaRegistro) ?> </span>
                                    <small> <br>
                                    <strong>Operador a cargo</strong> -- <cite title="" data-original-title=""><?php $usuarioAsesor= Usuario::obtenerUsuario($dataChoque->idusuario); echo $usuarioAsesor->usuarioNombre;echo "&nbsp;"; echo $usuarioAsesor->usuarioApellidoPaterno  ?></cite></small>             
                                   <!--   <form action="" method="post" enctype="multipart/form-data">
                                          <input type="file" name="voucher" required id="voucherImg" />
                                          <button class="btn btn-primary" type="submit" name="subirAudio" value="<?= $var->idseguimientoChoque ?>"  >Subir</button>
                                    </form> -->
                                </div>
                            </div>
                           <?php $datosSeguimiento=SeguimientoChoque::obtenerDatosChoquesSeguimientoASC($dataChoque->idchoque) ?> 
                           <?php if ($datosSeguimiento): ?> 
<?php foreach ($datosSeguimiento as $var): ?>
                  <div class="vertical-timeline-block">
                    <div class="vertical-timeline-icon gray-bg">
                        <i class="fa <?php if ($var->tipoContacto==1): ?>
                            fa-phone-square
                        <?php elseif($var->tipoContacto==2): ?>
                            fa fa-phone
                        <?php elseif ($var->tipoContacto==3): ?>fa-envelope-o                   
                        <?php else: ?>
                             fa-file-text-o                 
                        <?php endif ?>-circle"></i>
                    </div>
                    <div class="vertical-timeline-content">
                        <p><?php if ($var->tipoContacto==1): ?>
                            Llamada realizada a
                        <?php elseif($var->tipoContacto==2): ?>
                            Llamada recibida de
                        <?php elseif ($var->tipoContacto==3): ?> 
                            Correo enviado a            
                        <?php else: ?>
                        <?php endif ?><strong> <?php echo $var->nombre ?></strong></p>
                        <p><?php echo $var->detalles ?>
                        <?php echo $var->notas ?>
                        </p>
                        <span class="vertical-date small text-muted"> <?php echo formato($var->fechaRegistro) ?> </span>
                        <small> <br>
                         -- <cite title="" data-original-title=""><?php $usuarioAsesor= Usuario::obtenerUsuario($var->idusuario); echo $usuarioAsesor->usuarioNombre;echo "&nbsp;"; echo $usuarioAsesor->usuarioApellidoPaterno  ?></cite></small>  
                         <br> 
                         <?php if ($var->audio==''): ?>
                            <?php if ($session): ?>
                                
                            <?php if ($session->idusuario==36 || $session->idusuario==38  || $session->idusuario==54 || $session->idusuario==2): ?>
                            <form action="" method="post" enctype="multipart/form-data">
                               <div class="row">
                                   <div class="col-md-6"> <input type="file" name="audioLlamada" required id="voucherImg" class="form-group" /></div>
                                   <div class="col-md-6"><button class="btn btn-primary" type="submit" name="subirAudio" value="<?= $var->idseguimientoChoque ?>"  >Subir</button></div>
                               </div>
                             </form>
                            <?php endif ?>
                            <?php endif ?>
                        <?php else: ?>
                            <?php if ($session): ?>
                                
                            <?php if ($session->idusuario==36 || $session->idusuario==38  || $session->idusuario==54 || $session->idusuario==2): ?>
                            <br>
                            <?php    
                              $tmp=explode('-', $var->audio);
                                $numero=end($tmp);   
                              $tmp2=explode('.', $numero);
                                $numeroReal=current($tmp2);

                                ?>
                            <small>Llamada del número <?= $numeroReal ?></small><br>
                           <audio controls>
                              <source src="audio/<?= $var->audio ?>" type="audio/ogg">
                            Tu navegador no soporta audo.
                            </audio>
                            <?php endif ?>
                            <?php endif ?>
                         <?php endif ?>
                    </div>
                </div>
 <?php endforeach ?>
  <?php endif ?>
                       
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
      <?php include("../../includes/footer.php") ?>

        </div>
        </div>
    <!-- Mainly scripts -->
    <script src="../../assets/js/jquery-2.1.1.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Peity -->
    <script src="../../assets/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="../../assets/js/inspinia.js"></script>
    <script src="../../assets/js/plugins/pace/pace.min.js"></script>
<script src="../../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <!-- Peity -->
    <script src="../../assets/js/demo/peity-demo.js"></script>

<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
 <?php include_once('../includes/script.php') ?> 
<script>
  <?php if (!empty($errores)): ?>
  <?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
<?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
  <?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
<?php endforeach ?>
<?php endif ?> 
</script>
    <script>
        $(document).ready(function(){

            // Local script for demo purpose only
            $('#lightVersion').click(function(event) {
                event.preventDefault()
                $('#ibox-content').removeClass('ibox-content');
                $('#vertical-timeline').removeClass('dark-timeline');
                $('#vertical-timeline').addClass('light-timeline');
            });

            $('#darkVersion').click(function(event) {
                event.preventDefault()
                $('#ibox-content').addClass('ibox-content');
                $('#vertical-timeline').removeClass('light-timeline');
                $('#vertical-timeline').addClass('dark-timeline');
            });

            $('#leftVersion').click(function(event) {
                event.preventDefault()
                $('#vertical-timeline').toggleClass('center-orientation');
            });


        });
    </script>

</body>

</html>

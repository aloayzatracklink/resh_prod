<?php $nav="seguimientoAsistencia" ?>
<?php 
  require_once("../../includes/head.php");
 ?> 
<?php if (!$session->yaEstaIngresado()) {
    irA("../../ingresar");
} ?>
<link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
<link rel="stylesheet" href="../../assets/css2/style.css">
<?php if (!$session->yaEstaIngresado()) { irA("ingresar");   } 

$id=limpiar($_GET['id']);
$choqueObj=Choque::obtenerDatosDeAsistenciaPorId($id);
$clienteDatos=Onix::ObtenerDatosDesdeChasis($choqueObj->chasis);
if (isset($_POST['actualizar'])) {
    $choqueObj->plan=strtoupper($_POST['plan']);
    $choqueObj->modalidad=strtoupper($_POST['tipo']);
    $choqueObj->lugar=strtoupper($_POST['lugar']);
    $choqueObj->aseguradora=strtoupper($_POST['aseguradora']);
    $choqueObj->numeroPoliza=strtoupper($_POST['numeroPoliza']);
    $choqueObj->numeroSiniestroAseguradora=strtoupper($_POST['numeroSiniestroAseguradora']);
    $choqueObj->estado=$_POST['estado'];
    if($choqueObj->actualizarDatosAsistencia()){
        Log::registrarLog("coqueActualizar",$id,$_POST,"SeguimientoCambios");   
        $mensaje="Los datos fueron actualizados correctamente";
    }else{
        $mensaje="";
    }
}

$seguimientoChoqueObj=new SeguimientoChoque();
if (isset($_POST['actualizarDC'])) {
        $seguimientoChoqueObj->placaChoque=$clienteDatos->placa;
        $seguimientoChoqueObj->marca=$clienteDatos->marca;
        $seguimientoChoqueObj->modelo=$clienteDatos->modelo;
        $apellidos=utf8_encode($clienteDatos->apellidos); $propietario = (!empty($clienteDatos->nombre)) ? utf8_encode($clienteDatos->nombre)." ".$apellidos : $apellidos;
        $seguimientoChoqueObj->propietario=$propietario;
        $seguimientoChoqueObj->idcliente=$clienteDatos->dni;
        if ($seguimientoChoqueObj->actualizarDatosCliente()) {
           
         } 

}

?>

<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<body>

    <div id="wrapper">

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
             <?php include("../../includes/menuIzquierdo.php") ?>
         </div>
     </nav>


     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../../includes/navegacionArriba.php") ?>
            </nav>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">

                <div class="col-sm-6"> 
                    <div class="ibox">

                        <div class="ibox-content">    					
    					<?php $apellidos=utf8_encode($clienteDatos->apellidos) ?>
						<h3><strong><?php echo $retVal = (!empty($clienteDatos->nombre)) ? "$clienteDatos->nombre"." "."$apellidos" : "$apellidos" ; ?></strong> <small>Contrato <?php echo vence($clienteDatos->hasta) ?></small></h3>
						<div class="row">
							<div class="col-md-3"><i class="fa fa-phone-square"></i> <?php echo $clienteDatos->celular ?></div>
							<div class="col-md-3"><i class="fa fa-mobile"></i> <?php echo $clienteDatos->celular2 ?></i></div>
							<div class="col-md-3"><i class="fa fa-phone"></i> <?php echo $clienteDatos->telefonoTrabajo ?></div>
							<div class="col-md-3"><i class="fa fa-phone-square"></i> <?php echo $clienteDatos->telefono ?></div>
						</div>
             <div class="ibox-content">

            <div class="row">
               <div class="form-group"><label class="col-sm-2 control-label">Apuntes:</label>
                <div class="col-sm-10"><input id="apuntesRegistrar" type="text" class="form-control input-lg" name="apuntes" required autocomplete="off" autofocus style="font-size: 25px;height: 60px;"> <span class="help-block m-b-none">Para registrar los apuntes presiona enter.</span>
                </div>
            </div> 
        </div>
                  
</div>
</div>
</div>
<div class="ibox">
    <div class="ibox-content">

<div class="form-group col-md-12" >
        <label for="Observaciones">Contactos Autorizados   </label>
        <div style="height: 80px;
    overflow-y: auto;">
  <?php 
  $objContacto=Onix::obtenerContactosAutorizadosDeCliente($clienteDatos->idlocation,$clienteDatos->idcustomer);
if($objContacto){
  $n=1;
  ?>
  <table class="table table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>Parentesco</th>
        <th>Nombres</th>
        <th>Celular</th>
        <th>Telefono</th>
        <th>Correo</th>
    </tr>
    </thead>
    <tbody>
  <?php
  foreach ($objContacto as $var4) {
  ?>  
  <tr>
      <td><?php echo $n++ ?> </td>
      <td><?php echo $var4->parentesco ?></td>
      <td><?php  echo $var4->nombreContacto." ".$var4->contactoApellido; ?></td>
      <td > <?php echo $var4->contactoCelular ?> </td>
      <td > <?php echo $var4->contactoTelefono ?> </td>
      <td > <?php echo $var4->correoContacto ?> </td>
  </tr>
  <?php

  }
  ?>
     </tbody>
 </table>
  <?php
  
}else{
  echo "sin contactos Autoriados ";
}
   ?>

        </div>
<br><hr>
      </div>
<div class="form-group col-md-6" >
				<label for="Observaciones">Observaciones Onyx </label>
				<div style="border-right: solid;height: 60px;
    overflow-y: auto;">
				<label class="border"><?php echo utf8_encode($clienteDatos->comentarios) ?></label>
				</div>
			</div>
<div class="col-md-6 form-group"  >
			<label for="">Ordenes de Vehículo</label>
		<div style="height: 60px;
    overflow-y: auto;">
				<?php $objOrdenes=Onix::obtenerOrdenesVehiculo($clienteDatos->idcustomer,$clienteDatos->idvehicle,$clienteDatos->idlocation) ?>
				<?php foreach ($objOrdenes as $var): ?>
					<?php echo utf8_encode($var->articulo) ?><br>
					<?php echo haceMinimoAnio($var->fechaOrden) ?><hr>
				<?php endforeach ?>
				</div>
			</div>
<div class="row">
	
</div>
</div>
</div>

<hr><hr><br>

</div>
<div class="col-sm-6">
    <div class="ibox ">

        <div class="ibox-content">
            <div class="tab-content">
                <div id="contact-1" class="tab-pane active">
                    <div class="row m-b-lg">
                        <div class="col-lg-4  text-center">
                            <h2><?php echo $choqueObj->placaAsistencia; ?></h2>
                            <h2><?php echo $choqueObj->marca ?></h2>
                            <h2><?php echo $choqueObj->modelo ?></h2> 
                            <h3>N° Siniestro <?php echo $choqueObj->idchoque ?></h3> 
                                             </div>
                                             <div class="col-lg-8">                 
                                                <ul class="list-group clear-list">
                                                  <li class="list-group-item">
                                                    <span class="pull-right"><?php echo $clienteDatos->color ?></span>
                                                    Color
                                                </li>

                                                <li class="list-group-item">
                                                    <span class="pull-right"> <?php echo $clienteDatos->anio ?> </span>
                                                    Año
                                                </li>
                                                <li class="list-group-item">
                                                    <span class="pull-right"> <?php echo $clienteDatos->chasis ?> </span>
                                                    Chasis:
                                                </li>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal2">
                                                            Actualizar datos 
                                                        </button>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <form method="post">
                                                            <button type="submit" class="btn btn-primary " name="estado" name="actualizarDC" >Actualizar</button>
                                                        </form>
                                                    </div>
                                                </div>
                                    </ul>
                                </div>
                            </div>
                            <div class="client-detail">
                               <div class="full-height-scroll" id="scrollSeguimiento">  
                               </div>
                           </div>

                       </div>



                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
<?php include("../../includes/footer.php") ?>
</div>
</div>
<!-- ////////////Modal Here -->
<div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
<div class="modal-content">
  <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
      <h4 class="modal-title">Actualizar datos de la asistencia</h4>
  </div>

  <form action="" method="post">
      <div class="modal-body">
          <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <div class="row">
                           <span class="help-block m-b-none text-danger">PLAN</span>
                          <div class="col-sm-12"><input type="text" class="form-control" name="plan" placeholder="Contrato" value="<?php echo $choqueObj->plan ?>">
                          </div>
                      </div>
                  </div>
                    <div class="form-group">
                      <div class="row">
                           <strong><span class="help-block m-b-none text-danger">Lugar de la asistencia</span></strong>
                          <div class="col-sm-12"><input id="lugarChoque" type="text" class="form-control" name="lugar" placeholder="Lugar de la asistencia" value="<?php echo $choqueObj->lugar ?>">
                          </div>
                      </div>
                  </div>
                  
              </div>
              <div class="col-md-6">
                
                    <div class="form-group">
                      <div class="row">
                           <strong><span class="help-block m-b-none text-danger">Compañia de seguros</span></strong>
                          <div class="col-sm-12"><input type="text" class="form-control" name="aseguradora" placeholder="Compañia de seguros" value="<?php echo $choqueObj->aseguradora ?>" >
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="row">
                          <strong><span class="help-block m-b-none text-danger">Número de póliza</span></strong> 
                          <div class="col-sm-12"><input type="text" class="form-control" name="numeroPoliza" placeholder="Número de póliza" value="<?php echo $choqueObj->numeroPoliza ?>">
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="row">
                          <strong><span class="help-block m-b-none text-danger">Número de siniestro aseguradora</span></strong>
                          <div class="col-sm-12"><input id="horaRoboH" type="text" class="form-control" name="numeroSiniestroAseguradora" placeholder="Numero de siniestro" value="<?php echo $choqueObj->numeroSiniestroAseguradora ?>"> 
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="row">
          <div class="col-md-12">
          <h2 class="text-center">Estado</h2>
            <div class="col-md-4">
                          <label for="asistiendo">Asistiendo</label>
                          <div class="switch">
                            <div class="onoffswitch">
                              <input type="radio" class="onoffswitch-checkbox" id="asistiendo" value="11" name="estado"  <?php echo $retVal = ($choqueObj->estado==11) ? "checked" : "" ; ?>  >
                              <label class="onoffswitch-label" for="asistiendo">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                            </div>
                          </div>
                        </div>

                  <div class="col-md-4">
                          <label for="asistido">Asistido</label>
                          <div class="switch">
                            <div class="onoffswitch">
                              <input type="radio" class="onoffswitch-checkbox" id="asistido" value="12" name="estado" <?php echo $retVal = ($choqueObj->estado==12) ? "checked" : "" ; ?> >
                              <label class="onoffswitch-label" for="asistido">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                            </div>
                          </div>
                        </div>

                  <div class="col-md-4">
                          <label for="noReportado">Cancelado</label>
                          <div class="switch">
                            <div class="onoffswitch">
                              <input type="radio" class="onoffswitch-checkbox" id="noReportado" value="32" name="estado" <?php echo $retVal = ($choqueObj->estado==32) ? "checked" : "" ; ?> >
                              <label class="onoffswitch-label" for="noReportado">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                            </div>
                          </div>
                        </div>

          </div>  
          </div>
<div class="row">
          <div class="col-md-12">
          <h2 class="text-center">Tipo asistencia</h2>
            <div class="col-md-12">
                <select id="tipo" class="input-sm form-control input-s-sm inline" required name='tipo'>
<?php
    if ($choqueObj->tipo=='') {
       ?>
                        <option disabled value='' selected  >Seleccione tipo</option>
       <?php 
    }else{
        ?>
            <option value='<?= $choqueObj->tipo ?>' selected  ><?= $choqueObj->tipo ?></option>
       <?php 
    }
?>
                            <option value="vial">Vial</option>
                            <option value="hogar">Hogar</option>
                            <option value="con orientacion legal">Orientacion legal</option>
                            <option value="con orientacion medica">Orientacion medica</option>
                            <option value="con ambulancia">Con ambulancia</option>
                </select>
            </div>
        </div>  
        </div>
        <br>
        <div class="row">
            <div class="col-md-3 ">
                <a href="../phpWord/asistenciaWord?id=<?php echo $id  ?>" class="btn btn-primary">Obtener reporte en Word</a>
            </div> 
            <div class="col-md-3">
                <a href="detalleAsistencia?id=<?php echo $id  ?>" class="btn btn-info">Visualizar</a> 
            </div> 
        </div>
<input type="hidden" name='ant' value="<?= print_r($choqueObj) ?>" >
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
          <button type="submit" name="actualizar" class="btn btn-primary">Guardar cambios</button>
      </form>

    </div>


                                                </div>
                                            </div>
                                        </div>
    <!-- finde moda -->



<!-- Mainly scripts -->
<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/js/bootstrap.min.js"></script>
<script src="../../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../../assets/js/inspinia.js"></script>
<script src="../../assets/js/plugins/pace/pace.min.js"></script>
<script src="../../assets/build/jquery.datetimepicker.full.js"></script>
<?php include_once('../../includes/script.php') ?> 
<script>
<?php $fechaA=date('Y-m-d H:00:00') ?>
     var eventos={}
    eventos.frejcar =function(){
        $.ajax({
            url: '../ajax/seguimientoAsistencia',
                type: 'POST', // Send post data
                data: 'idChoque=<?php echo $id ?>',
                async: true,
                success: function(data){
                    $('#scrollSeguimiento').html(data);
                }
                // error: function(e){                     
                //     // revertFunc();
                //     alert('Error processing your request: '+e.responseText);
                // }  
            });
    }
    eventos.interval = setInterval(eventos.frejcar, 4000);
    eventos.frejcar();

    eventos.throwMessage = function(message) {
    if ($.trim(message.length != 0)) {
        $.ajax({
            url: '../ajax/seguimientoAsistencia',
            type: 'post',
            data: {method: 'throw', message: message, idChoque: <?php echo $id ?>},
            success: function(data) {
                eventos.frejcar();
                eventos.entry.val('');
            }
        }

        );
    }
}

eventos.entry = $('#apuntesRegistrar');
eventos.entry.bind('keydown', function(e) {
    if (e.keyCode === 13 && e.shiftKey === false) {
      // alert("fun")  ;
        eventos.throwMessage($(this).val());
        e.preventDefault();
    }

});
</script>
</body>
</html>

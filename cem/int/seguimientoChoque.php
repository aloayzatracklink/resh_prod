<?php $nav="seguimientoChoque" ?>
<?php 
  require_once("../../includes/head.php");
 ?> 
<?php if (!$session->yaEstaIngresado()) {
    irA("../../ingresar");
} ?>
<link rel="stylesheet" type="text/css" href="../../jquery.datetimepicker.css"/>
<link rel="stylesheet" href="../../assets/css2/style.css">
<?php if (!$session->yaEstaIngresado()) { irA("ingresar");   } 

$id=$_GET['id'];
$choqueObj=Choque::obtenerDatosDeChoquePorId($id);
$clienteDatos=Onix::ObtenerDatosDesdeChasis($choqueObj->chasis);
if (isset($_POST['actualizar'])) {
    $choqueObj->impacto=strtoupper($_POST['impacto']);
    $choqueObj->velocidad=strtoupper($_POST['velocidad']);
    $choqueObj->plan=strtoupper($_POST['plan']);
    $choqueObj->lugar=strtoupper($_POST['lugar']);
    $choqueObj->fechaChoque=strtoupper($_POST['fechaChoque']);
    $choqueObj->aseguradora=strtoupper($_POST['aseguradora']);
    $choqueObj->numeroPoliza=strtoupper($_POST['numeroPoliza']);
    $choqueObj->numeroSiniestroAseguradora=strtoupper($_POST['numeroSiniestroAseguradora']);
    $choqueObj->estado=$_POST['estado'];
    if($choqueObj->actualizarDatos()){
        Log::registrarLog("coqueActualizar",$id,$_POST,"SeguimientoCambios");   
        $mensaje="Los datos fueron actualizados correctamente";
    }else{
        $mensaje="";
    }
}

$seguimientoChoqueObj=new SeguimientoChoque();
if (isset($_POST['registrarSeguimiento'])) {
    $seguimientoChoqueObj->tipoContacto=$_POST['tipoContacto'];
    $seguimientoChoqueObj->nombre=$_POST['nombre'];
    $seguimientoChoqueObj->detalles=$_POST['detalles'];
    $seguimientoChoqueObj->idusuario=$session->idusuario;
    $seguimientoChoqueObj->idchoque=$choqueObj->idchoque;
    if ($seguimientoChoqueObj->registrarSeguimientoChoque()) {
        // echo "fasdfsdf";
    }else{
        echo "Fail detected";
    }
}
if (isset($_POST['registrarApuntee'])) {
    $seguimientoChoqueObj->notas=$_POST['apuntes'];
    $seguimientoChoqueObj->idchoque=$choqueObj->idchoque;
    if ($seguimientoChoqueObj->registrarNota()) {

    }else{
        echo "Fail detected parte 2";
    }
}
if (isset($_POST['actualizarDC'])) {
        $seguimientoChoqueObj->placaChoque=$clienteDatos->placa;
        $seguimientoChoqueObj->marca=$clienteDatos->marca;
        $seguimientoChoqueObj->modelo=$clienteDatos->modelo;
        $apellidos=utf8_encode($clienteDatos->apellidos); $propietario = (!empty($clienteDatos->nombre)) ? utf8_encode($clienteDatos->nombre)." ".$apellidos : $apellidos;
        $seguimientoChoqueObj->propietario=$propietario;
        $seguimientoChoqueObj->idcliente=$clienteDatos->dni;
        if ($seguimientoChoqueObj->actualizarDatosCliente()) {
           
         } 

}

if (isset($_POST['enviarCorreoNoContactado'])) {
    Log::registrarLog("coqueActualizar",$id,$_POST,"Correo No Contactado");   
    require_once '../../mailphp/PHPMailer/PHPMailerAutoload.php';
    $placa=$choqueObj->placaChoque;
    $nombreCliente=$choqueObj->propietario;
    $marca=$choqueObj->marca;
    $modelo=$choqueObj->modelo;
    $chasis=$choqueObj->chasis;
    $clienteObj=Onix::ObtenerDatosDesdeChasis($chasis);
    $correosCliente=$clienteObj->correo;
    $choqueObj->vence=$clienteObj->hasta;
    if (!empty($clienteObj->correo2))
    $correosCliente.=",".$clienteObj->correo2;
  $usuario=Usuario::obtenerUsuario($session->idusuario);  $usuario->usuarioNombre; $usuario->usuarioApellidoPaterno;  
      /////Aqui se envia el correo ///
  $thehtml="<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<meta name='viewport' content='width=device-width'>
<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<title></title><style type='text/css' id='media-query'>
body {
margin: 0;
padding: 0; }
table, tr, td {
vertical-align: top;
border-collapse: collapse; }
.ie-browser table, .mso-container table {
table-layout: fixed; }
* {
line-height: inherit; }
a[x-apple-data-detectors=true] {
color: inherit !important;
text-decoration: none !important; }
[owa] .img-container div, [owa] .img-container button {
display: block !important; }
[owa] .fullwidth button {
width: 100% !important; }
.ie-browser .col, [owa] .block-grid .col {
display: table-cell;
float: none !important;
vertical-align: top; }
.ie-browser .num12, .ie-browser .block-grid, [owa] .num12, [owa] .block-grid {
width: 500px !important; }
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
line-height: 100%; }
.ie-browser .mixed-two-up .num4, [owa] .mixed-two-up .num4 {
width: 164px !important; }
.ie-browser .mixed-two-up .num8, [owa] .mixed-two-up .num8 {
width: 328px !important; }
.ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {
width: 250px !important; }
.ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {
width: 166px !important; }
.ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {
width: 125px !important; }
.ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {
width: 100px !important; }
.ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {
width: 83px !important; }
.ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {
width: 71px !important; }
.ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {
width: 62px !important; }
.ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {
width: 55px !important; }
.ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {
width: 50px !important; }
.ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {
width: 45px !important; }
.ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {
width: 41px !important; }
@media only screen and (min-width: 520px) {
.block-grid {
width: 500px !important; }
.block-grid .col {
display: table-cell;
Float: none !important;
vertical-align: top; }
.block-grid .col.num12 {
width: 500px !important; }
.block-grid.mixed-two-up .col.num4 {
width: 164px !important; }
.block-grid.mixed-two-up .col.num8 {
width: 328px !important; }
.block-grid.two-up .col {
width: 250px !important; }
.block-grid.three-up .col {
width: 166px !important; }
.block-grid.four-up .col {
width: 125px !important; }
.block-grid.five-up .col {
width: 100px !important; }
.block-grid.six-up .col {
width: 83px !important; }
.block-grid.seven-up .col {
width: 71px !important; }
.block-grid.eight-up .col {
width: 62px !important; }
.block-grid.nine-up .col {
width: 55px !important; }
.block-grid.ten-up .col {
width: 50px !important; }
.block-grid.eleven-up .col {
width: 45px !important; }
.block-grid.twelve-up .col {
width: 41px !important; } }
@media (max-width: 520px) {
.block-grid, .col {
min-width: 320px !important;
max-width: 100% !important; }
.block-grid {
width: calc(100% - 40px) !important; }
.col {
width: 100% !important; }
.col > div {
margin: 0 auto; }
img.fullwidth {
max-width: 100% !important; } }
</style>
</head>
<body class='clean-body' style='margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #FFFFFF'>
<div class='nl-container' style='min-width: 320px;Margin: 0 auto;background-color: #FFFFFF'>
<div style='background-color:#003399;'>
  <div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
    <div style='border-collapse: collapse;display: table;width: 100%;'>
      <div class='col num12' style='min-width: 320px;max-width: 500px;width: 500px;width: calc(18000% - 89500px);background-color: transparent;'>
        <div style='background-color: transparent; width: 100% !important;'>
          <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 5px; padding-left: 5px;'>
            <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
              <div align='center'><div style='border-top: 10px solid transparent; width:100%; line-height:0px;'>&nbsp;</div></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>    
<div style='background-color:#e7e7e7;'>
<div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid two-up'>
  <div style='border-collapse: collapse;display: table;width: 100%;'>
    <div class='col num6' style='Float: left;max-width: 320px;min-width: 250px;width: 250px;width: calc(35250px - 7000%);background-color: transparent;'>
      <div style='background-color: transparent; width: 100% !important;'>
        <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 5px; padding-left: 5px;'>
          <div align='center' class='img-container center' style='padding-right: 0px;  padding-left: 0px;'>
            <a href='http://www.tracklink.pe/TracklinkPeru/' target='_blank'>
              <img class='center' align='center' border='0' src='cid:logo' alt='Image' title='Tracklink' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;width: 100%;max-width: 217px' width='217'>
            </a>
          </div>
        </div>
      </div>
    </div>
<div class='col num6' style='Float: left;max-width: 320px;min-width: 250px;width: 250px;width: calc(35250px - 7000%);background-color: transparent;'>
  <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:15px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;'>
      <div align='center' class='img-container center' style='padding-right: 0px;  padding-left: 0px;'>
        <a href='http://www.tracklink.pe/TracklinkPeru/' target='_blank'>
          <img class='center' align='center' border='0' src='cid:eCall' alt='Image' title='Octo' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;width: 100%;max-width: 100px' width='100'>
        </a>
      </div>
    </div>
  </div>
</div>
  </div>
</div>
</div>    
<div style='background-color:#EDEDED;'>
<div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
  <div style='border-collapse: collapse;display: table;width: 100%;'>

    <div class='col num12' style='min-width: 320px;max-width: 500px;width: 500px;width: calc(18000% - 89500px);background-color: transparent;'>
      <div style='background-color: transparent; width: 100% !important;'>
        <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'>
          <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
           <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px'>Estimado(a) :&nbsp;<strong>$nombreCliente.</strong></p></div>
         </div>
         <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
           <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 17px;text-align: justify'>Acabamos de recibir en nuestra plataforma una alerta de posible colisión de su vehículo. <br> <br>Hemos intentado comunicarnos con  usted sin éxito, agradeceremos nos brinde mayor detalle de lo sucedido comunicándose a la oficina central de TRACKLINK OCTO llamando al (01)630-7576, para que podamos asistirlo.</p></div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div> 
<div style='background-color:#EDEDED;'>
<div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
 <div style='border-collapse: collapse;display: table;width: 100%;'>
  <div class='col num12' style='min-width: 320px;max-width: 500px;width: 500px;width: calc(18000% - 89500px);background-color: transparent;'>
    <div style='background-color: transparent; width: 100% !important;'>
      <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'>
        <div align='center' class='img-container center' style='padding-right: 0px;  padding-left: 0px;'>
          <img class='center' align='center' border='0' src='cid:crash' alt='Image' title='Choque' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 200px' width='200'>
        </div>       
      </div>
    </div>
  </div>
</div>
</div>
</div>
<div style='background-color:transparent;'>
  <div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid mixed-two-up'>
    <div style='border-collapse: collapse;display: table;width: 100%;'>

      <div class='col num4' style='Float: left;max-width: 320px;min-width: 164px;width: 167px;width: calc(78164px - 15600%);background-color: transparent;'>
        <div style='background-color: transparent; width: 100% !important;'>
          <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 5px; padding-left: 5px;'>
            <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
             <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px'><strong><span style='font-size: 14px; line-height: 16px;'>Placa:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$placa</span></strong></p></div>
           </div>             
           <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
             <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px'><strong><span style='font-size: 14px; line-height: 16px;'>Marca: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$marca</span></strong></p></div>
           </div>            
           <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
             <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px'><strong><span style='font-size: 14px; line-height: 16px;'>Modelo:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$modelo</span></strong></p></div>
           </div>             
           <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
             <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 12px;line-height: 14px'><strong><span style='font-size: 14px; line-height: 16px;'>Chasis:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$chasis</span></strong></p></div>
           </div>       
         </div>
       </div>
     </div>

 </div>
</div>
</div>    
 <div style='background-color:#EDEDED;'>
 <div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
  <div style='border-collapse: collapse;display: table;width: 100%;'>

    <div class='col num12' style='min-width: 320px;max-width: 500px;width: 500px;width: calc(18000% - 89500px);background-color: transparent;'>
      <div style='background-color: transparent; width: 100% !important;'>
        <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'>

          <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
           <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 17px;text-align: justify'>Usted actualmente cuenta con nuestra tecnología que salva vidas <strong><span style='color: red'>e-Call</span></strong>, Sistema europeo que detecta automáticamente choques y avisa a nuestra central de emergencia para hacer llegar ayuda especializada y asistencia vial proactiva en el menor tiempo posible y con el servicio de GPS con recuperación vehicular en caso de robo. </p></div>
         </div>   
       </div>
     </div>
   </div>

 </div>
</div>
</div> 
<div style='background-color:#EDEDED;'>
<div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid two-up'>
<div style='border-collapse: collapse;display: table;width: 100%;'>

<div class='col num6' style='Float: left;max-width: 320px;min-width: 250px;width: 250px;width: calc(35250px - 7000%);background-color: transparent;'>
  <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
      <div align='center' class='button-container center' style='padding-right: 10px; padding-left: 10px; padding-top:10px; padding-bottom:10px;'>
        <a href='mailto:centralemergencia@tracklink.pe?subject=He%20tenido%20un%20choque.&amp;body=Gracias.' target='_blank' style='display: inline-block;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #ffffff; background-color: #3AAEE0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 108px; width: 68px; width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent; padding-top: 5px; padding-right: 20px; padding-bottom: 5px; padding-left: 20px; font-family: Arial, Helvetica, sans-serif;mso-border-alt: none'>
          <span style='font-size:12px;line-height:24px;'><strong>Responder</strong></span>
        </a>
      </div>
    </div>
  </div>
</div>
<div class='col num6' style='Float: left;max-width: 320px;min-width: 250px;width: 250px;width: calc(35250px - 7000%);background-color: transparent;'>
  <div style='background-color: transparent; width: 100% !important;'>
    <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;'>
      <div align='center' class='button-container center' style='padding-right: 10px; padding-left: 10px; padding-top:10px; padding-bottom:10px;'>
        <a href='tel:016307576' target='_blank' style='display: inline-block;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #ffffff; background-color: #C04747; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 106px; width: 46px; width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent; padding-top: 5px; padding-right: 30px; padding-bottom: 5px; padding-left: 30px; font-family: Arial, Helvetica, sans-serif;mso-border-alt: none'>
          <span style='font-size:12px;line-height:24px;'><strong>Llamar</strong></span>
        </a>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>  
<div style='background-color:#e7e7e7;'>
<div style='Margin: 0 auto;min-width: 320px;max-width: 500px;width: 500px;width: calc(19000% - 98300px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;' class='block-grid '>
<div style='border-collapse: collapse;display: table;width: 100%;'>

  <div class='col num12' style='min-width: 320px;max-width: 500px;width: 500px;width: calc(18000% - 89500px);background-color: transparent;'>
    <div style='background-color: transparent; width: 100% !important;'>
      <div style='border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;'>
        <div align='center' style='padding-right: 10px; padding-left: 10px; padding-bottom: 10px;'>
          <div style='line-height:10px;font-size:1px'>&nbsp;</div>
          <div style='display: table; max-width:151;'>
            <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px'>
              <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
                <a href='https://www.facebook.com/tracklinkperu/' title='Facebook' target='_blank'>
                  <img src='cid:facebook' alt='Facebook' title='Facebook' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
                </a>
                <div style='line-height:5px;font-size:1px'>&nbsp;</div>
              </td></tr>
            </tbody></table>
            <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px'>
              <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
                <a href='http://twitter.com/TracklinkPE/' title='Twitter' target='_blank'>
                  <img src='cid:twitter' alt='Twitter' title='Twitter' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
                </a>
                <div style='line-height:5px;font-size:1px'>&nbsp;</div>
              </td></tr>
            </tbody></table>
            <table align='left' border='0' cellspacing='0' cellpadding='0' width='32' height='32' style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 0'>
              <tbody><tr style='vertical-align: top'><td align='left' valign='middle' style='word-break: break-word;border-collapse: collapse !important;vertical-align: top'>
                <a href='https://www.youtube.com/channel/UCylRCGZ4LBaNKk-XMJliURw' title='YouTube' target='_blank'>
                  <img src='cid:youtube@2x' alt='YouTube' title='YouTube' width='32' style='outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important'>
                </a>
                <div style='line-height:5px;font-size:1px'>&nbsp;</div>
              </td></tr>
            </tbody></table>
          </div>
        </div>             
        <div style='padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;'>
         <div style='font-size:12px;line-height:14px;color:#555555;font-family:Arial, Helvetica, sans-serif;text-align:left;'><p style='margin: 0;font-size: 14px;line-height: 17px;text-align: center'>TRACKLINK OCTO</p><p style='margin: 0;font-size: 14px;line-height: 16px;text-align: center'> Calle Ricardo Angulo 762 Urb Corpac - San Isidro, Lima. <br><a style='color:#0068A5;text-decoration: underline;' title='Tracklink' href='http://www.tracklink.pe' target='_blank' rel='noopener noreferrer'>www.tracklink.pe</a>&nbsp;/ <a style='color:#0068A5;text-decoration: underline;' title='Octo' href='http://www.octotelematics.com' target='_blank' rel='noopener noreferrer'>www.octotelematics.com</a></p></div>
       </div>           
     </div>
   </div>
 </div>          
</div>
</div>
</div>   
</div>
</body>
</html>";
//El correo para el cliente aqui /// <---->
$mail = new PHPMailer;
$mail->isSMTP();
$mail->SetLanguage( 'es', '../mailphp/PHPMailer/language/' );
$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->Host = gethostbyname('mail.tracklink.pe');
$mail->Port = 25;
$mail->SMTPAuth = false;
$mail->CharSet = 'UTF-8';
$mail->setFrom('no-responder@tracklink.pe', 'Central de emergencias Tracklink');  
$mail->addReplyTo("centralemergencia@tracklink.pe", 'Central de emergencias - '.$usuario->usuarioNombre.''); // donde deven responder
//Set who the message is to be sent to
$mail->addAddress("centralemergencia@tracklink.pe","Central De Emergencia"); // aquien 
// $mail->addAddress("wilmanpabel@gmail.com","Sr Pabel"); // aquien se manda el
$correo= explode(",", $correosCliente);
foreach ($correo as $corr){
        $mail->addAddress("$corr");        
  }
//mensaje
// $mail->addAddress("ldelgado@tracklink.pe","Luis"); // aquien se manda el mensaje
// $mail->addAddress("eaponte@tracklink.pe","Enrique"); // aquien se manda el mensaje
// die("test");
// $mail->addAddress("pvasquez@tracklink.pe","Pabel"); // aquien se manda el mensaje
// $mail->addAddress("drodriguez@tracklink.pe","Daniel Rodriguez"); // aquien se manda el mensaje
// $mail->addAddress("rmiranda@tracklink.pe","Renzo Miranda Quiñones"); // aquien se manda el mensaje
// $mail->addAddress("aquichiz@tracklink.pe","Andres Quichiz"); // aquien se manda el mensaje
//CC and BCC
// $mail->addCC("pruebas-ti@tracklink.pe");
$mail->addBCC("pruebas-ti@tracklink.pe");
//$mail->addBCC("lgiordanelli@tracklink.pe");
//Set the subject line
$mail->Subject = 'Colisión detectada:'.ucwords(strtolower($nombreCliente)).' placa '.$placa ." "; //// ASUNTO //////
//Read an HTML message body from an external file, convert referenced images to embedded,
$mail->AddEmbeddedImage('../../assets/img/face.png', 'facebook');
$mail->AddEmbeddedImage('../../assets/img/twitter.png', 'twitter');
$mail->AddEmbeddedImage('../../assets/img/youtube@2x.png', 'youtube@2x');
$mail->AddEmbeddedImage('../../assets/img/track_new.png', 'logo');    
$mail->AddEmbeddedImage('../../assets/img/ecall_new.png', 'eCall');
$mail->AddEmbeddedImage('../../assets/img/crash2.png', 'crash');
// $mail->AddEmbeddedImage('img/tracklink.png', 'tracklink');
//convert HTML into a basic plain-text alternative body
$mail->msgHTML($thehtml);
//Replace the plain text body with one created manually
$mail->AltBody = 'Estimado(as) '.$nombreCliente.' hemos detectdo un choque en '.$placa." ".' seguimiento por '.$usuario->usuarioNombre." "; ////////ESTE SE EL CONTENIDO//// QUE SE BERA EN PRIERA INSTANACIA
//Attach an image file
  //aqui se adjunta los archivos que queremos enviar :D

// $mail->addAttachment('images/phpmailer_mini.png');

//send the message, check for errors
if (!$mail->send()) {
  echo "Mailer Error: " . $mail->ErrorInfo;
 } else {
   $echo[]="Correo enviado";
 }
///Aqui se envia el correo ///
}
?>

<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<body>

    <div id="wrapper">

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
             <?php include("../../includes/menuIzquierdo.php") ?>
         </div>
     </nav>


     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php include("../../includes/navegacionArriba.php") ?>
            </nav>
        </div>

        <div class="wrapper wrapper-content">
            <div class="row">

                <div class="col-sm-6"> 
                    <div class="ibox">

                        <div class="ibox-content">    					
    					<?php $apellidos=utf8_encode($clienteDatos->apellidos) ?>
						<h3><strong><?php echo $retVal = (!empty($clienteDatos->nombre)) ? "$clienteDatos->nombre"." "."$apellidos" : "$apellidos" ; ?></strong> <small>Contrato <?php echo vence($clienteDatos->hasta) ?></small></h3>
						<div class="row">
							<div class="col-md-3"><i class="fa fa-phone-square"></i> <?php echo $clienteDatos->celular ?></div>
							<div class="col-md-3"><i class="fa fa-mobile"></i> <?php echo $clienteDatos->celular2 ?></i></div>
							<div class="col-md-3"><i class="fa fa-phone"></i> <?php echo $clienteDatos->telefonoTrabajo ?></div>
							<div class="col-md-3"><i class="fa fa-phone-square"></i> <?php echo $clienteDatos->telefono ?></div>
						</div>
             <div class="ibox-content">

            <div class="row">
               <div class="form-group"><label class="col-sm-2 control-label">Apuntes:</label>
                <div class="col-sm-10"><input id="apuntesRegistrar" type="text" class="form-control input-lg" name="apuntes" required autocomplete="off" autofocus style="font-size: 25px;height: 60px;"> <span class="help-block m-b-none">Para registrar los apuntes presiona enter.</span>
                </div>
            </div> 
        </div>
                  
</div>
</div>
</div>
<div class="ibox">
    <div class="ibox-content">
            
       
<div class="row">
  
<h3 class="col-md-6">Datos importantes </h3><form method="post"><button type="submit" name="enviarCorreoNoContactado" class="col-md-6 btn btn-primary" onclick="return confirm_envio()">Enviar correo no contactado</button></form>
</div>


<div class="form-group col-md-12" >
        <label for="Observaciones">Contactos Autorizados   </label>
        <div style="height: 80px;
    overflow-y: auto;">
  <?php 
  $objContacto=Onix::obtenerContactosAutorizadosDeCliente($clienteDatos->idlocation,$clienteDatos->idcustomer);
if($objContacto){
  $n=1;
  ?>
  <table class="table table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>Parentesco</th>
        <th>Nombres</th>
        <th>Celular</th>
        <th>Telefono</th>
        <th>Correo</th>
    </tr>
    </thead>
    <tbody>
  <?php
  foreach ($objContacto as $var4) {
  ?>  
  <tr>
      <td><?php echo $n++ ?> </td>
      <td><?php echo $var4->parentesco ?></td>
      <td><?php  echo $var4->nombreContacto." ".$var4->contactoApellido; ?></td>
      <td > <?php echo $var4->contactoCelular ?> </td>
      <td > <?php echo $var4->contactoTelefono ?> </td>
      <td > <?php echo $var4->correoContacto ?> </td>
  </tr>
  <?php

  }
  ?>
     </tbody>
 </table>
  <?php
  
}else{
  echo "sin contactos Autoriados ";
}
   ?>

        </div>
<br><hr>
      </div>
<div class="form-group col-md-6" >
				<label for="Observaciones">Observaciones Onyx </label>
				<div style="border-right: solid;height: 60px;
    overflow-y: auto;">
				<label class="border"><?php echo utf8_encode($clienteDatos->comentarios) ?></label>
				</div>
			</div>
<div class="col-md-6 form-group"  >
			<label for="">Ordenes de Vehículo</label>
		<div style="height: 60px;
    overflow-y: auto;">
				<?php $objOrdenes=Onix::obtenerOrdenesVehiculo($clienteDatos->idcustomer,$clienteDatos->idvehicle,$clienteDatos->idlocation) ?>
				<?php foreach ($objOrdenes as $var): ?>
					<?php echo utf8_encode($var->articulo) ?><br>
					<?php echo haceMinimoAnio($var->fechaOrden) ?><hr>
				<?php endforeach ?>
				</div>
			</div>
<div class="row">
	
</div>
</div>
</div>

<hr><hr><br>





</div>
<div class="col-sm-6">
    <div class="ibox ">

        <div class="ibox-content">
            <div class="tab-content">
                <div id="contact-1" class="tab-pane active">
                    <div class="row m-b-lg">
                        <div class="col-lg-4  text-center">
                            <h2><?php echo $choqueObj->placaChoque; ?></h2>
                            <h2><?php echo $choqueObj->marca ?></h2>
                            <h2><?php echo $choqueObj->modelo ?></h2> 
                            <h3>N° Siniestro <?php echo $choqueObj->idchoque ?></h3> 
                                   
                                             </div>
                                             <div class="col-lg-8">                 
                                                <ul class="list-group clear-list">
                                                  <li class="list-group-item">
                                                    <span class="pull-right"><?php echo $clienteDatos->color ?></span>
                                                    Color
                                                </li>

                                                <li class="list-group-item">
                                                    <span class="pull-right"> <?php echo $clienteDatos->anio ?> </span>
                                                    Año
                                                </li>
                                                <li class="list-group-item">
                                                    <span class="pull-right"> <?php echo $clienteDatos->chasis ?> </span>
                                                    Chasis:
                                                </li>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal2">
                                                            Actualizar datos 
                                                        </button>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <form method="post">
                                                            <button type="submit" class="btn btn-primary " name="estado" name="actualizarDC" >Actualizar</button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                <?php if (!empty($choqueObj->impacto)): ?>
                                                  <div class="col-md-6">Impacto <?php echo $choqueObj->impacto ?></div>
                                                <?php endif ?>
                                                <?php if (!empty($choqueObj->velocidad)): ?>
                                                   <div class="col-md-6">Velocidad <?php echo $choqueObj->velocidad ?></div>            
                                                <?php endif ?>
                                                </div>
                                    </ul>
                                </div>
                            </div>
                            <div class="client-detail">
                               <div class="full-height-scroll" id="scrollSeguimiento">  
                               </div>
                           </div>

                       </div>



                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
<?php include("../../includes/footer.php") ?>
</div>
</div>
<!-- ////////////Modal Here -->
<div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
<div class="modal-content">
  <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
      <h4 class="modal-title">Actualizar datos del Choque</h4>
  </div>


  <form action="" method="post">
      <div class="modal-body">
          <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <div class="row">
                          <strong><span class="help-block m-b-none text-danger">Impacto</span></strong> 
                          <div class="col-sm-12"><input type="text" class="form-control" name="impacto" placeholder="Impacto" value="<?php echo $retVal = (empty($choqueObj->impacto)) ? "" : $choqueObj->impacto ; ?>">
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="row">
                          <strong><span class="help-block m-b-none text-danger">Velocidad</span></strong> 
                          <div class="col-sm-12"><input type="text" class="form-control" name="velocidad" placeholder="Velocidad" value="<?php echo $retVal = (empty($choqueObj->velocidad)) ? "" : $choqueObj->velocidad ; ?>">
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="row">
                           <span class="help-block m-b-none text-danger">PLAN</span>
                          <div class="col-sm-12"><input type="text" class="form-control" name="plan" placeholder="Contrato" value="<?php echo $choqueObj->plan ?>">
                          </div>
                      </div>
                  </div>
                    <div class="form-group">
                      <div class="row">
                           <strong><span class="help-block m-b-none text-danger">Lugar del choque</span></strong>
                          <div class="col-sm-12"><input id="lugarChoque" type="text" class="form-control" name="lugar" placeholder="Lugar del Robo" value="<?php echo $choqueObj->lugar ?>">
                          </div>
                      </div>
                  </div>
                  
              </div>
              <div class="col-md-6">
                
                  <div class="form-group">
                      <div class="row">
                           <strong><span class="help-block m-b-none text-danger">Hora y fecha del choque</span></strong>
                          <div class="col-sm-12"><input id="horaRoboH" type="text" class="form-control" name="fechaChoque" placeholder="Hora y fecha del robo" value="<?php echo $choqueObj->fechaChoque ?>">
                          </div>
                      </div>
                  </div>
                    <div class="form-group">
                      <div class="row">
                           <strong><span class="help-block m-b-none text-danger">Compañia de seguros</span></strong>
                          <div class="col-sm-12"><input type="text" class="form-control" name="aseguradora" placeholder="Compañia de seguros" value="<?php echo $choqueObj->aseguradora ?>" >
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="row">
                          <strong><span class="help-block m-b-none text-danger">Número de póliza</span></strong> 
                          <div class="col-sm-12"><input type="text" class="form-control" name="numeroPoliza" placeholder="Número de póliza" value="<?php echo $choqueObj->numeroPoliza ?>">
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="row">
                          <strong><span class="help-block m-b-none text-danger">Número de siniestro aseguradora</span></strong>
                          <div class="col-sm-12"><input id="horaRoboH" type="text" class="form-control" name="numeroSiniestroAseguradora" placeholder="Numero de siniestro" value="<?php echo $choqueObj->numeroSiniestroAseguradora ?>"> 
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="row">
          <div class="col-md-12">
          <h2 class="text-center">Estado</h2>

            <div class="col-md-3">
                          <label for="asistiendo">Asistiendo</label>
                          <div class="switch">
                            <div class="onoffswitch">
                              <input type="radio" class="onoffswitch-checkbox" id="asistiendo" value="11" name="estado"  <?php echo $retVal = ($choqueObj->estado==11) ? "checked" : "" ; ?>  >
                              <label class="onoffswitch-label" for="asistiendo">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                            </div>
                          </div>
                        </div>

                  <div class="col-md-3">
                          <label for="asistido">Asistido</label>
                          <div class="switch">
                            <div class="onoffswitch">
                              <input type="radio" class="onoffswitch-checkbox" id="asistido" value="12" name="estado" <?php echo $retVal = ($choqueObj->estado==12) ? "checked" : "" ; ?> >
                              <label class="onoffswitch-label" for="asistido">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                            </div>
                          </div>
                        </div>

                  <div class="col-md-3">
                          <label for="noReportado">No reportado</label>
                          <div class="switch">
                            <div class="onoffswitch">
                              <input type="radio" class="onoffswitch-checkbox" id="noReportado" value="90" name="estado" <?php echo $retVal = ($choqueObj->estado==90) ? "checked" : "" ; ?> >
                              <label class="onoffswitch-label" for="noReportado">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                            </div>
                          </div>
                        </div>

                  <div class="col-md-3">
                          <label for="falsoCrash">Falso crash</label>
                          <div class="switch">
                            <div class="onoffswitch">
                              <input type="radio" class="onoffswitch-checkbox" id="falsoCrash" value="79" name="estado" <?php echo $retVal = ($choqueObj->estado==79) ? "checked" : "" ; ?> >
                              <label class="onoffswitch-label" for="falsoCrash">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                              </label>
                            </div>
                          </div>
                        </div>

            
          </div>  
          </div>
          <br>
          <div class="row">
                <div class="col-md-3 ">
                  <a href="../phpWord/choqueWord?id=<?php echo $id  ?>" class="btn btn-primary">Obtener reporte en Word</a>
                </div> 
                <div class="col-md-3">
                  <a href="detalleChoque?id=<?php echo $id  ?>" class="btn btn-info">Visualizar</a> 
                </div> 
          </div>
<input type="hidden" name='ant' value="<?= print_r($choqueObj) ?>" >
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
          <button type="submit" name="actualizar" class="btn btn-primary">Guardar cambios</button>

      </form>
       

    </div>


                                                </div>
                                            </div>
                                        </div>
    <!-- finde moda -->



<!-- Mainly scripts -->
<script src="../../assets/js/jquery-2.1.1.js"></script>
<script src="../../assets/js/bootstrap.min.js"></script>
<script src="../../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../../assets/js/inspinia.js"></script>
<script src="../../assets/js/plugins/pace/pace.min.js"></script>

<script src="../../assets/build/jquery.datetimepicker.full.js"></script>

<?php include_once('../../includes/script.php') ?> 
<script type="text/javascript">
function confirm_envio() {
  return confirm('estas seguro ?');
}
</script>
<script>
<?php $fechaA=date('Y-m-d H:00:00') ?>
     var eventos={}
    eventos.frejcar =function(){
        $.ajax({
            url: '../ajax/seguimientoChoque',
                type: 'POST', // Send post data
                data: 'idChoque=<?php echo $id ?>',
                async: true,
                success: function(data){
                    $('#scrollSeguimiento').html(data);
                }
                // error: function(e){                     
                //     // revertFunc();
                //     alert('Error processing your request: '+e.responseText);
                // }  
            });
    }
    eventos.interval = setInterval(eventos.frejcar, 4000);
    eventos.frejcar();

    eventos.throwMessage = function(message) {
    if ($.trim(message.length != 0)) {
        $.ajax({
            url: '../ajax/seguimientoChoque',
            type: 'post',
            data: {method: 'throw', message: message, idChoque: <?php echo $id ?>},
            success: function(data) {
                eventos.frejcar();
                eventos.entry.val('');
            }
        }

        );
    }
}

eventos.entry = $('#apuntesRegistrar');
eventos.entry.bind('keydown', function(e) {
    if (e.keyCode === 13 && e.shiftKey === false) {
      // alert("fun")  ;
        eventos.throwMessage($(this).val());
        e.preventDefault();
    }

});
</script>
</body>
</html>

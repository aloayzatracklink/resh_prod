<?php
$nav = "gestionAccesos";
?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("../ingresar");
} ?>
<?php $obMenu = Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->chequeos == "si"): ?>
    <!--gestionDeAccesos se movió a gestionAccesosRegistro-->
    <style>
        .swal2-container {
            z-index: 2080 !important;
        }
    </style>

    <body>

        <link rel="stylesheet" href="../assets/Datatable/CSS/jquery.dataTables.css">
        <link rel="stylesheet" href="../assets/Datatable/CSS/buttons.dataTables.min.css">

        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <?php include("../includes/menuIzquierdo.php") ?>
                </div>
            </nav>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <?php include("../includes/navegacionArriba.php") ?>
                    </nav>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins col-lg-12">
                                <div class="ibox-title">
                                    <h5>Accesos de Clientes</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="col-md-8"></div>
                                    <div class="col-md-2">
                                        <button class="btn btn-sm btn-warning" data-toggle="modal"
                                            data-target="#editarTecnicos" data-id="0" data-tipo="save">Registrar Acceso <i
                                                class="fa fa-user"></i></button>
                                    </div>
                                    <!-- <div class="col-md-2">
                                        <form action="PHPExcel/AccesoExcel" method="POST">
                                            <div class="col-md-2" id="resultado1">
                                                <button class="btn btn-primary" name="Descargar" id="resultado1">Descargar
                                                    Excel</button>
                                            </div>
                                        </form>
                                    </div> -->
                                    <!-- <input type="text" class="form-control input-sm m-b-xs" id="filter"
                                        placeholder="Filtrar "> -->
                                    <table id="tablaGestionA" class="table table-stripped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Financiera</th>
                                                <th>Cliente</th>
                                                <th>Chasis</th>
                                                <th>Placa</th>
                                                <th>Usuario</th>
                                                <th>Clave</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php include("../includes/footer.php") ?>
                </div>
            </div>

            <div class="modal inmodal" id="editarTecnicos" tabindex="-1" role="dialog" aria-hidden="true"
                data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content animated bounceIn">
                        <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                            <button type="button" class="close" data-dismiss="modal"><span
                                    aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <form role="form" id="editarTecnicosform">
                            <div class="modal-body" style="background: #fff;padding-top: 0"></div>
                            <div class="modal-footer">

                                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                <!-- Agrega loading -->
                                <div id="loading"
                                    style="display: none; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); z-index: 1000;">
                                    <img src="loadingAccesos.gif" alt="Cargando..." width="50" height="50">
                                </div>

                                <button type="submit" id="guardarButton" name="guardarCambios" class="btn btn-primary"
                                    value="<?php echo Token::generar() ?>">Guardar</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="../assets/js/jquery-2.1.1.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="../assets/js/plugins/clockpicker/clockpicker.js"></script>
        <script src="../assets/js/inspinia.js"></script>
        <script src="../assets/js/plugins/pace/pace.min.js"></script>
        <script src="../assets/js/plugins/toastr/toastr.min.js"></script>
        <script>
            $('#editarTecnicos').on('show.bs.modal', function (e) {
                var rowid = $(e.relatedTarget).data('id');
                var tipo = $(e.relatedTarget).data('tipo');

                $(".modal-body").html("");
                $.ajax({
                    type: 'post',
                    url: 'modal/editarDataModalAccesos',
                    data: 'rowid=' + rowid + "&tipoOpera=" + tipo,
                    success: function (data) {
                        $('.modal-body').html(data);
                    }
                });
            });
        </script>
        <script src="../assets/Datatable/JS/jquery.dataTables.js"></script>
        <script src="../assets/Datatable/JS/dataTables.buttons.min.js"></script>
        <script src="../assets/Datatable/JS/ajax_libs_jszip_3.1.3_jszip.min.js"></script>
        <script src="../assets/Datatable/JS/buttons.html5.min.js"></script>
        <script src="../assets/Datatable/JS/buttons.print.min.js"></script>
        <script src="../assets/Datatable/JS/pdfmake.min.js"></script>
        <script src="../assets/Datatable/JS/vfs_fonts.js"></script>
        <script src="../assets/sweetalertsV11.9.0/sweetalert2@11.js"></script>
        <script>
            let tablaAccesos;
            $(document).ready(function () {
                tablaAccesos = $('#tablaGestionA').DataTable({
                    ajax: {
                        url: `gestionAccesosTablasAjax`,
                        dataSrc: ""
                    },
                    language: {
                        url: "../assets/Datatable/json/Spanish.json",
                    },
                    columns: [
                        { data: "idaccfi" },
                        { data: "financiera" },
                        { data: "cliente" },
                        { data: "chasis" },
                        { data: "placa" },
                        { data: "usuario" },
                        { data: "contrasenia" },
                        {
                            data: null,
                            render: function (data, type, row, meta) {
                                return '<button class="btn btn-primary" style="padding-top: 0;padding-bottom: 0" data-toggle="modal" data-target="#editarTecnicos" data-id="' + row.idaccfi + '" data-tipo="edit"><i class="fa fa-edit"></i></button>';
                            }
                        }
                    ],

                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "excel",
                            text: "Descargar Excel",
                            className: 'btn-excel',
                            customizeData: function (data) {
                                for (var i = 0; i < data.body.length; i++) {
                                    // Agregar un número de fila en la primera celda de cada fila
                                    data.body[i][0] = i + 1;
                                }
                            },
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6],
                            },

                            filename: function () {
                                var d = new Date();
                                var year = d.getFullYear();
                                var month = ('0' + (d.getMonth() + 1)).slice(-2);
                                var day = ('0' + d.getDate()).slice(-2);
                                var hour = ('0' + d.getHours()).slice(-2);
                                var minute = ('0' + d.getMinutes()).slice(-2);
                                var second = ('0' + d.getSeconds()).slice(-2);
                                return 'GestionAccesos-' + year + '-' + month + '-' + day + '-' + hour + '-' + minute + '-' + second;
                            }
                        },

                        {
                            text: 'Actualizar',
                            action: function (e, dt, node, config) {
                                dt.ajax.reload();
                            }
                        },
                    ],
                    pageLength: 15,
                    order: [[0, "desc"]],


                });
            });

            function envCred() {

                // Agrega el identificador "loading" a un div de carga
                const loadingDiv = document.getElementById('loading');

                // Muestra el lading de carga
                loadingDiv.style.display = 'block';

                let chasis = document.getElementById('nchasis').value;
                let usu = document.getElementById('nusuario').value;
                let contra = document.getElementById('ncontrasenia').value;

                // console.log(chasis);
                // console.log(usu);
                // console.log(contra);

                if (chasis == '' || usu == '' || contra == '') {

                    // Oculta el lading de carga en caso de error
                    loadingDiv.style.display = 'none';

                    swal.fire('error', 'Debe completar todos los datos');
                    return false;
                }

                const data = new FormData();
                data.append('txtchasis', chasis);
                data.append('txtusu', usu);
                data.append('txtcontra', contra);

                console.log(data);

                fetch("gestionAccesosCorreoAjax", {
                    method: 'post',
                    body: data
                }).then((res) => res.json())
                    .catch((error) => {
                        // Oculta el lading de carga en caso de error en la solicitud xd
                        loadingDiv.style.display = 'none';

                        console.error("Error:", error);

                    })
                    .then((response) => {

                        // Oculta el loading de carga cuando la solicitud se completa:D
                        loadingDiv.style.display = 'none';

                        if (!response.status) {
                            Swal.fire({
                                icon: 'error',
                                title: response.response,
                                //text: 'Envío de correo',
                            });

                            //quitar carga y habilitar botón

                            return false;
                        }

                        Swal.fire({
                            icon: 'success',
                            title: '¡MUY BIEN!',
                            text: response.response,

                            confirmButtonText: 'OK',

                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                window.location.reload();
                            }
                        });
                        console.log(response);

                    }
                    )
            }


            const miformulario = document.getElementById("editarTecnicosform");
            miformulario.addEventListener("submit", (e) => {
                e.preventDefault();

                // Muestra el loading de carga al iniciar la solicitud
                document.getElementById('loading').style.display = 'block';

                const data = new FormData(miformulario);

                fetch("gestionAccesosRegistro", {
                    method: 'post',
                    body: data
                }).then((res) => res.json())
                    .catch((error) => {
                        // Oculta el lading de carga en caso de error en la solicitud xd
                        document.getElementById('loading').style.display = 'none';
                        console.error("Error:", error);
                    })
                    .then((response) => {
                        // Oculta el loading de carga cuando la solicitud se completa:D
                        document.getElementById('loading').style.display = 'none';
                        if (!response.status) {
                            Swal.fire({
                                icon: 'error',
                                title: response.response,
                                //text: 'Oops...',
                            });

                            //quitar carga y habilitar botón

                            return false;
                        }

                        Swal.fire({
                            icon: 'success',
                            title: '¡MUY BIEN!',
                            text: response.response,

                            confirmButtonText: 'OK',

                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                if (response.tipo == 'save') {
                                    window.location.reload();
                                }
                                tablaAccesos.ajax.reload();


                            }
                        });


                    }
                    );

            });

        </script>
    <?php else: ?>
        <?php $session->salir(); ?>
        <?php echo "NO NO NO NO ; presiona F5" ?>
    <?php endif ?>
<?php $nav = "clienteMTC"; ?>
<?php include("../includes/head.php") ?>
<?php if (!$session->yaEstaIngresado()) {
    irA("ingresar");
}
?>

<?php $obMenu = Usuario::obtenerMenu($session->idusuario) ?>

<body>
    <link href="../assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
    <link href="../assets/calendario/fullcalendar.min.css" rel="stylesheet">
    <link href="../assets/calendario/fullcalendar.print.min.css" rel='stylesheet' media='print'>
    <link href='../assets/calendario/scheduler.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="../assets/css/jquery-confirm.min.css">
    <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/utilities.css">
    <link rel="stylesheet" href="../assets/Datatable/CSS/jquery.dataTables.css">
    <link rel="stylesheet" href="../assets/Datatable/CSS/buttons.dataTables.min.css">

    <!--//860ryp0nz-EZ-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" />




    <div id="wrapper">
        <!-- Menu -->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <?php include("../includes/menuIzquierdo.php"); ?>
            </div>
        </nav>

        <!-- Main content -->
        <div id="page-wrapper" class="gray-bg">
            <!-- Menu responsive -->
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>
                </nav>
            </div>

            <section class="row p-2">
                <div class="col-md-12 white-bg p-2">
                    <h3>Cliente MTC</h3>

                    <style>
                        .btn-shadow {
                            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
                        }

                        .btn-medium {
                            padding: 8px 16px;
                            font-size: 16px;
                            font-weight: bold;
                            border-radius: 20px;
                        }

                        .button-container {
                            text-align: right;
                            margin-top: -20px;
                            margin-right: 10px;
                            /* Ajusta el margen derecho según sea necesario */
                        }

                        .widget p {
                            display: inline-block;
                        }

                        .fecha {
                            font-family: Helvetica, Arial;
                            text-align: center;
                            font-size: 14px;
                        }

                        .reloj {
                            font-family: Helvetica, Arial;
                            font-size: 23px;
                            text-align: center;
                            margin-top: 6px;
                        }

                        .reloj .caja-segundos {
                            display: inline-block;
                        }

                        .reloj .segundos,
                        .reloj .ampm {
                            font-size: 10px;
                            display: block;
                        }

                        .no-js #loader {
                            display: none;
                        }

                        .js #loader {
                            display: block;
                            position: absolute;
                            left: 100px;
                            top: 0;
                        }

                        .se-pre-con {
                            position: fixed;
                            left: 0px;
                            top: 0px;
                            width: 100%;
                            height: 100%;
                            z-index: 9999;
                            background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
                        }

                        .pagination1 {
                            margin: 0;
                            padding: 0;
                        }

                        .pagination1 li {
                            display: inline;
                            padding: 6px 10px 6px 10px;
                            border: 1px solid #ddd;
                            margin-right: -1px;
                            font: 15px/20px Arial, Helvetica, sans-serif;
                            background: #FFFFFF;
                            box-shadow: inset 1px 1px 5px #F4F4F4;
                        }

                        .pagination1 li a {
                            text-decoration: none;
                            color: rgb(89, 141, 235);
                        }

                        .pagination1 li.first {
                            border-radius: 5px 0px 0px 5px;
                        }

                        .pagination1 li.last {
                            border-radius: 0px 5px 5px 0px;
                        }

                        .pagination1 li:hover {
                            background: #CFF;
                        }

                        .pagination1 li.active {
                            background: #F0F0F0;
                            color: #333;
                        }

                        .button-container {
                            display: flex;
                            justify-content: flex-end;
                            gap: 10px;
                            /* Espacio entre los botones */
                        }

                        .btn {
                            border-radius: 20px;
                            /* Bordes redondos */
                            box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.3);
                            /* Sombra */
                            padding: 10px 20px;
                        }

                        /*//860ryp0nz-EZ */
                        .modal {
                            z-index: 1050 !important;
                        }

                        .modal-backdrop {
                            z-index: 1040 !important;
                        }
                    </style>

                    <div class="button-container">
                        <!--860ryp0nz-EZ-->
                        <!-- <button class="btn btn-primary btn-shadow btn-medium" onclick="location.reload()">
                            <i class="fas fa-sync-alt"></i> Actualizar
                        </button> -->

                        <!-- <form action="descargar_consulta" method="POST">
                            <div class="col-md-2" id="resultado1">
                                <button class="btn btn-primary btn-shadow btn-medium" name="DescargarMTC">
                                    Descargar
                                </button>
                            </div>
                        </form> -->

                        <!--//860ryp0nz-EZ-->
                        <button id="btnAbrirModalListaNegra" class="btn btn-primary">Ver Lista Negra</button>
                    </div>

                    <!-- Modal para mostrar la Lista Negra MTC -->
                    <div class="modal fade" id="modalListaNegra" tabindex="-1" role="dialog"
                        aria-labelledby="modalListaNegraLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalListaNegraLabel">Lista Negra MTC</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table id="tablaListaNegra" class="display" style="width:100%">
                                        <thead>
                                            <th>#</th>
                                            <th>Documento del cliente</th>
                                            <th>Plan</th>
                                            <th>Chasis</th>
                                            <th>Placa</th>
                                            <th>Marca</th>
                                            <th>Modelo</th>
                                            <th>Motivo de depuración</th>
                                            <th>Fecha de ultima actualizacion</th>
                                            <th>Regresar</th>
                                        </thead>
                                        <tbody id="resultadoModal">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="modal fade" id="modalListaNegra" tabindex="-1" role="dialog" aria-labelledby="modalListaNegraLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document" style="width: 90%;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalListaNegraLabel" style="font-size: 200%; text-align: center;">Lista negra MTC</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body" style="padding: 1rem;">
                                <table id="tablaListaNegra" class="display" style="width: 100%;">
                                    <thead>
                                        <th>#</th>
                                        <th>Documento del cliente</th>
                                        <th>Plan</th>
                                        <th>Chasis</th>
                                        <th>Placa</th>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Motivo de depuración</th>
                                        <th>Fecha de ultima actualizacion</th>
                                        <th>Regresar</th>
                                    </thead>
                                    <tbody id="resultadoModal">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> -->



            </section>
            <section class="row" style="margin-bottom: 2rem;">
                <div class="col-md-12 table bg-white">
                    <!--860ryp0nz-EZ-->
                    <table id="MyTable" class="table table-condensed table-hover p-1">
                        <thead>
                            <th>#</th>
                            <th>Documento del cliente</th>
                            <th>Plan</th>
                            <th>Chasis</th>
                            <th>Placa</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Estado</th>
                            <th>Fecha de ultima actualizacion</th>
                            <th>Depurar</th> <!--//860ryp0nz-EZ-->
                        </thead>
                        <tbody id="resultado">
                        </tbody>
                    </table>
                    <!-- <p id="buscando" class="text-center"><strong>Buscando resultados ...</strong></p> -->
                </div>
            </section>
            <div class="row">
                <?php include("../includes/footer.php") ?>
            </div>
        </div>
    </div>


    <?php //include("../includes/footer.php") ?>
    <script src="../assets/js/jquery-2.1.1.js"></script> <!-- jQuery debe cargarse primero -->
    <script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../assets/js/plugins/pace/pace.min.js"></script>
    <script src="../assets/js/plugins/footable/footable.all.min.js"></script>
    <script src="../assets/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
    <script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="../assets/js/plugins/iCheck/icheck.min.js"></script>
    <script src="../assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="../assets/js/clipboard.min.js"></script>
    <script src="../assets/js/plugins/toastr/toastr.min.js"></script>
    <script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
    <script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="../assets/Datatable/JS/jquery.dataTables.js"></script>
    <script src="../assets/Datatable/JS/dataTables.buttons.min.js"></script>
    <script src="../assets/Datatable/JS/ajax_libs_jszip_3.1.3_jszip.min.js"></script>
    <script src="../assets/Datatable/JS/buttons.html5.min.js"></script>
    <script src="../assets/Datatable/JS/buttons.print.min.js"></script>
    <script src="../assets/Datatable/JS/pdfmake.min.js"></script>
    <script src="../assets/Datatable/JS/vfs_fonts.js"></script>
    <script src="../assets/sweetalertsV11.9.0/sweetalert2@11.js"></script> <!--//860ryp0nz-EZ-->
    <script src="../assets/js/bootstrap.min.js"></script> <!-- Bootstrap se carga después de jQuery -->




    <?php include_once('../includes/script.php') ?>
    <script>
        //860ryp0nz-EZ
        let tablaMTC;
        $(document).ready(function () {

            tablaMTC = $("#MyTable").DataTable({
                processing: true,
                ajax: {
                    url: `clienteMTCTablasAjax`,
                    dataSrc: "",
                },
                language: {
                    url: "../assets/Datatable/json/Spanish.json",
                },
                columns: [
                    {
                        "searchable": false,
                        "orderable": false,
                        "render": function (data, type, full, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1; // Esto establece la numeración basada en la visualización actual
                        }
                    },
                    { data: "identdoc" },
                    { data: "descarticle" },
                    { data: "idchasis" },
                    { data: "license" },
                    { data: "descbrand" },
                    { data: "descmodel" },
                    {
                        data: "statcontract",
                        render: function (data, type, row, meta) {
                            if (row.statcontract == "P") {
                                return '<span class="badge badge-warning">Está pendiente</span>';
                            }
                        }
                    },


                    {
                        data: "formatted_date",
                        render: function (data) {
                            var date = new Date(data);
                            var year = date.getFullYear();
                            var month = ('0' + (date.getMonth() + 1)).slice(-2);
                            var day = ('0' + date.getDate()).slice(-2);
                            return year + '-' + month + '-' + day;
                        }
                    },

                    {
                        data: null,
                        render: function (data, type, row, meta) {
                            return `<button class='depurar-button btn btn-danger' style='padding-top: 0; padding-bottom: 0' data-idcustomer='${row.idcustomer}' data-idvehicle='${row.idvehicle}' data-license='${row.license}' data-chasis='${row.idchasis}' data-action='insertar'><i class="fa-solid fa-trash-can"></i></button>`;
                        },
                    }



                ],
                dom: "Bfrtip",
                buttons: [
                    {
                        extend: "excel",
                        text: "Descargar Excel",
                        className: 'btn-excel',
                        customizeData: function (data) {
                            for (var i = 0; i < data.body.length; i++) {
                                // Agregar un número de fila en la primera celda de cada fila
                                data.body[i][0] = i + 1;
                            }
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8],
                        },

                        filename: function () {
                            var d = new Date();
                            var year = d.getFullYear();
                            var month = ('0' + (d.getMonth() + 1)).slice(-2);
                            var day = ('0' + d.getDate()).slice(-2);
                            var hour = ('0' + d.getHours()).slice(-2);
                            var minute = ('0' + d.getMinutes()).slice(-2);
                            var second = ('0' + d.getSeconds()).slice(-2);
                            return 'CLIENTEMTC-' + year + '-' + month + '-' + day + '-' + hour + '-' + minute + '-' + second;
                        }
                    },

                    {
                        text: 'Actualizar',
                        action: function (e, dt, node, config) {
                            dt.ajax.reload(function () {
                                var PageInfo = $('#MyTable').DataTable().page.info();
                                dt.column(0, { page: 'current' }).nodes().each(function (cell, i) {
                                    cell.innerHTML = i + 1 + PageInfo.start;
                                });
                            });
                        }
                    },

                    // {
                    // text: 'Ver lista negra',
                    // action: function ( e, dt, node, config ) {
                    //     return `<button class='btn btn-primary' style='padding-top: 0; padding-bottom: 0' data-toggle='modal' data-target='#bateriasOModal'><i class='fa fa-cog'></i></button>`;
                    // }
                    // },
                ],
                pageLength: 15,
                order: [[8, "desc"]],
            });

            tablaListaNegra = $('#tablaListaNegra').DataTable({
                // Configuración del DataTable
                ajax: {
                    url: 'ClienteMtcVerListaNegraAjax', // Ruta al archivo que obtiene los datos
                    dataSrc: ''
                },
                language: {
                    url: "../assets/Datatable/json/Spanish.json",
                },
                columns: [
                    {
                        "searchable": false,
                        "orderable": false,
                        "render": function (data, type, full, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1; // Esto establece la numeración basada en la visualización actual
                        }
                    },
                    { data: "identdoc" },
                    { data: "descarticle" },
                    { data: "idchasis" },
                    { data: "license" },
                    { data: "descbrand" },
                    { data: "descmodel" },
                    {
                        data: "motivoBL", // Asegúrate de tener esta columna en tu fuente de datos
                        render: function (data) {
                            return data; // Puedes personalizar la representación si es necesario
                        }
                    },

                    {
                        data: "formatted_date",
                        render: function (data) {
                            var date = new Date(data);
                            var year = date.getFullYear();
                            var month = ('0' + (date.getMonth() + 1)).slice(-2);
                            var day = ('0' + date.getDate()).slice(-2);
                            return year + '-' + month + '-' + day;
                        }
                    },

                    {
                        data: null,
                        render: function (data, type, row, meta) {
                            return `<button class='depurar-button btn btn-primary' style='padding-top: 0; padding-bottom: 0' data-idcustomer='${row.idcustomer}' data-idvehicle='${row.idvehicle}' data-license='${row.license}' data-chasis='${row.idchasis}' data-action='eliminar'><i class="fa-solid fa-recycle fa-fade"></i></button>`;
                        },
                    }

                ],
                dom: "Bfrtip",
                buttons: [
                    {
                        extend: "excel",
                        text: "Descargar Excel",
                        className: 'btn-excel',
                        customizeData: function (data) {
                            for (var i = 0; i < data.body.length; i++) {
                                // Agregar un número de fila en la primera celda de cada fila
                                data.body[i][0] = i + 1;
                            }
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                        },

                        filename: function () {
                            var d = new Date();
                            var year = d.getFullYear();
                            var month = ('0' + (d.getMonth() + 1)).slice(-2);
                            var day = ('0' + d.getDate()).slice(-2);
                            var hour = ('0' + d.getHours()).slice(-2);
                            var minute = ('0' + d.getMinutes()).slice(-2);
                            var second = ('0' + d.getSeconds()).slice(-2);
                            return 'LISTA_NEGRA-' + year + '-' + month + '-' + day + '-' + hour + '-' + minute + '-' + second;
                        }
                    },

                    {
                        text: 'Actualizar',
                        action: function (e, dt, node, config) {
                            dt.ajax.reload(function () {
                                var PageInfo = $('#MyTable').DataTable().page.info();
                                dt.column(0, { page: 'current' }).nodes().each(function (cell, i) {
                                    cell.innerHTML = i + 1 + PageInfo.start;
                                });
                            });
                        }
                    },

                ],
                pageLength: 10,
                order: [[8, "desc"]],
            });

            tablaMTC.on('order.dt search.dt', function () {
                tablaMTC.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                    // Esto asegura que la columna de numeración se actualice correctamente sin afectar el ordenamiento de otras columnas
                });
            }).draw();

        });
        // function actualizarDataTable() {
        //     tablaMTC.ajax.reload();
        // }

        //let motivo;
        $('#MyTable tbody').on('click', '.depurar-button', function () {
            let idcustomer = $(this).data('idcustomer');
            let idvehicle = $(this).data('idvehicle');
            let placa = $(this).data('license');
            let chasis = $(this).data('chasis');
            let accion = $(this).data('action'); //data-action <-Ese 
            let motivo;
            //console.log(idcustomer,idvehicle);

            // Mostrar una alerta
            Swal.fire({
                title: `¿Enviar vehículo con placa ${placa} y chasis ${chasis} a LISTA NEGRA?`,
                icon: "warning",
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Sí",
                cancelButtonText: "No",
                denyButtonText: `No`,
                // html: '<tex type="text" id="motivoInput" name="motivoInput" class="swal2-input" placeholder="Motivo" required>',
                html: '<textarea id="motivoInput" name="motivoInput" rows="10" cols="40" class="swal2-input" placeholder="Motivo" required style="min-height:10vh;"></textarea>',
                preConfirm: function () {
                    // const motivo = $('#motivoInput').val();
                    motivo = $('[name="motivoInput"]').val();
                    if (!motivo) {
                        Swal.showValidationMessage('Por favor, ingrese un motivo');
                    }
                    return motivo;
                }

            }).then((result) => {
                if (result.isConfirmed) {
                    console.log(motivo);
                    // Realizar una solicitud AJAX para llamar a la función en el backend
                    $.ajax({
                        url: 'clienteMtcDepurarTablasAjax',
                        type: 'POST',
                        data: {
                            //El primero se envía _Este___ : _____,
                            action: accion,
                            idcustomer: idcustomer,
                            idvehicle: idvehicle,
                            motivo: motivo, // Enviar el motivo al servidor
                        },
                        success: function (response) {
                            // Manejar la respuesta del servidor si es necesario
                            console.log(response);
                            Swal.fire("Enviado a la lista negra", "", "success");
                            //actualizarDataTable();
                            tablaMTC.ajax.reload();
                        }
                    });

                } else if (result.isDenied) {
                    Swal.fire("El vehículo no se envió a la lista negra", "", "info");
                }
            });
        });

        let tablaListaNegra;
        // $(document).ready(function () {
        // Configurar el DataTable para la Lista Negra

        function actualizarDataTableListaNegra() {
            tablaListaNegra.ajax.reload();
        }

        $('#tablaListaNegra tbody').on('click', '.depurar-button', function () {
            let idcustomer = $(this).data('idcustomer');
            let idvehicle = $(this).data('idvehicle');
            let placa = $(this).data('license');
            let chasis = $(this).data('chasis');
            let accion = $(this).data('action'); //data-action <-Ese 
            //console.log(idcustomer,idvehicle);

            // Mostrar una alerta
            Swal.fire({
                title: `¿Regresar vehículo con placa ${placa} y chasis ${chasis} a LISTA MTC?`,
                icon: "warning",
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Sí",
                cancelButtonText: "No",
                denyButtonText: `No`
            }).then((result) => {
                if (result.isConfirmed) {
                    // Realizar una solicitud AJAX para llamar a la función en el backend
                    $.ajax({
                        url: 'clienteMtcDepurarTablasAjax',
                        type: 'POST',
                        data: {
                            action: accion,
                            idcustomer: idcustomer,
                            idvehicle: idvehicle
                        },
                        success: function (response) {
                            // Manejar la respuesta del servidor si es necesario
                            console.log(response);
                        }
                    });

                    Swal.fire("Enviado a la lista Clientes MTC", "", "success");
                    //actualizarDataTableListaNegra();
                    tablaListaNegra.ajax.reload();
                    tablaMTC.ajax.reload();
                } else if (result.isDenied) {
                    Swal.fire("El vehículo no se envió a la lista Clientes MTC", "", "info");
                }
            });
        });



        //})


        $('#btnAbrirModalListaNegra').click(function () {

            $('#modalListaNegra').modal('show');

            tablaListaNegra.ajax.reload();


            tablaListaNegra.on('order.dt search.dt', function () {
                tablaListaNegra.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                    // Esto asegura que la columna de numeración se actualice correctamente sin afectar el ordenamiento de otras columnas
                });
            }).draw();
        });

    </script>
</body>

</html>
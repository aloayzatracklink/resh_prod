
<?php include("../../includes/init.php") ?>
<?php 


  //   header('Expires: 0');
  // header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  //   header('Pragma: public');

if (isset($_GET['id'])) {
        $id=$_GET['id'];
        $roboObj=new Choque;
        $choqueDetalleObj=$roboObj->obtenerDatosDeChoquePorId($id);

        $clienteObj=Onix::ObtenerDatosDesdeChasis($choqueDetalleObj->chasis);
        $apellidos=utf8_encode($clienteObj->apellidos); $propietario = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ;
        $nombre='REPORTE DE ATENCION DE EMERGENCIA DE CLIENTE '.$propietario.' PLACA  '.$clienteObj->placa;

    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment;filename='.$nombre.'.docx');
    }
    if (empty($id)) {
        echo "<a href='../ingresar'>fail detected <h1>presione aqui</h1></a>";
     die();   # code...
}
?>

<HTML>
<HEAD>
<META HTTP-EQUIV='CONTENT-TYPE' CONTENT='text/html; charset=utf-8'>
<TITLE></TITLE>
<META NAME='GENERATOR' CONTENT='LibreOffice 4.1.6.2 (Linux)'>

<STYLE TYPE='text/css'>
    <!--
    @page { margin-left: 1.18in; margin-right: 1.18in }
    P { margin-bottom: 0.08in; direction: ltr; line-height: 100%; widows: 2; orphans: 2 }
    P.western { font-family: 'Times New Roman', serif; font-size: 12pt; so-language: es-ES }
    P.cjk { font-family: 'Times New Roman'; font-size: 12pt; so-language: es-ES }
    P.ctl { font-family: 'Times New Roman'; font-size: 12pt }
    H1 { margin-top: 0.33in; margin-bottom: 0in; direction: ltr; color: #365f91; line-height: 100%; page-break-inside: avoid; widows: 2; orphans: 2 }
    H1.western { font-family: 'Cambria', serif; font-size: 14pt; so-language: es-ES }
    H1.cjk { font-family: 'Times New Roman'; font-size: 14pt; so-language: es-ES }
    H1.ctl { font-family: ; font-size: 14pt }
    A:link { color: #0000ff; so-language: zxx }
-->
</STYLE>
</HEAD>
<BODY LANG='es-PE' LINK='#0000ff' DIR='LTR'>

<H1 LANG='es-ES' CLASS='western'>REPORTE DE ATENCION DE EMERGENCIA</H1>
<P LANG='es-ES' CLASS='western' STYLE='margin-bottom: 0in'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><B>Dirigido
    a:   TRACKLINK PERÚ</B></FONT></FONT></P>
    <P LANG='es-ES' CLASS='western' STYLE='margin-bottom: 0in'><BR>
    </P>
    <TABLE WIDTH=595 CELLPADDING=5 CELLSPACING=0>
        <COL WIDTH=260>
            <COL WIDTH=61>
                <COL WIDTH=91>
                    <COL WIDTH=46>
                        <COL WIDTH=86>
                            <TR>
                                <TD COLSPAN=5 WIDTH=583 HEIGHT=8 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Datos del Vehículo: </B></SPAN></FONT></FONT></FONT>
                                    </P>
                                </TD>
                            </TR>
                            <TR>
                                <TD WIDTH=260 HEIGHT=21 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Marca del Vehículo: <?php echo $clienteObj->marca ?></B></SPAN></FONT></FONT></FONT></P>
                                    </TD>
                                    <TD WIDTH=61 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Modelo:</B></SPAN></FONT></FONT></FONT>
                                    </P>
                                </TD>
                                <TD COLSPAN=3 WIDTH=243 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo $clienteObj->modelo ?></B></SPAN></FONT></FONT></FONT></P>
                                </TD>
                            </TR>
                            <TR>
                                <TD WIDTH=260 HEIGHT=25 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Placa: <?php echo $clienteObj->placa ?></B></SPAN></FONT></FONT></FONT></P>
                                    </TD>
                                    <TD WIDTH=61 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Color:</B></SPAN></FONT></FONT></FONT></P>
                                    </TD>
                                    <TD COLSPAN=3 WIDTH=243 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo $clienteObj->color ?></B></SPAN></FONT></FONT></FONT></P>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                            <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Chasis:</B></SPAN></FONT></FONT></FONT>
                                                <FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo $clienteObj->chasis ?></B></SPAN></FONT></FONT></FONT></P>
                                            </TD>
                                            <TD WIDTH=61 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Año:
                                                </B></SPAN></FONT></FONT></FONT>
                                            </P>
                                        </TD>
                                        <TD COLSPAN=3 WIDTH=243 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                            <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo $clienteObj->anio ?></B></SPAN></FONT></FONT></FONT></P>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD WIDTH=260 HEIGHT=11 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                            <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Propietario:
                                            </B></SPAN></FONT></FONT></FONT>
                                        </P>
                                    </TD>
                                    <TD COLSPAN=4 WIDTH=314 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='en-US'><B><?php echo $propietario ?></B></SPAN></FONT></FONT></FONT></P>
                                    </TD>
                                </TR>
                                <TR>
                                    <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>DNI O RUC:
                                        </B></SPAN></FONT></FONT></FONT>
                                    </P>
                                </TD>
                                <TD COLSPAN=4 WIDTH=314 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo $clienteObj->dni ?></B></SPAN></FONT></FONT></FONT></P>
                                </TD>
                            </TR>
                            <TR>
                                <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Aseguradora: <?php echo $choqueDetalleObj->aseguradora ?></B></SPAN></FONT></FONT></FONT></P>
                                    </TD>
                                    <TD COLSPAN=4 WIDTH=314 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>PLAN: <?php echo $choqueDetalleObj->plan ?></B></SPAN></FONT></FONT></FONT></P>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD WIDTH=260 HEIGHT=21 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                            <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Nro. Póliza: <?php echo $choqueDetalleObj->numeroPoliza ?></B></SPAN></FONT></FONT></FONT>
                                            </P>
                                        </TD>
                                        <TD COLSPAN=4 WIDTH=314 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                            <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Nro.
                                                Siniestro: <?php echo $choqueDetalleObj->numeroSiniestroAseguradora ?></B></SPAN></FONT></FONT></FONT></P>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Fecha
                                                    y Hora del Choque: </B></SPAN></FONT></FONT></FONT>
                                                </P>
                                            </TD>
                                            <TD COLSPAN=4 WIDTH=162 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                <P LANG='es-ES' CLASS='western' ><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php
                                                    echo $choqueDetalleObj->fechaChoque;
                                                 ?></B></SPAN></FONT></FONT></FONT></P>
                                            </TD>
                                            </TR>
                                            <TR>
                                                <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Plataforma:
                                                    </B></SPAN></FONT></FONT></FONT>
                                                </P>
                                            </TD>
                                            <TD COLSPAN=4 WIDTH=314 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo $choqueDetalleObj->plataforma ?></B></SPAN></FONT></FONT></FONT></P>
                                                </TD>
                                            </TR>
                                            <TR>
                                                <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Fecha
                                                        y hora de reporte a TRACKLINK: </B></SPAN></FONT></FONT></FONT>
                                                    </P>
                                                </TD>
                                                <TD COLSPAN=4 WIDTH=162 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                    <P LANG='es-ES' CLASS='western' ><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo formato($choqueDetalleObj->fechaRegistro) ?></B></SPAN></FONT></FONT></FONT></P>
                                                </TD>
                                            </TR>
                                            <TR>
                                                <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Velocidad:</B></SPAN></FONT></FONT></FONT></P>
                                                    </TD>
                                                    <TD COLSPAN=2 WIDTH=162 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                        <P LANG='es-ES' CLASS='western' ALIGN=RIGHT><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php 
                                                        echo  $choqueDetalleObj->velocidad
                                                        ?></B></SPAN></FONT></FONT></FONT></P>
                                                    </TD>
                                                    <TD WIDTH=46 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>IMPACTO</B></SPAN></FONT></FONT></FONT></P>
                                                    </TD>
                                                    <TD WIDTH=86 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo $choqueDetalleObj->impacto ?>
                                                        </B></SPAN></FONT></FONT></FONT>
                                                    </P>
                                                </TD>
                                            </TR>
                                            <TR>
                                                <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'> <FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Lugar
                                                        de choque:</B></SPAN></FONT></FONT></FONT></P>
                                                    </TD>
                                                    <TD COLSPAN=4 WIDTH=314 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><B><?php echo $choqueDetalleObj->lugar ?></B></FONT></FONT></P>
                                                        </TD>
                                                    </TR>
                                                    <TR>
                                                        <TD COLSPAN=5 WIDTH=583 HEIGHT=15 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
<UL>
<LI><P STYLE='margin-right: 0.36in; margin-bottom: 0in'>
    <FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><FONT COLOR='#1f3864'><FONT FACE='Calibri, serif'><FONT SIZE=3><B><?php echo formato($choqueDetalleObj->fechaRegistro) ?>.</B></FONT></FONT></FONT><FONT COLOR='#1f3864'><FONT FACE='Calibri, serif'></FONT></FONT><FONT COLOR='#1f3864'><FONT FACE='Calibri, serif'><FONT SIZE=3>Se registra evento Crash en la plataforma <?php echo $choqueDetalleObj->plataforma ?> correspondiente al cliente  <strong><?php echo $choqueDetalleObj->propietario ?></strong>  con Identificacion Numero <?php echo $choqueDetalleObj->idcliente ?>, el crash se registró con una intensidad de <?php echo $retVal = (empty($choqueDetalleObj->impacto)) ? "<span class='text-danger'>No se registro</span>" : $choqueDetalleObj->impacto ; ?> en la siguiente dirección: <?php echo $retVal = (empty($choqueDetalleObj->lugar)) ? "<span class='text-danger'>No se registro</span>" : "<strong>".$choqueDetalleObj->lugar."</strong>" ; ?>
            cliente cuenta con Plan <?php echo $retVal = (empty($choqueDetalleObj->plan)) ? "<span class='text-danger'>No se registro</span>" : $choqueDetalleObj->plan ; ?>. Contrato <?php echo vence($choqueDetalleObj->vence) ?> <?php echo formatoSinHora($choqueDetalleObj->vence) ?>
    <small> <br><strong>Operador a cargo</strong> -- <cite title="" data-original-title=""><?php $usuarioAsesor= Usuario::obtenerUsuario($choqueDetalleObj->idusuario); echo $usuarioAsesor->usuarioNombre;echo "&nbsp;"; echo $usuarioAsesor->usuarioApellidoPaterno  ?></cite></small>
        </FONT></FONT></FONT></FONT></FONT></P>
        </LI>
 </UL>
 <?php $seguimientoData=SeguimientoChoque::obtenerDatosChoquesSeguimientoASC($choqueDetalleObj->idchoque) ?>
 <?php if ($seguimientoData): ?>
    <?php foreach ($seguimientoData as $var): ?>     
   <UL>
                <LI><P STYLE='margin-right: 0.36in; margin-bottom: 0in'>
                    <FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><FONT COLOR='#1f3864'><FONT FACE='Calibri, serif'><FONT SIZE=3><B><?php echo formato($var->fechaRegistro) ?>.</B></FONT></FONT></FONT><FONT COLOR='#1f3864'><FONT FACE='Calibri, serif'></FONT></FONT><FONT COLOR='#1f3864'><FONT FACE='Calibri, serif'><FONT SIZE=3>
                    <?php if ($var->tipoContacto==0): ?>
                        <?php echo $var->notas ?>
                    <?php endif ?>
                    <?php if ($var->tipoContacto==1): ?>
                        Llamada realizada a <?php echo $var->nombre ?> <?php echo $var->detalles ?>
                    <?php endif ?>
                    <?php if ($var->tipoContacto==2): ?>
                        Llamada decibida de <?php echo $var->nombre ?> <?php echo $var->detalles ?><?php endif ?>
                        <small> <br>
                            -- <cite title="" data-original-title=""><?php $usuarioAsesor= Usuario::obtenerUsuario($var->idusuario); echo $usuarioAsesor->usuarioNombre;echo "&nbsp;"; echo $usuarioAsesor->usuarioApellidoPaterno  ?></cite></small> 
                        </FONT></FONT></FONT></FONT></FONT></P>
                    </LI>
                </UL>
<?php endforeach ?>
 <?php endif ?>
                                                    </TD>
                                                </TR>
                                            </TABLE>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                           <tr>
<td style=''padding: 0 0 20px'>
    <div style='color: #000066; font-family: sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'><span color='#17365d' style='color: #17365d;'><span style='font-size: 13.3333px;'>Un cordial saludo,</span></span></div>
    <div style='color: #000066; font-family: sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'></div>
    
    <p style='color: #000066; font-family: sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; margin: 0px;'><span style='font-family: sans-serif; color: #17365d;'>&nbsp;</span></p>
    <div style='color: #000066; font-family: sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'>
        <p style='margin: 0px;'><span style='color: #003366;'><strong><?php $usuario=Usuario::obtenerUsuario($choqueDetalleObj->idusuario);  echo $usuario->usuarioNombre;?> <?php echo $usuario->usuarioApellidoPaterno;  ?> <?php echo $usuario->usuarioApellidoMaterno; ?><br/></strong></span></p>
        <p style='margin: 0px;'><span style='font-size: 11.5pt; color: #003366;'><?php $rolUsuario=Usuario::obtenerRoldeUsuario($usuario->idrol); echo $rolUsuario->cargo ?>.<br /></span></p>
        <p style='margin: 0px;'><strong class='' style='color: #003366; font-family:  sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'><span class='' style='font-size: 11.5pt;'>Tracklink-</span></strong><strong class='' style='color: #003366; font-family:  sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'><span class='' style='font-size: 11.5pt;'>Octo Telematics</span></strong></p>
   
       
    </div>
</td>

                                        </BODY>
                                    </HTML>
<!-- <?php
echo $contenido;
// echo "Hola mundo";

 ?>
 -->



                                                    
           

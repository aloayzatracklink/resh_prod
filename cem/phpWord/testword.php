
<?php include("../../includes/init.php") ?>
<?php 




  //   header('Expires: 0');
  // header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  //   header('Pragma: public');

if (isset($_GET['id'])) {
        $id=$_GET['id'];
        $roboDetalleObj=Robo::obtenerRobo($id);
        $nombre='Reporte de robo '.$roboDetalleObj->placaRobo.' '.$roboDetalleObj->propietario;
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment;filename='.$nombre.'.docx');
    }
    if (empty($id)) {
        echo "<a href='../ingresar'>fail detected <h1>presione aqui</h1></a>";
     die();   # code...
}
?>

<HTML>
<HEAD>
<META HTTP-EQUIV='CONTENT-TYPE' CONTENT='text/html; charset=utf-8'>
<TITLE></TITLE>
<META NAME='GENERATOR' CONTENT='LibreOffice 4.1.6.2 (Linux)'>

<STYLE TYPE='text/css'>
    <!--
    @page { margin-left: 1.18in; margin-right: 1.18in }
    P { margin-bottom: 0.08in; direction: ltr; line-height: 100%; widows: 2; orphans: 2 }
    P.western { font-family: 'Times New Roman', serif; font-size: 12pt; so-language: es-ES }
    P.cjk { font-family: 'Times New Roman'; font-size: 12pt; so-language: es-ES }
    P.ctl { font-family: 'Times New Roman'; font-size: 12pt }
    H1 { margin-top: 0.33in; margin-bottom: 0in; direction: ltr; color: #365f91; line-height: 100%; page-break-inside: avoid; widows: 2; orphans: 2 }
    H1.western { font-family: 'Cambria', serif; font-size: 14pt; so-language: es-ES }
    H1.cjk { font-family: 'Times New Roman'; font-size: 14pt; so-language: es-ES }
    H1.ctl { font-family: ; font-size: 14pt }
    A:link { color: #0000ff; so-language: zxx }
-->
</STYLE>
</HEAD>
<BODY LANG='es-PE' LINK='#0000ff' DIR='LTR'>

<H1 LANG='es-ES' CLASS='western'>REPORTE DE RECUPERACIÓN DE VEHÍCULO
ROBADO</H1>
<P LANG='es-ES' CLASS='western' STYLE='margin-bottom: 0in'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><B>Dirigido
    a:   TRACKLINK OCTO</B></FONT></FONT></P>
    <P LANG='es-ES' CLASS='western' STYLE='margin-bottom: 0in'><BR>
    </P>
    <TABLE WIDTH=595 CELLPADDING=5 CELLSPACING=0>
        <COL WIDTH=260>
            <COL WIDTH=61>
                <COL WIDTH=91>
                    <COL WIDTH=46>
                        <COL WIDTH=86>
                            <TR>
                                <TD COLSPAN=5 WIDTH=583 HEIGHT=8 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Datos del Vehículo: </B></SPAN></FONT></FONT></FONT>
                                    </P>
                                </TD>
                            </TR>
                            <TR>
                                <TD WIDTH=260 HEIGHT=21 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Marca del Vehículo: <?php echo $roboDetalleObj->marca ?></B></SPAN></FONT></FONT></FONT></P>
                                    </TD>
                                    <TD WIDTH=61 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Modelo:</B></SPAN></FONT></FONT></FONT>
                                    </P>
                                </TD>
                                <TD COLSPAN=3 WIDTH=243 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo $roboDetalleObj->modelo ?></B></SPAN></FONT></FONT></FONT></P>
                                </TD>
                            </TR>
                            <TR>
                                <TD WIDTH=260 HEIGHT=25 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Placa: <?php echo $roboDetalleObj->placaRobo ?></B></SPAN></FONT></FONT></FONT></P>
                                    </TD>
                                    <TD WIDTH=61 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Color:</B></SPAN></FONT></FONT></FONT></P>
                                    </TD>
                                    <TD COLSPAN=3 WIDTH=243 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo $roboDetalleObj->color ?></B></SPAN></FONT></FONT></FONT></P>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                            <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Chasis:</B></SPAN></FONT></FONT></FONT>
                                                <FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo $roboDetalleObj->chasis ?></B></SPAN></FONT></FONT></FONT></P>
                                            </TD>
                                            <TD WIDTH=61 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Año:
                                                </B></SPAN></FONT></FONT></FONT>
                                            </P>
                                        </TD>
                                        <TD COLSPAN=3 WIDTH=243 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                            <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo $roboDetalleObj->anio ?></B></SPAN></FONT></FONT></FONT></P>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD WIDTH=260 HEIGHT=11 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                            <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Propietario:
                                            </B></SPAN></FONT></FONT></FONT>
                                        </P>
                                    </TD>
                                    <TD COLSPAN=4 WIDTH=314 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='en-US'><B><?php echo $roboDetalleObj->propietario ?></B></SPAN></FONT></FONT></FONT></P>
                                    </TD>
                                </TR>
                                <TR>
                                    <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>DNI O RUC:
                                        </B></SPAN></FONT></FONT></FONT>
                                    </P>
                                </TD>
                                <TD COLSPAN=4 WIDTH=314 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo $roboDetalleObj->dniOrucPropietario ?></B></SPAN></FONT></FONT></FONT></P>
                                </TD>
                            </TR>
                            <TR>
                                <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Aseguradora: <?php echo $roboDetalleObj->seguro ?></B></SPAN></FONT></FONT></FONT></P>
                                    </TD>
                                    <TD COLSPAN=4 WIDTH=314 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Sistema: <?php echo $roboDetalleObj->contrato ?></B></SPAN></FONT></FONT></FONT></P>
                                        </TD>
                                    </TR>
                                    <TR>
                                        <TD WIDTH=260 HEIGHT=21 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                            <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Nro. Póliza: <?php echo $roboDetalleObj->poliza ?></B></SPAN></FONT></FONT></FONT>
                                            </P>
                                        </TD>
                                        <TD COLSPAN=4 WIDTH=314 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                            <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Nro.
                                                Siniestro: <?php echo $roboDetalleObj->siniestro ?></B></SPAN></FONT></FONT></FONT></P>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Fecha
                                                    y Hora del Robo: </B></SPAN></FONT></FONT></FONT>
                                                </P>
                                            </TD>
                                            <TD COLSPAN=2 WIDTH=162 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                <P LANG='es-ES' CLASS='western' ALIGN=RIGHT><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php
                                                     $horaRobo = substr($roboDetalleObj->horaRobo, 0, 10);echo  $horaRobo." HORAS"; 
                                                 ?></B></SPAN></FONT></FONT></FONT></P>
                                            </TD>
                                            <TD COLSPAN=2 WIDTH=142 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'>  <FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php  

                                                $fecha = substr($roboDetalleObj->horaRobo, 11, 17);echo  $fecha;


                                                 ?></B></SPAN></FONT></FONT></FONT></P>
                                                </TD>
                                            </TR>
                                            <TR>
                                                <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Lugar Robo:
                                                    </B></SPAN></FONT></FONT></FONT>
                                                </P>
                                            </TD>
                                            <TD COLSPAN=4 WIDTH=314 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'> <FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo $roboDetalleObj->lugarRobo ?></B></SPAN></FONT></FONT></FONT></P>
                                                </TD>
                                            </TR>
                                            <TR>
                                                <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Modalidad:
                                                    </B></SPAN></FONT></FONT></FONT>
                                                </P>
                                            </TD>
                                            <TD COLSPAN=4 WIDTH=314 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo $roboDetalleObj->modalidad ?></B></SPAN></FONT></FONT></FONT></P>
                                                </TD>
                                            </TR>
                                            <TR>
                                                <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Fecha
                                                        y hora de reporte a TRACKLINK: </B></SPAN></FONT></FONT></FONT>
                                                    </P>
                                                </TD>
                                                <TD COLSPAN=2 WIDTH=162 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                    <P LANG='es-ES' CLASS='western' ALIGN=RIGHT><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php $fe=formato($roboDetalleObj->fechaRegistro);
                                                                echo $fecha=substr($fe, 0, 10);

                                                     ?></B></SPAN></FONT></FONT></FONT></P>
                                                </TD>
                                                <TD WIDTH=46 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php echo $roboDetalleObj->horaRegistro ?></B></SPAN></FONT></FONT></FONT></P>
                                                </TD>
                                                <TD WIDTH=86 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>HORAS</B></SPAN></FONT></FONT></FONT></P>
                                                </TD>
                                            </TR>
                                            <TR>
                                                <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Fecha
                                                        y Hora de la Recuperación:</B></SPAN></FONT></FONT></FONT></P>
                                                    </TD>
                                                    <TD COLSPAN=2 WIDTH=162 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                        <P LANG='es-ES' CLASS='western' ALIGN=RIGHT><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php
                                                         $fechaRecupero = substr($roboDetalleObj->horaRecupero, 0, 10);echo  $fechaRecupero;

                                                         ?></B></SPAN></FONT></FONT></FONT></P>
                                                    </TD>
                                                    <TD WIDTH=46 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B><?php
                                                         $fechaRecupero = substr($roboDetalleObj->horaRecupero, 11, 17);echo  $fechaRecupero;

                                                         ?></B></SPAN></FONT></FONT></FONT></P>
                                                    </TD>
                                                    <TD WIDTH=86 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>HORAS
                                                        </B></SPAN></FONT></FONT></FONT>
                                                    </P>
                                                </TD>
                                            </TR>
                                            <TR>
                                                <TD WIDTH=260 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                    <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'> <FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><SPAN LANG='es-PE'><B>Lugar
                                                        de Recuperación:</B></SPAN></FONT></FONT></FONT></P>
                                                    </TD>
                                                    <TD COLSPAN=4 WIDTH=314 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
                                                        <P LANG='es-ES' CLASS='western'><FONT COLOR='#002060'><FONT FACE='Calibri, serif'><B><?php echo $roboDetalleObj->lugarRecupero ?></B></FONT></FONT></P>
                                                        </TD>
                                                    </TR>
                                                    <TR>
                                                        <TD COLSPAN=5 WIDTH=583 HEIGHT=15 STYLE='border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in'>
<UL>
<LI><P STYLE='margin-right: 0.36in; margin-bottom: 0in'>
    <FONT FACE='Calibri, serif'><FONT SIZE=2 STYLE='font-size: 11pt'><FONT COLOR='#1f3864'><FONT FACE='Calibri, serif'><FONT SIZE=3><B><?php echo formato($roboDetalleObj->fechaRegistro) ?>.</B></FONT></FONT></FONT><FONT COLOR='#1f3864'><FONT FACE='Calibri, serif'></FONT></FONT><FONT COLOR='#1f3864'><FONT FACE='Calibri, serif'><FONT SIZE=3>

Central de emergencia recibe llamada de <strong><?php echo $roboDetalleObj->nombreInformante ?></strong>  con DNI Numero <?php echo $roboDetalleObj->dniInformante ?> quien reporta el robo de vehículo con placa:  <?php echo $roboDetalleObj->placaRobo ?> pide que nos contactemos al número <strong><?php echo $roboDetalleObj->numeroContacto ?></strong>
    <small> <br><strong>Operador a cargo</strong> -- <cite title="" data-original-title=""><?php $usuarioAsesor= Usuario::obtenerUsuario($roboDetalleObj->idusuario); echo $usuarioAsesor->usuarioNombre;echo "&nbsp;"; echo $usuarioAsesor->usuarioApellidoPaterno  ?></cite></small>
        </FONT></FONT></FONT></FONT></FONT></P>
        </LI>
 </UL>
    <?php SeguimientoRobo::obtenerSeguimientoRoboParaWord($roboDetalleObj->idrobo) ?>
                                                    </TD>
                                                </TR>
                                            </TABLE>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                           <tr>
<td style=''padding: 0 0 20px'>
    <div style='color: #000066; font-family: sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'><span color='#17365d' style='color: #17365d;'><span style='font-size: 13.3333px;'>Un cordial saludo,</span></span></div>
    <div style='color: #000066; font-family: sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'></div>
    
    <p style='color: #000066; font-family: sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; margin: 0px;'><span style='font-family: sans-serif; color: #17365d;'>&nbsp;</span></p>
    <div style='color: #000066; font-family: sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'>
        <p style='margin: 0px;'><span style='color: #003366;'><strong><?php $usuario=Usuario::obtenerUsuario($roboDetalleObj->idusuario);  echo $usuario->usuarioNombre;?> <?php echo $usuario->usuarioApellidoPaterno;  ?> <?php echo $usuario->usuarioApellidoMaterno; ?><br/></strong></span></p>
        <p style='margin: 0px;'><span style='font-size: 11.5pt; color: #003366;'><?php $rolUsuario=Usuario::obtenerRoldeUsuario($usuario->idrol); echo $rolUsuario->cargo ?>.<br /></span></p>
        <p style='margin: 0px;'><strong class='' style='color: #003366; font-family:  sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'><span class='' style='font-size: 11.5pt;'>Tracklink-</span></strong><strong class='' style='color: #003366; font-family:  sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'><span class='' style='font-size: 11.5pt;'>Octo Telematics</span></strong></p>
        <p style='margin: 0px;'><span style='color: #003366;'><span style='font-size: 11.5pt;'>Calle Ricardo Angulo 762 - Urb Corpac - San Isidro, Lima.<span style='color: #17365d;'><span class='Object' id='OBJ_PREFIX_DWT1132_com_zimbra_phone' style='color: #005a95; text-decoration: none; cursor: pointer;'><a href='callto:+51%20950.596.228' target='_blank' style='color: #005a95; text-decoration: none; cursor: pointer;'> </a></span></span></span><span style='font-size: 11.5pt;'><br /></span><span style='font-size: 11.5pt;'>Oficina principal:&nbsp;<span class='' style='color: #17365d; orphans: 2; widows: 2;'><span class='Object' id='OBJ_PREFIX_DWT1131_com_zimbra_phone' style='color: #005a95; text-decoration: none; cursor: pointer;'><a href='callto:+511%20630%207575' target='_blank' style='color: #005a95; text-decoration: none; cursor: pointer;'>+511 630 7575</a></span></span> Anexo 0.&nbsp;</span><br /></span></p>
        <div class='' style='color: #000000; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; line-height: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; font-family: helvetica; font-size: 12px; background-color: #ffffff;'><span class='' style='font-size: 11pt; color: #0096ff; font-family: calibri, sans-serif; orphans: 2; widows: 2;'><span class='Object' id='OBJ_PREFIX_DWT1133_com_zimbra_url' style='color: #005a95; text-decoration: none; cursor: pointer;'><span class='Object' id='OBJ_PREFIX_DWT1169_com_zimbra_url' style='color: #005a95; text-decoration: none; cursor: pointer;'><span class='Object' id='OBJ_PREFIX_DWT435_com_zimbra_url' style='color: #005a95; text-decoration: none; cursor: pointer;'><span class='Object' id='OBJ_PREFIX_DWT500_com_zimbra_url' style='color: #005a95; text-decoration: none; cursor: pointer;'><span class='Object' role='link' id='OBJ_PREFIX_DWT53_com_zimbra_url' style='color: #005a95; text-decoration: none; cursor: pointer;'><a href='http://www.tracklink.pe/' class='' target='_blank' style='color: #005a95; text-decoration: none; cursor: pointer;'>www.tracklink.pe</a></span></span></span></span></span>&nbsp;/<a href='https://www.octotelematics.com/' class='' target='_blank' style='color: #005a95; text-decoration: none; cursor: pointer;'>www.octotelematics.com</a></span></div>
        <div class='' style='color: #000000; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; line-height: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; font-family: helvetica; font-size: 12px; background-color: #ffffff;'><span class='' style='font-size: 11pt; color: #0096ff; font-family: calibri, sans-serif; orphans: 2; widows: 2;'><span class='Object' role='link' id='OBJ_PREFIX_DWT84_com_zimbra_url' style='color: #005a95; text-decoration: none; cursor: pointer; font-family:  sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;'></span><span style='font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: #000000; font-family: arial, helvetica, sans-serif; background-color: #ffffff; float: none; display: inline !important;'></span></span></div>
    </div>
</td>

                                        </BODY>
                                    </HTML>
<?php
echo $contenido;
// echo "Hola mundo";

 ?>




                                                    
           

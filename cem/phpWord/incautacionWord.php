<?php include("../../includes/init.php") ?>
<?php 
if (isset($_GET['id'])) {
        $id=$_GET['id'];
        $roboObj=new Choque;
        $choqueDetalleObj=$roboObj->obtenerDatosDeIncautacionPorId($id);
if ($choqueDetalleObj->chasis<>'') {
	$clienteObj=Onix::ObtenerDatosDesdeChasis($choqueDetalleObj->chasis);
        $apellidos=utf8_encode($clienteObj->apellidos); $propietario = (!empty($clienteObj->nombre)) ? "$clienteObj->nombre"." "."$apellidos" : "$apellidos" ;
        $nombre='REPORTE DE INCAUTACION '.$choqueDetalleObj->tipo.' DE CLIENTE '.$propietario.' PLACA  '.$clienteObj->placa;
}else{
	$clienteObj=false;
	$propietario=$choqueDetalleObj->propietario;
        $nombre='REPORTE DE INCAUTACION '.$choqueDetalleObj->tipo.' DE CLIENTE '.$choqueDetalleObj->propietario.' PLACA  '.$choqueDetalleObj->placaAsistencia ;
}
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename='.$nombre.'.docx');
    }
if (empty($id)) {
        echo "<a href='../ingresar'>fail detected <h1>presione aqui</h1></a>";
        die();   
}
?>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title></title>
	<meta name="generator" content="LibreOffice 5.2.7.2 (Linux)"/>
	<meta name="author" content="Pabel Vasquez"/>
	<meta name="created" content="2017-06-21T21:46:00"/>
	<meta name="changed" content="2017-06-21T23:17:00"/>
	<meta name="AppVersion" content="14.0000"/> <meta name="Company" content="Hewlett-Packard Company"/>
	<meta name="DocSecurity" content="0"/>
	<meta name="HyperlinksChanged" content="false"/>
	<meta name="LinksUpToDate" content="false"/>
	<meta name="ScaleCrop" content="false"/>
	<meta name="ShareDoc" content="false"/>
	<style type="text/css">
		@page { size: 8.27in 11.69in; margin-left: 1.18in; margin-right: 1.18in }
		p { margin-bottom: 0.1in; direction: ltr; line-height: 120%; text-align: left; orphans: 2; widows: 2 }
		p.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: es-ES }
		p.cjk { font-family: "Times New Roman"; font-size: 12pt; so-language: es-ES }
		p.ctl { font-family: "Times New Roman"; font-size: 12pt }
		a:link { color: #0000ff }
	</style>
</head>
<body lang="es-PE" link="#0000ff" dir="ltr">
<div title="header">
	<p style="margin-left: -1.18in; margin-bottom: 0.94in; line-height: 100%">
	<!-- <img src="top.jpg" name="0 Imagen" align="bottom" hspace="1" vspace="1" width="794" height="114" border="0"/> -->
</p>
</div>
<p lang="es-ES" class="western" style="margin-bottom: 0in; line-height: 108%">
<font color="#1f3864"><font face="Calibri, serif"><u><b>REPORTE DE
ATENCIÓN ASISTENCIA <?= $choqueDetalleObj->tipo ?></b></u></font></font></p>
<p lang="es-ES" class="western" style="margin-bottom: 0.17in; line-height: 108%">
<font color="#1f3864"><font face="Calibri, serif"><b>Dirigido a:   
TRACKLINK OCTO</b></font></font></p>
<table width="662" cellpadding="5" cellspacing="0">
	<col width="112">
	<col width="159">
	<col width="4359">
	<col width="161">
	<col width="4359">
	<col width="187">
	<tr>
		<td colspan="6" width="650" height="5" bgcolor="#f2f2f2" style="background: #f2f2f2" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b><span style="background: #f2f2f2">DATOS
			DEL VEHICULO:</span></b></span></font></font></font></p>
		</td>
	</tr>
	<tr>
		<td width="112" height="9" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b>MARCA:</b></span></font></font></font></p>
		</td>
		<td width="159" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#002060"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><b><?php echo ($clienteObj)?$clienteObj->marca:$choqueDetalleObj->marca ?></b></font></font></font></p>
		</td>
		<td colspan="2" width="161" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b>MODELO:
			</b></span></font></font></font>
			</p>
		</td>
		<td colspan="2" width="188" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#002060"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="en-US"><b><?php echo ($clienteObj)?$clienteObj->modelo:$choqueDetalleObj->modelo ?></b></span></font></font></font></p>
		</td>
	</tr>
	<tr>
		<td width="112" height="8" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b>PLACA:
			</b></span></font></font></font>
			</p>
		</td>
		<td width="159" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><b><?php echo ($clienteObj)?$clienteObj->placa:$choqueDetalleObj->placaAsistencia ?></b></font></font></font></p>
		</td>
		<td colspan="2" width="161" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b>COLOR:
			</b></span></font></font></font>
			</p>
		</td>
		<td colspan="2" width="188" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b><?php echo  ($clienteObj)?$clienteObj->color:'' ?></b></span></font></font></font></p>
		</td>
	</tr>
	<tr>
		<td width="112" height="9" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b>CHASIS:</b></span></font></font></font><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE">
			</span></font></font>
			</p>
		</td>
		<td width="159" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b><?php echo ($clienteObj)?$clienteObj->chasis:'' ?></b></span></font></font></font></p>
		</td>
		<td colspan="2" width="161" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b>AÑO:
			</b></span></font></font></font>
			</p>
		</td>
		<td colspan="2" width="188" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b><?php echo ($clienteObj)?$clienteObj->anio:'' ?></b></span></font></font></font></p>
		</td>
	</tr>
	<tr>
		<td colspan="6" width="650" height="9" bgcolor="#f2f2f2" style="background: #f2f2f2" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><b>DATOS
			DEL CLIENTE:</b></font></font></font></p>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="281" height="9" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><b>PROPIETARIO:
			 </b></font></font></font>
			</p>
		</td>
		<td colspan="3" width="359" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><span lang="es-PE"><b><?php echo $propietario ?></b></span></font></font></p>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="281" height="9" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b>DNI/RUC:
			</b></span></font></font></font>
			</p>
		</td>
		<td colspan="3" width="359" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b><?php echo ($clienteObj)?$clienteObj->dni:$choqueDetalleObj->idcliente ?></b></span></font></font></font></p>
		</td>
	</tr>
	<tr>
		<td width="112" height="9" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b>PLAN:</b></span></font></font></font></p>
		</td>
		<td width="159" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b>
			<?php echo $choqueDetalleObj->plan ?></b></span></font></font></font></p>
		</td>
		<td colspan="2" width="161" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b>ASEGURADORA:</b></span></font></font></font></p>
		</td>
		<td colspan="2" width="188" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b>
			<?php echo $choqueDetalleObj->aseguradora ?></b></span></font></font></font></p>
		</td>
	</tr>
	<tr>
		<td colspan="6" width="650" height="9" bgcolor="#f2f2f2" style="background: #f2f2f2" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b>DATOS DE LA INCAUTACION:</b></span></font></font></font></p>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="281" height="8" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b>LUGAR INCAUTACIÓN:
			</b></span></font></font></font>
			</p>
		</td>
		<td colspan="3" width="359" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><span lang="es-PE"><b><?php echo $choqueDetalleObj->lugar ?></b></span></font></font></p>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="281" height="9" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b>FECHA Y HORA DE INICIO</b></span></font></font></font></p>
		</td>
		<td colspan="3" width="161" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b><?php echo formato($choqueDetalleObj->fechaRegistro) ?></b></span></font></font></font></p>
		</td>
	</tr>
	<tr>
		<td colspan="3" width="281" height="9" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b>FECHA Y HORA DE INCAUTACION</b></span></font></font></font></p>
		</td>
		<td colspan="3" width="161" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western"><font color="#1f3864"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="es-PE"><b><?php echo $choqueDetalleObj->fechaAsistencia ?></b></span></font></font></font></p>
		</td>
	</tr>
	
	<tr>
		<td colspan="6" width="650" height="108" valign="bottom" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p class="western" align="justify" style="margin-right: 0.74in; margin-bottom: 0in">
			<br/>

			</p>
			<ul>
				<li/>
<p align="justify" style="margin-right: 0.25in; margin-bottom: 0in">
				<font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#1f3864"><font size="3" style="font-size: 12pt"><b><?php echo formato($choqueDetalleObj->fechaRegistro) ?> Horas.
				</b></font></font><font color="#17365d"><font face="Calibri, serif"><font size="3" style="font-size: 12pt">La central de emergencias y Monitoreo inicia seguimiento del vehiculo  del clente  <?php echo $choqueDetalleObj->propietario ?> </font></font></font></font></font></p>
			</ul>
			<p align="justify" style="margin-left: 0.75in; margin-right: 0.25in; margin-bottom: 0in">
			<br/>
			</p>
 <?php $seguimientoData=SeguimientoChoque::obtenerDatosIncautacionSeguimientoASC($choqueDetalleObj->idincautacion) ?>
 <?php if ($seguimientoData): ?>
    <?php foreach ($seguimientoData as $var): ?> 
			<ul>
				<li/>
<p align="justify" style="margin-right: 0.25in; margin-bottom: 0in">
				<font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#1f3864"><font size="3" style="font-size: 12pt"><b><?php echo formato($var->fechaRegistro) ?></b></font></font><font color="#17365d"><font face="Calibri, serif"><font size="3" style="font-size: 12pt">
				</font></font></font><font color="#002060"><font size="3" style="font-size: 12pt"><?php if ($var->tipoContacto==0): ?>
                        <?php echo $var->notas ?>
                    <?php endif ?>
			<?php if ($var->tipoContacto==1): ?>
                        Llamada realizada a <?php echo $var->nombre ?> <?php echo $var->detalles ?>
                    <?php endif ?>
                    <?php if ($var->tipoContacto==2): ?>
                        Llamada decibida de <?php echo $var->nombre ?> <?php echo $var->detalles ?><?php endif ?>
                        <small> <br>
                            -- <cite title="" data-original-title=""><?php $usuarioAsesor= Usuario::obtenerUsuario($var->idusuario); echo $usuarioAsesor->usuarioNombre;echo "&nbsp;"; echo $usuarioAsesor->usuarioApellidoPaterno  ?></cite></small> 
                    .</font></font></font></font></p>
			</ul>
			
			<p style="margin-left: 0.5in; margin-bottom: 0in"><br/>

			</p>
			<?php endforeach ?>
 <?php endif ?>
			


		</td>
	</tr>
	<tr>
		<td colspan="6" width="650" valign="bottom" style="border: 1px solid #bfbfbf; padding-top: 0in; padding-bottom: 0in; padding-left: 0.05in; padding-right: 0.05in">
			<p lang="es-ES" class="western" style="margin-bottom: 0in"><font color="#002060"><font face="Calibri, serif"><span lang="es-PE">Se
			informa&nbsp;para los fines convenientes.</span></font></font></p>
			<p style="margin-bottom: 0in; background: #ffffff"><br/>

			</p>
			<p style="margin-bottom: 0in; background: #ffffff"><b><font color="#17365d"><font face="Calibri, serif"><?php $usuario=Usuario::obtenerUsuario($choqueDetalleObj->idusuario);  echo $usuario->usuarioNombre;?> <?php echo $usuario->usuarioApellidoPaterno;  ?> <?php echo $usuario->usuarioApellidoMaterno; ?></font></font></b></p>
			<p style="margin-bottom: 0in; background: #ffffff"><font color="#17365d"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><?php $rolUsuario=Usuario::obtenerRoldeUsuario($usuario->idrol); echo $rolUsuario->cargo ?></font></font></font></p>
			<p style="margin-bottom: 0in; background: #ffffff"><font color="#17365d"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="en-US">TRACKLINK
			 - OCTO</span></font></font></font></p>
			<p lang="es-ES" class="western" align="justify" style="margin-right: 0.74in">
			<font color="#17365d"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><span lang="en-US">Octo
			telematics Business Partner</span></font></font></font></p>
		</td>
	</tr>
</table>
<p lang="es-ES" class="western" style="margin-bottom: 0in; line-height: 100%">
<br/>

</p>
<div title="footer">
	<p style="margin-left: -1.18in; margin-top: 0.94in; margin-bottom: 0in; line-height: 100%">
	<!-- <img src="bottom.jpg" name="Image1" align="bottom" vspace="1" width="796" height="94" border="0"/> -->
</p>
</div>
</body>
</html>

<?php $nav="instalacion" ?>
<?php require("../includes/head.php") ?>
<?php
    if (!$session->yaEstaIngresado()) {
        irA("../ingresar");
    }
?>
<?php $obMenu=Usuario::obtenerMenu($session->idusuario) ?>
<?php if ($obMenu->instalaciones=="si"): ?>
<?php 
require_once("../includes/init.track.php");
$heObj=new Healt;

$operacionObj=new Operacion();
$calObj=new Calendario();
$vehiObj=new Vehiculo();
$datosIObj=new DatosInstalacion;
$noAtender=new noAtender();
$datos="";
$errores=array();
$echo=array();

    if (isset($_POST['idve'])) {
        $idvehiculo=$_POST['idve'];
    }


    if (isset($_POST["guardarCambios"]) AND !isset($_POST['agregarTEMP'])) {

    if(isset($_POST['motivos'])){
        $class="Chequeos";
        $class_id=$_POST['idop'];
        foreach ($_POST['motivos'] as $mt){
          if(!Motivos::getMotivosByClassAndIdMotivo($class,$class_id,$mt)){
            Motivos::registrar_motivo($mt,$class,$class_id);
          }
        }

         foreach(Motivos::getMotivosForClass("Chequeos",$class_id) as $mtc){
           if(!in_array($mtc->motivo_id,$_POST['motivos'])){
                Motivos::borrar_motivo($mt,$class,$class_id);
           }
         }
    }


    if(isset($_POST['modo2'])){
        $noAtender->idcustomer=$_POST['idcustomer'];
        $noAtender->idlocation=$_POST['idlocation'];
        $noAtender->idvehicle=$_POST['idvehicle'];
        $noAtender->actualizarDatosDeInstalacion();
      //  isset($_POST['idcustomer']) && isset($_POST['idlocation']) && isset($_POST['idvehicle'])
    }

    if ($_POST['fechaInstalacion'] != $_POST['fechaInstalacionOLD']) {
        $fechaOld=$_POST['fechaInstalacionOLD'];
        $fechaN=$_POST['fechaInstalacion'];
        $operacionObj->idoperacion=$_POST['idop'];
        $calObj->idoperacion=$_POST['idop'];
       if (!$operacionObj->cambiarFecha($fechaOld,$fechaN)) {
          $errores[] ="Error en actualizar datos operacion";
       } 
       if (!$calObj->cambiarFecha($fechaOld,$fechaN)) {
          $errores[] ="Error en actualizar calendar";
       } 
    }


$datosIObj->cliente=$_POST['nombreCliente'];
$datosIObj->orte=$_POST['orte'];
$datosIObj->placa=$_POST['placa'];
$datosIObj->chasis=$_POST['chasis'];

if (isset($_POST['imei'])) {
    $datosIObj->imei=$_POST['imei'];
}

$datosIObj->sistema=$_POST['plan'];

$pl=strtoupper($datosIObj->sistema);

if (isset($_POST['id'])) {

    $datosIObj->id=$_POST['id'];
    if (strpos($pl, 'CONTROL') !== false) {
        if($_POST['trabajo']!="Desinstalacion"){

//    $onObj->idcustomer=limpiar($_POST['idcustomer']);
//    $onObj->idvehicle=limpiar($_POST['idvehicle']);
//    $onObj->idlocation=limpiar($_POST['idlocation']);
//var_dump($_POST);
            if(isset($_POST['dniOruc']) && isset($_POST['idcustomer']) && isset($_POST['idlocation']) && isset($_POST['idvehicle'])  ){

                Log::registrarLog("HABITOS_MANEJO",$_POST['dniOruc'],$_POST,"Proc");          
                if(!$heObj->setUserQuilReports(trim($_POST['dniOruc']),limpiar($_POST['idcustomer']) ,limpiar($_POST['idlocation']),limpiar($_POST['idvehicle']), addslashes($_POST['lastname']))){
                   echo "UN ERROR INESPERADO OCURRIO AL ACTIVAR HABITOS DE MANEJO"; 
                }else{
                    Log::registrarLog("HABITOS_MANEJO",$_POST['dniOruc'],$_POST,"OK");          
                }
            }else{
                echo "NOT SET CORRECT";
            }

        }
    }
}

if (isset($_POST['odometro'])) {
$datosIObj->odo=$_POST['odometro'];
}
$datosIObj->ubicacionSistema=$_POST['ubicacionSistema'];
$datosIObj->lugarInstalacion=$_POST['lugarInstalacion'];
//
$usuarioMonitoreo=Usuario::obtenerUsuario($session->idusuario);
$monitoreo=$usuarioMonitoreo->usuarioNombre." ".$usuarioMonitoreo->usuarioApellidoPaterno;
$usuariotEC=Usuario::obtenerUsuario($_POST['tecnicoInstalador']);
$tecnicoI=$usuariotEC->usuarioNombre." ".$usuariotEC->usuarioApellidoPaterno;
////OBTENER TEJNICO
$datosIObj->tecnicoInstalador=$tecnicoI;
////OBTENER TEJNICO

$datosIObj->servicio=$_POST['trabajo'];
//
////OBTENER cem
$datosIObj->operador=$monitoreo;
////OBTENER cem
if ($_POST['estado']=='pendiente') {
    $datosIObj->estado='99';

    // notificar instalacion toyota ---  Inicio
    $path =  "/sendStatusVehicle";
    $param = array(
        "chasis" => $_POST['chasis'],
        "estado" => "installed"
    );
    $rptaService = APIToyota::PostService($path,$param);
    // notificar instalacion toyota ---  fin
}else{
    $datosIObj->estado='4';
    if (isset($_POST['horaActivacion'])) {
        $datosIObj->horaActivacion=$_POST['horaActivacion'];
    }else{
        $datosIObj->horaActivacion=date('YmdHis');
    }
}

$datosIObj->fecha=soloNumero($_POST['fechaInstalacion']);
$datosIObj->horaActivacion;
if (isset($_POST['odometro'])) {
    $datosIObj->cordenadas=$_POST['coordenadas'];
}
$datosIObj->observaciones=$_POST['observacion'];
$datosIObj->idoperacion=$_POST['idop'];
if ($datosIObj->verificarExistenciaOperacion()) {
    if ($datosIObj->actualizarDatosDeInstalacion()) {


        if ($_POST['estado']=='pendiente') {
    $idoperacion=$_POST['idop'];
    $operacionObj->idTecnicoInstalador=$_POST['tecnicoInstalador'];
    $operacionObj->imei=$_POST['imei'];
    $operacionObj->id=$_POST['id'];
    $operacionObj->odometro=$_POST['odometro'];
    $operacionObj->ubicacionSistema=$_POST['ubicacionSistema'];
    $operacionObj->lugarActivacion=$_POST['lugarInstalacion'];
    $operacionObj->coordenadas=$_POST['coordenadas'];
    $operacionObj->pruebas=$datos;
    $operacionObj->observacion=$_POST['observacion'];
    if($operacionObj->guardarComoPendiente($idoperacion)){
        $fecha=date("YmdHis");
        $vehiObj->idvehiculo=$idvehiculo;
        $vehiObj->cambiarEstadoInstaladorPeroNoreporta();
        $calObjData=Calendario::obtenerTituloInstalacion($idoperacion);
         $usuarioMonitoreo=Usuario::obtenerUsuario($session->idusuario);
         $monitoreo=$usuarioMonitoreo->usuarioNombre." ".$usuarioMonitoreo->usuarioApellidoPaterno;
         #$titulo=$calObjData->title;
         $calObj->color="#f48b97";
         #$calObj->title=$titulo." ==> Pendiente monitoreo Por: ".$monitoreo." el  ".formato($fecha)." ";
         $calObj->idoperacion=$idoperacion;
         if($calObj->guardarComoPendiente()){
            $echo[]="Guardado Como pendiente de instalación";
         }
    }
}
else{
    $idoperacion=$_POST['idop'];
    if (isset($_POST['idve'])) {
        $idvehiculo=$_POST['idve'];        
    }
    $operacionObj->idTecnicoInstalador=$_POST['tecnicoInstalador'];
    if (isset($_POST['imei'])) {
        $operacionObj->imei=$_POST['imei'];
    }
    if (isset($_POST['id'])) {
        $operacionObj->id=$_POST['id'];
    }
    if (isset($_POST['odometro'])) {
        $operacionObj->odometro=$_POST['odometro'];
    }
    if (isset($_POST['ubicacionSistema'])) {
        $operacionObj->ubicacionSistema=$_POST['ubicacionSistema'];
    }
    if (isset($_POST['lugarInstalacion'])) {
        $operacionObj->lugarActivacion=$_POST['lugarInstalacion'];
    }
    if (isset($_POST['coordenadas'])) {
        $operacionObj->coordenadas=$_POST['coordenadas'];
    }    
    $operacionObj->observacion=$_POST['observacion'];  
    if (isset($_POST['ubicacion'])) {
            $ubicacion="Ubicacion ok ";
            $datos.=" Ubicacion ok ";
    }
        if (isset($_POST['noReporta'])) {
            $datos.=" Dispositivo no reporta ";
    }   if (isset($_POST['bloqueoDesbloqueo'])) {         
            $datos.=" Pruebas de bloqueo y desbloqueo OK ";
    }   if (isset($_POST['panico'])) {
            $datos.=" Boton de panico OK ";
    }   if (isset($_POST['pestillos'])) {
            $datos.=" Apertura de pestillos OK ";
    }   if (isset($_POST['sensorCombustible'])) {
            $datos.=" Sensor de combustible OK ";
    }   if (isset($_POST['kit'])) {
            $datos.=" Kit de habla escucha OK ";
    }   if (isset($_POST['alertaSonora'])) {
            $datos.=" Alerta sonora OK ";
    }
    $operacionObj->pruebas=$datos;
if (empty($errores)) {
    if (isset($_POST['idve'])) {
        if($operacionObj->instalar($idoperacion,$idvehiculo)){ 
         $fecha=date("YmdHis");
         $calObjData=Calendario::obtenerTituloInstalacion($_POST['idop']);
         $usuarioMonitoreo=Usuario::obtenerUsuario($session->idusuario);
         $usuariotEC=Usuario::obtenerUsuario($_POST['tecnicoInstalador']);
         $vehiD=Vehiculo::obtenerVehiculo($idvehiculo);
         if ($vehiD->estado!=4) {
             $vehiObj->idvehiculo=$idvehiculo;
             $vehiObj->cambiarEstadoInstaladosinbienvenida();
         }
         $monitoreo=$usuarioMonitoreo->usuarioNombre." ".$usuarioMonitoreo->usuarioApellidoPaterno;
         $tecnicoI=$usuariotEC->usuarioNombre." ".$usuariotEC->usuarioApellidoPaterno;
         $calObj->resourceId=$_POST['idTecnicoAsignado'];
         #$titulo=$calObjData->title;
         $calObj->color="#84c600";
         #$calObj->startdate=$calObjData->startdate;
         #$calObj->title=$titulo." Monitoreo Por: ".$monitoreo." el  ".formato($fecha)." ";
         $calObj->confirmarFechaInstalacion($_POST['idop']);
         $echo[]="Registro realizado con exito ";
    }
}else{
    if ($operacionObj->guardarChequeoOk($_POST['idop'])) {
        $fecha=date("YmdHis");        
        $usuarioMonitoreo=Usuario::obtenerUsuario($session->idusuario);
        $calObjData=Calendario::obtenerTituloInstalacion($_POST['idop']);
        $echo[]="Chequeo registrado exitosamente.";
        $monitoreo=$usuarioMonitoreo->usuarioNombre." ".$usuarioMonitoreo->usuarioApellidoPaterno;
        $titulo=$calObjData->title;
        $calObj->color="#84c600";
        $calObj->title=$titulo." Monitoreo Por: ".$monitoreo." el  ".formato($fecha)." ";
        $calObj->idoperacion=$_POST['idop'];
        if ($calObj->confirmarChequeoEnMonitoreo()) {
            $echo[]="Chequeo Guardado Exitosamete";
        }
    }
}
}
}
    }else{
        $error[] ="ERROR X212D ";
    }
///UPDATE 
}else{
///NEW 
if (!$datosIObj->registrarDatosDeInstalacion()) {
   $error[] ="ERROR XD ";
}else{

if ($_POST['estado']=='pendiente') {
    $idoperacion=$_POST['idop'];
    $operacionObj->idTecnicoInstalador=$_POST['tecnicoInstalador'];
    $operacionObj->imei=$_POST['imei'];
    $operacionObj->id=$_POST['id'];
    $operacionObj->odometro=$_POST['odometro'];
    $operacionObj->ubicacionSistema=$_POST['ubicacionSistema'];
    $operacionObj->lugarActivacion=$_POST['lugarInstalacion'];
    $operacionObj->coordenadas=$_POST['coordenadas'];
    $operacionObj->pruebas=$datos;
    $operacionObj->observacion=$_POST['observacion'];
    if($operacionObj->guardarComoPendiente($idoperacion)){
        $fecha=date("YmdHis");
        $vehiObj->idvehiculo=$idvehiculo;
        $vehiObj->cambiarEstadoInstaladorPeroNoreporta();
        $calObjData=Calendario::obtenerTituloInstalacion($idoperacion);
         $usuarioMonitoreo=Usuario::obtenerUsuario($session->idusuario);
         $monitoreo=$usuarioMonitoreo->usuarioNombre." ".$usuarioMonitoreo->usuarioApellidoPaterno;
         #$titulo=$calObjData->title;
         $calObj->color="#f48b97";
         #$calObj->title=$titulo." ==> Pendiente monitoreo Por: ".$monitoreo." el  ".formato($fecha)." ";
         $calObj->idoperacion=$idoperacion;
         if($calObj->guardarComoPendiente()){
            $echo[]="Guardado Como pendiente de instalación";
         }
    }
}else{
    $idoperacion=$_POST['idop'];
    if (isset($_POST['idve'])) {
        $idvehiculo=$_POST['idve'];        
    }
    $operacionObj->idTecnicoInstalador=$_POST['tecnicoInstalador'];
    if (isset($_POST['imei'])) {
        $operacionObj->imei=$_POST['imei'];
    }
    if (isset($_POST['id'])) {
        $operacionObj->id=$_POST['id'];
    }
    if (isset($_POST['odometro'])) {
        $operacionObj->odometro=$_POST['odometro'];
    }
    if (isset($_POST['ubicacionSistema'])) {
        $operacionObj->ubicacionSistema=$_POST['ubicacionSistema'];
    }
    if (isset($_POST['lugarInstalacion'])) {
        $operacionObj->lugarActivacion=$_POST['lugarInstalacion'];
    }
    if (isset($_POST['coordenadas'])) {
        $operacionObj->coordenadas=$_POST['coordenadas'];
    }    
    $operacionObj->observacion=$_POST['observacion'];  
    if (isset($_POST['ubicacion'])) {
            $ubicacion="Ubicacion ok ";
            $datos.=" Ubicacion ok ";
    }
        if (isset($_POST['noReporta'])) {
            $datos.=" Dispositivo no reporta ";
    }   if (isset($_POST['bloqueoDesbloqueo'])) {         
            $datos.=" Pruebas de bloqueo y desbloqueo OK ";
    }   if (isset($_POST['panico'])) {
            $datos.=" Boton de panico OK ";
    }   if (isset($_POST['pestillos'])) {
            $datos.=" Apertura de pestillos OK ";
    }   if (isset($_POST['sensorCombustible'])) {
            $datos.=" Sensor de combustible OK ";
    }   if (isset($_POST['kit'])) {
            $datos.=" Kit de habla escucha OK ";
    }   if (isset($_POST['alertaSonora'])) {
            $datos.=" Alerta sonora OK ";
    }
    $operacionObj->pruebas=$datos;
if (empty($errores)) {
    if (isset($_POST['idve'])) {

        if (isset($_POST['datosFacturacionId'])) {
              if (isset($_POST['ordenContrato']) and $_POST['ordenContrato'] !='') {
                if(DatosFacturacion::crearContrato($_POST['datosFacturacionId'])){
                ##GENERATE CONTRACT
                    $ordenn=explode("-",$_POST['ordenContrato']);
                    $ordenn[0];
                    $ordenn[1];
                    $_POST['datosFacturacionId'];
                    $command = escapeshellcmd('python3.7 ../../pdf/run_queue.py contract '.$ordenn[1].' '.$ordenn[0].' '.$_POST['datosFacturacionId']);
                    $output = shell_exec($command);
                }
            }
        }
        if($operacionObj->instalar($idoperacion,$idvehiculo)){ 

         $fecha=date("YmdHis");
         $calObjData=Calendario::obtenerTituloInstalacion($_POST['idop']);
         $usuarioMonitoreo=Usuario::obtenerUsuario($session->idusuario);
         $usuariotEC=Usuario::obtenerUsuario($_POST['tecnicoInstalador']);
         $vehiD=Vehiculo::obtenerVehiculo($idvehiculo);
         if ($vehiD->estado!=4) {
             $vehiObj->idvehiculo=$idvehiculo;
             $vehiObj->cambiarEstadoInstaladosinbienvenida();
         }
         $monitoreo=$usuarioMonitoreo->usuarioNombre." ".$usuarioMonitoreo->usuarioApellidoPaterno;
         $tecnicoI=$usuariotEC->usuarioNombre." ".$usuariotEC->usuarioApellidoPaterno;
         $calObj->resourceId=$_POST['idTecnicoAsignado'];
         #$titulo=$calObjData->title;
         $calObj->color="#84c600";
         #$calObj->startdate=$calObjData->startdate;
         #$calObj->title=$titulo." Monitoreo Por: ".$monitoreo." el  ".formato($fecha)." ";
         $calObj->confirmarFechaInstalacion($_POST['idop']);
         $echo[]="Registro realizado con exito ";
    }
}else{
    if ($operacionObj->guardarChequeoOk($_POST['idop'])) {
        $fecha=date("YmdHis");        
        $usuarioMonitoreo=Usuario::obtenerUsuario($session->idusuario);
        $calObjData=Calendario::obtenerTituloInstalacion($_POST['idop']);
        $echo[]="Chequeo registrado exitosamente.";
        $monitoreo=$usuarioMonitoreo->usuarioNombre." ".$usuarioMonitoreo->usuarioApellidoPaterno;
        $titulo=$calObjData->title;
        $calObj->color="#84c600";
        $calObj->title=$titulo." Monitoreo Por: ".$monitoreo." el  ".formato($fecha)." ";
        $calObj->idoperacion=$_POST['idop'];
        if ($calObj->confirmarChequeoEnMonitoreo()) {
            $echo[]="Chequeo Guardado Exitosamete";
        }
    }
}
}
}
}
}

} 

if(isset($_POST['agregarTEMP'])){
    $detalles="Regularizar ".limpiar($_POST['trabajo'])." orte ";
    $detalles.=limpiar($_POST['numeroOrte']);
    $detalles.=" de vehículo con placa ";
    $detalles.=limpiar($_POST['placa']);
    $detalles.=" cliente  ";
    $detalles.=limpiar($_POST['nombreCliente']);
    Notificaciones::registrarNotificacionRegularizarTemporal($detalles);
$datosIObj->cliente=$_POST['nombreCliente'];
$datosIObj->orte=$_POST['numeroOrte'];
$datosIObj->placa=$_POST['placa'];
$datosIObj->chasis=$_POST['chasis'];
if (isset($_POST['imei'])) {
    $datosIObj->imei=$_POST['imei'];
}
if (isset($_POST['id'])) {
$datosIObj->id=$_POST['id'];
}
if (isset($_POST['odometro'])) {
$datosIObj->odo=$_POST['odometro'];
}
$datosIObj->ubicacionSistema=$_POST['ubicacionSistema'];
$datosIObj->lugarInstalacion=$_POST['lugarInstalacion'];
//
//
$usuarioMonitoreo=Usuario::obtenerUsuario($session->idusuario);
$monitoreo=$usuarioMonitoreo->usuarioNombre." ".$usuarioMonitoreo->usuarioApellidoPaterno;
$usuariotEC=Usuario::obtenerUsuario($_POST['tecnicoInstalador']);
$tecnicoI=$usuariotEC->usuarioNombre." ".$usuariotEC->usuarioApellidoPaterno;
////OBTENER TEJNICO
$datosIObj->tecnicoInstalador=$tecnicoI;
////OBTENER TEJNICO
$datosIObj->sistema=$_POST['plan'];
$datosIObj->servicio=$_POST['trabajo'];
//
////OBTENER cem
$datosIObj->operador=$monitoreo;
////OBTENER cem
if ($_POST['estado']=='pendiente') {
    $datosIObj->estado='99';
}else{
    $datosIObj->estado='15';
    if (isset($_POST['horaActivacion'])) {
        $datosIObj->horaActivacion=$_POST['horaActivacion'];
    }else{
        $datosIObj->horaActivacion=date('YmdHis');
    }
}

$datosIObj->fecha=soloNumero($_POST['fechaInstalacion']);
$datosIObj->horaActivacion;
if (isset($_POST['odometro'])) {
    $datosIObj->cordenadas=$_POST['coordenadas'];
}
$datosIObj->observaciones=$_POST['observacion'];
if (!$datosIObj->registrarDatosDeInstalacionTMP()) {
   $error[]='FAIL EN REGISTRAR EL TMP';
}else{
    $echo[]="Registrado";
}
}
if(isset($_POST['eliminarTMP241'])){
$datosIObj->iddatosInstalacion=$_POST['eliminarTMP241'];
//if($datosIObj->eliminarTmpPorId())
//    $echo[]="Se elimino el temporal con exito";
} 


    ?>
   <link href="../assets/css/plugins/footable/footable.core.css" rel="stylesheet">
   <link href="../assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
   <link href="../assets/css/sweetalert2.css" rel="stylesheet">
   <link href="../assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
    <link href="../assets/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
   <style>
    .widget p {
        display: inline-block;        
    }

    .fecha {
        font-family: Helvetica, Arial;
        text-align: center;
        font-size:14px;
    }

    .reloj {
        font-family: Helvetica, Arial;
        font-size:23px;
        text-align: center;        
        margin-top: 6px;
    }
    .reloj .caja-segundos {
        display: inline-block;
    }
    .reloj .segundos,
    .reloj .ampm {
        font-size: 10px;
        display: block;
    }
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(../assets/img/Preloader_6.gif) center no-repeat #fff;
}
.pagination1{margin:0;padding:0;}
.pagination1 li{
    display: inline;
    padding: 6px 10px 6px 10px;
    border: 1px solid #ddd;
    margin-right: -1px;
    font: 15px/20px Arial, Helvetica, sans-serif;
    background: #FFFFFF;
    box-shadow: inset 1px 1px 5px #F4F4F4;
}
.pagination1 li a{
    text-decoration:none;
    color: rgb(89, 141, 235);
}
.pagination1 li.first {
    border-radius: 5px 0px 0px 5px;
}
.pagination1 li.last {
    border-radius: 0px 5px 5px 0px;
}
.pagination1 li:hover{
    background: #CFF;
}
.pagination1 li.active{
    background: #F0F0F0;
    color: #333;
}
</style>
 <body>
<div class="se-pre-con"></div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <?php include("../includes/menuIzquierdo.php") ?>
               </div>
           </nav>
           <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <?php include("../includes/navegacionArriba.php") ?>
                </nav>
            </div>
            <div class="wrapper wrapper-content">

                <div class="row ibox float-e-margins ibox-content ">
                    <div class="col-md-12">
                        <div class="row">
                        <div class="col-md-3">
                           <a href="todoLosReigistros" class="btn btn-primary">Ver todo los registros</a>
                        </div>
                        <div class="col-md-5">
                            <div data-toggle="buttons" class="btn-group">
                                <label class="btn btn-sm btn-white"> <input class="fechas" type="radio" id="option" name="fecha" value ='pasados'> Pasado </label>
                                <label class="btn btn-sm btn-white active"> <input class="fechas" type="radio" id="option1" checked name="fecha" value='hoy'> Hoy </label>
                                <label class="btn btn-sm btn-white "> <input class="fechas" type="radio" id="option3" name="fecha" value ='siguientes'> Futuro </label>
                                <label class="btn btn-sm btn-danger "> <input class="fechas" type="radio" id="option4" name="fecha" value ='pendiente'> No activados </label>
                                <label class="btn btn-sm btn-primary"> <input class="fechas" type="radio" id="option5" name="fecha" value ='listo'> Listo monitoreo </label>
<?php
$cantidadTMPS=DatosInstalacion::cantidadTempo();
    if ($cantidadTMPS) {
       ?>
           <button class="btn btn-sm btn-warning" data-toggle="modal" data-target="#bateriasOModal" data-id="rerTemporales">Temporales <?= $cantidadTMPS ?> <i class="fa fa-eye"></i></button>  </label>
       <?php 
    }
?>
                            </div>
                        </div>    
                        <div class=" col-md-4 ">
                    <div class="widget" style="line-height: 0px;       
                           padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px">
                           <div class="fecha">
                            <p id="diaSemana" class="diaSemana">Martes</p>
                            <p id="dia" class="dia">27</p>
                            <p>de </p>
                            <p id="mes" class="mes">Octubre</p>
                            <p>del </p>
                            <p id="year" class="year">2015</p>
                        </div>
                        <div class="reloj">
                            <p id="horas" class="horas">11</p>
                            <p>:</p>
                            <p id="minutos" class="minutos">48</p>
                            <p>:</p>
                            <div class="caja-segundos">
                                <p id="ampm" class="ampm">AM</p>
                                <p id="segundos" class="segundos">12</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox-content" id="resultados">
        <input type="text" class="form-control input-sm m-b-xs" id="filter"
        placeholder="Buscar" >
        <div class="table-responsive">
        <table class="table table-condensed  table-hover" >
            <thead>
                <tr>                   
                    <th>Orte</th>
                    <th>Lugar</th>
                    <th>Trabajo</th>
                    <th>Cliente</th>
                    <th>Fecha Programada</th>
                    <th>Ejecutivo operaciones</th>
                    <th>Campaña</th>
                    <th>Tipo Registro</th>
                    <th>Estado</th>
                    <th>Acción</th>
                </tr>
            </thead>  
            <tbody  id="resultado">          
            </tbody>  
        </table>
        </div>
    </div>
</div>
<?php include("../includes/footer.php") ?>
</div>
</div>
<div class="modal inmodal" id="bateriasOModal" tabindex="-1"  role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceIn">
            <div class="modal-header" style="padding-top: 0;padding-bottom: 0">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title">Gestión Instalación/Chequeos </h4> 
                <small class="font-bold"><div class="form-group ">
                </div></small>
            </div>
            <form  role="form" method="post" id='instalacionForm'>
                <div class="modal-body" style="background: #fff;padding-top: 0">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>                    
                    <button type="submit" id="guardarCambio" name="guardarCambios" class="btn btn-primary">Guardar</button>
                </div>
            </form>   
        </div>
    </div>
</div>
</body>
<script src="../assets/js/jquery-2.1.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="../assets/js/inspinia.js"></script>
<script src="../assets/js/plugins/pace/pace.min.js"></script>
<script src="../assets/js/plugins/footable/footable.all.min.js"></script>
<script src="../assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="../assets/js/clipboard.min.js"></script>
<script src="../assets/calendario/dist/sweetalert.min.js"></script>
<script src="../assets/js/plugins/toastr/toastr.min.js"></script>
<script src="../assets/js/plugins/fullcalendar/moment.min.js"></script>
<script src="../assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="../assets/js/plugins/validate/jquery.validate.min.js"></script>
<?php include_once('../includes/script.php') ?> 
<script>
<?php if (!empty($errores)): ?>
<?php foreach ($errores as $error): ?>
  toastr.error('<?php echo $error ?>', 'Error', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?> 
<?php endif ?>
<?php if(!empty($echo)): ?>
<?php foreach($echo as $ok): ?>
  toastr.success("<?php echo $ok ?>", 'Exito', {timeOut: 5000,progressBar:true,closeButton:true});
  <?php endforeach ?>
 <?php endif ?> 
$(document).ready(function(){
    var convenio=$("#convenio").val();
    var fechas=$(".fechas:checked").val();
    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionInstalacion',
        data : {convenio:convenio,fechas:fechas},
        success : function(data)
        {
         $("#resultado").html(data);
        }
      });

$(".fechas").change(function(){
    var convenio=$("#convenio").val();
    var fechas=$(".fechas:checked").val();
    $.ajax({
        type : 'POST',
        url  : 'pag/paginacionInstalacion',
        data : {convenio:convenio,fechas:fechas},
        success : function(data)
        {
         $("#resultado").html(data);
        }
      });
});
    $( "#convenio" ).change(function() { 
        var convenio = $(this).val(); 
        var fecha=$("#rango").val();      
        var fechas=$(".fechas:checked").val();
        var page = $('#resultado').attr("data-page"); 
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionInstalacion',
        data : {convenio:convenio,page:page,fecha:fecha,fechas:fechas},
        success : function(data)
        {
         $("#resultado").html(data);
        }
      });
    });     

    $("#resultado").on( "click", ".pagination1 a", function (e){
        e.preventDefault();
        var fecha=$("#rango").val();
        var convenio=$("#convenio").val();        
        var fechas=$(".fechas:checked").val();
        var page = $(this).attr("data-page"); 
        $.ajax({
        type : 'POST',
        url  : 'pag/paginacionInstalacion',
        data : {convenio:convenio,page:page,fecha:fecha,fechas:fechas},
        success : function(data)
        {
         $("#resultado").html(data);
        }
        });
    });

$("#filter").bind("keyup focus",function()
    {   
        var datosS = $(this).val();
        var convenio=$("#convenio").val();
        var fecha=$("#rango").val();
        datosS = datosS.replace('<>/"', '');
        if(datosS.length >= 1)
        {
            $.ajax({
                type : 'POST',
                url  : 'pag/resutaldoBusquedacemI',
                data :  {convenio:convenio,fecha:fecha,datos:datosS},
                success : function(data)
                {
                    $("#resultado").html(data);
                }
            });
            return false;
        }else{
            var convenio=$("#convenio").val();        
            var fecha=$("#rango").val();
            var page = $("#resultado").attr("data-page");
            var fechas=$(".fechas:checked").val();
            $.ajax({
            type : 'POST',
            url  : 'pag/paginacionInstalacion',
            data : {convenio:convenio,page:page,fecha:fecha,fechas:fechas},
            success : function(data)
            {
             $("#resultado").html(data);
            }
            });
    
        }
    });
});
$('#bateriasOModal').on('show.bs.modal', function (e) {
    $('.modal-body').html("");
    var rowid = $(e.relatedTarget).data('id');
    $.ajax({
        type : 'post',
        url : 'modal/instalarDataModal', //Here you will fetch records 
        data :  'rowid='+ rowid, //Pass $id
        success : function(data){
                $('.modal-body').html(data);//Show fetched data from database
        }
    }); 
});

 function reprogramar(){
        var a=confirm()
    }
(function(){
    var actualizarHora = function(){
        // Obtenemos la fecha actual, incluyendo las horas, minutos, segundos, dia de la semana, dia del mes, mes y año;
        var fecha = new Date(),
        horas = fecha.getHours(),
        ampm,
        minutos = fecha.getMinutes(),
        segundos = fecha.getSeconds(),
        diaSemana = fecha.getDay(),
        dia = fecha.getDate(),
        mes = fecha.getMonth(),
        year = fecha.getFullYear();

        // Accedemos a los elementos del DOM para agregar mas adelante sus correspondientes valores
        var pHoras = document.getElementById('horas'),
        pAMPM = document.getElementById('ampm'),
        pMinutos = document.getElementById('minutos'),
        pSegundos = document.getElementById('segundos'),
        pDiaSemana = document.getElementById('diaSemana'),
        pDia = document.getElementById('dia'),
        pMes = document.getElementById('mes'),
        pYear = document.getElementById('year');

        
        // Obtenemos el dia se la semana y lo mostramos
        var semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        pDiaSemana.textContent = semana[diaSemana];

        // Obtenemos el dia del mes
        pDia.textContent = dia;

        // Obtenemos el Mes y año y lo mostramos
        var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        pMes.textContent = meses[mes];
        pYear.textContent = year;

        // Cambiamos las hora de 24 a 12 horas y establecemos si es AM o PM

        if (horas >= 12) {
            horas = horas - 12;
            ampm = 'PM';
        } else {
            ampm = 'AM';
        }

        // Detectamos cuando sean las 0 AM y transformamos a 12 AM
        if (horas == 0 ){
            horas = 12;
        }

        // Si queremos mostrar un cero antes de las horas ejecutamos este condicional
        // if (horas < 10){horas = '0' + horas;}
        pHoras.textContent = horas;
        pAMPM.textContent = ampm;

        // Minutos y Segundos
        if (minutos < 10){ minutos = "0" + minutos; }
        if (segundos < 10){ segundos = "0" + segundos; }

        pMinutos.textContent = minutos;
        pSegundos.textContent = segundos;
    };
    actualizarHora();
    var intervalo = setInterval(actualizarHora, 1000);
}())
</script>
<script>
$(window).load(function() {
    $(".se-pre-con").fadeOut("slow");;
});
</script>
<?php else: ?> 
    <?php $session->salir();
    echo "NO NO NO NO ;"
    ?> 
<?php endif ?>

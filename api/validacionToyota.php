<?php 
    require('../includes/configuracion.php');
    require('../includes/baseDatos.php');

    $http_code_status = 0;
        // Decode raw json for array

    $data = json_decode(file_get_contents('php://input'), true);

    if($_SERVER['REQUEST_METHOD'] === 'PUT' && !empty($data)) {
        $docType = $data["doc-type"];
        $docNumber = $data["doc-number"];
        $name = $data["name"];
        $lastname = $data["lastname"];
        $mainPhone = $data["main-phone"];
        $altPhone = $data["alt-phone"];
        $email = $data["email"];
        $department = $data["department"];
        $province = $data["province"];
        $district = $data["district"];
        $address = $data["address"];
        $insurance = $data["insurance"];

        if($docType && $docNumber && $docType != "" && $docNumber != ""){
            global $db;
            $confirm = $db->query("CALL updateClientFromToyota(
                '$docType', '$docNumber', '$name',
                '$lastname', '$mainPhone',
                '$altPhone', '$email',
                '$department', '$province', '$district', 
                '$address', '$insurance')");

           
            $result = mysqli_fetch_array($confirm, MYSQLI_ASSOC);

            if($result != null){
                $response = [
                    "doc-type" => $result['tipo_documento'],
                    "doc-number" => $result['num_documento'],
                    "name" => $result['nombre_asegurado'],
                    "lastname" => $result['apellido_asegurado'],
                    "mainPhone" => $result['celular'],
                    "altPhone" => $result['celular2'],
                    "email" => $result['correo'],
                    "department" => $result['departamento'],
                    "address" => $result['direccion'],
                    "insurance" => $result['compania'],
                ];
    
                $http_code_status = 200;
                http_response_code($http_code_status);
                echo json_encode($response);
            }else{
                $http_code_status = 400;
                http_response_code($http_code_status);
                echo json_encode([
                    "error" => "¡Campos TIPO DE DOCUMENTO Y NUMERO DE DOCUMENTOS no registrados!"
                ]);
            }

        }else{
            $http_code_status = 400;
            http_response_code($http_code_status);
            echo json_encode([
                "error" => "¡Datos incompletos!"
            ]);
        }
    }else{
        $http_code_status = 404;
        http_response_code($http_code_status);
        echo json_encode([
            "error" => "¡No es una peticion PUT!"
        ]);
    }

    
?>
<?php
require_once("includes/init.php");
require_once("includes/tablaProgramacion.php");

if (isset($_GET['chasis'])) {

  $chasis = $_GET['chasis'];

  if (!empty($chasis)) {
    $clientFechaResh = Operacion::obtenerFechaParaValidacion($chasis);
    $client = Onix::obtenerIdclientePorChasis($chasis);

    if (empty($clientFechaResh->fecha)) {

      if (!empty($client->idcustomer)) {

        $clientSearchId = Operacion::obtenerFechaPorCliente($client->idcustomer, $client->idvehicle);

        if (!empty($clientSearchId)) {
          $clienteObj = Onix::obtenerDatosDesdeIds($clientSearchId->idcliente, $clientSearchId->idvehiculo, $clientSearchId->idlocation);
          //ONYX
          //printResult($nombreCliente, $apellidos, $clientSearchId->fecha, $clientSearchId->hora, $clientSearchId->idoperacion,$clientSearchId->idChequeo, $clientSearchId->nomtec, $clienteObj->chasis, $clienteObj->placa, $clienteObj->marca, $clienteObj->modelo);
          printResult($clienteObj->nombre, $clienteObj->apellidos, $clientSearchId->fecha, $clientSearchId->hora, $clientSearchId->idoperacion, $clientSearchId->idChequeo, $clientSearchId->nomtec, $clienteObj->chasis, $clienteObj->placa, $clienteObj->marca, $clienteObj->modelo, $clientSearchId->tecnicolist);
          die();
        } else {
          echo "El chasis ingresado no tiene programación";
          die();
        }
      }
    }

    if (empty($client->idcustomer)) {
      //RESH
      if (!empty($clientFechaResh->fecha)) {
        printResult($clientFechaResh->nombre, $clientFechaResh->apellidosC, $clientFechaResh->fecha, $clientFechaResh->hora, $clientFechaResh->idoperacion, $clientFechaResh->idChequeo, $clientFechaResh->nomtec, $clientFechaResh->chasis, $clientFechaResh->placa, $clientFechaResh->marca, $clientFechaResh->modelo, $clientFechaResh->tecnicolist);
        die();
      }
    }

    if (empty($clientFechaResh->fecha) && empty($client->idcustomer)) {
      echo "El chasis ingresado no tiene programación";
      die();
    }

    $clientSearchId = Operacion::obtenerFechaPorCliente($client->idcustomer, $client->idvehicle);
    if (empty($clientSearchId)) {
      //RESH
      printResult($clientFechaResh->nombre, $clientFechaResh->apellidosC, $clientFechaResh->fecha, $clientFechaResh->hora, $clientFechaResh->idoperacion, $clientFechaResh->idChequeo, $clientFechaResh->nomtec, $clientFechaResh->chasis, $clientFechaResh->placa, $clientFechaResh->marca, $clientFechaResh->modelo, $clientFechaResh->tecnicolist);
      die();
    }

    if ($clientFechaResh->fecha > $clientSearchId->fecha) {
      //RESH
      printResult($clientFechaResh->nombre, $clientFechaResh->apellidosC, $clientFechaResh->fecha, $clientFechaResh->hora, $clientFechaResh->idoperacion, $clientFechaResh->idChequeo, $clientFechaResh->nomtec, $clientFechaResh->chasis, $clientFechaResh->placa, $clientFechaResh->marca, $clientFechaResh->modelo, $clientFechaResh->tecnicolist);
    } else {
      $clienteObj = Onix::obtenerDatosDesdeIds($clientSearchId->idcliente, $clientSearchId->idvehiculo, $clientSearchId->idlocation);
      //ONYX      
      //printResult($nombreCliente, $apellidos, $clientSearchId->fecha, $clientSearchId->hora, $clientSearchId->idoperacion,$clientSearchId->idChequeo, $clientSearchId->nomtec,$clienteObj->chasis, $clienteObj->placa, $clienteObj->marca, $clienteObj->modelo);
      printResult($clienteObj->nombre, $clienteObj->apellidos, $clientSearchId->fecha, $clientSearchId->hora, $clientSearchId->idoperacion, $clientSearchId->idChequeo, $clientSearchId->nomtec, $clienteObj->chasis, $clienteObj->placa, $clienteObj->marca, $clienteObj->modelo, $clientSearchId->tecnicolist);
    }

  } else {
    echo "<p style='text-align: center;'>Debe ingresar un número de chasis.</p>";
  }
}

function printResult($nombreCliente, $apellidos, $fecha, $hora, $idoperacion, $idChequeo, $tecnico, $chasis, $plac, $marc, $model, $tecnicolist)
{
  echo "<div style='text-align: center;'>";
  echo "<p><strong>ID de operación:</strong> " . "<strong><span style='color: #32CD32; font-weight: bold;'>" . $idoperacion . "</span></strong></p>";
  //style='font-weight: bold;'
  //echo "<p>ID de chequeo: " . $idChequeo . "</p>";
  //echo "<p>Fecha de programación: " . $fecha . "</p>";
  echo "<p><strong>Fecha de programación:</strong> " . "<strong><span style='color: #32CD32; font-weight: bold;'>" . $fecha . "</span></strong></p>";
  echo "<p><strong>Hora de programación:</strong> " . "<strong><span style='color: #32CD32; font-weight: bold;'>" . $hora . "</span></strong></p>";
  // echo "<p>Observación: " . $row['observacion'] . "</p>";
  // echo "<p>Ubicación: " . $row['ubicacion'] . "</p>";
  // echo "<p>Servicio: " . $row['trabajo'] . "</p>";
  echo "<p><strong>Cliente:</strong> " . "<strong><span style='color: #32CD32; font-weight: bold;'>" . $nombreCliente . ' ' . $apellidos . "</span></strong></p>";
  //echo "<p>Apellidos del cliente: " . $apellidos . "</p>";
  echo "<p><strong>Chasis:</strong> " . "<strong><span style='color: #32CD32; font-weight: bold;'>" . $chasis . "</span></strong></p>";
  echo "<p><strong>Placa:</strong> " . "<strong><span style='color: #32CD32; font-weight: bold;'>" . $plac . "</span></strong></p>";
  echo "<p><strong>Marca:</strong> " . "<strong><span style='color: #32CD32; font-weight: bold;'>" . $marc . "</span></strong></p>";
  echo "<p><strong>Modelo:</strong> " . "<strong><span style='color: #32CD32; font-weight: bold;'>" . $model . "</span></strong></p>";
  echo "<p><strong>Técnico asignado:</strong> " . "<strong><span style='color: #32CD32; font-weight: bold;'>" . $tecnico . "</span></strong></p>";
  echo "<p><strong>Técnico:</strong> " . "<strong><span style='color: #32CD32; font-weight: bold;'>" . $tecnicolist . "</span></strong></p>";
  // echo "<p>Apellidos del técnico: " . $row['last_name'] . "</p>";
}
?>